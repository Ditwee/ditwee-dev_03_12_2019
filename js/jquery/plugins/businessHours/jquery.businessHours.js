/**
 jquery.businessHours v1.0.1
 https://github.com/gEndelf/jquery.businessHours

 requirements:
 - jQuery 1.7+

 recommended time-picker:
 - jquery-timepicker 1.2.7+ // https://github.com/jonthornton/jquery-timepicker
 **/

(function($) {
    $.fn.businessHours = function(opts) {
        var defaults = {
            preInit: function() {
            },
            postInit: function() {
            },
            inputDisabled: false,
            checkedColorClass: "WorkingDayState",
            uncheckedColorClass: "RestDayState",
            colorBoxValContainerClass: "colorBoxContainer",
            weekdays: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            operationTime: [
                {},
                {},
                {},
                {},
                {},
                {isActive: false},
                {isActive: false}
            ],
            defaultOperationTimeFrom: '9:00',
            defaultOperationTimeTill: '18:00',
            defaultActive: true,
            //labelOn: "Working day",
            //labelOff: "Day off",
            //labelTimeFrom: "from:",
            //labelTimeTill: "till:",
            containerTmpl: '<div class="clean"/>',
            dayTmpl: '<div class="dayContainer">' +
            '<div data-original-title="" class="colorBox"><input type="checkbox" class="invisible operationState"/></div>' +
            '<div class="weekday"></div>' +
            '<div class="operationDayTimeContainer">' +
            '<div class="operationTime"><input type="text" name="startTime" class="mini-time operationTimeFrom" value=""/></div>' +
            '<div class="operationTime"><input type="text" name="endTime" class="mini-time operationTimeTill" value=""/></div>' +
            '</div></div>'
        };

        var container = $(this);

        function initTimeBox(timeBoxSelector, time, isInputDisabled) {
            timeBoxSelector.val(time);

            if(isInputDisabled) {
                timeBoxSelector.prop('readonly', true);
            }
        }

        var methods = {
            getValueOrDefault: function(val, defaultVal) {
                return (jQuery.type(val) === "undefined" || val == null) ? defaultVal : val;
            },
			convertTime: function(val){
				a = val.split(':');
				return (+a[0]) * 60 * 60 + (+a[1]) * 60; 
			},
            init: function(opts) {
                this.options = $.extend(defaults, opts);
                container.html("");

                if(typeof this.options.preInit === "function") {
                    this.options.preInit();
                }

                this.initView(this.options);

                if(typeof this.options.postInit === "function") {
                    //$('.operationTimeFrom, .operationTimeTill').timepicker(options.timepickerOptions);
                    this.options.postInit();
                }

                return {
                    serialize: function() {
                        var data = [];

                        container.find(".operationState").each(function(num, item) {
                            var isWorkingDay = $(item).prop("checked");
                            var dayContainer = $(item).parents(".dayContainer");
							var isBreak = isWorkingDay && dayContainer.find(".chkBreak").prop("checked");
							
                            data.push({
                                isActive: isWorkingDay,
                                timeFrom: isWorkingDay ? dayContainer.find("[name='startTime']").val() : null,
                                timeTill: isWorkingDay ? dayContainer.find("[name='endTime']").val() : null,
								isBreak : isBreak,
								timeFrom2: isBreak ? dayContainer.find("[name='startTime2']").val() : null,
								timeTill2: isBreak ? dayContainer.find("[name='endTime2']").val() : null,
                            });
                        });
                        return data;
                    }
                };
            },
            initView: function(options) {
                var stateClasses = [options.checkedColorClass, options.uncheckedColorClass];
                var subContainer = container.append($(options.containerTmpl));
                var $this = this;

                for(var i = 0; i < options.weekdays.length; i++) {
                    subContainer.append(options.dayTmpl);
                }

                $.each(options.weekdays, function(pos, weekday) {
                    // populate form
                    var day = options.operationTime[pos];
                    var operationDayNode = container.find(".dayContainer").eq(pos);
                    operationDayNode.find('.weekday').html(weekday);

                    var isWorkingDay = $this.getValueOrDefault(day.isActive, options.defaultActive);
                    operationDayNode.find('.operationState').prop('checked', isWorkingDay);

                    var timeFrom = $this.getValueOrDefault(day.timeFrom, options.defaultOperationTimeFrom);
                    initTimeBox(operationDayNode.find('[name="startTime"]'), timeFrom, options.inputDisabled);

                    var endTime = $this.getValueOrDefault(day.timeTill, options.defaultOperationTimeTill);
                    initTimeBox(operationDayNode.find('[name="endTime"]'), endTime, options.inputDisabled);
					
					var isBreak = $this.getValueOrDefault(day.isBreak, false);
                    operationDayNode.find('.chkBreak').prop('checked', isBreak);
					if(!isBreak){
					
						operationDayNode.find('.divBreak').attr('style','display:none');
					}
					var timeFrom = $this.getValueOrDefault(day.timeFrom2, options.defaultOperationTimeFrom);
                    initTimeBox(operationDayNode.find('[name="startTime2"]'), timeFrom, options.inputDisabled);
					
					var endTime = $this.getValueOrDefault(day.timeTill2, options.defaultOperationTimeTill);
                    initTimeBox(operationDayNode.find('[name="endTime2"]'), endTime, options.inputDisabled);
					

					
                });
				container.find('input[type=text]').keypress(function(e){
					e.preventDefault();
					
				}).change(function(){

					$start1 = $this.convertTime( $(this).parents(".dayContainer").find("[name='startTime']").val() );
					$end1= $this.convertTime( $(this).parents(".dayContainer").find("[name='endTime']").val() );
					$start2 = $this.convertTime( $(this).parents(".dayContainer").find("[name='startTime2']").val() );
					$end2 = $this.convertTime( $(this).parents(".dayContainer").find("[name='endTime2']").val() );
					
					if($end1<$start1)
						$(this).parents(".dayContainer").find("[name='endTime']").val( $(this).parents(".dayContainer").find("[name='startTime']").val() )
					
					if($start2<$end1)
						$(this).parents(".dayContainer").find("[name='startTime2']").val( $(this).parents(".dayContainer").find("[name='endTime']").val() )
					
					if($end2<$start2)
						$(this).parents(".dayContainer").find("[name='endTime2']").val( $(this).parents(".dayContainer").find("[name='startTime2']").val() )
					
					
				}).focusout(function(){$(this).trigger("change")}).focusin(function(){$(this).trigger("change")});
				container.find(".chkBreak").change(function(){
					var checkbox = $(this);
					if(	checkbox.prop("checked")) {
						
						checkbox.parents('.dayContainer').find(".divBreak").show();
					}
					else{
						checkbox.parents('.dayContainer').find(".divBreak").hide();
					}
				
				
				}).trigger("change");
				
				
                container.find(".operationState").change(function() {
                    var checkbox = $(this);
                    var boxClass = options.checkedColorClass;
                    var timeControlDisabled = false;

                    if(!checkbox.prop("checked")) {
                        // disabled
                        boxClass = options.uncheckedColorClass;
                        timeControlDisabled = true;
                    }

                    checkbox.parents(".colorBox").removeClass(stateClasses.join(' ')).addClass(boxClass);
                    checkbox.parents(".dayContainer").find(".operationTime").toggle(!timeControlDisabled);
                }).trigger("change");

                if(!options.inputDisabled) {
                    container.find(".colorBox").on("click", function() {
                        var checkbox = $(this).find(".operationState");
                        checkbox.prop("checked", !checkbox.prop('checked')).trigger("change");
                    });
                }
            }
        };

        return methods.init(opts);
    };
})(jQuery);
