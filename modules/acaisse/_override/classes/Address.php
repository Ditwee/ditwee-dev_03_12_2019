<?php
class Address extends AddressCore
{
	const PRESTATILL_TICKET_POSITION_TOP = 0;
	const PRESTATILL_TICKET_POSITION_BOTTOM = 1; 
	
	public $is_pointshop = 0;
	
	public $common_customer_info = '';
	public $common_delivery_info = '';
	public $common_invoice_info = '';
	public $common_picture_path = '';
	public $common_currency_change = '{}'; //{CHF:1.2,USD:0.8}
	
	//ticket
	public $ticket_header = '';
	public $ticket_customer_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $ticket_delivery_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $ticket_invoice_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $ticket_picture_path = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $ticket_currency_change = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $ticket_footer = '';
	
	//slip
	public $slip_header = '';
	public $slip_customer_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $slip_delivery_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $slip_invoice_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $slip_footer = '';
	
	//preorder
	public $preorder_header = '';
	public $preorder_customer_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $preorder_delivery_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $preorder_invoice_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $preorder_footer = '';
	
	// Kdo
	public $kdo_footer = '';
	public $kdo_header = '';
	
	//estimate
	public $estimate_header = '';
	public $estimate_customer_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $estimate_delivery_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $estimate_invoice_position = Address::PRESTATILL_TICKET_POSITION_TOP;
	public $estimate_footer = '';
	
	
	//public function __construct($id_address = null, $id_lang = null)
	
	
    public function __construct($id = null, $id_lang = null)
    {
    	/**
		 * Add in Prestatill 2.3
		 */   
        self::$definition['fields']['is_pointshop'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );   
		
		
		// common tickets fields
        self::$definition['fields']['common_customer_info'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );  
        self::$definition['fields']['common_delivery_info'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );    
        self::$definition['fields']['common_invoice_info'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );   		   
        self::$definition['fields']['common_picture_path'] = array(
            'type' => self::TYPE_STRING,
            'validate' => 'isString'
        );		
        self::$definition['fields']['common_currency_change'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
		);
		
		// ticket fields
        self::$definition['fields']['ticket_header'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );  
        self::$definition['fields']['ticket_customer_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );   
        self::$definition['fields']['ticket_delivery_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );   
        self::$definition['fields']['ticket_invoice_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );		
        self::$definition['fields']['ticket_footer'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );		
		
		
		// slip fields
        self::$definition['fields']['slip_header'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );  
        self::$definition['fields']['slip_customer_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );   
        self::$definition['fields']['slip_delivery_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );   
        self::$definition['fields']['slip_invoice_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );		
        self::$definition['fields']['slip_footer'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );
		
		
		
		// preorder fields
        self::$definition['fields']['preorder_header'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );  
        self::$definition['fields']['preorder_customer_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );   
        self::$definition['fields']['preorder_delivery_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );   
        self::$definition['fields']['preorder_invoice_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );		
        self::$definition['fields']['preorder_footer'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );
		
		// kdo_fields
		self::$definition['fields']['kdo_header'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );  
		self::$definition['fields']['kdo_footer'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );  
		
		// estimate fields
        self::$definition['fields']['estimate_header'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );  
        self::$definition['fields']['estimate_customer_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );   
        self::$definition['fields']['estimate_delivery_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );   
        self::$definition['fields']['estimate_invoice_position'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );		
        self::$definition['fields']['estimate_footer'] = array(
        	'type' => self::TYPE_HTML ,
        	'validate' => 'isCleanHtml'
        );
		parent::__construct($id, $id_lang);
    }

	static public function getPrestatillPointshop() {
		$sql = 'SELECT id_address FROM ' . _DB_PREFIX_ . 'address WHERE is_pointshop != 0';
		$ids_address = Db::getInstance()->executeS($sql);
		$addresses = array();
		foreach($ids_address as $id_address)
		{			
			$addresses[] = new Address($id_address['id_address']);
		}		
		
		return $addresses;
	}
}