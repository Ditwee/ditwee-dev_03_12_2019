<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Cart extends CartCore
{
	public $id_pointshop=0;		//online website order
	public $id_ticket=0;		//online website order

	public function __construct($id = null, $id_lang = null)
	{
		Cart::$definition['fields']['id_pointshop']=array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => false);
		Cart::$definition['fields']['id_ticket']=array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => false);
		parent::__construct($id, $id_lang);
	}
    
    
    /**
     * Update product quantity
     *
     * @param int $quantity Quantity to add (or substract)
     * @param int $id_product Product ID
     * @param int $id_product_attribute Attribute ID if needed
     * @param string $operator Indicate if quantity must be increased or decreased
     */
    public function updateQty(
        $quantity,
        $id_product,
        $id_product_attribute = null,
        $id_customization = false,
        $operator = 'up',
        $id_address_delivery = 0,
        Shop $shop = null,
        $auto_add_cart_rule = true,
        $skipAvailabilityCheckOutOfStock = false
    ) {
    	
		if ( _PS_VERSION_ > '1.7.3.0') 
		{
	       if (!$shop) {
            $shop = Context::getContext()->shop;
	        }
	
	        if (Context::getContext()->customer->id) {
	            if ($id_address_delivery == 0 && (int)$this->id_address_delivery) { // The $id_address_delivery is null, use the cart delivery address
	                $id_address_delivery = $this->id_address_delivery;
	            } elseif ($id_address_delivery == 0) { // The $id_address_delivery is null, get the default customer address
	                $id_address_delivery = (int)Address::getFirstCustomerAddressId((int)Context::getContext()->customer->id);
	            } elseif (!Customer::customerHasAddress(Context::getContext()->customer->id, $id_address_delivery)) { // The $id_address_delivery must be linked with customer
	                $id_address_delivery = 0;
	            }
	        }
	
	        $quantity = (int)$quantity;
	        $id_product = (int)$id_product;
	        $id_product_attribute = (int)$id_product_attribute;
	        $product = new Product($id_product, false, Configuration::get('PS_LANG_DEFAULT'), $shop->id);
	
	        if ($id_product_attribute) {
	            $combination = new Combination((int)$id_product_attribute);
	            if ($combination->id_product != $id_product) {
	                return false;
	            }
	        }
	
	        /* If we have a product combination, the minimal quantity is set with the one of this combination */
	        if (!empty($id_product_attribute)) {
	            $minimal_quantity = (int)Attribute::getAttributeMinimalQty($id_product_attribute);
	        } else {
	            $minimal_quantity = (int)$product->minimal_quantity;
	        }
	
	        if (!Validate::isLoadedObject($product)) {
	            die(Tools::displayError());
	        }
	
	        if (isset(self::$_nbProducts[$this->id])) {
	            unset(self::$_nbProducts[$this->id]);
	        }
	
	        if (isset(self::$_totalWeight[$this->id])) {
	            unset(self::$_totalWeight[$this->id]);
	        }
	
	        $data = array(
	            'cart' => $this,
	            'product' => $product,
	            'id_product_attribute' => $id_product_attribute,
	            'id_customization' => $id_customization,
	            'quantity' => $quantity,
	            'operator' => $operator,
	            'id_address_delivery' => $id_address_delivery,
	            'shop' => $shop,
	            'auto_add_cart_rule' => $auto_add_cart_rule,
	        );
	
	        /* @deprecated deprecated since 1.6.1.1 */
	        // Hook::exec('actionBeforeCartUpdateQty', $data);
	        Hook::exec('actionCartUpdateQuantityBefore', $data);
	
	        if ((int)$quantity <= 0) {
	            return $this->deleteProduct($id_product, $id_product_attribute, (int)$id_customization);
	        } elseif (!$product->available_for_order
	                || (Configuration::isCatalogMode() && !defined('_PS_ADMIN_DIR_'))
	        ) {
	            return false;
	        } else {
	            /* Check if the product is already in the cart */
	            $cartProductQuantity = $this->getProductQuantity($id_product, $id_product_attribute, (int)$id_customization, (int)$id_address_delivery);
	
	            /* Update quantity if product already exist */
	            if (/******* PRESTATILL************/ !$product->is_generic_product /**********/ && !empty($cartProductQuantity['quantity'])) {
	                $productQuantity = Product::getQuantity($id_product, $id_product_attribute, null, $this);
	                $availableOutOfStock = Product::isAvailableWhenOutOfStock($product->out_of_stock);
	
	                if ($operator == 'up') {
	                    $updateQuantity = '+ ' . $quantity;
	                    $newProductQuantity = $productQuantity - $quantity;
	
	                    if ($newProductQuantity < 0 && !$availableOutOfStock && !$skipAvailabilityCheckOutOfStock) {
	                        return false;
	                    }
	                } else if ($operator == 'down') {
	                    $cartFirstLevelProductQuantity = $this->getProductQuantity((int) $id_product, (int) $id_product_attribute, $id_customization);
	                    $updateQuantity = '- ' . $quantity;
	                    $newProductQuantity = $productQuantity + $quantity;
	
	                    if ($cartFirstLevelProductQuantity['quantity'] <= 1) {
	                        return $this->deleteProduct((int)$id_product, (int)$id_product_attribute, (int)$id_customization);
	                    }
	                } else {
	                    return false;
	                }
	                Db::getInstance()->execute(
	                    'UPDATE `'._DB_PREFIX_.'cart_product`
	                    SET `quantity` = `quantity` ' . $updateQuantity . '
	                    WHERE `id_product` = '.(int)$id_product.
	                    ' AND `id_customization` = '.(int)$id_customization.
	                    (!empty($id_product_attribute) ? ' AND `id_product_attribute` = '.(int)$id_product_attribute : '').'
	                    AND `id_cart` = '.(int)$this->id.(Configuration::get('PS_ALLOW_MULTISHIPPING') && $this->isMultiAddressDelivery() ? ' AND `id_address_delivery` = '.(int)$id_address_delivery : '').'
	                    LIMIT 1'
	                );
	            } elseif ($operator == 'up') {
	                /* Add product to the cart */
	
	                $sql = 'SELECT stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity
	                        FROM '._DB_PREFIX_.'product p
	                        '.Product::sqlStock('p', $id_product_attribute, true, $shop).'
	                        WHERE p.id_product = '.$id_product;
	
	                $result2 = Db::getInstance()->getRow($sql);
	
	                // Quantity for product pack
	                if (Pack::isPack($id_product)) {
	                    $result2['quantity'] = Pack::getQuantity($id_product, $id_product_attribute, null, $this);
	                }
	
	                if (!Product::isAvailableWhenOutOfStock((int)$result2['out_of_stock']) && !$skipAvailabilityCheckOutOfStock) {
	                    if ((int)$quantity > $result2['quantity']) {
	                        return false;
	                    }
	                }
	
	                if ((int)$quantity < $minimal_quantity) {
	                    return -1;
	                }
	
	                $result_add = Db::getInstance()->insert('cart_product', array(
	                    'id_product' =>            (int)$id_product,
	                    'id_product_attribute' =>    (int)$id_product_attribute,
	                    'id_cart' =>                (int)$this->id,
	                    'id_address_delivery' =>    (int)$id_address_delivery,
	                    'id_shop' =>                $shop->id,
	                    'quantity' =>                (int)$quantity,
	                    'date_add' =>                date('Y-m-d H:i:s'),
	                    'id_customization' =>       (int)$id_customization,
	                ));
	
	                if (!$result_add) {
	                    return false;
	                }
	            }
	        }
	
	        // refresh cache of self::_products
	        $this->_products = $this->getProducts(true);
	        $this->update();
	        $context = Context::getContext()->cloneContext();
	        $context->cart = $this;
	        Cache::clean('getContextualValue_*');
	        if ($auto_add_cart_rule) {
	            CartRule::autoAddToCart($context);
	        }
	
	        if ($product->customizable) {
	            return $this->_updateCustomizationQuantity((int)$quantity, (int)$id_customization, (int)$id_product, (int)$id_product_attribute, (int)$id_address_delivery, $operator);
	        } else {
	            return true;
	        }
		} 
		else
		{
			if (!$shop) {
            $shop = Context::getContext()->shop;
	        }
	
	        if (Context::getContext()->customer->id) {
	            if ($id_address_delivery == 0 && (int)$this->id_address_delivery) { // The $id_address_delivery is null, use the cart delivery address
	                $id_address_delivery = $this->id_address_delivery;
	            } elseif ($id_address_delivery == 0) { // The $id_address_delivery is null, get the default customer address
	                $id_address_delivery = (int)Address::getFirstCustomerAddressId((int)Context::getContext()->customer->id);
	            } elseif (!Customer::customerHasAddress(Context::getContext()->customer->id, $id_address_delivery)) { // The $id_address_delivery must be linked with customer
	                $id_address_delivery = 0;
	            }
	        }
	
	        $quantity = (int)$quantity;
	        $id_product = (int)$id_product;
	        $id_product_attribute = (int)$id_product_attribute;
	        $product = new Product($id_product, false, Configuration::get('PS_LANG_DEFAULT'), $shop->id);
	
	        if ($id_product_attribute) {
	            $combination = new Combination((int)$id_product_attribute);
	            if ($combination->id_product != $id_product) {
	                return false;
	            }
	        }
	
	        /* If we have a product combination, the minimal quantity is set with the one of this combination */
	        if (!empty($id_product_attribute)) {
	            $minimal_quantity = (int)Attribute::getAttributeMinimalQty($id_product_attribute);
	        } else {
	            $minimal_quantity = (int)$product->minimal_quantity;
	        }
	
	        if (!Validate::isLoadedObject($product)) {
	            die(Tools::displayError());
	        }
	
	        if (isset(self::$_nbProducts[$this->id])) {
	            unset(self::$_nbProducts[$this->id]);
	        }
	
	        if (isset(self::$_totalWeight[$this->id])) {
	            unset(self::$_totalWeight[$this->id]);
	        }
	
	        Hook::exec('actionBeforeCartUpdateQty', array(
	            'cart' => $this,
	            'product' => $product,
	            'id_product_attribute' => $id_product_attribute,
	            'id_customization' => $id_customization,
	            'quantity' => $quantity,
	            'operator' => $operator,
	            'id_address_delivery' => $id_address_delivery,
	            'shop' => $shop,
	            'auto_add_cart_rule' => $auto_add_cart_rule,
	        ));
	
	        if ((int)$quantity <= 0) {
	            return $this->deleteProduct($id_product, $id_product_attribute, (int)$id_customization, 0, $auto_add_cart_rule);
	        } elseif (!$product->available_for_order || (Configuration::get('PS_CATALOG_MODE') && !defined('_PS_ADMIN_DIR_'))) {
	            return false;
	        } else {
	            /* Check if the product is already in the cart */
	            $result = $this->containsProduct($id_product, $id_product_attribute, (int)$id_customization, (int)$id_address_delivery);
	
	            /* Update quantity if product already exist */
	            if (/******* PRESTATILL************/ !$product->is_generic_product /**********/ && $result) {
	                if ($operator == 'up') {
	                    $sql = 'SELECT stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity
								FROM '._DB_PREFIX_.'product p
								'.Product::sqlStock('p', $id_product_attribute, true, $shop).'
								WHERE p.id_product = '.$id_product;
	
	                    $result2 = Db::getInstance()->getRow($sql);
	                    $product_qty = (int)$result2['quantity'];
	                    // Quantity for product pack
	                    if (Pack::isPack($id_product)) {
	                        $product_qty = Pack::getQuantity($id_product, $id_product_attribute);
	                    }
	                    $new_qty = (int)$result['quantity'] + (int)$quantity;
	                    $qty = '+ '.(int)$quantity;
	
	                    if (!Product::isAvailableWhenOutOfStock((int)$result2['out_of_stock'])) {
	                        if ($new_qty > $product_qty) {
	                            return false;
	                        }
	                    }
	                } elseif ($operator == 'down') {
	                    $qty = '- '.(int)$quantity;
	                    $new_qty = (int)$result['quantity'] - (int)$quantity;
	                    if ($new_qty < $minimal_quantity && $minimal_quantity > 1) {
	                        return -1;
	                    }
	                } else {
	                    return false;
	                }
	
	                /* Delete product from cart */
	                if ($new_qty <= 0) {
	                    return $this->deleteProduct((int)$id_product, (int)$id_product_attribute, (int)$id_customization, 0, $auto_add_cart_rule);
	                } elseif ($new_qty < $minimal_quantity) {
	                    return -1;
	                } else {
	                    Db::getInstance()->execute('
							UPDATE `'._DB_PREFIX_.'cart_product`
							SET `quantity` = `quantity` '.$qty.', `date_add` = NOW()
							WHERE `id_product` = '.(int)$id_product.
	                        (!empty($id_product_attribute) ? ' AND `id_product_attribute` = '.(int)$id_product_attribute : '').'
							AND `id_cart` = '.(int)$this->id.(Configuration::get('PS_ALLOW_MULTISHIPPING') && $this->isMultiAddressDelivery() ? ' AND `id_address_delivery` = '.(int)$id_address_delivery : '').'
							LIMIT 1'
	                    );
	                }
	            }
	            /* Add product to the cart */
	            elseif ($operator == 'up') {
	                $sql = 'SELECT stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity
							FROM '._DB_PREFIX_.'product p
							'.Product::sqlStock('p', $id_product_attribute, true, $shop).'
							WHERE p.id_product = '.$id_product;
	
	                $result2 = Db::getInstance()->getRow($sql);
	
	                // Quantity for product pack
	                if (Pack::isPack($id_product)) {
	                    $result2['quantity'] = Pack::getQuantity($id_product, $id_product_attribute);
	                }
	
	                if (!Product::isAvailableWhenOutOfStock((int)$result2['out_of_stock'])) {
	                    if ((int)$quantity > $result2['quantity']) {
	                        return false;
	                    }
	                }
	
	                if ((int)$quantity < $minimal_quantity) {
	                    return -1;
	                }
	
	                $result_add = Db::getInstance()->insert('cart_product', array(
	                    'id_product' =>            (int)$id_product,
	                    'id_product_attribute' =>    (int)$id_product_attribute,
	                    'id_cart' =>                (int)$this->id,
	                    'id_address_delivery' =>    (int)$id_address_delivery,
	                    'id_shop' =>                $shop->id,
	                    'quantity' =>                (int)$quantity,
	                    'date_add' =>                date('Y-m-d H:i:s')
	                ));
	
	                if (!$result_add) {
	                    return false;
	                }
	            }
	        }
	
	        // refresh cache of self::_products
	        $this->_products = $this->getProducts(true);
	        $this->update();
	        $context = Context::getContext()->cloneContext();
	        $context->cart = $this;
	        Cache::clean('getContextualValue_*');
	        if ($auto_add_cart_rule) {
	            CartRule::autoAddToCart($context);
	        }
	
	        if ($product->customizable) {
	            return $this->_updateCustomizationQuantity((int)$quantity, (int)$id_customization, (int)$id_product, (int)$id_product_attribute, (int)$id_address_delivery, $operator);
	        } else {
	            return true;
	        }
		}
    }

	/**
	 * Get the delivery option selected, or if no delivery option was selected,
	 * the cheapest option for each address
	 *
	 * @param Country|null $default_country
	 * @param bool         $dontAutoSelectOptions
	 * @param bool         $use_cache
	 *
	 * @return array|bool|mixed Delivery option
	 */
	
	public function getDeliveryOption($default_country = null, $dontAutoSelectOptions = false, $use_cache = true)
	{
		static $cache = array();
		$cache_id = (int)(is_object($default_country) ? $default_country->id : 0).'-'.(int)$dontAutoSelectOptions;
		if (isset($cache[$cache_id]) && $use_cache)
			return $cache[$cache_id];

		$delivery_option_list = $this->getDeliveryOptionList($default_country);

		// The delivery option was selected
		if (isset($this->delivery_option) && $this->delivery_option != '')
		{
			$delivery_option = Tools::unSerialize($this->delivery_option);
			$validated = true;
			foreach ($delivery_option as $id_address => $key)
				if (!isset($delivery_option_list[$id_address][$key]))
				{
					$validated = false;
					break;
				}

			if ($validated)
			{
				$cache[$cache_id] = $delivery_option;
				return $delivery_option;
			}
		}

		if ($dontAutoSelectOptions)
			return false;

		// No delivery option selected or delivery option selected is not valid, get the better for all options
		$delivery_option = array();
		foreach ($delivery_option_list as $id_address => $options)
		{
			foreach ($options as $key => $option)
			
			// OVERRIDE PRESTATILL
			if(Configuration::get('ACAISSE_FORCE_DEFAULT_CARRIER') == true && $this->id_pointshop > 0)
			{
				if ($option['is_best_price'])
				{
					$delivery_option[$id_address] = $key;
					break;
				}
			}
			else 
			{
				if (Configuration::get('PS_CARRIER_DEFAULT') == -1 && $option['is_best_price'])
				{
					$delivery_option[$id_address] = $key;
					break;
				}
				elseif (Configuration::get('PS_CARRIER_DEFAULT') == -2 && $option['is_best_grade'])
				{
					$delivery_option[$id_address] = $key;
					break;
				}
				elseif ($option['unique_carrier'] && in_array(Configuration::get('PS_CARRIER_DEFAULT'), array_keys($option['carrier_list'])))
				{
					$delivery_option[$id_address] = $key;
					break;
				}

				reset($options);
				if (!isset($delivery_option[$id_address]))
					$delivery_option[$id_address] = key($options);
			}
		}

		$cache[$cache_id] = $delivery_option;

		return $delivery_option;
	}

	public function getOrderTotal($with_taxes = true, $type = Cart::BOTH, $products = null, $id_carrier = null, $use_cache = true)
	{
		// Dependencies
		$address_factory 	= Adapter_ServiceLocator::get('Adapter_AddressFactory');
		$price_calculator 	= Adapter_ServiceLocator::get('Adapter_ProductPriceCalculator');
		$configuration 		= Adapter_ServiceLocator::get('Core_Business_ConfigurationInterface');

		$ps_tax_address_type = $configuration->get('PS_TAX_ADDRESS_TYPE');
		$ps_use_ecotax = $configuration->get('PS_USE_ECOTAX');
		$ps_round_type = $configuration->get('PS_ROUND_TYPE');
		$ps_ecotax_tax_rules_group_id = $configuration->get('PS_ECOTAX_TAX_RULES_GROUP_ID');
		$compute_precision = $configuration->get('_PS_PRICE_COMPUTE_PRECISION_');
		
		/* OVERRIDE PRESTATILL */
		if($this->id_pointshop > 0){
			$compute_precision = 5;//$configuration->get('_PS_PRICE_COMPUTE_PRECISION_');
		}
		/* END OVERRIDE PRESTATILL */

		if (!$this->id)
			return 0;

		$type = (int)$type;
		$array_type = array(
			Cart::ONLY_PRODUCTS,
			Cart::ONLY_DISCOUNTS,
			Cart::BOTH,
			Cart::BOTH_WITHOUT_SHIPPING,
			Cart::ONLY_SHIPPING,
			Cart::ONLY_WRAPPING,
			Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING,
			Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING,
		);

		// Define virtual context to prevent case where the cart is not the in the global context
		$virtual_context = Context::getContext()->cloneContext();
		$virtual_context->cart = $this;

		if (!in_array($type, $array_type))
			die(Tools::displayError());

		$with_shipping = in_array($type, array(Cart::BOTH, Cart::ONLY_SHIPPING));

		// if cart rules are not used
		if ($type == Cart::ONLY_DISCOUNTS && !CartRule::isFeatureActive())
			return 0;

		// no shipping cost if is a cart with only virtuals products
		$virtual = $this->isVirtualCart();
		if ($virtual && $type == Cart::ONLY_SHIPPING)
			return 0;

		if ($virtual && $type == Cart::BOTH)
			$type = Cart::BOTH_WITHOUT_SHIPPING;

		if ($with_shipping || $type == Cart::ONLY_DISCOUNTS)
		{
			if (is_null($products) && is_null($id_carrier))
				$shipping_fees = $this->getTotalShippingCost(null, (bool)$with_taxes);
			else
				$shipping_fees = $this->getPackageShippingCost($id_carrier, (bool)$with_taxes, null, $products);
		}
		else
			$shipping_fees = 0;

		if ($type == Cart::ONLY_SHIPPING)
			return $shipping_fees;

		if ($type == Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING)
			$type = Cart::ONLY_PRODUCTS;

		$param_product = true;
		if (is_null($products))
		{
			$param_product = false;
			$products = $this->getProducts();
		}

		if ($type == Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING)
		{
			foreach ($products as $key => $product)
				if ($product['is_virtual'])
					unset($products[$key]);
			$type = Cart::ONLY_PRODUCTS;
		}

		$order_total = 0;
		if (Tax::excludeTaxeOption())
			$with_taxes = false;

		$products_total = array();
		$ecotax_total = 0;

		foreach ($products as $product) // products refer to the cart details
		{
			if ($virtual_context->shop->id != $product['id_shop'])
				$virtual_context->shop = new Shop((int)$product['id_shop']);

			if ($ps_tax_address_type == 'id_address_invoice')
				$id_address = (int)$this->id_address_invoice;
			else
				$id_address = (int)$product['id_address_delivery']; // Get delivery address of the product from the cart
			if (!$address_factory->addressExists($id_address))
				$id_address = null;

			// The $null variable below is not used,
			// but it is necessary to pass it to getProductPrice because
			// it expects a reference.
			$null = null;
			$price = $price_calculator->getProductPrice(
				(int)$product['id_product'],
				$with_taxes,
				(int)$product['id_product_attribute'],
				6,
				null,
				false,
				true,
				$product['cart_quantity'],
				false,
				(int)$this->id_customer ? (int)$this->id_customer : null,
				(int)$this->id,
				$id_address,
				$null,
				$ps_use_ecotax,
				true,
				$virtual_context
			);

			$address = $address_factory->findOrCreate($id_address, true);

			if ($with_taxes)
			{
				$id_tax_rules_group = Product::getIdTaxRulesGroupByIdProduct((int)$product['id_product'], $virtual_context);
				$tax_calculator = TaxManagerFactory::getManager($address, $id_tax_rules_group)->getTaxCalculator();
			}
			else
				$id_tax_rules_group = 0;

			if (in_array($ps_round_type, array(Order::ROUND_ITEM, Order::ROUND_LINE)))
			{
				if (!isset($products_total[$id_tax_rules_group]))
					$products_total[$id_tax_rules_group] = 0;
			}
			else
				if (!isset($products_total[$id_tax_rules_group.'_'.$id_address]))
					$products_total[$id_tax_rules_group.'_'.$id_address] = 0;

			switch ($ps_round_type)
			{
				case Order::ROUND_TOTAL:
					$products_total[$id_tax_rules_group.'_'.$id_address] += $price * (int)$product['cart_quantity'];
					break;

				case Order::ROUND_LINE:
					$product_price = $price * $product['cart_quantity'];
					$products_total[$id_tax_rules_group] += Tools::ps_round($product_price, $compute_precision);
					break;

				case Order::ROUND_ITEM:
				default:
					$product_price = /*$with_taxes ? $tax_calculator->addTaxes($price) : */$price;
					$products_total[$id_tax_rules_group] += Tools::ps_round($product_price, $compute_precision) * (int)$product['cart_quantity'];
					break;
			}
		}

		foreach ($products_total as $key => $price)
			$order_total += $price;

		$order_total_products = $order_total;

		if ($type == Cart::ONLY_DISCOUNTS)
			$order_total = 0;

		// Wrapping Fees
		$wrapping_fees = 0;

		// With PS_ATCP_SHIPWRAP on the gift wrapping cost computation calls getOrderTotal with $type === Cart::ONLY_PRODUCTS, so the flag below prevents an infinite recursion.
		$include_gift_wrapping = (!$configuration->get('PS_ATCP_SHIPWRAP') || $type !== Cart::ONLY_PRODUCTS);

		if ($this->gift && $include_gift_wrapping)
			$wrapping_fees = Tools::convertPrice(Tools::ps_round($this->getGiftWrappingPrice($with_taxes), $compute_precision), Currency::getCurrencyInstance((int)$this->id_currency));
		if ($type == Cart::ONLY_WRAPPING)
			return $wrapping_fees;

		$order_total_discount = 0;
		$order_shipping_discount = 0;
		if (!in_array($type, array(Cart::ONLY_SHIPPING, Cart::ONLY_PRODUCTS)) && CartRule::isFeatureActive())
		{
			// First, retrieve the cart rules associated to this "getOrderTotal"
			if ($with_shipping || $type == Cart::ONLY_DISCOUNTS)
				$cart_rules = $this->getCartRules(CartRule::FILTER_ACTION_ALL);
			else
			{
				$cart_rules = $this->getCartRules(CartRule::FILTER_ACTION_REDUCTION);
				// Cart Rules array are merged manually in order to avoid doubles
				foreach ($this->getCartRules(CartRule::FILTER_ACTION_GIFT) as $tmp_cart_rule)
				{
					$flag = false;
					foreach ($cart_rules as $cart_rule)
						if ($tmp_cart_rule['id_cart_rule'] == $cart_rule['id_cart_rule'])
							$flag = true;
					if (!$flag)
						$cart_rules[] = $tmp_cart_rule;
				}
			}

			$id_address_delivery = 0;
			if (isset($products[0]))
				$id_address_delivery = (is_null($products) ? $this->id_address_delivery : $products[0]['id_address_delivery']);
			$package = array('id_carrier' => $id_carrier, 'id_address' => $id_address_delivery, 'products' => $products);

			// Then, calculate the contextual value for each one
			$flag = false;
			foreach ($cart_rules as $cart_rule)
			{
				// If the cart rule offers free shipping, add the shipping cost
				if (($with_shipping || $type == Cart::ONLY_DISCOUNTS) && $cart_rule['obj']->free_shipping && !$flag)
				{
					$order_shipping_discount = (float)Tools::ps_round($cart_rule['obj']->getContextualValue($with_taxes, $virtual_context, CartRule::FILTER_ACTION_SHIPPING, ($param_product ? $package : null), $use_cache), $compute_precision);
					$flag = true;
				}

				// If the cart rule is a free gift, then add the free gift value only if the gift is in this package
				if ((int)$cart_rule['obj']->gift_product)
				{
					$in_order = false;
					if (is_null($products))
						$in_order = true;
					else
						foreach ($products as $product)
							if ($cart_rule['obj']->gift_product == $product['id_product'] && $cart_rule['obj']->gift_product_attribute == $product['id_product_attribute'])
								$in_order = true;

					if ($in_order)
						$order_total_discount += $cart_rule['obj']->getContextualValue($with_taxes, $virtual_context, CartRule::FILTER_ACTION_GIFT, $package, $use_cache);
				}

				// If the cart rule offers a reduction, the amount is prorated (with the products in the package)
				if ($cart_rule['obj']->reduction_percent > 0 || $cart_rule['obj']->reduction_amount > 0)
					$order_total_discount += Tools::ps_round($cart_rule['obj']->getContextualValue($with_taxes, $virtual_context, CartRule::FILTER_ACTION_REDUCTION, $package, $use_cache), $compute_precision);
			}
			$order_total_discount = min(Tools::ps_round($order_total_discount, 2), (float)$order_total_products) + (float)$order_shipping_discount;
			$order_total -= $order_total_discount;
		}

		if ($type == Cart::BOTH)
			$order_total += $shipping_fees + $wrapping_fees;

		if ($order_total < 0 && $type != Cart::ONLY_DISCOUNTS)
			return 0;

		if ($type == Cart::ONLY_DISCOUNTS)
			return $order_total_discount;

		return Tools::ps_round((float)$order_total, $compute_precision);
	}
}
