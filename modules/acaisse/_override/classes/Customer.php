<?php

class Customer extends CustomerCore
{
	
	public $id_pointshop = ' ';
	
	/*
	 * IS THE CLIENT COMMON FOR ALL THE POINTSHOPS
	 */
	public $is_common = 1;		

	public function __construct($id = null)
	{
		Customer::$definition['fields']['id_pointshop']=array('type' => self::TYPE_STRING, 'required' => false);
		Customer::$definition['fields']['is_common']=array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => false);
		parent::__construct($id);
	}
	
	
}