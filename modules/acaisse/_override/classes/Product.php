<?php

class Product extends ProductCore
{
    /** @var string list of pointshop id */
    public $not_available_in_pointshops = '';
    
    /**
     * added in 1.4.2
     */
    public $remove_after_payment_validation = 0;
    /**
     * added in 1.4.3
     */
    public $is_generic_product = 0;
	
	/**
     * added in module prestatillgiftcard 1.0
     */

    public $prestatillgiftcard_amount = 0;
    public $prestatillgiftcard_minimum_amount = 0;
    public $prestatillgiftcard_partial_usage = 1;
    public $prestatillgiftcard_validity_period = '+1 year';

    public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, \Context $context = null)
    {
        
        self::$definition['fields']['not_available_in_pointshops'] = array(
            'type' => self::TYPE_STRING,
            'validate' => 'isString'
        );
        //added in 1.4.2
        self::$definition['fields']['remove_after_payment_validation'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );
        //added in 1.4.3
        self::$definition['fields']['is_generic_product'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );
		
        //added in module prestatillgiftcard 1.0
        self::$definition['fields']['prestatillgiftcard_validity_period'] = array(
            'type' => self::TYPE_STRING,
            'validate' => 'isString'
        );
        self::$definition['fields']['prestatillgiftcard_amount'] = array(
            'type' => self::TYPE_FLOAT,
            'validate' => 'isFloat'
        );
        self::$definition['fields']['prestatillgiftcard_partial_usage'] = array(
            'type' => self::TYPE_FLOAT,
            'validate' => 'isFloat'
        );
        self::$definition['fields']['prestatillgiftcard_minimum_amount'] = array(
            'type' => self::TYPE_FLOAT,
            'validate' => 'isFloat'
        );
		
        parent::__construct($id_product, $full, $id_lang, $id_shop, $context);        
    }
}