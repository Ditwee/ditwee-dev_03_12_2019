<?php


class Order extends OrderCore
{

    public $id_pointshop = 0;  //online website order
    public $id_ticket = 0;  //online website order
    public $id_employee = 0;  
    
    public function __construct($id = null, $id_lang = null)
    {
        self::$definition['fields']['id_pointshop'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => false);
        self::$definition['fields']['id_ticket'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => false);
        self::$definition['fields']['id_employee'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => false);
        parent::__construct($id, $id_lang);
    }
	
	/**
     * Get customer orders number 
     *
     * @param int $id_customer Customer id
	 * @param Based on id_pointshop if configuration pointshop
     * @return array Customer orders number
     */
    public static function getCustomerNbOrders($id_customer, $id_pointshop = false)
    {
        $sql = 'SELECT COUNT(`id_order`) AS nb
				FROM `'._DB_PREFIX_.'orders`
				WHERE `id_customer` = '.(int)$id_customer
                    .Shop::addSqlRestriction();
		
		if($id_pointshop != false)
		{
			$sql .=' AND id_pointshop = '.$id_pointshop;	
		}
					
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);

        return isset($result['nb']) ? $result['nb'] : 0;
    }

}
