<?php

class OrderDetail extends OrderDetailCore
{
    /**
     * added in module prestatillgiftcard 1.1
     */
    public $is_giftcard = 0;

    public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, \Context $context = null)
    {
    	//added in module prestatillgiftcard 1.1
        self::$definition['fields']['is_giftcard'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );
        
        parent::__construct($id_product, $full, $id_lang, $id_shop, $context);        
    }
	
	/*
	 *  OVERRIDE PRESTATILL : pour conserver l'id entreprôt s'il est spécifié dans la caisse	
	 */
    public function createList(Order $order, Cart $cart, $id_order_state, $product_list, $id_order_invoice = 0, $use_taxes = true, $id_warehouse = 0)
    {
        // START PRESTATILL
        if($cart->id_pointshop > 0)
		{
			$id_warehouse = 0;	
			$pointshop = new ACaissePointshop($cart->id_pointshop);
			if(Validate::isLoadedObject($pointshop))
			{
				if($pointshop->id_warehouse > 0)
				$id_warehouse = $pointshop->id_warehouse;	
			}
		} 
		// TEST A VALIDER : si on est pas en gestion des stocks avancée, on force l'id_warehouse à 0, sinon bug commande
		else if(Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') == 0)
		{
			$id_warehouse = 0;
		}
        // END OVERRIDE	
			
        $this->vat_address = new Address((int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
        $this->customer = new Customer((int)$order->id_customer);

        $this->id_order = $order->id;
        $this->outOfStock = false;

        foreach ($product_list as $product) {
            $this->create($order, $cart, $product, $id_order_state, $id_order_invoice, $use_taxes, $id_warehouse);
        }

        unset($this->vat_address);
        unset($products);
        unset($this->customer);
    }
}