<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
require_once(_PS_MODULE_DIR_.'acaisse/classes/ACaissePointshop.php');
class AdminOrdersController extends AdminOrdersControllerCore
{
    public function __construct()
    {        
        parent::__construct();
        
                
        $pointshop_array = array();
        $pointshops = ACaissePointshop::getAll();

        foreach ($pointshops as $pointshop)
            $pointshop_array[$pointshop['id_a_caisse_pointshop']] = $pointshop['name'];
        
        $pointshop_array[0] = 'En ligne';
         
        $this->fields_list['id_pointshop'] = array(
            'title' => $this->l('pointshop'),
            'width' => 280,
            'type' => 'select',
            'list' => $pointshop_array,
            'filter_key' => 'a!id_pointshop',
            'filter_type' => 'int',
        );        
    }    
    
    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
        
                
        $pointshop_array = array();
        $pointshops = ACaissePointshop::getAll();

        foreach ($pointshops as $pointshop)
            $pointshop_array[$pointshop['id_a_caisse_pointshop']] = $pointshop['name'];
        
        $pointshop_array[0] = 'En ligne';
        
        
        $new_list = array();
        foreach($this->_list as $key=>$item)
        {
                         
            $item['id_pointshop'] = $pointshop_array[$item['id_pointshop']];
                
            $new_list[$key] = $item;
        }
        $this->_list = $new_list;
    }
}