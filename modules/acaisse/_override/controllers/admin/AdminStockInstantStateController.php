<?php
class AdminStockInstantStateController extends AdminStockInstantStateControllerCore
{
    public function __construct()
    {
        parent::__construct();
    		
        $this->fields_list = array(
            'reference' => array(
                'title' => $this->l('Reference'),
                'align' => 'center',
                'havingFilter' => true
            ),
            'ean13' => array(
                'title' => $this->l('EAN13'),
                'align' => 'center',
            ),
            /**************
            'upc' => array(
                'title' => $this->l('UPC'),
                'align' => 'center',
            ),*/
            'name' => array(
                'title' => $this->l('Name'),
                'havingFilter' => true
            ),
            
            /***** NOUVELLES COLONNES **********/            
            'cl.name' => array(
                'title' => $this->l('Category'),
                'havingFilter' => false,
                'search' => false,
                'orderby' => false,
            ),
            'man.name' => array(
                'title' => $this->l('Manufacturer'),
                'havingFilter' => false,
                'search' => false,
                'orderby' => false,
            ),
            
            'supp.name' => array(
                'title' => $this->l('Supplier'),
                'havingFilter' => false,
                'search' => false,
                'orderby' => false,
            ),
            'supp.ref' => array(
                'title' => $this->l('Supplier ref'),
                'havingFilter' => false,
                'search' => false,
                'orderby' => false,
            ),
            
            
            
            /*****************************************/
            'price_te' => array(
                'title' => $this->l('Price (tax excl.)'),
                'orderby' => true,
                'search' => false,
                'type' => 'price',
                'currency' => true,
            ),
            'valuation' => array(
                'title' => $this->l('Valuation'),
                'orderby' => false,
                'search' => false,
                'type' => 'price',
                'currency' => true,
                'hint' => $this->l('Total value of the physical quantity. The sum (for all prices) is not available for all warehouses, please filter by warehouse.')
            ),
            'physical_quantity' => array(
                'title' => $this->l('Physical quantity'),
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'orderby' => true,
                'search' => false
            ),
            'usable_quantity' => array(
                'title' => $this->l('Usable quantity'),
                'class' => 'fixed-width-xs',
                'align' => 'center',
                'orderby' => true,
                'search' => false,
            ),
        );

        $this->addRowAction('details');
        $this->addRowAction('addstock');
        $this->addRowAction('removestock');
        $this->addRowAction('transfertstock');
                
        //on assign les liste pour les filtres
        $this->stock_instant_state_warehouses = Warehouse::getWarehouses(true);
        array_unshift($this->stock_instant_state_warehouses, array('id_warehouse' => -1, 'name' => $this->l('All Warehouses')));
        
        //categories
        $cats = Category::getCategories(1,false,false,NULL,'ORDER BY c.nleft ASC'); //id_lang, active_only;
                
        $this->stock_instant_state_categories = $cats;
        $this->stock_instant_state_categories[0]['name'] = $this->l('All categories');
        $this->stock_instant_state_categories[0]['id_category'] = -1;
        
         
        
        //manufacturers
        $this->stock_instant_state_manufacturers = Manufacturer::getManufacturers();
        array_unshift($this->stock_instant_state_manufacturers, array('id_manufacturer' => -1, 'name' => $this->l('All Manufacturers')));
        
        //suppliers
        $this->stock_instant_state_suppliers = Supplier::getSuppliers();
        array_unshift($this->stock_instant_state_suppliers, array('id_supplier' => -1, 'name' => $this->l('All Suppliers')));
        
        

    }
    
    
    public function renderList()
    {
       
        if ($this->getCurrentCoverageWarehouse() != -1) {
            $this->_where .= ' AND a.id_warehouse = '.$this->getCurrentCoverageWarehouse();
            self::$currentIndex .= '&id_warehouse='.(int)$this->getCurrentCoverageWarehouse();
        }
        
        if ($this->getCurrentCoverageCategory() != -1) {
            $this->_where .= ' AND p.id_product IN (SELECT id_product FROM '._DB_PREFIX_.'category_product WHERE id_category='.(int)$this->getCurrentCoverageCategory().' )';
        }
        if ($this->getCurrentCoverageManufacturer() != -1) {
            $this->_where .= ' AND p.id_manufacturer = '.(int)$this->getCurrentCoverageManufacturer();
        }
        if ($this->getCurrentCoverageSupplier() != -1) {
            $this->_where .= ' AND p.id_product IN (SELECT id_product FROM '._DB_PREFIX_.'product_supplier WHERE id_supplier='.(int)$this->getCurrentCoverageSupplier().' )';
        }
        
        
        if(isset($_POST['stockFilter_cl.name']) && trim($_POST['stockFilter_cl.name'])!='')
        {
        	$this->_where .= ' AND cl.name LIKE \'%'.$_POST['stockFilter_cl.name'].'%\'';
		}
        

        // toolbar btn
        $this->toolbar_btn = array();
        // disables link
        $this->list_no_link = true;

        // smarty
        $this->tpl_list_vars['stock_instant_state_warehouses'] = $this->stock_instant_state_warehouses;
        $this->tpl_list_vars['stock_instant_state_cur_warehouse'] = $this->getCurrentCoverageWarehouse();
        
        
        $this->tpl_list_vars['stock_instant_state_categories'] = $this->stock_instant_state_categories;
        $this->tpl_list_vars['stock_instant_state_cur_category'] = $this->getCurrentCoverageCategory();
        
        
        $this->tpl_list_vars['stock_instant_state_suppliers'] = $this->stock_instant_state_suppliers;
        $this->tpl_list_vars['stock_instant_state_cur_supplier'] = $this->getCurrentCoverageSupplier();
        
        
        $this->tpl_list_vars['stock_instant_state_manufacturers'] = $this->stock_instant_state_manufacturers;
        $this->tpl_list_vars['stock_instant_state_cur_manufacturer'] = $this->getCurrentCoverageManufacturer();
        
        
        // adds ajax params
        $this->ajax_params = array(
            'id_warehouse' => $this->getCurrentCoverageWarehouse(),    
            'id_category' => $this->getCurrentCoverageCategory(),    
            'id_supplier' => $this->getCurrentCoverageSupplier(),    
            'id_manufacturer' => $this->getCurrentCoverageManufacturer(),        	
		);
        
        $list = parent::renderList(); //on peut pas appeler celle du parent direct

        // if export requested
        if ((Tools::isSubmit('csv_quantities') || Tools::isSubmit('csv_prices')) &&
            (int)Tools::getValue('id_warehouse') != -1) {
            if (count($this->_list) > 0) {
                $this->renderCSV();
                die;
            } else {
                $this->displayWarning($this->l('There is nothing to export as CSV.'));
            }
        }

        return $list;
    }
    
    
    /**
     * Exports CSV
     */
    public function renderCSV()
    {
        if (count($this->_list) <= 0)
            return;

        // sets warehouse id and warehouse name
        $id_warehouse = (int)Tools::getValue('id_warehouse');
        $warehouse_name = Warehouse::getWarehouseNameById($id_warehouse);

        // if quantities requested
        if (Tools::isSubmit('csv_quantities'))
        {
            // filename
            $filename = $this->l('stock_instant_state_quantities').'_'.$warehouse_name.'.csv';

            // header
            header('Content-type: text/csv');
            header('Cache-Control: no-store, no-cache');
            header('Content-disposition: attachment; filename="'.$filename);

            // puts keys
            $keys = array('id_product', 'id_product_attribute', 'reference', 'ean13', 'upc', 'name'
            , 'category', 'manufacturer', 'supplier', 'supplier_ref'
            , 'physical_quantity', 'usable_quantity', 'real_quantity');
            echo sprintf("%s\n", implode(';', $keys));

            // puts rows
            foreach ($this->_list as $row)
            {   
                $row_csv = array($row['id_product'], $row['id_product_attribute'], $row['reference'],
                                 $row['ean13'], $row['upc'], $row['name'],
                                 $row['cl.name'],
                                 $row['man.name'], $row['supp.name'], $row['supp.ref'],
                                 $row['physical_quantity'], $row['usable_quantity'], $row['real_quantity']
                );

                // puts one row
                echo sprintf("%s\n", implode(';', array_map(array('CSVCore', 'wrap'), $row_csv)));
            }
        }
        // if prices requested
        else if (Tools::isSubmit('csv_prices'))
        {
            // sets filename
            $filename = $this->l('stock_instant_state_prices').'_'.$warehouse_name.'.csv';

            // header
            header('Content-type: text/csv');
            header('Cache-Control: no-store, no-cache');
            header('Content-disposition: attachment; filename="'.$filename);

            // puts keys
            $keys = array('id_product', 'id_product_attribute', 'reference', 'ean13', 'upc', 'name'            
            , 'category', 'manufacturer', 'supplier', 'supplier_ref'
            ,'price_te', 'physical_quantity', 'usable_quantity');
            echo sprintf("%s\n", implode(';', $keys));

            foreach ($this->_list as $row)
            {
                $id_product = (int)$row['id_product'];
                $id_product_attribute = (int)$row['id_product_attribute'];

                // gets prices
                $query = new DbQuery();
                $query->select('s.price_te, SUM(s.physical_quantity) as physical_quantity, SUM(s.usable_quantity) as usable_quantity');
                $query->from('stock', 's');
                $query->leftJoin('warehouse', 'w', 'w.id_warehouse = s.id_warehouse');
                $query->where('s.id_product = '.$id_product.' AND s.id_product_attribute = '.$id_product_attribute);
                $query->where('s.id_warehouse = '.$id_warehouse);
                $query->groupBy('s.price_te');
                $datas = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);

                
                // puts data
                foreach ($datas as $data)
                {
                    $row_csv = array($row['id_product'], $row['id_product_attribute'], $row['reference'],
                                     $row['ean13'], $row['upc'], $row['name'],
                                     $row['cl.name'],
                                     $row['man.name'], $row['supp.name'], $row['supp.ref'],
                                     $data['price_te'], $data['physical_quantity'], $data['usable_quantity']);

                    // puts one row
                    echo sprintf("%s\n", implode(';', array_map(array('CSVCore', 'wrap'), $row_csv)));
                }
            }
        }
    }
    

    /**
     * Gets the current warehouse used
     *
     * @return int id_warehouse
     */
    protected function getCurrentCoverageWarehouse()
    {
        static $warehouse = 0;

        if ($warehouse == 0) {
            $warehouse = -1; // all warehouses
            if ((int)Tools::getValue('id_warehouse')) {
                $warehouse = (int)Tools::getValue('id_warehouse');
            }
        }
        return $warehouse;
    }
    
    
    protected function getCurrentCoverageCategory()
    {
        static $category = 0;

        if ($category == 0) {
            $category = -1; // all categories
            if ((int)Tools::getValue('id_category')) {
                $category = (int)Tools::getValue('id_category');
            }
        }
        return $category;
    }
    protected function getCurrentCoverageManufacturer()
    {
        static $manufacturer = 0;

        if ($manufacturer == 0) {
            $manufacturer = -1; // all manufacturers
            if ((int)Tools::getValue('id_manufacturer')) {
                $manufacturer = (int)Tools::getValue('id_manufacturer');
            }
        }
        return $manufacturer;
    }
    protected function getCurrentCoverageSupplier()
    {
        static $supplier = 0;

        if ($supplier == 0) {
            $supplier = -1; // all suppliers
            if ((int)Tools::getValue('id_supplier')) {
                $supplier = (int)Tools::getValue('id_supplier');
            }
        }
        return $supplier;
    }
    
    
        /**
     * AdminController::getList() override
     * @see AdminController::getList()
     *
     * @param int         $id_lang
     * @param string|null $order_by
     * @param string|null $order_way
     * @param int         $start
     * @param int|null    $limit
     * @param int|bool    $id_lang_shop
     *
     * @throws PrestaShopException
     */
    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {      
        if (Tools::isSubmit('id_stock')) {
        				
        	
            parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);

            $nb_items = count($this->_list);

            
            for ($i = 0; $i < $nb_items; $i++) {
                $item = &$this->_list[$i];
                
                
                $manager = StockManagerFactory::getManager();

                // gets quantities and valuation
                $query = new DbQuery();
                $query->select('physical_quantity');
                $query->select('p.id_product');
                $query->select('s.id_product_attribute');
                $query->select('s.id_stock');
                $query->select('usable_quantity');
                $query->select('SUM(price_te * physical_quantity) as valuation');
                $query->from('stock','AS s');
                $query->where('s.id_stock = '.(int)$item['id_stock'].' AND s.id_product = '.(int)$item['id_product'].' AND s.id_product_attribute = '.(int)$item['id_product_attribute']);
                
                //join product
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'product` AS p ON (s.id_product = p.id_product)');
                
                //category
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'category` AS c ON (c.id_category = p.id_category_default)');
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'category_lang` AS cl ON (
                    c.id_category = cl.id_category
                    AND cl.id_lang = '.(int)$this->context->language->id.'
                )');
                $query->select('cl.name as cl_name');
                
                //supplier   
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'product_supplier` AS psupp ON (psupp.id_product = p.id_product
                    AND s.id_product_attribute = psupp.id_product_attribute
                )');
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'supplier` AS supp ON (supp.id_supplier = psupp.id_supplier)');  
                
                $query->select('supp.name as supp_name, psupp.product_supplier_reference as supp_ref');                                            
                
                //manufacturer
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'manufacturer` AS man ON (man.id_manufacturer = p.id_manufacturer)');
                $query->select('man.name as `man_name`');
                
                
                
                if ($this->getCurrentCoverageWarehouse() != -1) {
                    $query->where('id_warehouse = '.(int)$this->getCurrentCoverageWarehouse());
                }

                $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query);
//p($query->build());d($res);
                
                                     
                $item['cl.name'] = $res['cl_name'];
                $item['man.name'] = $res['man_name'];
                $item['supp.name'] = $res['supp_name'];
                $item['supp.ref'] = $res['supp_ref'];
                
                $item['physical_quantity'] = $res['physical_quantity'];
                $item['usable_quantity'] = $res['usable_quantity'];
                $item['valuation'] = $res['valuation'];
                          
                //actions sur le stock
                $tocken = Tools::getAdminTokenLite('AdminStockManagement');
                $item['addstock'] = '<a href="index.php?controller=AdminStockManagement&id_product='.$res['id_product'].($res['id_product_attribute']==0?'':'&id_product_attribute='.$res['id_product_attribute']).'&addstock&token='.$tocken.'"><i class="icon-circle-arrow-up"></i> Ajouter au stock</a>';
                $item['removestock'] = '<a href="index.php?controller=AdminStockManagement&id_stock='.$res['id_stock'].'&removestock&token='.$tocken.'"><i class="icon-circle-arrow-down"></i> Retirer du stock</a>';
                
                $item['real_quantity'] = $manager->getProductRealQuantities(
                    $item['id_product'],
                    $item['id_product_attribute'],
                    ($this->getCurrentCoverageWarehouse() == -1 ? null : array($this->getCurrentCoverageWarehouse())),
                    true
                );
            }
        } else {
            if ((Tools::isSubmit('csv_quantities') || Tools::isSubmit('csv_prices')) &&
                (int)Tools::getValue('id_warehouse') != -1) {
                $limit = false;
            }

            $order_by_valuation = false;
            $order_by_real_quantity = false;

            if ($this->context->cookie->{$this->table.'Orderby'} == 'valuation') {
                unset($this->context->cookie->{$this->table.'Orderby'});
                $order_by_valuation = true;
            } elseif ($this->context->cookie->{$this->table.'Orderby'} == 'real_quantity') {
                unset($this->context->cookie->{$this->table.'Orderby'});
                $order_by_real_quantity = true;
            }
            
            parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
            $nb_items = count($this->_list);
        			
            
            for ($i = 0; $i < $nb_items; ++$i) {
                $item = &$this->_list[$i];
                
                $item['price_te'] = '--';
                $item[$this->identifier] = $item['id_product'].'_'.$item['id_product_attribute'];

                // gets stock manager
                $manager = StockManagerFactory::getManager();

                // gets quantities and valuation
                $query = new DbQuery();
                $query->select('SUM(physical_quantity) as physical_quantity');
                $query->select('p.id_product');
                $query->select('s.id_stock');
                $query->select('SUM(usable_quantity) as usable_quantity');
                $query->select('SUM(price_te * physical_quantity) as valuation');
                $query->from('stock','AS s');
                $query->where('s.id_product = '.(int)$item['id_product'].' AND s.id_product_attribute = '.(int)$item['id_product_attribute']);
                
                //join product
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'product` AS p ON (s.id_product = p.id_product)');
                
                //category
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'category` AS c ON (c.id_category = p.id_category_default)');
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'category_lang` AS cl ON (
                    c.id_category = cl.id_category
                    AND cl.id_lang = '.(int)$this->context->language->id.'
                )');
                $query->select('cl.name as cl_name');
                
                //supplier   
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'product_supplier` AS psupp ON (psupp.id_product = p.id_product
                    AND s.id_product_attribute = psupp.id_product_attribute
                )');
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'supplier` AS supp ON (supp.id_supplier = psupp.id_supplier)');  
                
                $query->select('supp.name as supp_name, psupp.product_supplier_reference as supp_ref');                                            
                
                //manufacturer
                $query->join(' LEFT JOIN `'._DB_PREFIX_.'manufacturer` AS man ON (man.id_manufacturer = p.id_manufacturer)');
                $query->select('man.name as `man_name`');
        
                
                
                if ($this->getCurrentCoverageWarehouse() != -1) {
                    $query->where('id_warehouse = '.(int)$this->getCurrentCoverageWarehouse());
                }

                //p($query->build());
                $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query);
                
                //d($res);

                $item['physical_quantity'] = $res['physical_quantity'];
                $item['usable_quantity'] = $res['usable_quantity'];
                //$item['cl.name'] = $res['category_name'];

                // gets real_quantity depending on the warehouse
                $item['real_quantity'] = $manager->getProductRealQuantities($item['id_product'],
                                                                            $item['id_product_attribute'],
                                                                            ($this->getCurrentCoverageWarehouse() == -1 ? null : array($this->getCurrentCoverageWarehouse())),
                                                                            true);

                
                
                
                                                                            
                $item['cl.name'] = $res['cl_name'];
                $item['man.name'] = $res['man_name'];
                $item['supp.name'] = $res['supp_name'];
                $item['supp.ref'] = $res['supp_ref'];
                
                
                //actions sur le stock
                $tocken = Tools::getAdminTokenLite('AdminStockManagement');
                $item['addstock'] = '<a href="index.php?controller=AdminStockManagement&id_product='.$res['id_product'].'$id_product_attribute='.$item['id_product_attribute'].'&addstock&token='.$tocken.'"><i class="icon-circle-arrow-up"></i> Ajouter au stock</a>';
                $item['removestock'] = '<a href="index.php?controller=AdminStockManagement&id_stock='.$res['id_stock'].'&removestock&token='.$tocken.'"><i class="icon-circle-arrow-down"></i> Retirer du stock</a>';

                // removes the valuation if the filter corresponds to 'all warehouses'
                if ($this->getCurrentCoverageWarehouse() == -1) {
                    $item['valuation'] = 'N/A';
                } else {
                    $item['valuation'] = $res['valuation'];
                }
            }

            if ($this->getCurrentCoverageWarehouse() != -1 && $order_by_valuation) {
                usort($this->_list, array($this, 'valuationCmp'));
            } elseif ($order_by_real_quantity) {
                usort($this->_list, array($this, 'realQuantityCmp'));
            }
        }
    }
}