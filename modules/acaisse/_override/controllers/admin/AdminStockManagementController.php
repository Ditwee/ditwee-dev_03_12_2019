<?php
class AdminStockManagementController extends AdminStockManagementControllerCore
{

    /**
     * Call if no GET id_stock, display a detail stock for a product/product_attribute (various price)
     * @param $id_product
     * @param $id_product_attribute
     */
    public function previousManagementStock($id_product, $id_product_attribute)
    {
        parent::previousManagementStock($id_product, $id_product_attribute);
        $this->_filter = '';
    }
	
	/**
	 * PRESTATILL
	 * [FIX] auto check warehouse in product when addStock
	 */
	public function postProcess()
    {
    	/**
		 * PRESTATILL 
		 */
    	//on va chercher à vérifier lorsque l'on fait un ajout de stock
    	//si l'entrepot etait coché
    	$id_product = null;
		$id_product_attribute = null;
		$id_warehouse = null;
		$prestatill_had_warehouse_before_product_add_stock = null;
    	if (Tools::isSubmit('addstock')  && Tools::isSubmit('is_post')) {
            // get product ID
            $id_product = (int)Tools::getValue('id_product', 0);
            
            // get product_attribute ID
            $id_product_attribute = (int)Tools::getValue('id_product_attribute', 0);
			$stockAttributes = $this->getStockAttributes();
            $id_warehouse = $stockAttributes['warehouse_id'];
			
			
			//on check l'entrepot
			$prestatill_had_warehouse_before_product_add_stock = (int)WarehouseProductLocation::getIdByProductAndWarehouse($id_product, $id_product_attribute, $id_warehouse);
            		
		}    	
		
        parent::postProcess();
		
		
		/**
		 * PRESTATILLait pas coché... 
		 * si il n'etait pas coché
		 * on va le redécocher
		 */
	    if (Tools::isSubmit('addstock')  && Tools::isSubmit('is_post') && $prestatill_had_warehouse_before_product_add_stock) {
	    	//on supp la location ET on resynchronise les stocks	    	
	    	$tmp = new WarehouseProductLocation($prestatill_had_warehouse_before_product_add_stock);
			$tmp->delete();
			StockAvailable::synchronize($id_product);   	
		}
	}
	
}