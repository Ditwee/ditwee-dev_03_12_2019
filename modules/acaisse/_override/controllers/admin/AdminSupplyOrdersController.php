<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 * @property SupplyOrder $object
 */
class AdminSupplyOrdersController extends AdminSupplyOrdersControllerCore
{

    /**
     * AdminController::postProcess() override
     * @see AdminController::postProcess()
     */
    public function postProcess()
    {
        $this->is_editing_order = false;

        // Checks access
        if (Tools::isSubmit('submitAddsupply_order') && !($this->tabAccess['add'] === '1')) {
            $this->errors[] = Tools::displayError('You do not have permission to add a supply order.');
        }
        if (Tools::isSubmit('submitBulkUpdatesupply_order_detail') && !($this->tabAccess['edit'] === '1')) {
            $this->errors[] = Tools::displayError('You do not have permission to edit an order.');
        }

        // Trick to use both Supply Order as template and actual orders
        if (Tools::isSubmit('is_template')) {
            $_GET['mod'] = 'template';
        }

        // checks if supply order reference is unique
        if (Tools::isSubmit('reference')) {
            // gets the reference
            $ref = pSQL(Tools::getValue('reference'));

            if (Tools::getValue('id_supply_order') != 0 && SupplyOrder::getReferenceById((int)Tools::getValue('id_supply_order')) != $ref) {
                if ((int)SupplyOrder::exists($ref) != 0) {
                    $this->errors[] = Tools::displayError('The reference has to be unique.');
                }
            } elseif (Tools::getValue('id_supply_order') == 0 && (int)SupplyOrder::exists($ref) != 0) {
                $this->errors[] = Tools::displayError('The reference has to be unique.');
            }
        }

        if ($this->errors) {
            return;
        }

        // Global checks when add / update a supply order
        if (Tools::isSubmit('submitAddsupply_order') || Tools::isSubmit('submitAddsupply_orderAndStay')) {
            $this->action = 'save';
            $this->is_editing_order = true;

            // get supplier ID
            $id_supplier = (int)Tools::getValue('id_supplier', 0);
            if ($id_supplier <= 0 || !Supplier::supplierExists($id_supplier)) {
                $this->errors[] = Tools::displayError('The selected supplier is not valid.');
            }

            // get warehouse id
            $id_warehouse = (int)Tools::getValue('id_warehouse', 0);
            if ($id_warehouse <= 0 || !Warehouse::exists($id_warehouse)) {
                $this->errors[] = Tools::displayError('The selected warehouse is not valid.');
            }

            // get currency id
            $id_currency = (int)Tools::getValue('id_currency', 0);
            if ($id_currency <= 0 || (!($result = Currency::getCurrency($id_currency)) || empty($result))) {
                $this->errors[] = Tools::displayError('The selected currency is not valid.');
            }

            // get delivery date
            if (Tools::getValue('mod') != 'template' && strtotime(Tools::getValue('date_delivery_expected')) <= strtotime('-1 day')) {
                $this->errors[] = Tools::displayError('The specified date cannot be in the past.');
            }

            // gets threshold
            $quantity_threshold = Tools::getValue('load_products');

            if (is_numeric($quantity_threshold)) {
                $quantity_threshold = (int)$quantity_threshold;
            } else {
                $quantity_threshold = null;
            }

            if (!count($this->errors)) {
                // forces date for templates
                if (Tools::isSubmit('is_template') && !Tools::getValue('date_delivery_expected')) {
                    $_POST['date_delivery_expected'] = date('Y-m-d h:i:s');
                }

                // specify initial state
                $_POST['id_supply_order_state'] = 1; //defaut creation state

                // specify global reference currency
                $_POST['id_ref_currency'] = Currency::getDefaultCurrency()->id;

                // specify supplier name
                $_POST['supplier_name'] = Supplier::getNameById($id_supplier);

                //specific discount check
                $_POST['discount_rate'] = (float)str_replace(array(' ', ','), array('', '.'), Tools::getValue('discount_rate', 0));
            }

            // manage each associated product
            $this->manageOrderProducts();

            // if the threshold is defined and we are saving the order
            if (Tools::isSubmit('submitAddsupply_order') && Validate::isInt($quantity_threshold)) {
                $this->loadProducts((int)$quantity_threshold);
            }
        }

        // Manage state change
        if (Tools::isSubmit('submitChangestate')
            && Tools::isSubmit('id_supply_order')
            && Tools::isSubmit('id_supply_order_state')) {
            if ($this->tabAccess['edit'] != '1') {
                $this->errors[] = Tools::displayError('You do not have permission to change the order status.');
            }

            // get state ID
            $id_state = (int)Tools::getValue('id_supply_order_state', 0);
            if ($id_state <= 0) {
                $this->errors[] = Tools::displayError('The selected supply order status is not valid.');
            }

            // get supply order ID
            $id_supply_order = (int)Tools::getValue('id_supply_order', 0);
            if ($id_supply_order <= 0) {
                $this->errors[] = Tools::displayError('The supply order ID is not valid.');
            }

            if (!count($this->errors)) {
                // try to load supply order
                $supply_order = new SupplyOrder($id_supply_order);

                if (Validate::isLoadedObject($supply_order)) {
                    // get valid available possible states for this order
                    $states = SupplyOrderState::getSupplyOrderStates($supply_order->id_supply_order_state);

                    foreach ($states as $state) {
                        // if state is valid, change it in the order
                        if ($id_state == $state['id_supply_order_state']) {
                            $new_state = new SupplyOrderState($id_state);
                            $old_state = new SupplyOrderState($supply_order->id_supply_order_state);

                            // special case of validate state - check if there are products in the order and the required state is not an enclosed state
                            if ($supply_order->isEditable() && !$supply_order->hasEntries() && !$new_state->enclosed) {
                                $this->errors[] = Tools::displayError('It is not possible to change the status of this order because you did not order any products.');
                            }

                            if (!count($this->errors)) {
                                $supply_order->id_supply_order_state = $state['id_supply_order_state'];
                                if ($supply_order->save()) {
                                    // create stock entry if not exist when order is in pending_receipt
                                    if ($new_state->pending_receipt) {
                                        $supply_order_details = $supply_order->getEntries();
                                        foreach ($supply_order_details as $supply_order_detail) {
                                            $is_present = Stock::productIsPresentInStock($supply_order_detail['id_product'], $supply_order_detail['id_product_attribute'], $supply_order->id_warehouse);
                                            if (!$is_present) {
                                                $stock = new Stock();

                                                $stock_params = array(
                                                    'id_product_attribute' => $supply_order_detail['id_product_attribute'],
                                                    'id_product' => $supply_order_detail['id_product'],
                                                    'physical_quantity' => 0,
                                                    'price_te' => $supply_order_detail['price_te'],
                                                    'usable_quantity' => 0,
                                                    'id_warehouse' => $supply_order->id_warehouse
                                                );

                                                // saves stock in warehouse
                                                $stock->hydrate($stock_params);
                                                $stock->add();
                                            }
                                        }
                                    }

                                    // add stock when is received completely
                                    if ($new_state->receipt_state && $new_state->enclosed) {
                                        $supply_order_details = $supply_order->getEntries();

                                        $warehouse = new Warehouse($supply_order->id_warehouse);
                                        $id_warehouse = $warehouse->id;

                                        $stock_manager = StockManagerFactory::getManager();

                                        foreach ($supply_order_details as $detail) {
                                            $id_product = $detail['id_product'];
                                            $id_product_attribute = $detail['id_product_attribute'];

                                            if ($stock_manager->addProduct(
                                                $detail['id_product'],
                                                $detail['id_product_attribute'],
                                                $warehouse,
                                                (int)($detail['quantity_expected'] - $detail['quantity_received']),
                                                Configuration::get('PS_STOCK_MVT_SUPPLY_ORDER'),
                                                $detail['unit_price_te'],
                                                true,
                                                $supply_order->id
                                            )) {
                                                // Create warehouse_product_location entry if we add stock to a new warehouse
                                                $id_wpl = (int)WarehouseProductLocation::getIdByProductAndWarehouse($id_product, $id_product_attribute, $id_warehouse);
                                                if (false && !$id_wpl) {
                                                    $wpl = new WarehouseProductLocation();
                                                    $wpl->id_product = (int)$id_product;
                                                    $wpl->id_product_attribute = (int)$id_product_attribute;
                                                    $wpl->id_warehouse = (int)$id_warehouse;
                                                    $wpl->save();
                                                }

                                                $supply_order_detail_mvt = new SupplyOrderDetail($detail['id_supply_order_detail']);
                                                $supply_order_detail_mvt_params = array(
                                                    'quantity_received' => (int)$detail['quantity_expected'],
                                                );
                                                // saves supply order detail
                                                $supply_order_detail_mvt->hydrate($supply_order_detail_mvt_params);
                                                $supply_order_detail_mvt->update();

                                            } else {
                                                $this->errors[] = $this->l('An error occurred. No stock was added.');
                                            }
                                        }
                                    }

                                    // if the order is being canceled,
                                    // or if the order is received completely
                                    // synchronizes StockAvailable
                                    if ((($old_state->receipt_state || $old_state->pending_receipt) && $new_state->enclosed && !$new_state->receipt_state) ||
                                        ($new_state->receipt_state && $new_state->enclosed)
                                    ) {
                                        $supply_order_details = $supply_order->getEntries();
                                        $products_done = array();
                                        foreach ($supply_order_details as $supply_order_detail) {
                                            if (!in_array($supply_order_detail['id_product'], $products_done)) {
                                                StockAvailable::synchronize($supply_order_detail['id_product']);
                                                $products_done[] = $supply_order_detail['id_product'];
                                            }
                                        }
                                    }

                                    $token = Tools::getValue('token') ? Tools::getValue('token') : $this->token;
                                    $redirect = self::$currentIndex.'&token='.$token;
                                    $this->redirect_after = $redirect.'&conf=5';
                                }
                            }
                        }
                    }
                } else {
                    $this->errors[] = Tools::displayError('The selected supplier is not valid.');
                }
            }
        }

        // updates receipt
        if (Tools::isSubmit('submitBulkUpdatesupply_order_detail') && Tools::isSubmit('id_supply_order')) {
            $this->postProcessUpdateReceipt();
        }

        // use template to create a supply order
        if (Tools::isSubmit('create_supply_order') && Tools::isSubmit('id_supply_order')) {
            $this->postProcessCopyFromTemplate();
        }

        if ((!count($this->errors) && $this->is_editing_order) || !$this->is_editing_order) {
            parent::postProcess();
        }
    }


}
