<?php
require_once(dirname(__FILE__).'/classes/ACaisseCashTill.php');
require_once(dirname(__FILE__).'/classes/ACaisseCash.php');
require_once(dirname(__FILE__).'/classes/ACaissePage.php');
require_once(dirname(__FILE__).'/classes/ACaissePageCell.php');
require_once(dirname(__FILE__).'/classes/ACaissePermission.php');
require_once(dirname(__FILE__).'/classes/ACaissePointshop.php');
require_once(dirname(__FILE__).'/classes/ACaisseTicketCaisse.php');
require_once(dirname(__FILE__).'/classes/ACaisseTicket.php');
require_once(dirname(__FILE__).'/classes/ACaisseTicketLine.php');
require_once(dirname(__FILE__).'/classes/ACaisseUnit.php');
require_once(dirname(__FILE__).'/classes/ACaisseMultiplePayment.php');
require_once(dirname(__FILE__).'/classes/ACaisseReturn.php');
require_once(dirname(__FILE__).'/classes/ACaisseReturnProduct.php');
require_once(dirname(__FILE__).'/classes/ACaisseReturnLine.php');
require_once(dirname(__FILE__).'/vendor/php-barcode-generator/include.php');
 
class ACaisse extends Module
{
    const MAX_PAYMENT_MODE=5;
    const PERMISSION_ACCESS=1;
    const PERMISSION_OPEN=2;
    const PERMISSION_MOUVMENT=3;
    const PERMISSION_CLOSE=4;
    const PERMISSION_CASH_REFLOW=5;
    const PERMISSION_STATS=7;
	
	public $upload_common_logo_dir;
	public $upload_common_logo_path;

    public function __construct() {
        $this->name = 'acaisse';
        $this->tab = 'other';
        $this->version = '2.5.2';
        $this->author = 'Prestatill SAS';
        $this->need_instance = 0;
		
        parent::__construct();
		
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap = true;
        $this->displayName = $this->l('Caisse connectée PrestaTill');
        $this->description = $this->l('La solution de vente cross-canal et omnicanal de référence pour Prestashop');
		
		$this->upload_common_logo_dir = $this->getLocalPath() .  'uploads' . DIRECTORY_SEPARATOR . 'ticket_logo' . DIRECTORY_SEPARATOR;
		$this->upload_common_logo_path = $this->getPathUri() .  'uploads' . DIRECTORY_SEPARATOR . 'ticket_logo' . DIRECTORY_SEPARATOR;
    }

    public function install() {
        if (
            parent::install() == false
            || !$this->_registerNewTab()
			|| !$this->_installSql()
			|| !$this->_updateCarrierDefault()
			|| !$this->_updatePaymentStates()
            || $this->registerHook('actionBeforeSendOrderConfirmationMail') == false
            //@TODO actionValidateOrderModuleMail // a priori le nom du hook equivalent en 1.5 A VERIFIER
            
            //le hook pour ajouter le CSS dans le BO (icone de la caisse)
            || $this->registerHook('displayBackOfficeHeader') == false
            
            //les hook lié aux catégories
            || $this->registerHook('actionObjectCategoryAddAfter') == false
            || $this->registerHook('actionObjectCategoryDeleteBefore') == false
            || $this->registerHook('actionObjectCategoryUpdateAfter') == false
            
            //les hook lié aux produits
            
            || $this->registerHook('actionObjectCombinationAddAfter') == false
            || $this->registerHook('actionObjectCombinationDeleteAfter') == false            
            || $this->registerHook('actionObjectProductDeleteBefore') == false
            || $this->registerHook('actionProductSave') == false
            
            //hook lié à la fiche produit
            || $this->registerHook('displayAdminProductsExtra') == false
            || $this->registerHook('actionProductUpdate') == false
            
			//Hook Prestatill Export Compta
			//rapport financier
			|| !$this->registerHook('displayPrestatillFinancialReportHeader')
			|| !$this->registerHook('displayPrestatillFinancialReportFooter')
			|| !$this->registerHook('displayPrestatillFinancialReportAfterReport')
            
            //les Hooks de page ou de point de vente
            || $this->registerHook('actionObjectACaissePointshopAddAfter') == false
            || $this->registerHook('actionObjectACaissePointshopUpdateAfter') == false
            || $this->registerHook('actionObjectACaissePageAddAfter') == false
            || $this->registerHook('actionObjectACaissePageUpdateAfter') == false
            || $this->registerHook('actionObjectACaissePageCellAddAfter') == false
            || $this->registerHook('actionObjectACaissePageCellUpdateAfter') == false
            
            //hook lié à la validation des paiement
            || $this->registerHook('actionValidateOrder') == false
            
			// hook des actualités
			|| $this->registerHook('displayPrestatillNews') == false
        ) {
            return false;
        }
        
        // on créer les paramètres par défaut
        if(Configuration::get('ACAISSE_profilsID') === false) {
        	Configuration::updateValue('ACAISSE_profilsID',1);
		}		
        	
        if(Configuration::get('ACAISSE_profilsIDNoChecking') === false) {
        	Configuration::updateValue('ACAISSE_profilsIDNoChecking','');
		}
        
        if(Configuration::get('ACAISSE_searchActiveCustomer') === false) {
        	Configuration::updateValue('ACAISSE_searchActiveCustomer',0);
		}
        
        if(Configuration::get('ACAISSE_useForceGroup') === false) {
        	Configuration::updateValue('ACAISSE_useForceGroup',0);
		}
        
        if(Configuration::get('ACAISSE_customerSearchFields') === false) {
        	Configuration::updateValue('ACAISSE_customerSearchFields','id_customer;secure_key;lastname;firstname');
		}        
		
		if(Configuration::get('PRESTATILL_PAYMENTS_METHOD_LIST') === false) {
			Configuration::updateValue('PRESTATILL_PAYMENTS_METHOD_LIST',serialize(array('Espèces','CB','CB Sans-contact','Chèque','Virement','Bon d\'achat','Contre-signature','Avoir')));
		} 
        
        Configuration::updateValue('ACAISSE_last_cache_generation_categories', '0000-00-00 00:00:00');
        Configuration::updateValue('ACAISSE_last_cache_generation_pointshoppages', '0000-00-00 00:00:00');
        Configuration::updateValue('ACAISSE_last_cache_generation_pricelist', '0000-00-00 00:00:00');
        Configuration::updateValue('ACAISSE_last_cache_generation_profiles', '0000-00-00 00:00:00');
        
        /**
		 * SIREEN 2.0
		 */		
        if(Configuration::get('ACAISSE_FORCE_DEFAULT_CARRIER') === false) {
        	Configuration::updateValue('ACAISSE_FORCE_DEFAULT_CARRIER',1);
		}
		if(Configuration::get('ACAISSE_INVOICE_IS_ACTIVE') === false) {
        	Configuration::updateValue('ACAISSE_INVOICE_IS_ACTIVE',1);
		}

        /**
		 * SIREEN 2.1
		 */		
        if(Configuration::get('ACAISSE_dont_export_inactive_product') === false) {
        	Configuration::updateValue('ACAISSE_dont_export_inactive_product',0);
		}
		if(Configuration::get('ACAISSE_export_only_product_in_default_category') === false) {
        	Configuration::updateValue('ACAISSE_export_only_product_in_default_category',0);
		}


        if(Configuration::get('PRESTATILL_PAYMENTS_METHOD_LIST') === false) {
			Configuration::updateValue('PRESTATILL_PAYMENTS_METHOD_LIST',serialize(array('Espèce','CB','CB Sans-contact','Chèque','Virement','Bon d\'achat','Contre-signature','Avoir')));
		}
		
		//on va creer la premiere addresse de point de vente si inexistante
		$addresses = Address::getPrestatillPointshop();
		
		//d($addresses);
		$new_address_created = false;
		$new_address_created_instance = null;		
		if(empty($addresses))
		{
			$pointhsop_address = new Address();
			$pointhsop_address->is_pointshop = 1;
			$pointhsop_address->lastname = 'xxxxxxxxxxx';
			$pointhsop_address->firstname = 'xxxxxxxxxxx';
			$pointhsop_address->alias = substr(Configuration::get('PS_SHOP_NAME'),0,32);
			$pointhsop_address->company = Configuration::get('PS_SHOP_NAME');
			$pointhsop_address->vat_number = 'FR-000000';
			$pointhsop_address->dni = '0000000000000';
			
			$pointhsop_address->address1 = '' . Configuration::get('PS_SHOP_ADDR1') == '' ? 'Rue' : Configuration::get('PS_SHOP_ADDR1');
			$pointhsop_address->address2 = '' . Configuration::get('PS_SHOP_ADDR2');
			$pointhsop_address->postcode = '' . Configuration::get('PS_SHOP_CODE') == '' ? '68000' : Configuration::get('PS_SHOP_CODE');
			$pointhsop_address->city = '' . Configuration::get('PS_SHOP_CITY') == '' ? 'Colmar' : Configuration::get('PS_SHOP_CITY');
			$pointhsop_address->id_country = Configuration::get('PS_SHOP_COUNTRY_ID') == '' ? Configuration::get('PS_SHOP_COUNTRY_ID') : 8 ;
			$pointhsop_address->phone = '' . Configuration::get('PS_SHOP_PHONE') == '' ? '03 00 00 00 00' : Configuration::get('PS_SHOP_PHONE');

			$pointhsop_address->common_picture_path = '../../../../img/' . Configuration::get('PS_LOGO');
			
			$pointhsop_address->common_currency_change = '{}';
			$pointhsop_address->common_customer_info = '';
			$pointhsop_address->common_delivery_info = '';
			$pointhsop_address->common_invoice_info = '';
			
			$pointhsop_address->ticket_header = '<p style="text-align: center;">{{ LOGO }}</p>
<p style="text-align: center;">{{ SHOP_STREET }}<br />{{ SHOP_CP }} {{ SHOP_CITY }}<br />{{ SHOP_PHONE }}</p>
<table style="width:90%;">
<tbody>
<tr>
<td>Vendeur</td>
<td>Date</td>
<td>Heure</td>
<td>Ticket</td>
</tr>
<tr>
<td>{{ SELLER_ID }}</td>
<td>{{ TICKET_DATE }}</td>
<td>{{ TICKET_TIME }}</td>
<td>{{ TICKET_NUM }}</td>
</tr>
</tbody>
</table>
<p style="text-align: center;"><span>{{ TICKET_EAN }}<br /></span>{{ TICKET_EAN_CODE }}</p>';
			$pointhsop_address->ticket_customer_position = 0;
			$pointhsop_address->ticket_delivery_position = 0;
			$pointhsop_address->ticket_invoice_position = 0;
			$pointhsop_address->ticket_footer = '<p style="text-align: center;">Vendeur : {{ SELLER_FIRSTNAME }} ({{ SELLER_ID }})<br />{{ TILL_NAME }}</p>
<p style="text-align: center;">Merci à bientôt<br />'.Configuration::get('PS_SHOP_DOMAIN').'<br />'.Configuration::get('PS_SHOP_EMAIL') . '</p>';
			
			$pointhsop_address->slip_header = '<p style="text-align: center;">{{ LOGO }}</p>
<p style="text-align: center;">{{ SHOP_STREET }}<br />{{ SHOP_CP }} {{ SHOP_CITY }}<br />{{ SHOP_PHONE }}</p>';
			$pointhsop_address->slip_customer_position = 0;
			$pointhsop_address->slip_delivery_position = 0;
			$pointhsop_address->slip_invoice_position = 0;
			$pointhsop_address->slip_footer = '<p style="text-align: center;">Vendeur : {{ SELLER_FIRSTNAME }} ({{ SELLER_ID }})<br />{{ TILL_NAME }}</p>
<p style="text-align: center;">Merci à bientôt<br />'.Configuration::get('PS_SHOP_DOMAIN').'<br />'.Configuration::get('PS_SHOP_EMAIL') . '</p>';

			$pointhsop_address->preorder_header = '<p style="text-align: center;">{{ LOGO }}</p>
<p style="text-align: center;">{{ SHOP_STREET }}<br />{{ SHOP_CP }} {{ SHOP_CITY }}<br />{{ SHOP_PHONE }}</p>';
			$pointhsop_address->preorder_customer_position = 0;
			$pointhsop_address->preorder_delivery_position = 0;
			$pointhsop_address->preorder_invoice_position = 0;
			$pointhsop_address->preorder_footer = '<p style="text-align: center;">Vendeur : {{ SELLER_FIRSTNAME }} ({{ SELLER_ID }})<br />{{ TILL_NAME }}</p>
<p style="text-align: center;">Merci à bientôt<br />'.Configuration::get('PS_SHOP_DOMAIN').'<br />'.Configuration::get('PS_SHOP_EMAIL') . '</p>';
			
			$pointhsop_address->kdo_header = '<p style="text-align: center;">{{ LOGO }}</p>
<p style="text-align: center;">{{ SHOP_STREET }}<br />{{ SHOP_CP }} {{ SHOP_CITY }}<br />{{ SHOP_PHONE }}</p>';
			$pointhsop_address->kdo_footer = '<p style="text-align: center;">Vendeur : {{ SELLER_FIRSTNAME }} ({{ SELLER_ID }})<br />
<p style="text-align: center;">Merci à bientôt<br />'.Configuration::get('PS_SHOP_DOMAIN').'<br />'.Configuration::get('PS_SHOP_EMAIL') . '</p>';
			
			$pointhsop_address->estimate_header = '<p style="text-align: center;">{{ LOGO }}</p>
<p style="text-align: center;">{{ SHOP_STREET }}<br />{{ SHOP_CP }} {{ SHOP_CITY }}<br />{{ SHOP_PHONE }}</p>';
			$pointhsop_address->estimate_customer_position = 0;
			$pointhsop_address->estimate_delivery_position = 0;
			$pointhsop_address->estimate_invoice_position = 0;
			$pointhsop_address->estimate_footer = '<p style="text-align: center;">Vendeur : {{ SELLER_FIRSTNAME }} ({{ SELLER_ID }})<br />{{ TILL_NAME }}</p>
<p style="text-align: center;">Merci à bientôt<br />'.Configuration::get('PS_SHOP_DOMAIN').'<br />'.Configuration::get('PS_SHOP_EMAIL') . '</p>';
			
			
			$pointhsop_address->save();
			
			$new_address_created = $pointhsop_address;
			$addresses = Address::getPrestatillPointshop();
		}
		
		
        
        // on créer un poinshop par defaut UNIQUEMENT si aucun Pointshop n'existe
        $pshops = ACaissePointshop::getAll();
		if($new_address_created !== false)
		{
			$need_logo = true;			
			foreach($pshops as $shop)
			{
				$shop = new ACaissePointshop($shop['id_a_caisse_pointshop']);
				if(Validate::isLoadedObject($shop) && $need_logo === true)
				{
					//on tente de récupérer le logo déjà configurer
					if($shop->base64_ticket_img != '')
					{						
						$filename = uniqid().'.png';
						file_put_contents($this->upload_common_logo_dir.$filename, base64_decode(str_replace('data:image/jpeg;base64,','',''.$shop->base64_ticket_img)));
						$new_address_created->common_picture_path = $filename;
						$new_address_created->save();					
						$need_logo = false;
					}
				}				
				$shop->id_address = $new_address_created->id;
				$shop->save();
			}
		}
		
		/* SINCE SIREEN 2.5 */
		// On créé une catégorie "A Ranger" pour accueillir les produits à la volée
		$trash_cat = Category::searchByName(Context::getContext()->language->id, 'A ranger',true);
		if(empty($trash_cat))
		{
			$cat = new Category();
			$cat->name = array((int)Configuration::get('PS_LANG_DEFAULT') => 'A Ranger');
			$cat->id_parent = Configuration::get('PS_HOME_CATEGORY');
			$cat->link_rewrite = array((int)Configuration::get('PS_LANG_DEFAULT') =>  tools::link_rewrite('A Ranger'));
			$cat->add();	
			
			// On met à jour la configuration
			Configuration::updateValue('PRESTATILL_TRASH_CAT',(int)$cat->id);
		}
		
		
		if(empty($pshops))
		{
			$id_home_category = Configuration::get('PS_HOME_CATEGORY');
			
			$caisse_pointshop = new ACaissePointshop();
	        $caisse_pointshop->name =  $this->l('Caisse n°1');
	        $caisse_pointshop->screen_mode = '';
	        $caisse_pointshop->id_root_category = $id_home_category;			
			$address = $addresses[0];					
			$caisse_pointshop->id_address = Validate::isLoadedObject($address) ? $address->id : 0; //on prends la première addresse;
	        $caisse_pointshop->save();	
			
			//Puis création de sa première page de caisse
			$page  = new ACaissePage();
			$page->id_pointshop = $caisse_pointshop->id;
			$page->name = $this->l('Page par default');
			$page->description = '';
			$page->active = 1;
			$page->is_main = 1;
			$page->save();
			
			//puis on pré-rempli la première case
			$cell=ACaissePageCell::getCellByPosition($page->id,0,0);
	        $cell->colspan = 0;
	        $cell->rowspan = 0;
	        $cell->style = 1;
	        $cell->type = 4;
	        $cell->id_object = $id_home_category;
	        $cell->id_object2 = 0;
	        $cell->save(); 
			
			$caisse_pointshop->date_upd = date('Y-m-d H:i:s');
			$caisse_pointshop->save();	
			
		}
        
        return true;
    }
    
    private function _prepareNewTab()
	{
	 	$id_product = (int)Tools::getValue('id_product');
	 	$product = new Product($id_product);
	 	$link = new Link();
	    $this->context->smarty->assign(
		    array(
		        'custom_field' => '',
		        'languages' => $this->context->controller->_languages,
		        'default_language' => (int)Configuration::get('PS_LANG_DEFAULT'),
		        'pointshops' => ACaissePointshop::getAll(),
		        'pointshops_exclude_list' => ACaissePointshop::getExcludePointshopForproduct($id_product),
		        'cancel_link' => $link->getAdminLink('AdminProducts'),
		        'the_product' => $product,
	    	)
		);	 
	}
	
	public function hookDisplayPrestatillFinancialReportAfterReport(&$params)
	{
		// Récupération des mouvements d'espèces
		//d($params);
		
		$all_movements = ACaisseCash::getCashHistory($params['id_pointshop'], $params['day'],null, false);
		$report_cash_mvt = array();	
		
		if(!empty($all_movements))
		{
			foreach($all_movements as $mvt)
			{
				$report_cash_mvt[$mvt['id_service']][] = $mvt;
			}
		}
			//d($report_cash_mvt);
		$html = '';	
		
		$tpl = $this->createTemplate('prestatill_financial_report_footer.tpl');
		
		$tpl->assign(array(
			// @TODO : Rajouter l'id service
           // 'id_service' => $params['id_service'],
            'currency' => $this->context->currency,
            'report_detail' => $params['report'],
            'report_cash_mvt' => $report_cash_mvt,
            'nb_id_service' => $params['report']->id_service,
            'tocken_order' => Tools::getAdminTokenLite('AdminOrders'),
            'currency' => new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
        ));
		
	    $html .= $tpl->fetch();
		return $html;
			
	}
	
	private function _updatePaymentStates($update = false)
	{
		$context = Context::getContext();
        $id_lang = $context->language->id;
		
		$prestatillshopdefault_exist = false;
		$prestatillpartial_exist = false;
		$prestatillmulti_exist = false;
		$prestatillpartial_refund_exist = false;
		$all_prestatill_states = array();
		
		// prestatillshopdefault, prestatillpartial, prestatillmulti
		$order_states = OrderState::getOrderStates($id_lang);
		foreach($order_states as $state)
		{	
			if($state['module_name'] == 'prestatillshopdefault')
			{
				$prestatillshopdefault_exist = $state['id_order_state'];
			} 
			else if($state['module_name'] == 'prestatillpartial') 
			{
				$prestatillpartial_exist = $state['id_order_state'];
			}
			else if($state['module_name'] == 'prestatillmulti') 
			{
				$prestatillmulti_exist = $state['id_order_state'];
			}
			else if (substr($state['module_name'],0,10) == 'prestatillpartialrefund')
			{
				$prestatillpartial_refund_exist = $state['id_order_state'];
				//d($prestatillpartial_refund_exist);
			}
			else if (substr($state['module_name'],0,10) == 'prestatill')
			{
				$all_prestatill_states[] = $state['id_order_state'];
			}
			 
		}
		
		// On créé les statuts qui ne seraient pas installés
		if($prestatillshopdefault_exist == false)
			$prestatillshopdefault_exist = $this->_createOrderState('prestatillshopdefault', 'Passage en caisse', $id_lang, '#3b3b3b', $update);
			
		if($prestatillpartial_exist == false)
			$prestatillpartial_exist = $this->_createOrderState('prestatillpartial', 'Paiement partiel', $id_lang, '#ff0000', $update);
		
		if($prestatillmulti_exist == false)
			$prestatillmulti_exist = $this->_createOrderState('prestatillmulti', 'Paiement Multiple', $id_lang, '#ff00ff', $update);
		
		/* SINCE 2.5.2 */
		//if($prestatillpartial_refund_exist == false)
		//	$prestatillpartial_refund_exist = $this->_createOrderState('prestatillpartialrefund', 'Remboursement Partiel', $id_lang, '#ec2e15', $update);
		
		// si on est dans le cas d'une mise à jour des statuts
		if($update == true)
		{
			$this->_createOrderState('prestatillshopdefault', 'Passage en caisse', $id_lang, '#3b3b3b', $update, (int)$prestatillshopdefault_exist);
			$this->_createOrderState('prestatillpartial', 'Paiement partiel', $id_lang, '#ff0000', $update, (int)$prestatillpartial_exist);
			$this->_createOrderState('prestatillmulti', 'Paiement Multiple', $id_lang, '#ff00ff', $update, (int)$prestatillmulti_exist);
		}
		
		if(!empty($all_prestatill_states) && $update == true)
		{
			foreach($all_prestatill_states as $prestatill_state)
			{
				$this->_createOrderState(false, false, $id_lang, false, $update, (int)$prestatill_state);
			}
		}
		
		// On met à jour les statuts pour la caisse (au moment de l'installation)
		Configuration::updateValue('ACAISSE_id_partial_state',(int)$prestatillpartial_exist);
		Configuration::updateValue('PS_PRESTATILL_PAYMENT',(int)$prestatillshopdefault_exist);
		Configuration::updateValue('ACAISSE_id_multiple_state',(int)$prestatillmulti_exist);
		Configuration::updateValue('ACAISSE_id_partial_refund',(int)$prestatillpartial_refund_exist);
		
		return true;
	}

	private function _createOrderState($module_name, $name, $id_lang, $color, $update = false, $id_order_state = null)
	{
		if($update == false)
		{
			$order_state = new OrderState();
			
			$order_state->send_email = 0;
			$order_state->module_name = $module_name;
			$order_state->color = $color;
			$order_state->unremovable = false;
			$order_state->hidden = false;
			$order_state->logable = true;
			$order_state->delivery = true;
			$order_state->shipped = true;
			$order_state->pdf_delivery = true;
			$order_state->deleted = false;
			$order_state->name[$id_lang] = $name;
		}
		else
		{
			$order_state = new OrderState($id_order_state);
			if(!Validate::isLoadedObject($order_state))
				return;
		}
		
		// On check si on autorise la génération de factures ou non	
		if(Configuration::get('ACAISSE_INVOICE_IS_ACTIVE') == false)
		{
			$order_state->invoice = false;
			$order_state->pdf_invoice = false;
		}
		else 
		{
			$order_state->invoice = true;
			$order_state->pdf_invoice = true;
		}
		
		// DANS TOUS LES CAS : on ne considère jamais la commande comme payée
		$order_state->paid = false; //IMPORTANT !
		
		$order_state->save();
		
		return $order_state->id;
	}
	
	private function _updateCarrierDefault()
	{
		$context = Context::getContext();
        $id_lang = $context->language->id;
		
		$carrier_list = Carrier::getCarriers($id_lang, $active = 0, $delete = 1);
		$carrier_exist = false;
		
		foreach($carrier_list as $carrier)
		{
			$c = new Carrier($carrier['id_carrier']);
			if($c->name == 'PRESTATILL')
			{
				$carrier_exist = true;
				
				// On met à jour l'information dans la table configuration
				Configuration::updateValue('CARRIER_DEFAULT', (int)$carrier['id_carrier']);
			}	
		}
			
		if($carrier_exist == false)
		{
			// On créé un transporteur "invisible" mais gratuit
			$carrier = new Carrier();
			$carrier->id_tax_rules_group = 0;
			$carrier->name = "PRESTATILL";
			$carrier->active = 0;
			$carrier->deleted = 1;
			$carrier->shipping_method = 1;
			$carrier->delay[1] = "Immediat";
			$carrier->save();
			
			// On met à jour l'information dans la table configuration
			Configuration::updateValue('CARRIER_DEFAULT', (int)$carrier->id);
		}
		
		return true;
	}
    
	public function hookDisplayAdminProductsExtra($params)
	{
		$this->_prepareNewTab();		
		return $this->display(__FILE__,'hook_admin_extra_product.tpl');
	}
	    
	public function hookActionProductUpdate($params) {
		
		if(isset($_POST['is_generic_product']))
		{	
		 	$product = &$params['product'];//new Product($id_product);
		 	
		 	if(Validate::isLoadedObject($product))
		 	{
				$pointshops = ACaissePointshop::getAll();
				$exclusion_list = array();
				foreach($pointshops as $pointshop)
				{
					if($_POST['not_available_in_pointshops_'.$pointshop['id_a_caisse_pointshop']] == 1)
					{
						$exclusion_list[] = $pointshop['id_a_caisse_pointshop'];
					} 
				}
				
				$exclusion_list = implode(';',$exclusion_list);
				$sql = 'UPDATE '._DB_PREFIX_.'product SET not_available_in_pointshops=\''.$exclusion_list.'\' WHERE id_product = '.(int)$product->id;
				DB::getInstance()->execute($sql);	
			}
		}
	}

    public function uninstall() {
        if (
            parent::uninstall() == false
            || !$this->_uninstallModuleTab('ACaisseUSBScreen')
            || !$this->_uninstallModuleTab('ACaisseCashView')
            || !$this->_uninstallModuleTab('ACaissePageConfigure')
            || !$this->_uninstallModuleTab('ACaissePointshopConfigure')
            || !$this->_uninstallModuleTab('ACaisseUse')
            || !$this->_uninstallModuleTab('ACaisseMenu')
        ) {
            return false;
        }
        return true;
    }

    private function _registerNewTab() {
        $id=$this->_installModuleTab('ACaisseMenu',array($this->context->language->id=>'Caisse'));
        $this->_installModuleTab('ACaisseUse',array($this->context->language->id=>'Ouvrir ma caisse'),$id);
        $this->_installModuleTab('ACaisseAddress',array($this->context->language->id=>'Points de vente'),$id);
        $this->_installModuleTab('ACaissePointshopConfigure',array($this->context->language->id=>'Configuration des caisses'),$id);
        $this->_installModuleTab('ACaissePageConfigure',array($this->context->language->id=>'Gestion des pages de caisse'),$id);
        $this->_installModuleTab('ACaisseCashView',array($this->context->language->id=>'Mouvements des fonds de caisse'),$id);
        $this->_installModuleTab('ACaisseUSBScreen',array($this->context->language->id=>'Afficheur client'),$id);

        @copy(_PS_MODULE_DIR_.$this->name.'/logo.gif', _PS_MODULE_DIR_.$this->name.'/ACaisseMenu.gif');

        return true;
    }
    
    public function consolidateProductCache()
    {
    	$use_sh = false;            
        if($use_sh === true)
        {            
            $cmd1 = _PS_ROOT_DIR_.'acaisse'.''.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'generate-products.sh';
            $result = exec($cmd1,$arg2, $arg3);
		}
		else
		{
            $cmd1 = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'acaisse'.''.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'generate-products.php';
            include($cmd1);
		}
        
        Configuration::updateValue('ACAISSE_last_cache_generation_pricelist',date('Y-m-d H:i:s'));
    }
    
    /***************************************************/
    /***** Traitement principale de page la config *****/
    /***************************************************/
    public function getContent() {
        $html='';
        $debug='';
        $id_lang = (int)Context::getContext()->language->id;
        
        // si on regénère les caches produits on renvoit d'abors la liste des id de produit à regénérer 
        if(isset($_POST['action']) && $_POST['action'] == 'loadAllProductsIDS') {
            $forceAll = ($_POST['force_all'] == 'true');
            
            $sql = 'SELECT id_product FROM '._DB_PREFIX_.'product';
            
            if(!$forceAll) {
                $sql.=' WHERE date_upd >= \''.Configuration::get('ACAISSE_last_cache_generation_pricelist').'\'';
            }
            
            $ids = DB::getInstance()->executeS($sql);
            
            Configuration::updateValue('ACAISSE_last_cache_generation_pricelist',date('Y-m-d H:i:s'));
            
            $ids_results = array();
            if(function_exists('array_column'))
            {
                $ids_results = array_column($ids,'id_product');
            }
            else
            {
                foreach($ids as $row)
                {
                    $ids_results[] = $row['id_product']; 
                }
            }            
            
            header('Content-type: application/json');
            echo(json_encode($ids_results));
            die();
        }
        
        // on renvoit en suite l'id du produit à regénérer. Le faire en deux temps permet simplement d'afficher l'avancement
        if(isset($_POST['action']) && $_POST['action'] == 'loadProduct') {
            $id_product = 0+$_POST['id_product'];
            
            if($id_product > 0) {            
                $part = ACaissePointshop::exportProductPart($id_product);
                
                //$file_ean = _PS_ROOT_DIR_._MODULE_DIR_.'acaisse'.'/js/cache/products-ean/ean-'.$id_product.'.cache.js';
				$file_ean = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'acaisse'.''.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'products-ean'.DIRECTORY_SEPARATOR.'ean-'.$id_product.'.cache.js';
                //$file_ean = _PS_ROOT_DIR_._MODULE_DIR_.'acaisse'.'/js/cache/products-parts/product-'.$id_product.'.cache.js';
				$file_part = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'acaisse'.''.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'products-parts'.DIRECTORY_SEPARATOR.'product-'.$id_product.'.cache.js';
                
                @unlink($file_ean);
                @unlink($file_part);
                
                if($part !== false) {
                    file_put_contents($file_part,''.substr(substr(json_encode($part['catalog']),1),0,-1).',');
                    
                    if(!empty($part['ean'])) {
                        file_put_contents($file_ean,''.substr(substr(json_encode($part['ean']),1),0,-1).',');
                    }
                }
            }

            $response=array(
                'success' => true,
                'id_product' => $id_product,
                //'results' => $part === false?false:$part,
            );

            header('Content-type: application/json');
            echo(json_encode($response));
            die();
        }

        // on renvoit en suite l'id du produit à regénérer. Le faire en deux temps permet simplement d'afficher l'avancement
        if(isset($_POST['action']) && $_POST['action'] == 'resyncProductStock') {
            $id_product = 0+$_POST['id_product'];
            
			$this->_resyncProductStockAvailability($id_product);
			
            $response=array(
                'success' => true,
                'id_product' => $id_product,
            );

            header('Content-type: application/json');
            echo(json_encode($response));
            die();
        }

        
        // si on veut consolider le cache on regroupe simplement toutes les parties du cache en un seule fichier
        if(isset($_POST['action']) && $_POST['action'] == 'consolidateProductCache') {
                
            // on laisse linux concaténer car il va bien plus vite que php... OU PAS ^^
            
            $this->consolidateProductCache();
            
            Configuration::updateValue('ACAISSE_last_cache_generation_pricelist',date('Y-m-d H:i:s'));
            
            $response=array(
                'success' => true,
            );
            
            header('Content-type: application/json');
            echo(json_encode($response));
            die();
        }
        
        // si on a envoyé le formulaire de configuration générale de la caissee
        if (Tools::isSubmit('submitCustomerOptions')) {
            //$html.='<pre>'.print_r($_POST,true).'</pre>';
            
            // on traite la configuration des clients
            Configuration::updateValue('ACAISSE_profilsID', Tools::getValue('profilsID'));
            Configuration::updateValue('ACAISSE_profilsIDNoChecking', Tools::getValue('profilsIDCheckingNotAllowed'));
            Configuration::updateValue('ACAISSE_defaultProfilID', Tools::getValue('defaultProfilID'));
            Configuration::updateValue('ACAISSE_searchActiveCustomer', Tools::getValue('searchActiveCustomer'));
            Configuration::updateValue('ACAISSE_useForceGroup', Tools::getValue('useForceGroup'));
			
            $tmp_search=array();
            foreach(array_merge(array('id_customer'=>true),Customer::$definition['fields']) as $key => $value) {
                if (Tools::getIsset('customerSearchFields'.$key)) {
                    $tmp_search[]=$key;
                }
            }

            Configuration::updateValue('ACAISSE_customerSearchFields',implode(';',$tmp_search));
		}

		if (Tools::isSubmit('submitGeneralOptions')) {  
	        Configuration::updateValue('ACAISSE_FORCE_DEFAULT_CARRIER', Tools::getValue('FORCE_DEFAULT_CARRIER'));
		}
		
		if(Tools::isSubmit('ACAISSE_export_only_product_in_default_category'))
		{			
	        Configuration::updateValue('ACAISSE_export_only_product_in_default_category', Tools::getValue('ACAISSE_export_only_product_in_default_category'));
	        Configuration::updateValue('ACAISSE_dont_export_inactive_product', Tools::getValue('ACAISSE_dont_export_inactive_product'));
		}
		
		
		if (Tools::isSubmit('submitPaymentOptions')) {  
			/**
			 * SIREEN 2.0
			 */
			Configuration::updateValue('ACAISSE_id_partial_state', Tools::getValue('ACAISSE_id_partial_state'));
			Configuration::updateValue('PS_PRESTATILL_PAYMENT', Tools::getValue('PS_PRESTATILL_PAYMENT'));
			Configuration::updateValue('ACAISSE_id_multiple_state', Tools::getValue('ACAISSE_id_multiple_state'));
			Configuration::updateValue('ACAISSE_INVOICE_IS_ACTIVE', Tools::getValue('ACAISSE_INVOICE_IS_ACTIVE'));
			
			// On met à jour les statuts de paiement si nécessaire
			$this->_updatePaymentStates(true);
			
			// Attribuer la meme valeur à PS_PAYMENT...
			

        }
        
        $token = Tools::getAdminTokenLite('AdminModules');
        $assigns = array();
        
        if (Tools::getIsset('exportAll') && $this->_exportAll() == true) {
            //TODO Dynamiser la liste des profils
            $assigns['exportAll']=true;
        }
        
        if (Tools::getIsset('exportPrices') && $this->_exportPriceList()==true) {
            //TODO Dynamiser la liste des profils
            $assigns['exportPrices']=true;
        }

        if (Tools::getIsset('exportPointshopPages') && $this->_exportPointshopPages()) {
            $assigns['exportPointshopPages']=true;
        }

        if (Tools::getIsset('exportProfiles') && $this->_exportProfiles()) {
            $assigns['exportProfiles']=true;
        }
        
        if (Tools::getIsset('exportCategories') && $this->_exportCategoryList()) {
            $assigns['exportCategories']=true;
        }

        $assigns['token'] = $token;
        $assigns['module']=$this;
        
        $assigns['ACAISSE_searchActiveCustomer'] = Configuration::get('ACAISSE_searchActiveCustomer');
        $assigns['ACAISSE_useForceGroup'] = Configuration::get('ACAISSE_useForceGroup');		
        $assigns['ACAISSE_profilsID'] = htmlentities(Configuration::get('ACAISSE_profilsID'),ENT_COMPAT,'UTF-8');
        $assigns['ACAISSE_profilsIDNoChecking'] = htmlentities(Configuration::get('ACAISSE_profilsIDNoChecking'),ENT_COMPAT,'UTF-8');
        
		/**
		 * SIREEN 2.0
		 */
		$assigns['ACAISSE_FORCE_DEFAULT_CARRIER'] = Configuration::get('ACAISSE_FORCE_DEFAULT_CARRIER');
		$assigns['ACAISSE_id_partial_state'] = Configuration::get('ACAISSE_id_partial_state');
		$assigns['PS_PRESTATILL_PAYMENT'] = Configuration::get('PS_PRESTATILL_PAYMENT');
		$assigns['ACAISSE_id_multiple_state'] = Configuration::get('ACAISSE_id_multiple_state');
		$assigns['ACAISSE_INVOICE_IS_ACTIVE'] = Configuration::get('ACAISSE_INVOICE_IS_ACTIVE');
		$assigns['ACAISSE_export_only_product_in_default_category'] = Configuration::get('ACAISSE_export_only_product_in_default_category');
		$assigns['ACAISSE_dont_export_inactive_product'] = Configuration::get('ACAISSE_dont_export_inactive_product');
		
		/**
		 * SIREEN 2.1
		 */
		$assigns['is_prestatill'] = substr(Context::getContext()->employee->email,-1*strlen('@prestatill.com')) == '@prestatill.com';
		
		
		// On vérifie si le transporteur par défaut est payant ou non
		$carrier_list = Carrier::getCarriers($id_lang, $active = 1, $delete = 0);
		$default_carrier = Carrier::getDefaultCarrierSelection($carrier_list);
		$default_c = new Carrier((int)$default_carrier);
		$carrier_is_free = false;

		if($default_c->is_free == 1 || Configuration::get('PS_CARRIER_DEFAULT') == -1)
		{
			// On met à jour l'information dans la table configuration
			$carrier_is_free = true;
		}
		
		$assigns['CARRIER_IS_FREE'] = $carrier_is_free;
		
		//On récupère la liste des statuts de paiement
		$order_states = OrderState::getOrderStates($id_lang);
		$assigns['ORDER_STATES'] = $order_states;
		
		
		
		/////////////////// END UPDATE SIREEN 2.0 //////////////////////////
		
        $currentSearchCustomerFields=self::getCustomerSearchFields();
        
        $notAlowedField=array(
            'passwd',
            'last_passwd_gen',
            'ip_registration_newsletter',
            'newsletter_date_add',
            'outstanding_allow_amount',
            'show_public_prices',
            'id_risk',
            'max_payment_days',
            'active',
            'deleted',
            'note',
            'id_shop',
            'id_shop_group',
            'id_lang',
            'date_add',
            'date_upd'
        );
        
        $assigns['clientAttributes'] = array();
        foreach(array_merge(array('id_customer'=>true),Customer::$definition['fields']) as $key => $detail) {
            if(!in_array($key,$notAlowedField)) {
                $assigns['clientAttributes'][$key]=(in_array($key, $currentSearchCustomerFields)?true:false);
            }
        }

        $paiementModules=PaymentModule::getInstalledPaymentModules();
        $assigns['paiementModules'] = $paiementModules;

        $states_available=OrderStateCore::getOrderStates($id_lang);
        array_unshift($states_available,array('id_order_state'=>0,'name'=>'ne pas activer ce mode de paiement'));
        $assigns['states_available'] = $states_available;
        
        $assigns['payment_modes'] = array();
        for($i=0; $i<self::MAX_PAYMENT_MODE; $i++)
        {
            $assigns['payment_modes'][$i] = array(
                'ACAISSE_mode_orderstate'=>(int)Configuration::get('ACAISSE_mode'.$i.'_orderstate'),
                'ACAISSE_mode_payment_module'=>Configuration::get('ACAISSE_mode'.$i.'_payment_module')
            );
            
            if ($i!=0) {
                $assigns['payment_modes'][$i]['ACAISSE_mode_name']=htmlentities(''.Configuration::get('ACAISSE_mode'.$i.'_name'),ENT_COMPAT,'UTF-8');
            }
        }
		
		$assigns['module_version'] = $this->version;
		$assigns['module_display_name'] = $this->displayName;

        $this->context->smarty->assign($assigns);
		
        $html = $this->display(__FILE__, 'getContent.tpl');

        return $html;
    }

	public function hookDisplayBackOfficeHeader($params)
	{
			$this->context->controller->addCSS($this->_path.'css/admin-theme-caisse.css');;

	} 

    /***************************************************/
    /********** récupération des id sérialisé **********/
    /***************************************************/
    public static function getProfilsID() {
        return explode(';',''.Configuration::get('ACAISSE_profilsID'));
    }

    public static function getProfilsIDCheckingNotAllowed() {
        return explode(';',''.Configuration::get('ACAISSE_profilsIDNoChecking'));
    }

    public static function getCustomerSearchFields() {
        return explode(';',''.Configuration::get('ACAISSE_customerSearchFields'));
    }

    /***************************************************/
    /**************** export des caches ****************/
    /***************************************************/
    private function _exportPointshopPages() {
        return ACaissePointshop::exportPointshopPages();
    }
    
    private function _exportProfiles() {
        return ACaissePointshop::exportProfiles();
    }
    
    private function _exportCategoryList() {
        return ACaissePointshop::exportCategoryList();
    }
    
    private function _exportAll() {
        return
            ACaissePointshop::exportProfiles() &&
            ACaissePointshop::exportPointshopPages() &&
            ACaissePointshop::exportCategoryList();
    }
	
	/***************************************************/
    /**************** sync stock level *****************/
    /***************************************************/
    private function _resyncProductStockAvailability($id_product) {        
		StockAvailable::synchronize($id_product);
    }
	
	/***************************************************/
    /***** Affichage des actualités dans la caisse *****/
    /***************************************************/
    public function hookDisplayPrestatillNews($params) {
    	
		$url = 'http://store.prestatill.com/actu.php'; 
		
		if(self::_is_404($url))
		{
			$timeout = 5; 
		
			$ch = curl_init($url); 
			
			curl_setopt($ch, CURLOPT_FRESH_CONNECT, true); 
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); 
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout); 
			
			if (preg_match('`^https://`i', $url)) 
			{ 
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
			} 
			
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			$page_content = curl_exec($ch);
			curl_close($ch); 
			return '<div id="actuContainer">'.$page_content.'</div>'; 
			
		}
		
		
    }
	
	public static function _is_404($url) {
		$handle = curl_init($url); 
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE); 
	 
		/* Get the HTML or whatever is linked in $url. */ 
		$response = curl_exec($handle);
	  
		/* Check for 404 (file not found). */ 
		$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE); 
		curl_close($handle);
	  
		/* If the document has loaded successfully without any redirection or error */ 
		if ($httpCode >= 200 && $httpCode < 300) {
			return true; 
		} else {
			return false; 
		} 
	}
    
    /***************************************************/
    /**************** creation des hooks ***************/
    /***************************************************/
    public function hookActionBeforeSendOrderConfirmationMail($params) {
    	
		//d($params);
		
        $pointshop = new ACaissePointshop(isset($params['cart'])?$params['cart']->id_pointshop:$params['order']->id_pointshop);
        $ticket = new ACaisseTicket(isset($params['cart'])?$params['cart']->id_ticket:$params['order']->id_ticket); 
        
        
        if (Validate::isLoadedObject($pointshop) && $pointshop->send_mail_to_custumer == 0)
        {
            $params['hook_executed'] = true;
        }
		
		// Version 2.3
		// si pas de détaxe alors il faut réassigner l'id_address_invoice de ticket au cart/order
		$order = $params['order'];
		
		if(Validate::isLoadedObject($ticket))
		{
			$order->id_address_invoice = $ticket->id_address_invoice;
			$order->save();
			
			//si facture demandée
			if($ticket->with_invoice)
			{					
				//$id_order_state = 171; //@TODO rendre paramétré			
				
				//self::changeOrderState($id_order_state,$order);
			}
			$params['order'] = $order;		
		}
		
    }
	
	
	/*
	 * API TEST : synchro manuelle d'une commande
	 */
	public static function changeOrderState($id_order_state, $order)
	{
		$order_state = new OrderState($id_order_state);

				if (!Validate::isLoadedObject($order_state))
					$this->errors[] = Tools::displayError('The new order status is invalid.');
				else
				{
					$current_order_state = $order->getCurrentOrderState();
					if ($current_order_state->id != $order_state->id)
					{
						// Create new OrderHistory
						$history = new OrderHistory();
						$history->id_order = $order->id;
						$history->id_employee = 0;

						$use_existings_payment = false;
						if (!$order->hasInvoice())
							$use_existings_payment = true;
						$history->changeIdOrderState((int)$order_state->id, $order, $use_existings_payment);

						$carrier = new Carrier($order->id_carrier, $order->id_lang);
						$templateVars = array();
						if ($history->id_order_state == Configuration::get('PS_OS_SHIPPING') && $order->shipping_number)
							$templateVars = array('{followup}' => str_replace('@', $order->shipping_number, $carrier->url));

						// Save all changes
						if ($history->addWithemail(true, $templateVars))
						{
							// synchronizes quantities if needed..
							if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))
							{
								foreach ($order->getProducts() as $product)
								{
									if (StockAvailable::dependsOnStock($product['product_id']))
										StockAvailable::synchronize($product['product_id'], (int)$product['id_shop']);
								}
							}
						}
					}
				}
	}
	
	
    
    /**
     * lorsqu'un produit est crée OU maj
     * contrairement à hookActionProductUpdate qui n'agit que lors de la mise a jour
     * d'un produit et pas à sa création
     */
    public function hookActionObjectCombinationUpdateAfter($params) {
        if(Validate::isLoadedObject($params['object']))
        {
            $this->hookActionProductSave(array('id_product' => $params['object']->id_product));
        }
    }
    
    /**
     * lorsqu'un produit est crée OU maj
     * contrairement à hookActionProductUpdate qui n'agit que lors de la mise a jour
     * d'un produit et pas à sa création
     */
    public function hookActionObjectCombinationAddAfter($params) {
        if(Validate::isLoadedObject($params['object']))
        {
            $this->hookActionProductSave(array('id_product' => $params['object']->id_product));
        }
    }
    
    public function hookActionObjectCombinationDeleteAfter($params) {
        if(Validate::isLoadedObject($params['object']))
        {
            $this->hookActionProductSave(array('id_product' => $params['object']->id_product));
        }
    }
    
    
    /**
     * lorsqu'un produit est crée OU maj
     * contrairement à hookActionProductUpdate qui n'agit que lors de la mise a jour
     * d'un produit et pas à sa création
     */
    public function hookActionProductSave($params) {
        /**
         *  @TODO dynamiser id_lang
         */
        
        $id_product = $params['id_product'];
        $part = ACaissepointshop::exportProductPart($id_product);
        
        $file_ean = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'acaisse'.''.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'products-ean'.DIRECTORY_SEPARATOR.'ean-'.$id_product.'.cache.js';
        $file_part = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'acaisse'.''.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'products-parts'.DIRECTORY_SEPARATOR.'product-'.$id_product.'.cache.js';
         
        @unlink($file_ean);
        @unlink($file_part);
            
        if($part !== false){
            file_put_contents($file_part,''.substr(substr(json_encode($part['catalog']),1),0,-1).',');
            
            if(!empty($part['ean'])) {
                file_put_contents($file_ean,''.substr(substr(json_encode($part['ean']),1),0,-1).',');
            }
        }
        
        // on laisse linux concaténer car il va bien plus vite que php...
        $this->consolidateProductCache();
    }
    
    /**
     * Lorsqu'un produit est supprimé
     */
    public function hookActionObjectProductDeleteBefore($params) {
        $product = $params['object'];
        $id_product = $product->id;
        
		$file_ean = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'acaisse'.''.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'products-ean'.DIRECTORY_SEPARATOR.'ean-'.$id_product.'.cache.js';
        $file_part = _PS_ROOT_DIR_.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'acaisse'.''.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'products-parts'.DIRECTORY_SEPARATOR.'product-'.$id_product.'.cache.js';
        
        //on vire les fragments
        @unlink($file_ean);
        @unlink($file_part);
        
        //on déclenche la regénération du cache de catégorie
        //@TODO pourra être optimiser
        $this->_exportCategoryList();
        $this->consolidateProductCache();
    }
    
    public function hookActionObjectCategoryDeleteBefore($params)
    {
        //@todo pourrait etre optimiser en fragmentant le cache de categorie
        /*
        $category = $params['object'];
        $cat_obj = new Category($category['id_category'],$id_lang);
        $product_ids = array();
        $tmp = $cat_obj->getProductsWs();
        
        foreach($tmp as $id)
        {
            $product_ids[] = 0+$id['id'];
        }
        */
        $this->hookActionObjectCategoryUpdateAfter($params);
    }

    public function hookActionObjectCategoryUpdateAfter($params)
    {        
        $this->_exportCategoryList();
    }
    
    public function hookActionObjectCategoryAddAfter($params)
    {
        $this->hookActionObjectCategoryUpdateAfter($params);
    }
    
    /**
     * Liste des HOOKs devant regénérer les pages de caisse
     * et/ou les informations sur les points de vente
     */    
    public function hookActionObjectACaissePointshopAddAfter($params)
    {
        ACaissePointshop::exportPointshopPages();
    }    
    public function hookActionObjectACaissePointshopUpdateAfter($params)
    {
        ACaissePointshop::exportPointshopPages();
    }
    public function hookActionObjectACaissePageAddAfter($params)
    {
        ACaissePointshop::exportPointshopPages();
    }
    
    public function hookActionObjectACaissePageUpdateAfter($params)
    {
        ACaissePointshop::exportPointshopPages();
    }
    public function hookActionObjectACaissePageCellAddAfter($params)
    {
        ACaissePointshop::exportPointshopPages();
    }
    
    public function hookActionObjectACaissePageCellUpdateAfter($params)
    {
        ACaissePointshop::exportPointshopPages();
    }
    
    /**
    * Hook permettant la suppression des produit éphémère
    */
    public function hookActionValidateOrder($params)
    {
    	$order = $params['order'];
    	foreach($order->product_list as $product)
    	{
    		$product = new Product($product['id_product']);
    		if($product->remove_after_payment_validation == 1)
    		{
    			$product->delete();
    		}
    	}
    }
    

    /**
     * Méthode pour installer un nouvel onglet dans le menu
     * 
     * @param string    $tabClassName
     * @param array     $tabLabel
     * @param int       $parent_tab_id
     * @param string    $module_name
     * @return id_new_tab
     */
    private function _installModuleTab($tabClassName, $tabLabel, $parent_tab_id = 0, $module_name = NULL) {
        $tab = new Tab();
        $tab->name = $tabLabel;
        $tab->class_name = $tabClassName;
        $tab->id_parent = $parent_tab_id;
        $tab->module = $module_name === NULL ? $this->name : $module_name;
        $tab->add();
        return $tab->id;
    }

    /**
     * Méthode pour désinstaller un onglet dans le menu
     * 
     * @param string $tabClass
     * @return boolean
     */
    private function _uninstallModuleTab($tabClass) {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            return $tab->delete();
        }
        return false;
    }

    private function _sendMail($tplVar, $recipients = null, $template, $message) {
        Mail::Send(
            $this->context->language->id, //lang
            $template, //template
            Mail::l('Résumé de la ' . $message . ' de caisse', $this->context->language->id), //sujet
            $tplVar, $recipients, //@TODO (doit être un array pour plusieurs destinataire
            '-', //to_name
            strval(Configuration::get('PS_SHOP_NAME')), //from
            strval(Configuration::get('PS_SHOP_NAME')), //from name
            null, //file attachment
            null, //mod smtp
            $this->getLocalPath() . 'mails/' //tpl path
        );
    }

    public function cronJobMailer() {
        
        $pointshops = ACaissePointshop::getAllPointshop();
        
        $today = date('Y-m-d');
        
        $aWeekAgo = date('Y-m-d', strtotime('-7 days', strtotime($today))); // on récupère la date de la semaine dernière
        
        $caBetweenDate = array();
        
        $style = 'style="vertical-align: middle; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; text-align: left; min-width: 0px; width: 20%; color: #222222; font-family: \'Helvetica\', \'Arial`\', sans-serif; font-weight: normal; line-height: 19px; font-size: 14px; background: #f2f2f2; margin: 0; padding: 10px; border: 1px solid #d9d9d9;" align="left" bgcolor="#f2f2f2" valign="middle"';
        
        //pour chaque Pointshop, on envoie un mail avec un tableau html
        foreach ($pointshops as $pointshop) {

            $tplVars = array();
            
            $pointShop = new ACaissePointshop($pointshop['id_a_caisse_pointshop']);
            
            //on recupere le ca de chaque poinstshop entre aujourd'hui et la semaine dernière
            $caBetweenDate = $pointShop->getCaBetweenDate($aWeekAgo, $today);
            $tplVars['{pointshop_name}'] = $caBetweenDate['pointshop'];

            $tplVars['{detail}'] = '';

            //les mode de paiement pour le template
            $paymentMode = array();
            for($i=0 ; $i<ACaisse::MAX_PAYMENT_MODE ; $i++){
                $accent = array('é','è');
                $nonAccent = array('e','e');
                $paymentName = str_replace($accent,$nonAccent, Configuration::get('ACAISSE_mode'.$i.'_name'));

                if($paymentName != ''){
                    $paymentMode[] = $paymentName;
                }
            }

            //on assigne les infos pour le template
            $this->smarty->assign('paymentMode', $paymentMode);
            $this->smarty->assign('style', $style);
            $this->smarty->assign('caBetweenDate', $caBetweenDate);
            $content = $this->display(__FILE__, './views/templates/admin/cron_mail.tpl');
            $tplVars['{detail}'] = $content;

            //envoie mail avec l'info
            $this->_sendMail($tplVars, explode(';', $pointShop->compta_email), 'cron_mail', 'semaine');
        }
    }
    
    private function _installSql()
    {
        include(dirname(__FILE__).'/sql/install.php');
        $result = true;
	    foreach ($sql as $request){
            if (!empty($request))
            $result &= Db::getInstance()->execute(trim($request));
        }
        return $result;
    }
	
	//surcharge pour tester aussi le dossier view dans le module
    public function createTemplate($tpl_name,$type = 'hook')
    {
    	//d(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    	//d(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    		
    	
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        return parent::createTemplate($tpl_name);
    }

    public function getTplPath($tpl_name)
    {
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name;
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name;
        }
    }
}