<?php

trait PrestatillCustomerController {
    private function ajaxRetreiveCustomerInformation($params)
    {
        $customer = new Customer($params['id_customer']);
		
        $error=false;
        if(!Validate::isLoadedObject($customer))
        {
            //erreur user invalid
            $error='Erreur 000928 : Le client n\'existe plus.';

        }
		
		$groups = $customer->getGroups();
		
		// On récupère l'id de la première adresse si elle existe
		$id_address = Address::getFirstCustomerAddressId($customer->id);
	
		if($id_address > 0)
		{
			$address = new Address((int)$id_address);
		}
		else
		{
			$address = null;
		}
		
		// A FAIRE PAR LA SUITE POUR RECUPERER TOUTES LES ADRESSES
		// : $addresses = $customer->getAddresses((int)Configuration::get('PS_LANG_DEFAULT'));

        if($error!==false) //il y a une erreur
        {
            $response=array('success'=>false,
                            'request'=>$params,
                            'errorMsg'=>$error,
                        );
            $this->_returnJSON($response);
        }
        else {
            $datas=array(
            	'customer'=>$customer,
            	'address' => $address,
            	'addresses' => $customer->getAddresses(Context::getContext()->language->id),
            	'groups' => $groups,
            	'additionnal_customer_informations' => array(),
			);

            $response=array('success'=>true,
                            'request'=>$params,
                            'response'=>$datas,
                        );
						
			Hook::exec('actionPrestaTillCompleteCustomerInformations',array('response' => &$response));
						
            $this->_returnJSON($response);
        }
    }

    private function ajaxSearchCustomer($params) {
        $words=explode(' ',$params['q']);
        $sql_search=array();
        $search_fields=ACaisse::getCustomerSearchFields();
        foreach($words as $word)
        {
            $tmp=array();
            foreach($search_fields as $field) {
                if($field == 'id_customer' || $field == 'card_number')
                {
                    $tmp[]='`'.$field.'` = \''.str_replace("'", "\'", $word).'\'';
                }
                else {
                    $tmp[]='`'.$field.'` LIKE \'%'.str_replace("'", "\'", $word).'%\'';
                }
            }
            $sql_search[]=implode(' OR ',$tmp);
        }
        $sql='SELECT id_customer FROM '._DB_PREFIX_.'customer WHERE ('.implode(') AND (', $sql_search).')';

        if (Configuration::get('ACAISSE_searchActiveCustomer')== 0)
            $sql .= ' AND active = 1';
		
		$pointshop = new ACaissePointshop($params['id_pointshop']);
		if(Validate::isLoadedObject($pointshop))
		{
			if($pointshop->common_customers == true)
			{
				//$sql .= ' AND is_common = 1';			
			}	
		}
       
        $db=DB::getInstance();
        $ids=$db->executeS($sql);
        
        $customers = array();
        for ($i = 0; $i < count($ids); $i++){
            $customer = new Customer($ids[$i]['id_customer']);
			
			if($pointshop->common_customers == false)
			{
				if($customer->is_common == 1)
				{
					$customers[$i]['customer'] = $customer;
			    	$customers[$i]['group'] = $customer->getGroups();
				}
				else if($customer->id_pointshop != ' ' && $customer->id_pointshop != '')
				{
					$pointshops = @unserialize($customer->id_pointshop);
					if(in_array($params['id_pointshop'], $pointshops))
					{
						$customers[$i]['customer'] = $customer;
            			$customers[$i]['group'] = $customer->getGroups();
					}
					
				}
			}
			else
			{
				$customers[$i]['customer'] = $customer;
			    $customers[$i]['group'] = $customer->getGroups();
			}
			
			
           // $customers[$i] = $customer;
            /*$customer = new Customer($ids[$i]['id_customer']);
             
            */
        }
        
        /*$customers=array();
        foreach($ids as $id)
        {
            $customer = new Customer($id['id_customer']);
            $customer->allGroups = $customer->getGroups();
            //$customers[]=new Customer($id['id_customer']);
            $customers[] = $customer;
        }*/

        $response=array('success'=>true,
                            'action'=>'SearchCustomer',
                            'request'=>$params,
                            'response'=>array('nb_results'=>count($customers),'customers'=>$customers),
                        );

        $this->_returnJSON($response);

    }
    
    private function genNewCustomerPassword($len) {
        return $this->genNewCartRuleCode($len);
    }
    
    private function ajaxEditCustomer($params)
    {
        $response=array();
        $error = array();
		$addressExist = 0;
		$id_address = null;
		//d($params);
		
		// On vérifie en premier lieu si l'adresse email de l'utilisateur existe en base ou non
		if(!empty($params['datas']['email']))
		{
			if(Validate::isEmail($params['datas']['email']))
			{
				$customer_exist = Customer::customerExists($params['datas']['email'], true);
				
				if($customer_exist > 0)
				{
					$customer = new Customer((int)$customer_exist);
					$id_address = Address::getFirstCustomerAddressId($customer->id);
					
					if($customer->id_pointshop != $params['datas']['id_pointshop'])
					{
						// On vérifie si on est dans une caisse et si c'est le cas on enregistre l'id_pointshop
						//$customer->id_pointshop = 0;
						$customer->is_common = 1;
						
						if($customer->id_pointshop != '')
						{
							$id_pointshops = unserialize($customer->id_pointshop);
						}
						else 
						{
							$id_pointshops = array();
						}
							$id_pointshops[] = $params['datas']['id_pointshop'];
							$customer->id_pointshop = serialize($id_pointshops);
					}
					//d($customer->id_pointshop);
				}
				else
				{
					if ($params['datas']['id'] == 0) {
			            $customer = new Customer();
			            $customer->passwd = $this->genNewCustomerPassword(8);
						
						// On vérifie si on est dans une caisse et si c'est le cas on enregistre l'id_pointshop
						$id_pointshops = array();
						$id_pointshops[] = $params['datas']['id_pointshop'];
						$customer->id_pointshop = serialize($id_pointshops);
						
						$customer->is_common = 0;
			        } 
				}
			}	
			else 
			{
				$error['email'] = true;
				$customer = new Customer();
			}
		}
		else
		{
			if ($params['datas']['id'] == 0) {
	            $customer = new Customer();
	            $customer->passwd = $this->genNewCustomerPassword(8);
	        }
			else 
			{
				$customer = new Customer((int)$params['datas']['id']);
			}
		} 
        
        if (!(empty($params['datas']['firstname']))) {
            $customer->firstname = $params['datas']['firstname'];
        } else {
            $error['firstname'] = true;
        }
        
        if(!(empty($params['datas']['lastname']))) { 
            $customer->lastname = $params['datas']['lastname'];
        } else {
            $error['lastname'] = true;
        }
        
		// Il faut tester l'email, il est forcément unique
        if (Validate::isEmail($params['datas']['email'])) {
			$customer->email = $params['datas']['email']; 
        } else {
            $error['email'] = true;
        }
        
        if (!(empty($params['datas']['birthday']))) {
            if (Validate::isDate($params['datas']['birthday'])) {
                $customer->birthday = $params['datas']['birthday'];
            } else {
                $error['birthday'] = true;
            }
        }
        
        if (!(empty($params['datas']['id_default_group']))) {
            if (Validate::isInt($params['datas']['id_default_group'])) {
                $customer->id_default_group = $params['datas']['id_default_group'];
            } else {
                $error['id_default_group'] = true;
            }
        }
		else 
		{
            $customer->id_default_group = (int)Configuration::get('PS_UNIDENTIFIED_GROUP');
        }
		
		// NEW COMPANY + SIRET
		$customer->company = $params['datas']['company'];		

		if (!(empty($params['datas']['siret']))) {
            if (Validate::isSiret($params['datas']['siret'])) {
                $customer->siret = $params['datas']['siret'];
            } else {
                $error['siret'] = true;
            }
        }
		
		/* Since 2.4
		 * @params : Added Newsletter and Opt-in
		 */
		if (!(empty($params['datas']['newsletter']))) {
        	$customer->newsletter = (int)$params['datas']['newsletter'];
        }
		
		if (!(empty($params['datas']['optin']))) {
        	$customer->optin = (int)$params['datas']['optin'];
        }
		
        
		
		if(empty($error))
		{
			$customer->save();
			if(!empty($params['datas']['groups']))
			{
				$customer->updateGroup($params['datas']['groups']);
			}
			else
			{
				$customer->updateGroup(array((int)Configuration::get('PS_UNIDENTIFIED_GROUP')));
			}
        	
	        //$address = new Address($params['datas']['id_address']);
	        
	        // Pour l'instant on se contente d'une adresse dans la caisse, on fera les multi-adresses plus tard :/
	        // On récupère l'id de la première adresse si elle existe
			if($id_address == null)
			{
				$id_address = Address::getFirstCustomerAddressId($customer->id);
			}
	
			$address = new Address((int)$id_address);
	
	        if (!Validate::isLoadedObject($address)) {
				$address = new Address();
	        }
	        
	        if ($address != null) 
	        {
	            $addressError = 0;
	            $address->firstname = $params['datas']['firstname'];
	            $address->lastname = $params['datas']['lastname'];
			
	            $address->id_customer = $customer->id;
	            
	            if (!(empty($params['datas']['alias'])) && Validate::isGenericName($params['datas']['alias'])) {
	                $address->alias = $params['datas']['alias'];
	            } else{
	                $address->alias = 'NewAddress';
	            }
	
	            if (!(empty($params['datas']['address1'])) && Validate::isAddress($params['datas']['address1'])) {
	                $address->address1 = $params['datas']['address1'];
	            } else {
	                $address->address1 = 'A renseigner';
	            }
	
	            if (!(empty($params['datas']['postcode'])) && Validate::isPostCode($params['datas']['postcode'])) {
	                $address->postcode = $params['datas']['postcode'];
	            } else {
	                $address->postcode = 'A renseigner';
	            }
	
	            if (!(empty($params['datas']['city'])) && Validate::isCityName($params['datas']['city'])) {
	                $address->city = $params['datas']['city'];
	            } else {
	                $address->city = 'A renseigner';
	            }
	
	            if (empty($params['datas']['phone']) && empty($params['datas']['phone_mobile'])) {
	                $address->phone = '0000000000';
					$address->phone_mobile = '0000000000';
	            } else {
	                if(Validate::isPhoneNumber($params['datas']['phone'])){
	                    $address->phone = $params['datas']['phone'];
	                } else {
	                    $address->phone = '0000000000';
	                }
	                
	                if(Validate::isPhoneNumber($params['datas']['phone_mobile'])) {
	                    $address->phone_mobile = $params['datas']['phone_mobile'];
	                } else {
	                    $address->phone_mobile = '0000000000';
	                }
	            }
	
	            //$address->id_country = Configuration::get('PS_SHOP_COUNTRY_ID');
				
				$address->id_country = $params['datas']['id_country'];
	            $address->save();
            }
        }
        
        if (Validate::isLoadedObject($customer)) {
            $response = array(
                'success' => true,
                'action' => 'EditCustomer',
                'request' => $params,
                'response' => array(
                    'addressId' => isset($address)?$address->id:null,
                    'successBut' => array(
                        'error' => $error,
                        'addressExist' => $addressExist,
                    ),
                'id_customer'=>$customer->id,
                'customer'=>$customer
                ),
            );
        } else {
        	
            $response = array(
                'success' => false,
                'request' => $params,
                'errorMsg' => array(
                    'error' => $error,
                ) 
            );
        }

        $this->_returnJSON($response);
    }

    
    
    private function ajaxLoadCustomerOrders($params) {
        $orders = Order::getCustomerOrders($params['id_customer']);
        $nb_total_orders = Order::getCustomerNbOrders($params['id_customer']);
		$see_all_orders = true;
		
		$pointshop = new ACaissePointshop($params['id_pointshop']);
		if(Validate::isLoadedObject($pointshop))
		{
			$see_all_orders = $pointshop->see_all_orders;
			$nb_total_orders = Order::getCustomerNbOrders($params['id_customer'],$params['id_pointshop']);
		}
		
        $resultOrder = array();
        for ($i=$params['from_nb_order']; $i<$nb_total_orders && $i<$params['from_nb_order']+50; $i++) {
        	if($see_all_orders == true)
			{
				if($orders[$i]['id_pointshop'] == $params['id_pointshop'])	
					$resultOrder[] = $orders[$i];	
			}
			else 
			{
				$resultOrder[] = $orders[$i];
			}
            
        }
        
        $response=array(
            'success'=>true,
            'action'=>'LoadCustomerOrders',
            'request'=>$params,
            'response'=>array(
                'id_order_state_cancelled'=>Configuration::get('PS_OS_CANCELED'),
                'orders'=>$resultOrder,
                'nb_total_orders'=>$nb_total_orders
            )
        );
        
        $this->_returnJSON($response);
    }

}
