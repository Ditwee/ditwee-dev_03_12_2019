var undefinedValue; 
var ACAISSE = {
  id_pointshop:null,
  total_cost:0,
  total_cost_ht:0,
};
var colorLaurent = '#363636'; //363636
var ping_interval = 120000;

// on va charger les caches
var products_stock_cache = localStorage.getItem("products_stock_cache");
if (products_stock_cache==null) {
	products_stock_cache = {};
} else {
	products_stock_cache = JSON.parse(products_stock_cache);
}


window.onbeforeunload = function() { // lors de la fermeture de la page, on enregistre tout les caches en localStorage
	localStorage.setItem("products_stock_cache",JSON.stringify(products_stock_cache));
	
	for (var i=0; i<windows.length; i++) {
		windows[i].close();
	}
};

Date.toString = function(date, lang) { // transfère une date 'Y-m-d H:i' vers une date 'd/m/Y H:i'
	// date : string -> 'Y-m-d H:i'
	if (lang==undefined || lang=='fr') { // langue par défaut : francais
		var b = date.split(' ');
		var a = b[0].split('-');
		
		if ((typeof a[2]=='string' && a[2].length<2) || (typeof a[2]!='string' && a[2]<10)) {a[2]='0'+a[2];}
		
		//if (a[1]<10) {a[1]=a[1];} // ? ^^
		
		if (b.length>1) {
			var c = b[1].split(':');
			if ((typeof c[0]=='string' && c[0].length<2) || (typeof c[0]!='string' && c[0]<10)) {c[0]='0'+c[0];}
			return a[2]+'/'+a[1]+'/'+a[0]+' '+c[0]+':'+c[1];
		}
		
		return a[2]+'/'+a[1]+'/'+a[0];
	}
}

String.prototype.sansAccent = function() { // dégage les accens d'une chaine de caractère
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
     
    var str = this;
    for(var i = 0; i < accent.length; i++){
        str = str.replace(accent[i], noaccent[i]);
    }
    
    return str;
};

function sendNotif(newClass, newText) {
	if ($('#notif').width()!=0) {
		$('#notif table').fadeIn();
		$('#notif')
			.delay(500)
			.addClass('active')			
			.queue(function (next) { 
			   	$(this)
					.find('td')
					.removeClass()
					.addClass(newClass)
					.html(newText);
				next();
			});
	} else {
		$('#notif')
			.find('td')
			.removeClass()
			.addClass(newClass)
			.html(newText);
	}
	// var closeNotif = setTimeout(function(){$('#notif table').fadeOut();$('#notif').removeClass('active');console.log('titi');clearTimeout(closeNotif);},10000);
	// var clearNotif = setTimeout(function(){$('#notif td').html('');clearTimeout(clearNotif);},11000);
	
}
	

function formatPrice(price) { // format un prix (enlève les caractère non numérique après un nombre et l'écris avec deux décimales)
	price=parseFloat(price);
	
	if (isNaN(price)) {
		price=0;
	}

    return price.toFixed(2).replace(/./g, function (c, i, a) {
        return i && c !== "." && !((a.length - i) % 3) ? ' ' + c : c;
    });
}

// on va rechercher le dernier pointshop utilisé sur le poste
var id_pointshop=1;
if (localStorage.getItem('id_pointshop')!=null) { // on va récupérer le derier qu'on avait lancé sur le poste
	id_pointshop=parseInt(localStorage.getItem('id_pointshop'));
}
if (acaisse_pointshops.pointshops[id_pointshop]==undefined) { // si pouur x raison le pointshop n'existe pas, on prend le premier qu'on a sous la main
	for (var id in acaisse_pointshops.pointshops) {
		id_pointshop=id;
		break;
	}
}

var current_id_page = acaisse_pointshops.pointshops[id_pointshop].id_main_page;

ACAISSE = {
	previousPageId : [],
	id_force_group : 3, //@TODO
    mode_debogue: false,    	
    total_cost: 0.0,
    scanbarcodeMode: 'product', // or 'preOrder' or 'customer'
    current_id_page: current_id_page,
    id_pointshop: id_pointshop,
    id_service: 0, //@TODO à dynamiser lors du démarrage du service
    cart_tot: false, //real total from serveur (width reduction)
    customer: {
        id_customer: 1,
        firstname: 'Firstname',
        lastname: 'Lastname',
        year: 'D2',
    },
    cart: {
        total: 0,
        nb_article: 0,
        articles: [],
        /*
         {id_product,id_product_attribute,label,cost,quantity},
         {id_product,id_product_attribute,label,cost,quantity},
         etc..
         * */
    },
    payment_parts:[],
    ticket: {}, //va remplacer l'ancien ACaisse.cart afin de mapper extactement les objets prestshop de type ACaisseTicket
    ps_cart: null,
    ajaxCount: 0,
    ajaxQueue: {},
    productActionLocked: false, //certaines opération vont locké les actions d'ajout et de modification de quantité d'une commande.
    //paiement information
    choosenPaymentMode: false,
    allActionLocked: false,
    cartModificationLocked: false,
    lastCA : {},
    veilleAllwaysClosed: 0,
    lastCaches : {
    	'lastReloadCat': params.lastCaches.lastReloadCat,
        'lastReloadProfiles': params.lastCaches.lastReloadProfiles,
        'lastReloadPriceList': params.lastCaches.lastReloadPriceList,
        'lastReloadPointshops': params.lastCaches.lastReloadPointshops,
    }
};

var reloadCacheInterval = setInterval(
	function() {
		$.ajax({
	        data: {
	            'action': 'ReloadCaches',
	            'lastReloadCat': ACAISSE.lastCaches.lastReloadCat,
	            'lastReloadProfiles': ACAISSE.lastCaches.lastReloadProfiles,
	            'lastReloadPriceList': ACAISSE.lastCaches.lastReloadPriceList,
	            'lastReloadPointshops': ACAISSE.lastCaches.lastReloadPointshops,
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	var response = response.response;
	        	
				//là on injecte les balises script
				if (response.html.length>0) {
					console.warn('Un fichier à été rechargé');
					$('#ajaxBox').html(response.html);
					ACAISSE.lastCaches = response.lastCaches;
				}
	        } else {
	        	alert('Erreur k_00110 : impossible de rafraichir les caches');
	        }
	    }).fail(function () {
	        console.log('Erreur k00110 : Erreur k_00110 : impossible de rafraichir les caches');
	    });
	},
	parseInt(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.time_before_reload_caches)*60000 
);

function sendCustomerSearch(automatically_switch_to_first_finded_customer) {//RECHERCHE DE CLIENT LORSQU'ON SAISIS UN NOM
    var expressions = $('#customerSearchResultsFilter input').val().trim();
    
    if (expressions == '')
    {
        $('#customerSearchResults').html('').hide();
        $('#defaultCustomersProfils').show();
    }
    else
    {
        $.ajax({
            type: 'post',
            data: {
                'action': 'SearchCustomer',
                'q': expressions,
                'ajaxRequestID': ++ACAISSE.ajaxCount,
            },
            dataType: 'json',
        }).done(function (response) {
            //pendant le temps de retour de la réponse l'utilisateur a peut être revider l'expression de recherche
            if ($('#customerSearchResultsFilter input').val().trim() == '')
            {
                return sendCustomerSearch();
            }
            else
            {
                if (response.success == true)
                {
                    $('#defaultCustomersProfils').hide();

                    var html = '';
                    
                    if (response.response.customers.length == 0)
                    {
                        html = 'Aucun client correspond à votre recherche.';
                    }
                    else
                    {
                        html += '<ul>';
                        for (var i = 0; i < response.response.customers.length; i++)
                        {
                            var customer = response.response.customers[i]['customer'];
                            var groups = response.response.customers[i]['group'];
                            
                            var groupClass = '';
                            for (var group in groups){
                                groupClass += 'cutomerGroup_' + groups[group] + ' ';
                            }
                            html += '<li data-id="' + customer.id + '" data-groups="'+groups.join(',')+'" data-id_group="' + customer.id_default_group + '" \
                                        class="gender-' + customer.id_gender + ' ' + groupClass +'" style="background-color:'+_getGroupColorForCustomer(customer)+'">';
                            html += '<span class="name">' + customer.firstname + ' ' + customer.lastname + '' + (customer.promotion_name ? ' <i>' + customer.promotion_name + '</i>' : '') + '<span>';
                            html += '<span class="email">' + customer.email + '</span>';
                            html += '<span class="id">' + customer.id + '</span>';
                            html += '</li>';
                        }
                        html += '</ul>';
                    }
                    $('#customerSearchResults').html(html).show();
                    
                    if (automatically_switch_to_first_finded_customer!=undefined && $('#customerSearchResults ul li').length==1) {
                    	$('#customerSearchResults ul li').click();
                    }
                }
                else
                {
                    alert('Erreur 000128: ' + response.errorMsg);
                }
            }
        }).fail(function () {
            alert('Erreur 000129 : Une erreur c\'est produite durant la recherche de client. Si cette erreur persiste veuillez contacter l\'administrateur du site.');
        });
    }
}

function _setProgressPrepare(elem, theoreticalCash) { // change la couleur du fond d'un elem en fonction d'un nombre theoreticalCash
    var progressVeryDown = 5;
    var progressDown = 3;
    if (elem.val() >= (theoreticalCash + progressVeryDown) || elem.val() <= (theoreticalCash - progressVeryDown))
        elem.css('background', '#B26F6D');
    else if (elem.val() >= (theoreticalCash + progressDown) || elem.val() <= (theoreticalCash - progressDown))
        elem.css('background', '#FFD4B2');
    else if (elem.val() == theoreticalCash)
        elem.css('background', '#89B269');
    else
        elem.css('background', '#E4FFB0');
}

function addToCart(id_product, id_product_attribute, quantity, id_ticket_line) {  
    closeAllPanel();
    
    /*
    console.log([
    	'addToCart(id_product, id_product_attribute, quantity, id_ticket_line)',
    	id_product, id_product_attribute, quantity, id_ticket_line
    	
    ]);
    */
    
    if( typeof(quantity) == 'undefined' || quantity == '' ) {
        quantity = 1;
    }
    
    
    /************ NOUVEAUTE RETOUR PRODUIT ********************/
    if(PRESTATILL_CAISSE.return.mode_return_is_active === true)
    {
    	var id_order = null;
    	PRESTATILL_CAISSE.return.openReturnPopup(id_product, id_product_attribute, quantity, id_order);	
    	return;
    }
   
   
    /***********************************************************/
    
    
    
    if( typeof(id_ticket_line) == 'undefined' || id_ticket_line == '' ) {
        id_ticket_line = false;
    }
    
    if (ACAISSE.ticket.id == null) {
        alert('Impossible d\'ajouter un produit. Aucun ticket crée.');
        return;
    }

    if (ACAISSE.cartModificationLocked == true)
    {
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: {
                'action': 'CancelCartFromTicket',
                'id_ticket': ACAISSE.ticket.id,
                'id_product': id_product,
                'id_product_attribute': id_product_attribute,
                'quantity': quantity,
                'id_ticket_line': id_ticket_line,
            }
        }).done(function (response) {
            if (!response.success) {
                alert('Erreur 001558' + response.errorMsg);
                return;
            }
            ACAISSE.ticket = response.response.ticket;
            
            //remise en place du panneau non encaisse
            
            //@TODO A VALIDER AVEC NOUVEAU SYSTEM 
            /*
            $('#orderButtonsPanel').hide();
            $('#preOrderButtonsPanel').show();
            $('#orderButtonsPanel .paymentButton').removeClass('selected');
            $('#orderButtonsPanel .paymentButton').removeClass('selected');
            $('#preOrderButtonsPanel #registerPayment').addClass('disabled');           
            */
           
           
           
            ACAISSE.choosenPaymentMode = false;
            ACAISSE.cartModificationLocked = false;
            ACAISSE.cart_tot = false;
            //puis rappel de l'instruction initiale
            addToCart(response.request.id_product, response.request.id_product_attribute, response.request.quantity, response.request.id_ticket_line);
            
        }).fail(function () {
            alert('Error 001559 Remove one qu after cancel real cart failed.');
        });

        return; //on stop ici, la commande sera retenté qu'après
        // annulation du panier et annulation du passage à l'encaissement	
    }
    //sinon on peut réaliser l'opération directement.


    //var c = acaisse_products_prices.catalog;
    var p = _loadProductFromId(id_product);
    
    if (id_product_attribute != undefined && id_product_attribute > 0) {
		//console.log('quantite',quantity);
		if (p.unity=="grammes" && p.attributes[id_product_attribute].attribute_name=='1g') {
			//quantity*=1000; // si on commente ctte ligne on a tout en gramme
		}
	}
	
	if(p.is_generic_product == true && id_ticket_line == false)
	{
		//console.log('On force la creation d\'une nouvelle ligne car produit générique');
		id_product_attribute = 0;
		id_ticket_line = -1; //on va forcer la création d'une nouvelle ligne dans le ticket
	}
	
    
    var a = null;
    var product_name = p.product_name;
    var attribute_name = '';
    var img = p.image;
    id_product_attribute=id_product_attribute==''?0:id_product_attribute;
    if (id_product_attribute != 0)
    {
        a = p.attributes[id_product_attribute];
        if(a != undefined)
        {
	        attribute_name = a.attribute_name;
	        img = a.image != '' ? a.image : img;
        }
    }
	
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='0') {
		img = '';
	} else if (img=='' || img==undefined) {
		img='../modules/acaisse/img/noPhoto.jpg';
	}
	
    //construction d'une icon de signalisation de l\'ajout
    $icon = $('<div id="ajax-icon-' + (++ACAISSE.ajaxCount) + '" style="background-image:url(' + img + '); height:50px;" class="addToBAsketIndicator">'+product_name+' '+(id_product_attribute != 0?attribute_name:'')+'</div>');
    $('#productList .switcher').append($icon);

    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {
            'action': 'AddProductToCart',
            'id_ticket': ACAISSE.ticket.id,
            'id_ticket_line': id_ticket_line,
            'is_generic_product': p.is_generic_product,            
            'id_product': id_product,
            'id_attribute': id_product_attribute,
            'ajaxRequestID': ACAISSE.ajaxCount,
            'listToRefresh': ACAISSE.pageProductBuffer,
            'quantity' : quantity,
        }
    }).done(function (response) {
        if (!response.success) {
            //alert('Erreur 000552'+response.errorMsg);
            $('#ajax-icon-' + response.request.ajaxRequestID).append('<i class="fa fa-exclamation-circle" style="color:red; font-size:200%;"><b>' + response.errorMsg + '</b></i>').on('click tap', function (e) {
                $(this).fadeOut("slow", function () {
                    $(this).remove();
                });
                e.preventDefault();
                e.stopPropagation();
            });
            return;
        }
        $('#ajax-icon-' + response.request.ajaxRequestID).append('<i class="fa fa-check" style="color:green; font-size:200%;"></i>').delay(3000).hide('slow', function () {
            $(this).remove();
        });
        request = response.request;
        response = response.response;
        ACAISSE.ticket = response.ticket;
        console.log("add");
        var qu = _refreshTicket(id_product);
        _doStockLevelUpdateOnPage(response.stocks);
        
    	var line = response.ticket.lines[response.ticket.lines.length-1];
    	    	
        //si c'était un produit générique on ouvre ca popup de personalisation
        if(request.is_generic_product && parseInt(request.id_ticket_line) == -1)
        {        	
        	var id_ticket_line = line.id;
        	var id_product = line.id_product;
        	var id_product_attribute = line.id_attribute;
			var cartLineInfos = ".cartLine_"+id_product+"-"+id_product_attribute+"-"+id_ticket_line;
			$(cartLineInfos+' .discountCartLine').click();
			
			//declencher le chargement du cache afin d'avoir le produit avec sa nouvelle declinaison
			_loadProductFromId(id_product,null,true);
        }
        
        
        //var module_obj = {action:'typed_object', line:line,product:_loadProductFromId(line.id_product),id_attribute:null/*(a!=null?line.id_product_attribute:null)*/,qte:qu};
		
        _recalcAndRefreshTotals();
		var module_obj = {
			action:'switch_ticket',
			//product_list:product_list,
			ticket:new Ticket(ACAISSE.ticket),
			raw_ticket:ACAISSE.ticket
		};
		
		modules_action(module_obj);
		
		// TEST LAURENT 
		//_validateCurrentTicket();
        
        //@TODO appeler le bon hook de changement de total...
        
    }).fail(function () {
        alert('Error AddProductToCart failed.');
    });

}

function _drawCartLine(ticket_line)
{	
    var imgLink;
    var p = ticket_line.p;
    var a = ticket_line.a;
    var line = ticket_line.ticket_line;
    var id_ticket_line = line.id;
    var id_product_attribute = (line.id_attribute != undefined ? line.id_attribute : 0);

    //console.log(['_drawCartLine',ticket_line, line, id_product_attribute]);
    //console.log(p);
    //console.log(a);
    
    if (a != undefined && a.image != undefined && a.image!='') {
    	imgLink = a.image;
    } else if (p.image != undefined && p.image!='') {
    	imgLink = p.image;
    } else {
    	imgLink = '../modules/acaisse/img/noPhoto.jpg';
    }
    
    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='0') {
    	imgLink = '';
    }
    
    
     var html = '<tr class="cartLine_' + p.id_product + '-' + id_product_attribute + '-' + id_ticket_line + ' line" '+
    	'data-idTicketLine="' + id_ticket_line + '"'+
        'data-id_product="' + p.id_product + '"'+
        'data-id_product_attribute="' + id_product_attribute + '"'+
        'data-idobject="' + p.id_product + '"'+
        'data-idobject2="' + id_product_attribute + '">';
        
    html += (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0'?'<td class="img"><img src="' + imgLink + '" width="50" height="50"/></td>':'');
    html += '<td class="label">' + ticket_line.label; /*' ' +(a != undefined ? a.attribute_name : '')) +*/
    html += '<br /><small>' + p.reference + '</small>';
    html += '</td>';
    
    /*
       NOUVEAU
       display_prix_final_ttc
	   display_prix_final_ht
	   display_prix_original_ttc
	   display_prix_original_ht
	   display_reduction
     */
    html += getCartLineCostLabelHTML(ticket_line);
    
    html += '<td class="discountCartLine"><i class="fa fa-pencil"></i></td>';
    html += '<td></td>';
    
    var unitId = params.units.from.indexOf(p.unity);
	if (unitId==-1) {
		//console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		unitId = params.units.from.indexOf(p.unity);
		//throw "Erreur d'unité : unité inconnue : ";
	}
    
	if (params.units.scale[unitId]==1 || (a != undefined && p.unity=="grammes" && a.attribute_name!='1g')) { // si on a pas une unité de mesure spéciale comme le 'g'
        html += '<td class="quantity"><span class="quantityMinus"><i class="fa fa-caret-left"></i></span><span class="nbr">1</span><span class="quantityPlus"><i class="fa fa-caret-right"></i></span></td>';
		html += '<td class="quantityRemove"><i class="fa fa-trash"></i></td>';
	} else {
    	html += '<td class="quantity"><span class="nbr">1</span></td>';
		html += '<td class="quantityRemove"><i class="fa fa-trash"></i></td>';
	}
	
	html += '<td class="totalLineCost"></td>';
	
	html += '</tr>';
    $('#ticketView .lines').append(html);
    
    var $line = $('#ticketView .lines tr').last();
    //on stock l'instance TicketLine dans data de la ligne (sa servira à la popup pour avoir accès à toutes les infos de la ligne)
    $line.data('ticket_line',ticket_line);
    
    var unitPrice = parseFloat($line.find('.cost .specialPrice').html());
    var qte = parseFloat($line.find('.quantity .nbr').html());
    
    var linePrice = unitPrice*qte;
    linePrice = linePrice.toFixed(2)+'€';
    
    
	$('td.totalLineCost',$line).replaceWith(getCartTotalLineCostLabelHTML(ticket_line));

    
    _recalcAndRefreshTotals();
    
}


function getCartTotalLineCostLabelHTML(ticket_line) {
	
	var html = '';
	//console.warn(['-----------',ticket_line]);
	html += '<td class="totalLineCost '+(ticket_line.has_reduction?' has_reduction':'')+'">';
	    html += '<span class="ht display_total_line_ht">'+ticket_line.display_line_price_ht+'</span>';
    	html += '<span class="ttc display_total_line_ttc">'+ticket_line.display_line_price_ttc+'</span>';
    html += '</td>';
	
	return html;
}

function getCartLineCostLabelHTML(ticket_line) {
	var html = '';
	var price_orig = false;
	var reduction_from_price_orig = false;
	
	if(ACAISSE.ticket.is_detaxed)
	{
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
			&& ticket_line.prix_final_ht < ticket_line.base_price_ht)
		{
			ticket_line.has_reduction = true;
			price_orig = true;
			reduction_from_price_orig = ticket_line.display_reduction_orig;
			reduction_from_price_orig_ht = ticket_line.display_reduction_orig_ht;
			//console.log(parseFloat(ticket_line.base_price_ttc).toFixed(2) + ' - ' + parseFloat(ticket_line.prix_final_ttc).toFixed(2) + ' - ' + ticket_line.display_reduction_orig + ' - ' + price_orig + ' - ' + reduction_from_price_orig);
		}
	}
	else
	{
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
			&& ticket_line.prix_final_ttc < ticket_line.base_price_ttc)
		{
			ticket_line.has_reduction = true;
			price_orig = true;
			reduction_from_price_orig = ticket_line.display_reduction_orig;
			reduction_from_price_orig_ht = ticket_line.display_reduction_orig_ht;
			//console.log(parseFloat(ticket_line.base_price_ttc).toFixed(2) + ' - ' + parseFloat(ticket_line.prix_final_ttc).toFixed(2) + ' - ' + ticket_line.display_reduction_orig + ' - ' + price_orig + ' - ' + reduction_from_price_orig);
		}
	}
	
	//price_orig==true?ticket_line.display_base_price_ht:
	
	html += '<td class="cost '+(ticket_line.has_reduction?' has_reduction':'')+'">';
	
	    if(ticket_line.has_reduction)
	    {
	    	if(price_orig == true)
	    	{
	    		html += '<span class="ht original_prix display_prix_original_ht">'+ticket_line.display_base_price_ht+'</span>';
	    		html += '<span class="ttc original_prix display_prix_original_ttc">'+ticket_line.display_base_price_ttc+'</span>';
	    		
	    		if(ACAISSE.price_display_method == 1) //HT
				{	
	    			html += '<br /><span class="reduction">'+reduction_from_price_orig_ht+'</span><br />';
	    		}
	    		else
	    		{
	    			html += '<br /><span class="reduction">'+reduction_from_price_orig+'</span><br />';
	    		}	
	    		
	    	}
	    	else
	    	{
	    		html += '<span class="ht original_prix display_prix_original_ht">'+ticket_line.display_prix_original_ht+'</span>';
	    		html += '<span class="ttc original_prix display_prix_original_ttc">'+ticket_line.display_prix_original_ttc+'</span>';
	    		
	    		if(ACAISSE.price_display_method == 1) //HT
				{	
	    			html += '<br /><span class="reduction">'+ticket_line.reduction_ht+'</span><br />';
	    		}
	    		else
	    		{
	    			html += '<br /><span class="reduction">'+ticket_line.display_reduction+'</span><br />';
	    		}	
	    	}
    	}
    	html += '<span class="ht final_prix display_prix_final_ht">'+ticket_line.display_prix_final_ht+'</span>';
    	html += '<span class="ttc final_prix display_prix_final_ttc">'+ticket_line.display_prix_final_ttc+'</span>';
    html += '</td>';
	
	return html;
}

function _cartUpdateLineQuantity(id_product, id_product_attribute, newQuantity, id_ticket_line, ticket_line) {
	
	if(typeof id_ticket_line == 'undefined' || id_ticket_line == '')
	{
		id_ticket_line = false;
	}
	
	var p = _loadProductFromId(id_product);
	if (id_product_attribute && p.has_attributes) {
		var a = p.attributes[id_product_attribute];
	}
	
	var unitId = params.units.from.indexOf(_loadProductFromId(id_product).unity);
	
	if (a!=null && p.unity=="grammes" && a.attribute_name!='1g') {
		unitId = params.units.from.indexOf('pièce');
	}
	
	if (unitId==-1) {
		console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		unitId = params.units.from.indexOf(_loadProductFromId(id_product).unity);
		//throw "Erreur d'unité : unité inconnue : ";
	}
	
	newQuantity /= params.units.scale[unitId];
	newQuantity += (' '+params.units.to[unitId].substring(1));
	
	var html='';
	
	if (params.units.scale[unitId]==1) { // si on a une unité de mesure spéciale comme le 'g'
		html += '<span class="quantityMinus"><i class="fa fa-caret-left"></i></span><span class="nbr">'+newQuantity+'</span><span class="quantityPlus"><i class="fa fa-caret-right"></i></span>';
	} else {
		html += '<span class="nbr">'+newQuantity+'</span>';
	}
	
	$('.cartLine_' + id_product + '-' + id_product_attribute + '-' + id_ticket_line + ' .quantity').html(html);
	
	$('.cartLine_' + id_product + '-' + id_product_attribute + '-' + id_ticket_line + ' .totalLineCost').html(
		formatPrice(
			parseFloat(
				$('.cartLine_' + id_product + '-' + id_product_attribute + ' .specialPrice').html()
			)*newQuantity
		)+'€'
	);
	
	var line_selector = '.mode-buy .cartLine_' + id_product + '-' + id_product_attribute + '-' + id_ticket_line;
	var $line = $(line_selector);
    
    var unitPrice = parseFloat($line.find('.cost .specialPrice').html());
    var qte = parseFloat($line.find('.quantity .nbr').html());
    
    var linePrice = unitPrice*qte;
    linePrice = linePrice.toFixed(2)+'€';
    
   // $line.find('.totalLineCost').html(linePrice);
    //var ticket_line = $line.data('ticket_line');
        
	$('td.totalLineCost',$line).replaceWith(getCartTotalLineCostLabelHTML(ticket_line));    
}

function _reloadPage() {
	loadPage(ACAISSE.current_id_page);
}

var timeoutLoadPage;
var timeoutPing;

function loadPage(id_page, id_pointshop) {
	clearTimeout(timeoutLoadPage);
	//console.log("j suis la");
	if(ACAISSE.mode_debogue === true)
	{
		console.log('loadPage(id_page='+id_page+', id_pointshop='+id_pointshop+')');
	}
	
	var productBuffer = []; //liste des produits sur la page, nécessitant la maj des stocks


	if (id_pointshop == null || typeof id_pointshop == 'undefined')
	{
		id_pointshop = ACAISSE['id_pointshop'];
	}
	var ps = acaisse_pointshops.pointshops[id_pointshop];
	
	params.print.nbCopie=parseInt(ps.datas.ticket_nbr_copies);
	params.print.nbJourSauvegardeLocal=parseInt(ps.datas.ticket_nbr_jour_sauvegarde_local);
	params.customerScreen.lcd_ip=ps.datas.screen_lcd_ip;
	params.customerScreen.mode=ps.datas.screen_mode;
	params.declinaisonPopup.closeAfterSelect=ps.datas.declinaison_close_popup_after_select=='1';
	params.products.STOCK_WARNING_LIMIT=parseInt(ps.datas.product_STOCK_WARNING_LIMIT);
	params.products.use_image=ps.datas.use_image=='1';
	
	//DEPRECATE open_module(); // ouvre tout les modules complementaire (afficher client, ...)
	
	if (ps.datas.navigation_by_category=='0') {
		$('#openRootCategorie').css('display','none');
	} else {
		$('#openRootCategorie').css('display','block');
	}
	
	if (typeof ps.pages[id_page] == 'undefined') { // on a pas de page de caisse
		
		if (ps.datas.navigation_by_category=='0' || acaisse_categories.categories[acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_root_category]==undefined) { // on a pas non plus de categorie
			
			$('#productList .content').html('');
			
			sendNotif('redFont','<i class="fa fa-exclamation-circle"></i> Caisse hors service.');
			
			timeoutLoadPage = setTimeout(function() {
				alert('Veuillez configurer la caisse en lui ajoutant soit des pages de caisse, soit en activant la navigation par categorie (contactez un responsable).');
			},1000);
			
		} else {
			generateCategoriePageHTML(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_root_category);
	  
			// On récupère les nbRow et nbCol dans les params
			ACAISSE.nbRow = ps.datas.max_row;
			ACAISSE.nbCol = ps.datas.max_col;
	
			// On fait la transition vers les pages par catgegories
			$('#footer .navActive').removeClass('navActive');
	        $('#openRootCategorie').addClass('navActive');
			
			// On appel une fonction toutes les X minutes pour ne pas être déconnecté de Prestashop
			if(timeoutPing != null) { clearInterval(timeoutPing); }
			timeoutPing = setInterval(function(){ _refreshProductPageStock(productBuffer);},ping_interval);		
		}
		
		return false;
	}
	var cells = ps.pages[id_page].cells;
	var html = '<table id="acaissepagecms">';
	
	ACAISSE.nbRow = cells.length;
	ACAISSE.nbCol = cells[0].length;
	
	for (var row = 0; row < cells.length; row++)
	{
		html += '<tr id="row_' + row + '">';
		var r = cells[row];
		for (var col = 0; col < r.length; col++)
		{
			var cell = r[col]; // cell = {id_object:5,id_object2:6}
			
			/*if (_loadProductFromId(cell.id_object)==undefined) {
				cell.type = 101;
			}*/			
			
			switch (parseInt(cell.type))
			{
				case 101:
					html += '<td class="cellType0 cellStyle1 rowspan1 colspan1" data-idobject="" data-idobject2=""></td>';
					break;
				case 100:
					html += '<!--spanned td-->';
					break;
				case 2:
				case 3:
					html += '<td id="cell_' + cell.row + '_' + cell.col + '"' + _injectCellAttributes(cell) + '>' + _injectCellProductContent(cell, ps) + '</td>';
					productBuffer.push({
						id_product: cell.id_object,
						id_product_attribute: cell.id_object2,
						row: cell.row,
						col: cell.col
					});
					break;
				case 4:
				  html += '<td' + _injectCellAttributes(cell) + '>' + _injectCellCategoryContent(cell, ps) + '</td>';
				  break;
				case 1:
					html += '<td' + _injectCellAttributes(cell) + '>' + _injectCellPageContent(cell, ps) + '</td>';
					break;
				case 0:
				default:
					html += '<td' + _injectCellAttributes(cell) + '></td>';
					break;
			}
		}
		html += '</tr>';

	}

	html += '</table>';

	$('#productList .content').html(html);

	_refreshProductPageStock(productBuffer);
	ACAISSE.pageProductBuffer = productBuffer; //on le stocks pour pouvoir redemander la
	//ACAISSE.pageProductBuffer.push(3);
	// mise à jour des stocks de la page sur
	// certaine action (ajout de produit, etc...)
	ACAISSE.current_id_page = id_page;
	resetTileHeight();
	
	// On appel une fonction toutes les 4 minutes pour ne pas être déconnecté de Prestashop
  if(timeoutPing != null) { clearInterval(timeoutPing); }
  timeoutPing = setInterval(function(){ _refreshProductPageStock(productBuffer);},ping_interval);
	
	return true;
}

function generateCategoriePageHTML(id_object) {
  
	if (acaisse_categories.categories[id_object]==undefined) {
		$('#openRootPageCaisse').click();
		//sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Veuillez configurer la categorie par défaut pour utiliser cette navigation.');
		console.log('<i class="fa fa-exclamation-circle"></i>Veuillez configurer la categorie par défaut pour utiliser cette navigation.');
		return;
	} else if (acaisse_categories.categories[id_object].active==false) {
		sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Cette catégorie a été désactivé.');
		return;
	} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.navigation_by_category=='0') {
		
		//on peut bloquer la navigation par categorie quoi qu'il arrive (si on a des lien dans les pages de caisse) en fonction du paramettre du pointshop en décommentant les ligne suivante
		
		/*$('#openRootPageCaisse').click();
		sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Veuillez configurer la poissibilité d\'utiliser de cette navigation.');
		return;*/
	}
  
	$('#footer .button').removeClass('navActive');
	$('#openRootCategorie').addClass('navActive');
  
	ACAISSE.pageProductBuffer = [];
	if(ACAISSE.mode_debogue === true)
		console.log('generateCategoriePageHTML('+id_object+')');
		
	var html = '<table id="acaissepagecms">';
	var categories = acaisse_categories.categories[id_object];

	var nbrLines = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.max_row;
	var nbrColumns = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.max_col;
	
	var line = 0;
	var column = 0;
	if(ACAISSE.mode_debogue === true)
		console.log(['merge categorie pageCaisse',ACAISSE.previousPageId]);
		
	if (ACAISSE.previousPageId.length>1) {
		column = 1;
		html += '<tr id="row_'+line+'">';
		html += '<td class="cellTypeReturn cellStyle9 rowspan1 colspan1" data-idobject="8" data-idobject2="" id="cell_0_0">Retour</td>';
	}
	for (var i=0; i<categories.sub_categories.length; i++) {
		if (acaisse_categories.categories[categories.sub_categories[i]].active==false) {
			continue;
		}
		
		if (column==0) {
			html+='<tr id="row_'+line+'">';
		}
		
		var categorie = acaisse_categories.categories[categories.sub_categories[i]];
		html += '<td id="cell_'+line+'_'+column+'" class="cellStyleDefault cellType4 cellStyle2 rowspan1 colspan1" data-idobject="'+categories.sub_categories[i]+'">';
			html += '<div class="cover with_attributes '+(acaisse_categories.categories[categories.sub_categories[i]]?'isCat':'')+' '+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2'?'fullImg':'')+''+((categorie.thumb!==false && acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0')?' withCategorieImage':'')+'">';
				html += '<div class="acaisse_productcell_content" style="';
				if (categorie.thumb!==false) {
					html += 'background-image: url('+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0'?categorie.thumb:'')+');'; // .thumb renvoie vers le lien d'une image de la catégorie
				}
				html += 'background-position:0px 0px;">'
				
					html += '<div class="product_stock"-></div>';
					html += '<div class="product_name_container">';
						html += '<div class="product_name_container_box">';
							html += '<div class="product_name"><i class="fa fa-folder-open-o" unselectable="on" style="user-select: none;"></i><span>';
								html += acaisse_categories.categories[categories.sub_categories[i]].name;
							html += '</span></div>';
						html += '</div>';
					html += '</div>';
				
				html += '</div>';
			html += '</div>';
		html += '</td>';
		
		column++;
		
		if (column==nbrColumns) {
			html+='</tr>';
			line++;
			column=0;
		}
	}
	
	// on doit maintenant parcourir les produits qu'il reste dans la categorie pour les afficher
	for (var i=0; i<categories.products.length; i++) {
	  
    
    var p = _loadProductFromId(categories.products[i]);
	  
		if (column==0) {
			html+='<tr id="row_'+line+'">';
		}
		/*
		if (_loadProductFromId(categories.products[i])==undefined || !_loadProductFromId(categories.products[i]).disponibility_by_shop[ACAISSE.id_pointshop]) {
			p=undefinedValue;
		}
		*/
		
		//console.log([typeof(p),p]);
		
	    if(typeof(p) != 'undefined' && p != null) {
	      
        if (p.active==false) {
          continue;
        }
	      
	  		var img;
	  		
	  		if (p.has_attributes && p.attributes[p.id_default_attribute]) {
	  			img = p.attributes[p.id_default_attribute].image;
	  		} else {
	  			img = p.image;
	  		}
	  		
	  		if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='0') {
	  			img='';
	  		}
	  
	  		html += '<td id="cell_'+line+'_'+column+'" class="cellStyleDefault cellType2 cellStyle'+(p.has_attributes?'':'1')+'3 rowspan1 colspan1" data-idobject="'+categories.products[i]+'" data-idobject2>';
	  			html += _injectCellProductContent({id_object:categories.products[i],id_object2:0});
	  		html += '</td>';
	  		
	  		ACAISSE.pageProductBuffer.push({
	  			id_product: categories.products[i],
	  			id_product_attribute: '',
	  			row: line,
	  			col: column
	  		});
	  		
	  		column++;
		}
		if (column==nbrColumns) {
			html+='</tr>';
			line++;
			column=0;
		}
	}
	
	var enter = false;
	while (column!=nbrColumns && column!=0) {
		html += '<td class="cellType0 cellStyle1 rowspan1 colspan1" data-idobject="" data-idobject2="" id="cell_'+line+'_'+column+'"></td>';
		column++;
		enter=true;
	}
	
	if (enter) {line++;}
	
	while (line <= nbrLines-1) {
		html += '<tr id="row_'+line+'">';
		for (var column=0; column<nbrColumns; column++) {
			html += '<td class="cellType0 cellStyle1 rowspan1 colspan1" data-idobject="" data-idobject2="" id="cell_'+line+'_'+column+'"></td>';
		}
		html += '</tr>';
		line++;
	}
	
	html += '</table>';
	
	
	
  $('#productList .content').html(html);
	//$('#acaissepagecms tbody').html(html);
	
	resetTileHeight();
	
	if (ACAISSE.pageProductBuffer.length>0) {
		_refreshProductPageStock(ACAISSE.pageProductBuffer);
	}
	
}

/**
 * recharge les stocks d'une liste de produit
 * @params object listToRefresh sous la forme : listToRefresh.push({
 *        id_product: cell.id_object,
 *        id_product_attribute: cell.id_object2,
 *        row: cell.row,
 *        col: cell.col
 *  });
 */
function _refreshProductPageStock(listToRefresh) { // 
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'RefreshProductStockFromPage',
			'listToRefresh': listToRefresh,
			'id_pointshop' : ACAISSE.id_pointshop,
		},
	}).done(function (response) {
		if (response.success)
		{
			if(ACAISSE.mode_debogue === true)
			{
				console.log(['_refreshProductPageStock() ajax response',response]);
			}
			_doStockLevelUpdateOnPage(response.response.stocks);
		}
		else
		{
			alert('Erreur 000947 : ' + response.errorMsg);
		}
	}).fail(function () {
		console.log('Erreur 000948c');
	});
}

function _doStockLevelUpdateOnPage(stocks) { // change le html de tout les stocks pour l'affichage
	if(ACAISSE.mode_debogue === true)
		{console.log(['stock level update',stocks]);}
	if(stocks)
	{
		for(var i=0 ; i<stocks.length ; i++)
		{			
			if(ACAISSE.mode_debogue === true)
			{console.log(['stock level update',stocks[i]]);}
			var cell=stocks[i][0];
			var real_stock = real_stock_aff = stocks[i]['physical_qu'];
			var preorder_stock = preorder_stock_aff = stocks[i][2];
			var estimateStock = estimateStock_aff = parseInt(real_stock)-parseInt(preorder_stock);
			
			try {
				var unitId = params.units.from.indexOf(_loadProductFromId(cell.id_product).unity);
				
				if (unitId==-1) {
					console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
					params.units.from.push(p.unity);
					params.units.to.push('');
					params.units.scale.push(1);
					unitId = params.units.from.indexOf(_loadProductFromId(cell.id_product).unity);
					//throw "Erreur d'unité : unité inconnue : ";
				}
				
				real_stock_aff = (real_stock/params.units.scale[unitId])+' '+params.units.to[unitId].substring(1);
				preorder_stock_aff = (preorder_stock/params.units.scale[unitId])+' '+params.units.to[unitId].substring(1);
				estimateStock_aff = (estimateStock_aff/params.units.scale[unitId])+' '+params.units.to[unitId].substring(1);

				var p = _loadProductFromId(cell.id_product);
				if (cell.id_product_attribute != 0) {
					p = p.attributes[cell.id_product_attribute];
				}
				
				if  (products_stock_cache[cell.id_product] == undefined) { // si on a jamais eu ce produit a ranger dans le cache
					products_stock_cache[cell.id_product] = {};
				}
				
				if (cell.id_product_attribute != "" && cell.id_product_attribute != 0) { // si en + il a un attribut
					products_stock_cache[cell.id_product][cell.id_product_attribute] = {};
					products_stock_cache[cell.id_product][cell.id_product_attribute].real_stock_aff = real_stock_aff;
					products_stock_cache[cell.id_product][cell.id_product_attribute].preorder_stock_aff = preorder_stock_aff;
					products_stock_cache[cell.id_product][cell.id_product_attribute].estimateStock_aff = estimateStock_aff;
				} else {
					products_stock_cache[cell.id_product].real_stock_aff = real_stock_aff;
					products_stock_cache[cell.id_product].preorder_stock_aff = preorder_stock_aff;
					products_stock_cache[cell.id_product].estimateStock_aff = estimateStock_aff;
				}
			} catch(e) {
				// @TODO : le dernier items de stocks est un élément bizzard... à creuser
			}

			//console.log('-----');
			//console.log('Tentative de mise à jour de '+cell.col+';'+cell.row);
			//console.log('Devrait contenir le produit '+cell.id_product+'/'+cell.id_product_attribute);
			var $cell=$('#cell_'+cell.row+'_'+cell.col);
			//console.log('Contient actuellement le produit '+$cell.data('idobject')+'/'+$cell.data('idobject2'));
			
			if(ACAISSE.mode_debogue === true)
			{
				console.log(cell.id_product+' == '+$cell.data('idobject')+' && '+cell.id_product_attribute+' == '+$cell.data('idobject2'));
				console.log($cell);
				console.log(cell.id_product == $cell.data('idobject'));
				console.log(cell.id_product_attribute == ($cell.data('idobject2')==''?0:$cell.data('idobject2')));
			}
			
			if(cell.id_product == $cell.data('idobject') && cell.id_product_attribute == ($cell.data('idobject2')==''?0:$cell.data('idobject2')))
			{
			  //console.log('maj stock OK');
			  $('.stock_real',$cell)
				  .html(real_stock=='0'?'rupture':real_stock_aff)
				  .removeClass('error')
				  .addClass(real_stock=='0'?'error':'');
			  $('.stock_inqueue',$cell).html('-'+preorder_stock_aff);
			  
			  $('.stock_available',$cell).html(estimateStock_aff)
				  .removeClass('error')
				  .removeClass('warning')
				  .removeClass('good')
				  .addClass(estimateStock>params.products.STOCK_WARNING_LIMIT?'good':estimateStock<=0?'error':'warning');
			}
			else
			{
			  //console.log('maj stock NOT OK');
	  			
	  		  // si on entre dans le else c'est qu'on a du avoir des envoie de demandes de stock qui se sont croisées dues à l'asynchronie d'AJAX, on a donc recu les stock d'un produit qui n'est plus affiché
	  		  // on ne doit donc par conséquent pas virer le text de stock du nouveau produit... 
			  
			  //@TODO supprimer le else si la mise en commentaire des lignes suivantes ne pose pas de problème
			  
			  /*$('.stock_real',$cell).html('error')
				  .addClass('error')
				  .removeClass('warning')
				  .removeClass('good');
			  $('.stock_inqueue',$cell).html('');
			  $('.stock_available',$cell).html('');*/
			}
		}
	}
}



function _injectCellCategoryContent(cell, ps) { // contenu du td des categories (system des dossiers)
  if(ACAISSE.mode_debogue === true)
  {
	console.log(['[***] _injectCellCategoryContent(cell,ps)',cell,ps]);
  }
	var html = '';

	var c = acaisse_categories.categories;
	var cat = c[cell.id_object];
	if(ACAISSE.mode_debogue === true) {console.log(['cat',cat]);}
	if(typeof(cat) != 'undefined')
	{       
	  	html += '<div class="cover'+((cat.thumb != false && acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0')?' withCategorieImage':'')+'"><div class="acaisse_categorycell_content"';
	  
	  	if (cat.thumb != false && acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0')
	  	{
	  		html += ' style="background-image:url(' + cat.thumb + ')"';
	  	}
	  	html += '>';
	  	html += '<i class="fa fa-folder-open-o" unselectable="on" style="user-select: none;"></i><span>' + cat.name + '</span>'
	  	html += '</div></div>';
	}
	
	return html;
}

function _injectCellProductContent(cell, ps) { // cell = {id_object:5,id_object2:6}
  /*
	if(ACAISSE.mode_debogue === true)
	{
		console.log(['_injectCellProductContent(cell,ps)',cell,ps]);
	}
	*/
	
	var html = '';

	//var c = acaisse_products_prices.catalog;
	var p = _loadProductFromId(cell.id_object);
	var name, price, img, reference;
	var pa = null;
	if (p)
	{
		name = p.product_name;
		price = getProductPrice(cell.id_object, cell.id_object2);
		base_price = getProductBasePrice(cell.id_object, cell.id_object2);
		img = p.image;
		reference = p.reference;

		//si pas un produit générique
		if(p.is_generic_product == 1)
		{
			//c'est un produit géérique...
			//prévoir ici un autre design et ne pas le traiter comme si le produit
			//avait des déclinaisons réelles
		}
		else
		{
			if(p.has_attributes === true)
			{
				if (cell.id_object2 != 0) //c'est une declinaison
				{
					pa = p.attributes[cell.id_object2];
					if (pa)
					{
						name += ' ' + pa.attribute_name;
						img = pa.image != '' ? pa.image : img;
						//reference += '' + pa.reference;
					}
					else
					{
						name = '<strong class="error404">Déclinaison inexistante</strong>';
						img = '';
						price = 0+'€';
					}
				}
				else //ce n'est pas ne case de décliaison MAIS le produit en possède (cf popup Kevin)
				{
					//que l'on va chercher dans la liste attributes
					price = false;
					if(p.attributes[p.id_default_attribute])
					{
						img = p.attributes[p.id_default_attribute].image;
					}					
				}
			}
		}
	}
	else
	{
		name = '<strong class="error404">Produit inexistant</strong>';
		img = '';
		price = 0+'€';
	}
	html += '<div class="cover '+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2'?'fullImg':'')+' '+(price===false?' with_attributes':'')+'"><div class="acaisse_productcell_content"';
	
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0') {
		if (img == '' || img == undefined) {
			img = '../modules/acaisse/img/noPhoto.jpg';
		}
		if(price === false) //produit avec declinaisons
		{
			html += ' style="background-image:url(' + img + '); background-position:center center;"'; 
		}
		else //produit simple ou declinaison simple
		{
			html += ' style="background-image:url(' + img + ')"';
		}
	}
	
	html += '>';
	html += '<div class="product_stock">';
	
	if(ACAISSE.mode_debogue === true)
	{
		console.log({'Cell data:':{
			'price':price,
			'img':img,
			'p':p,
			
		}});
	}
	
	if(price === false) //c'est une case d'un produit mais qui a des declinaisons
	{
		html += '</div>';
		html += '<div class="product_name_container"><div class="product_name_container_box"><div class="product_name"><i class="fa fa-list-ol"></i><span>' + name + '</span></div></div></div>';
		html += '</div></div>';
	}
	else
	{
		var real_stock_aff, preorder_stock_aff, estimateStock_aff;
		var cache_stock = products_stock_cache[cell.id_object];
		if (cache_stock == undefined) {
			real_stock_aff = 'XX';
			preorder_stock_aff = 'XX';
			estimateStock_aff = 'XX';
		} else if (cell.id_object2 != 0) {
			cache_stock = cache_stock[cell.id_object2];
			if (cache_stock == undefined) {
				real_stock_aff = 'XX';
				preorder_stock_aff = 'XX';
				estimateStock_aff = 'XX';
			} else {
				real_stock_aff = cache_stock.real_stock_aff;
				preorder_stock_aff = cache_stock.preorder_stock_aff;
				estimateStock_aff = '<span class="no_internet">'+cache_stock.estimateStock_aff+'</span>';
			}
		} else {
			real_stock_aff = cache_stock.real_stock_aff;
			preorder_stock_aff = cache_stock.preorder_stock_aff;
			estimateStock_aff = cache_stock.estimateStock_aff;
		}
		//console.log(p.base_price*(1+(p.tax_rate/100)) + ' <> ' + p.prices[ACAISSE.id_force_group]);
		
		
		var display_base_price = 'none';
		
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
		&& p.base_price*(1+(p.tax_rate/100)) > p.prices[ACAISSE.id_force_group])
		{
			//$('#cell_'+cell.row+'_'+cell.col+' .product_base_price').show();
			display_base_price = 'block';
		}
		
		html += '<div class="product_base_price" style="display:'+display_base_price+'">' + base_price + '</div>';
		
		html += '<div class="product_price">' + price + '</div>';
		
		html += '<div class="availability">';
			html += '<span class="stock_available">'+estimateStock_aff+'</span>';
			html += '<span class="stock_real">'+real_stock_aff+'</span>';
			html += '<span class="stock_inqueue">-'+preorder_stock_aff+'</span>';
		html += '</div>';
		
		
		html += '</div>';
		html += '<div class="product_name_container"><div class="product_name_container_box"><div class="product_name"><span>' + name + (reference == ''?'':'<br /><em>'+reference+'</em>')+'</span></div></div></div>';
		html += '</div></div>';
	}

	return html;
}

function _injectCellProductContentInSearchResults(cell, ps) { // cell = {id_object:5,id_object2:6}
	
	var html = '<table><tr>';

	//var c = acaisse_products_prices.catalog;
	var p = _loadProductFromId(cell.id_object);
	var name, price, img, reference;
	var pa = null;
	if (p)
	{
		name = p.product_name;
		price = getProductPrice(cell.id_object, cell.id_object2);
		base_price = getProductBasePrice(cell.id_object, cell.id_object2);
		img = p.image;
		reference = p.reference;

		//si pas un produit générique
		if(p.is_generic_product == 1)
		{
			//c'est un produit géérique...
			//prévoir ici un autre design et ne pas le traiter comme si le produit
			//avait des déclinaisons réelles
		}
		else
		{
			if(p.has_attributes === true)
			{
				if (cell.id_object2 != 0) //c'est une declinaison
				{
					pa = p.attributes[cell.id_object2];
					if (pa)
					{
						name += ' ' + pa.attribute_name;
						img = pa.image != '' ? pa.image : img;
						//reference += '' + pa.reference;
					}
					else
					{
						name = '<strong class="error404">Déclinaison inexistante</strong>';
						img = '';
						price = 0+'€';
					}
				}
				else //ce n'est pas ne case de décliaison MAIS le produit en possède (cf popup Kevin)
				{
					//que l'on va chercher dans la liste attributes
					price = false;
					if(p.attributes[p.id_default_attribute])
					{
						img = p.attributes[p.id_default_attribute].image;
					}					
				}
			}
		}
	}
	else
	{
		name = '<strong class="error404">Produit inexistant</strong>';
		img = '';
		price = 0+'€';
	}
	html += '<td class="cover '+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2'?'fullImg':'')+' '+(price===false?' with_attributes':'')+'"><div class="acaisse_productcell_content"';
	
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0') {
		if (img == '' || img == undefined) {
			img = '../modules/acaisse/img/noPhoto.jpg';
		}
		if(price === false) //produit avec declinaisons
		{
			html += ' style="background-image:url(' + img + ');"'; 
		}
		else //produit simple ou declinaison simple
		{
			html += ' style="background-image:url(' + img + ')"';
		}
	}
	
	html += '></div></td>';
	html += '<td class="reference">'+ reference +'</td>';

	
	if(price === false) //c'est une case d'un produit mais qui a des declinaisons
	{
		html += '<td class="product_name_container"><div class="product_name_container_box"><div class="product_name"><span>' + name + '</span></div></div></td>';
		html += '<td class="availability with_attr" colspan="2"><div class="btn"><i class="fa fa-list-ol"></i> voir les détails</div>';
		//html += '<table><tr>';
			// html += '<td colspan="3"><div class="btn"><i class="fa fa-list-ol"></i> voir les détails</div></td>';
		//html += '</tr></table>';
		html += '</td>';
		//html += '</td>';
	}
	else
	{
		var real_stock_aff, preorder_stock_aff, estimateStock_aff;
		var cache_stock = products_stock_cache[cell.id_object];
		if (cache_stock == undefined) {
			real_stock_aff = 'XX';
			preorder_stock_aff = 'XX';
			estimateStock_aff = 'XX';
		} else if (cell.id_object2 != 0) {
			cache_stock = cache_stock[cell.id_object2];
			if (cache_stock == undefined) {
				real_stock_aff = 'XX';
				preorder_stock_aff = 'XX';
				estimateStock_aff = 'XX';
			} else {
				real_stock_aff = cache_stock.real_stock_aff;
				preorder_stock_aff = cache_stock.preorder_stock_aff;
				estimateStock_aff = '<span class="no_internet">'+cache_stock.estimateStock_aff+'</span>';
			}
		} else {
			real_stock_aff = cache_stock.real_stock_aff;
			preorder_stock_aff = cache_stock.preorder_stock_aff;
			estimateStock_aff = cache_stock.estimateStock_aff;
		}
		//console.log(p.base_price*(1+(p.tax_rate/100)) + ' <> ' + p.prices[ACAISSE.id_force_group]);
		html += '<td class="product_name_container"><div class="product_name_container_box"><div class="product_name"><span>' + name +'</span></div></div></td>';
	
		
		var display_base_price = 'none';
		
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
		&& p.base_price*(1+(p.tax_rate/100)) > p.prices[ACAISSE.id_force_group])
		{
			//$('#cell_'+cell.row+'_'+cell.col+' .product_base_price').show();
			display_base_price = 'block';
		}
		
		html += '<td class="availability">';
		
			html += '<table><tr>';
				html += '<td class="stock_available">'+estimateStock_aff+'</td>';
				html += '<td class="stock_real">'+real_stock_aff+'</td>';
				html += '<td class="stock_inqueue">-'+preorder_stock_aff+'</td>';
			html += '</tr></table>';
			
		html += '</td>';
		
		html += '<td class="p_price">';
			html += '<div class="product_base_price" style="display:'+display_base_price+'">' + base_price + '</div>';
			
			html += '<div class="product_price">' + price + '</div>';
		html += '</td>';
		
		//html += '</div>';
		//html += '</div></div>';
	}
	
	html +='</tr></table>';

	return html;
}

function _injectCellPageContent(cell, ps) {
	if (typeof (ps.pages[cell.id_object]) == 'undefined')
	{
		return 'Error 404';
	}
	else
	{
		var p = ps.pages[cell.id_object];
		var html = '<div class="name">' + p.name + '</div>';
		if (p.description)
		{
			html += '<div class="description">' + p.description + '</div>';
		}
		return html;
	}
}

function _injectCellAttributes(cell) {
	var a = ' class="cellType' + cell.type + ' cellStyle' + cell.style + ' rowspan' + cell.rowspan + ' colspan' + cell.colspan + '"';
	if (cell.colspan > 1)
	{
		a += ' colspan="' + cell.colspan + '"';
	}
	if (cell.rowspan > 1)
	{
		a += ' rowspan="' + cell.rowspan + '"';
	}
	a += ' data-idobject="' + cell.id_object + '" data-idobject2="' + cell.id_object2 + '"';
	a += ' id="cell_' + cell.row + '_' + cell.col + '"';

	return a;
}

function _refreshTicket(export_product_id_quantity) {
	if(ACAISSE.mode_debogue === true) {console.log(["export research : ",export_product_id_quantity]);}
	var total_quantity = 0;
	var total_cost = 0;
	var result = false;
	ACAISSE.cart.articles = [];
	
	//on vide la liste des produits
	$('#ticketView .lines tr').not('.header').remove();
	//Puis on recrer les lignes de produits si nécessaire  	
	if(ACAISSE.ticket.lines)
	{
		for (var i = 0; i < ACAISSE.ticket.lines.length; i++)
		{
			var line = ACAISSE.ticket.lines[i];
			var p = _loadProductFromId(line.id_product);
			if(p !== null)
			{
				var a = p.has_attributes === true ? _loadProductFromId(line.id_product).attributes[line.id_attribute] : null;
						
				var quantity = parseFloat(line.quantity);
				var cost = getProductPrice(p.id_product, a!=null?a.id_product_attribute:0); //TODO : ERROR de chercher dans le cache, faut se baser sur la ligne
				
				var unitId = params.units.from.indexOf(p.unity);
			
				if (a!=null && p.unity=="grammes" && a.attribute_name!='1g') { //@TODO unity other like ml, mm, ...
					unitId = params.units.from.indexOf('pièce');
				}
				
				if (unitId==-1) {
					params.units.from.push(p.unity);
					params.units.to.push('');
					params.units.scale.push(1);
					unitId = params.units.from.indexOf(p.unity);
				}
				
				quantity/=params.units.scale[unitId];
				
				total_quantity += (quantity!=line.quantity ? 1 : quantity);
				total_cost += quantity * parseFloat(cost);
		
				var ticket_line = new TicketLine(line);
			
				_drawCartLine(ticket_line);

				_cartUpdateLineQuantity(line.id_product, line.id_attribute, line.quantity, line.id, ticket_line);
				
				if(line.id_product == export_product_id_quantity)
				{
					result = line.quantity;
				}
				ACAISSE.cart.articles.push([line.id_product, line.id_attribute, line.quantity, cost]);
		   }
		}
	}
	
	//console.log(ACAISSE.ticket.id_customer+'<->'+ACAISSE.customer.id);
	
	//rechargement de l'utilisateur aussi.
	if (ACAISSE.ticket.id_customer != undefined && ACAISSE.ticket.id_customer != ACAISSE.customer.id)
	{
		_retreiveCustomerInformation(ACAISSE.ticket.id_customer);
		
	}
	// @TODO : à voir poru recharger directement sans revenir à la page principale LAURENT
	//_reloadPage();
	//puis on demande le rafraichissement des totaux
	//_refreshTotals();
	_recalcAndRefreshTotals();
	
	var ticket_date = ACAISSE.ticket.day;
	var ticket_date_dm = ticket_date.substring(5,10);
	ticket_date = ticket_date_dm.split('-');
	
	$('.currentTicketNum').html('<sup>'+ticket_date[1]+'-'+ticket_date[0]+'</sup>' + ACAISSE.ticket.ticket_num);
	
	return result;
}

// fonction permettant de charger les stock
function _refreshProductPageStock2(listToRefresh) {
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'RefreshProductStockFromPage',
			'listToRefresh': listToRefresh,
			'id_pointshop': ACAISSE.id_pointshop
		},
	}).done(function (response) {
		if (response.success)
		{
			if(ACAISSE.mode_debogue === true)
			{
				console.log(['_refreshProductPageStock2() ajax response',response]);
			}
			_doStockLevelUpdateOnPage2(response.response.stocks);
		}
		else
		{
			alert('Erreur 000947a : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 000948a');
	});
}

// fonction pour remettre à kour notre stock dans le html
function _doStockLevelUpdateOnPage2(stocks) {
	if (stocks)
	{
		var line;
		for (var i = 0; i < stocks.length; i++)
		{
			var cell = stocks[i][0];
			var real_stock = real_stock_aff = stocks[i]['physical_qu'];
			var preorder_stock = preorder_stock_aff = stocks[i][2];
			var estimateStock = estimateStock_aff = parseInt(real_stock)-parseInt(preorder_stock);
			
			if  (products_stock_cache[cell.id_product] == undefined) { // si on a jamais eu ce produit a ranger dans le cache
				products_stock_cache[cell.id_product] = {};
			}
			
			if (cell.id_product_attribute != "" && cell.id_product_attribute != 0) { // si en + il a un attribut
				if(ACAISSE.mode_debogue === true)
				{console.log(['prd attr', cell.id_product_attribute, cell.id_product]);}
				products_stock_cache[cell.id_product][cell.id_product_attribute] = {};
				products_stock_cache[cell.id_product][cell.id_product_attribute].real_stock_aff = real_stock_aff;
				products_stock_cache[cell.id_product][cell.id_product_attribute].preorder_stock_aff = preorder_stock_aff;
				products_stock_cache[cell.id_product][cell.id_product_attribute].estimateStock_aff = estimateStock_aff;
			} else {
				products_stock_cache[cell.id_product].real_stock_aff = real_stock_aff;
				products_stock_cache[cell.id_product].preorder_stock_aff = preorder_stock_aff;
				products_stock_cache[cell.id_product].estimateStock_aff = estimateStock_aff;
			}
			
			line = $($('#productList .contentOverlay tr')[cell.row]);
		
			line.find('.stock_real').html(real_stock == '0' ? 'rupture' : real_stock).removeClass('error').addClass(real_stock == '0' ? 'error' : '');
			
			line.find('.stock_inqueue').html('-' + preorder_stock);

			line.find('.stock_available').html(estimateStock)
					.removeClass('error')
					.removeClass('warning')
					.removeClass('good')
					.addClass(estimateStock > params.products.STOCK_WARNING_LIMIT ? 'good' : estimateStock <= 0 ? 'error' : 'warning');

			//@TODO : stockage dans le cache product
		}
	}
}



/**
 * Les deux foctions là peuvent surmenent etre amélioré.
 * @param {$Jquery} $elemMsg id in caisse.tpl
 */
function showSuccessMessage($elemMsg){
   $('#alertMessage').addClass('successMessage');
   $elemMsg.show();
   setTimeout(function(){ 
       $('#alertMessage').removeClass('successMessage');
        $elemMsg.hide();
   }, 1000);
}

function showErrorMessage($elemMsg){
   $('#alertMessage').addClass('errorMessage');
   $elemMsg.show();
   setTimeout(function(){ 
       $('#alertMessage').removeClass('errorMessage');
        $elemMsg.hide();
   }, 1000);
}



function _updateCustomerEditForm(customer, groups) {
	console.log(customer);
	$('input[name^=customer-edit-]').val('');
	$('input[name^=customer-edit-alias]').val('MonAdresse');
	$('#customer-edit-group input[type=checkbox]').prop('checked', false);
	$('#customer-edit-more input[type=checkbox]').prop('checked', false);
	$('#customer-edit-group input[type=checkbox][readonly]').click();
	
	if(customer.newsletter == 1)
		$('#customer-edit-newsletter').prop('checked',true);
		
	if(customer.optin == 1)
		$('#customer-edit-optin').prop('checked',true);
	
	if ($('#userEditContainer [data-page-id="pageUser1"]').html().indexOf('Nouveau')!=-1) {
		return;
	}
	
	// On met à jour l'addresse principale si elle existe
	for (var key in customer.address) {
		$('#customer-edit-' + key).val(customer.address[key]);
	}
	if(customer.address)
	{
		$('#customer-id_country').val(customer.address['id_country']);
	}
	
	// On met à jour les informations du Customer
	for (var key in customer) {
		$('#customer-edit-' + key).val(customer[key]);
	}
	
	if (typeof customer.groups != 'undefined') {
		for (var key in customer.groups){
			$('#customer-edit-group-'+customer.groups[key])[0].checked=true;
		}
	}
	
	var chckbx = $('#customer-edit-group input[type=checkbox][value='+customer.id_default_group+']')[0];
	chckbx.readOnly=chckbx.indeterminate=true;
	}

/**
 * @TODO : errur conception importante 
 * METHODE A REVOIR, LES PRIX DANS LE PANIEr DOIVENT ETRE CEUX RENVOY2 PAR LE SERVEUR ET PAS LE CACCHE
 * */
function refreshPrices(cart_lines) { //rafraichir les prix afficher en fonction du groupe de prix...
	
	//console.warn(['refreshPrices',cart_lines]);
	var sum = 0;
	var sum_ht = 0;
	var sum_ttc = 0;	
	
	if (cart_lines!=undefined) {
		//console.warn('TODO : AUTRE CAS DE REFRESH.... lorsque l\'argument est apssé à refreshPrice...')
		//console.log(ACAISSE);
		
		var product_list = [];
		
		for (var i=0; i<cart_lines.length; i++) {
			
			var cart_line = new CartLine(cart_lines[i]);
			//console.log(cart_line);
			
			product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(cart_lines[i].id_product))));
			product_list[i].id_attribute = cart_lines[i].id_product_attribute;
			product_list[i].qte = cart_lines[i].quantity;
			product_list[i].price_wt = cart_lines[i].price_wt;
			sum_ttc += cart_line.line_price_ht;
			sum_ttc += cart_line.line_price_ttc;
			
		}
		
		// on remet à jour la liste des produits sur les écrans...
		//var module_obj = {action:'switch_ticket',product_list:product_list, cart_lines:cart_lines};
		
		var module_obj = {
			action:'switch_ticket',
			product_list:product_list,
			ticket:new Ticket(ACAISSE.ticket),
			raw_ticket:ACAISSE.ticket
		};
		
		modules_action(module_obj);
		
		// on remet à jour le total sur les écrans... 
		module_obj = {action:'tot',tot:formatPrice(ACAISSE.cart_tot),id_ticket:ACAISSE.ticket.ticket_num};
		modules_action(module_obj);
		
		/*
		 //les totaux sont calcules dans les deux cas de figure par l'appel commun à _recalcAndRefreshTotals
		
		//@TODO vérifier l'utilité du bloc ci-dessous...
		if (parseFloat($('#totalCost').html())>ACAISSE.cart_tot) {
			module_obj = {action:'reduce',reduce:formatPrice(parseFloat($('#totalCost').html()) - ACAISSE.cart_tot),new_tot:ACAISSE.cart_tot};
			modules_action(module_obj);
		}
		
		//$('#totalCost').html(formatPrice(ACAISSE.cart_tot)+'€');
		*/
		
	} else if (typeof ACAISSE.ticket.lines=='object') {
		
		var product_list = [];
			
		
		var $trs = $('#ticketView .lines tbody tr');
		
		for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
			var ticket_line = new TicketLine(ACAISSE.ticket.lines[i]);
			/*
			console.log(ticket_line);
			*/
			product_list.push(_loadProductFromId(ACAISSE.ticket.lines[i].id_product));
			product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
			product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
			/*
			var price = getProductPrice(ACAISSE.ticket.lines[i].id_product, ACAISSE.ticket.lines[i].id_attribute)
			var unity = price.split('/');
			sum += parseFloat(price)*(parseFloat(ACAISSE.ticket.lines[i].quantity)/(price.indexOf('/')==-1?1:params.units.scale[params.units.to.indexOf('/'+unity[1])]));
			*/
			sum_ht = ticket_line.line_price_ht;
			sum_ttc = ticket_line.line_price_ttc;
			
			var $tr = $trs.eq(i+1); //la ligne de ticket à rafraîchir
			//faut rafraichier le PU de cette ligne
			$('td.cost',$tr).replaceWith(getCartLineCostLabelHTML(ticket_line));
			
			//mise à jour du total en bout de ligne
			$('td.totalLineCost',$tr).replaceWith(getCartTotalLineCostLabelHTML(ticket_line));

		}
		
		// on remet à jour la liste des produits sur les écrans...
		//@TODO : à refaire AUSSI en utilisant les infos au format TicketLine instance
		var module_obj = {
			action:'switch_ticket',
			product_list:product_list,
			ticket:new Ticket(ACAISSE.ticket),
			raw_ticket:ACAISSE.ticket
		};
		modules_action(module_obj);
		
		// on remet à jour le total sur les écrans... 
		var module_obj = {action:'tot',tot:formatPrice(sum),id_ticket:ACAISSE.ticket.ticket_num};
		modules_action(module_obj);
		
		//$('#totalCost').html(formatPrice(sum)+'€ TTC');
	}	
	/*
	ACAISSE.cart_tot = sum;
	ACAISSE.cart_tot_ht = sum_ht;
	ACAISSE.cart_tot_ttc = sum_ttc;
	*/
	_recalcAndRefreshTotals();
	
}


function _loadProductFromId(id_product,use_product_cache = null, force_reload_from_server = false)
{
  if (id_product==undefined) {
  	return null;
  }
  
  if(acaisse_products_prices.catalog[id_product]==undefined || force_reload_from_server === true)
  { 
  	 $.ajax({
        type: 'post',
        async:false,
        data: {
          'action': 'loadProduct',
          'id_product': id_product,
          'ajaxRequestID': ++ACAISSE.ajaxCount,
        },
        dataType: 'json',
      }).done(function (response) {
        if (response.success == true) { 
        	
        	for(id_product in response.results.catalog)
        	{
        		var p = response.results.catalog[id_product];
        		acaisse_products_prices.catalog[id_product] = p;
        	}
        	for(ean in response.results.ean)
        	{
        		var p = response.results.ean[ean];
        		acaisse_products_prices.ean[ean] = p;
        	}
        	//acaisse_products_prices.push(response);
        	
        } else {
          console.log('Erreur OG000404 : ' + response.errorMsg);
        }
      }).fail(function () {
        console.log('Erreur OG001404 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
      });
  	 
  }
  
  //maintenant qu'on a tenté de complété le cache on reteste si le produit est toujorus absent du cache de la caisse'
  if(acaisse_products_prices.catalog[id_product]==undefined)
  {
  	 return false;
  }
  
  if(use_product_cache === null)
  {
    use_product_cache = 'js';  
  }
  
  if(use_product_cache == 'js')
  {
  	if(ACAISSE.mode_debogue === true)
  	{
    	console.log('Loading _loadProductFromId('+id_product+') USING CACHE SYSTEM');
    }
    if (true) {// mettre à true dès que le cache remonte minimifié
    	
    	// AU MOMENT DU CHARGEMENT DU PRODUIT, ON REGARDE S'IL EST DISPONIBLE DANS LE POINTSHOP OU NON
    	// SI NON, ON NE L'AFFICHE PAS
    	var temp_p = new Product(id_product);
    	
    	for(var shop in temp_p.disponibility_by_shop) {
    		
    		//console.log(id_product+'-----------'+temp_p.disponibility_by_shop[ACAISSE.id_pointshop]);
    		 
    		if(ACAISSE.id_pointshop == shop)
    		{
    			//console.log(id_product+'-----------'+temp_p.disponibility_by_shop[ACAISSE.id_pointshop]);
    			if(temp_p.disponibility_by_shop[ACAISSE.id_pointshop] == false)
    			{
    				return null;
    			}
    			else
    			{
    				return new Product(id_product);
    			}
    		}
		}
	} else {
		return acaisse_products_prices.catalog[id_product];
	}
  }
  else
  {    
    if(localStorage.getItem('product__'+id_product) == undefined)
    {        
      if(ACAISSE.mode_debogue === true)
      {
      	console.log('Loading _loadProductFromId('+id_product+') WITHOUT CACHE SYSTEM');
      }
      var ajax_id = ++ACAISSE.ajaxCount;
      var result = undefined;
      $.ajax({
        type: 'post',
        async:false,
        data: {
          'action': 'loadProduct',
          'id_product': id_product,
          'ajaxRequestID': ++ACAISSE.ajaxCount,
        },
        dataType: 'json',
      }).done(function (response) {
        if (response.success == true) {          
          var response = response.results;
          if(response != false)
          {
            localStorage.setItem('product__'+id_product,JSON.stringify(response['catalog'][id_product]));
          }
        } else {
          alert('Erreur 000404 : ' + response.errorMsg);
        }
      }).fail(function () {
        alert('Erreur 001404 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
      });
      
    }    
    return JSON.parse(localStorage.getItem('product__'+id_product));
  }  
}

function longPressOnProduct(container, elem) {
	
	if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2') {
		$('#popupNumeric.cover').addClass('fullImg');
	}
	
	var pressTimer;
	$(container).delegate(elem, 'mouseup', function () {
		if (!_loadProductFromId($(this).data('idobject')).has_attributes || $(this).data('idobject2')!="") {
			clearTimeout(pressTimer);
			// Clear timeout
			return false;
		}
	}).delegate(elem, 'mousedown', function (e) {
		if (!_loadProductFromId($(this).data('idobject')).has_attributes || $(this).data('idobject2')!="") {
			//on desactive le menu contextuel (clic droit)
			$(this)[0].oncontextmenu = function () {
				return false;
			};
			
			pressTimer = window.setTimeout(function () {
				
				if(ACAISSE.mode_debogue === true)
				{
					console.log(['longpress this',$(this).data('idobject2')]);
					
					//on affiche le clavier au bout de 250ms
					console.log("on affiche la pavé numérique");
					
					//cloneProductWithStock($(this).data('idobject'), $(this).data('idobject2')); //n'a pas d'utilité à mon sens (Kévin)
					
					console.log(['popupProduct',$(this).first('.product_price').html()]);
				}
				
				$('#popupNumeric')
					.data('idobject',$(this).data('idobject'))
					.data('idobject2',$(this).data('idobject2'));
				
				$('#popupNumeric .product_price')
					.html(getProductPrice($(this).data('idobject'), $(this).data('idobject2')));
				
				var p = _loadProductFromId($(this).data('idobject'));
				var name = p.product_name;
				var unity = p.unity;
				if (p.has_attributes) {
					p = p.attributes[$(this).data('idobject2')];
					name += p.attribute_name;
				}
				
				var real_stock_aff, preorder_stock_aff, estimateStock_aff;
				var cache_stock = products_stock_cache[$(this).data('idobject')];
				if (cache_stock == undefined) {
					real_stock_aff = '';
					preorder_stock_aff = '';
					estimateStock_aff = '';
				} else if ($(this).data('idobject2') != 0) {
					cache_stock = cache_stock[$(this).data('idobject2')];
					if (cache_stock == undefined) {
						real_stock_aff = '';
						preorder_stock_aff = '';
						estimateStock_aff = '';
					} else {
						real_stock_aff = cache_stock.real_stock_aff;
						preorder_stock_aff = cache_stock.preorder_stock_aff;
						estimateStock_aff = '<span class="no_internet">'+cache_stock.estimateStock_aff+'</span>';
					}
				} else {
					real_stock_aff = cache_stock.real_stock_aff;
					preorder_stock_aff = cache_stock.preorder_stock_aff;
					estimateStock_aff = '<span class="no_internet">'+cache_stock.estimateStock_aff+'</span>';
				}
				
				$('#popupNumeric .stock_available')
					.html(estimateStock_aff)
					.removeClass('error')
					.removeClass('warning')
					.removeClass('good');
				
				$('#popupNumeric .stock_real').html(real_stock_aff);
				
				$('#popupNumeric .stock_inqueue').html('-'+preorder_stock_aff);
				
				$('#popupNumeric .product_name').html('');
				
				if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0') {
					$('#popupNumeric .acaisse_productcell_content')
					.css('background-image','url('+p.image+')')
				}
				
				$('#popupNumeric .acaisse_productcell_content')
					.data('idobject',$(this).data('idobject'))
					.data('idobject2',$(this).data('idobject2'));
				
				_refreshProductPageStock([{
					id_product: $(this).data('idobject'),
					id_product_attribute: $(this).data('idobject2')==""?0:$(this).data('idobject2'),
					row: 'popup',
					col: 0
				}]);
				
				var unitId = params.units.from.indexOf(unity);
				
				if (unitId==-1) {
					//console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
					params.units.from.push(p.unity);
					params.units.to.push('');
					params.units.scale.push(1);
					unitId = params.units.from.indexOf(unity);
					//throw "Erreur d'unité : unité inconnue : ";
				}
				
				// On récupère les informations du produit
				_getProductInfos($(this).data('idobject'), $(this).data('idticketline'));
				
				$('#popupNumeric .title').show();
				
				$('#popupNumeric .global_reduc_amount').remove();
				$('#popupNumeric .cart_line_reduc_amount').remove();
				
				showPopup($(container), params.units.scale[unitId]!=1);
			   	
			}.bind(this), 250);
			return false;
		}
	});
}

function _foundAttributeById(attributes,id_product_attribute)
{
	var result = false;
	$.each(attributes,function(i,attr){
		console.log(['comparaison',attr,id_product_attribute,attr.id_product_attribute]);
		if(id_product_attribute == parseInt(attr.id_product_attribute))
		{
			result = attr;
		}	
	});
	
	return result;
}

function _drawProductInfos(response, idTicketLine, open_specific_tab)
{
	var product = response.response.product;
	
	if(idTicketLine !== 'undefined')
		$('#popupNumeric').data('idticketline', idTicketLine);
	
	
	$('#productInfos')
		.data('product',product)
		.data('stocks',response.response.stocks)
		.data('stockMvtReasons',response.response.stock_mvt_reason)
		.data('stocks_available',response.response.stocks_available)		
		.data('PS_STOCK_MVT_TRANSFER_FROM', response.response.PS_STOCK_MVT_TRANSFER_FROM)
		.data('PS_STOCK_MVT_TRANSFER_TO', response.response.PS_STOCK_MVT_TRANSFER_TO)
		.data('PS_STOCK_MVT_DEC_REASON_DEFAULT', response.response.PS_STOCK_MVT_DEC_REASON_DEFAULT)
		.data('PS_STOCK_MVT_INC_REASON_DEFAULT', response.response.PS_STOCK_MVT_INC_REASON_DEFAULT);
	
	// INFOS POPUP PRIX
	$('#productInfos .prices .wholesale-price').html(product.wholesale_price);
	$('#productInfos .prices .price').html(product.wholesale_price);
	
	// S'il y a des caractéristiques
	var features = response.response.features;
	if(features.length > 0)
	{
		for (var i=0; i < features.length; i++) {
		 $('#features ul').prepend('<li><span class="fname">'+ features[i].name +'</span> <span class="fvalue">'+ features[i].value +'</span> </li>');
		};
	}
	else
	{
		 $('#features ul').prepend('<li>Aucune caractéristique</li>');
	}
	
	// S'il y a des informations de stocks avancés
	var stocks = response.response.stocks;
	var stocks_available = response.response.stocks_available;
	var attributes = response.response.attributes;
	var infos_attr = response.response.infos_attr;
	var id_product = parseInt(0 + $('#popupNumeric').data('idobject'));
	var id_product_attribute = parseInt(0 + $('#popupNumeric').data('idobject2'));
	
	var attr = _foundAttributeById(attributes,id_product_attribute);
	
	//console.log({ATTR:attr, attributes:attributes, id_product_attribute:id_product_attribute });
	
	if(id_product_attribute!=0 && attr!=false)
	{
		product.name += ' <strong>' + attr.attribute_name + '</strong>';
		product.reference = attr.reference != ''? attr.reference : product.reference;
		product.ean13 = attr.ean13 != ''? attr.ean13 : product.reference;		
	}
	
	
	
	$('#productInfos .title').html('<h3>'+product.name+'</h3>');
	$('#productInfos .title').append('<span class="productId label-info"><a target="_blank" href=" ' + response.response.bo_product_link + '"><i class="fa fa-external-link"></i> '+product.id+'</a></span>');
	product.reference!=''?$('#productInfos .title').append('<span class="reference label-info"><i class="hashtag">#</i> '+product.reference+'</span>'):'';
	product.category!=''?$('#productInfos .title').append('<span class="mainCat label-info"><i class="fa fa-folder-open-o"></i> '+product.category+'</span>'):'';
	product.ean13!=''?$('#productInfos .title').append('<span class="ean label-info"><i class="fa fa-barcode"></i> '+product.ean13+'</span>'):'';      
	$('#details .descShort').html(product.description_short); 
	$('#details .desc').html(product.description); 
	
	/*
	console.log({
		id_product:id_product,
		id_product_attribute:id_product_attribute,
		stocks:stocks,
		stocks_available:stocks_available,
		attributes:attributes,
		infos_attr:infos_attr,
	});
	*/
	if(stocks !== null && stocks !== false)
	{
		$('#stockBtn').prop('disabled',false);
		
		// Si le produit a des attributs
		
		/*****************
		 * DESACTIVATION DE LA VUE AVEC ATTRIBUTS 
		 */
		
		if(false && ((infos_attr !== null && infos_attr.length > 0) || (infos_attr !== null && infos_attr.length == undefined)))
		{
			var html = '<table cellspacing="0" cellpadding="0"><thead><td>Attribut</td><td>Entrepôt</td><td>Qté physique</td><td>Qté utilisable</td><td> En commande</td><td>&nbsp;</td></thead>';

			for (var a in infos_attr) 
			{						
				html += '<tr class="attrRow">';					
				html += '<td rowspan="'+ parseInt(stocks.length+infos_attr[a].w.cmf+infos_attr[a].w.nbCmf) +'">';
					for(var ag in infos_attr[a].attr_group)
					{
						html+= infos_attr[a].attr_group[ag] +' : '+ infos_attr[a].attr_val[ag] +'<br />';
					}
					html +='</td>';
				
				for (var i=0; i < stocks.length; i++) {
					var id_w = stocks[i].id_warehouse;
					
					html += '<tr class="'+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_warehouse==stocks[i].id_warehouse?'isWarehouse':'')+'">';
					html += '<td class="first">'+ stocks[i].wname +'</td>';
					html += '<td><span class="qty qty-physique">'+ infos_attr[a].w[id_w].pQ +'</span></td>';
					html += '<td><span class="qty qty-utilisable">'+infos_attr[a].w[id_w].pU+'</span></td>';
					
					// Si on a des commandes en cours, on affiche le total et le bouton détail
					if(infos_attr[a].w[id_w]['os'] != undefined && infos_attr[a].w[id_w]['os'].length > 0)
					{
						html += '<td><span class="qty qtos">'+infos_attr[a].w.tot+'</span></td>';
					}
					else 
					{
						html += '<td><span class="qty qtos">0</span></td>';
					}
					html += '</tr>';
					
					
					if(infos_attr[a].w[id_w]['os'] != undefined && infos_attr[a].w[id_w]['os'].length > 0)
					{
						html += '<tr class="cmf">';
						html += '<td>Réf. commande</td>';
						html += '<td>Qté commandée</td>';
						html += '<td>Reliquat</td>';
						html += '<td>Livraison</td>';
						html += '<td></td>';
						html += '</tr>';
						
						for (var j=0; j < infos_attr[a].w[id_w]['os'].length; j++) 
						{
							if(infos_attr[a].w[id_w]['os'][j].qtyE !== undefined && infos_attr[a].w[id_w]['os'][j].qtyR !== undefined)
							{
								html += '<tr class="cmf">';
								html += '<td class="oRef"><i class="fa fa-truck"></i> '+ infos_attr[a].w[id_w]['os'][j].ref +'</td> ';
								html += '<td class="oQtyE">'+ infos_attr[a].w[id_w]['os'][j].qtyE +'</td> ';
								html += '<td class="oQtyR">'+ infos_attr[a].w[id_w]['os'][j].qtyR +'</td> ';
								html += '<td class="oDate">'+ infos_attr[a].w[id_w]['os'][j].date +'</td>';
								html += '<td></td>';
								html += '</tr>';
							}
						}
					}
				};
				html += '</tr>';
			}
			html += '</table>';
			$('#stocksPopup').html(html);
		}
		
		
		if(true) //toujours utyiliser cette vue, qu'il y ai attribus ou pas else //si pas d'attribus
		{			
			//console.log(infos_attr);
			//console.log(stocks);
			
			var html = '<table class="warehouses" cellspacing="0" cellpadding="0"><thead><td>Entrepôt</td><td>Qté physique</td><td>Qté utilisable</td><td> En commande</td><td></td></thead>';
			
			for (var i=0; i < stocks.length; i++) 
			{
				var idw = id_w = stocks[i].id_warehouse;
				
				var extra_classes = '';
				var warehouse_active = stocks_available[stocks[i].id_warehouse] != undefined;						
				extra_classes += acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_warehouse==stocks[i].id_warehouse?' isWarehouse':'';
				extra_classes += warehouse_active ? ' active':' inactive';
				
				
				html += '<tr class="'+extra_classes+'" data-idWarehouse="'+stocks[i].id_warehouse+'">';
				html += '<td class="first">'+ stocks[i].wname + (warehouse_active && stocks_available[stocks[i].id_warehouse] != '' ?' ('+stocks_available[stocks[i].id_warehouse]+')' :'') +'</td>';
				
				if(id_product_attribute == 0)
				{
				
					html += '<td><span class="qty qty-physique">'+ stocks[i].pQ +'</span></td>';
					html += '<td><span class="qty qty-utilisable">'+ stocks[i].pU +'</span></td>';
					
					// Si on a des commandes en cours, on affiche le total et le bouton détail
					if(stocks[i][idw].os != undefined && stocks[i][idw].os.length > 0)
					{
						html += '<td><span class="qty qtos">'+stocks[i].tot+'</span></td>';
					}
					else 
					{
						html += '<td><span class="qty qtos">0</span></td>';
					}
					
					html += '<td>';
						html +='<span class="button button-success add"><i class="fa fa-chevron-up"></i></span>';
						html +='<span class="button button-error'+(stocks[i].pQ==0?' disabled':'')+' remove"><i class="fa fa-chevron-down"></i></span>';
						html +='<span class="button button-info'+(stocks[i].pQ==0?' disabled':'')+' transfert"><i class="fa fa-exchange"></i></span>';
					if(warehouse_active) //si entreport actif
					{
						html +='<span class="button active-warehouse"><i class="fa fa-chain-broken "></i></span>';
					}
					else
					{
						html +='<span class="button active-warehouse"><i class="fa fa-chain"></i></span>';
					}
					html += '</td>';
				
				}
				else //c'est une déclinaison
				{
				
					html += '<td><span class="qty qty-physique">'+ infos_attr[id_product_attribute].w[idw].pQ +'</span></td>';
					html += '<td><span class="qty qty-utilisable">'+ infos_attr[id_product_attribute].w[idw].pU +'</span></td>';
					
					
					// Si on a des commandes en cours, on affiche le total et le bouton détail
					if(false && stocks[i][idw].os != undefined && stocks[i][idw].os.length > 0) 
					{
						html += '<td><span class="qty qtos">'+stocks[i].tot+'</span></td>';
					}
					else 
					{
						html += '<td><span class="qty qtos">0</span></td>';
					}
					
					html += '<td>';
						html +='<span class="button button-success add"><i class="fa fa-chevron-up"></i></span>';
						html +='<span class="button button-error'+(stocks[i].pQ==0?' disabled':'')+' remove"><i class="fa fa-chevron-down"></i></span>';
						html +='<span class="button button-info'+(stocks[i].pQ==0?' disabled':'')+' transfert"><i class="fa fa-exchange"></i></span>';
					if(warehouse_active) //si entreport actif
					{
						html +='<span class="button active-warehouse"><i class="fa fa-chain-broken "></i></span>';
					}
					else
					{
						html +='<span class="button active-warehouse"><i class="fa fa-chain"></i></span>';
					}
					html += '</td>';
				
				}
				
				html += '</tr>';
				
				
				if(false && 	stocks[i][idw].os != undefined && stocks[i][idw].os.length > 0)
				{
					html += '<tr class="cmf">';
					html += '<td>Réf. commande</td>';
					html += '<td>Qté commandée</td>';
					html += '<td>Reliquat</td>';
					html += '<td>Livraison</td>';
					html += '<td></td>';
					html += '</tr>';
					
					for (var j=0; j < stocks[i][idw].os.length; j++) 
					{
						if(stocks[i][idw].os[j].qtyE !== undefined && stocks[i][idw].os[j].qtyR !== undefined)
						{
							html += '<tr class="cmf">';
							html +='<td class="oRef"><i class="fa fa-truck"></i> '+ stocks[i][idw].os[j].ref +'</td> ';
							html +='<td class="oQtyE">'+ stocks[i][idw].os[j].qtyE +'</td> ';
							html +='<td class="oQtyR">'+ stocks[i][idw].os[j].qtyR +'</td> ';
							html +='<td class="oDate">'+ stocks[i][idw].os[j].date +'</td>';
							html +='<td></td>';
							html += '</tr>';
						}
					}
				}
			};
			html += '</table>';
			$('#stocksPopup').html(html);
		}
		
	}
	else
	{
		 $('#stockBtn').prop('disabled','disabled');
	}
	
	
	
	$('#popupProduct, #features, #stocksPopup, #prices').height(parseInt($('#middleContainer').height()-$('#productInfos .title').height()-23));
	//return product; 
	//$('#details .descShort').html(response.product.description);
	
	if(open_specific_tab != undefined)
	{
		$(open_specific_tab).click();
	}
	
}


function _getProductInfos(idProduct, idTicketLine){
	//console.log(idProduct);
	
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'GetProductDetails',
			'id_product': idProduct,
			'id_ticket_line': idTicketLine,
		},
	}).done(function (response) {
		//console.log(response);
		if (response.success)
		{
			 _drawProductInfos(response,idTicketLine);
		}
		else
		{
			alert('Erreur L000969 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur L000969 : ma première =P');
	});
}

function cloneProductWithStock(idProduct, idProductAttribute){
	if(ACAISSE.mode_debogue === true)
	{
		console.log(idProduct);
		console.log(idProductAttribute);
	}
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'GetProductWithStock',
			'idProduct': idProduct,
			'idProductAttribute': idProductAttribute,
			'id_pointshop': ACAISSE.id_pointshop
		},
	}).done(function (response) {
		if (response.success)
		{
			var p = response.response.product;
			
			var name = _loadProductFromId(idProduct).product_name;
			var img = _loadProductFromId(idProduct).image;
			var price = getProductPrice(idProduct, idProductAttribute);
			
			//si il y a un attribut
			if(typeof(_loadProductFromId(idProduct)['attributes'][idProductAttribute]) !='undefined')
			{
			  name = _loadProductFromId(idProduct)['attributes'][idProductAttribute].attribute_name;
			  img = _loadProductFromId(idProduct)['attributes'][idProductAttribute].image;
			  price = getProductPrice(idProduct,idProductAttribute);
			}

			var product = {
				real_stock : p.ps_stock,
				preorder_stock : p.preorder_stock,
				estimate_stock : parseInt(p.ps_stock) - parseInt(p.preorder_stock),
				name : name,
				img : img,
				price : price,
				id : idProduct,
				id_attribute : idProductAttribute
			};      
			doCloneProduct(product);
			return product;  
		}
		else
		{
			alert('Erreur 000947 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 000948b');
	});
}

function doCloneProduct(product) {
	//var product = getProductWithStock($elemToClone.data('idobject'), $elemToClone.data('idobject2'));
	$('#popupNumeric').data('idobject', product.id).data('idobject2', product.id_attribute);
	$('#popupNumeric .stock_real')
			.text(product.real_stock == '0' ? 'rupture' : product.real_stock).removeClass('error').addClass(product.real_stock == '0' ? 'error' : '');
	$('#popupNumeric .stock_inqueue').text('-' + product.preorder_stock);
	$('#popupNumeric .product_price').text(product.price);


	$('#popupNumeric .stock_available').text(product.estimate_stock)
			.removeClass('error')
			.removeClass('warning')
			.removeClass('good')
			.addClass(product.estimate_stock > params.products.STOCK_WARNING_LIMIT ? 'good' : product.estimate_stock <= 0 ? 'error' : 'warning');
	
	$('#popupNumeric .product_name').text(product.name);
	
	var pImg;
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0') {
		if(product.img !== undefined){
			pImg = product.img
		}
		else {
			pImg = '../modules/acaisse/img/noPhoto.jpg';
		}
		$('#popupNumeric .acaisse_productcell_content').css('background-image','url('+ pImg +')');
		
		if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2') {
			$("#popupNumeric").addClass("fullImg");
		}	
	}
}



function _cancelTicket() {
	$.ajax({
		type: 'post',
		data: {
			'action': 'CancelTicket',
			'id_ticket': ACAISSE.ticket.id,
			'ajaxRequestID': ++ACAISSE.ajaxCount,
			//'listToRefresh':ACAISSE.pageProductBuffer,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true)
		{
			_startNewPreorder();
		}
		else
		{
			alert('Erreur 00177 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 00178 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
	});
}

function _validateCurrentTicket() {
	//si le ticket est vide rien à faire
	var error = false;
	if (ACAISSE.ticket.lines.length == 0)
	{
		alert('Veuillez d\'abord sélectionner au moins un produit');
		error = true;
	}
	else if (acaisse_profilesNoCheckinIDs.indexOf('' + ACAISSE.customer.id) != -1)
	{
		alert('Veuillez d\'abord sélectionner un client.');
		error = true;
	}
	else if (!error)//tout est bon, on va demander au serveur la conversion du ticket en véritable commande Prestashop
	{
		$.ajax({
			type: 'post',
			data: {
				'action': 'ValidateTicket',
				'id_ticket': ACAISSE.ticket.id,
				'id_customer': ACAISSE.customer.id,
				'id_group': ACAISSE.customer.id_default_group, //3
				'id_force_group': ACAISSE.id_force_group, //5
				'ajaxRequestID': ++ACAISSE.ajaxCount,
			},
			dataType: 'json',
		}).done(function (response) {
			if (response.success == true) {
				response = response.response;
				
				if (response.id_not_found.length>0) {
					var p = _loadProductFromId(response.id_not_found[0]);
					console.log(response);
					alert("ATTENTION !! Le produit #"+p.id_product+" : "+p.product_name+" n'as pas été correctement ajouté au ticket. Il se peut que la quantité minimal de commande dans la fiche produit soit inférieur à la quantité actuelle du ticket.");
					return false;
				}
				
				//console.log(['validateTicket','response',response]);
				
				/*
				$('#preOrderButtonsPanel').hide();
				$('#orderButtonsPanel').show();
				*/
				
				ACAISSE.cart_tot = response.cart_tot;
				ACAISSE.cart_tot_ht = response.cart_tot_ht;
				ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
				
				//console.log('ACAISSE.cart_tot : ' + ACAISSE.cart_tot);
				//alert(ACAISSE.ticket);
				ACAISSE.ticket = response.ticket;
				ACAISSE.cartModificationLocked = true; //on bloque les action d'ajout de produits et de modification des quantités
				
				refreshPrices(response.cart_products);
				
			} else {
				alert('Erreur 00177 : ' + response.errorMsg);
			}
		}).fail(function () {
			alert('Erreur 00178 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
		});
	}
}


function trashLineFromCart(id_product, id_product_attribute, id_ticket_line) {
	if (ACAISSE.ticket.id == null)
	{
		alert('Impossible d\'ajouter un produit. Aucun ticket crée.');
		return;
	}

	if (ACAISSE.cartModificationLocked == true)
	{
		$.ajax({
			type: 'post',
			dataType: 'json',
			data: {
				'action': 'CancelCartFromTicket',
				'id_ticket': ACAISSE.ticket.id,
				'id_product': id_product,
				'id_product_attribute': id_product_attribute,
			}
		}).done(function (response) {
			if (!response.success) {
				alert('Erreur 001554' + response.errorMsg);
				return;
			}
			ACAISSE.ticket = response.response.ticket;
			//remise en place du panneau non encaisse
			$('#orderButtonsPanel').hide();
			$('#preOrderButtonsPanel').show();
			$('#orderButtonsPanel .paymentButton').removeClass('selected');
			$('#orderButtonsPanel .paymentButton').removeClass('selected');
			$('#preOrderButtonsPanel #registerPayment').addClass('disabled');
			ACAISSE.choosenPaymentMode = false;
			ACAISSE.cartModificationLocked = false;
			ACAISSE.cart_tot = false;
			//puis rappel de l'instruction initiale
			trashLineFromCart(response.request.id_product, response.request.id_product_attribute, response.request.id_ticket_line);

		}).fail(function () {
			alert('Error 001555 Trash cart after cancel real cart failed.');
		});

		return; //on stop ici, la commande sera retenté qu'après
		// annulation du panier et annulation du passage à l'encaissement	
	}
	//sinon on peut réaliser l'opération directement.

	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'TrashLineFromCart',
			'id_ticket': ACAISSE.ticket.id,
			'id_product': id_product,
			'id_attribute': id_product_attribute,
			'id_ticket_line': id_ticket_line,
			'ajaxRequestID': ACAISSE.ajaxCount,
			'listToRefresh': ACAISSE.pageProductBuffer,
		}
	}).done(function (response) {
		if (!response.success) {
			alert('Erreur 000554' + response.errorMsg);
			return;
		}
		response = response.response;
		ACAISSE.ticket = response.ticket;
		
		var product_list = [];
		for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
			product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
			product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
			product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
		}
		if(ACAISSE.mode_debogue === true)
			{console.log(product_list);}
		//var module_obj = {action:'switch_ticket',product_list:product_list};
		
		var module_obj = {
			action:'switch_ticket',
			product_list:product_list,
			ticket:new Ticket(ACAISSE.ticket),
			raw_ticket:ACAISSE.ticket
		};
		
		modules_action(module_obj);
		
		_refreshTicket();
		_doStockLevelUpdateOnPage(response.stocks);
	}).fail(function () {
		alert('Error AddProductToCart failed.');
	});
}

function _editCustomer(logg)
{
	if ($('input[name="groupBox[]"][readonly]').length != 1) {
    	//alert('Veuillez choisir un et un seul groupe par défaut en cliquant une seconde fois sur la case à cocher désiré.');
    	//return false;
    }
    
    var datas = {};
    datas.id_default_group = $('input[name="groupBox[]"][readonly]').val();
    
    var groupBox = [];
    $('input[name="groupBox[]"]:checked, input[name="groupBox[]"][readonly]').each(function () {
        groupBox.push($(this).val());
    });
    
    var newsletter = $('input[name="customer-edit-newsletter"]').prop('checked');
    var optin = $('input[name="customer-edit-optin"]').prop('checked');
    
    $('input[name^="customer-edit-"]').each(function () {
        datas[$(this).attr('name').replace('customer-edit-','')] = $(this).val();
    });
    
    datas['groups'] = groupBox;
    datas['newsletter'] = newsletter;
  	datas['optin'] = optin;
         
    datas['logg'] = logg;
    datas['id_country'] = $('#customer-id_country').val();
    
    if ($('#userEditContainer [data-page-id="pageUser1"]').html().indexOf('Nouveau')!=-1) {
		datas.id=0;
	}
    
    PRESTATILL_CAISSE.loader.start();
    
    $.ajax({
        type: 'post',
        data: {
            'action': 'EditCustomer',
            'datas' : datas,
        },
        dataType: 'json',
    }).done(function (response) {
        if (response.success == true)
        {
            for (var key in response.response.successBut.error){
                $('#customer-edit-error-' + key).fadeIn();
            }
            if((response.response.successBut.error).length != null){
                sendNotif('greenFont','<i class="fa fa-check"></i> Enregistré');
                closeAllPanel();
            }
            if(response.response.successBut.addressError == 1){
                $('#customer-edit-error-blank-input').fadeIn();
            }
            if(response.response.addressId != null){
                $('#customer-edit-id_address').val(response.response.addressId);
            }           
            
            console.log(response);
            
            PRESTATILL_CAISSE.loader.end();
            
            if(response.request.datas.logg == 'true')
            {
                //$('#customerSearchResultsFilter input').val(response.response.id_customer);
            	//sendCustomerSearch(true);
            
            	// A VALIDER : On passe au client qu'on vient d'enregistrer
				switchTicketCustomer(response.response.customer.id,response.response.customer.id_default_group);
            	_switchCustomer(response.response.customer, false, response.response.additionnal_customer_informations);
            	_retreiveCustomerInformation(response.response.customer.id); 
            	
            	// TEST LAURENT 
            	//_validateCurrentTicket();	
            }
        }
        else
        {
            for (var key in response.errorMsg.error){
                $('#customer-edit-error-' + key).fadeIn();
            }
            //alert('Erreur 000656 : ' + response.errorMsg);
            PRESTATILL_CAISSE.loader.end();
        }
        
    }).fail(function () {
        alert('Erreur 000656 : Une erreur est survenue. Si l\'erreur persiste veuillez contacter l\'adminstrateur du service');
    });
}

function removeOneFromCart(id_product, id_product_attribute, quantity, id_ticket_line) {
	if( typeof(quantity) == 'undefined' || quantity == '' ){
		quantity = 1;
	}
	
	if( typeof(id_ticket_line) == 'undefined' || id_ticket_line == '' ){
		id_ticket_line = false;
	}
	
	if (ACAISSE.ticket.id == null)
	{
		alert('Impossible d\'ajouter un produit. Aucun ticket crée.');
		return;
	}


	if (ACAISSE.cartModificationLocked == true)
	{
		$.ajax({
			type: 'post',
			dataType: 'json',
			data: {
				'action': 'CancelCartFromTicket',
				'id_ticket': ACAISSE.ticket.id,
				'id_product': id_product,
				'id_product_attribute': id_product_attribute,
			}
		}).done(function (response) {
			if (!response.success) {
				alert('Erreur 001556' + response.errorMsg);
				return;
			}
			ACAISSE.ticket = response.response.ticket;
			//remise en place du panneau non encaisse
			$('#orderButtonsPanel').hide();
			$('#preOrderButtonsPanel').show();
			$('#orderButtonsPanel .paymentButton').removeClass('selected');
			$('#orderButtonsPanel .paymentButton').removeClass('selected');
			$('#preOrderButtonsPanel #registerPayment').addClass('disabled');
			ACAISSE.choosenPaymentMode = false;
			ACAISSE.cartModificationLocked = false;
			//puis rappel de l'instruction initiale
			removeOneFromCart(response.request.id_product, response.request.id_product_attribute , response.request.quantity , response.request.id_ticket_line);

		}).fail(function () {
			alert('Error 001557 Remove one qu after cancel real cart failed.');
		});

		return; //on stop ici, la commande sera retenté qu'après
		// annulation du panier et annulation du passage à l'encaissement	
	}
	//sinon on peut réaliser l'opération directement.

	
	if(ACAISSE.mode_debogue === true)
			{console.log(['laurent casse tout2', quantity]);}
	
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'RemoveOneProductToCart',
			'id_ticket': ACAISSE.ticket.id,
			'id_product': id_product,
			'id_attribute': id_product_attribute,
			'id_ticket_line': id_ticket_line,
			'ajaxRequestID': ACAISSE.ajaxCount,
			'listToRefresh': ACAISSE.pageProductBuffer,
			'quantity' : quantity,
		}
	}).done(function (response) {
		if (!response.success) {
			alert('Erreur 000555' + response.errorMsg);
			return;
		}
		response = response.response;
		ACAISSE.ticket = response.ticket;
		
		var product_list = [];
		for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
			product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
			product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
			product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
		}
		if(ACAISSE.mode_debogue === true)
			{console.log(product_list);}
		//var module_obj = {action:'switch_ticket',product_list:product_list};
		
		var module_obj = {
			action:'switch_ticket',
			product_list:product_list,
			ticket:new Ticket(ACAISSE.ticket),
			raw_ticket:ACAISSE.ticket
		};
		
		modules_action(module_obj);
		
		_refreshTicket();
		_doStockLevelUpdateOnPage(response.stocks);
	}).fail(function () {
		alert('Error AddProductToCart failed.');
	});

}



function _startNewPreorder() {
	
	PRESTATILL_CAISSE.loader.start();
	
	if(ACAISSE.mode_debogue === true)
	{
		console.log('_startNewPreorder()');
		console.log(ACAISSE);
		console.log('ici >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
	}
	
	//appel au serveur pour obtenir un nouveau numéro de ticket.
	$.ajax({
		//sans url: ca poste sur le meme controller
		data: {
			'action': 'StartNewPreorder',
			'id_pointshop': ACAISSE.id_pointshop,
			//'id_customer': ACAISSE.customer.id,
			//'id_group': ACAISSE.customer.id_default_group,
		},
		type: 'post',
		dataType: 'json',
	}).done(function (response) {
			
		if (response.success == true)
		{
			if(ACAISSE.mode_debogue === true)
			{
				console.log('resultat requete serveur StartNewPreorder');
				console.log(response);
			}
			ACAISSE.ticket = response.response.ticket;
			
			ACAISSE.payment_parts=[];
			$('#orderButtonsPanel .paymentButton').removeClass('disabled');
			
			ACAISSE.recu = 0;
			ACAISSE.rendu = 0;
			ACAISSE.total_cost = 0;
			ACAISSE.total_cost_ht = 0;
			ACAISSE.rendu = 0;
			_refreshTicket();
			//_refreshTotals();
			_recalcAndRefreshTotals();
			ACAISSE.allActionLocked = false;		  	// on re-autorise l'ensemble des action
			ACAISSE.cartModificationLocked = false;		// on reautorise les action de modification du panier
			_refreshLockedUI();							// on rafraichit l'UI suppression des class "disabled" sur
			//	 les buttons qui était inactif a cause des actions locked
			ACAISSE.choosenPaymentMode = false;			// on déselectionne le mode de paiement choisi
			
			$('#registerPayment').addClass('disabled'); // on grise le bouton de validation d'un encaissement'
			$('.paymentButton').removeClass('selected');// on déselectionne le mode de paiement
			$('#multiplePayment').removeClass('disabled'); // on réactive le btn pour le payment multiple
			
			//@TODO A VALIDEr AVEC NOUVEAU SYSTEM DE PAIEMENT
			/*
			$('#orderButtonsPanel').hide();				// on masque le panneau des moyens de paiement
			$('#preOrderButtonsPanel').show();			// on reaffiche le panneau de validation du ticket
			*/
			
			closeAllPanel();							// on ferme tous les panneaux verticaux
			var ps = acaisse_pointshops.pointshops[ACAISSE.id_pointshop];
			ACAISSE.current_id_page = ps.id_main_page;	// on rebascule sur la page d'accueil du point de vente
			
			if(ACAISSE.mode_debogue === true)
			{console.log(ACAISSE.ticket.ticket_num);}
			
			
			if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 1) {
				$('#openRootPageCaisse').click();
				_gotoProductList();
			} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 2
				&& acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.navigation_by_category == 1) {
				$('#openRootCategorie').click(); 
			} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 0) {
				//$('.switcher').click(); 
				_gotoTicketView();
			}
			
			resetTileHeight();
			
			$('#reductionGlobalOnCart .pastille').remove();
			
			//sendNotif('greenFont','<i class="fa fa-check"></i> Nouveau ticket');
			_switchToDefaultCustomer(); //on retourne au profil par défaut			
			
			_addIdEmployeeToTicket();
			
			//var module_obj = {action:'switch_ticket',product_list:[],id_ticket:ACAISSE.ticket.ticket_num};
			
			var module_obj = {
				action:'switch_ticket',
				id_ticket:ACAISSE.ticket.ticket_num,
				product_list:[],
				ticket:new Ticket(ACAISSE.ticket),
				raw_ticket:ACAISSE.ticket
			};
			
			modules_action(module_obj);
			PRESTATILL_CAISSE.loader.end();
			
			$('.addToBAsketIndicator').remove();
			_refreshVeilleInterface();
			
			// On met à 0 les champs de réduction globale
			$('.selectProductFloatNb .lbabK-inputable, #popup_global_title, #popup_global_textarea').val('');
		}
		else
		{
			alert('Erreur 000453 : ' + response.errorMsg);
		}

	}).fail(function () {
		alert('Erreur 000452 : Une erreur est survenue. Merci de recharger la page.');
	});
}

function _refreshLockedUI() {
	//console.log('@TODO rafraichissement de l\'UI en fonction des "locked"');
	//grisage/dégrisage des buttons
}

function togglePanel(panelId) {
	for (var i = 0; i < ACAISSE_vertical_panels.length; i++)
	{
		var panel = ACAISSE_vertical_panels[i];
		if (panel.id == panelId)
		{
			if ($('#' + panel.id).hasClass('open')) {
				//il faut le fermer
				closeAllPanel();
			}
			else
			{
				//faut fermer l'un des autres ouvrir celui-ci
				//ce que fait la méthode suivante:
				openPanel(panelId);
			}
		}
	}
}

function openPanel(panelId) {
	
	//console.log('call openPanel('+panelId+')');
	
	closeAllPanel(panelId);
	
	$('#footer .navActive,#footer .open').removeClass('navActive').removeClass('open');
    
	for (var i = 0; i < ACAISSE_vertical_panels.length; i++)
	{
		var panel = ACAISSE_vertical_panels[i];
		if (panel.id == panelId)
		{
			$(panel.zone + ',#' + panel.id).addClass('open').addClass('navActive');
			panel.openCallback();
		}
	}
}

function closeAllPanel(exeptionForPanelId) {
	for (var i = 0; i < ACAISSE_vertical_panels.length; i++)
	{
		var panel = ACAISSE_vertical_panels[i];
		if (panel.id != exeptionForPanelId)
		{
			if ($('#' + panel.id).hasClass('open'))
			{
				$(panel.zone + ',#' + panel.id).removeClass('open').removeClass('navActive');
				panel.closeCallback();
			}
		}
	}
}

function _switchToTicket(id_ticket, callback, gototicket_view) {
	$('#preOrderNumber').prepend('<i class="fa fa-spinner fa-spin"></i>');
	$.ajax({
		//sans url: ca poste sur le meme controller
		data: {
			'action': 'ReloadFromTicketID',
			'id_pointshop': ACAISSE.id_pointshop,
			'id_ticket': id_ticket,
			'id_customer': ACAISSE.customer.id,
			'id_group': ACAISSE.customer.id_default_group,
		},
		type: 'post',
		dataType: 'json',
	}).done(function (response) {
		if (response.success)
		{
			ACAISSE.ticket = response.response.ticket;
			ACAISSE.payment_parts = response.response.payment_parts;
			
			$('#reductionGlobalOnCart .pastille').remove();
			
			if (response.response.globalCartRule != undefined) {
				var cartRule = response.response.globalCartRule;
				var text = '';
				
				if (parseFloat(cartRule.reduction_amount)>0) {
					if (parseFloat(cartRule.reduction_amount)==parseInt(cartRule.reduction_amount)) { // on a à faire à un entier
						text = parseInt(cartRule.reduction_amount)+'€';
					} else {
						test = parseFloat(cartRule.reduction_amount).toFixed(2)+'€';
					}
				} else {
					if (parseFloat(cartRule.reduction_percent)==parseInt(cartRule.reduction_percent)) { // on a à faire à un entier
						text = parseInt(cartRule.reduction_percent)+'%';
					} else {
						test = parseFloat(cartRule.reduction_percent).toFixed(2)+'%';
					}
				}
				
				$('#reductionGlobalOnCart').prepend('<span class="pastille big"><span class="label">Remise appliquée :</span> <br />-'+text+'<br /><span class="modify">modifier</span></span>');
			}
			
			if (ACAISSE.payment_parts.length>0) {
	        	$('#orderButtonsPanel .paymentButton').addClass('disabled');
	        } else {
	        	$('#orderButtonsPanel .paymentButton').removeClass('disabled');
			}
			
			var product_list = [];
			for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
				product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
				product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
				product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
			}
			if(ACAISSE.mode_debogue === true)
			{console.log(product_list);}
			
			//var module_obj = {action:'switch_ticket',product_list:product_list};
			
			var module_obj = {
				action:'switch_ticket',
				id_ticket:ACAISSE.ticket.ticket_num,
				product_list:product_list,
				ticket:new Ticket(ACAISSE.ticket),
				raw_ticket:ACAISSE.ticket
			};
			
			modules_action(module_obj);
			
			_refreshTicket();
			if(gototicket_view != undefined && gototicket_view != false)
			{
				_gotoTicketView();
			}

      /**
       * @TODO s'occuper ici de remettre à jour l'etat des bouttons une fois les payements parts rechargé pour ce tocket qui vient d'etre rechargé
       * (anciennement la ligne ci-dessous)
       */
			//recheckIfPaymentCompleted();
			
			if (typeof callback==='function') {
				callback();
			}
			
			ACAISSE.scanbarcodeMode = 'product';
			
			if(ACAISSE.ticket.is_bloqued == 1){  
				
				sendNotif('greenFont','<i class="fa fa-check"></i> Commande N°'+ACAISSE.ticket.id);
			}else if(ACAISSE.ticket.is_devis == 1){
				sendNotif('greenFont','<i class="fa fa-check"></i> Devis N°'+ACAISSE.ticket.num_devis);
			}else {
				sendNotif('greenFont','<i class="fa fa-check"></i> Ticket N°'+ACAISSE.ticket.ticket_num);
			}
			
			$('.ticketBox').hide();
			
		}
		else
		{
			alert('Erreur 000911 : ' + response.errorMsg);
		}
		$('#preOrderNumber i').eq(0).remove();
		$('div.open').click();
	}).fail(function () {
		alert('Erreur 000912 : une erreur est survenue. Si celle-ci persiste, veuillez contacter l\'administrateur du service.');
		$('#preOrderNumber i').eq(0).remove();
		$('div.open').click();
	});
	
	lcd_write_line('',0);
	lcd_write_line('',1);
	lcd_write_line('',2);
	lcd_write_line('',3);
}

function date_heure(id)
{
        date = new Date;
        annee = date.getFullYear();
        moi = date.getMonth();
        mois = new Array('Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre');
        j = date.getDate();
        jour = date.getDay();
        jours = new Array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        resultat = '<span class="dday">'+jours[jour]+' '+j+' '+mois[moi]+' '+annee+'</span><br /><span class="hhour">'+h+':'+m+':'+s+'</span>';
        document.getElementById(id).innerHTML = resultat;
        setTimeout('date_heure("'+id+'");','1000');
        return true;
}

function _refreshFilterPreOrderList(filter) {
	var count = 0;
	var lastID = 0;
	var filter = $('#preOrderListContainer .keyboard span.display').html();

	$('#preOrderListContainer .left ul li').each(function (i, elt) {
		if ((filter == '' || ('' + $(elt).data('num')).indexOf(filter) != -1) && $(elt).data('id') != ACAISSE.ticket.id)
		{
			$(elt).show();
			count++;
			lastID = $(elt).data('id');
		}
		else
		{
			$(elt).hide();
		}
	});
	if (!$('#preOrderListContainer .left ul').hasClass('notLoaded') && count === 1) {
		_switchToTicket(lastID);
	}
}

function _initDefaultProfilesList() {
	var html = '<ul>';
	for (var i = 0; i < acaisse_profilesIDs.length; i++) {
		var customer = acaisse_profiles[acaisse_profilesIDs[i]];
		html += '<li data-id="' + customer.id + '" data-id_group="' + customer.id_default_group + '" class="gender-' + customer.id_gender + '"><span class="name">' + customer.firstname + ' ' + customer.lastname + '<span><span class="email">' + customer.email + '</span><span class="group">' + customer.id_default_group + '</span></li>';
	}
	html += '</ul>';
	$('#defaultCustomersProfils').html(html);
}

function _initMainPage(id_pointshop) {   
	if (id_pointshop == null || typeof (id_pointshop) == 'undefined')
	{
		id_pointshop = ACAISSE.id_pointshop;
	}
	
	// On check si l'employé connecté a le droit de gérer la boutique
	//_checkPointshopPermission();
	var nb_pointshops = $('#pointshop-list select > option').length;
	if(nb_pointshops < 2)
	{
		// Si on a qu'un pointshop, on force le pointshop
		id_pointshop = $('#pointshop-list option:first-child').attr('value');
		// A VOIR POUR SETLE LOCAL STORAGE
	}
	
	var ps = acaisse_pointshops.pointshops[id_pointshop];
	
	ACAISSE.id_pointshop = id_pointshop;
	ACAISSE.till_width = ps.datas.till_width;
	ACAISSE.till_height = ps.datas.till_height;
	
	var pointshopList = '<select>';
    	$('#pointshop-list option[value="'+ACAISSE.id_pointshop+'"]').prop('selected', true);
	
	if(ACAISSE.mode_debogue === true)
			{console.log("TITI >> ");}
	
	ACAISSE.current_id_page = ps.id_main_page;
	ACAISSE.current_pointshop = ps;
	$('div.open').click();
	_reloadPage();
	//Ce point de vente permet la modification de la fiche du client
	if (ACAISSE.current_pointshop.datas.editable_customer == '1'
			&& acaisse_profilesIDs.indexOf('' + ACAISSE.customer.id) == -1
			)
	{
		$('#editUserButton').show();
	}
	else //ou pas
	{
		$('#editUserButton').hide();
	}
	
	//le paiement multiple est-il activé
  if (ACAISSE.current_pointshop.datas.active_multi_payment == '1')
  {
    $('#multiplePayment').show();
  }
  else //ou pas
  {
    $('#multiplePayment').hide();
  }
	
	// Gestion des employés
	_getEmployeesForThisPointshop();

	$('.switcher .addToBAsketIndicator').remove();
	
	_setWidthAndHeight();
	//_updateCustomerEditForm(ACAISSE.customer);
	_startNewPreorder();
	_reloadCashHistory();
	_searchLastCa();
	_createInputGroup();
}

function _pointshopList() {
	if(ACAISSE.mode_debogue === true)
			{console.log(ACAISSE);}
}

function _createInputGroup() {
	$('#customer-edit-group').empty();
	if(ACAISSE.mode_debogue === true)
			{console.log('on passse');}
	for (var key in acaisse_group) {
		var div = $('<div></div>', {
			class: 'checkbox-group',
		});
		
		var checkbox = $('<input>', {
			type: 'checkbox',
			name: 'groupBox[]',
			id: 'customer-edit-group-' + acaisse_group[key].id_group,
			value: acaisse_group[key].id_group,
		});
		var label = $('<label></label>', {
			text: acaisse_group[key].name + ' ',
		});
		//checkbox.attr('type', 'checkbox').attr('name', 'customer-edit-group'+acaisse_group[key].id_group).val(acaisse_group[key].name)
		div.append(checkbox);
		$('#customer-edit-group').append(div);
		checkbox.before(label);
		checkbox.after('<br>');
	}
}

function _reloadCashHistory() {
	$.ajax({
		data: {
			'action': 'CheckPointshopCashHistory',
			'id_pointshop': ACAISSE.id_pointshop,
			'cash_is_open': ACAISSE.cash_open!=undefined?ACAISSE.cash_open:false,
		},
		type: 'post',
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true)
		{
			_refreshCashPanel();
			ACAISSE.cash_history = response.response.cash_history;
			ACAISSE.last_cash_amounts = response.response.last_cash_amounts;
			_refreshCashPanel();
		}
		else
		{
			alert('Erreur 01004 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 01005 : Impossible de communiquer les informations au serveur');
	});
}

function _checkPointshopPermission() {
	$.ajax({
		data: {
			'action': 'CheckPointshopPermission',
			'id_pointshop': ACAISSE.id_pointshop
		},
		type: 'post',
		dataType: 'json',
	}).done(function (response) {
		
		if (response.success == true)
		{
			if(response.response.is_ok == false)
			{
				$('#global-loader').html('<div>Vous n\'êtes pas autorisé à ouvrir cette boutique</div>');
				PRESTATILL_CAISSE.loader.start();
				$('#pointshop-list').css('z-index','5001').css('position','relative');
				$('#middleContainer').display('none');
			}
			else
			{
				$('#middleContainer').display('block');
			}
				
		}
		else
		{
			alert('Erreur 01004 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 01005 : Impossible de communiquer les informations au serveur');
	});
}


function _refreshCashPanel() {
	
	var nb_states = 0;
	if (ACAISSE.cash_history != undefined) {
		nb_states = ACAISSE.cash_history.length;
	}
	
	if((ACAISSE.last_cash_amounts != undefined && ACAISSE.last_cash_amounts.length > 0) && acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos == true)
	{
		var input_cash = ACAISSE.last_cash_amounts[0];
		var form_to_fill = $('#cash_open_form');
		
		if(ACAISSE.cash_open == true)
		{
			form_to_fill = $('#cash_close_form');
		}
		//console.log(form_to_fill);
		$.each(input_cash, function(index, value) {
			form_to_fill.find('#'+index).val(value); 
		});
		
		$('#tabPrepareCash').hide();
		
		$('.totalCash').text(function () {
            var money = numeral(
                    $(this).prev('td').children().data('money')
                    );
            var result = money.multiply(
                    $(this).prev('td').children().val()
                    );
            return result.format();
        });

        //calcul du total en euro à l'ouverture, mvt et fermeture
        var totalCash = numeral(0);
        $('li.open .totalCash').each(function () {
            var cash = numeral().unformat($(this).text());
            totalCash = totalCash.add(cash);
        });
        
        var caisse_state = 'fermeture de caisse.';
        if(ACAISSE.cash_open == true)
		{
			caisse_state = 'ouverture de caisse. <br /><b>Attention, cela n\'inclue pas le chiffre d\'affaire du jour.</b>';
		}
        
        $('#cash_close_form #lastAmountTitle th, #cash_open_form #lastAmountTitle th').html('Les montants ci-dessous correspondent à ceux que vous avez saisi lors de la dernière '+caisse_state);
		
	}
	
	// @todo : remplacer par type 5 pour la preparation
	//quand on ferme la caisse
	
	if(ACAISSE.mode_debogue === true)
			{console.log('_refreshCashPanel() avec nb_states = '+nb_states);}
	
	console.log(nb_states);		
	if(nb_states == 0)
	{
		$('#tabPrepareCash, #tabMvtCash, #tabCloseCash').addClass('unselectable');
		$('#tabOpenCash').removeClass('unselectable');
		$('#tabOpenCash').trigger('click');
		$('.cash_close_panel, .cash_prepare_panel').hide();
		$('.cash_open_panel').show();
		$('.preselectMoney').text('');
		$('#lastCa').hide();        	
	}
	else if (ACAISSE.cash_history[nb_states - 1].type == 2) //la caisse et fermé
	{
		ACAISSE.cash_open = false;
		$('.cash_open_panel, .cash_close_panel').hide();
		$('.cash_prepare_panel').show();
		
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos == true)
		{
			$('#tabMvtCash, #tabCloseCash').addClass('unselectable');
			$('#tabOpenCash, #tabPrintZList').removeClass('unselectable');
			$('#tabOpenCash').trigger('click');
			$('.cash_open_panel').show();
		}
		else
		{
			$('#tabOpenCash, #tabMvtCash, #tabCloseCash').addClass('unselectable');
			$('#tabPrepareCash, #tabPrintZList').removeClass('unselectable');
			$('#tabPrepareCash').trigger('click');
		}
		
		//on reporte le montant total à la fermeture dans la preparation
		$('#totalCashOnClose').val(function () {
			var result = nb_states==0?0:numeral(ACAISSE.cash_history[nb_states - 1].cash);
			result = result.divide(100);
			return result.format();
		});

		//on reporte le nombre de pièce therorique
		var totalCashTheoretical = numeral(0);
		$('[data-theoretical-cash]').each(function () {
			var dataTheoretical = ACAISSE.current_pointshop.datas[$(this).data('theoretical-cash')];
			$(this).val(dataTheoretical);
			totalCashTheoretical = totalCashTheoretical.add(dataTheoretical * $(this).closest('tr').data('money'));
		});
		$('#totalCashTheoretical').val(totalCashTheoretical.format());
		//console.log(totalCashTheoretical);

		//on reporte le nombre de pièce à la fermeture dans la preparation
		$('[data-cash-on-close]').each(function () {
			$(this).val(nb_states==0?0:ACAISSE.cash_history[nb_states - 1][$(this).data('cash-on-close')]);
		});

		//remplissage des boutons de présselection 
		$('tr[data-money]').each(function () {
			var result = parseInt($('[data-theoretical-cash]', this).val()) - parseInt($('[data-cash-on-close]', this).val());
			if (result < 0) {
				//result = result.replace('-', '');
				$('.takeOffMoney .preselectMoney', this).text(Math.abs(result));
			}
			else if (result > 0) {
				$('.requestMoney .preselectMoney', this).text(result);
			}
			//console.log(result);
		});
		//nombre total de pièce retiré/ajouté
		$('li.open .totalLineMoney input').each(function () {
			$(this).val(parseInt($('[data-cash-on-close]', $(this).closest('tr')).val()));
			var theoreticalCash = parseInt($('[data-theoretical-cash]', $(this).closest('tr')).val());
			_setProgressPrepare($(this), theoreticalCash);
		});

		//console.log('//@TODO : quand on ferme la caisse fermée');
		/*$('#payByCash').hide();
		$('#payByNoCash').show();*/
	}
	//quand on ouvre la caisse
	else if (ACAISSE.cash_history[nb_states - 1].type == 1 || ACAISSE.cash_history[nb_states - 1].type == 3) //pas status fermer en dernier
			//donc elle est ouverte ou encore ouverte suite à mouvement en cours de service
			{
				ACAISSE.cash_open = true;

				$('#tabOpenCash,#tabPrepareCash, #tabPrintZList').addClass('unselectable');
				$('#tabMvtCash, #tabCloseCash').removeClass('unselectable');
				$('#tabMvtCash').trigger('click');

				$('.cash_close_panel').show();
				$('.cash_prepare_panel').hide();
				$('#payByCash').show();
				$('#payByNoCash').hide();
				//$('#lastCa').show();
				
			}
	//quand on termine la preparation de la caisse
	else if (ACAISSE.cash_history[nb_states - 1].type == 5 || ACAISSE.cash_history[nb_states - 1].type == 6) {
		
		$('#tabPrepareCash, #tabMvtCash, #tabCloseCash').addClass('unselectable');
		
		$('#tabOpenCash,#tabPrintZList').removeClass('unselectable');
		$('#tabOpenCash').trigger('click');
		$('.cash_close_panel, .cash_prepare_panel').hide();
		$('.cash_open_panel').show();
		$('.preselectMoney').text('');
		$('#lastCa').hide();
	}
	
	$('.cash_action_right textarea').val('');
	
	_refreshVeilleInterface();
}

function _sendCashState(action, datas, noMessage) {
	noMessage = ((typeof noMessage === "undefined") || false) ? confirm('Vérifiez à deux fois le décompte des pièces et billets. Vous ne pourrez pas modifier ce décompte une fois la caisse ouverte.') : true;
	if (noMessage) {
		$('.submitActionPanel input[type="submit"]').prepend('<i class="fa fa-spinner fa-spin"></i>');
		$.ajax({
			data: {
				'action': action,
				'id_pointshop': ACAISSE.id_pointshop,
				'count': datas,
			},
			type: 'post',
			dataType: 'json',
		}).done(function (response) {            	
			if (response.success == true)
			{
				if (action == 'ClosePointshop') {
					//printZList();
					alert('Vous pouvez à présent imprimer la bande Z du jour. Le rapport financier de la journée sera disponible en fin de journée.')
					
				}
				
				if (action == 'OpenPointshop') {
					_startNewPreorder();				
				}
				
				response = response.response;

				ACAISSE.cash_history = response.cash_history;

				$('#tab0 input,#tab1 input,#tab2 input,#tab3 input').not('.submitActionPanel input').val('');
				//_refreshCashPanel();
				$('.submitActionPanel input[type="submit"] i.fa-spinner').remove();
				//console.log(ACAISSE);
			}
			else
			{
				alert(['Erreur 01001 : ' + response.errorMsg,response]);
			}
			_reloadCashHistory();
		}).fail(function () {
			alert(['Erreur 01002 : Impossible de communiquer les informations au serveur',datas]);
			_reloadCashHistory();
		});
	}
}

function _searchLastCa(){
	$.ajax({
		type: 'post',
		data: {
			'action': 'SearchLastCa',
			'id_pointshop': ACAISSE.id_pointshop,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true){
			ACAISSE.lastCA = response.response.lastCa;
		}
		else{
			alert('Erreur 300121 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 300122 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
	});
}

function _getEmployeesForThisPointshop(){
	$.ajax({
		type: 'post',
		data: {
			'action': 'EmployeesForThisPointshop',
			'id_pointshop': ACAISSE.id_pointshop,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true){
			ACAISSE.employeesPointshop = response.response.employeesPointshop;
			
			var html = '';
			var emp_active = null;
			for(var i = 0; i < ACAISSE.employeesPointshop.length; i++){
				if(ACAISSE.employeesPointshop[i].name != null)
				{
					var firstLetter = ACAISSE.employeesPointshop[i].name;
					var avatar = firstLetter.substring(0,1);
					
			        html +='<div class="emp emp_'+ ACAISSE.employeesPointshop[i].id_employee +'" data-id_emp="'+ ACAISSE.employeesPointshop[i].id_employee +'">';
						html +='<i class="avatar">'+ avatar +'</i>';
						html +='<span data-id_employee="'+ ACAISSE.employeesPointshop[i].id_employee +'" class="firstname">'+ ACAISSE.employeesPointshop[i].name+'</span>';
					html +='</div>';
					
					// On initialise le ticket avec l'id de l'employé par défaut qui est connecté
					if(ACAISSE.employeesPointshop[i].id_employee == ACAISSE.employee.id)
					{
						//console.log(ACAISSE.employeesPointshop[i].id_employee);
						emp_active = ACAISSE.employeesPointshop[i].id_employee;
					}
				}
		    }
			$('#veilleEmployee').append(html);
			
			//$('#veilleEmployee .emp_'+ emp_active).trigger('click');
			
		}
		else{
			alert('Erreur 000002L =P : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 300122 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
	});
}

function _addIdEmployeeToTicket(){
	
	var id_employee = $('#showEmployee').data('id_employee');
	
	if(id_employee == undefined)
	{
		id_employee = ACAISSE.employee.id;
		$('#veilleEmployee .emp_'+ id_employee).trigger('click');
	}	
	
	$.ajax({
		type: 'post',
		data: {
			'action': 'addIdEmployeeToTicket',
			'id_ticket': ACAISSE.ticket.id,
			'id_employee': id_employee,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true){
			
		}
		else{
			alert('Erreur 000003L =P : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 300122 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
	});
}

function _refreshLastCa(elem){
    //console.log(ACAISSE.lastCA);
    $(elem).empty();
    
    
    var $strLastCa = $('<span/>');
    
    for(var key in ACAISSE.lastCA) {
        if(ACAISSE.lastCA[key] != null) {
            if( ACAISSE.lastCA[key].hasOwnProperty('name')) {
                var $lastCaType = $('<span/>');
                $lastCaType.append(ACAISSE.lastCA[key].name + ' : ' + ACAISSE.lastCA[key].total + '€');
                $strLastCa.append($lastCaType);
            }  
        }
    }
    var $lastCaTotal = $('<span/>').append('Total CA : ' + ACAISSE.lastCA.total_ca+ '€');
    //var $lastCaTotalCash = $('<span/>').append('Total CA cash : ' + ACAISSE.lastCA.total_ca_cash+ '€');
    $strLastCa.append($lastCaTotal);//.append($lastCaTotalCash);
    $(elem).append($strLastCa);
}

function _refreshVeilleInterface()
{
	// CAISSE
    if(ACAISSE.cash_open == true)
    {
    	$('#veille .open_caisse').addClass('disabled');
    	$('#preOrderButtonsPanel .validTicket').removeClass('disabled');
    	sendNotif('greenFont','<i class="fa fa-check"></i> Connecté');
    	
    	// Indiquer la derniere oueerture de caisse #lastOpenCaisse (vert ou rouge)
	    var todayDayTicket = ACAISSE.ticket.day;
	    var todayDateTicket = ACAISSE.ticket.date_add;
	    
	    if(todayDayTicket != undefined)
	    {
	    	var formatedDay = new Date(todayDayTicket).toLocaleDateString();;
	    	
	    	if(todayDayTicket.substring(0,10) != todayDateTicket.substring(0,10))
	    	{
	    		$('#lastOpenCaisse').addClass('alert').html('Attention : la caisse est ouverte depuis le '+formatedDay);
	    	}
	    	else
	    	{
	    		$('#lastOpenCaisse').removeClass('alert').html('Caisse ouverte');
	    	}
	    }
    }
    else
    {
  		$('#preOrderButtonsPanel .validTicket').addClass('disabled');
  		// On vérifie si la caisse est ouverte
  		if(ACAISSE.cash_open != undefined)
    		sendNotif('greenFont','Bonjour, veuillez ouvrir la caisse afin de réaliser une vente.');
    
    	$('#lastOpenCaisse').addClass('alert').html('CAISSE FERMEE');
    }
    	
    $('#tabMvtCash').hasClass('unselectable') == true?$('#veille .caisse_movements').addClass('disabled'):$('#veille .caisse_movements').removeClass('disabled')
    $('#tabCloseCash').hasClass('unselectable') == true?$('#veille .close_caisse').addClass('disabled'):$('#veille .close_caisse').removeClass('disabled');
    $('#tabPrepareCash').hasClass('unselectable') == true?$('#veille .prepare_caisse').addClass('disabled'):$('#veille .prepare_caisse').removeClass('disabled');
    
    if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos == true)
    	$('#veille .prepare_caisse').hide();
    // CLIENTS
    
    // COMPTABILITE
    ACAISSE.cash_open==false?$('#veille .print_z').removeClass('disabled'):$('#veille .print_z').addClass('disabled');
    ACAISSE.cash_open==false?$('#veille .show_cash').addClass('disabled'):$('#veille .show_cash').removeClass('disabled');
    
    
    // ON VERIFIE LES PERMISSIONS DU PROFIL EMPLOYE
    if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_search_product == 0)
    {
    	$('#veille .search_product').remove();
    }
    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_add_product  == 0) 
    {
    	$('#veille .create_product').remove();
    }
    
    // PRODUCTS
    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 0)
    {
    	$('#veille .view_catalogue').remove();
    }
}

function switchTicketCustomer(id_customer,id_group)
{
	$('#userName').addClass('loading');
       
   $.ajax({
       type: 'post',
       data: {
           'action': 'SwitchTicketCustomer',
           'id_ticket': ACAISSE.ticket.id,
           'id_new_customer': id_customer,
           'id_new_group': id_group,
           'ajaxRequestID': ++ACAISSE.ajaxCount,
       },
       dataType: 'json',
   }).done(function (response) {
       if (response.success == true)
       {
           var customer = response.response.new_customer;
           customer.groups = response.response.groups;
           customer.adress = response.response.address;
           ACAISSE.ticket = response.response.ticket;
           _switchCustomer(response.response.new_customer, false, response.response.additionnal_customer_informations);
       }
       else
       {
           alert('Erreur 000655 : ' + response.errorMsg);
       }
       $('#userName').removeClass('loading');
   }).fail(function () {
       $('#userName').removeClass('loading');
       alert('Erreur 000654 : Une erreur est survenue. Si l\'erreur persiste veuillez contacter l\'adminstrateur du service');
   });
}

$(function() {
	
    $('#fullscreen').on('click tap', function () { // plein ecran

		if (!document.fullscreenElement && !document.webkitFullscreenElement) {
		    if (document.documentElement.requestFullscreen) {
		      document.documentElement.requestFullscreen();
		      if (ACAISSE.till_width!=screen.width || ACAISSE.till_height!=screen.height) {
				ACAISSE.till_width = screen.width;
				ACAISSE.till_height = screen.height;
			  }
		    } else if (document.documentElement.webkitRequestFullscreen) {
		      document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		      if (ACAISSE.till_width!=screen.width || ACAISSE.till_height!=screen.height) {
				ACAISSE.till_width = screen.width;
				ACAISSE.till_height = screen.height;
			  }
		    }
		} else {
		    if (document.cancelFullScreen) {
		      document.cancelFullScreen();
		      ACAISSE.till_width = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_width;
		      ACAISSE.till_height = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_heighth;
		    } else if (document.webkitCancelFullScreen) {
		      document.webkitCancelFullScreen();
		      ACAISSE.till_width = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_width;
		      ACAISSE.till_height = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_height;
		    }
		}
        
    });
    
    // PopupProduct Calto
    $('#showHideCalc').on('click tap', function (){
    	if($(this).hasClass('hide')){
	    	$('#productInfos #popupProduct, #productInfos .title, #features, #stocksPopup').animate({
										    width: "100%"
										  }, 200);
			$(this).removeClass('hide').addClass('show');							  
    	}
    	else
    	{
	    	$('#productInfos #popupProduct, #productInfos .title, #features, #stocksPopup').animate({
										    width: "+=262"
										  }, 200);
			$(this).addClass('hide').removeClass('show');
    	}
    });

    $('#calc-keyboard span').on('click tap', function (e) { // lors du clic sur les touches du clavier numerique
        if ($(this).parents('.disabled').length>0) {
        	return false;
        }
        
        e.preventDefault();
        e.stopPropagation();

        var c = $(this).html();
        
        _cashAddChar(c);
    });
    
    // 3 states rotation checkbox
    $('body').on('click tap','#customer-edit-group input[type=checkbox]',function() {
    	if (this.readOnly) {this.checked=this.readOnly=false;}
  		else if (!this.checked) {this.readOnly=this.indeterminate=true;}
    });
    
    // 2 states checkbox "classique"
    $('body').on('click tap','#customer-edit-more input[type=checkbox]',function() {
  		if (!this.checked) {this.checked=false} else {this.checked}
    });
    
    $('#customer-edit-save').on('click tap', function () { // lorsqu'on clic sur le boutton save lors de l'edition d'un client
        _editCustomer(false);
    });
    
     $('#customer-edit-save-and-log').on('click tap', function () { // lorsqu'on clic sur le boutton save lors de l'edition d'un client
        _editCustomer(true);
    });
    

    //switching customer / changement de client
    $('#customerListContainer').delegate('#customerSearchResults li,#defaultCustomersProfils li', 'click tap', function (e) {
        switchTicketCustomer($(this).data('id'),$(this).data('id_group'));
    });

    
    // lorsqu'on change  horizontalement de vu entre la liste des produits et le ticket
    $('#productList .switcher').on('click tap', function () {
        if ($(this).hasClass('show-ticket')) {
            _gotoTicketView();
        } else {
            _gotoProductList();
            resetTileHeight();
        }
    });
    
    /**quand on clique longtemps sur un produit**/                        
    longPressOnProduct('#productList','td.cellType2,td.cellType3'); 
    longPressOnProduct('#searchProducts','td.cellType2,td.cellType3'); 
    longPressOnProduct('#ticketView', 'tr.line');
    
    //clic sur cellule de type page
    $('#productList').delegate('td.cellType1', 'click tap', function () {
        loadPage($(this).data('idobject'), null);
    });
    
    //annulation d'un ticket pour conversion vers commande prestashop
    $('#preOrderButtonsPanel .cancelTicket').on('click tap', function () {
        _cancelTicket();
    });

    //passage au ticket suivant
    $('.newTicket').on('click tap', function () {
    	 // On déroule les actions possibles
        $('.ticketBox').slideToggle();
    });
    
    
    // evenement des bouttons du ticket
    $('#ticketView .lines').delegate('.quantityPlus', 'click tap', function () {
        //faut recupere id_product et id_product_attribute dans le tr parent direct.
        var $parent = $(this).parent('td').parent('tr');
        var id_ticket_line = $parent.data('idticketline');   
        var id_product = $parent.data('id_product');   
        var id_product_attribute = $parent.data('id_product_attribute');     
        addToCart(id_product, id_product_attribute, 1, id_ticket_line);
    }).delegate('.quantityMinus', 'click tap', function () {
        //faut recupere id_product et id_product_attribute dans le tr parent direct.
        var $parent = $(this).parent('td').parent('tr');
        var id_ticket_line = $parent.data('idticketline');
        var id_product = $parent.data('id_product');   
        var id_product_attribute = $parent.data('id_product_attribute');  
        removeOneFromCart(id_product, id_product_attribute, 1, id_ticket_line); //@TODO : utiliser le id_ticket_line
    }).delegate('.quantityRemove', 'click tap', function () {
        //faut recupere id_product et id_product_attribute dans le tr parent direct.
        var $parent = $(this).parent('tr');
        var id_ticket_line = $parent.data('idticketline');
        var id_product = $parent.data('id_product');   
        var id_product_attribute = $parent.data('id_product_attribute');  
        trashLineFromCart(id_product, id_product_attribute, id_ticket_line);
    });

    

    /**********************
     *VERTICAL PANELS open/close action 
     */
    ACAISSE_vertical_panels = [
        {
            id: 'customerListContainer',
            zone: '#userName,#changeUserButton,#customerListContainer .horizontalCloseOverlay',
            openCallback: function () {
                ACAISSE.scanbarcodeMode = 'customer';
                $('#changeUserButton i').removeClass('fa-angle-down').addClass('fa-angle-up');
                //on vide le filtre et on refraichis la liste en consequence
                $('#customerSearchResultsFilter input').val('');
                sendCustomerSearch();
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
                $('#changeUserButton i').removeClass('fa-angle-up').addClass('fa-angle-down');
                $('#customerSearchResultsFilter input').focusout();
            }
        },
        {
            id: 'preOrderListContainer',
            zone: '#preOrderNumber,#preOrderListContainer .horizontalCloseOverlay,#changeOrderButton',
            openCallback: function () {
                PRESTATILL_CAISSE.preorder.openTab();
                $('#preorder_option_block .selected').trigger('click');
            },
            closeCallback: function () {            	
                PRESTATILL_CAISSE.preorder.closeTab();
            }
        },
        {
            id: 'specialsContainer',
            zone: '#specials,#specialsContainer .horizontalCloseOverlay',
            openCallback: function () {
                ACAISSE.scanbarcodeMode = 'action';
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
            }
        },
        {
            id: 'userEditContainer',
            zone: '#editUserButton,#userEditContainer .horizontalCloseOverlay',
            openCallback: function () {
                ACAISSE.scanbarcodeMode = 'editCustomer';
                
                $('#discountContainer .discount .minimum_amount').each(function() {
	               if (this.innerHTML>$('#totalCost').html()) {
	               		$(this).addClass('notEnough');
	               } else {
	               		$(this).removeClass('notEnough');
	               }
               });
               
               $('#discountContainer .discount button:disabled').each(function() {
               		if ($(this).parent().find('.qte').html()!='0') {
               			this.disabled = false;
           			}
               });
               
               loadDiscount();
               loadOrders(0);
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
            }
        }
    ];

    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_search_ticket == 1) {
    	ACAISSE_vertical_panels.push({
            id: 'printTickets',
            zone: '#lastPrints, #printTickets .horizontalCloseOverlay',
            openCallback: function () {
                ACAISSE.scanbarcodeMode = 'ticket';
                
                document.querySelector('#searchTicketsDate').value=getEngDate(new Date(), false);
                document.querySelector('#searchTicketsDate').onchange();
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
            }
      });
    } else {
    	$('#lastPrints').css('display','none');
    }
    
	/*
	 *  Activer / Désactiver l'ajout de produit à la volée
	 */
   if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_add_product  == 0) {
    	$('#addNewProduct').css('display','none');
    }
    else
    {
    	$('#addNewProduct').css('display','block');
    }
    
	/*
	 *  Activer / Désactiver le paiement partiel
	 */   
	 if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.active_partial_paiement  == 0) {
    	$('#paymentView .partial-validation').css('display','none');
    }
    else
    {
    	$('#paymentView .partial-validation').css('display','block');
    }
    
    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_search_product == 1) {
        ACAISSE_vertical_panels.push({
            id: 'searchProducts',
            zone: '#productResearch, #searchProducts .horizontalCloseOverlay',
            openCallback: function () {
                //resetTileHeight();
                _gotoProductList();
                $('#productSearchResultsFilter input').val('');
                setTimeout(function() {
	                loadProductSearchList();
	                resetTileHeight();
	            },500);
                ACAISSE.scanbarcodeMode = 'productSearch';
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
            }
        });
    } else {
    	$('#productResearch').css('display','none');
    }
    
    
    
    //panel d'ajout de nouveau produits
    if (true || acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.active_add_product_in_till == 1) {
        ACAISSE_vertical_panels.push({
            id: 'addNewProductContainer',
            zone: '#addNewProduct, #addNewProductContainer .horizontalCloseOverlay,#addNewProductContainer .closeBuPanel',
            openCallback: function () {
            	console.log('open add_new_prodict panel');
                ACAISSE.scanbarcodeMode = 'newProduct';
                resetTileHeight();
                
                addProductController.resetForm();
                
                setTimeout(function() {
	                resetTileHeight();
	            },500);
                
            },
            closeCallback: function () {
            	console.log('close add_new_prodict panel');
                ACAISSE.scanbarcodeMode = 'product';
            }
        });
    } else {
    	$('#addNewProduct').css('display','none');
    }
    
    

    //INIT PANEL EVENTS
    for (var i = 0; i < ACAISSE_vertical_panels.length; i++)
    {
        var panel = ACAISSE_vertical_panels[i];
        var instruction = '$(\'' + panel.zone + '\').on(\'click tap\',function(){ togglePanel(\'' + panel.id + '\');});';
        eval(instruction);
    }

    /********** END panel open/close actions *******/

    //reload preOrder : rechargement d'une autre pré commande
    $('#preOrderListContainer .left ul').delegate('li', 'click tap', function () {
        _switchToTicket($(this).data('id'));
        _gotoTicketView();
        
        // TEST LAURENT
        //_validateCurrentTicket();
    });

    //gestion du clavier numérique de sélection d'une commande
    $('#preOrderListContainer .keyboard span').on('click tap', function () {
    	if (!$(this).hasClass('display'))
        {
            if ($(this).hasClass('reset'))
            {
                $('#preOrderListContainer .keyboard span.display').html('');
                _refreshFilterPreOrderList('');
            }
            else if ($(this).hasClass('backspace'))
            {
                if ($('#preOrderListContainer .keyboard span.display').html().length > 0)
                {
                    $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html().substring(0, ($('#preOrderListContainer .keyboard span.display').html().length - 1)));
                }
            }
            else
            {
                $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html() + $(this).html());
            }
            _refreshFilterPreOrderList($('#preOrderListContainer .keyboard span.display').html());
        }
    });
    
    //gestion du clavier numérique de sélection d'une commande
    $('#printTickets .keyboard span').on('click tap', function () {
    	if (!$(this).hasClass('display'))
        {
            if ($(this).hasClass('reset'))
            {
                $('#printTickets .keyboard span.display').html('');
            }
            else if ($(this).hasClass('backspace'))
            {
                if ($('#printTickets .keyboard span.display').html().length > 0)
                {
                    $('#printTickets .keyboard span.display').html($('#printTickets .keyboard span.display').html().substring(0, ($('#printTickets .keyboard span.display').html().length - 1)));
                }
            }
            else
            {
                $('#printTickets .keyboard span.display').html($('#printTickets .keyboard span.display').html() + $(this).html());
            }
            //TODO 1 : recharger les tickets enf fonction de ce qui est entré
            getNumTickets($('#printTickets .keyboard span.display').html());
        }
    });

    $('.paymentButton').on('click tap', function () { // lorqu'on veut encaisser
        if ($(this).hasClass('disabled') || $(this).parents('.disabled').length>0) {
        	return false;
        }
        
        var mode = $(this).attr('id');
        
        //@todo rendre l'ouverture de caisse obligatoire
        if (mode=='payByCash' && !ACAISSE.cash_open) {
        	alert("Veuillez ouvrir la caisse pour pouvoir encaisser en liquide.");
        	return false;
        }

        //on désélectionne le mode actuelle
        if (ACAISSE.choosenPaymentMode != false) {
            $('#' + ACAISSE.choosenPaymentMode).removeClass('selected');
        }
        if (ACAISSE.choosenPaymentMode == mode) //on désactive le paiement actuellement sélectionné
        {
            $('#registerPayment').addClass('disabled');
            ACAISSE.choosenPaymentMode = false;
        }
        else //on change de mode de paiement
        {
            ACAISSE.choosenPaymentMode = mode;
            $('#' + ACAISSE.choosenPaymentMode).addClass('selected');


            if (ACAISSE.choosenPaymentMode == 'payByCash')
            {
                $('#registerPayment').addClass('disabled'); //en liquide il faut d'abord saisir le montant versée en liquide
                $('#cashMessage').html('');
                $('#cashReceived').html('-');
                _refreshCashOverdue(0);

                ACAISSE.scanbarcodeMode = 'cash';
            }
            else
            {
                $('#registerPayment').removeClass('disabled');
                ACAISSE.scanbarcodeMode = 'product';
            }
        }

    });


    /****************************************************************/   
   $('#pointshop-list').on('change', 'select', function (e) {
        //_initMainPage(this.value);
        localStorage.setItem('id_pointshop',this.value);
        PRESTATILL_CAISSE.loader.start();
        // PROVISOIRE : on recharge la page au changement de pointshop pour recharger caisse.js et les différentes permissions
        // (il faudrait que ce soit autre part que dans le JS je pense, genre direct PHP ou TPL)
        window.location.reload();
    });
    /****************************************************************/

    

    $('#cash_open_form').submit(function (e) {
        e.preventDefault();
        if(ACAISSE.mode_debogue === true)
			{console.log('open cash');}
        _sendCashState('OpenPointshop', $(this).serializeArray());
    });
    $('#cash_close_form').submit(function (e) {
        e.preventDefault();
        if(ACAISSE.mode_debogue === true)
			{console.log('close cash');}
        _sendCashState('ClosePointshop', $(this).serializeArray());
        
        console.log(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos);
        if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos == true){
        	
        	
        }
        
    });
    $('#cash_mvt_form').submit(function (e) {
        e.preventDefault();
        if(ACAISSE.mode_debogue === true)
			{console.log('mvt cash');}
        _sendCashState('SaveCashMvt', $(this).serializeArray());
    });
    $('#cash_prepare_form').submit(function (e) {
        e.preventDefault();
        if(ACAISSE.mode_debogue === true)
			{console.log('prepare cash');}
        //var fieldsRequest = [];
        var fieldsRequest = [];
        $('.requestMoney input', this).each(function () {
            fieldsRequest.push({
                name: $(this).attr('name'),
                value: $(this).val(),
            });
        });
        var fieldsTakeOff = [];
        $('.takeOffMoney input', this).each(function () {
            fieldsTakeOff.push({
                name: $(this).attr('name'),
                value: $(this).val(),
            });
        });

        fieldsRequest.push({
            name: $('textarea', this).attr('name'),
            value: $('textarea', this).val(),
        });
        fieldsTakeOff.push({
            name: $('textarea', this).attr('name'),
            value: $('textarea', this).val(),
        });
        if(ACAISSE.mode_debogue === true)
			{console.log(fieldsRequest);}

        _sendCashState('PreparePointShopTakeOff', fieldsTakeOff);
        _sendCashState('PreparePointShopRequest', fieldsRequest, true);
    });

	$('#specials').on('click tap',function(){
        //_searchLastCa();
        //_refreshLastCa('#specialsContainer #lastCa');
        _refreshCashPanel();
    });

    $('#cash_prepare_form .preselectMoney').on('click tap', function () {
        $(this).next('input').val($(this).text()).trigger('change');
    });

    $('#logo img').on('click tap', function () {
        /* loadPage(ACAISSE.current_pointshop.id_main_page, ACAISSE.id_pointshop);
        $('#footer .navActive').removeClass('navActive');
        $('#openRootPageCaisse').addClass('navActive'); */
       _refreshVeilleInterface();
       $('#veille').fadeToggle();
       $('#veilleEmployee').hide();
    });
    
    // pour fermer le popup quand on clic sur l'overlay
    $('body').delegate('.overlay', 'click tap', function() {
    	$(this).parent().find('.contentOverlay').remove(); // on degage les declinaisons
    	$(this).remove();// on  degage l'overlay
    });
    
    // pour fermer le popupNumeric quand on clic sur l'overlay
    $('body').delegate('.overlayNumeric', 'click tap', function() {
    	hidePopupAndKeyboard();
		$(this).remove();// on  degage l'overlay
    });
    
    // pour fermer le popup quand on clic sur fermer
    $('#productList').delegate('.closeOverlay', 'click tap', function() {
    	$('#productList .overlay').click();
    });
    
    // pour fermer le popup quand on clic sur fermer
    $('#ticketView').delegate('.closeOverlay', 'click tap', function() {
    	$('#ticketView .overlay').click();
    });
    
    // lorsqu'on clic sur un produit du popup
    $('#productList').delegate('.contentOverlay tr[class!="closeOverlay"]', 'click tap', function () {
        addToCart($(this).data('idobject'), $(this).data('idobject2'));
        if (params.declinaisonPopup.closeAfterSelect) {
        	$('.contentOverlay .closeOverlay').click();
        }
    });
    
    //on rajoute le clavier numerique
    longPressOnProduct('#productList', '.contentOverlay tr[class!="closeOverlay"]');
    
    //ajout produit au panier
    //clic sur cellule de type produit/Déclinaison
    $('#productList .content, #productResearchList').delegate('td.cellType2,td.cellType3', 'click tap', function () {
        if (!_loadProductFromId($(this).data('idobject')).disponibility_by_shop[ACAISSE.id_pointshop]) {
        	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i> Produit désactivé dans ce point de vente');
        } else if (_loadProductFromId($(this).data('idobject')).has_attributes && $(this).data('idobject2')=="") {
        	closeAllPanel();
        	showPopupDeclinaison($(this).data('idobject'));
		} else {
			var qte = 1;
			
			var unitId = params.units.from.indexOf(_loadProductFromId($(this).data('idobject')).unity);
			
			if (unitId==-1) {
				console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
				params.units.from.push(p.unity);
				params.units.to.push('');
				params.units.scale.push(1);
				unitId = params.units.from.indexOf(_loadProductFromId($(this).data('idobject')).unity);
				//throw "Erreur d'unité : unité inconnue : ";
			}
			
			qte*=params.units.scale[unitId];
			closeAllPanel();
			addToCart($(this).data('idobject'), $(this).data('idobject2'), qte);
		}
    });
    
    //clic sur une cellule de type categorie
    $('#productList .content').delegate('td.cellType4', 'click tap', function () {
        
	     if (ACAISSE.previousPageId.length>=32) {
	     	ACAISSE.previousPageId.shift();
	     }
        
        if ($('.navActive').attr('id')=='openRootCategorie') {
        	ACAISSE.previousPageId.push({id:$(this).data('idobject'),type:'categorie'});
        } else if (ACAISSE.previousPageId.length==0) {
        	ACAISSE.previousPageId.push({id:ACAISSE.current_id_page,type:'pageCaisse'});
        	ACAISSE.previousPageId.push({id:$(this).data('idobject'),type:'categorie'});
        } else {
        	ACAISSE.previousPageId.push({id:$(this).data('idobject'),type:'categorie'});
        }
        
        generateCategoriePageHTML($(this).data('idobject'));
        
    });
    
    $('#productList .content').delegate('td.cellTypeReturn', 'click tap', function () {
         
         if (ACAISSE.previousPageId.length>1) {
         	ACAISSE.previousPageId.pop();
         	var lastPage = ACAISSE.previousPageId[ACAISSE.previousPageId.length-1];
         	if (lastPage.type=='categorie') {
         		generateCategoriePageHTML(lastPage.id);
         	} else {
         		loadPage(lastPage.id);
         		$('#footer .navActive,#footer .open').removeClass('navActive').removeClass('open');
       			$('#openRootPageCaisse').addClass('navActive').addClass('open');
         	}
         }
         
    });
    
    $('#openRootCategorie').on('click tap', function() {
    	if (!$('#productList .switcher').hasClass('show-ticket')) {
            _gotoProductList();
       }
    	
    	ACAISSE.previousPageId = [{id:acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_root_category,type:'categorie'}];
        generateCategoriePageHTML(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_root_category);
        closeAllPanel();
        $('#footer .navActive,#footer .open').removeClass('open').removeClass('navActive');
        $(this).addClass('navActive').addClass('open');
    });
    
    $('#openRootPageCaisse').on('click tap', function() {
    	if (!$('#productList .switcher').hasClass('show-ticket')) {
            _gotoProductList();
        }
    	
    	ACAISSE.previousPageId = [];
    	
    	loadPage(ACAISSE.current_pointshop.id_main_page);
    	
    	closeAllPanel();
        $('#footer .navActive,#footer .open').removeClass('navActive').removeClass('open');
        $(this).addClass('navActive').addClass('open');
    });
    
    // lorsqu'on clic sur un ticket de caisse
    $('#printTickets').delegate('.left table tr','click tap',function() {
		$('#printTickets .display').html($(this).data('num'));
		getNumTickets($(this).data('num'));
	});
	
	//lorsqu'on veut imprimer un ticket de caisse
	$('#printTickets').delegate('.left .ticket .print','click tap',function() {
		$('#printTickets .left .ticket.toPrint .kdoprint').show(); //DEPRECIER A PARTIR DE 2.3
		$('#printTickets .left .ticket.toPrint .kdotitle').hide(); //DEPRECIER A PARTIR DE 2.3
		printTicket($('#printTickets .left .ticket.toPrint').html(), 1, 'duplicata'); //duplicata == la class CSS qui va aller s'ajouter sur #ticketPrint
		printQueue();
	});
	
	//lorsqu'on veut imprimer un ticket de caisse
	$('#printTickets').delegate('.left .ticket .kdo','click tap',function() {
		$('#printTickets .left .ticket.toPrint .kdoprint').hide(); //DEPRECIER A PARTIR DE 2.3
		$('#printTickets .left .ticket.toPrint .kdotitle').show(); //DEPRECIER A PARTIR DE 2.3
		printTicket($('#printTickets .left .ticket.toPrint').html(), 1, 'gift'); //duplicata == la class CSS qui va aller s'ajouter sur #ticketPrint
		printQueue();
		
	});
	
	$('#notif').delegate('td','click tap',function(e) {
		e.stopPropagation();
		e.preventDefault();
		$('#notif').removeClass('active');
		$('#notif table')
		.fadeOut()
		.queue(function (next) { 
		   	$(this)
		   		.find('td')
				.removeClass()
				.html('');
			next();
		});
	});
	
	$('#tabPrintZList').on('click tap',function(e) {
		
		if ($(this).hasClass('unselectable')) 
		{
			e.preventDefault();
		}
		else
		{
			printZList();
		}
	});
	
	$('#printTickets').on('click tap', '.rePrintZ', function(e) {
		
		if ($(this).hasClass('disabled')) 
		{
			e.preventDefault();
		}
		else
		{
			var dateen = $('#searchTicketsDate').val();
			console.log(dateen);
			printZList(dateen);
		}
	});
	
	window.addEventListener('online',  function() {
		sendNotif('greenFont','<i class="fa fa-check"></i>Connecté');
	});
	
	window.addEventListener('offline', function() {
		sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Déconnecté');
	});
	
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='0') {
		$('.lines th.img').remove();
	}
	
	// action sur les bouttons de paiements multiple (moyen de paiements)
	$('body').on('click tap','#multi-payment-mode-list .button',function(elt) {
		
		if($('#multi-payments-popup').prop('disabled') == true) {
     		alert('Veuillez attendre la finalisation de l\'encaissement précédent');		 
		} else if (!$(this).hasClass('disabled')) {
			if ($(this).data('moduleName') == undefined) { return false;}
  		
	  		var amount = $('input#multi-payment-amount').val();
	  		var moduleName = this.dataset.moduleName;
	  		var paymentModuleLabel = this.textContent.trim();
	  		
	  		registerPaymentPart(amount, moduleName, paymentModuleLabel);
	  		
	  		$('input#multi-payment-amount').val(0);
		}
	});
	
	$('#userEditContainer .tab-bar .tab-button').on('click tap',function() {
		$('#userEditContainer .content > div').css('display','none');
		$('#userEditContainer #'+$(this).data('pageId')).css('display','block');
	});
	
	var lastUserOrderScroll = new Date().getTime();
	$('#userEditContainer #pageUser3').scroll(function() {
		
		if (new Date().getTime()-lastUserOrderScroll<1000 || $('#pageUser3 table').data('nbOrder')==0 || this.scrollHeight-this.scrollTop-$(this).height()>200) {return false;}
		
		lastUserOrderScroll = new Date().getTime();
		
		var nb_order = $('#pageUser3 table').data('nb_order');
		
		if (nb_order==undefined || nb_order==0) {
			nb_order=50;
		}
		
    	$('#pageUser3 table').data('nb_order',0);
		
		$('#pageUser3').append('<div><i class="fa fa-spinner fa-spin"></i></div>');
    
	    $.ajax({
	        data: {
	            'action': 'LoadCustomerOrders',
	            'id_customer': ACAISSE.customer.id,
	            'from_nb_order':nb_order,
	            'id_pointshop': ACAISSE.id_pointshop,
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        var response = response.response;
	        if (response.success != false) {
	            var orders=response.orders;
	            
	            var html='';
	            
				for (var id in orders) {
	            	html += '<tr>'
	            		html += '<td>'+orders[id].id_order+'</td>';
	            		if(acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined)
						{
							html += '<td><span class="pointshop_name">' + acaisse_pointshops.pointshops[orders[id].id_pointshop].name + '</span></td>';
						}
						else
						{
							html += '<td><span class="pointshop_name">En ligne</span></td>';
						}
	            		html += '<td><span class="date">'+Date.toString(orders[id].date_add)+'</span></td>';
	            		html += '<td><span class="state">'+orders[id].order_state+'</span></td>';
	            		html += '<td><span class="paid '+ ((parseFloat(formatPrice(orders[id].total_paid))>parseFloat(formatPrice(orders[id].total_paid_real)))?'notAllPaid':'')+' '+((parseFloat(formatPrice(orders[id].total_paid))<parseFloat(formatPrice(orders[id].total_paid_real)))?'tooMuch':'')+'">'+formatPrice(orders[id].total_paid)+'€</span></td>';
	        			html += '<td><span class="paid_real '+ ((parseFloat(formatPrice(orders[id].total_paid))>parseFloat(formatPrice(orders[id].total_paid_real)))?'notAllPaid':'')+' '+((parseFloat(formatPrice(orders[id].total_paid))<parseFloat(formatPrice(orders[id].total_paid_real)))?'tooMuch':'')+'">'+formatPrice(orders[id].total_paid_real)+'€</span></td>';
	        			
	        			html +='<td class="actions">';
	        			if (parseFloat(orders[id].total_paid_real) < parseFloat(orders[id].total_paid) && response.id_order_state_cancelled != orders[id].id_order_state && acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
							html += '<button onclick="payRestOfOrder(' + orders[id].id_order + ');"><i class="fa fa-money" aria-hidden="true"></i> <span class="btLabel">PAYER</span></button>';
						} else if (parseFloat(orders[id].total_paid_real) > parseFloat(orders[id].total_paid) && response.id_order_state_cancelled != orders[id].id_order_state) {
							/**@todo : Se démerder pour degager le surplut et donner une reduc ?*/
							//html += '<button disabled="disabled" style="opacity:0.2;"><i class="fa fa-undo"></i> <span class="btLabel">AVOIR</span></button>';
						}
						if(acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined)
						{
							html += '<button class="seeTicket" data-id="' + orders[id].id_ticket + '"><i class="fa fa-ticket"></i> <span class="btLabel">TICKET</span></button>';
						}
						else
						{
							html += '<button disabled="disabled" style="opacity:0.2" data-id="0"><i class="fa fa-ticket"></i> <span class="btLabel">TICKET</span></button>';
						}
						if (params.orders.acaisse_invoice_is_active == 1 && orders[id].valid == 1 && orders[id].invoice_number > 0) {
							html += '<button  class="seeInvoice" data-id="' + orders[id].id_order + '"><i class="fa fa-file-text"></i> <span class="btLabel">FACTURE</span></button>';
						}
						else
						{
							html += '<button disabled="disabled" style="opacity:0.2"  class="seeInvoice" data-id="0"><i class="fa fa-file-text"></i> <span class="btLabel">FACTURE</span></button>';
						}
						html += '<button  class="openOrder" data-id="' + orders[id].id_order + '"><i class="fa fa-eye"></i> <span class="btLabel">VOIR</span></button>';
						html += '</td></tr>';
	            }
	            
	            $('#pageUser3 table').append(html);
	            $('#pageUser3 .fa-spinner').remove();
	            
	            $('#pageUser3 table').data('nb_order',(parseInt(nb_order)+50));
	        } else {
	        	alert('Erreur k_00011 : impossible d\'obtenir la liste des commandes du client. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	        }
	    }).fail(function () {
	        alert('Erreur k00011 : impossible d\'obtenir la liste des commandes du client. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	    });
	});
	
	$('#userEditContainer ul .open').click();
	
	$('.ticketBox').hide();

	
	//INIT UI
    $('#productList .content').append('<br /><i style="font-size:300%;" class="fa fa-spinner fa-spin"></i>');
    
    setTimeout(function () {
        _initDefaultProfilesList();
        _switchCustomer(acaisse_profiles[acaisse_profilesIDs[0]], true, []);
        _initMainPage();
        _gotoProductList();
        //@TODO reactive le bloc ci dessous en production
		
        $('*').attr('unselectable', 'on')
                .css({'-moz-user-select': '-moz-none',
                    '-moz-user-select':'none',
                    '-o-user-select': 'none',
                    '-khtml-user-select': 'none', // you could also put this in a class
                    '-webkit-user-select': 'none', // and add the CSS class here instead 
                    '-ms-user-select': 'none',
                    'user-select': 'none'
                }).bind('selectstart', function (e) {
            e.preventDefault();
            return false;
        });
    }, 1);
    
    
    
    //on active le troisieme stade de l'input pour la légende du groupe par défaut du client
    var editCustomerDefaultGroupLegend = $('#editCustomerDefaultGroupLegend')[0];
    editCustomerDefaultGroupLegend.readOnly=editCustomerDefaultGroupLegend.indeterminate=true;
    
    $('#printTicket').css('font-size',localStorage.getItem('ticket_font_size'));
    
    // ECRAN D'ACCUEIL ET VEILLE DE PRESTATILL
    $('#veille .closeBt').on('click tap', function(){
    	$('#veille').fadeOut();
    });
    
    // Interface d'identification 
   	$('#veilleEmployee').on('click tap', '.emp', function(){
   		
   		$('#showEmployee i').css('backgroundColor', $(this).find('i').css('backgroundColor'));
   		$('#showEmployee i').text($(this).find('i').html());
   		$('#showEmployee ').data('id_employee', $(this).find('span').data('id_employee'));
   		
   		if(ACAISSE.veilleAllwaysClosed == 1)
   		{
   			$('#veille, #veilleEmployee').fadeOut();
   		}
   		else
   		{
   			$('#veilleEmployee').fadeOut();
   		}
   		
   		ACAISSE.veilleAllwaysClosed = 1;
   		_addIdEmployeeToTicket();
   		
   		// On met à jour les tickets en attente
   		$('#preorder_option_block .selected').trigger('click');
   		$('#preorder_option_block .item[data-value=0] span').html($('#showEmployee').html());
   	
   	});
    
    $('#veilleContent .firstname').html(ACAISSE.employee.firstname);
    
    $('#veille li li').on('click tap', function(){
    	var action = $(this).attr('class');
    	
    	if($(this).hasClass('disabled') == false)
    	{
    		$('#veille').fadeOut();
    		
	    	setTimeout(function(){
	    		
	    		closeAllPanel();
	    		
			    switch (action)
		    	{
		    		// CAISSE
		    		case 'open_caisse':
		    		$('#specials').trigger('click');
		    		break;
		    		
		    		case 'prepare_caisse':
		    		if($('#specials').hasClass('navActive') == false)
		    			$('#specials').trigger('click');
		    		$('#tabPrepareCash').trigger('click');
		    		break;
		    		
		    		case 'caisse_movements':
		    		if($('#specials').hasClass('navActive') == false)
		    			$('#specials').trigger('click');
		    		$('#tabMvtCash').trigger('click');
		    		break;
		    		
		    		case 'close_caisse':
		    		if($('#specials').hasClass('navActive') == false)
		    			$('#specials').trigger('click');		    			
		    		$('#tabCloseCash').trigger('click');
		    		break;
		    		
		    		// CUSTOMER
		    		case 'create_customer':
		    		if(acaisse_profilesIDs.indexOf(""+ACAISSE.customer.id) > -1)
					{
						if($('#editUserButton').hasClass('navActive') == false)
							$('#editUserButton').trigger('click').delay(500);
		    			$('li[data-page-id=pageUser1]').trigger('click');
		    			sendNotif('greenFont','Il suffit de saisir un nom, prénom et mail pour créer un nouveau client.');
					}
					else
					{
						sendNotif('redFont','Veuillez sélectionner le client par défaut');
					}
		    		break;
		    		
		    		case 'find_customer':
						if($('#userName').hasClass('navActive') == false)
							$('#userName').trigger('click');
		    			sendNotif('greenFont','Tapez nom, prénom ou ID du client ou douchez sa carte de fidélité pour l\'identifier.');
		    		break;
		    		
		    		case 'see_orders':
						if($('#editUserButton').hasClass('navActive') == false)
							$('#editUserButton').trigger('click');
		    			$('li[data-page-id=pageUser3]').trigger('click');
		    		break;
		    		
		    		case 'see_vouchers':
						if($('#editUserButton').hasClass('navActive') == false)
							$('#editUserButton').trigger('click');
		    			$('li[data-page-id=pageUser2]').trigger('click');
		    		break;
		    		
		    		// PRODUCTS
		    		case 'create_product':
		    		$('#addNewProduct').trigger('click');
		    		break;
		    		
		    		case 'search_product':
		    		$('#productResearch').trigger('click');
		    		break;
		    		
		    		case 'view_catalogue':
		    		$('#openRootCategorie').trigger('click');
		    		break;
		    		
		    		// COMPTA
		    		case 'print_z':
		    		$('#specials').trigger('click');
		    		$('#tabPrintZList').trigger('click');
		    		break;
		    		
		    		case 'find_z':
		    		$('#lastPrints').trigger('click');
		    		sendNotif('greenFont','Utilisez le calendrier pour indiquer le jour pour lequel vous souhaitez réimprimer la bande Z.');
		    		break;
		    		
		    		case 'show_cash':
					if($('#editUserButton').hasClass('navActive') == false)
	    				$('#specials').trigger('click');
		    			$('li[data-module-name=prestatillexportcompta]').trigger('click');
		    		break;
		    		
		    		// TICKET
		    		case 'new_ticket':
		    		if($('.switcher').hasClass('show-product-list') == false)
		    			$('#productList').trigger('click');
		    			$('.newTicket').trigger('click');
		    		break;
		    		
		    		case 'find_ticket':
		    			$('#lastPrints').trigger('click');
		    		break;
		    		
		    		case 'show_open_tickets':
		    			$('#preOrderNumber').trigger('click');
		    		break;
		    		
		    	}
	    		
	    	},500);
	    	
	    	
    	}
    	
    });
    
    setTimeout(function(){ $('#veilleBox').fadeIn(); }, 1000);
    
    date_heure('hourOfTheDay');
    
    /* SHOW EMPLOYEE */
   	$('#showPointshops').on('click tap', function(){
   		$(this).toggleClass('active');
   		$('#pointshop-list').slideToggle("fast");
   	});
   	
   	$('#showEmployee').on('click tap', function(){
   		$('#veille').fadeIn();
   		$('#veilleEmployee').fadeIn();
   	});
    
});