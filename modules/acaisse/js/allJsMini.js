
function CaissePayment(caisse_ref)
{
	
	var currency = '€';
	
	var caisse = caisse_ref;
	var DEBUG_LEVEL = 0;
	
	
	var number_formatter = new Intl.NumberFormat('fr-FR',{minimumFractionDigits:2});
	
	var current_payment_module_name = null;
	
	var tot_a_payer = 0; 
	var tot_deja_paye = 0;
	var extra_datas = {}; //données complémentaire qui seront transmise au serveur
	
	var diff_str = parseFloat(tot_a_payer-payment_amount-tot_deja_paye);
	diff_str = ""+diff_str;
	
	var dif_str_res = diff_str.replace(" ","");
	var diff = 0+dif_str_res;
	
	var formatted_diff = 0;
	var formatted_payment_amount = 0;
	var payment_amount = 0;
	var repaid = 0; //rendu
	var formatted_repaid = 0;
	
	var real_payment_amount = 0;
	var formatted_real_payment_amount = 0;
	
	
	this.getPaymentAmount = function() { return payment_amount; };
	
	
	/**
	 * Liste des modules de paiement 
	 */
	this.modules = [];
	
	var getCurrentPaymentModule = function() {
		
		for(i in caisse.payment.modules)
		{
			var module = caisse.payment.modules[i];
			if(module.constructor.name == current_payment_module_name)
			{
				return module;
			}
		}		
		return {};
	};
	
	this.getPaymentModuleByName = function(payment_module_name) {
		
		
		for(i in caisse.payment.modules)
		{
			var module = caisse.payment.modules[i];
			if(module.constructor.name == payment_module_name)
			{
				return module;
			}
		}		
		return false;
	};
	
	this.getCurrentPaymentModule = function() {
		return getCurrentPaymentModule();
	};
	
	this.dispatchScanBarCode = function(e) {
		var module = this.getCurrentPaymentModule();
		if(module.captureScanner != undefined)
		{
			if(DEBUG_LEVEL > 5)
			{
				console.log('module '+module.constructor.name+' capture scan event');
			}
			module.captureScanner(e);
		}		
	};
	
	
	//Handlers
	this.choosePaymentHandler = function(e) {		
		if($(this).hasClass('selected'))
		{
			caisse.payment.changePaymentMode(null);
			$('.prestatill_payment_button').removeClass('selected');
			$('.payment_actions').hide();
			//$('#paymentView .help').hide();.show();
		}
		else
		{
			caisse.payment.changePaymentMode($(this).data('module-name'));	
			$('.prestatill_payment_button').removeClass('selected');
			$('.payment_actions').show();
			$(this).addClass('selected');
			//$('#paymentView .help').hide();.hide();
			
			var module = getCurrentPaymentModule();
			
			//console.log(module);
			
			if(module.init)
			{
				module.init();
			}			
		}		
	};
	
	this.getTotal = function() {
		
		var tot_to_pay = $('#totalCost').html();
		var tot_to_pay_res = tot_to_pay.replace(" ","");
		
		return parseFloat('0'+tot_to_pay_res);
		return ACAISSE.cart_tot; //@TODO à réécrire une fois les totaux stocké dans PRESTATILL_CAISSE
	};
	
	this.initOnOpenPaymentInterface = function() {			
		PRESTATILL_CAISSE.loader.hideActions();
		this.changePaymentMode(null);
		this.changePaymentAmount();
		$('.prestatill_payment_button.selected').removeClass('selected');
		$('.payment_actions').hide();
		$('#launchRegisterPayment').show(); // A vider
	    	
		//rechargement des paiments déjà effectué lorsque l'on recharge une commande qu'il faut solder	
		
		total_deja_paye = 0;
					
		$('#details-paiements').html('');
		for(var i = 0 ; i < ACAISSE.payment_parts.length ; i++)
		{
			var html_new_tr = _renderPaymentPartLine(ACAISSE.payment_parts[i]);
			total_deja_paye += parseFloat(ACAISSE.payment_parts[i]['payment_amount']);			
		    $('#details-paiements').append(html_new_tr);
        }
        total_deja_paye = " "+total_deja_paye;
        var tot_deja_paye_res = total_deja_paye.replace(" ","");	
		
		this.changePaymentAmount(0.0);
	    $('.payment_amount').html('0.00');
	    $('#real_payment_amount').html('0.00'+currency); //a prirori c'est plus utilisé
	    $('#keepToHaveToPay').html(formatPrice(this.getTotal()-tot_deja_paye_res)+currency);
	    $('.solde_restant').html(formatPrice(this.getTotal() - tot_deja_paye_res));
	    //$('#paymentView tr.solde-restant').removeClass('success').addClass('warning');
	    $('.display_back').addClass('not_enough').removeClass('good');
		
		if(ACAISSE.payment_parts.length > 0)
		{
			//on est entrain de payé un relicat d'une commande			
	    	$('#paymentView .cancel').addClass('disabled');
	    	if(ACAISSE.ticket.invoice_total_products_wt > 0)
	    	{
				$('#keepToHaveToPay').html(formatPrice(ACAISSE.ticket.order_total_paid-tot_deja_paye_res)+currency);
	    		$('.solde_restant').html(formatPrice(ACAISSE.ticket.order_total_paid - tot_deja_paye_res));
	    		//$('#totalCost').html(formatPrice(ACAISSE.ticket.order_total_paid+'<small>TTC</small>')+currency);	    	
	    	}
		}
		else
		{
			//on va effectuer le tout premier paiement			
	    	$('#paymentView .cancel').removeClass('disabled');
		}
		//totalCost
		_refreshButtonStates(parseFloat('0'+$('#keepToHaveToPay').html()));
						
	};
	
	
	//init des handlers de payement (commun)
	$('#paymentView').on('click tap', '.prestatill_payment_button', this.choosePaymentHandler);
	
	
	// Méthode publique lié au(x) paiement(s)
	this.changePaymentMode = function(new_payment_module_name) {
		if(DEBUG_LEVEL>2)
		{
			console.log('Calling CaissePayment::changePaymentMode(new_payment_module_name)');
		}		
		
		//on masque l'ensemble des modules
		$('#paymentView .col-interface .prestatill_payment_interface,#paymentView .col-interface .help').hide();
		
		current_payment_module_name = new_payment_module_name;
		//console.log(current_payment_module_name);		
		var module = getCurrentPaymentModule();	
		//si on a déselectionné le moyen de paiement
		if(current_payment_module_name === null)
		{			
			$('#paymentView .col-interface help').show();
				
			this.changePaymentAmount(0,module.with_repaid);
		}
		else ////on charge l'interface de saisi du module
		{
			$('#paymentView .col-interface .prestatill_payment_interface[data-module-name="'+current_payment_module_name+'"]').show();			
		}
		
		caisse.executeHook('paiement_change',{
			payment_module_name : new_payment_module_name,
			payment_amount : 0, //@TODO récupéré le montant
			total_amount : 0, //@TODO récupéré le montant
			formated_total_amount : 0, //@TODO récupéré le montant
			formated_payment_amount : 0, //@TODO récupéré le montant
		});
		
	};
	
	//Méthode publique a appelé en cours de saisie du montant reçu
	this.changePaymentAmount = function(amount, with_repaid){
		
		//avec gestion d'un rendu monnaie?
		if(with_repaid == undefined)
		{
			with_repaid = false;
		}
		
		if(DEBUG_LEVEL>2)
		{
			console.log('Calling CaissePayment::changePaymentAmount(amount:='+amount+') ');
		}
		
		/**
		 * calcule entre la difference du total à payer et du montant du paiement
		 */		
		payment_amount = amount;
		tot_a_payer = parseFloat(this.getTotal()); 
		
		if($('#keepToHaveToPay').html() == '')
		{
			$('#keepToHaveToPay').html(tot_a_payer);
		}
		
		var tot_dp = $('#keepToHaveToPay').html();
		var tot_dp_res = tot_dp.replace(" ","");
		
		tot_deja_paye = tot_a_payer-parseFloat('0'+tot_dp_res);
		diff = tot_a_payer-payment_amount-tot_deja_paye;
		extra_datas = extra_datas;
		
		if(with_repaid === true && diff < 0)
		{
			repaid = -diff;
			diff = 0;			 
			real_payment_amount = payment_amount - repaid;
		}
		else
		{
			repaid = 0;
			real_payment_amount = Math.min(payment_amount,tot_a_payer); //@TODO attention ici j'empeche le trop perçu... c'est juste?
		}
		
		formatted_repaid = number_formatter.format(repaid);		
		formatted_diff = number_formatter.format(diff<0?-diff:diff);
		formatted_payment_amount = number_formatter.format(amount);
		formatted_real_payment_amount = number_formatter.format(real_payment_amount);
		
		this.refreshAmountDisplay();			
		
		caisse.executeHook('paiement_change',{
			payment_module_name : current_payment_module_name,
			payment_amount : payment_amount,
			formated_payment_amount : formatted_payment_amount,
			total_amount : formatted_payment_amount, //@TODO récupéré le montant
			formated_total_amount : 0, //@TODO récupéré le montant
		});
	};	
	
	
	this.refreshAmountDisplay = function() {
		$('.solde_restant').html(diff==0 && repaid == 0?'':repaid>0?formatted_repaid:formatted_diff);
		$('.payment_amount').html(formatted_payment_amount);		
		$('.display_back').removeClass('too_much not_enough good repaid').addClass(diff==0 && repaid == 0?'good':(diff>0?'not_enough':(repaid>0?'repaid too_much':'too_much')));
		$('#real_payment_amount').html(formatted_real_payment_amount + currency);	
				
	};
	
	var _renderPaymentPartLine = function(payment_part) {
				
          var html_new_tr = '<tr>';
            html_new_tr += '<td>'+payment_part['label'].toLowerCase()+'</td>';
            html_new_tr += '<td>'+formatPrice(payment_part['payment_amount'])+'€</td>';
            //html_new_tr += '<td><i class="fa fa-trash"></i></td>';
          html_new_tr += '</tr>';
          
          return html_new_tr;
	};
	
	this.registerRefund = function() {
		
		var module = getCurrentPaymentModule();
		var extra_datas = {};
		var datas = {
			'action' : 'RegisterRefund',					//nom de l'action côrté serveur
			'id_ticket' : PRESTATILL_CAISSE.return.ticket.id,				//id du ticket de caisse
			'id_pointshop' : ACAISSE.id_pointshop,			//id du point de vente prestatill
			'id_service' : ACAISSE.id_service,			//id du point de vente prestatill
															//a DEJA était converti en panier Pretashop à ce stade)
			'id_customer' : ACAISSE.customer.id,			//id du client
			'ajaxRequestID' : ++ACAISSE.ajaxCount,			//id de transaction ajax
			'payment_module_name' : current_payment_module_name,	//nom du module de paiement utilisé
			'payment_part_amount' : formatPrice(real_payment_amount),			//montant payé
			'label' : current_payment_module_name,			//label du module de paiement, à vérifier SI utile
			'extra_datas' : extra_datas,
		};
		
		//PRESTATILL_CAISSE.loader.start();
		
		PRESTATILL_CAISSE.return.registerRefund(datas);
		
	};	
	
	
	
	//Méthode publique lié à l'enregistrement d'un paiement
	this.registerPayment = function() {
		if(DEBUG_LEVEL>2)
		{
			console.log('Calling CaissePayment::registerPayment(paymentModuleName, amount) @TODO');
		}		
		
		var module = getCurrentPaymentModule();
		var extra_datas = {};
		if(module.getExtraDatas != undefined)
		{
			extra_datas = module.getExtraDatas();
		}
		
		if ($('#popup_global_title').val() != '')
		{
			extra_datas['ticket_title'] = $('#popup_global_title').val();
		}
		
		if ($('#popup_global_textarea').val() != '')
		{
			extra_datas['ticket_textarea'] = $('#popup_global_textarea').val();
		}
		
		var datas = {
			'action' : 'RegisterPayment',					//nom de l'action côrté serveur
			'id_ticket' : ACAISSE.ticket.id,				//id du ticket de caisse
			'id_pointshop' : ACAISSE.id_pointshop,			//id du point de vente prestatill
			'id_service' : ACAISSE.id_service,			//id du point de vente prestatill
			'ps_id_cart' : ACAISSE.ticket.ps_id_cart,		//id du panier Prestashop (cela suppose que le ticket				
															//a DEJA était converti en panier Pretashop à ce stade)
			'id_customer' : ACAISSE.customer.id,			//id du client
			'id_group' : ACAISSE.customer.id_default_group,				//group par defaut du client
			'id_force_group' : ACAISSE.id_force_group,		//groupe forcé
			'ajaxRequestID' : ++ACAISSE.ajaxCount,			//id de transaction ajax
			'id_order_state' : 10,		//id_order_state //@TODO à dynamiser en fonction du module !!!! ou alors à gérer côté SERVEUR
			//'cash_received' : 'xxx',		//DEPRECATED
			'payment_module_name' : current_payment_module_name,	//nom du module de paiement utilisé
			'payment_part_amount' : formatPrice(real_payment_amount),			//montant payé
			'label' : current_payment_module_name,			//label du module de paiement, à vérifier SI utile
			'extra_datas' : extra_datas,
		};
		
		PRESTATILL_CAISSE.loader.start();
		
		$.ajax({
			type: 'post',
			data: datas,
			dataType: 'json',
		}).done(function (response) {
			
			
			if(response.success)
			{
			
				var request = response.request;
				var response = response.response;				
				
				/////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////
				//on ajoute juste le dernier venant d'etre insere
		          var payment_part = response.payment_part;
		          var html_new_tr = _renderPaymentPartLine(payment_part);
		          
		          ACAISSE.payment_parts.push({
		          	payment_module_name:payment_part['module_name'],
		          	payment_part_amount:payment_part['payment_amount'],
		          	label:response.payment_part['label']
		          });
		          
		          $('#details-paiements').append(html_new_tr);	          
		          
		          $('#keepToHaveToPay').html(formatPrice(response.restant_a_payer));
		          
		          //on désactive le boutton d'annulation, maintenant c'est trop tard
		          $('#paymentView .cancel').addClass('disabled');
		          
		          _refreshButtonStates(response.restant_a_payer);
		          
		          //on click sur le module de paiement qui a était utilisé
		          $('.prestatill_payment_button.selected').click();	                 
		          
		          ACAISSE.lastCA = response.lastCa;
		          _refreshLastCa();	          
			  	  PRESTATILL_CAISSE.loader.end();			
			}
			else
			{
				PRESTATILL_CAISSE.loader.end();
				alert('Error 0103245 : ' + response.errorMsg);
			}		
			
			return;
			/*
			   //Pour démarre automatiquement un nouveau ticket
				_startNewPreorder();
				
			*/
		}).fail(function (response) {
			alert('Erreur 010122 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
			PRESTATILL_CAISSE.loader.end();
		});
		
		caisse.executeHook('paiement_validation',{
			//@TODO
		});
	};
	
	var _refreshButtonStates = function(restant_a_payer) {
		  //si le paiement est maintenant complet
          if(restant_a_payer == 0 && $('#details-paiements > tr').length > 0)
          {
            $('#paymentView .partial-validation').addClass('disabled');
            $('#paymentView .validation').removeClass('disabled');
            
            //console.log('tout est payé cool');
            $('#paymentView tr.solde-restant').removeClass('warning').addClass('success');
            $('#launchRegisterPayment').hide();
            //$('#paymentView .help').hide();.hide();
            
            // Ici on choisi si on souhaite un ticket de caisse ou si on l'envoi par mail
            //alert('ici ?');
            getTicketCaisse();
            
          }
          else if($('#details-paiements > tr').length == 0)
          {
          	   $('#paymentView .partial-validation').addClass('disabled');
          	   $('#paymentView .validation').addClass('disabled');
          }
          else  //sinon
          {
            $('#paymentView .validation').addClass('disabled');
            $('#paymentView .partial-validation').removeClass('disabled');
            $('#launchRegisterPayment').show();
          }	   
		          
	};
	
	$('#emptyPopup').on('click tap', '#ticketInfos .devisButtons .print_to_ticket', function (e){
		e.preventDefault();
		
		printQueue();
		$('#multi-payments-popup').remove();
	    $('.strict-overlay').remove();
	    
	    $('#emptyPopup .closePopup').click();
	    //on demande un passage au ticket suivant
	    _startNewPreorder();
	});
	/*
	 * Si on ferme la popup à l'aide de la croix sans choisir d'option
	 */
	$('#emptyPopup').on('click tap', '.closePopup', function (){
		
		if($('#emptyPopup').find('#ticketInfos').length > 0)
		{
			ACAISSE_print_queue.shift();
    		_startNewPreorder();
		}
    });
	
	$('#emptyPopup').on('click tap', '#ticketInfos .devisButtons .send', function (){
    	$('#ticketInfos .overflow, #ticketInfos .send_form').slideDown();
    	$('.devisButtons').slideUp();
    });
    
    $('#emptyPopup').on('click tap', '#ticketInfos .devisButtons .no_ticket', function (){
    	$('#emptyPopup .closePopup').click();
    	ACAISSE_print_queue.shift();
    	_startNewPreorder();
    });
    
    $('#emptyPopup').on('click tap', '#ticketInfos #abort_send', function (e){
    	e.preventDefault();
    	$('#ticketInfos .overflow, #ticketInfos .send_form').slideUp();
    	$('.devisButtons').slideDown();
    });
    
    $('#emptyPopup').on('click tap', '#ticketInfos #confirm_send', function (e){
    	e.preventDefault();
    	if(validate($('#send_form_email').val()) == true)
    	{
    		$('#emptyPopup .closePopup').click();
    		$(this).prop('disabled','disabled');
    		printQueue(true);
    		PRESTATILL_CAISSE.loader.start();
    	}
    	else
    	{
    		alert('Veuillez renseigner une adresse email correcte svp.');
    	}
    	
    });
	
}
function CaisseLoader(caisse_ref)
{
	var caisse = caisse_ref;
	this.is_loading = false;
		
	/**
	 * Pour afficher le voile de chargement global 
	 */
	this.start = function() {
		$('#global-loader').css('display','table');
	};
	
	/**
	 * Pour masquer le message de chargement 
	 */
	this.end = function() {
		$('#global-loader').css('display','none');
	};
	
	
	/**
	 * Afficher les actions (top et bottom) 
	 */
	this.showActions = function() {
		this.showTopActions();
		this.showBottomActions();
	};
	
	/**
	 *  Masquer le voile suppérieur
	 */
	this.hideActions = function() {
		this.hideTopActions();
		this.hideBottomActions();
	};	
	
	/**
	 *  Afficher le voile suppérieur
	 */	
	this.hideTopActions = function() {
		$('#top-loader').css('display','table');
	};
	
	/**
	 *  Masquer le voile suppérieur
	 */
	this.showTopActions = function() {
		$('#top-loader').css('display','none');
	};
	
	/**
	 * Afficher un voile en bas
	 */
	this.hideBottomActions = function() {
		$('#bottom-loader').css('display','table');
	};
	
	/**
	 * Pour masquer le voile du bas 
	 */
	this.showBottomActions = function() {
		$('#bottom-loader').css('display','none');
	};
	
}
function Caisse()
{	
	
	//propriété publique
	this.version_name = 'V2.4.1';
	var _id_pointshop = parseInt(localStorage.getItem('id_pointshop'));
	var _pointshop = acaisse_pointshops.pointshops[_id_pointshop];
	
	//si pas de pointshop alors on initialise en prenant le premier
	if(_pointshop == undefined)
	{
		for(var i in acaisse_pointshops.pointshops)
		{
			_id_pointshop = i;
			_pointshop = acaisse_pointshops.pointshops[_id_pointshop];
			break;
		}
		if(_pointshop == undefined)
		{
			alert('Erreur OG90012 : Aucune configuration de caisse en cache');
		}
	}
	
	//propriétées privées
	var DEBUG_LEVEL = 0;
	var hooks = {	
		/*
		 * lorsque le moyen, ou le montant de paiement saisie est modifié
		 */
		paiement_change : [
			function(params){
				if(DEBUG_LEVEL > 5)
				{
					console.log(['debug hook paiement_change',params]);
				}
			}],
			 
			
		
		/*
		 * lorsqu'un paiement simple est validé
		 */
		paiement_validation : [],
		/**
		 * lorsqu'un paiement multiple est validé  
		 */
		paiement_mulitple_validation : [],
		
		/**
		 * lorsque le contenu du ticket/panier change OU son total
		 */
		cart_change : [
			function(params){
				console.log(['debug hook cart_change',params]);
			}
		],
		
	};
	
	//méthodes publiques
	
	// Enregistrement d'une nouvelle fonction sur un hook
	this.registerHook = function(name,func)
	{
		if(!hooks[name]) {
			hooks[name]=[];
		}
	  hooks[name].push(func);
	};	
	
	// Méthode permettant l'éxécution d'un hook avec passage des arguments
	this.executeHook = function(name,params) {
		if(hooks[name]) {
			return hooks[name].map(func=>func(params)).join('');
		};
	};
	
	//méthode privée
	var _toto = function() {
		
	};
	
	//Initialisation de la caisse
	var _init = function(){
		_initPointshop();
	};
	
	this.reInit = function() {
		_init();
	};
	
	var _initPointshop = function() {
		
		var options = _pointshop.datas;
		
		if(options.activate_preorder_function == 1) //data-value=2
		{
			$('#preorder_option_block div.item[data-value="2"], div.isCommandeBloquee.button').show();
		}
		else
		{
			$('#preorder_option_block div.item[data-value="2"], div.isCommandeBloquee.button').hide();
		}
		
		if(options.activate_quotation_function == 1) //data-value=3
		{
			$('#preorder_option_block div.item[data-value="3"], div.isDevis.button').show();
		}
		else
		{
			$('#preorder_option_block div.item[data-value="3"], div.isDevis.button').hide();
		}
		
		
		var id_employee = _getIdEmployee();
		
		//init du boutton de gestion des stocks
		var ids_employee_autorized_for_stock = options.limit_stock_managment_access.split(';');
		if(ids_employee_autorized_for_stock == '' || ids_employee_autorized_for_stock.indexOf('' + id_employee) > -1)
		{
			$('button#stockBtn').show();
		}
		else
		{
			$('button#stockBtn').hide();			
		}
		//init du boutton de visualisation des commandes d'un client
		var ids_employee_autorized_for_order = options.limit_order_access.split(';');

		if(options.limit_order_access == '' || ids_employee_autorized_for_order.indexOf('' + id_employee) > -1)
		{
			$('#userEditContainer li[data-page-id="pageUser3"]').show();
		}
		else
		{
			$('#userEditContainer li[data-page-id="pageUser3"]').hide();			
		}		
	};
	
	//Permissions	
	var _getIdEmployee = function() {
		return parseInt('0' + $('#showEmployee').data('id_employee'));
	};
	
	
	/***************************************************************
	 * chargement des lib de caisse
	 ***************************************************************/
	this.payment = new CaissePayment(this);
	this.loader = new CaisseLoader(this);	
	this.customerScreen = new USBCustomerScreen(this);
	this.return = new CaisseReturn(this);
	this.delivery = new CaisseDelivery(this);
	this.invoice = new CaisseInvoice(this);
	this.carrier = new CaisseCarrier(this);	
	this.preorder = new CaissePreorder(this);	
	this.employee = new CaisseEmployee(this);
	this.customer = new CaisseCustomer(this);
	this.printer = new CaissePrinter(this);
	this.ticket = new Ticket(this);
	
	// init
	_init(this);
}
function Product(id_product) {
	
	// @todo : refaire otutes les correspondances
	this.id_cat=acaisse_products_prices.catalog[id_product].a;
	this.reference=acaisse_products_prices.catalog[id_product].b;
	this.product_name = acaisse_products_prices.catalog[id_product].c;
	this.has_attributes = false;
	this.attributes = {};
	this.id_default_attribute = acaisse_products_prices.catalog[id_product].f;
	this.prices = acaisse_products_prices.catalog[id_product].g;
	this.base_price = acaisse_products_prices.catalog[id_product].d;
	this.unit_price = acaisse_products_prices.catalog[id_product].h;
	this.unity = acaisse_products_prices.catalog[id_product].i;
	this.tax_rate = acaisse_products_prices.catalog[id_product].j;
	this.id_tax_rule_group = acaisse_products_prices.catalog[id_product].k;
	this.ecotax = acaisse_products_prices.catalog[id_product].l;
	this.unit_price_ratio = acaisse_products_prices.catalog[id_product].m;
	this.id_product = acaisse_products_prices.catalog[id_product].n;
	this.disponibility_by_shop = acaisse_products_prices.catalog[id_product].o;
	this.active = acaisse_products_prices.catalog[id_product].p;
	this.is_generic_product = acaisse_products_prices.catalog[id_product].y == '1';
	
	//@TODO pourquoi pas ajouter this.image = ',oPthoto.jpg'; //directement ici
	if (acaisse_products_prices.catalog[id_product].q!=undefined) {
		this.image = (''+acaisse_products_prices.catalog[id_product].q).replace('http://','//').replace('https://','//');
	}
	
	for (var id in acaisse_products_prices.catalog[id_product].e) {
		this.has_attributes = true;
		
		this.attributes[id] = {};
		this.attributes[id].attribute_name = acaisse_products_prices.catalog[id_product].e[id].r;
		this.attributes[id].prices = acaisse_products_prices.catalog[id_product].e[id].s;
		this.attributes[id].infos = acaisse_products_prices.catalog[id_product].e[id].t;
		this.attributes[id].ref = acaisse_products_prices.catalog[id_product].e[id].u;
		this.attributes[id].ecotax = acaisse_products_prices.catalog[id_product].e[id].v;
		this.attributes[id].image = (''+acaisse_products_prices.catalog[id_product].e[id].w).replace('http://','//').replace('https://','//');
		this.attributes[id].id_product_attribute = acaisse_products_prices.catalog[id_product].e[id].x;
	}
		
	if(this.is_generic_product === true) //un produit générique ne peut pas avoir "réellement" des déclinaisons
	{
		this.has_attributes = false;
	}
}
function TicketLine(ticket_line)
{
	this.is_detaxed = ACAISSE.ticket.is_detaxed == 1;
	
	//raw datas from DB
	this.details = ticket_line.details;
	var discount = ticket_line.discount;
	var force_id = ticket_line.force_id;
	this.id = ticket_line.id;
	var id_a_caisse_ticket = ticket_line.id_a_caisse_ticket;
	this.id_product_attribute = ticket_line.id_attribute;
	this.id_product = ticket_line.id_product;
	var id_shop_list = ticket_line.id_shop_list;
	this.label = ticket_line.label;
	var price_with_tax = ticket_line.price_with_tax;
	var price_without_tax = ticket_line.price_without_tax;
	var original_price_with_tax = ticket_line.original_price_with_tax;
	this.quantity = ticket_line.quantity;
	var original_price_without_tax = ticket_line.original_price_without_tax;
	
	var original_price_is_forced_by_user = ticket_line.original_price_is_forced_by_user;
	
    var tax = ticket_line.tax;
	
	//donnees retravaillées est exploitable
	this.p = _loadProductFromId(this.id_product);
	
	//console.log(['class TicketLine',this.p,ticket_line, this.id_product_attribute, this.p.is_generic_product]);
    this.a; //l'attribut
    var attribute_name='';
    if (this.id_product_attribute>0) {
    	this.a = this.p.attributes[this.id_product_attribute];
    	//attribute_name = this.a.attribute_name;
    }
	
    this.unity = _loadProductFromId(this.id_product).unity;
    this.unitId = params.units.from.indexOf(this.unity);	
    
    //on ajoute l'unité manquante à la volée, mais elle aura aucun effet
    if (this.unitId==-1) {
		//console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(this.p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		this.unitId = params.units.from.indexOf(this.unity);
	}
    
    
    this.scale = params.units.scale[this.unitId];
    this.to = params.units.to[this.unitId];
	var ps_price = parseFloat(ticket_line.price_with_tax);
	
	this.reduction = discount;
	
	if(this.unity != '')
    {
    	var discount_length = this.reduction.length;
    	var discount_format = this.reduction.substr(0,discount_length-1);
    	
    	this.reduction_ttc = parseFloat('0'+discount_format*this.scale);

    }
    else
    {
    	this.reduction_ttc = parseFloat('0'+discount);
    }	
    //console.log(this.reduction_ttc);
     
    // Ajustement des quantités si prix unitaire 
    this.quantity = parseFloat(this.quantity/this.scale);
    
    //this.reduction_ttc = parseFloat('0'+discount);
    this.prix_final_ttc = parseFloat(this.scale*price_with_tax);
    this.prix_final_ht = parseFloat(this.scale*price_without_tax);
    this.prix_original_ttc = parseFloat(this.scale*original_price_with_tax);
    this.prix_original_ht = parseFloat(this.scale*original_price_without_tax);
    this.reduction_type = false;
    
    this.base_price_ht = parseFloat(this.scale*this.p.base_price);
    this.base_price_ttc = parseFloat(this.scale*this.p.base_price*(1+(this.p.tax_rate/100)));
    
    this.price_orig = false;
    
    //propriété formaté pour l'affichage
    if(this.detaxed)
    {
    	this.has_reduction = this.prix_final_ttc != this.prix_original_ttc;
    }
    else
    {
    	this.has_reduction = this.prix_final_ht != this.prix_original_ht;
    }
    
    
    if(this.reduction.indexOf('%')!=-1) //si reduction en pourcentage
    {
    	this.reduction_type = '%';
    	if(this.unity != '')
    	{
    		this.reduction_ttc = this.prix_original_ttc * this.reduction_ttc / 10000;
    	}else
    	{
    		this.reduction_ttc = this.prix_original_ttc * this.reduction_ttc / 100;
    	}
		this.display_reduction = this.has_reduction ? '- ' + this.reduction : '';
    }
    else if(this.reduction.indexOf('€')!=-1) //si reduction en pourcentage
    {
    	this.reduction_type = '€';
    	if(this.unity != '')
    	{
    		this.reduction_ttc = discount_format*this.scale;
			this.display_reduction = this.has_reduction ? '- ' + this.reduction_ttc.toFixed(2)+this.reduction_type : '';
    	}
    	else
    	{
    		this.display_reduction = this.has_reduction ? '- ' + this.reduction : '';
    	}
    	    	
    }
    
    this.reduction_orig_ht = parseFloat(this.base_price_ht - this.prix_final_ht);   
    this.reduction_orig = this.is_detaxed?this.reduction_orig_ht:parseFloat(this.base_price_ttc - this.prix_final_ttc); 
      
    this.display_reduction_orig = (this.reduction_orig.toFixed(2))+'€'+ this.to;
    this.display_reduction_orig_ht = (this.reduction_orig_ht.toFixed(2))+'€'+ this.to;
    
    this.reduction_ht = this.prix_original_ht * this.reduction_ttc / this.prix_original_ttc;
    this.taux_tva = 100 * (this.prix_original_ttc - this.prix_original_ht) / this.prix_original_ht;
    
    //pour backup
	this.ticket_line = ticket_line;
	
    this.display_prix_final_ttc = (this.prix_final_ttc.toFixed(2))+'€'+ this.to;
    this.display_prix_final_ht = (this.prix_final_ht.toFixed(2))+'€'+ this.to;
    this.display_prix_original_ttc = (this.prix_original_ttc.toFixed(2))+'€'+ this.to;
    this.display_prix_original_ht = (this.prix_original_ht.toFixed(2))+'€'+ this.to;

	//this.display_reduction = this.has_reduction ? '- ' + this.reduction_ttc.toFixed(2)+this.reduction_type : '';
    
    this.line_price_ht = this.quantity * this.prix_final_ht;
    this.line_price_ttc = this.quantity * this.prix_final_ttc;
    this.display_line_price_ht = (this.quantity * this.prix_final_ht).toFixed(2)+'€';
    this.display_line_price_ttc = (this.quantity * this.prix_final_ttc).toFixed(2)+'€';

	if(original_price_is_forced_by_user == 1)
	{
	    this.display_base_price_ht = (this.prix_final_ht.toFixed(2))+'€'+ this.to;
	    this.display_base_price_ttc = (this.prix_final_ttc.toFixed(2))+'€'+ this.to;  
	    this.base_price_ht = parseFloat(this.scale*this.prix_final_ht);
    	this.base_price_ttc = parseFloat(this.scale*this.prix_final_ht*(1+(this.p.tax_rate/100)));
    	//this.prix_original_ttc = this.base_price_ttc;
    	//this.prix_original_ht = this.base_price_ht;
    	
    	if(this.detaxed)
	    {
	    	this.has_reduction = this.prix_final_ttc != this.prix_original_ttc;
	    }
	    else
	    {
	    	this.has_reduction = this.prix_final_ht != this.prix_original_ht;
	    }
	} 
	else
	{
		this.display_base_price_ht = (this.base_price_ht.toFixed(2))+'€'+ this.to;
	    this.display_base_price_ttc = (this.base_price_ttc.toFixed(2))+'€'+ this.to;
	} 
    
    this.original_price_is_forced_by_user = original_price_is_forced_by_user;
    
    //console.log(this.has_reduction);
}

function CartLine(cart_line_from_prestashop, detaxed_ticket)
{	
	if(detaxed_ticket == undefined)
	{
		detaxed_ticket = false;
	} 
	
	ticket_line = cart_line_from_prestashop;
	
	//raw datas from DB
	//this.details = ticket_line.details;
	var discount = ticket_line.discount;
	//var force_id = ticket_line.force_id;
	//var id = ticket_line.id;
	//var id_a_caisse_ticket = ticket_line.id_a_caisse_ticket;
	var id_product_attribute = ticket_line.id_product_attribute;
	var id_product = ticket_line.id_product;
	//var id_shop_list = ticket_line.id_shop_list;
	
	//this.label = ticket_line.label;
	
	
	var price_with_tax = ticket_line.price_with_reduction;
	var price_without_tax = ticket_line.price_with_reduction_without_tax;
	var original_price_without_tax = ticket_line.price_without_reduction;
	var original_price_with_tax = ticket_line.price_wt;
	var ecotax = ticket_line.ecotax;
	this.quantity = ticket_line.cart_quantity;
	//var tax = ticket_line.tax;
	
	//donnees retravaillées est exploitable
	this.p = _loadProductFromId(id_product);
    this.a;
    var attribute_name='';
    if (id_product_attribute>0) {
    	this.a = this.p.attributes[id_product_attribute];
    	//attribute_name = this.a.attribute_name;
    }
	
    this.unity = _loadProductFromId(ticket_line.id_product).unity;
    this.unitId = params.units.from.indexOf(this.unity);	
    
    //on ajoute l'unité manquante à la volée, mais elle aura aucun effet
    if (this.unitId==-1) {
		//console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(this.p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		this.unitId = params.units.from.indexOf(this.unity);
	}
    
    
    this.scale = params.units.scale[this.unitId];
    this.to = params.units.to[this.unitId];
    
	this.reduction = discount;
		
    if(this.unity != '' && this.reduction != undefined)
    {
    	var discount_length = this.reduction.length;
    	var discount_format = this.reduction.substr(0,parseInt(discount_length-1));
    	this.reduction_ttc = parseFloat('0'+discount_format*this.scale);

    }
    else
    {
    	if(this.reduction != undefined)
    	{
    		this.reduction_ttc = parseFloat('0'+discount);
    	}
    	else
    	{
    		this.reduction_ttc = 0;
    	}
    	
    }	
     
    // Ajustement des quantités si prix unitaire 
    this.quantity = parseFloat(this.quantity/this.scale);
    
    this.reduction_ttc = parseFloat('0'+discount_format*this.scale);
    this.prix_final_ttc = parseFloat(this.scale*price_with_tax);
    this.prix_final_ht = parseFloat(this.scale*price_without_tax);
    this.prix_original_ttc = parseFloat(this.scale*original_price_with_tax);
    this.prix_original_ht = parseFloat(this.scale*original_price_without_tax);
    this.reduction_type = false;
    
    //TODO : attention ici n'apparaisse plus les reduciton avec les cart_line... a corriger
    if(this.reduction)
    {
	    if(this.reduction.indexOf('%')!=-1) //si reduction en pourcentage
	    {
	    	if(this.unity != '')
	    	{
	    		this.reduction_ttc = this.prix_original_ttc * this.reduction_ttc / 10000;
	    	}else
	    	{
	    		this.reduction_ttc = this.prix_original_ttc * this.reduction_ttc / 100;
	    	}
	    	this.reduction_type = '%';
	    }
	    else if(this.reduction.indexOf('€')!=-1) //si reduction en pourcentage
	    {
	    	this.reduction_type = '€';
	    }
    }
    
    this.reduction_ht = this.prix_original_ht * this.reduction_ttc / this.prix_original_ttc;
    this.taux_tva = 100 * (this.prix_original_ttc - this.prix_original_ht) / this.prix_original_ht;
    
    //pour backup
	this.ticket_line = ticket_line;
	
	console.log('ici');
    
    //propriété formaté pour l'affichage
    this.has_reduction = this.prix_final_ttc != this.prix_original_ttc;    
    this.display_prix_final_ttc = (this.prix_final_ttc.toFixed(2))+'€'+ this.to;
    this.display_prix_final_ht = (this.prix_final_ht.toFixed(2))+'€'+ this.to;
    this.display_prix_original_ttc = (this.prix_original_ttc.toFixed(2))+'€'+ this.to;
    this.display_prix_original_ht = (this.prix_original_ht.toFixed(2))+'€'+ this.to;
    
    this.display_reduction = this.has_reduction ? '- ' + this.reduction_ttc.toFixed(2)+this.reduction_type : '';
    this.line_price_ht = this.quantity * ticket_line.total;
    this.line_price_ttc = this.quantity * ticket_line.total_wt;
    this.display_line_price_ht = (this.quantity * this.line_price_ht).toFixed(2)+'€';
    this.display_line_price_ttc = (this.quantity * this.line_price_ttc).toFixed(2)+'€';
}
function Ticket(raw_ticket)
{
	var raw_datas = raw_ticket;
	
	//console.log('++++++++++++++++');
	//console.log(raw_datas);
	
	this.lines = [];
	
	if(raw_datas.lines != undefined)
	{
		for(var i = 0 ; i < raw_datas.lines.length ; i++)
		{
			this.lines.push(new TicketLine(raw_datas.lines[i]));
		}	
	}
	
}

function USBCustomerScreen(caisse_ref)
{	
	
	var MODE_DEBUG = false;
	
	var caisse = caisse_ref;
	var usb_screen = null;
	
	
	var pointshop_params = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas;
	params.customerScreen.mode = pointshop_params.screen_mode;
	
	var _init = function() {
		
		if(MODE_DEBUG == true)
		{
			console.log([
				'init usb screen module',
				params.customerScreen.mode,
				params.customerScreen.mode.indexOf('usb'),
				usb_screen,
			]);		
		}
		
		if (params.customerScreen.mode.indexOf('usb')!=-1 && usb_screen==undefined) {		
			usb_screen = window.open(
				params.customerScreen.usb_screen_controller_url,
				"_blank",
				"fullscreen=yes, toolbar=no, scrollbars=no, resizable=non, top=0, left=0, width=800, height=600"
			);
			
			// On met l'usb screen dans windows pour pouvoir le fermer
			windows.push(usb_screen);		}
	};
	
	/*
	caisse.registerHook('test', function() {
		
	});
	*/
	
	_init();	
}

function CaisseReturn(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	this.mode_return_is_active = false;
	
	this.ticket = null;//new CaisseReTurnTicket();

	var number_formatter = new Intl.NumberFormat('fr-FR',{minimumFractionDigits:2});
	
	var current_payment_module_name = null;
	var tot_a_rembourser = 0; 
	var tot_deja_rembourser = 0;
	var extra_datas = {}; //données complémentaire qui seront transmise au serveur
	
	this.switchToReturnMode = function() {
		if(LOG_LEVEL > 9)
		{
			console.log('"RETURN MODE" activated');
		}
		$('.mode-buy').hide();
		
		$('#help-return').show();
		
		$('.mode-return').show();
		$('#container').addClass('is-mode-return').removeClass('is-mode-buy');
		closeAllPanel();
		this.mode_return_is_active = true;
	};
	
	this.switchToBuyMode = function() {
		if(LOG_LEVEL > 9)
		{
			console.log('"BUY MODE" activated');
		}
		$('.mode-return').hide();	
		$('.mode-buy').show();	
		$('#container').addClass('is-mode-buy').removeClass('is-mode-return');
		closeAllPanel();
		hidePopup();
		this.mode_return_is_active = false;
	};
	
	this.modules = [];
	
	var getCurrentPaymentModule = function() {
		
		for(i in caisse.payment.modules)
		{
			var module = caisse.payment.modules[i];
			if(module.constructor.name == current_payment_module_name)
			{
				return module;
			}
		}		
		return {};
	};
	
	this.getPaymentModuleByName = function(payment_module_name) {
		
		
		for(i in caisse.payment.modules)
		{
			var module = caisse.payment.modules[i];
			if(module.constructor.name == payment_module_name)
			{
				return module;
			}
		}		
		return false;
	};
	
	this.getCurrentPaymentModule = function() {
		return getCurrentPaymentModule();
	};
	
	this.dispatchScanBarCode = function(e) {
		var module = this.getCurrentPaymentModule();
		if(module.captureScanner != undefined)
		{
			if(DEBUG_LEVEL > 5)
			{
				console.log('module '+module.constructor.name+' capture scan event');
			}
			module.captureScanner(e);
		}		
	};
	
	/**
	 * Gestion de l'ouverture de la popup de retour
	 * et son remplissage avec les dernière commande 
	 */
	this.openReturnPopup = function(id_product, id_product_attribute, quantity, id_order) {
		if(LOG_LEVEL > 7)
		{
			console.log('openReturnPopup() with args :');
			console.log({id_product:id_product, id_product_attribute:id_product_attribute, quantity:quantity, id_order:id_order});
		}
		//requete au serveur pour récupérer la liste des commandes
		
		$list = $('#popupReturn .orders_list tbody').html('<tr><td colspan="7"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i>'+'Chargement'+'</td></tr>');
		
		$('#popupReturn').data({
			'idProduct' : id_product,
			'idProductAttribute' : id_product_attribute,
		});
		
		var p = _loadProductFromId(id_product);
		var a = null;
		if (id_product_attribute && p.has_attributes) {
			a = p.attributes[id_product_attribute];
		}
		
		//chargement du nom produit
		$('#popupReturn .header-inner h1 span').html(p.product_name);
		//chargement du nom de la déclinaisons (si déclinaison)
		$('#popupReturn .header-inner h1 small').html(a===null?'':' / ' + a.attribute_name);
		
		
		$.ajax({
	        data: {
	            'action' : 'GetOrdersContainingProduct',
	            'id_customer' : ACAISSE.customer.id,
	            'id_product' : id_product,
	            'id_product_attribute' : id_product_attribute,
	            'nb_day' : 180, //A dynamiser dans le pointshop je pense
	            'ids_pointshop' : '',
	            'quantity' : quantity,
	            'id_return' : PRESTATILL_CAISSE.return.ticket === null ? 0 : PRESTATILL_CAISSE.return.ticket.id,
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	
	        	
	        	var request = response.request;
	        	var response = response.response;	  
	        	  
	        	
	        	
				var tbody = '';
	        	    	
				//là on injecte les balises script
				if (response.list_orders.length>0) {
					
					$list = $('#popupReturn .orders_list tbody');
					$list.html(response.list_orders);
					
					var restant_a_refund = request.quantity;
					var restant_a_retourner = request.quantity;
					//console.log(response);
					
					$.each(response.list_orders, function(i,line){
						
						if(LOG_LEVEL > 6)
						{
	        				console.log(line);
	        			}
						var product_quantity = line.product_quantity;
						var product_quantity_refunded = line.product_quantity_refunded;
						var product_quantity_return = line.product_quantity_return;
						var max_refund = product_quantity - product_quantity_refunded;
						var max_retournable = product_quantity - product_quantity_return;
						var value_refund = Math.min(max_refund, restant_a_retourner);
						var value_retournable = Math.min(max_retournable, restant_a_retourner);	
						
						//chargement ici des trucs actuellement reservé
						
						restant_a_refund -= value_refund;
						restant_a_retourner -= value_retournable;
						
						if(value_refund <= 0)
						{
							value_refund = '';
						}
						
						if(value_retournable <= 0)
						{
							value_retournable = '';
						}
						
						//var 					
						
						tbody += '<tr class="return-sub-detail-line">';
						tbody += '	<td>'+line.reference+'</td>';
						tbody += '	<td width="80px">'+line.order_date+'</td>';
						tbody += '	<td>'+product_quantity+'</td>';
						tbody += '	<td>'+product_quantity_refunded+'</td>';
						//tbody += '	<td>'+product_quantity_return+'</td>';
						tbody += '	<td><span class="ui-spinner-group refund" data-min="0" data-max="'+max_refund+'"><input class="ui-spinner" name="quantity['+line.id_order+'__'+line.id_order_detail+']" value="'+value_refund+'" readonly /></span></td>';
						//tbody += '	<td><span class="ui-spinner-group return" data-min="0" data-max="'+max_retournable+'"><input class="ui-spinner" name="return_quantity['+line.id_order+'__'+line.id_order_detail+']" value="" /></span></td>';
						//tbody += '	<td><span class="ui-spinner-group reinject" data-min="0" data-max="0"><input class="ui-spinner" name="reinjected['+line.id_order+'__'+line.id_order_detail+']" value=""  /></span></td>';
						tbody += '	<td>';
						tbody += '     <span class="ttc">' + parseFloat(line.unit_price_tax_incl).toFixed(2) + '€ TTC</span>';
						tbody += '     <span class="ht">' + parseFloat(line.unit_price_tax_excl).toFixed(2) + '€ HT</span>';
						tbody += '  </td>';
						tbody += '	<td><input'+(value_refund==''?' disabled="true" style="opacity:0.5"':'')+' class="ui-spinner-group price" name="price['+line.id_order+'__'+line.id_order_detail+']" value="' + line.unit_price_tax_incl + '"></td>';
						tbody += '</tr>';					
					});					
				}
				else
				{
					tbody += '<tr class="return-sub-detail-line">';
					tbody += '	<td colspan="7">Aucune commande trouvée</td>';
					tbody += '</tr>';					
				}
				
				$list.html(tbody);
				
				$('.ui-spinner-group.refund').uiSpinner({
					onChange:function(plugin,$element,value) {
						//faut trouver le spinner de la même ligne lié
						$line = $element.parents('.return-sub-detail-line');
						if(value == '' || value == 0)
						{
							$price = $('.ui-spinner-group.price',$line).prop('disabled',true).css('opacity','0.5');
						}
						else
						{							
							$price = $('.ui-spinner-group.price',$line).prop('disabled',false).css('opacity','1');
						}
						_calcul_total_popup();
					}
				});
    			$('#popupReturn').show();  
    			$('#popupReturn').find('.body').scrollTop( 0 );
    			$('#popupReturnForm .ui-spinner-group.refund .ui-spinner').prop('readonly', true);
    			
    			_calcul_total_popup(); 
				
	        } else {
	        	alert('Erreur o_ret0121 : impossible de rafraichir la liste des commandes');
	        }
	    }).fail(function () {
	        alert('Erreur o_ret0120 : impossible d\'obtenir la liste des commandes contenant ces ventes du serveur');
	    });
		
		 	
	};
	
	this.registerRefund = function(params) {
		
		var extra_datas = {};
		//console.log(params);
		
		PRESTATILL_CAISSE.loader.start();
		
		$.ajax({
			type: 'post',
			data: params,
			dataType: 'json',
		}).done(function (response) {
			
			
			if(response.success)
			{
				var html = response.html;
				var request = response.request;
				var response = response.response;	
				
				PRESTATILL_CAISSE.return.ticket = null;
	        	//PRESTATILL_CAISSE.return.redrawTicket();
	        	$('#ticketView .return-lines tbody').html('');
					          
			  	PRESTATILL_CAISSE.loader.end();	
			  	caisse_ref.return.switchToBuyMode();
			  	
			  	$.each(html,function(i,refund){
					
					$('#printTickets .left .ticket.toPrint .kdoprint').hide();
					$('#printTickets .left .ticket.toPrint .kdotitle').show();
					$('#printTicket').html(refund);
					//window.print();
					printTicket(refund,1);
				});			
				
				printQueue();	
			  	
			  	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 1) {
					$('#openRootPageCaisse').click();
					_gotoProductList();
				} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 2
					&& acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.navigation_by_category == 1) {
					$('#openRootCategorie').click(); 
				} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 0) {
					//$('.switcher').click(); 
					_gotoTicketView();
				}
			  			
			}
			else
			{
				PRESTATILL_CAISSE.loader.end();
				alert('Error 0103245 : ' + response.errorMsg);
			}		
			
			return;
			
		}).fail(function (response) {
			alert('Erreur 010122 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
			PRESTATILL_CAISSE.loader.end();
		});
		
		caisse.executeHook('paiement_validation',{
			//@TODO
		});
		
	};	
	
	
	this.redrawTicket = function() {
		var html = '';
		
		$('.return-lines').show();
		$('#help-return').hide();
		
		if(this.ticket != undefined)
		{
			$.each(this.ticket.products,function(i,product){
			  html += '<tr class="line return_line" unselectable="on" style="user-select: none;">';
		        html += '<td class="label" unselectable="on" style="user-select: none;">'+product.product_name+'</td>';
		        html += '<td unselectable="on" style="user-select: none;"><b>'+product.total_quantity+'</b></td>';
		        html += '<td class="totalLineCost" unselectable="on" style="user-select: none;"><b>'+parseFloat(product.total_return_price).toFixed(2)+'€</b></td>';
		        //todo rajouter ici des infos
		      html += '</tr>';
			});
		}
		
		
		$('#ticketView .return-lines tbody').html(html);
		
		this.redrawTotals();
	};
	
	this.redrawTotals = function() {
		$('#totalReturnBlock').html(parseFloat(PRESTATILL_CAISSE.return.ticket!=null?PRESTATILL_CAISSE.return.ticket.total_refund:0).toFixed(2)+'€');
		$('#totalItem').html(PRESTATILL_CAISSE.return.ticket!=null?PRESTATILL_CAISSE.return.ticket.total_quantity:0);
	};
	
	
	//evenement
	$('body').on('click tap','#return-switch-mode-button',function(){
		caisse_ref.return.switchToReturnMode();
		
		// On vide le ticket
		
		
		$(this).removeClass('open');
	});
	
	// annulation du ticket retour
	$('#returnButtonsPanel').on('click tap','.cancelreturn',function(){
		//on demande la suppression de ce ticket côté serveur		
		var id_return = PRESTATILL_CAISSE.return.ticket == null ? 0:PRESTATILL_CAISSE.return.ticket.id;
		if(id_return == 0)
		{
			caisse_ref.return.switchToBuyMode();
		}
		else
		{
			$.ajax({
		        data: {
		        	'action' : 'CancelReturnTicket',
			        'id_customer' : ACAISSE.customer.id,
			        'id_employee' : ACAISSE.customer.id,
			        'id_product' : $('#popupReturn').data('idProduct'),
			        'id_product_attribute' : $('#popupReturn').data('idProductAttribute'),
			        'id_return' : PRESTATILL_CAISSE.return.ticket == null ? 0:PRESTATILL_CAISSE.return.ticket.id,
		        },
		        type: 'post',
		        dataType: 'json',
		    }).done(function (response) {
		        if (response.success !== false) {
		        	PRESTATILL_CAISSE.return.ticket = null;
		        	$('#ticketView .return-lines tbody').html('');
		        	PRESTATILL_CAISSE.return.redrawTicket();
					caisse_ref.return.switchToBuyMode();		        		        					
		        } else {
		        	alert('Erreur o_ret0143 : impossible sauvegarder les informations');
		        }
		    }).fail(function () {
		        alert('Erreur o_ret0144 : impossible de communiquer avec le serveur');
		    });
	    }
	});
	
	$('.mode-return').on('click tap','#launchRegisterRefundPaymentRefund',function(){
		if(PRESTATILL_CAISSE.return.ticket == null)
		{			
	        alert('Erreur o_ret0640 : Veuillez d\'abord enregistrer des informations de retour');
	        return;
		}
		
		if($(this).hasClass('disabled'))
		{
			return;
		}
		_gotoPaymentPanel();
        	
        $('#paymentView').find('.prestatill_payment_interface .numeric_keyboard').hide();
        
	});
	
	
	$('.mode-return').on('click tap','#launchRegisterRefundPayment',function(){
		
		//var module_refund = PRESTATILL_CAISSE.payment.getCurrentPaymentModule();
		if(PRESTATILL_CAISSE.return.ticket == null)
		{			
	        alert('Erreur o_ret0640 : Veuillez d\'abord enregistrer des informations de retour');
	        return;
		}
		
		if($(this).hasClass('disabled'))
		{
			return;
		}
		
		$.ajax({
	        data: {
	        	'action' : 'RegisterRefundAndReturn',
		        'id_customer' : ACAISSE.customer.id,
		        'id_employee' : $('#showEmployee').data('id_employee'),
		        'id_pointshop' : ACAISSE.id_pointshop,
		        'id_return' : PRESTATILL_CAISSE.return.ticket.id,
		        'summary' : '',
		        'module_name' : 'prestatillslip'
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	PRESTATILL_CAISSE.return.ticket = null;
	        	//PRESTATILL_CAISSE.return.redrawTicket();
	        	$('#ticketView .return-lines tbody').html('');
				caisse_ref.return.switchToBuyMode();
				
				//alert('Avoir généré... cool!');
				//console.log('*****************');
				//console.log(response);				
				
				$.each(response.response.printable_slips,function(i,slip){
					
					$('#printTickets .left .ticket.toPrint .kdoprint').hide();
					$('#printTickets .left .ticket.toPrint .kdotitle').show();
					$('#printTicket').html(slip);
					//window.print();
					printTicket(slip,1);
				});			
				
				printQueue();	
				
				_gotoTicketView();
	        } else {
	        	alert('Erreur o_ret01643 : impossible sauvegarder ce retour pour le moment');
	        }
	    }).fail(function () {
	        alert('Erreur o_ret0644 : impossible de communiquer avec le serveur');
	    });
	});
	
	//validation de la popup de retour
	$('#popupReturn').on('tap click','#popupReturnValidateButton',function(){
		
	   var datas = {
	        'action' : 'SaveReturnPopupDatas',
	        'id_customer' : ACAISSE.customer.id,
	        'id_employee' : ACAISSE.customer.id,
	        'id_product' : $('#popupReturn').data('idProduct'),
	        'id_product_attribute' : $('#popupReturn').data('idProductAttribute'),
	        'id_return' : PRESTATILL_CAISSE.return.ticket == null ? 0:PRESTATILL_CAISSE.return.ticket.id,
 	   };
	   
	   $.each($('#popupReturn form').serializeArray(),function(){
	   		//console.log(this);
	   		datas[this.name] = this.value;
	   });
	   		
		$.ajax({
	        data: datas,
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	
	        	
	        	var request = response.request;
	        	var response = response.response;	  
	        	PRESTATILL_CAISSE.return.ticket = response.return_ticket;
	        	
	        	PRESTATILL_CAISSE.return.redrawTicket();
	        	
	        	//on referme la popup
	        	$('#popupReturn').hide();
	        	$list = $('#popupReturn .orders_list tbody').html('');
	        		        		        					
	        } else {
	        	alert('Erreur o_ret0123 : impossible sauvegarder les informations');
	        }
	    }).fail(function () {
	        alert('Erreur o_ret0124 : impossible de communiquer avec le serveur');
	    });
		
		
		/************************/
		
		
		
	});
	
	function _calcul_total_popup()
	{
		var tot_lines = 0;
		$.each($('#popupReturn form input.price'),function(){
			if($(this).prop('disabled') == false)
			{
				//found qu
				var qu = Math.max(0,parseInt('0' + $(this).parents('tr').find('input[name^="quantity"]').val()));
				//console.log('qu sur ligne : ' + qu  + " * " + $(this).val());
				
				tot_lines += qu * parseFloat($(this).val());
			}
			
		});
		$('#popupReturn .footer .total_popup span').html(parseFloat(tot_lines).toFixed(2)+'€');
	}
	
	
	this.refreshAmountDisplay = function() {
		//$('.solde_restant').html(diff==0 && repaid == 0?'':repaid>0?formatted_repaid:formatted_diff);
		//$('.payment_amount').html(formatted_refund_amount);		
		//$('.display_back').removeClass('too_much not_enough good repaid').addClass(diff==0 && repaid == 0?'good':(diff>0?'not_enough':(repaid>0?'repaid too_much':'too_much')));
		//$('#real_refund_amount').html(formatted_real_payment_amount + currency);
		
		$('#real_refund_amount').html(PRESTATILL_CAISSE.return.ticket.total_refund + currency);							
	};
	
	this.getTotal = function() {		
		var tot_to_pay = $('#totalReturnBlock').html();
		var tot_to_pay_res = tot_to_pay.replace(" ","");
		
		return parseFloat('0'+tot_to_pay_res);
	};
	
	
	$(function(){
		//par défaut la caisse pass en mode BUY
		caisse.return.switchToBuyMode();		
	});
	
}
function CaisseReturnTicket(caisse_ref)
{	
	var caisse = caisse_ref;
	
	this.lines = []; 
	
	
};

function CaisseDelivery(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	
	this.STORE_DELIVERY = 0;
	this.HOME_DELIVERY = 1;
	
	
	var drawAddressDetails = function(address, with_name) {
		/*
		*
		active / address1 / address2 / alias / city / company / country / date_add / date_upd / deleted / dni
		firstname / id_address / id_country / id_customer / id_manufacturer / id_state / id_supplier
		/ id_warehouse / lastname / other / phone / phone_mobile / postcode / state / state_iso / vat_number
						 */
		var html = '';
		if(with_name == undefined) { with_name=true; }
		
		if(with_name && (address.lastname != '' || address.firstname != ''))
		{
			if(html != '') { html += '<br />'; }
			html += (address.lastname==''?'':address.lastname + ' ') + (address.firstname==''?'':address.firstname);
		}
		
		if(address.address1 != '')
		{
			if(html != '') { html += '<br />'; }
			html += address.address1;
		}
		
		
		if(address.address2 != '')
		{
			if(html != '') { html += '<br />'; }
			html += address.address2;
		}
		if(''+address.city != '' || ''+address.postcode != '')
		{
			if(html != '') { html += '<br />'; }
			html += (address.postcode==''?'':address.postcode + ' ') + (address.city==''?'':address.city);
		}
		
		console.log(['drawAddressDetails',address,html]);
		
		return html;
	};
	
	
	this.initPopupDatas = function() {
		//console.log('showPopupGlobalCartReduction('+reduc+','+type_reduc+','+inputValue+')');
		
		//$('#popupDelivery .selected').removeClass('selected');
		
		
		
		
		//chargement des transporteurs actifs
		$.ajax({
	        type: 'post',
	        data: {
	        	'action': 'loadCarriers',
				'id_pointshop': ACAISSE.id_pointshop
	        },
	        dataType: 'json',
			
		}).done(function(response){
			if(response.success === true)
			{
				var html = '';
				$.each(response.response.carriers,function(i,carrier) {
					html += '<li data-id="'+carrier.id_carrier+'">';
					html += '   <h3>'+carrier.name+'</h3>';
					html += '</li>';
				});				
				$('#id_carrier').html(html);
				
				$('#id_carrier li').click(function(){					
					$('#id_carrier li').removeClass('selected');
					$('input[name="id_carrier"]').val($(this).addClass("selected").data('id'));
				});
				
				
				
			    console.log('********-+-********');
			    console.log(ACAISSE.ticket);
				
				$('#carrier_option').val(ACAISSE.ticket.with_carrier);
				$('#invoice_option').val(ACAISSE.ticket.with_invoice);
				
				//carrier
			    $('#carrier_option_block').uiSelector({
			      input : $('#carrier_option'),
			      onChange : function(new_value) {
			      	if(new_value == PRESTATILL_CAISSE.carrier.WITHOUT) //sans transporteur
			      	{         		
			      		$('#id_carrier').addClass('hide');
			      		$('input[name="id_carrier"]').val(0);
			      	}
			      	
			      	if(new_value == PRESTATILL_CAISSE.carrier.WITH) //avec transporteur
			      	{
			      		$('#id_carrier').removeClass('hide').find('li').eq(0).click();
			      	}
			      	
			      }
			    });
			    
			    
			    
			    //il faut peupler avec les adresses du customer actuellement sélectionné dans la caisse
		
				var html = '';		
				$.each(ACAISSE.customer.addresses,function(i,address){
					html += '<li data-id="'+address.id_address+'"><h3>'+address.alias+'</h3>';
						html += '<legend>';
						html += drawAddressDetails(address);						
						html += '</legend>';
					html += '</li>';
				});
		
				$('#id_delivery_address_home').html(html);
				$('#id_invoice_address').html(html);
				
				//chargement des adresses de point de vente prestatill
				var html = '';
				$.each(acaisse_pointshops.pointshops,function(i,pointshop){
					html += '<li data-id="'+i+'"><h3>'+pointshop.name+'</h3>';
						html += '<legend>';
						console.log(pointshop);
						html += drawAddressDetails(pointshop.datas.address, false);					
						html += '</legend>';
					html += '</li>';		
				});
				$('#id_delivery_address_store').html(html);
			    
			    
			    //rechargemenet des infos de detaxe depuis le ticket
			    $('#detax_option_block div').removeClass('selected');
			    if(ACAISSE.ticket.is_detaxed)
			    {
			    	$('#detax_option_block div[data-value="1"]').click();//addClass('selected');
			    }
			    else
			    {
			    	$('#detax_option_block div[data-value="0"]').click();//addClass('selected');
			    }
			    
				
			}
			else
			{
				alert('Error O_deliv_1001');
			}
		}).fail(function(){
			alert('Error O_deliv_1000');
		});
		
		
	};
		
	
}
function CaisseInvoice(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	
	
	this.WITHOUT = 0;
	this.WITH = 1;
	this.WITHOUT_DETAX = 0;
	this.WITH_DETAX = 1;
	
	this.initPopupDatas = function() {
		
		
	};
	
}
function CaisseCarrier(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	
	this.WITHOUT = 0;
	this.WITH = 1;
	
	this.initPopupDatas = function() {
		
		
	};
	
	
}
function CaissePreorder(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	
	
	this.ONLY_MY_PREORDERS = 0;
	this.OTHERS_PREORDERS = 1;
	this.ALL_PREORDERS = 2;
	
	this.openTab = function() {
	      
		$('#changeOrderButton i').removeClass('fa-angle-up').addClass('fa-angle-down');
		ACAISSE.scanbarcodeMode = 'preOrder';
		
		$('#preOrderListContainer .keyboard span.display').html('');
		
		//ici on charge la valeur de l'onglet a préselectionner
		$('#preorder_option').val('1');
              
		$('#preorder_option_block').uiSelector({
			default_value : 1,
	        onChange:function(filter_value){
	        	
	          
			  $('#preOrderListContainer .left ul li').remove().addClass('notLoaded');
			  $('#preOrderListContainer .left ul').html('<li><i class="fa fa-spinner fa-spin"></i>Chargement en cours...</li>');
	        	
	          //console.log('filter_value : '+filter_value);
                $.ajax({
                    data: {
                        'action': 'RetreiveDayTicketList',
                        'id_pointshop': ACAISSE.id_pointshop,
                        'filter' : filter_value,
                        'id_employee' : PRESTATILL_CAISSE.employee.getCurrentIdEmployee(),
                    },
                    type: 'post',
                    dataType: 'json',
                }).done(function (response) {
                    if (response.success != false) {
                    	var response = response.response;
                        $('#preOrderListContainer .left ul li').remove();
                        
                        var html = '';
                        //console.log(response);
                        
                        for (var i = 0; i < response.tickets.length; i++)
                        {
                            if (response.tickets[i].id_a_caisse_ticket != ACAISSE.ticket.id)
                            {
                                	var emp_initiale = response.tickets[i].emp_firstname;
                                	var emp_complete_name = response.tickets[i].emp_firstname+' '+response.tickets[i].emp_lastname;
                                	
                                	var ticket_date = response.tickets[i].day;
									var ticket_date_dm = ticket_date.substring(0,10);
									ticket_date = ticket_date_dm.split('-');
                                	
                                	// Afficher la date
                                	// Afficher "Commande" ou "Devis" au lieu de ticket si is_bloqued ou is_devis
                                	if(response.tickets[i].is_bloqued == 1)
                                	{
                                		html +='<li '+(response.tickets[i].isNotTotalyPaid!='1'?'class="notTotalyPayed"':'')+' data-id="' + response.tickets[i].id_a_caisse_ticket + '" data-num="' + response.tickets[i].id_a_caisse_ticket + '">';
                                	}
                                	else if(response.tickets[i].is_devis == 1)
                                	{
                                		html +='<li '+(response.tickets[i].isNotTotalyPaid!='1'?'class="notTotalyPayed"':'')+' data-id="' + response.tickets[i].id_a_caisse_ticket + '" data-num="' + response.tickets[i].num_devis + '">';
                                	}
                                	else
                                	{
                                		html +='<li '+(response.tickets[i].isNotTotalyPaid!='1'?'class="notTotalyPayed"':'')+' data-id="' + response.tickets[i].id_a_caisse_ticket + '" data-num="' + response.tickets[i].ticket_num + '">';
                                	}
                                	
                                	html +='<div class="emp"><i class="avatar">'+ emp_initiale.substr(0,1) +'</i>'+emp_complete_name+'</div>';
                                	html +='<span class="picto"><i class="fa fa-ticket"></i></span>';
                                	//html +='<span class="num_ticket">'+ response.tickets[i].id_a_caisse_ticket +'</span>';
                                	html +='<div><span class="date">' + ticket_date[2] + '/' + ticket_date[1] + '/' + ticket_date[0] + '</span></div>';
                                	html +='<div class="first_last_name">' + response.tickets[i].firstname + ' ' + response.tickets[i].lastname + '</div>';
                                	html +='<div>';
                                	if(response.tickets[i].is_bloqued == 1)
                                	{
                                		html +='<span class="preorder_label">Commande</span> n°' + response.tickets[i].id_a_caisse_ticket;
                                	}
                                	else if(response.tickets[i].is_devis == 1)
                                	{
                                		html +='<span class="predevis_label">Devis</span> n°' + response.tickets[i].num_devis;
                                	}
                                	else
                                	{
                                		html +='<span class="preticket_label">Ticket</span> n°' + response.tickets[i].ticket_num;
                                	}
                                	html +='</div>';                        	
                                	html +='<div><span class="pastille">' + parseFloat(response.tickets[i].ptinc-response.tickets[i].global_discount).toFixed(2) + '€</span></div>';
                                	html +='</li>';
                            }
                        }
                        
                        $('#preOrderListContainer .left ul').html(html);
                        $('#preOrderListContainer .left ul').removeClass('notLoaded');
                        
                        PRESTATILL_CAISSE.executeHook('actionPrestatillGeneratePreOrderListLine');
                    }
                    
                }).fail(function () {
                    alert('Erreur 000784 : impossible d\'obtenir la liste des précommandes du serveur. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
                });
	          
	          
	          
	          
	        }
	      });
		
	};
	
	this.closeTab = function() {		
                ACAISSE.scanbarcodeMode = 'product';
                $('#changeOrderButton i').removeClass('fa-angle-down').addClass('fa-angle-up');
	};
	
	this.printTicketPreorder = function (piece_type, big_format=false, send_email=false) {
	  	$.ajax({
	        type: 'post',
	        data: {
	          'action': 'GetTicketBloqued',
	          'id_ticket': ACAISSE.ticket.id,
	          'big_format': big_format,
	          'send_email': send_email,
	        },
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success != false) {
	        	
	          printTicket(response.response.ticket, acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.ticket_nbr_copies,piece_type);
	          if(send_email == false)
	          {	
	          	printQueue();
	          }
	          else
	          {
	          	PRESTATILL_CAISSE.loader.start();
	          	
	          	// On envoi le mail si les paramètres d'envoi de mail sont activés
	          	$.ajax({
			        type: 'post',
			        data: {
			          'action': 'SendEstimateEmail',
			          'id_ticket': ACAISSE.ticket.id,
			          'big_format': big_format,
			          'send_email': $('#send_form_email').val(),
			        },
			        dataType: 'json',
			    }).done(function (response) {
			        if (response.success != false) {
			        	
			          // On confirme que le mail a bien été envoyé
			          //closeAllPanel();
			          PRESTATILL_CAISSE.loader.end();
			          //alert('Mail envoyé avec succès');
			          sendNotif('greenFont','<i class="fa fa-check"></i> Mail envoyé avec succès');
			          
			        } else {
			          alert('Error : l_send0012 / Une erreur s\'est produite lors de l\'envoi du mail.');
			          //console.error(response.error);
			        }
			    }).fail(function () { 
			        alert('Error : l_send0011 / Une erreur s\'est produite lors de l\'envoi du mail.');
			    });
	          	
	          }   
	          
	          //on demande un passage au ticket suivant
	          _startNewPreorder();
	        } else {
	          alert('Error : o_print0012 / Erreur lors de la génération du ticket de caisse');
	          console.error(response.error);
	        }
	    }).fail(function () { 
	        alert('Error : o_print0011 / Erreur lors de la génération du ticket de caisse');
	    });
	};
	
	this.transformTicket = function(transform_to, big_format=false, send_email=false) {
		
		if (ACAISSE.ticket.lines.length == 0)
		{
			alert('Veuillez d\'abord sélectionner au moins un produit');
		}
		else
		{
			var extra_datas = {};
			
			if(transform_to == 'is_devis')
			{
				extra_datas = {
					'num_devis': '',
					'ref_devis': $('#popup_global_title').val(),
					'commentaire_devis': $('#popup_global_textarea').val(), 
				};
			}
		
			$.ajax({
		        type: 'post',
		        dataType: 'json',
		        data: {
		            'action': 'TransformTicketTo',
		            'id_ticket': ACAISSE.ticket.id,
		            'transform_to': transform_to,
		            'extra_datas': extra_datas,
		            
		        }
		    }).done(function (response) {
		        if (!response.success) {
		            //alert('Erreur 000552'+response.errorMsg);
		            return;
		        }
		        $('.ticketBox').slideUp();
				
				// Send NOTIF
				//_startNewPreorder();
				if(transform_to == 'is_bloqued')
				{
					caisse.preorder.printTicketPreorder('preorder');
				}
				else if(transform_to == 'is_devis')
				{
					caisse.preorder.printTicketPreorder('estimate', big_format, send_email);
				}
				else
				{
					_startNewPreorder();
				}
		        
		    }).fail(function () {
		        alert('Impossible de bloquer la commande.');
		    });
		}	
	};
	
	/*
     * Filtrer les preorder par nom
     */
    $('body').on('keyup','#filterPreOrder',function(e) {
    	var fliterPreOrder = $('#filterPreOrder input').val();
    	console.log(fliterPreOrder);
    	$('#preList li').each(function(){
    		console.log($(this).find('.first_last_name').html().toLowerCase().indexOf(fliterPreOrder.toLowerCase()));
    		if($(this).find('.first_last_name').html().toLowerCase().indexOf(fliterPreOrder.toLowerCase()) == -1)
    		{
    			$(this).hide();
    		}
    		
    	});   	
    });
	

	$('.isCommandeBloquee').on('click tap', function () {
			
		if (acaisse_profilesNoCheckinIDs.indexOf('' + ACAISSE.customer.id) != -1)
		{
			alert('Veuillez d\'abord identifier ou créer un client.');
		} 
		else 
		{
			 caisse.preorder.transformTicket('is_bloqued');
		}
       
    });
	
	$('.isDevis').on('click tap', function () {
		
		var html = '';
		
		html +='<div id="devisInfos" class="popupBox">';
			html +='<div class="overflow" style="display:none;"></div>';
			
	        html +='<div class="title">';
	       		html +='<h3>Créer un Devis</h3>';
	        html +='</div>';
	        html +='<div class="devisInfosComp">';
				html +='<form id="popup_epgloal__form">';
	            	html +='<fieldset>';
	            		html +='<span class="alert alert-info" >Vous pouvez personnaliser les informations uniquement pour ce ticket</span>';
	            		html +='<div>';
		            		html +='<div class="label">Libellé de la vente</div>';
		            		html +='<input type="text" value="" id="popup_global_title" ><div class="label" >Informations complémentaires</div>';
	            			html +='<textarea id="popup_global_textarea"></textarea>';
	            			html +='<span class="alert alert-info" >Exemple : C\'est un devis (ref, dates, clauses, etc...)</span>';
	            			html +='<div class="send_form" style="display:none;"><div class="label" >Adresse email d\'envoi :</div>';
								html +='<input type="text" value="'+ACAISSE.customer.email+'" id="send_form_email" >';
								html +='<div>';
									html +='<div id="abort_send" class="button">Annuler</div>';
									html +='<div id="confirm_send" class="button">Envoyer</div>';
								html +='</div>';
							html +='</div>';
	            		html +='</div>';
	            	html +='</fieldset>'; 
	            html +='</form>';
	            html +='<div class="devisButtons">'; 
	            //@TODO : Bouton "imprimer" / "Envoyer un mail"
	            	html +='<div class="button print_to_ticket"><i class="fa fa-print"></i><div class="label">Imprimer</div><div class="legend">(format ticket)</div></div>';
	            	html +='<div class="button print_big"><i class="fa fa-file"></i><div class="label">Imprimer</div><div class="legend">(format A4)</div></div>';
	            	html +='<div class="button send"><i class="fa fa-envelope"></i><div class="label">Envoyer</div><div class="legend">par e-mail</div></div>';
	            html +='</div>';
	        html +='</div>';
		html +='</div>';
		
		showEmptyPopup($("#ticketView"),html);
    });
    
    $('.isPending').on('click tap', function () {
        caisse.preorder.transformTicket('is_pending');
        
    });
    
    $('#emptyPopup').on('click tap', '#devisInfos .devisButtons .print_to_ticket', function (){
    	$('.closePopup').trigger('click');
    	caisse.preorder.transformTicket('is_devis');
    }); 
    
    $('#emptyPopup').on('click tap', '#devisInfos .devisButtons .print_big', function (){
    	$('.closePopup').trigger('click');
    	caisse.preorder.transformTicket('is_devis',true);
    });
	
	$('#emptyPopup').on('click tap', '#devisInfos .devisButtons .send', function (){
    	//$('.closePopup').trigger('click');
    	$('#devisInfos .overflow, #devisInfos .send_form').slideDown();
    	$('.devisButtons').slideUp();
    	
    	//
    });
    
    $('#emptyPopup').on('click tap', '#devisInfos #abort_send', function (e){
    	e.preventDefault();
    	$('#devisInfos .overflow, #devisInfos .send_form').slideUp();
    	$('.devisButtons').slideDown();
    });
    
    $('#emptyPopup').on('click tap', '#devisInfos #confirm_send', function (e){
    	e.preventDefault();
    	if(validate($('#send_form_email').val()) == true)
    	{
    		$('#emptyPopup .closePopup').click();
    		$(this).prop('disabled','disabled');
    		caisse.preorder.transformTicket('is_devis',true,true);
    	}
    	else
    	{
    		alert('Veuillez renseigner une adresse email correcte svp.');
    	}
    	
    });

}
function CaisseEmployee(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	
	
	this.getCurrentIdEmployee = function() {
		return $('#showEmployee').data('id_employee');		
	};
	
}
function CaisseCustomer(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	
	/*
	 * AFFICHAGE DES COMMANDES PRESTASHOP
	 */	
	$('body').on('click tap','#pageUser3 .openOrder',function() {
	
		var id_order = $(this).data('id');
		PRESTATILL_CAISSE.customer._loadOrderTpl(id_order);
    });
    
    /*
     * AFFICHAGE DE LA FACTURE
     */
    $('body').on('click tap','#pageUser3 .seeInvoice',function(e) {
    	var id_order = $(this).data('id');
    	$.ajax({
	        data: {
	            'action': 'LoadOrderInvoice',
	            'id_order': id_order
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	var response = response.response.link;

	        	//$("#popupNumeric").html(response);
	        	window.location.href = response;
	        	e.stopPropagation();
	        	e.preventDefault();
	            
	        } else {
	        	alert('Erreur k02011 : impossible de charger la facture. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	        }
	    }).fail(function () {
	        console.log('Erreur k02010 : impossible de charger la facture. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	    });
    });
    
    /*
     * AFFICHAGE DU TICKET DANS L'INTERFACE CLIENT
     */
    $('body').on('click tap','#pageUser3 .seeTicket',function(e) {
    	var id_ticket = $(this).data('id');
    	
    	$.ajax({
	        data: {
	            'action': 'LoadOrderTicket',
	            'id_ticket': id_ticket
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	var response = response.response.html;
	        	
	        	if($('.switcher').hasClass('show-product-list') == false)
	        	{
	        		showEmptyPopup($("#productList"),response);
	        	}
	        	else
	        	{
	        		showEmptyPopup($("#ticketView"),response);
	        	}
	        	
		    	/* Since 2.5.1 */
		    	$('#id_ticket_to_send').val(id_ticket);
	            
	        } else {
	        	alert('Erreur k02011 : impossible de charger le ticket. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	        }
	    }).fail(function () {
	        console.log('Erreur k02010 : impossible de charger le ticket. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	    });
    });
    
    /*
     * ADRESSE MAIL "POUBELLE" (si le client ne souhaite pas la donner) en se basant sur l'adresse mail du Client de passage
     */
    $('body').on('click tap','#placehldr',function(e) {
    	
    	var fake_mail = ACAISSE.customer.email;
    	var fake_base_mail = fake_mail.split('@');
    	var fake_name = $('#customer-edit-lastname').val();
    	var fake_firstname = $('#customer-edit-firstname').val();
    	$('#customer-edit-email').val(fake_firstname+'_'+fake_name+'@'+fake_base_mail[1]);
    	
    });
    
    /*
     * LORSQUE L'ON AJOUTE / MODIFIER UN PAIEMENT
     */
    $('body').on('click tap','.modify-payment',function(e) {
    	e.preventDefault();
    	
    	var id_order = $(this).data('id');
    	var pay_method = $('#payment_method').val();
    	var pay_info = $('#payment_id').val();
    	var pay_amount = $('#payment_amount').val();
    	
    	$.ajax({
	        data: {
	            'action': 'modifyPayment',
	            'id_order': id_order,
	            'method': pay_method,
	            'id_transaction': pay_info,
	            'amount': pay_amount
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	    	//console.log(response);
	    	
	        if (response.success !== false) {
	        	// On rafraichit la vue des paiements
	        	$.ajax({
			        data: {
			            'action': 'LoadOrderInfos',
			            'id_order': response.response.id_order,
			            'filter' : 0,
			        },
			        type: 'post',
			        dataType: 'json',
			    }).done(function (response) {
			        if (response.success !== false) {
			        	var response = response.response.html;
			        	$('#orderDetail .oDTable').html(response);
			        	
			        	$('#orderDetail .oDTable ul.notLoaded').removeClass('notLoaded').html('');
			        	
			        	// On rafraichit la liste des commandes
			        	loadOrders(0);
					}
				});
	            
	        } else {
	        	alert(response.error);
	        }
	    }).fail(function () {
	        alert('Erreur L03042 : Le montant du règlement n\'est pas correct.');
	    });
    });
    
    /*
     * LORSQUE L'ON AJOUTE / MODIFIER UN STATUT / ETAT DE VENTE
     */
    $('body').on('click tap','.modify-state',function(e) {
    	e.preventDefault();
    	
    	var id_order = $(this).data('id');
    	var id_order_state = $('#order_state').val();
    	
    	PRESTATILL_CAISSE.loader.start();
    	
    	$.ajax({
	        data: {
	            'action': 'modifyState',
	            'id_order': id_order,
	            'id_order_state': id_order_state,
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	    	//console.log(response);
	    	
	        if (response.success !== false) {
	        	// On rafraichit la vue des paiements
	        	$.ajax({
			        data: {
			            'action': 'LoadOrderInfos',
			            'id_order': response.response.id_order,
			            'filter' : 2,
			        },
			        type: 'post',
			        dataType: 'json',
			    }).done(function (response) {
			        if (response.success !== false) {
			        	var response = response.response.html;
			        	$('#orderDetail .oDTable').html(response);
			        	
			        	$('#orderDetail .oDTable ul.notLoaded').removeClass('notLoaded').html('');
			        	
			        	// On rafraichit la liste des commandes
			        	loadOrders(0);
			        	PRESTATILL_CAISSE.loader.end();
					}
				});
	            
	        } else {
	        	alert(response.error);
	        }
	    }).fail(function () {
	        alert('Erreur L03043 : l\'état de vente n\'est pas correct.');
	    });
    });
    
    /*
     * RETROUVER LES COMMANDES D'UN CLIENT AVEC LES FILTRES
     */
    
    $('#userEditContainer').on('click tap', '#order_filter .button', function(){
	
		var datas  = {};
		
		if($(this).hasClass('validate'))
		{
			$('[id^="filter-"]').each(function () {
		        datas[$(this).attr('id').replace('filter-','')] = $(this).val();
		    });
		}
		PRESTATILL_CAISSE.loader.start();
		loadOrders(0, datas);
	});
	
	$('#emptyPopup ').on('keyup', 'input.return', function(){
		//console.log([$(this).val(),$(this).parents('tr').find('.qty span').html()]);
		var qty = $(this).parents('tr').find('.qty span').html();
		var qty_refund = $(this).parents('tr').find('.qty_refund span').html();
		//console.log([qty,qty_refund,$(this).val()]);
		if($(this).parents('td').hasClass('qty'))
		{
			if($(this).val() > parseInt(qty-qty_refund))
			{
				$(this).val(parseInt(qty-qty_refund));
			}	
		}
		
		//console.log([$(this).parents('tr').find('.qty input').val(),$(this).parents('tr').find('.unit_amount input').val()])
		$(this).parents('tr').find('.total_cost input.return').val(parseFloat($(this).parents('tr').find('.qty input').val()*$(this).parents('tr').find('.unit_amount input').val()).toFixed(2));
		
	});
	
	
	$('#emptyPopup').on('click tap', '.popupActions', function(){
		PRESTATILL_CAISSE.loader.start();
		var params = {};
		$('#orderDetail tr').each(function(){
			if($(this).find('.actions input').prop('checked') == true)
			{
				params[$(this).data('id')] = {
					'qty':$(this).find('.qty input').val(),
					'price_ttc':$(this).find('.unit_amount input').val(),
				};
			}
		});
		
		caisse_ref.return.switchToReturnMode();
		
		var datas = {
	        'action' : 'SaveReturnPopupDatas',
	        'id_customer' : null,
	        'id_employee' : ACAISSE.customer.id,
	        'id_product' : null,
	        'id_product_attribute' : null,
	        'id_return' : PRESTATILL_CAISSE.return.ticket == null ? 0:PRESTATILL_CAISSE.return.ticket.id,
	        'params' : params,
 	   };
	   
		$.ajax({
	        data: datas,
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	_gotoTicketView();
	        	
	        	var request = response.request;
	        	var response = response.response;	  
	        	PRESTATILL_CAISSE.return.ticket = response.return_ticket;
	        	
	        	PRESTATILL_CAISSE.return.redrawTicket();
	        	
	        	$('.closePopup').trigger('click');
	        	PRESTATILL_CAISSE.loader.end();
	        		        		        					
	        } else {
	        	alert('Erreur o_ret0123 : impossible sauvegarder les informations');
	        }
	    }).fail(function () {
	        alert('Erreur o_ret0124 : impossible de communiquer avec le serveur');
	    });
		console.log(params);
	});
	
	$('#emptyPopup').on('click tap', '.actions input', function(){
		var is_checked = 0;
		if($(this).prop('checked')==true)
		{
			$(this).parents('tr').find('.return').show();
		} 
		else 
		{
			$(this).parents('tr').find('.return').hide();
		}
		
		$('#emptyPopup .actions input').each(function(){
			if($(this).prop('checked') == true)
			{
				is_checked++;
			}
		});
		
		if(is_checked>0)
		{
			$('#emptyPopup .popupActions').show();
		}
		else
		{
			$('#emptyPopup .popupActions').hide();
		}
	});
	
	this._loadOrderTpl = function (id_order = false, ean13 = false) 
	{
	    	$.ajax({
		        data: {
		            'action': 'LoadOrderTpl',
		            'id_order': id_order,
		            'ean13': ean13,
		        },
		        type: 'post',
		        dataType: 'json',
		    }).done(function (response) {
		        if (response.success !== false) {
		        	var response = response.response.html;
		        	
		        	
		        	if($('.switcher').hasClass('show-product-list') == false)
		        	{
		        		showEmptyPopup($("#productList"),response);
		        	}
		        	else
		        	{
		        		showEmptyPopup($("#ticketView"),response);
		        	}
		        	
		        	$('#orders_option').val('1');
			
					$('#orders_actions').uiSelector({
							default_value : 1,
					        onChange:function(filter_value){	
							
							$.ajax({
						        data: {
						            'action': 'LoadOrderInfos',
						            'id_order': id_order,
						            'filter' : filter_value,
						            'ean13' : ean13,
						        },
						        type: 'post',
						        dataType: 'json',
						    }).done(function (response) {
						        if (response.success !== false) {
						        	var response = response.response.html;
						        	$('#orderDetail .oDTable').html(response);
						        	
						        	$('#orderDetail .oDTable ul.notLoaded').removeClass('notLoaded').html('');
						        	$('#emptyPopup .popupActions').hide();
						        	
								}
							});
							
					    }
				    });
				    
				    $('#orderDetail ul').removeClass('notLoaded').html('');
				    $('#emptyPopup .popupActions').html('<i class="fa fa-share-square-o fa-fw"></i>');
				    
		            
		        } else {
		        	alert('Erreur k02011 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
		        }
		    }).fail(function () {
		        console.log('Erreur k02010 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
		    });
	}
	
	
}
function CaissePrinter(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 0;
	var caisse = caisse_ref;
	
	this.printEmptyTicket = function() {
		caisse_ref.printer.clearPrintQueue();
		printTicket('', 1, ''); //duplicata == la class CSS qui va aller s'ajouter sur #ticketPrint
		printQueue();
	};
	
	this.clearPrintQueue = function() {
		ACAISSE_print_queue = [];
	};
	
	
	
	
	//mise en place des écouteurs
	var _initHandler = function() {
		if(LOG_LEVEL>=10)
		{
			console.log(['initHandler',$('#footer'),$('#print_empty_ticket')]);
		}
		$('#print_empty_ticket').on('click tap', function(e){
			
			if(LOG_LEVEL>=5)
			{
				console.log('start printing empty ticket');
			}
			PRESTATILL_CAISSE.printer.printEmptyTicket();
		});
	};
	
	_initHandler();
	
}
var PRESTATILL_CAISSE;
$(function(){
	PRESTATILL_CAISSE = new Caisse();
});

function _recalcAndRefreshTotals() {
	var total_quantity = 0;
	var total_cost = 0;
	var total_cost_ht = 0;
	//total ticket
	if(ACAISSE.ticket.lines)
	{
		for (var i = 0; i < ACAISSE.ticket.lines.length; i++)
		{
			ticket_line = new TicketLine(ACAISSE.ticket.lines[i]);
			total_cost += ticket_line.line_price_ttc;
			total_cost_ht += ticket_line.line_price_ht;
			if(ACAISSE.mode_debogue === true)
			{
				console.log(ticket_line);
			}
			total_quantity += parseInt(ticket_line.quantity);
		}
	}
	ACAISSE.total_cost = total_cost;
	ACAISSE.total_cost_ht = total_cost_ht;
	//ACAISSE.cartModificationLocked = false;
	ACAISSE.total_quantity = total_quantity;
	_refreshTotals();
}

function _refreshTotals() {
	/*
	console.log(['comparaison refreshtot',
		ACAISSE.total_cost,
		ACAISSE.cart_tot_ttc,
		ACAISSE.total_cost_ht,
		ACAISSE.cartModificationLocked,
		ACAISSE.ticket
	]);
	*/
	
	if (ACAISSE.cartModificationLocked == true)
	{
		
		//console.log('TOTO 1');
		
		//console.log('ATTENTION on travail avec un cart');
		//console.log(ACAISSE);
		
		
		//console.log($('#totalCost').html
		//console.log($('#totalCost').html()+' <> '+formatPrice(ACAISSE.cart_tot_ttc) + '€ <small>NET</small>');
		if(ACAISSE.cart_tot_ttc > $('#totalCost').html())
		{
			$('#totalWithoutDiscount').html(ACAISSE.cart_tot).show();
		}
		
		if(ACAISSE.total_cost > ACAISSE.cart_tot_ttc)
		{
			$('#totalWithoutDiscount').html(formatPrice(ACAISSE.total_cost)+'€ <small>TTC (hors remises)</small>').fadeIn();
		}
		else
		{
			$('#totalWithoutDiscount').html('').fadeOut();
		}	
		//si net de tax
		if(ACAISSE.total_cost == ACAISSE.total_cost_ht)
		{
			$('#totalCost').html(formatPrice(ACAISSE.cart_tot_ttc) + '€ <small>NET</small>');
		}
		else
		{	
			$('#totalCost').html(formatPrice(ACAISSE.cart_tot_ttc) + '€ <small>TTC</small>');
		}
		//$('#totalReduction').html('Réduction de ' + formatPrice(ACAISSE.total_cost - ACAISSE.cart_tot_ttc) + '€ TTC incluse');
		$('#totalEstimationMention').hide();
		
		//var module_obj = {action:'reduce',reduce:formatPrice(ACAISSE.total_cost - ACAISSE.cart_tot_ttc),new_tot:(ACAISSE.cart_tot_ttc==0?'?':ACAISSE.cart_tot_ttc)};
		//modules_action(module_obj);
		
		
		var module_obj = {action:'tot',tot:formatPrice(ACAISSE.total_cost),id_ticket:ACAISSE.ticket.ticket_num};
		modules_action(module_obj);
		
		/*
		lcd_write_line('Réduction de ' + formatPrice(ACAISSE.total_cost - ACAISSE.cart_tot_ttc) + '€',2,undefinedValue,undefinedValue,module_obj);
		lcd_write_line(ACAISSE.cart_tot_ttc==0?'':'Total '+ formatPrice(ACAISSE.cart_tot_ttc) + '€',3,'right');
		*/
	}
	else
	{
		
		
		//console.log('TOTO 2');
		
		if(ACAISSE.mode_debogue === true)
			{console.log(modules_action);}
		
		
		
		//si net de tax
		if(ACAISSE.total_cost == ACAISSE.total_cost_ht && parseInt(ACAISSE.total_cost) > 0)
		{
			$('#totalCost').html(formatPrice(ACAISSE.total_cost_ht) + '€ <small>NET</small>'); //il y a ristourne
		}
		else
		{		
			$('#totalCost').html(formatPrice(ACAISSE.total_cost) + '€ <small>TTC</small>');
		}
		
		$('#totalReductionva').html('');
		$('#totalEstimationMention').show();
		
		var module_obj = {action:'tot',tot:formatPrice(ACAISSE.total_cost),id_ticket:ACAISSE.ticket.ticket_num};
		modules_action(module_obj);
		/*
		lcd_write_line(' ',2,undefinedValue,undefinedValue,module_obj);
		lcd_write_line(ACAISSE.total_cost==0?'':'Total '+ formatPrice(ACAISSE.total_cost) + '€',3,'right');
		*/          	

	}
	
	$('#totalItem').html(ACAISSE.total_quantity == 0 ? '0' : ACAISSE.total_quantity > 1 ? ACAISSE.total_quantity + '' : '1');

}


function registerDiscountOnCart(amount,type, extra_datas) {
	
	//console.log(amount);
	if(isNaN(amount))
	{
		amount = 0;
	}
	
	$.ajax({
        data: {
            'action': 'RegisterQuickDiscountOnGlobalCart',
            'id_ticket': ACAISSE.ticket.id,
            'id_force_group':ACAISSE.id_force_group,
            'amount' : amount,
            'type' : type,
            'extra_datas' : extra_datas
        },
        type: 'post',
        dataType: 'json',
    }).done(function (response) {
        if (response.success !== false) {
        	
        	var response = response.response;
        
			ACAISSE.cart_tot = response.cart_tot;
			ACAISSE.cart_tot_ht = response.cart_tot_ht;
			ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
			
			ACAISSE.ticket = response.ticket;
			ACAISSE.cart = response.cart;
			
			// On execute le hook pour mettre à jour les tickets lines
			//PRESTATILL_CAISSE.executeHook('actionUpdatePrestatillTicketGlobalDiscount',{'id_ticket':ACAISSE.ticket.id});
			
			//_switchToTicket(response.ticket.id,false,true);
			
			refreshPrices(response.cart_products);
            
            $('#reductionGlobalOnCart .pastille').remove();
            
            if (amount!=0) {
            	$('#reductionGlobalOnCart').prepend('<span class="pastille big"><span class="label">Remise appliquée :</span> <br />-'+amount+type+' <br /><span class="modify">modifier</span></span>');
            }
            
            var module_obj = {action:'tot',tot:formatPrice(response.cart_tot_ttc),id_ticket:response.ticket.ticket_num};
			modules_action(module_obj);
            
            
        } else {
        	alert('Erreur k_00010 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
        	
        }
    }).fail(function () {
        alert('Erreur k00010 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
    });
}

function registerDiscountOnCartLine(amount,type,id_product,id_product_attribute,id_ticket_line, extra_datas) {
	
	if(ACAISSE.cartModificationLocked !== true)
	{
		alert('Error 20312 : le ticket n\'est pas prêt');
	}
	else
	{
		$.ajax({
	        data: $.extend({},{
	            'action': 'RegisterQuickDiscountOnCartLine',
	            'id_ticket': ACAISSE.ticket.id,
	            'amount' : amount,
	            'type' : type,
	            'id_product' : id_product,
	            'id_product_attribute' : id_product_attribute,
	            'id_ticket_line' : id_ticket_line
	        },extra_datas),
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	var response = response.response;
	        	/*console.log([
	        		'registerDiscountOnCartLine response (success true)',
	        		response
	        	]);*/
				ACAISSE.cart_tot = response.cart_tot;
				ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
				ACAISSE.cart_tot_ht= response.cart_tot_ht;
				ACAISSE.ticket = response.ticket;
				
				refreshPrices(/*response.cart_products*/);
				
				_refreshTicket(null,false);
				
	        } else {
	        	alert('Erreur k003106 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	        }
	    }).fail(function () {
	        console.log('Erreur k0003107 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	    });
    }
}

function loadDiscount() {
	$('#discountContainer').html('<div><i class="fa fa-spinner fa-spin"></i></div>');
    
    $.ajax({
        data: {
            'action': 'LoadDiscountForCustomer',
            'id_customer': ACAISSE.customer.id,
        },
        type: 'post',
        dataType: 'json',
    }).done(function (response) {
        var response = response.response;
        if (response.success != false) {
            var html = '';
            var bons = response.bons;
            
            if (bons.length==0) {
            	$('#discountContainer').html('<span class="no-discount">Ce client ne possède pas de bons d\'achat.</span>');
            	return false;
            }
            
            var cartRules = [];
            if (ACAISSE.ticket.id_cart_rules!=undefined) {
            	cartRules = ACAISSE.ticket.id_cart_rules.split(';');
            }
            
            for (var i=0; i<bons.length; i++) {
            	html += '<div class="discount"><div class="inner">'
            	  html += '<i class="fa fa-scissors"></i>';
            		html += '<div><span class="qte">' + bons[i].qte + '</span></div>';
            		html += '<div class="left_block"><span class="price">' + (bons[i].amount.indexOf('%')==-1?formatPrice(bons[i].amount)+'€':parseInt(bons[i].amount)+'%') + '<span class="immediat">DE REMISE IMMEDIATE</span></span>';
            		html += '<div class="date_box '+(bons[i].validityDate.to<bons[i].validityDate.now?'tooLate':'')+'"><span class="date '+(bons[i].validityDate.from>bons[i].validityDate.now?'notYet':'')+'">du '+Date.toString(bons[i].validityDate.from)+'</span><br /><span class="date '+(bons[i].validityDate.to<bons[i].validityDate.now?'tooLate':'')+'">au '+Date.toString(bons[i].validityDate.to)+'</span></div>';
            		html += '<div class="conditions">';
            		if (bons[i].minimum_amount>0) {
            			html += 'à partir de <span class="minimum_amount"><b>' + formatPrice(bons[i].minimum_amount) + '€</b></span> d\'achat';
        			}
            		html += '<div>Utilisation partielle : <b>' + (bons[i].is_partial==1?'oui':'non') + '</b></div>';
            		html += '</div></div>';
            		html += '<div class="infos">';
            		html += '<h4>'+bons[i].name+'</h4>';
            		html += '<span class="descript">'+bons[i].description+'</span>';
            		html += '</div>';
        			html += '<button data-is_active="'+(cartRules.indexOf(bons[i].id_cart_rule)==-1?'0':'1')+'" '+(bons[i].qte==0?'disabled ':'')+'onclick="if ($(this).data(\'is_active\')==\'0\') {useDiscount(\''+bons[i].code+'\',\''+bons[i].id_cart_rule+'\',this);} else {deleteDiscount(\''+bons[i].code+'\',\''+bons[i].id_cart_rule+'\',this);}">'+(cartRules.indexOf(bons[i].id_cart_rule)==-1?'Faire valoir':'Retirer le bon')+'</button>';
            	html += '</div></div>';
            }
            $('#discountContainer').html(html);
        } else {
        	alert('Erreur k_00001 : impossible d\'obtenir la liste des bons de réductions du serveur. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
        }
    }).fail(function () {
        alert('Erreur k00001 : impossible d\'obtenir la liste des bons de réductions du serveur. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
    });
}

function deleteDiscount(code, id_cart_rule, _this) {
	_this.disabled = true;
	$.ajax({
        data: {
            'action': 'DeleteDiscount',
            'id_ticket':ACAISSE.ticket.id,
            'id_cart_rule':id_cart_rule
        },
        type: 'post',
        dataType: 'json',
    }).done(function (response) {
        if (response.success != false) {
			var response = response.response;
			
			ACAISSE.cart_tot = response.cart_tot;
			ACAISSE.cart_tot_ht = response.cart_tot_ht;
			ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
			
			ACAISSE.ticket = response.ticket;
			ACAISSE.cart = response.cart;
			
			refreshPrices(response.cart_products);
			
			var product_list = [];
			for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
				product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
				product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
				product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
			}
			if(ACAISSE.mode_debogue === true) {console.log(product_list);}
			var module_obj = {action:'switch_ticket',product_list:product_list};
			
			modules_action(module_obj);
			
			_refreshTicket(null,false);
			
			sendNotif('greenFont','<i class="fa fa-check"></i>Bon '+code+' retiré');
			
			_this.disabled = false;
			$(_this).data('is_active','0');
			$(_this).html('Faire valoir');
			
			$('#reductionGlobalOnCart .pastille').remove();
			
			closeAllPanel();
			
			//refreshPrices(response.cart_products);
        } else {
        	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>'+response.error);
        }
    }).fail(function () {
        sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Bon '+code+' non retiré');
    });
}

function useDiscount(code, id_cart_rule, _this) {
	_this.disabled = true;
	
	// on valide le ticket en commande	
	if (!ACAISSE.cartModificationLocked) {    		
		//$('.button.validTicket').click();
		_validateCurrentTicket(false);
	}
	
	// on doit passer par un timeout pour attendre la validation du payment
	function checkToApply() {
		if ($('#orderButtonsPanel').css('display')=='none') {
			setTimeout(checkToApply,50);
		} else {
			useDisc();
		}
	}
	
	function useDisc() {
		$.ajax({
	        data: {
	            'action': 'UseDiscount',
	            'id_ticket':ACAISSE.ticket.id,
	            'id_cart_rule':id_cart_rule
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success != false) {
				var response = response.response;
				
				ACAISSE.cart_tot = response.cart_tot;
				ACAISSE.cart_tot_ht = response.cart_tot_ht;
				ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
				
				ACAISSE.ticket = response.ticket;
				ACAISSE.cart = response.cart;
				
				
				refreshPrices(response.cart_products);

				var product_list = [];
				for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
					product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
					product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
					product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
				}
				
				if(ACAISSE.mode_debogue === true) {console.log(product_list);}
				var module_obj = {action:'switch_ticket',product_list:product_list};
				
				modules_action(module_obj);
				
				_refreshTicket(null,false);
				
				sendNotif('greenFont','<i class="fa fa-check"></i>Bon '+code+' appliqué');
				
				_this.disabled = false;
				$(_this).data('is_active','1');
				$(_this).html('Retirer le bon');
				
				closeAllPanel();
				
				//refreshPrices(response.cart_products);
				$('#reductionGlobalOnCart').prepend('<span class="pastille big"><span class="label">Remise appliquée :</span> <br />fidélité<br /></span>');
				
				/**@todo Kévin : Lancer une action vers les afficheurs pour afficher le bon utilisé ?*/
	        } else {
	        	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>'+response.error);
	        }
	    }).fail(function () {
	        sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Bon '+code+' non appliqué');
	    });
    }
    
	setTimeout(checkToApply,50);
}

var layouts = {
    azerty: [
        {
            normal: ['`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 'backspace'],
            shift: ['~', '!', '@', '#', '%', '^', '&', '*', '(', ')', '_', '+', '=', 'backspace'],
        },
        {
            normal: ['tab', 'a', 'z', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\\'],
            shift: ['tab', 'A', 'Z', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\\'],
        },
        {
            normal: ['capsLock', 'q', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', '\'', 'return'],
            shift: ['capsLock', 'Q', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', '%', 'return'],
        },
        {
            normal: ['shift', 'w', 'x', 'c', 'v', 'b', 'n', ',', '.', ';', '/', 'reset'],
            shift: ['shift', 'W', 'X', 'C', 'V', 'B', 'N', '?', '.', '/', '§', 'reset'],
        },
        {
            normal: ['space'],
            shift: ['space'],
        },
    ],
    qwerty: [
        {
            normal: ['`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 'backspace'],
            shift: ['`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 'backspace'],
        },
        {
            normal: ['tab', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\\'],
            shift: ['tab', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\\'],
        },
        {
            normal: ['capsLock', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', 'return'],
            shift: ['capsLock', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ';', '\'', 'return'],
        },
        {
            normal: ['shift', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', 'shift'],
            shift: ['shift', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.', '/', 'shift'],
        },
        {
            normal: ['space'],
            shift: ['space'],
        },
    ],
    numeric: [
        {
            normal: ['backspace'],
            shift: ['backspace'],
        },
        {
            normal: ['7', '8', '9'],
            shift: ['7', '8', '9'],
        },
        {
            normal: ['4', '5', '6'],
            shift: ['4', '5', '6'],
        },
        {
            normal: ['1', '2', '3'],
            shift: ['1', '2', '3'],
        },
        {
            normal: ['0', 'return'],
            shift: ['0', 'return'],
        },
    ],
    floatNumeric: [
        {
            normal: ['backspace'],
            shift: ['backspace'],
        },
        {
            normal: ['7', '8', '9'],
            shift: ['7', '8', '9'],
        },
        {
            normal: ['4', '5', '6'],
            shift: ['4', '5', '6'],
        },
        {
            normal: ['1', '2', '3'],
            shift: ['1', '2', '3'],
        },
        {
            normal: ['0', '.', 'floatReturn'],
            shift: ['0', '.', 'floatReturn'],
        },
    ],
    money: [
        {
            normal: ['backspace'],
            shift: ['backspace'],
        },
        {
            normal: ['7', '8', '9'],
            shift: ['7', '8', '9'],
        },
        {
            normal: ['4', '5', '6'],
            shift: ['4', '5', '6'],
        },
        {
            normal: ['1', '2', '3'],
            shift: ['1', '2', '3'],
        },
        {
            normal: ['0', '.','reset'],
            shift: ['0', '.','reset'],
        },
    ]
};

(function ($) {
    // Declaration de notre plugin
    $.lbabKeyboard = function (element, options) {

        // Mise en place des options par défaut
        var defaults = {
            'layout' : 'azerty',
            'inputable' : null,
            min : false,
            max : false,
            //'createInputable' : true,
            onType : function(){}, //callback
            onReset : function(){},
            onReturn : function() {},
        };

        //instance de notre plugin
        var plugin = this;
        
        // les options de notre plugin
        plugin.options = {};

        // Référence à l'élément jQuery que le plugin affecte
        var $element = $(element);
        // Référence à l'élément HTML que le plugin affecte
        var element = element;
        
        var keyLook = {
            shift: false,
            capsLock: false,
            uppercase: false,
        };
        
        var functionKeys = {
            backspace: {
                text: 'C',
                class : 'double',
            },
            return: {
                text: 'OK',
                class : 'double',
            },
            floatReturn : {
            	text: 'OK'
            },
            shift: {
                text: 'Shift',
                class : 'triple',
            },
            space: {
                text: 'Espace',
                character: ' ',
            },
            capsLock: {
                text: 'Caps lock',
                class : 'double',
            },
            tab: {
                text: 'Tab',
                class : 'double',
                character: '\t',
            },
            reset: {
                text: 'Reset',
            }
        };
        // La méthode dite "constructeur" qui sera appelée
        // lorsque l'objet sera crée
        plugin.init = function () {

            // on stocke les options dans un objet en fusionnant
            // les options par defaut et celle ajoutée en parametre
            plugin.options = $.extend({}, defaults, options);
            /*private methode*/
            createKeyboard(plugin.options.layout);  
            event();
        };
        
        createKeyboard = function (layout) {
            $element.addClass('lbabK');
            $element.addClass(layout ==='floatNumeric'?'floatNumeric':(layout!=='numeric' && layout!=='money')?'alpha':'numeric');
            $element.addClass(layout);
            var keyboardContainer = $('<ul/>');
            layouts[layout].forEach(function (line, index) {
            //layouts[layout][style].forEach(function (line, index) {
                var lineContainer = $('<li/>').addClass('lbabK-line');
                lineContainer.append(createLine(line));
                keyboardContainer.append(lineContainer);
            });
            $element.html('').append(keyboardContainer);
            
            if(plugin.options.inputable === null){
                createInput();
            }
            else if(plugin.options.inputable !== null && plugin.options.inputable instanceof jQuery){
                //console.log('input dans inputable');
            }
        };
        
        var createLine = function (line) {
            var lineContainer = $('<ul/>');
            
            line['normal'].forEach(function (key, indexKey) {
                //touche symbol
                var shiftKey = line['shift'][indexKey];
                var keyContainer = $('<li/>').addClass('lbabK-key unselectable');
                
                if (functionKeys[key] || functionKeys[shiftKey]) {
                    keyContainer.addClass(key).html(functionKeys[key].text).data('lbabK-command', shiftKey);
                }
                else{
                    var keyCell = $('<span/>').html(key).data('lbabK-command', key);
                    var shiftCell = $('<span/>').addClass('none lbabK-shift').html(shiftKey).data('lbabK-command', shiftKey);
                    if(/[a-zA-Z]/.test(key) || /[a-zA-Z]/.test(shiftKey)){ //si c'est une lettre
                        keyCell.addClass('lbabK-letter');
                        shiftCell.addClass('lbabK-letter');
                    }
                     else{
                        keyCell.addClass('lbabK-symbol');
                        shiftCell.addClass('lbabK-symbol');
                    }
                    keyContainer.append(keyCell, shiftCell);
                }
                lineContainer.append(keyContainer);
            });
            
            return lineContainer;
        };
        
        var createInput = function() {
            var inputable = $('<input/>').addClass('lbabK-inputable ' + plugin.options.layout);
            plugin.options.inputable = inputable;
            $element.prepend(inputable);
        };
        
        var event = function(){
            $element.on('click tap','.lbabK-key',function(){
                var Action = new eventAction();
                if(functionKeys.hasOwnProperty($(this).data('lbabK-command'))){
                    //si on clique sur un touche d'action (functionKeys)
                    //on apelle une fonction dynamic privée pour l'action clique
                    Action[$(this).data('lbabK-command')+"Event"]();
                }
                else{
                    //sinon, c'est une touche normal
                    Action.pressKey($('span:visible',this).html());
                }
            });
        };
        
        var eventAction = function() {
            this.inputable = plugin.options.inputable;
            this.character = '';
            this.addCharacter = function(){
                var sentence = this.inputable.val();
                this.inputable.val(sentence + this.character);
                //this.inputable.focus();
                if(keyLook.shift === true){
                    this.shiftEvent();
                }
                checkErrors();
                //plugin.options.onType.call(this);
                callBack('onType',this.inputable.val());
            };
            this.backspaceEvent = function () {
                //s'il l'input n'est pas vide
                if (this.inputable.val().length > 0) {
                    this.inputable.val(this.inputable.val().substring(0, (this.inputable.val().length - 1)));
                }
                checkErrors();
                callBack('onType',this.inputable.val());
            };
            this.returnEvent = function () {
            	if(!$element.hasClass('error'))
            	{
               	 	callBack('onReturn',this.inputable.val());
               	}
            };
            this.floatReturnEvent = function() {
            	if(!$element.hasClass('error'))
            	{
               	 	callBack('onReturn',this.inputable.val());
               	}
            };
            this.shiftEvent = function(){
                keyLook.uppercase === true ? keyLook.uppercase = false : keyLook.uppercase = true;
                keyLook.shift === true ? keyLook.shift = false : keyLook.shift = true;
                $('.lbabK-key span',$element).toggleClass('none');
                   
            };
            this.spaceEvent = function(){
                this.character = functionKeys.space.character;
                this.addCharacter();
            };
            this.capsLockEvent = function(){
                keyLook.uppercase === true ? keyLook.uppercase = false : keyLook.uppercase = true;
                $('.lbabK-key span',$element).toggleClass('none');
            };
            this.tabEvent = function(){
                this.character = functionKeys.tab.character;
                this.addCharacter();
            };
            this.resetEvent = function(){
                this.inputable.val('');
                callBack('onReset');
            };
            this.pressKey = function(htmlKey){
                this.character = htmlKey;
                if(keyLook.uppercase === true){
                    this.character = this.character.toUpperCase();
                }
                this.addCharacter();
            };
        };
        
        var callBack = function(func,args){
            if (typeof plugin.options[func] === 'function' && plugin.options[func] !== '') {
                 return plugin.options[func](args,$element);
            }
            else{
                alert('bad function');
                return 'bad function';
            }
        };
        
        var checkErrors = function(){
        	var hasError = false;
        	if(
        		plugin.options.min !== false
        		&& plugin.options.inputable.val() < plugin.options.min
        	)
        	{
        		hasError = true;
        	}
        	if(
        		plugin.options.max !== false
        		&& plugin.options.inputable.val() > plugin.options.max
        	)
        	{
        		hasError = true;        		
        	}
        	
        	if(hasError === true)
        	{
        		$element.addClass('error');
        	}
        	else
        	{
        		$element.removeClass('error');
        	}
        };
        
        /*function public*/
        plugin.setMin = function(min) {
            plugin.options.min = min;
        };
        plugin.setMax = function(max) {
            plugin.options.max = max;
        };
        plugin.unsetMin = function() {
            plugin.options.min = false;
        };
        plugin.unsetMax = function() {
            plugin.options.max = false;
        };
        
        plugin.revalidateValue = function() {
        	checkErrors();
        };

        // On appele la méthode publique init
        // qui va se charger de mettre en place
        // toutes les méthodes de notre plugin
        // pour qu'il fonctionne
        plugin.init();

    };

    // On ajoute le plugin à l'objet jQuery $.fn
    $.fn.lbabKeyboard = function (options) {

        // Pour chacuns des élément du dom à qui
        // on a assigné le plugin
        return this.each(function () {
            // Si le plugin n'as pas deja été assigné à l'élément
            if (undefined == $(this).data('lbabKeyboard')) {

                // On crée une instance du plugin
                // avec les options renseignées
                var plugin = new $.lbabKeyboard(this, options);

                // on stocke une référence de notre plugin
                // pour pouvoir accéder à ses méthode publiques
                // (non utilisé dans ce plugin)
                $(this).data('lbabKeyboard', plugin);

            }

        });

    };

})(jQuery);
var usb_screen; // page html de l'ecran usb client
var windows = [];

var hooks = {
	'payment_exact':[],
  	'payment_overdue':[],
  	'payment_lack':[],
  	'reduce' :[],
  	'demo':[],
  	'typed_object':[],
  	'switch_ticket':[],
  	'tot':[]
};


function modules_action(module_obj) {
	
	//console.log({'modules_action' : module_obj});
	
	var hooksToCall = hooks[module_obj.action];
	for (var i=0; i<hooksToCall.length; i++) {
		if (hooksToCall[i].window.window) { // si la fenetre existe toujours
			
			// console.log('test : ');
			// console.log('test : ' + hooksToCall[i].func);
			
			hooksToCall[i].window.actions[hooksToCall[i].func](module_obj); // on l'utilise
		} else { // sinon 
			hooksToCall.splice(i,1); // on supprime le hook
		}
	}
}

/***
	var module_obj = {action:'payment_exact'};
	var module_obj = {action:'payment_overdue',received:formatPrice(received),diff:formatPrice(diff)};
	var module_obj = {action:'payment_lack',received:formatPrice(received),diff:formatPrice(diff)};
	var module_obj = {action:'demo'};
	MODIFIED AND PUBLIC CALL DEPRECATE var module_obj = {action:'typed_object',product:p,id_attribute:(a!=null?id_product_attribute:null),qte:qte};
	var module_obj = {action:'reduce',reduce:formatPrice(total_cost - ACAISSE.cart_tot),new_tot:(ACAISSE.cart_tot==0?'?':ACAISSE.cart_tot)};
	var module_obj = {action:'tot',tot:formatPrice(total_cost)};
	MODIFIED var module_obj = {action:'switch_ticket',ticket:ticket};
***/

function loadOrders(nb_order, filter = {}) {
	$('#pageUser3').html('<div><i class="fa fa-spinner fa-spin"></i></div>');
	$.ajax({
		data : {
			'action' : 'LoadCustomerOrders',
			'id_customer' : ACAISSE.customer.id,
			'from_nb_order' : nb_order,
			'id_pointshop' : ACAISSE.id_pointshop,
			'filter' : filter,
		},
		type : 'post',
		dataType : 'json',
	}).done(function(response) {
		if (response.success != false) {
			var response = response.response;
			var orders = response.orders;
			var gift_cards = response.gift_cards;
			var html = '<table data-nb_order="0" cellspacing="0">';
			var filter = response.filter;
			var pointshop_selected = '';
			
			//html +='<div class="col-xs-6">'+acaisse_pointshops.pointshops[ACAISSE.id_pointshop].name+'</div>';

			/* MISE EN PLACE D'UN FILTRE */
			html +='<div id="order_filter">';
			html +='<div class="filter col-xs-2"><input placeholder="N° de commande" type="number" value="' +filter.id_order+ '" id="filter-id_order"></div>';
			html +='<div class="filter col-xs-2"><input placeholder="N° de ticket" type="number" value="' +filter.id_ticket+ '" id="filter-id_ticket"></div>';
			html +='<div class="filter col-xs-2"><input placeholder="Date de commande" type="date" value="' +filter.date+ '" id="filter-date"></div>';
			
			html +='<div class="filter col-xs-4">';
			if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.see_all_orders == false)
			{
				filter.id_pointshop=='0'?pointshop_selected='selected="selected"':pointshop_selected='';
				html +='<select id="filter-id_pointshop">';
					html +='<option value="-1">Tous les points de ventes</option>';
					html +='<option '+pointshop_selected+' value="0">En ligne</option>';
					pointshop_selected = '';
					for (var p in acaisse_pointshops.pointshops) {
						//console.log([parseInt(filter.id_pointshop),parseInt(acaisse_pointshops.pointshops[p].datas.id)]);
						parseInt(filter.id_pointshop)==parseInt(acaisse_pointshops.pointshops[p].datas.id)?pointshop_selected='selected="selected"':pointshop_selected='';
						html +='<option '+pointshop_selected+'" value="'+acaisse_pointshops.pointshops[p].datas.id+'">'+acaisse_pointshops.pointshops[p].name+'</option>';
					}
				html +='</select>';
				
			}
			else
			{
				html +='<input type="text" value="'+acaisse_pointshops.pointshops[ACAISSE.id_pointshop].name+'" readonly />';
			}
			html +='</div>';
			
			html +='<div class="col-xs-2 filter">';
				html +='<div class="button validate">FILTRER</div>';
				html +='<div class="button cancel">EFFACER</div>';
			html +='<div class="clearfix"></div>';

			html += '<thead><tr><th class="commande">N° de CMD</th><th>N° de ticket</th><th>Point de vente</th><th>Date</th><th>Etat</th><th>Montant total</th><th>Montant payé</th><th>Actions</th></tr></thead>';
			var buffer_totals_by_id_ticket = [];
			
			if(orders.length > 0)
			{
				/*
				for (var i = 0; i < orders.length; i++) {
					if (buffer_totals_by_id_ticket[orders[i].id_ticket] == undefined) {
						buffer_totals_by_id_ticket[orders[i].id_ticket] = {
							order : orders[i],
							total_paid : parseFloat(orders[i].total_paid),
							total_paid_real : parseFloat(orders[i].total_paid_real)
						};
					} else {
						//il y avait deja une commande pour ce ticket
						//on mets à jour les totaux
						buffer_totals_by_id_ticket[orders[i].id_ticket] = {
							order : orders[i],
							total_paid : buffer_totals_by_id_ticket[orders[i].id_ticket].total_paid + parseFloat(orders[i].total_paid),
							total_paid_real : buffer_totals_by_id_ticket[orders[i].id_ticket].total_paid_real + parseFloat(orders[i].total_paid_real)
						};
					}
				}*/
				
				for (var id = 0; id < orders.length; id++) {
					//console.log(orders[i]);
					var i = id;
					
					if (true/*buffer_totals_by_id_ticket[orders[i].id_ticket] != undefined*/) {
						if (true/*buffer_totals_by_id_ticket[orders[i].id_ticket].order.id_order == orders[id].id_order*/) {
							//orders[id].total_paid = buffer_totals_by_id_ticket[orders[i].id_ticket].total_paid;
							//orders[id].total_paid_real = buffer_totals_by_id_ticket[orders[i].id_ticket].total_paid_real;
							html += '<tr data-id_order='+orders[id].id_order+'>';
							html += '<td>' + orders[id].id_order + '</td>';
							if (acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
								html += '<td>' + orders[id].id_ticket + '</td>';
							}
							else
							{
								html += '<td>--</td>';
							}
	
							if (acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
								html += '<td><span class="pointshop_name">' + acaisse_pointshops.pointshops[orders[id].id_pointshop].name + '</span></td>';
							} else {
								html += '<td><span class="pointshop_name">En ligne</span></td>';
							}
							html += '<td><span class="date">' + Date.toString(orders[id].date_add) + '</span></td>';
							html += '<td><span class="state">' + orders[id].order_state + '</span></td>';
							html += '<td><span class="paid ' + ((parseFloat(formatPrice(orders[id].total_paid)) > parseFloat(formatPrice(orders[id].total_paid_real))) ? 'notAllPaid' : '') + ' ' + ((parseFloat(formatPrice(orders[id].total_paid)) < parseFloat(formatPrice(orders[id].total_paid_real))) ? 'tooMuch' : '') + '">' + formatPrice(orders[id].total_paid) + '€</span></td>';
							html += '<td><span class="paid_real ' + ((parseFloat(formatPrice(orders[id].total_paid)) > parseFloat(formatPrice(orders[id].total_paid_real))) ? 'notAllPaid' : '') + ' ' + ((parseFloat(formatPrice(orders[id].total_paid)) < parseFloat(formatPrice(orders[id].total_paid_real))) ? 'tooMuch' : '') + '">' + formatPrice(orders[id].total_paid_real) + '€</span></td>';
							html += '<td class="actions">';
							if (parseFloat(orders[id].total_paid_real) < parseFloat(orders[id].total_paid) && response.id_order_state_cancelled != orders[id].id_order_state && acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
								html += '<button onclick="payRestOfOrder(' + orders[id].id_order + ');"><i class="fa fa-money" aria-hidden="true"></i> <span class="btLabel">PAYER</span></button>';
							} else if (parseFloat(orders[id].total_paid_real) > parseFloat(orders[id].total_paid) && response.id_order_state_cancelled != orders[id].id_order_state) {
								/**@todo : Se démerder pour degager le surplut et donner une reduc ?*/
								//html += '<button disabled="disabled" style="opacity:0.2;"><i class="fa fa-undo"></i> <span class="btLabel">AVOIR</span></button>';
							}
							
							html += PRESTATILL_CAISSE.executeHook('displayPrestatillCustomerOrdersLine',{'id_order':orders[id].id_order});
							
							if (acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
								html += '<button class="seeTicket" data-id="' + orders[id].id_ticket + '"><i class="fa fa-ticket"></i> <span class="btLabel">TICKET</span></button>';
							} else {
								html += '<button disabled="disabled" style="opacity:0.2" data-id="0"><i class="fa fa-ticket"></i> <span class="btLabel">TICKET</span></button>';
							}
							
							/*
							 if (params.orders.acaisse_invoice_is_active == 1 && orders[id].valid == 1 && orders[id].invoice_number > 0) {
							 html += '<button  class="seeInvoice" data-id="' + orders[id].id_order + '"><i class="fa fa-file-text"></i> <span class="btLabel">FACTURE</span></button>';
							 }
							 else
							 {
							 html += '<button disabled="disabled" style="opacity:0.2"  class="seeInvoice" data-id="0"><i class="fa fa-file-text"></i> <span class="btLabel">FACTURE</span></button>';
							 }*/
							html += '<button  class="openOrder" data-id="' + orders[id].id_order + '"><i class="fa fa-eye"></i> <span class="btLabel">VOIR</span></button>';
							html += '</td></tr>';
						}
					}
				}
			}
			else
			{			
				html +='<tr><td colspan="8" class="no_order">Aucun résultat</td></tr>';
			}
			html += '</table>';
			$('#pageUser3').html(html);
			$('#userEditContainer li[data-page-id="pageUser3"] .pastille').html(response.nb_total_orders);
			PRESTATILL_CAISSE.loader.end();
		} else {
			alert('Erreur k_00011 : impossible d\'obtenir la liste des commandes du client. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
		}
	}).fail(function() {
		alert('Erreur k00011 : impossible d\'obtenir la liste des commandes du client. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	});
}

function payRestOfOrder(id_order) {
	$.ajax({
		data : {
			'action' : 'ReloadOrderToFinishPayment',
			'id_order' : id_order,
		},
		type : 'post',
		dataType : 'json',
	}).done(function(response) {
		if (response.success) {
			_switchToTicket(response.response.id_ticket, function() {
				_gotoPaymentPanel();
			}, false);
		} else {
			alert('Erreur, impossible de recharger la commande, si l\'erreur persiste veuillez contacter un administrateur');
		}
	});
}

var lcd_ip = params.customerScreen.lcd_ip; // on a plus besoin d'un false
var lcd_line_length = 20;
var lcd_queue = [];
var do_lcd_queue_in_action = false;

/*
 * param string text (20 char) 
 */
function lcd_write_line(text,line,align,prefix, module_obj) {
	if (params.customerScreen.mode.indexOf('lcd')!=-1) {
		lcd_queue.push([text,line,align,prefix]);
		if(!do_lcd_queue_in_action) { do_lcd_queue(); }
	}
	
	if (module_obj) {
		modules_action(module_obj);
	}
};

function do_lcd_queue() {
	if(lcd_queue.length>0)
	{
		do_lcd_queue_in_action = true;
		var next = lcd_queue.shift();
		_lcd_write_line(next[0],next[1],next[2],next[3]);
	}
	else
	{
		do_lcd_queue_in_action = false;
	}
}

function _lcd_write_line(text,line,align,prefix) {
	
	if(lcd_ip === false)
	{
		return;
	}
	
	if(typeof(align) == 'undefined')
	{
		align = 'left';
	}
	if(typeof(prefix) == 'undefined')
	{
		prefix = '';
	}
	if(typeof(text) == 'undefined')
	{
		return false;
	}
	
	text = text.sansAccent();
	
	line = typeof(line) == 'undefined' ? 0 : line;
	line = Math.max(0,Math.min(3,line));		
	
			
	if(text.length < lcd_line_length)
	{
		if(align == 'right')
		{
			text = '                                '+text;
			text = text.substring(text.length-lcd_line_length,text.length);
		}
		else
		{
			text += ' '.repeat(lcd_line_length); //on complete la ligne
			text = text.substr(0,lcd_line_length);
		}	
	}
	else if (text.length > lcd_line_length)
	{
		text = text.substr(0,lcd_line_length-3) + '...';			
	}
	
	var prefix_len = prefix.length;
	if(prefix_len>0)
	{
		if(align == 'right')
		{
			text = prefix + text.substring(prefix_len,lcd_line_length);
		}
	}
	
	$.ajax({
		url:'http://'+lcd_ip+'/?lcdpos00'+line,
		method:'GET',
		crossDomain:true,			
	}).done(function(msg){ 	
		console.log('ecriture de "'+text+'" à la ligne n°'+line);
		$.ajax({
			url:'http://'+lcd_ip+'/?lcdwrite'+text+'|',
			method:'GET',
			crossDomain:true,		
		}).done(do_lcd_queue);
	});
}
function printZList(dateToPrint) {
	// si on ne demande pas de dtae en particulier on imprime la bande Z du jour
	if (dateToPrint==undefined) {
		dateToPrint = getEngDate(new Date(),false);
	}
	
	PRESTATILL_CAISSE.loader.start();
	
	$.ajax({
        data: {
            'action': 'GetZCaisse',
            'date' : dateToPrint,
            'id_pointshop' : ACAISSE.id_pointshop
        },
        type: 'post',
        dataType: 'json',
    }).fail(function(response) {
    	//$('#printTicket').attr('class','').html(response.response.z_caisse);
    	PRESTATILL_CAISSE.loader.end();
		alert('Pas d\'encaissement ce jour');
	}).done(function(response) {
    	$('#printTicket').attr('class','').html(response.response.z_caisse);
    	PRESTATILL_CAISSE.loader.end();
		window.print();
	});
}

function getTicketCaisse() {
  	$.ajax({
        type: 'post',
        data: {
          'action': 'GetTicketCaisse',
          'id_ticket': ACAISSE.ticket.id,
        },
        dataType: 'json',
    }).done(function (response) {
        if (response.success != false) {
          
          printTicket(response.response.ticket_caisse, acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.ticket_nbr_copies);
                    
  		  for(var i = 0 ; i < response.response.extra_tickets.length ; i++)
  		  {
  			 printTicket(response.response.extra_tickets[i], 1);
  		  }
  		  
  		  // Soit envoi par mail soit impression
  		  if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.ticket_email == 1)
  		  {
  		  	
  		  	var html = '';
		
			html +='<div id="ticketInfos" class="popupBox">';
				html +='<div class="overflow" style="display:none;"></div>';
		        html +='<div class="title">';
		       		html +='<h3>Souhaitez-vous imprimer le ticket ou l\'envoyer par mail ?</h3>';
		        html +='</div>';
		        html +='<div class="sendTicket infosComp">';
					html +='<form id="popup_epgloal__form">';
		            	html +='<fieldset>';
		            			html +='<div class="send_form" style="display:none;"><div class="label" >Adresse email d\'envoi :</div>';
									html +='<input type="text" value="'+ACAISSE.customer.email+'" id="send_form_email" >';
									html +='<div>';
										html +='<button id="abort_send" class="button">Annuler</button>';
										html +='<button id="confirm_send" class="button">Envoyer</button>';
									html +='</div>';
								html +='</div>';
		            	html +='</fieldset>'; 
		            html +='</form>';
		            html +='<div class="devisButtons">'; 
		            	html +='<div class="button no_ticket"><i class="fa fa-file"></i><div class="label">Pas de ticket</div><div class="legend">(Ni imprimé, ni envoyé)</div></div>';
		            	html +='<div class="button print_to_ticket"><i class="fa fa-print"></i><div class="label">Imprimer</div><div class="legend">(format ticket)</div></div>';
		            	html +='<div class="button send"><i class="fa fa-envelope"></i><div class="label">Envoyer</div><div class="legend">par e-mail</div></div>';
		            html +='</div>';
		        html +='</div>';
			html +='</div>';
			
			showEmptyPopup($("#paymentView"),html);
			
		  }
		  else {
		  	printQueue();
		  	
		  	  $('#multi-payments-popup').remove();
	          $('.strict-overlay').remove();
	          //on demande un passage au ticket suivant
	          _startNewPreorder();
		  }	
		
		  
  		  
          
          
        } else {
          alert('Error : o_print0012 / Erreur lors de la génération du ticket de caisse');
          console.error(response.error);
        }
    }).fail(function () { 
        alert('Error : o_print0011 / Erreur lors de la génération du ticket de caisse');
    });
}

function getDayTickets(date) {
	$.ajax({
		data: {
			'action': 'GetDayTickets',
			'id_pointshop': ACAISSE.id_pointshop,
			'dat':date
		},
		type: 'post',
		dataType: 'json',
	}).done(function(response) {            	
		if (response.success == true) {
			var ticketBuffer = response.response.tickets;
			
			$('#printTickets .rePrintZ').removeClass('disabled');
			//console.log(ticketBuffer[i]);
			$('#printTickets .notLoaded').css('display','block');
	
			if (false && ticketBuffer.length==1) {
				// on affiche le ticket de caisse avec le bouton de réimpression
				$('#printTickets .left .ticket').remove();
				$('#printTickets .rePrintZ').addClass('disabled');
				$('#printTickets .notLoaded, #printTickets .loaded').css('display','none');
				$('#printTickets .left').append('<div class="ticket toPrint">'+ticketBuffer[0].htm+'</div><div class="ticket"><button class="print"><i class="fa fa-print fa-2x"></i> Imprimer</button> <button class="kdo"><i class="fa fa-gift fa-2x"></i> Imprimer Ticket Cadeau</button></div>');
			} else {
				var html ='';
				html += '<table>';
				
				html +='<thead><tr><th></th><th>N° de ticket</th><th>Client</th><th>Date</th><th>Référence</th><th>N° de Commande</th><th>Montant total</th></tr></thead>';
				
				for (var i=0; i<ticketBuffer.length; i++) {
					
					//console.log(ticketBuffer[i]);
					
					html += '<tr data-tblId="'+i+'" data-num="'+ticketBuffer[i].num+'">';
					
					html += 	'<td class="text-center" ><i  class="fa fa-ticket"></td>';
					html += 	'<td class="text-center" >'+ticketBuffer[i].num+'</td>';
					html += 	'<td class="text-center" >'+ticketBuffer[i].client+'</td>';
					html +=	 	'<td class="text-center" '+(ticketBuffer[i].valid != 1?' style="text-decoration:line-through;"':'')+'><span class="date">'+ Date.toString(ticketBuffer[i].dat) + '</span>';
					if(ticketBuffer[i].valid != 1)
					{
						html +=		' <span class="badge badge-error">ANNULE<span>';
					}
					html += '</td>';
					html += 	'<td class="text-center" ><span class="state">'+ ticketBuffer[i].reference +'<span></td>';
					html += 	'<td class="text-center" >'+ ticketBuffer[i].id_order +'</td>';
					html += 	'<td class="text-center" ><span class="paid">'+ticketBuffer[i].amount+'€</span></td>';
					html += '</tr>';
				}
				html += '</table>';
				$('#printTickets .left .ticket').remove();
				$('#printTickets .loaded').html(html);
				$('#printTickets .loaded').css('display','block');
				$('#printTickets .notLoaded').css('display','none');
			
				var todayDate = getEngDate(new Date(),false); 
				//console.log(date);
				//console.log(getEngDate(new Date(),false));
				if(date == todayDate)
				{
					$('#printTickets .rePrintZ').addClass('disabled');
				}
				
			}
		} else {
			alert(['Erreur ????? (getDayTickets) : ' + response.errorMsg, response]);
		}
	}).fail(function () {
		alert(['Erreur 01002 : Impossible de communiquer les informations au serveur',ticket]);
	});
}

function getNumTickets(num) {
	$.ajax({
		data: {
			'action': 'GetNumTickets',
			'id_pointshop': ACAISSE.id_pointshop,
			'num':num
		},
		type: 'post',
		dataType: 'json',
	}).done(function(response) {            	
		if (response.success == true) {
			var ticketBuffer = response.response.tickets;
			
			$('#printTickets .notLoaded').css('display','block');
	
			if (ticketBuffer.length==1) {
				// on affiche le ticket de caisse avec le bouton de réimpression
				$('#printTickets .left .ticket').remove();
				$('#printTickets .notLoaded, #printTickets .loaded').css('display','none');
				//buffer de preview
				$('#printTickets .left').append('<div class="ticket toPrint">'+ticketBuffer[0].htm+'</div>');
				//on ajoute les bouttons d'impression
				//@TODO ici faudra certainement faire des diffrences				
				var html = '<div class="ticket print_buttons">';
				html += '<button class="print"><i class="fa fa-print fa-2x"></i> Duplicata</button> ';
				//@TODO factureetet =>*****************
				//html += '<button class="facturette"><i class="fa fa-print fa-2x"></i> Facturette</button> ';
				//**************************************
				html += ' <button class="kdo"><i class="fa fa-gift fa-2x"></i> Imprimer Ticket Cadeau</button>';
				html += '</div>';
				$('#printTickets .left').append(html);
			} else {
				var html = '<table>';
				for (var i=0; i<ticketBuffer.length; i++) {
					html += '<tr data-tblId="'+i+'" data-num="'+ticketBuffer[i].num+'"><td><i  class="fa fa-ticket"></i>Ticket n°'+ticketBuffer[i].num+'</td><td>' + Date.toString(ticketBuffer[i].dat) + '</td><td>'+ticketBuffer[i].amount+'</td></tr>';
				}
				html += '</table>';
				$('#printTickets .left .ticket').remove();
				$('#printTickets .loaded').html(html);
				$('#printTickets .loaded').css('display','block');
				$('#printTickets .notLoaded').css('display','none');
			}
		} else {
			alert(['Erreur ????? (getNumTickets) : ' + response.errorMsg, response]);
		}
	}).fail(function () {
		alert(['Erreur 01002 : Impossible de communiquer les informations au serveur',ticket]);
	});
}

function getEngDate(d, withTime) { // YYYY-mm-dd
	var date=d.getFullYear()+'-';
	
	if (d.getMonth()+1<10) {
		date+=('0'+(d.getMonth()+1)+'-');
	} else {
		date+=((d.getMonth()+1)+'-');
	}
	
	if (d.getDate()<10) {
		date+=('0'+d.getDate());
	} else {
		date+=(d.getDate());
	}
	
	if (withTime) {
		date+=' ';
		if (d.getHours()<10) {
			date+=('0'+(d.getHours()+1)+':');
		} else {
			date+=((d.getHours())+':');
		}
		
		if (d.getMinutes()<10) {
			date+=('0'+d.getMinutes());
		} else {
			date+=(d.getMinutes());
		}
	}	
	return date;
}

var ACAISSE_print_queue = [];


printTicket = function(html, nb_print, class_name) {
	
	var class_name = class_name==undefined?'ticket':class_name;
	for (var i=0; i<nb_print; i++) {		
		ACAISSE_print_queue.push({html:html,'class_name':class_name});
	}
};

var printQueue = function(send = false) {
	if(ACAISSE_print_queue.length>0)
	{
		if (send == false)
		{
			var next_ticket = ACAISSE_print_queue.shift();
			$('#printTicket').attr('class',next_ticket.class_name).html(next_ticket.html);			
			setTimeout(function(){			
				window.print();				
				printQueue();
			},500);
		}
		else
		{
			$('#ticket_email').html('').append('<div style="width:650px;">'+ACAISSE_print_queue[0]['html']+'</div>');
			$('#ticket_email h2.gift_print, #ticket_email h2.duplicata_print').hide(); 
			
			html2canvas($('#ticket_email')[0],
			{
				windowWidth: '650px',
			}).then(function(canvas) {
			    
			    var base64image = canvas.toDataURL();
			    
			    $.ajax({
			        type: 'post',
			        dataType: 'json',
			        data: {
			            'action': 'SendTicketsByMail',
			            'id_ticket': ACAISSE.ticket.id,
			            'tickets': ACAISSE_print_queue,
				        'send_email': $('#send_form_email').val(),
				        imgBase64: base64image,
			        }
			    }).done(function (response) {
			        if (response.success != false) {
		        	
		        	/*  ACAISSE_print_queue.shift();
			          // On confirme que le mail a bien été envoyé
			          PRESTATILL_CAISSE.loader.end();
			          //alert('Mail envoyé avec succès');
			          $('#ticket_email').html('');
			          
			         if(ACAISSE_print_queue.length > 0)
			         {
			         	 // On imprime les tickets cadeau ou autres tickets rattachés
				         var next_ticket = ACAISSE_print_queue.shift();
						 $('#printTicket').attr('class',next_ticket.class_name).html(next_ticket.html);			
						 setTimeout(function(){			
							window.print();				
							printQueue();
						 },500);
			         } 
			          
			         //on demande un passage au ticket suivant
	         		 _startNewPreorder(); 
	         		 
	         		 sendNotif('greenFont','<i class="fa fa-check"></i> Mail envoyé avec succès');
			         */ 
			        } else {
			          alert('Error : l_send0012 / Une erreur s\'est produite lors de l\'envoi du mail.');
			          //console.error(response.error);
			        }
			    }).fail(function () { 
			        alert('Error : l_send0011 / Une erreur s\'est produite lors de l\'envoi du mail.');
			    });  
			
			    ACAISSE_print_queue.shift();
		        // On confirme que le mail a bien été envoyé
		        PRESTATILL_CAISSE.loader.end();
		        //alert('Mail envoyé avec succès');
		        $('#ticket_email').html('');
		        
		        if(ACAISSE_print_queue.length > 0)
		        {
		        	// On imprime les tickets cadeau ou autres tickets rattachés
			        var next_ticket = ACAISSE_print_queue.shift();
					$('#printTicket').attr('class',next_ticket.class_name).html(next_ticket.html);			
					setTimeout(function(){			
						window.print();				
						printQueue();
					},500);
		        } 
		          
		        //on demande un passage au ticket suivant
         		_startNewPreorder(); 
         		
         		sendNotif('greenFont','<i class="fa fa-check"></i> Mail envoyé avec succès');
			});
			
			
		}
	}
};


$(function() {
	$('#printTickets .right').prepend('<div style="text-align:center;"><button class="rePrintZ"><i class="fa fa-print"></i> Ré-imprimer le Z du jour</button></div><div class="searchTicketBox"><label for="searchTicketDate">Date du ticket : </label><input id="searchTicketsDate" type="date" style="width: 130px;" />');
	$('#searchTicketsDate')[0].value=getEngDate(new Date());
	resetTileHeight();
	
	document.querySelector('#searchTicketsDate').onchange = function() {
		var date = this.value;
		var d = date.split('-');
		if (d[0]>2000) {
			// on concidère qu'on a une bonne date, on envoie la recherche au serveur
			$('#printTickets .left .notLoaded').css('display','block');
			
			getDayTickets(this.value);
		}
	};
	
	setTimeout(function() {
		document.querySelector('#searchTicketsDate').onchange();
	},1000);
});
var scanner = {
	buffer : '',
	resetterProcess : setInterval(function () {scanner.buffer = '';}, 500),
	relauncherProcess : null,
	customerKeyIntervalProcess : null
};

$(function() {
	//GESTION DES SAISIES AU CLAVIER / KEYBOARD ENTRY
    //pour les touches spéciales
    $(document).on('keydown', 'body', function (e) {

        var key = e.which || e.keyCode || e.charCode;

        switch (ACAISSE.scanbarcodeMode)
        {
            case 'cash':
                var key = e.which || e.keyCode || e.charCode;
                if (key == 8 || key == 46) {
                    //pour les touche backspace et delete
                    _cashDeleteChar();
                }
                break;
            case 'customer':
                var key = e.which || e.keyCode || e.charCode;
                if (key == 8 || key == 46) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('#customerSearchResultsFilter input').val($('#customerSearchResultsFilter input').val().substring(0, $('#customerSearchResultsFilter input').val().length - 1));
                    return false;
                }
                break;
             case 'productSearch':
                var key = e.which || e.keyCode || e.charCode;
                if (key == 8 || key == 46) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('#productSearchResultsFilter input').val($('#productSearchResultsFilter input').val().substring(0, $('#productSearchResultsFilter input').val().length - 1));
                    loadProductSearchList();
                    return false;
                }
                break;
            case 'preOrder':
                if (key == 8 || key == 46) {
                    if ($('#preOrderListContainer .keyboard span.display').html().length > 0)
                    {
                        $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html().substring(0, ($('#preOrderListContainer .keyboard span.display').html().length - 1)));
                    }
                    _refreshFilterPreOrderList($('#preOrderListContainer .keyboard span.display').html());
                }
                break;
        }


        if ((key == 8 || key == 46) && ($('input:focus, textarea:focus').length == 0)) {
            //console.log('not focus');
            e.preventDefault();
            e.stopPropagation();
            return false; 
        }
        
    });
    //Pour les saisies
    $(document).on('keypress', 'body', function (e) {
        switch (ACAISSE.scanbarcodeMode)
        {
            case 'payment':            	
            	PRESTATILL_CAISSE.payment.dispatchScanBarCode(e);
            	break;
            case 'cash': //lors du paiment en liquide
                //console.log('cash');
                var c = String.fromCharCode(e.keyCode);
                _cashAddChar(c);
                //console.log([e.keyCode, c]);
                break;

            case 'preOrder':
                var num = parseInt(String.fromCharCode(e.keyCode));
                if (num >= 0 && num <= 9)
                {
                    $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html() + num);
                }
                else
                {
                    if ($('#preOrderListContainer .keyboard span.display').html().length > 0)
                    {
                        $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html().substring(0, ($('#preOrderListContainer .keyboard span.display').html().length - 1)));
                    }
                }
                _refreshFilterPreOrderList($('#preOrderListContainer .keyboard span.display').html());
                break;
            case 'customer':
                clearTimeout(scanner.customerKeyIntervalProcess);
                var ch = String.fromCharCode(e.keyCode);
                $('#customerSearchResultsFilter input').val($('#customerSearchResultsFilter input').val() + ch);
                scanner.customerKeyIntervalProcess = setTimeout(function () {
                    idCustomer = $('#customerSearchResultsFilter input').val();
                    if (idCustomer.length==13 && parseInt(idCustomer)==idCustomer && idCustomer[0]==2) {// on a à faire à un ean13
                    	idCustomer=parseInt(idCustomer.substring(1,idCustomer.length-1));
                    	$('#customerSearchResultsFilter input').val(idCustomer);
                    	sendCustomerSearch(true);
                    } else {
                    	sendCustomerSearch();
                    }
                    
                }, 500);
                break;
            case 'productSearch':
                clearTimeout(scanner.customerKeyIntervalProcess);
                var ch = String.fromCharCode(e.keyCode);
                $('#productSearchResultsFilter input').val($('#productSearchResultsFilter input').val() + ch);
                scanner.customerKeyIntervalProcess = setTimeout(function () {
                    loadProductSearchList();
                }, 500);
                break;
            case 'editCustomer' :
                $('[id^="customer-edit-error-"]').each(function(){
                    if($(this).is(':visible')){
                        $(this).fadeOut();
                    }
                });
                break;
            case 'action' :
            	break;
            case 'product':
            
            default:
                clearInterval(scanner.resetterProcess);
                if (scanner.relauncherProcess !== null) {
                    clearTimeout(scanner.relauncherProcess);
                }
                scanner.relauncherProcess = setTimeout(function () {
                    scanner.resetterProcess = setInterval(function () {
                        scanner.buffer = '';
                    }, 500);
                }, 150);

                if (e.keyCode == 13) // touche entrée
                {
                	var resultat_scan_normal = doBarcodeScan(scanner.buffer);
                	if(resultat_scan_normal !== true) //le code ean13 ne correspond pas en l'état à une code ean13 d'un produit
                	{
	                    //analyse du code bar
	                    if (scanner.buffer[0]==2) {
	                    	
	                    	var id_product = parseInt(scanner.buffer.substring(1,7));
	                    	var id_product_attribute = 0;
	                    	
	                    	var product = _loadProductFromId(id_product);
	                    	
	                    	if (product && !product.disponibility_by_shop[ACAISSE.id_pointshop]) {
	                    		sendNotif('redFont','<i class="fa fa-exclamation-circle"></i> Produit désactivé dans ce point de vente');
	                    	} else if (product === undefined) {
						    	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>2Produit inexistant');
						    } else {
						    	var masseGramme = parseInt(scanner.buffer.substring(7,12));
		                    	
		                    	if (product.has_attributes && product.unity=="grammes") {
									for (var id_attribute in product.attributes) {
										if (product.attributes[id_attribute].attribute_name=='1g') {
											id_product_attribute = id_attribute;
											//masseGramme /= 1000;
										}
									}
								}
		                    	
		                    	var masse = product.unity=='g'?masseGramme:masseGramme/1000;
		                    	
		                    	var prixTTC = Math.round(masse*product.unit_price*100)/100;
		                    	
		                    	addToCart(id_product,id_product_attribute,masseGramme);
						    }
	                    } else if (scanner.buffer[0]==8) {
	                    	idCustomer=scanner.buffer.substring(1,scanner.buffer.length-1);
	                    	$('#customerSearchResultsFilter input').val(idCustomer);
	                    	sendCustomerSearch(true);
	                    }
	                    else {
	                    	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i> '+resultat_scan_normal);
	                    }
                    }
                    
                    scanner.buffer = '';
                }
                else
                {
                    scanner.buffer += String.fromCharCode(e.keyCode);
                }
                break;
        }

    });

    $(document).on('keyup change', 'body', function (e) {
        switch (ACAISSE.scanbarcodeMode)
        {
            case 'action':
                //calcul du montant rentré lors de l'ouverture de la caisse sur chaque ligne
                $('.totalCash').text(function () {
                    var money = numeral(
                            $(this).prev('td').children().data('money')
                            );
                    var result = money.multiply(
                            $(this).prev('td').children().val()
                            );
                    return result.format();
                });

                //calcul du total en euro à l'ouverture, mvt et fermeture
                var totalCash = numeral(0);
                $('li.open .totalCash').each(function () {
                    var cash = numeral().unformat($(this).text());
                    totalCash = totalCash.add(cash);
                });
                $('li.open input.fullTotalCash').val(totalCash.format());
                $('li.open input[name="total_cash"]').val(totalCash.multiply(100).format('0'));

                //calcul des pièces retiré à la préparation
                var totalStayCash = numeral(0);
                $('li.open .takeOffMoney').each(function () {
                    var result = numeral($(this).closest('tr').data('money')).multiply($('input', this).val());
                    totalStayCash = totalStayCash.add(result);
                });
                $('li.open #totalStayCash').val(function () {
                    var totalCashOnClose = numeral().unformat($('#totalCashOnClose').val());
                    return numeral(totalCashOnClose).subtract(totalStayCash).format();
                });

                //calcul des pièces demandés à la préparation
                var totalRequestMoney = numeral(0);
                $('li.open .requestMoney').each(function () {
                    var result = numeral($(this).closest('tr').data('money')).multiply($('input', this).val());
                    totalRequestMoney = totalRequestMoney.add(result);
                });
                $('li.open #totalRequestMoney').val(function () {
                    var totalStay = numeral().unformat($('#totalStayCash').val());
                    return numeral(totalStay).add(totalRequestMoney).format();
                });
                $('.totalRequestMoney').val(totalRequestMoney.multiply(100).format('0'));
                $('.totalTakeOffMoney').val(totalStayCash.multiply(100).format('0'));

                //nombre total de pièce retiré/ajouté
                $('li.open .totalLineMoney input').each(function () {
                    var thisParent = $(this).closest('tr');
                    var moneyIn = parseInt($('[data-cash-on-close]', thisParent).val());
                    var requestMoney = $('.requestMoney input', thisParent).val();
                    var takeOffMoney = $('.takeOffMoney input', thisParent).val();
                    var theoreticalCash = parseInt($('[data-theoretical-cash]', thisParent).val());
                    $(this).val((requestMoney - takeOffMoney) + moneyIn);
                    _setProgressPrepare($(this), theoreticalCash);
                });

                break;
        }
    });
    
    
    var doBarcodeScan = function(ean13) {
		var e = acaisse_products_prices.ean;
		
		if(ACAISSE.mode_debogue === true)
		{
				console.log(['ean13',ean13,e[ean13],e]);
		}				
		if (e[ean13] == undefined)
		{
			// On essaye de biper un ticket de caisse
			if(ean13.substring(0,2) == '87')
			{
				console.log('ticket de caisse trouvé');
				PRESTATILL_CAISSE.customer._loadOrderTpl(false,ean13);
				
				return;
			}
			
			if(true || ACAISSE.mode_debogue === true)
				{console.log(['ean13 not found ',ean13]);}
			//si le code barre n'est pas dans le cache, on intérroge totu de même la serveur avant de notifier que le produit est inexstant.
			$.ajax({
					type: 'post',
					async:false,
					data: {
					  'action': 'loadProductByEAN13',
					  'ean13': ean13,
					  'ajaxRequestID': ++ACAISSE.ajaxCount,
					},
					dataType: 'json',
		    }).done(function (response) {
			    if (response.success == true)
			    { 
			    	console.log(response);
			    	if(response.results !== false)
			    	{
				    	for(id_product in response.results.catalog)
				    	{
				    		var p = response.results.catalog[id_product];
				    		acaisse_products_prices.catalog[id_product] = p;
				    	}
				    	for(ean in response.results.ean)
				    	{
				    		var p = response.results.ean[ean];
				    		acaisse_products_prices.ean[ean] = p;
				    	}
			    	}
			    	else
			    	{
			    		console.log('Erreur OG002403 : product not found on serveur');
			    	}
			    	//acaisse_products_prices.push(response);
				
				} else {
		  			console.log('Erreur OG002404 : ' + response.errorMsg);
		       	}
		   }).fail(function () {
		    	console.log('Erreur OG001404 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
		   });
		}
		
		if (typeof (e[ean13]) != 'undefined')
		{
			var p = _loadProductFromId(e[ean13].id_product);
			if (p.disponibility_by_shop[ACAISSE.id_pointshop]) {
				
				/* SINCE 2.5.2 */
        		PRESTATILL_CAISSE.executeHook('actionPrestatillDefautScanMode', {'p': p});
        		
        		if(ACAISSE.scanbarcodeMode == 'other')
        		{
        			console.log('test OK');
        			ACAISSE.scanbarcodeMode = 'product';
        			return;
        		} 
        		else if(p.has_attributes === true && e[ean13].id_product_attribute == 0)
				{
					showPopupDeclinaison(e[ean13].id_product);
					return true;
				}
				else
				{
					addToCart(e[ean13].id_product, e[ean13].id_product_attribute);
					return true;
				} 
			} else {
				return 'Produit désactivé dans ce point de vente';
			}
		}
		else
		{
			return 'Produit inexistant';
		}
	};

    
});


function getProductPrice(id_product, id_product_attribute) { // récupère le prix du produit
	var p = _loadProductFromId(id_product);
	var unitId = params.units.from.indexOf(p.unity);


	if (id_product_attribute != undefined && id_product_attribute > 0) {
		if (p.unity == '') {
			p = p.attributes[id_product_attribute];
			return formatPrice(p.prices[ACAISSE.id_force_group]) + "€";
		}
	}
	
	if (unitId==-1) {
		console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		//throw "Erreur d'unité : unité inconnue : ";
	}
	// ATTENTION MODIFICATION A VALIDER : remplacement de p.unit_price par prices[ACAISSE.id_force_group]
	if (params.units.scale[unitId]!=1) {
		return formatPrice(p.prices[ACAISSE.id_force_group]*params.units.scale[unitId]).replace(' ','')+'€'+params.units.to[unitId];
	} else {
		return formatPrice(p.prices[ACAISSE.id_force_group]) + "€";
	}
}

function getProductBasePrice(id_product, id_product_attribute) { // récupère le prix du produit
	var p = _loadProductFromId(id_product);
	
	var unitId = params.units.from.indexOf(p.unity);
	if (unitId==-1) {
		console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		//throw "Erreur d'unité : unité inconnue : ";
	}
	var base_price = p.base_price;
	
	if(ACAISSE.price_display_method == 1) //HT
	{
		base_price = parseFloat(p.base_price);	
	}
	else
	{
		// On ajoute la TVA (pour l'affichage)
		base_price = parseFloat(p.base_price*(1+(p.tax_rate/100)));
	}
			
	if (params.units.scale[unitId]!=1) {
		return formatPrice(base_price*params.units.scale[unitId]).replace(' ','')+'€'+params.units.to[unitId];
	} else {
		return formatPrice(base_price) + "€";
	}
}


/**
 *copie de PrestaShop/js/admin/price.js en version 1.6 
 */




function getTax()
{
	if (noTax)
		return 0;

	var selectedTax = document.getElementById('product-edit-id_tax_rules_group');
	var taxId = selectedTax.options[selectedTax.selectedIndex].value;
	return taxesArray[taxId].rates[0];
}

function getTaxes()
{
	if (noTax)
		taxesArray[taxId];

	var selectedTax = document.getElementById('product-edit-id_tax_rules_group');
	var taxId = selectedTax.options[selectedTax.selectedIndex].value;
	return taxesArray[taxId];
}

function addTaxes(price)
{
	var taxes = getTaxes();
	var price_with_taxes = price;
	if (taxes.computation_method == 0) {
		for (i in taxes.rates) {
			price_with_taxes *= (1 + taxes.rates[i] / 100);
			break;
		}
	}
	else if (taxes.computation_method == 1) {
		var rate = 0;
		for (i in taxes.rates) {
			 rate += taxes.rates[i];
		}
		price_with_taxes *= (1 + rate / 100);
	}
	else if (taxes.computation_method == 2) {
		for (i in taxes.rates) {
			price_with_taxes *= (1 + taxes.rates[i] / 100);
		}
	}

	return price_with_taxes;
}

function removeTaxes(price)
{
	var taxes = getTaxes();
	var price_without_taxes = price;
	if (taxes.computation_method == 0) {
		for (i in taxes.rates) {
			price_without_taxes /= (1 + taxes.rates[i] / 100);
			break;
		}
	}
	else if (taxes.computation_method == 1) {
		var rate = 0;
		for (i in taxes.rates) {
			 rate += taxes.rates[i];
		}
		price_without_taxes /= (1 + rate / 100);
	}
	else if (taxes.computation_method == 2) {
		for (i in taxes.rates) {
			price_without_taxes /= (1 + taxes.rates[i] / 100);
		}
	}

	return price_without_taxes;
}

function getEcotaxTaxIncluded()
{
	return ecotax_tax_incl;
}

function getEcotaxTaxExcluded()
{
	return ecotax_tax_excl;
}

function formatPrice(price)
{
	var fixedToSix = (Math.round(price * 1000000) / 1000000);
	return (Math.round(fixedToSix) == fixedToSix + 0.000001 ? fixedToSix + 0.000001 : fixedToSix);
}

function calcPrice()
{
	initEcoTaxBeforePriceCalc();
	
	var priceType = $('#product-edit-tax').val();
	if (priceType == 'TE')
	{
		calcPriceTI();
	}
	else
	{
		calcPriceTE();
	}
	
	addProductRefreshPrices();
}

function initEcoTaxBeforePriceCalc()
{
	ecotaxTaxRate = getTax();
	ecotax_tax_incl = parseFloat(document.getElementById('product-edit-eco-tax').value.replace(/,/g, '.'));
	if(isNaN(ecotax_tax_incl))
	{
		ecotax_tax_incl = 0;
	}
	ecotax_tax_excl = ps_round(removeTaxes(ecotax_tax_incl), 2);
}

function calcPriceTI()
{
	priceTE = parseFloat(document.getElementById('product-edit-price_editor').value.replace(/,/g, '.'));
	if(isNaN(priceTE))
	{
		priceTE = 0;
	}
	var newPrice = addTaxes(priceTE+ecotax_tax_excl);
	priceTI = (isNaN(newPrice) == true || newPrice < 0) ? '' : ps_round(newPrice, priceDisplayPrecision).toFixed(5);
}

function calcPriceTE()
{
	//ecotax_tax_excl =  $('#product-edit-eco-tax').val() / (1 + ecotaxTaxRate);
	priceTI = parseFloat(document.getElementById('product-edit-price_editor').value.replace(/,/g, '.'));
	if(isNaN(priceTI))
	{
		priceTI = 0;
	}
	var newPrice = removeTaxes(ps_round(priceTI - getEcotaxTaxIncluded(), priceDisplayPrecision));
	priceTE = (isNaN(newPrice) == true || newPrice < 0) ? '' : ps_round(newPrice, priceDisplayPrecision).toFixed(5);
}

function addProductRefreshPrices()
{
	
	$('#converted_price_value').html($('#product-edit-tax').val() == 'TE'?priceTI:priceTE);
	$('#converted_price_vat').html($('#product-edit-tax').val() == 'TE'?'TTC':'HT');
	$('#converted_eco_tax').html($('#product-edit-tax').val() == 'TE'?'Prix de vente TTC incluant l\'éco taxe (TTC)':'Prix de vente TTC incluant l\'éco taxe (TTC)');
	$('#convert_price_currency').html(currency.sign);
	$('#product-edit-price').val(priceTE);
}

function decimalTruncate(source, decimals)
{
	if (typeof(decimals) == 'undefined')
		decimals = 6;
	source = source.toString();
	var pos = source.indexOf('.');
	return parseFloat(source.substr(0, pos + decimals + 1));
}

/**
 * En provenance de /prestashop/js/tools.js 
 */
function formatedNumberToFloat(price, currencyFormat, currencySign)
{
	price = price.replace(currencySign, '');
	if (currencyFormat === 1)
		return parseFloat(price.replace(',', '').replace(' ', ''));
	else if (currencyFormat === 2)
		return parseFloat(price.replace(' ', '').replace(',', '.'));
	else if (currencyFormat === 3)
		return parseFloat(price.replace('.', '').replace(' ', '').replace(',', '.'));
	else if (currencyFormat === 4)
		return parseFloat(price.replace(',', '').replace(' ', ''));
	return price;
}

//return a formatted number
function formatNumber(value, numberOfDecimal, thousenSeparator, virgule)
{
	value = value.toFixed(numberOfDecimal);
	var val_string = value+'';
	var tmp = val_string.split('.');
	var abs_val_string = (tmp.length === 2) ? tmp[0] : val_string;
	var deci_string = ('0.' + (tmp.length === 2 ? tmp[1] : 0)).substr(2);
	var nb = abs_val_string.length;

	for (var i = 1 ; i < 4; i++)
		if (value >= Math.pow(10, (3 * i)))
			abs_val_string = abs_val_string.substring(0, nb - (3 * i)) + thousenSeparator + abs_val_string.substring(nb - (3 * i));

	if (parseInt(numberOfDecimal) === 0)
		return abs_val_string;
	return abs_val_string + virgule + (deci_string > 0 ? deci_string : '00');
}

function formatCurrency(price, currencyFormat, currencySign, currencyBlank)
{
	// if you modified this function, don't forget to modify the PHP function displayPrice (in the Tools.php class)
	var blank = '';
	price = parseFloat(price).toFixed(10);
	price = ps_round(price, priceDisplayPrecision);
	if (currencyBlank > 0)
		blank = ' ';
	if (currencyFormat == 1)
		return currencySign + blank + formatNumber(price, priceDisplayPrecision, ',', '.');
	if (currencyFormat == 2)
		return (formatNumber(price, priceDisplayPrecision, ' ', ',') + blank + currencySign);
	if (currencyFormat == 3)
		return (currencySign + blank + formatNumber(price, priceDisplayPrecision, '.', ','));
	if (currencyFormat == 4)
		return (formatNumber(price, priceDisplayPrecision, ',', '.') + blank + currencySign);
	if (currencyFormat == 5)
		return (currencySign + blank + formatNumber(price, priceDisplayPrecision, '\'', '.'));
	return price;
}

function ps_round_helper(value, mode)
{
	// From PHP Math.c
	if (value >= 0.0)
	{
		tmp_value = Math.floor(value + 0.5);
		if ((mode == 3 && value == (-0.5 + tmp_value)) ||
			(mode == 4 && value == (0.5 + 2 * Math.floor(tmp_value / 2.0))) ||
			(mode == 5 && value == (0.5 + 2 * Math.floor(tmp_value / 2.0) - 1.0)))
			tmp_value -= 1.0;
	}
	else
	{
		tmp_value = Math.ceil(value - 0.5);
		if ((mode == 3 && value == (0.5 + tmp_value)) ||
			(mode == 4 && value == (-0.5 + 2 * Math.ceil(tmp_value / 2.0))) ||
			(mode == 5 && value == (-0.5 + 2 * Math.ceil(tmp_value / 2.0) + 1.0)))
			tmp_value += 1.0;
	}

	return tmp_value;
}

function ps_log10(value)
{
	return Math.log(value) / Math.LN10;
}

function ps_round_half_up(value, precision)
{
	var mul = Math.pow(10, precision);
	var val = value * mul;

	var next_digit = Math.floor(val * 10) - 10 * Math.floor(val);
	if (next_digit >= 5)
		val = Math.ceil(val);
	else
		val = Math.floor(val);

	return val / mul;
}

function ps_round(value, places)
{
	if (typeof(roundMode) === 'undefined')
		roundMode = 2;
	if (typeof(places) === 'undefined')
		places = 2;

	var method = roundMode;

	if (method === 0)
		return ceilf(value, places);
	else if (method === 1)
		return floorf(value, places);
	else if (method === 2)
		return ps_round_half_up(value, places);
	else if (method == 3 || method == 4 || method == 5)
	{
		// From PHP Math.c
		var precision_places = 14 - Math.floor(ps_log10(Math.abs(value)));
		var f1 = Math.pow(10, Math.abs(places));

		if (precision_places > places && precision_places - places < 15)
		{
			var f2 = Math.pow(10, Math.abs(precision_places));
			if (precision_places >= 0)
				tmp_value = value * f2;
			else
				tmp_value = value / f2;

			tmp_value = ps_round_helper(tmp_value, roundMode);

			/* now correctly move the decimal point */
			f2 = Math.pow(10, Math.abs(places - precision_places));
			/* because places < precision_places */
			tmp_value /= f2;
		}
		else
		{
			/* adjust the value */
			if (places >= 0)
				tmp_value = value * f1;
			else
				tmp_value = value / f1;

			if (Math.abs(tmp_value) >= 1e15)
				return value;
		}

		tmp_value = ps_round_helper(tmp_value, roundMode);
		if (places > 0)
			tmp_value = tmp_value / f1;
		else
			tmp_value = tmp_value * f1;

		return tmp_value;
	}
}
$(function(){
	
	var DEBUG_LEVEL = 0;
	
	var open_stock_interface = function($tr,id_warehouse,html) {
		$('tr.stock-inline-interface').remove();
		var $interface = $('<tr class="stock-inline-interface"><td colspan="5"><div class="interface-content" style="display:none;">'+html+'</div></td></tr>');
		$tr.after($interface);
		$('.ui-spinner-group.stock',$interface).uiSpinner();
		$('.interface-content',$interface).slideDown();
	};
	
	var send = function(e) {
		
		$('.validate-stock-action').prop('disabled',true);
		
		e.preventDefault();
		$.ajax({
			type: 'post',
			dataType: 'json',
			data: $('.stock-form').serialize(),
		}).done(function (response) {
			//console.log(response);
			$('.validate-stock-action').prop('disabled',false);
			if (response.success)
			{
				_drawProductInfos(response,null,'#stockBtn');
				
				if(response.request.id_warehouse != undefined)
				{						
					$('tr[data-idwarehouse="'+response.request.id_warehouse+'"] .qty')
						.animate({'fontSize':'25px'}).animate({'fontSize':'14px'},200)
						.animate({'fontSize':'22px'}).animate({'fontSize':'14px'},400);					
				}

				if(response.request.id_warehouse_from != undefined)
				{						
					$('tr[data-idwarehouse="'+response.request.id_warehouse_from+'"] .qty')
						.animate({'fontSize':'25px'}).animate({'fontSize':'14px'},200)
						.animate({'fontSize':'22px'}).animate({'fontSize':'14px'},400);					
				}

				if(response.request.id_warehouse_to != undefined)
				{				
					$('tr[data-idwarehouse="'+response.request.id_warehouse_to+'"] .qty')
						.animate({'fontSize':'25px'}).animate({'fontSize':'14px'},200)
						.animate({'fontSize':'22px'}).animate({'fontSize':'14px'},400);					
				}				
			}
			else
			{
				alert('Error o_stock00011');				
			}
		}).fail(function(){
			alert('Error o_stock00010');
		});
	};
	
	var interface_active_warehouse = function($tr,id_warehouse) {
		var html ='';
		
		html += '<input type="hidden" name="action" value="StockActiveProduct" />';
		html += '<input type="hidden" name="id_pointshop" value="'+ACAISSE.id_pointshop+'" />';
		html += '<input type="hidden" name="id_warehouse" value="'+id_warehouse+'" />';
		html += '<input type="hidden" name="is_usable" value="1" />';
		html += '<input type="hidden" name="id_employee" value="'+$('#showEmployee').data('id_employee')+'" />';
		html += '<input type="hidden" name="id_product" value="'+parseInt('0' + $('#popupNumeric').data('idobject'))+'" />';
		html += '<input type="hidden" name="id_product_attribute" value="'+parseInt('0' + $('#popupNumeric').data('idobject2'))+'" />';
			
		
		var location = '';
		var is_active = false;
		var stocks_available = $('#productInfos').data('stocks_available');
		
		if(stocks_available[id_warehouse] != undefined)
		{
			location = stocks_available[id_warehouse];
			is_active = true;
		}		
		
		html += '<div class="flex inline-group">';
			html += '<label for="stock_qu" style="line-height:18px;">Emplacement<br /><small>(Aller, rayon, étagère)</small></label>';
			html += '<input name="location" value="'+$('').text(location).html()+'" />';
		html += '</div>';
		
		html += '<div>';
		html += '	<span class="stock_active_item" data-id="1">Oui</span>';
		html += '	<span class="stock_active_item" data-id="0">Non</span>';
		html += '</div>';
		html += '<input type="hidden" value="'+(is_active?'1':'0')+'" name="is_active" />';
		
		var stock = null;
		$.each($('#productInfos').data('stocks'),function(i,item) {
			if(item.id_warehouse == id_warehouse)
			{
				stock = item;
			}
		});
		return html;
	};
	var interface_add_stock = function($tr,id_warehouse, suppliers) {
		var html ='';
		
		var product = $('#productInfos').data('product');
		
		html += '<form class="stock-form">';
			html += '<input type="hidden" name="action" value="StockAddProduct" />';
			html += '<input type="hidden" name="id_pointshop" value="'+ACAISSE.id_pointshop+'" />';
			html += '<input type="hidden" name="id_warehouse" value="'+id_warehouse+'" />';
			html += '<input type="hidden" name="is_usable" value="1" />';
			html += '<input type="hidden" name="id_employee" value="'+$('#showEmployee').data('id_employee')+'" />';
			html += '<input type="hidden" name="id_product" value="'+parseInt('0' + $('#popupNumeric').data('idobject'))+'" />';
			html += '<input type="hidden" name="id_product_attribute" value="'+parseInt('0' + $('#popupNumeric').data('idobject2'))+'" />';
			
			html += '<div id="supplierBox">';
			if(suppliers != '')
			{
				var table_suppliers = suppliers.split(',');
				html += '<label for="supplier">Fournisseur</label>';
				html +='<select name="supplier" id="supplier">';
					$.each(table_suppliers,function(i,supplier){
						html +='<option value="'+supplier+'">'+supplier+'</option>'
					});
				html +='</select>';
			}
			html += '</div>';
			
			html += '<div id="stockReasonBox">';
			var first_id = 0;
				html += '<label for="mvt_reason">Mouvement</label>';
				html +='<select name="mvt_reason" id="mvt_reason">'; 
					$.each($('#productInfos').data('stockMvtReasons'),function(i,reason){
						
						if(reason.sign == 1)
						{
							if(reason.id_stock_mvt_reason == $('#productInfos').data('PS_STOCK_MVT_INC_REASON_DEFAULT'))
							{
								first_id = reason.id_stock_mvt_reason;
								html += '<option class="stock_mvt_reason_item" value="'+reason.id_stock_mvt_reason+'">'+reason.name+'</option>';
							}
							else
							{
								html += '<option class="stock_mvt_reason_item" value="'+reason.id_stock_mvt_reason+'">'+reason.name+'</option>';
							}
						}
					});
				html +='</select>';
			html += '</div>';
			
			html +='<div id="stockInputsBox">';
				html += '<input type="hidden" value="'+first_id+'" name="id_stock_mvt_reason" />';
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Quantité</label>';
					html += '<span class="stock" data-min="1"><input class="ui-spinner" id="stock_qu" name="stock_qu" value="1" data-min="0" /></span>';
				html += '</div>';
				
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Prix d\'achat unitaire</label>';
					html += '<input name="price_te" value="'+product.wholesale_price+'" />';
					html += '<span class="suffix">€ HT</span>';
				html += '</div>';
			html += '</div>';	
			
			html +='<div class="clearfix"></div>';
			
			html += '<p class="top-spacing">';
				html += '<button class="cancel-stock-action">Fermer</button>';
				html += '<button class="validate-stock-action">Enregistrer le mouvement</button>';
			html += '</p>';
		html += '</form>';
		
		html +='<div class="clearfix"></div>';
		
		return html;
	};
	var interface_remove_stock = function($tr,id_warehouse, suppliers) {
		var html ='';
		
		var product = $('#productInfos').data('product');
		
		html += '<form class="stock-form">';
			html += '<input type="hidden" name="action" value="StockRemoveProduct" />';
			html += '<input type="hidden" name="id_pointshop" value="'+ACAISSE.id_pointshop+'" />';
			html += '<input type="hidden" name="id_warehouse" value="'+id_warehouse+'" />';
			html += '<input type="hidden" name="id_employee" value="'+$('#showEmployee').data('id_employee')+'" />';
			html += '<input type="hidden" name="id_product" value="'+parseInt('0' + $('#popupNumeric').data('idobject'))+'" />';
			html += '<input type="hidden" name="id_product_attribute" value="'+parseInt('0' + $('#popupNumeric').data('idobject2'))+'" />';
			
			html += '<div id="supplierBox">';
			if(suppliers != '')
			{
				var table_suppliers = suppliers.split(',');
				html += '<label for="supplier">Fournisseur</label>';
				html +='<select name="supplier" id="supplier">';
					$.each(table_suppliers,function(i,supplier){
						html +='<option value="'+supplier+'">'+supplier+'</option>'
					});
				html +='</select>';
			}
			html += '</div>';
			
			html += '<div id="stockReasonBox">';
			var first_id = 0;
				html += '<label for="mvt_reason">Mouvement</label>';
				html +='<select name="mvt_reason" id="mvt_reason">'; 
					$.each($('#productInfos').data('stockMvtReasons'),function(i,reason){
						
						if(reason.sign == -1)
						{
							if(reason.id_stock_mvt_reason == $('#productInfos').data('PS_STOCK_MVT_DEC_REASON_DEFAULT'))
							{
								first_id = reason.id_stock_mvt_reason;
								html += '<option class="stock_mvt_reason_item" value="'+reason.id_stock_mvt_reason+'">'+reason.name+'</option>';
							}
							else
							{
								html += '<option class="stock_mvt_reason_item" value="'+reason.id_stock_mvt_reason+'">'+reason.name+'</option>';
							}
						}
					});
				html +='</select>';
			html += '</div>';
			
			html +='<div id="stockInputsBox">';
				html += '<input type="hidden" value="'+first_id+'" name="id_stock_mvt_reason" />';
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Quantité</label>';
					html += '<span class="stock" data-min="1"><input class="ui-spinner" id="stock_qu" name="stock_qu" value="1" data-min="0" /></span>';
				html += '</div>';
				
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Prix d\'achat unitaire</label>';
					html += '<input name="price_te" value="'+product.wholesale_price+'" />';
					html += '<span class="suffix">€ HT</span>';
				html += '</div>';
			html += '</div>';	
			
			html +='<div class="clearfix"></div>';
			
			html += '<p class="top-spacing">';
				html += '<button class="cancel-stock-action">Fermer</button>';
				html += '<button class="validate-stock-action">Enregistrer le mouvement</button>';
			html += '</p>';
		html += '</form>';
		
		html +='<div class="clearfix"></div>';
		
		return html;
	};
	var interface_transfert_stock = function($tr,id_warehouse) {
		var html ='';
		var product = $('#productInfos').data('product');
		
		html += '<form class="stock-form">';
			html += '<input type="hidden" name="action" value="StockTransfertProduct" />';
			html += '<input type="hidden" name="id_warehouse_from" value="'+id_warehouse+'" />';
			html += '<input type="hidden" name="is_usable" value="1" />';
			html += '<input type="hidden" name="id_employee" value="'+$('#showEmployee').data('id_employee')+'" />';
			html += '<input type="hidden" name="id_product" value="'+parseInt('0' + $('#popupNumeric').data('idobject'))+'" />';
			html += '<input type="hidden" name="id_product_attribute" value="'+parseInt('0' + $('#popupNumeric').data('idobject2'))+'" />';
			
			html +='<div id="stockTransertBox">';
				html += '<label>Vers</label> ';
				var first_id = 0;
				html +='<select name="transfert_warehouse" id="transfert_warehouse">';
				$.each($('#productInfos').data('stocks'),function(i,stock){
					if(stock.id_warehouse != id_warehouse)
					{
						if(first_id == 0)
						{
							first_id = stock.id_warehouse;
							html += '<option class="stock_item" value="'+stock.id_warehouse+'">'+stock.name+'</span>';
						}
						else
						{
							html += '<option class="stock_item" value="'+stock.id_warehouse+'">'+stock.name+'</span>';
						}
					}				
				});
				html +='</select>';
			html += '</div>';
			
			html +='<div id="stockInputsBisBox">';
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Quantité à transférer</label>';
					html += '<span class="ui-spinner-group stock" data-min="1" data-max="'+$('.qty-physique',$tr).text()+'"><input class="ui-spinner" id="stock_qu" name="stock_qu" value="1" /></span>';
				html += '</div>';
			html += '</div>';
			
			html +='<div class="clearfix"></div>';	
				
			html += '<input type="hidden" value="'+first_id+'" name="id_warehouse_to" />';
			
			html += '<p class="top-spacing">';
				html += '<button class="cancel-stock-action">Fermer</button>';
				html += '<button class="validate-stock-action">Enregistrer le transfert</button>';
			html += '</p>';
		html += '</form>';
		
		return html;
	};
	
	
	
	$('#stocksPopup').on('change','#mvt_reason',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on selected mvt reason');
		}
		//console.log($(this).val());
		//$('span.stock_mvt_reason_item').removeClass('checked');
		//$(this).addClass('checked');
		$('[name="id_stock_mvt_reason"]').val($(this).val());
		
	});
	$('#stocksPopup').on('click tap','span.stock_item',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on selected stock');
		}
		$('span.stock_item').removeClass('checked');
		$(this).addClass('checked');
		$('[name="id_warehouse_to"]').val($(this).data('id'));
		
	});
	
	
	$('#stocksPopup').on('click tap','.button.active-warehouse',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on activate warehouse button');
		}
		
		var $tr = $(this).parents('tr');
		var id_warehouse = $tr.data('idwarehouse');
		open_stock_interface($tr,id_warehouse,interface_active_warehouse($tr,id_warehouse));
		
	});
	$('#stocksPopup').on('click tap','.button.add',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on add button');
		}		
		
		var $tr = $(this).parents('tr');
		var id_warehouse = $tr.data('idwarehouse');
		var suppliers = $tr.data('suppliers');	
		window.tr = $tr;
		open_stock_interface($tr,id_warehouse,interface_add_stock($tr,id_warehouse,suppliers));
	});
	$('#stocksPopup').on('click tap','.button.remove',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on remove button');
		}
		
		if($(this).hasClass('disabled')) {
			return;
		}
		
		var $tr = $(this).parents('tr');
		var id_warehouse = $tr.data('idwarehouse');	
		var suppliers = $tr.data('suppliers');	
		open_stock_interface($tr,id_warehouse,interface_remove_stock($tr,id_warehouse,suppliers));
		
	});
	$('#stocksPopup').on('click tap','.button.transfert',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on transfert button');
		}
		
		if($(this).hasClass('disabled')) {
			return;
		}
		
		var $tr = $(this).parents('tr');
		var id_warehouse = $tr.data('idwarehouse');		
		open_stock_interface($tr,id_warehouse,interface_transfert_stock($tr,id_warehouse));
		
	});
	
	$('#stocksPopup').on('click tap','.validate-stock-action',function(e){
		send(e);
	});
	
	
	$('#stocksPopup').on('click tap','.cancel-stock-action',function(e){
		e.preventDefault();
		$(this).parents('tr').find('.interface-content').slideUp(function(){ $(this).parents('tr').remove(); });
	});
	
	
	
	
	
});

function showPopup(inContainer, isFloatNumeric, typeOfContent){
	_setWidthAndHeight();
	
	$(inContainer).append('<div class="search-container"><div class="search-bg"></div><div class="overlayNumeric"></div></div>');
	if (isFloatNumeric) {
		// on display none le numeric et display block le floatNumeric
		$('#selectProductNb').css('display','none');
		$('#selectProductFloatNb').css('display','inline-block');
	} else {
		$('#selectProductNb').css('display','inline-block');
		$('#selectProductFloatNb').css('display','none');
	}
	
	// On ouvre le tpl en fonction du type de contenu attendu
	if(typeOfContent != 'undefined')
	{
		$('#popupNumeric').attr('data-content',typeOfContent);
		typeOfContent = 'popupNumeric';
	}
	
	if(typeof inContainer !== 'undefined'){	
		$('#'+typeOfContent).appendTo($(inContainer));
	}
	$('#'+typeOfContent).css('display','table');
}





function hidePopup(){
	$('#middleContainer .popup,#middleContainer .popup-overlay').css('display','none');
	$('.overlayNumeric, .search-bg, .search-container').remove();
}

function showEmptyPopup(inContainer,html){
	_setWidthAndHeight();
	
	$(inContainer).append('<div class="search-container"><div class="search-bg"></div><div class="overlayNumeric"></div></div>');
	
	$('#emptyPopup .inner-container .body').html(html);
	
	if(typeof inContainer !== 'undefined'){	
		$('#emptyPopup').prependTo($('body'));
	}
	
	$('.popupActions').hide();
	$('#emptyPopup').css('display','block');
}


function hidePopupAndKeyboard(){
	hidePopup();
	//$('#popupNumeric').empty();
	// On vide le contenu de la popupNumeric
	$('#productInfos .title').html('Chargement en cours');
	$('#features ul').html('');
	$('#details .descShort, #details .desc').html('');
	
	// On repasse sur l'onglet "Fiche produit"
	$('#popupProductMenu #firstShow').trigger('click');
	
	$('#selectProductNb .lbabK-inputable').val('');
	$('#selectProductFloatNb .lbabK-inputable').val('');
	
	$('.overlayNumeric, .search-bg, .search-container').remove();
}

// ouvre un popoup avec le declinaison de produit ayant l'id idObject
function showPopupDeclinaison(idObject){
	if ($('.switcher').hasClass('show-ticket')) {
        $('<div class="overlay"></div>').appendTo($("#productList"));
    } else {
        $('<div class="overlay"></div>').appendTo($("#ticketView"));
    }
   
    
    var html = '<div class="contentOverlay"><table>';
   	html += '<tr class="closeOverlay"><td colspan="4">Fermer</td></tr>';
    
    var attributes = _loadProductFromId(idObject).attributes;
    
    var productBuffer = []; //liste des produits sur la page, nécessitant la maj des stocks
    
    var real_stock_aff, preorder_stock_aff, estimateStock_aff;
    var cache_stock = products_stock_cache[idObject], cach_stock2;
    
    var i=1;
    
    for (index in attributes) { // faire deux fois la boucle pour verifier la scrollabilité
    	html += '<tr data-idobject="'+idObject+'" data-idobject2="'+index+'">';
    	html += (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0'?'<td>'+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0'?'<img src="' + (attributes[index].image != undefined && attributes[index].image != '' ? attributes[index].image : '../modules/acaisse/img/noPhoto.jpg') + '" width="50" height="50"/>':'')+'</td>':'');
    	
    	//console.log(parseFloat(_loadProductFromId(idObject).base_price*(1+(_loadProductFromId(idObject).tax_rate/100))).toFixed(2) +' - ' + parseFloat(attributes[index].prices[ACAISSE.id_force_group]).toFixed(2));
    	
	    var display_base_price = 'none';
	
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
		&& parseFloat(_loadProductFromId(idObject).base_price*(1+(_loadProductFromId(idObject).tax_rate/100))).toFixed(2) > parseFloat(attributes[index].prices[ACAISSE.id_force_group].toFixed(2)))
		{
			display_base_price = 'inline-block';
		}	
    	
    	html += '<td>'+ _loadProductFromId(idObject).product_name
    				+ '<br /><em>Réf : ' + attributes[index].ref +'</em>'
    				+ '<br /><strong>' + attributes[index].attribute_name +'</strong></td>'
    	
		if (cache_stock == undefined) {
			real_stock_aff = 'XX';
			preorder_stock_aff = 'XX';
			estimateStock_aff = 'XX';
		} else {
			cach_stock2 = cache_stock[index];
			if (cach_stock2 == undefined) {
				real_stock_aff = 'XX';
				preorder_stock_aff = 'XX';
				estimateStock_aff = 'XX';
			} else {
				real_stock_aff = cach_stock2.real_stock_aff;
				preorder_stock_aff = cach_stock2.preorder_stock_aff;
				estimateStock_aff = '<span class="no_internet">'+cach_stock2.estimateStock_aff+'</span>';
			}
		}
		html += '<td><span class="priceBaseAttr" style="display:'+display_base_price+'">'+getProductBasePrice(idObject)+'</span><span class="priceAttr">'+getProductPrice(idObject, index)+'</span></td>';
    	
    	html += '<td><span class="stock_available">'+estimateStock_aff+'</span><span class="stock_real">'+real_stock_aff+'</span><span class="stock_inqueue">-'+preorder_stock_aff+'</span></td>';
    	html += '</tr>';
    	
    	productBuffer.push({
            id_product: idObject,
            id_product_attribute: index,
            row: i,
            col: 0
        });
        
        i++;
    }
    
    html += '</table></div>';
    
    if ($('.switcher').hasClass('show-ticket')) {
    	$(html).appendTo($('#productList .content'));
    } else
    {
    	$(html).appendTo($('#ticketView .content'));
    }
    
    _refreshProductPageStock2(productBuffer);
    ACAISSE.pageProductBuffer2 = productBuffer; // on le stock pour des maj de temps à autres (on le fais dans caisse.js donc je le fais ici, il faudra demander à olivier quand on doit le mettre a jour mais vu que ça se fais à chaque ouverture de popup je ne suis pas sur que ce soit bien util)
}



/**
 * common popup action 
 */
$(function(){
	
	$('body').on('click tap','.closePopup',function(e) {
		e.preventDefault();
		//on vide l'info de montant de la popup
		$('#popupEditCartLine .lbabK-inputable').val('');
		$('.popup-overlay').hide();
		hidePopup();
	});
	
	$('body').on('click tap','#popupProductMenu button',function(){
		$('#popupProductMenu button').removeClass('open').each(function(){
			$($(this).data('to')).css('display','none');
		});
		
		$(this).addClass('open');
		$($(this).data('to')).css('display','block');
	});
	
});



/************************************************************************************
 * POPUP EDITION LIGNE du ticket (discount line, edit details, ...) 
 ************************************************************************************/

$(function(){
	
	var refreshLiveDiscountPreview = function(value,$elt)
	{
		var type = $('#popupEditCartLine .selected').data('type');
		var original_margin_rate = parseFloat($('#popupEditCartLine .original_margin_rate').html());
		var margin_rate = 0;
		
		
			
    	if(
    		value === '' || value == undefined
    		|| $('#popupEditCartLine .selected').length == 0
    	)
    	{
    		$('#popupEditCartLine .product_price').removeClass('price_with_discount');
    		$('#popupEditCartLine .product_price_reduction').html('');
    		$('#popupEditCartLine .product_final_price').html('aucune remise');
    		$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+original_margin_rate+'%</span>');
    		//console.log("value = 0");
    	}
    	else
    	{
    		$('#popupEditCartLine .product_price').addClass('price_with_discount');
    		
    		var price = parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-$('#whole_sale .whole_sale_price_ht').html());
    		var price_origin = parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val());
    		var discount = 0;        		
    		var discount_price = price;
    		
    		//console.log(parseFloat(value).toFixed(5)+' > '+parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val()).toFixed(5));
    		
    		if(type == '€')
    		{
    			discount = value;
    			discount_price -= discount;
    			
    			discount_price = Math.round(parseFloat(discount_price*100)/100);
    			$('#popupEditCartLine .product_price_reduction')
    				.html('-'+value+type);
				$('#popupEditCartLine .product_final_price')
					.html('soit '+parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val()-discount).toFixed(2)+'€ TTC');
					
				// On calcule la marge en direct
				margin_rate = parseFloat((($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-value)-$('#whole_sale .whole_sale_price_ht').html())/($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-value)*100).toFixed(2);
				$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+margin_rate+'%</span>');		
    			
    			if(value > price)
	    		{
	    			$('#popupEditCartLine .margin_rate').html('<span class="alert-danger">Vente à perte !</span>');	
	    		}
	    		
	    		if(value > price_origin)
	    		{
	    			console.log("what ?");
	    			$('#popupEditCartLine .product_price_reduction').html('');
	    			$('#popupEditCartLine .product_final_price').html('<span class="alert-danger">Prix de vente < 0€</span>');
	    		}
    		
    		}
    		else if(type == '%')
    		{
    			discount = price * value / 100;
    			discount_price -= discount;
    			discount_price = Math.round(parseFloat(discount_price*100))/100;
    			$('#popupEditCartLine .product_price_reduction')
    				.html('-'+value+type);
				$('#popupEditCartLine .product_final_price')
					.html('soit '+parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val()-$('#popupEditCartLine #popup_epline__product_price_tax_incl').val()*value/100).toFixed(2)+'€ TTC');
					
				// On calcule la marge en direct
				margin_rate = parseFloat((($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()*value/100))-$('#whole_sale .whole_sale_price_ht').html())/($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-$('#popupEditCartLine #popup_epline__product_price_tax_excl').val()*value/100)*100).toFixed(2);
				$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+margin_rate+'%</span>');
    			
    			//console.log($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()*value/100);
    			
    			if(parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()*value/100) > price)
	    		{
	    			$('#popupEditCartLine .margin_rate').html('<span class="alert-danger">Vente à perte !</span>');	
	    		}
	    		
	    		if(parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val()*value/100) > price_origin)
	    		{
	    			$('#popupEditCartLine .product_price_reduction').html('');
	    			$('#popupEditCartLine .product_final_price').html('<span class="alert-danger">Prix de vente < 0€</span>');
	    		}
    		
    		}
    		else if(type == 'gift')
    		{
    			discount = price * value / 100;
    			discount_price -= discount;
    			
    			discount_price = Math.round(parseInt(discount_price*100))/100;

    			
    			$('#popupEditCartLine .product_price_reduction')
    				.html('');
				$('#popupEditCartLine .product_final_price')
					.html('Dont '+value+' offert');
					
				// marge = ???
				margin_rate = parseFloat(0).toFixed(2);
				$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+margin_rate+'%</span>');	
    			
    			if(value > price)
	    		{
	    			$('#popupEditCartLine .margin_rate').html('<span class="alert-danger">Vente à perte !</span>');	
	    		}
	    		
	    		if(value > $('#popupEditCartLine #popup_epline__product_price_tax_incl').val())
	    		{
	    			$('#popupEditCartLine .product_price_reduction').html('');
	    			$('#popupEditCartLine .product_final_price').html('<span class="alert-danger">Prix de vente < 0€</span>');
	    		}
	    		
	    		
    		
    		}
    		var discount_price = price - discount
    		
	    	// On rafraichit la marge si le prix d'achat est nul
			if(parseFloat($('#whole_sale .whole_sale_price_ht').html()).toFixed(0) == 0)
			{
				$('#popupEditCartLine .margin_rate').html('<span class="alert-danger">Prix d\'achat non renseigné</span>');
			}
			}	
	};
	
	$('body').on('click tap','#popupEditCartLine .chosseDiscountType>div',function(){
		
		$plugin = $('#popupEditCartLine .selectProductFloatNb').data('lbabKeyboard');
		
		if($(this).hasClass('reductAmountInEuro'))
		{
			//$plugin.setMax($('#popupEditCartLine').data('price'));
		}
		else if($(this).hasClass('reductAmountInPercent'))
		{
			//$plugin.setMax(100);
		}
		
		var inputValue = $('#popupEditCartLine .lbabK-inputable').val();
		refreshLiveDiscountPreview(inputValue);
	});
	
	$('body').on('click tap','.discountCartLine',function() {
    	
    	if (!ACAISSE.cartModificationLocked) {    		
    		//$('.button.validTicket').click();
    		_validateCurrentTicket(false);
    	}
    	
    	
    	var id_product = $(this).parent().data('id_product');
    	var id_product_attribute = id_attribute = $(this).parent().data('id_product_attribute');
    	var id_ticket_line = $(this).parent().data('idticketline');
    	
		var cartLineInfos = ".cartLine_"+id_product+"-"+id_product_attribute+"-"+id_ticket_line;
		
		
		var reduc = $(this).parent().find('.pastille').html();
		var type_reduc = '€';
		if (reduc!=undefined) {
			type_reduc = reduc[reduc.length-1];
		} else {
			reduc='-0€'; 
		}		
		
		var inputValue = Math.abs(parseFloat(reduc));
		
		// On affiche uniquement les informations de marge et prix d'achat si souhaité
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_wholesale_price == 2)
		{
			$('#popupEditCartLine #whole_sale').show();
			$('#popupEditCartLine #whole_sale_box').hide();
		} else {
			$('#popupEditCartLine #whole_sale').hide();
			$('#popupEditCartLine #whole_sale_box').show();
		}
		
		// On récupère les informations du produit
		//_getProductInfos($(this).parent().data('id_product'));
		showPopupEditCartLine(
			id_product,
			id_product_attribute,
			reduc,
			type_reduc,
			inputValue,
			//cartLineInfos,
			id_ticket_line
		);//$('#ticketView'), true, 'popupEditCartLine');
	});
	
	//ajout du clavier de saisis remise
	$('#popupEditCartLine .selectProductFloatNb').lbabKeyboard({ //clavier numerique pour la saisie de quantité
        'layout': 'floatNumeric',
        //'min' : 0,
        //'max' : 12,
        onType: function(value,$elt) {        	
        	refreshLiveDiscountPreview(value,$elt);        	
        },
        onReturn: function (value) {        	
           // if ($('#popupEditCartLine .lbabK-inputable').val()=='') {
            	$('#popupEditCartLine .lbabK-inputable').val('');
          //  }
            var discount = Math.round(parseFloat(value)*100)/100; 
            var type = $('#popupEditCartLine .selected').data('type');
            
            var p = _loadProductFromId($('#popupEditCartLine').data('idobject'));
            var a;
            if ($('#popupEditCartLine').data('idobject')>0) {
            	a = p.attributes[$('#popupEditCartLine').data('idobject2')]; 
            }
            var unitId = params.units.from.indexOf(p.unity);
            var scale = params.units.scale[unitId];
            var opwtexcl = $('#popup_epline__product_price_tax_excl').val();
            var opwtincl = $('#popup_epline__product_price_tax_incl').val();
            
            if (type=='€' && scale!=1) { // si on a un produit au poids
            	discount/=scale;
            }

			$('.popup-overlay').css('display','none');	
			
			//console.log(p.unity);
			
			if(p.unity != '')
			{
				opwtexcl = parseFloat($('#popup_epline__product_price_tax_excl').val()/scale);	
				opwtincl = parseFloat($('#popup_epline__product_price_tax_incl').val()/scale);
			}	
			//console.log(parseFloat($('#popup_epline__product_price_tax_incl').val()).toFixed(5)+'<->'+parseFloat($('#popup_epline__original_tax_incl').html()).toFixed(5));
            registerDiscountOnCartLine(
            	discount,
            	type,
            	$('#popupEditCartLine').data('idobject'),
            	$('#popupEditCartLine').data('idobject2'),
            	$('#popupEditCartLine').data('id_ticket_line'),
            	{
            		product_name : $('#popup_epline__product_name').val(),
            		original_price_with_tax : parseFloat(opwtincl),
            		original_price_without_tax : parseFloat(opwtexcl),
            		original_price_is_forced_by_user : parseFloat($('#popup_epline__product_price_tax_incl').val()).toFixed(5) !== parseFloat($('#popup_epline__original_tax_incl').html()).toFixed(5) ? 1:0,
            	}
        	);
        }
    });
    
    
	$('#popupEditCartLine').on('click tap','#whole_sale_box',function(){
		$('#popupEditCartLine #whole_sale').slideToggle();
		$(this).find('i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
	});
	
	
	var showPopupEditCartLine = function(id_product,id_product_attribute,reduc,type_reduc,inputValue,id_ticket_line)
	{
		console.log('showPopupEditCartLine('+id_product+','+id_product_attribute+','+reduc+','+type_reduc+','+inputValue+','+id_ticket_line+')');
		//console.log($(cart_line_id+' .img img').attr('src'));
		
		//on va demander les informations détaillées sur CE produit au serveur
		$.ajax({
	        data: {
	            'action': 'GetProductDetailsFromServeur',
	            'id_product': id_product,
	            'id_product_attribute':id_product_attribute,
	            'id_ticket_line' : id_ticket_line,
	            'id_customer': ACAISSE.customer.id,
	            'id_force_group' : ACAISSE.id_force_group
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success != false) {
	        	
				var request = response.request;
	        	var response = response.response;
	        	var cart_line_id = '.cartLine_'+request.id_product+'-'+request.id_product_attribute+'-'+request.id_ticket_line;
	        	
	        	var p = _loadProductFromId(request.id_product);
	        	var unitId = params.units.from.indexOf(p.unity);
            	var scale = params.units.scale[unitId];
            	var unitTo = params.units.to[unitId];
            	
            	if(p.unity != '')
	        	{
	        		$('#popup_epline__manual_price_box .unit').html('€'+unitTo);
	        	}
            	
	        	//console.log(unitTo);
	        	//pas de prix forcé par l'utilisateur
	        	if(response.line.original_price_is_forced_by_user == 0)
	        	{	        		
		        	$('#popup_epline__product_price_tax_incl').val(parseFloat(response.product.original_price_tax_incl).toFixed(5));
		        	$('#popup_epline__product_price_tax_excl').val(parseFloat(response.product.original_price_tax_excl).toFixed(5));
		        }
	        	else
	        	{
		        	//mise à jour des informations
		        	if(p.unity != '')
		        	{
		        		response.line.original_price_with_tax = parseFloat(response.line.original_price_with_tax*scale).toFixed(5);
		        		response.line.original_price_without_tax = parseFloat(response.line.original_price_without_tax*scale).toFixed(5);
		        	}
		        	
		        	$('#popup_epline__product_price_tax_incl').val(response.line.original_price_with_tax);
		        	$('#popup_epline__product_price_tax_excl').val(response.line.original_price_without_tax);
		        	
	        	}
	        	if(p.is_generic_product == true)
	        	{
	        		$('#popup_epline__manual_price_box, .popup_epline__manual_price_box').show();
	        	}
	        	else
	        	{
	        		$('#popup_epline__manual_price_box, .popup_epline__manual_price_box').hide();
	        	}
	        	
	        	//tarifs original du produit générique
	        	$('#popup_epline__original_tax_incl').html(response.product.original_price_tax_incl);
	        	$('#popup_epline__original_tax_excl').html(response.product.original_price_tax_excl);
	        	
	        	$('#popup_epline__product_name').val(response.line.label);
	        	$('#popupEditCartLine .product_name').html(response.product.product_name);
	        	
	        	
            	$('#popupEditCartLine').data('id_ticket_line', request.id_ticket_line);
	        	
				$('#popupEditCartLine')
		    		.data('idobject',id_product)
		    		.data('idobject2',id_product_attribute);
				$('#popupEditCartLine .acaisse_productcell_content').css('background-image','');
				$('#popupEditCartLine .acaisse_productcell_content').data('idobject','').data('idobject2','');
				$('#popupEditCartLine .stock_available').removeClass('warning').removeClass('error').removeClass('good').html('');
				
				// On affiche le prix d'achat & marge	
				$('#popupEditCartLine .whole_sale_price_ht').html(parseFloat(response.product.whole_sale_price_tax_excl).toFixed(2));	
				$('#popupEditCartLine .whole_sale_price_ttc').html(parseFloat(response.product.whole_sale_price_tax_incl).toFixed(2));	
				$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+parseFloat(response.product.margin_rate).toFixed(2)+'%</span>');	
				$('#popupEditCartLine .original_margin_rate').html(parseFloat(response.product.margin_rate).toFixed(2)+'%');	
						
				//console.log(['LINE',response.line]);
				$('#popupEditCartLine').data('price', response.line.price_with_tax);
				
				$('#popupEditCartLine .stock_inqueue').html('');
				if($(cart_line_id+' .display_prix_final_ttc').length > 0)
				{
					$('#popupEditCartLine .product_price').html($(cart_line_id+' .display_prix_final_ttc').html());
				}
				else
				{
					$('#popupEditCartLine .product_price').html($(cart_line_id+' .specialPrice').html());
				}
				$('#popupEditCartLine .acaisse_productcell_content').css('background-image','url('+$(cart_line_id+' .img img').attr('src')+')');
				
				$('#popupEditCartLine .stock_real').html('');
				
				/*** réduction appliquée sur la ligne **/
				
				
				$('#popupEditCartLine .cart_line_reduc_amount').data('id_product',id_product);
				$('#popupEditCartLine .cart_line_reduc_amount').data('id_product_attribute',id_product_attribute);
				//$('#popupEditCartLine .cart_line_reduc_amount').append('');
				
				
				if (inputValue>0) {
					$('#popupEditCartLine .lbabK input.lbabK-inputabl').val(inputValue);
				}
				else
				{
					$('#popupEditCartLine .lbabK input.lbabK-inputabl').val('');
				}
								
				switch(type_reduc)
				{
					case '€':
						$('#popupEditCartLine .selected').removeClass('selected');
						$('#popupEditCartLine .reductAmountInEuro').addClass('selected');
						break;
					case '%':
						$('#popupEditCartLine .selected').removeClass('selected');
						$('#popupEditCartLine .reductAmountInPercent').addClass('selected');
						break;
					case 'gift':
						$('#popupEditCartLine .selected').removeClass('selected');
						$('#popupEditCartLine .reductAmountInGift').addClass('selected');
						break;			
				}
				
				//$('#popupEditCartLine .product_price').html($('#popupEditCartLine').data('price')+'€');
				
				$('#popupEditCartLine').prependTo($('body'));
				$('#popupEditCartLine').css('display','table');
				
				//mise en place des handlers de saisi du prix
				
				//modification manuelle du prix de vente TTC
				$('#popupEditCartLine').on('change keyup','#popup_epline__product_price_tax_incl',function(){
					
					var ttc = parseFloat('0'+$(this).val());
					var o_ttc = parseFloat($('#popup_epline__original_tax_incl').html()).toFixed(5); 
					var o_ht = parseFloat($('#popup_epline__original_tax_excl').html()).toFixed(5);
					
					var ht = 0;
					
					if(o_ttc == o_ht)
					{
						var ht = ttc;
					}
					else
					{	
						var ht = ttc*o_ht/o_ttc;
					}
					
					$('#popup_epline__product_price_tax_excl').val(parseFloat(ht).toFixed(5));
					
					refreshLiveDiscountPreview($('#popupEditCartLine .lbabK-inputable').val());
					
				});
				
				$('body').on('click tap','.cart_line_reduc_amount .chosseDiscountType > div',function() {
					
					$('.chosseDiscountType > div').removeClass('selected');
					$(this).addClass('selected');
					refreshLiveDiscountPreview($('#popupEditCartLine .lbabK-inputable').val());
				});
				
				//lors du changment du forcage ou non du prix manuellement
				$('#popupEditCartLine').on('change click blur','#popup_epline__original_price_is_forced_by_user_off,#popup_epline__original_price_is_forced_by_user_on',function(){
					
					
					//pas de prix forcé par l'utilisateur
		        	if($('#popup_epline__original_price_is_forced_by_user_on').prop('checked') != true)
		        	{	        		
			        	//$('#popup_epline__product_price_tax_incl').html(response.product.original_price_tax_incl);
			        	//$('#popup_epline__product_price_tax_excl').html(response.product.original_price_tax_excl);
			        	$('#popup_epline__manual_price_box').hide();
			        	$('#popup_epline__original_price_is_forced_by_user_on').prop('checked',false);
			        	$('#popup_epline__original_price_is_forced_by_user_off').prop('checked',true);
			        }
		        	else
		        	{
			        	//$('#popup_epline__product_price_tax_incl').val(response.line.original_price_with_tax);
			        	//$('#popup_epline__product_price_tax_excl').val(response.line.original_price_without_tax);
			        	$('#popup_epline__manual_price_box').show();
			        	$('#popup_epline__original_price_is_forced_by_user_off').prop('checked',false);
			        	$('#popup_epline__original_price_is_forced_by_user_on').prop('checked',true);
			        }
					
				});
				
				refreshLiveDiscountPreview();
				
	        } else {
	        	alert('Erreur O-00001 : impossible d\'obtenir les détails sur ce produit (#'+id_product+')');
	        }
	    }).fail(function () {
	        alert('Erreur O-00002 : impossible d\'obtenir les détails sur ce produit (#'+id_product+')');
	    });
		
		
    	
	};
	
});



/************************************************************************************
 * POPUP DE REDUCTION GLOBAL + informations complementaire SUR La VENTE
 ************************************************************************************/
$(function(){
	
	var showPopupGlobalCartReduction = function(reduc,type_reduc,inputValue) {
		//console.log('showPopupGlobalCartReduction('+reduc+','+type_reduc+','+inputValue+')');
		
		if (inputValue>0) {
			$('#popupGlobalCart .lbabK input.lbabK-inputabl').val(inputValue);
		}
		else
		{
			$('#popupGlobalCart .lbabK input.lbabK-inputabl').val('');
		}
		
		$('#popupGlobalCart .selected').removeClass('selected');
		
		switch(type_reduc)
		{
			case '€':
				$('#popupGlobalCart .reductAmountInEuro').addClass('selected');
				break;
			case '%':
				$('#popupGlobalCart .reductAmountInPercent').addClass('selected');
				break;
			case 'gift':
				$('#popupGlobalCart .reductAmountInGift').addClass('selected');
				break;			
		}
		
		$('#popupGlobalCart').prependTo($('body'));
		$('#popupGlobalCart').css('display','table');
	};
	
	//ajout du clavier de saisis remise gobale
	$('#popupGlobalCart .selectProductFloatNb').lbabKeyboard({ //clavier numerique pour la saisie de quantité
        'layout': 'floatNumeric',
        //'min' : 0,
        //'max' : 12,
        onType: function(value,$elt) {        	
        	//refreshLiveGlobalDiscountPreview(value,$elt);        	
        },
        onReturn: function (value) {        	
            if ($('#popupGlobalCart .lbabK-inputable').val()=='') {
            	$('#popupGlobalCart .lbabK-inputable').val(0);
            }
            var discount = Math.round(parseFloat(value)*100)/100;
            var type = $('#popupGlobalCart .selected').data('type');
            if(type == undefined || discount == 0)
            {
            	type = '€';
            	discount = 0;
            }

			$('.popup-overlay').css('display','none');

            registerDiscountOnCart(
            	discount,
            	type
        	);
        }
    });	
	
	$('#reductionGlobalOnCart').on('click tap',function() {
		
		if ($(this).hasClass('disabled') || $(this).parents('.disabled').length>0) {
        	//sendNotif('greenFont','Veuillez ouvrir la caisse afin de réaliser une vente.');
        	return false;
        }
		
		if (!ACAISSE.cartModificationLocked) {
    		//$('.button.validTicket').click();
    		_validateCurrentTicket(false);
    	}    	
		
		var reduc = $(this).parent().find('.pastille b').html();
		
		var type_reduc = '€';
		if (reduc!=undefined) {
			type_reduc = reduc[reduc.length-1];
		} else {
			reduc='-0€'; 
		}		
		
		var inputValue = Math.abs(parseFloat(reduc));
		
		// On récupère les informations du produit
		//_getProductInfos($(this).parent().data('id_product'));
		showPopupGlobalCartReduction(
			reduc,
			type_reduc,
			inputValue			
		);
		
	});
	
	$('body').on('click tap','#popupGlobalCart .chosseDiscountType > div',function() {
		
		$('.chosseDiscountType > div').removeClass('selected');
		$(this).addClass('selected');
		
		//var inputValue = $('#popupEditCartLine .lbabK-inputable').val();
		//refreshLiveDiscountPreview(inputValue);
	});
	
	$('body').on('click tap','#ticketDetail .header_ticket_toPrint .print_to_ticket',function(e) {$
		e.preventDefault();
		window.print();
	});
	
	$('body').on('click tap','#ticketDetail .header_ticket_toPrint .send',function(e) {
		e.preventDefault(); 
		$('#ticketDetail #send_form_email').val(ACAISSE.customer.email);
		$('#ticketDetail .content_ticket_toPrint').slideDown();
    	$('#ticketDetail .header_ticket_toPrint').slideUp();
	});
	
	$('#emptyPopup').on('click tap', '#ticketDetail #confirm_send', function (e){
    	e.preventDefault();
    	if(validate($('#send_form_email').val()) == true)
    	{
    		$('#emptyPopup .closePopup').click();
    		$(this).prop('disabled','disabled');
    		PRESTATILL_CAISSE.loader.start();
    		
    		$('#ticket_email').html('').append('<div style="width:650px;">'+$('#ticketDetail .ticket.toPrint').html()+'</div>');
			$('#ticket_email h2.gift_print').hide(); 
			
			html2canvas($('#ticket_email')[0],
			{
				windowWidth: '650px',
			}).then(function(canvas) {
			    
			    var base64image = canvas.toDataURL();
			    
			    $.ajax({
			        type: 'post',
			        dataType: 'json',
			        data: {
			            'action': 'SendTicketsByMail',
			            'id_ticket': $('#ticketDetail #id_ticket_to_send').val(),
				        'send_email': $('#send_form_email').val(),
				        imgBase64: base64image,
			        }
			    }).done(function (response) {
			        if (response.success != false) {
			        	
			        } else {
			          alert('Error : l_send0012 / Une erreur s\'est produite lors de l\'envoi du mail.');
			          //console.error(response.error);
			        }
			    }).fail(function () { 
			        alert('Error : l_send0011 / Une erreur s\'est produite lors de l\'envoi du mail.');
			    });  
			
			    ACAISSE_print_queue.shift();
		        // On confirme que le mail a bien été envoyé
		        PRESTATILL_CAISSE.loader.end();
		        //alert('Mail envoyé avec succès');
		        $('#ticket_email').html('');
		        
		        if(ACAISSE_print_queue.length > 0)
		        {
		        	// On imprime les tickets cadeau ou autres tickets rattachés
			        var next_ticket = ACAISSE_print_queue.shift();
					$('#printTicket').attr('class',next_ticket.class_name).html(next_ticket.html);			
					setTimeout(function(){			
						window.print();				
						printQueue();
					},500);
		        } 
		          
		        //on demande un passage au ticket suivant
         		_startNewPreorder(); 
         		
         		sendNotif('greenFont','<i class="fa fa-check"></i> Mail envoyé avec succès');
         	});
    	}
    	else
    	{
    		alert('Veuillez renseigner une adresse email correcte svp.');
    	}
    	
    });

});

$(function(){
	
	
	
	$('#deliveryButton').on('click tap',function() {
		if (!ACAISSE.cartModificationLocked) {
    		//$('.button.validTicket').click();
    		_validateCurrentTicket(false);
    	}    	
		
		
		// On récupère les informations du produit
		//_getProductInfos($(this).parent().data('id_product'));
		PRESTATILL_CAISSE.delivery.initPopupDatas();
		PRESTATILL_CAISSE.invoice.initPopupDatas();
		PRESTATILL_CAISSE.carrier.initPopupDatas();
		
		$('#popupDelivery').prependTo($('body'));
		$('#popupDelivery').css('display','table');
		
	});
	
});
    

var DEBUG_SEARCH_LEVEL = 0;

var progress = null;

function loadProductSearchList() {
	var filter = $('#productSearchResultsFilter input')[0].value.sansAccent().toLowerCase(); // on met tout en lowerCase pour etre insensible à la case
   
    if (filter.length<acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.nb_caract_for_search) {
    	if(DEBUG_SEARCH_LEVEL >= 3)
	    {
	    	console.log('Expression "'+filter+'" is too short less than '+acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.nb_caract_for_search + ' caracters');
	    }    	
    	return false;
    }
    
    
    $('#productResearchButton').html('<i class="fa fa-spinner fa-spin"></i>Recherche du serveur en cours...');
	$("#searchLoadSpinner").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
	
	if (progress) {
        progress.abort();
    }
    progress = $.ajax({
        type: 'post',
        data: {
            'action': 'findProduct',
            'research': filter,
            'id_pointshop' : ACAISSE.id_pointshop,
            'ajaxRequestID': ++ACAISSE.ajaxCount,
        },
        dataType: 'json',
    }).done(function (response) {
        if(ACAISSE.mode_debogue === true)
			console.log(['research product response',response]);
        
        var result = response.results;
        var hasMoreResult = false, bool;
        
        for (var i in result) {
        	bool = true; 
        	for (var j in buffer) {
	        	if (buffer[j].indexOf(result[i]+'')!=-1) {
	        		bool = false;
	        		break;
	        	}
        	}
        	if (bool) {
        		if (buffer[1000000]==undefined) {buffer[1000000]=[];}
	    		buffer[1000000].push(result[i]+'');
	    		hasMoreResult = true;
	    	}
        }
        
        if (hasMoreResult) {
        	$('#productResearchButton')
    			.addClass('button-success')
        		.html('<i class="fa fa-list-ul fa-5x"></i><br />Afficher les résultats supplémentaires');
        	$('#productResearchButton').prop('disabled',false);
        	$('#productResearchButton')[0].onclick = function() {
        		this.onclick=function(){};
        		$('#productResearchButton')
        			.removeClass('button-success')
        			.html('');
				generateHtmlResearchProductList(buffer);
        	}; // évènement unique
        } else {
        	$('#productResearchButton')[0].disabled = true;
        	$('#productResearchButton')
        			.removeClass('button-success')
        			.html('Aucun résultat supplémentaire à afficher');
        }
        
        progress = null;
        
    }).fail(function () {
        console.log('Erreur 000129b : Une erreur c\'est produite durant la recherche de client. Si cette erreur persiste veuillez contacter l\'administrateur du site.');
    });
    
    
    
    
    if(DEBUG_SEARCH_LEVEL >= 1)
    {
    	console.log('loadProductSearchList using expression "'+filter+'"');
    }
    
    var show_only_active_product = $('.button.toggle-button.activeProduct').is('.selected');
    
	var buffer = [];	
	var buffer_product = [];
	
	var nbrMatchs = 0;
	for (var i in acaisse_products_prices.catalog) {
		
		var p = _loadProductFromId(i);
		
		if(!p)
		{
			continue;
		}
				
		if (show_only_active_product === true && p.active != 1) {
			continue;
		}	
		if (!p.disponibility_by_shop[ACAISSE.id_pointshop]) {
			continue;
		}
		
		nbrMatchs = 0;

		if(
			$('.button.toggle-button.searchByName').is('.selected')
			&& p.product_name != undefined)
		{	
			
			
			var txt= p.product_name.sansAccent().toLowerCase();
			var index = txt.indexOf(filter);
			if (index!=-1) {
						
				if(DEBUG_SEARCH_LEVEL >= 3)
			    {
			    	console.log('searchByName is active for product "'+p.product_name+'" compare to '+filter);
			    }
						
				if(DEBUG_SEARCH_LEVEL >= 3)
			    {
			    	console.log('index founded '+index);
			    }
				
				nbrMatchs += index==0?2:1;
			}
			else
			{
						
				if(DEBUG_SEARCH_LEVEL >= 6)
			    {
			    	console.log('index NOT founded for "'+p.product_name+'" compare to '+filter);
			    }
				
			}
			
			
			if(p.has_attributes == true)
			{
				for(var j in p.attributes)
				{
					var txt= p.attributes[j].attribute_name.sansAccent().toLowerCase();
					var index = txt.indexOf(filter);
					if (index!=-1) {
						nbrMatchs += index==0?2:1;
					}
				}
			}
		
			if (nbrMatchs>0) {
				if(buffer_product[p.id_product] == undefined)
				{
					buffer_product[p.id_product] = 0;
				}
				buffer_product[p.id_product] += nbrMatchs;
			}	
			
						
		}
		
		if(
			$('.button.toggle-button.searchByRef').is('.selected')
			&& p.reference)
		{
			var txt = p.reference.sansAccent().toLowerCase();
			var index = txt.indexOf(filter);
			
			if (index !=-1) {
				nbrMatchs += index==0?20:10;
			}	
			
			if(p.has_attributes == true)
			{
				for(var j in p.attributes)
				{
					if(p.attributes[j].ref)
					{
						var txt= p.attributes[j].ref.sansAccent().toLowerCase();
						var index = txt.indexOf(filter);
						if (index!=-1) {
							nbrMatchs += index==0?20:10;
						}
					}
				}
			}	
		
			if (nbrMatchs>0) {
				if(buffer_product[p.id_product] == undefined)
				{
					buffer_product[p.id_product] = 0;
				}
				buffer_product[p.id_product] += nbrMatchs;
			}	
			
				
		}
		
		
			
	}
	
	
	if($('.button.toggle-button.searchByEAN13').is('.selected'))
	{			
		if (acaisse_products_prices.ean[filter] != undefined) {
			if(DEBUG_SEARCH_LEVEL >= 5)
		    {
		    	console.log('index NOT founded for EAN "'+filter+'"');
		    }
			if(buffer_product[acaisse_products_prices.ean[filter].id_product] == undefined)
			{
				buffer_product[acaisse_products_prices.ean[filter].id_product] = 0;
			}
			buffer_product[acaisse_products_prices.ean[filter].id_product] += 20;
		}
		else
		{					
			if(DEBUG_SEARCH_LEVEL >= 6)
		    {
		    	console.log('index NOT founded for EAN "'+filter+'"');
		    }			
		}
	}
	
	for(id_product in buffer_product)
	{
		nbrMatchs = buffer_product[id_product];
	
		if (buffer[nbrMatchs]==undefined) {
			buffer[nbrMatchs] = [];
		}
		
		buffer[nbrMatchs].push(id_product); 
	}			
	
	generateHtmlResearchProductList(buffer);	
    setTimeout(function(){
    	$("#searchLoadSpinner").html('<i class="fa fa-search fa-fw"></i>');
	},500);	
}



function generateHtmlResearchProductList(buffer) {
	//ACAISSE.mode_debogue = true;
	
	if(ACAISSE.mode_debogue === true)
		console.log(['buffer',buffer]);
	
	var cles = [], productBuffer=[];
	var nb_lines = 0;
	var type, style, col=0;
	for (var id in buffer) {
		cles.push(id);
	}
	cles.sort(function(x,y) {return y-x;}); // on trie les clés par ordre décroissant
	
	var html = '<table id="productSearchResultTr">';
	
	//html +='<tbody>';
	for (var i=0; i<cles.length; i++) {
		for (var j=0; j<buffer[cles[i]].length; j++) {
			// TODO : on ajoute à l'affichage buffer[cles][j] (cles représente le nombre de match qu'on a eu sur le produits)
			
			type=2;
			if (_loadProductFromId(buffer[cles[i]][j]).has_attributes) {
				style=3;
			} else {
				style=13;
			}
			html += '<tr>';
			//html += '<td id="cell_pdt_'+col+'" class="cellStyleDefault colspan1 rowspan1 cellType'+type+' cellStyle'+style+'" data-idobject="'+buffer[cles[i]][j]+'" data-idobject2="0">' + _injectCellProductContent({id_object:buffer[cles[i]][j], id_object2:0}) + '</td>';
			
			var add_or_choose = '<i class="fa fa-list-ol"></i>';
			
			if (_loadProductFromId(buffer[cles[i]][j]).has_attributes) {
				add_or_choose = '<i class="fa fa-list-ol"></i>';
			}
			
			//html += '<td>' + _injectCellProductContentInSearchResults({id_object:buffer[cles[i]][j], id_object2:0}) + '</td>';
			html += '<td id="cell_pdt_'+col+'" class="cellStyleDefault colspan1 rowspan1 cellType'+type+' cellStyle'+style+'" data-idobject="'+buffer[cles[i]][j]+'" data-idobject2="0">' + _injectCellProductContentInSearchResults({id_object:buffer[cles[i]][j], id_object2:0}) + '</td>'
			html += '</tr>';
			
			
			if (!_loadProductFromId(buffer[cles[i]][j]).has_attributes) { 
				productBuffer.push({
					id_product: buffer[cles[i]][j],
					id_product_attribute: 0,
					row: 'pdt',
					col: col
				});
			}			
			col++;
			nb_lines++;
		}
	}
	//html +='</tbody>';
	html += '</table>';
	
	//var largeur_cell = ($('#acaissepagecms tbody').width() / ACAISSE.nbCol);

	
	$('#productResearchList').html(html);
	//$('#productResearchList td').width(largeur_cell);
	console.log(parseInt(cles.length*65));
	if(parseInt(nb_lines*65) >= $("#searchProducts").height())
	{
		$('#productResearchList').height('100%');
	}
	
	_refreshProductPageStock(productBuffer);
}



/**
 * hanlder sur le sfiltres 
 */
$(function(){
	$('.search-col-filters .toggle-button').click(function(){
		$(this).toggleClass('selected');
		loadProductSearchList();
	});	
	
	$('#searchProducts').on('click tap','#resetSearchInputContent',function(){
		$('#productSearchResultsFilter input').val('');
		$('#productResearchList').html('');
		loadProductSearchList();
	});
	
    
    $('#productKeyboardResearch').lbabKeyboard({ //clavier azerty pour la recherche de produit
        'layout': 'azerty',
        'inputable': $('#productSearchResultsFilter input'),
        onType: function () {
            clearTimeout(scanner.customerKeyIntervalProcess);
            scanner.customerKeyIntervalProcess = setTimeout(function () {
                loadProductSearchList();
            }, 500);
        }
    });
	
	
});

$(function(){	
	$('body').on('click tap','#launchRegisterPayment',function(){
		PRESTATILL_CAISSE.payment.registerPayment();		
	});
	
	$('body').on('click tap','#launchRegisterRefund',function(){
		PRESTATILL_CAISSE.payment.registerRefund();		
	});
	
	
	/**
	 * Bouton d'annulation de la phase de paiement 
	 */
	$('body').on('click tap', '#paymentView .cancel.button', function(elt) {
		if ($(this).hasClass('disabled')) {
			alert('Vous avez déjà enregistré au moins un encaissement, vous devez soit solder la vente, soit la clôturer sans la solder en cliquant sur Paiement Partiel.');
		} else {
			ACAISSE.cartModificationLocked = false;			
			_gotoTicketView();
		}
	});

	$('body').on('click tap', '#paymentView .partial-validation', function(elt) {
		if ($(this).hasClass('disabled')) {
			alert('Votre commande ne correspond plus à un paiement partiel (Code 000611)');
		} else {
			if (confirm('Confirmez-vous la finalisation de cette commande non soldée?')) {
				getTicketCaisse();
			}
		}
	});
	
	$('body').on('click tap', '#paymentView .no-payment', function(elt) {
		if ($(this).hasClass('disabled')) {
			alert('Impossible de valider cette vente sans paiement (Code L000300)');
		} else {
			if (confirm('Confirmez-vous la finalisation de cette vente sans enregistrer d\'encaissement ?')) {
				PRESTATILL_CAISSE.payment.registerPayment();
				getTicketCaisse();
			}
		}
	});

	$('body').on('click tap', '#paymentView .validation.button', function(elt) {
		if ($(this).hasClass('disabled')) {
			alert('Veuillez d\'abord enregistrer un ou plusieurs paiements permettant de solder la totalité la commande (Code 000612)');
		} else {
			getTicketCaisse();
		}
	});
	
	
	/******** vieux script **********/
	
	/**
	 * enregistrer le paiement total de la commande
	 */
	function _DEPRECATED_registerFullPayement() {
		
		if (ACAISSE.payment_parts.length>0) {
			ACAISSE.cartModificationLocked = false;
			_startNewPreorder();
			return true;
		}
		
		
		ACAISSE.allActionLocked = true;
		$('#registerPayment').prepend('<i class="fa fa-spinner fa-spin"></li>');
		
		$.ajax({
			type: 'post',
			data: {
				'action': 'RegisterFullPaiement',
				'id_ticket': ACAISSE.ticket.id,
				'id_pointshop': ACAISSE.id_pointshop,
				'ps_id_cart': ACAISSE.ticket.ps_id_cart,
				'id_customer': ACAISSE.customer.id,
				'id_group': ACAISSE.customer.id_default_group,
				'id_force_group': ACAISSE.id_force_group, //5
				'ajaxRequestID': ++ACAISSE.ajaxCount,
				'payment_module_name': ACAISSE.payment_parts.length>0?ACAISSE.payment_parts[0].payment_module_name:$('#' + ACAISSE.choosenPaymentMode).data('module-name'),
				'id_order_state': ACAISSE.payment_parts.length>0?13:$('#' + ACAISSE.choosenPaymentMode).data('id-order-state'),
				'cash_received': ACAISSE.choosenPaymentMode == 'payByCash' ? $('#cashReceived').html() : 0,
				'payment_parts' : ACAISSE.payment_parts,
				'payment_label' : $('.paymentButton.selected .label').html()
			},
			dataType: 'json',
		}).done(function (response) {
			if (response.success == true)
			{
				if(ACAISSE.mode_debogue === true)
				{console.log(['print response check', response]);}
				response = response.response;
				ACAISSE.ticket.lines = response.ticket.lines;
				$('#registerPayment i.fa-spinner').remove();
				//ticket suivant
				
				printTicket(response.ticket_caisse);
				
				ACAISSE.cartModificationLocked = false;
				
				_startNewPreorder();
				
				ACAISSE.lastCA = response.lastCa;
				_refreshLastCa();
			}
			else
			{
				alert('Erreur 00121 : ' + response.errorMsg);
			}
		}).fail(function () {
			alert('Erreur 00122 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
		});
	
	}
	
	
    //validation d'un ticket pour conversion vers commande prestashop
    $('.validTicket').on('click tap', function () {
        if ($(this).hasClass('disabled') || $(this).parents('.disabled').length>0) {
        	sendNotif('greenFont','Veuillez ouvrir la caisse afin de réaliser une vente.');
        	return false;
        }   
        
        _validateCurrentTicket();
        _gotoPaymentPanel();    
        $('#paymentView').find('.prestatill_payment_interface .numeric_keyboard').show();         
    });


	/**
	 * Méthode initiant la transformation d'un ticket en panier Prestashop (cart)
	 */
	function _validateCurrentTicket(open_payment_popup) {
		
		if(open_payment_popup == undefined)
		{
			open_payment_popup = true;
		}
		
		//si le ticket est vide rien à faire
		var error = false;
		if (ACAISSE.ticket.lines.length == 0)
		{
			alert('Veuillez d\'abord sélectionner au moins un produit');
			error = true;
		}
		else if (acaisse_profilesNoCheckinIDs.indexOf('' + ACAISSE.customer.id) != -1)
		{
			alert('Veuillez d\'abord sélectionner un client.');
			error = true;
		}
		else if (!error)//tout est bon, on va demander au serveur la conversion du ticket en véritable commande Prestashop
		{			
			$.ajax({
				type: 'post',
				data: {
					'action': 'ValidateTicket',
					'id_ticket': ACAISSE.ticket.id,
					'id_customer': ACAISSE.customer.id,
					'id_group': ACAISSE.customer.id_default_group, //3
					'id_force_group': ACAISSE.id_force_group, //5
					'ajaxRequestID': ++ACAISSE.ajaxCount,
					'open_payment_popup' : open_payment_popup,
				},
				dataType: 'json',
			}).done(function (response) {
				if (response.success == true) {
					request = response.request;
					response = response.response;
					
					if (response.id_not_found.length>0) {
						var p = _loadProductFromId(response.id_not_found[0]);
						alert("ATTENTION !! Le produit #"+p.id_product+" : "+p.product_name+" n'as pas été correctement ajouté au ticket. Il se peut que la quantité minimal de commande dans la fiche produit soit inférieur à la quantité actuelle du ticket.");
						return false;
					}
					
					//console.log(['validateTicket','response',response]);
					ACAISSE.cart_tot = response.cart_tot;
					ACAISSE.cart_tot_ht = response.cart_tot_ht;
					ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
					
					//console.log('ACAISSE.cart_tot : ' + ACAISSE.cart_tot);
					//alert(ACAISSE.ticket);
					ACAISSE.ticket = response.ticket;
					ACAISSE.cartModificationLocked = true; //on bloque les action d'ajout de produits et de modification des quantités
					
					refreshPrices(response.cart_products);
					
					var module_obj = {action:'tot',tot:formatPrice(response.cart_tot_ttc),id_ticket:response.ticket.ticket_num};
					modules_action(module_obj);
					
					if(request.open_payment_popup !== false)
					{
						_gotoPaymentPanel();
					}
				} else {
					alert('Erreur 00177C : ' + response.errorMsg);
				}
			}).fail(function () {
				alert('Erreur 00178C : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
			});
		}
	}


	

	/******************************************************************************************************/
	/******************************************************************************************************/
	/******************************************************************************************************/
	//LA SUITE VA ETRE DEPRECIEE A LA SORTIE DE SIREEN
    $('#registerPayment').on('click tap', function () {
        if ($(this).hasClass('disabled'))
        {
            alert('Veuillez d\'abord sélectionner un moyen de paiement');
        }
        else
        {
            if (ACAISSE.allActionLocked !== true)
            {
            	if(ACAISSE.mode_debogue === true) {console.log(ACAISSE.ticket.ps_id_cart);}
            	//if(prompt('message de deboguage, cliquez sur ok/oui pour valider'))
            	//{
            		_registerFullPayement();
            		//on cherche un paramètre pour savoir s'i oui ou non le client veut un ticket pour lui'il faut un deuxieme ticket
            	//}
            }
        }
    });
    
});
function _switchToDefaultCustomer() {
	_switchCustomer(acaisse_profiles[acaisse_profilesIDs[0]], false, []);
}

function _getGroupColorForCustomer(customer)
{
  var id_default_group = customer.id_default_group;
  for(var i=0 ; i < acaisse_group.length ; i++)
  {
    if(acaisse_group[i].id_group == customer.id_default_group)
    {
      return acaisse_group[i].color;
    }
  }
  
}

function _getGroupForCustomer(customer)
{
  var id_default_group = customer.id_default_group;
  for(var i=0 ; i < acaisse_group.length ; i++)
  {
    if(acaisse_group[i].id_group == customer.id_default_group)
    {
      return acaisse_group[i];
    }
  }
  return false;  
}

function _switchCustomer(customer, initMode, additionnal_customer_informations) {
	if(ACAISSE.mode_debogue === true)
	{
		console.log('_switchCustomer()');
		console.log('ACAISSE.cartModificationLocked = '+ACAISSE.cartModificationLocked?'true':'false');
	}
	/*
	if (ACAISSE.cartModificationLocked == true)
	{
		
		$.ajax({
			type: 'post',
			dataType: 'json',
			data: {
				'action': 'CancelCartFromTicket',
				'id_ticket': ACAISSE.ticket.id,
				'customer': customer,
				'additionnal_customer_informations' : additionnal_customer_informations,
				'initMode': initMode,
				'deleteCartRules':true
			}
		}).done(function (response) {
			if (!response.success) {
				alert('Erreur 001558' + response.errorMsg);
				return;
			}
			
			ACAISSE.ticket = response.response.ticket;

			//remise en place du panneau non encaisse
			$('#orderButtonsPanel').hide();
			$('#preOrderButtonsPanel').show();
			$('#orderButtonsPanel .paymentButton.selected').click();
			$('#preOrderButtonsPanel #registerPayment').addClass('disabled');
			ACAISSE.choosenPaymentMode = false;
			ACAISSE.cartModificationLocked = false;
			ACAISSE.cart_tot = false;
			
			//modification de la couleur du boutton du customer en fonction de la couleur de son groupe principal
			var group = _getGroupForCustomer(response.request.customer);
			
			ACAISSE.price_display_method = group.price_display_method;
			$('#userName').css('background-color',group.color);
			
			if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1)
			{
				$('.product_base_price').show();
			}
			else
			{
				$('.product_base_price').hide();
			}
			
			
			//puis rappel de l'instruction initiale
			_switchCustomer(response.request.customer, response.request.initMode, response.request.additionnal_customer_informations);
		}).fail(function () {
			alert('Error 001559 Remove one qu after cancel real cart failed.');
		});

		return; //on stop ici, la commande sera retenté qu'après
		// annulation du panier et annulation du passage à l'encaissement	
	}
	*/
	//sinon on peut réaliser l'opération directement.
	var old_price_group = ACAISSE.id_force_group;
	if (typeof (initMode) == 'undefined')
	{
		initMode = false;
	}
	
	//maj des infos dans ACAISSE
	ACAISSE.customer = customer;
	ACAISSE.additionnal_customer_informations = additionnal_customer_informations;
	ACAISSE.customer.id_default_group = customer.id_default_group;
	ACAISSE.id_force_group = _searchSubGroup(customer.id_default_group);
	ACAISSE.customer.groups = customer.groups;

	//if(initMode==true) { return; } //lors de la phase d'initilisation de l'ui on ne peut pas poursuivre.
	
	var module_obj = {action:'demo'};
	
	/*
	lcd_write_line('Démonstration de la',0,undefinedValue,undefinedValue,module_obj);
	lcd_write_line('Caisse PrestaTill !',1);
	lcd_write_line('',2);
	lcd_write_line('',3);
	*/
  //modification de la couleur du boutton du customer en fonction de la couleur de son groupe principal
  
    var group = _getGroupForCustomer(customer);
    $('#userName').css('background-color',group.color);
    
	ACAISSE.price_display_method = group.price_display_method;
	
	_retreiveCustomerInformation(ACAISSE.customer.id);
	
	if (old_price_group != ACAISSE.id_force_group) //il y a eu changement de groupe
	{
		refreshPrices();
	}
	
	if (initMode != true && ACAISSE.current_pointshop.datas.editable_customer == '1' && acaisse_profilesIDs.indexOf('' + customer.id) == -1) {
		$('#userEditContainer [data-page-id="pageUser1"]').html('<i class="fa fa-pencil-square-o" unselectable="on" style="-webkit-user-select: none;"></i> Informations du client');
	} else {
		$('#userEditContainer [data-page-id="pageUser1"]').html('<i class="fa fa-pencil-square-o" unselectable="on" style="-webkit-user-select: none;"></i> Nouveau client');
		$('#userEditContainer [data-page-id="pageUser1"]').click();
	}
	
	$("#editUserButton").show();
	
	_updateCustomerEditForm(ACAISSE.customer);
	// LAURENT A VERIF
	//_refreshTicket();
	closeAllPanel();
	//_reloadPage();
}

function _retreiveCustomerInformation(id_customer) {
	//console.warn(id_customer);
	$('#username').addClass('loading');
	//.html('Chargement...');

	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'RetreiveCustomerInformation',
			'id_customer': id_customer,
		},
	}).done(function (response) {
		if (response.success)
		{
			//console.log(response.response);
			ACAISSE.customer = response.response.customer;
			ACAISSE.customer.address = response.response.address;
			ACAISSE.customer.addresses = response.response.addresses;
			ACAISSE.customer.groups = response.response.groups;
			ACAISSE.additionnal_customer_informations = response.response.additionnal_customer_informations;
			_refreshCustomerInformation();
			_updateCustomerEditForm(ACAISSE.customer, ACAISSE.customer.groups);
			refreshPrices();
			
			/*$.each(PRESTATILL_CAISSE.customer.hooks['afterRetreiveCustomerInformations'], function(i,hook_callback){
				hook_callback(response.response);
			});*/
			PRESTATILL_CAISSE.executeHook('afterRetreiveCustomerInformations');
		}
		else
		{
			alert('Erreur 00922 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 00923');
	});
}

function _refreshCustomerInformation() {
	var html = '<i class="fa fa-spinner fa-spin"></i>';
	html += ACAISSE.customer.firstname + ' ' + ACAISSE.customer.lastname + '<span style="display:none;"> (' + ACAISSE.customer.id_default_group + '/'+ACAISSE.id_force_group+')</span>';
	if(ACAISSE.price_display_method == 1) //HT
	{
		$('body').addClass('ht').removeClass('ttc');
		$('.price_display_method').html('HT');
		html+='<span style="display: block; position: relative; top: -45px; font-size: 10px;">Prix affichés HT</span>';

	}
	else
	{		
		$('body').addClass('ttc').removeClass('ht');
		$('.price_display_method').html('TTC');
		html+='<span style="display: block; position: relative; top: -45px; font-size: 10px;">Prix affichés TTC</span>';

	}				
	$('#userName').removeClass('loading').html(html);
}

/**
 * Recherche le sous groupe lié au groupe par défaut principal de l'utilisateur 
 */
function _searchSubGroup(id_group)
{
	//je cherche un groupe avec id_pointshop = point de vente de la caisse
	//id_parent_group === id_group
	//et groupe actif
	for(var i = 0 ; i < acaisse_group.length ; i++)
  {
    if(acaisse_group[i].id_parent_group == id_group && ACAISSE.id_pointshop == acaisse_group[i].id_pointshop)
    {
      console.log('sub group '+acaisse_group[i].id_group+' for group '+id_group);
      return acaisse_group[i].id_group;
    }
  }
	return id_group;
}
$(function(){
	
	//delivery	
	$('#delivery_option_block').uiSelector({
      input : $('#delivery_option'),
      onChange : function(new_value) {      	
      	$('#popupDeliveryForm .delivery_address_column ul.adresse_selector_list').addClass('hide');
      	
      	if(new_value == PRESTATILL_CAISSE.delivery.STORE_DELIVERY) //retrait magasin
      	{      		
      		$('#popupDeliveryForm #id_delivery_address_store').removeClass('hide').find('li').eq(0).click();
      		$('#carrier_option_block .item[data-value="0"]').click();
      	}
      	
      	if(new_value == PRESTATILL_CAISSE.delivery.HOME_DELIVERY) //livraison à domicile
      	{
      		$('#popupDeliveryForm #id_delivery_address_home').removeClass('hide').find('li').eq(0).click();
      		$('#carrier_option_block .item[data-value="1"]').click();
      	}
      	
      }
    });
    
    
    $('#popupDeliveryForm').on('click tap','.delivery_address_column .adresse_selector_list li',function(){
    	$('#popupDeliveryForm .delivery_address_column .adresse_selector_list li').removeClass('selected');
    	$('#popupDeliveryForm .delivery_address_column input[name="id_delivery_address"]').val($(this).addClass('selected').data('id'));
    });
    
    //carrier
    	//chargement dynamique
    
    
    $('#popupDeliveryForm').on('click tap','.carrier_address_column .adresse_selector_list li',function(){
    	$('#popupDeliveryForm .carrier_address_column .adresse_selector_list li').removeClass('selected');
    	$('#popupDeliveryForm .carrier_address_column input[name="id_carrier_address"]').val($(this).addClass('selected').data('id'));
    });  
    
    
	//invoice	
	$('#invoice_option_block').uiSelector({
      input : $('#invoice_option'),
      onChange : function(new_value) {      	
      	//$('#popupDeliveryForm .invoice_address_column ul.adresse_selector_list').addClass('hide');
      	$('#popupDeliveryForm .invoice_address_column ul.adresse_selector_list li').eq(0).click();
      	
      	if(new_value == PRESTATILL_CAISSE.invoice.WITHOUT) //retrait magasin
      	{ 
      		$('#invoice_info_block').addClass('hide');
      	}
      	
      	if(new_value == PRESTATILL_CAISSE.invoice.WITH) //livraison à domicile
      	{
      		$('#invoice_info_block').removeClass('hide');  
      	}      	
      }
    });
    
    $('#popupDeliveryForm').on('click tap','.invoice_address_column .adresse_selector_list li',function(){
    	$('#popupDeliveryForm .invoice_address_column .adresse_selector_list li').removeClass('selected');
    	$('#popupDeliveryForm .invoice_address_column input[name="id_invoice_address"]').val($(this).addClass('selected').data('id'));
    });
    
    //detax	
	$('#detax_option_block').uiSelector({
      input : $('#detax_option'),
      onChange : function(new_value) {      	
      	//$('#popupDeliveryForm .detax_address_column ul.adresse_selector_list').addClass('hide');
      	$('#popupDeliveryForm .detax_address_column ul.adresse_selector_list li').eq(0).click();
      	
      	if(new_value == PRESTATILL_CAISSE.invoice.WITHOUT_DETAX) //retrait magasin
      	{      		
      		$('#detax_info_block').addClass('hide');
      	}
      	
      	if(new_value == PRESTATILL_CAISSE.invoice.WITH_DETAX) //livraison à domicile
      	{      		
      		$('#detax_info_block').removeClass('hide');      		
      	}
      	
      }
    });
    
    
        
    $('#popupDelivery').on('click tap','#popupDeliveryPopupValidateButton',function(){
    	var datas = $('#popupDeliveryForm').serializeArray();
    	datas.push({'name':'action', 'value':'SaveDeliveryPopupDatas'});
    	datas.push({'name':'id_pointshop', 'value':ACAISSE.id_pointshop});
    	datas.push({'name':'id_ticket', 'value':ACAISSE.ticket.id});
    	
    	PRESTATILL_CAISSE.loader.start();
    	
    	$.ajax({
	        type: 'post',
	        data: datas,
	        dataType: 'json',			
		}).done(function(response){
			if(response.success === true)
			{
				ACAISSE.ticket = response.response.ticket;
				$('#popupDelivery .closePopup').click();
				PRESTATILL_CAISSE.loader.end();
			}
			else
			{
				alert('Error O_deliv_1002');
			}
		}).fail(function(){
			alert('Error O_deliv_1003');
		});
    	
    	
    	//ex : delivery_option=0&id_delivery_address=8&carrier_option=1&id_carrier=35&invoice_option=1&id_invoice_address=1&detax_option=0&detax_info=dsqsq
    });
    
    
    
});

/**/
$(function() { // initialisation des claviers et des évènements associés

    $('#searchKeyboard').lbabKeyboard({ //clavier azerty pour la recherche de client
        'layout': 'azerty',
        'inputable': $('#customerSearchResultsFilter input'),
        onType: function () {
            clearTimeout(scanner.customerKeyIntervalProcess);
            scanner.customerKeyIntervalProcess = setTimeout(function () {
                sendCustomerSearch();
            }, 500);
        }
    });
    
    
    
    $('#selectProductFloatNb').lbabKeyboard({ //clavier numerique pour la saisie de quantité
        'layout': 'floatNumeric',
        onReturn: function () {
            
            //si on est dans le panneau des produits/pages
            if ($('#popupNumeric').parent().is("#productList")) {            	
                var quantity = $('#selectProductFloatNb .lbabK-inputable').val();
                if (quantity != '' && quantity != 0 && ACAISSE.allActionLocked !== true) {
                    var p = _loadProductFromId($('#popupNumeric').data('idobject'));
                    var id_ticket_line = false; //@TODO : retrouver ici le id_ticket_line si existant
                    quantity = Math.round(parseFloat(quantity)*1000); // TODO : recupérer le scale en fonction du produit depuis les params.units pour faire la multiplication
					
					
					
                    addToCart(
                       	$('#popupNumeric').data('idobject'),
                        $('#popupNumeric').data('idobject2'),
                        quantity,
                        false,
                        id_ticket_line
                    );
                }
            }/*
            // si on est dans le popup pour créer une ristourne sur une ligne de panier
            else if ($('#popupNumeric').parent().is("#ticketView") && $('#popupNumeric .cart_line_reduc_amount').length>0) {
                if ($('#selectProductFloatNb .lbabK-inputable').val()=='') {
                	$('#selectProductFloatNb .lbabK-inputable').val(0);
                }
                var discount = Math.round(parseFloat($('#selectProductFloatNb .lbabK-inputable').val())*100)/100;
                var type = $('#popupNumeric .selected').data('type');//.html();
                
                var p = _loadProductFromId($('#popupNumeric').data('idobject'));
                var a;
                if ($('#popupNumeric').data('idobject')>0) {
                	a = p.attributes[$('#popupNumeric').data('idobject2')]; 
                }
                
                var unitId = params.units.from.indexOf(p.unity);
                var scale = params.units.scale[unitId];
                if (a!=null && p.unity=="grammes" && a.attribute_name!='1g') {
                	scale=1;
                }
                
                if (type=='€' && scale!=1) { // si on a un produit au poids
                	discount/=scale;
                }
                
                registerDiscountOnCartLine(discount,type,$('#popupNumeric .cart_line_reduc_amount').data('id_product'),$('#popupNumeric .cart_line_reduc_amount').data('id_product_attribute'));
            }*/
            // si on est dans le popup pour créer une ristourne globale
            else if ($('#popupNumeric').parent().is("#ticketView") && $('#popupNumeric .global_reduc_amount').length>0) {
                if ($('#selectProductFloatNb .lbabK-inputable').val()=='') {
                	$('#selectProductFloatNb .lbabK-inputable').val(0);
                }
                var discount = Math.round(parseFloat($('#selectProductFloatNb .lbabK-inputable').val())*100)/100;
                var type = $('#popupNumeric .chosseDiscountType .selected').data('type');//.html();
                var extra_datas = $('#popup_epgloal__form').serialize();
                
                registerDiscountOnCart(discount,type, extra_datas);
            }
            //si on est dans le panneau des paiements
            else if ($('#popupNumeric').parent().is("#ticketView") && $('#popupNumeric .title').html()!='') {
            	var quantity = $('#selectProductFloatNb .lbabK-inputable').val();
            	var id_ticket_line = $('#popupNumeric').data('idticketline');
            	var p = _loadProductFromId($('#popupNumeric').data('idobject'));
                
                quantity = Math.round(parseFloat(quantity)*1000);
                
                var ids = {
                    id_product : $('#popupNumeric').data('idobject'),
                    id_product_attribute : $('#popupNumeric').data('idobject2'),
                };
                
                var ticketQuantity = Math.round(parseFloat($('.cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+' .quantity').text())*1000);
                
                if(quantity != '' && quantity != ticketQuantity){
                    if (quantity > ticketQuantity) {
                        addToCart(ids.id_product, ids.id_product_attribute, quantity - ticketQuantity);
                    } else if (quantity === '0'){
                        trashLineFromCart(ids.id_product, ids.id_product_attribute);
                    } else if (quantity < ticketQuantity){
                        removeOneFromCart(ids.id_product, ids.id_product_attribute, ticketQuantity - quantity);
                    }
                }
            }
            else if ($('#popupNumeric').parent().is("#ticketView")) {            	
            	var priceWished = Math.round(parseFloat($('#selectProductFloatNb .lbabK-inputable').val())*100)/100; // prix de la subdivision du paiement au centime
            	
            	var everRegisterToPay = 0;
            	$('#popupNumeric .wishedToPayPrice').each(function() {
            		everRegisterToPay += parseFloat(this.innerHTML);
            	});
            	
            	if (priceWished+everRegisterToPay>parseFloat($('#keepToHaveToPay').html()) || priceWished<0) {
            		alert('Montant choisit impossible');
            	} else {
            		registerPaymentPart(priceWished,$('#popupNumeric').data('paymentModule'),$('#popupNumeric').data('paymentModuleLabel'));
            	}
            	
            	$('#selectProductFloatNb .lbabK-inputable').val(0);
            }
            hidePopupAndKeyboard();
            if (params.declinaisonPopup.closeAfterSelect) {
            	$('#productList .overlay').click();
            }
        }
    });
    
    $('#selectProductNb').lbabKeyboard({ //clavier numerique pour la saisie de quantité
        'layout': 'numeric',
        onReturn: function () {
        	
            //si on est dans le panneau des produits/pages
            if ($('#popupNumeric').parent().is("#productList,#searchProducts")) {
            	
                var quantity = $('#selectProductNb .lbabK-inputable').val();
                if (quantity != '' && quantity != 0 && ACAISSE.allActionLocked !== true) {
                    addToCart(
                            $('#popupNumeric').data('idobject'),
                            $('#popupNumeric').data('idobject2'),
                            quantity);
                }
            }
            //si on est dans le panneau des paiements
            else if ($('#popupNumeric').parent().is("#ticketView")) {
            	
            	
                var quantity = $('#selectProductNb .lbabK-inputable').val();
                var id_ticket_line = $('#popupNumeric').data('idticketline');
                var ids = {
                    id_product : $('#popupNumeric').data('idobject'),
                    id_product_attribute : $('#popupNumeric').data('idobject2'),
                };
                
                var ticketQuantity = $('.mode-buy .cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+' .quantity .nbr').text().trim();
                
                /*
            	console.log('YEEEEEE BISSSSS');
            	console.log([
            		$('.mode-buy .cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+''),
            		$('.mode-buy .cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+' .quantity'),
            		$('.mode-buy .cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+' .quantity .nbr')
            		
            	]);
                */
                if(quantity != '' && quantity != ticketQuantity && ACAISSE.allActionLocked !== true){
                    if (quantity < ticketQuantity && quantity > 0) {
                        addToCart(ids.id_product, ids.id_product_attribute, parseInt(quantity - ticketQuantity));
                    }
                    else if(quantity === '0'){
                        trashLineFromCart(ids.id_product, ids.id_product_attribute);
                    }
                    else if(quantity > ticketQuantity){
                    	if((ticketQuantity - quantity) > 1)
                    	{
                        	removeOneFromCart(ids.id_product, ids.id_product_attribute, parseInt(ticketQuantity - quantity));
                    	}
                    	else
                    	{
                    		addToCart(ids.id_product, ids.id_product_attribute, parseInt(quantity - ticketQuantity));
                    	}
                    }
                }
            }
            hidePopupAndKeyboard();
            if (params.declinaisonPopup.closeAfterSelect) {
            	$('#productList .overlay').click();
            }
        }
    });

    $('#closeKeyboard').on('click tap', function(){
        hidePopupAndKeyboard();
    });
   
   
   
});
function _cashDeleteChar() { // efface un caractère de la somme donné par le client quand il paye en liquide
	
    if ($('#cashReceived').html().length > 0) {
        $('#cashReceived').html($('#cashReceived').html().substring(0, $('#cashReceived').html().length - 1));
    }

    if ($('#cashReceived').html() == '' || $('#cashReceived').html() == 'NaN') {
		$('#cashReceived').html('-');
		_refreshCashOverdue(0);
		//console.log('okazertyuiop');
    } else {
		var val = '0' + $('#cashReceived').html();
        var finishWithPoint = '.' == val.substr(val.length - 1, 1);
        val = parseInt(100 * parseFloat(val)) / 100;
        $('#cashReceived').html(val + (finishWithPoint ? '.' : ''));
        _refreshCashOverdue(val);
    }

}

function _cashAddChar(c) {  // ajoute un caractère de la somme donné par le client quand il paye en liquide   	
    if (c == ',') {
        c = '.';
    }

	if ($('#cashReceived').html() == '-') {
        $('#cashReceived').html('');
    }
	
    if ($.inArray(c, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']) != -1)
    {
        if (c == '.' && $('#cashReceived').html().length == 0)
        {
            c = '0.';
        }
        $('#cashReceived').html($('#cashReceived').html() + c);
    }
    else if (c == $('#calc-keyboard .calc-key-c').eq(0).html())
    {
        _cashDeleteChar();
    }

    if ($('#cashReceived').html() == '' || $('#cashReceived').html() == 'NaN' || $('#cashReceived').html() == '-') {
        $('#cashReceived').html('-');
		_refreshCashOverdue(0);
    }
    else
    {
        var val = /*'0' + */$('#cashReceived').html();
        var finishWithPoint = '.' == val.substr(val.length - 1, 1);
		var hasPoint = val.indexOf('.') != -1;
        
        if(hasPoint) //il y a un point
        {
        	var parts = val.split('.');
        	var entier = parts[0];
        	var solde = parts[1].substr(0,2);
        	val = entier+'.'+solde;
        	
        	//console.log(val,parts,entier,solde);
        	
        	$('#cashReceived').html(val);
        }
        else
        {
        	$('#cashReceived').html(val);
        }
        _refreshCashOverdue(val);
        
        return;
    }
}

function _refreshCashOverdue(received) {
	lcd_write_line(' ',0);
	lcd_write_line(' ',1);
	var diff = received - ACAISSE.cart_tot;
	if (diff == 0) //le compte est bon
	{
		var module_obj = {action:'payment_exact'};
		
		$('#cashMessage').html('Appoint correct').removeClass('more less').addClass('ok');
		$('#registerPayment').removeClass('disabled');
		lcd_write_line(' ',2,undefinedValue,undefinedValue,module_obj);
	}
	else if (diff > 0) //trop percu, il faut rendre
	{
		 $('#cashMessage').html(formatPrice(diff) + ' € à rendre').removeClass('ok less').addClass('more');
		$('#registerPayment').removeClass('disabled');
		
		var module_obj = {action:'payment_overdue',received:formatPrice(received),diff:formatPrice(diff)};
		ACAISSE.rendu = diff;
		ACAISSE.recu = received;
		
		lcd_write_line('Reçu '+formatPrice(received) + '€',1,undefinedValue,undefinedValue,module_obj);
		lcd_write_line('Rendu '+formatPrice(diff) + '€',2);
	}
	else //pas assez percu, il manque ...
	{
		$('#cashMessage').html('Il manque ' + formatPrice(diff*-1) + ' €').removeClass('more ok').addClass('less');
		$('#registerPayment').addClass('disabled');
		
		var module_obj = {action:'payment_lack',received:formatPrice(received),diff:formatPrice(diff)};
		
		lcd_write_line(' ',2,undefinedValue,undefinedValue,module_obj);
	}
}

$(function(){
	window.onresize = function() {
		_setWidthAndHeight();
	};
});


function _gotoTicketView() { //Horizontal navigation
	ACAISSE.scanbarcodeMode = 'product';
	var moveTo = ACAISSE.till_width-$('#productList > .switcher').outerWidth();
	$('#middle').animate({'margin-left': -moveTo + 'px'});
	$('#productList .switcher').removeClass('show-ticket').addClass('show-product-list');
	$('#productList .switcher h6').html('Catalogue');
	$('#productList .switcher .boxCart i').removeClass().addClass('icon-uniE600');
	$('#productList .switcher #totalItem').fadeOut();
	PRESTATILL_CAISSE.loader.showActions();
}

function _gotoProductList() { //Horizontal navigation
	ACAISSE.scanbarcodeMode = 'product';	
	$('#middle').animate({'margin-left': 0});
	$('#productList .switcher').addClass('show-ticket').removeClass('show-product-list');
	$('#productList .switcher h6').html('Mon ticket');
	$('#productList .switcher .boxCart i').removeClass().addClass('icon-cart2');
	$('#productList .switcher #totalItem').fadeIn();
	PRESTATILL_CAISSE.loader.showActions();
}

/**
 * Pour switcher sur le dernier panneau correspond au panneau de paiement 
 */
function _gotoPaymentPanel() {	
	PRESTATILL_CAISSE.payment.initOnOpenPaymentInterface();
	ACAISSE.scanbarcodeMode = 'payment';
	var moveTo = 2*ACAISSE.till_width-$('#productList > .switcher').outerWidth();//-$('.orderButtons').outerWidth();//;
	$('#middle').animate({'margin-left': -moveTo + 'px'});
}

/**
 * @TODO : Pour allez sur le panneau de sélection du point de vente 
 */
function _gotoPointhopChooser() {
	var moveTo = -ACAISSE.till_width;
	ACAISSE.scanbarcodeMode = 'product';
	$('#middle').animate({'margin-left': -moveTo + 'px'});
}



function _setWidthAndHeight(){
	
	var header_height = footer_height = 64;
	
	if(ACAISSE.mode_debogue === true)
	{
		console.log('_setWidthAndHeight()');
	}
	
	if (window.innerHeight>acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_height) {
		ACAISSE.till_height = window.innerHeight;
	} else {
		ACAISSE.till_height = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_height;
	}
	
	if (window.innerWidth>acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_width) {
		ACAISSE.till_width = window.innerWidth;
	} else {
		ACAISSE.till_width = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_width;
	}

	$('#content').width(ACAISSE.till_width).height(ACAISSE.till_height);
	//largeur multiplier par 3 car 3 panneaux horizontaux
	$('#middle').width(ACAISSE.till_width*4).css('left','-'+ACAISSE.till_width+'px');
	$('.panel').width(ACAISSE.till_width);
	
	var switcherWidth = $('.switcher').outerWidth();
	$('#productList .content, #ticketView .content .content, #ticketView').width(ACAISSE.till_width-switcherWidth-1);
	
	$('#paymentView').css({		
		    'position': 'relative',
		    'width' : (ACAISSE.till_width)+'px', //-$('.orderButtons').outerWidth()
		    'top' : 0,
		    'height': ACAISSE.till_height-header_height-footer_height+'px',
		    'left': (3*ACAISSE.till_width-switcherWidth)+'px',
	});
	
	$('#paymentView .flexrow').css({				
		   // 'margin-top': -(ACAISSE.till_height-header_height-footer_height)+'px',
		   'position' : 'relative',
	});
	
	$('#popupNumeric').width(ACAISSE.till_width-switcherWidth);
	
	if($('#productList .switcher').hasClass('show-ticket'))
	{
		var halfTillWidth = parseInt((ACAISSE.till_width-switcherWidth)/2);
	}
	else
	{
		var halfTillWidth = parseInt((ACAISSE.till_width)/2);
	}
	
	$('body style.inline-resize-style').remove();
	
	$('body').append('<style class="inline-resize-style">#popupNumeric {left:50%;margin-left: '+-halfTillWidth+'px!important;}</style>');
	//$('body').append('<style>#popupNumeric {margin-left: calc((100% - 800px) / 2) !important;}</style>');

	var orderButtonsWidth = $('.orderButtons').outerWidth();
	$('#ticketView .inner-content').outerWidth(ACAISSE.till_width-switcherWidth-orderButtonsWidth-1);
	
	var sumHeight = 0;
	$('#customerListContainer>div').each(function(i) {
		sumHeight += $(this).height();
	});
	if (sumHeight > $('#customerListContainer').height()) {
		$('#defaultCustomersProfils,#customerSearchResults')
			.css('height','100px')
			.css('overflow-x','auto')
			.css('overflow-y','hidden')
			.css('white-space','nowrap');
		$('#customerListContainer .preview').css('height','150px');
	}
	
	if ($('#specialsContainer').height() > ACAISSE.till_height-128) {
		$('#specialsContainer ul, #specialsContainer .tab-container li').css('height','477px');
		$('#specialsContainer .tab-content').css('overflow','hidden');
		$('#specialsContainer>.content').css('height',$('#specialsContainer ul').css('height'));
		$('#specialsContainer').css('height',$('#productList').css('height'));
	}
	
	if ($('#userEditContainer>.content').height() > ACAISSE.till_height-128) {
		$('#userEditContainer>.content').css('height', (ACAISSE.till_height-128-$('#userEditContainer .reverse').height())+'px');
		$('#userEditContainer').css('height', (ACAISSE.till_height-128)+'px');
	}
	
	$('#productList > div.switcher.show-ticket').css('border-right-width','4px');
	
	
	/* TEST MOBILE VIEW ONLY WITH DOUCHETTE ^^ */
	if(window.innerWidth < 600)
	{
		//console.log(window.innerWidth);
		$('#ticketView').width(window.innerWidth+4);
		$('#ticketView .inner-content').width(parseInt(window.innerWidth));
		$('#ticketView th.cost').attr('colspan',2);
		$('#veille .closeBt').trigger('click');
	}
	
	
	setTimeout(function() { // timeout nécéssaire pour une raison inconnu :/
		resetTileHeight();
	},500);
		
}

function resetTileHeight() {
	$('#acaissepagecms tbody').css('display','block'); // Pête les grosses tuiles de la caisse
	$('#acaissepagecms tbody').css('width','100%');
	
	$('#acaissepagecms tbody').css('overflow','auto');
	
	if ($('#productList > div.switcher.show-ticket').outerHeight()!=null) { // ça vaut null quand on imprime le ticket à cause du media print je suppose :/
		$('#acaissepagecms tbody').css('height',($('#productList > div.switcher.show-ticket').outerHeight()-5)+'px');
	}
	$('#acaissepagecms tbody td').css('border','5px solid '+colorLaurent);
	$('#acaissepagecms').css('border','0px');
	
	var hauteur_cell = ((($('#acaissepagecms tbody').height()-3)/acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.max_display_row)-1*ACAISSE.nbRow);
	var largeur_cell = ($('#acaissepagecms tbody').width() / ACAISSE.nbCol);
	
	$('#acaissepagecms tbody td').css('height',hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan2').css('height',2*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan3').css('height',3*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan4').css('height',4*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan5').css('height',5*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan6').css('height',6*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan7').css('height',7*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan8').css('height',8*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan9').css('height',9*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan10').css('height',10*hauteur_cell+'px');
	$('#acaissepagecms tbody td').css('width',largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan2').css('width',2*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan3').css('width',3*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan4').css('width',4*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan5').css('width',5*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan6').css('width',6*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan7').css('width',7*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan8').css('width',8*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan9').css('width',9*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan10').css('width',10*largeur_cell+'px');
	
	if((($('#acaissepagecms tbody').width() / ACAISSE.nbCol)/10) > 13) {
		$('#acaissepagecms').css('font-size',(($('#acaissepagecms tbody').width() / ACAISSE.nbCol)/10)+'px');
	} else {
		$('#acaissepagecms').css('font-size','13px');
	}
	
	$('.product_stock').each(function() {
		if ($(this).html().trim()=='') {
			$(this).css('display','none');
		}
	});
	
	if ($('#acaissepagecms > tbody').length>0) {
		if ($('#acaissepagecms > tbody')[0].scrollHeight - $('#acaissepagecms > tbody').height() <= 10) { // si on a 5px ou moins a scroller
			$('#acaissepagecms > tbody').css('overflow','hidden');
		} else {
			$('#acaissepagecms > tbody').css('overflow','auto');
		}
	}
	
	$('div .left').each(function() {$(this).height($('#printTickets').height()-52);});
	
	var height = $('#searchProducts').height() - $('#searchProducts .searchBox').height() - 30;
	if (height>250) {height=250;}
	
	var width = (height*180)/250; 
	
	//$('#productResearchList td').width(width);
	//$('#searchProducts > div').first().height(height);
	
	$('#printTickets .right').css('margin-top',($('#printTickets').height()/2 - $('#printTickets .right').height()/2)+'px');
	$('#preOrderListContainer .right').css('margin-top',($('#preOrderListContainer').height()/2 - $('#preOrderListContainer .right').height()/2)+'px');
	/*
	var veilleBoxHeight = $('#veilleBox').height();
	$('#veilleBox').css('margin-top',-veilleBoxHeight/2);
	*/
	
	
}

var undefinedValue; 
var ACAISSE = {
  id_pointshop:null,
  total_cost:0,
  total_cost_ht:0,
  mode_debogue:true
};
var colorLaurent = '#363636'; //363636
var ping_interval = 120000;

// on va charger les caches
var products_stock_cache = localStorage.getItem("products_stock_cache");
if (products_stock_cache==null) {
	products_stock_cache = {};
} else {
	products_stock_cache = JSON.parse(products_stock_cache);
}


window.onbeforeunload = function() { // lors de la fermeture de la page, on enregistre tout les caches en localStorage
	localStorage.setItem("products_stock_cache",JSON.stringify(products_stock_cache));
	
	for (var i=0; i<windows.length; i++) {
		windows[i].close();
	}
};

Date.toString = function(date, lang) { // transfère une date 'Y-m-d H:i' vers une date 'd/m/Y H:i'
	// date : string -> 'Y-m-d H:i'
	if (lang==undefined || lang=='fr') { // langue par défaut : francais
		var b = date.split(' ');
		var a = b[0].split('-');
		
		if ((typeof a[2]=='string' && a[2].length<2) || (typeof a[2]!='string' && a[2]<10)) {a[2]='0'+a[2];}
		
		//if (a[1]<10) {a[1]=a[1];} // ? ^^
		
		if (b.length>1) {
			var c = b[1].split(':');
			if ((typeof c[0]=='string' && c[0].length<2) || (typeof c[0]!='string' && c[0]<10)) {c[0]='0'+c[0];}
			return a[2]+'/'+a[1]+'/'+a[0]+' '+c[0]+':'+c[1];
		}
		
		return a[2]+'/'+a[1]+'/'+a[0];
	}
}

String.prototype.sansAccent = function() { // dégage les accens d'une chaine de caractère
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
     
    var str = this;
    for(var i = 0; i < accent.length; i++){
        str = str.replace(accent[i], noaccent[i]);
    }
    
    return str;
};

function sendNotif(newClass, newText) {
	if ($('#notif').width()!=0) {
		$('#notif table').fadeIn();
		$('#notif')
			.delay(500)
			.addClass('active')			
			.queue(function (next) { 
			   	$(this)
					.find('td')
					.removeClass()
					.addClass(newClass)
					.html(newText);
				next();
			});
	} else {
		$('#notif')
			.find('td')
			.removeClass()
			.addClass(newClass)
			.html(newText);
	}
	// var closeNotif = setTimeout(function(){$('#notif table').fadeOut();$('#notif').removeClass('active');console.log('titi');clearTimeout(closeNotif);},10000);
	// var clearNotif = setTimeout(function(){$('#notif td').html('');clearTimeout(clearNotif);},11000);
	
}
	

function formatPrice(price) { // format un prix (enlève les caractère non numérique après un nombre et l'écris avec deux décimales)
	price=parseFloat(price);
	
	if (isNaN(price)) {
		price=0;
	}

    return price.toFixed(2).replace(/./g, function (c, i, a) {
        return i && c !== "." && !((a.length - i) % 3) ? ' ' + c : c;
    });
}

// on va rechercher le dernier pointshop utilisé sur le poste
var id_pointshop=1;
if (localStorage.getItem('id_pointshop')!=null) { // on va récupérer le derier qu'on avait lancé sur le poste
	id_pointshop=parseInt(localStorage.getItem('id_pointshop'));
}
if (acaisse_pointshops.pointshops[id_pointshop]==undefined) { // si pouur x raison le pointshop n'existe pas, on prend le premier qu'on a sous la main
	for (var id in acaisse_pointshops.pointshops) {
		id_pointshop=id;
		break;
	}
}

var current_id_page = acaisse_pointshops.pointshops[id_pointshop].id_main_page;

ACAISSE = {
	previousPageId : [],
	id_force_group : 3, //@TODO
    mode_debogue: false,    	
    total_cost: 0.0,
    scanbarcodeMode: 'product', // or 'preOrder' or 'customer'
    current_id_page: current_id_page,
    id_pointshop: id_pointshop,
    id_service: 0, //@TODO à dynamiser lors du démarrage du service
    cart_tot: false, //real total from serveur (width reduction)
    customer: {
        id_customer: 1,
        firstname: 'Firstname',
        lastname: 'Lastname',
        year: 'D2',
    },
    cart: {
        total: 0,
        nb_article: 0,
        articles: [],
        /*
         {id_product,id_product_attribute,label,cost,quantity},
         {id_product,id_product_attribute,label,cost,quantity},
         etc..
         * */
    },
    payment_parts:[],
    ticket: {}, //va remplacer l'ancien ACaisse.cart afin de mapper extactement les objets prestshop de type ACaisseTicket
    ps_cart: null,
    ajaxCount: 0,
    ajaxQueue: {},
    productActionLocked: false, //certaines opération vont locké les actions d'ajout et de modification de quantité d'une commande.
    //paiement information
    choosenPaymentMode: false,
    allActionLocked: false,
    cartModificationLocked: false,
    lastCA : {},
    veilleAllwaysClosed: 0,
    lastCaches : {
    	'lastReloadCat': params.lastCaches.lastReloadCat,
        'lastReloadProfiles': params.lastCaches.lastReloadProfiles,
        'lastReloadPriceList': params.lastCaches.lastReloadPriceList,
        'lastReloadPointshops': params.lastCaches.lastReloadPointshops,
    }
};

var reloadCacheInterval = setInterval(
	function() {
		$.ajax({
	        data: {
	            'action': 'ReloadCaches',
	            'lastReloadCat': ACAISSE.lastCaches.lastReloadCat,
	            'lastReloadProfiles': ACAISSE.lastCaches.lastReloadProfiles,
	            'lastReloadPriceList': ACAISSE.lastCaches.lastReloadPriceList,
	            'lastReloadPointshops': ACAISSE.lastCaches.lastReloadPointshops,
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	var response = response.response;
	        	
				//là on injecte les balises script
				if (response.html.length>0) {
					console.warn('Un fichier à été rechargé');
					$('#ajaxBox').html(response.html);
					ACAISSE.lastCaches = response.lastCaches;
				}
	        } else {
	        	alert('Erreur k_00110 : impossible de rafraichir les caches');
	        }
	    }).fail(function () {
	        console.log('Erreur k00110 : Erreur k_00110 : impossible de rafraichir les caches');
	    });
	},
	parseInt(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.time_before_reload_caches)*60000 
);

function sendCustomerSearch(automatically_switch_to_first_finded_customer) {//RECHERCHE DE CLIENT LORSQU'ON SAISIS UN NOM
    var expressions = $('#customerSearchResultsFilter input').val().trim();
    
    if (expressions == '')
    {
        $('#customerSearchResults').html('').hide();
        $('#defaultCustomersProfils').show();
    }
    else
    {
        $.ajax({
            type: 'post',
            data: {
                'action': 'SearchCustomer',
                'q': expressions,
                'ajaxRequestID': ++ACAISSE.ajaxCount,
                'id_pointshop': ACAISSE.id_pointshop,
            },
            dataType: 'json',
        }).done(function (response) {
            //pendant le temps de retour de la réponse l'utilisateur a peut être revider l'expression de recherche
            if ($('#customerSearchResultsFilter input').val().trim() == '')
            {
                return sendCustomerSearch();
            }
            else
            {
                if (response.success == true)
                {
                    $('#defaultCustomersProfils').hide();

                    var html = '';
                    
                    if (response.response.customers.length == 0)
                    {
                        html = 'Aucun client correspond à votre recherche.';
                    }
                    else
                    {
                        html += '<ul>';
                        for (var cutomer in response.response.customers)
                        {
                            var customer = response.response.customers[cutomer]['customer'];
                            var groups = response.response.customers[cutomer]['group'];
                            
                            var groupClass = '';
                            for (var group in groups){
                                groupClass += 'cutomerGroup_' + groups[group] + ' ';
                            }
                            html += '<li data-id="' + customer.id + '" data-groups="'+groups.join(',')+'" data-id_group="' + customer.id_default_group + '" \
                                        class="gender-' + customer.id_gender + ' ' + groupClass +'" style="background-color:'+_getGroupColorForCustomer(customer)+'">';
                            html += '<span class="name">' + customer.firstname + ' ' + customer.lastname + '' + (customer.promotion_name ? ' <i>' + customer.promotion_name + '</i>' : '') + '<span>';
                            html += '<span class="email">' + customer.email + '</span>';
                            html += '<span class="id">' + customer.id + '</span>';
                            html += '</li>';
                        }
                        html += '</ul>';
                    }
                    $('#customerSearchResults').html(html).show();
                    
                    if (automatically_switch_to_first_finded_customer!=undefined && $('#customerSearchResults ul li').length==1) {
                    	$('#customerSearchResults ul li').click();
                    }
                }
                else
                {
                    alert('Erreur 000128: ' + response.errorMsg);
                }
            }
        }).fail(function () {
            alert('Erreur 000129 : Une erreur c\'est produite durant la recherche de client. Si cette erreur persiste veuillez contacter l\'administrateur du site.');
        });
    }
}

function _setProgressPrepare(elem, theoreticalCash) { // change la couleur du fond d'un elem en fonction d'un nombre theoreticalCash
    var progressVeryDown = 5;
    var progressDown = 3;
    if (elem.val() >= (theoreticalCash + progressVeryDown) || elem.val() <= (theoreticalCash - progressVeryDown))
        elem.css('background', '#B26F6D');
    else if (elem.val() >= (theoreticalCash + progressDown) || elem.val() <= (theoreticalCash - progressDown))
        elem.css('background', '#FFD4B2');
    else if (elem.val() == theoreticalCash)
        elem.css('background', '#89B269');
    else
        elem.css('background', '#E4FFB0');
}

var add_progress = null;

function addToCart(id_product, id_product_attribute, quantity, id_ticket_line) {  
   
  	//PRESTATILL_CAISSE.loader.start();
   
    if (add_progress) {
        add_progress.abort();
    }
   
    if( typeof(quantity) == 'undefined' || quantity == '' ) {
        quantity = 1;
    }
    
    /************ NOUVEAUTE RETOUR PRODUIT ********************/
    if(PRESTATILL_CAISSE.return.mode_return_is_active === true)
    {
    	var id_order = null;
    	PRESTATILL_CAISSE.return.openReturnPopup(id_product, id_product_attribute, quantity, id_order);	
    	//PRESTATILL_CAISSE.loader.end();
    	return;
    }
    /***********************************************************/
    
    if( typeof(id_ticket_line) == 'undefined' || id_ticket_line == '' ) {
        id_ticket_line = false;
    }
    
    if (ACAISSE.ticket.id == null) {
        alert('Impossible d\'ajouter un produit. Aucun ticket crée.');
        return;
    }
    
    //var c = acaisse_products_prices.catalog;
    var p = _loadProductFromId(id_product);
    
    if (id_product_attribute != undefined && id_product_attribute > 0) {
		//console.log('quantite',quantity);
		if (p.unity=="grammes" && p.attributes[id_product_attribute].attribute_name=='1g') {
			//quantity*=1000; // si on commente ctte ligne on a tout en gramme
		}
	}
	
	if(p.is_generic_product == true && id_ticket_line == false)
	{
		//console.log('On force la creation d\'une nouvelle ligne car produit générique');
		id_product_attribute = 0;
		id_ticket_line = -1; //on va forcer la création d'une nouvelle ligne dans le ticket
	}
    
    var a = null;
    var product_name = p.product_name;
    var attribute_name = '';
    var img = p.image;
    id_product_attribute=id_product_attribute==''?0:id_product_attribute;
    if (id_product_attribute != 0)
    {
        a = p.attributes[id_product_attribute];
        if(a != undefined)
        {
	        attribute_name = a.attribute_name;
	        img = a.image != '' ? a.image : img;
        }
    }
	
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='0') {
		img = '';
	} else if (img=='' || img==undefined) {
		img='../modules/acaisse/img/noPhoto.jpg';
	}
	
    //construction d'une icon de signalisation de l\'ajout
    $icon = $('<div id="ajax-icon-' + (++ACAISSE.ajaxCount) + '" style="background-image:url(' + img + '); height:50px;" class="addToBAsketIndicator">'+product_name+' '+(id_product_attribute != 0?attribute_name:'')+'</div>');
    $('#productList .switcher').append($icon);

    add_progress = $.ajax({
        type: 'post',
        dataType: 'json',
        data: {
            'action': 'AddProductToCart',
            'id_ticket': ACAISSE.ticket.id,
            'id_ticket_line': id_ticket_line,
            'is_generic_product': p.is_generic_product,            
            'id_product': id_product,
            'id_attribute': id_product_attribute,
            'ajaxRequestID': ACAISSE.ajaxCount,
            'listToRefresh': ACAISSE.pageProductBuffer,
            'quantity' : quantity,
			'id_customer': ACAISSE.customer.id,
			'id_group': ACAISSE.customer.id_default_group, //3
			'id_force_group': ACAISSE.id_force_group, //5
        }
    }).done(function (response) {
        if (!response.success) {
        	console.log('erreur XX'+response.request.ajaxRequestID);
            //alert('Erreur 000552'+response.errorMsg);
            $('#ajax-icon-' + response.request.ajaxRequestID).append('<i class="fa fa-exclamation-circle" style="color:red; font-size:200%;"><b>' + response.errorMsg + '</b></i>').on('click tap', function (e) {
                $(this).fadeOut("slow", function () {
                    $(this).remove();
                });
                e.preventDefault();
                e.stopPropagation();
            });
            //PRESTATILL_CAISSE.loader.end();
            return;
        }
        $('#ajax-icon-' + response.request.ajaxRequestID).append('<i class="fa fa-check" style="color:green; font-size:200%;"></i>').delay(3000).hide('slow', function () {
            $(this).remove();
        });
        
        request = response.request;
        response = response.response;
        ACAISSE.ticket = response.ticket;
        //console.log("add");
        var qu = _refreshTicket(id_product,true);
        _refreshProductPageStock(ACAISSE.pageProductBuffer);
        
    	var line = response.ticket.lines[response.ticket.lines.length-1];
    	    	
        //si c'était un produit générique on ouvre ca popup de personalisation
        if(request.is_generic_product && parseInt(request.id_ticket_line) == -1)
        {        	
        	var id_ticket_line = line.id;
        	var id_product = line.id_product;
        	var id_product_attribute = line.id_attribute;
			var cartLineInfos = ".cartLine_"+id_product+"-"+id_product_attribute+"-"+id_ticket_line;
			$(cartLineInfos+' .discountCartLine').click();
			
			//declencher le chargement du cache afin d'avoir le produit avec sa nouvelle declinaison
			_loadProductFromId(id_product,null,true);
        }
        
        $('#totalWithoutDiscount').fadeOut().html('');
        //var module_obj = {action:'typed_object', line:line,product:_loadProductFromId(line.id_product),id_attribute:null/*(a!=null?line.id_product_attribute:null)*/,qte:qu};
		
        _recalcAndRefreshTotals();
		var module_obj = {
			action:'switch_ticket',
			//product_list:product_list,
			ticket:new Ticket(ACAISSE.ticket),
			raw_ticket:ACAISSE.ticket
		};
		
		modules_action(module_obj);
		
		add_progress = null;
		//PRESTATILL_CAISSE.loader.end();
        
    }).fail(function (response) {
 
        setTimeout(function(){
        	$('.switcher .addToBAsketIndicator').fadeOut("slow", function () {
	        	$(this).remove();
	        });
		}, 3000);
        
    });

}

function _drawCartLine(ticket_line)
{	
    var imgLink;
    var p = ticket_line.p;
    var a = ticket_line.a;
    var line = ticket_line.ticket_line;
    var id_ticket_line = line.id;
    var id_product_attribute = (line.id_attribute != undefined ? line.id_attribute : 0);

    //console.log(['_drawCartLine',ticket_line, line, id_product_attribute]);
    //console.log(p);
    //console.log(a);
    
    if (a != undefined && a.image != undefined && a.image!='') {
    	imgLink = a.image;
    } else if (p.image != undefined && p.image!='') {
    	imgLink = p.image;
    } else {
    	imgLink = '../modules/acaisse/img/noPhoto.jpg';
    }
    
    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='0') {
    	imgLink = '';
    }
    
    
     var html = '<tr class="cartLine_' + p.id_product + '-' + id_product_attribute + '-' + id_ticket_line + ' line" '+
    	'data-idTicketLine="' + id_ticket_line + '"'+
        'data-id_product="' + p.id_product + '"'+
        'data-id_product_attribute="' + id_product_attribute + '"'+
        'data-idobject="' + p.id_product + '"'+
        'data-idobject2="' + id_product_attribute + '">';
        
    html += (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0'?'<td class="img"><img src="' + imgLink + '" width="50" height="50"/></td>':'');
    html += '<td class="label">' + ticket_line.label; /*' ' +(a != undefined ? a.attribute_name : '')) +*/
    html += '<br /><small>' + p.reference + '</small>';
    html += '</td>';
    
    /*
       NOUVEAU
       display_prix_final_ttc
	   display_prix_final_ht
	   display_prix_original_ttc
	   display_prix_original_ht
	   display_reduction
     */
    html += getCartLineCostLabelHTML(ticket_line);
    
    var unitId = params.units.from.indexOf(p.unity);
	if (unitId==-1) {
		//console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		unitId = params.units.from.indexOf(p.unity);
		//throw "Erreur d'unité : unité inconnue : ";
	}
    
	if (params.units.scale[unitId]==1 || (a != undefined && p.unity=="grammes" && a.attribute_name!='1g')) { // si on a pas une unité de mesure spéciale comme le 'g'
        html += '<td class="quantity"><span class="quantityMinus"><i class="fa fa-caret-left"></i></span><span class="nbr">1</span><span class="quantityPlus"><i class="fa fa-caret-right"></i></span></td>';
	} else {
    	html += '<td class="quantity"><span class="nbr">1</span></td>';
		
	}
	
	html += '<td class="totalLineCost"></td>';
    html += '<td class="discountCartLine"><i class="fa fa-pencil"></i></td>';
    //html += '<td class="infosProduct"><i class="fa fa-info-circle"></i></td>';
	html += PRESTATILL_CAISSE.executeHook('displayPrestatillTicketLineBody',{'id_ticket_line':id_ticket_line,'id_product':p.id_product,'id_product_attribute':id_product_attribute});
	html += '<td class="quantityRemove"><i class="fa fa-times-circle"></i></td>';
	
	html += '</tr>';
    $('#ticketView .lines').append(html);
    
    
    
    var $line = $('#ticketView .lines tr').last();
    //on stock l'instance TicketLine dans data de la ligne (sa servira à la popup pour avoir accès à toutes les infos de la ligne)
    $line.data('ticket_line',ticket_line);
    
    var unitPrice = parseFloat($line.find('.cost .specialPrice').html());
    var qte = parseFloat($line.find('.quantity .nbr').html());
    
    var linePrice = unitPrice*qte;
    linePrice = linePrice.toFixed(2)+'€';
    
    
	$('td.totalLineCost',$line).replaceWith(getCartTotalLineCostLabelHTML(ticket_line));

    
    _recalcAndRefreshTotals();
    
}


function getCartTotalLineCostLabelHTML(ticket_line) {
	
	var html = '';
	//console.warn(['-----------',ticket_line]);
	html += '<td class="totalLineCost '+(ticket_line.has_reduction?' has_reduction':'')+'">';
	    html += '<span class="ht display_total_line_ht">'+ticket_line.display_line_price_ht+'</span>';
    	html += '<span class="ttc display_total_line_ttc">'+ticket_line.display_line_price_ttc+'</span>';
    html += '</td>';
	
	return html;
}

function getCartLineCostLabelHTML(ticket_line) {
	var html = '';
	var price_orig = false;
	var reduction_from_price_orig = false;
	
	// @LAURENT : On force un ticket à ne pas être détaxé de base !
	ACAISSE.ticket.is_detaxed = false;
	
	if(ACAISSE.ticket.is_detaxed)
	{
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
			&& parseFloat(ticket_line.prix_final_ht).toFixed(2) < parseFloat(ticket_line.base_price_ht).toFixed(2))
		{
			//console.log('je passe');
			ticket_line.has_reduction = true;
			price_orig = true;
			reduction_from_price_orig = ticket_line.display_reduction_orig;
			reduction_from_price_orig_ht = ticket_line.display_reduction_orig_ht;
			//console.log(parseFloat(ticket_line.base_price_ttc).toFixed(2) + ' - ' + parseFloat(ticket_line.prix_final_ttc).toFixed(2) + ' - ' + ticket_line.display_reduction_orig + ' - ' + price_orig + ' - ' + reduction_from_price_orig);
		}
		//console.log(parseFloat(ticket_line.base_price_ttc).toFixed(2) + ' - ' + parseFloat(ticket_line.prix_final_ttc).toFixed(2) + ' - ' + ticket_line.display_reduction_orig + ' - ' + price_orig + ' - ' + reduction_from_price_orig);
		//console.log(ticket_line.prix_final_ttc+' - ??? '+ticket_line.base_price_ttc);
	}
	else
	{
		//console.log(ticket_line.prix_final_ttc+' - '+ticket_line.base_price_ttc);
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
			&& parseFloat(ticket_line.prix_final_ttc).toFixed(2) < parseFloat(ticket_line.base_price_ttc).toFixed(2))
		{
			ticket_line.has_reduction = true;
			price_orig = true;
			reduction_from_price_orig = ticket_line.display_reduction_orig;
			reduction_from_price_orig_ht = ticket_line.display_reduction_orig_ht;
			//console.log(parseFloat(ticket_line.base_price_ttc).toFixed(2) + ' - ' + parseFloat(ticket_line.prix_final_ttc).toFixed(2) + ' - ' + ticket_line.display_reduction_orig + ' - ' + price_orig + ' - ' + reduction_from_price_orig);
		}
	}
	
	// Rattrapage lorsqu'un prix est forcé par l'utilisateur (à rattraper en amont)
	if(ticket_line.original_price_is_forced_by_user == 1)
	{
		if(parseFloat(ticket_line.prix_final_ttc).toFixed(2) < parseFloat(ticket_line.prix_original_ttc).toFixed(2))
		{
			ticket_line.has_reduction = true;
		}
		else
		{
			ticket_line.has_reduction = false;
		}
	}
	
	//console.log(parseFloat(ticket_line.prix_final_ttc).toFixed(2) + ' - ' + parseFloat(ticket_line.prix_final_ttc).toFixed(2) + ' - ' + parseFloat(ticket_line.prix_original_ttc).toFixed(2) + ' - ' + price_orig + ' - ' + reduction_from_price_orig);
	//price_orig==true?ticket_line.display_base_price_ht:
	//console.log(ticket_line.original_price_is_forced_by_user);
	
	//console.log(ticket_line.has_reduction);
	
	html += '<td class="cost '+(ticket_line.has_reduction?' has_reduction':'')+'">';
	
	    if(ticket_line.has_reduction)
	    {
	    	if(price_orig == true)
	    	{
	    		html += '<span class="ht original_prix display_prix_original_ht">'+ticket_line.display_base_price_ht+'</span>';
	    		html += '<span class="ttc original_prix display_prix_original_ttc">'+ticket_line.display_base_price_ttc+'</span>';
	    		//console.log(reduction_from_price_orig_ht);
	    		if(reduction_from_price_orig_ht !== undefined)
	    		{
	    			if(ACAISSE.price_display_method == 1) //HT
					{	
		    			html += '<br /><span class="reduction">'+reduction_from_price_orig_ht+'</span><br />';
		    		}
		    		else
		    		{
		    			html += '<br /><span class="reduction">'+reduction_from_price_orig+'</span><br />';
		    		}	
		    		
	    		}
	    		
	    	}
	    	else
	    	{
	    		html += '<span class="ht original_prix display_prix_original_ht">'+ticket_line.display_prix_original_ht+'</span>';
	    		html += '<span class="ttc original_prix display_prix_original_ttc">'+ticket_line.display_prix_original_ttc+'</span>';
	    		//console.log(ticket_line.reduction_ht);
	    		if(ticket_line.display_reduction !== undefined && ticket_line.reduction_ht !== undefined && ticket_line.reduction_ht > 0)
	    		{
	    			if(ACAISSE.price_display_method == 1) //HT
					{	
		    			html += '<br /><span class="reduction">'+ticket_line.reduction_ht+'</span><br />';
		    		}
		    		else
		    		{
		    			html += '<br /><span class="reduction">'+ticket_line.display_reduction+'</span><br />';
		    		}	
	    		}
	    		
	    	}
    	}
    	html += '<span class="ht final_prix display_prix_final_ht">'+ticket_line.display_prix_final_ht+'</span>';
    	html += '<span class="ttc final_prix display_prix_final_ttc">'+ticket_line.display_prix_final_ttc+'</span>';
    html += '</td>';
	
	return html;
}

function _cartUpdateLineQuantity(id_product, id_product_attribute, newQuantity, id_ticket_line, ticket_line, auto_totals_refresh) {
	
	if(typeof id_ticket_line == 'undefined' || id_ticket_line == '')
	{
		id_ticket_line = false;
	}
	
	var p = _loadProductFromId(id_product);
	if (id_product_attribute && p.has_attributes) {
		var a = p.attributes[id_product_attribute];
	}
	
	var unitId = params.units.from.indexOf(_loadProductFromId(id_product).unity);
	
	if (a!=null && p.unity=="grammes" && a.attribute_name!='1g') {
		unitId = params.units.from.indexOf('pièce');
	}
	
	if (unitId==-1) {
		console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		unitId = params.units.from.indexOf(_loadProductFromId(id_product).unity);
		//throw "Erreur d'unité : unité inconnue : ";
	}
	
	newQuantity /= params.units.scale[unitId];
	newQuantity += (' '+params.units.to[unitId].substring(1));
	
	var html='';
	
	if (params.units.scale[unitId]==1) { // si on a une unité de mesure spéciale comme le 'g'
		html += '<span class="quantityMinus"><i class="fa fa-caret-left"></i></span><span class="nbr">'+newQuantity+'</span><span class="quantityPlus"><i class="fa fa-caret-right"></i></span>';
	} else {
		html += '<span class="nbr">'+newQuantity+'</span>';
	}
	
	$('.cartLine_' + id_product + '-' + id_product_attribute + '-' + id_ticket_line + ' .quantity').html(html);
	
	$('.cartLine_' + id_product + '-' + id_product_attribute + '-' + id_ticket_line + ' .totalLineCost').html(
		formatPrice(
			parseFloat(
				$('.cartLine_' + id_product + '-' + id_product_attribute + ' .specialPrice').html()
			)*newQuantity
		)+'€'
	);
	
	var line_selector = '.mode-buy .cartLine_' + id_product + '-' + id_product_attribute + '-' + id_ticket_line;
	var $line = $(line_selector);
    
    var unitPrice = parseFloat($line.find('.cost .specialPrice').html());
    var qte = parseFloat($line.find('.quantity .nbr').html());
    
    var linePrice = unitPrice*qte;
    linePrice = linePrice.toFixed(2)+'€';
    
    // $line.find('.totalLineCost').html(linePrice);
    //var ticket_line = $line.data('ticket_line');
    // TEST LAURENT 
    /*
    if(auto_totals_refresh != undefined && auto_totals_refresh !== false)
    {
    	_validateCurrentTicket();
		ACAISSE.cartModificationLocked = false;
    }
	*/
	
        
	$('td.totalLineCost',$line).replaceWith(getCartTotalLineCostLabelHTML(ticket_line));    
}

function _reloadPage() {
	loadPage(ACAISSE.current_id_page);
}

var timeoutLoadPage;
var timeoutPing;

function loadPage(id_page, id_pointshop) {
	clearTimeout(timeoutLoadPage);
	//console.log("j suis la");
	if(ACAISSE.mode_debogue === true)
	{
		console.log('loadPage(id_page='+id_page+', id_pointshop='+id_pointshop+')');
	}
	
	var productBuffer = []; //liste des produits sur la page, nécessitant la maj des stocks


	if (id_pointshop == null || typeof id_pointshop == 'undefined')
	{
		id_pointshop = ACAISSE['id_pointshop'];
	}
	var ps = acaisse_pointshops.pointshops[id_pointshop];
	
	params.print.nbCopie=parseInt(ps.datas.ticket_nbr_copies);
	params.print.nbJourSauvegardeLocal=parseInt(ps.datas.ticket_nbr_jour_sauvegarde_local);
	params.customerScreen.lcd_ip=ps.datas.screen_lcd_ip;
	params.customerScreen.mode=ps.datas.screen_mode;
	params.declinaisonPopup.closeAfterSelect=ps.datas.declinaison_close_popup_after_select=='1';
	params.products.STOCK_WARNING_LIMIT=parseInt(ps.datas.product_STOCK_WARNING_LIMIT);
	params.products.use_image=ps.datas.use_image=='1';
	
	//DEPRECATE open_module(); // ouvre tout les modules complementaire (afficher client, ...)
	
	if (ps.datas.navigation_by_category=='0') {
		$('#openRootCategorie').css('display','none');
	} else {
		$('#openRootCategorie').css('display','block');
	}
	
	if (typeof ps.pages[id_page] == 'undefined') { // on a pas de page de caisse
		
		if (ps.datas.navigation_by_category=='0' || acaisse_categories.categories[acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_root_category]==undefined) { // on a pas non plus de categorie
			
			$('#productList .content').html('');
			
			sendNotif('redFont','<i class="fa fa-exclamation-circle"></i> Caisse hors service.');
			
			timeoutLoadPage = setTimeout(function() {
				alert('Veuillez configurer la caisse en lui ajoutant soit des pages de caisse, soit en activant la navigation par categorie (contactez un responsable).');
			},1000);
			
		} else {
			generateCategoriePageHTML(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_root_category);
	  
			// On récupère les nbRow et nbCol dans les params
			ACAISSE.nbRow = ps.datas.max_row;
			ACAISSE.nbCol = ps.datas.max_col;
	
			// On fait la transition vers les pages par catgegories
			$('#footer .navActive').removeClass('navActive');
	        $('#openRootCategorie').addClass('navActive');
			
			// On appel une fonction toutes les X minutes pour ne pas être déconnecté de Prestashop
			if(timeoutPing != null) { clearInterval(timeoutPing); }
			timeoutPing = setInterval(function(){ _refreshProductPageStock(productBuffer);},ping_interval);		
		}
		
		return false;
	}
	var cells = ps.pages[id_page].cells;
	var html = '<table id="acaissepagecms">';
	
	ACAISSE.nbRow = cells.length;
	ACAISSE.nbCol = cells[0].length;
	
	for (var row = 0; row < cells.length; row++)
	{
		html += '<tr id="row_' + row + '">';
		var r = cells[row];
		for (var col = 0; col < r.length; col++)
		{
			var cell = r[col]; // cell = {id_object:5,id_object2:6}
			
			/*if (_loadProductFromId(cell.id_object)==undefined) {
				cell.type = 101;
			}*/			
			
			switch (parseInt(cell.type))
			{
				case 101:
					html += '<td class="cellType0 cellStyle1 rowspan1 colspan1" data-idobject="" data-idobject2=""></td>';
					break;
				case 100:
					html += '<!--spanned td-->';
					break;
				case 2:
				case 3:
					html += '<td id="cell_' + cell.row + '_' + cell.col + '"' + _injectCellAttributes(cell) + '>' + _injectCellProductContent(cell, ps) + '</td>';
					productBuffer.push({
						id_product: cell.id_object,
						id_product_attribute: cell.id_object2,
						row: cell.row,
						col: cell.col
					});
					break;
				case 4:
				  html += '<td' + _injectCellAttributes(cell) + '>' + _injectCellCategoryContent(cell, ps) + '</td>';
				  break;
				case 1:
					html += '<td' + _injectCellAttributes(cell) + '>' + _injectCellPageContent(cell, ps) + '</td>';
					break;
				case 0:
				default:
					html += '<td' + _injectCellAttributes(cell) + '></td>';
					break;
			}
		}
		html += '</tr>';

	}

	html += '</table>';

	$('#productList .content').html(html);

	_refreshProductPageStock(productBuffer);
	ACAISSE.pageProductBuffer = productBuffer; //on le stocks pour pouvoir redemander la
	//ACAISSE.pageProductBuffer.push(3);
	// mise à jour des stocks de la page sur
	// certaine action (ajout de produit, etc...)
	ACAISSE.current_id_page = id_page;
	resetTileHeight();
	
	// On appel une fonction toutes les 4 minutes pour ne pas être déconnecté de Prestashop
  if(timeoutPing != null) { clearInterval(timeoutPing); }
  timeoutPing = setInterval(function(){ _refreshProductPageStock(productBuffer);},ping_interval);
	
	return true;
}

function generateCategoriePageHTML(id_object) {
  
	if (acaisse_categories.categories[id_object]==undefined) {
		$('#openRootPageCaisse').click();
		//sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Veuillez configurer la categorie par défaut pour utiliser cette navigation.');
		console.log('<i class="fa fa-exclamation-circle"></i>Veuillez configurer la categorie par défaut pour utiliser cette navigation.');
		return;
	} else if (acaisse_categories.categories[id_object].active==false) {
		sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Cette catégorie a été désactivé.');
		return;
	} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.navigation_by_category=='0') {
		
		//on peut bloquer la navigation par categorie quoi qu'il arrive (si on a des lien dans les pages de caisse) en fonction du paramettre du pointshop en décommentant les ligne suivante
		
		/*$('#openRootPageCaisse').click();
		sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Veuillez configurer la poissibilité d\'utiliser de cette navigation.');
		return;*/
	}
  
	$('#footer .button').removeClass('navActive');
	$('#openRootCategorie').addClass('navActive');
  
	ACAISSE.pageProductBuffer = [];
	if(ACAISSE.mode_debogue === true)
		console.log('generateCategoriePageHTML('+id_object+')');
		
	var html = '<table id="acaissepagecms">';
	var categories = acaisse_categories.categories[id_object];

	var nbrLines = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.max_row;
	var nbrColumns = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.max_col;
	
	var line = 0;
	var column = 0;
	if(ACAISSE.mode_debogue === true)
		console.log(['merge categorie pageCaisse',ACAISSE.previousPageId]);
		
	if (ACAISSE.previousPageId.length>1) {
		column = 1;
		html += '<tr id="row_'+line+'">';
		html += '<td class="cellTypeReturn cellStyle9 rowspan1 colspan1" data-idobject="8" data-idobject2="" id="cell_0_0">Retour</td>';
	}
	for (var i=0; i<categories.sub_categories.length; i++) {
		if (acaisse_categories.categories[categories.sub_categories[i]].active==false) {
			continue;
		}
		
		if (column==0) {
			html+='<tr id="row_'+line+'">';
		}
		
		var categorie = acaisse_categories.categories[categories.sub_categories[i]];
		html += '<td id="cell_'+line+'_'+column+'" class="cellStyleDefault cellType4 cellStyle2 rowspan1 colspan1" data-idobject="'+categories.sub_categories[i]+'">';
			html += '<div class="cover with_attributes '+(acaisse_categories.categories[categories.sub_categories[i]]?'isCat':'')+' '+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2'?'fullImg':'')+''+((categorie.thumb!==false && acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0')?' withCategorieImage':'')+'">';
				html += '<div class="acaisse_productcell_content" style="';
				if (categorie.thumb!==false) {
					html += 'background-image: url('+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0'?categorie.thumb:'')+');'; // .thumb renvoie vers le lien d'une image de la catégorie
				}
				html += 'background-position:0px 0px;">'
				
					html += '<div class="product_stock"-></div>';
					html += '<div class="product_name_container">';
						html += '<div class="product_name_container_box">';
							html += '<div class="product_name"><i class="fa fa-folder-open-o" unselectable="on" style="user-select: none;"></i><span>';
								html += acaisse_categories.categories[categories.sub_categories[i]].name;
							html += '</span></div>';
						html += '</div>';
					html += '</div>';
				
				html += '</div>';
			html += '</div>';
		html += '</td>';
		
		column++;
		
		if (column==nbrColumns) {
			html+='</tr>';
			line++;
			column=0;
		}
	}
	
	// on doit maintenant parcourir les produits qu'il reste dans la categorie pour les afficher
	for (var i=0; i<categories.products.length; i++) {
	  
    
    var p = _loadProductFromId(categories.products[i]);
	  
		if (column==0) {
			html+='<tr id="row_'+line+'">';
		}
		/*
		if (_loadProductFromId(categories.products[i])==undefined || !_loadProductFromId(categories.products[i]).disponibility_by_shop[ACAISSE.id_pointshop]) {
			p=undefinedValue;
		}
		*/
		
		//console.log([typeof(p),p]);
		
	    if(typeof(p) != 'undefined' && p != null) {
	      
        if (p.active==false) {
          continue;
        }
	      
	  		var img;
	  		
	  		if (p.has_attributes && p.attributes[p.id_default_attribute]) {
	  			img = p.attributes[p.id_default_attribute].image;
	  		} else {
	  			img = p.image;
	  		}
	  		
	  		if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='0') {
	  			img='';
	  		}
	  
	  		html += '<td id="cell_'+line+'_'+column+'" class="cellStyleDefault cellType2 cellStyle'+(p.has_attributes?'':'1')+'3 rowspan1 colspan1" data-idobject="'+categories.products[i]+'" data-idobject2>';
	  			html += _injectCellProductContent({id_object:categories.products[i],id_object2:0});
	  		html += '</td>';
	  		
	  		ACAISSE.pageProductBuffer.push({
	  			id_product: categories.products[i],
	  			id_product_attribute: '',
	  			row: line,
	  			col: column
	  		});
	  		
	  		column++;
		}
		if (column==nbrColumns) {
			html+='</tr>';
			line++;
			column=0;
		}
	}
	
	var enter = false;
	while (column!=nbrColumns && column!=0) {
		html += '<td class="cellType0 cellStyle1 rowspan1 colspan1" data-idobject="" data-idobject2="" id="cell_'+line+'_'+column+'"></td>';
		column++;
		enter=true;
	}
	
	if (enter) {line++;}
	
	while (line <= nbrLines-1) {
		html += '<tr id="row_'+line+'">';
		for (var column=0; column<nbrColumns; column++) {
			html += '<td class="cellType0 cellStyle1 rowspan1 colspan1" data-idobject="" data-idobject2="" id="cell_'+line+'_'+column+'"></td>';
		}
		html += '</tr>';
		line++;
	}
	
	html += '</table>';
	
	
	
  $('#productList .content').html(html);
	//$('#acaissepagecms tbody').html(html);
	
	resetTileHeight();
	
	if (ACAISSE.pageProductBuffer.length>0) {
		_refreshProductPageStock(ACAISSE.pageProductBuffer);
	}
	
}

/**
 * recharge les stocks d'une liste de produit
 * @params object listToRefresh sous la forme : listToRefresh.push({
 *        id_product: cell.id_object,
 *        id_product_attribute: cell.id_object2,
 *        row: cell.row,
 *        col: cell.col
 *  });
 */
function _refreshProductPageStock(listToRefresh) { // 
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'RefreshProductStockFromPage',
			'listToRefresh': listToRefresh,
			'id_pointshop' : ACAISSE.id_pointshop,
		},
	}).done(function (response) {
		if (response.success)
		{
			if(ACAISSE.mode_debogue === true)
			{
				console.log(['_refreshProductPageStock() ajax response',response]);
			}
			_doStockLevelUpdateOnPage(response.response.stocks);
		}
		else
		{
			alert('Erreur 000947 : ' + response.errorMsg);
		}
	}).fail(function () {
		console.log('Erreur 000948c');
	});
}

function _doStockLevelUpdateOnPage(stocks) { // change le html de tout les stocks pour l'affichage
	if(ACAISSE.mode_debogue === true)
		{console.log(['stock level update',stocks]);}
	if(stocks)
	{
		for(var i=0 ; i<stocks.length ; i++)
		{			
			if(ACAISSE.mode_debogue === true)
			{console.log(['stock level update',stocks[i]]);}
			var cell=stocks[i][0];
			var real_stock = real_stock_aff = stocks[i]['physical_qu'];
			var preorder_stock = preorder_stock_aff = stocks[i][2];
			var estimateStock = estimateStock_aff = parseInt(real_stock)-parseInt(preorder_stock);
			
			try {
				var unitId = params.units.from.indexOf(_loadProductFromId(cell.id_product).unity);
				
				if (unitId==-1) {
					console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
					params.units.from.push(p.unity);
					params.units.to.push('');
					params.units.scale.push(1);
					unitId = params.units.from.indexOf(_loadProductFromId(cell.id_product).unity);
					//throw "Erreur d'unité : unité inconnue : ";
				}
				
				real_stock_aff = (real_stock/params.units.scale[unitId])+' '+params.units.to[unitId].substring(1);
				preorder_stock_aff = (preorder_stock/params.units.scale[unitId])+' '+params.units.to[unitId].substring(1);
				estimateStock_aff = (estimateStock_aff/params.units.scale[unitId])+' '+params.units.to[unitId].substring(1);

				var p = _loadProductFromId(cell.id_product);
				if (cell.id_product_attribute != 0) {
					p = p.attributes[cell.id_product_attribute];
				}
				
				if  (products_stock_cache[cell.id_product] == undefined) { // si on a jamais eu ce produit a ranger dans le cache
					products_stock_cache[cell.id_product] = {};
				}
				
				if (cell.id_product_attribute != "" && cell.id_product_attribute != 0) { // si en + il a un attribut
					products_stock_cache[cell.id_product][cell.id_product_attribute] = {};
					products_stock_cache[cell.id_product][cell.id_product_attribute].real_stock_aff = real_stock_aff;
					products_stock_cache[cell.id_product][cell.id_product_attribute].preorder_stock_aff = preorder_stock_aff;
					products_stock_cache[cell.id_product][cell.id_product_attribute].estimateStock_aff = estimateStock_aff;
				} else {
					products_stock_cache[cell.id_product].real_stock_aff = real_stock_aff;
					products_stock_cache[cell.id_product].preorder_stock_aff = preorder_stock_aff;
					products_stock_cache[cell.id_product].estimateStock_aff = estimateStock_aff;
				}
			} catch(e) {
				// @TODO : le dernier items de stocks est un élément bizzard... à creuser
			}

			//console.log('-----');
			//console.log('Tentative de mise à jour de '+cell.col+';'+cell.row);
			//console.log('Devrait contenir le produit '+cell.id_product+'/'+cell.id_product_attribute);
			var $cell=$('#cell_'+cell.row+'_'+cell.col);
			//console.log('Contient actuellement le produit '+$cell.data('idobject')+'/'+$cell.data('idobject2'));
			
			if(ACAISSE.mode_debogue === true)
			{
				console.log(cell.id_product+' == '+$cell.data('idobject')+' && '+cell.id_product_attribute+' == '+$cell.data('idobject2'));
				console.log($cell);
				console.log(cell.id_product == $cell.data('idobject'));
				console.log(cell.id_product_attribute == ($cell.data('idobject2')==''?0:$cell.data('idobject2')));
			}
			
			if(cell.id_product == $cell.data('idobject') && cell.id_product_attribute == ($cell.data('idobject2')==''?0:$cell.data('idobject2')))
			{
			  //console.log('maj stock OK');
			  $('.stock_real',$cell)
				  .html(real_stock=='0'?'rupture':real_stock_aff)
				  .removeClass('error')
				  .addClass(real_stock=='0'?'error':'');
			  $('.stock_inqueue',$cell).html('-'+preorder_stock_aff);
			  
			  $('.stock_available',$cell).html(estimateStock_aff)
				  .removeClass('error')
				  .removeClass('warning')
				  .removeClass('good')
				  .addClass(estimateStock>params.products.STOCK_WARNING_LIMIT?'good':estimateStock<=0?'error':'warning');
			}
			else
			{
			  //console.log('maj stock NOT OK');
	  			
	  		  // si on entre dans le else c'est qu'on a du avoir des envoie de demandes de stock qui se sont croisées dues à l'asynchronie d'AJAX, on a donc recu les stock d'un produit qui n'est plus affiché
	  		  // on ne doit donc par conséquent pas virer le text de stock du nouveau produit... 
			  
			  //@TODO supprimer le else si la mise en commentaire des lignes suivantes ne pose pas de problème
			  
			  /*$('.stock_real',$cell).html('error')
				  .addClass('error')
				  .removeClass('warning')
				  .removeClass('good');
			  $('.stock_inqueue',$cell).html('');
			  $('.stock_available',$cell).html('');*/
			}
		}
	}
}



function _injectCellCategoryContent(cell, ps) { // contenu du td des categories (system des dossiers)
  if(ACAISSE.mode_debogue === true)
  {
	console.log(['[***] _injectCellCategoryContent(cell,ps)',cell,ps]);
  }
	var html = '';

	var c = acaisse_categories.categories;
	var cat = c[cell.id_object];
	if(ACAISSE.mode_debogue === true) {console.log(['cat',cat]);}
	if(typeof(cat) != 'undefined')
	{       
	  	html += '<div class="cover'+((cat.thumb != false && acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0')?' withCategorieImage':'')+'"><div class="acaisse_categorycell_content"';
	  
	  	if (cat.thumb != false && acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0')
	  	{
	  		html += ' style="background-image:url(' + cat.thumb + ')"';
	  	}
	  	html += '>';
	  	html += '<i class="fa fa-folder-open-o" unselectable="on" style="user-select: none;"></i><span>' + cat.name + '</span>'
	  	html += '</div></div>';
	}
	
	return html;
}

function _injectCellProductContent(cell, ps) { // cell = {id_object:5,id_object2:6}
  /*
	if(ACAISSE.mode_debogue === true)
	{
		console.log(['_injectCellProductContent(cell,ps)',cell,ps]);
	}
	*/
	
	var html = '';

	//var c = acaisse_products_prices.catalog;
	var p = _loadProductFromId(cell.id_object);
	var name, price, img, reference, best_attribute_price = null, same_price_for_all_attribute = true;
	var pa = null;
	if (p)
	{
		name = p.product_name;
		price = getProductPrice(cell.id_object, cell.id_object2);
		base_price = getProductBasePrice(cell.id_object, cell.id_object2);
		img = p.image;
		reference = p.reference;

		//si pas un produit générique
		if(p.is_generic_product == 1)
		{
			//c'est un produit géérique...
			//prévoir ici un autre design et ne pas le traiter comme si le produit
			//avait des déclinaisons réelles
		}
		else
		{
			if(p.has_attributes === true)
			{				
				best_attribute_price = null;
				same_price_for_all_attribute = true;
				
				//on boucle sur les déclinaisons
				for(id_attribute in p.attributes)
				{
					pa = p.attributes[id_attribute];
					
						//console.log(getProductPrice(p.id_product,id_attribute));
					if(best_attribute_price == null)
					{
						best_attribute_price = getProductPrice(p.id_product,id_attribute);
					}
					else
					{
						var pa_price = getProductPrice(p.id_product,id_attribute);
						if(pa_price != best_attribute_price)
						{
							same_price_for_all_attribute = false;
						}
						if(parseFloat(pa_price) < parseFloat(best_attribute_price))
						{
							best_attribute_price = pa_price;
						}
					}
				}
				
								
				if (cell.id_object2 != 0) //c'est une declinaison
				{
					pa = p.attributes[cell.id_object2];
					if (pa)
					{
						name += ' ' + pa.attribute_name;
						img = pa.image != '' ? pa.image : img;
						//reference += '' + pa.reference;
					}
					else
					{
						name = '<strong class="error404">Déclinaison inexistante</strong>';
						img = '';
						price = '';//0+'€';
					}
				}
				else //ce n'est pas ne case de décliaison MAIS le produit en possède (cf popup Kevin)
				{
					//que l'on va chercher dans la liste attributes
					price = false;
					if(p.attributes[p.id_default_attribute])
					{
						img = p.attributes[p.id_default_attribute].image;
					}					
				}
			}
		}
	}
	else
	{
		name = '<strong class="error404">Produit inexistant</strong>';
		img = '';
		price = 0+'€';
	}
	html += '<div class="cover '+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2'?'fullImg':'')+' '+(price===false?' with_attributes':'')+'"><div class="acaisse_productcell_content"';
	
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0') {
		if (img == '' || img == undefined) {
			img = '../modules/acaisse/img/noPhoto.jpg';
		}
		if(price === false) //produit avec declinaisons
		{
			html += ' style="background-image:url(' + img + '); background-position:center center;"'; 
		}
		else //produit simple ou declinaison simple
		{
			html += ' style="background-image:url(' + img + ')"';
		}
	}
	
	html += '>';
	html += '<div class="product_stock">';
	
	if(ACAISSE.mode_debogue === true)
	{
		console.log({'Cell data:':{
			'price':price,
			'img':img,
			'p':p,
			
		}});
	}
	
	if(price === false) //c'est une case d'un produit mais qui a des declinaisons
	{
		html += '</div>';
		
		//console.log(['best_attribute_price',best_attribute_price,same_price_for_all_attribute]);
		
		html += '<div>';
		if(same_price_for_all_attribute == true)
		{			
			html += '<div class="product_price">' + best_attribute_price + '</div>'; 
		}
		else
		{
			html += '<div class="product_price"><small>A partir de </small>' + best_attribute_price + '</div>'; 			
		}
		
		
		html += '<div class="product_name_container"><div class="product_name_container_box"><div class="product_name"><i class="fa fa-list-ol"></i><span>' + name + '</span></div></div></div>';
		html += '</div></div>';
	}
	else
	{
		var real_stock_aff, preorder_stock_aff, estimateStock_aff;
		var cache_stock = products_stock_cache[cell.id_object];
		if (cache_stock == undefined) {
			real_stock_aff = 'XX';
			preorder_stock_aff = 'XX';
			estimateStock_aff = 'XX';
		} else if (cell.id_object2 != 0) {
			cache_stock = cache_stock[cell.id_object2];
			if (cache_stock == undefined) {
				real_stock_aff = 'XX';
				preorder_stock_aff = 'XX';
				estimateStock_aff = 'XX';
			} else {
				real_stock_aff = cache_stock.real_stock_aff;
				preorder_stock_aff = cache_stock.preorder_stock_aff;
				estimateStock_aff = '<span class="no_internet">'+cache_stock.estimateStock_aff+'</span>';
			}
		} else {
			real_stock_aff = cache_stock.real_stock_aff;
			preorder_stock_aff = cache_stock.preorder_stock_aff;
			estimateStock_aff = cache_stock.estimateStock_aff;
		}
		
		//console.log([p,(p.base_price*(1+(p.tax_rate/100))).toFixed(2) + ' <> ' + (p.prices[ACAISSE.id_force_group]).toFixed(2)]);
		
		
		var display_base_price = 'none';
		
		if(p.is_generic_product == false && acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
		&& (p.base_price*(1+(p.tax_rate/100))).toFixed(2) > (p.prices[ACAISSE.id_force_group]).toFixed(2))
		{
			//$('#cell_'+cell.row+'_'+cell.col+' .product_base_price').show();
			display_base_price = 'block';
		}
		
		html += '<div class="product_base_price" style="display:'+display_base_price+'">' + base_price + '</div>';
		
		html += '<div class="product_price">' + price + '</div>';
		
		html += '<div class="availability">';
			html += '<span class="stock_available">'+estimateStock_aff+'</span>';
			html += '<span class="stock_real">'+real_stock_aff+'</span>';
			html += '<span class="stock_inqueue">-'+preorder_stock_aff+'</span>';
		html += '</div>';
		
		
		html += '</div>';
		html += '<div class="product_name_container"><div class="product_name_container_box"><div class="product_name"><span>' + name + (reference == ''?'':'<br /><em>'+reference+'</em>')+'</span></div></div></div>';
		html += '</div></div>';
	}
	return html;
}

function _injectCellProductContentInSearchResults(cell, ps) { // cell = {id_object:5,id_object2:6}
	
	var html = '<table><tr>';

	//var c = acaisse_products_prices.catalog;
	var p = _loadProductFromId(cell.id_object);
	var name, price, img, reference;
	var pa = null;
	if (p)
	{
		name = p.product_name;
		price = getProductPrice(cell.id_object, cell.id_object2);
		base_price = getProductBasePrice(cell.id_object, cell.id_object2);
		img = p.image;
		reference = p.reference;

		//si pas un produit générique
		if(p.is_generic_product == 1)
		{
			//c'est un produit géérique...
			//prévoir ici un autre design et ne pas le traiter comme si le produit
			//avait des déclinaisons réelles
		}
		else
		{
			if(p.has_attributes === true)
			{
				if (cell.id_object2 != 0) //c'est une declinaison
				{
					pa = p.attributes[cell.id_object2];
					if (pa)
					{
						name += ' ' + pa.attribute_name;
						img = pa.image != '' ? pa.image : img;
						//reference += '' + pa.reference;
					}
					else
					{
						name = '<strong class="error404">Déclinaison inexistante</strong>';
						img = '';
						price = 0+'€';
					}
				}
				else //ce n'est pas ne case de décliaison MAIS le produit en possède (cf popup Kevin)
				{
					//que l'on va chercher dans la liste attributes
					price = false;
					if(p.attributes[p.id_default_attribute])
					{
						img = p.attributes[p.id_default_attribute].image;
					}					
				}
			}
		}
	}
	else
	{
		name = '<strong class="error404">Produit inexistant</strong>';
		img = '';
		price = 0+'€';
	}
	html += '<td class="cover '+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2'?'fullImg':'')+' '+(price===false?' with_attributes':'')+'"><div class="acaisse_productcell_content"';
	
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0') {
		if (img == '' || img == undefined) {
			img = '../modules/acaisse/img/noPhoto.jpg';
		}
		if(price === false) //produit avec declinaisons
		{
			html += ' style="background-image:url(' + img + ');"'; 
		}
		else //produit simple ou declinaison simple
		{
			html += ' style="background-image:url(' + img + ')"';
		}
	}
	
	html += '></div></td>';
	html += '<td class="reference">'+ reference +'</td>';

	
	if(price === false) //c'est une case d'un produit mais qui a des declinaisons
	{
		html += '<td class="product_name_container"><div class="product_name_container_box"><div class="product_name"><span>' + name + '</span></div></div></td>';
		html += '<td class="availability with_attr" colspan="2"><div class="btn"><i class="fa fa-list-ol"></i> voir les détails</div>';
		//html += '<table><tr>';
			// html += '<td colspan="3"><div class="btn"><i class="fa fa-list-ol"></i> voir les détails</div></td>';
		//html += '</tr></table>';
		html += '</td>';
		//html += '</td>';
	}
	else
	{
		var real_stock_aff, preorder_stock_aff, estimateStock_aff;
		var cache_stock = products_stock_cache[cell.id_object];
		if (cache_stock == undefined) {
			real_stock_aff = 'XX';
			preorder_stock_aff = 'XX';
			estimateStock_aff = 'XX';
		} else if (cell.id_object2 != 0) {
			cache_stock = cache_stock[cell.id_object2];
			if (cache_stock == undefined) {
				real_stock_aff = 'XX';
				preorder_stock_aff = 'XX';
				estimateStock_aff = 'XX';
			} else {
				real_stock_aff = cache_stock.real_stock_aff;
				preorder_stock_aff = cache_stock.preorder_stock_aff;
				estimateStock_aff = '<span class="no_internet">'+cache_stock.estimateStock_aff+'</span>';
			}
		} else {
			real_stock_aff = cache_stock.real_stock_aff;
			preorder_stock_aff = cache_stock.preorder_stock_aff;
			estimateStock_aff = cache_stock.estimateStock_aff;
		}
		//console.log(p.base_price*(1+(p.tax_rate/100)) + ' <> ' + p.prices[ACAISSE.id_force_group]);
		html += '<td class="product_name_container"><div class="product_name_container_box"><div class="product_name"><span>' + name +'</span></div></div></td>';
	
		
		var display_base_price = 'none';
		
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
		&& p.base_price*(1+(p.tax_rate/100)) > p.prices[ACAISSE.id_force_group])
		{
			//$('#cell_'+cell.row+'_'+cell.col+' .product_base_price').show();
			display_base_price = 'block';
		}
		
		html += '<td class="availability">';
		
			html += '<table><tr>';
				html += '<td class="stock_available">'+estimateStock_aff+'</td>';
				html += '<td class="stock_real">'+real_stock_aff+'</td>';
				html += '<td class="stock_inqueue">-'+preorder_stock_aff+'</td>';
			html += '</tr></table>';
			
		html += '</td>';
		
		html += '<td class="p_price">';
			html += '<div class="product_base_price" style="display:'+display_base_price+'">' + base_price + '</div>';
			
			html += '<div class="product_price">' + price + '</div>';
		html += '</td>';
		
		//html += '</div>';
		//html += '</div></div>';
	}
	
	html +='</tr></table>';

	return html;
}

function _injectCellPageContent(cell, ps) {
	if (typeof (ps.pages[cell.id_object]) == 'undefined')
	{
		return 'Error 404';
	}
	else
	{
		var p = ps.pages[cell.id_object];
		var html = '<div class="name">' + p.name + '</div>';
		if (p.description)
		{
			html += '<div class="description">' + p.description + '</div>';
		}
		return html;
	}
}

function _injectCellAttributes(cell) {
	var a = ' class="cellType' + cell.type + ' cellStyle' + cell.style + ' rowspan' + cell.rowspan + ' colspan' + cell.colspan + '"';
	if (cell.colspan > 1)
	{
		a += ' colspan="' + cell.colspan + '"';
	}
	if (cell.rowspan > 1)
	{
		a += ' rowspan="' + cell.rowspan + '"';
	}
	a += ' data-idobject="' + cell.id_object + '" data-idobject2="' + cell.id_object2 + '"';
	a += ' id="cell_' + cell.row + '_' + cell.col + '"';

	return a;
}

function _refreshTicket(export_product_id_quantity, auto_totals_refresh) {
	if(ACAISSE.mode_debogue === true) {console.log(["export research : ",export_product_id_quantity]);}
	var total_quantity = 0;
	var total_cost = 0;
	var result = false;
	ACAISSE.cart.articles = [];
	
	//on vide la liste des produits
	$('#ticketView .lines tr').not('.header').remove();
	//Puis on recrer les lignes de produits si nécessaire  	
	if(ACAISSE.ticket.lines)
	{
		for (var i = 0; i < ACAISSE.ticket.lines.length; i++)
		{
			var line = ACAISSE.ticket.lines[i];
			var p = _loadProductFromId(line.id_product);
			if(p !== null)
			{
				var a = p.has_attributes === true ? _loadProductFromId(line.id_product).attributes[line.id_attribute] : null;
						
				var quantity = parseFloat(line.quantity);
				var cost = getProductPrice(p.id_product, a!=null?a.id_product_attribute:0); //TODO : ERROR de chercher dans le cache, faut se baser sur la ligne
				
				var unitId = params.units.from.indexOf(p.unity);
			
				if (a!=null && p.unity=="grammes" && a.attribute_name!='1g') { //@TODO unity other like ml, mm, ...
					unitId = params.units.from.indexOf('pièce');
				}
				
				if (unitId==-1) {
					params.units.from.push(p.unity);
					params.units.to.push('');
					params.units.scale.push(1);
					unitId = params.units.from.indexOf(p.unity);
				}
				
				quantity/=params.units.scale[unitId];
				
				total_quantity += (quantity!=line.quantity ? 1 : quantity);
				total_cost += quantity * parseFloat(cost);
		
				var ticket_line = new TicketLine(line);
			
				_drawCartLine(ticket_line);

				_cartUpdateLineQuantity(line.id_product, line.id_attribute, line.quantity, line.id, ticket_line, auto_totals_refresh);
				
				if(line.id_product == export_product_id_quantity)
				{
					result = line.quantity;
				}
				ACAISSE.cart.articles.push([line.id_product, line.id_attribute, line.quantity, cost]);
				
				_validateCurrentTicket();
				ACAISSE.cartModificationLocked = false;
		   }
		}
	}
	
	//console.log(ACAISSE.ticket.id_customer+'<->'+ACAISSE.customer.id);
	
	//rechargement de l'utilisateur aussi.
	if (ACAISSE.ticket.id_customer != undefined && ACAISSE.ticket.id_customer != ACAISSE.customer.id)
	{
		_retreiveCustomerInformation(ACAISSE.ticket.id_customer);
		
	}
	// @TODO : à voir poru recharger directement sans revenir à la page principale LAURENT
	//_reloadPage();
	//puis on demande le rafraichissement des totaux
	//_refreshTotals();
	_recalcAndRefreshTotals();
	
	var ticket_date = ACAISSE.ticket.day;
	var ticket_date_dm = ticket_date.substring(5,10);
	ticket_date = ticket_date_dm.split('-');
	
	$('.currentTicketNum').html('<sup>'+ticket_date[1]+'-'+ticket_date[0]+'</sup>' + ACAISSE.ticket.ticket_num);
	
	return result;
}

// fonction permettant de charger les stock
function _refreshProductPageStock2(listToRefresh) {
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'RefreshProductStockFromPage',
			'listToRefresh': listToRefresh,
			'id_pointshop': ACAISSE.id_pointshop
		},
	}).done(function (response) {
		if (response.success)
		{
			if(ACAISSE.mode_debogue === true)
			{
				console.log(['_refreshProductPageStock2() ajax response',response]);
			}
			_doStockLevelUpdateOnPage2(response.response.stocks);
		}
		else
		{
			alert('Erreur 000947a : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 000948a');
	});
}

// fonction pour remettre à kour notre stock dans le html
function _doStockLevelUpdateOnPage2(stocks) {
	if (stocks)
	{
		var line;
		for (var i = 0; i < stocks.length; i++)
		{
			var cell = stocks[i][0];
			var real_stock = real_stock_aff = stocks[i]['physical_qu'];
			var preorder_stock = preorder_stock_aff = stocks[i][2];
			var estimateStock = estimateStock_aff = parseInt(real_stock)-parseInt(preorder_stock);
			
			if  (products_stock_cache[cell.id_product] == undefined) { // si on a jamais eu ce produit a ranger dans le cache
				products_stock_cache[cell.id_product] = {};
			}
			
			if (cell.id_product_attribute != "" && cell.id_product_attribute != 0) { // si en + il a un attribut
				if(ACAISSE.mode_debogue === true)
				{console.log(['prd attr', cell.id_product_attribute, cell.id_product]);}
				products_stock_cache[cell.id_product][cell.id_product_attribute] = {};
				products_stock_cache[cell.id_product][cell.id_product_attribute].real_stock_aff = real_stock_aff;
				products_stock_cache[cell.id_product][cell.id_product_attribute].preorder_stock_aff = preorder_stock_aff;
				products_stock_cache[cell.id_product][cell.id_product_attribute].estimateStock_aff = estimateStock_aff;
			} else {
				products_stock_cache[cell.id_product].real_stock_aff = real_stock_aff;
				products_stock_cache[cell.id_product].preorder_stock_aff = preorder_stock_aff;
				products_stock_cache[cell.id_product].estimateStock_aff = estimateStock_aff;
			}
			
			line = $($('#productList .contentOverlay tr')[cell.row]);
		
			line.find('.stock_real').html(real_stock == '0' ? 'rupture' : real_stock).removeClass('error').addClass(real_stock == '0' ? 'error' : '');
			
			line.find('.stock_inqueue').html('-' + preorder_stock);

			line.find('.stock_available').html(estimateStock)
					.removeClass('error')
					.removeClass('warning')
					.removeClass('good')
					.addClass(estimateStock > params.products.STOCK_WARNING_LIMIT ? 'good' : estimateStock <= 0 ? 'error' : 'warning');

			//@TODO : stockage dans le cache product
		}
	}
}



/**
 * Les deux foctions là peuvent surmenent etre amélioré.
 * @param {$Jquery} $elemMsg id in caisse.tpl
 */
function showSuccessMessage($elemMsg){
   $('#alertMessage').addClass('successMessage');
   $elemMsg.show();
   setTimeout(function(){ 
       $('#alertMessage').removeClass('successMessage');
        $elemMsg.hide();
   }, 1000);
}

function showErrorMessage($elemMsg){
   $('#alertMessage').addClass('errorMessage');
   $elemMsg.show();
   setTimeout(function(){ 
       $('#alertMessage').removeClass('errorMessage');
        $elemMsg.hide();
   }, 1000);
}



function _updateCustomerEditForm(customer, groups) {
	//console.log(customer);
	$('input[name^=customer-edit-]').val('');
	$('input[name^=customer-edit-alias]').val('MonAdresse');
	//$('#customer-edit-group input[type=checkbox]').prop('checked', false);
	//$('#customer-edit-more input[type=checkbox]').prop('checked', false);
	//$('#customer-edit-group input[type=checkbox][readonly]').click();
	if(customer.newsletter == 1)
		$('#customer-edit-newsletter').prop('checked',true);
		
	if(customer.optin == 1)
		$('#customer-edit-optin').prop('checked',true);
	
	if ($('#userEditContainer [data-page-id="pageUser1"]').html().indexOf('Nouveau')==-1) {
		// On met à jour l'addresse principale si elle existe
		for (var key in customer.address) {
			$('#customer-edit-' + key).val(customer.address[key]);
		}
		if(customer.address)
		{
			$('#customer-id_country').val(customer.address['id_country']);
		}
		// On met à jour les informations du Customer
		for (var key in customer) {
			$('#customer-edit-' + key).val(customer[key]);
		}
	}		
		if (typeof customer.groups != 'undefined') {
			for (var key in customer.groups){
				$('#customer-edit-group-'+customer.groups[key])[0].checked=true;
			}
			
			var chckbx = $('#customer-edit-group input[type=checkbox][value='+customer.id_default_group+']')[0];
			chckbx.readOnly=chckbx.indeterminate=true;
		}
	
	}

/**
 * METHODE A REVOIR, LES PRIX DANS LE PANIEr DOIVENT ETRE CEUX RENVOY2 PAR LE SERVEUR ET PAS LE CACCHE
 * */
function refreshPrices(cart_lines) { //rafraichir les prix afficher en fonction du groupe de prix...
	
	//console.warn(['refreshPrices',cart_lines]);
	var sum = 0;
	var sum_ht = 0;
	var sum_ttc = 0;	
	
	if (cart_lines!=undefined) {
		//console.warn('TODO : AUTRE CAS DE REFRESH.... lorsque l\'argument est apssé à refreshPrice...')
		//console.log(ACAISSE);
		
		var product_list = [];
		
		for (var i=0; i<cart_lines.length; i++) {
			
			var cart_line = new CartLine(cart_lines[i]);
			//console.log(cart_line);
			
			product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(cart_lines[i].id_product))));
			product_list[i].id_attribute = cart_lines[i].id_product_attribute;
			product_list[i].qte = cart_lines[i].quantity;
			product_list[i].price_wt = cart_lines[i].price_wt;
			sum_ttc += cart_line.line_price_ht;
			sum_ttc += cart_line.line_price_ttc;
			
		}
		
		// on remet à jour la liste des produits sur les écrans...
		//var module_obj = {action:'switch_ticket',product_list:product_list, cart_lines:cart_lines};
		
		var module_obj = {
			action:'switch_ticket',
			product_list:product_list,
			ticket:new Ticket(ACAISSE.ticket),
			raw_ticket:ACAISSE.ticket
		};
		
		modules_action(module_obj);
		
		// on remet à jour le total sur les écrans... 
		module_obj = {action:'tot',tot:formatPrice(ACAISSE.cart_tot),id_ticket:ACAISSE.ticket.ticket_num};
		modules_action(module_obj);
		
		/*
		 //les totaux sont calcules dans les deux cas de figure par l'appel commun à _recalcAndRefreshTotals
		
		//@TODO vérifier l'utilité du bloc ci-dessous...
		if (parseFloat($('#totalCost').html())>ACAISSE.cart_tot) {
			module_obj = {action:'reduce',reduce:formatPrice(parseFloat($('#totalCost').html()) - ACAISSE.cart_tot),new_tot:ACAISSE.cart_tot};
			modules_action(module_obj);
		}
		
		//$('#totalCost').html(formatPrice(ACAISSE.cart_tot)+'€');
		*/
		
	} else if (typeof ACAISSE.ticket.lines=='object') {
		
		var product_list = [];
			
		
		var $trs = $('#ticketView .lines tbody tr');
		
		for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
			var ticket_line = new TicketLine(ACAISSE.ticket.lines[i]);
			/*
			console.log(ticket_line);
			*/
			product_list.push(_loadProductFromId(ACAISSE.ticket.lines[i].id_product));
			product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
			product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
			/*
			var price = getProductPrice(ACAISSE.ticket.lines[i].id_product, ACAISSE.ticket.lines[i].id_attribute)
			var unity = price.split('/');
			sum += parseFloat(price)*(parseFloat(ACAISSE.ticket.lines[i].quantity)/(price.indexOf('/')==-1?1:params.units.scale[params.units.to.indexOf('/'+unity[1])]));
			*/
			sum_ht = ticket_line.line_price_ht;
			sum_ttc = ticket_line.line_price_ttc;
			
			var $tr = $trs.eq(i+1); //la ligne de ticket à rafraîchir
			//faut rafraichier le PU de cette ligne
			$('td.cost',$tr).replaceWith(getCartLineCostLabelHTML(ticket_line));
			
			//mise à jour du total en bout de ligne
			$('td.totalLineCost',$tr).replaceWith(getCartTotalLineCostLabelHTML(ticket_line));

		}
		
		// on remet à jour la liste des produits sur les écrans...
		//@TODO : à refaire AUSSI en utilisant les infos au format TicketLine instance
		var module_obj = {
			action:'switch_ticket',
			product_list:product_list,
			ticket:new Ticket(ACAISSE.ticket),
			raw_ticket:ACAISSE.ticket
		};
		modules_action(module_obj);
		
		// on remet à jour le total sur les écrans... 
		var module_obj = {action:'tot',tot:formatPrice(sum),id_ticket:ACAISSE.ticket.ticket_num};
		modules_action(module_obj);
		
		//$('#totalCost').html(formatPrice(sum)+'€ TTC');
	}	
	/*
	ACAISSE.cart_tot = sum;
	ACAISSE.cart_tot_ht = sum_ht;
	ACAISSE.cart_tot_ttc = sum_ttc;
	*/
	_recalcAndRefreshTotals();
	
}


function _loadProductFromId(id_product,use_product_cache = null, force_reload_from_server = false)
{
  if (id_product==undefined) {
  	return null;
  }
  
  if(acaisse_products_prices.catalog[id_product]==undefined || force_reload_from_server === true)
  { 
  	 $.ajax({
        type: 'post',
        async:false,
        data: {
          'action': 'loadProduct',
          'id_product': id_product,
          'ajaxRequestID': ++ACAISSE.ajaxCount,
        },
        dataType: 'json',
      }).done(function (response) {
        if (response.success == true) { 
        	
        	for(id_product in response.results.catalog)
        	{
        		var p = response.results.catalog[id_product];
        		acaisse_products_prices.catalog[id_product] = p;
        	}
        	for(ean in response.results.ean)
        	{
        		var p = response.results.ean[ean];
        		acaisse_products_prices.ean[ean] = p;
        	}
        	//acaisse_products_prices.push(response);
        	
        } else {
          console.log('Erreur OG000404 : ' + response.errorMsg);
        }
      }).fail(function () {
        console.log('Erreur OG001404 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
      });
  	 
  }
  
  //maintenant qu'on a tenté de complété le cache on reteste si le produit est toujorus absent du cache de la caisse'
  if(acaisse_products_prices.catalog[id_product]==undefined)
  {
  	 return false;
  }
  
  if(use_product_cache === null)
  {
    use_product_cache = 'js';  
  }
  
  if(use_product_cache == 'js')
  {
  	if(ACAISSE.mode_debogue === true)
  	{
    	console.log('Loading _loadProductFromId('+id_product+') USING CACHE SYSTEM');
    }
    if (true) {// mettre à true dès que le cache remonte minimifié
    	
    	// AU MOMENT DU CHARGEMENT DU PRODUIT, ON REGARDE S'IL EST DISPONIBLE DANS LE POINTSHOP OU NON
    	// SI NON, ON NE L'AFFICHE PAS
    	var temp_p = new Product(id_product);
    	
    	for(var shop in temp_p.disponibility_by_shop) {
    		
    		//console.log(id_product+'-----------'+temp_p.disponibility_by_shop[ACAISSE.id_pointshop]);
    		 
    		if(ACAISSE.id_pointshop == shop)
    		{
    			//console.log(id_product+'-----------'+temp_p.disponibility_by_shop[ACAISSE.id_pointshop]);
    			if(temp_p.disponibility_by_shop[ACAISSE.id_pointshop] == false)
    			{
    				return null;
    			}
    			else
    			{
    				return new Product(id_product);
    			}
    		}
		}
	} else {
		return acaisse_products_prices.catalog[id_product];
	}
  }
  else
  {    
    if(localStorage.getItem('product__'+id_product) == undefined)
    {        
      if(ACAISSE.mode_debogue === true)
      {
      	console.log('Loading _loadProductFromId('+id_product+') WITHOUT CACHE SYSTEM');
      }
      var ajax_id = ++ACAISSE.ajaxCount;
      var result = undefined;
      $.ajax({
        type: 'post',
        async:false,
        data: {
          'action': 'loadProduct',
          'id_product': id_product,
          'ajaxRequestID': ++ACAISSE.ajaxCount,
        },
        dataType: 'json',
      }).done(function (response) {
        if (response.success == true) {          
          var response = response.results;
          if(response != false)
          {
            localStorage.setItem('product__'+id_product,JSON.stringify(response['catalog'][id_product]));
          }
        } else {
          alert('Erreur 000404 : ' + response.errorMsg);
        }
      }).fail(function () {
        alert('Erreur 001404 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
      });
      
    }    
    return JSON.parse(localStorage.getItem('product__'+id_product));
  }  
}

function longPressOnProduct(container, elem) {
	
	if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2') {
		$('#popupNumeric.cover').addClass('fullImg');
	}
	var pressTimer;
	
	$(container).delegate(elem, 'mouseup touchend', function () {
		if (!_loadProductFromId($(this).data('idobject')).has_attributes || $(this).data('idobject2')!="") {
			clearTimeout(pressTimer);
			// Clear timeout
			//return false;
		}
	}).delegate(elem, 'mousedown touchstart', function (e) {
		if (!_loadProductFromId($(this).data('idobject')).has_attributes || $(this).data('idobject2')!="") {
			//on desactive le menu contextuel (clic droit)
			$(this)[0].oncontextmenu = function (e) {
				e.preventDefault();
				//return false;
			};
			
			pressTimer = window.setTimeout(function () {
				
				if(ACAISSE.mode_debogue === true)
				{
					console.log(['longpress this',$(this).data('idobject2')]);
					
					//on affiche le clavier au bout de 250ms
					console.log("on affiche la pavé numérique");
					
					//cloneProductWithStock($(this).data('idobject'), $(this).data('idobject2')); //n'a pas d'utilité à mon sens (Kévin)
					
					console.log(['popupProduct',$(this).first('.product_price').html()]);
				}
				
				$('#popupNumeric')
					.data('idobject',$(this).data('idobject'))
					.data('idobject2',$(this).data('idobject2'));
				
				$('#popupNumeric .product_price')
					.html(getProductPrice($(this).data('idobject'), $(this).data('idobject2')));
				
				var p = _loadProductFromId($(this).data('idobject'));
				var name = p.product_name;
				var unity = p.unity;
				if (p.has_attributes) {
					p = p.attributes[$(this).data('idobject2')];
					name += p.attribute_name;
				}
				
				var real_stock_aff, preorder_stock_aff, estimateStock_aff;
				var cache_stock = products_stock_cache[$(this).data('idobject')];
				if (cache_stock == undefined) {
					real_stock_aff = '';
					preorder_stock_aff = '';
					estimateStock_aff = '';
				} else if ($(this).data('idobject2') != 0) {
					cache_stock = cache_stock[$(this).data('idobject2')];
					if (cache_stock == undefined) {
						real_stock_aff = '';
						preorder_stock_aff = '';
						estimateStock_aff = '';
					} else {
						real_stock_aff = cache_stock.real_stock_aff;
						preorder_stock_aff = cache_stock.preorder_stock_aff;
						estimateStock_aff = '<span class="no_internet">'+cache_stock.estimateStock_aff+'</span>';
					}
				} else {
					real_stock_aff = cache_stock.real_stock_aff;
					preorder_stock_aff = cache_stock.preorder_stock_aff;
					estimateStock_aff = '<span class="no_internet">'+cache_stock.estimateStock_aff+'</span>';
				}
				
				$('#popupNumeric .stock_available')
					.html(estimateStock_aff)
					.removeClass('error')
					.removeClass('warning')
					.removeClass('good');
				
				$('#popupNumeric .stock_real').html(real_stock_aff);
				
				$('#popupNumeric .stock_inqueue').html('-'+preorder_stock_aff);
				
				$('#popupNumeric .product_name').html('');
				
				if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0') {
					$('#popupNumeric .acaisse_productcell_content')
					.css('background-image','url('+p.image+')')
				}
				
				$('#popupNumeric .acaisse_productcell_content')
					.data('idobject',$(this).data('idobject'))
					.data('idobject2',$(this).data('idobject2'));
				
				_refreshProductPageStock([{
					id_product: $(this).data('idobject'),
					id_product_attribute: $(this).data('idobject2')==""?0:$(this).data('idobject2'),
					row: 'popup',
					col: 0
				}]);
				
				var unitId = params.units.from.indexOf(unity);
				
				if (unitId==-1) {
					//console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
					params.units.from.push(p.unity);
					params.units.to.push('');
					params.units.scale.push(1);
					unitId = params.units.from.indexOf(unity);
					//throw "Erreur d'unité : unité inconnue : ";
				}
				
				// On récupère les informations du produit
				_getProductInfos($(this).data('idobject'), $(this).data('idticketline'));
				
				$('#popupNumeric .title').show();
				
				$('#popupNumeric .global_reduc_amount').remove();
				$('#popupNumeric .cart_line_reduc_amount').remove();
				
				showPopup($(container), params.units.scale[unitId]!=1);
			   	
			}.bind(this), 250);
			//return false;
		}
	}); 
}

function _foundAttributeById(attributes,id_product_attribute)
{
	var result = false;
	$.each(attributes,function(i,attr){
		console.log(['comparaison',attr,id_product_attribute,attr.id_product_attribute]);
		if(id_product_attribute == parseInt(attr.id_product_attribute))
		{
			result = attr;
		}	
	});
	
	return result;
}

function _drawProductInfos(response, idTicketLine, open_specific_tab)
{
	var product = response.response.product;
	
	if(idTicketLine !== 'undefined')
		$('#popupNumeric').data('idticketline', idTicketLine);
	
	
	$('#productInfos')
		.data('product',product)
		.data('stocks',response.response.stocks)
		.data('stockMvtReasons',response.response.stock_mvt_reason)
		.data('stocks_available',response.response.stocks_available)		
		.data('PS_STOCK_MVT_TRANSFER_FROM', response.response.PS_STOCK_MVT_TRANSFER_FROM)
		.data('PS_STOCK_MVT_TRANSFER_TO', response.response.PS_STOCK_MVT_TRANSFER_TO)
		.data('PS_STOCK_MVT_DEC_REASON_DEFAULT', response.response.PS_STOCK_MVT_DEC_REASON_DEFAULT)
		.data('PS_STOCK_MVT_INC_REASON_DEFAULT', response.response.PS_STOCK_MVT_INC_REASON_DEFAULT);
	
	// INFOS POPUP PRIX
	$('#productInfos .prices .wholesale-price').html(product.wholesale_price);
	$('#productInfos .prices .price').html(product.wholesale_price);
	
	// S'il y a des caractéristiques
	var features = response.response.features;
	if(features.length > 0)
	{
		for (var i=0; i < features.length; i++) {
		 $('#features ul').prepend('<li><span class="fname">'+ features[i].name +'</span> <span class="fvalue">'+ features[i].value +'</span> </li>');
		};
	}
	else
	{
		 $('#features ul').prepend('<li>Aucune caractéristique</li>');
	}
	
	// S'il y a des informations de stocks avancés
	var stocks = response.response.stocks;
	var stocks_available = response.response.stocks_available;
	var attributes = response.response.attributes;
	var infos_attr = response.response.infos_attr;
	var id_product = parseInt(0 + $('#popupNumeric').data('idobject'));
	var id_product_attribute = parseInt(0 + $('#popupNumeric').data('idobject2'));
	
	var suppliers = response.response.suppliers;
	
	var attr = _foundAttributeById(attributes,id_product_attribute);
	
	//console.log({ATTR:attr, attributes:attributes, id_product_attribute:id_product_attribute });
	
	if(id_product_attribute!=0 && attr!=false)
	{
		product.name += ' <strong>' + attr.attribute_name + '</strong>';
		product.reference = attr.reference != ''? attr.reference : product.reference;
		product.ean13 = attr.ean13 != ''? attr.ean13 : product.reference;		
	}
	
	
	
	$('#productInfos .title').html('<h3>'+product.name+'</h3>');
	$('#productInfos .title').append('<span class="productId label-info"><a target="_blank" href=" ' + response.response.bo_product_link + '"><i class="fa fa-external-link"></i> '+product.id+'</a></span>');
	product.reference!=''?$('#productInfos .title').append('<span class="reference label-info"><i class="hashtag">#</i> '+product.reference+'</span>'):'';
	product.category!=''?$('#productInfos .title').append('<span class="mainCat label-info"><i class="fa fa-folder-open-o"></i> '+product.category+'</span>'):'';
	product.ean13!=''?$('#productInfos .title').append('<span class="ean label-info"><i class="fa fa-barcode"></i> '+product.ean13+'</span>'):'';      
	$('#details .descShort').html(product.description_short); 
	$('#details .desc').html(product.description); 
	
	/*
	console.log({
		id_product:id_product,
		id_product_attribute:id_product_attribute,
		stocks:stocks,
		stocks_available:stocks_available,
		attributes:attributes,
		infos_attr:infos_attr,
	});
	*/
	if(stocks !== null && stocks !== false)
	{
		$('#stockBtn').prop('disabled',false);
		
		// Si le produit a des attributs
		
		/*****************
		 * DESACTIVATION DE LA VUE AVEC ATTRIBUTS 
		 */
		
		if(false && ((infos_attr !== null && infos_attr.length > 0) || (infos_attr !== null && infos_attr.length == undefined)))
		{
			var html = '<table cellspacing="0" cellpadding="0"><thead><td>Attribut</td><td>Entrepôt</td><td>Qté physique</td><td>Qté utilisable</td><td> En commande</td><td>&nbsp;</td></thead>';

			for (var a in infos_attr) 
			{						
				html += '<tr class="attrRow">';					
				html += '<td rowspan="'+ parseInt(stocks.length+infos_attr[a].w.cmf+infos_attr[a].w.nbCmf) +'">';
					for(var ag in infos_attr[a].attr_group)
					{
						html+= infos_attr[a].attr_group[ag] +' : '+ infos_attr[a].attr_val[ag] +'<br />';
					}
					html +='</td>';
				
				for (var i=0; i < stocks.length; i++) {
					var id_w = stocks[i].id_warehouse;
					
					html += '<tr class="'+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_warehouse==stocks[i].id_warehouse?'isWarehouse':'')+'">';
					html += '<td class="first">'+ stocks[i].wname +'</td>';
					html += '<td><span class="qty qty-physique">'+ infos_attr[a].w[id_w].pQ +'</span></td>';
					html += '<td><span class="qty qty-utilisable">'+infos_attr[a].w[id_w].pU+'</span></td>';
					
					// Si on a des commandes en cours, on affiche le total et le bouton détail
					if(infos_attr[a].w[id_w]['os'] != undefined && infos_attr[a].w[id_w]['os'].length > 0)
					{
						html += '<td><span class="qty qtos">'+infos_attr[a].w.tot+'</span></td>';
					}
					else 
					{
						html += '<td><span class="qty qtos">0</span></td>';
					}
					html += '</tr>';
					
					
					if(infos_attr[a].w[id_w]['os'] != undefined && infos_attr[a].w[id_w]['os'].length > 0)
					{
						html += '<tr class="cmf">';
						html += '<td>Réf. commande</td>';
						html += '<td>Qté commandée</td>';
						html += '<td>Reliquat</td>';
						html += '<td>Livraison</td>';
						html += '<td></td>';
						html += '</tr>';
						
						for (var j=0; j < infos_attr[a].w[id_w]['os'].length; j++) 
						{
							if(infos_attr[a].w[id_w]['os'][j].qtyE !== undefined && infos_attr[a].w[id_w]['os'][j].qtyR !== undefined)
							{
								html += '<tr class="cmf">';
								html += '<td class="oRef"><i class="fa fa-truck"></i> '+ infos_attr[a].w[id_w]['os'][j].ref +'</td> ';
								html += '<td class="oQtyE">'+ infos_attr[a].w[id_w]['os'][j].qtyE +'</td> ';
								html += '<td class="oQtyR">'+ infos_attr[a].w[id_w]['os'][j].qtyR +'</td> ';
								html += '<td class="oDate">'+ infos_attr[a].w[id_w]['os'][j].date +'</td>';
								html += '<td></td>';
								html += '</tr>';
							}
						}
					}
				};
				html += '</tr>';
			}
			html += '</table>';
			$('#stocksPopup').html(html);
		}
		
		
		if(true) //toujours utyiliser cette vue, qu'il y ai attribus ou pas else //si pas d'attribus
		{			
			//console.log(infos_attr);
			//console.log(stocks);
			
			var html = '<table class="warehouses" cellspacing="0" cellpadding="0"><thead><td>Entrepôt</td><td>Qté physique</td><td>Qté utilisable</td><td> En commande</td><td></td></thead>';
			
			for (var i=0; i < stocks.length; i++) 
			{
				var idw = id_w = stocks[i].id_warehouse;
				
				var extra_classes = '';
				var warehouse_active = stocks_available[stocks[i].id_warehouse] != undefined;						
				extra_classes += acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_warehouse==stocks[i].id_warehouse?' isWarehouse':'';
				extra_classes += warehouse_active ? ' active':' inactive';
				
				
				html += '<tr class="'+extra_classes+'" data-idWarehouse="'+stocks[i].id_warehouse+'" data-suppliers='+suppliers+'>';
				html += '<td class="first">'+ stocks[i].wname + (warehouse_active && stocks_available[stocks[i].id_warehouse] != '' ?' ('+stocks_available[stocks[i].id_warehouse]+')' :'') +'</td>';
				
				if(id_product_attribute == 0)
				{
				
					html += '<td><span class="qty qty-physique">'+ stocks[i].pQ +'</span></td>';
					html += '<td><span class="qty qty-utilisable">'+ stocks[i].pU +'</span></td>';
					
					// Si on a des commandes en cours, on affiche le total et le bouton détail
					if(stocks[i][idw].os != undefined && stocks[i][idw].os.length > 0)
					{
						html += '<td><span class="qty qtos">'+stocks[i].tot+'</span></td>';
					}
					else 
					{
						html += '<td><span class="qty qtos">0</span></td>';
					}
					
					html += '<td>';
						html +='<span class="button button-success add"><i class="fa fa-chevron-up"></i></span>';
						html +='<span class="button button-error'+(stocks[i].pQ==0?' disabled':'')+' remove"><i class="fa fa-chevron-down"></i></span>';
						html +='<span class="button button-info'+(stocks[i].pQ==0?' disabled':'')+' transfert"><i class="fa fa-exchange"></i></span>';
					/*if(warehouse_active) //si entreport actif
					{
						html +='<span class="button active-warehouse"><i class="fa fa-chain-broken "></i></span>';
					}
					else
					{
						html +='<span class="button active-warehouse"><i class="fa fa-chain"></i></span>';
					}
					html += '</td>';*/
				
				}
				else //c'est une déclinaison
				{
				
					html += '<td><span class="qty qty-physique">'+ infos_attr[id_product_attribute].w[idw].pQ +'</span></td>';
					html += '<td><span class="qty qty-utilisable">'+ infos_attr[id_product_attribute].w[idw].pU +'</span></td>';
					
					
					// Si on a des commandes en cours, on affiche le total et le bouton détail
					if(false && stocks[i][idw].os != undefined && stocks[i][idw].os.length > 0) 
					{
						html += '<td><span class="qty qtos">'+stocks[i].tot+'</span></td>';
					}
					else 
					{
						html += '<td><span class="qty qtos">0</span></td>';
					}
					
					html += '<td>';
						html +='<span class="button button-success add"><i class="fa fa-chevron-up"></i></span>';
						html +='<span class="button button-error'+(stocks[i].pQ==0?' disabled':'')+' remove"><i class="fa fa-chevron-down"></i></span>';
						html +='<span class="button button-info'+(stocks[i].pQ==0?' disabled':'')+' transfert"><i class="fa fa-exchange"></i></span>';
					/*if(warehouse_active) //si entreport actif
					{
						html +='<span class="button active-warehouse"><i class="fa fa-chain-broken "></i></span>';
					}
					else
					{
						html +='<span class="button active-warehouse"><i class="fa fa-chain"></i></span>';
					}*/
					html += '</td>';
				
				}
				
				html += '</tr>';
				
				
				if(false && 	stocks[i][idw].os != undefined && stocks[i][idw].os.length > 0)
				{
					html += '<tr class="cmf">';
					html += '<td>Réf. commande</td>';
					html += '<td>Qté commandée</td>';
					html += '<td>Reliquat</td>';
					html += '<td>Livraison</td>';
					html += '<td></td>';
					html += '</tr>';
					
					for (var j=0; j < stocks[i][idw].os.length; j++) 
					{
						if(stocks[i][idw].os[j].qtyE !== undefined && stocks[i][idw].os[j].qtyR !== undefined)
						{
							html += '<tr class="cmf">';
							html +='<td class="oRef"><i class="fa fa-truck"></i> '+ stocks[i][idw].os[j].ref +'</td> ';
							html +='<td class="oQtyE">'+ stocks[i][idw].os[j].qtyE +'</td> ';
							html +='<td class="oQtyR">'+ stocks[i][idw].os[j].qtyR +'</td> ';
							html +='<td class="oDate">'+ stocks[i][idw].os[j].date +'</td>';
							html +='<td></td>';
							html += '</tr>';
						}
					}
				}
			};
			html += '</table>';
			$('#stocksPopup').html(html);
		}
		
	}
	else
	{
		 $('#stockBtn').prop('disabled','disabled');
	}
	
	
	
	$('#popupProduct, #features, #stocksPopup, #prices').height(parseInt($('#middleContainer').height()-$('#productInfos .title').height()-23));
	//return product; 
	//$('#details .descShort').html(response.product.description);
	
	if(open_specific_tab != undefined)
	{
		$(open_specific_tab).click();
	}
	
}


function _getProductInfos(idProduct, idTicketLine){
	//console.log(idProduct);
	
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'GetProductDetails',
			'id_product': idProduct,
			'id_ticket_line': idTicketLine,
		},
	}).done(function (response) {
		//console.log(response);
		if (response.success)
		{
			 _drawProductInfos(response,idTicketLine);
		}
		else
		{
			alert('Erreur L000969 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur L000969 : ma première =P');
	});
}

function cloneProductWithStock(idProduct, idProductAttribute){
	if(ACAISSE.mode_debogue === true)
	{
		console.log(idProduct);
		console.log(idProductAttribute);
	}
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'GetProductWithStock',
			'idProduct': idProduct,
			'idProductAttribute': idProductAttribute,
			'id_pointshop': ACAISSE.id_pointshop
		},
	}).done(function (response) {
		if (response.success)
		{
			var p = response.response.product;
			
			var name = _loadProductFromId(idProduct).product_name;
			var img = _loadProductFromId(idProduct).image;
			var price = getProductPrice(idProduct, idProductAttribute);
			
			//si il y a un attribut
			if(typeof(_loadProductFromId(idProduct)['attributes'][idProductAttribute]) !='undefined')
			{
			  name = _loadProductFromId(idProduct)['attributes'][idProductAttribute].attribute_name;
			  img = _loadProductFromId(idProduct)['attributes'][idProductAttribute].image;
			  price = getProductPrice(idProduct,idProductAttribute);
			}

			var product = {
				real_stock : p.ps_stock,
				preorder_stock : p.preorder_stock,
				estimate_stock : parseInt(p.ps_stock) - parseInt(p.preorder_stock),
				name : name,
				img : img,
				price : price,
				id : idProduct,
				id_attribute : idProductAttribute
			};      
			doCloneProduct(product);
			return product;  
		}
		else
		{
			alert('Erreur 000947 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 000948b');
	});
}

function doCloneProduct(product) {
	//var product = getProductWithStock($elemToClone.data('idobject'), $elemToClone.data('idobject2'));
	$('#popupNumeric').data('idobject', product.id).data('idobject2', product.id_attribute);
	$('#popupNumeric .stock_real')
			.text(product.real_stock == '0' ? 'rupture' : product.real_stock).removeClass('error').addClass(product.real_stock == '0' ? 'error' : '');
	$('#popupNumeric .stock_inqueue').text('-' + product.preorder_stock);
	$('#popupNumeric .product_price').text(product.price);


	$('#popupNumeric .stock_available').text(product.estimate_stock)
			.removeClass('error')
			.removeClass('warning')
			.removeClass('good')
			.addClass(product.estimate_stock > params.products.STOCK_WARNING_LIMIT ? 'good' : product.estimate_stock <= 0 ? 'error' : 'warning');
	
	$('#popupNumeric .product_name').text(product.name);
	
	var pImg;
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0') {
		if(product.img !== undefined){
			pImg = product.img
		}
		else {
			pImg = '../modules/acaisse/img/noPhoto.jpg';
		}
		$('#popupNumeric .acaisse_productcell_content').css('background-image','url('+ pImg +')');
		
		if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='2') {
			$("#popupNumeric").addClass("fullImg");
		}	
	}
}



function _cancelTicket() {
	$.ajax({
		type: 'post',
		data: {
			'action': 'CancelTicket',
			'id_ticket': ACAISSE.ticket.id,
			'ajaxRequestID': ++ACAISSE.ajaxCount,
			//'listToRefresh':ACAISSE.pageProductBuffer,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true)
		{
			_startNewPreorder();
			$('#totalWithoutDiscount').fadeOut().html('');
		}
		else
		{
			alert('Erreur 00177A : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 00178A : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
	});
}

function _validateCurrentTicket() {
	//si le ticket est vide rien à faire
	var error = false;
	if (ACAISSE.ticket.lines.length == 0)
	{
		alert('Veuillez d\'abord sélectionner au moins un produit');
		error = true;
	}
	else if (acaisse_profilesNoCheckinIDs.indexOf('' + ACAISSE.customer.id) != -1)
	{
		alert('Veuillez d\'abord sélectionner un client.');
		error = true;
	}
	else if (!error)//tout est bon, on va demander au serveur la conversion du ticket en véritable commande Prestashop
	{
		$.ajax({
			type: 'post',
			data: {
				'action': 'ValidateTicket',
				'id_ticket': ACAISSE.ticket.id,
				'id_customer': ACAISSE.customer.id,
				'id_group': ACAISSE.customer.id_default_group, //3
				'id_force_group': ACAISSE.id_force_group, //5
				'ajaxRequestID': ++ACAISSE.ajaxCount,
			},
			dataType: 'json',
		}).done(function (response) {
			if (response.success == true) {
				response = response.response;
				/*
				if (response.id_not_found.length>0) {
					var p = _loadProductFromId(response.id_not_found[0]);
					console.log(response);
					alert("ATTENTION !! Le produit #"+p.id_product+" : "+p.product_name+" n'as pas été correctement ajouté au ticket. Il se peut que la quantité minimal de commande dans la fiche produit soit inférieur à la quantité actuelle du ticket.");
					return false;
				}
				*/
				//console.log(['validateTicket','response',response]);
				
				/*
				$('#preOrderButtonsPanel').hide();
				$('#orderButtonsPanel').show();
				*/
				
				
				ACAISSE.cart_tot = response.cart_tot;
				ACAISSE.cart_tot_ht = response.cart_tot_ht;
				ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
				
				//console.log('ACAISSE.cart_tot : ' + ACAISSE.cart_tot);
				//alert(ACAISSE.ticket);
				ACAISSE.ticket = response.ticket;
				ACAISSE.cartModificationLocked = true; //on bloque les action d'ajout de produits et de modification des quantités
				
				refreshPrices(response.cart_products);
				
			} else {
				alert('Erreur 00177B : ' + response.errorMsg);
			}
		}).fail(function () {
			alert('Erreur 00178B : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
		});
	}
}


function trashLineFromCart(id_product, id_product_attribute, id_ticket_line) {
	if (ACAISSE.ticket.id == null)
	{
		alert('Impossible d\'ajouter un produit. Aucun ticket crée.');
		return;
	}
	
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'TrashLineFromCart',
			'id_ticket': ACAISSE.ticket.id,
			'id_product': id_product,
			'id_attribute': id_product_attribute,
			'id_ticket_line': id_ticket_line,
			'ajaxRequestID': ACAISSE.ajaxCount,
			'listToRefresh': ACAISSE.pageProductBuffer,
		}
	}).done(function (response) {
		if (!response.success) {
			alert('Erreur 000554' + response.errorMsg);
			return;
		}
		response = response.response;
		ACAISSE.ticket = response.ticket;
		
		console.log(ACAISSE.ticket.lines.length);
		if (ACAISSE.ticket.lines.length == 0)
		{
			$.ajax({
				type: 'post',
				dataType: 'json',
				data: {
					'action': 'CancelCartFromTicket',
					'id_ticket': ACAISSE.ticket.id,
					'id_product': id_product,
					'id_product_attribute': id_product_attribute,
				}
			}).done(function (response) {
				if (!response.success) {
					alert('Erreur 001554' + response.errorMsg);
					return;
				}
				
				_startNewPreorder();
	
			}).fail(function () {
				alert('Error 001555 Trash cart after cancel real cart failed.');
			});
	
			return; //on stop ici, la commande sera retenté qu'après
			// annulation du panier et annulation du passage à l'encaissement	
		}
		else
		{
			var product_list = [];
			for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
				product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
				product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
				product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
			}
			if(ACAISSE.mode_debogue === true)
				{console.log(product_list);}
			//var module_obj = {action:'switch_ticket',product_list:product_list};
			
			var module_obj = {
				action:'switch_ticket',
				product_list:product_list,
				ticket:new Ticket(ACAISSE.ticket),
				raw_ticket:ACAISSE.ticket
			};
			
			modules_action(module_obj);
			
			_refreshTicket(null,false);
			_doStockLevelUpdateOnPage(response.stocks);
		}
		
		
	}).fail(function () {
		alert('Error AddProductToCart failed.');
	});
}

function _editCustomer(logg)
{
	if ($('input[name="groupBox[]"][readonly]').length != 1) {
    	//alert('Veuillez choisir un et un seul groupe par défaut en cliquant une seconde fois sur la case à cocher désiré.');
    	//return false;
    }
    
    var datas = {};
    datas.id_default_group = $('input[name="groupBox[]"][readonly]').val();
    
    var groupBox = [];
    $('input[name="groupBox[]"]:checked, input[name="groupBox[]"][readonly]').each(function () {
        groupBox.push($(this).val());
    });
    
    var newsletter = $('input[name="customer-edit-newsletter"]').prop('checked');
    var optin = $('input[name="customer-edit-optin"]').prop('checked');
    var note = $('textarea[name="customer-edit-note"]').val();
    
    $('input[name^="customer-edit-"]').each(function () {
        datas[$(this).attr('name').replace('customer-edit-','')] = $(this).val();
    });
    
    datas['groups'] = groupBox;
    datas['newsletter'] = newsletter;
  	datas['optin'] = optin;
    datas['logg'] = logg;
    datas['id_country'] = $('#customer-id_country').val();
    datas['note'] = note;
    
    // Since 2.4  : on passe l'id_pointshop
    datas['id_pointshop'] = ACAISSE.id_pointshop;
    
    // Since 2.4.6
    datas['id_gender'] = $('#customer-edit-id_gender').val();
    
    if ($('#userEditContainer [data-page-id="pageUser1"]').html().indexOf('Nouveau')!=-1) {
		datas.id=0;
	}
    
    PRESTATILL_CAISSE.loader.start();
    
    $.ajax({
        type: 'post',
        data: {
            'action': 'EditCustomer',
            'datas' : datas,
        },
        dataType: 'json',
    }).done(function (response) {
        if (response.success == true)
        {
            for (var key in response.response.successBut.error){
                $('#customer-edit-error-' + key).fadeIn();
            }
            if((response.response.successBut.error).length != null){
                sendNotif('greenFont','<i class="fa fa-check"></i> Enregistré');
                closeAllPanel();
            }
            if(response.response.successBut.addressError == 1){
                $('#customer-edit-error-blank-input').fadeIn();
            }
            if(response.response.addressId != null){
                $('#customer-edit-id_address').val(response.response.addressId);
            }     
            
            PRESTATILL_CAISSE.loader.end();
            
            if(response.request.datas.logg == 'true')
            {
            	// A VALIDER : On passe au client qu'on vient d'enregistrer
				switchTicketCustomer(response.response.customer.id,response.response.customer.id_default_group);
            	_switchCustomer(response.response.customer, false, response.response.additionnal_customer_informations);
            	_retreiveCustomerInformation(response.response.customer.id); 
            	
            }
            else
            {
            	// Vidage des champs de saisi lorsqu'on enregistre uniquement le client
			    $('input[name="groupBox[]"]').each(function () {
			        $(this).prop('checked', false);
			    });
			    $('input[id^="customer-edit-group"]').each(function () {
			        $(this).prop('checked', false);
			    });
			    
			    $('input[name="customer-edit-newsletter"]').prop('checked', false);
			    $('input[name="customer-edit-optin"]').prop('checked', false);
			    $('textarea[name="customer-edit-note"]').val('');
			    
			    $('input[name^="customer-edit-"]').each(function () {
			        $(this).val('');
			    });
            }
        }
        else
        {
            for (var key in response.errorMsg.error){
                $('#customer-edit-error-' + key).fadeIn();
            }
            //alert('Erreur 000656 : ' + response.errorMsg);
            PRESTATILL_CAISSE.loader.end();
        }
        
    }).fail(function () {
        alert('Erreur 000656 : Une erreur est survenue. Si l\'erreur persiste veuillez contacter l\'adminstrateur du service');
    });
}

function removeOneFromCart(id_product, id_product_attribute, quantity, id_ticket_line) {
	if( typeof(quantity) == 'undefined' || quantity == '' ){
		quantity = 1;
	}
	
	if( typeof(id_ticket_line) == 'undefined' || id_ticket_line == '' ){
		id_ticket_line = false;
	}
	
	if (ACAISSE.ticket.id == null)
	{
		alert('Impossible d\'ajouter un produit. Aucun ticket crée.');
		return;
	}

	/*
	if (ACAISSE.cartModificationLocked == true)
	{
		$.ajax({
			type: 'post',
			dataType: 'json',
			data: {
				'action': 'CancelCartFromTicket',
				'id_ticket': ACAISSE.ticket.id,
				'id_product': id_product,
				'id_product_attribute': id_product_attribute,
			}
		}).done(function (response) {
			if (!response.success) {
				alert('Erreur 001556' + response.errorMsg);
				return;
			}
			ACAISSE.ticket = response.response.ticket;
			//remise en place du panneau non encaisse
			$('#orderButtonsPanel').hide();
			$('#preOrderButtonsPanel').show();
			$('#orderButtonsPanel .paymentButton').removeClass('selected');
			$('#orderButtonsPanel .paymentButton').removeClass('selected');
			$('#preOrderButtonsPanel #registerPayment').addClass('disabled');
			ACAISSE.choosenPaymentMode = false;
			ACAISSE.cartModificationLocked = false;
			//puis rappel de l'instruction initiale
			removeOneFromCart(response.request.id_product, response.request.id_product_attribute , response.request.quantity , response.request.id_ticket_line);

		}).fail(function () {
			alert('Error 001557 Remove one qu after cancel real cart failed.');
		});

		return; //on stop ici, la commande sera retenté qu'après
		// annulation du panier et annulation du passage à l'encaissement	
	}
	//sinon on peut réaliser l'opération directement.
	*/
	
	if(ACAISSE.mode_debogue === true)
			{console.log(['laurent casse tout2', quantity]);}
	
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'RemoveOneProductToCart',
			'id_ticket': ACAISSE.ticket.id,
			'id_product': id_product,
			'id_attribute': id_product_attribute,
			'id_ticket_line': id_ticket_line,
			'ajaxRequestID': ACAISSE.ajaxCount,
			'listToRefresh': ACAISSE.pageProductBuffer,
            'quantity' : quantity,
			'id_customer': ACAISSE.customer.id,
			'id_group': ACAISSE.customer.id_default_group, //3
			'id_force_group': ACAISSE.id_force_group, //5
		}
	}).done(function (response) {
		if (!response.success) {
			alert('Erreur 000555' + response.errorMsg);
			return;
		}
		response = response.response;
		ACAISSE.ticket = response.ticket;
		
		var product_list = [];
		for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
			product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
			product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
			product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
		}
		if(ACAISSE.mode_debogue === true)
			{console.log(product_list);}
		//var module_obj = {action:'switch_ticket',product_list:product_list};
		
		var module_obj = {
			action:'switch_ticket',
			product_list:product_list,
			ticket:new Ticket(ACAISSE.ticket),
			raw_ticket:ACAISSE.ticket
		};
		
		modules_action(module_obj);
		
		$('#totalWithoutDiscount').fadeOut().html('');
		
		_refreshTicket(null,false);
		_doStockLevelUpdateOnPage(response.stocks);
	}).fail(function () {
		alert('Error AddProductToCart failed.');
	});

}



function _startNewPreorder() {
	
	PRESTATILL_CAISSE.loader.start();
	
	//appel au serveur pour obtenir un nouveau numéro de ticket.
	$.ajax({
		//sans url: ca poste sur le meme controller
		data: {
			'action': 'StartNewPreorder',
			'id_pointshop': ACAISSE.id_pointshop,
			//'id_customer': ACAISSE.customer.id,
			//'id_group': ACAISSE.customer.id_default_group,
		},
		type: 'post',
		dataType: 'json',
	}).done(function (response) {
			
		if (response.success == true)
		{
			if(ACAISSE.mode_debogue === true)
			{
				console.log('resultat requete serveur StartNewPreorder');
				console.log(response);
			}
			ACAISSE.ticket = response.response.ticket;
			
			ACAISSE.payment_parts=[];
			$('#orderButtonsPanel .paymentButton').removeClass('disabled');
			
			ACAISSE.recu = 0;
			ACAISSE.rendu = 0;
			ACAISSE.total_cost = 0;
			ACAISSE.total_cost_ht = 0;
			ACAISSE.rendu = 0;
			_refreshTicket(null,false);
			//_refreshTotals();
			_recalcAndRefreshTotals();
			ACAISSE.allActionLocked = false;		  	// on re-autorise l'ensemble des action
			ACAISSE.cartModificationLocked = false;		// on reautorise les action de modification du panier
			_refreshLockedUI();							// on rafraichit l'UI suppression des class "disabled" sur
			//	 les buttons qui était inactif a cause des actions locked
			ACAISSE.choosenPaymentMode = false;			// on déselectionne le mode de paiement choisi
			
			$('#registerPayment').addClass('disabled'); // on grise le bouton de validation d'un encaissement'
			$('.paymentButton').removeClass('selected');// on déselectionne le mode de paiement
			$('#multiplePayment').removeClass('disabled'); // on réactive le btn pour le payment multiple

			closeAllPanel();							// on ferme tous les panneaux verticaux
			var ps = acaisse_pointshops.pointshops[ACAISSE.id_pointshop];
			ACAISSE.current_id_page = ps.id_main_page;	// on rebascule sur la page d'accueil du point de vente
			
			if(ACAISSE.mode_debogue === true)
			{console.log(ACAISSE.ticket.ticket_num);}
			
			
			if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 1) {
				$('#openRootPageCaisse').click();
				_gotoProductList();
			} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 2
				&& acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.navigation_by_category == 1) {
				$('#openRootCategorie').click(); 
			} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 0) {
				//$('.switcher').click(); 
				_gotoTicketView();
			}
			
			resetTileHeight();
			
			$('#reductionGlobalOnCart .pastille').remove();
			
			_switchToDefaultCustomer(); //on retourne au profil par défaut			
			
			_addIdEmployeeToTicket();
			
			var module_obj = {
				action:'switch_ticket',
				id_ticket:ACAISSE.ticket.ticket_num,
				product_list:[],
				ticket:new Ticket(ACAISSE.ticket),
				raw_ticket:ACAISSE.ticket
			};
			
			modules_action(module_obj);
			PRESTATILL_CAISSE.loader.end();
			
			$('.addToBAsketIndicator').remove();
			_refreshVeilleInterface();
			
			// On met à 0 les champs de réduction globale
			$('.selectProductFloatNb .lbabK-inputable, #popup_global_title, #popup_global_textarea').val('');
		}
		else
		{
			alert('Erreur 000453 : ' + response.errorMsg);
		}

	}).fail(function () {
		alert('Erreur 000452 : Une erreur est survenue. Merci de recharger la page.');
	});
}

function _refreshLockedUI() {
	//console.log('@TODO rafraichissement de l\'UI en fonction des "locked"');
	//grisage/dégrisage des buttons
}

function togglePanel(panelId) {
	for (var i = 0; i < ACAISSE_vertical_panels.length; i++)
	{
		var panel = ACAISSE_vertical_panels[i];
		if (panel.id == panelId)
		{
			if ($('#' + panel.id).hasClass('open')) {
				//il faut le fermer
				closeAllPanel();
			}
			else
			{
				//faut fermer l'un des autres ouvrir celui-ci
				//ce que fait la méthode suivante:
				openPanel(panelId);
			}
		}
	}
}

function openPanel(panelId) {
	
	//console.log('call openPanel('+panelId+')');
	
	closeAllPanel(panelId);
	
	$('#footer .navActive,#footer .open').removeClass('navActive').removeClass('open');
    
	for (var i = 0; i < ACAISSE_vertical_panels.length; i++)
	{
		var panel = ACAISSE_vertical_panels[i];
		if (panel.id == panelId)
		{
			$(panel.zone + ',#' + panel.id).addClass('open').addClass('navActive');
			panel.openCallback();
		}
	}
}

function closeAllPanel(exeptionForPanelId) {
	for (var i = 0; i < ACAISSE_vertical_panels.length; i++)
	{
		var panel = ACAISSE_vertical_panels[i];
		if (panel.id != exeptionForPanelId)
		{
			if ($('#' + panel.id).hasClass('open'))
			{
				$(panel.zone + ',#' + panel.id).removeClass('open').removeClass('navActive');
				panel.closeCallback();
			}
		}
	}
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate(email) {
	//console.log(email);
  if (validateEmail(email)) {
    return true;
  } 
  return false;
}

function _switchToTicket(id_ticket, callback, gototicket_view) {
	$('#preOrderNumber').prepend('<i class="fa fa-spinner fa-spin"></i>');
	$.ajax({
		//sans url: ca poste sur le meme controller
		data: {
			'action': 'ReloadFromTicketID',
			'id_pointshop': ACAISSE.id_pointshop,
			'id_ticket': id_ticket,
			'id_customer': ACAISSE.customer.id,
			'id_group': ACAISSE.customer.id_default_group,
		},
		type: 'post',
		dataType: 'json',
	}).done(function (response) {
		if (response.success)
		{
			ACAISSE.ticket = response.response.ticket;
			ACAISSE.payment_parts = response.response.payment_parts;
			
			$('#reductionGlobalOnCart .pastille').remove();
			
			if (response.response.globalCartRule != undefined) {
				var cartRule = response.response.globalCartRule;
				var text = '';
				
				if (parseFloat(cartRule.reduction_amount)>0) {
					if (parseFloat(cartRule.reduction_amount)==parseInt(cartRule.reduction_amount)) { // on a à faire à un entier
						text = parseInt(cartRule.reduction_amount)+'€';
					} else {
						test = parseFloat(cartRule.reduction_amount).toFixed(2)+'€';
					}
				} else {
					if (parseFloat(cartRule.reduction_percent)==parseInt(cartRule.reduction_percent)) { // on a à faire à un entier
						text = parseInt(cartRule.reduction_percent)+'%';
					} else {
						test = parseFloat(cartRule.reduction_percent).toFixed(2)+'%';
					}
				}
				
				$('#reductionGlobalOnCart').prepend('<span class="pastille big"><span class="label">Remise appliquée :</span> <br /><b>-'+text+'</b><br /><span class="modify">modifier</span></span>');
			}
			
			if (ACAISSE.payment_parts.length>0) {
	        	$('#orderButtonsPanel .paymentButton').addClass('disabled');
	        } else {
	        	$('#orderButtonsPanel .paymentButton').removeClass('disabled');
			}
			
			var product_list = [];
			for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
				product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
				product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
				product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
			}
			if(ACAISSE.mode_debogue === true)
			{console.log(product_list);}
			
			//var module_obj = {action:'switch_ticket',product_list:product_list};
			
			var module_obj = {
				action:'switch_ticket',
				id_ticket:ACAISSE.ticket.ticket_num,
				product_list:product_list,
				ticket:new Ticket(ACAISSE.ticket),
				raw_ticket:ACAISSE.ticket
			};
			
			modules_action(module_obj);
			
			_refreshTicket(null,false);
			
			
			if(gototicket_view != undefined && gototicket_view != false)
			{
				_gotoTicketView();
			}

      /**
       * @TODO s'occuper ici de remettre à jour l'etat des bouttons une fois les payements parts rechargé pour ce tocket qui vient d'etre rechargé
       * (anciennement la ligne ci-dessous)
       */
			//recheckIfPaymentCompleted();
			
			if (typeof callback==='function') {
				callback();
			}
			
			ACAISSE.scanbarcodeMode = 'product';
			
			if(ACAISSE.ticket.is_bloqued == 1){  
				
				sendNotif('greenFont','<i class="fa fa-check"></i> Commande N°'+ACAISSE.ticket.id);
			}else if(ACAISSE.ticket.is_devis == 1){
				sendNotif('greenFont','<i class="fa fa-check"></i> Devis N°'+ACAISSE.ticket.num_devis);
			}else {
				sendNotif('greenFont','<i class="fa fa-check"></i> Ticket N°'+ACAISSE.ticket.ticket_num);
			}
			
			$('.ticketBox').hide();
			
			/*
			 * Hook d'action au chargement du ticket
			 */
			PRESTATILL_CAISSE.executeHook('actionPrestatillSwtchToTicket',{'id_ticket':ACAISSE.ticket.id});
		}
		else
		{
			//alert('Erreur 000911 : ' + response.errorMsg);
		}
		$('#preOrderNumber i').eq(0).remove();
		$('div.open').click();
	}).fail(function () {
		alert('Erreur 000912 : une erreur est survenue. Si celle-ci persiste, veuillez contacter l\'administrateur du service.');
		$('#preOrderNumber i').eq(0).remove();
		$('div.open').click();
	});
	
	lcd_write_line('',0);
	lcd_write_line('',1);
	lcd_write_line('',2);
	lcd_write_line('',3);
}

function date_heure(id)
{
        date = new Date;
        annee = date.getFullYear();
        moi = date.getMonth();
        mois = new Array('Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre');
        j = date.getDate();
        jour = date.getDay();
        jours = new Array('Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        resultat = '<span class="dday">'+jours[jour]+' '+j+' '+mois[moi]+' '+annee+'</span><br /><span class="hhour">'+h+':'+m+':'+s+'</span>';
        document.getElementById(id).innerHTML = resultat;
        setTimeout('date_heure("'+id+'");','1000');
        return true;
}

function _refreshFilterPreOrderList(filter) {
	var count = 0;
	var lastID = 0;
	var filter = $('#preOrderListContainer .keyboard span.display').html();

	$('#preOrderListContainer .left ul li').each(function (i, elt) {
		if ((filter == '' || ('' + $(elt).data('num')).indexOf(filter) != -1) && $(elt).data('id') != ACAISSE.ticket.id)
		{
			$(elt).show();
			count++;
			lastID = $(elt).data('id');
		}
		else
		{
			$(elt).hide();
		}
	});
	if (!$('#preOrderListContainer .left ul').hasClass('notLoaded') && count === 1) {
		_switchToTicket(lastID);
	}
}

function _initDefaultProfilesList() {
	var html = '<ul>';
	for (var i = 0; i < acaisse_profilesIDs.length; i++) {
		var customer = acaisse_profiles[acaisse_profilesIDs[i]];
		html += '<li data-id="' + customer.id + '" data-id_group="' + customer.id_default_group + '" class="gender-' + customer.id_gender + '"><span class="name">' + customer.firstname + ' ' + customer.lastname + '<span><span class="email">' + customer.email + '</span><span class="group">' + customer.id_default_group + '</span></li>';
	}
	html += '</ul>';
	$('#defaultCustomersProfils').html(html);
}

function _initMainPage(id_pointshop) {   
	if (id_pointshop == null || typeof (id_pointshop) == 'undefined')
	{
		id_pointshop = ACAISSE.id_pointshop;
	}
	
	// On check si l'employé connecté a le droit de gérer la boutique
	//_checkPointshopPermission();
	var nb_pointshops = $('#pointshop-list select > option').length;
	if(nb_pointshops < 2)
	{
		// Si on a qu'un pointshop, on force le pointshop
		id_pointshop = $('#pointshop-list option:first-child').attr('value');
		// A VOIR POUR SETLE LOCAL STORAGE
	}
	
	var ps = acaisse_pointshops.pointshops[id_pointshop];
	
	ACAISSE.id_pointshop = id_pointshop;
	ACAISSE.till_width = ps.datas.till_width;
	ACAISSE.till_height = ps.datas.till_height;
	
	var pointshopList = '<select>';
    	$('#pointshop-list option[value="'+ACAISSE.id_pointshop+'"]').prop('selected', true);
	
	if(ACAISSE.mode_debogue === true)
			{console.log("TITI >> ");}
	
	ACAISSE.current_id_page = ps.id_main_page;
	ACAISSE.current_pointshop = ps;
	$('div.open').click();
	_reloadPage();
	//Ce point de vente permet la modification de la fiche du client
	if (ACAISSE.current_pointshop.datas.editable_customer == '1'
			&& acaisse_profilesIDs.indexOf('' + ACAISSE.customer.id) == -1
			)
	{
		$('#editUserButton').show();
	}
	else //ou pas
	{
		$('#editUserButton').hide();
	}
	
	//le paiement multiple est-il activé
  if (ACAISSE.current_pointshop.datas.active_multi_payment == '1')
  {
    $('#multiplePayment').show();
  }
  else //ou pas
  {
    $('#multiplePayment').hide();
  }
  
  	_initDisplayableDeliveryPopup(ACAISSE.current_pointshop.datas);
  
	// Gestion des employés
	_getEmployeesForThisPointshop();

	$('.switcher .addToBAsketIndicator').remove();
	
	_setWidthAndHeight();
	//_updateCustomerEditForm(ACAISSE.customer);
	_startNewPreorder();
	_reloadCashHistory();
	_searchLastCa();
	_createInputGroup();
}

function _initDisplayableDeliveryPopup(pointshop_datas)
{
	
	if(pointshop_datas.choose_delivery_address == 0 && pointshop_datas.choose_carrier == 0 && pointshop_datas.choose_invoice_address == 0 && pointshop_datas.activate_detaxed_sale == 0)
	{
		$('#deliveryButton').hide();
	}	
	else
	{
		$('#deliveryButton').show();
		
		if(pointshop_datas.choose_delivery_address == 1)
		{
			$('#popupDeliveryForm .delivery_address_column').show();
		}
		else
		{			
			$('#popupDeliveryForm .delivery_address_column').hide();
		}
		
		
		if(pointshop_datas.choose_carrier == 1)
		{
			$('#popupDeliveryForm .carrier_address_column').show();
		}
		else
		{			
			$('#popupDeliveryForm .carrier_address_column').hide();
		}
		
		
		if(pointshop_datas.choose_invoice_address == 1)
		{
			$('#popupDeliveryForm .invoice_address_column').show();
		}
		else
		{			
			$('#popupDeliveryForm .invoice_address_column').hide();
		}
		
		
		if(pointshop_datas.activate_detaxed_sale == 1)
		{
			$('#popupDeliveryForm .detax_address_column').show();
		}
		else
		{			
			$('#popupDeliveryForm .detax_address_column').hide();
		}
		
		
		
	}
	
}

function _pointshopList() {
	if(ACAISSE.mode_debogue === true)
			{console.log(ACAISSE);}
}

function _createInputGroup() {
	$('#customer-edit-group').empty();
	if(ACAISSE.mode_debogue === true)
			{console.log('on passse');}
	for (var key in acaisse_group) {
		var div = $('<div></div>', {
			class: 'checkbox-group',
		});
		
		var checkbox = $('<input>', {
			type: 'checkbox',
			name: 'groupBox[]',
			id: 'customer-edit-group-' + acaisse_group[key].id_group,
			value: acaisse_group[key].id_group,
		});
		var label = $('<label></label>', {
			text: acaisse_group[key].name + ' ',
		});
		//checkbox.attr('type', 'checkbox').attr('name', 'customer-edit-group'+acaisse_group[key].id_group).val(acaisse_group[key].name)
		div.append(checkbox);
		$('#customer-edit-group').append(div);
		checkbox.before(label);
		checkbox.after('<br>');
	}
}

function _reloadCashHistory() {
	$.ajax({
		data: {
			'action': 'CheckPointshopCashHistory',
			'id_pointshop': ACAISSE.id_pointshop,
			'cash_is_open': ACAISSE.cash_open!=undefined?ACAISSE.cash_open:false,
		},
		type: 'post',
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true)
		{
			_refreshCashPanel();
			ACAISSE.cash_history = response.response.cash_history;
			ACAISSE.last_cash_amounts = response.response.last_cash_amounts;
			_refreshCashPanel();
		}
		else
		{
			alert('Erreur 01004 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 01005 : Impossible de communiquer les informations au serveur');
	});
}

function _checkPointshopPermission() {
	$.ajax({
		data: {
			'action': 'CheckPointshopPermission',
			'id_pointshop': ACAISSE.id_pointshop
		},
		type: 'post',
		dataType: 'json',
	}).done(function (response) {
		
		if (response.success == true)
		{
			if(response.response.is_ok == false)
			{
				$('#global-loader').html('<div>Vous n\'êtes pas autorisé à ouvrir cette boutique</div>');
				PRESTATILL_CAISSE.loader.start();
				$('#pointshop-list').css('z-index','5001').css('position','relative');
				$('#middleContainer').display('none');
			}
			else
			{
				$('#middleContainer').display('block');
			}
				
		}
		else
		{
			alert('Erreur 01004 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 01005 : Impossible de communiquer les informations au serveur');
	});
}


function _refreshCashPanel() {
	
	var nb_states = 0;
	if (ACAISSE.cash_history != undefined) {
		nb_states = ACAISSE.cash_history.length;
	}
	
	if((ACAISSE.last_cash_amounts != undefined && ACAISSE.last_cash_amounts.length > 0) && acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos == true)
	{
		var input_cash = ACAISSE.last_cash_amounts[0];
		var form_to_fill = $('#cash_open_form');
		
		if(ACAISSE.cash_open == true)
		{
			form_to_fill = $('#cash_close_form');
		}
		//console.log(form_to_fill);
		$.each(input_cash, function(index, value) {
			form_to_fill.find('#'+index).val(value); 
		});
		
		$('#tabPrepareCash').hide();
		
		$('.totalCash').text(function () {
            var money = numeral(
                    $(this).prev('td').children().data('money')
                    );
            var result = money.multiply(
                    $(this).prev('td').children().val()
                    );
            return result.format();
        });

        //calcul du total en euro à l'ouverture, mvt et fermeture
        var totalCash = numeral(0);
        $('li.open .totalCash').each(function () {
            var cash = numeral().unformat($(this).text());
            totalCash = totalCash.add(cash);
        });
        
        var caisse_state = 'fermeture de caisse.';
        if(ACAISSE.cash_open == true)
		{
			caisse_state = 'ouverture de caisse. <br /><b>Attention, cela n\'inclut pas le chiffre d\'affaire du jour.</b>';
		}
        
        $('#cash_close_form #lastAmountTitle th, #cash_open_form #lastAmountTitle th').html('Les montants ci-dessous correspondent à ceux que vous avez saisis lors de la dernière '+caisse_state);
		
	}
	
	// @todo : remplacer par type 5 pour la preparation
	//quand on ferme la caisse
	
	if(ACAISSE.mode_debogue === true)
			{console.log('_refreshCashPanel() avec nb_states = '+nb_states);}
	
	//console.log(nb_states);		
	if(nb_states == 0)
	{
		$('#tabPrepareCash, #tabMvtCash, #tabCloseCash').addClass('unselectable');
		$('#tabOpenCash').removeClass('unselectable');
		$('#tabOpenCash').trigger('click');
		$('.cash_close_panel, .cash_prepare_panel').hide();
		$('.cash_open_panel').show();
		$('.preselectMoney').text('');
		$('#lastCa').hide();        	
	}
	else if (ACAISSE.cash_history[nb_states - 1].type == 2) //la caisse et fermé
	{
		ACAISSE.cash_open = false;
		$('.cash_open_panel, .cash_close_panel').hide();
		$('.cash_prepare_panel').show();
		
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos == true)
		{
			$('#tabMvtCash, #tabCloseCash').addClass('unselectable');
			$('#tabOpenCash, #tabPrintZList').removeClass('unselectable');
			$('#tabOpenCash').trigger('click');
			$('.cash_open_panel').show();
		}
		else
		{
			$('#tabOpenCash, #tabMvtCash, #tabCloseCash').addClass('unselectable');
			$('#tabPrepareCash, #tabPrintZList').removeClass('unselectable');
			$('#tabPrepareCash').trigger('click');
		}
		
		//on reporte le montant total à la fermeture dans la preparation
		$('#totalCashOnClose').val(function () {
			var result = nb_states==0?0:numeral(ACAISSE.cash_history[nb_states - 1].cash);
			result = result.divide(100);
			return result.format();
		});

		//on reporte le nombre de pièce therorique
		var totalCashTheoretical = numeral(0);
		$('[data-theoretical-cash]').each(function () {
			var dataTheoretical = ACAISSE.current_pointshop.datas[$(this).data('theoretical-cash')];
			$(this).val(dataTheoretical);
			totalCashTheoretical = totalCashTheoretical.add(dataTheoretical * $(this).closest('tr').data('money'));
		});
		$('#totalCashTheoretical').val(totalCashTheoretical.format());
		//console.log(totalCashTheoretical);

		//on reporte le nombre de pièce à la fermeture dans la preparation
		$('[data-cash-on-close]').each(function () {
			$(this).val(nb_states==0?0:ACAISSE.cash_history[nb_states - 1][$(this).data('cash-on-close')]);
		});

		//remplissage des boutons de présselection 
		$('tr[data-money]').each(function () {
			var result = parseInt($('[data-theoretical-cash]', this).val()) - parseInt($('[data-cash-on-close]', this).val());
			if (result < 0) {
				//result = result.replace('-', '');
				$('.takeOffMoney .preselectMoney', this).text(Math.abs(result));
			}
			else if (result > 0) {
				$('.requestMoney .preselectMoney', this).text(result);
			}
			//console.log(result);
		});
		//nombre total de pièce retiré/ajouté
		$('li.open .totalLineMoney input').each(function () {
			$(this).val(parseInt($('[data-cash-on-close]', $(this).closest('tr')).val()));
			var theoreticalCash = parseInt($('[data-theoretical-cash]', $(this).closest('tr')).val());
			_setProgressPrepare($(this), theoreticalCash);
		});

		//console.log('//@TODO : quand on ferme la caisse fermée');
		/*$('#payByCash').hide();
		$('#payByNoCash').show();*/
	}
	//quand on ouvre la caisse
	else if (ACAISSE.cash_history[nb_states - 1].type == 1 || ACAISSE.cash_history[nb_states - 1].type == 3) //pas status fermer en dernier
			//donc elle est ouverte ou encore ouverte suite à mouvement en cours de service
			{
				ACAISSE.cash_open = true;

				$('#tabOpenCash,#tabPrepareCash, #tabPrintZList').addClass('unselectable');
				$('#tabMvtCash, #tabCloseCash').removeClass('unselectable');
				$('#tabMvtCash').trigger('click');

				$('.cash_close_panel').show();
				$('.cash_prepare_panel').hide();
				$('#payByCash').show();
				$('#payByNoCash').hide();
				//$('#lastCa').show();
				
			}
	//quand on termine la preparation de la caisse
	else if (ACAISSE.cash_history[nb_states - 1].type == 5 || ACAISSE.cash_history[nb_states - 1].type == 6) {
		
		$('#tabPrepareCash, #tabMvtCash, #tabCloseCash').addClass('unselectable');
		
		$('#tabOpenCash,#tabPrintZList').removeClass('unselectable');
		$('#tabOpenCash').trigger('click');
		$('.cash_close_panel, .cash_prepare_panel').hide();
		$('.cash_open_panel').show();
		$('.preselectMoney').text('');
		$('#lastCa').hide();
	}
	
	$('.cash_action_right textarea').val('');
	$('#cash_001').trigger('keyup'); // Astuce pour mettre à jour le total
	
	_refreshVeilleInterface();
}

function _sendCashState(action, datas, noMessage) {
	//noMessage = ((typeof noMessage === "undefined") || false) ? confirm('Êtes-vous sur de vouloir .') : true;
	if (true) {
		$('.submitActionPanel input[type="submit"]').prepend('<i class="fa fa-spinner fa-spin"></i>');
		$.ajax({
			data: {
				'action': action,
				'id_pointshop': ACAISSE.id_pointshop,
				'count': datas,
			},
			type: 'post',
			dataType: 'json',
		}).done(function (response) {            	
			if (response.success == true)
			{
				if (action == 'ClosePointshop') {
					//printZList();
					alert('Vous pouvez à présent imprimer la bande Z du jour. Le rapport financier de la journée sera disponible en fin de journée.')
					
				}
				
				var html = response.html;
				response = response.response;
				
				// Since 2.5.1 : on imprime un justificatif de mouvement
				if (action == 'SaveCashMvt') {	
					printTicket(html, 1);
					printQueue();		
				}
				
				ACAISSE.cash_history = response.cash_history;

				$('#tab0 input,#tab1 input,#tab2 input,#tab3 input').not('.submitActionPanel input').val('');
				//_refreshCashPanel();
				$('.submitActionPanel input[type="submit"] i.fa-spinner').remove();
				//console.log(ACAISSE);
			}
			else
			{
				alert(['Erreur 01001 : ' + response.errorMsg,response]);
			}
			_reloadCashHistory();
		}).fail(function () {
			alert(['Erreur 01002 : Impossible de communiquer les informations au serveur',datas]);
			_reloadCashHistory();
		});
	}
}

function _searchLastCa(){
	$.ajax({
		type: 'post',
		data: {
			'action': 'SearchLastCa',
			'id_pointshop': ACAISSE.id_pointshop,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true){
			ACAISSE.lastCA = response.response.lastCa;
		}
		else{
			alert('Erreur 300121A : ' + response.errorMsg);
		}
	}).fail(function () {
		ACAISSE.lastCA = {"date":"0000-00-00 00:00:00","total_ca":0,"payment_method_count":0};
		alert('Erreur 300122A : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
	});
}

function _getEmployeesForThisPointshop(){
	$.ajax({
		type: 'post',
		data: {
			'action': 'EmployeesForThisPointshop',
			'id_pointshop': ACAISSE.id_pointshop,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true){
			ACAISSE.employeesPointshop = response.response.employeesPointshop;
			
			var html = '';
			var emp_active = null;
			for(var i = 0; i < ACAISSE.employeesPointshop.length; i++){
				if(ACAISSE.employeesPointshop[i].name != null)
				{
					var firstLetter = ACAISSE.employeesPointshop[i].name;
					var avatar = firstLetter.substring(0,1);
					
			        html +='<div class="emp emp_'+ ACAISSE.employeesPointshop[i].id_employee +'" data-id_emp="'+ ACAISSE.employeesPointshop[i].id_employee +'">';
						html +='<i class="avatar">'+ avatar +'</i>';
						html +='<span data-id_employee="'+ ACAISSE.employeesPointshop[i].id_employee +'" class="firstname">'+ ACAISSE.employeesPointshop[i].name+'</span>';
					html +='</div>';
					
					// On initialise le ticket avec l'id de l'employé par défaut qui est connecté
					if(ACAISSE.employeesPointshop[i].id_employee == ACAISSE.employee.id)
					{
						//console.log(ACAISSE.employeesPointshop[i].id_employee);
						emp_active = ACAISSE.employeesPointshop[i].id_employee;
					}
				}
		    }
			$('#veilleEmployee').append(html);
			
			//$('#veilleEmployee .emp_'+ emp_active).trigger('click');
			
		}
		else{
			alert('Erreur 000002L =P : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 300122B : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
	});
}

function _addIdEmployeeToTicket(){
	
	var id_employee = $('#showEmployee').data('id_employee');
	
	if(id_employee == undefined)
	{
		id_employee = ACAISSE.employee.id;
		$('#veilleEmployee .emp_'+ id_employee).trigger('click');
	}	
	
	$.ajax({
		type: 'post',
		data: {
			'action': 'addIdEmployeeToTicket',
			'id_ticket': ACAISSE.ticket.id,
			'id_employee': id_employee,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response.success == true){
			
		}
		else{
			alert('Erreur 000003L =P : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 300122C : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
	});
}

function _refreshLastCa(elem){
    //console.log(ACAISSE.lastCA);
    $(elem).empty();
    
    
    var $strLastCa = $('<span/>');
    
    for(var key in ACAISSE.lastCA) {
        if(ACAISSE.lastCA[key] != null) {
            if( ACAISSE.lastCA[key].hasOwnProperty('name')) {
                var $lastCaType = $('<span/>');
                $lastCaType.append(ACAISSE.lastCA[key].name + ' : ' + ACAISSE.lastCA[key].total + '€');
                $strLastCa.append($lastCaType);
            }  
        }
    }
    var $lastCaTotal = $('<span/>').append('Total CA : ' + ACAISSE.lastCA.total_ca+ '€');
    //var $lastCaTotalCash = $('<span/>').append('Total CA cash : ' + ACAISSE.lastCA.total_ca_cash+ '€');
    $strLastCa.append($lastCaTotal);//.append($lastCaTotalCash);
    $(elem).append($strLastCa);
}

function _refreshVeilleInterface()
{
	// CAISSE
    if(ACAISSE.cash_open == true)
    {
    	$('#veille .open_caisse').addClass('disabled');
    	$('#preOrderButtonsPanel .validTicket,#returnButtonsPanel .validateReturn, #returnButtonsPanel .validateRefund').removeClass('disabled');
    	sendNotif('greenFont','<i class="fa fa-check"></i> Connecté');
    	
    	// Indiquer la derniere oueerture de caisse #lastOpenCaisse (vert ou rouge)
	    var todayDayTicket = ACAISSE.ticket.day;
	    var todayDateTicket = ACAISSE.ticket.date_add;
	    if(todayDayTicket != undefined)
	    {
	    	var formatedDay = new Date(todayDayTicket).toLocaleDateString();;
	    	
	    	if(todayDayTicket.substring(0,10) != todayDateTicket.substring(0,10))
	    	{
	    		$('#lastOpenCaisse').addClass('alert').html('Attention : la caisse est ouverte depuis le '+formatedDay);
	    	}
	    	else
	    	{
	    		$('#lastOpenCaisse').removeClass('alert').html('Caisse ouverte');
	    	}
	    }
    }
    else
    {
  		$('#preOrderButtonsPanel .validTicket,#returnButtonsPanel .validateReturn, #returnButtonsPanel .validateRefund').addClass('disabled');
  		$('#veille .open_caisse').removeClass('disabled');
  		// On vérifie si la caisse est ouverte
  		if(ACAISSE.cash_open != undefined)
    		sendNotif('greenFont','Bonjour, veuillez ouvrir la caisse afin de réaliser une vente.');
    
    	$('#lastOpenCaisse').addClass('alert').html('CAISSE FERMEE');
    }
    	
    $('#tabMvtCash').hasClass('unselectable') == true?$('#veille .caisse_movements').addClass('disabled'):$('#veille .caisse_movements').removeClass('disabled')
    $('#tabCloseCash').hasClass('unselectable') == true?$('#veille .close_caisse').addClass('disabled'):$('#veille .close_caisse').removeClass('disabled');
    $('#tabPrepareCash').hasClass('unselectable') == true?$('#veille .prepare_caisse').addClass('disabled'):$('#veille .prepare_caisse').removeClass('disabled');
    
    if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos == true)
    	$('#veille .prepare_caisse').hide();
    // CLIENTS
    
    // COMPTABILITE
    ACAISSE.cash_open==false?$('#veille .print_z').removeClass('disabled'):$('#veille .print_z').addClass('disabled');
    ACAISSE.cash_open==false?$('#veille .show_cash').addClass('disabled'):$('#veille .show_cash').removeClass('disabled');
    
    
    // ON VERIFIE LES PERMISSIONS DU PROFIL EMPLOYE
    if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_search_product == 0)
    {
    	$('#veille .search_product').remove();
    }
    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_add_product  == 0) 
    {
    	$('#veille .create_product').remove();
    }
    
    // PRODUCTS
    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 0)
    {
    	$('#veille .view_catalogue').remove();
    }
    
    // REFUND
    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.active_refund == 0)
    {
    	$('#returnButtonsPanel .validateRefund').remove();
    }
}

function switchTicketCustomer(id_customer,id_group)
{
	$('#userName').addClass('loading');
       
   $.ajax({
       type: 'post',
       data: {
           'action': 'SwitchTicketCustomer',
           'id_ticket': ACAISSE.ticket.id,
           'id_new_customer': id_customer,
           'id_new_group': id_group,
           'ajaxRequestID': ++ACAISSE.ajaxCount,
       },
       dataType: 'json',
   }).done(function (response) {
       if (response.success == true)
       {
           var customer = response.response.new_customer;
           customer.groups = response.response.groups;
           customer.adress = response.response.address;
           ACAISSE.ticket = response.response.ticket;
           _switchCustomer(response.response.new_customer, false, response.response.additionnal_customer_informations);	
       }
       else
       {
           alert('Erreur 000655 : ' + response.errorMsg);
       }
       $('#userName').removeClass('loading');
   }).fail(function () {
       $('#userName').removeClass('loading');
       alert('Erreur 000654 : Une erreur est survenue. Si l\'erreur persiste veuillez contacter l\'adminstrateur du service');
   });
}

$(function() {
	
	//init des infos selon le points ente
	
	if(acaisse_pointshops.pointshops[id_pointshop].datas.activate_reduction_plan_listing == 1)
	{
		$('#userEditContainer ul.tab-bar li[data-page-id="pageUser2"]').show();
	}
	else
	{
		$('#userEditContainer ul.tab-bar li[data-page-id="pageUser2"]').hide();	
	}
	
	
    $('#fullscreen').on('click tap', function () { // plein ecran

		if (!document.fullscreenElement && !document.webkitFullscreenElement) {
		    if (document.documentElement.requestFullscreen) {
		      document.documentElement.requestFullscreen();
		      if (ACAISSE.till_width!=screen.width || ACAISSE.till_height!=screen.height) {
				ACAISSE.till_width = screen.width;
				ACAISSE.till_height = screen.height;
			  }
		    } else if (document.documentElement.webkitRequestFullscreen) {
		      document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
		      if (ACAISSE.till_width!=screen.width || ACAISSE.till_height!=screen.height) {
				ACAISSE.till_width = screen.width;
				ACAISSE.till_height = screen.height;
			  }
		    }
		} else {
		    if (document.cancelFullScreen) {
		      document.cancelFullScreen();
		      ACAISSE.till_width = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_width;
		      ACAISSE.till_height = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_heighth;
		    } else if (document.webkitCancelFullScreen) {
		      document.webkitCancelFullScreen();
		      ACAISSE.till_width = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_width;
		      ACAISSE.till_height = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_height;
		    }
		}
        
    });
    
    // PopupProduct Calto
    $('#showHideCalc').on('click tap', function (){
    	if($(this).hasClass('hide')){
	    	$('#productInfos #popupProduct, #productInfos .title, #features, #stocksPopup').animate({
										    width: "100%"
										  }, 200);
			$(this).removeClass('hide').addClass('show');							  
    	}
    	else
    	{
	    	$('#productInfos #popupProduct, #productInfos .title, #features, #stocksPopup').animate({
										    width: "+=262"
										  }, 200);
			$(this).addClass('hide').removeClass('show');
    	}
    });

    $('#calc-keyboard span').on('click tap', function (e) { // lors du clic sur les touches du clavier numerique
        if ($(this).parents('.disabled').length>0) {
        	return false;
        }
        
        e.preventDefault();
        e.stopPropagation();

        var c = $(this).html();
        
        _cashAddChar(c);
    });
    
    // 3 states rotation checkbox
    $('body').on('click tap','#customer-edit-group input[type=checkbox]',function() {
    	if (this.readOnly) {this.checked=this.readOnly=false;}
  		else if (!this.checked) {this.readOnly=this.indeterminate=true;}
    });
    
    // 2 states checkbox "classique"
    $('body').on('click tap','#customer-edit-more input[type=checkbox]',function() {
  		if (!this.checked) {this.checked=false} else {this.checked}
    });
    
    $('#customer-edit-save').on('click tap', function () { // lorsqu'on clic sur le boutton save lors de l'edition d'un client
        _editCustomer(false);
    });
    
     $('#customer-edit-save-and-log').on('click tap', function () { // lorsqu'on clic sur le boutton save lors de l'edition d'un client
        _editCustomer(true);
    });
    

    //switching customer / changement de client
    $('#customerListContainer').delegate('#customerSearchResults li,#defaultCustomersProfils li', 'click tap', function (e) {
        switchTicketCustomer($(this).data('id'),$(this).data('id_group'));
    });

    
    // lorsqu'on change  horizontalement de vu entre la liste des produits et le ticket
    $('#productList .switcher').on('click tap', function () {
    	closeAllPanel();
        if ($(this).hasClass('show-ticket')) {
            _gotoTicketView();
        } else {
            _gotoProductList();
            resetTileHeight();
        }
    });
    
    /**quand on clique longtemps sur un produit**/                        
    longPressOnProduct('#productList','td.cellType2,td.cellType3'); 
    longPressOnProduct('#searchProducts','td.cellType2,td.cellType3'); 
    longPressOnProduct('#ticketView', 'tr.line');
    
    //clic sur cellule de type page
    $('#productList').delegate('td.cellType1', 'click tap', function () {
        loadPage($(this).data('idobject'), null);
    });
    
    //annulation d'un ticket pour conversion vers commande prestashop
    $('#preOrderButtonsPanel .cancelTicket').on('click tap', function () {
        _cancelTicket();
        $('.ticketBox').slideUp();
    });

    //passage au ticket suivant
    $('.newTicket').on('click tap', function () {
    	 // On déroule les actions possibles
        $('.ticketBox').slideToggle();
    });
    
    
    // evenement des bouttons du ticket
    $('#ticketView .lines').delegate('.quantityPlus', 'click tap', function () {
        //faut recupere id_product et id_product_attribute dans le tr parent direct.
        var $parent = $(this).parent('td').parent('tr');
        var id_ticket_line = $parent.data('idticketline');   
        var id_product = $parent.data('id_product');   
        var id_product_attribute = $parent.data('id_product_attribute');     
        addToCart(id_product, id_product_attribute, 1, id_ticket_line);
    }).delegate('.quantityMinus', 'click tap', function () {
        //faut recupere id_product et id_product_attribute dans le tr parent direct.
        var $parent = $(this).parent('td').parent('tr');
        var id_ticket_line = $parent.data('idticketline');
        var id_product = $parent.data('id_product');   
        var id_product_attribute = $parent.data('id_product_attribute');  
        removeOneFromCart(id_product, id_product_attribute, 1, id_ticket_line); //@TODO : utiliser le id_ticket_line
    }).delegate('.quantityRemove', 'click tap', function () {
        //faut recupere id_product et id_product_attribute dans le tr parent direct.
        var $parent = $(this).parent('tr');
        var id_ticket_line = $parent.data('idticketline');
        var id_product = $parent.data('id_product');   
        var id_product_attribute = $parent.data('id_product_attribute');  
        trashLineFromCart(id_product, id_product_attribute, id_ticket_line);
    });

    

    /**********************
     *VERTICAL PANELS open/close action 
     */
    ACAISSE_vertical_panels = [
        {
            id: 'customerListContainer',
            zone: '#userName,#changeUserButton,#customerListContainer .horizontalCloseOverlay',
            openCallback: function () {
                ACAISSE.scanbarcodeMode = 'customer';
                $('#changeUserButton i').removeClass('fa-angle-down').addClass('fa-angle-up');
                //on vide le filtre et on refraichis la liste en consequence
                $('#customerSearchResultsFilter input').val('');
                sendCustomerSearch();
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
                $('#changeUserButton i').removeClass('fa-angle-up').addClass('fa-angle-down');
                $('#customerSearchResultsFilter input').focusout();
            }
        },
        {
            id: 'preOrderListContainer',
            zone: '#preOrderNumber,#preOrderListContainer .horizontalCloseOverlay,#changeOrderButton',
            openCallback: function () {
                PRESTATILL_CAISSE.preorder.openTab();
                $('#preorder_option_block .selected').trigger('click');
                $('#filterPreOrder input').val('');
            },
            closeCallback: function () {            	
                PRESTATILL_CAISSE.preorder.closeTab();
            }
        },
        {
            id: 'specialsContainer',
            zone: '#specials,#specialsContainer .horizontalCloseOverlay',
            openCallback: function () {
                ACAISSE.scanbarcodeMode = 'action';
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
            }
        },
        {
            id: 'userEditContainer',
            zone: '#editUserButton,#userEditContainer .horizontalCloseOverlay',
            openCallback: function () {
                ACAISSE.scanbarcodeMode = 'editCustomer';
                
                $('#discountContainer .discount .minimum_amount').each(function() {
	               if (this.innerHTML>$('#totalCost').html()) {
	               		$(this).addClass('notEnough');
	               } else {
	               		$(this).removeClass('notEnough');
	               }
               });
               
               $('#discountContainer .discount button:disabled').each(function() {
               		if ($(this).parent().find('.qte').html()!='0') {
               			this.disabled = false;
           			}
               });
               
               loadDiscount();
               loadOrders(0);
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
            }
        }
    ];

    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_search_ticket == 1) {
    	ACAISSE_vertical_panels.push({
            id: 'printTickets',
            zone: '#lastPrints, #printTickets .horizontalCloseOverlay',
            openCallback: function () {
                ACAISSE.scanbarcodeMode = 'ticket';
                
                document.querySelector('#searchTicketsDate').value=getEngDate(new Date(), false);
                document.querySelector('#searchTicketsDate').onchange();
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
            }
      });
    } else {
    	$('#lastPrints').css('display','none');
    }
    
	/*
	 *  Activer / Désactiver l'ajout de produit à la volée
	 */
   if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_add_product  == 0) {
    	$('#addNewProduct').css('display','none');
    }
    else
    {
    	$('#addNewProduct').css('display','block');
    }
    
	/*
	 *  Activer / Désactiver le paiement partiel
	 */   
	 if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.active_partial_paiement  == 0) {
    	$('#paymentView .partial-validation').css('display','none');
    }
    else
    {
    	$('#paymentView .partial-validation').css('display','block');
    }
    
    if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.activate_search_product == 1) {
        ACAISSE_vertical_panels.push({
            id: 'searchProducts',
            zone: '#productResearch, #searchProducts .horizontalCloseOverlay',
            openCallback: function () {
                //resetTileHeight();
                _gotoProductList();
                $('#productSearchResultsFilter input').val('');
                setTimeout(function() {
	                loadProductSearchList();
	                resetTileHeight();
	            },500);
                ACAISSE.scanbarcodeMode = 'productSearch';
            },
            closeCallback: function () {
                ACAISSE.scanbarcodeMode = 'product';
            }
        });
    } else {
    	$('#productResearch').css('display','none');
    }
    
    
    
    //panel d'ajout de nouveau produits
    if (true || acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.active_add_product_in_till == 1) {
        ACAISSE_vertical_panels.push({
            id: 'addNewProductContainer',
            zone: '#addNewProduct, #addNewProductContainer .horizontalCloseOverlay,#addNewProductContainer .closeBuPanel',
            openCallback: function () {
            	console.log('open add_new_prodict panel');
                ACAISSE.scanbarcodeMode = 'newProduct';
                resetTileHeight();
                
                addProductController.resetForm();
                
                setTimeout(function() {
	                resetTileHeight();
	            },500);
                
            },
            closeCallback: function () {
            	console.log('close add_new_prodict panel');
                ACAISSE.scanbarcodeMode = 'product';
            }
        });
    } else {
    	$('#addNewProduct').css('display','none');
    }
    
    

    //INIT PANEL EVENTS
    for (var i = 0; i < ACAISSE_vertical_panels.length; i++)
    {
        var panel = ACAISSE_vertical_panels[i];
        var instruction = '$(\'' + panel.zone + '\').on(\'click tap\',function(){ togglePanel(\'' + panel.id + '\');});';
        eval(instruction);
    }

    /********** END panel open/close actions *******/

    //reload preOrder : rechargement d'une autre pré commande
    $('#preOrderListContainer .left ul').delegate('li', 'click tap', function () {
        _switchToTicket($(this).data('id'));
        _gotoTicketView();
        
        // TEST LAURENT
        //_validateCurrentTicket();
    });

    //gestion du clavier numérique de sélection d'une commande
    $('#preOrderListContainer .keyboard span').on('click tap', function () {
    	if (!$(this).hasClass('display'))
        {
            if ($(this).hasClass('reset'))
            {
                $('#preOrderListContainer .keyboard span.display').html('');
                _refreshFilterPreOrderList('');
            }
            else if ($(this).hasClass('backspace'))
            {
                if ($('#preOrderListContainer .keyboard span.display').html().length > 0)
                {
                    $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html().substring(0, ($('#preOrderListContainer .keyboard span.display').html().length - 1)));
                }
            }
            else
            {
                $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html() + $(this).html());
            }
            _refreshFilterPreOrderList($('#preOrderListContainer .keyboard span.display').html());
        }
    });
    
    //gestion du clavier numérique de sélection d'une commande
    $('#printTickets .keyboard span').on('click tap', function () {
    	if (!$(this).hasClass('display'))
        {
            if ($(this).hasClass('reset'))
            {
                $('#printTickets .keyboard span.display').html('');
            }
            else if ($(this).hasClass('backspace'))
            {
                if ($('#printTickets .keyboard span.display').html().length > 0)
                {
                    $('#printTickets .keyboard span.display').html($('#printTickets .keyboard span.display').html().substring(0, ($('#printTickets .keyboard span.display').html().length - 1)));
                }
            }
            else
            {
                $('#printTickets .keyboard span.display').html($('#printTickets .keyboard span.display').html() + $(this).html());
            }
            //TODO 1 : recharger les tickets enf fonction de ce qui est entré
            getNumTickets($('#printTickets .keyboard span.display').html());
        }
    });

    $('.paymentButton').on('click tap', function () { // lorqu'on veut encaisser
        if ($(this).hasClass('disabled') || $(this).parents('.disabled').length>0) {
        	return false;
        }
        
        var mode = $(this).attr('id');
        
        //@todo rendre l'ouverture de caisse obligatoire
        if (mode=='payByCash' && !ACAISSE.cash_open) {
        	alert("Veuillez ouvrir la caisse pour pouvoir encaisser en liquide.");
        	return false;
        }

        //on désélectionne le mode actuelle
        if (ACAISSE.choosenPaymentMode != false) {
            $('#' + ACAISSE.choosenPaymentMode).removeClass('selected');
        }
        if (ACAISSE.choosenPaymentMode == mode) //on désactive le paiement actuellement sélectionné
        {
            $('#registerPayment').addClass('disabled');
            ACAISSE.choosenPaymentMode = false;
        }
        else //on change de mode de paiement
        {
            ACAISSE.choosenPaymentMode = mode;
            $('#' + ACAISSE.choosenPaymentMode).addClass('selected');


            if (ACAISSE.choosenPaymentMode == 'payByCash')
            {
                $('#registerPayment').addClass('disabled'); //en liquide il faut d'abord saisir le montant versée en liquide
                $('#cashMessage').html('');
                $('#cashReceived').html('-');
                _refreshCashOverdue(0);

                ACAISSE.scanbarcodeMode = 'cash';
            }
            else
            {
                $('#registerPayment').removeClass('disabled');
                ACAISSE.scanbarcodeMode = 'product';
            }
        }

    });


    /****************************************************************/   
   $('#pointshop-list').on('change', 'select', function (e) {
        //_initMainPage(this.value);
        localStorage.setItem('id_pointshop',this.value);
        PRESTATILL_CAISSE.loader.start();
        // PROVISOIRE : on recharge la page au changement de pointshop pour recharger caisse.js et les différentes permissions
        // (il faudrait que ce soit autre part que dans le JS je pense, genre direct PHP ou TPL)
        window.location.reload();
    });
    /****************************************************************/

    

    $('#cash_open_form').submit(function (e) {
        e.preventDefault();
        if(ACAISSE.mode_debogue === true)
			{console.log('open cash');}
        _sendCashState('OpenPointshop', $(this).serializeArray());
    });
    $('#cash_close_form').submit(function (e) {
        e.preventDefault();
        if(ACAISSE.mode_debogue === true)
			{console.log('close cash');}
        _sendCashState('ClosePointshop', $(this).serializeArray());
        
        console.log(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos);
        if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.retrieve_cash_jnfos == true){
        	
        	
        }
        
    });
    $('#cash_mvt_form').submit(function (e) {
        e.preventDefault();
        if(ACAISSE.mode_debogue === true)
			{console.log('mvt cash');}
        _sendCashState('SaveCashMvt', $(this).serializeArray());
    });
    $('#cash_prepare_form').submit(function (e) {
        e.preventDefault();
        if(ACAISSE.mode_debogue === true)
			{console.log('prepare cash');}
        //var fieldsRequest = [];
        var fieldsRequest = [];
        $('.requestMoney input', this).each(function () {
            fieldsRequest.push({
                name: $(this).attr('name'),
                value: $(this).val(),
            });
        });
        var fieldsTakeOff = [];
        $('.takeOffMoney input', this).each(function () {
            fieldsTakeOff.push({
                name: $(this).attr('name'),
                value: $(this).val(),
            });
        });

        fieldsRequest.push({
            name: $('textarea', this).attr('name'),
            value: $('textarea', this).val(),
        });
        fieldsTakeOff.push({
            name: $('textarea', this).attr('name'),
            value: $('textarea', this).val(),
        });
        if(ACAISSE.mode_debogue === true)
			{console.log(fieldsRequest);}

        _sendCashState('PreparePointShopTakeOff', fieldsTakeOff);
        _sendCashState('PreparePointShopRequest', fieldsRequest, true);
    });

	$('#specials').on('click tap',function(){
        //_searchLastCa();
        //_refreshLastCa('#specialsContainer #lastCa');
        _refreshCashPanel();
    });

    $('#cash_prepare_form .preselectMoney').on('click tap', function () {
        $(this).next('input').val($(this).text()).trigger('change');
    });

    $('#logo img').on('click tap', function () {
        /* loadPage(ACAISSE.current_pointshop.id_main_page, ACAISSE.id_pointshop);
        $('#footer .navActive').removeClass('navActive');
        $('#openRootPageCaisse').addClass('navActive'); */
       _refreshVeilleInterface();
       $('#veille').fadeToggle();
       $('#veilleEmployee').hide();
    });
    
    // pour fermer le popup quand on clic sur l'overlay
    $('body').delegate('.overlay', 'click tap', function() {
    	$(this).parent().find('.contentOverlay').remove(); // on degage les declinaisons
    	$(this).remove();// on  degage l'overlay
    });
    
    // pour fermer le popupNumeric quand on clic sur l'overlay
    $('body').delegate('.overlayNumeric', 'click tap', function() {
    	hidePopupAndKeyboard();
		$(this).remove();// on  degage l'overlay
    });
    
    // pour fermer le popup quand on clic sur fermer
    $('#productList').delegate('.closeOverlay', 'click tap', function() {
    	$('#productList .overlay').click();
    });
    
    // pour fermer le popup quand on clic sur fermer
    $('#ticketView').delegate('.closeOverlay', 'click tap', function() {
    	$('#ticketView .overlay').click();
    });
    
    // lorsqu'on clic sur un produit du popup
    $('#productList').delegate('.contentOverlay tr[class!="closeOverlay"]', 'click tap', function () {
        addToCart($(this).data('idobject'), $(this).data('idobject2'));
        if (params.declinaisonPopup.closeAfterSelect) {
        	$('.contentOverlay .closeOverlay').click();
        }
    });
    
    //on rajoute le clavier numerique
    longPressOnProduct('#productList', '.contentOverlay tr[class!="closeOverlay"]');
    
    //ajout produit au panier
    //clic sur cellule de type produit/Déclinaison
    $('#productList .content, #productResearchList').delegate('td.cellType2,td.cellType3', 'click tap', function () {
        if (!_loadProductFromId($(this).data('idobject')).disponibility_by_shop[ACAISSE.id_pointshop]) {
        	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i> Produit désactivé dans ce point de vente');
        } else if (_loadProductFromId($(this).data('idobject')).has_attributes && $(this).data('idobject2')=="") {
        	//closeAllPanel();
        	showPopupDeclinaison($(this).data('idobject'));
		} else {
			var qte = 1;
			
			var unitId = params.units.from.indexOf(_loadProductFromId($(this).data('idobject')).unity);
			
			if (unitId==-1) {
				console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
				params.units.from.push(p.unity);
				params.units.to.push('');
				params.units.scale.push(1);
				unitId = params.units.from.indexOf(_loadProductFromId($(this).data('idobject')).unity);
				//throw "Erreur d'unité : unité inconnue : ";
			}
			
			qte*=params.units.scale[unitId];
			//closeAllPanel();
			addToCart($(this).data('idobject'), $(this).data('idobject2'), qte);
		}
    });
    
    //clic sur une cellule de type categorie
    $('#productList .content').delegate('td.cellType4', 'click tap', function () {
        
	     if (ACAISSE.previousPageId.length>=32) {
	     	ACAISSE.previousPageId.shift();
	     }
        
        if ($('.navActive').attr('id')=='openRootCategorie') {
        	ACAISSE.previousPageId.push({id:$(this).data('idobject'),type:'categorie'});
        } else if (ACAISSE.previousPageId.length==0) {
        	ACAISSE.previousPageId.push({id:ACAISSE.current_id_page,type:'pageCaisse'});
        	ACAISSE.previousPageId.push({id:$(this).data('idobject'),type:'categorie'});
        } else {
        	ACAISSE.previousPageId.push({id:$(this).data('idobject'),type:'categorie'});
        }
        
        generateCategoriePageHTML($(this).data('idobject'));
        
    });
    
    $('#productList .content').delegate('td.cellTypeReturn', 'click tap', function () {
         
         if (ACAISSE.previousPageId.length>1) {
         	ACAISSE.previousPageId.pop();
         	var lastPage = ACAISSE.previousPageId[ACAISSE.previousPageId.length-1];
         	if (lastPage.type=='categorie') {
         		generateCategoriePageHTML(lastPage.id);
         	} else {
         		loadPage(lastPage.id);
         		$('#footer .navActive,#footer .open').removeClass('navActive').removeClass('open');
       			$('#openRootPageCaisse').addClass('navActive').addClass('open');
         	}
         }
         
    });
    
    $('#openRootCategorie').on('click tap', function() {
    	if (!$('#productList .switcher').hasClass('show-ticket')) {
            _gotoProductList();
       }
    	
    	ACAISSE.previousPageId = [{id:acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_root_category,type:'categorie'}];
        generateCategoriePageHTML(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.id_root_category);
        closeAllPanel();
        $('#footer .navActive,#footer .open').removeClass('open').removeClass('navActive');
        $(this).addClass('navActive').addClass('open');
    });
    
    $('#openRootPageCaisse').on('click tap', function() {
    	if (!$('#productList .switcher').hasClass('show-ticket')) {
            _gotoProductList();
        }
    	
    	ACAISSE.previousPageId = [];
    	
    	loadPage(ACAISSE.current_pointshop.id_main_page);
    	
    	closeAllPanel();
        $('#footer .navActive,#footer .open').removeClass('navActive').removeClass('open');
        $(this).addClass('navActive').addClass('open');
    });
    
    // lorsqu'on clic sur un ticket de caisse
    $('#printTickets').delegate('.left table tr','click tap',function() {
		$('#printTickets .display').html($(this).data('num'));
		getNumTickets($(this).data('num'));
	});
	
	//lorsqu'on veut imprimer un ticket de caisse
	$('#printTickets').delegate('.left .ticket .print','click tap',function() {
		$('#printTickets .left .ticket.toPrint .kdoprint').show(); //DEPRECIER A PARTIR DE 2.3
		$('#printTickets .left .ticket.toPrint .kdotitle').hide(); //DEPRECIER A PARTIR DE 2.3
		printTicket($('#printTickets .left .ticket.toPrint').html(), 1, 'duplicata'); //duplicata == la class CSS qui va aller s'ajouter sur #ticketPrint
		printQueue();
	});
	
	//lorsqu'on veut imprimer un ticket de caisse
	$('#printTickets').delegate('.left .ticket .kdo','click tap',function() {
		$('#printTickets .left .ticket.toPrint .kdoprint').hide(); //DEPRECIER A PARTIR DE 2.3
		$('#printTickets .left .ticket.toPrint .kdotitle').show(); //DEPRECIER A PARTIR DE 2.3
		printTicket($('#printTickets .left .ticket.toPrint').html(), 1, 'gift'); //duplicata == la class CSS qui va aller s'ajouter sur #ticketPrint
		printQueue();
		
	});
	
	$('#notif').delegate('td','click tap',function(e) {
		e.stopPropagation();
		e.preventDefault();
		$('#notif').removeClass('active');
		$('#notif table')
		.fadeOut()
		.queue(function (next) { 
		   	$(this)
		   		.find('td')
				.removeClass()
				.html('');
			next();
		});
	});
	
	$('#tabPrintZList').on('click tap',function(e) {
		
		if ($(this).hasClass('unselectable')) 
		{
			e.preventDefault();
		}
		else
		{
			printZList();
		}
	});
	
	$('#printTickets').on('click tap', '.rePrintZ', function(e) {
		
		if ($(this).hasClass('disabled')) 
		{
			e.preventDefault();
		}
		else
		{
			var dateen = $('#searchTicketsDate').val();
			console.log(dateen);
			printZList(dateen);
		}
	});
	
	window.addEventListener('online',  function() {
		sendNotif('greenFont','<i class="fa fa-check"></i>Connecté');
	});
	
	window.addEventListener('offline', function() {
		sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Déconnecté');
	});
	
	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image=='0') {
		$('.lines th.img').remove();
	}
	
	// action sur les bouttons de paiements multiple (moyen de paiements)
	$('body').on('click tap','#multi-payment-mode-list .button',function(elt) {
		
		if($('#multi-payments-popup').prop('disabled') == true) {
     		alert('Veuillez attendre la finalisation de l\'encaissement précédent');		 
		} else if (!$(this).hasClass('disabled')) {
			if ($(this).data('moduleName') == undefined) { return false;}
  		
	  		var amount = $('input#multi-payment-amount').val();
	  		var moduleName = this.dataset.moduleName;
	  		var paymentModuleLabel = this.textContent.trim();
	  		
	  		registerPaymentPart(amount, moduleName, paymentModuleLabel);
	  		
	  		$('input#multi-payment-amount').val(0);
		}
	});
	
	$('#userEditContainer .tab-bar .tab-button').on('click tap',function() {
		$('#userEditContainer .content > div').css('display','none');
		$('#userEditContainer #'+$(this).data('pageId')).css('display','block');
	});
	
	var lastUserOrderScroll = new Date().getTime();
	$('#userEditContainer #pageUser3').scroll(function() {
		
		var datas  = {};
		
		$('#order_filter [id^="filter-"]').each(function () {
	        datas[$(this).attr('id').replace('filter-','')] = $(this).val();
	    });
		
		if (new Date().getTime()-lastUserOrderScroll<1000 || $('#pageUser3 table').data('nbOrder')==0 || this.scrollHeight-this.scrollTop-$(this).height()>200) {return false;}
		
		lastUserOrderScroll = new Date().getTime();
		
		var nb_order = $('#pageUser3 table').data('nb_order');
		
		if (nb_order==undefined || nb_order==0) {
			nb_order=50;
		}
		
    	$('#pageUser3 table').data('nb_order',0);
		
		$('#pageUser3').append('<div><i class="fa fa-spinner fa-spin"></i></div>');
    
	    $.ajax({
	        data: {
	            'action': 'LoadCustomerOrders',
	            'id_customer': ACAISSE.customer.id,
	            'from_nb_order':nb_order,
	            'id_pointshop': ACAISSE.id_pointshop,
	            'filter': datas,
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        var response = response.response;
	        if (response.success != false) {
	            var orders=response.orders;
	            
	            var html='';
	            
				for (var id in orders) {
	            	html += '<tr>'
	            		html += '<td>'+orders[id].id_order+'</td>';
	            		if (acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
							html += '<td>' + orders[id].id_ticket + '</td>';
						}
						else
						{
							html += '<td>--</td>';
						}
	            		if(acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined)
						{
							html += '<td><span class="pointshop_name">' + acaisse_pointshops.pointshops[orders[id].id_pointshop].name + '</span></td>';
						}
						else
						{
							html += '<td><span class="pointshop_name">En ligne</span></td>';
						}
	            		html += '<td><span class="date">'+Date.toString(orders[id].date_add)+'</span></td>';
	            		html += '<td><span class="state">'+orders[id].order_state+'</span></td>';
	            		html += '<td><span class="paid '+ ((parseFloat(formatPrice(orders[id].total_paid))>parseFloat(formatPrice(orders[id].total_paid_real)))?'notAllPaid':'')+' '+((parseFloat(formatPrice(orders[id].total_paid))<parseFloat(formatPrice(orders[id].total_paid_real)))?'tooMuch':'')+'">'+formatPrice(orders[id].total_paid)+'€</span></td>';
	        			html += '<td><span class="paid_real '+ ((parseFloat(formatPrice(orders[id].total_paid))>parseFloat(formatPrice(orders[id].total_paid_real)))?'notAllPaid':'')+' '+((parseFloat(formatPrice(orders[id].total_paid))<parseFloat(formatPrice(orders[id].total_paid_real)))?'tooMuch':'')+'">'+formatPrice(orders[id].total_paid_real)+'€</span></td>';
	        			
	        			html +='<td class="actions">';
	        			if (parseFloat(orders[id].total_paid_real) < parseFloat(orders[id].total_paid) && response.id_order_state_cancelled != orders[id].id_order_state && acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
							html += '<button onclick="payRestOfOrder(' + orders[id].id_order + ');"><i class="fa fa-money" aria-hidden="true"></i> <span class="btLabel">PAYER</span></button>';
						} else if (parseFloat(orders[id].total_paid_real) > parseFloat(orders[id].total_paid) && response.id_order_state_cancelled != orders[id].id_order_state) {
							/**@todo : Se démerder pour degager le surplut et donner une reduc ?*/
							//html += '<button disabled="disabled" style="opacity:0.2;"><i class="fa fa-undo"></i> <span class="btLabel">AVOIR</span></button>';
						}
						if(acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined)
						{
							html += '<button class="seeTicket" data-id="' + orders[id].id_ticket + '"><i class="fa fa-ticket"></i> <span class="btLabel">TICKET</span></button>';
						}
						else
						{
							html += '<button disabled="disabled" style="opacity:0.2" data-id="0"><i class="fa fa-ticket"></i> <span class="btLabel">TICKET</span></button>';
						}
						/*
						if (params.orders.acaisse_invoice_is_active == 1 && orders[id].valid == 1 && orders[id].invoice_number > 0) {
							html += '<button  class="seeInvoice" data-id="' + orders[id].id_order + '"><i class="fa fa-file-text"></i> <span class="btLabel">FACTURE</span></button>';
						}
						else
						{
							html += '<button disabled="disabled" style="opacity:0.2"  class="seeInvoice" data-id="0"><i class="fa fa-file-text"></i> <span class="btLabel">FACTURE</span></button>';
						}*/
						html += '<button  class="openOrder" data-id="' + orders[id].id_order + '"><i class="fa fa-eye"></i> <span class="btLabel">VOIR</span></button>';
						html += '</td></tr>';
	            }
	            
	            $('#pageUser3 table').append(html);
	            $('#pageUser3 .fa-spinner').remove();
	            
	            $('#pageUser3 table').data('nb_order',(parseInt(nb_order)+50));
	        } else {
	        	alert('Erreur k_00011 : impossible d\'obtenir la liste des commandes du client. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	        }
	    }).fail(function () {
	        alert('Erreur k00011 : impossible d\'obtenir la liste des commandes du client. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	    });
	});
	
	$('#userEditContainer ul .open').click();
	
	$('.ticketBox').hide();

	
	//INIT UI
    $('#productList .content').append('<br /><i style="font-size:300%;" class="fa fa-spinner fa-spin"></i>');
    
    setTimeout(function () {
        _initDefaultProfilesList();
        _switchCustomer(acaisse_profiles[acaisse_profilesIDs[0]], true, []);
        _initMainPage();
        _gotoProductList();
        //@TODO reactive le bloc ci dessous en production
		
        $('*').attr('unselectable', 'on')
                .css({'-moz-user-select': '-moz-none',
                    '-moz-user-select':'none',
                    '-o-user-select': 'none',
                    '-khtml-user-select': 'none', // you could also put this in a class
                    '-webkit-user-select': 'none', // and add the CSS class here instead 
                    '-ms-user-select': 'none',
                    'user-select': 'none'
                }).bind('selectstart', function (e) {
            e.preventDefault();
            return false;
        });
    }, 1);
    
    
    
    //on active le troisieme stade de l'input pour la légende du groupe par défaut du client
    var editCustomerDefaultGroupLegend = $('#editCustomerDefaultGroupLegend')[0];
    editCustomerDefaultGroupLegend.readOnly=editCustomerDefaultGroupLegend.indeterminate=true;
    
    $('#printTicket').css('font-size',localStorage.getItem('ticket_font_size'));
    
    // ECRAN D'ACCUEIL ET VEILLE DE PRESTATILL
    $('#veille .closeBt').on('click tap', function(){
    	$('#veille').fadeOut();
    });
    
    // Interface d'identification 
   	$('#veilleEmployee').on('click tap', '.emp', function(){
   		
   		$('#showEmployee i').css('backgroundColor', $(this).find('i').css('backgroundColor'));
   		$('#showEmployee i').text($(this).find('i').html());
   		$('#showEmployee ').data('id_employee', $(this).find('span').data('id_employee'));
   		
   		if(ACAISSE.veilleAllwaysClosed == 1)
   		{
   			$('#veille, #veilleEmployee').fadeOut();
   		}
   		else
   		{
   			$('#veilleEmployee').fadeOut();
   		}
   		
   		ACAISSE.veilleAllwaysClosed = 1;
   		_addIdEmployeeToTicket();
   		
   		// On met à jour les tickets en attente
   		$('#preorder_option_block .selected').trigger('click');
   		$('#preorder_option_block .item[data-value=0] span').html($('#showEmployee').html());
   		
		PRESTATILL_CAISSE.reInit();   	
   	});
    
    $('#veilleContent .firstname').html(ACAISSE.employee.firstname);
    
    $('#veille li li').on('click tap', function(){
    	var action = $(this).attr('class');
    	
    	if($(this).hasClass('disabled') == false)
    	{
    		$('#veille').fadeOut();
    		
	    	setTimeout(function(){
	    		
	    		closeAllPanel();
	    		
			    switch (action)
		    	{
		    		// CAISSE
		    		case 'open_caisse':
		    		$('#specials').trigger('click');
		    		break;
		    		
		    		case 'prepare_caisse':
		    		if($('#specials').hasClass('navActive') == false)
		    			$('#specials').trigger('click');
		    		$('#tabPrepareCash').trigger('click');
		    		break;
		    		
		    		case 'caisse_movements':
		    		if($('#specials').hasClass('navActive') == false)
		    			$('#specials').trigger('click');
		    		$('#tabMvtCash').trigger('click');
		    		break;
		    		
		    		case 'close_caisse':
		    		if($('#specials').hasClass('navActive') == false)
		    			$('#specials').trigger('click');		    			
		    		$('#tabCloseCash').trigger('click');
		    		break;
		    		
		    		// CUSTOMER
		    		case 'create_customer':
		    		if(acaisse_profilesIDs.indexOf(""+ACAISSE.customer.id) > -1)
					{
						if($('#editUserButton').hasClass('navActive') == false)
							$('#editUserButton').trigger('click').delay(500);
		    			$('li[data-page-id=pageUser1]').trigger('click');
		    			sendNotif('greenFont','Il suffit de saisir un nom, prénom et mail pour créer un nouveau client.');
					}
					else
					{
						sendNotif('redFont','Veuillez sélectionner le client par défaut');
					}
		    		break;
		    		
		    		case 'find_customer':
						if($('#userName').hasClass('navActive') == false)
							$('#userName').trigger('click');
		    			sendNotif('greenFont','Tapez nom, prénom ou ID du client ou douchez sa carte de fidélité pour l\'identifier.');
		    		break;
		    		
		    		case 'see_orders':
						if($('#editUserButton').hasClass('navActive') == false)
							$('#editUserButton').trigger('click');
		    			$('li[data-page-id=pageUser3]').trigger('click');
		    		break;
		    		
		    		case 'see_vouchers':
						if($('#editUserButton').hasClass('navActive') == false)
							$('#editUserButton').trigger('click');
		    			$('li[data-page-id=pageUser2]').trigger('click');
		    		break;
		    		
		    		// PRODUCTS
		    		case 'create_product':
		    		$('#addNewProduct').trigger('click');
		    		break;
		    		
		    		case 'search_product':
		    		$('#productResearch').trigger('click');
		    		break;
		    		
		    		case 'view_catalogue':
		    		$('#openRootCategorie').trigger('click');
		    		break;
		    		
		    		// COMPTA
		    		case 'print_z':
		    		$('#specials').trigger('click');
		    		$('#tabPrintZList').trigger('click');
		    		break;
		    		
		    		case 'find_z':
		    		$('#lastPrints').trigger('click');
		    		sendNotif('greenFont','Utilisez le calendrier pour indiquer le jour pour lequel vous souhaitez réimprimer la bande Z.');
		    		break;
		    		
		    		case 'show_cash':
					if($('#editUserButton').hasClass('navActive') == false)
	    				$('#specials').trigger('click');
		    			$('li[data-module-name=prestatillexportcompta]').trigger('click');
		    		break;
		    		
		    		// TICKET
		    		case 'new_ticket':
		    		if($('.switcher').hasClass('show-product-list') == false)
		    			$('#productList').trigger('click');
		    			$('.newTicket').trigger('click');
		    		break;
		    		
		    		case 'find_ticket':
		    			$('#lastPrints').trigger('click');
		    		break;
		    		
		    		case 'show_open_tickets':
		    			$('#preOrderNumber').trigger('click');
		    		break;
		    		
		    	}
	    		
	    	},500);
	    	
	    	
    	}
    	
    });
    
    setTimeout(function(){ $('#veilleBox').fadeIn(); }, 1000);
    
    date_heure('hourOfTheDay');
    
    /* SHOW EMPLOYEE */
   	$('#showPointshops').on('click tap', function(){
   		$(this).toggleClass('active');
   		$('#pointshop-list').slideToggle("fast");
   	});
   	
   	$('#showEmployee').on('click tap', function(){
   		$('#veille').fadeIn();
   		$('#veilleEmployee').fadeIn();
   	});
   	
   	$('#footerSearchBar i.fa-keyboard-o').on('click tap', function(){
   		$('#productKeyboardResearch, #footerSearchBar i.fa-keyboard-o').toggleClass('active');
   		//$('#productKeyboardResearch').slideToggle("fast");
   		
   		if($('#productKeyboardResearch').hasClass('active'))
   		{
   			$('#globalSearchBox').css('height', 'calc( 100% - 390px )');
   		}
   		else
   		{
   			$('#globalSearchBox').css('height', '100%');
   		}
   		
   	});
    
});
/**********************
 * GEstionnaire de l'onglet extra/option du module de caisse 
 */
$(function(){
	
	$('.tab-button').on('click tap',function(e){
        $tab=$('#'+$(this).data('tab-id'));
        if($(this).hasClass('unselectable')){
        	e.preventDefault();
            $('.alert_message', $(this)).fadeIn();
            $('.alert_message', $(this)).delay(1000).fadeOut();
            return;
        }
        else{
			$tab=$('#'+$(this).data('tab-id'));
			$('li',$tab.parent()).removeClass('open');
			$tab.addClass('open');
			$('.tab-button',$(this).parent()).removeClass('open');
			$(this).addClass('open');
        }
	});
});

$(function(){
	
	$('#specialsContainer ul.tab-bar').on('tap click','li[data-module-name]',function(){
		console.log($(this).data('moduleName'));
		
		//ajax
		$.ajax({
	        type: 'post',
	        data: {
	          'action': 'GetSpecialModuleView',
	          'module_name': $(this).data('moduleName'),
	          'id_pointshop': ACAISSE.id_pointshop,
	          'id_customer': ACAISSE.customer.id,
	          'id_group': ACAISSE.customer.id_default_group,
	          'id_force_group': ACAISSE.id_force_group,
	          'ajaxRequestID': ++ACAISSE.ajaxCount,
	        },
	        dataType: 'json',
	    }).done(function (response) {
	    	console.log('response')
	        if (response.success != false) {
	        	$('#specialsContainer .tab-content li').removeClass('open');	        	
	        	$('#specialsContainer .tab-content #tab-specials-modules-container').html(response.response.html).addClass('open');
	        }
	        else
	        {
	        	
	        }
	    }).fail(function () { 
	        alert('Erreur o00079 : impossible de communiquer avec le serveur');
	        
	      $('#details-paiements tr.loader').remove();
	      $('#multi-payments-popup').prop('disabled',false);
	    });
		
		
	});
	
});

/**
 * Travail avec une structure suivante
 *   -  <= en ::before
 *   <input name="...." class="ui-spinner" data-min="" data-max="" [data-increment=""] [data-double-increment=""] [data-allow-press="false"]/>
 * 	 +  <= en ::after
 */


// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

// IMPORTANT remember to change every instance of "uiSpinner" to the name of your plugin!
;(function($) {

    // here we go!
    $.uiSpinner = function(element, options) {
    	
    	var DEBUG_LEVEL = 0;
    	
        var defaults = {
        	align_button : 'right', // 'both-side', 'left' or 'right'
        	'step' : 1,
        	'fast_step' : 10, //when staying press on button
        	'decimal' : false, //only int
        	'min' : null,	//son automatiquement initialiser par $element.data('min')
        	'max' : null,	//son automatiquement initialiser par $element.data('min')
        	'onChange' : function(plugin,$element,value) {},
        	'filter_value' : function(value) { return value==0?'':value; },
			'keyboardInput' : true,
        };

        var plugin = this;
        plugin.settings = {};

        var $element = $(element),  // reference to the jQuery version of DOM element the plugin is attached to
             element = element;        // reference to the actual DOM element
             
        var $minus = $('<span class="ui-spinner-minus">-</span>'),
        	$plus = $('<span class="ui-spinner-plus">+</span>'),
        	$input,
        	value=0;             

        // the "constructor" method that gets called when the object is created
        plugin.init = function() {

            // the plugin's final properties are the merged default and user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

			//init des instances jquery
			$input = $('input',$element);
			$input.prop('readonly',false /*!plugin.settings.keyboardInput*/);
			plugin.settings.min = $element.data('min')!=undefined?$element.data('min'):null;
			plugin.settings.max = $element.data('max')!=undefined?$element.data('max'):null;
			
			value = plugin.settings.decimal === false?parseInt('0'+$input.val()):parseFloat('0'+$input.val());
			
			
			switch(plugin.settings.align_button)
			{
				case 'left':
					$element.prepend($minus);
					$element.prepend($plus);
					break;
				case 'both-side':
					$element.prepend($minus);
					$element.append($plus);
					break;		
				case 'right':
				default:
					$element.append($minus);
					$element.append($plus);
					break;			
			}

            //mise en place des callback sur les bouttons
            $minus.click(function(e){ _decrement(); });
            $minus.mousedown(function(e){ _continious_decrement(); });
            $minus.mouseup(function(e){ _stop_continious_decrement(); });
            
            $plus.click(function(e){ _increment(); });
            $plus.mousedown(function(e){ _continious_increment(); });
            $plus.mouseup(function(e){ _stop_continious_increment(); });
            
            
        
        	$input.keypress(function(e){ 	(e); });
        	$input.keydown(function(e){ _filterKeyDown(e); });
            
        };

        // public methods
        // these methods can be called like:
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // element.data('uiSpinner').publicMethod(arg1, arg2, ... argn) from outside the plugin, where "element"
        // is the element the plugin is attached to;
        plugin.setValue = function(new_value) {
            value = plugin.settings.filter_value(new_value);
            $input
            	.val(value)
            	.change(); //trigger "change" event
            
            //appel du callback de change
        	plugin.settings.onChange(plugin,$element,value);            	
            
        };
        plugin.setMin = function(new_min) {
            plugin.settings.min = new_min;
            if(value<new_min)
            {
            	plugin.setValue(new_min);
            }
        };
        plugin.setMax = function(new_max) {
            plugin.settings.max = new_max;
            if(value>new_max	)
            {
            	plugin.setValue(new_max);
            }
        };

        // private methods
        // these methods can be called only from inside the plugin like:
        // methodName(arg1, arg2, ... argn)
        
        
        var _filterKeyPress = function(e) {        	
        	if(!(
				e.key == 0
				|| e.key == 1
				|| e.key == 2
				|| e.key == 3
				|| e.key == 4
				|| e.key == 5
				|| e.key == 6
				|| e.key == 7
				|| e.key == 8
				|| e.key == 9				
				|| (
					plugin.settings.decimal == true && (						
						e.key == '.'
						|| e.key == ','						
					) 
				)		
			))
			{
				e.preventDefault();			
			}
			else
			{
				value = plugin.settings.decimal === false?parseInt('0'+$input.val()):parseFloat('0'+$input.val());
			}
    	};
		
    	var _filterKeyDown = function(e) {
        	switch(e.keyCode)
        	{
        		case 40: //Down
					e.preventDefault();
        			_decrement();
        			break;
        		case 38: //up
					e.preventDefault();
        			_decrement();
        			break;
        	}
    	};
        

        // a private method. for demonstration purposes only - remove it!
        var _decrement = function(step) {
        	if(step == undefined)
        	{
        		step = plugin.settings.step;
			}
			
			if(DEBUG_LEVEL>6)
			{
				console.log({
					'_decrement' : '',
					'this.value' : value,
					'step' : step,
					'new value' : value-step,
				});
			}
			value = plugin.settings.decimal === false?parseInt('0'+$input.val()):parseFloat('0'+$input.val());
			if(plugin.settings.min !== null && value <= plugin.settings.min)
			{
				return;
			}
			value -= step;
			plugin.setValue(value);
        };
        var _increment = function(step) {
        	if(step == undefined)
        	{
        		step = plugin.settings.step;
        	}
        	
			if(DEBUG_LEVEL>6)
			{
				console.log({
					'_increment' : '',
					'this.value' : value,
					'step' : step,
					'new value' : value+step,
					'max' : plugin.settings.max,
				});
			}
        	value = plugin.settings.decimal === false?parseInt('0'+$input.val()):parseFloat('0'+$input.val());
        	
        	if(plugin.settings.max !== null && value >= plugin.settings.max)
			{
				return;
			}
        	
        	value += step;
			plugin.setValue(value);
        };
        
        var continious_increment_id = null;
        var continious_increment_count = null;
        var _continious_increment = function(step) {
        	continious_increment_id = setInterval(function(){
        		continious_increment_count++;
        		if(continious_increment_count > 10)
        		{        			
        			_increment(plugin.settings.fast_step);
        		}
        		else
        		{
        			_increment(plugin.settings.step);
        		}
        	},200); 	
        };
        
        var _stop_continious_increment = function() {
        	
        	if(continious_increment_id !== null)
        	{
        		clearInterval(continious_increment_id);
        		continious_increment_id = null;
        	}
        	        	
        };
        
        
        
        
        var continious_decrement_id = null;
        var continious_decrement_count = null;
        var _continious_decrement = function(step) {
        	continious_decrement_id = setInterval(function(){
        		continious_decrement_count++;
        		if(continious_decrement_count > 10)
        		{        			
        			_decrement(plugin.settings.fast_step);
        		}
        		else
        		{
        			_decrement(plugin.settings.step);
        		}
        	},200); 	
        };
        
        var _stop_continious_decrement = function() {
        	
        	if(continious_decrement_id !== null)
        	{
        		clearInterval(continious_decrement_id);
        		continious_decrement_id = null;
        	}
        	        	
        };
        

        // fire up the plugin!
        // call the "constructor" method
        plugin.init();
        //plugin.setValue(value);

    };

    // add the plugin to the jQuery.fn object
    $.fn.uiSpinner = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('uiSpinner')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.uiSpinner(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('uiSpinner').publicMethod(arg1, arg2, ... argn) or
                // element.data('uiSpinner').settings.propertyName
                $(this).data('uiSpinner', plugin);

            }

        });

    };

})(jQuery);
/**
 * Travail avec une structure suivante
 *   -  <= en ::before
 *   <input name="...." class="ui-spinner" data-min="" data-max="" [data-increment=""] [data-double-increment=""] [data-allow-press="false"]/>
 * 	 +  <= en ::after
 */


// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

// IMPORTANT remember to change every instance of "uiTab" to the name of your plugin!
;(function($) {

    // here we go!
    $.uiTab = function(element, options) {
    	
    	var DEBUG_LEVEL = 0;
    	
        var defaults = {
        	'direction' : 'horizontal', // 'horizontal' or 'vertical' (not implemented)
        	'onChange' : function(plugin,target,$target) {},
        };

        var plugin = this;
        plugin.settings = {};

        var $element = $(element),  // reference to the jQuery version of DOM element the plugin is attached to
             element = element;        // reference to the actual DOM element
             
        var $menu, $content, $barcode_input;             

        // the "constructor" method that gets called when the object is created
        plugin.init = function() {

            // the plugin's final properties are the merged default and user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

			//init des instances jquery
			$menu = $('.ui-tab-menu',$element);
			$content = $('.ui-tab-content',$element);
			
			$menu.on('click tap','li',function(){
				
				var target = $(this).data('target');
				if(target == null || target == '')
				{
					alert('You need to define a data-target="selector" on this li');
				}				
				var $target = $(target);
				
				if($target.length == 0)
				{
					alert('No target correspondance for "'+target+'" selector.');
				}
				
				$('.current',$element).removeClass('current');
				$target.addClass('current');
				$(this).addClass('current');
				
				plugin.settings.onChange(this,$(this).data('target'),$target);								
			});		
			
			            
        };

        // public methods
        // these methods can be called like:
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // element.data('uiTab').publicMethod(arg1, arg2, ... argn) from outside the plugin, where "element"
        // is the element the plugin is attached to;
        plugin.setValue = function(new_value) {
            value = plugin.settings.filter_value(new_value);
            $input
            	.val(value)
            	.change(); //trigger "change" event
            
            //appel du callback de change
        	plugin.settings.onChange(plugin,$element,value);            	
            
        };

        // private methods
        // these methods can be called only from inside the plugin like:
        // methodName(arg1, arg2, ... argn)
        
        var _toto = function() {
        	
        };
        

        // fire up the plugin!
        // call the "constructor" method
        plugin.init();
        //plugin.setValue(value);

    };

    // add the plugin to the jQuery.fn object
    $.fn.uiTab = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('uiTab')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.uiTab(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('uiTab').publicMethod(arg1, arg2, ... argn) or
                // element.data('uiTab').settings.propertyName
                $(this).data('uiTab', plugin);

            }

        });

    };

})(jQuery);
// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

// IMPORTANT remember to change every instance of "uiSelector" to the name of your plugin!
;(function($) {

    // here we go!
    $.uiSelector = function(element, options) {

        // plugin's default options
        // this is private property and is  accessible only from inside the plugin
        var defaults = {
        	multiple:false,
        	onChange:function(new_values) {}
        };

        // to avoid confusions, use "plugin" to reference the current instance of the object
        var plugin = this;

        // this will hold the merged default, and user-provided options
        // plugin's properties will be available through this object like:
        // plugin.settings.propertyName from inside the plugin or
        // element.data('uiSelector').settings.propertyName from outside the plugin, where "element" is the
        // element the plugin is attached to;
        plugin.settings = {};

        var $element = $(element),  // reference to the jQuery version of DOM element the plugin is attached to
             element = element,
             $value = $('input'),
             _value = '';

        // the "constructor" method that gets called when the object is created
        plugin.init = function() {

            // the plugin's final properties are the merged default and user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

            $value = plugin.settings.input == undefined ? $('input',$element) : plugin.settings.input;            
            
            $element.addClass('ui-selector-container');
            
            $element.on('click tap','.item',function(e){
            	
            	if(false && $(this).is('.disabled'))
            	{
            		return;
            	}
            	
            	if(plugin.settings.multiple === false)
            	{
	            	$('.item.selected',$element).removeClass('selected');
	            	$(this).addClass('selected');	       
            	}
            	else
            	{
            		$(this).toggleClass('selected');          		
            	}  
            	_refreshValue();          	           	
            });
            
            $('.item[data-value="'+$value.val()+'"]',$element).click();
        };

    
	    var _refreshValue = function() {    	
			var tmp = [];
			
			$.each($('.item.selected',$element),function(i,elt){
				tmp.push($(this).data('value'));
			});
			_value = tmp.join(';');
			$value.val(_value);
			//console.log(_value);
			plugin.settings.onChange(_value);
	    };


        // fire up the plugin!
        // call the "constructor" method
        plugin.init();

    };

    // add the plugin to the jQuery.fn object
    $.fn.uiSelector = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('uiSelector')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.uiSelector(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('uiSelector').publicMethod(arg1, arg2, ... argn) or
                // element.data('uiSelector').settings.propertyName
                $(this).data('uiSelector', plugin);

            }

        });

    };

})(jQuery);
/*!
 * html2canvas 1.0.0-alpha.11 <https://html2canvas.hertzen.com>
 * Copyright (c) 2018 Niklas von Hertzen <https://hertzen.com>
 * Released under MIT License
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["html2canvas"] = factory();
	else
		root["html2canvas"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 27);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// http://dev.w3.org/csswg/css-color/

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var HEX3 = /^#([a-f0-9]{3})$/i;
var hex3 = function hex3(value) {
    var match = value.match(HEX3);
    if (match) {
        return [parseInt(match[1][0] + match[1][0], 16), parseInt(match[1][1] + match[1][1], 16), parseInt(match[1][2] + match[1][2], 16), null];
    }
    return false;
};

var HEX6 = /^#([a-f0-9]{6})$/i;
var hex6 = function hex6(value) {
    var match = value.match(HEX6);
    if (match) {
        return [parseInt(match[1].substring(0, 2), 16), parseInt(match[1].substring(2, 4), 16), parseInt(match[1].substring(4, 6), 16), null];
    }
    return false;
};

var RGB = /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/;
var rgb = function rgb(value) {
    var match = value.match(RGB);
    if (match) {
        return [Number(match[1]), Number(match[2]), Number(match[3]), null];
    }
    return false;
};

var RGBA = /^rgba\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d?\.?\d+)\s*\)$/;
var rgba = function rgba(value) {
    var match = value.match(RGBA);
    if (match && match.length > 4) {
        return [Number(match[1]), Number(match[2]), Number(match[3]), Number(match[4])];
    }
    return false;
};

var fromArray = function fromArray(array) {
    return [Math.min(array[0], 255), Math.min(array[1], 255), Math.min(array[2], 255), array.length > 3 ? array[3] : null];
};

var namedColor = function namedColor(name) {
    var color = NAMED_COLORS[name.toLowerCase()];
    return color ? color : false;
};

var Color = function () {
    function Color(value) {
        _classCallCheck(this, Color);

        var _ref = Array.isArray(value) ? fromArray(value) : hex3(value) || rgb(value) || rgba(value) || namedColor(value) || hex6(value) || [0, 0, 0, null],
            _ref2 = _slicedToArray(_ref, 4),
            r = _ref2[0],
            g = _ref2[1],
            b = _ref2[2],
            a = _ref2[3];

        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    _createClass(Color, [{
        key: 'isTransparent',
        value: function isTransparent() {
            return this.a === 0;
        }
    }, {
        key: 'toString',
        value: function toString() {
            return this.a !== null && this.a !== 1 ? 'rgba(' + this.r + ',' + this.g + ',' + this.b + ',' + this.a + ')' : 'rgb(' + this.r + ',' + this.g + ',' + this.b + ')';
        }
    }]);

    return Color;
}();

exports.default = Color;


var NAMED_COLORS = {
    transparent: [0, 0, 0, 0],
    aliceblue: [240, 248, 255, null],
    antiquewhite: [250, 235, 215, null],
    aqua: [0, 255, 255, null],
    aquamarine: [127, 255, 212, null],
    azure: [240, 255, 255, null],
    beige: [245, 245, 220, null],
    bisque: [255, 228, 196, null],
    black: [0, 0, 0, null],
    blanchedalmond: [255, 235, 205, null],
    blue: [0, 0, 255, null],
    blueviolet: [138, 43, 226, null],
    brown: [165, 42, 42, null],
    burlywood: [222, 184, 135, null],
    cadetblue: [95, 158, 160, null],
    chartreuse: [127, 255, 0, null],
    chocolate: [210, 105, 30, null],
    coral: [255, 127, 80, null],
    cornflowerblue: [100, 149, 237, null],
    cornsilk: [255, 248, 220, null],
    crimson: [220, 20, 60, null],
    cyan: [0, 255, 255, null],
    darkblue: [0, 0, 139, null],
    darkcyan: [0, 139, 139, null],
    darkgoldenrod: [184, 134, 11, null],
    darkgray: [169, 169, 169, null],
    darkgreen: [0, 100, 0, null],
    darkgrey: [169, 169, 169, null],
    darkkhaki: [189, 183, 107, null],
    darkmagenta: [139, 0, 139, null],
    darkolivegreen: [85, 107, 47, null],
    darkorange: [255, 140, 0, null],
    darkorchid: [153, 50, 204, null],
    darkred: [139, 0, 0, null],
    darksalmon: [233, 150, 122, null],
    darkseagreen: [143, 188, 143, null],
    darkslateblue: [72, 61, 139, null],
    darkslategray: [47, 79, 79, null],
    darkslategrey: [47, 79, 79, null],
    darkturquoise: [0, 206, 209, null],
    darkviolet: [148, 0, 211, null],
    deeppink: [255, 20, 147, null],
    deepskyblue: [0, 191, 255, null],
    dimgray: [105, 105, 105, null],
    dimgrey: [105, 105, 105, null],
    dodgerblue: [30, 144, 255, null],
    firebrick: [178, 34, 34, null],
    floralwhite: [255, 250, 240, null],
    forestgreen: [34, 139, 34, null],
    fuchsia: [255, 0, 255, null],
    gainsboro: [220, 220, 220, null],
    ghostwhite: [248, 248, 255, null],
    gold: [255, 215, 0, null],
    goldenrod: [218, 165, 32, null],
    gray: [128, 128, 128, null],
    green: [0, 128, 0, null],
    greenyellow: [173, 255, 47, null],
    grey: [128, 128, 128, null],
    honeydew: [240, 255, 240, null],
    hotpink: [255, 105, 180, null],
    indianred: [205, 92, 92, null],
    indigo: [75, 0, 130, null],
    ivory: [255, 255, 240, null],
    khaki: [240, 230, 140, null],
    lavender: [230, 230, 250, null],
    lavenderblush: [255, 240, 245, null],
    lawngreen: [124, 252, 0, null],
    lemonchiffon: [255, 250, 205, null],
    lightblue: [173, 216, 230, null],
    lightcoral: [240, 128, 128, null],
    lightcyan: [224, 255, 255, null],
    lightgoldenrodyellow: [250, 250, 210, null],
    lightgray: [211, 211, 211, null],
    lightgreen: [144, 238, 144, null],
    lightgrey: [211, 211, 211, null],
    lightpink: [255, 182, 193, null],
    lightsalmon: [255, 160, 122, null],
    lightseagreen: [32, 178, 170, null],
    lightskyblue: [135, 206, 250, null],
    lightslategray: [119, 136, 153, null],
    lightslategrey: [119, 136, 153, null],
    lightsteelblue: [176, 196, 222, null],
    lightyellow: [255, 255, 224, null],
    lime: [0, 255, 0, null],
    limegreen: [50, 205, 50, null],
    linen: [250, 240, 230, null],
    magenta: [255, 0, 255, null],
    maroon: [128, 0, 0, null],
    mediumaquamarine: [102, 205, 170, null],
    mediumblue: [0, 0, 205, null],
    mediumorchid: [186, 85, 211, null],
    mediumpurple: [147, 112, 219, null],
    mediumseagreen: [60, 179, 113, null],
    mediumslateblue: [123, 104, 238, null],
    mediumspringgreen: [0, 250, 154, null],
    mediumturquoise: [72, 209, 204, null],
    mediumvioletred: [199, 21, 133, null],
    midnightblue: [25, 25, 112, null],
    mintcream: [245, 255, 250, null],
    mistyrose: [255, 228, 225, null],
    moccasin: [255, 228, 181, null],
    navajowhite: [255, 222, 173, null],
    navy: [0, 0, 128, null],
    oldlace: [253, 245, 230, null],
    olive: [128, 128, 0, null],
    olivedrab: [107, 142, 35, null],
    orange: [255, 165, 0, null],
    orangered: [255, 69, 0, null],
    orchid: [218, 112, 214, null],
    palegoldenrod: [238, 232, 170, null],
    palegreen: [152, 251, 152, null],
    paleturquoise: [175, 238, 238, null],
    palevioletred: [219, 112, 147, null],
    papayawhip: [255, 239, 213, null],
    peachpuff: [255, 218, 185, null],
    peru: [205, 133, 63, null],
    pink: [255, 192, 203, null],
    plum: [221, 160, 221, null],
    powderblue: [176, 224, 230, null],
    purple: [128, 0, 128, null],
    rebeccapurple: [102, 51, 153, null],
    red: [255, 0, 0, null],
    rosybrown: [188, 143, 143, null],
    royalblue: [65, 105, 225, null],
    saddlebrown: [139, 69, 19, null],
    salmon: [250, 128, 114, null],
    sandybrown: [244, 164, 96, null],
    seagreen: [46, 139, 87, null],
    seashell: [255, 245, 238, null],
    sienna: [160, 82, 45, null],
    silver: [192, 192, 192, null],
    skyblue: [135, 206, 235, null],
    slateblue: [106, 90, 205, null],
    slategray: [112, 128, 144, null],
    slategrey: [112, 128, 144, null],
    snow: [255, 250, 250, null],
    springgreen: [0, 255, 127, null],
    steelblue: [70, 130, 180, null],
    tan: [210, 180, 140, null],
    teal: [0, 128, 128, null],
    thistle: [216, 191, 216, null],
    tomato: [255, 99, 71, null],
    turquoise: [64, 224, 208, null],
    violet: [238, 130, 238, null],
    wheat: [245, 222, 179, null],
    white: [255, 255, 255, null],
    whitesmoke: [245, 245, 245, null],
    yellow: [255, 255, 0, null],
    yellowgreen: [154, 205, 50, null]
};

var TRANSPARENT = exports.TRANSPARENT = new Color([0, 0, 0, 0]);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.calculateLengthFromValueWithUnit = exports.LENGTH_TYPE = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _NodeContainer = __webpack_require__(3);

var _NodeContainer2 = _interopRequireDefault(_NodeContainer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LENGTH_WITH_UNIT = /([\d.]+)(px|r?em|%)/i;

var LENGTH_TYPE = exports.LENGTH_TYPE = {
    PX: 0,
    PERCENTAGE: 1
};

var Length = function () {
    function Length(value) {
        _classCallCheck(this, Length);

        this.type = value.substr(value.length - 1) === '%' ? LENGTH_TYPE.PERCENTAGE : LENGTH_TYPE.PX;
        var parsedValue = parseFloat(value);
        if (true && isNaN(parsedValue)) {
            console.error('Invalid value given for Length: "' + value + '"');
        }
        this.value = isNaN(parsedValue) ? 0 : parsedValue;
    }

    _createClass(Length, [{
        key: 'isPercentage',
        value: function isPercentage() {
            return this.type === LENGTH_TYPE.PERCENTAGE;
        }
    }, {
        key: 'getAbsoluteValue',
        value: function getAbsoluteValue(parentLength) {
            return this.isPercentage() ? parentLength * (this.value / 100) : this.value;
        }
    }], [{
        key: 'create',
        value: function create(v) {
            return new Length(v);
        }
    }]);

    return Length;
}();

exports.default = Length;


var getRootFontSize = function getRootFontSize(container) {
    var parent = container.parent;
    return parent ? getRootFontSize(parent) : parseFloat(container.style.font.fontSize);
};

var calculateLengthFromValueWithUnit = exports.calculateLengthFromValueWithUnit = function calculateLengthFromValueWithUnit(container, value, unit) {
    switch (unit) {
        case 'px':
        case '%':
            return new Length(value + unit);
        case 'em':
        case 'rem':
            var length = new Length(value);
            length.value *= unit === 'em' ? parseFloat(container.style.font.fontSize) : getRootFontSize(container);
            return length;
        default:
            // TODO: handle correctly if unknown unit is used
            return new Length('0');
    }
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseBoundCurves = exports.calculatePaddingBoxPath = exports.calculateBorderBoxPath = exports.parsePathForBorder = exports.parseDocumentSize = exports.calculateContentBox = exports.calculatePaddingBox = exports.parseBounds = exports.Bounds = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Vector = __webpack_require__(7);

var _Vector2 = _interopRequireDefault(_Vector);

var _BezierCurve = __webpack_require__(32);

var _BezierCurve2 = _interopRequireDefault(_BezierCurve);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TOP = 0;
var RIGHT = 1;
var BOTTOM = 2;
var LEFT = 3;

var H = 0;
var V = 1;

var Bounds = exports.Bounds = function () {
    function Bounds(x, y, w, h) {
        _classCallCheck(this, Bounds);

        this.left = x;
        this.top = y;
        this.width = w;
        this.height = h;
    }

    _createClass(Bounds, null, [{
        key: 'fromClientRect',
        value: function fromClientRect(clientRect, scrollX, scrollY) {
            return new Bounds(clientRect.left + scrollX, clientRect.top + scrollY, clientRect.width, clientRect.height);
        }
    }]);

    return Bounds;
}();

var parseBounds = exports.parseBounds = function parseBounds(node, scrollX, scrollY) {
    return Bounds.fromClientRect(node.getBoundingClientRect(), scrollX, scrollY);
};

var calculatePaddingBox = exports.calculatePaddingBox = function calculatePaddingBox(bounds, borders) {
    return new Bounds(bounds.left + borders[LEFT].borderWidth, bounds.top + borders[TOP].borderWidth, bounds.width - (borders[RIGHT].borderWidth + borders[LEFT].borderWidth), bounds.height - (borders[TOP].borderWidth + borders[BOTTOM].borderWidth));
};

var calculateContentBox = exports.calculateContentBox = function calculateContentBox(bounds, padding, borders) {
    // TODO support percentage paddings
    var paddingTop = padding[TOP].value;
    var paddingRight = padding[RIGHT].value;
    var paddingBottom = padding[BOTTOM].value;
    var paddingLeft = padding[LEFT].value;

    return new Bounds(bounds.left + paddingLeft + borders[LEFT].borderWidth, bounds.top + paddingTop + borders[TOP].borderWidth, bounds.width - (borders[RIGHT].borderWidth + borders[LEFT].borderWidth + paddingLeft + paddingRight), bounds.height - (borders[TOP].borderWidth + borders[BOTTOM].borderWidth + paddingTop + paddingBottom));
};

var parseDocumentSize = exports.parseDocumentSize = function parseDocumentSize(document) {
    var body = document.body;
    var documentElement = document.documentElement;

    if (!body || !documentElement) {
        throw new Error( true ? 'Unable to get document size' : '');
    }
    var width = Math.max(Math.max(body.scrollWidth, documentElement.scrollWidth), Math.max(body.offsetWidth, documentElement.offsetWidth), Math.max(body.clientWidth, documentElement.clientWidth));

    var height = Math.max(Math.max(body.scrollHeight, documentElement.scrollHeight), Math.max(body.offsetHeight, documentElement.offsetHeight), Math.max(body.clientHeight, documentElement.clientHeight));

    return new Bounds(0, 0, width, height);
};

var parsePathForBorder = exports.parsePathForBorder = function parsePathForBorder(curves, borderSide) {
    switch (borderSide) {
        case TOP:
            return createPathFromCurves(curves.topLeftOuter, curves.topLeftInner, curves.topRightOuter, curves.topRightInner);
        case RIGHT:
            return createPathFromCurves(curves.topRightOuter, curves.topRightInner, curves.bottomRightOuter, curves.bottomRightInner);
        case BOTTOM:
            return createPathFromCurves(curves.bottomRightOuter, curves.bottomRightInner, curves.bottomLeftOuter, curves.bottomLeftInner);
        case LEFT:
        default:
            return createPathFromCurves(curves.bottomLeftOuter, curves.bottomLeftInner, curves.topLeftOuter, curves.topLeftInner);
    }
};

var createPathFromCurves = function createPathFromCurves(outer1, inner1, outer2, inner2) {
    var path = [];
    if (outer1 instanceof _BezierCurve2.default) {
        path.push(outer1.subdivide(0.5, false));
    } else {
        path.push(outer1);
    }

    if (outer2 instanceof _BezierCurve2.default) {
        path.push(outer2.subdivide(0.5, true));
    } else {
        path.push(outer2);
    }

    if (inner2 instanceof _BezierCurve2.default) {
        path.push(inner2.subdivide(0.5, true).reverse());
    } else {
        path.push(inner2);
    }

    if (inner1 instanceof _BezierCurve2.default) {
        path.push(inner1.subdivide(0.5, false).reverse());
    } else {
        path.push(inner1);
    }

    return path;
};

var calculateBorderBoxPath = exports.calculateBorderBoxPath = function calculateBorderBoxPath(curves) {
    return [curves.topLeftOuter, curves.topRightOuter, curves.bottomRightOuter, curves.bottomLeftOuter];
};

var calculatePaddingBoxPath = exports.calculatePaddingBoxPath = function calculatePaddingBoxPath(curves) {
    return [curves.topLeftInner, curves.topRightInner, curves.bottomRightInner, curves.bottomLeftInner];
};

var parseBoundCurves = exports.parseBoundCurves = function parseBoundCurves(bounds, borders, borderRadius) {
    var tlh = borderRadius[CORNER.TOP_LEFT][H].getAbsoluteValue(bounds.width);
    var tlv = borderRadius[CORNER.TOP_LEFT][V].getAbsoluteValue(bounds.height);
    var trh = borderRadius[CORNER.TOP_RIGHT][H].getAbsoluteValue(bounds.width);
    var trv = borderRadius[CORNER.TOP_RIGHT][V].getAbsoluteValue(bounds.height);
    var brh = borderRadius[CORNER.BOTTOM_RIGHT][H].getAbsoluteValue(bounds.width);
    var brv = borderRadius[CORNER.BOTTOM_RIGHT][V].getAbsoluteValue(bounds.height);
    var blh = borderRadius[CORNER.BOTTOM_LEFT][H].getAbsoluteValue(bounds.width);
    var blv = borderRadius[CORNER.BOTTOM_LEFT][V].getAbsoluteValue(bounds.height);

    var factors = [];
    factors.push((tlh + trh) / bounds.width);
    factors.push((blh + brh) / bounds.width);
    factors.push((tlv + blv) / bounds.height);
    factors.push((trv + brv) / bounds.height);
    var maxFactor = Math.max.apply(Math, factors);

    if (maxFactor > 1) {
        tlh /= maxFactor;
        tlv /= maxFactor;
        trh /= maxFactor;
        trv /= maxFactor;
        brh /= maxFactor;
        brv /= maxFactor;
        blh /= maxFactor;
        blv /= maxFactor;
    }

    var topWidth = bounds.width - trh;
    var rightHeight = bounds.height - brv;
    var bottomWidth = bounds.width - brh;
    var leftHeight = bounds.height - blv;

    return {
        topLeftOuter: tlh > 0 || tlv > 0 ? getCurvePoints(bounds.left, bounds.top, tlh, tlv, CORNER.TOP_LEFT) : new _Vector2.default(bounds.left, bounds.top),
        topLeftInner: tlh > 0 || tlv > 0 ? getCurvePoints(bounds.left + borders[LEFT].borderWidth, bounds.top + borders[TOP].borderWidth, Math.max(0, tlh - borders[LEFT].borderWidth), Math.max(0, tlv - borders[TOP].borderWidth), CORNER.TOP_LEFT) : new _Vector2.default(bounds.left + borders[LEFT].borderWidth, bounds.top + borders[TOP].borderWidth),
        topRightOuter: trh > 0 || trv > 0 ? getCurvePoints(bounds.left + topWidth, bounds.top, trh, trv, CORNER.TOP_RIGHT) : new _Vector2.default(bounds.left + bounds.width, bounds.top),
        topRightInner: trh > 0 || trv > 0 ? getCurvePoints(bounds.left + Math.min(topWidth, bounds.width + borders[LEFT].borderWidth), bounds.top + borders[TOP].borderWidth, topWidth > bounds.width + borders[LEFT].borderWidth ? 0 : trh - borders[LEFT].borderWidth, trv - borders[TOP].borderWidth, CORNER.TOP_RIGHT) : new _Vector2.default(bounds.left + bounds.width - borders[RIGHT].borderWidth, bounds.top + borders[TOP].borderWidth),
        bottomRightOuter: brh > 0 || brv > 0 ? getCurvePoints(bounds.left + bottomWidth, bounds.top + rightHeight, brh, brv, CORNER.BOTTOM_RIGHT) : new _Vector2.default(bounds.left + bounds.width, bounds.top + bounds.height),
        bottomRightInner: brh > 0 || brv > 0 ? getCurvePoints(bounds.left + Math.min(bottomWidth, bounds.width - borders[LEFT].borderWidth), bounds.top + Math.min(rightHeight, bounds.height + borders[TOP].borderWidth), Math.max(0, brh - borders[RIGHT].borderWidth), brv - borders[BOTTOM].borderWidth, CORNER.BOTTOM_RIGHT) : new _Vector2.default(bounds.left + bounds.width - borders[RIGHT].borderWidth, bounds.top + bounds.height - borders[BOTTOM].borderWidth),
        bottomLeftOuter: blh > 0 || blv > 0 ? getCurvePoints(bounds.left, bounds.top + leftHeight, blh, blv, CORNER.BOTTOM_LEFT) : new _Vector2.default(bounds.left, bounds.top + bounds.height),
        bottomLeftInner: blh > 0 || blv > 0 ? getCurvePoints(bounds.left + borders[LEFT].borderWidth, bounds.top + leftHeight, Math.max(0, blh - borders[LEFT].borderWidth), blv - borders[BOTTOM].borderWidth, CORNER.BOTTOM_LEFT) : new _Vector2.default(bounds.left + borders[LEFT].borderWidth, bounds.top + bounds.height - borders[BOTTOM].borderWidth)
    };
};

var CORNER = {
    TOP_LEFT: 0,
    TOP_RIGHT: 1,
    BOTTOM_RIGHT: 2,
    BOTTOM_LEFT: 3
};

var getCurvePoints = function getCurvePoints(x, y, r1, r2, position) {
    var kappa = 4 * ((Math.sqrt(2) - 1) / 3);
    var ox = r1 * kappa; // control point offset horizontal
    var oy = r2 * kappa; // control point offset vertical
    var xm = x + r1; // x-middle
    var ym = y + r2; // y-middle

    switch (position) {
        case CORNER.TOP_LEFT:
            return new _BezierCurve2.default(new _Vector2.default(x, ym), new _Vector2.default(x, ym - oy), new _Vector2.default(xm - ox, y), new _Vector2.default(xm, y));
        case CORNER.TOP_RIGHT:
            return new _BezierCurve2.default(new _Vector2.default(x, y), new _Vector2.default(x + ox, y), new _Vector2.default(xm, ym - oy), new _Vector2.default(xm, ym));
        case CORNER.BOTTOM_RIGHT:
            return new _BezierCurve2.default(new _Vector2.default(xm, y), new _Vector2.default(xm, y + oy), new _Vector2.default(x + ox, ym), new _Vector2.default(x, ym));
        case CORNER.BOTTOM_LEFT:
        default:
            return new _BezierCurve2.default(new _Vector2.default(xm, ym), new _Vector2.default(xm - ox, ym), new _Vector2.default(x, y + oy), new _Vector2.default(x, y));
    }
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Color = __webpack_require__(0);

var _Color2 = _interopRequireDefault(_Color);

var _Util = __webpack_require__(4);

var _background = __webpack_require__(5);

var _border = __webpack_require__(12);

var _borderRadius = __webpack_require__(33);

var _display = __webpack_require__(34);

var _float = __webpack_require__(35);

var _font = __webpack_require__(36);

var _letterSpacing = __webpack_require__(37);

var _lineBreak = __webpack_require__(38);

var _listStyle = __webpack_require__(8);

var _margin = __webpack_require__(39);

var _overflow = __webpack_require__(40);

var _overflowWrap = __webpack_require__(18);

var _padding = __webpack_require__(17);

var _position = __webpack_require__(19);

var _textDecoration = __webpack_require__(11);

var _textShadow = __webpack_require__(41);

var _textTransform = __webpack_require__(20);

var _transform = __webpack_require__(42);

var _visibility = __webpack_require__(43);

var _wordBreak = __webpack_require__(44);

var _zIndex = __webpack_require__(45);

var _Bounds = __webpack_require__(2);

var _Input = __webpack_require__(21);

var _ListItem = __webpack_require__(14);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var INPUT_TAGS = ['INPUT', 'TEXTAREA', 'SELECT'];

var NodeContainer = function () {
    function NodeContainer(node, parent, resourceLoader, index) {
        var _this = this;

        _classCallCheck(this, NodeContainer);

        this.parent = parent;
        this.tagName = node.tagName;
        this.index = index;
        this.childNodes = [];
        this.listItems = [];
        if (typeof node.start === 'number') {
            this.listStart = node.start;
        }
        var defaultView = node.ownerDocument.defaultView;
        var scrollX = defaultView.pageXOffset;
        var scrollY = defaultView.pageYOffset;
        var style = defaultView.getComputedStyle(node, null);
        var display = (0, _display.parseDisplay)(style.display);

        var IS_INPUT = node.type === 'radio' || node.type === 'checkbox';

        var position = (0, _position.parsePosition)(style.position);

        this.style = {
            background: IS_INPUT ? _Input.INPUT_BACKGROUND : (0, _background.parseBackground)(style, resourceLoader),
            border: IS_INPUT ? _Input.INPUT_BORDERS : (0, _border.parseBorder)(style),
            borderRadius: (node instanceof defaultView.HTMLInputElement || node instanceof HTMLInputElement) && IS_INPUT ? (0, _Input.getInputBorderRadius)(node) : (0, _borderRadius.parseBorderRadius)(style),
            color: IS_INPUT ? _Input.INPUT_COLOR : new _Color2.default(style.color),
            display: display,
            float: (0, _float.parseCSSFloat)(style.float),
            font: (0, _font.parseFont)(style),
            letterSpacing: (0, _letterSpacing.parseLetterSpacing)(style.letterSpacing),
            listStyle: display === _display.DISPLAY.LIST_ITEM ? (0, _listStyle.parseListStyle)(style) : null,
            lineBreak: (0, _lineBreak.parseLineBreak)(style.lineBreak),
            margin: (0, _margin.parseMargin)(style),
            opacity: parseFloat(style.opacity),
            overflow: INPUT_TAGS.indexOf(node.tagName) === -1 ? (0, _overflow.parseOverflow)(style.overflow) : _overflow.OVERFLOW.HIDDEN,
            overflowWrap: (0, _overflowWrap.parseOverflowWrap)(style.overflowWrap ? style.overflowWrap : style.wordWrap),
            padding: (0, _padding.parsePadding)(style),
            position: position,
            textDecoration: (0, _textDecoration.parseTextDecoration)(style),
            textShadow: (0, _textShadow.parseTextShadow)(style.textShadow),
            textTransform: (0, _textTransform.parseTextTransform)(style.textTransform),
            transform: (0, _transform.parseTransform)(style),
            visibility: (0, _visibility.parseVisibility)(style.visibility),
            wordBreak: (0, _wordBreak.parseWordBreak)(style.wordBreak),
            zIndex: (0, _zIndex.parseZIndex)(position !== _position.POSITION.STATIC ? style.zIndex : 'auto')
        };

        if (this.isTransformed()) {
            // getBoundingClientRect provides values post-transform, we want them without the transformation
            node.style.transform = 'matrix(1,0,0,1,0,0)';
        }

        if (display === _display.DISPLAY.LIST_ITEM) {
            var listOwner = (0, _ListItem.getListOwner)(this);
            if (listOwner) {
                var listIndex = listOwner.listItems.length;
                listOwner.listItems.push(this);
                this.listIndex = node.hasAttribute('value') && typeof node.value === 'number' ? node.value : listIndex === 0 ? typeof listOwner.listStart === 'number' ? listOwner.listStart : 1 : listOwner.listItems[listIndex - 1].listIndex + 1;
            }
        }

        // TODO move bound retrieval for all nodes to a later stage?
        if (node.tagName === 'IMG') {
            node.addEventListener('load', function () {
                _this.bounds = (0, _Bounds.parseBounds)(node, scrollX, scrollY);
                _this.curvedBounds = (0, _Bounds.parseBoundCurves)(_this.bounds, _this.style.border, _this.style.borderRadius);
            });
        }
        this.image = getImage(node, resourceLoader);
        this.bounds = IS_INPUT ? (0, _Input.reformatInputBounds)((0, _Bounds.parseBounds)(node, scrollX, scrollY)) : (0, _Bounds.parseBounds)(node, scrollX, scrollY);
        this.curvedBounds = (0, _Bounds.parseBoundCurves)(this.bounds, this.style.border, this.style.borderRadius);

        if (true) {
            this.name = '' + node.tagName.toLowerCase() + (node.id ? '#' + node.id : '') + node.className.toString().split(' ').map(function (s) {
                return s.length ? '.' + s : '';
            }).join('');
        }
    }

    _createClass(NodeContainer, [{
        key: 'getClipPaths',
        value: function getClipPaths() {
            var parentClips = this.parent ? this.parent.getClipPaths() : [];
            var isClipped = this.style.overflow !== _overflow.OVERFLOW.VISIBLE;

            return isClipped ? parentClips.concat([(0, _Bounds.calculatePaddingBoxPath)(this.curvedBounds)]) : parentClips;
        }
    }, {
        key: 'isInFlow',
        value: function isInFlow() {
            return this.isRootElement() && !this.isFloating() && !this.isAbsolutelyPositioned();
        }
    }, {
        key: 'isVisible',
        value: function isVisible() {
            return !(0, _Util.contains)(this.style.display, _display.DISPLAY.NONE) && this.style.opacity > 0 && this.style.visibility === _visibility.VISIBILITY.VISIBLE;
        }
    }, {
        key: 'isAbsolutelyPositioned',
        value: function isAbsolutelyPositioned() {
            return this.style.position !== _position.POSITION.STATIC && this.style.position !== _position.POSITION.RELATIVE;
        }
    }, {
        key: 'isPositioned',
        value: function isPositioned() {
            return this.style.position !== _position.POSITION.STATIC;
        }
    }, {
        key: 'isFloating',
        value: function isFloating() {
            return this.style.float !== _float.FLOAT.NONE;
        }
    }, {
        key: 'isRootElement',
        value: function isRootElement() {
            return this.parent === null;
        }
    }, {
        key: 'isTransformed',
        value: function isTransformed() {
            return this.style.transform !== null;
        }
    }, {
        key: 'isPositionedWithZIndex',
        value: function isPositionedWithZIndex() {
            return this.isPositioned() && !this.style.zIndex.auto;
        }
    }, {
        key: 'isInlineLevel',
        value: function isInlineLevel() {
            return (0, _Util.contains)(this.style.display, _display.DISPLAY.INLINE) || (0, _Util.contains)(this.style.display, _display.DISPLAY.INLINE_BLOCK) || (0, _Util.contains)(this.style.display, _display.DISPLAY.INLINE_FLEX) || (0, _Util.contains)(this.style.display, _display.DISPLAY.INLINE_GRID) || (0, _Util.contains)(this.style.display, _display.DISPLAY.INLINE_LIST_ITEM) || (0, _Util.contains)(this.style.display, _display.DISPLAY.INLINE_TABLE);
        }
    }, {
        key: 'isInlineBlockOrInlineTable',
        value: function isInlineBlockOrInlineTable() {
            return (0, _Util.contains)(this.style.display, _display.DISPLAY.INLINE_BLOCK) || (0, _Util.contains)(this.style.display, _display.DISPLAY.INLINE_TABLE);
        }
    }]);

    return NodeContainer;
}();

exports.default = NodeContainer;


var getImage = function getImage(node, resourceLoader) {
    if (node instanceof node.ownerDocument.defaultView.SVGSVGElement || node instanceof SVGSVGElement) {
        var s = new XMLSerializer();
        return resourceLoader.loadImage('data:image/svg+xml,' + encodeURIComponent(s.serializeToString(node)));
    }
    switch (node.tagName) {
        case 'IMG':
            // $FlowFixMe
            var img = node;
            return resourceLoader.loadImage(img.currentSrc || img.src);
        case 'CANVAS':
            // $FlowFixMe
            var canvas = node;
            return resourceLoader.loadCanvas(canvas);
        case 'IFRAME':
            var iframeKey = node.getAttribute('data-html2canvas-internal-iframe-key');
            if (iframeKey) {
                return iframeKey;
            }
            break;
    }

    return null;
};

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var contains = exports.contains = function contains(bit, value) {
    return (bit & value) !== 0;
};

var distance = exports.distance = function distance(a, b) {
    return Math.sqrt(a * a + b * b);
};

var copyCSSStyles = exports.copyCSSStyles = function copyCSSStyles(style, target) {
    // Edge does not provide value for cssText
    for (var i = style.length - 1; i >= 0; i--) {
        var property = style.item(i);
        // Safari shows pseudoelements if content is set
        if (property !== 'content') {
            target.style.setProperty(property, style.getPropertyValue(property));
        }
    }
    return target;
};

var SMALL_IMAGE = exports.SMALL_IMAGE = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseBackgroundImage = exports.parseBackground = exports.calculateBackgroundRepeatPath = exports.calculateBackgroundPosition = exports.calculateBackgroungPositioningArea = exports.calculateBackgroungPaintingArea = exports.calculateGradientBackgroundSize = exports.calculateBackgroundSize = exports.BACKGROUND_ORIGIN = exports.BACKGROUND_CLIP = exports.BACKGROUND_SIZE = exports.BACKGROUND_REPEAT = undefined;

var _Color = __webpack_require__(0);

var _Color2 = _interopRequireDefault(_Color);

var _Length = __webpack_require__(1);

var _Length2 = _interopRequireDefault(_Length);

var _Size = __webpack_require__(31);

var _Size2 = _interopRequireDefault(_Size);

var _Vector = __webpack_require__(7);

var _Vector2 = _interopRequireDefault(_Vector);

var _Bounds = __webpack_require__(2);

var _padding = __webpack_require__(17);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BACKGROUND_REPEAT = exports.BACKGROUND_REPEAT = {
    REPEAT: 0,
    NO_REPEAT: 1,
    REPEAT_X: 2,
    REPEAT_Y: 3
};

var BACKGROUND_SIZE = exports.BACKGROUND_SIZE = {
    AUTO: 0,
    CONTAIN: 1,
    COVER: 2,
    LENGTH: 3
};

var BACKGROUND_CLIP = exports.BACKGROUND_CLIP = {
    BORDER_BOX: 0,
    PADDING_BOX: 1,
    CONTENT_BOX: 2
};

var BACKGROUND_ORIGIN = exports.BACKGROUND_ORIGIN = BACKGROUND_CLIP;

var AUTO = 'auto';

var BackgroundSize = function BackgroundSize(size) {
    _classCallCheck(this, BackgroundSize);

    switch (size) {
        case 'contain':
            this.size = BACKGROUND_SIZE.CONTAIN;
            break;
        case 'cover':
            this.size = BACKGROUND_SIZE.COVER;
            break;
        case 'auto':
            this.size = BACKGROUND_SIZE.AUTO;
            break;
        default:
            this.value = new _Length2.default(size);
    }
};

var calculateBackgroundSize = exports.calculateBackgroundSize = function calculateBackgroundSize(backgroundImage, image, bounds) {
    var width = 0;
    var height = 0;
    var size = backgroundImage.size;
    if (size[0].size === BACKGROUND_SIZE.CONTAIN || size[0].size === BACKGROUND_SIZE.COVER) {
        var targetRatio = bounds.width / bounds.height;
        var currentRatio = image.width / image.height;
        return targetRatio < currentRatio !== (size[0].size === BACKGROUND_SIZE.COVER) ? new _Size2.default(bounds.width, bounds.width / currentRatio) : new _Size2.default(bounds.height * currentRatio, bounds.height);
    }

    if (size[0].value) {
        width = size[0].value.getAbsoluteValue(bounds.width);
    }

    if (size[0].size === BACKGROUND_SIZE.AUTO && size[1].size === BACKGROUND_SIZE.AUTO) {
        height = image.height;
    } else if (size[1].size === BACKGROUND_SIZE.AUTO) {
        height = width / image.width * image.height;
    } else if (size[1].value) {
        height = size[1].value.getAbsoluteValue(bounds.height);
    }

    if (size[0].size === BACKGROUND_SIZE.AUTO) {
        width = height / image.height * image.width;
    }

    return new _Size2.default(width, height);
};

var calculateGradientBackgroundSize = exports.calculateGradientBackgroundSize = function calculateGradientBackgroundSize(backgroundImage, bounds) {
    var size = backgroundImage.size;
    var width = size[0].value ? size[0].value.getAbsoluteValue(bounds.width) : bounds.width;
    var height = size[1].value ? size[1].value.getAbsoluteValue(bounds.height) : size[0].value ? width : bounds.height;

    return new _Size2.default(width, height);
};

var AUTO_SIZE = new BackgroundSize(AUTO);

var calculateBackgroungPaintingArea = exports.calculateBackgroungPaintingArea = function calculateBackgroungPaintingArea(curves, clip) {
    switch (clip) {
        case BACKGROUND_CLIP.BORDER_BOX:
            return (0, _Bounds.calculateBorderBoxPath)(curves);
        case BACKGROUND_CLIP.PADDING_BOX:
        default:
            return (0, _Bounds.calculatePaddingBoxPath)(curves);
    }
};

var calculateBackgroungPositioningArea = exports.calculateBackgroungPositioningArea = function calculateBackgroungPositioningArea(backgroundOrigin, bounds, padding, border) {
    var paddingBox = (0, _Bounds.calculatePaddingBox)(bounds, border);

    switch (backgroundOrigin) {
        case BACKGROUND_ORIGIN.BORDER_BOX:
            return bounds;
        case BACKGROUND_ORIGIN.CONTENT_BOX:
            var paddingLeft = padding[_padding.PADDING_SIDES.LEFT].getAbsoluteValue(bounds.width);
            var paddingRight = padding[_padding.PADDING_SIDES.RIGHT].getAbsoluteValue(bounds.width);
            var paddingTop = padding[_padding.PADDING_SIDES.TOP].getAbsoluteValue(bounds.width);
            var paddingBottom = padding[_padding.PADDING_SIDES.BOTTOM].getAbsoluteValue(bounds.width);
            return new _Bounds.Bounds(paddingBox.left + paddingLeft, paddingBox.top + paddingTop, paddingBox.width - paddingLeft - paddingRight, paddingBox.height - paddingTop - paddingBottom);
        case BACKGROUND_ORIGIN.PADDING_BOX:
        default:
            return paddingBox;
    }
};

var calculateBackgroundPosition = exports.calculateBackgroundPosition = function calculateBackgroundPosition(position, size, bounds) {
    return new _Vector2.default(position[0].getAbsoluteValue(bounds.width - size.width), position[1].getAbsoluteValue(bounds.height - size.height));
};

var calculateBackgroundRepeatPath = exports.calculateBackgroundRepeatPath = function calculateBackgroundRepeatPath(background, position, size, backgroundPositioningArea, bounds) {
    var repeat = background.repeat;
    switch (repeat) {
        case BACKGROUND_REPEAT.REPEAT_X:
            return [new _Vector2.default(Math.round(bounds.left), Math.round(backgroundPositioningArea.top + position.y)), new _Vector2.default(Math.round(bounds.left + bounds.width), Math.round(backgroundPositioningArea.top + position.y)), new _Vector2.default(Math.round(bounds.left + bounds.width), Math.round(size.height + backgroundPositioningArea.top + position.y)), new _Vector2.default(Math.round(bounds.left), Math.round(size.height + backgroundPositioningArea.top + position.y))];
        case BACKGROUND_REPEAT.REPEAT_Y:
            return [new _Vector2.default(Math.round(backgroundPositioningArea.left + position.x), Math.round(bounds.top)), new _Vector2.default(Math.round(backgroundPositioningArea.left + position.x + size.width), Math.round(bounds.top)), new _Vector2.default(Math.round(backgroundPositioningArea.left + position.x + size.width), Math.round(bounds.height + bounds.top)), new _Vector2.default(Math.round(backgroundPositioningArea.left + position.x), Math.round(bounds.height + bounds.top))];
        case BACKGROUND_REPEAT.NO_REPEAT:
            return [new _Vector2.default(Math.round(backgroundPositioningArea.left + position.x), Math.round(backgroundPositioningArea.top + position.y)), new _Vector2.default(Math.round(backgroundPositioningArea.left + position.x + size.width), Math.round(backgroundPositioningArea.top + position.y)), new _Vector2.default(Math.round(backgroundPositioningArea.left + position.x + size.width), Math.round(backgroundPositioningArea.top + position.y + size.height)), new _Vector2.default(Math.round(backgroundPositioningArea.left + position.x), Math.round(backgroundPositioningArea.top + position.y + size.height))];
        default:
            return [new _Vector2.default(Math.round(bounds.left), Math.round(bounds.top)), new _Vector2.default(Math.round(bounds.left + bounds.width), Math.round(bounds.top)), new _Vector2.default(Math.round(bounds.left + bounds.width), Math.round(bounds.height + bounds.top)), new _Vector2.default(Math.round(bounds.left), Math.round(bounds.height + bounds.top))];
    }
};

var parseBackground = exports.parseBackground = function parseBackground(style, resourceLoader) {
    return {
        backgroundColor: new _Color2.default(style.backgroundColor),
        backgroundImage: parseBackgroundImages(style, resourceLoader),
        backgroundClip: parseBackgroundClip(style.backgroundClip),
        backgroundOrigin: parseBackgroundOrigin(style.backgroundOrigin)
    };
};

var parseBackgroundClip = function parseBackgroundClip(backgroundClip) {
    switch (backgroundClip) {
        case 'padding-box':
            return BACKGROUND_CLIP.PADDING_BOX;
        case 'content-box':
            return BACKGROUND_CLIP.CONTENT_BOX;
    }
    return BACKGROUND_CLIP.BORDER_BOX;
};

var parseBackgroundOrigin = function parseBackgroundOrigin(backgroundOrigin) {
    switch (backgroundOrigin) {
        case 'padding-box':
            return BACKGROUND_ORIGIN.PADDING_BOX;
        case 'content-box':
            return BACKGROUND_ORIGIN.CONTENT_BOX;
    }
    return BACKGROUND_ORIGIN.BORDER_BOX;
};

var parseBackgroundRepeat = function parseBackgroundRepeat(backgroundRepeat) {
    switch (backgroundRepeat.trim()) {
        case 'no-repeat':
            return BACKGROUND_REPEAT.NO_REPEAT;
        case 'repeat-x':
        case 'repeat no-repeat':
            return BACKGROUND_REPEAT.REPEAT_X;
        case 'repeat-y':
        case 'no-repeat repeat':
            return BACKGROUND_REPEAT.REPEAT_Y;
        case 'repeat':
            return BACKGROUND_REPEAT.REPEAT;
    }

    if (true) {
        console.error('Invalid background-repeat value "' + backgroundRepeat + '"');
    }

    return BACKGROUND_REPEAT.REPEAT;
};

var parseBackgroundImages = function parseBackgroundImages(style, resourceLoader) {
    var sources = parseBackgroundImage(style.backgroundImage).map(function (backgroundImage) {
        if (backgroundImage.method === 'url') {
            var key = resourceLoader.loadImage(backgroundImage.args[0]);
            backgroundImage.args = key ? [key] : [];
        }
        return backgroundImage;
    });
    var positions = style.backgroundPosition.split(',');
    var repeats = style.backgroundRepeat.split(',');
    var sizes = style.backgroundSize.split(',');

    return sources.map(function (source, index) {
        var size = (sizes[index] || AUTO).trim().split(' ').map(parseBackgroundSize);
        var position = (positions[index] || AUTO).trim().split(' ').map(parseBackgoundPosition);

        return {
            source: source,
            repeat: parseBackgroundRepeat(typeof repeats[index] === 'string' ? repeats[index] : repeats[0]),
            size: size.length < 2 ? [size[0], AUTO_SIZE] : [size[0], size[1]],
            position: position.length < 2 ? [position[0], position[0]] : [position[0], position[1]]
        };
    });
};

var parseBackgroundSize = function parseBackgroundSize(size) {
    return size === 'auto' ? AUTO_SIZE : new BackgroundSize(size);
};

var parseBackgoundPosition = function parseBackgoundPosition(position) {
    switch (position) {
        case 'bottom':
        case 'right':
            return new _Length2.default('100%');
        case 'left':
        case 'top':
            return new _Length2.default('0%');
        case 'auto':
            return new _Length2.default('0');
    }
    return new _Length2.default(position);
};

var parseBackgroundImage = exports.parseBackgroundImage = function parseBackgroundImage(image) {
    var whitespace = /^\s$/;
    var results = [];

    var args = [];
    var method = '';
    var quote = null;
    var definition = '';
    var mode = 0;
    var numParen = 0;

    var appendResult = function appendResult() {
        var prefix = '';
        if (method) {
            if (definition.substr(0, 1) === '"') {
                definition = definition.substr(1, definition.length - 2);
            }

            if (definition) {
                args.push(definition.trim());
            }

            var prefix_i = method.indexOf('-', 1) + 1;
            if (method.substr(0, 1) === '-' && prefix_i > 0) {
                prefix = method.substr(0, prefix_i).toLowerCase();
                method = method.substr(prefix_i);
            }
            method = method.toLowerCase();
            if (method !== 'none') {
                results.push({
                    prefix: prefix,
                    method: method,
                    args: args
                });
            }
        }
        args = [];
        method = definition = '';
    };

    image.split('').forEach(function (c) {
        if (mode === 0 && whitespace.test(c)) {
            return;
        }
        switch (c) {
            case '"':
                if (!quote) {
                    quote = c;
                } else if (quote === c) {
                    quote = null;
                }
                break;
            case '(':
                if (quote) {
                    break;
                } else if (mode === 0) {
                    mode = 1;
                    return;
                } else {
                    numParen++;
                }
                break;
            case ')':
                if (quote) {
                    break;
                } else if (mode === 1) {
                    if (numParen === 0) {
                        mode = 0;
                        appendResult();
                        return;
                    } else {
                        numParen--;
                    }
                }
                break;

            case ',':
                if (quote) {
                    break;
                } else if (mode === 0) {
                    appendResult();
                    return;
                } else if (mode === 1) {
                    if (numParen === 0 && !method.match(/^url$/i)) {
                        args.push(definition.trim());
                        definition = '';
                        return;
                    }
                }
                break;
        }

        if (mode === 0) {
            method += c;
        } else {
            definition += c;
        }
    });

    appendResult();
    return results;
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var PATH = exports.PATH = {
    VECTOR: 0,
    BEZIER_CURVE: 1,
    CIRCLE: 2
};

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _Path = __webpack_require__(6);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Vector = function Vector(x, y) {
    _classCallCheck(this, Vector);

    this.type = _Path.PATH.VECTOR;
    this.x = x;
    this.y = y;
    if (true) {
        if (isNaN(x)) {
            console.error('Invalid x value given for Vector');
        }
        if (isNaN(y)) {
            console.error('Invalid y value given for Vector');
        }
    }
};

exports.default = Vector;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseListStyle = exports.parseListStyleType = exports.LIST_STYLE_TYPE = exports.LIST_STYLE_POSITION = undefined;

var _background = __webpack_require__(5);

var LIST_STYLE_POSITION = exports.LIST_STYLE_POSITION = {
    INSIDE: 0,
    OUTSIDE: 1
};

var LIST_STYLE_TYPE = exports.LIST_STYLE_TYPE = {
    NONE: -1,
    DISC: 0,
    CIRCLE: 1,
    SQUARE: 2,
    DECIMAL: 3,
    CJK_DECIMAL: 4,
    DECIMAL_LEADING_ZERO: 5,
    LOWER_ROMAN: 6,
    UPPER_ROMAN: 7,
    LOWER_GREEK: 8,
    LOWER_ALPHA: 9,
    UPPER_ALPHA: 10,
    ARABIC_INDIC: 11,
    ARMENIAN: 12,
    BENGALI: 13,
    CAMBODIAN: 14,
    CJK_EARTHLY_BRANCH: 15,
    CJK_HEAVENLY_STEM: 16,
    CJK_IDEOGRAPHIC: 17,
    DEVANAGARI: 18,
    ETHIOPIC_NUMERIC: 19,
    GEORGIAN: 20,
    GUJARATI: 21,
    GURMUKHI: 22,
    HEBREW: 22,
    HIRAGANA: 23,
    HIRAGANA_IROHA: 24,
    JAPANESE_FORMAL: 25,
    JAPANESE_INFORMAL: 26,
    KANNADA: 27,
    KATAKANA: 28,
    KATAKANA_IROHA: 29,
    KHMER: 30,
    KOREAN_HANGUL_FORMAL: 31,
    KOREAN_HANJA_FORMAL: 32,
    KOREAN_HANJA_INFORMAL: 33,
    LAO: 34,
    LOWER_ARMENIAN: 35,
    MALAYALAM: 36,
    MONGOLIAN: 37,
    MYANMAR: 38,
    ORIYA: 39,
    PERSIAN: 40,
    SIMP_CHINESE_FORMAL: 41,
    SIMP_CHINESE_INFORMAL: 42,
    TAMIL: 43,
    TELUGU: 44,
    THAI: 45,
    TIBETAN: 46,
    TRAD_CHINESE_FORMAL: 47,
    TRAD_CHINESE_INFORMAL: 48,
    UPPER_ARMENIAN: 49,
    DISCLOSURE_OPEN: 50,
    DISCLOSURE_CLOSED: 51
};

var parseListStyleType = exports.parseListStyleType = function parseListStyleType(type) {
    switch (type) {
        case 'disc':
            return LIST_STYLE_TYPE.DISC;
        case 'circle':
            return LIST_STYLE_TYPE.CIRCLE;
        case 'square':
            return LIST_STYLE_TYPE.SQUARE;
        case 'decimal':
            return LIST_STYLE_TYPE.DECIMAL;
        case 'cjk-decimal':
            return LIST_STYLE_TYPE.CJK_DECIMAL;
        case 'decimal-leading-zero':
            return LIST_STYLE_TYPE.DECIMAL_LEADING_ZERO;
        case 'lower-roman':
            return LIST_STYLE_TYPE.LOWER_ROMAN;
        case 'upper-roman':
            return LIST_STYLE_TYPE.UPPER_ROMAN;
        case 'lower-greek':
            return LIST_STYLE_TYPE.LOWER_GREEK;
        case 'lower-alpha':
            return LIST_STYLE_TYPE.LOWER_ALPHA;
        case 'upper-alpha':
            return LIST_STYLE_TYPE.UPPER_ALPHA;
        case 'arabic-indic':
            return LIST_STYLE_TYPE.ARABIC_INDIC;
        case 'armenian':
            return LIST_STYLE_TYPE.ARMENIAN;
        case 'bengali':
            return LIST_STYLE_TYPE.BENGALI;
        case 'cambodian':
            return LIST_STYLE_TYPE.CAMBODIAN;
        case 'cjk-earthly-branch':
            return LIST_STYLE_TYPE.CJK_EARTHLY_BRANCH;
        case 'cjk-heavenly-stem':
            return LIST_STYLE_TYPE.CJK_HEAVENLY_STEM;
        case 'cjk-ideographic':
            return LIST_STYLE_TYPE.CJK_IDEOGRAPHIC;
        case 'devanagari':
            return LIST_STYLE_TYPE.DEVANAGARI;
        case 'ethiopic-numeric':
            return LIST_STYLE_TYPE.ETHIOPIC_NUMERIC;
        case 'georgian':
            return LIST_STYLE_TYPE.GEORGIAN;
        case 'gujarati':
            return LIST_STYLE_TYPE.GUJARATI;
        case 'gurmukhi':
            return LIST_STYLE_TYPE.GURMUKHI;
        case 'hebrew':
            return LIST_STYLE_TYPE.HEBREW;
        case 'hiragana':
            return LIST_STYLE_TYPE.HIRAGANA;
        case 'hiragana-iroha':
            return LIST_STYLE_TYPE.HIRAGANA_IROHA;
        case 'japanese-formal':
            return LIST_STYLE_TYPE.JAPANESE_FORMAL;
        case 'japanese-informal':
            return LIST_STYLE_TYPE.JAPANESE_INFORMAL;
        case 'kannada':
            return LIST_STYLE_TYPE.KANNADA;
        case 'katakana':
            return LIST_STYLE_TYPE.KATAKANA;
        case 'katakana-iroha':
            return LIST_STYLE_TYPE.KATAKANA_IROHA;
        case 'khmer':
            return LIST_STYLE_TYPE.KHMER;
        case 'korean-hangul-formal':
            return LIST_STYLE_TYPE.KOREAN_HANGUL_FORMAL;
        case 'korean-hanja-formal':
            return LIST_STYLE_TYPE.KOREAN_HANJA_FORMAL;
        case 'korean-hanja-informal':
            return LIST_STYLE_TYPE.KOREAN_HANJA_INFORMAL;
        case 'lao':
            return LIST_STYLE_TYPE.LAO;
        case 'lower-armenian':
            return LIST_STYLE_TYPE.LOWER_ARMENIAN;
        case 'malayalam':
            return LIST_STYLE_TYPE.MALAYALAM;
        case 'mongolian':
            return LIST_STYLE_TYPE.MONGOLIAN;
        case 'myanmar':
            return LIST_STYLE_TYPE.MYANMAR;
        case 'oriya':
            return LIST_STYLE_TYPE.ORIYA;
        case 'persian':
            return LIST_STYLE_TYPE.PERSIAN;
        case 'simp-chinese-formal':
            return LIST_STYLE_TYPE.SIMP_CHINESE_FORMAL;
        case 'simp-chinese-informal':
            return LIST_STYLE_TYPE.SIMP_CHINESE_INFORMAL;
        case 'tamil':
            return LIST_STYLE_TYPE.TAMIL;
        case 'telugu':
            return LIST_STYLE_TYPE.TELUGU;
        case 'thai':
            return LIST_STYLE_TYPE.THAI;
        case 'tibetan':
            return LIST_STYLE_TYPE.TIBETAN;
        case 'trad-chinese-formal':
            return LIST_STYLE_TYPE.TRAD_CHINESE_FORMAL;
        case 'trad-chinese-informal':
            return LIST_STYLE_TYPE.TRAD_CHINESE_INFORMAL;
        case 'upper-armenian':
            return LIST_STYLE_TYPE.UPPER_ARMENIAN;
        case 'disclosure-open':
            return LIST_STYLE_TYPE.DISCLOSURE_OPEN;
        case 'disclosure-closed':
            return LIST_STYLE_TYPE.DISCLOSURE_CLOSED;
        case 'none':
        default:
            return LIST_STYLE_TYPE.NONE;
    }
};

var parseListStyle = exports.parseListStyle = function parseListStyle(style) {
    var listStyleImage = (0, _background.parseBackgroundImage)(style.getPropertyValue('list-style-image'));
    return {
        listStyleType: parseListStyleType(style.getPropertyValue('list-style-type')),
        listStyleImage: listStyleImage.length ? listStyleImage[0] : null,
        listStylePosition: parseListStylePosition(style.getPropertyValue('list-style-position'))
    };
};

var parseListStylePosition = function parseListStylePosition(position) {
    switch (position) {
        case 'inside':
            return LIST_STYLE_POSITION.INSIDE;
        case 'outside':
        default:
            return LIST_STYLE_POSITION.OUTSIDE;
    }
};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _textTransform = __webpack_require__(20);

var _TextBounds = __webpack_require__(22);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TextContainer = function () {
    function TextContainer(text, parent, bounds) {
        _classCallCheck(this, TextContainer);

        this.text = text;
        this.parent = parent;
        this.bounds = bounds;
    }

    _createClass(TextContainer, null, [{
        key: 'fromTextNode',
        value: function fromTextNode(node, parent) {
            var text = transform(node.data, parent.style.textTransform);
            return new TextContainer(text, parent, (0, _TextBounds.parseTextBounds)(text, parent, node));
        }
    }]);

    return TextContainer;
}();

exports.default = TextContainer;


var CAPITALIZE = /(^|\s|:|-|\(|\))([a-z])/g;

var transform = function transform(text, _transform) {
    switch (_transform) {
        case _textTransform.TEXT_TRANSFORM.LOWERCASE:
            return text.toLowerCase();
        case _textTransform.TEXT_TRANSFORM.CAPITALIZE:
            return text.replace(CAPITALIZE, capitalize);
        case _textTransform.TEXT_TRANSFORM.UPPERCASE:
            return text.toUpperCase();
        default:
            return text;
    }
};

function capitalize(m, p1, p2) {
    if (m.length > 0) {
        return p1 + p2.toUpperCase();
    }

    return m;
}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _ForeignObjectRenderer = __webpack_require__(23);

var testRangeBounds = function testRangeBounds(document) {
    var TEST_HEIGHT = 123;

    if (document.createRange) {
        var range = document.createRange();
        if (range.getBoundingClientRect) {
            var testElement = document.createElement('boundtest');
            testElement.style.height = TEST_HEIGHT + 'px';
            testElement.style.display = 'block';
            document.body.appendChild(testElement);

            range.selectNode(testElement);
            var rangeBounds = range.getBoundingClientRect();
            var rangeHeight = Math.round(rangeBounds.height);
            document.body.removeChild(testElement);
            if (rangeHeight === TEST_HEIGHT) {
                return true;
            }
        }
    }

    return false;
};

// iOS 10.3 taints canvas with base64 images unless crossOrigin = 'anonymous'
var testBase64 = function testBase64(document, src) {
    var img = new Image();
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');

    return new Promise(function (resolve) {
        // Single pixel base64 image renders fine on iOS 10.3???
        img.src = src;

        var onload = function onload() {
            try {
                ctx.drawImage(img, 0, 0);
                canvas.toDataURL();
            } catch (e) {
                return resolve(false);
            }

            return resolve(true);
        };

        img.onload = onload;
        img.onerror = function () {
            return resolve(false);
        };

        if (img.complete === true) {
            setTimeout(function () {
                onload();
            }, 500);
        }
    });
};

var testCORS = function testCORS() {
    return typeof new Image().crossOrigin !== 'undefined';
};

var testResponseType = function testResponseType() {
    return typeof new XMLHttpRequest().responseType === 'string';
};

var testSVG = function testSVG(document) {
    var img = new Image();
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    img.src = 'data:image/svg+xml,<svg xmlns=\'http://www.w3.org/2000/svg\'></svg>';

    try {
        ctx.drawImage(img, 0, 0);
        canvas.toDataURL();
    } catch (e) {
        return false;
    }
    return true;
};

var isGreenPixel = function isGreenPixel(data) {
    return data[0] === 0 && data[1] === 255 && data[2] === 0 && data[3] === 255;
};

var testForeignObject = function testForeignObject(document) {
    var canvas = document.createElement('canvas');
    var size = 100;
    canvas.width = size;
    canvas.height = size;
    var ctx = canvas.getContext('2d');
    ctx.fillStyle = 'rgb(0, 255, 0)';
    ctx.fillRect(0, 0, size, size);

    var img = new Image();
    var greenImageSrc = canvas.toDataURL();
    img.src = greenImageSrc;
    var svg = (0, _ForeignObjectRenderer.createForeignObjectSVG)(size, size, 0, 0, img);
    ctx.fillStyle = 'red';
    ctx.fillRect(0, 0, size, size);

    return (0, _ForeignObjectRenderer.loadSerializedSVG)(svg).then(function (img) {
        ctx.drawImage(img, 0, 0);
        var data = ctx.getImageData(0, 0, size, size).data;
        ctx.fillStyle = 'red';
        ctx.fillRect(0, 0, size, size);

        var node = document.createElement('div');
        node.style.backgroundImage = 'url(' + greenImageSrc + ')';
        node.style.height = size + 'px';
        // Firefox 55 does not render inline <img /> tags
        return isGreenPixel(data) ? (0, _ForeignObjectRenderer.loadSerializedSVG)((0, _ForeignObjectRenderer.createForeignObjectSVG)(size, size, 0, 0, node)) : Promise.reject(false);
    }).then(function (img) {
        ctx.drawImage(img, 0, 0);
        // Edge does not render background-images
        return isGreenPixel(ctx.getImageData(0, 0, size, size).data);
    }).catch(function (e) {
        return false;
    });
};

var FEATURES = {
    // $FlowFixMe - get/set properties not yet supported
    get SUPPORT_RANGE_BOUNDS() {
        'use strict';

        var value = testRangeBounds(document);
        Object.defineProperty(FEATURES, 'SUPPORT_RANGE_BOUNDS', { value: value });
        return value;
    },
    // $FlowFixMe - get/set properties not yet supported
    get SUPPORT_SVG_DRAWING() {
        'use strict';

        var value = testSVG(document);
        Object.defineProperty(FEATURES, 'SUPPORT_SVG_DRAWING', { value: value });
        return value;
    },
    // $FlowFixMe - get/set properties not yet supported
    get SUPPORT_BASE64_DRAWING() {
        'use strict';

        return function (src) {
            var _value = testBase64(document, src);
            Object.defineProperty(FEATURES, 'SUPPORT_BASE64_DRAWING', { value: function value() {
                    return _value;
                } });
            return _value;
        };
    },
    // $FlowFixMe - get/set properties not yet supported
    get SUPPORT_FOREIGNOBJECT_DRAWING() {
        'use strict';

        var value = typeof Array.from === 'function' && typeof window.fetch === 'function' ? testForeignObject(document) : Promise.resolve(false);
        Object.defineProperty(FEATURES, 'SUPPORT_FOREIGNOBJECT_DRAWING', { value: value });
        return value;
    },
    // $FlowFixMe - get/set properties not yet supported
    get SUPPORT_CORS_IMAGES() {
        'use strict';

        var value = testCORS();
        Object.defineProperty(FEATURES, 'SUPPORT_CORS_IMAGES', { value: value });
        return value;
    },
    // $FlowFixMe - get/set properties not yet supported
    get SUPPORT_RESPONSE_TYPE() {
        'use strict';

        var value = testResponseType();
        Object.defineProperty(FEATURES, 'SUPPORT_RESPONSE_TYPE', { value: value });
        return value;
    },
    // $FlowFixMe - get/set properties not yet supported
    get SUPPORT_CORS_XHR() {
        'use strict';

        var value = 'withCredentials' in new XMLHttpRequest();
        Object.defineProperty(FEATURES, 'SUPPORT_CORS_XHR', { value: value });
        return value;
    }
};

exports.default = FEATURES;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseTextDecoration = exports.TEXT_DECORATION_LINE = exports.TEXT_DECORATION = exports.TEXT_DECORATION_STYLE = undefined;

var _Color = __webpack_require__(0);

var _Color2 = _interopRequireDefault(_Color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TEXT_DECORATION_STYLE = exports.TEXT_DECORATION_STYLE = {
    SOLID: 0,
    DOUBLE: 1,
    DOTTED: 2,
    DASHED: 3,
    WAVY: 4
};

var TEXT_DECORATION = exports.TEXT_DECORATION = {
    NONE: null
};

var TEXT_DECORATION_LINE = exports.TEXT_DECORATION_LINE = {
    UNDERLINE: 1,
    OVERLINE: 2,
    LINE_THROUGH: 3,
    BLINK: 4
};

var parseLine = function parseLine(line) {
    switch (line) {
        case 'underline':
            return TEXT_DECORATION_LINE.UNDERLINE;
        case 'overline':
            return TEXT_DECORATION_LINE.OVERLINE;
        case 'line-through':
            return TEXT_DECORATION_LINE.LINE_THROUGH;
    }
    return TEXT_DECORATION_LINE.BLINK;
};

var parseTextDecorationLine = function parseTextDecorationLine(line) {
    if (line === 'none') {
        return null;
    }

    return line.split(' ').map(parseLine);
};

var parseTextDecorationStyle = function parseTextDecorationStyle(style) {
    switch (style) {
        case 'double':
            return TEXT_DECORATION_STYLE.DOUBLE;
        case 'dotted':
            return TEXT_DECORATION_STYLE.DOTTED;
        case 'dashed':
            return TEXT_DECORATION_STYLE.DASHED;
        case 'wavy':
            return TEXT_DECORATION_STYLE.WAVY;
    }
    return TEXT_DECORATION_STYLE.SOLID;
};

var parseTextDecoration = exports.parseTextDecoration = function parseTextDecoration(style) {
    var textDecorationLine = parseTextDecorationLine(style.textDecorationLine ? style.textDecorationLine : style.textDecoration);
    if (textDecorationLine === null) {
        return TEXT_DECORATION.NONE;
    }

    var textDecorationColor = style.textDecorationColor ? new _Color2.default(style.textDecorationColor) : null;
    var textDecorationStyle = parseTextDecorationStyle(style.textDecorationStyle);

    return {
        textDecorationLine: textDecorationLine,
        textDecorationColor: textDecorationColor,
        textDecorationStyle: textDecorationStyle
    };
};

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseBorder = exports.BORDER_SIDES = exports.BORDER_STYLE = undefined;

var _Color = __webpack_require__(0);

var _Color2 = _interopRequireDefault(_Color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var BORDER_STYLE = exports.BORDER_STYLE = {
    NONE: 0,
    SOLID: 1
};

var BORDER_SIDES = exports.BORDER_SIDES = {
    TOP: 0,
    RIGHT: 1,
    BOTTOM: 2,
    LEFT: 3
};

var SIDES = Object.keys(BORDER_SIDES).map(function (s) {
    return s.toLowerCase();
});

var parseBorderStyle = function parseBorderStyle(style) {
    switch (style) {
        case 'none':
            return BORDER_STYLE.NONE;
    }
    return BORDER_STYLE.SOLID;
};

var parseBorder = exports.parseBorder = function parseBorder(style) {
    return SIDES.map(function (side) {
        var borderColor = new _Color2.default(style.getPropertyValue('border-' + side + '-color'));
        var borderStyle = parseBorderStyle(style.getPropertyValue('border-' + side + '-style'));
        var borderWidth = parseFloat(style.getPropertyValue('border-' + side + '-width'));
        return {
            borderColor: borderColor,
            borderStyle: borderStyle,
            borderWidth: isNaN(borderWidth) ? 0 : borderWidth
        };
    });
};

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var toCodePoints = exports.toCodePoints = function toCodePoints(str) {
    var codePoints = [];
    var i = 0;
    var length = str.length;
    while (i < length) {
        var value = str.charCodeAt(i++);
        if (value >= 0xd800 && value <= 0xdbff && i < length) {
            var extra = str.charCodeAt(i++);
            if ((extra & 0xfc00) === 0xdc00) {
                codePoints.push(((value & 0x3ff) << 10) + (extra & 0x3ff) + 0x10000);
            } else {
                codePoints.push(value);
                i--;
            }
        } else {
            codePoints.push(value);
        }
    }
    return codePoints;
};

var fromCodePoint = exports.fromCodePoint = function fromCodePoint() {
    if (String.fromCodePoint) {
        return String.fromCodePoint.apply(String, arguments);
    }

    var length = arguments.length;
    if (!length) {
        return '';
    }

    var codeUnits = [];

    var index = -1;
    var result = '';
    while (++index < length) {
        var codePoint = arguments.length <= index ? undefined : arguments[index];
        if (codePoint <= 0xffff) {
            codeUnits.push(codePoint);
        } else {
            codePoint -= 0x10000;
            codeUnits.push((codePoint >> 10) + 0xd800, codePoint % 0x400 + 0xdc00);
        }
        if (index + 1 === length || codeUnits.length > 0x4000) {
            result += String.fromCharCode.apply(String, codeUnits);
            codeUnits.length = 0;
        }
    }
    return result;
};

var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

// Use a lookup table to find the index.
var lookup = typeof Uint8Array === 'undefined' ? [] : new Uint8Array(256);
for (var i = 0; i < chars.length; i++) {
    lookup[chars.charCodeAt(i)] = i;
}

var decode = exports.decode = function decode(base64) {
    var bufferLength = base64.length * 0.75,
        len = base64.length,
        i = void 0,
        p = 0,
        encoded1 = void 0,
        encoded2 = void 0,
        encoded3 = void 0,
        encoded4 = void 0;

    if (base64[base64.length - 1] === '=') {
        bufferLength--;
        if (base64[base64.length - 2] === '=') {
            bufferLength--;
        }
    }

    var buffer = typeof ArrayBuffer !== 'undefined' && typeof Uint8Array !== 'undefined' && typeof Uint8Array.prototype.slice !== 'undefined' ? new ArrayBuffer(bufferLength) : new Array(bufferLength);
    var bytes = Array.isArray(buffer) ? buffer : new Uint8Array(buffer);

    for (i = 0; i < len; i += 4) {
        encoded1 = lookup[base64.charCodeAt(i)];
        encoded2 = lookup[base64.charCodeAt(i + 1)];
        encoded3 = lookup[base64.charCodeAt(i + 2)];
        encoded4 = lookup[base64.charCodeAt(i + 3)];

        bytes[p++] = encoded1 << 2 | encoded2 >> 4;
        bytes[p++] = (encoded2 & 15) << 4 | encoded3 >> 2;
        bytes[p++] = (encoded3 & 3) << 6 | encoded4 & 63;
    }

    return buffer;
};

var polyUint16Array = exports.polyUint16Array = function polyUint16Array(buffer) {
    var length = buffer.length;
    var bytes = [];
    for (var _i = 0; _i < length; _i += 2) {
        bytes.push(buffer[_i + 1] << 8 | buffer[_i]);
    }
    return bytes;
};

var polyUint32Array = exports.polyUint32Array = function polyUint32Array(buffer) {
    var length = buffer.length;
    var bytes = [];
    for (var _i2 = 0; _i2 < length; _i2 += 4) {
        bytes.push(buffer[_i2 + 3] << 24 | buffer[_i2 + 2] << 16 | buffer[_i2 + 1] << 8 | buffer[_i2]);
    }
    return bytes;
};

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.createCounterText = exports.inlineListItemElement = exports.getListOwner = undefined;

var _Util = __webpack_require__(4);

var _NodeContainer = __webpack_require__(3);

var _NodeContainer2 = _interopRequireDefault(_NodeContainer);

var _TextContainer = __webpack_require__(9);

var _TextContainer2 = _interopRequireDefault(_TextContainer);

var _listStyle = __webpack_require__(8);

var _Unicode = __webpack_require__(24);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Margin between the enumeration and the list item content
var MARGIN_RIGHT = 7;

var ancestorTypes = ['OL', 'UL', 'MENU'];

var getListOwner = exports.getListOwner = function getListOwner(container) {
    var parent = container.parent;
    if (!parent) {
        return null;
    }

    do {
        var isAncestor = ancestorTypes.indexOf(parent.tagName) !== -1;
        if (isAncestor) {
            return parent;
        }
        parent = parent.parent;
    } while (parent);

    return container.parent;
};

var inlineListItemElement = exports.inlineListItemElement = function inlineListItemElement(node, container, resourceLoader) {
    var listStyle = container.style.listStyle;

    if (!listStyle) {
        return;
    }

    var style = node.ownerDocument.defaultView.getComputedStyle(node, null);
    var wrapper = node.ownerDocument.createElement('html2canvaswrapper');
    (0, _Util.copyCSSStyles)(style, wrapper);

    wrapper.style.position = 'absolute';
    wrapper.style.bottom = 'auto';
    wrapper.style.display = 'block';
    wrapper.style.letterSpacing = 'normal';

    switch (listStyle.listStylePosition) {
        case _listStyle.LIST_STYLE_POSITION.OUTSIDE:
            wrapper.style.left = 'auto';
            wrapper.style.right = node.ownerDocument.defaultView.innerWidth - container.bounds.left - container.style.margin[1].getAbsoluteValue(container.bounds.width) + MARGIN_RIGHT + 'px';
            wrapper.style.textAlign = 'right';
            break;
        case _listStyle.LIST_STYLE_POSITION.INSIDE:
            wrapper.style.left = container.bounds.left - container.style.margin[3].getAbsoluteValue(container.bounds.width) + 'px';
            wrapper.style.right = 'auto';
            wrapper.style.textAlign = 'left';
            break;
    }

    var text = void 0;
    var MARGIN_TOP = container.style.margin[0].getAbsoluteValue(container.bounds.width);
    var styleImage = listStyle.listStyleImage;
    if (styleImage) {
        if (styleImage.method === 'url') {
            var image = node.ownerDocument.createElement('img');
            image.src = styleImage.args[0];
            wrapper.style.top = container.bounds.top - MARGIN_TOP + 'px';
            wrapper.style.width = 'auto';
            wrapper.style.height = 'auto';
            wrapper.appendChild(image);
        } else {
            var size = parseFloat(container.style.font.fontSize) * 0.5;
            wrapper.style.top = container.bounds.top - MARGIN_TOP + container.bounds.height - 1.5 * size + 'px';
            wrapper.style.width = size + 'px';
            wrapper.style.height = size + 'px';
            wrapper.style.backgroundImage = style.listStyleImage;
        }
    } else if (typeof container.listIndex === 'number') {
        text = node.ownerDocument.createTextNode(createCounterText(container.listIndex, listStyle.listStyleType, true));
        wrapper.appendChild(text);
        wrapper.style.top = container.bounds.top - MARGIN_TOP + 'px';
    }

    // $FlowFixMe
    var body = node.ownerDocument.body;
    body.appendChild(wrapper);

    if (text) {
        container.childNodes.push(_TextContainer2.default.fromTextNode(text, container));
        body.removeChild(wrapper);
    } else {
        // $FlowFixMe
        container.childNodes.push(new _NodeContainer2.default(wrapper, container, resourceLoader, 0));
    }
};

var ROMAN_UPPER = {
    integers: [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1],
    values: ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I']
};

var ARMENIAN = {
    integers: [9000, 8000, 7000, 6000, 5000, 4000, 3000, 2000, 1000, 900, 800, 700, 600, 500, 400, 300, 200, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
    values: ['Ք', 'Փ', 'Ւ', 'Ց', 'Ր', 'Տ', 'Վ', 'Ս', 'Ռ', 'Ջ', 'Պ', 'Չ', 'Ո', 'Շ', 'Ն', 'Յ', 'Մ', 'Ճ', 'Ղ', 'Ձ', 'Հ', 'Կ', 'Ծ', 'Խ', 'Լ', 'Ի', 'Ժ', 'Թ', 'Ը', 'Է', 'Զ', 'Ե', 'Դ', 'Գ', 'Բ', 'Ա']
};

var HEBREW = {
    integers: [10000, 9000, 8000, 7000, 6000, 5000, 4000, 3000, 2000, 1000, 400, 300, 200, 100, 90, 80, 70, 60, 50, 40, 30, 20, 19, 18, 17, 16, 15, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
    values: ['י׳', 'ט׳', 'ח׳', 'ז׳', 'ו׳', 'ה׳', 'ד׳', 'ג׳', 'ב׳', 'א׳', 'ת', 'ש', 'ר', 'ק', 'צ', 'פ', 'ע', 'ס', 'נ', 'מ', 'ל', 'כ', 'יט', 'יח', 'יז', 'טז', 'טו', 'י', 'ט', 'ח', 'ז', 'ו', 'ה', 'ד', 'ג', 'ב', 'א']
};

var GEORGIAN = {
    integers: [10000, 9000, 8000, 7000, 6000, 5000, 4000, 3000, 2000, 1000, 900, 800, 700, 600, 500, 400, 300, 200, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
    values: ['ჵ', 'ჰ', 'ჯ', 'ჴ', 'ხ', 'ჭ', 'წ', 'ძ', 'ც', 'ჩ', 'შ', 'ყ', 'ღ', 'ქ', 'ფ', 'ჳ', 'ტ', 'ს', 'რ', 'ჟ', 'პ', 'ო', 'ჲ', 'ნ', 'მ', 'ლ', 'კ', 'ი', 'თ', 'ჱ', 'ზ', 'ვ', 'ე', 'დ', 'გ', 'ბ', 'ა']
};

var createAdditiveCounter = function createAdditiveCounter(value, min, max, symbols, fallback, suffix) {
    if (value < min || value > max) {
        return createCounterText(value, fallback, suffix.length > 0);
    }

    return symbols.integers.reduce(function (string, integer, index) {
        while (value >= integer) {
            value -= integer;
            string += symbols.values[index];
        }
        return string;
    }, '') + suffix;
};

var createCounterStyleWithSymbolResolver = function createCounterStyleWithSymbolResolver(value, codePointRangeLength, isNumeric, resolver) {
    var string = '';

    do {
        if (!isNumeric) {
            value--;
        }
        string = resolver(value) + string;
        value /= codePointRangeLength;
    } while (value * codePointRangeLength >= codePointRangeLength);

    return string;
};

var createCounterStyleFromRange = function createCounterStyleFromRange(value, codePointRangeStart, codePointRangeEnd, isNumeric, suffix) {
    var codePointRangeLength = codePointRangeEnd - codePointRangeStart + 1;

    return (value < 0 ? '-' : '') + (createCounterStyleWithSymbolResolver(Math.abs(value), codePointRangeLength, isNumeric, function (codePoint) {
        return (0, _Unicode.fromCodePoint)(Math.floor(codePoint % codePointRangeLength) + codePointRangeStart);
    }) + suffix);
};

var createCounterStyleFromSymbols = function createCounterStyleFromSymbols(value, symbols) {
    var suffix = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '. ';

    var codePointRangeLength = symbols.length;
    return createCounterStyleWithSymbolResolver(Math.abs(value), codePointRangeLength, false, function (codePoint) {
        return symbols[Math.floor(codePoint % codePointRangeLength)];
    }) + suffix;
};

var CJK_ZEROS = 1 << 0;
var CJK_TEN_COEFFICIENTS = 1 << 1;
var CJK_TEN_HIGH_COEFFICIENTS = 1 << 2;
var CJK_HUNDRED_COEFFICIENTS = 1 << 3;

var createCJKCounter = function createCJKCounter(value, numbers, multipliers, negativeSign, suffix, flags) {
    if (value < -9999 || value > 9999) {
        return createCounterText(value, _listStyle.LIST_STYLE_TYPE.CJK_DECIMAL, suffix.length > 0);
    }
    var tmp = Math.abs(value);
    var string = suffix;

    if (tmp === 0) {
        return numbers[0] + string;
    }

    for (var digit = 0; tmp > 0 && digit <= 4; digit++) {
        var coefficient = tmp % 10;

        if (coefficient === 0 && (0, _Util.contains)(flags, CJK_ZEROS) && string !== '') {
            string = numbers[coefficient] + string;
        } else if (coefficient > 1 || coefficient === 1 && digit === 0 || coefficient === 1 && digit === 1 && (0, _Util.contains)(flags, CJK_TEN_COEFFICIENTS) || coefficient === 1 && digit === 1 && (0, _Util.contains)(flags, CJK_TEN_HIGH_COEFFICIENTS) && value > 100 || coefficient === 1 && digit > 1 && (0, _Util.contains)(flags, CJK_HUNDRED_COEFFICIENTS)) {
            string = numbers[coefficient] + (digit > 0 ? multipliers[digit - 1] : '') + string;
        } else if (coefficient === 1 && digit > 0) {
            string = multipliers[digit - 1] + string;
        }
        tmp = Math.floor(tmp / 10);
    }

    return (value < 0 ? negativeSign : '') + string;
};

var CHINESE_INFORMAL_MULTIPLIERS = '十百千萬';
var CHINESE_FORMAL_MULTIPLIERS = '拾佰仟萬';
var JAPANESE_NEGATIVE = 'マイナス';
var KOREAN_NEGATIVE = '마이너스 ';

var createCounterText = exports.createCounterText = function createCounterText(value, type, appendSuffix) {
    var defaultSuffix = appendSuffix ? '. ' : '';
    var cjkSuffix = appendSuffix ? '、' : '';
    var koreanSuffix = appendSuffix ? ', ' : '';
    switch (type) {
        case _listStyle.LIST_STYLE_TYPE.DISC:
            return '•';
        case _listStyle.LIST_STYLE_TYPE.CIRCLE:
            return '◦';
        case _listStyle.LIST_STYLE_TYPE.SQUARE:
            return '◾';
        case _listStyle.LIST_STYLE_TYPE.DECIMAL_LEADING_ZERO:
            var string = createCounterStyleFromRange(value, 48, 57, true, defaultSuffix);
            return string.length < 4 ? '0' + string : string;
        case _listStyle.LIST_STYLE_TYPE.CJK_DECIMAL:
            return createCounterStyleFromSymbols(value, '〇一二三四五六七八九', cjkSuffix);
        case _listStyle.LIST_STYLE_TYPE.LOWER_ROMAN:
            return createAdditiveCounter(value, 1, 3999, ROMAN_UPPER, _listStyle.LIST_STYLE_TYPE.DECIMAL, defaultSuffix).toLowerCase();
        case _listStyle.LIST_STYLE_TYPE.UPPER_ROMAN:
            return createAdditiveCounter(value, 1, 3999, ROMAN_UPPER, _listStyle.LIST_STYLE_TYPE.DECIMAL, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.LOWER_GREEK:
            return createCounterStyleFromRange(value, 945, 969, false, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.LOWER_ALPHA:
            return createCounterStyleFromRange(value, 97, 122, false, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.UPPER_ALPHA:
            return createCounterStyleFromRange(value, 65, 90, false, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.ARABIC_INDIC:
            return createCounterStyleFromRange(value, 1632, 1641, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.ARMENIAN:
        case _listStyle.LIST_STYLE_TYPE.UPPER_ARMENIAN:
            return createAdditiveCounter(value, 1, 9999, ARMENIAN, _listStyle.LIST_STYLE_TYPE.DECIMAL, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.LOWER_ARMENIAN:
            return createAdditiveCounter(value, 1, 9999, ARMENIAN, _listStyle.LIST_STYLE_TYPE.DECIMAL, defaultSuffix).toLowerCase();
        case _listStyle.LIST_STYLE_TYPE.BENGALI:
            return createCounterStyleFromRange(value, 2534, 2543, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.CAMBODIAN:
        case _listStyle.LIST_STYLE_TYPE.KHMER:
            return createCounterStyleFromRange(value, 6112, 6121, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.CJK_EARTHLY_BRANCH:
            return createCounterStyleFromSymbols(value, '子丑寅卯辰巳午未申酉戌亥', cjkSuffix);
        case _listStyle.LIST_STYLE_TYPE.CJK_HEAVENLY_STEM:
            return createCounterStyleFromSymbols(value, '甲乙丙丁戊己庚辛壬癸', cjkSuffix);
        case _listStyle.LIST_STYLE_TYPE.CJK_IDEOGRAPHIC:
        case _listStyle.LIST_STYLE_TYPE.TRAD_CHINESE_INFORMAL:
            return createCJKCounter(value, '零一二三四五六七八九', CHINESE_INFORMAL_MULTIPLIERS, '負', cjkSuffix, CJK_TEN_COEFFICIENTS | CJK_TEN_HIGH_COEFFICIENTS | CJK_HUNDRED_COEFFICIENTS);
        case _listStyle.LIST_STYLE_TYPE.TRAD_CHINESE_FORMAL:
            return createCJKCounter(value, '零壹貳參肆伍陸柒捌玖', CHINESE_FORMAL_MULTIPLIERS, '負', cjkSuffix, CJK_ZEROS | CJK_TEN_COEFFICIENTS | CJK_TEN_HIGH_COEFFICIENTS | CJK_HUNDRED_COEFFICIENTS);
        case _listStyle.LIST_STYLE_TYPE.SIMP_CHINESE_INFORMAL:
            return createCJKCounter(value, '零一二三四五六七八九', CHINESE_INFORMAL_MULTIPLIERS, '负', cjkSuffix, CJK_TEN_COEFFICIENTS | CJK_TEN_HIGH_COEFFICIENTS | CJK_HUNDRED_COEFFICIENTS);
        case _listStyle.LIST_STYLE_TYPE.SIMP_CHINESE_FORMAL:
            return createCJKCounter(value, '零壹贰叁肆伍陆柒捌玖', CHINESE_FORMAL_MULTIPLIERS, '负', cjkSuffix, CJK_ZEROS | CJK_TEN_COEFFICIENTS | CJK_TEN_HIGH_COEFFICIENTS | CJK_HUNDRED_COEFFICIENTS);
        case _listStyle.LIST_STYLE_TYPE.JAPANESE_INFORMAL:
            return createCJKCounter(value, '〇一二三四五六七八九', '十百千万', JAPANESE_NEGATIVE, cjkSuffix, 0);
        case _listStyle.LIST_STYLE_TYPE.JAPANESE_FORMAL:
            return createCJKCounter(value, '零壱弐参四伍六七八九', '拾百千万', JAPANESE_NEGATIVE, cjkSuffix, CJK_ZEROS | CJK_TEN_COEFFICIENTS | CJK_TEN_HIGH_COEFFICIENTS);
        case _listStyle.LIST_STYLE_TYPE.KOREAN_HANGUL_FORMAL:
            return createCJKCounter(value, '영일이삼사오육칠팔구', '십백천만', KOREAN_NEGATIVE, koreanSuffix, CJK_ZEROS | CJK_TEN_COEFFICIENTS | CJK_TEN_HIGH_COEFFICIENTS);
        case _listStyle.LIST_STYLE_TYPE.KOREAN_HANJA_INFORMAL:
            return createCJKCounter(value, '零一二三四五六七八九', '十百千萬', KOREAN_NEGATIVE, koreanSuffix, 0);
        case _listStyle.LIST_STYLE_TYPE.KOREAN_HANJA_FORMAL:
            return createCJKCounter(value, '零壹貳參四五六七八九', '拾百千', KOREAN_NEGATIVE, koreanSuffix, CJK_ZEROS | CJK_TEN_COEFFICIENTS | CJK_TEN_HIGH_COEFFICIENTS);
        case _listStyle.LIST_STYLE_TYPE.DEVANAGARI:
            return createCounterStyleFromRange(value, 0x966, 0x96f, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.GEORGIAN:
            return createAdditiveCounter(value, 1, 19999, GEORGIAN, _listStyle.LIST_STYLE_TYPE.DECIMAL, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.GUJARATI:
            return createCounterStyleFromRange(value, 0xae6, 0xaef, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.GURMUKHI:
            return createCounterStyleFromRange(value, 0xa66, 0xa6f, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.HEBREW:
            return createAdditiveCounter(value, 1, 10999, HEBREW, _listStyle.LIST_STYLE_TYPE.DECIMAL, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.HIRAGANA:
            return createCounterStyleFromSymbols(value, 'あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわゐゑをん');
        case _listStyle.LIST_STYLE_TYPE.HIRAGANA_IROHA:
            return createCounterStyleFromSymbols(value, 'いろはにほへとちりぬるをわかよたれそつねならむうゐのおくやまけふこえてあさきゆめみしゑひもせす');
        case _listStyle.LIST_STYLE_TYPE.KANNADA:
            return createCounterStyleFromRange(value, 0xce6, 0xcef, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.KATAKANA:
            return createCounterStyleFromSymbols(value, 'アイウエオカキクケコサシスセソタチツテトナニヌネノハヒフヘホマミムメモヤユヨラリルレロワヰヱヲン', cjkSuffix);
        case _listStyle.LIST_STYLE_TYPE.KATAKANA_IROHA:
            return createCounterStyleFromSymbols(value, 'イロハニホヘトチリヌルヲワカヨタレソツネナラムウヰノオクヤマケフコエテアサキユメミシヱヒモセス', cjkSuffix);
        case _listStyle.LIST_STYLE_TYPE.LAO:
            return createCounterStyleFromRange(value, 0xed0, 0xed9, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.MONGOLIAN:
            return createCounterStyleFromRange(value, 0x1810, 0x1819, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.MYANMAR:
            return createCounterStyleFromRange(value, 0x1040, 0x1049, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.ORIYA:
            return createCounterStyleFromRange(value, 0xb66, 0xb6f, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.PERSIAN:
            return createCounterStyleFromRange(value, 0x6f0, 0x6f9, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.TAMIL:
            return createCounterStyleFromRange(value, 0xbe6, 0xbef, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.TELUGU:
            return createCounterStyleFromRange(value, 0xc66, 0xc6f, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.THAI:
            return createCounterStyleFromRange(value, 0xe50, 0xe59, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.TIBETAN:
            return createCounterStyleFromRange(value, 0xf20, 0xf29, true, defaultSuffix);
        case _listStyle.LIST_STYLE_TYPE.DECIMAL:
        default:
            return createCounterStyleFromRange(value, 48, 57, true, defaultSuffix);
    }
};

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Path = __webpack_require__(6);

var _textDecoration = __webpack_require__(11);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var addColorStops = function addColorStops(gradient, canvasGradient) {
    var maxStop = Math.max.apply(null, gradient.colorStops.map(function (colorStop) {
        return colorStop.stop;
    }));
    var f = 1 / Math.max(1, maxStop);
    gradient.colorStops.forEach(function (colorStop) {
        canvasGradient.addColorStop(f * colorStop.stop, colorStop.color.toString());
    });
};

var CanvasRenderer = function () {
    function CanvasRenderer(canvas) {
        _classCallCheck(this, CanvasRenderer);

        this.canvas = canvas ? canvas : document.createElement('canvas');
    }

    _createClass(CanvasRenderer, [{
        key: 'render',
        value: function render(options) {
            this.ctx = this.canvas.getContext('2d');
            this.options = options;
            this.canvas.width = Math.floor(options.width * options.scale);
            this.canvas.height = Math.floor(options.height * options.scale);
            this.canvas.style.width = options.width + 'px';
            this.canvas.style.height = options.height + 'px';

            this.ctx.scale(this.options.scale, this.options.scale);
            this.ctx.translate(-options.x, -options.y);
            this.ctx.textBaseline = 'bottom';
            options.logger.log('Canvas renderer initialized (' + options.width + 'x' + options.height + ' at ' + options.x + ',' + options.y + ') with scale ' + this.options.scale);
        }
    }, {
        key: 'clip',
        value: function clip(clipPaths, callback) {
            var _this = this;

            if (clipPaths.length) {
                this.ctx.save();
                clipPaths.forEach(function (path) {
                    _this.path(path);
                    _this.ctx.clip();
                });
            }

            callback();

            if (clipPaths.length) {
                this.ctx.restore();
            }
        }
    }, {
        key: 'drawImage',
        value: function drawImage(image, source, destination) {
            this.ctx.drawImage(image, source.left, source.top, source.width, source.height, destination.left, destination.top, destination.width, destination.height);
        }
    }, {
        key: 'drawShape',
        value: function drawShape(path, color) {
            this.path(path);
            this.ctx.fillStyle = color.toString();
            this.ctx.fill();
        }
    }, {
        key: 'fill',
        value: function fill(color) {
            this.ctx.fillStyle = color.toString();
            this.ctx.fill();
        }
    }, {
        key: 'getTarget',
        value: function getTarget() {
            this.canvas.getContext('2d').setTransform(1, 0, 0, 1, 0, 0);
            return Promise.resolve(this.canvas);
        }
    }, {
        key: 'path',
        value: function path(_path) {
            var _this2 = this;

            this.ctx.beginPath();
            if (Array.isArray(_path)) {
                _path.forEach(function (point, index) {
                    var start = point.type === _Path.PATH.VECTOR ? point : point.start;
                    if (index === 0) {
                        _this2.ctx.moveTo(start.x, start.y);
                    } else {
                        _this2.ctx.lineTo(start.x, start.y);
                    }

                    if (point.type === _Path.PATH.BEZIER_CURVE) {
                        _this2.ctx.bezierCurveTo(point.startControl.x, point.startControl.y, point.endControl.x, point.endControl.y, point.end.x, point.end.y);
                    }
                });
            } else {
                this.ctx.arc(_path.x + _path.radius, _path.y + _path.radius, _path.radius, 0, Math.PI * 2, true);
            }

            this.ctx.closePath();
        }
    }, {
        key: 'rectangle',
        value: function rectangle(x, y, width, height, color) {
            this.ctx.fillStyle = color.toString();
            this.ctx.fillRect(x, y, width, height);
        }
    }, {
        key: 'renderLinearGradient',
        value: function renderLinearGradient(bounds, gradient) {
            var linearGradient = this.ctx.createLinearGradient(bounds.left + gradient.direction.x1, bounds.top + gradient.direction.y1, bounds.left + gradient.direction.x0, bounds.top + gradient.direction.y0);

            addColorStops(gradient, linearGradient);
            this.ctx.fillStyle = linearGradient;
            this.ctx.fillRect(bounds.left, bounds.top, bounds.width, bounds.height);
        }
    }, {
        key: 'renderRadialGradient',
        value: function renderRadialGradient(bounds, gradient) {
            var _this3 = this;

            var x = bounds.left + gradient.center.x;
            var y = bounds.top + gradient.center.y;

            var radialGradient = this.ctx.createRadialGradient(x, y, 0, x, y, gradient.radius.x);
            if (!radialGradient) {
                return;
            }

            addColorStops(gradient, radialGradient);
            this.ctx.fillStyle = radialGradient;

            if (gradient.radius.x !== gradient.radius.y) {
                // transforms for elliptical radial gradient
                var midX = bounds.left + 0.5 * bounds.width;
                var midY = bounds.top + 0.5 * bounds.height;
                var f = gradient.radius.y / gradient.radius.x;
                var invF = 1 / f;

                this.transform(midX, midY, [1, 0, 0, f, 0, 0], function () {
                    return _this3.ctx.fillRect(bounds.left, invF * (bounds.top - midY) + midY, bounds.width, bounds.height * invF);
                });
            } else {
                this.ctx.fillRect(bounds.left, bounds.top, bounds.width, bounds.height);
            }
        }
    }, {
        key: 'renderRepeat',
        value: function renderRepeat(path, image, imageSize, offsetX, offsetY) {
            this.path(path);
            this.ctx.fillStyle = this.ctx.createPattern(this.resizeImage(image, imageSize), 'repeat');
            this.ctx.translate(offsetX, offsetY);
            this.ctx.fill();
            this.ctx.translate(-offsetX, -offsetY);
        }
    }, {
        key: 'renderTextNode',
        value: function renderTextNode(textBounds, color, font, textDecoration, textShadows) {
            var _this4 = this;

            this.ctx.font = [font.fontStyle, font.fontVariant, font.fontWeight, font.fontSize, font.fontFamily].join(' ');

            textBounds.forEach(function (text) {
                _this4.ctx.fillStyle = color.toString();
                if (textShadows && text.text.trim().length) {
                    textShadows.slice(0).reverse().forEach(function (textShadow) {
                        _this4.ctx.shadowColor = textShadow.color.toString();
                        _this4.ctx.shadowOffsetX = textShadow.offsetX * _this4.options.scale;
                        _this4.ctx.shadowOffsetY = textShadow.offsetY * _this4.options.scale;
                        _this4.ctx.shadowBlur = textShadow.blur;

                        _this4.ctx.fillText(text.text, text.bounds.left, text.bounds.top + text.bounds.height);
                    });
                } else {
                    _this4.ctx.fillText(text.text, text.bounds.left, text.bounds.top + text.bounds.height);
                }

                if (textDecoration !== null) {
                    var textDecorationColor = textDecoration.textDecorationColor || color;
                    textDecoration.textDecorationLine.forEach(function (textDecorationLine) {
                        switch (textDecorationLine) {
                            case _textDecoration.TEXT_DECORATION_LINE.UNDERLINE:
                                // Draws a line at the baseline of the font
                                // TODO As some browsers display the line as more than 1px if the font-size is big,
                                // need to take that into account both in position and size
                                var _options$fontMetrics$ = _this4.options.fontMetrics.getMetrics(font),
                                    baseline = _options$fontMetrics$.baseline;

                                _this4.rectangle(text.bounds.left, Math.round(text.bounds.top + baseline), text.bounds.width, 1, textDecorationColor);
                                break;
                            case _textDecoration.TEXT_DECORATION_LINE.OVERLINE:
                                _this4.rectangle(text.bounds.left, Math.round(text.bounds.top), text.bounds.width, 1, textDecorationColor);
                                break;
                            case _textDecoration.TEXT_DECORATION_LINE.LINE_THROUGH:
                                // TODO try and find exact position for line-through
                                var _options$fontMetrics$2 = _this4.options.fontMetrics.getMetrics(font),
                                    middle = _options$fontMetrics$2.middle;

                                _this4.rectangle(text.bounds.left, Math.ceil(text.bounds.top + middle), text.bounds.width, 1, textDecorationColor);
                                break;
                        }
                    });
                }
            });
        }
    }, {
        key: 'resizeImage',
        value: function resizeImage(image, size) {
            if (image.width === size.width && image.height === size.height) {
                return image;
            }

            var canvas = this.canvas.ownerDocument.createElement('canvas');
            canvas.width = size.width;
            canvas.height = size.height;
            var ctx = canvas.getContext('2d');
            ctx.drawImage(image, 0, 0, image.width, image.height, 0, 0, size.width, size.height);
            return canvas;
        }
    }, {
        key: 'setOpacity',
        value: function setOpacity(opacity) {
            this.ctx.globalAlpha = opacity;
        }
    }, {
        key: 'transform',
        value: function transform(offsetX, offsetY, matrix, callback) {
            this.ctx.save();
            this.ctx.translate(offsetX, offsetY);
            this.ctx.transform(matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5]);
            this.ctx.translate(-offsetX, -offsetY);

            callback();

            this.ctx.restore();
        }
    }]);

    return CanvasRenderer;
}();

exports.default = CanvasRenderer;

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Logger = function () {
    function Logger(enabled, id, start) {
        _classCallCheck(this, Logger);

        this.enabled = typeof window !== 'undefined' && enabled;
        this.start = start ? start : Date.now();
        this.id = id;
    }

    _createClass(Logger, [{
        key: 'child',
        value: function child(id) {
            return new Logger(this.enabled, id, this.start);
        }

        // eslint-disable-next-line flowtype/no-weak-types

    }, {
        key: 'log',
        value: function log() {
            if (this.enabled && window.console && window.console.log) {
                for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                    args[_key] = arguments[_key];
                }

                Function.prototype.bind.call(window.console.log, window.console).apply(window.console, [Date.now() - this.start + 'ms', this.id ? 'html2canvas (' + this.id + '):' : 'html2canvas:'].concat([].slice.call(args, 0)));
            }
        }

        // eslint-disable-next-line flowtype/no-weak-types

    }, {
        key: 'error',
        value: function error() {
            if (this.enabled && window.console && window.console.error) {
                for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                    args[_key2] = arguments[_key2];
                }

                Function.prototype.bind.call(window.console.error, window.console).apply(window.console, [Date.now() - this.start + 'ms', this.id ? 'html2canvas (' + this.id + '):' : 'html2canvas:'].concat([].slice.call(args, 0)));
            }
        }
    }]);

    return Logger;
}();

exports.default = Logger;

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parsePadding = exports.PADDING_SIDES = undefined;

var _Length = __webpack_require__(1);

var _Length2 = _interopRequireDefault(_Length);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PADDING_SIDES = exports.PADDING_SIDES = {
    TOP: 0,
    RIGHT: 1,
    BOTTOM: 2,
    LEFT: 3
};

var SIDES = ['top', 'right', 'bottom', 'left'];

var parsePadding = exports.parsePadding = function parsePadding(style) {
    return SIDES.map(function (side) {
        return new _Length2.default(style.getPropertyValue('padding-' + side));
    });
};

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var OVERFLOW_WRAP = exports.OVERFLOW_WRAP = {
    NORMAL: 0,
    BREAK_WORD: 1
};

var parseOverflowWrap = exports.parseOverflowWrap = function parseOverflowWrap(overflow) {
    switch (overflow) {
        case 'break-word':
            return OVERFLOW_WRAP.BREAK_WORD;
        case 'normal':
        default:
            return OVERFLOW_WRAP.NORMAL;
    }
};

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var POSITION = exports.POSITION = {
    STATIC: 0,
    RELATIVE: 1,
    ABSOLUTE: 2,
    FIXED: 3,
    STICKY: 4
};

var parsePosition = exports.parsePosition = function parsePosition(position) {
    switch (position) {
        case 'relative':
            return POSITION.RELATIVE;
        case 'absolute':
            return POSITION.ABSOLUTE;
        case 'fixed':
            return POSITION.FIXED;
        case 'sticky':
            return POSITION.STICKY;
    }

    return POSITION.STATIC;
};

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var TEXT_TRANSFORM = exports.TEXT_TRANSFORM = {
    NONE: 0,
    LOWERCASE: 1,
    UPPERCASE: 2,
    CAPITALIZE: 3
};

var parseTextTransform = exports.parseTextTransform = function parseTextTransform(textTransform) {
    switch (textTransform) {
        case 'uppercase':
            return TEXT_TRANSFORM.UPPERCASE;
        case 'lowercase':
            return TEXT_TRANSFORM.LOWERCASE;
        case 'capitalize':
            return TEXT_TRANSFORM.CAPITALIZE;
    }

    return TEXT_TRANSFORM.NONE;
};

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.reformatInputBounds = exports.inlineSelectElement = exports.inlineTextAreaElement = exports.inlineInputElement = exports.getInputBorderRadius = exports.INPUT_BACKGROUND = exports.INPUT_BORDERS = exports.INPUT_COLOR = undefined;

var _TextContainer = __webpack_require__(9);

var _TextContainer2 = _interopRequireDefault(_TextContainer);

var _background = __webpack_require__(5);

var _border = __webpack_require__(12);

var _Circle = __webpack_require__(50);

var _Circle2 = _interopRequireDefault(_Circle);

var _Vector = __webpack_require__(7);

var _Vector2 = _interopRequireDefault(_Vector);

var _Color = __webpack_require__(0);

var _Color2 = _interopRequireDefault(_Color);

var _Length = __webpack_require__(1);

var _Length2 = _interopRequireDefault(_Length);

var _Bounds = __webpack_require__(2);

var _TextBounds = __webpack_require__(22);

var _Util = __webpack_require__(4);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var INPUT_COLOR = exports.INPUT_COLOR = new _Color2.default([42, 42, 42]);
var INPUT_BORDER_COLOR = new _Color2.default([165, 165, 165]);
var INPUT_BACKGROUND_COLOR = new _Color2.default([222, 222, 222]);
var INPUT_BORDER = {
    borderWidth: 1,
    borderColor: INPUT_BORDER_COLOR,
    borderStyle: _border.BORDER_STYLE.SOLID
};
var INPUT_BORDERS = exports.INPUT_BORDERS = [INPUT_BORDER, INPUT_BORDER, INPUT_BORDER, INPUT_BORDER];
var INPUT_BACKGROUND = exports.INPUT_BACKGROUND = {
    backgroundColor: INPUT_BACKGROUND_COLOR,
    backgroundImage: [],
    backgroundClip: _background.BACKGROUND_CLIP.PADDING_BOX,
    backgroundOrigin: _background.BACKGROUND_ORIGIN.PADDING_BOX
};

var RADIO_BORDER_RADIUS = new _Length2.default('50%');
var RADIO_BORDER_RADIUS_TUPLE = [RADIO_BORDER_RADIUS, RADIO_BORDER_RADIUS];
var INPUT_RADIO_BORDER_RADIUS = [RADIO_BORDER_RADIUS_TUPLE, RADIO_BORDER_RADIUS_TUPLE, RADIO_BORDER_RADIUS_TUPLE, RADIO_BORDER_RADIUS_TUPLE];

var CHECKBOX_BORDER_RADIUS = new _Length2.default('3px');
var CHECKBOX_BORDER_RADIUS_TUPLE = [CHECKBOX_BORDER_RADIUS, CHECKBOX_BORDER_RADIUS];
var INPUT_CHECKBOX_BORDER_RADIUS = [CHECKBOX_BORDER_RADIUS_TUPLE, CHECKBOX_BORDER_RADIUS_TUPLE, CHECKBOX_BORDER_RADIUS_TUPLE, CHECKBOX_BORDER_RADIUS_TUPLE];

var getInputBorderRadius = exports.getInputBorderRadius = function getInputBorderRadius(node) {
    return node.type === 'radio' ? INPUT_RADIO_BORDER_RADIUS : INPUT_CHECKBOX_BORDER_RADIUS;
};

var inlineInputElement = exports.inlineInputElement = function inlineInputElement(node, container) {
    if (node.type === 'radio' || node.type === 'checkbox') {
        if (node.checked) {
            var size = Math.min(container.bounds.width, container.bounds.height);
            container.childNodes.push(node.type === 'checkbox' ? [new _Vector2.default(container.bounds.left + size * 0.39363, container.bounds.top + size * 0.79), new _Vector2.default(container.bounds.left + size * 0.16, container.bounds.top + size * 0.5549), new _Vector2.default(container.bounds.left + size * 0.27347, container.bounds.top + size * 0.44071), new _Vector2.default(container.bounds.left + size * 0.39694, container.bounds.top + size * 0.5649), new _Vector2.default(container.bounds.left + size * 0.72983, container.bounds.top + size * 0.23), new _Vector2.default(container.bounds.left + size * 0.84, container.bounds.top + size * 0.34085), new _Vector2.default(container.bounds.left + size * 0.39363, container.bounds.top + size * 0.79)] : new _Circle2.default(container.bounds.left + size / 4, container.bounds.top + size / 4, size / 4));
        }
    } else {
        inlineFormElement(getInputValue(node), node, container, false);
    }
};

var inlineTextAreaElement = exports.inlineTextAreaElement = function inlineTextAreaElement(node, container) {
    inlineFormElement(node.value, node, container, true);
};

var inlineSelectElement = exports.inlineSelectElement = function inlineSelectElement(node, container) {
    var option = node.options[node.selectedIndex || 0];
    inlineFormElement(option ? option.text || '' : '', node, container, false);
};

var reformatInputBounds = exports.reformatInputBounds = function reformatInputBounds(bounds) {
    if (bounds.width > bounds.height) {
        bounds.left += (bounds.width - bounds.height) / 2;
        bounds.width = bounds.height;
    } else if (bounds.width < bounds.height) {
        bounds.top += (bounds.height - bounds.width) / 2;
        bounds.height = bounds.width;
    }
    return bounds;
};

var inlineFormElement = function inlineFormElement(value, node, container, allowLinebreak) {
    var body = node.ownerDocument.body;
    if (value.length > 0 && body) {
        var wrapper = node.ownerDocument.createElement('html2canvaswrapper');
        (0, _Util.copyCSSStyles)(node.ownerDocument.defaultView.getComputedStyle(node, null), wrapper);
        wrapper.style.position = 'absolute';
        wrapper.style.left = container.bounds.left + 'px';
        wrapper.style.top = container.bounds.top + 'px';
        if (!allowLinebreak) {
            wrapper.style.whiteSpace = 'nowrap';
        }
        var text = node.ownerDocument.createTextNode(value);
        wrapper.appendChild(text);
        body.appendChild(wrapper);
        container.childNodes.push(_TextContainer2.default.fromTextNode(text, container));
        body.removeChild(wrapper);
    }
};

var getInputValue = function getInputValue(node) {
    var value = node.type === 'password' ? new Array(node.value.length + 1).join('\u2022') : node.value;

    return value.length === 0 ? node.placeholder || '' : value;
};

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseTextBounds = exports.TextBounds = undefined;

var _Bounds = __webpack_require__(2);

var _textDecoration = __webpack_require__(11);

var _Feature = __webpack_require__(10);

var _Feature2 = _interopRequireDefault(_Feature);

var _Unicode = __webpack_require__(24);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TextBounds = exports.TextBounds = function TextBounds(text, bounds) {
    _classCallCheck(this, TextBounds);

    this.text = text;
    this.bounds = bounds;
};

var parseTextBounds = exports.parseTextBounds = function parseTextBounds(value, parent, node) {
    var letterRendering = parent.style.letterSpacing !== 0;
    var textList = letterRendering ? (0, _Unicode.toCodePoints)(value).map(function (i) {
        return (0, _Unicode.fromCodePoint)(i);
    }) : (0, _Unicode.breakWords)(value, parent);
    var length = textList.length;
    var defaultView = node.parentNode ? node.parentNode.ownerDocument.defaultView : null;
    var scrollX = defaultView ? defaultView.pageXOffset : 0;
    var scrollY = defaultView ? defaultView.pageYOffset : 0;
    var textBounds = [];
    var offset = 0;
    for (var i = 0; i < length; i++) {
        var text = textList[i];
        if (parent.style.textDecoration !== _textDecoration.TEXT_DECORATION.NONE || text.trim().length > 0) {
            if (_Feature2.default.SUPPORT_RANGE_BOUNDS) {
                textBounds.push(new TextBounds(text, getRangeBounds(node, offset, text.length, scrollX, scrollY)));
            } else {
                var replacementNode = node.splitText(text.length);
                textBounds.push(new TextBounds(text, getWrapperBounds(node, scrollX, scrollY)));
                node = replacementNode;
            }
        } else if (!_Feature2.default.SUPPORT_RANGE_BOUNDS) {
            node = node.splitText(text.length);
        }
        offset += text.length;
    }
    return textBounds;
};

var getWrapperBounds = function getWrapperBounds(node, scrollX, scrollY) {
    var wrapper = node.ownerDocument.createElement('html2canvaswrapper');
    wrapper.appendChild(node.cloneNode(true));
    var parentNode = node.parentNode;
    if (parentNode) {
        parentNode.replaceChild(wrapper, node);
        var bounds = (0, _Bounds.parseBounds)(wrapper, scrollX, scrollY);
        if (wrapper.firstChild) {
            parentNode.replaceChild(wrapper.firstChild, wrapper);
        }
        return bounds;
    }
    return new _Bounds.Bounds(0, 0, 0, 0);
};

var getRangeBounds = function getRangeBounds(node, offset, length, scrollX, scrollY) {
    var range = node.ownerDocument.createRange();
    range.setStart(node, offset);
    range.setEnd(node, offset + length);
    return _Bounds.Bounds.fromClientRect(range.getBoundingClientRect(), scrollX, scrollY);
};

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ForeignObjectRenderer = function () {
    function ForeignObjectRenderer(element) {
        _classCallCheck(this, ForeignObjectRenderer);

        this.element = element;
    }

    _createClass(ForeignObjectRenderer, [{
        key: 'render',
        value: function render(options) {
            var _this = this;

            this.options = options;
            this.canvas = document.createElement('canvas');
            this.ctx = this.canvas.getContext('2d');
            this.canvas.width = Math.floor(options.width) * options.scale;
            this.canvas.height = Math.floor(options.height) * options.scale;
            this.canvas.style.width = options.width + 'px';
            this.canvas.style.height = options.height + 'px';

            options.logger.log('ForeignObject renderer initialized (' + options.width + 'x' + options.height + ' at ' + options.x + ',' + options.y + ') with scale ' + options.scale);
            var svg = createForeignObjectSVG(Math.max(options.windowWidth, options.width) * options.scale, Math.max(options.windowHeight, options.height) * options.scale, options.scrollX * options.scale, options.scrollY * options.scale, this.element);

            return loadSerializedSVG(svg).then(function (img) {
                if (options.backgroundColor) {
                    _this.ctx.fillStyle = options.backgroundColor.toString();
                    _this.ctx.fillRect(0, 0, options.width * options.scale, options.height * options.scale);
                }

                _this.ctx.drawImage(img, -options.x * options.scale, -options.y * options.scale);
                return _this.canvas;
            });
        }
    }]);

    return ForeignObjectRenderer;
}();

exports.default = ForeignObjectRenderer;
var createForeignObjectSVG = exports.createForeignObjectSVG = function createForeignObjectSVG(width, height, x, y, node) {
    var xmlns = 'http://www.w3.org/2000/svg';
    var svg = document.createElementNS(xmlns, 'svg');
    var foreignObject = document.createElementNS(xmlns, 'foreignObject');
    svg.setAttributeNS(null, 'width', width);
    svg.setAttributeNS(null, 'height', height);

    foreignObject.setAttributeNS(null, 'width', '100%');
    foreignObject.setAttributeNS(null, 'height', '100%');
    foreignObject.setAttributeNS(null, 'x', x);
    foreignObject.setAttributeNS(null, 'y', y);
    foreignObject.setAttributeNS(null, 'externalResourcesRequired', 'true');
    svg.appendChild(foreignObject);

    foreignObject.appendChild(node);

    return svg;
};

var loadSerializedSVG = exports.loadSerializedSVG = function loadSerializedSVG(svg) {
    return new Promise(function (resolve, reject) {
        var img = new Image();
        img.onload = function () {
            return resolve(img);
        };
        img.onerror = reject;

        img.src = 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(new XMLSerializer().serializeToString(svg));
    });
};

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.breakWords = exports.fromCodePoint = exports.toCodePoints = undefined;

var _cssLineBreak = __webpack_require__(46);

Object.defineProperty(exports, 'toCodePoints', {
    enumerable: true,
    get: function get() {
        return _cssLineBreak.toCodePoints;
    }
});
Object.defineProperty(exports, 'fromCodePoint', {
    enumerable: true,
    get: function get() {
        return _cssLineBreak.fromCodePoint;
    }
});

var _NodeContainer = __webpack_require__(3);

var _NodeContainer2 = _interopRequireDefault(_NodeContainer);

var _overflowWrap = __webpack_require__(18);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var breakWords = exports.breakWords = function breakWords(str, parent) {
    var breaker = (0, _cssLineBreak.LineBreaker)(str, {
        lineBreak: parent.style.lineBreak,
        wordBreak: parent.style.overflowWrap === _overflowWrap.OVERFLOW_WRAP.BREAK_WORD ? 'break-word' : parent.style.wordBreak
    });

    var words = [];
    var bk = void 0;

    while (!(bk = breaker.next()).done) {
        words.push(bk.value.slice());
    }

    return words;
};

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.FontMetrics = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Util = __webpack_require__(4);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SAMPLE_TEXT = 'Hidden Text';

var FontMetrics = exports.FontMetrics = function () {
    function FontMetrics(document) {
        _classCallCheck(this, FontMetrics);

        this._data = {};
        this._document = document;
    }

    _createClass(FontMetrics, [{
        key: '_parseMetrics',
        value: function _parseMetrics(font) {
            var container = this._document.createElement('div');
            var img = this._document.createElement('img');
            var span = this._document.createElement('span');

            var body = this._document.body;
            if (!body) {
                throw new Error( true ? 'No document found for font metrics' : '');
            }

            container.style.visibility = 'hidden';
            container.style.fontFamily = font.fontFamily;
            container.style.fontSize = font.fontSize;
            container.style.margin = '0';
            container.style.padding = '0';

            body.appendChild(container);

            img.src = _Util.SMALL_IMAGE;
            img.width = 1;
            img.height = 1;

            img.style.margin = '0';
            img.style.padding = '0';
            img.style.verticalAlign = 'baseline';

            span.style.fontFamily = font.fontFamily;
            span.style.fontSize = font.fontSize;
            span.style.margin = '0';
            span.style.padding = '0';

            span.appendChild(this._document.createTextNode(SAMPLE_TEXT));
            container.appendChild(span);
            container.appendChild(img);
            var baseline = img.offsetTop - span.offsetTop + 2;

            container.removeChild(span);
            container.appendChild(this._document.createTextNode(SAMPLE_TEXT));

            container.style.lineHeight = 'normal';
            img.style.verticalAlign = 'super';

            var middle = img.offsetTop - container.offsetTop + 2;

            body.removeChild(container);

            return { baseline: baseline, middle: middle };
        }
    }, {
        key: 'getMetrics',
        value: function getMetrics(font) {
            var key = font.fontFamily + ' ' + font.fontSize;
            if (this._data[key] === undefined) {
                this._data[key] = this._parseMetrics(font);
            }

            return this._data[key];
        }
    }]);

    return FontMetrics;
}();

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Proxy = undefined;

var _Feature = __webpack_require__(10);

var _Feature2 = _interopRequireDefault(_Feature);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Proxy = exports.Proxy = function Proxy(src, options) {
    if (!options.proxy) {
        return Promise.reject( true ? 'No proxy defined' : null);
    }
    var proxy = options.proxy;

    return new Promise(function (resolve, reject) {
        var responseType = _Feature2.default.SUPPORT_CORS_XHR && _Feature2.default.SUPPORT_RESPONSE_TYPE ? 'blob' : 'text';
        var xhr = _Feature2.default.SUPPORT_CORS_XHR ? new XMLHttpRequest() : new XDomainRequest();
        xhr.onload = function () {
            if (xhr instanceof XMLHttpRequest) {
                if (xhr.status === 200) {
                    if (responseType === 'text') {
                        resolve(xhr.response);
                    } else {
                        var reader = new FileReader();
                        // $FlowFixMe
                        reader.addEventListener('load', function () {
                            return resolve(reader.result);
                        }, false);
                        // $FlowFixMe
                        reader.addEventListener('error', function (e) {
                            return reject(e);
                        }, false);
                        reader.readAsDataURL(xhr.response);
                    }
                } else {
                    reject( true ? 'Failed to proxy resource ' + src.substring(0, 256) + ' with status code ' + xhr.status : '');
                }
            } else {
                resolve(xhr.responseText);
            }
        };

        xhr.onerror = reject;
        xhr.open('GET', proxy + '?url=' + encodeURIComponent(src) + '&responseType=' + responseType);

        if (responseType !== 'text' && xhr instanceof XMLHttpRequest) {
            xhr.responseType = responseType;
        }

        if (options.imageTimeout) {
            var timeout = options.imageTimeout;
            xhr.timeout = timeout;
            xhr.ontimeout = function () {
                return reject( true ? 'Timed out (' + timeout + 'ms) proxying ' + src.substring(0, 256) : '');
            };
        }

        xhr.send();
    });
};

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _CanvasRenderer = __webpack_require__(15);

var _CanvasRenderer2 = _interopRequireDefault(_CanvasRenderer);

var _Logger = __webpack_require__(16);

var _Logger2 = _interopRequireDefault(_Logger);

var _Window = __webpack_require__(28);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var html2canvas = function html2canvas(element, conf) {
    var config = conf || {};
    var logger = new _Logger2.default(typeof config.logging === 'boolean' ? config.logging : true);
    logger.log('html2canvas ' + "1.0.0-alpha.11");

    if (true && typeof config.onrendered === 'function') {
        logger.error('onrendered option is deprecated, html2canvas returns a Promise with the canvas as the value');
    }

    var ownerDocument = element.ownerDocument;
    if (!ownerDocument) {
        return Promise.reject('Provided element is not within a Document');
    }
    var defaultView = ownerDocument.defaultView;

    var defaultOptions = {
        async: true,
        allowTaint: false,
        backgroundColor: '#ffffff',
        imageTimeout: 15000,
        logging: true,
        proxy: null,
        removeContainer: true,
        foreignObjectRendering: false,
        scale: defaultView.devicePixelRatio || 1,
        target: new _CanvasRenderer2.default(config.canvas),
        useCORS: false,
        windowWidth: defaultView.innerWidth,
        windowHeight: defaultView.innerHeight,
        scrollX: defaultView.pageXOffset,
        scrollY: defaultView.pageYOffset
    };

    var result = (0, _Window.renderElement)(element, _extends({}, defaultOptions, config), logger);

    if (true) {
        return result.catch(function (e) {
            logger.error(e);
            throw e;
        });
    }
    return result;
};

html2canvas.CanvasRenderer = _CanvasRenderer2.default;

module.exports = html2canvas;

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.renderElement = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _Logger = __webpack_require__(16);

var _Logger2 = _interopRequireDefault(_Logger);

var _NodeParser = __webpack_require__(29);

var _Renderer = __webpack_require__(51);

var _Renderer2 = _interopRequireDefault(_Renderer);

var _ForeignObjectRenderer = __webpack_require__(23);

var _ForeignObjectRenderer2 = _interopRequireDefault(_ForeignObjectRenderer);

var _Feature = __webpack_require__(10);

var _Feature2 = _interopRequireDefault(_Feature);

var _Bounds = __webpack_require__(2);

var _Clone = __webpack_require__(54);

var _Font = __webpack_require__(25);

var _Color = __webpack_require__(0);

var _Color2 = _interopRequireDefault(_Color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var renderElement = exports.renderElement = function renderElement(element, options, logger) {
    var ownerDocument = element.ownerDocument;

    var windowBounds = new _Bounds.Bounds(options.scrollX, options.scrollY, options.windowWidth, options.windowHeight);

    // http://www.w3.org/TR/css3-background/#special-backgrounds
    var documentBackgroundColor = ownerDocument.documentElement ? new _Color2.default(getComputedStyle(ownerDocument.documentElement).backgroundColor) : _Color.TRANSPARENT;
    var bodyBackgroundColor = ownerDocument.body ? new _Color2.default(getComputedStyle(ownerDocument.body).backgroundColor) : _Color.TRANSPARENT;

    var backgroundColor = element === ownerDocument.documentElement ? documentBackgroundColor.isTransparent() ? bodyBackgroundColor.isTransparent() ? options.backgroundColor ? new _Color2.default(options.backgroundColor) : null : bodyBackgroundColor : documentBackgroundColor : options.backgroundColor ? new _Color2.default(options.backgroundColor) : null;

    return (options.foreignObjectRendering ? // $FlowFixMe
    _Feature2.default.SUPPORT_FOREIGNOBJECT_DRAWING : Promise.resolve(false)).then(function (supportForeignObject) {
        return supportForeignObject ? function (cloner) {
            if (true) {
                logger.log('Document cloned, using foreignObject rendering');
            }

            return cloner.inlineFonts(ownerDocument).then(function () {
                return cloner.resourceLoader.ready();
            }).then(function () {
                var renderer = new _ForeignObjectRenderer2.default(cloner.documentElement);

                var defaultView = ownerDocument.defaultView;
                var scrollX = defaultView.pageXOffset;
                var scrollY = defaultView.pageYOffset;

                var isDocument = element.tagName === 'HTML' || element.tagName === 'BODY';

                var _ref = isDocument ? (0, _Bounds.parseDocumentSize)(ownerDocument) : (0, _Bounds.parseBounds)(element, scrollX, scrollY),
                    width = _ref.width,
                    height = _ref.height,
                    left = _ref.left,
                    top = _ref.top;

                return renderer.render({
                    backgroundColor: backgroundColor,
                    logger: logger,
                    scale: options.scale,
                    x: typeof options.x === 'number' ? options.x : left,
                    y: typeof options.y === 'number' ? options.y : top,
                    width: typeof options.width === 'number' ? options.width : Math.ceil(width),
                    height: typeof options.height === 'number' ? options.height : Math.ceil(height),
                    windowWidth: options.windowWidth,
                    windowHeight: options.windowHeight,
                    scrollX: options.scrollX,
                    scrollY: options.scrollY
                });
            });
        }(new _Clone.DocumentCloner(element, options, logger, true, renderElement)) : (0, _Clone.cloneWindow)(ownerDocument, windowBounds, element, options, logger, renderElement).then(function (_ref2) {
            var _ref3 = _slicedToArray(_ref2, 3),
                container = _ref3[0],
                clonedElement = _ref3[1],
                resourceLoader = _ref3[2];

            if (true) {
                logger.log('Document cloned, using computed rendering');
            }

            var stack = (0, _NodeParser.NodeParser)(clonedElement, resourceLoader, logger);
            var clonedDocument = clonedElement.ownerDocument;

            if (backgroundColor === stack.container.style.background.backgroundColor) {
                stack.container.style.background.backgroundColor = _Color.TRANSPARENT;
            }

            return resourceLoader.ready().then(function (imageStore) {
                var fontMetrics = new _Font.FontMetrics(clonedDocument);
                if (true) {
                    logger.log('Starting renderer');
                }

                var defaultView = clonedDocument.defaultView;
                var scrollX = defaultView.pageXOffset;
                var scrollY = defaultView.pageYOffset;

                var isDocument = clonedElement.tagName === 'HTML' || clonedElement.tagName === 'BODY';

                var _ref4 = isDocument ? (0, _Bounds.parseDocumentSize)(ownerDocument) : (0, _Bounds.parseBounds)(clonedElement, scrollX, scrollY),
                    width = _ref4.width,
                    height = _ref4.height,
                    left = _ref4.left,
                    top = _ref4.top;

                var renderOptions = {
                    backgroundColor: backgroundColor,
                    fontMetrics: fontMetrics,
                    imageStore: imageStore,
                    logger: logger,
                    scale: options.scale,
                    x: typeof options.x === 'number' ? options.x : left,
                    y: typeof options.y === 'number' ? options.y : top,
                    width: typeof options.width === 'number' ? options.width : Math.ceil(width),
                    height: typeof options.height === 'number' ? options.height : Math.ceil(height)
                };

                if (Array.isArray(options.target)) {
                    return Promise.all(options.target.map(function (target) {
                        var renderer = new _Renderer2.default(target, renderOptions);
                        return renderer.render(stack);
                    }));
                } else {
                    var renderer = new _Renderer2.default(options.target, renderOptions);
                    var canvas = renderer.render(stack);
                    if (options.removeContainer === true) {
                        if (container.parentNode) {
                            container.parentNode.removeChild(container);
                        } else if (true) {
                            logger.log('Cannot detach cloned iframe as it is not in the DOM anymore');
                        }
                    }

                    return canvas;
                }
            });
        });
    });
};

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.NodeParser = undefined;

var _StackingContext = __webpack_require__(30);

var _StackingContext2 = _interopRequireDefault(_StackingContext);

var _NodeContainer = __webpack_require__(3);

var _NodeContainer2 = _interopRequireDefault(_NodeContainer);

var _TextContainer = __webpack_require__(9);

var _TextContainer2 = _interopRequireDefault(_TextContainer);

var _Input = __webpack_require__(21);

var _ListItem = __webpack_require__(14);

var _listStyle = __webpack_require__(8);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NodeParser = exports.NodeParser = function NodeParser(node, resourceLoader, logger) {
    if (true) {
        logger.log('Starting node parsing');
    }

    var index = 0;

    var container = new _NodeContainer2.default(node, null, resourceLoader, index++);
    var stack = new _StackingContext2.default(container, null, true);

    parseNodeTree(node, container, stack, resourceLoader, index);

    if (true) {
        logger.log('Finished parsing node tree');
    }

    return stack;
};

var IGNORED_NODE_NAMES = ['SCRIPT', 'HEAD', 'TITLE', 'OBJECT', 'BR', 'OPTION'];

var parseNodeTree = function parseNodeTree(node, parent, stack, resourceLoader, index) {
    if (true && index > 50000) {
        throw new Error('Recursion error while parsing node tree');
    }

    for (var childNode = node.firstChild, nextNode; childNode; childNode = nextNode) {
        nextNode = childNode.nextSibling;
        var defaultView = childNode.ownerDocument.defaultView;
        if (childNode instanceof defaultView.Text || childNode instanceof Text || defaultView.parent && childNode instanceof defaultView.parent.Text) {
            if (childNode.data.trim().length > 0) {
                parent.childNodes.push(_TextContainer2.default.fromTextNode(childNode, parent));
            }
        } else if (childNode instanceof defaultView.HTMLElement || childNode instanceof HTMLElement || defaultView.parent && childNode instanceof defaultView.parent.HTMLElement) {
            if (IGNORED_NODE_NAMES.indexOf(childNode.nodeName) === -1) {
                var container = new _NodeContainer2.default(childNode, parent, resourceLoader, index++);
                if (container.isVisible()) {
                    if (childNode.tagName === 'INPUT') {
                        // $FlowFixMe
                        (0, _Input.inlineInputElement)(childNode, container);
                    } else if (childNode.tagName === 'TEXTAREA') {
                        // $FlowFixMe
                        (0, _Input.inlineTextAreaElement)(childNode, container);
                    } else if (childNode.tagName === 'SELECT') {
                        // $FlowFixMe
                        (0, _Input.inlineSelectElement)(childNode, container);
                    } else if (container.style.listStyle && container.style.listStyle.listStyleType !== _listStyle.LIST_STYLE_TYPE.NONE) {
                        (0, _ListItem.inlineListItemElement)(childNode, container, resourceLoader);
                    }

                    var SHOULD_TRAVERSE_CHILDREN = childNode.tagName !== 'TEXTAREA';
                    var treatAsRealStackingContext = createsRealStackingContext(container, childNode);
                    if (treatAsRealStackingContext || createsStackingContext(container)) {
                        // for treatAsRealStackingContext:false, any positioned descendants and descendants
                        // which actually create a new stacking context should be considered part of the parent stacking context
                        var parentStack = treatAsRealStackingContext || container.isPositioned() ? stack.getRealParentStackingContext() : stack;
                        var childStack = new _StackingContext2.default(container, parentStack, treatAsRealStackingContext);
                        parentStack.contexts.push(childStack);
                        if (SHOULD_TRAVERSE_CHILDREN) {
                            parseNodeTree(childNode, container, childStack, resourceLoader, index);
                        }
                    } else {
                        stack.children.push(container);
                        if (SHOULD_TRAVERSE_CHILDREN) {
                            parseNodeTree(childNode, container, stack, resourceLoader, index);
                        }
                    }
                }
            }
        } else if (childNode instanceof defaultView.SVGSVGElement || childNode instanceof SVGSVGElement || defaultView.parent && childNode instanceof defaultView.parent.SVGSVGElement) {
            var _container = new _NodeContainer2.default(childNode, parent, resourceLoader, index++);
            var _treatAsRealStackingContext = createsRealStackingContext(_container, childNode);
            if (_treatAsRealStackingContext || createsStackingContext(_container)) {
                // for treatAsRealStackingContext:false, any positioned descendants and descendants
                // which actually create a new stacking context should be considered part of the parent stacking context
                var _parentStack = _treatAsRealStackingContext || _container.isPositioned() ? stack.getRealParentStackingContext() : stack;
                var _childStack = new _StackingContext2.default(_container, _parentStack, _treatAsRealStackingContext);
                _parentStack.contexts.push(_childStack);
            } else {
                stack.children.push(_container);
            }
        }
    }
};

var createsRealStackingContext = function createsRealStackingContext(container, node) {
    return container.isRootElement() || container.isPositionedWithZIndex() || container.style.opacity < 1 || container.isTransformed() || isBodyWithTransparentRoot(container, node);
};

var createsStackingContext = function createsStackingContext(container) {
    return container.isPositioned() || container.isFloating();
};

var isBodyWithTransparentRoot = function isBodyWithTransparentRoot(container, node) {
    return node.nodeName === 'BODY' && container.parent instanceof _NodeContainer2.default && container.parent.style.background.backgroundColor.isTransparent();
};

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _NodeContainer = __webpack_require__(3);

var _NodeContainer2 = _interopRequireDefault(_NodeContainer);

var _position = __webpack_require__(19);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var StackingContext = function () {
    function StackingContext(container, parent, treatAsRealStackingContext) {
        _classCallCheck(this, StackingContext);

        this.container = container;
        this.parent = parent;
        this.contexts = [];
        this.children = [];
        this.treatAsRealStackingContext = treatAsRealStackingContext;
    }

    _createClass(StackingContext, [{
        key: 'getOpacity',
        value: function getOpacity() {
            return this.parent ? this.container.style.opacity * this.parent.getOpacity() : this.container.style.opacity;
        }
    }, {
        key: 'getRealParentStackingContext',
        value: function getRealParentStackingContext() {
            return !this.parent || this.treatAsRealStackingContext ? this : this.parent.getRealParentStackingContext();
        }
    }]);

    return StackingContext;
}();

exports.default = StackingContext;

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Size = function Size(width, height) {
    _classCallCheck(this, Size);

    this.width = width;
    this.height = height;
};

exports.default = Size;

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Path = __webpack_require__(6);

var _Vector = __webpack_require__(7);

var _Vector2 = _interopRequireDefault(_Vector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var lerp = function lerp(a, b, t) {
    return new _Vector2.default(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t);
};

var BezierCurve = function () {
    function BezierCurve(start, startControl, endControl, end) {
        _classCallCheck(this, BezierCurve);

        this.type = _Path.PATH.BEZIER_CURVE;
        this.start = start;
        this.startControl = startControl;
        this.endControl = endControl;
        this.end = end;
    }

    _createClass(BezierCurve, [{
        key: 'subdivide',
        value: function subdivide(t, firstHalf) {
            var ab = lerp(this.start, this.startControl, t);
            var bc = lerp(this.startControl, this.endControl, t);
            var cd = lerp(this.endControl, this.end, t);
            var abbc = lerp(ab, bc, t);
            var bccd = lerp(bc, cd, t);
            var dest = lerp(abbc, bccd, t);
            return firstHalf ? new BezierCurve(this.start, ab, abbc, dest) : new BezierCurve(dest, bccd, cd, this.end);
        }
    }, {
        key: 'reverse',
        value: function reverse() {
            return new BezierCurve(this.end, this.endControl, this.startControl, this.start);
        }
    }]);

    return BezierCurve;
}();

exports.default = BezierCurve;

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseBorderRadius = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _Length = __webpack_require__(1);

var _Length2 = _interopRequireDefault(_Length);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SIDES = ['top-left', 'top-right', 'bottom-right', 'bottom-left'];

var parseBorderRadius = exports.parseBorderRadius = function parseBorderRadius(style) {
    return SIDES.map(function (side) {
        var value = style.getPropertyValue('border-' + side + '-radius');

        var _value$split$map = value.split(' ').map(_Length2.default.create),
            _value$split$map2 = _slicedToArray(_value$split$map, 2),
            horizontal = _value$split$map2[0],
            vertical = _value$split$map2[1];

        return typeof vertical === 'undefined' ? [horizontal, horizontal] : [horizontal, vertical];
    });
};

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var DISPLAY = exports.DISPLAY = {
    NONE: 1 << 0,
    BLOCK: 1 << 1,
    INLINE: 1 << 2,
    RUN_IN: 1 << 3,
    FLOW: 1 << 4,
    FLOW_ROOT: 1 << 5,
    TABLE: 1 << 6,
    FLEX: 1 << 7,
    GRID: 1 << 8,
    RUBY: 1 << 9,
    SUBGRID: 1 << 10,
    LIST_ITEM: 1 << 11,
    TABLE_ROW_GROUP: 1 << 12,
    TABLE_HEADER_GROUP: 1 << 13,
    TABLE_FOOTER_GROUP: 1 << 14,
    TABLE_ROW: 1 << 15,
    TABLE_CELL: 1 << 16,
    TABLE_COLUMN_GROUP: 1 << 17,
    TABLE_COLUMN: 1 << 18,
    TABLE_CAPTION: 1 << 19,
    RUBY_BASE: 1 << 20,
    RUBY_TEXT: 1 << 21,
    RUBY_BASE_CONTAINER: 1 << 22,
    RUBY_TEXT_CONTAINER: 1 << 23,
    CONTENTS: 1 << 24,
    INLINE_BLOCK: 1 << 25,
    INLINE_LIST_ITEM: 1 << 26,
    INLINE_TABLE: 1 << 27,
    INLINE_FLEX: 1 << 28,
    INLINE_GRID: 1 << 29
};

var parseDisplayValue = function parseDisplayValue(display) {
    switch (display) {
        case 'block':
            return DISPLAY.BLOCK;
        case 'inline':
            return DISPLAY.INLINE;
        case 'run-in':
            return DISPLAY.RUN_IN;
        case 'flow':
            return DISPLAY.FLOW;
        case 'flow-root':
            return DISPLAY.FLOW_ROOT;
        case 'table':
            return DISPLAY.TABLE;
        case 'flex':
            return DISPLAY.FLEX;
        case 'grid':
            return DISPLAY.GRID;
        case 'ruby':
            return DISPLAY.RUBY;
        case 'subgrid':
            return DISPLAY.SUBGRID;
        case 'list-item':
            return DISPLAY.LIST_ITEM;
        case 'table-row-group':
            return DISPLAY.TABLE_ROW_GROUP;
        case 'table-header-group':
            return DISPLAY.TABLE_HEADER_GROUP;
        case 'table-footer-group':
            return DISPLAY.TABLE_FOOTER_GROUP;
        case 'table-row':
            return DISPLAY.TABLE_ROW;
        case 'table-cell':
            return DISPLAY.TABLE_CELL;
        case 'table-column-group':
            return DISPLAY.TABLE_COLUMN_GROUP;
        case 'table-column':
            return DISPLAY.TABLE_COLUMN;
        case 'table-caption':
            return DISPLAY.TABLE_CAPTION;
        case 'ruby-base':
            return DISPLAY.RUBY_BASE;
        case 'ruby-text':
            return DISPLAY.RUBY_TEXT;
        case 'ruby-base-container':
            return DISPLAY.RUBY_BASE_CONTAINER;
        case 'ruby-text-container':
            return DISPLAY.RUBY_TEXT_CONTAINER;
        case 'contents':
            return DISPLAY.CONTENTS;
        case 'inline-block':
            return DISPLAY.INLINE_BLOCK;
        case 'inline-list-item':
            return DISPLAY.INLINE_LIST_ITEM;
        case 'inline-table':
            return DISPLAY.INLINE_TABLE;
        case 'inline-flex':
            return DISPLAY.INLINE_FLEX;
        case 'inline-grid':
            return DISPLAY.INLINE_GRID;
    }

    return DISPLAY.NONE;
};

var setDisplayBit = function setDisplayBit(bit, display) {
    return bit | parseDisplayValue(display);
};

var parseDisplay = exports.parseDisplay = function parseDisplay(display) {
    return display.split(' ').reduce(setDisplayBit, 0);
};

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var FLOAT = exports.FLOAT = {
    NONE: 0,
    LEFT: 1,
    RIGHT: 2,
    INLINE_START: 3,
    INLINE_END: 4
};

var parseCSSFloat = exports.parseCSSFloat = function parseCSSFloat(float) {
    switch (float) {
        case 'left':
            return FLOAT.LEFT;
        case 'right':
            return FLOAT.RIGHT;
        case 'inline-start':
            return FLOAT.INLINE_START;
        case 'inline-end':
            return FLOAT.INLINE_END;
    }
    return FLOAT.NONE;
};

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});


var parseFontWeight = function parseFontWeight(weight) {
    switch (weight) {
        case 'normal':
            return 400;
        case 'bold':
            return 700;
    }

    var value = parseInt(weight, 10);
    return isNaN(value) ? 400 : value;
};

var parseFont = exports.parseFont = function parseFont(style) {
    var fontFamily = style.fontFamily;
    var fontSize = style.fontSize;
    var fontStyle = style.fontStyle;
    var fontVariant = style.fontVariant;
    var fontWeight = parseFontWeight(style.fontWeight);

    return {
        fontFamily: fontFamily,
        fontSize: fontSize,
        fontStyle: fontStyle,
        fontVariant: fontVariant,
        fontWeight: fontWeight
    };
};

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var parseLetterSpacing = exports.parseLetterSpacing = function parseLetterSpacing(letterSpacing) {
    if (letterSpacing === 'normal') {
        return 0;
    }
    var value = parseFloat(letterSpacing);
    return isNaN(value) ? 0 : value;
};

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var LINE_BREAK = exports.LINE_BREAK = {
    NORMAL: 'normal',
    STRICT: 'strict'
};

var parseLineBreak = exports.parseLineBreak = function parseLineBreak(wordBreak) {
    switch (wordBreak) {
        case 'strict':
            return LINE_BREAK.STRICT;
        case 'normal':
        default:
            return LINE_BREAK.NORMAL;
    }
};

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseMargin = undefined;

var _Length = __webpack_require__(1);

var _Length2 = _interopRequireDefault(_Length);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SIDES = ['top', 'right', 'bottom', 'left'];

var parseMargin = exports.parseMargin = function parseMargin(style) {
    return SIDES.map(function (side) {
        return new _Length2.default(style.getPropertyValue('margin-' + side));
    });
};

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var OVERFLOW = exports.OVERFLOW = {
    VISIBLE: 0,
    HIDDEN: 1,
    SCROLL: 2,
    AUTO: 3
};

var parseOverflow = exports.parseOverflow = function parseOverflow(overflow) {
    switch (overflow) {
        case 'hidden':
            return OVERFLOW.HIDDEN;
        case 'scroll':
            return OVERFLOW.SCROLL;
        case 'auto':
            return OVERFLOW.AUTO;
        case 'visible':
        default:
            return OVERFLOW.VISIBLE;
    }
};

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseTextShadow = undefined;

var _Color = __webpack_require__(0);

var _Color2 = _interopRequireDefault(_Color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var NUMBER = /^([+-]|\d|\.)$/i;

var parseTextShadow = exports.parseTextShadow = function parseTextShadow(textShadow) {
    if (textShadow === 'none' || typeof textShadow !== 'string') {
        return null;
    }

    var currentValue = '';
    var isLength = false;
    var values = [];
    var shadows = [];
    var numParens = 0;
    var color = null;

    var appendValue = function appendValue() {
        if (currentValue.length) {
            if (isLength) {
                values.push(parseFloat(currentValue));
            } else {
                color = new _Color2.default(currentValue);
            }
        }
        isLength = false;
        currentValue = '';
    };

    var appendShadow = function appendShadow() {
        if (values.length && color !== null) {
            shadows.push({
                color: color,
                offsetX: values[0] || 0,
                offsetY: values[1] || 0,
                blur: values[2] || 0
            });
        }
        values.splice(0, values.length);
        color = null;
    };

    for (var i = 0; i < textShadow.length; i++) {
        var c = textShadow[i];
        switch (c) {
            case '(':
                currentValue += c;
                numParens++;
                break;
            case ')':
                currentValue += c;
                numParens--;
                break;
            case ',':
                if (numParens === 0) {
                    appendValue();
                    appendShadow();
                } else {
                    currentValue += c;
                }
                break;
            case ' ':
                if (numParens === 0) {
                    appendValue();
                } else {
                    currentValue += c;
                }
                break;
            default:
                if (currentValue.length === 0 && NUMBER.test(c)) {
                    isLength = true;
                }
                currentValue += c;
        }
    }

    appendValue();
    appendShadow();

    if (shadows.length === 0) {
        return null;
    }

    return shadows;
};

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseTransform = undefined;

var _Length = __webpack_require__(1);

var _Length2 = _interopRequireDefault(_Length);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var toFloat = function toFloat(s) {
    return parseFloat(s.trim());
};

var MATRIX = /(matrix|matrix3d)\((.+)\)/;

var parseTransform = exports.parseTransform = function parseTransform(style) {
    var transform = parseTransformMatrix(style.transform || style.webkitTransform || style.mozTransform ||
    // $FlowFixMe
    style.msTransform ||
    // $FlowFixMe
    style.oTransform);
    if (transform === null) {
        return null;
    }

    return {
        transform: transform,
        transformOrigin: parseTransformOrigin(style.transformOrigin || style.webkitTransformOrigin || style.mozTransformOrigin ||
        // $FlowFixMe
        style.msTransformOrigin ||
        // $FlowFixMe
        style.oTransformOrigin)
    };
};

// $FlowFixMe
var parseTransformOrigin = function parseTransformOrigin(origin) {
    if (typeof origin !== 'string') {
        var v = new _Length2.default('0');
        return [v, v];
    }
    var values = origin.split(' ').map(_Length2.default.create);
    return [values[0], values[1]];
};

// $FlowFixMe
var parseTransformMatrix = function parseTransformMatrix(transform) {
    if (transform === 'none' || typeof transform !== 'string') {
        return null;
    }

    var match = transform.match(MATRIX);
    if (match) {
        if (match[1] === 'matrix') {
            var matrix = match[2].split(',').map(toFloat);
            return [matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5]];
        } else {
            var matrix3d = match[2].split(',').map(toFloat);
            return [matrix3d[0], matrix3d[1], matrix3d[4], matrix3d[5], matrix3d[12], matrix3d[13]];
        }
    }
    return null;
};

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var VISIBILITY = exports.VISIBILITY = {
    VISIBLE: 0,
    HIDDEN: 1,
    COLLAPSE: 2
};

var parseVisibility = exports.parseVisibility = function parseVisibility(visibility) {
    switch (visibility) {
        case 'hidden':
            return VISIBILITY.HIDDEN;
        case 'collapse':
            return VISIBILITY.COLLAPSE;
        case 'visible':
        default:
            return VISIBILITY.VISIBLE;
    }
};

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var WORD_BREAK = exports.WORD_BREAK = {
    NORMAL: 'normal',
    BREAK_ALL: 'break-all',
    KEEP_ALL: 'keep-all'
};

var parseWordBreak = exports.parseWordBreak = function parseWordBreak(wordBreak) {
    switch (wordBreak) {
        case 'break-all':
            return WORD_BREAK.BREAK_ALL;
        case 'keep-all':
            return WORD_BREAK.KEEP_ALL;
        case 'normal':
        default:
            return WORD_BREAK.NORMAL;
    }
};

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var parseZIndex = exports.parseZIndex = function parseZIndex(zIndex) {
    var auto = zIndex === 'auto';
    return {
        auto: auto,
        order: auto ? 0 : parseInt(zIndex, 10)
    };
};

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Util = __webpack_require__(13);

Object.defineProperty(exports, 'toCodePoints', {
  enumerable: true,
  get: function get() {
    return _Util.toCodePoints;
  }
});
Object.defineProperty(exports, 'fromCodePoint', {
  enumerable: true,
  get: function get() {
    return _Util.fromCodePoint;
  }
});

var _LineBreak = __webpack_require__(47);

Object.defineProperty(exports, 'LineBreaker', {
  enumerable: true,
  get: function get() {
    return _LineBreak.LineBreaker;
  }
});

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.LineBreaker = exports.inlineBreakOpportunities = exports.lineBreakAtIndex = exports.codePointsToCharacterClasses = exports.UnicodeTrie = exports.BREAK_ALLOWED = exports.BREAK_NOT_ALLOWED = exports.BREAK_MANDATORY = exports.classes = exports.LETTER_NUMBER_MODIFIER = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _Trie = __webpack_require__(48);

var _linebreakTrie = __webpack_require__(49);

var _linebreakTrie2 = _interopRequireDefault(_linebreakTrie);

var _Util = __webpack_require__(13);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var LETTER_NUMBER_MODIFIER = exports.LETTER_NUMBER_MODIFIER = 50;

// Non-tailorable Line Breaking Classes
var BK = 1; //  Cause a line break (after)
var CR = 2; //  Cause a line break (after), except between CR and LF
var LF = 3; //  Cause a line break (after)
var CM = 4; //  Prohibit a line break between the character and the preceding character
var NL = 5; //  Cause a line break (after)
var SG = 6; //  Do not occur in well-formed text
var WJ = 7; //  Prohibit line breaks before and after
var ZW = 8; //  Provide a break opportunity
var GL = 9; //  Prohibit line breaks before and after
var SP = 10; // Enable indirect line breaks
var ZWJ = 11; // Prohibit line breaks within joiner sequences
// Break Opportunities
var B2 = 12; //  Provide a line break opportunity before and after the character
var BA = 13; //  Generally provide a line break opportunity after the character
var BB = 14; //  Generally provide a line break opportunity before the character
var HY = 15; //  Provide a line break opportunity after the character, except in numeric context
var CB = 16; //   Provide a line break opportunity contingent on additional information
// Characters Prohibiting Certain Breaks
var CL = 17; //  Prohibit line breaks before
var CP = 18; //  Prohibit line breaks before
var EX = 19; //  Prohibit line breaks before
var IN = 20; //  Allow only indirect line breaks between pairs
var NS = 21; //  Allow only indirect line breaks before
var OP = 22; //  Prohibit line breaks after
var QU = 23; //  Act like they are both opening and closing
// Numeric Context
var IS = 24; //  Prevent breaks after any and before numeric
var NU = 25; //  Form numeric expressions for line breaking purposes
var PO = 26; //  Do not break following a numeric expression
var PR = 27; //  Do not break in front of a numeric expression
var SY = 28; //  Prevent a break before; and allow a break after
// Other Characters
var AI = 29; //  Act like AL when the resolvedEAW is N; otherwise; act as ID
var AL = 30; //  Are alphabetic characters or symbols that are used with alphabetic characters
var CJ = 31; //  Treat as NS or ID for strict or normal breaking.
var EB = 32; //  Do not break from following Emoji Modifier
var EM = 33; //  Do not break from preceding Emoji Base
var H2 = 34; //  Form Korean syllable blocks
var H3 = 35; //  Form Korean syllable blocks
var HL = 36; //  Do not break around a following hyphen; otherwise act as Alphabetic
var ID = 37; //  Break before or after; except in some numeric context
var JL = 38; //  Form Korean syllable blocks
var JV = 39; //  Form Korean syllable blocks
var JT = 40; //  Form Korean syllable blocks
var RI = 41; //  Keep pairs together. For pairs; break before and after other classes
var SA = 42; //  Provide a line break opportunity contingent on additional, language-specific context analysis
var XX = 43; //  Have as yet unknown line breaking behavior or unassigned code positions

var classes = exports.classes = {
    BK: BK,
    CR: CR,
    LF: LF,
    CM: CM,
    NL: NL,
    SG: SG,
    WJ: WJ,
    ZW: ZW,
    GL: GL,
    SP: SP,
    ZWJ: ZWJ,
    B2: B2,
    BA: BA,
    BB: BB,
    HY: HY,
    CB: CB,
    CL: CL,
    CP: CP,
    EX: EX,
    IN: IN,
    NS: NS,
    OP: OP,
    QU: QU,
    IS: IS,
    NU: NU,
    PO: PO,
    PR: PR,
    SY: SY,
    AI: AI,
    AL: AL,
    CJ: CJ,
    EB: EB,
    EM: EM,
    H2: H2,
    H3: H3,
    HL: HL,
    ID: ID,
    JL: JL,
    JV: JV,
    JT: JT,
    RI: RI,
    SA: SA,
    XX: XX
};

var BREAK_MANDATORY = exports.BREAK_MANDATORY = '!';
var BREAK_NOT_ALLOWED = exports.BREAK_NOT_ALLOWED = '×';
var BREAK_ALLOWED = exports.BREAK_ALLOWED = '÷';
var UnicodeTrie = exports.UnicodeTrie = (0, _Trie.createTrieFromBase64)(_linebreakTrie2.default);

var ALPHABETICS = [AL, HL];
var HARD_LINE_BREAKS = [BK, CR, LF, NL];
var SPACE = [SP, ZW];
var PREFIX_POSTFIX = [PR, PO];
var LINE_BREAKS = HARD_LINE_BREAKS.concat(SPACE);
var KOREAN_SYLLABLE_BLOCK = [JL, JV, JT, H2, H3];
var HYPHEN = [HY, BA];

var codePointsToCharacterClasses = exports.codePointsToCharacterClasses = function codePointsToCharacterClasses(codePoints) {
    var lineBreak = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'strict';

    var types = [];
    var indicies = [];
    var categories = [];
    codePoints.forEach(function (codePoint, index) {
        var classType = UnicodeTrie.get(codePoint);
        if (classType > LETTER_NUMBER_MODIFIER) {
            categories.push(true);
            classType -= LETTER_NUMBER_MODIFIER;
        } else {
            categories.push(false);
        }

        if (['normal', 'auto', 'loose'].indexOf(lineBreak) !== -1) {
            // U+2010, – U+2013, 〜 U+301C, ゠ U+30A0
            if ([0x2010, 0x2013, 0x301c, 0x30a0].indexOf(codePoint) !== -1) {
                indicies.push(index);
                return types.push(CB);
            }
        }

        if (classType === CM || classType === ZWJ) {
            // LB10 Treat any remaining combining mark or ZWJ as AL.
            if (index === 0) {
                indicies.push(index);
                return types.push(AL);
            }

            // LB9 Do not break a combining character sequence; treat it as if it has the line breaking class of
            // the base character in all of the following rules. Treat ZWJ as if it were CM.
            var prev = types[index - 1];
            if (LINE_BREAKS.indexOf(prev) === -1) {
                indicies.push(indicies[index - 1]);
                return types.push(prev);
            }
            indicies.push(index);
            return types.push(AL);
        }

        indicies.push(index);

        if (classType === CJ) {
            return types.push(lineBreak === 'strict' ? NS : ID);
        }

        if (classType === SA) {
            return types.push(AL);
        }

        if (classType === AI) {
            return types.push(AL);
        }

        // For supplementary characters, a useful default is to treat characters in the range 10000..1FFFD as AL
        // and characters in the ranges 20000..2FFFD and 30000..3FFFD as ID, until the implementation can be revised
        // to take into account the actual line breaking properties for these characters.
        if (classType === XX) {
            if (codePoint >= 0x20000 && codePoint <= 0x2fffd || codePoint >= 0x30000 && codePoint <= 0x3fffd) {
                return types.push(ID);
            } else {
                return types.push(AL);
            }
        }

        types.push(classType);
    });

    return [indicies, types, categories];
};

var isAdjacentWithSpaceIgnored = function isAdjacentWithSpaceIgnored(a, b, currentIndex, classTypes) {
    var current = classTypes[currentIndex];
    if (Array.isArray(a) ? a.indexOf(current) !== -1 : a === current) {
        var i = currentIndex;
        while (i <= classTypes.length) {
            i++;
            var next = classTypes[i];

            if (next === b) {
                return true;
            }

            if (next !== SP) {
                break;
            }
        }
    }

    if (current === SP) {
        var _i = currentIndex;

        while (_i > 0) {
            _i--;
            var prev = classTypes[_i];

            if (Array.isArray(a) ? a.indexOf(prev) !== -1 : a === prev) {
                var n = currentIndex;
                while (n <= classTypes.length) {
                    n++;
                    var _next = classTypes[n];

                    if (_next === b) {
                        return true;
                    }

                    if (_next !== SP) {
                        break;
                    }
                }
            }

            if (prev !== SP) {
                break;
            }
        }
    }
    return false;
};

var previousNonSpaceClassType = function previousNonSpaceClassType(currentIndex, classTypes) {
    var i = currentIndex;
    while (i >= 0) {
        var type = classTypes[i];
        if (type === SP) {
            i--;
        } else {
            return type;
        }
    }
    return 0;
};

var _lineBreakAtIndex = function _lineBreakAtIndex(codePoints, classTypes, indicies, index, forbiddenBreaks) {
    if (indicies[index] === 0) {
        return BREAK_NOT_ALLOWED;
    }

    var currentIndex = index - 1;
    if (Array.isArray(forbiddenBreaks) && forbiddenBreaks[currentIndex] === true) {
        return BREAK_NOT_ALLOWED;
    }

    var beforeIndex = currentIndex - 1;
    var afterIndex = currentIndex + 1;
    var current = classTypes[currentIndex];

    // LB4 Always break after hard line breaks.
    // LB5 Treat CR followed by LF, as well as CR, LF, and NL as hard line breaks.
    var before = beforeIndex >= 0 ? classTypes[beforeIndex] : 0;
    var next = classTypes[afterIndex];

    if (current === CR && next === LF) {
        return BREAK_NOT_ALLOWED;
    }

    if (HARD_LINE_BREAKS.indexOf(current) !== -1) {
        return BREAK_MANDATORY;
    }

    // LB6 Do not break before hard line breaks.
    if (HARD_LINE_BREAKS.indexOf(next) !== -1) {
        return BREAK_NOT_ALLOWED;
    }

    // LB7 Do not break before spaces or zero width space.
    if (SPACE.indexOf(next) !== -1) {
        return BREAK_NOT_ALLOWED;
    }

    // LB8 Break before any character following a zero-width space, even if one or more spaces intervene.
    if (previousNonSpaceClassType(currentIndex, classTypes) === ZW) {
        return BREAK_ALLOWED;
    }

    // LB8a Do not break between a zero width joiner and an ideograph, emoji base or emoji modifier.
    if (UnicodeTrie.get(codePoints[currentIndex]) === ZWJ && (next === ID || next === EB || next === EM)) {
        return BREAK_NOT_ALLOWED;
    }

    // LB11 Do not break before or after Word joiner and related characters.
    if (current === WJ || next === WJ) {
        return BREAK_NOT_ALLOWED;
    }

    // LB12 Do not break after NBSP and related characters.
    if (current === GL) {
        return BREAK_NOT_ALLOWED;
    }

    // LB12a Do not break before NBSP and related characters, except after spaces and hyphens.
    if ([SP, BA, HY].indexOf(current) === -1 && next === GL) {
        return BREAK_NOT_ALLOWED;
    }

    // LB13 Do not break before ‘]’ or ‘!’ or ‘;’ or ‘/’, even after spaces.
    if ([CL, CP, EX, IS, SY].indexOf(next) !== -1) {
        return BREAK_NOT_ALLOWED;
    }

    // LB14 Do not break after ‘[’, even after spaces.
    if (previousNonSpaceClassType(currentIndex, classTypes) === OP) {
        return BREAK_NOT_ALLOWED;
    }

    // LB15 Do not break within ‘”[’, even with intervening spaces.
    if (isAdjacentWithSpaceIgnored(QU, OP, currentIndex, classTypes)) {
        return BREAK_NOT_ALLOWED;
    }

    // LB16 Do not break between closing punctuation and a nonstarter (lb=NS), even with intervening spaces.
    if (isAdjacentWithSpaceIgnored([CL, CP], NS, currentIndex, classTypes)) {
        return BREAK_NOT_ALLOWED;
    }

    // LB17 Do not break within ‘——’, even with intervening spaces.
    if (isAdjacentWithSpaceIgnored(B2, B2, currentIndex, classTypes)) {
        return BREAK_NOT_ALLOWED;
    }

    // LB18 Break after spaces.
    if (current === SP) {
        return BREAK_ALLOWED;
    }

    // LB19 Do not break before or after quotation marks, such as ‘ ” ’.
    if (current === QU || next === QU) {
        return BREAK_NOT_ALLOWED;
    }

    // LB20 Break before and after unresolved CB.
    if (next === CB || current === CB) {
        return BREAK_ALLOWED;
    }

    // LB21 Do not break before hyphen-minus, other hyphens, fixed-width spaces, small kana, and other non-starters, or after acute accents.
    if ([BA, HY, NS].indexOf(next) !== -1 || current === BB) {
        return BREAK_NOT_ALLOWED;
    }

    // LB21a Don't break after Hebrew + Hyphen.
    if (before === HL && HYPHEN.indexOf(current) !== -1) {
        return BREAK_NOT_ALLOWED;
    }

    // LB21b Don’t break between Solidus and Hebrew letters.
    if (current === SY && next === HL) {
        return BREAK_NOT_ALLOWED;
    }

    // LB22 Do not break between two ellipses, or between letters, numbers or exclamations and ellipsis.
    if (next === IN && ALPHABETICS.concat(IN, EX, NU, ID, EB, EM).indexOf(current) !== -1) {
        return BREAK_NOT_ALLOWED;
    }

    // LB23 Do not break between digits and letters.
    if (ALPHABETICS.indexOf(next) !== -1 && current === NU || ALPHABETICS.indexOf(current) !== -1 && next === NU) {
        return BREAK_NOT_ALLOWED;
    }

    // LB23a Do not break between numeric prefixes and ideographs, or between ideographs and numeric postfixes.
    if (current === PR && [ID, EB, EM].indexOf(next) !== -1 || [ID, EB, EM].indexOf(current) !== -1 && next === PO) {
        return BREAK_NOT_ALLOWED;
    }

    // LB24 Do not break between numeric prefix/postfix and letters, or between letters and prefix/postfix.
    if (ALPHABETICS.indexOf(current) !== -1 && PREFIX_POSTFIX.indexOf(next) !== -1 || PREFIX_POSTFIX.indexOf(current) !== -1 && ALPHABETICS.indexOf(next) !== -1) {
        return BREAK_NOT_ALLOWED;
    }

    // LB25 Do not break between the following pairs of classes relevant to numbers:
    if (
    // (PR | PO) × ( OP | HY )? NU
    [PR, PO].indexOf(current) !== -1 && (next === NU || [OP, HY].indexOf(next) !== -1 && classTypes[afterIndex + 1] === NU) ||
    // ( OP | HY ) × NU
    [OP, HY].indexOf(current) !== -1 && next === NU ||
    // NU ×	(NU | SY | IS)
    current === NU && [NU, SY, IS].indexOf(next) !== -1) {
        return BREAK_NOT_ALLOWED;
    }

    // NU (NU | SY | IS)* × (NU | SY | IS | CL | CP)
    if ([NU, SY, IS, CL, CP].indexOf(next) !== -1) {
        var prevIndex = currentIndex;
        while (prevIndex >= 0) {
            var type = classTypes[prevIndex];
            if (type === NU) {
                return BREAK_NOT_ALLOWED;
            } else if ([SY, IS].indexOf(type) !== -1) {
                prevIndex--;
            } else {
                break;
            }
        }
    }

    // NU (NU | SY | IS)* (CL | CP)? × (PO | PR))
    if ([PR, PO].indexOf(next) !== -1) {
        var _prevIndex = [CL, CP].indexOf(current) !== -1 ? beforeIndex : currentIndex;
        while (_prevIndex >= 0) {
            var _type = classTypes[_prevIndex];
            if (_type === NU) {
                return BREAK_NOT_ALLOWED;
            } else if ([SY, IS].indexOf(_type) !== -1) {
                _prevIndex--;
            } else {
                break;
            }
        }
    }

    // LB26 Do not break a Korean syllable.
    if (JL === current && [JL, JV, H2, H3].indexOf(next) !== -1 || [JV, H2].indexOf(current) !== -1 && [JV, JT].indexOf(next) !== -1 || [JT, H3].indexOf(current) !== -1 && next === JT) {
        return BREAK_NOT_ALLOWED;
    }

    // LB27 Treat a Korean Syllable Block the same as ID.
    if (KOREAN_SYLLABLE_BLOCK.indexOf(current) !== -1 && [IN, PO].indexOf(next) !== -1 || KOREAN_SYLLABLE_BLOCK.indexOf(next) !== -1 && current === PR) {
        return BREAK_NOT_ALLOWED;
    }

    // LB28 Do not break between alphabetics (“at”).
    if (ALPHABETICS.indexOf(current) !== -1 && ALPHABETICS.indexOf(next) !== -1) {
        return BREAK_NOT_ALLOWED;
    }

    // LB29 Do not break between numeric punctuation and alphabetics (“e.g.”).
    if (current === IS && ALPHABETICS.indexOf(next) !== -1) {
        return BREAK_NOT_ALLOWED;
    }

    // LB30 Do not break between letters, numbers, or ordinary symbols and opening or closing parentheses.
    if (ALPHABETICS.concat(NU).indexOf(current) !== -1 && next === OP || ALPHABETICS.concat(NU).indexOf(next) !== -1 && current === CP) {
        return BREAK_NOT_ALLOWED;
    }

    // LB30a Break between two regional indicator symbols if and only if there are an even number of regional
    // indicators preceding the position of the break.
    if (current === RI && next === RI) {
        var i = indicies[currentIndex];
        var count = 1;
        while (i > 0) {
            i--;
            if (classTypes[i] === RI) {
                count++;
            } else {
                break;
            }
        }
        if (count % 2 !== 0) {
            return BREAK_NOT_ALLOWED;
        }
    }

    // LB30b Do not break between an emoji base and an emoji modifier.
    if (current === EB && next === EM) {
        return BREAK_NOT_ALLOWED;
    }

    return BREAK_ALLOWED;
};

var lineBreakAtIndex = exports.lineBreakAtIndex = function lineBreakAtIndex(codePoints, index) {
    // LB2 Never break at the start of text.
    if (index === 0) {
        return BREAK_NOT_ALLOWED;
    }

    // LB3 Always break at the end of text.
    if (index >= codePoints.length) {
        return BREAK_MANDATORY;
    }

    var _codePointsToCharacte = codePointsToCharacterClasses(codePoints),
        _codePointsToCharacte2 = _slicedToArray(_codePointsToCharacte, 2),
        indicies = _codePointsToCharacte2[0],
        classTypes = _codePointsToCharacte2[1];

    return _lineBreakAtIndex(codePoints, classTypes, indicies, index);
};

var cssFormattedClasses = function cssFormattedClasses(codePoints, options) {
    if (!options) {
        options = { lineBreak: 'normal', wordBreak: 'normal' };
    }

    var _codePointsToCharacte3 = codePointsToCharacterClasses(codePoints, options.lineBreak),
        _codePointsToCharacte4 = _slicedToArray(_codePointsToCharacte3, 3),
        indicies = _codePointsToCharacte4[0],
        classTypes = _codePointsToCharacte4[1],
        isLetterNumber = _codePointsToCharacte4[2];

    if (options.wordBreak === 'break-all' || options.wordBreak === 'break-word') {
        classTypes = classTypes.map(function (type) {
            return [NU, AL, SA].indexOf(type) !== -1 ? ID : type;
        });
    }

    var forbiddenBreakpoints = options.wordBreak === 'keep-all' ? isLetterNumber.map(function (isLetterNumber, i) {
        return isLetterNumber && codePoints[i] >= 0x4e00 && codePoints[i] <= 0x9fff;
    }) : null;

    return [indicies, classTypes, forbiddenBreakpoints];
};

var inlineBreakOpportunities = exports.inlineBreakOpportunities = function inlineBreakOpportunities(str, options) {
    var codePoints = (0, _Util.toCodePoints)(str);
    var output = BREAK_NOT_ALLOWED;

    var _cssFormattedClasses = cssFormattedClasses(codePoints, options),
        _cssFormattedClasses2 = _slicedToArray(_cssFormattedClasses, 3),
        indicies = _cssFormattedClasses2[0],
        classTypes = _cssFormattedClasses2[1],
        forbiddenBreakpoints = _cssFormattedClasses2[2];

    codePoints.forEach(function (codePoint, i) {
        output += (0, _Util.fromCodePoint)(codePoint) + (i >= codePoints.length - 1 ? BREAK_MANDATORY : _lineBreakAtIndex(codePoints, classTypes, indicies, i + 1, forbiddenBreakpoints));
    });

    return output;
};

var Break = function () {
    function Break(codePoints, lineBreak, start, end) {
        _classCallCheck(this, Break);

        this._codePoints = codePoints;
        this.required = lineBreak === BREAK_MANDATORY;
        this.start = start;
        this.end = end;
    }

    _createClass(Break, [{
        key: 'slice',
        value: function slice() {
            return _Util.fromCodePoint.apply(undefined, _toConsumableArray(this._codePoints.slice(this.start, this.end)));
        }
    }]);

    return Break;
}();

var LineBreaker = exports.LineBreaker = function LineBreaker(str, options) {
    var codePoints = (0, _Util.toCodePoints)(str);

    var _cssFormattedClasses3 = cssFormattedClasses(codePoints, options),
        _cssFormattedClasses4 = _slicedToArray(_cssFormattedClasses3, 3),
        indicies = _cssFormattedClasses4[0],
        classTypes = _cssFormattedClasses4[1],
        forbiddenBreakpoints = _cssFormattedClasses4[2];

    var length = codePoints.length;
    var lastEnd = 0;
    var nextIndex = 0;

    return {
        next: function next() {
            if (nextIndex >= length) {
                return { done: true };
            }
            var lineBreak = BREAK_NOT_ALLOWED;
            while (nextIndex < length && (lineBreak = _lineBreakAtIndex(codePoints, classTypes, indicies, ++nextIndex, forbiddenBreakpoints)) === BREAK_NOT_ALLOWED) {}

            if (lineBreak !== BREAK_NOT_ALLOWED || nextIndex === length) {
                var value = new Break(codePoints, lineBreak, lastEnd, nextIndex);
                lastEnd = nextIndex;
                return { value: value, done: false };
            }

            return { done: true };
        }
    };
};

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Trie = exports.createTrieFromBase64 = exports.UTRIE2_INDEX_2_MASK = exports.UTRIE2_INDEX_2_BLOCK_LENGTH = exports.UTRIE2_OMITTED_BMP_INDEX_1_LENGTH = exports.UTRIE2_INDEX_1_OFFSET = exports.UTRIE2_UTF8_2B_INDEX_2_LENGTH = exports.UTRIE2_UTF8_2B_INDEX_2_OFFSET = exports.UTRIE2_INDEX_2_BMP_LENGTH = exports.UTRIE2_LSCP_INDEX_2_LENGTH = exports.UTRIE2_DATA_MASK = exports.UTRIE2_DATA_BLOCK_LENGTH = exports.UTRIE2_LSCP_INDEX_2_OFFSET = exports.UTRIE2_SHIFT_1_2 = exports.UTRIE2_INDEX_SHIFT = exports.UTRIE2_SHIFT_1 = exports.UTRIE2_SHIFT_2 = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Util = __webpack_require__(13);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/** Shift size for getting the index-2 table offset. */
var UTRIE2_SHIFT_2 = exports.UTRIE2_SHIFT_2 = 5;

/** Shift size for getting the index-1 table offset. */
var UTRIE2_SHIFT_1 = exports.UTRIE2_SHIFT_1 = 6 + 5;

/**
 * Shift size for shifting left the index array values.
 * Increases possible data size with 16-bit index values at the cost
 * of compactability.
 * This requires data blocks to be aligned by UTRIE2_DATA_GRANULARITY.
 */
var UTRIE2_INDEX_SHIFT = exports.UTRIE2_INDEX_SHIFT = 2;

/**
 * Difference between the two shift sizes,
 * for getting an index-1 offset from an index-2 offset. 6=11-5
 */
var UTRIE2_SHIFT_1_2 = exports.UTRIE2_SHIFT_1_2 = UTRIE2_SHIFT_1 - UTRIE2_SHIFT_2;

/**
 * The part of the index-2 table for U+D800..U+DBFF stores values for
 * lead surrogate code _units_ not code _points_.
 * Values for lead surrogate code _points_ are indexed with this portion of the table.
 * Length=32=0x20=0x400>>UTRIE2_SHIFT_2. (There are 1024=0x400 lead surrogates.)
 */
var UTRIE2_LSCP_INDEX_2_OFFSET = exports.UTRIE2_LSCP_INDEX_2_OFFSET = 0x10000 >> UTRIE2_SHIFT_2;

/** Number of entries in a data block. 32=0x20 */
var UTRIE2_DATA_BLOCK_LENGTH = exports.UTRIE2_DATA_BLOCK_LENGTH = 1 << UTRIE2_SHIFT_2;
/** Mask for getting the lower bits for the in-data-block offset. */
var UTRIE2_DATA_MASK = exports.UTRIE2_DATA_MASK = UTRIE2_DATA_BLOCK_LENGTH - 1;

var UTRIE2_LSCP_INDEX_2_LENGTH = exports.UTRIE2_LSCP_INDEX_2_LENGTH = 0x400 >> UTRIE2_SHIFT_2;
/** Count the lengths of both BMP pieces. 2080=0x820 */
var UTRIE2_INDEX_2_BMP_LENGTH = exports.UTRIE2_INDEX_2_BMP_LENGTH = UTRIE2_LSCP_INDEX_2_OFFSET + UTRIE2_LSCP_INDEX_2_LENGTH;
/**
 * The 2-byte UTF-8 version of the index-2 table follows at offset 2080=0x820.
 * Length 32=0x20 for lead bytes C0..DF, regardless of UTRIE2_SHIFT_2.
 */
var UTRIE2_UTF8_2B_INDEX_2_OFFSET = exports.UTRIE2_UTF8_2B_INDEX_2_OFFSET = UTRIE2_INDEX_2_BMP_LENGTH;
var UTRIE2_UTF8_2B_INDEX_2_LENGTH = exports.UTRIE2_UTF8_2B_INDEX_2_LENGTH = 0x800 >> 6; /* U+0800 is the first code point after 2-byte UTF-8 */
/**
 * The index-1 table, only used for supplementary code points, at offset 2112=0x840.
 * Variable length, for code points up to highStart, where the last single-value range starts.
 * Maximum length 512=0x200=0x100000>>UTRIE2_SHIFT_1.
 * (For 0x100000 supplementary code points U+10000..U+10ffff.)
 *
 * The part of the index-2 table for supplementary code points starts
 * after this index-1 table.
 *
 * Both the index-1 table and the following part of the index-2 table
 * are omitted completely if there is only BMP data.
 */
var UTRIE2_INDEX_1_OFFSET = exports.UTRIE2_INDEX_1_OFFSET = UTRIE2_UTF8_2B_INDEX_2_OFFSET + UTRIE2_UTF8_2B_INDEX_2_LENGTH;

/**
 * Number of index-1 entries for the BMP. 32=0x20
 * This part of the index-1 table is omitted from the serialized form.
 */
var UTRIE2_OMITTED_BMP_INDEX_1_LENGTH = exports.UTRIE2_OMITTED_BMP_INDEX_1_LENGTH = 0x10000 >> UTRIE2_SHIFT_1;

/** Number of entries in an index-2 block. 64=0x40 */
var UTRIE2_INDEX_2_BLOCK_LENGTH = exports.UTRIE2_INDEX_2_BLOCK_LENGTH = 1 << UTRIE2_SHIFT_1_2;
/** Mask for getting the lower bits for the in-index-2-block offset. */
var UTRIE2_INDEX_2_MASK = exports.UTRIE2_INDEX_2_MASK = UTRIE2_INDEX_2_BLOCK_LENGTH - 1;

var createTrieFromBase64 = exports.createTrieFromBase64 = function createTrieFromBase64(base64) {
    var buffer = (0, _Util.decode)(base64);
    var view32 = Array.isArray(buffer) ? (0, _Util.polyUint32Array)(buffer) : new Uint32Array(buffer);
    var view16 = Array.isArray(buffer) ? (0, _Util.polyUint16Array)(buffer) : new Uint16Array(buffer);
    var headerLength = 24;

    var index = view16.slice(headerLength / 2, view32[4] / 2);
    var data = view32[5] === 2 ? view16.slice((headerLength + view32[4]) / 2) : view32.slice(Math.ceil((headerLength + view32[4]) / 4));

    return new Trie(view32[0], view32[1], view32[2], view32[3], index, data);
};

var Trie = exports.Trie = function () {
    function Trie(initialValue, errorValue, highStart, highValueIndex, index, data) {
        _classCallCheck(this, Trie);

        this.initialValue = initialValue;
        this.errorValue = errorValue;
        this.highStart = highStart;
        this.highValueIndex = highValueIndex;
        this.index = index;
        this.data = data;
    }

    /**
     * Get the value for a code point as stored in the Trie.
     *
     * @param codePoint the code point
     * @return the value
     */


    _createClass(Trie, [{
        key: 'get',
        value: function get(codePoint) {
            var ix = void 0;
            if (codePoint >= 0) {
                if (codePoint < 0x0d800 || codePoint > 0x0dbff && codePoint <= 0x0ffff) {
                    // Ordinary BMP code point, excluding leading surrogates.
                    // BMP uses a single level lookup.  BMP index starts at offset 0 in the Trie2 index.
                    // 16 bit data is stored in the index array itself.
                    ix = this.index[codePoint >> UTRIE2_SHIFT_2];
                    ix = (ix << UTRIE2_INDEX_SHIFT) + (codePoint & UTRIE2_DATA_MASK);
                    return this.data[ix];
                }

                if (codePoint <= 0xffff) {
                    // Lead Surrogate Code Point.  A Separate index section is stored for
                    // lead surrogate code units and code points.
                    //   The main index has the code unit data.
                    //   For this function, we need the code point data.
                    // Note: this expression could be refactored for slightly improved efficiency, but
                    //       surrogate code points will be so rare in practice that it's not worth it.
                    ix = this.index[UTRIE2_LSCP_INDEX_2_OFFSET + (codePoint - 0xd800 >> UTRIE2_SHIFT_2)];
                    ix = (ix << UTRIE2_INDEX_SHIFT) + (codePoint & UTRIE2_DATA_MASK);
                    return this.data[ix];
                }

                if (codePoint < this.highStart) {
                    // Supplemental code point, use two-level lookup.
                    ix = UTRIE2_INDEX_1_OFFSET - UTRIE2_OMITTED_BMP_INDEX_1_LENGTH + (codePoint >> UTRIE2_SHIFT_1);
                    ix = this.index[ix];
                    ix += codePoint >> UTRIE2_SHIFT_2 & UTRIE2_INDEX_2_MASK;
                    ix = this.index[ix];
                    ix = (ix << UTRIE2_INDEX_SHIFT) + (codePoint & UTRIE2_DATA_MASK);
                    return this.data[ix];
                }
                if (codePoint <= 0x10ffff) {
                    return this.data[this.highValueIndex];
                }
            }

            // Fall through.  The code point is outside of the legal range of 0..0x10ffff.
            return this.errorValue;
        }
    }]);

    return Trie;
}();

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = 'KwAAAAAAAAAACA4AIDoAAPAfAAACAAAAAAAIABAAGABAAEgAUABYAF4AZgBeAGYAYABoAHAAeABeAGYAfACEAIAAiACQAJgAoACoAK0AtQC9AMUAXgBmAF4AZgBeAGYAzQDVAF4AZgDRANkA3gDmAOwA9AD8AAQBDAEUARoBIgGAAIgAJwEvATcBPwFFAU0BTAFUAVwBZAFsAXMBewGDATAAiwGTAZsBogGkAawBtAG8AcIBygHSAdoB4AHoAfAB+AH+AQYCDgIWAv4BHgImAi4CNgI+AkUCTQJTAlsCYwJrAnECeQKBAk0CiQKRApkCoQKoArACuALAAsQCzAIwANQC3ALkAjAA7AL0AvwCAQMJAxADGAMwACADJgMuAzYDPgOAAEYDSgNSA1IDUgNaA1oDYANiA2IDgACAAGoDgAByA3YDfgOAAIQDgACKA5IDmgOAAIAAogOqA4AAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAK8DtwOAAIAAvwPHA88D1wPfAyAD5wPsA/QD/AOAAIAABAQMBBIEgAAWBB4EJgQuBDMEIAM7BEEEXgBJBCADUQRZBGEEaQQwADAAcQQ+AXkEgQSJBJEEgACYBIAAoASoBK8EtwQwAL8ExQSAAIAAgACAAIAAgACgAM0EXgBeAF4AXgBeAF4AXgBeANUEXgDZBOEEXgDpBPEE+QQBBQkFEQUZBSEFKQUxBTUFPQVFBUwFVAVcBV4AYwVeAGsFcwV7BYMFiwWSBV4AmgWgBacFXgBeAF4AXgBeAKsFXgCyBbEFugW7BcIFwgXIBcIFwgXQBdQF3AXkBesF8wX7BQMGCwYTBhsGIwYrBjMGOwZeAD8GRwZNBl4AVAZbBl4AXgBeAF4AXgBeAF4AXgBeAF4AXgBeAGMGXgBqBnEGXgBeAF4AXgBeAF4AXgBeAF4AXgB5BoAG4wSGBo4GkwaAAIADHgR5AF4AXgBeAJsGgABGA4AAowarBrMGswagALsGwwbLBjAA0wbaBtoG3QbaBtoG2gbaBtoG2gblBusG8wb7BgMHCwcTBxsHCwcjBysHMAc1BzUHOgdCB9oGSgdSB1oHYAfaBloHaAfaBlIH2gbaBtoG2gbaBtoG2gbaBjUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHbQdeAF4ANQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQd1B30HNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1B4MH2gaKB68EgACAAIAAgACAAIAAgACAAI8HlwdeAJ8HpweAAIAArwe3B14AXgC/B8UHygcwANAH2AfgB4AA6AfwBz4B+AcACFwBCAgPCBcIogEYAR8IJwiAAC8INwg/CCADRwhPCFcIXwhnCEoDGgSAAIAAgABvCHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIhAiLCI4IMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlggwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAANQc1BzUHNQc1BzUHNQc1BzUHNQc1B54INQc1B6II2gaqCLIIugiAAIAAvgjGCIAAgACAAIAAgACAAIAAgACAAIAAywiHAYAA0wiAANkI3QjlCO0I9Aj8CIAAgACAAAIJCgkSCRoJIgknCTYHLwk3CZYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiAAIAAAAFAAXgBeAGAAcABeAHwAQACQAKAArQC9AJ4AXgBeAE0A3gBRAN4A7AD8AMwBGgEAAKcBNwEFAUwBXAF4QkhCmEKnArcCgAHHAsABz4LAAcABwAHAAd+C6ABoAG+C/4LAAcABwAHAAc+DF4MAAcAB54M3gweDV4Nng3eDaABoAGgAaABoAGgAaABoAGgAaABoAGgAaABoAGgAaABoAGgAaABoAEeDqABVg6WDqABoQ6gAaABoAHXDvcONw/3DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DncPAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcAB7cPPwlGCU4JMACAAIAAgABWCV4JYQmAAGkJcAl4CXwJgAkwADAAMAAwAIgJgACLCZMJgACZCZ8JowmrCYAAswkwAF4AXgB8AIAAuwkABMMJyQmAAM4JgADVCTAAMAAwADAAgACAAIAAgACAAIAAgACAAIAAqwYWBNkIMAAwADAAMADdCeAJ6AnuCR4E9gkwAP4JBQoNCjAAMACAABUK0wiAAB0KJAosCjQKgAAwADwKQwqAAEsKvQmdCVMKWwowADAAgACAALcEMACAAGMKgABrCjAAMAAwADAAMAAwADAAMAAwADAAMAAeBDAAMAAwADAAMAAwADAAMAAwADAAMAAwAIkEPQFzCnoKiQSCCooKkAqJBJgKoAqkCokEGAGsCrQKvArBCjAAMADJCtEKFQHZCuEK/gHpCvEKMAAwADAAMACAAIwE+QowAIAAPwEBCzAAMAAwADAAMACAAAkLEQswAIAAPwEZCyELgAAOCCkLMAAxCzkLMAAwADAAMAAwADAAXgBeAEELMAAwADAAMAAwADAAMAAwAEkLTQtVC4AAXAtkC4AAiQkwADAAMAAwADAAMAAwADAAbAtxC3kLgAuFC4sLMAAwAJMLlwufCzAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAApwswADAAMACAAIAAgACvC4AAgACAAIAAgACAALcLMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAvwuAAMcLgACAAIAAgACAAIAAyguAAIAAgACAAIAA0QswADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAANkLgACAAIAA4AswADAAMAAwADAAMAAwADAAMAAwADAAMAAwAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACJCR4E6AswADAAhwHwC4AA+AsADAgMEAwwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMACAAIAAGAwdDCUMMAAwAC0MNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQw1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHPQwwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADUHNQc1BzUHNQc1BzUHNQc2BzAAMAA5DDUHNQc1BzUHNQc1BzUHNQc1BzUHNQdFDDAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAgACAAIAATQxSDFoMMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAF4AXgBeAF4AXgBeAF4AYgxeAGoMXgBxDHkMfwxeAIUMXgBeAI0MMAAwADAAMAAwAF4AXgCVDJ0MMAAwADAAMABeAF4ApQxeAKsMswy7DF4Awgy9DMoMXgBeAF4AXgBeAF4AXgBeAF4AXgDRDNkMeQBqCeAM3Ax8AOYM7Az0DPgMXgBeAF4AXgBeAF4AXgBeAF4AXgBeAF4AXgBeAF4AXgCgAAANoAAHDQ4NFg0wADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAeDSYNMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAIAAgACAAIAAgACAAC4NMABeAF4ANg0wADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAD4NRg1ODVYNXg1mDTAAbQ0wADAAMAAwADAAMAAwADAA2gbaBtoG2gbaBtoG2gbaBnUNeg3CBYANwgWFDdoGjA3aBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gaUDZwNpA2oDdoG2gawDbcNvw3HDdoG2gbPDdYN3A3fDeYN2gbsDfMN2gbaBvoN/g3aBgYODg7aBl4AXgBeABYOXgBeACUG2gYeDl4AJA5eACwO2w3aBtoGMQ45DtoG2gbaBtoGQQ7aBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gZJDjUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1B1EO2gY1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQdZDjUHNQc1BzUHNQc1B2EONQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHaA41BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1B3AO2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gY1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1B2EO2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gZJDtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBkkOeA6gAKAAoAAwADAAMAAwAKAAoACgAKAAoACgAKAAgA4wADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAD//wQABAAEAAQABAAEAAQABAAEAA0AAwABAAEAAgAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAKABMAFwAeABsAGgAeABcAFgASAB4AGwAYAA8AGAAcAEsASwBLAEsASwBLAEsASwBLAEsAGAAYAB4AHgAeABMAHgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAFgAbABIAHgAeAB4AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQABYADQARAB4ABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABAAEAAQABAAEAAUABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAkAFgAaABsAGwAbAB4AHQAdAB4ATwAXAB4ADQAeAB4AGgAbAE8ATwAOAFAAHQAdAB0ATwBPABcATwBPAE8AFgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB0AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAB4AHgAeAB4AUABQAFAAUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAB4AHgAeAFAATwBAAE8ATwBPAEAATwBQAFAATwBQAB4AHgAeAB4AHgAeAB0AHQAdAB0AHgAdAB4ADgBQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgBQAB4AUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAJAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAkACQAJAAkACQAJAAkABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAeAB4AHgAeAFAAHgAeAB4AKwArAFAAUABQAFAAGABQACsAKwArACsAHgAeAFAAHgBQAFAAUAArAFAAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAAEAAQABAAEAAQABAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAUAAeAB4AHgAeAB4AHgArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwAYAA0AKwArAB4AHgAbACsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQADQAEAB4ABAAEAB4ABAAEABMABAArACsAKwArACsAKwArACsAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAKwArACsAKwArAFYAVgBWAB4AHgArACsAKwArACsAKwArACsAKwArACsAHgAeAB4AHgAeAB4AHgAeAB4AGgAaABoAGAAYAB4AHgAEAAQABAAEAAQABAAEAAQABAAEAAQAEwAEACsAEwATAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABABLAEsASwBLAEsASwBLAEsASwBLABoAGQAZAB4AUABQAAQAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQABMAUAAEAAQABAAEAAQABAAEAB4AHgAEAAQABAAEAAQABABQAFAABAAEAB4ABAAEAAQABABQAFAASwBLAEsASwBLAEsASwBLAEsASwBQAFAAUAAeAB4AUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwAeAFAABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQABAAEAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQAUABQAB4AHgAYABMAUAArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAFAABAAEAAQABAAEAFAABAAEAAQAUAAEAAQABAAEAAQAKwArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAArACsAHgArAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAeAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAFAABAAEAAQABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAAQABAANAA0ASwBLAEsASwBLAEsASwBLAEsASwAeAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQAKwBQAFAAUABQAFAAUABQAFAAKwArAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUAArAFAAKwArACsAUABQAFAAUAArACsABABQAAQABAAEAAQABAAEAAQAKwArAAQABAArACsABAAEAAQAUAArACsAKwArACsAKwArACsABAArACsAKwArAFAAUAArAFAAUABQAAQABAArACsASwBLAEsASwBLAEsASwBLAEsASwBQAFAAGgAaAFAAUABQAFAAUABMAB4AGwBQAB4AKwArACsABAAEAAQAKwBQAFAAUABQAFAAUAArACsAKwArAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUAArAFAAUAArAFAAUAArAFAAUAArACsABAArAAQABAAEAAQABAArACsAKwArAAQABAArACsABAAEAAQAKwArACsABAArACsAKwArACsAKwArAFAAUABQAFAAKwBQACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwAEAAQAUABQAFAABAArACsAKwArACsAKwArACsAKwArACsABAAEAAQAKwBQAFAAUABQAFAAUABQAFAAUAArAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUAArAFAAUAArAFAAUABQAFAAUAArACsABABQAAQABAAEAAQABAAEAAQABAArAAQABAAEACsABAAEAAQAKwArAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAAQABAArACsASwBLAEsASwBLAEsASwBLAEsASwAeABsAKwArACsAKwArACsAKwBQAAQABAAEAAQABAAEACsABAAEAAQAKwBQAFAAUABQAFAAUABQAFAAKwArAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQAKwArAAQABAArACsABAAEAAQAKwArACsAKwArACsAKwArAAQABAArACsAKwArAFAAUAArAFAAUABQAAQABAArACsASwBLAEsASwBLAEsASwBLAEsASwAeAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwAEAFAAKwBQAFAAUABQAFAAUAArACsAKwBQAFAAUAArAFAAUABQAFAAKwArACsAUABQACsAUAArAFAAUAArACsAKwBQAFAAKwArACsAUABQAFAAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwAEAAQABAAEAAQAKwArACsABAAEAAQAKwAEAAQABAAEACsAKwBQACsAKwArACsAKwArAAQAKwArACsAKwArACsAKwArACsAKwBLAEsASwBLAEsASwBLAEsASwBLAFAAUABQAB4AHgAeAB4AHgAeABsAHgArACsAKwArACsABAAEAAQABAArAFAAUABQAFAAUABQAFAAUAArAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArAFAABAAEAAQABAAEAAQABAArAAQABAAEACsABAAEAAQABAArACsAKwArACsAKwArAAQABAArAFAAUABQACsAKwArACsAKwBQAFAABAAEACsAKwBLAEsASwBLAEsASwBLAEsASwBLACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAB4AUAAEAAQABAArAFAAUABQAFAAUABQAFAAUAArAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQACsAKwAEAFAABAAEAAQABAAEAAQABAArAAQABAAEACsABAAEAAQABAArACsAKwArACsAKwArAAQABAArACsAKwArACsAKwArAFAAKwBQAFAABAAEACsAKwBLAEsASwBLAEsASwBLAEsASwBLACsAUABQACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAFAABAAEAAQABAAEAAQABAArAAQABAAEACsABAAEAAQABABQAB4AKwArACsAKwBQAFAAUAAEAFAAUABQAFAAUABQAFAAUABQAFAABAAEACsAKwBLAEsASwBLAEsASwBLAEsASwBLAFAAUABQAFAAUABQAFAAUABQABoAUABQAFAAUABQAFAAKwArAAQABAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQACsAUAArACsAUABQAFAAUABQAFAAUAArACsAKwAEACsAKwArACsABAAEAAQABAAEAAQAKwAEACsABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArAAQABAAeACsAKwArACsAKwArACsAKwArACsAKwArAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAAqAFwAXAAqACoAKgAqACoAKgAqACsAKwArACsAGwBcAFwAXABcAFwAXABcACoAKgAqACoAKgAqACoAKgAeAEsASwBLAEsASwBLAEsASwBLAEsADQANACsAKwArACsAKwBcAFwAKwBcACsAKwBcAFwAKwBcACsAKwBcACsAKwArACsAKwArAFwAXABcAFwAKwBcAFwAXABcAFwAXABcACsAXABcAFwAKwBcACsAXAArACsAXABcACsAXABcAFwAXAAqAFwAXAAqACoAKgAqACoAKgArACoAKgBcACsAKwBcAFwAXABcAFwAKwBcACsAKgAqACoAKgAqACoAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArAFwAXABcAFwAUAAOAA4ADgAOAB4ADgAOAAkADgAOAA0ACQATABMAEwATABMACQAeABMAHgAeAB4ABAAEAB4AHgAeAB4AHgAeAEsASwBLAEsASwBLAEsASwBLAEsAUABQAFAAUABQAFAAUABQAFAAUAANAAQAHgAEAB4ABAAWABEAFgARAAQABABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAANAAQABAAEAAQABAANAAQABABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQABAAEACsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsADQANAB4AHgAeAB4AHgAeAAQAHgAeAB4AHgAeAB4AKwAeAB4ADgAOAA0ADgAeAB4AHgAeAB4ACQAJACsAKwArACsAKwBcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqAFwASwBLAEsASwBLAEsASwBLAEsASwANAA0AHgAeAB4AHgBcAFwAXABcAFwAXAAqACoAKgAqAFwAXABcAFwAKgAqACoAXAAqACoAKgBcAFwAKgAqACoAKgAqACoAKgBcAFwAXAAqACoAKgAqAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKgAqACoAKgAqACoAKgAqACoAKgAqACoAXAAqAEsASwBLAEsASwBLAEsASwBLAEsAKgAqACoAKgAqACoAUABQAFAAUABQAFAAKwBQACsAKwArACsAKwBQACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAFAAUABQAFAAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQACsAKwBQAFAAUABQAFAAUABQACsAUAArAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUAArACsAUABQAFAAUABQAFAAUAArAFAAKwBQAFAAUABQACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwAEAAQABAAeAA0AHgAeAB4AHgAeAB4AHgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAB4AHgAeAB4AHgAeAB4AHgAeACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQACsAKwANAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAB4AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAA0AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQABYAEQArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAADQANAA0AUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAABAAEAAQAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAA0ADQArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQACsABAAEACsAKwArACsAKwArACsAKwArACsAKwArAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoADQANABUAXAANAB4ADQAbAFwAKgArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArAB4AHgATABMADQANAA4AHgATABMAHgAEAAQABAAJACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAUABQAFAAUABQAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABABQACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsABAAEAAQABAAEAAQABAAEAAQABAAEAAQAKwArACsAKwAeACsAKwArABMAEwBLAEsASwBLAEsASwBLAEsASwBLAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcACsAKwBcAFwAXABcAFwAKwArACsAKwArACsAKwArACsAKwArAFwAXABcAFwAXABcAFwAXABcAFwAXABcACsAKwArACsAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwBcACsAKwArACoAKgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEACsAKwAeAB4AXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKgAqACoAKgAqACoAKgAqACoAKgArACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgArACsABABLAEsASwBLAEsASwBLAEsASwBLACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsAKgAqACoAKgAqACoAKgBcACoAKgAqACoAKgAqACsAKwAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArAAQABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQAUABQAFAAUABQAFAAUAArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsADQANAB4ADQANAA0ADQAeAB4AHgAeAB4AHgAeAB4AHgAeAAQABAAEAAQABAAEAAQABAAEAB4AHgAeAB4AHgAeAB4AHgAeACsAKwArAAQABAAEAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAUABQAEsASwBLAEsASwBLAEsASwBLAEsAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsAKwArACsAKwArACsAHgAeAB4AHgBQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsAKwANAA0ADQANAA0ASwBLAEsASwBLAEsASwBLAEsASwArACsAKwBQAFAAUABLAEsASwBLAEsASwBLAEsASwBLAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAANAA0AUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgAeAB4AHgAeAB4AHgArACsAKwArACsAKwArACsABAAEAAQAHgAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAFAAUABQAFAABABQAFAAUABQAAQABAAEAFAAUAAEAAQABAArACsAKwArACsAKwAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAKwAEAAQABAAEAAQAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArACsAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUAArAFAAKwBQACsAUAArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACsAHgAeAB4AHgAeAB4AHgAeAFAAHgAeAB4AUABQAFAAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAKwArAB4AHgAeAB4AHgAeACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArACsAUABQAFAAKwAeAB4AHgAeAB4AHgAeAA4AHgArAA0ADQANAA0ADQANAA0ACQANAA0ADQAIAAQACwAEAAQADQAJAA0ADQAMAB0AHQAeABcAFwAWABcAFwAXABYAFwAdAB0AHgAeABQAFAAUAA0AAQABAAQABAAEAAQABAAJABoAGgAaABoAGgAaABoAGgAeABcAFwAdABUAFQAeAB4AHgAeAB4AHgAYABYAEQAVABUAFQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgANAB4ADQANAA0ADQAeAA0ADQANAAcAHgAeAB4AHgArAAQABAAEAAQABAAEAAQABAAEAAQAUABQACsAKwBPAFAAUABQAFAAUAAeAB4AHgAWABEATwBQAE8ATwBPAE8AUABQAFAAUABQAB4AHgAeABYAEQArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAGwAbABsAGwAbABsAGwAaABsAGwAbABsAGwAbABsAGwAbABsAGwAbABsAGwAaABsAGwAbABsAGgAbABsAGgAbABsAGwAbABsAGwAbABsAGwAbABsAGwAbABsAGwAbABsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgBQABoAHgAdAB4AUAAeABoAHgAeAB4AHgAeAB4AHgAeAB4ATwAeAFAAGwAeAB4AUABQAFAAUABQAB4AHgAeAB0AHQAeAFAAHgBQAB4AUAAeAFAATwBQAFAAHgAeAB4AHgAeAB4AHgBQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAB4AUABQAFAAUABPAE8AUABQAFAAUABQAE8AUABQAE8AUABPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBQAFAAUABQAE8ATwBPAE8ATwBPAE8ATwBPAE8AUABQAFAAUABQAFAAUABQAFAAHgAeAFAAUABQAFAATwAeAB4AKwArACsAKwAdAB0AHQAdAB0AHQAdAB0AHQAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB4AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAeAB0AHQAeAB4AHgAdAB0AHgAeAB0AHgAeAB4AHQAeAB0AGwAbAB4AHQAeAB4AHgAeAB0AHgAeAB0AHQAdAB0AHgAeAB0AHgAdAB4AHQAdAB0AHQAdAB0AHgAdAB4AHgAeAB4AHgAdAB0AHQAdAB4AHgAeAB4AHQAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAeAB4AHgAdAB4AHgAeAB4AHgAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAdAB4AHgAdAB0AHQAdAB4AHgAdAB0AHgAeAB0AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB0AHgAeAB0AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB0AHgAeAB4AHQAeAB4AHgAeAB4AHgAeAB0AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeABQAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAWABEAFgARAB4AHgAeAB4AHgAeAB0AHgAeAB4AHgAeAB4AHgAlACUAHgAeAB4AHgAeAB4AHgAeAB4AFgARAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACUAJQAlACUAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBQAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB4AHgAeAB4AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHgAeAB0AHQAdAB0AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB0AHgAdAB0AHQAdAB0AHQAdAB4AHgAeAB4AHgAeAB4AHgAdAB0AHgAeAB0AHQAeAB4AHgAeAB0AHQAeAB4AHgAeAB0AHQAdAB4AHgAdAB4AHgAdAB0AHQAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAdAB0AHQAeAB4AHgAeAB4AHgAeAB4AHgAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAeAB0AHQAeAB4AHQAeAB4AHgAeAB0AHQAeAB4AHgAeACUAJQAdAB0AJQAeACUAJQAlACAAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAHgAeAB4AHgAdAB4AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAdAB4AHQAdAB0AHgAdACUAHQAdAB4AHQAdAB4AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB0AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAJQAlACUAJQAlACUAHQAdAB0AHQAlAB4AJQAlACUAHQAlACUAHQAdAB0AJQAlAB0AHQAlAB0AHQAlACUAJQAeAB0AHgAeAB4AHgAdAB0AJQAdAB0AHQAdAB0AHQAlACUAJQAlACUAHQAlACUAIAAlAB0AHQAlACUAJQAlACUAJQAlACUAHgAeAB4AJQAlACAAIAAgACAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB4AHgAeABcAFwAXABcAFwAXAB4AEwATACUAHgAeAB4AFgARABYAEQAWABEAFgARABYAEQAWABEAFgARAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAWABEAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AFgARABYAEQAWABEAFgARABYAEQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeABYAEQAWABEAFgARABYAEQAWABEAFgARABYAEQAWABEAFgARABYAEQAWABEAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AFgARABYAEQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeABYAEQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAdAB0AHQAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwAeAB4AHgAeAB4AHgAeAB4AHgArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAEAAQABAAeAB4AKwArACsAKwArABMADQANAA0AUAATAA0AUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAUAANACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAEAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQACsAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAA0ADQANAA0ADQANAA0ADQAeAA0AFgANAB4AHgAXABcAHgAeABcAFwAWABEAFgARABYAEQAWABEADQANAA0ADQATAFAADQANAB4ADQANAB4AHgAeAB4AHgAMAAwADQANAA0AHgANAA0AFgANAA0ADQANAA0ADQANACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACsAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAKwArACsAKwArACsAKwArACsAKwArACsAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAlACUAJQAlACUAJQAlACUAJQAlACUAJQArACsAKwArAA0AEQARACUAJQBHAFcAVwAWABEAFgARABYAEQAWABEAFgARACUAJQAWABEAFgARABYAEQAWABEAFQAWABEAEQAlAFcAVwBXAFcAVwBXAFcAVwBXAAQABAAEAAQABAAEACUAVwBXAFcAVwA2ACUAJQBXAFcAVwBHAEcAJQAlACUAKwBRAFcAUQBXAFEAVwBRAFcAUQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFEAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBRAFcAUQBXAFEAVwBXAFcAVwBXAFcAUQBXAFcAVwBXAFcAVwBRAFEAKwArAAQABAAVABUARwBHAFcAFQBRAFcAUQBXAFEAVwBRAFcAUQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFEAVwBRAFcAUQBXAFcAVwBXAFcAVwBRAFcAVwBXAFcAVwBXAFEAUQBXAFcAVwBXABUAUQBHAEcAVwArACsAKwArACsAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAKwArAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwArACUAJQBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAKwArACsAKwArACUAJQAlACUAKwArACsAKwArACsAKwArACsAKwArACsAUQBRAFEAUQBRAFEAUQBRAFEAUQBRAFEAUQBRAFEAUQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACsAVwBXAFcAVwBXAFcAVwBXAFcAVwAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAE8ATwBPAE8ATwBPAE8ATwAlAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAEcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAKwArACsAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAADQATAA0AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABLAEsASwBLAEsASwBLAEsASwBLAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAFAABAAEAAQABAAeAAQABAAEAAQABAAEAAQABAAEAAQAHgBQAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AUABQAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAeAA0ADQANAA0ADQArACsAKwArACsAKwArACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAUABQAFAAUABQAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAB4AHgAeAB4AHgAeAFAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArAB4AHgAeAB4AHgAeAB4AHgArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAAQAUABQAFAABABQAFAAUABQAAQAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAeAB4AHgAeACsAKwArACsAUABQAFAAUABQAFAAHgAeABoAHgArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAADgAOABMAEwArACsAKwArACsAKwArACsABAAEAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwANAA0ASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABABQAFAAUABQAFAAUAAeAB4AHgBQAA4AUAArACsAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAA0ADQBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAKwArACsAKwArACsAKwArACsAKwArAB4AWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYACsAKwArAAQAHgAeAB4AHgAeAB4ADQANAA0AHgAeAB4AHgArAFAASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArAB4AHgBcAFwAXABcAFwAKgBcAFwAXABcAFwAXABcAFwAXABcAEsASwBLAEsASwBLAEsASwBLAEsAXABcAFwAXABcACsAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwArAFAAUABQAAQAUABQAFAAUABQAFAAUABQAAQABAArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAHgANAA0ADQBcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKgAqACoAXAAqACoAKgBcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAAqAFwAKgAqACoAXABcACoAKgBcAFwAXABcAFwAKgAqAFwAKgBcACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFwAXABcACoAKgBQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAA0ADQBQAFAAUAAEAAQAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUAArACsAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQADQAEAAQAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsAVABVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBUAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVACsAKwArACsAKwArACsAKwArACsAKwArAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAKwArACsAKwBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAKwArACsAKwAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAJQAlACUAJQAlACUAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAKwArACsAKwArAFYABABWAFYAVgBWAFYAVgBWAFYAVgBWAB4AVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgArAFYAVgBWAFYAVgArAFYAKwBWAFYAKwBWAFYAKwBWAFYAVgBWAFYAVgBWAFYAVgBWAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAEQAWAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUAAaAB4AKwArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAGAARABEAGAAYABMAEwAWABEAFAArACsAKwArACsAKwAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACUAJQAlACUAJQAWABEAFgARABYAEQAWABEAFgARABYAEQAlACUAFgARACUAJQAlACUAJQAlACUAEQAlABEAKwAVABUAEwATACUAFgARABYAEQAWABEAJQAlACUAJQAlACUAJQAlACsAJQAbABoAJQArACsAKwArAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAAcAKwATACUAJQAbABoAJQAlABYAEQAlACUAEQAlABEAJQBXAFcAVwBXAFcAVwBXAFcAVwBXABUAFQAlACUAJQATACUAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXABYAJQARACUAJQAlAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwAWACUAEQAlABYAEQARABYAEQARABUAVwBRAFEAUQBRAFEAUQBRAFEAUQBRAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAEcARwArACsAVwBXAFcAVwBXAFcAKwArAFcAVwBXAFcAVwBXACsAKwBXAFcAVwBXAFcAVwArACsAVwBXAFcAKwArACsAGgAbACUAJQAlABsAGwArAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwAEAAQABAAQAB0AKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsADQANAA0AKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAFAAHgAeAB4AKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArACsAKwArAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAAQAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsADQBQAFAAUABQACsAKwArACsAUABQAFAAUABQAFAAUABQAA0AUABQAFAAUABQACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUAArACsAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQACsAKwArAFAAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAA0AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAB4AHgBQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsADQBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArAB4AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwBQAFAAUABQAFAABAAEAAQAKwAEAAQAKwArACsAKwArAAQABAAEAAQAUABQAFAAUAArAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsABAAEAAQAKwArACsAKwAEAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsADQANAA0ADQANAA0ADQANAB4AKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAB4AUABQAFAAUABQAFAAUABQAB4AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEACsAKwArACsAUABQAFAAUABQAA0ADQANAA0ADQANABQAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwANAA0ADQANAA0ADQANAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAHgAeAB4AHgArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwBQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAA0ADQAeAB4AHgAeAB4AKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAeAB4AHgANAA0ADQANACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwBLAEsASwBLAEsASwBLAEsASwBLACsAKwArACsAKwArAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsASwBLAEsASwBLAEsASwBLAEsASwANAA0ADQANACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAeAA4AUAArACsAKwArACsAKwArACsAKwAEAFAAUABQAFAADQANAB4ADQAeAAQABAAEAB4AKwArAEsASwBLAEsASwBLAEsASwBLAEsAUAAOAFAADQANAA0AKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAANAA0AHgANAA0AHgAEACsAUABQAFAAUABQAFAAUAArAFAAKwBQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAA0AKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsABAAEAAQABAArAFAAUABQAFAAUABQAFAAUAArACsAUABQACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAArACsABAAEACsAKwAEAAQABAArACsAUAArACsAKwArACsAKwAEACsAKwArACsAKwBQAFAAUABQAFAABAAEACsAKwAEAAQABAAEAAQABAAEACsAKwArAAQABAAEAAQABAArACsAKwArACsAKwArACsAKwArACsABAAEAAQABAAEAAQABABQAFAAUABQAA0ADQANAA0AHgBLAEsASwBLAEsASwBLAEsASwBLACsADQArAB4AKwArAAQABAAEAAQAUABQAB4AUAArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEACsAKwAEAAQABAAEAAQABAAEAAQABAAOAA0ADQATABMAHgAeAB4ADQANAA0ADQANAA0ADQANAA0ADQANAA0ADQANAA0AUABQAFAAUAAEAAQAKwArAAQADQANAB4AUAArACsAKwArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwAOAA4ADgAOAA4ADgAOAA4ADgAOAA4ADgAOACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAArACsAKwAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAXABcAA0ADQANACoASwBLAEsASwBLAEsASwBLAEsASwBQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwBQAFAABAAEAAQABAAEAAQABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAFAABAAEAAQABAAOAB4ADQANAA0ADQAOAB4ABAArACsAKwArACsAKwArACsAUAAEAAQABAAEAAQABAAEAAQABAAEAAQAUABQAFAAUAArACsAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAA0ADQANACsADgAOAA4ADQANACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEACsABAAEAAQABAAEAAQABAAEAFAADQANAA0ADQANACsAKwArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwAOABMAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQACsAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAArACsAKwAEACsABAAEACsABAAEAAQABAAEAAQABABQAAQAKwArACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsADQANAA0ADQANACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAASABIAEgAQwBDAEMAUABQAFAAUABDAFAAUABQAEgAQwBIAEMAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAASABDAEMAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABIAEMAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwANAA0AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAAQABAAEAAQABAANACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAA0ADQANAB4AHgAeAB4AHgAeAFAAUABQAFAADQAeACsAKwArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwArAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAEcARwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwArACsAKwArACsAKwArACsAKwArACsAKwArAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQACsAKwAeAAQABAANAAQABAAEAAQAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACsAKwArACsAKwArACsAKwArACsAHgAeAB4AHgAeAB4AHgArACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAAEAAQABAAEAB4AHgAeAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAHgAeAAQABAAEAAQABAAEAAQAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAEAAQABAAEAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgAEAAQABAAeACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArAFAAUAArACsAUAArACsAUABQACsAKwBQAFAAUABQACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwBQACsAUABQAFAAUABQAFAAUAArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAKwAeAB4AUABQAFAAUABQACsAUAArACsAKwBQAFAAUABQAFAAUABQACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgAeAB4AKwArAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAB4AHgAeAB4ABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAB4AHgAeAB4AHgAeAB4AHgAEAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAAeAB4ADQANAA0ADQAeACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAAQABAAEAAQABAArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABAAEAAQABAAEAAQABAArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsABAAEAAQABAAEAAQABAArAAQABAArAAQABAAEAAQABAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQAKwArACsAKwArACsAKwArACsAHgAeAB4AHgAEAAQABAAEAAQABAAEACsAKwArACsAKwBLAEsASwBLAEsASwBLAEsASwBLACsAKwArACsAFgAWAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUAArAFAAKwArAFAAKwBQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUAArAFAAKwBQACsAKwArACsAKwArAFAAKwArACsAKwBQACsAUAArAFAAKwBQAFAAUAArAFAAUAArAFAAKwArAFAAKwBQACsAUAArAFAAKwBQACsAUABQACsAUAArACsAUABQAFAAUAArAFAAUABQAFAAUABQAFAAKwBQAFAAUABQACsAUABQAFAAUAArAFAAKwBQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwBQAFAAUAArAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgArACsAKwArACsAKwArACsAKwArACsAKwArACsATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwAlACUAJQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAeACUAHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHgAeACUAJQAlACUAHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACkAKQApACkAKQApACkAKQApACkAKQApACkAKQApACkAKQApACkAKQApACkAKQApACkAKQAlACUAJQAlACUAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAB4AHgAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAHgAeACUAJQAlACUAJQAeACUAJQAlACUAJQAgACAAIAAlACUAIAAlACUAIAAgACAAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIQAhACEAIQAhACUAJQAgACAAJQAlACAAIAAgACAAIAAgACAAIAAgACAAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIAAgACAAIAAlACUAJQAlACAAJQAgACAAIAAgACAAIAAgACAAIAAlACUAJQAgACUAJQAlACUAIAAgACAAJQAgACAAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAeACUAHgAlAB4AJQAlACUAJQAlACAAJQAlACUAJQAeACUAHgAeACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAHgAeAB4AHgAeAB4AHgAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIAAgACUAJQAlACUAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIAAlACUAJQAlACAAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAB4AHgAeAB4AHgAeACUAJQAlACUAJQAlACUAIAAgACAAJQAlACUAIAAgACAAIAAgAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AFwAXABcAFQAVABUAHgAeAB4AHgAlACUAJQAgACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIAAgACAAJQAlACUAJQAlACUAJQAlACUAIAAlACUAJQAlACUAJQAlACUAJQAlACUAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAlACUAJQAlACUAJQAlACUAJQAlACUAJQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAlACUAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAHgAeAB4AHgAeAB4AHgAeACUAJQAlACUAJQAlACUAJQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACUAJQAlACUAJQAlACUAJQAlACUAJQAlACAAIAAgACAAIAAlACAAIAAlACUAJQAlACUAJQAgACUAJQAlACUAJQAlACUAJQAlACAAIAAgACAAIAAgACAAIAAgACAAJQAlACUAIAAgACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACsAKwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAJQAlACUAJQAlACUAJQAlACUAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAJQAlACUAJQAlACUAJQAlACUAJQAlAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQArAAQAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsA';

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _Path = __webpack_require__(6);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Circle = function Circle(x, y, radius) {
    _classCallCheck(this, Circle);

    this.type = _Path.PATH.CIRCLE;
    this.x = x;
    this.y = y;
    this.radius = radius;
    if (true) {
        if (isNaN(x)) {
            console.error('Invalid x value given for Circle');
        }
        if (isNaN(y)) {
            console.error('Invalid y value given for Circle');
        }
        if (isNaN(radius)) {
            console.error('Invalid radius value given for Circle');
        }
    }
};

exports.default = Circle;

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Bounds = __webpack_require__(2);

var _Font = __webpack_require__(25);

var _Gradient = __webpack_require__(52);

var _TextContainer = __webpack_require__(9);

var _TextContainer2 = _interopRequireDefault(_TextContainer);

var _background = __webpack_require__(5);

var _border = __webpack_require__(12);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Renderer = function () {
    function Renderer(target, options) {
        _classCallCheck(this, Renderer);

        this.target = target;
        this.options = options;
        target.render(options);
    }

    _createClass(Renderer, [{
        key: 'renderNode',
        value: function renderNode(container) {
            if (container.isVisible()) {
                this.renderNodeBackgroundAndBorders(container);
                this.renderNodeContent(container);
            }
        }
    }, {
        key: 'renderNodeContent',
        value: function renderNodeContent(container) {
            var _this = this;

            var callback = function callback() {
                if (container.childNodes.length) {
                    container.childNodes.forEach(function (child) {
                        if (child instanceof _TextContainer2.default) {
                            var style = child.parent.style;
                            _this.target.renderTextNode(child.bounds, style.color, style.font, style.textDecoration, style.textShadow);
                        } else {
                            _this.target.drawShape(child, container.style.color);
                        }
                    });
                }

                if (container.image) {
                    var _image = _this.options.imageStore.get(container.image);
                    if (_image) {
                        var contentBox = (0, _Bounds.calculateContentBox)(container.bounds, container.style.padding, container.style.border);
                        var _width = typeof _image.width === 'number' && _image.width > 0 ? _image.width : contentBox.width;
                        var _height = typeof _image.height === 'number' && _image.height > 0 ? _image.height : contentBox.height;
                        if (_width > 0 && _height > 0) {
                            _this.target.clip([(0, _Bounds.calculatePaddingBoxPath)(container.curvedBounds)], function () {
                                _this.target.drawImage(_image, new _Bounds.Bounds(0, 0, _width, _height), contentBox);
                            });
                        }
                    }
                }
            };
            var paths = container.getClipPaths();
            if (paths.length) {
                this.target.clip(paths, callback);
            } else {
                callback();
            }
        }
    }, {
        key: 'renderNodeBackgroundAndBorders',
        value: function renderNodeBackgroundAndBorders(container) {
            var _this2 = this;

            var HAS_BACKGROUND = !container.style.background.backgroundColor.isTransparent() || container.style.background.backgroundImage.length;

            var hasRenderableBorders = container.style.border.some(function (border) {
                return border.borderStyle !== _border.BORDER_STYLE.NONE && !border.borderColor.isTransparent();
            });

            var callback = function callback() {
                var backgroundPaintingArea = (0, _background.calculateBackgroungPaintingArea)(container.curvedBounds, container.style.background.backgroundClip);

                if (HAS_BACKGROUND) {
                    _this2.target.clip([backgroundPaintingArea], function () {
                        if (!container.style.background.backgroundColor.isTransparent()) {
                            _this2.target.fill(container.style.background.backgroundColor);
                        }

                        _this2.renderBackgroundImage(container);
                    });
                }

                container.style.border.forEach(function (border, side) {
                    if (border.borderStyle !== _border.BORDER_STYLE.NONE && !border.borderColor.isTransparent()) {
                        _this2.renderBorder(border, side, container.curvedBounds);
                    }
                });
            };

            if (HAS_BACKGROUND || hasRenderableBorders) {
                var paths = container.parent ? container.parent.getClipPaths() : [];
                if (paths.length) {
                    this.target.clip(paths, callback);
                } else {
                    callback();
                }
            }
        }
    }, {
        key: 'renderBackgroundImage',
        value: function renderBackgroundImage(container) {
            var _this3 = this;

            container.style.background.backgroundImage.slice(0).reverse().forEach(function (backgroundImage) {
                if (backgroundImage.source.method === 'url' && backgroundImage.source.args.length) {
                    _this3.renderBackgroundRepeat(container, backgroundImage);
                } else if (/gradient/i.test(backgroundImage.source.method)) {
                    _this3.renderBackgroundGradient(container, backgroundImage);
                }
            });
        }
    }, {
        key: 'renderBackgroundRepeat',
        value: function renderBackgroundRepeat(container, background) {
            var image = this.options.imageStore.get(background.source.args[0]);
            if (image) {
                var backgroundPositioningArea = (0, _background.calculateBackgroungPositioningArea)(container.style.background.backgroundOrigin, container.bounds, container.style.padding, container.style.border);
                var backgroundImageSize = (0, _background.calculateBackgroundSize)(background, image, backgroundPositioningArea);
                var position = (0, _background.calculateBackgroundPosition)(background.position, backgroundImageSize, backgroundPositioningArea);
                var _path = (0, _background.calculateBackgroundRepeatPath)(background, position, backgroundImageSize, backgroundPositioningArea, container.bounds);

                var _offsetX = Math.round(backgroundPositioningArea.left + position.x);
                var _offsetY = Math.round(backgroundPositioningArea.top + position.y);
                this.target.renderRepeat(_path, image, backgroundImageSize, _offsetX, _offsetY);
            }
        }
    }, {
        key: 'renderBackgroundGradient',
        value: function renderBackgroundGradient(container, background) {
            var backgroundPositioningArea = (0, _background.calculateBackgroungPositioningArea)(container.style.background.backgroundOrigin, container.bounds, container.style.padding, container.style.border);
            var backgroundImageSize = (0, _background.calculateGradientBackgroundSize)(background, backgroundPositioningArea);
            var position = (0, _background.calculateBackgroundPosition)(background.position, backgroundImageSize, backgroundPositioningArea);
            var gradientBounds = new _Bounds.Bounds(Math.round(backgroundPositioningArea.left + position.x), Math.round(backgroundPositioningArea.top + position.y), backgroundImageSize.width, backgroundImageSize.height);

            var gradient = (0, _Gradient.parseGradient)(container, background.source, gradientBounds);
            if (gradient) {
                switch (gradient.type) {
                    case _Gradient.GRADIENT_TYPE.LINEAR_GRADIENT:
                        // $FlowFixMe
                        this.target.renderLinearGradient(gradientBounds, gradient);
                        break;
                    case _Gradient.GRADIENT_TYPE.RADIAL_GRADIENT:
                        // $FlowFixMe
                        this.target.renderRadialGradient(gradientBounds, gradient);
                        break;
                }
            }
        }
    }, {
        key: 'renderBorder',
        value: function renderBorder(border, side, curvePoints) {
            this.target.drawShape((0, _Bounds.parsePathForBorder)(curvePoints, side), border.borderColor);
        }
    }, {
        key: 'renderStack',
        value: function renderStack(stack) {
            var _this4 = this;

            if (stack.container.isVisible()) {
                var _opacity = stack.getOpacity();
                if (_opacity !== this._opacity) {
                    this.target.setOpacity(stack.getOpacity());
                    this._opacity = _opacity;
                }

                var _transform = stack.container.style.transform;
                if (_transform !== null) {
                    this.target.transform(stack.container.bounds.left + _transform.transformOrigin[0].value, stack.container.bounds.top + _transform.transformOrigin[1].value, _transform.transform, function () {
                        return _this4.renderStackContent(stack);
                    });
                } else {
                    this.renderStackContent(stack);
                }
            }
        }
    }, {
        key: 'renderStackContent',
        value: function renderStackContent(stack) {
            var _splitStackingContext = splitStackingContexts(stack),
                _splitStackingContext2 = _slicedToArray(_splitStackingContext, 5),
                negativeZIndex = _splitStackingContext2[0],
                zeroOrAutoZIndexOrTransformedOrOpacity = _splitStackingContext2[1],
                positiveZIndex = _splitStackingContext2[2],
                nonPositionedFloats = _splitStackingContext2[3],
                nonPositionedInlineLevel = _splitStackingContext2[4];

            var _splitDescendants = splitDescendants(stack),
                _splitDescendants2 = _slicedToArray(_splitDescendants, 2),
                inlineLevel = _splitDescendants2[0],
                nonInlineLevel = _splitDescendants2[1];

            // https://www.w3.org/TR/css-position-3/#painting-order
            // 1. the background and borders of the element forming the stacking context.


            this.renderNodeBackgroundAndBorders(stack.container);
            // 2. the child stacking contexts with negative stack levels (most negative first).
            negativeZIndex.sort(sortByZIndex).forEach(this.renderStack, this);
            // 3. For all its in-flow, non-positioned, block-level descendants in tree order:
            this.renderNodeContent(stack.container);
            nonInlineLevel.forEach(this.renderNode, this);
            // 4. All non-positioned floating descendants, in tree order. For each one of these,
            // treat the element as if it created a new stacking context, but any positioned descendants and descendants
            // which actually create a new stacking context should be considered part of the parent stacking context,
            // not this new one.
            nonPositionedFloats.forEach(this.renderStack, this);
            // 5. the in-flow, inline-level, non-positioned descendants, including inline tables and inline blocks.
            nonPositionedInlineLevel.forEach(this.renderStack, this);
            inlineLevel.forEach(this.renderNode, this);
            // 6. All positioned, opacity or transform descendants, in tree order that fall into the following categories:
            //  All positioned descendants with 'z-index: auto' or 'z-index: 0', in tree order.
            //  For those with 'z-index: auto', treat the element as if it created a new stacking context,
            //  but any positioned descendants and descendants which actually create a new stacking context should be
            //  considered part of the parent stacking context, not this new one. For those with 'z-index: 0',
            //  treat the stacking context generated atomically.
            //
            //  All opacity descendants with opacity less than 1
            //
            //  All transform descendants with transform other than none
            zeroOrAutoZIndexOrTransformedOrOpacity.forEach(this.renderStack, this);
            // 7. Stacking contexts formed by positioned descendants with z-indices greater than or equal to 1 in z-index
            // order (smallest first) then tree order.
            positiveZIndex.sort(sortByZIndex).forEach(this.renderStack, this);
        }
    }, {
        key: 'render',
        value: function render(stack) {
            var _this5 = this;

            if (this.options.backgroundColor) {
                this.target.rectangle(this.options.x, this.options.y, this.options.width, this.options.height, this.options.backgroundColor);
            }
            this.renderStack(stack);
            var target = this.target.getTarget();
            if (true) {
                return target.then(function (output) {
                    _this5.options.logger.log('Render completed');
                    return output;
                });
            }
            return target;
        }
    }]);

    return Renderer;
}();

exports.default = Renderer;


var splitDescendants = function splitDescendants(stack) {
    var inlineLevel = [];
    var nonInlineLevel = [];

    var length = stack.children.length;
    for (var i = 0; i < length; i++) {
        var child = stack.children[i];
        if (child.isInlineLevel()) {
            inlineLevel.push(child);
        } else {
            nonInlineLevel.push(child);
        }
    }
    return [inlineLevel, nonInlineLevel];
};

var splitStackingContexts = function splitStackingContexts(stack) {
    var negativeZIndex = [];
    var zeroOrAutoZIndexOrTransformedOrOpacity = [];
    var positiveZIndex = [];
    var nonPositionedFloats = [];
    var nonPositionedInlineLevel = [];
    var length = stack.contexts.length;
    for (var i = 0; i < length; i++) {
        var child = stack.contexts[i];
        if (child.container.isPositioned() || child.container.style.opacity < 1 || child.container.isTransformed()) {
            if (child.container.style.zIndex.order < 0) {
                negativeZIndex.push(child);
            } else if (child.container.style.zIndex.order > 0) {
                positiveZIndex.push(child);
            } else {
                zeroOrAutoZIndexOrTransformedOrOpacity.push(child);
            }
        } else {
            if (child.container.isFloating()) {
                nonPositionedFloats.push(child);
            } else {
                nonPositionedInlineLevel.push(child);
            }
        }
    }
    return [negativeZIndex, zeroOrAutoZIndexOrTransformedOrOpacity, positiveZIndex, nonPositionedFloats, nonPositionedInlineLevel];
};

var sortByZIndex = function sortByZIndex(a, b) {
    if (a.container.style.zIndex.order > b.container.style.zIndex.order) {
        return 1;
    } else if (a.container.style.zIndex.order < b.container.style.zIndex.order) {
        return -1;
    }

    return a.container.index > b.container.index ? 1 : -1;
};

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.transformWebkitRadialGradientArgs = exports.parseGradient = exports.RadialGradient = exports.LinearGradient = exports.RADIAL_GRADIENT_SHAPE = exports.GRADIENT_TYPE = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _NodeContainer = __webpack_require__(3);

var _NodeContainer2 = _interopRequireDefault(_NodeContainer);

var _Angle = __webpack_require__(53);

var _Color = __webpack_require__(0);

var _Color2 = _interopRequireDefault(_Color);

var _Length = __webpack_require__(1);

var _Length2 = _interopRequireDefault(_Length);

var _Util = __webpack_require__(4);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SIDE_OR_CORNER = /^(to )?(left|top|right|bottom)( (left|top|right|bottom))?$/i;
var PERCENTAGE_ANGLES = /^([+-]?\d*\.?\d+)% ([+-]?\d*\.?\d+)%$/i;
var ENDS_WITH_LENGTH = /(px)|%|( 0)$/i;
var FROM_TO_COLORSTOP = /^(from|to|color-stop)\((?:([\d.]+)(%)?,\s*)?(.+?)\)$/i;
var RADIAL_SHAPE_DEFINITION = /^\s*(circle|ellipse)?\s*((?:([\d.]+)(px|r?em|%)\s*(?:([\d.]+)(px|r?em|%))?)|closest-side|closest-corner|farthest-side|farthest-corner)?\s*(?:at\s*(?:(left|center|right)|([\d.]+)(px|r?em|%))\s+(?:(top|center|bottom)|([\d.]+)(px|r?em|%)))?(?:\s|$)/i;

var GRADIENT_TYPE = exports.GRADIENT_TYPE = {
    LINEAR_GRADIENT: 0,
    RADIAL_GRADIENT: 1
};

var RADIAL_GRADIENT_SHAPE = exports.RADIAL_GRADIENT_SHAPE = {
    CIRCLE: 0,
    ELLIPSE: 1
};

var LENGTH_FOR_POSITION = {
    left: new _Length2.default('0%'),
    top: new _Length2.default('0%'),
    center: new _Length2.default('50%'),
    right: new _Length2.default('100%'),
    bottom: new _Length2.default('100%')
};

var LinearGradient = exports.LinearGradient = function LinearGradient(colorStops, direction) {
    _classCallCheck(this, LinearGradient);

    this.type = GRADIENT_TYPE.LINEAR_GRADIENT;
    this.colorStops = colorStops;
    this.direction = direction;
};

var RadialGradient = exports.RadialGradient = function RadialGradient(colorStops, shape, center, radius) {
    _classCallCheck(this, RadialGradient);

    this.type = GRADIENT_TYPE.RADIAL_GRADIENT;
    this.colorStops = colorStops;
    this.shape = shape;
    this.center = center;
    this.radius = radius;
};

var parseGradient = exports.parseGradient = function parseGradient(container, _ref, bounds) {
    var args = _ref.args,
        method = _ref.method,
        prefix = _ref.prefix;

    if (method === 'linear-gradient') {
        return parseLinearGradient(args, bounds, !!prefix);
    } else if (method === 'gradient' && args[0] === 'linear') {
        // TODO handle correct angle
        return parseLinearGradient(['to bottom'].concat(transformObsoleteColorStops(args.slice(3))), bounds, !!prefix);
    } else if (method === 'radial-gradient') {
        return parseRadialGradient(container, prefix === '-webkit-' ? transformWebkitRadialGradientArgs(args) : args, bounds);
    } else if (method === 'gradient' && args[0] === 'radial') {
        return parseRadialGradient(container, transformObsoleteColorStops(transformWebkitRadialGradientArgs(args.slice(1))), bounds);
    }
};

var parseColorStops = function parseColorStops(args, firstColorStopIndex, lineLength) {
    var colorStops = [];

    for (var i = firstColorStopIndex; i < args.length; i++) {
        var value = args[i];
        var HAS_LENGTH = ENDS_WITH_LENGTH.test(value);
        var lastSpaceIndex = value.lastIndexOf(' ');
        var _color = new _Color2.default(HAS_LENGTH ? value.substring(0, lastSpaceIndex) : value);
        var _stop = HAS_LENGTH ? new _Length2.default(value.substring(lastSpaceIndex + 1)) : i === firstColorStopIndex ? new _Length2.default('0%') : i === args.length - 1 ? new _Length2.default('100%') : null;
        colorStops.push({ color: _color, stop: _stop });
    }

    var absoluteValuedColorStops = colorStops.map(function (_ref2) {
        var color = _ref2.color,
            stop = _ref2.stop;

        var absoluteStop = lineLength === 0 ? 0 : stop ? stop.getAbsoluteValue(lineLength) / lineLength : null;

        return {
            color: color,
            // $FlowFixMe
            stop: absoluteStop
        };
    });

    var previousColorStop = absoluteValuedColorStops[0].stop;
    for (var _i = 0; _i < absoluteValuedColorStops.length; _i++) {
        if (previousColorStop !== null) {
            var _stop2 = absoluteValuedColorStops[_i].stop;
            if (_stop2 === null) {
                var n = _i;
                while (absoluteValuedColorStops[n].stop === null) {
                    n++;
                }
                var steps = n - _i + 1;
                var nextColorStep = absoluteValuedColorStops[n].stop;
                var stepSize = (nextColorStep - previousColorStop) / steps;
                for (; _i < n; _i++) {
                    previousColorStop = absoluteValuedColorStops[_i].stop = previousColorStop + stepSize;
                }
            } else {
                previousColorStop = _stop2;
            }
        }
    }

    return absoluteValuedColorStops;
};

var parseLinearGradient = function parseLinearGradient(args, bounds, hasPrefix) {
    var angle = (0, _Angle.parseAngle)(args[0]);
    var HAS_SIDE_OR_CORNER = SIDE_OR_CORNER.test(args[0]);
    var HAS_DIRECTION = HAS_SIDE_OR_CORNER || angle !== null || PERCENTAGE_ANGLES.test(args[0]);
    var direction = HAS_DIRECTION ? angle !== null ? calculateGradientDirection(
    // if there is a prefix, the 0° angle points due East (instead of North per W3C)
    hasPrefix ? angle - Math.PI * 0.5 : angle, bounds) : HAS_SIDE_OR_CORNER ? parseSideOrCorner(args[0], bounds) : parsePercentageAngle(args[0], bounds) : calculateGradientDirection(Math.PI, bounds);
    var firstColorStopIndex = HAS_DIRECTION ? 1 : 0;

    // TODO: Fix some inaccuracy with color stops with px values
    var lineLength = Math.min((0, _Util.distance)(Math.abs(direction.x0) + Math.abs(direction.x1), Math.abs(direction.y0) + Math.abs(direction.y1)), bounds.width * 2, bounds.height * 2);

    return new LinearGradient(parseColorStops(args, firstColorStopIndex, lineLength), direction);
};

var parseRadialGradient = function parseRadialGradient(container, args, bounds) {
    var m = args[0].match(RADIAL_SHAPE_DEFINITION);
    var shape = m && (m[1] === 'circle' || // explicit shape specification
    m[3] !== undefined && m[5] === undefined) // only one radius coordinate
    ? RADIAL_GRADIENT_SHAPE.CIRCLE : RADIAL_GRADIENT_SHAPE.ELLIPSE;
    var radius = {};
    var center = {};

    if (m) {
        // Radius
        if (m[3] !== undefined) {
            radius.x = (0, _Length.calculateLengthFromValueWithUnit)(container, m[3], m[4]).getAbsoluteValue(bounds.width);
        }

        if (m[5] !== undefined) {
            radius.y = (0, _Length.calculateLengthFromValueWithUnit)(container, m[5], m[6]).getAbsoluteValue(bounds.height);
        }

        // Position
        if (m[7]) {
            center.x = LENGTH_FOR_POSITION[m[7].toLowerCase()];
        } else if (m[8] !== undefined) {
            center.x = (0, _Length.calculateLengthFromValueWithUnit)(container, m[8], m[9]);
        }

        if (m[10]) {
            center.y = LENGTH_FOR_POSITION[m[10].toLowerCase()];
        } else if (m[11] !== undefined) {
            center.y = (0, _Length.calculateLengthFromValueWithUnit)(container, m[11], m[12]);
        }
    }

    var gradientCenter = {
        x: center.x === undefined ? bounds.width / 2 : center.x.getAbsoluteValue(bounds.width),
        y: center.y === undefined ? bounds.height / 2 : center.y.getAbsoluteValue(bounds.height)
    };
    var gradientRadius = calculateRadius(m && m[2] || 'farthest-corner', shape, gradientCenter, radius, bounds);

    return new RadialGradient(parseColorStops(args, m ? 1 : 0, Math.min(gradientRadius.x, gradientRadius.y)), shape, gradientCenter, gradientRadius);
};

var calculateGradientDirection = function calculateGradientDirection(radian, bounds) {
    var width = bounds.width;
    var height = bounds.height;
    var HALF_WIDTH = width * 0.5;
    var HALF_HEIGHT = height * 0.5;
    var lineLength = Math.abs(width * Math.sin(radian)) + Math.abs(height * Math.cos(radian));
    var HALF_LINE_LENGTH = lineLength / 2;

    var x0 = HALF_WIDTH + Math.sin(radian) * HALF_LINE_LENGTH;
    var y0 = HALF_HEIGHT - Math.cos(radian) * HALF_LINE_LENGTH;
    var x1 = width - x0;
    var y1 = height - y0;

    return { x0: x0, x1: x1, y0: y0, y1: y1 };
};

var parseTopRight = function parseTopRight(bounds) {
    return Math.acos(bounds.width / 2 / ((0, _Util.distance)(bounds.width, bounds.height) / 2));
};

var parseSideOrCorner = function parseSideOrCorner(side, bounds) {
    switch (side) {
        case 'bottom':
        case 'to top':
            return calculateGradientDirection(0, bounds);
        case 'left':
        case 'to right':
            return calculateGradientDirection(Math.PI / 2, bounds);
        case 'right':
        case 'to left':
            return calculateGradientDirection(3 * Math.PI / 2, bounds);
        case 'top right':
        case 'right top':
        case 'to bottom left':
        case 'to left bottom':
            return calculateGradientDirection(Math.PI + parseTopRight(bounds), bounds);
        case 'top left':
        case 'left top':
        case 'to bottom right':
        case 'to right bottom':
            return calculateGradientDirection(Math.PI - parseTopRight(bounds), bounds);
        case 'bottom left':
        case 'left bottom':
        case 'to top right':
        case 'to right top':
            return calculateGradientDirection(parseTopRight(bounds), bounds);
        case 'bottom right':
        case 'right bottom':
        case 'to top left':
        case 'to left top':
            return calculateGradientDirection(2 * Math.PI - parseTopRight(bounds), bounds);
        case 'top':
        case 'to bottom':
        default:
            return calculateGradientDirection(Math.PI, bounds);
    }
};

var parsePercentageAngle = function parsePercentageAngle(angle, bounds) {
    var _angle$split$map = angle.split(' ').map(parseFloat),
        _angle$split$map2 = _slicedToArray(_angle$split$map, 2),
        left = _angle$split$map2[0],
        top = _angle$split$map2[1];

    var ratio = left / 100 * bounds.width / (top / 100 * bounds.height);

    return calculateGradientDirection(Math.atan(isNaN(ratio) ? 1 : ratio) + Math.PI / 2, bounds);
};

var findCorner = function findCorner(bounds, x, y, closest) {
    var corners = [{ x: 0, y: 0 }, { x: 0, y: bounds.height }, { x: bounds.width, y: 0 }, { x: bounds.width, y: bounds.height }];

    // $FlowFixMe
    return corners.reduce(function (stat, corner) {
        var d = (0, _Util.distance)(x - corner.x, y - corner.y);
        if (closest ? d < stat.optimumDistance : d > stat.optimumDistance) {
            return {
                optimumCorner: corner,
                optimumDistance: d
            };
        }

        return stat;
    }, {
        optimumDistance: closest ? Infinity : -Infinity,
        optimumCorner: null
    }).optimumCorner;
};

var calculateRadius = function calculateRadius(extent, shape, center, radius, bounds) {
    var x = center.x;
    var y = center.y;
    var rx = 0;
    var ry = 0;

    switch (extent) {
        case 'closest-side':
            // The ending shape is sized so that that it exactly meets the side of the gradient box closest to the gradient’s center.
            // If the shape is an ellipse, it exactly meets the closest side in each dimension.
            if (shape === RADIAL_GRADIENT_SHAPE.CIRCLE) {
                rx = ry = Math.min(Math.abs(x), Math.abs(x - bounds.width), Math.abs(y), Math.abs(y - bounds.height));
            } else if (shape === RADIAL_GRADIENT_SHAPE.ELLIPSE) {
                rx = Math.min(Math.abs(x), Math.abs(x - bounds.width));
                ry = Math.min(Math.abs(y), Math.abs(y - bounds.height));
            }
            break;

        case 'closest-corner':
            // The ending shape is sized so that that it passes through the corner of the gradient box closest to the gradient’s center.
            // If the shape is an ellipse, the ending shape is given the same aspect-ratio it would have if closest-side were specified.
            if (shape === RADIAL_GRADIENT_SHAPE.CIRCLE) {
                rx = ry = Math.min((0, _Util.distance)(x, y), (0, _Util.distance)(x, y - bounds.height), (0, _Util.distance)(x - bounds.width, y), (0, _Util.distance)(x - bounds.width, y - bounds.height));
            } else if (shape === RADIAL_GRADIENT_SHAPE.ELLIPSE) {
                // Compute the ratio ry/rx (which is to be the same as for "closest-side")
                var c = Math.min(Math.abs(y), Math.abs(y - bounds.height)) / Math.min(Math.abs(x), Math.abs(x - bounds.width));
                var corner = findCorner(bounds, x, y, true);
                rx = (0, _Util.distance)(corner.x - x, (corner.y - y) / c);
                ry = c * rx;
            }
            break;

        case 'farthest-side':
            // Same as closest-side, except the ending shape is sized based on the farthest side(s)
            if (shape === RADIAL_GRADIENT_SHAPE.CIRCLE) {
                rx = ry = Math.max(Math.abs(x), Math.abs(x - bounds.width), Math.abs(y), Math.abs(y - bounds.height));
            } else if (shape === RADIAL_GRADIENT_SHAPE.ELLIPSE) {
                rx = Math.max(Math.abs(x), Math.abs(x - bounds.width));
                ry = Math.max(Math.abs(y), Math.abs(y - bounds.height));
            }
            break;

        case 'farthest-corner':
            // Same as closest-corner, except the ending shape is sized based on the farthest corner.
            // If the shape is an ellipse, the ending shape is given the same aspect ratio it would have if farthest-side were specified.
            if (shape === RADIAL_GRADIENT_SHAPE.CIRCLE) {
                rx = ry = Math.max((0, _Util.distance)(x, y), (0, _Util.distance)(x, y - bounds.height), (0, _Util.distance)(x - bounds.width, y), (0, _Util.distance)(x - bounds.width, y - bounds.height));
            } else if (shape === RADIAL_GRADIENT_SHAPE.ELLIPSE) {
                // Compute the ratio ry/rx (which is to be the same as for "farthest-side")
                var _c = Math.max(Math.abs(y), Math.abs(y - bounds.height)) / Math.max(Math.abs(x), Math.abs(x - bounds.width));
                var _corner = findCorner(bounds, x, y, false);
                rx = (0, _Util.distance)(_corner.x - x, (_corner.y - y) / _c);
                ry = _c * rx;
            }
            break;

        default:
            // pixel or percentage values
            rx = radius.x || 0;
            ry = radius.y !== undefined ? radius.y : rx;
            break;
    }

    return {
        x: rx,
        y: ry
    };
};

var transformWebkitRadialGradientArgs = exports.transformWebkitRadialGradientArgs = function transformWebkitRadialGradientArgs(args) {
    var shape = '';
    var radius = '';
    var extent = '';
    var position = '';
    var idx = 0;

    var POSITION = /^(left|center|right|\d+(?:px|r?em|%)?)(?:\s+(top|center|bottom|\d+(?:px|r?em|%)?))?$/i;
    var SHAPE_AND_EXTENT = /^(circle|ellipse)?\s*(closest-side|closest-corner|farthest-side|farthest-corner|contain|cover)?$/i;
    var RADIUS = /^\d+(px|r?em|%)?(?:\s+\d+(px|r?em|%)?)?$/i;

    var matchStartPosition = args[idx].match(POSITION);
    if (matchStartPosition) {
        idx++;
    }

    var matchShapeExtent = args[idx].match(SHAPE_AND_EXTENT);
    if (matchShapeExtent) {
        shape = matchShapeExtent[1] || '';
        extent = matchShapeExtent[2] || '';
        if (extent === 'contain') {
            extent = 'closest-side';
        } else if (extent === 'cover') {
            extent = 'farthest-corner';
        }
        idx++;
    }

    var matchStartRadius = args[idx].match(RADIUS);
    if (matchStartRadius) {
        idx++;
    }

    var matchEndPosition = args[idx].match(POSITION);
    if (matchEndPosition) {
        idx++;
    }

    var matchEndRadius = args[idx].match(RADIUS);
    if (matchEndRadius) {
        idx++;
    }

    var matchPosition = matchEndPosition || matchStartPosition;
    if (matchPosition && matchPosition[1]) {
        position = matchPosition[1] + (/^\d+$/.test(matchPosition[1]) ? 'px' : '');
        if (matchPosition[2]) {
            position += ' ' + matchPosition[2] + (/^\d+$/.test(matchPosition[2]) ? 'px' : '');
        }
    }

    var matchRadius = matchEndRadius || matchStartRadius;
    if (matchRadius) {
        radius = matchRadius[0];
        if (!matchRadius[1]) {
            radius += 'px';
        }
    }

    if (position && !shape && !radius && !extent) {
        radius = position;
        position = '';
    }

    if (position) {
        position = 'at ' + position;
    }

    return [[shape, extent, radius, position].filter(function (s) {
        return !!s;
    }).join(' ')].concat(args.slice(idx));
};

var transformObsoleteColorStops = function transformObsoleteColorStops(args) {
    return args.map(function (color) {
        return color.match(FROM_TO_COLORSTOP);
    })
    // $FlowFixMe
    .map(function (v, index) {
        if (!v) {
            return args[index];
        }

        switch (v[1]) {
            case 'from':
                return v[4] + ' 0%';
            case 'to':
                return v[4] + ' 100%';
            case 'color-stop':
                if (v[3] === '%') {
                    return v[4] + ' ' + v[2];
                }
                return v[4] + ' ' + parseFloat(v[2]) * 100 + '%';
        }
    });
};

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var ANGLE = /([+-]?\d*\.?\d+)(deg|grad|rad|turn)/i;

var parseAngle = exports.parseAngle = function parseAngle(angle) {
    var match = angle.match(ANGLE);

    if (match) {
        var value = parseFloat(match[1]);
        switch (match[2].toLowerCase()) {
            case 'deg':
                return Math.PI * value / 180;
            case 'grad':
                return Math.PI / 200 * value;
            case 'rad':
                return value;
            case 'turn':
                return Math.PI * 2 * value;
        }
    }

    return null;
};

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.cloneWindow = exports.DocumentCloner = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Bounds = __webpack_require__(2);

var _Proxy = __webpack_require__(26);

var _ResourceLoader = __webpack_require__(55);

var _ResourceLoader2 = _interopRequireDefault(_ResourceLoader);

var _Util = __webpack_require__(4);

var _background = __webpack_require__(5);

var _CanvasRenderer = __webpack_require__(15);

var _CanvasRenderer2 = _interopRequireDefault(_CanvasRenderer);

var _PseudoNodeContent = __webpack_require__(56);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var IGNORE_ATTRIBUTE = 'data-html2canvas-ignore';

var DocumentCloner = exports.DocumentCloner = function () {
    function DocumentCloner(element, options, logger, copyInline, renderer) {
        _classCallCheck(this, DocumentCloner);

        this.referenceElement = element;
        this.scrolledElements = [];
        this.copyStyles = copyInline;
        this.inlineImages = copyInline;
        this.logger = logger;
        this.options = options;
        this.renderer = renderer;
        this.resourceLoader = new _ResourceLoader2.default(options, logger, window);
        this.pseudoContentData = {
            counters: {},
            quoteDepth: 0
        };
        // $FlowFixMe
        this.documentElement = this.cloneNode(element.ownerDocument.documentElement);
    }

    _createClass(DocumentCloner, [{
        key: 'inlineAllImages',
        value: function inlineAllImages(node) {
            var _this = this;

            if (this.inlineImages && node) {
                var style = node.style;
                Promise.all((0, _background.parseBackgroundImage)(style.backgroundImage).map(function (backgroundImage) {
                    if (backgroundImage.method === 'url') {
                        return _this.resourceLoader.inlineImage(backgroundImage.args[0]).then(function (img) {
                            return img && typeof img.src === 'string' ? 'url("' + img.src + '")' : 'none';
                        }).catch(function (e) {
                            if (true) {
                                _this.logger.log('Unable to load image', e);
                            }
                        });
                    }
                    return Promise.resolve('' + backgroundImage.prefix + backgroundImage.method + '(' + backgroundImage.args.join(',') + ')');
                })).then(function (backgroundImages) {
                    if (backgroundImages.length > 1) {
                        // TODO Multiple backgrounds somehow broken in Chrome
                        style.backgroundColor = '';
                    }
                    style.backgroundImage = backgroundImages.join(',');
                });

                if (node instanceof HTMLImageElement) {
                    this.resourceLoader.inlineImage(node.src).then(function (img) {
                        if (img && node instanceof HTMLImageElement && node.parentNode) {
                            var parentNode = node.parentNode;
                            var clonedChild = (0, _Util.copyCSSStyles)(node.style, img.cloneNode(false));
                            parentNode.replaceChild(clonedChild, node);
                        }
                    }).catch(function (e) {
                        if (true) {
                            _this.logger.log('Unable to load image', e);
                        }
                    });
                }
            }
        }
    }, {
        key: 'inlineFonts',
        value: function inlineFonts(document) {
            var _this2 = this;

            return Promise.all(Array.from(document.styleSheets).map(function (sheet) {
                if (sheet.href) {
                    return fetch(sheet.href).then(function (res) {
                        return res.text();
                    }).then(function (text) {
                        return createStyleSheetFontsFromText(text, sheet.href);
                    }).catch(function (e) {
                        if (true) {
                            _this2.logger.log('Unable to load stylesheet', e);
                        }
                        return [];
                    });
                }
                return getSheetFonts(sheet, document);
            })).then(function (fonts) {
                return fonts.reduce(function (acc, font) {
                    return acc.concat(font);
                }, []);
            }).then(function (fonts) {
                return Promise.all(fonts.map(function (font) {
                    return fetch(font.formats[0].src).then(function (response) {
                        return response.blob();
                    }).then(function (blob) {
                        return new Promise(function (resolve, reject) {
                            var reader = new FileReader();
                            reader.onerror = reject;
                            reader.onload = function () {
                                // $FlowFixMe
                                var result = reader.result;
                                resolve(result);
                            };
                            reader.readAsDataURL(blob);
                        });
                    }).then(function (dataUri) {
                        font.fontFace.setProperty('src', 'url("' + dataUri + '")');
                        return '@font-face {' + font.fontFace.cssText + ' ';
                    });
                }));
            }).then(function (fontCss) {
                var style = document.createElement('style');
                style.textContent = fontCss.join('\n');
                _this2.documentElement.appendChild(style);
            });
        }
    }, {
        key: 'createElementClone',
        value: function createElementClone(node) {
            var _this3 = this;

            if (this.copyStyles && node instanceof HTMLCanvasElement) {
                var img = node.ownerDocument.createElement('img');
                try {
                    img.src = node.toDataURL();
                    return img;
                } catch (e) {
                    if (true) {
                        this.logger.log('Unable to clone canvas contents, canvas is tainted');
                    }
                }
            }

            if (node instanceof HTMLIFrameElement) {
                var tempIframe = node.cloneNode(false);
                var iframeKey = generateIframeKey();
                tempIframe.setAttribute('data-html2canvas-internal-iframe-key', iframeKey);

                var _parseBounds = (0, _Bounds.parseBounds)(node, 0, 0),
                    width = _parseBounds.width,
                    height = _parseBounds.height;

                this.resourceLoader.cache[iframeKey] = getIframeDocumentElement(node, this.options).then(function (documentElement) {
                    return _this3.renderer(documentElement, {
                        async: _this3.options.async,
                        allowTaint: _this3.options.allowTaint,
                        backgroundColor: '#ffffff',
                        canvas: null,
                        imageTimeout: _this3.options.imageTimeout,
                        logging: _this3.options.logging,
                        proxy: _this3.options.proxy,
                        removeContainer: _this3.options.removeContainer,
                        scale: _this3.options.scale,
                        foreignObjectRendering: _this3.options.foreignObjectRendering,
                        useCORS: _this3.options.useCORS,
                        target: new _CanvasRenderer2.default(),
                        width: width,
                        height: height,
                        x: 0,
                        y: 0,
                        windowWidth: documentElement.ownerDocument.defaultView.innerWidth,
                        windowHeight: documentElement.ownerDocument.defaultView.innerHeight,
                        scrollX: documentElement.ownerDocument.defaultView.pageXOffset,
                        scrollY: documentElement.ownerDocument.defaultView.pageYOffset
                    }, _this3.logger.child(iframeKey));
                }).then(function (canvas) {
                    return new Promise(function (resolve, reject) {
                        var iframeCanvas = document.createElement('img');
                        iframeCanvas.onload = function () {
                            return resolve(canvas);
                        };
                        iframeCanvas.onerror = reject;
                        iframeCanvas.src = canvas.toDataURL();
                        if (tempIframe.parentNode) {
                            tempIframe.parentNode.replaceChild((0, _Util.copyCSSStyles)(node.ownerDocument.defaultView.getComputedStyle(node), iframeCanvas), tempIframe);
                        }
                    });
                });
                return tempIframe;
            }

            if (node instanceof HTMLStyleElement && node.sheet && node.sheet.cssRules) {
                var css = [].slice.call(node.sheet.cssRules, 0).reduce(function (css, rule) {
                    try {
                        if (rule && rule.cssText) {
                            return css + rule.cssText;
                        }
                        return css;
                    } catch (err) {
                        _this3.logger.log('Unable to access cssText property', rule.name);
                        return css;
                    }
                }, '');
                var style = node.cloneNode(false);
                style.textContent = css;
                return style;
            }

            return node.cloneNode(false);
        }
    }, {
        key: 'cloneNode',
        value: function cloneNode(node) {
            var clone = node.nodeType === Node.TEXT_NODE ? document.createTextNode(node.nodeValue) : this.createElementClone(node);

            var window = node.ownerDocument.defaultView;
            var style = node instanceof window.HTMLElement ? window.getComputedStyle(node) : null;
            var styleBefore = node instanceof window.HTMLElement ? window.getComputedStyle(node, ':before') : null;
            var styleAfter = node instanceof window.HTMLElement ? window.getComputedStyle(node, ':after') : null;

            if (this.referenceElement === node && clone instanceof window.HTMLElement) {
                this.clonedReferenceElement = clone;
            }

            if (clone instanceof window.HTMLBodyElement) {
                createPseudoHideStyles(clone);
            }

            var counters = (0, _PseudoNodeContent.parseCounterReset)(style, this.pseudoContentData);
            var contentBefore = (0, _PseudoNodeContent.resolvePseudoContent)(node, styleBefore, this.pseudoContentData);

            for (var child = node.firstChild; child; child = child.nextSibling) {
                if (child.nodeType !== Node.ELEMENT_NODE || child.nodeName !== 'SCRIPT' &&
                // $FlowFixMe
                !child.hasAttribute(IGNORE_ATTRIBUTE) && (typeof this.options.ignoreElements !== 'function' ||
                // $FlowFixMe
                !this.options.ignoreElements(child))) {
                    if (!this.copyStyles || child.nodeName !== 'STYLE') {
                        clone.appendChild(this.cloneNode(child));
                    }
                }
            }

            var contentAfter = (0, _PseudoNodeContent.resolvePseudoContent)(node, styleAfter, this.pseudoContentData);
            (0, _PseudoNodeContent.popCounters)(counters, this.pseudoContentData);

            if (node instanceof window.HTMLElement && clone instanceof window.HTMLElement) {
                if (styleBefore) {
                    this.inlineAllImages(inlinePseudoElement(node, clone, styleBefore, contentBefore, PSEUDO_BEFORE));
                }
                if (styleAfter) {
                    this.inlineAllImages(inlinePseudoElement(node, clone, styleAfter, contentAfter, PSEUDO_AFTER));
                }
                if (style && this.copyStyles && !(node instanceof HTMLIFrameElement)) {
                    (0, _Util.copyCSSStyles)(style, clone);
                }
                this.inlineAllImages(clone);
                if (node.scrollTop !== 0 || node.scrollLeft !== 0) {
                    this.scrolledElements.push([clone, node.scrollLeft, node.scrollTop]);
                }
                switch (node.nodeName) {
                    case 'CANVAS':
                        if (!this.copyStyles) {
                            cloneCanvasContents(node, clone);
                        }
                        break;
                    case 'TEXTAREA':
                    case 'SELECT':
                        clone.value = node.value;
                        break;
                }
            }
            return clone;
        }
    }]);

    return DocumentCloner;
}();

var getSheetFonts = function getSheetFonts(sheet, document) {
    // $FlowFixMe
    return (sheet.cssRules ? Array.from(sheet.cssRules) : []).filter(function (rule) {
        return rule.type === CSSRule.FONT_FACE_RULE;
    }).map(function (rule) {
        var src = (0, _background.parseBackgroundImage)(rule.style.getPropertyValue('src'));
        var formats = [];
        for (var i = 0; i < src.length; i++) {
            if (src[i].method === 'url' && src[i + 1] && src[i + 1].method === 'format') {
                var a = document.createElement('a');
                a.href = src[i].args[0];
                if (document.body) {
                    document.body.appendChild(a);
                }

                var font = {
                    src: a.href,
                    format: src[i + 1].args[0]
                };
                formats.push(font);
            }
        }

        return {
            // TODO select correct format for browser),

            formats: formats.filter(function (font) {
                return (/^woff/i.test(font.format)
                );
            }),
            fontFace: rule.style
        };
    }).filter(function (font) {
        return font.formats.length;
    });
};

var createStyleSheetFontsFromText = function createStyleSheetFontsFromText(text, baseHref) {
    var doc = document.implementation.createHTMLDocument('');
    var base = document.createElement('base');
    // $FlowFixMe
    base.href = baseHref;
    var style = document.createElement('style');

    style.textContent = text;
    if (doc.head) {
        doc.head.appendChild(base);
    }
    if (doc.body) {
        doc.body.appendChild(style);
    }

    return style.sheet ? getSheetFonts(style.sheet, doc) : [];
};

var restoreOwnerScroll = function restoreOwnerScroll(ownerDocument, x, y) {
    if (ownerDocument.defaultView && (x !== ownerDocument.defaultView.pageXOffset || y !== ownerDocument.defaultView.pageYOffset)) {
        ownerDocument.defaultView.scrollTo(x, y);
    }
};

var cloneCanvasContents = function cloneCanvasContents(canvas, clonedCanvas) {
    try {
        if (clonedCanvas) {
            clonedCanvas.width = canvas.width;
            clonedCanvas.height = canvas.height;
            var ctx = canvas.getContext('2d');
            var clonedCtx = clonedCanvas.getContext('2d');
            if (ctx) {
                clonedCtx.putImageData(ctx.getImageData(0, 0, canvas.width, canvas.height), 0, 0);
            } else {
                clonedCtx.drawImage(canvas, 0, 0);
            }
        }
    } catch (e) {}
};

var inlinePseudoElement = function inlinePseudoElement(node, clone, style, contentItems, pseudoElt) {
    if (!style || !style.content || style.content === 'none' || style.content === '-moz-alt-content' || style.display === 'none') {
        return;
    }

    var anonymousReplacedElement = clone.ownerDocument.createElement('html2canvaspseudoelement');
    (0, _Util.copyCSSStyles)(style, anonymousReplacedElement);

    if (contentItems) {
        var len = contentItems.length;
        for (var i = 0; i < len; i++) {
            var item = contentItems[i];
            switch (item.type) {
                case _PseudoNodeContent.PSEUDO_CONTENT_ITEM_TYPE.IMAGE:
                    var img = clone.ownerDocument.createElement('img');
                    img.src = (0, _background.parseBackgroundImage)('url(' + item.value + ')')[0].args[0];
                    img.style.opacity = '1';
                    anonymousReplacedElement.appendChild(img);
                    break;
                case _PseudoNodeContent.PSEUDO_CONTENT_ITEM_TYPE.TEXT:
                    anonymousReplacedElement.appendChild(clone.ownerDocument.createTextNode(item.value));
                    break;
            }
        }
    }

    anonymousReplacedElement.className = PSEUDO_HIDE_ELEMENT_CLASS_BEFORE + ' ' + PSEUDO_HIDE_ELEMENT_CLASS_AFTER;
    clone.className += pseudoElt === PSEUDO_BEFORE ? ' ' + PSEUDO_HIDE_ELEMENT_CLASS_BEFORE : ' ' + PSEUDO_HIDE_ELEMENT_CLASS_AFTER;
    if (pseudoElt === PSEUDO_BEFORE) {
        clone.insertBefore(anonymousReplacedElement, clone.firstChild);
    } else {
        clone.appendChild(anonymousReplacedElement);
    }

    return anonymousReplacedElement;
};

var URL_REGEXP = /^url\((.+)\)$/i;
var PSEUDO_BEFORE = ':before';
var PSEUDO_AFTER = ':after';
var PSEUDO_HIDE_ELEMENT_CLASS_BEFORE = '___html2canvas___pseudoelement_before';
var PSEUDO_HIDE_ELEMENT_CLASS_AFTER = '___html2canvas___pseudoelement_after';

var PSEUDO_HIDE_ELEMENT_STYLE = '{\n    content: "" !important;\n    display: none !important;\n}';

var createPseudoHideStyles = function createPseudoHideStyles(body) {
    createStyles(body, '.' + PSEUDO_HIDE_ELEMENT_CLASS_BEFORE + PSEUDO_BEFORE + PSEUDO_HIDE_ELEMENT_STYLE + '\n         .' + PSEUDO_HIDE_ELEMENT_CLASS_AFTER + PSEUDO_AFTER + PSEUDO_HIDE_ELEMENT_STYLE);
};

var createStyles = function createStyles(body, styles) {
    var style = body.ownerDocument.createElement('style');
    style.innerHTML = styles;
    body.appendChild(style);
};

var initNode = function initNode(_ref) {
    var _ref2 = _slicedToArray(_ref, 3),
        element = _ref2[0],
        x = _ref2[1],
        y = _ref2[2];

    element.scrollLeft = x;
    element.scrollTop = y;
};

var generateIframeKey = function generateIframeKey() {
    return Math.ceil(Date.now() + Math.random() * 10000000).toString(16);
};

var DATA_URI_REGEXP = /^data:text\/(.+);(base64)?,(.*)$/i;

var getIframeDocumentElement = function getIframeDocumentElement(node, options) {
    try {
        return Promise.resolve(node.contentWindow.document.documentElement);
    } catch (e) {
        return options.proxy ? (0, _Proxy.Proxy)(node.src, options).then(function (html) {
            var match = html.match(DATA_URI_REGEXP);
            if (!match) {
                return Promise.reject();
            }

            return match[2] === 'base64' ? window.atob(decodeURIComponent(match[3])) : decodeURIComponent(match[3]);
        }).then(function (html) {
            return createIframeContainer(node.ownerDocument, (0, _Bounds.parseBounds)(node, 0, 0)).then(function (cloneIframeContainer) {
                var cloneWindow = cloneIframeContainer.contentWindow;
                var documentClone = cloneWindow.document;

                documentClone.open();
                documentClone.write(html);
                var iframeLoad = iframeLoader(cloneIframeContainer).then(function () {
                    return documentClone.documentElement;
                });

                documentClone.close();
                return iframeLoad;
            });
        }) : Promise.reject();
    }
};

var createIframeContainer = function createIframeContainer(ownerDocument, bounds) {
    var cloneIframeContainer = ownerDocument.createElement('iframe');

    cloneIframeContainer.className = 'html2canvas-container';
    cloneIframeContainer.style.visibility = 'hidden';
    cloneIframeContainer.style.position = 'fixed';
    cloneIframeContainer.style.left = '-10000px';
    cloneIframeContainer.style.top = '0px';
    cloneIframeContainer.style.border = '0';
    cloneIframeContainer.width = bounds.width.toString();
    cloneIframeContainer.height = bounds.height.toString();
    cloneIframeContainer.scrolling = 'no'; // ios won't scroll without it
    cloneIframeContainer.setAttribute(IGNORE_ATTRIBUTE, 'true');
    if (!ownerDocument.body) {
        return Promise.reject( true ? 'Body element not found in Document that is getting rendered' : '');
    }

    ownerDocument.body.appendChild(cloneIframeContainer);

    return Promise.resolve(cloneIframeContainer);
};

var iframeLoader = function iframeLoader(cloneIframeContainer) {
    var cloneWindow = cloneIframeContainer.contentWindow;
    var documentClone = cloneWindow.document;

    return new Promise(function (resolve, reject) {
        cloneWindow.onload = cloneIframeContainer.onload = documentClone.onreadystatechange = function () {
            var interval = setInterval(function () {
                if (documentClone.body.childNodes.length > 0 && documentClone.readyState === 'complete') {
                    clearInterval(interval);
                    resolve(cloneIframeContainer);
                }
            }, 50);
        };
    });
};

var cloneWindow = exports.cloneWindow = function cloneWindow(ownerDocument, bounds, referenceElement, options, logger, renderer) {
    var cloner = new DocumentCloner(referenceElement, options, logger, false, renderer);
    var scrollX = ownerDocument.defaultView.pageXOffset;
    var scrollY = ownerDocument.defaultView.pageYOffset;

    return createIframeContainer(ownerDocument, bounds).then(function (cloneIframeContainer) {
        var cloneWindow = cloneIframeContainer.contentWindow;
        var documentClone = cloneWindow.document;

        /* Chrome doesn't detect relative background-images assigned in inline <style> sheets when fetched through getComputedStyle
             if window url is about:blank, we can assign the url to current by writing onto the document
             */

        var iframeLoad = iframeLoader(cloneIframeContainer).then(function () {
            cloner.scrolledElements.forEach(initNode);
            cloneWindow.scrollTo(bounds.left, bounds.top);
            if (/(iPad|iPhone|iPod)/g.test(navigator.userAgent) && (cloneWindow.scrollY !== bounds.top || cloneWindow.scrollX !== bounds.left)) {
                documentClone.documentElement.style.top = -bounds.top + 'px';
                documentClone.documentElement.style.left = -bounds.left + 'px';
                documentClone.documentElement.style.position = 'absolute';
            }

            var result = Promise.resolve([cloneIframeContainer, cloner.clonedReferenceElement, cloner.resourceLoader]);

            var onclone = options.onclone;

            return cloner.clonedReferenceElement instanceof cloneWindow.HTMLElement || cloner.clonedReferenceElement instanceof ownerDocument.defaultView.HTMLElement || cloner.clonedReferenceElement instanceof HTMLElement ? typeof onclone === 'function' ? Promise.resolve().then(function () {
                return onclone(documentClone);
            }).then(function () {
                return result;
            }) : result : Promise.reject( true ? 'Error finding the ' + referenceElement.nodeName + ' in the cloned document' : '');
        });

        documentClone.open();
        documentClone.write(serializeDoctype(document.doctype) + '<html></html>');
        // Chrome scrolls the parent document for some reason after the write to the cloned window???
        restoreOwnerScroll(referenceElement.ownerDocument, scrollX, scrollY);
        documentClone.replaceChild(documentClone.adoptNode(cloner.documentElement), documentClone.documentElement);
        documentClone.close();

        return iframeLoad;
    });
};

var serializeDoctype = function serializeDoctype(doctype) {
    var str = '';
    if (doctype) {
        str += '<!DOCTYPE ';
        if (doctype.name) {
            str += doctype.name;
        }

        if (doctype.internalSubset) {
            str += doctype.internalSubset;
        }

        if (doctype.publicId) {
            str += '"' + doctype.publicId + '"';
        }

        if (doctype.systemId) {
            str += '"' + doctype.systemId + '"';
        }

        str += '>';
    }

    return str;
};

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ResourceStore = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Feature = __webpack_require__(10);

var _Feature2 = _interopRequireDefault(_Feature);

var _Proxy = __webpack_require__(26);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ResourceLoader = function () {
    function ResourceLoader(options, logger, window) {
        _classCallCheck(this, ResourceLoader);

        this.options = options;
        this._window = window;
        this.origin = this.getOrigin(window.location.href);
        this.cache = {};
        this.logger = logger;
        this._index = 0;
    }

    _createClass(ResourceLoader, [{
        key: 'loadImage',
        value: function loadImage(src) {
            var _this = this;

            if (this.hasResourceInCache(src)) {
                return src;
            }
            if (isBlobImage(src)) {
                this.cache[src] = _loadImage(src, this.options.imageTimeout || 0);
                return src;
            }

            if (!isSVG(src) || _Feature2.default.SUPPORT_SVG_DRAWING) {
                if (this.options.allowTaint === true || isInlineImage(src) || this.isSameOrigin(src)) {
                    return this.addImage(src, src, false);
                } else if (!this.isSameOrigin(src)) {
                    if (typeof this.options.proxy === 'string') {
                        this.cache[src] = (0, _Proxy.Proxy)(src, this.options).then(function (src) {
                            return _loadImage(src, _this.options.imageTimeout || 0);
                        });
                        return src;
                    } else if (this.options.useCORS === true && _Feature2.default.SUPPORT_CORS_IMAGES) {
                        return this.addImage(src, src, true);
                    }
                }
            }
        }
    }, {
        key: 'inlineImage',
        value: function inlineImage(src) {
            var _this2 = this;

            if (isInlineImage(src)) {
                return _loadImage(src, this.options.imageTimeout || 0);
            }
            if (this.hasResourceInCache(src)) {
                return this.cache[src];
            }
            if (!this.isSameOrigin(src) && typeof this.options.proxy === 'string') {
                return this.cache[src] = (0, _Proxy.Proxy)(src, this.options).then(function (src) {
                    return _loadImage(src, _this2.options.imageTimeout || 0);
                });
            }

            return this.xhrImage(src);
        }
    }, {
        key: 'xhrImage',
        value: function xhrImage(src) {
            var _this3 = this;

            this.cache[src] = new Promise(function (resolve, reject) {
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status !== 200) {
                            reject('Failed to fetch image ' + src.substring(0, 256) + ' with status code ' + xhr.status);
                        } else {
                            var reader = new FileReader();
                            reader.addEventListener('load', function () {
                                // $FlowFixMe
                                var result = reader.result;
                                resolve(result);
                            }, false);
                            reader.addEventListener('error', function (e) {
                                return reject(e);
                            }, false);
                            reader.readAsDataURL(xhr.response);
                        }
                    }
                };
                xhr.responseType = 'blob';
                if (_this3.options.imageTimeout) {
                    var timeout = _this3.options.imageTimeout;
                    xhr.timeout = timeout;
                    xhr.ontimeout = function () {
                        return reject( true ? 'Timed out (' + timeout + 'ms) fetching ' + src.substring(0, 256) : '');
                    };
                }
                xhr.open('GET', src, true);
                xhr.send();
            }).then(function (src) {
                return _loadImage(src, _this3.options.imageTimeout || 0);
            });

            return this.cache[src];
        }
    }, {
        key: 'loadCanvas',
        value: function loadCanvas(node) {
            var key = String(this._index++);
            this.cache[key] = Promise.resolve(node);
            return key;
        }
    }, {
        key: 'hasResourceInCache',
        value: function hasResourceInCache(key) {
            return typeof this.cache[key] !== 'undefined';
        }
    }, {
        key: 'addImage',
        value: function addImage(key, src, useCORS) {
            var _this4 = this;

            if (true) {
                this.logger.log('Added image ' + key.substring(0, 256));
            }

            var imageLoadHandler = function imageLoadHandler(supportsDataImages) {
                return new Promise(function (resolve, reject) {
                    var img = new Image();
                    img.onload = function () {
                        return resolve(img);
                    };
                    //ios safari 10.3 taints canvas with data urls unless crossOrigin is set to anonymous
                    if (!supportsDataImages || useCORS) {
                        img.crossOrigin = 'anonymous';
                    }

                    img.onerror = reject;
                    img.src = src;
                    if (img.complete === true) {
                        // Inline XML images may fail to parse, throwing an Error later on
                        setTimeout(function () {
                            resolve(img);
                        }, 500);
                    }
                    if (_this4.options.imageTimeout) {
                        var timeout = _this4.options.imageTimeout;
                        setTimeout(function () {
                            return reject( true ? 'Timed out (' + timeout + 'ms) fetching ' + src.substring(0, 256) : '');
                        }, timeout);
                    }
                });
            };

            this.cache[key] = isInlineBase64Image(src) && !isSVG(src) ? // $FlowFixMe
            _Feature2.default.SUPPORT_BASE64_DRAWING(src).then(imageLoadHandler) : imageLoadHandler(true);
            return key;
        }
    }, {
        key: 'isSameOrigin',
        value: function isSameOrigin(url) {
            return this.getOrigin(url) === this.origin;
        }
    }, {
        key: 'getOrigin',
        value: function getOrigin(url) {
            var link = this._link || (this._link = this._window.document.createElement('a'));
            link.href = url;
            link.href = link.href; // IE9, LOL! - http://jsfiddle.net/niklasvh/2e48b/
            return link.protocol + link.hostname + link.port;
        }
    }, {
        key: 'ready',
        value: function ready() {
            var _this5 = this;

            var keys = Object.keys(this.cache);
            var values = keys.map(function (str) {
                return _this5.cache[str].catch(function (e) {
                    if (true) {
                        _this5.logger.log('Unable to load image', e);
                    }
                    return null;
                });
            });
            return Promise.all(values).then(function (images) {
                if (true) {
                    _this5.logger.log('Finished loading ' + images.length + ' images', images);
                }
                return new ResourceStore(keys, images);
            });
        }
    }]);

    return ResourceLoader;
}();

exports.default = ResourceLoader;

var ResourceStore = exports.ResourceStore = function () {
    function ResourceStore(keys, resources) {
        _classCallCheck(this, ResourceStore);

        this._keys = keys;
        this._resources = resources;
    }

    _createClass(ResourceStore, [{
        key: 'get',
        value: function get(key) {
            var index = this._keys.indexOf(key);
            return index === -1 ? null : this._resources[index];
        }
    }]);

    return ResourceStore;
}();

var INLINE_SVG = /^data:image\/svg\+xml/i;
var INLINE_BASE64 = /^data:image\/.*;base64,/i;
var INLINE_IMG = /^data:image\/.*/i;

var isInlineImage = function isInlineImage(src) {
    return INLINE_IMG.test(src);
};
var isInlineBase64Image = function isInlineBase64Image(src) {
    return INLINE_BASE64.test(src);
};
var isBlobImage = function isBlobImage(src) {
    return src.substr(0, 4) === 'blob';
};

var isSVG = function isSVG(src) {
    return src.substr(-3).toLowerCase() === 'svg' || INLINE_SVG.test(src);
};

var _loadImage = function _loadImage(src, timeout) {
    return new Promise(function (resolve, reject) {
        var img = new Image();
        img.onload = function () {
            return resolve(img);
        };
        img.onerror = reject;
        img.src = src;
        if (img.complete === true) {
            // Inline XML images may fail to parse, throwing an Error later on
            setTimeout(function () {
                resolve(img);
            }, 500);
        }
        if (timeout) {
            setTimeout(function () {
                return reject( true ? 'Timed out (' + timeout + 'ms) loading image' : '');
            }, timeout);
        }
    });
};

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.parseContent = exports.resolvePseudoContent = exports.popCounters = exports.parseCounterReset = exports.TOKEN_TYPE = exports.PSEUDO_CONTENT_ITEM_TYPE = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _ListItem = __webpack_require__(14);

var _listStyle = __webpack_require__(8);

var PSEUDO_CONTENT_ITEM_TYPE = exports.PSEUDO_CONTENT_ITEM_TYPE = {
    TEXT: 0,
    IMAGE: 1
};

var TOKEN_TYPE = exports.TOKEN_TYPE = {
    STRING: 0,
    ATTRIBUTE: 1,
    URL: 2,
    COUNTER: 3,
    COUNTERS: 4,
    OPENQUOTE: 5,
    CLOSEQUOTE: 6
};

var parseCounterReset = exports.parseCounterReset = function parseCounterReset(style, data) {
    if (!style || !style.counterReset || style.counterReset === 'none') {
        return [];
    }

    var counterNames = [];
    var counterResets = style.counterReset.split(/\s*,\s*/);
    var lenCounterResets = counterResets.length;

    for (var i = 0; i < lenCounterResets; i++) {
        var _counterResets$i$spli = counterResets[i].split(/\s+/),
            _counterResets$i$spli2 = _slicedToArray(_counterResets$i$spli, 2),
            counterName = _counterResets$i$spli2[0],
            initialValue = _counterResets$i$spli2[1];

        counterNames.push(counterName);
        var counter = data.counters[counterName];
        if (!counter) {
            counter = data.counters[counterName] = [];
        }
        counter.push(parseInt(initialValue || 0, 10));
    }

    return counterNames;
};

var popCounters = exports.popCounters = function popCounters(counterNames, data) {
    var lenCounters = counterNames.length;
    for (var i = 0; i < lenCounters; i++) {
        data.counters[counterNames[i]].pop();
    }
};

var resolvePseudoContent = exports.resolvePseudoContent = function resolvePseudoContent(node, style, data) {
    if (!style || !style.content || style.content === 'none' || style.content === '-moz-alt-content' || style.display === 'none') {
        return null;
    }

    var tokens = parseContent(style.content);

    var len = tokens.length;
    var contentItems = [];
    var s = '';

    // increment the counter (if there is a "counter-increment" declaration)
    var counterIncrement = style.counterIncrement;
    if (counterIncrement && counterIncrement !== 'none') {
        var _counterIncrement$spl = counterIncrement.split(/\s+/),
            _counterIncrement$spl2 = _slicedToArray(_counterIncrement$spl, 2),
            counterName = _counterIncrement$spl2[0],
            incrementValue = _counterIncrement$spl2[1];

        var counter = data.counters[counterName];
        if (counter) {
            counter[counter.length - 1] += incrementValue === undefined ? 1 : parseInt(incrementValue, 10);
        }
    }

    // build the content string
    for (var i = 0; i < len; i++) {
        var token = tokens[i];
        switch (token.type) {
            case TOKEN_TYPE.STRING:
                s += token.value || '';
                break;

            case TOKEN_TYPE.ATTRIBUTE:
                if (node instanceof HTMLElement && token.value) {
                    s += node.getAttribute(token.value) || '';
                }
                break;

            case TOKEN_TYPE.COUNTER:
                var _counter = data.counters[token.name || ''];
                if (_counter) {
                    s += formatCounterValue([_counter[_counter.length - 1]], '', token.format);
                }
                break;

            case TOKEN_TYPE.COUNTERS:
                var _counters = data.counters[token.name || ''];
                if (_counters) {
                    s += formatCounterValue(_counters, token.glue, token.format);
                }
                break;

            case TOKEN_TYPE.OPENQUOTE:
                s += getQuote(style, true, data.quoteDepth);
                data.quoteDepth++;
                break;

            case TOKEN_TYPE.CLOSEQUOTE:
                data.quoteDepth--;
                s += getQuote(style, false, data.quoteDepth);
                break;

            case TOKEN_TYPE.URL:
                if (s) {
                    contentItems.push({ type: PSEUDO_CONTENT_ITEM_TYPE.TEXT, value: s });
                    s = '';
                }
                contentItems.push({ type: PSEUDO_CONTENT_ITEM_TYPE.IMAGE, value: token.value || '' });
                break;
        }
    }

    if (s) {
        contentItems.push({ type: PSEUDO_CONTENT_ITEM_TYPE.TEXT, value: s });
    }

    return contentItems;
};

var parseContent = exports.parseContent = function parseContent(content, cache) {
    if (cache && cache[content]) {
        return cache[content];
    }

    var tokens = [];
    var len = content.length;

    var isString = false;
    var isEscaped = false;
    var isFunction = false;
    var str = '';
    var functionName = '';
    var args = [];

    for (var i = 0; i < len; i++) {
        var c = content.charAt(i);

        switch (c) {
            case "'":
            case '"':
                if (isEscaped) {
                    str += c;
                } else {
                    isString = !isString;
                    if (!isFunction && !isString) {
                        tokens.push({ type: TOKEN_TYPE.STRING, value: str });
                        str = '';
                    }
                }
                break;

            case '\\':
                if (isEscaped) {
                    str += c;
                    isEscaped = false;
                } else {
                    isEscaped = true;
                }
                break;

            case '(':
                if (isString) {
                    str += c;
                } else {
                    isFunction = true;
                    functionName = str;
                    str = '';
                    args = [];
                }
                break;

            case ')':
                if (isString) {
                    str += c;
                } else if (isFunction) {
                    if (str) {
                        args.push(str);
                    }

                    switch (functionName) {
                        case 'attr':
                            if (args.length > 0) {
                                tokens.push({ type: TOKEN_TYPE.ATTRIBUTE, value: args[0] });
                            }
                            break;

                        case 'counter':
                            if (args.length > 0) {
                                var counter = {
                                    type: TOKEN_TYPE.COUNTER,
                                    name: args[0]
                                };
                                if (args.length > 1) {
                                    counter.format = args[1];
                                }
                                tokens.push(counter);
                            }
                            break;

                        case 'counters':
                            if (args.length > 0) {
                                var _counters2 = {
                                    type: TOKEN_TYPE.COUNTERS,
                                    name: args[0]
                                };
                                if (args.length > 1) {
                                    _counters2.glue = args[1];
                                }
                                if (args.length > 2) {
                                    _counters2.format = args[2];
                                }
                                tokens.push(_counters2);
                            }
                            break;

                        case 'url':
                            if (args.length > 0) {
                                tokens.push({ type: TOKEN_TYPE.URL, value: args[0] });
                            }
                            break;
                    }

                    isFunction = false;
                    str = '';
                }
                break;

            case ',':
                if (isString) {
                    str += c;
                } else if (isFunction) {
                    args.push(str);
                    str = '';
                }
                break;

            case ' ':
            case '\t':
                if (isString) {
                    str += c;
                } else if (str) {
                    addOtherToken(tokens, str);
                    str = '';
                }
                break;

            default:
                str += c;
        }

        if (c !== '\\') {
            isEscaped = false;
        }
    }

    if (str) {
        addOtherToken(tokens, str);
    }

    if (cache) {
        cache[content] = tokens;
    }

    return tokens;
};

var addOtherToken = function addOtherToken(tokens, identifier) {
    switch (identifier) {
        case 'open-quote':
            tokens.push({ type: TOKEN_TYPE.OPENQUOTE });
            break;
        case 'close-quote':
            tokens.push({ type: TOKEN_TYPE.CLOSEQUOTE });
            break;
    }
};

var getQuote = function getQuote(style, isOpening, quoteDepth) {
    var quotes = style.quotes ? style.quotes.split(/\s+/) : ["'\"'", "'\"'"];
    var idx = quoteDepth * 2;
    if (idx >= quotes.length) {
        idx = quotes.length - 2;
    }
    if (!isOpening) {
        ++idx;
    }
    return quotes[idx].replace(/^["']|["']$/g, '');
};

var formatCounterValue = function formatCounterValue(counter, glue, format) {
    var len = counter.length;
    var result = '';

    for (var i = 0; i < len; i++) {
        if (i > 0) {
            result += glue || '';
        }
        result += (0, _ListItem.createCounterText)(counter[i], (0, _listStyle.parseListStyleType)(format || 'decimal'), false);
    }

    return result;
};

/***/ })
/******/ ]);
});