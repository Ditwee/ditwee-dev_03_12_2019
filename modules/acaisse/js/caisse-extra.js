/**********************
 * GEstionnaire de l'onglet extra/option du module de caisse 
 */
$(function(){
	
	$('.tab-button').on('click tap',function(e){
        $tab=$('#'+$(this).data('tab-id'));
        if($(this).hasClass('unselectable')){
        	e.preventDefault();
            $('.alert_message', $(this)).fadeIn();
            $('.alert_message', $(this)).delay(1000).fadeOut();
            return;
        }
        else{
			$tab=$('#'+$(this).data('tab-id'));
			$('li',$tab.parent()).removeClass('open');
			$tab.addClass('open');
			$('.tab-button',$(this).parent()).removeClass('open');
			$(this).addClass('open');
        }
	});
});
