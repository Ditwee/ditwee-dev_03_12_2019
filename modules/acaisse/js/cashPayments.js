function _cashDeleteChar() { // efface un caractère de la somme donné par le client quand il paye en liquide
	
    if ($('#cashReceived').html().length > 0) {
        $('#cashReceived').html($('#cashReceived').html().substring(0, $('#cashReceived').html().length - 1));
    }

    if ($('#cashReceived').html() == '' || $('#cashReceived').html() == 'NaN') {
		$('#cashReceived').html('-');
		_refreshCashOverdue(0);
		//console.log('okazertyuiop');
    } else {
		var val = '0' + $('#cashReceived').html();
        var finishWithPoint = '.' == val.substr(val.length - 1, 1);
        val = parseInt(100 * parseFloat(val)) / 100;
        $('#cashReceived').html(val + (finishWithPoint ? '.' : ''));
        _refreshCashOverdue(val);
    }

}

function _cashAddChar(c) {  // ajoute un caractère de la somme donné par le client quand il paye en liquide   	
    if (c == ',') {
        c = '.';
    }

	if ($('#cashReceived').html() == '-') {
        $('#cashReceived').html('');
    }
	
    if ($.inArray(c, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']) != -1)
    {
        if (c == '.' && $('#cashReceived').html().length == 0)
        {
            c = '0.';
        }
        $('#cashReceived').html($('#cashReceived').html() + c);
    }
    else if (c == $('#calc-keyboard .calc-key-c').eq(0).html())
    {
        _cashDeleteChar();
    }

    if ($('#cashReceived').html() == '' || $('#cashReceived').html() == 'NaN' || $('#cashReceived').html() == '-') {
        $('#cashReceived').html('-');
		_refreshCashOverdue(0);
    }
    else
    {
        var val = /*'0' + */$('#cashReceived').html();
        var finishWithPoint = '.' == val.substr(val.length - 1, 1);
		var hasPoint = val.indexOf('.') != -1;
        
        if(hasPoint) //il y a un point
        {
        	var parts = val.split('.');
        	var entier = parts[0];
        	var solde = parts[1].substr(0,2);
        	val = entier+'.'+solde;
        	
        	//console.log(val,parts,entier,solde);
        	
        	$('#cashReceived').html(val);
        }
        else
        {
        	$('#cashReceived').html(val);
        }
        _refreshCashOverdue(val);
        
        return;
    }
}

function _refreshCashOverdue(received) {
	lcd_write_line(' ',0);
	lcd_write_line(' ',1);
	var diff = received - ACAISSE.cart_tot;
	if (diff == 0) //le compte est bon
	{
		var module_obj = {action:'payment_exact'};
		
		$('#cashMessage').html('Appoint correct').removeClass('more less').addClass('ok');
		$('#registerPayment').removeClass('disabled');
		lcd_write_line(' ',2,undefinedValue,undefinedValue,module_obj);
	}
	else if (diff > 0) //trop percu, il faut rendre
	{
		 $('#cashMessage').html(formatPrice(diff) + ' € à rendre').removeClass('ok less').addClass('more');
		$('#registerPayment').removeClass('disabled');
		
		var module_obj = {action:'payment_overdue',received:formatPrice(received),diff:formatPrice(diff)};
		ACAISSE.rendu = diff;
		ACAISSE.recu = received;
		
		lcd_write_line('Reçu '+formatPrice(received) + '€',1,undefinedValue,undefinedValue,module_obj);
		lcd_write_line('Rendu '+formatPrice(diff) + '€',2);
	}
	else //pas assez percu, il manque ...
	{
		$('#cashMessage').html('Il manque ' + formatPrice(diff*-1) + ' €').removeClass('more ok').addClass('less');
		$('#registerPayment').addClass('disabled');
		
		var module_obj = {action:'payment_lack',received:formatPrice(received),diff:formatPrice(diff)};
		
		lcd_write_line(' ',2,undefinedValue,undefinedValue,module_obj);
	}
}
