function Caisse()
{	
	
	//propriété publique
	this.version_name = 'V2.4.1';
	var _id_pointshop = parseInt(localStorage.getItem('id_pointshop'));
	var _pointshop = acaisse_pointshops.pointshops[_id_pointshop];
	
	//si pas de pointshop alors on initialise en prenant le premier
	if(_pointshop == undefined)
	{
		for(var i in acaisse_pointshops.pointshops)
		{
			_id_pointshop = i;
			_pointshop = acaisse_pointshops.pointshops[_id_pointshop];
			break;
		}
		if(_pointshop == undefined)
		{
			alert('Erreur OG90012 : Aucune configuration de caisse en cache');
		}
	}
	
	//propriétées privées
	var DEBUG_LEVEL = 0;
	var hooks = {	
		/*
		 * lorsque le moyen, ou le montant de paiement saisie est modifié
		 */
		paiement_change : [
			function(params){
				if(DEBUG_LEVEL > 5)
				{
					console.log(['debug hook paiement_change',params]);
				}
			}],
			 
			
		
		/*
		 * lorsqu'un paiement simple est validé
		 */
		paiement_validation : [],
		/**
		 * lorsqu'un paiement multiple est validé  
		 */
		paiement_mulitple_validation : [],
		
		/**
		 * lorsque le contenu du ticket/panier change OU son total
		 */
		cart_change : [
			function(params){
				console.log(['debug hook cart_change',params]);
			}
		],
		
	};
	
	//méthodes publiques
	
	// Enregistrement d'une nouvelle fonction sur un hook
	this.registerHook = function(name,func)
	{
		if(!hooks[name]) {
			hooks[name]=[];
		}
	  hooks[name].push(func);
	};	
	
	// Méthode permettant l'éxécution d'un hook avec passage des arguments
	this.executeHook = function(name,params) {
		if(hooks[name]) {
			return hooks[name].map(func=>func(params)).join('');
		};
	};
	
	//méthode privée
	var _toto = function() {
		
	};
	
	//Initialisation de la caisse
	var _init = function(){
		_initPointshop();
	};
	
	this.reInit = function() {
		_init();
	};
	
	var _initPointshop = function() {
		
		var options = _pointshop.datas;
		
		if(options.activate_preorder_function == 1) //data-value=2
		{
			$('#preorder_option_block div.item[data-value="2"], div.isCommandeBloquee.button').show();
		}
		else
		{
			$('#preorder_option_block div.item[data-value="2"], div.isCommandeBloquee.button').hide();
		}
		
		if(options.activate_quotation_function == 1) //data-value=3
		{
			$('#preorder_option_block div.item[data-value="3"], div.isDevis.button').show();
		}
		else
		{
			$('#preorder_option_block div.item[data-value="3"], div.isDevis.button').hide();
		}
		
		
		var id_employee = _getIdEmployee();
		
		//init du boutton de gestion des stocks
		var ids_employee_autorized_for_stock = options.limit_stock_managment_access.split(';');
		if(ids_employee_autorized_for_stock == '' || ids_employee_autorized_for_stock.indexOf('' + id_employee) > -1)
		{
			$('button#stockBtn').show();
		}
		else
		{
			$('button#stockBtn').hide();			
		}
		//init du boutton de visualisation des commandes d'un client
		var ids_employee_autorized_for_order = options.limit_order_access.split(';');

		if(options.limit_order_access == '' || ids_employee_autorized_for_order.indexOf('' + id_employee) > -1)
		{
			$('#userEditContainer li[data-page-id="pageUser3"]').show();
		}
		else
		{
			$('#userEditContainer li[data-page-id="pageUser3"]').hide();			
		}		
	};
	
	//Permissions	
	var _getIdEmployee = function() {
		return parseInt('0' + $('#showEmployee').data('id_employee'));
	};
	
	
	/***************************************************************
	 * chargement des lib de caisse
	 ***************************************************************/
	this.payment = new CaissePayment(this);
	this.loader = new CaisseLoader(this);	
	this.customerScreen = new USBCustomerScreen(this);
	this.return = new CaisseReturn(this);
	this.delivery = new CaisseDelivery(this);
	this.invoice = new CaisseInvoice(this);
	this.carrier = new CaisseCarrier(this);	
	this.preorder = new CaissePreorder(this);	
	this.employee = new CaisseEmployee(this);
	this.customer = new CaisseCustomer(this);
	this.printer = new CaissePrinter(this);
	this.ticket = new Ticket(this);
	
	// init
	_init(this);
}