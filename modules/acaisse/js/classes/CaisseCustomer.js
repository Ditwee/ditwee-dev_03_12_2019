function CaisseCustomer(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	
	/*
	 * AFFICHAGE DES COMMANDES PRESTASHOP
	 */	
	$('body').on('click tap','#pageUser3 .openOrder',function() {
	
		var id_order = $(this).data('id');
		PRESTATILL_CAISSE.customer._loadOrderTpl(id_order);
    });
    
    /*
     * AFFICHAGE DE LA FACTURE
     */
    $('body').on('click tap','#pageUser3 .seeInvoice',function(e) {
    	var id_order = $(this).data('id');
    	$.ajax({
	        data: {
	            'action': 'LoadOrderInvoice',
	            'id_order': id_order
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	var response = response.response.link;

	        	//$("#popupNumeric").html(response);
	        	window.location.href = response;
	        	e.stopPropagation();
	        	e.preventDefault();
	            
	        } else {
	        	alert('Erreur k02011 : impossible de charger la facture. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	        }
	    }).fail(function () {
	        console.log('Erreur k02010 : impossible de charger la facture. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	    });
    });
    
    /*
     * AFFICHAGE DU TICKET DANS L'INTERFACE CLIENT
     */
    $('body').on('click tap','#pageUser3 .seeTicket',function(e) {
    	var id_ticket = $(this).data('id');
    	
    	$.ajax({
	        data: {
	            'action': 'LoadOrderTicket',
	            'id_ticket': id_ticket
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	var response = response.response.html;
	        	
	        	if($('.switcher').hasClass('show-product-list') == false)
	        	{
	        		showEmptyPopup($("#productList"),response);
	        	}
	        	else
	        	{
	        		showEmptyPopup($("#ticketView"),response);
	        	}
	        	
		    	/* Since 2.5.1 */
		    	$('#id_ticket_to_send').val(id_ticket);
	            
	        } else {
	        	alert('Erreur k02011 : impossible de charger le ticket. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	        }
	    }).fail(function () {
	        console.log('Erreur k02010 : impossible de charger le ticket. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	    });
    });
    
    /*
     * ADRESSE MAIL "POUBELLE" (si le client ne souhaite pas la donner) en se basant sur l'adresse mail du Client de passage
     */
    $('body').on('click tap','#placehldr',function(e) {
    	
    	var fake_mail = ACAISSE.customer.email;
    	var fake_base_mail = fake_mail.split('@');
    	var fake_name = $('#customer-edit-lastname').val();
    	var fake_firstname = $('#customer-edit-firstname').val();
    	$('#customer-edit-email').val(fake_firstname+'_'+fake_name+'@'+fake_base_mail[1]);
    	
    });
    
    /*
     * LORSQUE L'ON AJOUTE / MODIFIER UN PAIEMENT
     */
    $('body').on('click tap','.modify-payment',function(e) {
    	e.preventDefault();
    	
    	var id_order = $(this).data('id');
    	var pay_method = $('#payment_method').val();
    	var pay_info = $('#payment_id').val();
    	var pay_amount = $('#payment_amount').val();
    	
    	$.ajax({
	        data: {
	            'action': 'modifyPayment',
	            'id_order': id_order,
	            'method': pay_method,
	            'id_transaction': pay_info,
	            'amount': pay_amount
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	    	//console.log(response);
	    	
	        if (response.success !== false) {
	        	// On rafraichit la vue des paiements
	        	$.ajax({
			        data: {
			            'action': 'LoadOrderInfos',
			            'id_order': response.response.id_order,
			            'filter' : 0,
			        },
			        type: 'post',
			        dataType: 'json',
			    }).done(function (response) {
			        if (response.success !== false) {
			        	var response = response.response.html;
			        	$('#orderDetail .oDTable').html(response);
			        	
			        	$('#orderDetail .oDTable ul.notLoaded').removeClass('notLoaded').html('');
			        	
			        	// On rafraichit la liste des commandes
			        	loadOrders(0);
					}
				});
	            
	        } else {
	        	alert(response.error);
	        }
	    }).fail(function () {
	        alert('Erreur L03042 : Le montant du règlement n\'est pas correct.');
	    });
    });
    
    /*
     * LORSQUE L'ON AJOUTE / MODIFIER UN STATUT / ETAT DE VENTE
     */
    $('body').on('click tap','.modify-state',function(e) {
    	e.preventDefault();
    	
    	var id_order = $(this).data('id');
    	var id_order_state = $('#order_state').val();
    	
    	PRESTATILL_CAISSE.loader.start();
    	
    	$.ajax({
	        data: {
	            'action': 'modifyState',
	            'id_order': id_order,
	            'id_order_state': id_order_state,
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	    	//console.log(response);
	    	
	        if (response.success !== false) {
	        	// On rafraichit la vue des paiements
	        	$.ajax({
			        data: {
			            'action': 'LoadOrderInfos',
			            'id_order': response.response.id_order,
			            'filter' : 2,
			        },
			        type: 'post',
			        dataType: 'json',
			    }).done(function (response) {
			        if (response.success !== false) {
			        	var response = response.response.html;
			        	$('#orderDetail .oDTable').html(response);
			        	
			        	$('#orderDetail .oDTable ul.notLoaded').removeClass('notLoaded').html('');
			        	
			        	// On rafraichit la liste des commandes
			        	loadOrders(0);
			        	PRESTATILL_CAISSE.loader.end();
					}
				});
	            
	        } else {
	        	alert(response.error);
	        }
	    }).fail(function () {
	        alert('Erreur L03043 : l\'état de vente n\'est pas correct.');
	    });
    });
    
    /*
     * RETROUVER LES COMMANDES D'UN CLIENT AVEC LES FILTRES
     */
    
    $('#userEditContainer').on('click tap', '#order_filter .button', function(){
	
		var datas  = {};
		
		if($(this).hasClass('validate'))
		{
			$('[id^="filter-"]').each(function () {
		        datas[$(this).attr('id').replace('filter-','')] = $(this).val();
		    });
		}
		PRESTATILL_CAISSE.loader.start();
		loadOrders(0, datas);
	});
	
	$('#emptyPopup ').on('keyup', 'input.return', function(){
		//console.log([$(this).val(),$(this).parents('tr').find('.qty span').html()]);
		var qty = $(this).parents('tr').find('.qty span').html();
		var qty_refund = $(this).parents('tr').find('.qty_refund span').html();
		//console.log([qty,qty_refund,$(this).val()]);
		if($(this).parents('td').hasClass('qty'))
		{
			if($(this).val() > parseInt(qty-qty_refund))
			{
				$(this).val(parseInt(qty-qty_refund));
			}	
		}
		
		//console.log([$(this).parents('tr').find('.qty input').val(),$(this).parents('tr').find('.unit_amount input').val()])
		$(this).parents('tr').find('.total_cost input.return').val(parseFloat($(this).parents('tr').find('.qty input').val()*$(this).parents('tr').find('.unit_amount input').val()).toFixed(2));
		
	});
	
	
	$('#emptyPopup').on('click tap', '.popupActions', function(){
		PRESTATILL_CAISSE.loader.start();
		var params = {};
		$('#orderDetail tr').each(function(){
			if($(this).find('.actions input').prop('checked') == true)
			{
				params[$(this).data('id')] = {
					'qty':$(this).find('.qty input').val(),
					'price_ttc':$(this).find('.unit_amount input').val(),
				};
			}
		});
		
		caisse_ref.return.switchToReturnMode();
		
		var datas = {
	        'action' : 'SaveReturnPopupDatas',
	        'id_customer' : null,
	        'id_employee' : ACAISSE.customer.id,
	        'id_product' : null,
	        'id_product_attribute' : null,
	        'id_return' : PRESTATILL_CAISSE.return.ticket == null ? 0:PRESTATILL_CAISSE.return.ticket.id,
	        'params' : params,
 	   };
	   
		$.ajax({
	        data: datas,
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	_gotoTicketView();
	        	
	        	var request = response.request;
	        	var response = response.response;	  
	        	PRESTATILL_CAISSE.return.ticket = response.return_ticket;
	        	
	        	PRESTATILL_CAISSE.return.redrawTicket();
	        	
	        	$('.closePopup').trigger('click');
	        	PRESTATILL_CAISSE.loader.end();
	        		        		        					
	        } else {
	        	alert('Erreur o_ret0123 : impossible sauvegarder les informations');
	        }
	    }).fail(function () {
	        alert('Erreur o_ret0124 : impossible de communiquer avec le serveur');
	    });
		console.log(params);
	});
	
	$('#emptyPopup').on('click tap', '.actions input', function(){
		var is_checked = 0;
		if($(this).prop('checked')==true)
		{
			$(this).parents('tr').find('.return').show();
		} 
		else 
		{
			$(this).parents('tr').find('.return').hide();
		}
		
		$('#emptyPopup .actions input').each(function(){
			if($(this).prop('checked') == true)
			{
				is_checked++;
			}
		});
		
		if(is_checked>0)
		{
			$('#emptyPopup .popupActions').show();
		}
		else
		{
			$('#emptyPopup .popupActions').hide();
		}
	});
	
	this._loadOrderTpl = function (id_order = false, ean13 = false) 
	{
	    	$.ajax({
		        data: {
		            'action': 'LoadOrderTpl',
		            'id_order': id_order,
		            'ean13': ean13,
		        },
		        type: 'post',
		        dataType: 'json',
		    }).done(function (response) {
		        if (response.success !== false) {
		        	var response = response.response.html;
		        	
		        	
		        	if($('.switcher').hasClass('show-product-list') == false)
		        	{
		        		showEmptyPopup($("#productList"),response);
		        	}
		        	else
		        	{
		        		showEmptyPopup($("#ticketView"),response);
		        	}
		        	
		        	$('#orders_option').val('1');
			
					$('#orders_actions').uiSelector({
							default_value : 1,
					        onChange:function(filter_value){	
							
							$.ajax({
						        data: {
						            'action': 'LoadOrderInfos',
						            'id_order': id_order,
						            'filter' : filter_value,
						            'ean13' : ean13,
						        },
						        type: 'post',
						        dataType: 'json',
						    }).done(function (response) {
						        if (response.success !== false) {
						        	var response = response.response.html;
						        	$('#orderDetail .oDTable').html(response);
						        	
						        	$('#orderDetail .oDTable ul.notLoaded').removeClass('notLoaded').html('');
						        	$('#emptyPopup .popupActions').hide();
						        	
								}
							});
							
					    }
				    });
				    
				    $('#orderDetail ul').removeClass('notLoaded').html('');
				    $('#emptyPopup .popupActions').html('<i class="fa fa-share-square-o fa-fw"></i>');
				    
		            
		        } else {
		        	alert('Erreur k02011 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
		        }
		    }).fail(function () {
		        console.log('Erreur k02010 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
		    });
	}
	
	
}