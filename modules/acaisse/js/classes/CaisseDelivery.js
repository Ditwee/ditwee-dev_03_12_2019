function CaisseDelivery(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	
	this.STORE_DELIVERY = 0;
	this.HOME_DELIVERY = 1;
	
	
	var drawAddressDetails = function(address, with_name) {
		/*
		*
		active / address1 / address2 / alias / city / company / country / date_add / date_upd / deleted / dni
		firstname / id_address / id_country / id_customer / id_manufacturer / id_state / id_supplier
		/ id_warehouse / lastname / other / phone / phone_mobile / postcode / state / state_iso / vat_number
						 */
		var html = '';
		if(with_name == undefined) { with_name=true; }
		
		if(with_name && (address.lastname != '' || address.firstname != ''))
		{
			if(html != '') { html += '<br />'; }
			html += (address.lastname==''?'':address.lastname + ' ') + (address.firstname==''?'':address.firstname);
		}
		
		if(address.address1 != '')
		{
			if(html != '') { html += '<br />'; }
			html += address.address1;
		}
		
		
		if(address.address2 != '')
		{
			if(html != '') { html += '<br />'; }
			html += address.address2;
		}
		if(''+address.city != '' || ''+address.postcode != '')
		{
			if(html != '') { html += '<br />'; }
			html += (address.postcode==''?'':address.postcode + ' ') + (address.city==''?'':address.city);
		}
		
		console.log(['drawAddressDetails',address,html]);
		
		return html;
	};
	
	
	this.initPopupDatas = function() {
		//console.log('showPopupGlobalCartReduction('+reduc+','+type_reduc+','+inputValue+')');
		
		//$('#popupDelivery .selected').removeClass('selected');
		
		
		
		
		//chargement des transporteurs actifs
		$.ajax({
	        type: 'post',
	        data: {
	        	'action': 'loadCarriers',
				'id_pointshop': ACAISSE.id_pointshop
	        },
	        dataType: 'json',
			
		}).done(function(response){
			if(response.success === true)
			{
				var html = '';
				$.each(response.response.carriers,function(i,carrier) {
					html += '<li data-id="'+carrier.id_carrier+'">';
					html += '   <h3>'+carrier.name+'</h3>';
					html += '</li>';
				});				
				$('#id_carrier').html(html);
				
				$('#id_carrier li').click(function(){					
					$('#id_carrier li').removeClass('selected');
					$('input[name="id_carrier"]').val($(this).addClass("selected").data('id'));
				});
				
				
				
			    console.log('********-+-********');
			    console.log(ACAISSE.ticket);
				
				$('#carrier_option').val(ACAISSE.ticket.with_carrier);
				$('#invoice_option').val(ACAISSE.ticket.with_invoice);
				
				//carrier
			    $('#carrier_option_block').uiSelector({
			      input : $('#carrier_option'),
			      onChange : function(new_value) {
			      	if(new_value == PRESTATILL_CAISSE.carrier.WITHOUT) //sans transporteur
			      	{         		
			      		$('#id_carrier').addClass('hide');
			      		$('input[name="id_carrier"]').val(0);
			      	}
			      	
			      	if(new_value == PRESTATILL_CAISSE.carrier.WITH) //avec transporteur
			      	{
			      		$('#id_carrier').removeClass('hide').find('li').eq(0).click();
			      	}
			      	
			      }
			    });
			    
			    
			    
			    //il faut peupler avec les adresses du customer actuellement sélectionné dans la caisse
		
				var html = '';		
				$.each(ACAISSE.customer.addresses,function(i,address){
					html += '<li data-id="'+address.id_address+'"><h3>'+address.alias+'</h3>';
						html += '<legend>';
						html += drawAddressDetails(address);						
						html += '</legend>';
					html += '</li>';
				});
		
				$('#id_delivery_address_home').html(html);
				$('#id_invoice_address').html(html);
				
				//chargement des adresses de point de vente prestatill
				var html = '';
				$.each(acaisse_pointshops.pointshops,function(i,pointshop){
					html += '<li data-id="'+i+'"><h3>'+pointshop.name+'</h3>';
						html += '<legend>';
						console.log(pointshop);
						html += drawAddressDetails(pointshop.datas.address, false);					
						html += '</legend>';
					html += '</li>';		
				});
				$('#id_delivery_address_store').html(html);
			    
			    
			    //rechargemenet des infos de detaxe depuis le ticket
			    $('#detax_option_block div').removeClass('selected');
			    if(ACAISSE.ticket.is_detaxed)
			    {
			    	$('#detax_option_block div[data-value="1"]').click();//addClass('selected');
			    }
			    else
			    {
			    	$('#detax_option_block div[data-value="0"]').click();//addClass('selected');
			    }
			    
				
			}
			else
			{
				alert('Error O_deliv_1001');
			}
		}).fail(function(){
			alert('Error O_deliv_1000');
		});
		
		
	};
		
	
}