function CaisseLoader(caisse_ref)
{
	var caisse = caisse_ref;
	this.is_loading = false;
		
	/**
	 * Pour afficher le voile de chargement global 
	 */
	this.start = function() {
		$('#global-loader').css('display','table');
	};
	
	/**
	 * Pour masquer le message de chargement 
	 */
	this.end = function() {
		$('#global-loader').css('display','none');
	};
	
	
	/**
	 * Afficher les actions (top et bottom) 
	 */
	this.showActions = function() {
		this.showTopActions();
		this.showBottomActions();
	};
	
	/**
	 *  Masquer le voile suppérieur
	 */
	this.hideActions = function() {
		this.hideTopActions();
		this.hideBottomActions();
	};	
	
	/**
	 *  Afficher le voile suppérieur
	 */	
	this.hideTopActions = function() {
		$('#top-loader').css('display','table');
	};
	
	/**
	 *  Masquer le voile suppérieur
	 */
	this.showTopActions = function() {
		$('#top-loader').css('display','none');
	};
	
	/**
	 * Afficher un voile en bas
	 */
	this.hideBottomActions = function() {
		$('#bottom-loader').css('display','table');
	};
	
	/**
	 * Pour masquer le voile du bas 
	 */
	this.showBottomActions = function() {
		$('#bottom-loader').css('display','none');
	};
	
}