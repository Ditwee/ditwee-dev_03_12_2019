function CaissePayment(caisse_ref)
{
	
	var currency = '€';
	
	var caisse = caisse_ref;
	var DEBUG_LEVEL = 0;
	
	
	var number_formatter = new Intl.NumberFormat('fr-FR',{minimumFractionDigits:2});
	
	var current_payment_module_name = null;
	
	var tot_a_payer = 0; 
	var tot_deja_paye = 0;
	var extra_datas = {}; //données complémentaire qui seront transmise au serveur
	
	var diff_str = parseFloat(tot_a_payer-payment_amount-tot_deja_paye);
	diff_str = ""+diff_str;
	
	var dif_str_res = diff_str.replace(" ","");
	var diff = 0+dif_str_res;
	
	var formatted_diff = 0;
	var formatted_payment_amount = 0;
	var payment_amount = 0;
	var repaid = 0; //rendu
	var formatted_repaid = 0;
	
	var real_payment_amount = 0;
	var formatted_real_payment_amount = 0;
	
	
	this.getPaymentAmount = function() { return payment_amount; };
	
	
	/**
	 * Liste des modules de paiement 
	 */
	this.modules = [];
	
	var getCurrentPaymentModule = function() {
		
		for(i in caisse.payment.modules)
		{
			var module = caisse.payment.modules[i];
			if(module.constructor.name == current_payment_module_name)
			{
				return module;
			}
		}		
		return {};
	};
	
	this.getPaymentModuleByName = function(payment_module_name) {
		
		
		for(i in caisse.payment.modules)
		{
			var module = caisse.payment.modules[i];
			if(module.constructor.name == payment_module_name)
			{
				return module;
			}
		}		
		return false;
	};
	
	this.getCurrentPaymentModule = function() {
		return getCurrentPaymentModule();
	};
	
	this.dispatchScanBarCode = function(e) {
		var module = this.getCurrentPaymentModule();
		if(module.captureScanner != undefined)
		{
			if(DEBUG_LEVEL > 5)
			{
				console.log('module '+module.constructor.name+' capture scan event');
			}
			module.captureScanner(e);
		}		
	};
	
	
	//Handlers
	this.choosePaymentHandler = function(e) {		
		if($(this).hasClass('selected'))
		{
			caisse.payment.changePaymentMode(null);
			$('.prestatill_payment_button').removeClass('selected');
			$('.payment_actions').hide();
			//$('#paymentView .help').hide();.show();
		}
		else
		{
			caisse.payment.changePaymentMode($(this).data('module-name'));	
			$('.prestatill_payment_button').removeClass('selected');
			$('.payment_actions').show();
			$(this).addClass('selected');
			//$('#paymentView .help').hide();.hide();
			
			var module = getCurrentPaymentModule();
			
			//console.log(module);
			
			if(module.init)
			{
				module.init();
			}			
		}		
	};
	
	this.getTotal = function() {
		
		var tot_to_pay = $('#totalCost').html();
		var tot_to_pay_res = tot_to_pay.replace(" ","");
		
		return parseFloat('0'+tot_to_pay_res);
		return ACAISSE.cart_tot; //@TODO à réécrire une fois les totaux stocké dans PRESTATILL_CAISSE
	};
	
	this.initOnOpenPaymentInterface = function() {			
		PRESTATILL_CAISSE.loader.hideActions();
		this.changePaymentMode(null);
		this.changePaymentAmount();
		$('.prestatill_payment_button.selected').removeClass('selected');
		$('.payment_actions').hide();
		$('#launchRegisterPayment').show(); // A vider
	    	
		//rechargement des paiments déjà effectué lorsque l'on recharge une commande qu'il faut solder	
		
		total_deja_paye = 0;
					
		$('#details-paiements').html('');
		for(var i = 0 ; i < ACAISSE.payment_parts.length ; i++)
		{
			var html_new_tr = _renderPaymentPartLine(ACAISSE.payment_parts[i]);
			total_deja_paye += parseFloat(ACAISSE.payment_parts[i]['payment_amount']);			
		    $('#details-paiements').append(html_new_tr);
        }
        total_deja_paye = " "+total_deja_paye;
        var tot_deja_paye_res = total_deja_paye.replace(" ","");	
		
		this.changePaymentAmount(0.0);
	    $('.payment_amount').html('0.00');
	    $('#real_payment_amount').html('0.00'+currency); //a prirori c'est plus utilisé
	    $('#keepToHaveToPay').html(formatPrice(this.getTotal()-tot_deja_paye_res)+currency);
	    $('.solde_restant').html(formatPrice(this.getTotal() - tot_deja_paye_res));
	    //$('#paymentView tr.solde-restant').removeClass('success').addClass('warning');
	    $('.display_back').addClass('not_enough').removeClass('good');
		
		if(ACAISSE.payment_parts.length > 0)
		{
			//on est entrain de payé un relicat d'une commande			
	    	$('#paymentView .cancel').addClass('disabled');
	    	if(ACAISSE.ticket.invoice_total_products_wt > 0)
	    	{
				$('#keepToHaveToPay').html(formatPrice(ACAISSE.ticket.order_total_paid-tot_deja_paye_res)+currency);
	    		$('.solde_restant').html(formatPrice(ACAISSE.ticket.order_total_paid - tot_deja_paye_res));
	    		//$('#totalCost').html(formatPrice(ACAISSE.ticket.order_total_paid+'<small>TTC</small>')+currency);	    	
	    	}
		}
		else
		{
			//on va effectuer le tout premier paiement			
	    	$('#paymentView .cancel').removeClass('disabled');
		}
		//totalCost
		_refreshButtonStates(parseFloat('0'+$('#keepToHaveToPay').html()));
						
	};
	
	
	//init des handlers de payement (commun)
	$('#paymentView').on('click tap', '.prestatill_payment_button', this.choosePaymentHandler);
	
	
	// Méthode publique lié au(x) paiement(s)
	this.changePaymentMode = function(new_payment_module_name) {
		if(DEBUG_LEVEL>2)
		{
			console.log('Calling CaissePayment::changePaymentMode(new_payment_module_name)');
		}		
		
		//on masque l'ensemble des modules
		$('#paymentView .col-interface .prestatill_payment_interface,#paymentView .col-interface .help').hide();
		
		current_payment_module_name = new_payment_module_name;
		//console.log(current_payment_module_name);		
		var module = getCurrentPaymentModule();	
		//si on a déselectionné le moyen de paiement
		if(current_payment_module_name === null)
		{			
			$('#paymentView .col-interface help').show();
				
			this.changePaymentAmount(0,module.with_repaid);
		}
		else ////on charge l'interface de saisi du module
		{
			$('#paymentView .col-interface .prestatill_payment_interface[data-module-name="'+current_payment_module_name+'"]').show();			
		}
		
		caisse.executeHook('paiement_change',{
			payment_module_name : new_payment_module_name,
			payment_amount : 0, //@TODO récupéré le montant
			total_amount : 0, //@TODO récupéré le montant
			formated_total_amount : 0, //@TODO récupéré le montant
			formated_payment_amount : 0, //@TODO récupéré le montant
		});
		
	};
	
	//Méthode publique a appelé en cours de saisie du montant reçu
	this.changePaymentAmount = function(amount, with_repaid){
		
		//avec gestion d'un rendu monnaie?
		if(with_repaid == undefined)
		{
			with_repaid = false;
		}
		
		if(DEBUG_LEVEL>2)
		{
			console.log('Calling CaissePayment::changePaymentAmount(amount:='+amount+') ');
		}
		
		/**
		 * calcule entre la difference du total à payer et du montant du paiement
		 */		
		payment_amount = amount;
		tot_a_payer = parseFloat(this.getTotal()); 
		
		if($('#keepToHaveToPay').html() == '')
		{
			$('#keepToHaveToPay').html(tot_a_payer);
		}
		
		var tot_dp = $('#keepToHaveToPay').html();
		var tot_dp_res = tot_dp.replace(" ","");
		
		tot_deja_paye = tot_a_payer-parseFloat('0'+tot_dp_res);
		diff = tot_a_payer-payment_amount-tot_deja_paye;
		extra_datas = extra_datas;
		
		if(with_repaid === true && diff < 0)
		{
			repaid = -diff;
			diff = 0;			 
			real_payment_amount = payment_amount - repaid;
		}
		else
		{
			repaid = 0;
			real_payment_amount = Math.min(payment_amount,tot_a_payer); //@TODO attention ici j'empeche le trop perçu... c'est juste?
		}
		
		formatted_repaid = number_formatter.format(repaid);		
		formatted_diff = number_formatter.format(diff<0?-diff:diff);
		formatted_payment_amount = number_formatter.format(amount);
		formatted_real_payment_amount = number_formatter.format(real_payment_amount);
		
		this.refreshAmountDisplay();			
		
		caisse.executeHook('paiement_change',{
			payment_module_name : current_payment_module_name,
			payment_amount : payment_amount,
			formated_payment_amount : formatted_payment_amount,
			total_amount : formatted_payment_amount, //@TODO récupéré le montant
			formated_total_amount : 0, //@TODO récupéré le montant
		});
	};	
	
	
	this.refreshAmountDisplay = function() {
		$('.solde_restant').html(diff==0 && repaid == 0?'':repaid>0?formatted_repaid:formatted_diff);
		$('.payment_amount').html(formatted_payment_amount);		
		$('.display_back').removeClass('too_much not_enough good repaid').addClass(diff==0 && repaid == 0?'good':(diff>0?'not_enough':(repaid>0?'repaid too_much':'too_much')));
		$('#real_payment_amount').html(formatted_real_payment_amount + currency);	
				
	};
	
	var _renderPaymentPartLine = function(payment_part) {
				
          var html_new_tr = '<tr>';
            html_new_tr += '<td>'+payment_part['label'].toLowerCase()+'</td>';
            html_new_tr += '<td>'+formatPrice(payment_part['payment_amount'])+'€</td>';
            //html_new_tr += '<td><i class="fa fa-trash"></i></td>';
          html_new_tr += '</tr>';
          
          return html_new_tr;
	};
	
	this.registerRefund = function() {
		
		var module = getCurrentPaymentModule();
		var extra_datas = {};
		var datas = {
			'action' : 'RegisterRefund',					//nom de l'action côrté serveur
			'id_ticket' : PRESTATILL_CAISSE.return.ticket.id,				//id du ticket de caisse
			'id_pointshop' : ACAISSE.id_pointshop,			//id du point de vente prestatill
			'id_service' : ACAISSE.id_service,			//id du point de vente prestatill
															//a DEJA était converti en panier Pretashop à ce stade)
			'id_customer' : ACAISSE.customer.id,			//id du client
			'ajaxRequestID' : ++ACAISSE.ajaxCount,			//id de transaction ajax
			'payment_module_name' : current_payment_module_name,	//nom du module de paiement utilisé
			'payment_part_amount' : formatPrice(real_payment_amount),			//montant payé
			'label' : current_payment_module_name,			//label du module de paiement, à vérifier SI utile
			'extra_datas' : extra_datas,
		};
		
		//PRESTATILL_CAISSE.loader.start();
		
		PRESTATILL_CAISSE.return.registerRefund(datas);
		
	};	
	
	
	
	//Méthode publique lié à l'enregistrement d'un paiement
	this.registerPayment = function() {
		if(DEBUG_LEVEL>2)
		{
			console.log('Calling CaissePayment::registerPayment(paymentModuleName, amount) @TODO');
		}		
		
		var module = getCurrentPaymentModule();
		var extra_datas = {};
		if(module.getExtraDatas != undefined)
		{
			extra_datas = module.getExtraDatas();
		}
		
		if ($('#popup_global_title').val() != '')
		{
			extra_datas['ticket_title'] = $('#popup_global_title').val();
		}
		
		if ($('#popup_global_textarea').val() != '')
		{
			extra_datas['ticket_textarea'] = $('#popup_global_textarea').val();
		}
		
		var datas = {
			'action' : 'RegisterPayment',					//nom de l'action côrté serveur
			'id_ticket' : ACAISSE.ticket.id,				//id du ticket de caisse
			'id_pointshop' : ACAISSE.id_pointshop,			//id du point de vente prestatill
			'id_service' : ACAISSE.id_service,			//id du point de vente prestatill
			'ps_id_cart' : ACAISSE.ticket.ps_id_cart,		//id du panier Prestashop (cela suppose que le ticket				
															//a DEJA était converti en panier Pretashop à ce stade)
			'id_customer' : ACAISSE.customer.id,			//id du client
			'id_group' : ACAISSE.customer.id_default_group,				//group par defaut du client
			'id_force_group' : ACAISSE.id_force_group,		//groupe forcé
			'ajaxRequestID' : ++ACAISSE.ajaxCount,			//id de transaction ajax
			'id_order_state' : 10,		//id_order_state //@TODO à dynamiser en fonction du module !!!! ou alors à gérer côté SERVEUR
			//'cash_received' : 'xxx',		//DEPRECATED
			'payment_module_name' : current_payment_module_name,	//nom du module de paiement utilisé
			'payment_part_amount' : formatPrice(real_payment_amount),			//montant payé
			'label' : current_payment_module_name,			//label du module de paiement, à vérifier SI utile
			'extra_datas' : extra_datas,
		};
		
		PRESTATILL_CAISSE.loader.start();
		
		$.ajax({
			type: 'post',
			data: datas,
			dataType: 'json',
		}).done(function (response) {
			
			
			if(response.success)
			{
			
				var request = response.request;
				var response = response.response;				
				
				/////////////////////////////////////////////////////////////////////
				////////////////////////////////////////////////////////////////////
				//on ajoute juste le dernier venant d'etre insere
		          var payment_part = response.payment_part;
		          var html_new_tr = _renderPaymentPartLine(payment_part);
		          
		          ACAISSE.payment_parts.push({
		          	payment_module_name:payment_part['module_name'],
		          	payment_part_amount:payment_part['payment_amount'],
		          	label:response.payment_part['label']
		          });
		          
		          $('#details-paiements').append(html_new_tr);	          
		          
		          $('#keepToHaveToPay').html(formatPrice(response.restant_a_payer));
		          
		          //on désactive le boutton d'annulation, maintenant c'est trop tard
		          $('#paymentView .cancel').addClass('disabled');
		          
		          _refreshButtonStates(response.restant_a_payer);
		          
		          //on click sur le module de paiement qui a était utilisé
		          $('.prestatill_payment_button.selected').click();	                 
		          
		          ACAISSE.lastCA = response.lastCa;
		          _refreshLastCa();	          
			  	  PRESTATILL_CAISSE.loader.end();			
			}
			else
			{
				PRESTATILL_CAISSE.loader.end();
				alert('Error 0103245 : ' + response.errorMsg);
			}		
			
			return;
			/*
			   //Pour démarre automatiquement un nouveau ticket
				_startNewPreorder();
				
			*/
		}).fail(function (response) {
			alert('Erreur 010122 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
			PRESTATILL_CAISSE.loader.end();
		});
		
		caisse.executeHook('paiement_validation',{
			//@TODO
		});
	};
	
	var _refreshButtonStates = function(restant_a_payer) {
		  //si le paiement est maintenant complet
          if(restant_a_payer == 0 && $('#details-paiements > tr').length > 0)
          {
            $('#paymentView .partial-validation').addClass('disabled');
            $('#paymentView .validation').removeClass('disabled');
            
            //console.log('tout est payé cool');
            $('#paymentView tr.solde-restant').removeClass('warning').addClass('success');
            $('#launchRegisterPayment').hide();
            //$('#paymentView .help').hide();.hide();
            
            // Ici on choisi si on souhaite un ticket de caisse ou si on l'envoi par mail
            //alert('ici ?');
            getTicketCaisse();
            
          }
          else if($('#details-paiements > tr').length == 0)
          {
          	   $('#paymentView .partial-validation').addClass('disabled');
          	   $('#paymentView .validation').addClass('disabled');
          }
          else  //sinon
          {
            $('#paymentView .validation').addClass('disabled');
            $('#paymentView .partial-validation').removeClass('disabled');
            $('#launchRegisterPayment').show();
          }	   
		          
	};
	
	$('#emptyPopup').on('click tap', '#ticketInfos .devisButtons .print_to_ticket', function (e){
		e.preventDefault();
		
		printQueue();
		$('#multi-payments-popup').remove();
	    $('.strict-overlay').remove();
	    
	    $('#emptyPopup .closePopup').click();
	    //on demande un passage au ticket suivant
	    _startNewPreorder();
	});
	/*
	 * Si on ferme la popup à l'aide de la croix sans choisir d'option
	 */
	$('#emptyPopup').on('click tap', '.closePopup', function (){
		
		if($('#emptyPopup').find('#ticketInfos').length > 0)
		{
			ACAISSE_print_queue.shift();
    		_startNewPreorder();
		}
    });
	
	$('#emptyPopup').on('click tap', '#ticketInfos .devisButtons .send', function (){
    	$('#ticketInfos .overflow, #ticketInfos .send_form').slideDown();
    	$('.devisButtons').slideUp();
    });
    
    $('#emptyPopup').on('click tap', '#ticketInfos .devisButtons .no_ticket', function (){
    	$('#emptyPopup .closePopup').click();
    	ACAISSE_print_queue.shift();
    	_startNewPreorder();
    });
    
    $('#emptyPopup').on('click tap', '#ticketInfos #abort_send', function (e){
    	e.preventDefault();
    	$('#ticketInfos .overflow, #ticketInfos .send_form').slideUp();
    	$('.devisButtons').slideDown();
    });
    
    $('#emptyPopup').on('click tap', '#ticketInfos #confirm_send', function (e){
    	e.preventDefault();
    	if(validate($('#send_form_email').val()) == true)
    	{
    		$('#emptyPopup .closePopup').click();
    		$(this).prop('disabled','disabled');
    		printQueue(true);
    		PRESTATILL_CAISSE.loader.start();
    	}
    	else
    	{
    		alert('Veuillez renseigner une adresse email correcte svp.');
    	}
    	
    });
	
}