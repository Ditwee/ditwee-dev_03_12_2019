function CaissePreorder(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	
	
	this.ONLY_MY_PREORDERS = 0;
	this.OTHERS_PREORDERS = 1;
	this.ALL_PREORDERS = 2;
	
	this.openTab = function() {
	      
		$('#changeOrderButton i').removeClass('fa-angle-up').addClass('fa-angle-down');
		ACAISSE.scanbarcodeMode = 'preOrder';
		
		$('#preOrderListContainer .keyboard span.display').html('');
		
		//ici on charge la valeur de l'onglet a préselectionner
		$('#preorder_option').val('1');
              
		$('#preorder_option_block').uiSelector({
			default_value : 1,
	        onChange:function(filter_value){
	        	
	          
			  $('#preOrderListContainer .left ul li').remove().addClass('notLoaded');
			  $('#preOrderListContainer .left ul').html('<li><i class="fa fa-spinner fa-spin"></i>Chargement en cours...</li>');
	        	
	          //console.log('filter_value : '+filter_value);
                $.ajax({
                    data: {
                        'action': 'RetreiveDayTicketList',
                        'id_pointshop': ACAISSE.id_pointshop,
                        'filter' : filter_value,
                        'id_employee' : PRESTATILL_CAISSE.employee.getCurrentIdEmployee(),
                    },
                    type: 'post',
                    dataType: 'json',
                }).done(function (response) {
                    if (response.success != false) {
                    	var response = response.response;
                        $('#preOrderListContainer .left ul li').remove();
                        
                        var html = '';
                        //console.log(response);
                        
                        for (var i = 0; i < response.tickets.length; i++)
                        {
                            if (response.tickets[i].id_a_caisse_ticket != ACAISSE.ticket.id)
                            {
                                	var emp_initiale = response.tickets[i].emp_firstname;
                                	var emp_complete_name = response.tickets[i].emp_firstname+' '+response.tickets[i].emp_lastname;
                                	
                                	var ticket_date = response.tickets[i].day;
									var ticket_date_dm = ticket_date.substring(0,10);
									ticket_date = ticket_date_dm.split('-');
                                	
                                	// Afficher la date
                                	// Afficher "Commande" ou "Devis" au lieu de ticket si is_bloqued ou is_devis
                                	if(response.tickets[i].is_bloqued == 1)
                                	{
                                		html +='<li '+(response.tickets[i].isNotTotalyPaid!='1'?'class="notTotalyPayed"':'')+' data-id="' + response.tickets[i].id_a_caisse_ticket + '" data-num="' + response.tickets[i].id_a_caisse_ticket + '">';
                                	}
                                	else if(response.tickets[i].is_devis == 1)
                                	{
                                		html +='<li '+(response.tickets[i].isNotTotalyPaid!='1'?'class="notTotalyPayed"':'')+' data-id="' + response.tickets[i].id_a_caisse_ticket + '" data-num="' + response.tickets[i].num_devis + '">';
                                	}
                                	else
                                	{
                                		html +='<li '+(response.tickets[i].isNotTotalyPaid!='1'?'class="notTotalyPayed"':'')+' data-id="' + response.tickets[i].id_a_caisse_ticket + '" data-num="' + response.tickets[i].ticket_num + '">';
                                	}
                                	
                                	html +='<div class="emp"><i class="avatar">'+ emp_initiale.substr(0,1) +'</i>'+emp_complete_name+'</div>';
                                	html +='<span class="picto"><i class="fa fa-ticket"></i></span>';
                                	//html +='<span class="num_ticket">'+ response.tickets[i].id_a_caisse_ticket +'</span>';
                                	html +='<div><span class="date">' + ticket_date[2] + '/' + ticket_date[1] + '/' + ticket_date[0] + '</span></div>';
                                	html +='<div class="first_last_name">' + response.tickets[i].firstname + ' ' + response.tickets[i].lastname + '</div>';
                                	html +='<div>';
                                	if(response.tickets[i].is_bloqued == 1)
                                	{
                                		html +='<span class="preorder_label">Commande</span> n°' + response.tickets[i].id_a_caisse_ticket;
                                	}
                                	else if(response.tickets[i].is_devis == 1)
                                	{
                                		html +='<span class="predevis_label">Devis</span> n°' + response.tickets[i].num_devis;
                                	}
                                	else
                                	{
                                		html +='<span class="preticket_label">Ticket</span> n°' + response.tickets[i].ticket_num;
                                	}
                                	html +='</div>';                        	
                                	html +='<div><span class="pastille">' + parseFloat(response.tickets[i].ptinc-response.tickets[i].global_discount).toFixed(2) + '€</span></div>';
                                	html +='</li>';
                            }
                        }
                        
                        $('#preOrderListContainer .left ul').html(html);
                        $('#preOrderListContainer .left ul').removeClass('notLoaded');
                        
                        PRESTATILL_CAISSE.executeHook('actionPrestatillGeneratePreOrderListLine');
                    }
                    
                }).fail(function () {
                    alert('Erreur 000784 : impossible d\'obtenir la liste des précommandes du serveur. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
                });
	          
	          
	          
	          
	        }
	      });
		
	};
	
	this.closeTab = function() {		
                ACAISSE.scanbarcodeMode = 'product';
                $('#changeOrderButton i').removeClass('fa-angle-down').addClass('fa-angle-up');
	};
	
	this.printTicketPreorder = function (piece_type, big_format=false, send_email=false) {
	  	$.ajax({
	        type: 'post',
	        data: {
	          'action': 'GetTicketBloqued',
	          'id_ticket': ACAISSE.ticket.id,
	          'big_format': big_format,
	          'send_email': send_email,
	        },
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success != false) {
	        	
	          printTicket(response.response.ticket, acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.ticket_nbr_copies,piece_type);
	          if(send_email == false)
	          {	
	          	printQueue();
	          }
	          else
	          {
	          	PRESTATILL_CAISSE.loader.start();
	          	
	          	// On envoi le mail si les paramètres d'envoi de mail sont activés
	          	$.ajax({
			        type: 'post',
			        data: {
			          'action': 'SendEstimateEmail',
			          'id_ticket': ACAISSE.ticket.id,
			          'big_format': big_format,
			          'send_email': $('#send_form_email').val(),
			        },
			        dataType: 'json',
			    }).done(function (response) {
			        if (response.success != false) {
			        	
			          // On confirme que le mail a bien été envoyé
			          //closeAllPanel();
			          PRESTATILL_CAISSE.loader.end();
			          //alert('Mail envoyé avec succès');
			          sendNotif('greenFont','<i class="fa fa-check"></i> Mail envoyé avec succès');
			          
			        } else {
			          alert('Error : l_send0012 / Une erreur s\'est produite lors de l\'envoi du mail.');
			          //console.error(response.error);
			        }
			    }).fail(function () { 
			        alert('Error : l_send0011 / Une erreur s\'est produite lors de l\'envoi du mail.');
			    });
	          	
	          }   
	          
	          //on demande un passage au ticket suivant
	          _startNewPreorder();
	        } else {
	          alert('Error : o_print0012 / Erreur lors de la génération du ticket de caisse');
	          console.error(response.error);
	        }
	    }).fail(function () { 
	        alert('Error : o_print0011 / Erreur lors de la génération du ticket de caisse');
	    });
	};
	
	this.transformTicket = function(transform_to, big_format=false, send_email=false) {
		
		if (ACAISSE.ticket.lines.length == 0)
		{
			alert('Veuillez d\'abord sélectionner au moins un produit');
		}
		else
		{
			var extra_datas = {};
			
			if(transform_to == 'is_devis')
			{
				extra_datas = {
					'num_devis': '',
					'ref_devis': $('#popup_global_title').val(),
					'commentaire_devis': $('#popup_global_textarea').val(), 
				};
			}
		
			$.ajax({
		        type: 'post',
		        dataType: 'json',
		        data: {
		            'action': 'TransformTicketTo',
		            'id_ticket': ACAISSE.ticket.id,
		            'transform_to': transform_to,
		            'extra_datas': extra_datas,
		            
		        }
		    }).done(function (response) {
		        if (!response.success) {
		            //alert('Erreur 000552'+response.errorMsg);
		            return;
		        }
		        $('.ticketBox').slideUp();
				
				// Send NOTIF
				//_startNewPreorder();
				if(transform_to == 'is_bloqued')
				{
					caisse.preorder.printTicketPreorder('preorder');
				}
				else if(transform_to == 'is_devis')
				{
					caisse.preorder.printTicketPreorder('estimate', big_format, send_email);
				}
				else
				{
					_startNewPreorder();
				}
		        
		    }).fail(function () {
		        alert('Impossible de bloquer la commande.');
		    });
		}	
	};
	
	/*
     * Filtrer les preorder par nom
     */
    $('body').on('keyup','#filterPreOrder',function(e) {
    	var fliterPreOrder = $('#filterPreOrder input').val();
    	console.log(fliterPreOrder);
    	$('#preList li').each(function(){
    		console.log($(this).find('.first_last_name').html().toLowerCase().indexOf(fliterPreOrder.toLowerCase()));
    		if($(this).find('.first_last_name').html().toLowerCase().indexOf(fliterPreOrder.toLowerCase()) == -1)
    		{
    			$(this).hide();
    		}
    		
    	});   	
    });
	

	$('.isCommandeBloquee').on('click tap', function () {
			
		if (acaisse_profilesNoCheckinIDs.indexOf('' + ACAISSE.customer.id) != -1)
		{
			alert('Veuillez d\'abord identifier ou créer un client.');
		} 
		else 
		{
			 caisse.preorder.transformTicket('is_bloqued');
		}
       
    });
	
	$('.isDevis').on('click tap', function () {
		
		var html = '';
		
		html +='<div id="devisInfos" class="popupBox">';
			html +='<div class="overflow" style="display:none;"></div>';
			
	        html +='<div class="title">';
	       		html +='<h3>Créer un Devis</h3>';
	        html +='</div>';
	        html +='<div class="devisInfosComp">';
				html +='<form id="popup_epgloal__form">';
	            	html +='<fieldset>';
	            		html +='<span class="alert alert-info" >Vous pouvez personnaliser les informations uniquement pour ce ticket</span>';
	            		html +='<div>';
		            		html +='<div class="label">Libellé de la vente</div>';
		            		html +='<input type="text" value="" id="popup_global_title" ><div class="label" >Informations complémentaires</div>';
	            			html +='<textarea id="popup_global_textarea"></textarea>';
	            			html +='<span class="alert alert-info" >Exemple : C\'est un devis (ref, dates, clauses, etc...)</span>';
	            			html +='<div class="send_form" style="display:none;"><div class="label" >Adresse email d\'envoi :</div>';
								html +='<input type="text" value="'+ACAISSE.customer.email+'" id="send_form_email" >';
								html +='<div>';
									html +='<div id="abort_send" class="button">Annuler</div>';
									html +='<div id="confirm_send" class="button">Envoyer</div>';
								html +='</div>';
							html +='</div>';
	            		html +='</div>';
	            	html +='</fieldset>'; 
	            html +='</form>';
	            html +='<div class="devisButtons">'; 
	            //@TODO : Bouton "imprimer" / "Envoyer un mail"
	            	html +='<div class="button print_to_ticket"><i class="fa fa-print"></i><div class="label">Imprimer</div><div class="legend">(format ticket)</div></div>';
	            	html +='<div class="button print_big"><i class="fa fa-file"></i><div class="label">Imprimer</div><div class="legend">(format A4)</div></div>';
	            	html +='<div class="button send"><i class="fa fa-envelope"></i><div class="label">Envoyer</div><div class="legend">par e-mail</div></div>';
	            html +='</div>';
	        html +='</div>';
		html +='</div>';
		
		showEmptyPopup($("#ticketView"),html);
    });
    
    $('.isPending').on('click tap', function () {
        caisse.preorder.transformTicket('is_pending');
        
    });
    
    $('#emptyPopup').on('click tap', '#devisInfos .devisButtons .print_to_ticket', function (){
    	$('.closePopup').trigger('click');
    	caisse.preorder.transformTicket('is_devis');
    }); 
    
    $('#emptyPopup').on('click tap', '#devisInfos .devisButtons .print_big', function (){
    	$('.closePopup').trigger('click');
    	caisse.preorder.transformTicket('is_devis',true);
    });
	
	$('#emptyPopup').on('click tap', '#devisInfos .devisButtons .send', function (){
    	//$('.closePopup').trigger('click');
    	$('#devisInfos .overflow, #devisInfos .send_form').slideDown();
    	$('.devisButtons').slideUp();
    	
    	//
    });
    
    $('#emptyPopup').on('click tap', '#devisInfos #abort_send', function (e){
    	e.preventDefault();
    	$('#devisInfos .overflow, #devisInfos .send_form').slideUp();
    	$('.devisButtons').slideDown();
    });
    
    $('#emptyPopup').on('click tap', '#devisInfos #confirm_send', function (e){
    	e.preventDefault();
    	if(validate($('#send_form_email').val()) == true)
    	{
    		$('#emptyPopup .closePopup').click();
    		$(this).prop('disabled','disabled');
    		caisse.preorder.transformTicket('is_devis',true,true);
    	}
    	else
    	{
    		alert('Veuillez renseigner une adresse email correcte svp.');
    	}
    	
    });

}