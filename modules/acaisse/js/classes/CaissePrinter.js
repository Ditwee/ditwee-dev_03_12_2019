function CaissePrinter(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 0;
	var caisse = caisse_ref;
	
	this.printEmptyTicket = function() {
		caisse_ref.printer.clearPrintQueue();
		printTicket('', 1, ''); //duplicata == la class CSS qui va aller s'ajouter sur #ticketPrint
		printQueue();
	};
	
	this.clearPrintQueue = function() {
		ACAISSE_print_queue = [];
	};
	
	
	
	
	//mise en place des écouteurs
	var _initHandler = function() {
		if(LOG_LEVEL>=10)
		{
			console.log(['initHandler',$('#footer'),$('#print_empty_ticket')]);
		}
		$('#print_empty_ticket').on('click tap', function(e){
			
			if(LOG_LEVEL>=5)
			{
				console.log('start printing empty ticket');
			}
			PRESTATILL_CAISSE.printer.printEmptyTicket();
		});
	};
	
	_initHandler();
	
}