function CaisseReturn(caisse_ref)
{
	var LOG_LEVEL = DEBUG_LEVEL = 5;
	var caisse = caisse_ref;
	this.mode_return_is_active = false;
	
	this.ticket = null;//new CaisseReTurnTicket();

	var number_formatter = new Intl.NumberFormat('fr-FR',{minimumFractionDigits:2});
	
	var current_payment_module_name = null;
	var tot_a_rembourser = 0; 
	var tot_deja_rembourser = 0;
	var extra_datas = {}; //données complémentaire qui seront transmise au serveur
	
	this.switchToReturnMode = function() {
		if(LOG_LEVEL > 9)
		{
			console.log('"RETURN MODE" activated');
		}
		$('.mode-buy').hide();
		
		$('#help-return').show();
		
		$('.mode-return').show();
		$('#container').addClass('is-mode-return').removeClass('is-mode-buy');
		closeAllPanel();
		this.mode_return_is_active = true;
	};
	
	this.switchToBuyMode = function() {
		if(LOG_LEVEL > 9)
		{
			console.log('"BUY MODE" activated');
		}
		$('.mode-return').hide();	
		$('.mode-buy').show();	
		$('#container').addClass('is-mode-buy').removeClass('is-mode-return');
		closeAllPanel();
		hidePopup();
		this.mode_return_is_active = false;
	};
	
	this.modules = [];
	
	var getCurrentPaymentModule = function() {
		
		for(i in caisse.payment.modules)
		{
			var module = caisse.payment.modules[i];
			if(module.constructor.name == current_payment_module_name)
			{
				return module;
			}
		}		
		return {};
	};
	
	this.getPaymentModuleByName = function(payment_module_name) {
		
		
		for(i in caisse.payment.modules)
		{
			var module = caisse.payment.modules[i];
			if(module.constructor.name == payment_module_name)
			{
				return module;
			}
		}		
		return false;
	};
	
	this.getCurrentPaymentModule = function() {
		return getCurrentPaymentModule();
	};
	
	this.dispatchScanBarCode = function(e) {
		var module = this.getCurrentPaymentModule();
		if(module.captureScanner != undefined)
		{
			if(DEBUG_LEVEL > 5)
			{
				console.log('module '+module.constructor.name+' capture scan event');
			}
			module.captureScanner(e);
		}		
	};
	
	/**
	 * Gestion de l'ouverture de la popup de retour
	 * et son remplissage avec les dernière commande 
	 */
	this.openReturnPopup = function(id_product, id_product_attribute, quantity, id_order) {
		if(LOG_LEVEL > 7)
		{
			console.log('openReturnPopup() with args :');
			console.log({id_product:id_product, id_product_attribute:id_product_attribute, quantity:quantity, id_order:id_order});
		}
		//requete au serveur pour récupérer la liste des commandes
		
		$list = $('#popupReturn .orders_list tbody').html('<tr><td colspan="7"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i>'+'Chargement'+'</td></tr>');
		
		$('#popupReturn').data({
			'idProduct' : id_product,
			'idProductAttribute' : id_product_attribute,
		});
		
		var p = _loadProductFromId(id_product);
		var a = null;
		if (id_product_attribute && p.has_attributes) {
			a = p.attributes[id_product_attribute];
		}
		
		//chargement du nom produit
		$('#popupReturn .header-inner h1 span').html(p.product_name);
		//chargement du nom de la déclinaisons (si déclinaison)
		$('#popupReturn .header-inner h1 small').html(a===null?'':' / ' + a.attribute_name);
		
		
		$.ajax({
	        data: {
	            'action' : 'GetOrdersContainingProduct',
	            'id_customer' : ACAISSE.customer.id,
	            'id_product' : id_product,
	            'id_product_attribute' : id_product_attribute,
	            'nb_day' : 180, //A dynamiser dans le pointshop je pense
	            'ids_pointshop' : '',
	            'quantity' : quantity,
	            'id_return' : PRESTATILL_CAISSE.return.ticket === null ? 0 : PRESTATILL_CAISSE.return.ticket.id,
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	
	        	
	        	var request = response.request;
	        	var response = response.response;	  
	        	  
	        	
	        	
				var tbody = '';
	        	    	
				//là on injecte les balises script
				if (response.list_orders.length>0) {
					
					$list = $('#popupReturn .orders_list tbody');
					$list.html(response.list_orders);
					
					var restant_a_refund = request.quantity;
					var restant_a_retourner = request.quantity;
					//console.log(response);
					
					$.each(response.list_orders, function(i,line){
						
						if(LOG_LEVEL > 6)
						{
	        				console.log(line);
	        			}
						var product_quantity = line.product_quantity;
						var product_quantity_refunded = line.product_quantity_refunded;
						var product_quantity_return = line.product_quantity_return;
						var max_refund = product_quantity - product_quantity_refunded;
						var max_retournable = product_quantity - product_quantity_return;
						var value_refund = Math.min(max_refund, restant_a_retourner);
						var value_retournable = Math.min(max_retournable, restant_a_retourner);	
						
						//chargement ici des trucs actuellement reservé
						
						restant_a_refund -= value_refund;
						restant_a_retourner -= value_retournable;
						
						if(value_refund <= 0)
						{
							value_refund = '';
						}
						
						if(value_retournable <= 0)
						{
							value_retournable = '';
						}
						
						//var 					
						
						tbody += '<tr class="return-sub-detail-line">';
						tbody += '	<td>'+line.reference+'</td>';
						tbody += '	<td width="80px">'+line.order_date+'</td>';
						tbody += '	<td>'+product_quantity+'</td>';
						tbody += '	<td>'+product_quantity_refunded+'</td>';
						//tbody += '	<td>'+product_quantity_return+'</td>';
						tbody += '	<td><span class="ui-spinner-group refund" data-min="0" data-max="'+max_refund+'"><input class="ui-spinner" name="quantity['+line.id_order+'__'+line.id_order_detail+']" value="'+value_refund+'" readonly /></span></td>';
						//tbody += '	<td><span class="ui-spinner-group return" data-min="0" data-max="'+max_retournable+'"><input class="ui-spinner" name="return_quantity['+line.id_order+'__'+line.id_order_detail+']" value="" /></span></td>';
						//tbody += '	<td><span class="ui-spinner-group reinject" data-min="0" data-max="0"><input class="ui-spinner" name="reinjected['+line.id_order+'__'+line.id_order_detail+']" value=""  /></span></td>';
						tbody += '	<td>';
						tbody += '     <span class="ttc">' + parseFloat(line.unit_price_tax_incl).toFixed(2) + '€ TTC</span>';
						tbody += '     <span class="ht">' + parseFloat(line.unit_price_tax_excl).toFixed(2) + '€ HT</span>';
						tbody += '  </td>';
						tbody += '	<td><input'+(value_refund==''?' disabled="true" style="opacity:0.5"':'')+' class="ui-spinner-group price" name="price['+line.id_order+'__'+line.id_order_detail+']" value="' + line.unit_price_tax_incl + '"></td>';
						tbody += '</tr>';					
					});					
				}
				else
				{
					tbody += '<tr class="return-sub-detail-line">';
					tbody += '	<td colspan="7">Aucune commande trouvée</td>';
					tbody += '</tr>';					
				}
				
				$list.html(tbody);
				
				$('.ui-spinner-group.refund').uiSpinner({
					onChange:function(plugin,$element,value) {
						//faut trouver le spinner de la même ligne lié
						$line = $element.parents('.return-sub-detail-line');
						if(value == '' || value == 0)
						{
							$price = $('.ui-spinner-group.price',$line).prop('disabled',true).css('opacity','0.5');
						}
						else
						{							
							$price = $('.ui-spinner-group.price',$line).prop('disabled',false).css('opacity','1');
						}
						_calcul_total_popup();
					}
				});
    			$('#popupReturn').show();  
    			$('#popupReturn').find('.body').scrollTop( 0 );
    			$('#popupReturnForm .ui-spinner-group.refund .ui-spinner').prop('readonly', true);
    			
    			_calcul_total_popup(); 
				
	        } else {
	        	alert('Erreur o_ret0121 : impossible de rafraichir la liste des commandes');
	        }
	    }).fail(function () {
	        alert('Erreur o_ret0120 : impossible d\'obtenir la liste des commandes contenant ces ventes du serveur');
	    });
		
		 	
	};
	
	this.registerRefund = function(params) {
		
		var extra_datas = {};
		//console.log(params);
		
		PRESTATILL_CAISSE.loader.start();
		
		$.ajax({
			type: 'post',
			data: params,
			dataType: 'json',
		}).done(function (response) {
			
			
			if(response.success)
			{
				var html = response.html;
				var request = response.request;
				var response = response.response;	
				
				PRESTATILL_CAISSE.return.ticket = null;
	        	//PRESTATILL_CAISSE.return.redrawTicket();
	        	$('#ticketView .return-lines tbody').html('');
					          
			  	PRESTATILL_CAISSE.loader.end();	
			  	caisse_ref.return.switchToBuyMode();
			  	
			  	$.each(html,function(i,refund){
					
					$('#printTickets .left .ticket.toPrint .kdoprint').hide();
					$('#printTickets .left .ticket.toPrint .kdotitle').show();
					$('#printTicket').html(refund);
					//window.print();
					printTicket(refund,1);
				});			
				
				printQueue();	
			  	
			  	if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 1) {
					$('#openRootPageCaisse').click();
					_gotoProductList();
				} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 2
					&& acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.navigation_by_category == 1) {
					$('#openRootCategorie').click(); 
				} else if (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.return_to_catalog_after == 0) {
					//$('.switcher').click(); 
					_gotoTicketView();
				}
			  			
			}
			else
			{
				PRESTATILL_CAISSE.loader.end();
				alert('Error 0103245 : ' + response.errorMsg);
			}		
			
			return;
			
		}).fail(function (response) {
			alert('Erreur 010122 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
			PRESTATILL_CAISSE.loader.end();
		});
		
		caisse.executeHook('paiement_validation',{
			//@TODO
		});
		
	};	
	
	
	this.redrawTicket = function() {
		var html = '';
		
		$('.return-lines').show();
		$('#help-return').hide();
		
		if(this.ticket != undefined)
		{
			$.each(this.ticket.products,function(i,product){
			  html += '<tr class="line return_line" unselectable="on" style="user-select: none;">';
		        html += '<td class="label" unselectable="on" style="user-select: none;">'+product.product_name+'</td>';
		        html += '<td unselectable="on" style="user-select: none;"><b>'+product.total_quantity+'</b></td>';
		        html += '<td class="totalLineCost" unselectable="on" style="user-select: none;"><b>'+parseFloat(product.total_return_price).toFixed(2)+'€</b></td>';
		        //todo rajouter ici des infos
		      html += '</tr>';
			});
		}
		
		
		$('#ticketView .return-lines tbody').html(html);
		
		this.redrawTotals();
	};
	
	this.redrawTotals = function() {
		$('#totalReturnBlock').html(parseFloat(PRESTATILL_CAISSE.return.ticket!=null?PRESTATILL_CAISSE.return.ticket.total_refund:0).toFixed(2)+'€');
		$('#totalItem').html(PRESTATILL_CAISSE.return.ticket!=null?PRESTATILL_CAISSE.return.ticket.total_quantity:0);
	};
	
	
	//evenement
	$('body').on('click tap','#return-switch-mode-button',function(){
		caisse_ref.return.switchToReturnMode();
		
		// On vide le ticket
		
		
		$(this).removeClass('open');
	});
	
	// annulation du ticket retour
	$('#returnButtonsPanel').on('click tap','.cancelreturn',function(){
		//on demande la suppression de ce ticket côté serveur		
		var id_return = PRESTATILL_CAISSE.return.ticket == null ? 0:PRESTATILL_CAISSE.return.ticket.id;
		if(id_return == 0)
		{
			caisse_ref.return.switchToBuyMode();
		}
		else
		{
			$.ajax({
		        data: {
		        	'action' : 'CancelReturnTicket',
			        'id_customer' : ACAISSE.customer.id,
			        'id_employee' : ACAISSE.customer.id,
			        'id_product' : $('#popupReturn').data('idProduct'),
			        'id_product_attribute' : $('#popupReturn').data('idProductAttribute'),
			        'id_return' : PRESTATILL_CAISSE.return.ticket == null ? 0:PRESTATILL_CAISSE.return.ticket.id,
		        },
		        type: 'post',
		        dataType: 'json',
		    }).done(function (response) {
		        if (response.success !== false) {
		        	PRESTATILL_CAISSE.return.ticket = null;
		        	$('#ticketView .return-lines tbody').html('');
		        	PRESTATILL_CAISSE.return.redrawTicket();
					caisse_ref.return.switchToBuyMode();		        		        					
		        } else {
		        	alert('Erreur o_ret0143 : impossible sauvegarder les informations');
		        }
		    }).fail(function () {
		        alert('Erreur o_ret0144 : impossible de communiquer avec le serveur');
		    });
	    }
	});
	
	$('.mode-return').on('click tap','#launchRegisterRefundPaymentRefund',function(){
		if(PRESTATILL_CAISSE.return.ticket == null)
		{			
	        alert('Erreur o_ret0640 : Veuillez d\'abord enregistrer des informations de retour');
	        return;
		}
		
		if($(this).hasClass('disabled'))
		{
			return;
		}
		_gotoPaymentPanel();
        	
        $('#paymentView').find('.prestatill_payment_interface .numeric_keyboard').hide();
        
	});
	
	
	$('.mode-return').on('click tap','#launchRegisterRefundPayment',function(){
		
		//var module_refund = PRESTATILL_CAISSE.payment.getCurrentPaymentModule();
		if(PRESTATILL_CAISSE.return.ticket == null)
		{			
	        alert('Erreur o_ret0640 : Veuillez d\'abord enregistrer des informations de retour');
	        return;
		}
		
		if($(this).hasClass('disabled'))
		{
			return;
		}
		
		$.ajax({
	        data: {
	        	'action' : 'RegisterRefundAndReturn',
		        'id_customer' : ACAISSE.customer.id,
		        'id_employee' : $('#showEmployee').data('id_employee'),
		        'id_pointshop' : ACAISSE.id_pointshop,
		        'id_return' : PRESTATILL_CAISSE.return.ticket.id,
		        'summary' : '',
		        'module_name' : 'prestatillslip'
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	PRESTATILL_CAISSE.return.ticket = null;
	        	//PRESTATILL_CAISSE.return.redrawTicket();
	        	$('#ticketView .return-lines tbody').html('');
				caisse_ref.return.switchToBuyMode();
				
				//alert('Avoir généré... cool!');
				//console.log('*****************');
				//console.log(response);				
				
				$.each(response.response.printable_slips,function(i,slip){
					
					$('#printTickets .left .ticket.toPrint .kdoprint').hide();
					$('#printTickets .left .ticket.toPrint .kdotitle').show();
					$('#printTicket').html(slip);
					//window.print();
					printTicket(slip,1);
				});			
				
				printQueue();	
				
				_gotoTicketView();
	        } else {
	        	alert('Erreur o_ret01643 : impossible sauvegarder ce retour pour le moment');
	        }
	    }).fail(function () {
	        alert('Erreur o_ret0644 : impossible de communiquer avec le serveur');
	    });
	});
	
	//validation de la popup de retour
	$('#popupReturn').on('tap click','#popupReturnValidateButton',function(){
		
	   var datas = {
	        'action' : 'SaveReturnPopupDatas',
	        'id_customer' : ACAISSE.customer.id,
	        'id_employee' : ACAISSE.customer.id,
	        'id_product' : $('#popupReturn').data('idProduct'),
	        'id_product_attribute' : $('#popupReturn').data('idProductAttribute'),
	        'id_return' : PRESTATILL_CAISSE.return.ticket == null ? 0:PRESTATILL_CAISSE.return.ticket.id,
 	   };
	   
	   $.each($('#popupReturn form').serializeArray(),function(){
	   		//console.log(this);
	   		datas[this.name] = this.value;
	   });
	   		
		$.ajax({
	        data: datas,
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	
	        	
	        	var request = response.request;
	        	var response = response.response;	  
	        	PRESTATILL_CAISSE.return.ticket = response.return_ticket;
	        	
	        	PRESTATILL_CAISSE.return.redrawTicket();
	        	
	        	//on referme la popup
	        	$('#popupReturn').hide();
	        	$list = $('#popupReturn .orders_list tbody').html('');
	        		        		        					
	        } else {
	        	alert('Erreur o_ret0123 : impossible sauvegarder les informations');
	        }
	    }).fail(function () {
	        alert('Erreur o_ret0124 : impossible de communiquer avec le serveur');
	    });
		
		
		/************************/
		
		
		
	});
	
	function _calcul_total_popup()
	{
		var tot_lines = 0;
		$.each($('#popupReturn form input.price'),function(){
			if($(this).prop('disabled') == false)
			{
				//found qu
				var qu = Math.max(0,parseInt('0' + $(this).parents('tr').find('input[name^="quantity"]').val()));
				//console.log('qu sur ligne : ' + qu  + " * " + $(this).val());
				
				tot_lines += qu * parseFloat($(this).val());
			}
			
		});
		$('#popupReturn .footer .total_popup span').html(parseFloat(tot_lines).toFixed(2)+'€');
	}
	
	
	this.refreshAmountDisplay = function() {
		//$('.solde_restant').html(diff==0 && repaid == 0?'':repaid>0?formatted_repaid:formatted_diff);
		//$('.payment_amount').html(formatted_refund_amount);		
		//$('.display_back').removeClass('too_much not_enough good repaid').addClass(diff==0 && repaid == 0?'good':(diff>0?'not_enough':(repaid>0?'repaid too_much':'too_much')));
		//$('#real_refund_amount').html(formatted_real_payment_amount + currency);
		
		$('#real_refund_amount').html(PRESTATILL_CAISSE.return.ticket.total_refund + currency);							
	};
	
	this.getTotal = function() {		
		var tot_to_pay = $('#totalReturnBlock').html();
		var tot_to_pay_res = tot_to_pay.replace(" ","");
		
		return parseFloat('0'+tot_to_pay_res);
	};
	
	
	$(function(){
		//par défaut la caisse pass en mode BUY
		caisse.return.switchToBuyMode();		
	});
	
}