function CartLine(cart_line_from_prestashop, detaxed_ticket)
{	
	if(detaxed_ticket == undefined)
	{
		detaxed_ticket = false;
	} 
	
	ticket_line = cart_line_from_prestashop;
	
	//raw datas from DB
	//this.details = ticket_line.details;
	var discount = ticket_line.discount;
	//var force_id = ticket_line.force_id;
	//var id = ticket_line.id;
	//var id_a_caisse_ticket = ticket_line.id_a_caisse_ticket;
	var id_product_attribute = ticket_line.id_product_attribute;
	var id_product = ticket_line.id_product;
	//var id_shop_list = ticket_line.id_shop_list;
	
	//this.label = ticket_line.label;
	
	
	var price_with_tax = ticket_line.price_with_reduction;
	var price_without_tax = ticket_line.price_with_reduction_without_tax;
	var original_price_without_tax = ticket_line.price_without_reduction;
	var original_price_with_tax = ticket_line.price_wt;
	var ecotax = ticket_line.ecotax;
	this.quantity = ticket_line.cart_quantity;
	//var tax = ticket_line.tax;
	
	//donnees retravaillées est exploitable
	this.p = _loadProductFromId(id_product);
    this.a;
    var attribute_name='';
    if (id_product_attribute>0) {
    	this.a = this.p.attributes[id_product_attribute];
    	//attribute_name = this.a.attribute_name;
    }
	
    this.unity = _loadProductFromId(ticket_line.id_product).unity;
    this.unitId = params.units.from.indexOf(this.unity);	
    
    //on ajoute l'unité manquante à la volée, mais elle aura aucun effet
    if (this.unitId==-1) {
		//console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(this.p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		this.unitId = params.units.from.indexOf(this.unity);
	}
    
    
    this.scale = params.units.scale[this.unitId];
    this.to = params.units.to[this.unitId];
    
	this.reduction = discount;
		
    if(this.unity != '' && this.reduction != undefined)
    {
    	var discount_length = this.reduction.length;
    	var discount_format = this.reduction.substr(0,parseInt(discount_length-1));
    	this.reduction_ttc = parseFloat('0'+discount_format*this.scale);

    }
    else
    {
    	if(this.reduction != undefined)
    	{
    		this.reduction_ttc = parseFloat('0'+discount);
    	}
    	else
    	{
    		this.reduction_ttc = 0;
    	}
    	
    }	
     
    // Ajustement des quantités si prix unitaire 
    this.quantity = parseFloat(this.quantity/this.scale);
    
    this.reduction_ttc = parseFloat('0'+discount_format*this.scale);
    this.prix_final_ttc = parseFloat(this.scale*price_with_tax);
    this.prix_final_ht = parseFloat(this.scale*price_without_tax);
    this.prix_original_ttc = parseFloat(this.scale*original_price_with_tax);
    this.prix_original_ht = parseFloat(this.scale*original_price_without_tax);
    this.reduction_type = false;
    
    //TODO : attention ici n'apparaisse plus les reduciton avec les cart_line... a corriger
    if(this.reduction)
    {
	    if(this.reduction.indexOf('%')!=-1) //si reduction en pourcentage
	    {
	    	if(this.unity != '')
	    	{
	    		this.reduction_ttc = this.prix_original_ttc * this.reduction_ttc / 10000;
	    	}else
	    	{
	    		this.reduction_ttc = this.prix_original_ttc * this.reduction_ttc / 100;
	    	}
	    	this.reduction_type = '%';
	    }
	    else if(this.reduction.indexOf('€')!=-1) //si reduction en pourcentage
	    {
	    	this.reduction_type = '€';
	    }
    }
    
    this.reduction_ht = this.prix_original_ht * this.reduction_ttc / this.prix_original_ttc;
    this.taux_tva = 100 * (this.prix_original_ttc - this.prix_original_ht) / this.prix_original_ht;
    
    //pour backup
	this.ticket_line = ticket_line;
	
	console.log('ici');
    
    //propriété formaté pour l'affichage
    this.has_reduction = this.prix_final_ttc != this.prix_original_ttc;    
    this.display_prix_final_ttc = (this.prix_final_ttc.toFixed(2))+'€'+ this.to;
    this.display_prix_final_ht = (this.prix_final_ht.toFixed(2))+'€'+ this.to;
    this.display_prix_original_ttc = (this.prix_original_ttc.toFixed(2))+'€'+ this.to;
    this.display_prix_original_ht = (this.prix_original_ht.toFixed(2))+'€'+ this.to;
    
    this.display_reduction = this.has_reduction ? '- ' + this.reduction_ttc.toFixed(2)+this.reduction_type : '';
    this.line_price_ht = this.quantity * ticket_line.total;
    this.line_price_ttc = this.quantity * ticket_line.total_wt;
    this.display_line_price_ht = (this.quantity * this.line_price_ht).toFixed(2)+'€';
    this.display_line_price_ttc = (this.quantity * this.line_price_ttc).toFixed(2)+'€';
}