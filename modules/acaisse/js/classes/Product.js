function Product(id_product) {
	
	// @todo : refaire otutes les correspondances
	this.id_cat=acaisse_products_prices.catalog[id_product].a;
	this.reference=acaisse_products_prices.catalog[id_product].b;
	this.product_name = acaisse_products_prices.catalog[id_product].c;
	this.has_attributes = false;
	this.attributes = {};
	this.id_default_attribute = acaisse_products_prices.catalog[id_product].f;
	this.prices = acaisse_products_prices.catalog[id_product].g;
	this.base_price = acaisse_products_prices.catalog[id_product].d;
	this.unit_price = acaisse_products_prices.catalog[id_product].h;
	this.unity = acaisse_products_prices.catalog[id_product].i;
	this.tax_rate = acaisse_products_prices.catalog[id_product].j;
	this.id_tax_rule_group = acaisse_products_prices.catalog[id_product].k;
	this.ecotax = acaisse_products_prices.catalog[id_product].l;
	this.unit_price_ratio = acaisse_products_prices.catalog[id_product].m;
	this.id_product = acaisse_products_prices.catalog[id_product].n;
	this.disponibility_by_shop = acaisse_products_prices.catalog[id_product].o;
	this.active = acaisse_products_prices.catalog[id_product].p;
	this.is_generic_product = acaisse_products_prices.catalog[id_product].y == '1';
	
	//@TODO pourquoi pas ajouter this.image = ',oPthoto.jpg'; //directement ici
	if (acaisse_products_prices.catalog[id_product].q!=undefined) {
		this.image = (''+acaisse_products_prices.catalog[id_product].q).replace('http://','//').replace('https://','//');
	}
	
	for (var id in acaisse_products_prices.catalog[id_product].e) {
		this.has_attributes = true;
		
		this.attributes[id] = {};
		this.attributes[id].attribute_name = acaisse_products_prices.catalog[id_product].e[id].r;
		this.attributes[id].prices = acaisse_products_prices.catalog[id_product].e[id].s;
		this.attributes[id].infos = acaisse_products_prices.catalog[id_product].e[id].t;
		this.attributes[id].ref = acaisse_products_prices.catalog[id_product].e[id].u;
		this.attributes[id].ecotax = acaisse_products_prices.catalog[id_product].e[id].v;
		this.attributes[id].image = (''+acaisse_products_prices.catalog[id_product].e[id].w).replace('http://','//').replace('https://','//');
		this.attributes[id].id_product_attribute = acaisse_products_prices.catalog[id_product].e[id].x;
	}
		
	if(this.is_generic_product === true) //un produit générique ne peut pas avoir "réellement" des déclinaisons
	{
		this.has_attributes = false;
	}
}