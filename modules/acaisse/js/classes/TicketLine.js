function TicketLine(ticket_line)
{
	this.is_detaxed = ACAISSE.ticket.is_detaxed == 1;
	
	//raw datas from DB
	this.details = ticket_line.details;
	var discount = ticket_line.discount;
	var force_id = ticket_line.force_id;
	this.id = ticket_line.id;
	var id_a_caisse_ticket = ticket_line.id_a_caisse_ticket;
	this.id_product_attribute = ticket_line.id_attribute;
	this.id_product = ticket_line.id_product;
	var id_shop_list = ticket_line.id_shop_list;
	this.label = ticket_line.label;
	var price_with_tax = ticket_line.price_with_tax;
	var price_without_tax = ticket_line.price_without_tax;
	var original_price_with_tax = ticket_line.original_price_with_tax;
	this.quantity = ticket_line.quantity;
	var original_price_without_tax = ticket_line.original_price_without_tax;
	
	var original_price_is_forced_by_user = ticket_line.original_price_is_forced_by_user;
	
    var tax = ticket_line.tax;
	
	//donnees retravaillées est exploitable
	this.p = _loadProductFromId(this.id_product);
	
	//console.log(['class TicketLine',this.p,ticket_line, this.id_product_attribute, this.p.is_generic_product]);
    this.a; //l'attribut
    var attribute_name='';
    if (this.id_product_attribute>0) {
    	this.a = this.p.attributes[this.id_product_attribute];
    	//attribute_name = this.a.attribute_name;
    }
	
    this.unity = _loadProductFromId(this.id_product).unity;
    this.unitId = params.units.from.indexOf(this.unity);	
    
    //on ajoute l'unité manquante à la volée, mais elle aura aucun effet
    if (this.unitId==-1) {
		//console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(this.p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		this.unitId = params.units.from.indexOf(this.unity);
	}
    
    
    this.scale = params.units.scale[this.unitId];
    this.to = params.units.to[this.unitId];
	var ps_price = parseFloat(ticket_line.price_with_tax);
	
	this.reduction = discount;
	
	if(this.unity != '')
    {
    	var discount_length = this.reduction.length;
    	var discount_format = this.reduction.substr(0,discount_length-1);
    	
    	this.reduction_ttc = parseFloat('0'+discount_format*this.scale);

    }
    else
    {
    	this.reduction_ttc = parseFloat('0'+discount);
    }	
    //console.log(this.reduction_ttc);
     
    // Ajustement des quantités si prix unitaire 
    this.quantity = parseFloat(this.quantity/this.scale);
    
    //this.reduction_ttc = parseFloat('0'+discount);
    this.prix_final_ttc = parseFloat(this.scale*price_with_tax);
    this.prix_final_ht = parseFloat(this.scale*price_without_tax);
    this.prix_original_ttc = parseFloat(this.scale*original_price_with_tax);
    this.prix_original_ht = parseFloat(this.scale*original_price_without_tax);
    this.reduction_type = false;
    
    this.base_price_ht = parseFloat(this.scale*this.p.base_price);
    this.base_price_ttc = parseFloat(this.scale*this.p.base_price*(1+(this.p.tax_rate/100)));
    
    this.price_orig = false;
    
    //propriété formaté pour l'affichage
    if(this.detaxed)
    {
    	this.has_reduction = this.prix_final_ttc != this.prix_original_ttc;
    }
    else
    {
    	this.has_reduction = this.prix_final_ht != this.prix_original_ht;
    }
    
    
    if(this.reduction.indexOf('%')!=-1) //si reduction en pourcentage
    {
    	this.reduction_type = '%';
    	if(this.unity != '')
    	{
    		this.reduction_ttc = this.prix_original_ttc * this.reduction_ttc / 10000;
    	}else
    	{
    		this.reduction_ttc = this.prix_original_ttc * this.reduction_ttc / 100;
    	}
		this.display_reduction = this.has_reduction ? '- ' + this.reduction : '';
    }
    else if(this.reduction.indexOf('€')!=-1) //si reduction en pourcentage
    {
    	this.reduction_type = '€';
    	if(this.unity != '')
    	{
    		this.reduction_ttc = discount_format*this.scale;
			this.display_reduction = this.has_reduction ? '- ' + this.reduction_ttc.toFixed(2)+this.reduction_type : '';
    	}
    	else
    	{
    		this.display_reduction = this.has_reduction ? '- ' + this.reduction : '';
    	}
    	    	
    }
    
    this.reduction_orig_ht = parseFloat(this.base_price_ht - this.prix_final_ht);   
    this.reduction_orig = this.is_detaxed?this.reduction_orig_ht:parseFloat(this.base_price_ttc - this.prix_final_ttc); 
      
    this.display_reduction_orig = (this.reduction_orig.toFixed(2))+'€'+ this.to;
    this.display_reduction_orig_ht = (this.reduction_orig_ht.toFixed(2))+'€'+ this.to;
    
    this.reduction_ht = this.prix_original_ht * this.reduction_ttc / this.prix_original_ttc;
    this.taux_tva = 100 * (this.prix_original_ttc - this.prix_original_ht) / this.prix_original_ht;
    
    //pour backup
	this.ticket_line = ticket_line;
	
    this.display_prix_final_ttc = (this.prix_final_ttc.toFixed(2))+'€'+ this.to;
    this.display_prix_final_ht = (this.prix_final_ht.toFixed(2))+'€'+ this.to;
    this.display_prix_original_ttc = (this.prix_original_ttc.toFixed(2))+'€'+ this.to;
    this.display_prix_original_ht = (this.prix_original_ht.toFixed(2))+'€'+ this.to;

	//this.display_reduction = this.has_reduction ? '- ' + this.reduction_ttc.toFixed(2)+this.reduction_type : '';
    
    this.line_price_ht = this.quantity * this.prix_final_ht;
    this.line_price_ttc = this.quantity * this.prix_final_ttc;
    this.display_line_price_ht = (this.quantity * this.prix_final_ht).toFixed(2)+'€';
    this.display_line_price_ttc = (this.quantity * this.prix_final_ttc).toFixed(2)+'€';

	if(original_price_is_forced_by_user == 1)
	{
	    this.display_base_price_ht = (this.prix_final_ht.toFixed(2))+'€'+ this.to;
	    this.display_base_price_ttc = (this.prix_final_ttc.toFixed(2))+'€'+ this.to;  
	    this.base_price_ht = parseFloat(this.scale*this.prix_final_ht);
    	this.base_price_ttc = parseFloat(this.scale*this.prix_final_ht*(1+(this.p.tax_rate/100)));
    	//this.prix_original_ttc = this.base_price_ttc;
    	//this.prix_original_ht = this.base_price_ht;
    	
    	if(this.detaxed)
	    {
	    	this.has_reduction = this.prix_final_ttc != this.prix_original_ttc;
	    }
	    else
	    {
	    	this.has_reduction = this.prix_final_ht != this.prix_original_ht;
	    }
	} 
	else
	{
		this.display_base_price_ht = (this.base_price_ht.toFixed(2))+'€'+ this.to;
	    this.display_base_price_ttc = (this.base_price_ttc.toFixed(2))+'€'+ this.to;
	} 
    
    this.original_price_is_forced_by_user = original_price_is_forced_by_user;
    
    //console.log(this.has_reduction);
}
