function USBCustomerScreen(caisse_ref)
{	
	
	var MODE_DEBUG = false;
	
	var caisse = caisse_ref;
	var usb_screen = null;
	
	
	var pointshop_params = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas;
	params.customerScreen.mode = pointshop_params.screen_mode;
	
	var _init = function() {
		
		if(MODE_DEBUG == true)
		{
			console.log([
				'init usb screen module',
				params.customerScreen.mode,
				params.customerScreen.mode.indexOf('usb'),
				usb_screen,
			]);		
		}
		
		if (params.customerScreen.mode.indexOf('usb')!=-1 && usb_screen==undefined) {		
			usb_screen = window.open(
				params.customerScreen.usb_screen_controller_url,
				"_blank",
				"fullscreen=yes, toolbar=no, scrollbars=no, resizable=non, top=0, left=0, width=800, height=600"
			);
			
			// On met l'usb screen dans windows pour pouvoir le fermer
			windows.push(usb_screen);		}
	};
	
	/*
	caisse.registerHook('test', function() {
		
	});
	*/
	
	_init();	
}
