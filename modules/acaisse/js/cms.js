/*
 *JS utiliser par la partie CMS du module de caisse
 */
$(function(){
    var loadCellEditor = function ($cell) {
        $("table#acaissepagecms td").removeClass('selected');
        $cell.addClass('selected');
        //on peuple le formulaire d'édition
        $("#acaissecms_row").val($cell.data("row"));
        $("#acaissecms_col").val($cell.data("col"));
        $("#acaissecms_rowspan").val($cell.data("rowspan"));
        $("#acaissecms_colspan").val($cell.data("colspan"));
        $("#acaissecms_style").val($cell.data("style"));
        $('.acaisse_styleSelector').removeClass("selected");
        $('.acaisse_styleSelector[data-value='+$cell.data("style")+']').addClass("selected");
        $("#acaissecms_type").val($cell.data("type"));
        $("#acaissecms_id_object").val($cell.data("idobject"));
        $("#acaissecms_id_object2").val($cell.data("idobject2"));
        $("#acaissecmspopup").slideDown();
    };
    
    $("table#acaissepagecms td").on('click tap',function(e) {
       loadCellEditor($(this));	 
    });
	
	/*
	 *     const TYPE_SPANNED = 100; 
    const TYPE_EMPTY = 0; 
    const TYPE_PAGE = 1; 
    const TYPE_PRODUCT = 2; 
    const TYPE_PRODUCT_ATTRIBUTE = 3; 
	 */
	
  $('.acaisse-page-selector').on('click tap',function(e) {
    $("#acaissecms_id_object").val($(this).data("id"));
    $("#acaissecms_id_object2").val("");
    $("#acaissecms_type").val(1);
    $("#acaissecms_selection_name").html("Allez vers la page <em>\""+$(this).html()+"\"</em>");
  });
  $('.acaisse-category-selector').on('click tap',function(e) {
    $("#acaissecms_id_object").val($(this).data("id"));
    $("#acaissecms_id_object2").val("");
    $("#acaissecms_type").val(4);
    $("#acaissecms_selection_name").html("Voir la category <em>\""+$(this).html()+"\"</em>");
  });
	
	//acaisse-product-selector
	$('.acaisse-product-selector').on('click tap',function(e) {
		$("#acaissecms_id_object").val($(this).data("id"));
		$("#acaissecms_id_object2").val("");
		$("#acaissecms_type").val(2);
		$("#acaissecms_selection_name").html("Ajouter au panier <em>\""+$(this).html()+"\"</em>");
	});
	
	
	//acaisse-product-attribute-selector
	$('.acaisse-product-attribute-selector').on('click tap',function(e) {
		$("#acaissecms_id_object").val($(this).data("id"));
		$("#acaissecms_id_object2").val($(this).data("id2"));
		$("#acaissecms_type").val(3);
		$("#acaissecms_selection_name").html("Ajouter au panier <em>\""+$(this).html()+"\"</em>");
	});
    
    $('.acaisse_styleSelector').on('click tap',function(e) {
        $("#acaissecms_style").val($(this).data("value"));
        $('.acaisse_styleSelector').removeClass("selected");
        $(this).addClass("selected");
    });
    
    $('.acaisse_typeSelector').on('click tap',function(e) {
        if($(this).data("value")=="0")
        {
        	alert('vidage de la cellule');
        	$("#acaisse_filter").hide();
        	$("#acaissecms_type").val(0);
        	$("#acaissecms_selection_name").html("Cellule sans contenu");
        }
        else { $("#acaisse_filter").show(); }
        $(".acaisse_searchpanel").hide();
        $("#acaisse_search_"+$(this).data("value")).show();                
    });
    
    $("#acaisse_filter_input").keyup(function(e){
       var filter=$(this).val().trim().toLowerCase();
       if(filter=="")
       {
           //reset pas de filtre!
           $(".acaisse_searchpanel>div").each(function(i,elt) {
               $(elt).show();
           }); 
       }
       else
       {
           $(".acaisse_searchpanel>div").each(function(i,elt) {
               if($(elt).text().toLowerCase().indexOf(filter)==-1 && !$(elt).hasClass("acaisse_explain"))
               {
                    $(elt).hide();
               }
               else
               {
                    $(elt).show();
               }
           });                   
       }
    });
    
    $("#acaisse_filter_reset").on('click tap',function(e){
       //reset du filtre
       $(".acaisse_searchpanel>div").each(function(i,elt) {
           $(elt).show();
       });
    });
    
    $("#acaisse_closepopup").on('click tap',function() {
        $("#acaissecmspopup").slideUp();
    });            
    
});