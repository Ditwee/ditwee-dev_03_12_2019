$(function(){
	
	//delivery	
	$('#delivery_option_block').uiSelector({
      input : $('#delivery_option'),
      onChange : function(new_value) {      	
      	$('#popupDeliveryForm .delivery_address_column ul.adresse_selector_list').addClass('hide');
      	
      	if(new_value == PRESTATILL_CAISSE.delivery.STORE_DELIVERY) //retrait magasin
      	{      		
      		$('#popupDeliveryForm #id_delivery_address_store').removeClass('hide').find('li').eq(0).click();
      		$('#carrier_option_block .item[data-value="0"]').click();
      	}
      	
      	if(new_value == PRESTATILL_CAISSE.delivery.HOME_DELIVERY) //livraison à domicile
      	{
      		$('#popupDeliveryForm #id_delivery_address_home').removeClass('hide').find('li').eq(0).click();
      		$('#carrier_option_block .item[data-value="1"]').click();
      	}
      	
      }
    });
    
    
    $('#popupDeliveryForm').on('click tap','.delivery_address_column .adresse_selector_list li',function(){
    	$('#popupDeliveryForm .delivery_address_column .adresse_selector_list li').removeClass('selected');
    	$('#popupDeliveryForm .delivery_address_column input[name="id_delivery_address"]').val($(this).addClass('selected').data('id'));
    });
    
    //carrier
    	//chargement dynamique
    
    
    $('#popupDeliveryForm').on('click tap','.carrier_address_column .adresse_selector_list li',function(){
    	$('#popupDeliveryForm .carrier_address_column .adresse_selector_list li').removeClass('selected');
    	$('#popupDeliveryForm .carrier_address_column input[name="id_carrier_address"]').val($(this).addClass('selected').data('id'));
    });  
    
    
	//invoice	
	$('#invoice_option_block').uiSelector({
      input : $('#invoice_option'),
      onChange : function(new_value) {      	
      	//$('#popupDeliveryForm .invoice_address_column ul.adresse_selector_list').addClass('hide');
      	$('#popupDeliveryForm .invoice_address_column ul.adresse_selector_list li').eq(0).click();
      	
      	if(new_value == PRESTATILL_CAISSE.invoice.WITHOUT) //retrait magasin
      	{ 
      		$('#invoice_info_block').addClass('hide');
      	}
      	
      	if(new_value == PRESTATILL_CAISSE.invoice.WITH) //livraison à domicile
      	{
      		$('#invoice_info_block').removeClass('hide');  
      	}      	
      }
    });
    
    $('#popupDeliveryForm').on('click tap','.invoice_address_column .adresse_selector_list li',function(){
    	$('#popupDeliveryForm .invoice_address_column .adresse_selector_list li').removeClass('selected');
    	$('#popupDeliveryForm .invoice_address_column input[name="id_invoice_address"]').val($(this).addClass('selected').data('id'));
    });
    
    //detax	
	$('#detax_option_block').uiSelector({
      input : $('#detax_option'),
      onChange : function(new_value) {      	
      	//$('#popupDeliveryForm .detax_address_column ul.adresse_selector_list').addClass('hide');
      	$('#popupDeliveryForm .detax_address_column ul.adresse_selector_list li').eq(0).click();
      	
      	if(new_value == PRESTATILL_CAISSE.invoice.WITHOUT_DETAX) //retrait magasin
      	{      		
      		$('#detax_info_block').addClass('hide');
      	}
      	
      	if(new_value == PRESTATILL_CAISSE.invoice.WITH_DETAX) //livraison à domicile
      	{      		
      		$('#detax_info_block').removeClass('hide');      		
      	}
      	
      }
    });
    
    
        
    $('#popupDelivery').on('click tap','#popupDeliveryPopupValidateButton',function(){
    	var datas = $('#popupDeliveryForm').serializeArray();
    	datas.push({'name':'action', 'value':'SaveDeliveryPopupDatas'});
    	datas.push({'name':'id_pointshop', 'value':ACAISSE.id_pointshop});
    	datas.push({'name':'id_ticket', 'value':ACAISSE.ticket.id});
    	
    	PRESTATILL_CAISSE.loader.start();
    	
    	$.ajax({
	        type: 'post',
	        data: datas,
	        dataType: 'json',			
		}).done(function(response){
			if(response.success === true)
			{
				ACAISSE.ticket = response.response.ticket;
				$('#popupDelivery .closePopup').click();
				PRESTATILL_CAISSE.loader.end();
			}
			else
			{
				alert('Error O_deliv_1002');
			}
		}).fail(function(){
			alert('Error O_deliv_1003');
		});
    	
    	
    	//ex : delivery_option=0&id_delivery_address=8&carrier_option=1&id_carrier=35&invoice_option=1&id_invoice_address=1&detax_option=0&detax_info=dsqsq
    });
    
    
    
});
