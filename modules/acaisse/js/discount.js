
function registerDiscountOnCart(amount,type, extra_datas) {
	
	//console.log(amount);
	if(isNaN(amount))
	{
		amount = 0;
	}
	
	$.ajax({
        data: {
            'action': 'RegisterQuickDiscountOnGlobalCart',
            'id_ticket': ACAISSE.ticket.id,
            'id_force_group':ACAISSE.id_force_group,
            'amount' : amount,
            'type' : type,
            'extra_datas' : extra_datas
        },
        type: 'post',
        dataType: 'json',
    }).done(function (response) {
        if (response.success !== false) {
        	
        	var response = response.response;
        
			ACAISSE.cart_tot = response.cart_tot;
			ACAISSE.cart_tot_ht = response.cart_tot_ht;
			ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
			
			ACAISSE.ticket = response.ticket;
			ACAISSE.cart = response.cart;
			
			// On execute le hook pour mettre à jour les tickets lines
			//PRESTATILL_CAISSE.executeHook('actionUpdatePrestatillTicketGlobalDiscount',{'id_ticket':ACAISSE.ticket.id});
			
			//_switchToTicket(response.ticket.id,false,true);
			
			refreshPrices(response.cart_products);
            
            $('#reductionGlobalOnCart .pastille').remove();
            
            if (amount!=0) {
            	$('#reductionGlobalOnCart').prepend('<span class="pastille big"><span class="label">Remise appliquée :</span> <br />-'+amount+type+' <br /><span class="modify">modifier</span></span>');
            }
            
            var module_obj = {action:'tot',tot:formatPrice(response.cart_tot_ttc),id_ticket:response.ticket.ticket_num};
			modules_action(module_obj);
            
            
        } else {
        	alert('Erreur k_00010 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
        	
        }
    }).fail(function () {
        alert('Erreur k00010 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
    });
}

function registerDiscountOnCartLine(amount,type,id_product,id_product_attribute,id_ticket_line, extra_datas) {
	
	if(ACAISSE.cartModificationLocked !== true)
	{
		alert('Error 20312 : le ticket n\'est pas prêt');
	}
	else
	{
		$.ajax({
	        data: $.extend({},{
	            'action': 'RegisterQuickDiscountOnCartLine',
	            'id_ticket': ACAISSE.ticket.id,
	            'amount' : amount,
	            'type' : type,
	            'id_product' : id_product,
	            'id_product_attribute' : id_product_attribute,
	            'id_ticket_line' : id_ticket_line
	        },extra_datas),
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	var response = response.response;
	        	/*console.log([
	        		'registerDiscountOnCartLine response (success true)',
	        		response
	        	]);*/
				ACAISSE.cart_tot = response.cart_tot;
				ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
				ACAISSE.cart_tot_ht= response.cart_tot_ht;
				ACAISSE.ticket = response.ticket;
				
				refreshPrices(/*response.cart_products*/);
				
				_refreshTicket(null,false);
				
	        } else {
	        	alert('Erreur k003106 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	        }
	    }).fail(function () {
	        console.log('Erreur k0003107 : impossible de créer ou modifier la réduction. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	    });
    }
}

function loadDiscount() {
	$('#discountContainer').html('<div><i class="fa fa-spinner fa-spin"></i></div>');
    
    $.ajax({
        data: {
            'action': 'LoadDiscountForCustomer',
            'id_customer': ACAISSE.customer.id,
        },
        type: 'post',
        dataType: 'json',
    }).done(function (response) {
        var response = response.response;
        if (response.success != false) {
            var html = '';
            var bons = response.bons;
            
            if (bons.length==0) {
            	$('#discountContainer').html('<span class="no-discount">Ce client ne possède pas de bons d\'achat.</span>');
            	return false;
            }
            
            var cartRules = [];
            if (ACAISSE.ticket.id_cart_rules!=undefined) {
            	cartRules = ACAISSE.ticket.id_cart_rules.split(';');
            }
            
            for (var i=0; i<bons.length; i++) {
            	html += '<div class="discount"><div class="inner">'
            	  html += '<i class="fa fa-scissors"></i>';
            		html += '<div><span class="qte">' + bons[i].qte + '</span></div>';
            		html += '<div class="left_block"><span class="price">' + (bons[i].amount.indexOf('%')==-1?formatPrice(bons[i].amount)+'€':parseInt(bons[i].amount)+'%') + '<span class="immediat">DE REMISE IMMEDIATE</span></span>';
            		html += '<div class="date_box '+(bons[i].validityDate.to<bons[i].validityDate.now?'tooLate':'')+'"><span class="date '+(bons[i].validityDate.from>bons[i].validityDate.now?'notYet':'')+'">du '+Date.toString(bons[i].validityDate.from)+'</span><br /><span class="date '+(bons[i].validityDate.to<bons[i].validityDate.now?'tooLate':'')+'">au '+Date.toString(bons[i].validityDate.to)+'</span></div>';
            		html += '<div class="conditions">';
            		if (bons[i].minimum_amount>0) {
            			html += 'à partir de <span class="minimum_amount"><b>' + formatPrice(bons[i].minimum_amount) + '€</b></span> d\'achat';
        			}
            		html += '<div>Utilisation partielle : <b>' + (bons[i].is_partial==1?'oui':'non') + '</b></div>';
            		html += '</div></div>';
            		html += '<div class="infos">';
            		html += '<h4>'+bons[i].name+'</h4>';
            		html += '<span class="descript">'+bons[i].description+'</span>';
            		html += '</div>';
        			html += '<button data-is_active="'+(cartRules.indexOf(bons[i].id_cart_rule)==-1?'0':'1')+'" '+(bons[i].qte==0?'disabled ':'')+'onclick="if ($(this).data(\'is_active\')==\'0\') {useDiscount(\''+bons[i].code+'\',\''+bons[i].id_cart_rule+'\',this);} else {deleteDiscount(\''+bons[i].code+'\',\''+bons[i].id_cart_rule+'\',this);}">'+(cartRules.indexOf(bons[i].id_cart_rule)==-1?'Faire valoir':'Retirer le bon')+'</button>';
            	html += '</div></div>';
            }
            $('#discountContainer').html(html);
        } else {
        	alert('Erreur k_00001 : impossible d\'obtenir la liste des bons de réductions du serveur. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
        }
    }).fail(function () {
        alert('Erreur k00001 : impossible d\'obtenir la liste des bons de réductions du serveur. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
    });
}

function deleteDiscount(code, id_cart_rule, _this) {
	_this.disabled = true;
	$.ajax({
        data: {
            'action': 'DeleteDiscount',
            'id_ticket':ACAISSE.ticket.id,
            'id_cart_rule':id_cart_rule
        },
        type: 'post',
        dataType: 'json',
    }).done(function (response) {
        if (response.success != false) {
			var response = response.response;
			
			ACAISSE.cart_tot = response.cart_tot;
			ACAISSE.cart_tot_ht = response.cart_tot_ht;
			ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
			
			ACAISSE.ticket = response.ticket;
			ACAISSE.cart = response.cart;
			
			refreshPrices(response.cart_products);
			
			var product_list = [];
			for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
				product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
				product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
				product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
			}
			if(ACAISSE.mode_debogue === true) {console.log(product_list);}
			var module_obj = {action:'switch_ticket',product_list:product_list};
			
			modules_action(module_obj);
			
			_refreshTicket(null,false);
			
			sendNotif('greenFont','<i class="fa fa-check"></i>Bon '+code+' retiré');
			
			_this.disabled = false;
			$(_this).data('is_active','0');
			$(_this).html('Faire valoir');
			
			$('#reductionGlobalOnCart .pastille').remove();
			
			closeAllPanel();
			
			//refreshPrices(response.cart_products);
        } else {
        	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>'+response.error);
        }
    }).fail(function () {
        sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Bon '+code+' non retiré');
    });
}

function useDiscount(code, id_cart_rule, _this) {
	_this.disabled = true;
	
	// on valide le ticket en commande	
	if (!ACAISSE.cartModificationLocked) {    		
		//$('.button.validTicket').click();
		_validateCurrentTicket(false);
	}
	
	// on doit passer par un timeout pour attendre la validation du payment
	function checkToApply() {
		if ($('#orderButtonsPanel').css('display')=='none') {
			setTimeout(checkToApply,50);
		} else {
			useDisc();
		}
	}
	
	function useDisc() {
		$.ajax({
	        data: {
	            'action': 'UseDiscount',
	            'id_ticket':ACAISSE.ticket.id,
	            'id_cart_rule':id_cart_rule
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success != false) {
				var response = response.response;
				
				ACAISSE.cart_tot = response.cart_tot;
				ACAISSE.cart_tot_ht = response.cart_tot_ht;
				ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
				
				ACAISSE.ticket = response.ticket;
				ACAISSE.cart = response.cart;
				
				
				refreshPrices(response.cart_products);

				var product_list = [];
				for (var i=0; i<ACAISSE.ticket.lines.length; i++) {
					product_list.push(JSON.parse(JSON.stringify(_loadProductFromId(ACAISSE.ticket.lines[i].id_product))));
					product_list[i].id_attribute = ACAISSE.ticket.lines[i].id_attribute;
					product_list[i].qte = ACAISSE.ticket.lines[i].quantity;
				}
				
				if(ACAISSE.mode_debogue === true) {console.log(product_list);}
				var module_obj = {action:'switch_ticket',product_list:product_list};
				
				modules_action(module_obj);
				
				_refreshTicket(null,false);
				
				sendNotif('greenFont','<i class="fa fa-check"></i>Bon '+code+' appliqué');
				
				_this.disabled = false;
				$(_this).data('is_active','1');
				$(_this).html('Retirer le bon');
				
				closeAllPanel();
				
				//refreshPrices(response.cart_products);
				$('#reductionGlobalOnCart').prepend('<span class="pastille big"><span class="label">Remise appliquée :</span> <br />fidélité<br /></span>');
				
				/**@todo Kévin : Lancer une action vers les afficheurs pour afficher le bon utilisé ?*/
	        } else {
	        	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>'+response.error);
	        }
	    }).fail(function () {
	        sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>Bon '+code+' non appliqué');
	    });
    }
    
	setTimeout(checkToApply,50);
}
