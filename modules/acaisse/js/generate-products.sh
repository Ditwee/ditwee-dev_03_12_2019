#!/bin/bash
rm ../modules/acaisse/js/cache/products-parts/product-.cache.js

#concatenation des caches produits
rm ../modules/acaisse/js/cache/products_prices.cache.js;
touch ../modules/acaisse/js/cache/products_prices.cache.js;
echo "var acaisse_products_prices = {'catalog':{" >> ../modules/acaisse/js/cache/products_prices.cache.js
ls -1v ../modules/acaisse/js/cache/products-parts/*.cache.js | xargs cat >> ../modules/acaisse/js/cache/products_prices.cache.js
echo "}" >> ../modules/acaisse/js/cache/products_prices.cache.js

#concatenation des caches de code EAN
echo ",'ean':{" >> ../modules/acaisse/js/cache/products_prices.cache.js
ls -1v ../modules/acaisse/js/cache/products-ean/*.cache.js | xargs cat >> ../modules/acaisse/js/cache/products_prices.cache.js
echo "}};" >> ../modules/acaisse/js/cache/products_prices.cache.js