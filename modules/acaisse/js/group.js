function _switchToDefaultCustomer() {
	_switchCustomer(acaisse_profiles[acaisse_profilesIDs[0]], false, []);
}

function _getGroupColorForCustomer(customer)
{
  var id_default_group = customer.id_default_group;
  for(var i=0 ; i < acaisse_group.length ; i++)
  {
    if(acaisse_group[i].id_group == customer.id_default_group)
    {
      return acaisse_group[i].color;
    }
  }
  
}

function _getGroupForCustomer(customer)
{
  var id_default_group = customer.id_default_group;
  for(var i=0 ; i < acaisse_group.length ; i++)
  {
    if(acaisse_group[i].id_group == customer.id_default_group)
    {
      return acaisse_group[i];
    }
  }
  return false;  
}

function _switchCustomer(customer, initMode, additionnal_customer_informations) {
	if(ACAISSE.mode_debogue === true)
	{
		console.log('_switchCustomer()');
		console.log('ACAISSE.cartModificationLocked = '+ACAISSE.cartModificationLocked?'true':'false');
	}
	/*
	if (ACAISSE.cartModificationLocked == true)
	{
		
		$.ajax({
			type: 'post',
			dataType: 'json',
			data: {
				'action': 'CancelCartFromTicket',
				'id_ticket': ACAISSE.ticket.id,
				'customer': customer,
				'additionnal_customer_informations' : additionnal_customer_informations,
				'initMode': initMode,
				'deleteCartRules':true
			}
		}).done(function (response) {
			if (!response.success) {
				alert('Erreur 001558' + response.errorMsg);
				return;
			}
			
			ACAISSE.ticket = response.response.ticket;

			//remise en place du panneau non encaisse
			$('#orderButtonsPanel').hide();
			$('#preOrderButtonsPanel').show();
			$('#orderButtonsPanel .paymentButton.selected').click();
			$('#preOrderButtonsPanel #registerPayment').addClass('disabled');
			ACAISSE.choosenPaymentMode = false;
			ACAISSE.cartModificationLocked = false;
			ACAISSE.cart_tot = false;
			
			//modification de la couleur du boutton du customer en fonction de la couleur de son groupe principal
			var group = _getGroupForCustomer(response.request.customer);
			
			ACAISSE.price_display_method = group.price_display_method;
			$('#userName').css('background-color',group.color);
			
			if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1)
			{
				$('.product_base_price').show();
			}
			else
			{
				$('.product_base_price').hide();
			}
			
			
			//puis rappel de l'instruction initiale
			_switchCustomer(response.request.customer, response.request.initMode, response.request.additionnal_customer_informations);
		}).fail(function () {
			alert('Error 001559 Remove one qu after cancel real cart failed.');
		});

		return; //on stop ici, la commande sera retenté qu'après
		// annulation du panier et annulation du passage à l'encaissement	
	}
	*/
	//sinon on peut réaliser l'opération directement.
	var old_price_group = ACAISSE.id_force_group;
	if (typeof (initMode) == 'undefined')
	{
		initMode = false;
	}
	
	//maj des infos dans ACAISSE
	ACAISSE.customer = customer;
	ACAISSE.additionnal_customer_informations = additionnal_customer_informations;
	ACAISSE.customer.id_default_group = customer.id_default_group;
	ACAISSE.id_force_group = _searchSubGroup(customer.id_default_group);
	ACAISSE.customer.groups = customer.groups;

	//if(initMode==true) { return; } //lors de la phase d'initilisation de l'ui on ne peut pas poursuivre.
	
	var module_obj = {action:'demo'};
	
	/*
	lcd_write_line('Démonstration de la',0,undefinedValue,undefinedValue,module_obj);
	lcd_write_line('Caisse PrestaTill !',1);
	lcd_write_line('',2);
	lcd_write_line('',3);
	*/
  //modification de la couleur du boutton du customer en fonction de la couleur de son groupe principal
  
    var group = _getGroupForCustomer(customer);
    $('#userName').css('background-color',group.color);
    
	ACAISSE.price_display_method = group.price_display_method;
	
	_retreiveCustomerInformation(ACAISSE.customer.id);
	
	if (old_price_group != ACAISSE.id_force_group) //il y a eu changement de groupe
	{
		refreshPrices();
	}
	
	if (initMode != true && ACAISSE.current_pointshop.datas.editable_customer == '1' && acaisse_profilesIDs.indexOf('' + customer.id) == -1) {
		$('#userEditContainer [data-page-id="pageUser1"]').html('<i class="fa fa-pencil-square-o" unselectable="on" style="-webkit-user-select: none;"></i> Informations du client');
	} else {
		$('#userEditContainer [data-page-id="pageUser1"]').html('<i class="fa fa-pencil-square-o" unselectable="on" style="-webkit-user-select: none;"></i> Nouveau client');
		$('#userEditContainer [data-page-id="pageUser1"]').click();
	}
	
	$("#editUserButton").show();
	
	_updateCustomerEditForm(ACAISSE.customer);
	// LAURENT A VERIF
	//_refreshTicket();
	closeAllPanel();
	//_reloadPage();
}

function _retreiveCustomerInformation(id_customer) {
	//console.warn(id_customer);
	$('#username').addClass('loading');
	//.html('Chargement...');

	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {
			'action': 'RetreiveCustomerInformation',
			'id_customer': id_customer,
		},
	}).done(function (response) {
		if (response.success)
		{
			//console.log(response.response);
			ACAISSE.customer = response.response.customer;
			ACAISSE.customer.address = response.response.address;
			ACAISSE.customer.addresses = response.response.addresses;
			ACAISSE.customer.groups = response.response.groups;
			ACAISSE.additionnal_customer_informations = response.response.additionnal_customer_informations;
			_refreshCustomerInformation();
			_updateCustomerEditForm(ACAISSE.customer, ACAISSE.customer.groups);
			refreshPrices();
			
			/*$.each(PRESTATILL_CAISSE.customer.hooks['afterRetreiveCustomerInformations'], function(i,hook_callback){
				hook_callback(response.response);
			});*/
			PRESTATILL_CAISSE.executeHook('afterRetreiveCustomerInformations');
		}
		else
		{
			alert('Erreur 00922 : ' + response.errorMsg);
		}
	}).fail(function () {
		alert('Erreur 00923');
	});
}

function _refreshCustomerInformation() {
	var html = '<i class="fa fa-spinner fa-spin"></i>';
	html += ACAISSE.customer.firstname + ' ' + ACAISSE.customer.lastname + '<span style="display:none;"> (' + ACAISSE.customer.id_default_group + '/'+ACAISSE.id_force_group+')</span>';
	if(ACAISSE.price_display_method == 1) //HT
	{
		$('body').addClass('ht').removeClass('ttc');
		$('.price_display_method').html('HT');
		html+='<span style="display: block; position: relative; top: -45px; font-size: 10px;">Prix affichés HT</span>';

	}
	else
	{		
		$('body').addClass('ttc').removeClass('ht');
		$('.price_display_method').html('TTC');
		html+='<span style="display: block; position: relative; top: -45px; font-size: 10px;">Prix affichés TTC</span>';

	}				
	$('#userName').removeClass('loading').html(html);
}

/**
 * Recherche le sous groupe lié au groupe par défaut principal de l'utilisateur 
 */
function _searchSubGroup(id_group)
{
	//je cherche un groupe avec id_pointshop = point de vente de la caisse
	//id_parent_group === id_group
	//et groupe actif
	for(var i = 0 ; i < acaisse_group.length ; i++)
  {
    if(acaisse_group[i].id_parent_group == id_group && ACAISSE.id_pointshop == acaisse_group[i].id_pointshop)
    {
      console.log('sub group '+acaisse_group[i].id_group+' for group '+id_group);
      return acaisse_group[i].id_group;
    }
  }
	return id_group;
}