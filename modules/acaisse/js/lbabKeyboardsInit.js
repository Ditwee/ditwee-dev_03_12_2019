$(function() { // initialisation des claviers et des évènements associés

    $('#searchKeyboard').lbabKeyboard({ //clavier azerty pour la recherche de client
        'layout': 'azerty',
        'inputable': $('#customerSearchResultsFilter input'),
        onType: function () {
            clearTimeout(scanner.customerKeyIntervalProcess);
            scanner.customerKeyIntervalProcess = setTimeout(function () {
                sendCustomerSearch();
            }, 500);
        }
    });
    
    
    
    $('#selectProductFloatNb').lbabKeyboard({ //clavier numerique pour la saisie de quantité
        'layout': 'floatNumeric',
        onReturn: function () {
            
            //si on est dans le panneau des produits/pages
            if ($('#popupNumeric').parent().is("#productList")) {            	
                var quantity = $('#selectProductFloatNb .lbabK-inputable').val();
                if (quantity != '' && quantity != 0 && ACAISSE.allActionLocked !== true) {
                    var p = _loadProductFromId($('#popupNumeric').data('idobject'));
                    var id_ticket_line = false; //@TODO : retrouver ici le id_ticket_line si existant
                    quantity = Math.round(parseFloat(quantity)*1000); // TODO : recupérer le scale en fonction du produit depuis les params.units pour faire la multiplication
					
					
					
                    addToCart(
                       	$('#popupNumeric').data('idobject'),
                        $('#popupNumeric').data('idobject2'),
                        quantity,
                        false,
                        id_ticket_line
                    );
                }
            }/*
            // si on est dans le popup pour créer une ristourne sur une ligne de panier
            else if ($('#popupNumeric').parent().is("#ticketView") && $('#popupNumeric .cart_line_reduc_amount').length>0) {
                if ($('#selectProductFloatNb .lbabK-inputable').val()=='') {
                	$('#selectProductFloatNb .lbabK-inputable').val(0);
                }
                var discount = Math.round(parseFloat($('#selectProductFloatNb .lbabK-inputable').val())*100)/100;
                var type = $('#popupNumeric .selected').data('type');//.html();
                
                var p = _loadProductFromId($('#popupNumeric').data('idobject'));
                var a;
                if ($('#popupNumeric').data('idobject')>0) {
                	a = p.attributes[$('#popupNumeric').data('idobject2')]; 
                }
                
                var unitId = params.units.from.indexOf(p.unity);
                var scale = params.units.scale[unitId];
                if (a!=null && p.unity=="grammes" && a.attribute_name!='1g') {
                	scale=1;
                }
                
                if (type=='€' && scale!=1) { // si on a un produit au poids
                	discount/=scale;
                }
                
                registerDiscountOnCartLine(discount,type,$('#popupNumeric .cart_line_reduc_amount').data('id_product'),$('#popupNumeric .cart_line_reduc_amount').data('id_product_attribute'));
            }*/
            // si on est dans le popup pour créer une ristourne globale
            else if ($('#popupNumeric').parent().is("#ticketView") && $('#popupNumeric .global_reduc_amount').length>0) {
                if ($('#selectProductFloatNb .lbabK-inputable').val()=='') {
                	$('#selectProductFloatNb .lbabK-inputable').val(0);
                }
                var discount = Math.round(parseFloat($('#selectProductFloatNb .lbabK-inputable').val())*100)/100;
                var type = $('#popupNumeric .chosseDiscountType .selected').data('type');//.html();
                var extra_datas = $('#popup_epgloal__form').serialize();
                
                registerDiscountOnCart(discount,type, extra_datas);
            }
            //si on est dans le panneau des paiements
            else if ($('#popupNumeric').parent().is("#ticketView") && $('#popupNumeric .title').html()!='') {
            	var quantity = $('#selectProductFloatNb .lbabK-inputable').val();
            	var id_ticket_line = $('#popupNumeric').data('idticketline');
            	var p = _loadProductFromId($('#popupNumeric').data('idobject'));
                
                quantity = Math.round(parseFloat(quantity)*1000);
                
                var ids = {
                    id_product : $('#popupNumeric').data('idobject'),
                    id_product_attribute : $('#popupNumeric').data('idobject2'),
                };
                
                var ticketQuantity = Math.round(parseFloat($('.cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+' .quantity').text())*1000);
                
                if(quantity != '' && quantity != ticketQuantity){
                    if (quantity > ticketQuantity) {
                        addToCart(ids.id_product, ids.id_product_attribute, quantity - ticketQuantity);
                    } else if (quantity === '0'){
                        trashLineFromCart(ids.id_product, ids.id_product_attribute);
                    } else if (quantity < ticketQuantity){
                        removeOneFromCart(ids.id_product, ids.id_product_attribute, ticketQuantity - quantity);
                    }
                }
            }
            else if ($('#popupNumeric').parent().is("#ticketView")) {            	
            	var priceWished = Math.round(parseFloat($('#selectProductFloatNb .lbabK-inputable').val())*100)/100; // prix de la subdivision du paiement au centime
            	
            	var everRegisterToPay = 0;
            	$('#popupNumeric .wishedToPayPrice').each(function() {
            		everRegisterToPay += parseFloat(this.innerHTML);
            	});
            	
            	if (priceWished+everRegisterToPay>parseFloat($('#keepToHaveToPay').html()) || priceWished<0) {
            		alert('Montant choisit impossible');
            	} else {
            		registerPaymentPart(priceWished,$('#popupNumeric').data('paymentModule'),$('#popupNumeric').data('paymentModuleLabel'));
            	}
            	
            	$('#selectProductFloatNb .lbabK-inputable').val(0);
            }
            hidePopupAndKeyboard();
            if (params.declinaisonPopup.closeAfterSelect) {
            	$('#productList .overlay').click();
            }
        }
    });
    
    $('#selectProductNb').lbabKeyboard({ //clavier numerique pour la saisie de quantité
        'layout': 'numeric',
        onReturn: function () {
        	
            //si on est dans le panneau des produits/pages
            if ($('#popupNumeric').parent().is("#productList,#searchProducts")) {
            	
                var quantity = $('#selectProductNb .lbabK-inputable').val();
                if (quantity != '' && quantity != 0 && ACAISSE.allActionLocked !== true) {
                    addToCart(
                            $('#popupNumeric').data('idobject'),
                            $('#popupNumeric').data('idobject2'),
                            quantity);
                }
            }
            //si on est dans le panneau des paiements
            else if ($('#popupNumeric').parent().is("#ticketView")) {
            	
            	
                var quantity = $('#selectProductNb .lbabK-inputable').val();
                var id_ticket_line = $('#popupNumeric').data('idticketline');
                var ids = {
                    id_product : $('#popupNumeric').data('idobject'),
                    id_product_attribute : $('#popupNumeric').data('idobject2'),
                };
                
                var ticketQuantity = $('.mode-buy .cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+' .quantity .nbr').text().trim();
                
                /*
            	console.log('YEEEEEE BISSSSS');
            	console.log([
            		$('.mode-buy .cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+''),
            		$('.mode-buy .cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+' .quantity'),
            		$('.mode-buy .cartLine_'+ids.id_product+'-'+ids.id_product_attribute+'-'+id_ticket_line+' .quantity .nbr')
            		
            	]);
                */
                if(quantity != '' && quantity != ticketQuantity && ACAISSE.allActionLocked !== true){
                    if (quantity < ticketQuantity && quantity > 0) {
                        addToCart(ids.id_product, ids.id_product_attribute, parseInt(quantity - ticketQuantity));
                    }
                    else if(quantity === '0'){
                        trashLineFromCart(ids.id_product, ids.id_product_attribute);
                    }
                    else if(quantity > ticketQuantity){
                    	if((ticketQuantity - quantity) > 1)
                    	{
                        	removeOneFromCart(ids.id_product, ids.id_product_attribute, parseInt(ticketQuantity - quantity));
                    	}
                    	else
                    	{
                    		addToCart(ids.id_product, ids.id_product_attribute, parseInt(quantity - ticketQuantity));
                    	}
                    }
                }
            }
            hidePopupAndKeyboard();
            if (params.declinaisonPopup.closeAfterSelect) {
            	$('#productList .overlay').click();
            }
        }
    });

    $('#closeKeyboard').on('click tap', function(){
        hidePopupAndKeyboard();
    });
   
   
   
});