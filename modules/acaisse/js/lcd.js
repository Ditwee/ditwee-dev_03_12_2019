var lcd_ip = params.customerScreen.lcd_ip; // on a plus besoin d'un false
var lcd_line_length = 20;
var lcd_queue = [];
var do_lcd_queue_in_action = false;

/*
 * param string text (20 char) 
 */
function lcd_write_line(text,line,align,prefix, module_obj) {
	if (params.customerScreen.mode.indexOf('lcd')!=-1) {
		lcd_queue.push([text,line,align,prefix]);
		if(!do_lcd_queue_in_action) { do_lcd_queue(); }
	}
	
	if (module_obj) {
		modules_action(module_obj);
	}
};

function do_lcd_queue() {
	if(lcd_queue.length>0)
	{
		do_lcd_queue_in_action = true;
		var next = lcd_queue.shift();
		_lcd_write_line(next[0],next[1],next[2],next[3]);
	}
	else
	{
		do_lcd_queue_in_action = false;
	}
}

function _lcd_write_line(text,line,align,prefix) {
	
	if(lcd_ip === false)
	{
		return;
	}
	
	if(typeof(align) == 'undefined')
	{
		align = 'left';
	}
	if(typeof(prefix) == 'undefined')
	{
		prefix = '';
	}
	if(typeof(text) == 'undefined')
	{
		return false;
	}
	
	text = text.sansAccent();
	
	line = typeof(line) == 'undefined' ? 0 : line;
	line = Math.max(0,Math.min(3,line));		
	
			
	if(text.length < lcd_line_length)
	{
		if(align == 'right')
		{
			text = '                                '+text;
			text = text.substring(text.length-lcd_line_length,text.length);
		}
		else
		{
			text += ' '.repeat(lcd_line_length); //on complete la ligne
			text = text.substr(0,lcd_line_length);
		}	
	}
	else if (text.length > lcd_line_length)
	{
		text = text.substr(0,lcd_line_length-3) + '...';			
	}
	
	var prefix_len = prefix.length;
	if(prefix_len>0)
	{
		if(align == 'right')
		{
			text = prefix + text.substring(prefix_len,lcd_line_length);
		}
	}
	
	$.ajax({
		url:'http://'+lcd_ip+'/?lcdpos00'+line,
		method:'GET',
		crossDomain:true,			
	}).done(function(msg){ 	
		console.log('ecriture de "'+text+'" à la ligne n°'+line);
		$.ajax({
			url:'http://'+lcd_ip+'/?lcdwrite'+text+'|',
			method:'GET',
			crossDomain:true,		
		}).done(do_lcd_queue);
	});
}