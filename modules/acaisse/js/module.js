$(function(){
	
	$('#specialsContainer ul.tab-bar').on('tap click','li[data-module-name]',function(){
		console.log($(this).data('moduleName'));
		
		//ajax
		$.ajax({
	        type: 'post',
	        data: {
	          'action': 'GetSpecialModuleView',
	          'module_name': $(this).data('moduleName'),
	          'id_pointshop': ACAISSE.id_pointshop,
	          'id_customer': ACAISSE.customer.id,
	          'id_group': ACAISSE.customer.id_default_group,
	          'id_force_group': ACAISSE.id_force_group,
	          'ajaxRequestID': ++ACAISSE.ajaxCount,
	        },
	        dataType: 'json',
	    }).done(function (response) {
	    	console.log('response')
	        if (response.success != false) {
	        	$('#specialsContainer .tab-content li').removeClass('open');	        	
	        	$('#specialsContainer .tab-content #tab-specials-modules-container').html(response.response.html).addClass('open');
	        }
	        else
	        {
	        	
	        }
	    }).fail(function () { 
	        alert('Erreur o00079 : impossible de communiquer avec le serveur');
	        
	      $('#details-paiements tr.loader').remove();
	      $('#multi-payments-popup').prop('disabled',false);
	    });
		
		
	});
	
});
