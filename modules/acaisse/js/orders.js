
function loadOrders(nb_order, filter = {}) {
	$('#pageUser3').html('<div><i class="fa fa-spinner fa-spin"></i></div>');
	$.ajax({
		data : {
			'action' : 'LoadCustomerOrders',
			'id_customer' : ACAISSE.customer.id,
			'from_nb_order' : nb_order,
			'id_pointshop' : ACAISSE.id_pointshop,
			'filter' : filter,
		},
		type : 'post',
		dataType : 'json',
	}).done(function(response) {
		if (response.success != false) {
			var response = response.response;
			var orders = response.orders;
			var gift_cards = response.gift_cards;
			var html = '<table data-nb_order="0" cellspacing="0">';
			var filter = response.filter;
			var pointshop_selected = '';
			
			//html +='<div class="col-xs-6">'+acaisse_pointshops.pointshops[ACAISSE.id_pointshop].name+'</div>';

			/* MISE EN PLACE D'UN FILTRE */
			html +='<div id="order_filter">';
			html +='<div class="filter col-xs-2"><input placeholder="N° de commande" type="number" value="' +filter.id_order+ '" id="filter-id_order"></div>';
			html +='<div class="filter col-xs-2"><input placeholder="N° de ticket" type="number" value="' +filter.id_ticket+ '" id="filter-id_ticket"></div>';
			html +='<div class="filter col-xs-2"><input placeholder="Date de commande" type="date" value="' +filter.date+ '" id="filter-date"></div>';
			
			html +='<div class="filter col-xs-4">';
			if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.see_all_orders == false)
			{
				filter.id_pointshop=='0'?pointshop_selected='selected="selected"':pointshop_selected='';
				html +='<select id="filter-id_pointshop">';
					html +='<option value="-1">Tous les points de ventes</option>';
					html +='<option '+pointshop_selected+' value="0">En ligne</option>';
					pointshop_selected = '';
					for (var p in acaisse_pointshops.pointshops) {
						//console.log([parseInt(filter.id_pointshop),parseInt(acaisse_pointshops.pointshops[p].datas.id)]);
						parseInt(filter.id_pointshop)==parseInt(acaisse_pointshops.pointshops[p].datas.id)?pointshop_selected='selected="selected"':pointshop_selected='';
						html +='<option '+pointshop_selected+'" value="'+acaisse_pointshops.pointshops[p].datas.id+'">'+acaisse_pointshops.pointshops[p].name+'</option>';
					}
				html +='</select>';
				
			}
			else
			{
				html +='<input type="text" value="'+acaisse_pointshops.pointshops[ACAISSE.id_pointshop].name+'" readonly />';
			}
			html +='</div>';
			
			html +='<div class="col-xs-2 filter">';
				html +='<div class="button validate">FILTRER</div>';
				html +='<div class="button cancel">EFFACER</div>';
			html +='<div class="clearfix"></div>';

			html += '<thead><tr><th class="commande">N° de CMD</th><th>N° de ticket</th><th>Point de vente</th><th>Date</th><th>Etat</th><th>Montant total</th><th>Montant payé</th><th>Actions</th></tr></thead>';
			var buffer_totals_by_id_ticket = [];
			
			if(orders.length > 0)
			{
				/*
				for (var i = 0; i < orders.length; i++) {
					if (buffer_totals_by_id_ticket[orders[i].id_ticket] == undefined) {
						buffer_totals_by_id_ticket[orders[i].id_ticket] = {
							order : orders[i],
							total_paid : parseFloat(orders[i].total_paid),
							total_paid_real : parseFloat(orders[i].total_paid_real)
						};
					} else {
						//il y avait deja une commande pour ce ticket
						//on mets à jour les totaux
						buffer_totals_by_id_ticket[orders[i].id_ticket] = {
							order : orders[i],
							total_paid : buffer_totals_by_id_ticket[orders[i].id_ticket].total_paid + parseFloat(orders[i].total_paid),
							total_paid_real : buffer_totals_by_id_ticket[orders[i].id_ticket].total_paid_real + parseFloat(orders[i].total_paid_real)
						};
					}
				}*/
				
				for (var id = 0; id < orders.length; id++) {
					//console.log(orders[i]);
					var i = id;
					
					if (true/*buffer_totals_by_id_ticket[orders[i].id_ticket] != undefined*/) {
						if (true/*buffer_totals_by_id_ticket[orders[i].id_ticket].order.id_order == orders[id].id_order*/) {
							//orders[id].total_paid = buffer_totals_by_id_ticket[orders[i].id_ticket].total_paid;
							//orders[id].total_paid_real = buffer_totals_by_id_ticket[orders[i].id_ticket].total_paid_real;
							html += '<tr data-id_order='+orders[id].id_order+'>';
							html += '<td>' + orders[id].id_order + '</td>';
							if (acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
								html += '<td>' + orders[id].id_ticket + '</td>';
							}
							else
							{
								html += '<td>--</td>';
							}
	
							if (acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
								html += '<td><span class="pointshop_name">' + acaisse_pointshops.pointshops[orders[id].id_pointshop].name + '</span></td>';
							} else {
								html += '<td><span class="pointshop_name">En ligne</span></td>';
							}
							html += '<td><span class="date">' + Date.toString(orders[id].date_add) + '</span></td>';
							html += '<td><span class="state">' + orders[id].order_state + '</span></td>';
							html += '<td><span class="paid ' + ((parseFloat(formatPrice(orders[id].total_paid)) > parseFloat(formatPrice(orders[id].total_paid_real))) ? 'notAllPaid' : '') + ' ' + ((parseFloat(formatPrice(orders[id].total_paid)) < parseFloat(formatPrice(orders[id].total_paid_real))) ? 'tooMuch' : '') + '">' + formatPrice(orders[id].total_paid) + '€</span></td>';
							html += '<td><span class="paid_real ' + ((parseFloat(formatPrice(orders[id].total_paid)) > parseFloat(formatPrice(orders[id].total_paid_real))) ? 'notAllPaid' : '') + ' ' + ((parseFloat(formatPrice(orders[id].total_paid)) < parseFloat(formatPrice(orders[id].total_paid_real))) ? 'tooMuch' : '') + '">' + formatPrice(orders[id].total_paid_real) + '€</span></td>';
							html += '<td class="actions">';
							if (parseFloat(orders[id].total_paid_real) < parseFloat(orders[id].total_paid) && response.id_order_state_cancelled != orders[id].id_order_state && acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
								html += '<button onclick="payRestOfOrder(' + orders[id].id_order + ');"><i class="fa fa-money" aria-hidden="true"></i> <span class="btLabel">PAYER</span></button>';
							} else if (parseFloat(orders[id].total_paid_real) > parseFloat(orders[id].total_paid) && response.id_order_state_cancelled != orders[id].id_order_state) {
								/**@todo : Se démerder pour degager le surplut et donner une reduc ?*/
								//html += '<button disabled="disabled" style="opacity:0.2;"><i class="fa fa-undo"></i> <span class="btLabel">AVOIR</span></button>';
							}
							
							html += PRESTATILL_CAISSE.executeHook('displayPrestatillCustomerOrdersLine',{'id_order':orders[id].id_order});
							
							if (acaisse_pointshops.pointshops[orders[id].id_pointshop] != undefined) {
								html += '<button class="seeTicket" data-id="' + orders[id].id_ticket + '"><i class="fa fa-ticket"></i> <span class="btLabel">TICKET</span></button>';
							} else {
								html += '<button disabled="disabled" style="opacity:0.2" data-id="0"><i class="fa fa-ticket"></i> <span class="btLabel">TICKET</span></button>';
							}
							
							/*
							 if (params.orders.acaisse_invoice_is_active == 1 && orders[id].valid == 1 && orders[id].invoice_number > 0) {
							 html += '<button  class="seeInvoice" data-id="' + orders[id].id_order + '"><i class="fa fa-file-text"></i> <span class="btLabel">FACTURE</span></button>';
							 }
							 else
							 {
							 html += '<button disabled="disabled" style="opacity:0.2"  class="seeInvoice" data-id="0"><i class="fa fa-file-text"></i> <span class="btLabel">FACTURE</span></button>';
							 }*/
							html += '<button  class="openOrder" data-id="' + orders[id].id_order + '"><i class="fa fa-eye"></i> <span class="btLabel">VOIR</span></button>';
							html += '</td></tr>';
						}
					}
				}
			}
			else
			{			
				html +='<tr><td colspan="8" class="no_order">Aucun résultat</td></tr>';
			}
			html += '</table>';
			$('#pageUser3').html(html);
			$('#userEditContainer li[data-page-id="pageUser3"] .pastille').html(response.nb_total_orders);
			PRESTATILL_CAISSE.loader.end();
		} else {
			alert('Erreur k_00011 : impossible d\'obtenir la liste des commandes du client. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
		}
	}).fail(function() {
		alert('Erreur k00011 : impossible d\'obtenir la liste des commandes du client. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	});
}

function payRestOfOrder(id_order) {
	$.ajax({
		data : {
			'action' : 'ReloadOrderToFinishPayment',
			'id_order' : id_order,
		},
		type : 'post',
		dataType : 'json',
	}).done(function(response) {
		if (response.success) {
			_switchToTicket(response.response.id_ticket, function() {
				_gotoPaymentPanel();
			}, false);
		} else {
			alert('Erreur, impossible de recharger la commande, si l\'erreur persiste veuillez contacter un administrateur');
		}
	});
}
