$(function(){	
	$('body').on('click tap','#launchRegisterPayment',function(){
		PRESTATILL_CAISSE.payment.registerPayment();		
	});
	
	$('body').on('click tap','#launchRegisterRefund',function(){
		PRESTATILL_CAISSE.payment.registerRefund();		
	});
	
	
	/**
	 * Bouton d'annulation de la phase de paiement 
	 */
	$('body').on('click tap', '#paymentView .cancel.button', function(elt) {
		if ($(this).hasClass('disabled')) {
			alert('Vous avez déjà enregistré au moins un encaissement, vous devez soit solder la vente, soit la clôturer sans la solder en cliquant sur Paiement Partiel.');
		} else {
			ACAISSE.cartModificationLocked = false;			
			_gotoTicketView();
		}
	});

	$('body').on('click tap', '#paymentView .partial-validation', function(elt) {
		if ($(this).hasClass('disabled')) {
			alert('Votre commande ne correspond plus à un paiement partiel (Code 000611)');
		} else {
			if (confirm('Confirmez-vous la finalisation de cette commande non soldée?')) {
				getTicketCaisse();
			}
		}
	});
	
	$('body').on('click tap', '#paymentView .no-payment', function(elt) {
		if ($(this).hasClass('disabled')) {
			alert('Impossible de valider cette vente sans paiement (Code L000300)');
		} else {
			if (confirm('Confirmez-vous la finalisation de cette vente sans enregistrer d\'encaissement ?')) {
				PRESTATILL_CAISSE.payment.registerPayment();
				getTicketCaisse();
			}
		}
	});

	$('body').on('click tap', '#paymentView .validation.button', function(elt) {
		if ($(this).hasClass('disabled')) {
			alert('Veuillez d\'abord enregistrer un ou plusieurs paiements permettant de solder la totalité la commande (Code 000612)');
		} else {
			getTicketCaisse();
		}
	});
	
	
	/******** vieux script **********/
	
	/**
	 * enregistrer le paiement total de la commande
	 */
	function _DEPRECATED_registerFullPayement() {
		
		if (ACAISSE.payment_parts.length>0) {
			ACAISSE.cartModificationLocked = false;
			_startNewPreorder();
			return true;
		}
		
		
		ACAISSE.allActionLocked = true;
		$('#registerPayment').prepend('<i class="fa fa-spinner fa-spin"></li>');
		
		$.ajax({
			type: 'post',
			data: {
				'action': 'RegisterFullPaiement',
				'id_ticket': ACAISSE.ticket.id,
				'id_pointshop': ACAISSE.id_pointshop,
				'ps_id_cart': ACAISSE.ticket.ps_id_cart,
				'id_customer': ACAISSE.customer.id,
				'id_group': ACAISSE.customer.id_default_group,
				'id_force_group': ACAISSE.id_force_group, //5
				'ajaxRequestID': ++ACAISSE.ajaxCount,
				'payment_module_name': ACAISSE.payment_parts.length>0?ACAISSE.payment_parts[0].payment_module_name:$('#' + ACAISSE.choosenPaymentMode).data('module-name'),
				'id_order_state': ACAISSE.payment_parts.length>0?13:$('#' + ACAISSE.choosenPaymentMode).data('id-order-state'),
				'cash_received': ACAISSE.choosenPaymentMode == 'payByCash' ? $('#cashReceived').html() : 0,
				'payment_parts' : ACAISSE.payment_parts,
				'payment_label' : $('.paymentButton.selected .label').html()
			},
			dataType: 'json',
		}).done(function (response) {
			if (response.success == true)
			{
				if(ACAISSE.mode_debogue === true)
				{console.log(['print response check', response]);}
				response = response.response;
				ACAISSE.ticket.lines = response.ticket.lines;
				$('#registerPayment i.fa-spinner').remove();
				//ticket suivant
				
				printTicket(response.ticket_caisse);
				
				ACAISSE.cartModificationLocked = false;
				
				_startNewPreorder();
				
				ACAISSE.lastCA = response.lastCa;
				_refreshLastCa();
			}
			else
			{
				alert('Erreur 00121 : ' + response.errorMsg);
			}
		}).fail(function () {
			alert('Erreur 00122 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
		});
	
	}
	
	
    //validation d'un ticket pour conversion vers commande prestashop
    $('.validTicket').on('click tap', function () {
        if ($(this).hasClass('disabled') || $(this).parents('.disabled').length>0) {
        	sendNotif('greenFont','Veuillez ouvrir la caisse afin de réaliser une vente.');
        	return false;
        }   
        
        _validateCurrentTicket();
        _gotoPaymentPanel();    
        $('#paymentView').find('.prestatill_payment_interface .numeric_keyboard').show();         
    });


	/**
	 * Méthode initiant la transformation d'un ticket en panier Prestashop (cart)
	 */
	function _validateCurrentTicket(open_payment_popup) {
		
		if(open_payment_popup == undefined)
		{
			open_payment_popup = true;
		}
		
		//si le ticket est vide rien à faire
		var error = false;
		if (ACAISSE.ticket.lines.length == 0)
		{
			alert('Veuillez d\'abord sélectionner au moins un produit');
			error = true;
		}
		else if (acaisse_profilesNoCheckinIDs.indexOf('' + ACAISSE.customer.id) != -1)
		{
			alert('Veuillez d\'abord sélectionner un client.');
			error = true;
		}
		else if (!error)//tout est bon, on va demander au serveur la conversion du ticket en véritable commande Prestashop
		{			
			$.ajax({
				type: 'post',
				data: {
					'action': 'ValidateTicket',
					'id_ticket': ACAISSE.ticket.id,
					'id_customer': ACAISSE.customer.id,
					'id_group': ACAISSE.customer.id_default_group, //3
					'id_force_group': ACAISSE.id_force_group, //5
					'ajaxRequestID': ++ACAISSE.ajaxCount,
					'open_payment_popup' : open_payment_popup,
				},
				dataType: 'json',
			}).done(function (response) {
				if (response.success == true) {
					request = response.request;
					response = response.response;
					
					if (response.id_not_found.length>0) {
						var p = _loadProductFromId(response.id_not_found[0]);
						alert("ATTENTION !! Le produit #"+p.id_product+" : "+p.product_name+" n'as pas été correctement ajouté au ticket. Il se peut que la quantité minimal de commande dans la fiche produit soit inférieur à la quantité actuelle du ticket.");
						return false;
					}
					
					//console.log(['validateTicket','response',response]);
					ACAISSE.cart_tot = response.cart_tot;
					ACAISSE.cart_tot_ht = response.cart_tot_ht;
					ACAISSE.cart_tot_ttc = response.cart_tot_ttc;
					
					//console.log('ACAISSE.cart_tot : ' + ACAISSE.cart_tot);
					//alert(ACAISSE.ticket);
					ACAISSE.ticket = response.ticket;
					ACAISSE.cartModificationLocked = true; //on bloque les action d'ajout de produits et de modification des quantités
					
					refreshPrices(response.cart_products);
					
					var module_obj = {action:'tot',tot:formatPrice(response.cart_tot_ttc),id_ticket:response.ticket.ticket_num};
					modules_action(module_obj);
					
					if(request.open_payment_popup !== false)
					{
						_gotoPaymentPanel();
					}
				} else {
					alert('Erreur 00177C : ' + response.errorMsg);
				}
			}).fail(function () {
				alert('Erreur 00178C : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.')
			});
		}
	}


	

	/******************************************************************************************************/
	/******************************************************************************************************/
	/******************************************************************************************************/
	//LA SUITE VA ETRE DEPRECIEE A LA SORTIE DE SIREEN
    $('#registerPayment').on('click tap', function () {
        if ($(this).hasClass('disabled'))
        {
            alert('Veuillez d\'abord sélectionner un moyen de paiement');
        }
        else
        {
            if (ACAISSE.allActionLocked !== true)
            {
            	if(ACAISSE.mode_debogue === true) {console.log(ACAISSE.ticket.ps_id_cart);}
            	//if(prompt('message de deboguage, cliquez sur ok/oui pour valider'))
            	//{
            		_registerFullPayement();
            		//on cherche un paramètre pour savoir s'i oui ou non le client veut un ticket pour lui'il faut un deuxieme ticket
            	//}
            }
        }
    });
    
});