function showPopup(inContainer, isFloatNumeric, typeOfContent){
	_setWidthAndHeight();
	
	$(inContainer).append('<div class="search-container"><div class="search-bg"></div><div class="overlayNumeric"></div></div>');
	if (isFloatNumeric) {
		// on display none le numeric et display block le floatNumeric
		$('#selectProductNb').css('display','none');
		$('#selectProductFloatNb').css('display','inline-block');
	} else {
		$('#selectProductNb').css('display','inline-block');
		$('#selectProductFloatNb').css('display','none');
	}
	
	// On ouvre le tpl en fonction du type de contenu attendu
	if(typeOfContent != 'undefined')
	{
		$('#popupNumeric').attr('data-content',typeOfContent);
		typeOfContent = 'popupNumeric';
	}
	
	if(typeof inContainer !== 'undefined'){	
		$('#'+typeOfContent).appendTo($(inContainer));
	}
	$('#'+typeOfContent).css('display','table');
}





function hidePopup(){
	$('#middleContainer .popup,#middleContainer .popup-overlay').css('display','none');
	$('.overlayNumeric, .search-bg, .search-container').remove();
}

function showEmptyPopup(inContainer,html){
	_setWidthAndHeight();
	
	$(inContainer).append('<div class="search-container"><div class="search-bg"></div><div class="overlayNumeric"></div></div>');
	
	$('#emptyPopup .inner-container .body').html(html);
	
	if(typeof inContainer !== 'undefined'){	
		$('#emptyPopup').prependTo($('body'));
	}
	
	$('.popupActions').hide();
	$('#emptyPopup').css('display','block');
}


function hidePopupAndKeyboard(){
	hidePopup();
	//$('#popupNumeric').empty();
	// On vide le contenu de la popupNumeric
	$('#productInfos .title').html('Chargement en cours');
	$('#features ul').html('');
	$('#details .descShort, #details .desc').html('');
	
	// On repasse sur l'onglet "Fiche produit"
	$('#popupProductMenu #firstShow').trigger('click');
	
	$('#selectProductNb .lbabK-inputable').val('');
	$('#selectProductFloatNb .lbabK-inputable').val('');
	
	$('.overlayNumeric, .search-bg, .search-container').remove();
}

// ouvre un popoup avec le declinaison de produit ayant l'id idObject
function showPopupDeclinaison(idObject){
	if ($('.switcher').hasClass('show-ticket')) {
        $('<div class="overlay"></div>').appendTo($("#productList"));
    } else {
        $('<div class="overlay"></div>').appendTo($("#ticketView"));
    }
   
    
    var html = '<div class="contentOverlay"><table>';
   	html += '<tr class="closeOverlay"><td colspan="4">Fermer</td></tr>';
    
    var attributes = _loadProductFromId(idObject).attributes;
    
    var productBuffer = []; //liste des produits sur la page, nécessitant la maj des stocks
    
    var real_stock_aff, preorder_stock_aff, estimateStock_aff;
    var cache_stock = products_stock_cache[idObject], cach_stock2;
    
    var i=1;
    
    for (index in attributes) { // faire deux fois la boucle pour verifier la scrollabilité
    	html += '<tr data-idobject="'+idObject+'" data-idobject2="'+index+'">';
    	html += (acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0'?'<td>'+(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.use_image!='0'?'<img src="' + (attributes[index].image != undefined && attributes[index].image != '' ? attributes[index].image : '../modules/acaisse/img/noPhoto.jpg') + '" width="50" height="50"/>':'')+'</td>':'');
    	
    	//console.log(parseFloat(_loadProductFromId(idObject).base_price*(1+(_loadProductFromId(idObject).tax_rate/100))).toFixed(2) +' - ' + parseFloat(attributes[index].prices[ACAISSE.id_force_group]).toFixed(2));
    	
	    var display_base_price = 'none';
	
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_original_price == 1
		&& parseFloat(_loadProductFromId(idObject).base_price*(1+(_loadProductFromId(idObject).tax_rate/100))).toFixed(2) > parseFloat(attributes[index].prices[ACAISSE.id_force_group].toFixed(2)))
		{
			display_base_price = 'inline-block';
		}	
    	
    	html += '<td>'+ _loadProductFromId(idObject).product_name
    				+ '<br /><em>Réf : ' + attributes[index].ref +'</em>'
    				+ '<br /><strong>' + attributes[index].attribute_name +'</strong></td>'
    	
		if (cache_stock == undefined) {
			real_stock_aff = 'XX';
			preorder_stock_aff = 'XX';
			estimateStock_aff = 'XX';
		} else {
			cach_stock2 = cache_stock[index];
			if (cach_stock2 == undefined) {
				real_stock_aff = 'XX';
				preorder_stock_aff = 'XX';
				estimateStock_aff = 'XX';
			} else {
				real_stock_aff = cach_stock2.real_stock_aff;
				preorder_stock_aff = cach_stock2.preorder_stock_aff;
				estimateStock_aff = '<span class="no_internet">'+cach_stock2.estimateStock_aff+'</span>';
			}
		}
		html += '<td><span class="priceBaseAttr" style="display:'+display_base_price+'">'+getProductBasePrice(idObject)+'</span><span class="priceAttr">'+getProductPrice(idObject, index)+'</span></td>';
    	
    	html += '<td><span class="stock_available">'+estimateStock_aff+'</span><span class="stock_real">'+real_stock_aff+'</span><span class="stock_inqueue">-'+preorder_stock_aff+'</span></td>';
    	html += '</tr>';
    	
    	productBuffer.push({
            id_product: idObject,
            id_product_attribute: index,
            row: i,
            col: 0
        });
        
        i++;
    }
    
    html += '</table></div>';
    
    if ($('.switcher').hasClass('show-ticket')) {
    	$(html).appendTo($('#productList .content'));
    } else
    {
    	$(html).appendTo($('#ticketView .content'));
    }
    
    _refreshProductPageStock2(productBuffer);
    ACAISSE.pageProductBuffer2 = productBuffer; // on le stock pour des maj de temps à autres (on le fais dans caisse.js donc je le fais ici, il faudra demander à olivier quand on doit le mettre a jour mais vu que ça se fais à chaque ouverture de popup je ne suis pas sur que ce soit bien util)
}



/**
 * common popup action 
 */
$(function(){
	
	$('body').on('click tap','.closePopup',function(e) {
		e.preventDefault();
		//on vide l'info de montant de la popup
		$('#popupEditCartLine .lbabK-inputable').val('');
		$('.popup-overlay').hide();
		hidePopup();
	});
	
	$('body').on('click tap','#popupProductMenu button',function(){
		$('#popupProductMenu button').removeClass('open').each(function(){
			$($(this).data('to')).css('display','none');
		});
		
		$(this).addClass('open');
		$($(this).data('to')).css('display','block');
	});
	
});



/************************************************************************************
 * POPUP EDITION LIGNE du ticket (discount line, edit details, ...) 
 ************************************************************************************/

$(function(){
	
	var refreshLiveDiscountPreview = function(value,$elt)
	{
		var type = $('#popupEditCartLine .selected').data('type');
		var original_margin_rate = parseFloat($('#popupEditCartLine .original_margin_rate').html());
		var margin_rate = 0;
		
		
			
    	if(
    		value === '' || value == undefined
    		|| $('#popupEditCartLine .selected').length == 0
    	)
    	{
    		$('#popupEditCartLine .product_price').removeClass('price_with_discount');
    		$('#popupEditCartLine .product_price_reduction').html('');
    		$('#popupEditCartLine .product_final_price').html('aucune remise');
    		$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+original_margin_rate+'%</span>');
    		//console.log("value = 0");
    	}
    	else
    	{
    		$('#popupEditCartLine .product_price').addClass('price_with_discount');
    		
    		var price = parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-$('#whole_sale .whole_sale_price_ht').html());
    		var price_origin = parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val());
    		var discount = 0;        		
    		var discount_price = price;
    		
    		//console.log(parseFloat(value).toFixed(5)+' > '+parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val()).toFixed(5));
    		
    		if(type == '€')
    		{
    			discount = value;
    			discount_price -= discount;
    			
    			discount_price = Math.round(parseFloat(discount_price*100)/100);
    			$('#popupEditCartLine .product_price_reduction')
    				.html('-'+value+type);
				$('#popupEditCartLine .product_final_price')
					.html('soit '+parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val()-discount).toFixed(2)+'€ TTC');
					
				// On calcule la marge en direct
				margin_rate = parseFloat((($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-value)-$('#whole_sale .whole_sale_price_ht').html())/($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-value)*100).toFixed(2);
				$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+margin_rate+'%</span>');		
    			
    			if(value > price)
	    		{
	    			$('#popupEditCartLine .margin_rate').html('<span class="alert-danger">Vente à perte !</span>');	
	    		}
	    		
	    		if(value > price_origin)
	    		{
	    			console.log("what ?");
	    			$('#popupEditCartLine .product_price_reduction').html('');
	    			$('#popupEditCartLine .product_final_price').html('<span class="alert-danger">Prix de vente < 0€</span>');
	    		}
    		
    		}
    		else if(type == '%')
    		{
    			discount = price * value / 100;
    			discount_price -= discount;
    			discount_price = Math.round(parseFloat(discount_price*100))/100;
    			$('#popupEditCartLine .product_price_reduction')
    				.html('-'+value+type);
				$('#popupEditCartLine .product_final_price')
					.html('soit '+parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val()-$('#popupEditCartLine #popup_epline__product_price_tax_incl').val()*value/100).toFixed(2)+'€ TTC');
					
				// On calcule la marge en direct
				margin_rate = parseFloat((($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()*value/100))-$('#whole_sale .whole_sale_price_ht').html())/($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()-$('#popupEditCartLine #popup_epline__product_price_tax_excl').val()*value/100)*100).toFixed(2);
				$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+margin_rate+'%</span>');
    			
    			//console.log($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()*value/100);
    			
    			if(parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_excl').val()*value/100) > price)
	    		{
	    			$('#popupEditCartLine .margin_rate').html('<span class="alert-danger">Vente à perte !</span>');	
	    		}
	    		
	    		if(parseFloat($('#popupEditCartLine #popup_epline__product_price_tax_incl').val()*value/100) > price_origin)
	    		{
	    			$('#popupEditCartLine .product_price_reduction').html('');
	    			$('#popupEditCartLine .product_final_price').html('<span class="alert-danger">Prix de vente < 0€</span>');
	    		}
    		
    		}
    		else if(type == 'gift')
    		{
    			discount = price * value / 100;
    			discount_price -= discount;
    			
    			discount_price = Math.round(parseInt(discount_price*100))/100;

    			
    			$('#popupEditCartLine .product_price_reduction')
    				.html('');
				$('#popupEditCartLine .product_final_price')
					.html('Dont '+value+' offert');
					
				// marge = ???
				margin_rate = parseFloat(0).toFixed(2);
				$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+margin_rate+'%</span>');	
    			
    			if(value > price)
	    		{
	    			$('#popupEditCartLine .margin_rate').html('<span class="alert-danger">Vente à perte !</span>');	
	    		}
	    		
	    		if(value > $('#popupEditCartLine #popup_epline__product_price_tax_incl').val())
	    		{
	    			$('#popupEditCartLine .product_price_reduction').html('');
	    			$('#popupEditCartLine .product_final_price').html('<span class="alert-danger">Prix de vente < 0€</span>');
	    		}
	    		
	    		
    		
    		}
    		var discount_price = price - discount
    		
	    	// On rafraichit la marge si le prix d'achat est nul
			if(parseFloat($('#whole_sale .whole_sale_price_ht').html()).toFixed(0) == 0)
			{
				$('#popupEditCartLine .margin_rate').html('<span class="alert-danger">Prix d\'achat non renseigné</span>');
			}
			}	
	};
	
	$('body').on('click tap','#popupEditCartLine .chosseDiscountType>div',function(){
		
		$plugin = $('#popupEditCartLine .selectProductFloatNb').data('lbabKeyboard');
		
		if($(this).hasClass('reductAmountInEuro'))
		{
			//$plugin.setMax($('#popupEditCartLine').data('price'));
		}
		else if($(this).hasClass('reductAmountInPercent'))
		{
			//$plugin.setMax(100);
		}
		
		var inputValue = $('#popupEditCartLine .lbabK-inputable').val();
		refreshLiveDiscountPreview(inputValue);
	});
	
	$('body').on('click tap','.discountCartLine',function() {
    	
    	if (!ACAISSE.cartModificationLocked) {    		
    		//$('.button.validTicket').click();
    		_validateCurrentTicket(false);
    	}
    	
    	
    	var id_product = $(this).parent().data('id_product');
    	var id_product_attribute = id_attribute = $(this).parent().data('id_product_attribute');
    	var id_ticket_line = $(this).parent().data('idticketline');
    	
		var cartLineInfos = ".cartLine_"+id_product+"-"+id_product_attribute+"-"+id_ticket_line;
		
		
		var reduc = $(this).parent().find('.pastille').html();
		var type_reduc = '€';
		if (reduc!=undefined) {
			type_reduc = reduc[reduc.length-1];
		} else {
			reduc='-0€'; 
		}		
		
		var inputValue = Math.abs(parseFloat(reduc));
		
		// On affiche uniquement les informations de marge et prix d'achat si souhaité
		if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.display_wholesale_price == 2)
		{
			$('#popupEditCartLine #whole_sale').show();
			$('#popupEditCartLine #whole_sale_box').hide();
		} else {
			$('#popupEditCartLine #whole_sale').hide();
			$('#popupEditCartLine #whole_sale_box').show();
		}
		
		// On récupère les informations du produit
		//_getProductInfos($(this).parent().data('id_product'));
		showPopupEditCartLine(
			id_product,
			id_product_attribute,
			reduc,
			type_reduc,
			inputValue,
			//cartLineInfos,
			id_ticket_line
		);//$('#ticketView'), true, 'popupEditCartLine');
	});
	
	//ajout du clavier de saisis remise
	$('#popupEditCartLine .selectProductFloatNb').lbabKeyboard({ //clavier numerique pour la saisie de quantité
        'layout': 'floatNumeric',
        //'min' : 0,
        //'max' : 12,
        onType: function(value,$elt) {        	
        	refreshLiveDiscountPreview(value,$elt);        	
        },
        onReturn: function (value) {        	
           // if ($('#popupEditCartLine .lbabK-inputable').val()=='') {
            	$('#popupEditCartLine .lbabK-inputable').val('');
          //  }
            var discount = Math.round(parseFloat(value)*100)/100; 
            var type = $('#popupEditCartLine .selected').data('type');
            
            var p = _loadProductFromId($('#popupEditCartLine').data('idobject'));
            var a;
            if ($('#popupEditCartLine').data('idobject')>0) {
            	a = p.attributes[$('#popupEditCartLine').data('idobject2')]; 
            }
            var unitId = params.units.from.indexOf(p.unity);
            var scale = params.units.scale[unitId];
            var opwtexcl = $('#popup_epline__product_price_tax_excl').val();
            var opwtincl = $('#popup_epline__product_price_tax_incl').val();
            
            if (type=='€' && scale!=1) { // si on a un produit au poids
            	discount/=scale;
            }

			$('.popup-overlay').css('display','none');	
			
			//console.log(p.unity);
			
			if(p.unity != '')
			{
				opwtexcl = parseFloat($('#popup_epline__product_price_tax_excl').val()/scale);	
				opwtincl = parseFloat($('#popup_epline__product_price_tax_incl').val()/scale);
			}	
			//console.log(parseFloat($('#popup_epline__product_price_tax_incl').val()).toFixed(5)+'<->'+parseFloat($('#popup_epline__original_tax_incl').html()).toFixed(5));
            registerDiscountOnCartLine(
            	discount,
            	type,
            	$('#popupEditCartLine').data('idobject'),
            	$('#popupEditCartLine').data('idobject2'),
            	$('#popupEditCartLine').data('id_ticket_line'),
            	{
            		product_name : $('#popup_epline__product_name').val(),
            		original_price_with_tax : parseFloat(opwtincl),
            		original_price_without_tax : parseFloat(opwtexcl),
            		original_price_is_forced_by_user : parseFloat($('#popup_epline__product_price_tax_incl').val()).toFixed(5) !== parseFloat($('#popup_epline__original_tax_incl').html()).toFixed(5) ? 1:0,
            	}
        	);
        }
    });
    
    
	$('#popupEditCartLine').on('click tap','#whole_sale_box',function(){
		$('#popupEditCartLine #whole_sale').slideToggle();
		$(this).find('i').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
	});
	
	
	var showPopupEditCartLine = function(id_product,id_product_attribute,reduc,type_reduc,inputValue,id_ticket_line)
	{
		console.log('showPopupEditCartLine('+id_product+','+id_product_attribute+','+reduc+','+type_reduc+','+inputValue+','+id_ticket_line+')');
		//console.log($(cart_line_id+' .img img').attr('src'));
		
		//on va demander les informations détaillées sur CE produit au serveur
		$.ajax({
	        data: {
	            'action': 'GetProductDetailsFromServeur',
	            'id_product': id_product,
	            'id_product_attribute':id_product_attribute,
	            'id_ticket_line' : id_ticket_line,
	            'id_customer': ACAISSE.customer.id,
	            'id_force_group' : ACAISSE.id_force_group
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success != false) {
	        	
				var request = response.request;
	        	var response = response.response;
	        	var cart_line_id = '.cartLine_'+request.id_product+'-'+request.id_product_attribute+'-'+request.id_ticket_line;
	        	
	        	var p = _loadProductFromId(request.id_product);
	        	var unitId = params.units.from.indexOf(p.unity);
            	var scale = params.units.scale[unitId];
            	var unitTo = params.units.to[unitId];
            	
            	if(p.unity != '')
	        	{
	        		$('#popup_epline__manual_price_box .unit').html('€'+unitTo);
	        	}
            	
	        	//console.log(unitTo);
	        	//pas de prix forcé par l'utilisateur
	        	if(response.line.original_price_is_forced_by_user == 0)
	        	{	        		
		        	$('#popup_epline__product_price_tax_incl').val(parseFloat(response.product.original_price_tax_incl).toFixed(5));
		        	$('#popup_epline__product_price_tax_excl').val(parseFloat(response.product.original_price_tax_excl).toFixed(5));
		        }
	        	else
	        	{
		        	//mise à jour des informations
		        	if(p.unity != '')
		        	{
		        		response.line.original_price_with_tax = parseFloat(response.line.original_price_with_tax*scale).toFixed(5);
		        		response.line.original_price_without_tax = parseFloat(response.line.original_price_without_tax*scale).toFixed(5);
		        	}
		        	
		        	$('#popup_epline__product_price_tax_incl').val(response.line.original_price_with_tax);
		        	$('#popup_epline__product_price_tax_excl').val(response.line.original_price_without_tax);
		        	
	        	}
	        	if(p.is_generic_product == true)
	        	{
	        		$('#popup_epline__manual_price_box, .popup_epline__manual_price_box').show();
	        	}
	        	else
	        	{
	        		$('#popup_epline__manual_price_box, .popup_epline__manual_price_box').hide();
	        	}
	        	
	        	//tarifs original du produit générique
	        	$('#popup_epline__original_tax_incl').html(response.product.original_price_tax_incl);
	        	$('#popup_epline__original_tax_excl').html(response.product.original_price_tax_excl);
	        	
	        	$('#popup_epline__product_name').val(response.line.label);
	        	$('#popupEditCartLine .product_name').html(response.product.product_name);
	        	
	        	
            	$('#popupEditCartLine').data('id_ticket_line', request.id_ticket_line);
	        	
				$('#popupEditCartLine')
		    		.data('idobject',id_product)
		    		.data('idobject2',id_product_attribute);
				$('#popupEditCartLine .acaisse_productcell_content').css('background-image','');
				$('#popupEditCartLine .acaisse_productcell_content').data('idobject','').data('idobject2','');
				$('#popupEditCartLine .stock_available').removeClass('warning').removeClass('error').removeClass('good').html('');
				
				// On affiche le prix d'achat & marge	
				$('#popupEditCartLine .whole_sale_price_ht').html(parseFloat(response.product.whole_sale_price_tax_excl).toFixed(2));	
				$('#popupEditCartLine .whole_sale_price_ttc').html(parseFloat(response.product.whole_sale_price_tax_incl).toFixed(2));	
				$('#popupEditCartLine .margin_rate').html('<span class="alert-success">'+parseFloat(response.product.margin_rate).toFixed(2)+'%</span>');	
				$('#popupEditCartLine .original_margin_rate').html(parseFloat(response.product.margin_rate).toFixed(2)+'%');	
						
				//console.log(['LINE',response.line]);
				$('#popupEditCartLine').data('price', response.line.price_with_tax);
				
				$('#popupEditCartLine .stock_inqueue').html('');
				if($(cart_line_id+' .display_prix_final_ttc').length > 0)
				{
					$('#popupEditCartLine .product_price').html($(cart_line_id+' .display_prix_final_ttc').html());
				}
				else
				{
					$('#popupEditCartLine .product_price').html($(cart_line_id+' .specialPrice').html());
				}
				$('#popupEditCartLine .acaisse_productcell_content').css('background-image','url('+$(cart_line_id+' .img img').attr('src')+')');
				
				$('#popupEditCartLine .stock_real').html('');
				
				/*** réduction appliquée sur la ligne **/
				
				
				$('#popupEditCartLine .cart_line_reduc_amount').data('id_product',id_product);
				$('#popupEditCartLine .cart_line_reduc_amount').data('id_product_attribute',id_product_attribute);
				//$('#popupEditCartLine .cart_line_reduc_amount').append('');
				
				
				if (inputValue>0) {
					$('#popupEditCartLine .lbabK input.lbabK-inputabl').val(inputValue);
				}
				else
				{
					$('#popupEditCartLine .lbabK input.lbabK-inputabl').val('');
				}
								
				switch(type_reduc)
				{
					case '€':
						$('#popupEditCartLine .selected').removeClass('selected');
						$('#popupEditCartLine .reductAmountInEuro').addClass('selected');
						break;
					case '%':
						$('#popupEditCartLine .selected').removeClass('selected');
						$('#popupEditCartLine .reductAmountInPercent').addClass('selected');
						break;
					case 'gift':
						$('#popupEditCartLine .selected').removeClass('selected');
						$('#popupEditCartLine .reductAmountInGift').addClass('selected');
						break;			
				}
				
				//$('#popupEditCartLine .product_price').html($('#popupEditCartLine').data('price')+'€');
				
				$('#popupEditCartLine').prependTo($('body'));
				$('#popupEditCartLine').css('display','table');
				
				//mise en place des handlers de saisi du prix
				
				//modification manuelle du prix de vente TTC
				$('#popupEditCartLine').on('change keyup','#popup_epline__product_price_tax_incl',function(){
					
					var ttc = parseFloat('0'+$(this).val());
					var o_ttc = parseFloat($('#popup_epline__original_tax_incl').html()).toFixed(5); 
					var o_ht = parseFloat($('#popup_epline__original_tax_excl').html()).toFixed(5);
					
					var ht = 0;
					
					if(o_ttc == o_ht)
					{
						var ht = ttc;
					}
					else
					{	
						var ht = ttc*o_ht/o_ttc;
					}
					
					$('#popup_epline__product_price_tax_excl').val(parseFloat(ht).toFixed(5));
					
					refreshLiveDiscountPreview($('#popupEditCartLine .lbabK-inputable').val());
					
				});
				
				$('body').on('click tap','.cart_line_reduc_amount .chosseDiscountType > div',function() {
					
					$('.chosseDiscountType > div').removeClass('selected');
					$(this).addClass('selected');
					refreshLiveDiscountPreview($('#popupEditCartLine .lbabK-inputable').val());
				});
				
				//lors du changment du forcage ou non du prix manuellement
				$('#popupEditCartLine').on('change click blur','#popup_epline__original_price_is_forced_by_user_off,#popup_epline__original_price_is_forced_by_user_on',function(){
					
					
					//pas de prix forcé par l'utilisateur
		        	if($('#popup_epline__original_price_is_forced_by_user_on').prop('checked') != true)
		        	{	        		
			        	//$('#popup_epline__product_price_tax_incl').html(response.product.original_price_tax_incl);
			        	//$('#popup_epline__product_price_tax_excl').html(response.product.original_price_tax_excl);
			        	$('#popup_epline__manual_price_box').hide();
			        	$('#popup_epline__original_price_is_forced_by_user_on').prop('checked',false);
			        	$('#popup_epline__original_price_is_forced_by_user_off').prop('checked',true);
			        }
		        	else
		        	{
			        	//$('#popup_epline__product_price_tax_incl').val(response.line.original_price_with_tax);
			        	//$('#popup_epline__product_price_tax_excl').val(response.line.original_price_without_tax);
			        	$('#popup_epline__manual_price_box').show();
			        	$('#popup_epline__original_price_is_forced_by_user_off').prop('checked',false);
			        	$('#popup_epline__original_price_is_forced_by_user_on').prop('checked',true);
			        }
					
				});
				
				refreshLiveDiscountPreview();
				
	        } else {
	        	alert('Erreur O-00001 : impossible d\'obtenir les détails sur ce produit (#'+id_product+')');
	        }
	    }).fail(function () {
	        alert('Erreur O-00002 : impossible d\'obtenir les détails sur ce produit (#'+id_product+')');
	    });
		
		
    	
	};
	
});



/************************************************************************************
 * POPUP DE REDUCTION GLOBAL + informations complementaire SUR La VENTE
 ************************************************************************************/
$(function(){
	
	var showPopupGlobalCartReduction = function(reduc,type_reduc,inputValue) {
		//console.log('showPopupGlobalCartReduction('+reduc+','+type_reduc+','+inputValue+')');
		
		if (inputValue>0) {
			$('#popupGlobalCart .lbabK input.lbabK-inputabl').val(inputValue);
		}
		else
		{
			$('#popupGlobalCart .lbabK input.lbabK-inputabl').val('');
		}
		
		$('#popupGlobalCart .selected').removeClass('selected');
		
		switch(type_reduc)
		{
			case '€':
				$('#popupGlobalCart .reductAmountInEuro').addClass('selected');
				break;
			case '%':
				$('#popupGlobalCart .reductAmountInPercent').addClass('selected');
				break;
			case 'gift':
				$('#popupGlobalCart .reductAmountInGift').addClass('selected');
				break;			
		}
		
		$('#popupGlobalCart').prependTo($('body'));
		$('#popupGlobalCart').css('display','table');
	};
	
	//ajout du clavier de saisis remise gobale
	$('#popupGlobalCart .selectProductFloatNb').lbabKeyboard({ //clavier numerique pour la saisie de quantité
        'layout': 'floatNumeric',
        //'min' : 0,
        //'max' : 12,
        onType: function(value,$elt) {        	
        	//refreshLiveGlobalDiscountPreview(value,$elt);        	
        },
        onReturn: function (value) {        	
            if ($('#popupGlobalCart .lbabK-inputable').val()=='') {
            	$('#popupGlobalCart .lbabK-inputable').val(0);
            }
            var discount = Math.round(parseFloat(value)*100)/100;
            var type = $('#popupGlobalCart .selected').data('type');
            if(type == undefined || discount == 0)
            {
            	type = '€';
            	discount = 0;
            }

			$('.popup-overlay').css('display','none');

            registerDiscountOnCart(
            	discount,
            	type
        	);
        }
    });	
	
	$('#reductionGlobalOnCart').on('click tap',function() {
		
		if ($(this).hasClass('disabled') || $(this).parents('.disabled').length>0) {
        	//sendNotif('greenFont','Veuillez ouvrir la caisse afin de réaliser une vente.');
        	return false;
        }
		
		if (!ACAISSE.cartModificationLocked) {
    		//$('.button.validTicket').click();
    		_validateCurrentTicket(false);
    	}    	
		
		var reduc = $(this).parent().find('.pastille b').html();
		
		var type_reduc = '€';
		if (reduc!=undefined) {
			type_reduc = reduc[reduc.length-1];
		} else {
			reduc='-0€'; 
		}		
		
		var inputValue = Math.abs(parseFloat(reduc));
		
		// On récupère les informations du produit
		//_getProductInfos($(this).parent().data('id_product'));
		showPopupGlobalCartReduction(
			reduc,
			type_reduc,
			inputValue			
		);
		
	});
	
	$('body').on('click tap','#popupGlobalCart .chosseDiscountType > div',function() {
		
		$('.chosseDiscountType > div').removeClass('selected');
		$(this).addClass('selected');
		
		//var inputValue = $('#popupEditCartLine .lbabK-inputable').val();
		//refreshLiveDiscountPreview(inputValue);
	});
	
	$('body').on('click tap','#ticketDetail .header_ticket_toPrint .print_to_ticket',function(e) {$
		e.preventDefault();
		window.print();
	});
	
	$('body').on('click tap','#ticketDetail .header_ticket_toPrint .send',function(e) {
		e.preventDefault(); 
		$('#ticketDetail #send_form_email').val(ACAISSE.customer.email);
		$('#ticketDetail .content_ticket_toPrint').slideDown();
    	$('#ticketDetail .header_ticket_toPrint').slideUp();
	});
	
	$('#emptyPopup').on('click tap', '#ticketDetail #confirm_send', function (e){
    	e.preventDefault();
    	if(validate($('#send_form_email').val()) == true)
    	{
    		$('#emptyPopup .closePopup').click();
    		$(this).prop('disabled','disabled');
    		PRESTATILL_CAISSE.loader.start();
    		
    		$('#ticket_email').html('').append('<div style="width:650px;">'+$('#ticketDetail .ticket.toPrint').html()+'</div>');
			$('#ticket_email h2.gift_print').hide(); 
			
			html2canvas($('#ticket_email')[0],
			{
				windowWidth: '650px',
			}).then(function(canvas) {
			    
			    var base64image = canvas.toDataURL();
			    
			    $.ajax({
			        type: 'post',
			        dataType: 'json',
			        data: {
			            'action': 'SendTicketsByMail',
			            'id_ticket': $('#ticketDetail #id_ticket_to_send').val(),
				        'send_email': $('#send_form_email').val(),
				        imgBase64: base64image,
			        }
			    }).done(function (response) {
			        if (response.success != false) {
			        	
			        } else {
			          alert('Error : l_send0012 / Une erreur s\'est produite lors de l\'envoi du mail.');
			          //console.error(response.error);
			        }
			    }).fail(function () { 
			        alert('Error : l_send0011 / Une erreur s\'est produite lors de l\'envoi du mail.');
			    });  
			
			    ACAISSE_print_queue.shift();
		        // On confirme que le mail a bien été envoyé
		        PRESTATILL_CAISSE.loader.end();
		        //alert('Mail envoyé avec succès');
		        $('#ticket_email').html('');
		        
		        if(ACAISSE_print_queue.length > 0)
		        {
		        	// On imprime les tickets cadeau ou autres tickets rattachés
			        var next_ticket = ACAISSE_print_queue.shift();
					$('#printTicket').attr('class',next_ticket.class_name).html(next_ticket.html);			
					setTimeout(function(){			
						window.print();				
						printQueue();
					},500);
		        } 
		          
		        //on demande un passage au ticket suivant
         		_startNewPreorder(); 
         		
         		sendNotif('greenFont','<i class="fa fa-check"></i> Mail envoyé avec succès');
         	});
    	}
    	else
    	{
    		alert('Veuillez renseigner une adresse email correcte svp.');
    	}
    	
    });

});

$(function(){
	
	
	
	$('#deliveryButton').on('click tap',function() {
		if (!ACAISSE.cartModificationLocked) {
    		//$('.button.validTicket').click();
    		_validateCurrentTicket(false);
    	}    	
		
		
		// On récupère les informations du produit
		//_getProductInfos($(this).parent().data('id_product'));
		PRESTATILL_CAISSE.delivery.initPopupDatas();
		PRESTATILL_CAISSE.invoice.initPopupDatas();
		PRESTATILL_CAISSE.carrier.initPopupDatas();
		
		$('#popupDelivery').prependTo($('body'));
		$('#popupDelivery').css('display','table');
		
	});
	
});
    
