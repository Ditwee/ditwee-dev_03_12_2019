
function getProductPrice(id_product, id_product_attribute) { // récupère le prix du produit
	var p = _loadProductFromId(id_product);
	var unitId = params.units.from.indexOf(p.unity);


	if (id_product_attribute != undefined && id_product_attribute > 0) {
		if (p.unity == '') {
			p = p.attributes[id_product_attribute];
			return formatPrice(p.prices[ACAISSE.id_force_group]) + "€";
		}
	}
	
	if (unitId==-1) {
		console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		//throw "Erreur d'unité : unité inconnue : ";
	}
	// ATTENTION MODIFICATION A VALIDER : remplacement de p.unit_price par prices[ACAISSE.id_force_group]
	if (params.units.scale[unitId]!=1) {
		return formatPrice(p.prices[ACAISSE.id_force_group]*params.units.scale[unitId]).replace(' ','')+'€'+params.units.to[unitId];
	} else {
		return formatPrice(p.prices[ACAISSE.id_force_group]) + "€";
	}
}

function getProductBasePrice(id_product, id_product_attribute) { // récupère le prix du produit
	var p = _loadProductFromId(id_product);
	
	var unitId = params.units.from.indexOf(p.unity);
	if (unitId==-1) {
		console.log("ATTENTION !! L'unité "+p.unity+" utilisé pour le produit #"+p.id_product+" : "+p.product_name+" est inconnu. Il se peut donc que le prix soit éroné pour ce produit. Merci de contacter l'éditeur pour ajouter cette unité.");
		params.units.from.push(p.unity);
		params.units.to.push('');
		params.units.scale.push(1);
		//throw "Erreur d'unité : unité inconnue : ";
	}
	var base_price = p.base_price;
	
	if(ACAISSE.price_display_method == 1) //HT
	{
		base_price = parseFloat(p.base_price);	
	}
	else
	{
		// On ajoute la TVA (pour l'affichage)
		base_price = parseFloat(p.base_price*(1+(p.tax_rate/100)));
	}
			
	if (params.units.scale[unitId]!=1) {
		return formatPrice(base_price*params.units.scale[unitId]).replace(' ','')+'€'+params.units.to[unitId];
	} else {
		return formatPrice(base_price) + "€";
	}
}


/**
 *copie de PrestaShop/js/admin/price.js en version 1.6 
 */




function getTax()
{
	if (noTax)
		return 0;

	var selectedTax = document.getElementById('product-edit-id_tax_rules_group');
	var taxId = selectedTax.options[selectedTax.selectedIndex].value;
	return taxesArray[taxId].rates[0];
}

function getTaxes()
{
	if (noTax)
		taxesArray[taxId];

	var selectedTax = document.getElementById('product-edit-id_tax_rules_group');
	var taxId = selectedTax.options[selectedTax.selectedIndex].value;
	return taxesArray[taxId];
}

function addTaxes(price)
{
	var taxes = getTaxes();
	var price_with_taxes = price;
	if (taxes.computation_method == 0) {
		for (i in taxes.rates) {
			price_with_taxes *= (1 + taxes.rates[i] / 100);
			break;
		}
	}
	else if (taxes.computation_method == 1) {
		var rate = 0;
		for (i in taxes.rates) {
			 rate += taxes.rates[i];
		}
		price_with_taxes *= (1 + rate / 100);
	}
	else if (taxes.computation_method == 2) {
		for (i in taxes.rates) {
			price_with_taxes *= (1 + taxes.rates[i] / 100);
		}
	}

	return price_with_taxes;
}

function removeTaxes(price)
{
	var taxes = getTaxes();
	var price_without_taxes = price;
	if (taxes.computation_method == 0) {
		for (i in taxes.rates) {
			price_without_taxes /= (1 + taxes.rates[i] / 100);
			break;
		}
	}
	else if (taxes.computation_method == 1) {
		var rate = 0;
		for (i in taxes.rates) {
			 rate += taxes.rates[i];
		}
		price_without_taxes /= (1 + rate / 100);
	}
	else if (taxes.computation_method == 2) {
		for (i in taxes.rates) {
			price_without_taxes /= (1 + taxes.rates[i] / 100);
		}
	}

	return price_without_taxes;
}

function getEcotaxTaxIncluded()
{
	return ecotax_tax_incl;
}

function getEcotaxTaxExcluded()
{
	return ecotax_tax_excl;
}

function formatPrice(price)
{
	var fixedToSix = (Math.round(price * 1000000) / 1000000);
	return (Math.round(fixedToSix) == fixedToSix + 0.000001 ? fixedToSix + 0.000001 : fixedToSix);
}

function calcPrice()
{
	initEcoTaxBeforePriceCalc();
	
	var priceType = $('#product-edit-tax').val();
	if (priceType == 'TE')
	{
		calcPriceTI();
	}
	else
	{
		calcPriceTE();
	}
	
	addProductRefreshPrices();
}

function initEcoTaxBeforePriceCalc()
{
	ecotaxTaxRate = getTax();
	ecotax_tax_incl = parseFloat(document.getElementById('product-edit-eco-tax').value.replace(/,/g, '.'));
	if(isNaN(ecotax_tax_incl))
	{
		ecotax_tax_incl = 0;
	}
	ecotax_tax_excl = ps_round(removeTaxes(ecotax_tax_incl), 2);
}

function calcPriceTI()
{
	priceTE = parseFloat(document.getElementById('product-edit-price_editor').value.replace(/,/g, '.'));
	if(isNaN(priceTE))
	{
		priceTE = 0;
	}
	var newPrice = addTaxes(priceTE+ecotax_tax_excl);
	priceTI = (isNaN(newPrice) == true || newPrice < 0) ? '' : ps_round(newPrice, priceDisplayPrecision).toFixed(5);
}

function calcPriceTE()
{
	//ecotax_tax_excl =  $('#product-edit-eco-tax').val() / (1 + ecotaxTaxRate);
	priceTI = parseFloat(document.getElementById('product-edit-price_editor').value.replace(/,/g, '.'));
	if(isNaN(priceTI))
	{
		priceTI = 0;
	}
	var newPrice = removeTaxes(ps_round(priceTI - getEcotaxTaxIncluded(), priceDisplayPrecision));
	priceTE = (isNaN(newPrice) == true || newPrice < 0) ? '' : ps_round(newPrice, priceDisplayPrecision).toFixed(5);
}

function addProductRefreshPrices()
{
	
	$('#converted_price_value').html($('#product-edit-tax').val() == 'TE'?priceTI:priceTE);
	$('#converted_price_vat').html($('#product-edit-tax').val() == 'TE'?'TTC':'HT');
	$('#converted_eco_tax').html($('#product-edit-tax').val() == 'TE'?'Prix de vente TTC incluant l\'éco taxe (TTC)':'Prix de vente TTC incluant l\'éco taxe (TTC)');
	$('#convert_price_currency').html(currency.sign);
	$('#product-edit-price').val(priceTE);
}

function decimalTruncate(source, decimals)
{
	if (typeof(decimals) == 'undefined')
		decimals = 6;
	source = source.toString();
	var pos = source.indexOf('.');
	return parseFloat(source.substr(0, pos + decimals + 1));
}

/**
 * En provenance de /prestashop/js/tools.js 
 */
function formatedNumberToFloat(price, currencyFormat, currencySign)
{
	price = price.replace(currencySign, '');
	if (currencyFormat === 1)
		return parseFloat(price.replace(',', '').replace(' ', ''));
	else if (currencyFormat === 2)
		return parseFloat(price.replace(' ', '').replace(',', '.'));
	else if (currencyFormat === 3)
		return parseFloat(price.replace('.', '').replace(' ', '').replace(',', '.'));
	else if (currencyFormat === 4)
		return parseFloat(price.replace(',', '').replace(' ', ''));
	return price;
}

//return a formatted number
function formatNumber(value, numberOfDecimal, thousenSeparator, virgule)
{
	value = value.toFixed(numberOfDecimal);
	var val_string = value+'';
	var tmp = val_string.split('.');
	var abs_val_string = (tmp.length === 2) ? tmp[0] : val_string;
	var deci_string = ('0.' + (tmp.length === 2 ? tmp[1] : 0)).substr(2);
	var nb = abs_val_string.length;

	for (var i = 1 ; i < 4; i++)
		if (value >= Math.pow(10, (3 * i)))
			abs_val_string = abs_val_string.substring(0, nb - (3 * i)) + thousenSeparator + abs_val_string.substring(nb - (3 * i));

	if (parseInt(numberOfDecimal) === 0)
		return abs_val_string;
	return abs_val_string + virgule + (deci_string > 0 ? deci_string : '00');
}

function formatCurrency(price, currencyFormat, currencySign, currencyBlank)
{
	// if you modified this function, don't forget to modify the PHP function displayPrice (in the Tools.php class)
	var blank = '';
	price = parseFloat(price).toFixed(10);
	price = ps_round(price, priceDisplayPrecision);
	if (currencyBlank > 0)
		blank = ' ';
	if (currencyFormat == 1)
		return currencySign + blank + formatNumber(price, priceDisplayPrecision, ',', '.');
	if (currencyFormat == 2)
		return (formatNumber(price, priceDisplayPrecision, ' ', ',') + blank + currencySign);
	if (currencyFormat == 3)
		return (currencySign + blank + formatNumber(price, priceDisplayPrecision, '.', ','));
	if (currencyFormat == 4)
		return (formatNumber(price, priceDisplayPrecision, ',', '.') + blank + currencySign);
	if (currencyFormat == 5)
		return (currencySign + blank + formatNumber(price, priceDisplayPrecision, '\'', '.'));
	return price;
}

function ps_round_helper(value, mode)
{
	// From PHP Math.c
	if (value >= 0.0)
	{
		tmp_value = Math.floor(value + 0.5);
		if ((mode == 3 && value == (-0.5 + tmp_value)) ||
			(mode == 4 && value == (0.5 + 2 * Math.floor(tmp_value / 2.0))) ||
			(mode == 5 && value == (0.5 + 2 * Math.floor(tmp_value / 2.0) - 1.0)))
			tmp_value -= 1.0;
	}
	else
	{
		tmp_value = Math.ceil(value - 0.5);
		if ((mode == 3 && value == (0.5 + tmp_value)) ||
			(mode == 4 && value == (-0.5 + 2 * Math.ceil(tmp_value / 2.0))) ||
			(mode == 5 && value == (-0.5 + 2 * Math.ceil(tmp_value / 2.0) + 1.0)))
			tmp_value += 1.0;
	}

	return tmp_value;
}

function ps_log10(value)
{
	return Math.log(value) / Math.LN10;
}

function ps_round_half_up(value, precision)
{
	var mul = Math.pow(10, precision);
	var val = value * mul;

	var next_digit = Math.floor(val * 10) - 10 * Math.floor(val);
	if (next_digit >= 5)
		val = Math.ceil(val);
	else
		val = Math.floor(val);

	return val / mul;
}

function ps_round(value, places)
{
	if (typeof(roundMode) === 'undefined')
		roundMode = 2;
	if (typeof(places) === 'undefined')
		places = 2;

	var method = roundMode;

	if (method === 0)
		return ceilf(value, places);
	else if (method === 1)
		return floorf(value, places);
	else if (method === 2)
		return ps_round_half_up(value, places);
	else if (method == 3 || method == 4 || method == 5)
	{
		// From PHP Math.c
		var precision_places = 14 - Math.floor(ps_log10(Math.abs(value)));
		var f1 = Math.pow(10, Math.abs(places));

		if (precision_places > places && precision_places - places < 15)
		{
			var f2 = Math.pow(10, Math.abs(precision_places));
			if (precision_places >= 0)
				tmp_value = value * f2;
			else
				tmp_value = value / f2;

			tmp_value = ps_round_helper(tmp_value, roundMode);

			/* now correctly move the decimal point */
			f2 = Math.pow(10, Math.abs(places - precision_places));
			/* because places < precision_places */
			tmp_value /= f2;
		}
		else
		{
			/* adjust the value */
			if (places >= 0)
				tmp_value = value * f1;
			else
				tmp_value = value / f1;

			if (Math.abs(tmp_value) >= 1e15)
				return value;
		}

		tmp_value = ps_round_helper(tmp_value, roundMode);
		if (places > 0)
			tmp_value = tmp_value / f1;
		else
			tmp_value = tmp_value * f1;

		return tmp_value;
	}
}