function printZList(dateToPrint) {
	// si on ne demande pas de dtae en particulier on imprime la bande Z du jour
	if (dateToPrint==undefined) {
		dateToPrint = getEngDate(new Date(),false);
	}
	
	PRESTATILL_CAISSE.loader.start();
	
	$.ajax({
        data: {
            'action': 'GetZCaisse',
            'date' : dateToPrint,
            'id_pointshop' : ACAISSE.id_pointshop
        },
        type: 'post',
        dataType: 'json',
    }).fail(function(response) {
    	//$('#printTicket').attr('class','').html(response.response.z_caisse);
    	PRESTATILL_CAISSE.loader.end();
		alert('Pas d\'encaissement ce jour');
	}).done(function(response) {
    	$('#printTicket').attr('class','').html(response.response.z_caisse);
    	PRESTATILL_CAISSE.loader.end();
		window.print();
	});
}

function getTicketCaisse() {
  	$.ajax({
        type: 'post',
        data: {
          'action': 'GetTicketCaisse',
          'id_ticket': ACAISSE.ticket.id,
        },
        dataType: 'json',
    }).done(function (response) {
        if (response.success != false) {
          
          printTicket(response.response.ticket_caisse, acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.ticket_nbr_copies);
                    
  		  for(var i = 0 ; i < response.response.extra_tickets.length ; i++)
  		  {
  			 printTicket(response.response.extra_tickets[i], 1);
  		  }
  		  
  		  // Soit envoi par mail soit impression
  		  if(acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.ticket_email == 1)
  		  {
  		  	
  		  	var html = '';
		
			html +='<div id="ticketInfos" class="popupBox">';
				html +='<div class="overflow" style="display:none;"></div>';
		        html +='<div class="title">';
		       		html +='<h3>Souhaitez-vous imprimer le ticket ou l\'envoyer par mail ?</h3>';
		        html +='</div>';
		        html +='<div class="sendTicket infosComp">';
					html +='<form id="popup_epgloal__form">';
		            	html +='<fieldset>';
		            			html +='<div class="send_form" style="display:none;"><div class="label" >Adresse email d\'envoi :</div>';
									html +='<input type="text" value="'+ACAISSE.customer.email+'" id="send_form_email" >';
									html +='<div>';
										html +='<button id="abort_send" class="button">Annuler</button>';
										html +='<button id="confirm_send" class="button">Envoyer</button>';
									html +='</div>';
								html +='</div>';
		            	html +='</fieldset>'; 
		            html +='</form>';
		            html +='<div class="devisButtons">'; 
		            	html +='<div class="button no_ticket"><i class="fa fa-file"></i><div class="label">Pas de ticket</div><div class="legend">(Ni imprimé, ni envoyé)</div></div>';
		            	html +='<div class="button print_to_ticket"><i class="fa fa-print"></i><div class="label">Imprimer</div><div class="legend">(format ticket)</div></div>';
		            	html +='<div class="button send"><i class="fa fa-envelope"></i><div class="label">Envoyer</div><div class="legend">par e-mail</div></div>';
		            html +='</div>';
		        html +='</div>';
			html +='</div>';
			
			showEmptyPopup($("#paymentView"),html);
			
		  }
		  else {
		  	printQueue();
		  	
		  	  $('#multi-payments-popup').remove();
	          $('.strict-overlay').remove();
	          //on demande un passage au ticket suivant
	          _startNewPreorder();
		  }	
		
		  
  		  
          
          
        } else {
          alert('Error : o_print0012 / Erreur lors de la génération du ticket de caisse');
          console.error(response.error);
        }
    }).fail(function () { 
        alert('Error : o_print0011 / Erreur lors de la génération du ticket de caisse');
    });
}

function getDayTickets(date) {
	$.ajax({
		data: {
			'action': 'GetDayTickets',
			'id_pointshop': ACAISSE.id_pointshop,
			'dat':date
		},
		type: 'post',
		dataType: 'json',
	}).done(function(response) {            	
		if (response.success == true) {
			var ticketBuffer = response.response.tickets;
			
			$('#printTickets .rePrintZ').removeClass('disabled');
			//console.log(ticketBuffer[i]);
			$('#printTickets .notLoaded').css('display','block');
	
			if (false && ticketBuffer.length==1) {
				// on affiche le ticket de caisse avec le bouton de réimpression
				$('#printTickets .left .ticket').remove();
				$('#printTickets .rePrintZ').addClass('disabled');
				$('#printTickets .notLoaded, #printTickets .loaded').css('display','none');
				$('#printTickets .left').append('<div class="ticket toPrint">'+ticketBuffer[0].htm+'</div><div class="ticket"><button class="print"><i class="fa fa-print fa-2x"></i> Imprimer</button> <button class="kdo"><i class="fa fa-gift fa-2x"></i> Imprimer Ticket Cadeau</button></div>');
			} else {
				var html ='';
				html += '<table>';
				
				html +='<thead><tr><th></th><th>N° de ticket</th><th>Client</th><th>Date</th><th>Référence</th><th>N° de Commande</th><th>Montant total</th></tr></thead>';
				
				for (var i=0; i<ticketBuffer.length; i++) {
					
					//console.log(ticketBuffer[i]);
					
					html += '<tr data-tblId="'+i+'" data-num="'+ticketBuffer[i].num+'">';
					
					html += 	'<td class="text-center" ><i  class="fa fa-ticket"></td>';
					html += 	'<td class="text-center" >'+ticketBuffer[i].num+'</td>';
					html += 	'<td class="text-center" >'+ticketBuffer[i].client+'</td>';
					html +=	 	'<td class="text-center" '+(ticketBuffer[i].valid != 1?' style="text-decoration:line-through;"':'')+'><span class="date">'+ Date.toString(ticketBuffer[i].dat) + '</span>';
					if(ticketBuffer[i].valid != 1)
					{
						html +=		' <span class="badge badge-error">ANNULE<span>';
					}
					html += '</td>';
					html += 	'<td class="text-center" ><span class="state">'+ ticketBuffer[i].reference +'<span></td>';
					html += 	'<td class="text-center" >'+ ticketBuffer[i].id_order +'</td>';
					html += 	'<td class="text-center" ><span class="paid">'+ticketBuffer[i].amount+'€</span></td>';
					html += '</tr>';
				}
				html += '</table>';
				$('#printTickets .left .ticket').remove();
				$('#printTickets .loaded').html(html);
				$('#printTickets .loaded').css('display','block');
				$('#printTickets .notLoaded').css('display','none');
			
				var todayDate = getEngDate(new Date(),false); 
				//console.log(date);
				//console.log(getEngDate(new Date(),false));
				if(date == todayDate)
				{
					$('#printTickets .rePrintZ').addClass('disabled');
				}
				
			}
		} else {
			alert(['Erreur ????? (getDayTickets) : ' + response.errorMsg, response]);
		}
	}).fail(function () {
		alert(['Erreur 01002 : Impossible de communiquer les informations au serveur',ticket]);
	});
}

function getNumTickets(num) {
	$.ajax({
		data: {
			'action': 'GetNumTickets',
			'id_pointshop': ACAISSE.id_pointshop,
			'num':num
		},
		type: 'post',
		dataType: 'json',
	}).done(function(response) {            	
		if (response.success == true) {
			var ticketBuffer = response.response.tickets;
			
			$('#printTickets .notLoaded').css('display','block');
	
			if (ticketBuffer.length==1) {
				// on affiche le ticket de caisse avec le bouton de réimpression
				$('#printTickets .left .ticket').remove();
				$('#printTickets .notLoaded, #printTickets .loaded').css('display','none');
				//buffer de preview
				$('#printTickets .left').append('<div class="ticket toPrint">'+ticketBuffer[0].htm+'</div>');
				//on ajoute les bouttons d'impression
				//@TODO ici faudra certainement faire des diffrences				
				var html = '<div class="ticket print_buttons">';
				html += '<button class="print"><i class="fa fa-print fa-2x"></i> Duplicata</button> ';
				//@TODO factureetet =>*****************
				//html += '<button class="facturette"><i class="fa fa-print fa-2x"></i> Facturette</button> ';
				//**************************************
				html += ' <button class="kdo"><i class="fa fa-gift fa-2x"></i> Imprimer Ticket Cadeau</button>';
				html += '</div>';
				$('#printTickets .left').append(html);
			} else {
				var html = '<table>';
				for (var i=0; i<ticketBuffer.length; i++) {
					html += '<tr data-tblId="'+i+'" data-num="'+ticketBuffer[i].num+'"><td><i  class="fa fa-ticket"></i>Ticket n°'+ticketBuffer[i].num+'</td><td>' + Date.toString(ticketBuffer[i].dat) + '</td><td>'+ticketBuffer[i].amount+'</td></tr>';
				}
				html += '</table>';
				$('#printTickets .left .ticket').remove();
				$('#printTickets .loaded').html(html);
				$('#printTickets .loaded').css('display','block');
				$('#printTickets .notLoaded').css('display','none');
			}
		} else {
			alert(['Erreur ????? (getNumTickets) : ' + response.errorMsg, response]);
		}
	}).fail(function () {
		alert(['Erreur 01002 : Impossible de communiquer les informations au serveur',ticket]);
	});
}

function getEngDate(d, withTime) { // YYYY-mm-dd
	var date=d.getFullYear()+'-';
	
	if (d.getMonth()+1<10) {
		date+=('0'+(d.getMonth()+1)+'-');
	} else {
		date+=((d.getMonth()+1)+'-');
	}
	
	if (d.getDate()<10) {
		date+=('0'+d.getDate());
	} else {
		date+=(d.getDate());
	}
	
	if (withTime) {
		date+=' ';
		if (d.getHours()<10) {
			date+=('0'+(d.getHours()+1)+':');
		} else {
			date+=((d.getHours())+':');
		}
		
		if (d.getMinutes()<10) {
			date+=('0'+d.getMinutes());
		} else {
			date+=(d.getMinutes());
		}
	}	
	return date;
}

var ACAISSE_print_queue = [];


printTicket = function(html, nb_print, class_name) {
	
	var class_name = class_name==undefined?'ticket':class_name;
	for (var i=0; i<nb_print; i++) {		
		ACAISSE_print_queue.push({html:html,'class_name':class_name});
	}
};

var printQueue = function(send = false) {
	if(ACAISSE_print_queue.length>0)
	{
		if (send == false)
		{
			var next_ticket = ACAISSE_print_queue.shift();
			$('#printTicket').attr('class',next_ticket.class_name).html(next_ticket.html);			
			setTimeout(function(){			
				window.print();				
				printQueue();
			},500);
		}
		else
		{
			$('#ticket_email').html('').append('<div style="width:650px;">'+ACAISSE_print_queue[0]['html']+'</div>');
			$('#ticket_email h2.gift_print, #ticket_email h2.duplicata_print').hide(); 
			
			html2canvas($('#ticket_email')[0],
			{
				windowWidth: '650px',
			}).then(function(canvas) {
			    
			    var base64image = canvas.toDataURL();
			    
			    $.ajax({
			        type: 'post',
			        dataType: 'json',
			        data: {
			            'action': 'SendTicketsByMail',
			            'id_ticket': ACAISSE.ticket.id,
			            'tickets': ACAISSE_print_queue,
				        'send_email': $('#send_form_email').val(),
				        imgBase64: base64image,
			        }
			    }).done(function (response) {
			        if (response.success != false) {
		        	
		        	/*  ACAISSE_print_queue.shift();
			          // On confirme que le mail a bien été envoyé
			          PRESTATILL_CAISSE.loader.end();
			          //alert('Mail envoyé avec succès');
			          $('#ticket_email').html('');
			          
			         if(ACAISSE_print_queue.length > 0)
			         {
			         	 // On imprime les tickets cadeau ou autres tickets rattachés
				         var next_ticket = ACAISSE_print_queue.shift();
						 $('#printTicket').attr('class',next_ticket.class_name).html(next_ticket.html);			
						 setTimeout(function(){			
							window.print();				
							printQueue();
						 },500);
			         } 
			          
			         //on demande un passage au ticket suivant
	         		 _startNewPreorder(); 
	         		 
	         		 sendNotif('greenFont','<i class="fa fa-check"></i> Mail envoyé avec succès');
			         */ 
			        } else {
			          alert('Error : l_send0012 / Une erreur s\'est produite lors de l\'envoi du mail.');
			          //console.error(response.error);
			        }
			    }).fail(function () { 
			        alert('Error : l_send0011 / Une erreur s\'est produite lors de l\'envoi du mail.');
			    });  
			
			    ACAISSE_print_queue.shift();
		        // On confirme que le mail a bien été envoyé
		        PRESTATILL_CAISSE.loader.end();
		        //alert('Mail envoyé avec succès');
		        $('#ticket_email').html('');
		        
		        if(ACAISSE_print_queue.length > 0)
		        {
		        	// On imprime les tickets cadeau ou autres tickets rattachés
			        var next_ticket = ACAISSE_print_queue.shift();
					$('#printTicket').attr('class',next_ticket.class_name).html(next_ticket.html);			
					setTimeout(function(){			
						window.print();				
						printQueue();
					},500);
		        } 
		          
		        //on demande un passage au ticket suivant
         		_startNewPreorder(); 
         		
         		sendNotif('greenFont','<i class="fa fa-check"></i> Mail envoyé avec succès');
			});
			
			
		}
	}
};


$(function() {
	$('#printTickets .right').prepend('<div style="text-align:center;"><button class="rePrintZ"><i class="fa fa-print"></i> Ré-imprimer le Z du jour</button></div><div class="searchTicketBox"><label for="searchTicketDate">Date du ticket : </label><input id="searchTicketsDate" type="date" style="width: 130px;" />');
	$('#searchTicketsDate')[0].value=getEngDate(new Date());
	resetTileHeight();
	
	document.querySelector('#searchTicketsDate').onchange = function() {
		var date = this.value;
		var d = date.split('-');
		if (d[0]>2000) {
			// on concidère qu'on a une bonne date, on envoie la recherche au serveur
			$('#printTickets .left .notLoaded').css('display','block');
			
			getDayTickets(this.value);
		}
	};
	
	setTimeout(function() {
		document.querySelector('#searchTicketsDate').onchange();
	},1000);
});