$(function(){
	window.onresize = function() {
		_setWidthAndHeight();
	};
});


function _gotoTicketView() { //Horizontal navigation
	ACAISSE.scanbarcodeMode = 'product';
	var moveTo = ACAISSE.till_width-$('#productList > .switcher').outerWidth();
	$('#middle').animate({'margin-left': -moveTo + 'px'});
	$('#productList .switcher').removeClass('show-ticket').addClass('show-product-list');
	$('#productList .switcher h6').html('Catalogue');
	$('#productList .switcher .boxCart i').removeClass().addClass('icon-uniE600');
	$('#productList .switcher #totalItem').fadeOut();
	PRESTATILL_CAISSE.loader.showActions();
}

function _gotoProductList() { //Horizontal navigation
	ACAISSE.scanbarcodeMode = 'product';	
	$('#middle').animate({'margin-left': 0});
	$('#productList .switcher').addClass('show-ticket').removeClass('show-product-list');
	$('#productList .switcher h6').html('Mon ticket');
	$('#productList .switcher .boxCart i').removeClass().addClass('icon-cart2');
	$('#productList .switcher #totalItem').fadeIn();
	PRESTATILL_CAISSE.loader.showActions();
}

/**
 * Pour switcher sur le dernier panneau correspond au panneau de paiement 
 */
function _gotoPaymentPanel() {	
	PRESTATILL_CAISSE.payment.initOnOpenPaymentInterface();
	ACAISSE.scanbarcodeMode = 'payment';
	var moveTo = 2*ACAISSE.till_width-$('#productList > .switcher').outerWidth();//-$('.orderButtons').outerWidth();//;
	$('#middle').animate({'margin-left': -moveTo + 'px'});
}

/**
 * @TODO : Pour allez sur le panneau de sélection du point de vente 
 */
function _gotoPointhopChooser() {
	var moveTo = -ACAISSE.till_width;
	ACAISSE.scanbarcodeMode = 'product';
	$('#middle').animate({'margin-left': -moveTo + 'px'});
}



function _setWidthAndHeight(){
	
	var header_height = footer_height = 64;
	
	if(ACAISSE.mode_debogue === true)
	{
		console.log('_setWidthAndHeight()');
	}
	
	if (window.innerHeight>acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_height) {
		ACAISSE.till_height = window.innerHeight;
	} else {
		ACAISSE.till_height = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_height;
	}
	
	if (window.innerWidth>acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_width) {
		ACAISSE.till_width = window.innerWidth;
	} else {
		ACAISSE.till_width = acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.till_width;
	}

	$('#content').width(ACAISSE.till_width).height(ACAISSE.till_height);
	//largeur multiplier par 3 car 3 panneaux horizontaux
	$('#middle').width(ACAISSE.till_width*4).css('left','-'+ACAISSE.till_width+'px');
	$('.panel').width(ACAISSE.till_width);
	
	var switcherWidth = $('.switcher').outerWidth();
	$('#productList .content, #ticketView .content .content, #ticketView').width(ACAISSE.till_width-switcherWidth-1);
	
	$('#paymentView').css({		
		    'position': 'relative',
		    'width' : (ACAISSE.till_width)+'px', //-$('.orderButtons').outerWidth()
		    'top' : 0,
		    'height': ACAISSE.till_height-header_height-footer_height+'px',
		    'left': (3*ACAISSE.till_width-switcherWidth)+'px',
	});
	
	$('#paymentView .flexrow').css({				
		   // 'margin-top': -(ACAISSE.till_height-header_height-footer_height)+'px',
		   'position' : 'relative',
	});
	
	$('#popupNumeric').width(ACAISSE.till_width-switcherWidth);
	
	if($('#productList .switcher').hasClass('show-ticket'))
	{
		var halfTillWidth = parseInt((ACAISSE.till_width-switcherWidth)/2);
	}
	else
	{
		var halfTillWidth = parseInt((ACAISSE.till_width)/2);
	}
	
	$('body style.inline-resize-style').remove();
	
	$('body').append('<style class="inline-resize-style">#popupNumeric {left:50%;margin-left: '+-halfTillWidth+'px!important;}</style>');
	//$('body').append('<style>#popupNumeric {margin-left: calc((100% - 800px) / 2) !important;}</style>');

	var orderButtonsWidth = $('.orderButtons').outerWidth();
	$('#ticketView .inner-content').outerWidth(ACAISSE.till_width-switcherWidth-orderButtonsWidth-1);
	
	var sumHeight = 0;
	$('#customerListContainer>div').each(function(i) {
		sumHeight += $(this).height();
	});
	if (sumHeight > $('#customerListContainer').height()) {
		$('#defaultCustomersProfils,#customerSearchResults')
			.css('height','100px')
			.css('overflow-x','auto')
			.css('overflow-y','hidden')
			.css('white-space','nowrap');
		$('#customerListContainer .preview').css('height','150px');
	}
	
	if ($('#specialsContainer').height() > ACAISSE.till_height-128) {
		$('#specialsContainer ul, #specialsContainer .tab-container li').css('height','477px');
		$('#specialsContainer .tab-content').css('overflow','hidden');
		$('#specialsContainer>.content').css('height',$('#specialsContainer ul').css('height'));
		$('#specialsContainer').css('height',$('#productList').css('height'));
	}
	
	if ($('#userEditContainer>.content').height() > ACAISSE.till_height-128) {
		$('#userEditContainer>.content').css('height', (ACAISSE.till_height-128-$('#userEditContainer .reverse').height())+'px');
		$('#userEditContainer').css('height', (ACAISSE.till_height-128)+'px');
	}
	
	$('#productList > div.switcher.show-ticket').css('border-right-width','4px');
	
	
	/* TEST MOBILE VIEW ONLY WITH DOUCHETTE ^^ */
	if(window.innerWidth < 600)
	{
		//console.log(window.innerWidth);
		$('#ticketView').width(window.innerWidth+4);
		$('#ticketView .inner-content').width(parseInt(window.innerWidth));
		$('#ticketView th.cost').attr('colspan',2);
		$('#veille .closeBt').trigger('click');
	}
	
	
	setTimeout(function() { // timeout nécéssaire pour une raison inconnu :/
		resetTileHeight();
	},500);
		
}

function resetTileHeight() {
	$('#acaissepagecms tbody').css('display','block'); // Pête les grosses tuiles de la caisse
	$('#acaissepagecms tbody').css('width','100%');
	
	$('#acaissepagecms tbody').css('overflow','auto');
	
	if ($('#productList > div.switcher.show-ticket').outerHeight()!=null) { // ça vaut null quand on imprime le ticket à cause du media print je suppose :/
		$('#acaissepagecms tbody').css('height',($('#productList > div.switcher.show-ticket').outerHeight()-5)+'px');
	}
	$('#acaissepagecms tbody td').css('border','5px solid '+colorLaurent);
	$('#acaissepagecms').css('border','0px');
	
	var hauteur_cell = ((($('#acaissepagecms tbody').height()-3)/acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.max_display_row)-1*ACAISSE.nbRow);
	var largeur_cell = ($('#acaissepagecms tbody').width() / ACAISSE.nbCol);
	
	$('#acaissepagecms tbody td').css('height',hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan2').css('height',2*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan3').css('height',3*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan4').css('height',4*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan5').css('height',5*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan6').css('height',6*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan7').css('height',7*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan8').css('height',8*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan9').css('height',9*hauteur_cell+'px');
		$('#acaissepagecms tbody td.rowspan10').css('height',10*hauteur_cell+'px');
	$('#acaissepagecms tbody td').css('width',largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan2').css('width',2*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan3').css('width',3*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan4').css('width',4*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan5').css('width',5*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan6').css('width',6*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan7').css('width',7*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan8').css('width',8*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan9').css('width',9*largeur_cell+'px');
		$('#acaissepagecms tbody td.colspan10').css('width',10*largeur_cell+'px');
	
	if((($('#acaissepagecms tbody').width() / ACAISSE.nbCol)/10) > 13) {
		$('#acaissepagecms').css('font-size',(($('#acaissepagecms tbody').width() / ACAISSE.nbCol)/10)+'px');
	} else {
		$('#acaissepagecms').css('font-size','13px');
	}
	
	$('.product_stock').each(function() {
		if ($(this).html().trim()=='') {
			$(this).css('display','none');
		}
	});
	
	if ($('#acaissepagecms > tbody').length>0) {
		if ($('#acaissepagecms > tbody')[0].scrollHeight - $('#acaissepagecms > tbody').height() <= 10) { // si on a 5px ou moins a scroller
			$('#acaissepagecms > tbody').css('overflow','hidden');
		} else {
			$('#acaissepagecms > tbody').css('overflow','auto');
		}
	}
	
	$('div .left').each(function() {$(this).height($('#printTickets').height()-52);});
	
	var height = $('#searchProducts').height() - $('#searchProducts .searchBox').height() - 30;
	if (height>250) {height=250;}
	
	var width = (height*180)/250; 
	
	//$('#productResearchList td').width(width);
	//$('#searchProducts > div').first().height(height);
	
	$('#printTickets .right').css('margin-top',($('#printTickets').height()/2 - $('#printTickets .right').height()/2)+'px');
	$('#preOrderListContainer .right').css('margin-top',($('#preOrderListContainer').height()/2 - $('#preOrderListContainer .right').height()/2)+'px');
	/*
	var veilleBoxHeight = $('#veilleBox').height();
	$('#veilleBox').css('margin-top',-veilleBoxHeight/2);
	*/
	
	
}
