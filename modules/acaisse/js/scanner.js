var scanner = {
	buffer : '',
	resetterProcess : setInterval(function () {scanner.buffer = '';}, 500),
	relauncherProcess : null,
	customerKeyIntervalProcess : null
};

$(function() {
	//GESTION DES SAISIES AU CLAVIER / KEYBOARD ENTRY
    //pour les touches spéciales
    $(document).on('keydown', 'body', function (e) {

        var key = e.which || e.keyCode || e.charCode;

        switch (ACAISSE.scanbarcodeMode)
        {
            case 'cash':
                var key = e.which || e.keyCode || e.charCode;
                if (key == 8 || key == 46) {
                    //pour les touche backspace et delete
                    _cashDeleteChar();
                }
                break;
            case 'customer':
                var key = e.which || e.keyCode || e.charCode;
                if (key == 8 || key == 46) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('#customerSearchResultsFilter input').val($('#customerSearchResultsFilter input').val().substring(0, $('#customerSearchResultsFilter input').val().length - 1));
                    return false;
                }
                break;
             case 'productSearch':
                var key = e.which || e.keyCode || e.charCode;
                if (key == 8 || key == 46) {
                    e.preventDefault();
                    e.stopPropagation();
                    $('#productSearchResultsFilter input').val($('#productSearchResultsFilter input').val().substring(0, $('#productSearchResultsFilter input').val().length - 1));
                    loadProductSearchList();
                    return false;
                }
                break;
            case 'preOrder':
                if (key == 8 || key == 46) {
                    if ($('#preOrderListContainer .keyboard span.display').html().length > 0)
                    {
                        $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html().substring(0, ($('#preOrderListContainer .keyboard span.display').html().length - 1)));
                    }
                    _refreshFilterPreOrderList($('#preOrderListContainer .keyboard span.display').html());
                }
                break;
        }


        if ((key == 8 || key == 46) && ($('input:focus, textarea:focus').length == 0)) {
            //console.log('not focus');
            e.preventDefault();
            e.stopPropagation();
            return false; 
        }
        
    });
    //Pour les saisies
    $(document).on('keypress', 'body', function (e) {
        switch (ACAISSE.scanbarcodeMode)
        {
            case 'payment':            	
            	PRESTATILL_CAISSE.payment.dispatchScanBarCode(e);
            	break;
            case 'cash': //lors du paiment en liquide
                //console.log('cash');
                var c = String.fromCharCode(e.keyCode);
                _cashAddChar(c);
                //console.log([e.keyCode, c]);
                break;

            case 'preOrder':
                var num = parseInt(String.fromCharCode(e.keyCode));
                if (num >= 0 && num <= 9)
                {
                    $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html() + num);
                }
                else
                {
                    if ($('#preOrderListContainer .keyboard span.display').html().length > 0)
                    {
                        $('#preOrderListContainer .keyboard span.display').html($('#preOrderListContainer .keyboard span.display').html().substring(0, ($('#preOrderListContainer .keyboard span.display').html().length - 1)));
                    }
                }
                _refreshFilterPreOrderList($('#preOrderListContainer .keyboard span.display').html());
                break;
            case 'customer':
                clearTimeout(scanner.customerKeyIntervalProcess);
                var ch = String.fromCharCode(e.keyCode);
                $('#customerSearchResultsFilter input').val($('#customerSearchResultsFilter input').val() + ch);
                scanner.customerKeyIntervalProcess = setTimeout(function () {
                    idCustomer = $('#customerSearchResultsFilter input').val();
                    if (idCustomer.length==13 && parseInt(idCustomer)==idCustomer && idCustomer[0]==2) {// on a à faire à un ean13
                    	idCustomer=parseInt(idCustomer.substring(1,idCustomer.length-1));
                    	$('#customerSearchResultsFilter input').val(idCustomer);
                    	sendCustomerSearch(true);
                    } else {
                    	sendCustomerSearch();
                    }
                    
                }, 500);
                break;
            case 'productSearch':
                clearTimeout(scanner.customerKeyIntervalProcess);
                var ch = String.fromCharCode(e.keyCode);
                $('#productSearchResultsFilter input').val($('#productSearchResultsFilter input').val() + ch);
                scanner.customerKeyIntervalProcess = setTimeout(function () {
                    loadProductSearchList();
                }, 500);
                break;
            case 'editCustomer' :
                $('[id^="customer-edit-error-"]').each(function(){
                    if($(this).is(':visible')){
                        $(this).fadeOut();
                    }
                });
                break;
            case 'action' :
            	break;
            case 'product':
            
            default:
                clearInterval(scanner.resetterProcess);
                if (scanner.relauncherProcess !== null) {
                    clearTimeout(scanner.relauncherProcess);
                }
                scanner.relauncherProcess = setTimeout(function () {
                    scanner.resetterProcess = setInterval(function () {
                        scanner.buffer = '';
                    }, 500);
                }, 150);

                if (e.keyCode == 13) // touche entrée
                {
                	var resultat_scan_normal = doBarcodeScan(scanner.buffer);
                	if(resultat_scan_normal !== true) //le code ean13 ne correspond pas en l'état à une code ean13 d'un produit
                	{
	                    //analyse du code bar
	                    if (scanner.buffer[0]==2) {
	                    	
	                    	var id_product = parseInt(scanner.buffer.substring(1,7));
	                    	var id_product_attribute = 0;
	                    	
	                    	var product = _loadProductFromId(id_product);
	                    	
	                    	if (product && !product.disponibility_by_shop[ACAISSE.id_pointshop]) {
	                    		sendNotif('redFont','<i class="fa fa-exclamation-circle"></i> Produit désactivé dans ce point de vente');
	                    	} else if (product === undefined) {
						    	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i>2Produit inexistant');
						    } else {
						    	var masseGramme = parseInt(scanner.buffer.substring(7,12));
		                    	
		                    	if (product.has_attributes && product.unity=="grammes") {
									for (var id_attribute in product.attributes) {
										if (product.attributes[id_attribute].attribute_name=='1g') {
											id_product_attribute = id_attribute;
											//masseGramme /= 1000;
										}
									}
								}
		                    	
		                    	var masse = product.unity=='g'?masseGramme:masseGramme/1000;
		                    	
		                    	var prixTTC = Math.round(masse*product.unit_price*100)/100;
		                    	
		                    	addToCart(id_product,id_product_attribute,masseGramme);
						    }
	                    } else if (scanner.buffer[0]==8) {
	                    	idCustomer=scanner.buffer.substring(1,scanner.buffer.length-1);
	                    	$('#customerSearchResultsFilter input').val(idCustomer);
	                    	sendCustomerSearch(true);
	                    }
	                    else {
	                    	sendNotif('redFont','<i class="fa fa-exclamation-circle"></i> '+resultat_scan_normal);
	                    }
                    }
                    
                    scanner.buffer = '';
                }
                else
                {
                    scanner.buffer += String.fromCharCode(e.keyCode);
                }
                break;
        }

    });

    $(document).on('keyup change', 'body', function (e) {
        switch (ACAISSE.scanbarcodeMode)
        {
            case 'action':
                //calcul du montant rentré lors de l'ouverture de la caisse sur chaque ligne
                $('.totalCash').text(function () {
                    var money = numeral(
                            $(this).prev('td').children().data('money')
                            );
                    var result = money.multiply(
                            $(this).prev('td').children().val()
                            );
                    return result.format();
                });

                //calcul du total en euro à l'ouverture, mvt et fermeture
                var totalCash = numeral(0);
                $('li.open .totalCash').each(function () {
                    var cash = numeral().unformat($(this).text());
                    totalCash = totalCash.add(cash);
                });
                $('li.open input.fullTotalCash').val(totalCash.format());
                $('li.open input[name="total_cash"]').val(totalCash.multiply(100).format('0'));

                //calcul des pièces retiré à la préparation
                var totalStayCash = numeral(0);
                $('li.open .takeOffMoney').each(function () {
                    var result = numeral($(this).closest('tr').data('money')).multiply($('input', this).val());
                    totalStayCash = totalStayCash.add(result);
                });
                $('li.open #totalStayCash').val(function () {
                    var totalCashOnClose = numeral().unformat($('#totalCashOnClose').val());
                    return numeral(totalCashOnClose).subtract(totalStayCash).format();
                });

                //calcul des pièces demandés à la préparation
                var totalRequestMoney = numeral(0);
                $('li.open .requestMoney').each(function () {
                    var result = numeral($(this).closest('tr').data('money')).multiply($('input', this).val());
                    totalRequestMoney = totalRequestMoney.add(result);
                });
                $('li.open #totalRequestMoney').val(function () {
                    var totalStay = numeral().unformat($('#totalStayCash').val());
                    return numeral(totalStay).add(totalRequestMoney).format();
                });
                $('.totalRequestMoney').val(totalRequestMoney.multiply(100).format('0'));
                $('.totalTakeOffMoney').val(totalStayCash.multiply(100).format('0'));

                //nombre total de pièce retiré/ajouté
                $('li.open .totalLineMoney input').each(function () {
                    var thisParent = $(this).closest('tr');
                    var moneyIn = parseInt($('[data-cash-on-close]', thisParent).val());
                    var requestMoney = $('.requestMoney input', thisParent).val();
                    var takeOffMoney = $('.takeOffMoney input', thisParent).val();
                    var theoreticalCash = parseInt($('[data-theoretical-cash]', thisParent).val());
                    $(this).val((requestMoney - takeOffMoney) + moneyIn);
                    _setProgressPrepare($(this), theoreticalCash);
                });

                break;
        }
    });
    
    
    var doBarcodeScan = function(ean13) {
		var e = acaisse_products_prices.ean;
		
		if(ACAISSE.mode_debogue === true)
		{
				console.log(['ean13',ean13,e[ean13],e]);
		}				
		if (e[ean13] == undefined)
		{
			// On essaye de biper un ticket de caisse
			if(ean13.substring(0,2) == '87')
			{
				console.log('ticket de caisse trouvé');
				PRESTATILL_CAISSE.customer._loadOrderTpl(false,ean13);
				
				return;
			}
			
			if(true || ACAISSE.mode_debogue === true)
				{console.log(['ean13 not found ',ean13]);}
			//si le code barre n'est pas dans le cache, on intérroge totu de même la serveur avant de notifier que le produit est inexstant.
			$.ajax({
					type: 'post',
					async:false,
					data: {
					  'action': 'loadProductByEAN13',
					  'ean13': ean13,
					  'ajaxRequestID': ++ACAISSE.ajaxCount,
					},
					dataType: 'json',
		    }).done(function (response) {
			    if (response.success == true)
			    { 
			    	console.log(response);
			    	if(response.results !== false)
			    	{
				    	for(id_product in response.results.catalog)
				    	{
				    		var p = response.results.catalog[id_product];
				    		acaisse_products_prices.catalog[id_product] = p;
				    	}
				    	for(ean in response.results.ean)
				    	{
				    		var p = response.results.ean[ean];
				    		acaisse_products_prices.ean[ean] = p;
				    	}
			    	}
			    	else
			    	{
			    		console.log('Erreur OG002403 : product not found on serveur');
			    	}
			    	//acaisse_products_prices.push(response);
				
				} else {
		  			console.log('Erreur OG002404 : ' + response.errorMsg);
		       	}
		   }).fail(function () {
		    	console.log('Erreur OG001404 : Impossible de communiquer avec le serveur. Si l\'erreur persiste veuillez contacter un administateur.');
		   });
		}
		
		if (typeof (e[ean13]) != 'undefined')
		{
			var p = _loadProductFromId(e[ean13].id_product);
			if (p.disponibility_by_shop[ACAISSE.id_pointshop]) {
				
				/* SINCE 2.5.2 */
        		PRESTATILL_CAISSE.executeHook('actionPrestatillDefautScanMode', {'p': p});
        		
        		if(ACAISSE.scanbarcodeMode == 'other')
        		{
        			console.log('test OK');
        			ACAISSE.scanbarcodeMode = 'product';
        			return;
        		} 
        		else if(p.has_attributes === true && e[ean13].id_product_attribute == 0)
				{
					showPopupDeclinaison(e[ean13].id_product);
					return true;
				}
				else
				{
					addToCart(e[ean13].id_product, e[ean13].id_product_attribute);
					return true;
				} 
			} else {
				return 'Produit désactivé dans ce point de vente';
			}
		}
		else
		{
			return 'Produit inexistant';
		}
	};

    
});
