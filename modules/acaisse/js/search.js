var DEBUG_SEARCH_LEVEL = 0;

var progress = null;

function loadProductSearchList() {
	var filter = $('#productSearchResultsFilter input')[0].value.sansAccent().toLowerCase(); // on met tout en lowerCase pour etre insensible à la case
   
    if (filter.length<acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.nb_caract_for_search) {
    	if(DEBUG_SEARCH_LEVEL >= 3)
	    {
	    	console.log('Expression "'+filter+'" is too short less than '+acaisse_pointshops.pointshops[ACAISSE.id_pointshop].datas.nb_caract_for_search + ' caracters');
	    }    	
    	return false;
    }
    
    
    $('#productResearchButton').html('<i class="fa fa-spinner fa-spin"></i>Recherche du serveur en cours...');
	$("#searchLoadSpinner").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
	
	if (progress) {
        progress.abort();
    }
    progress = $.ajax({
        type: 'post',
        data: {
            'action': 'findProduct',
            'research': filter,
            'id_pointshop' : ACAISSE.id_pointshop,
            'ajaxRequestID': ++ACAISSE.ajaxCount,
        },
        dataType: 'json',
    }).done(function (response) {
        if(ACAISSE.mode_debogue === true)
			console.log(['research product response',response]);
        
        var result = response.results;
        var hasMoreResult = false, bool;
        
        for (var i in result) {
        	bool = true; 
        	for (var j in buffer) {
	        	if (buffer[j].indexOf(result[i]+'')!=-1) {
	        		bool = false;
	        		break;
	        	}
        	}
        	if (bool) {
        		if (buffer[1000000]==undefined) {buffer[1000000]=[];}
	    		buffer[1000000].push(result[i]+'');
	    		hasMoreResult = true;
	    	}
        }
        
        if (hasMoreResult) {
        	$('#productResearchButton')
    			.addClass('button-success')
        		.html('<i class="fa fa-list-ul fa-5x"></i><br />Afficher les résultats supplémentaires');
        	$('#productResearchButton').prop('disabled',false);
        	$('#productResearchButton')[0].onclick = function() {
        		this.onclick=function(){};
        		$('#productResearchButton')
        			.removeClass('button-success')
        			.html('');
				generateHtmlResearchProductList(buffer);
        	}; // évènement unique
        } else {
        	$('#productResearchButton')[0].disabled = true;
        	$('#productResearchButton')
        			.removeClass('button-success')
        			.html('Aucun résultat supplémentaire à afficher');
        }
        
        progress = null;
        
    }).fail(function () {
        console.log('Erreur 000129b : Une erreur c\'est produite durant la recherche de client. Si cette erreur persiste veuillez contacter l\'administrateur du site.');
    });
    
    
    
    
    if(DEBUG_SEARCH_LEVEL >= 1)
    {
    	console.log('loadProductSearchList using expression "'+filter+'"');
    }
    
    var show_only_active_product = $('.button.toggle-button.activeProduct').is('.selected');
    
	var buffer = [];	
	var buffer_product = [];
	
	var nbrMatchs = 0;
	for (var i in acaisse_products_prices.catalog) {
		
		var p = _loadProductFromId(i);
		
		if(!p)
		{
			continue;
		}
				
		if (show_only_active_product === true && p.active != 1) {
			continue;
		}	
		if (!p.disponibility_by_shop[ACAISSE.id_pointshop]) {
			continue;
		}
		
		nbrMatchs = 0;

		if(
			$('.button.toggle-button.searchByName').is('.selected')
			&& p.product_name != undefined)
		{	
			
			
			var txt= p.product_name.sansAccent().toLowerCase();
			var index = txt.indexOf(filter);
			if (index!=-1) {
						
				if(DEBUG_SEARCH_LEVEL >= 3)
			    {
			    	console.log('searchByName is active for product "'+p.product_name+'" compare to '+filter);
			    }
						
				if(DEBUG_SEARCH_LEVEL >= 3)
			    {
			    	console.log('index founded '+index);
			    }
				
				nbrMatchs += index==0?2:1;
			}
			else
			{
						
				if(DEBUG_SEARCH_LEVEL >= 6)
			    {
			    	console.log('index NOT founded for "'+p.product_name+'" compare to '+filter);
			    }
				
			}
			
			
			if(p.has_attributes == true)
			{
				for(var j in p.attributes)
				{
					var txt= p.attributes[j].attribute_name.sansAccent().toLowerCase();
					var index = txt.indexOf(filter);
					if (index!=-1) {
						nbrMatchs += index==0?2:1;
					}
				}
			}
		
			if (nbrMatchs>0) {
				if(buffer_product[p.id_product] == undefined)
				{
					buffer_product[p.id_product] = 0;
				}
				buffer_product[p.id_product] += nbrMatchs;
			}	
			
						
		}
		
		if(
			$('.button.toggle-button.searchByRef').is('.selected')
			&& p.reference)
		{
			var txt = p.reference.sansAccent().toLowerCase();
			var index = txt.indexOf(filter);
			
			if (index !=-1) {
				nbrMatchs += index==0?20:10;
			}	
			
			if(p.has_attributes == true)
			{
				for(var j in p.attributes)
				{
					if(p.attributes[j].ref)
					{
						var txt= p.attributes[j].ref.sansAccent().toLowerCase();
						var index = txt.indexOf(filter);
						if (index!=-1) {
							nbrMatchs += index==0?20:10;
						}
					}
				}
			}	
		
			if (nbrMatchs>0) {
				if(buffer_product[p.id_product] == undefined)
				{
					buffer_product[p.id_product] = 0;
				}
				buffer_product[p.id_product] += nbrMatchs;
			}	
			
				
		}
		
		
			
	}
	
	
	if($('.button.toggle-button.searchByEAN13').is('.selected'))
	{			
		if (acaisse_products_prices.ean[filter] != undefined) {
			if(DEBUG_SEARCH_LEVEL >= 5)
		    {
		    	console.log('index NOT founded for EAN "'+filter+'"');
		    }
			if(buffer_product[acaisse_products_prices.ean[filter].id_product] == undefined)
			{
				buffer_product[acaisse_products_prices.ean[filter].id_product] = 0;
			}
			buffer_product[acaisse_products_prices.ean[filter].id_product] += 20;
		}
		else
		{					
			if(DEBUG_SEARCH_LEVEL >= 6)
		    {
		    	console.log('index NOT founded for EAN "'+filter+'"');
		    }			
		}
	}
	
	for(id_product in buffer_product)
	{
		nbrMatchs = buffer_product[id_product];
	
		if (buffer[nbrMatchs]==undefined) {
			buffer[nbrMatchs] = [];
		}
		
		buffer[nbrMatchs].push(id_product); 
	}			
	
	generateHtmlResearchProductList(buffer);	
    setTimeout(function(){
    	$("#searchLoadSpinner").html('<i class="fa fa-search fa-fw"></i>');
	},500);	
}



function generateHtmlResearchProductList(buffer) {
	//ACAISSE.mode_debogue = true;
	
	if(ACAISSE.mode_debogue === true)
		console.log(['buffer',buffer]);
	
	var cles = [], productBuffer=[];
	var nb_lines = 0;
	var type, style, col=0;
	for (var id in buffer) {
		cles.push(id);
	}
	cles.sort(function(x,y) {return y-x;}); // on trie les clés par ordre décroissant
	
	var html = '<table id="productSearchResultTr">';
	
	//html +='<tbody>';
	for (var i=0; i<cles.length; i++) {
		for (var j=0; j<buffer[cles[i]].length; j++) {
			// TODO : on ajoute à l'affichage buffer[cles][j] (cles représente le nombre de match qu'on a eu sur le produits)
			
			type=2;
			if (_loadProductFromId(buffer[cles[i]][j]).has_attributes) {
				style=3;
			} else {
				style=13;
			}
			html += '<tr>';
			//html += '<td id="cell_pdt_'+col+'" class="cellStyleDefault colspan1 rowspan1 cellType'+type+' cellStyle'+style+'" data-idobject="'+buffer[cles[i]][j]+'" data-idobject2="0">' + _injectCellProductContent({id_object:buffer[cles[i]][j], id_object2:0}) + '</td>';
			
			var add_or_choose = '<i class="fa fa-list-ol"></i>';
			
			if (_loadProductFromId(buffer[cles[i]][j]).has_attributes) {
				add_or_choose = '<i class="fa fa-list-ol"></i>';
			}
			
			//html += '<td>' + _injectCellProductContentInSearchResults({id_object:buffer[cles[i]][j], id_object2:0}) + '</td>';
			html += '<td id="cell_pdt_'+col+'" class="cellStyleDefault colspan1 rowspan1 cellType'+type+' cellStyle'+style+'" data-idobject="'+buffer[cles[i]][j]+'" data-idobject2="0">' + _injectCellProductContentInSearchResults({id_object:buffer[cles[i]][j], id_object2:0}) + '</td>'
			html += '</tr>';
			
			
			if (!_loadProductFromId(buffer[cles[i]][j]).has_attributes) { 
				productBuffer.push({
					id_product: buffer[cles[i]][j],
					id_product_attribute: 0,
					row: 'pdt',
					col: col
				});
			}			
			col++;
			nb_lines++;
		}
	}
	//html +='</tbody>';
	html += '</table>';
	
	//var largeur_cell = ($('#acaissepagecms tbody').width() / ACAISSE.nbCol);

	
	$('#productResearchList').html(html);
	//$('#productResearchList td').width(largeur_cell);
	console.log(parseInt(cles.length*65));
	if(parseInt(nb_lines*65) >= $("#searchProducts").height())
	{
		$('#productResearchList').height('100%');
	}
	
	_refreshProductPageStock(productBuffer);
}



/**
 * hanlder sur le sfiltres 
 */
$(function(){
	$('.search-col-filters .toggle-button').click(function(){
		$(this).toggleClass('selected');
		loadProductSearchList();
	});	
	
	$('#searchProducts').on('click tap','#resetSearchInputContent',function(){
		$('#productSearchResultsFilter input').val('');
		$('#productResearchList').html('');
		loadProductSearchList();
	});
	
    
    $('#productKeyboardResearch').lbabKeyboard({ //clavier azerty pour la recherche de produit
        'layout': 'azerty',
        'inputable': $('#productSearchResultsFilter input'),
        onType: function () {
            clearTimeout(scanner.customerKeyIntervalProcess);
            scanner.customerKeyIntervalProcess = setTimeout(function () {
                loadProductSearchList();
            }, 500);
        }
    });
	
	
});
