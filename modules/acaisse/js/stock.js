$(function(){
	
	var DEBUG_LEVEL = 0;
	
	var open_stock_interface = function($tr,id_warehouse,html) {
		$('tr.stock-inline-interface').remove();
		var $interface = $('<tr class="stock-inline-interface"><td colspan="5"><div class="interface-content" style="display:none;">'+html+'</div></td></tr>');
		$tr.after($interface);
		$('.ui-spinner-group.stock',$interface).uiSpinner();
		$('.interface-content',$interface).slideDown();
	};
	
	var send = function(e) {
		
		$('.validate-stock-action').prop('disabled',true);
		
		e.preventDefault();
		$.ajax({
			type: 'post',
			dataType: 'json',
			data: $('.stock-form').serialize(),
		}).done(function (response) {
			//console.log(response);
			$('.validate-stock-action').prop('disabled',false);
			if (response.success)
			{
				_drawProductInfos(response,null,'#stockBtn');
				
				if(response.request.id_warehouse != undefined)
				{						
					$('tr[data-idwarehouse="'+response.request.id_warehouse+'"] .qty')
						.animate({'fontSize':'25px'}).animate({'fontSize':'14px'},200)
						.animate({'fontSize':'22px'}).animate({'fontSize':'14px'},400);					
				}

				if(response.request.id_warehouse_from != undefined)
				{						
					$('tr[data-idwarehouse="'+response.request.id_warehouse_from+'"] .qty')
						.animate({'fontSize':'25px'}).animate({'fontSize':'14px'},200)
						.animate({'fontSize':'22px'}).animate({'fontSize':'14px'},400);					
				}

				if(response.request.id_warehouse_to != undefined)
				{				
					$('tr[data-idwarehouse="'+response.request.id_warehouse_to+'"] .qty')
						.animate({'fontSize':'25px'}).animate({'fontSize':'14px'},200)
						.animate({'fontSize':'22px'}).animate({'fontSize':'14px'},400);					
				}				
			}
			else
			{
				alert('Error o_stock00011');				
			}
		}).fail(function(){
			alert('Error o_stock00010');
		});
	};
	
	var interface_active_warehouse = function($tr,id_warehouse) {
		var html ='';
		
		html += '<input type="hidden" name="action" value="StockActiveProduct" />';
		html += '<input type="hidden" name="id_pointshop" value="'+ACAISSE.id_pointshop+'" />';
		html += '<input type="hidden" name="id_warehouse" value="'+id_warehouse+'" />';
		html += '<input type="hidden" name="is_usable" value="1" />';
		html += '<input type="hidden" name="id_employee" value="'+$('#showEmployee').data('id_employee')+'" />';
		html += '<input type="hidden" name="id_product" value="'+parseInt('0' + $('#popupNumeric').data('idobject'))+'" />';
		html += '<input type="hidden" name="id_product_attribute" value="'+parseInt('0' + $('#popupNumeric').data('idobject2'))+'" />';
			
		
		var location = '';
		var is_active = false;
		var stocks_available = $('#productInfos').data('stocks_available');
		
		if(stocks_available[id_warehouse] != undefined)
		{
			location = stocks_available[id_warehouse];
			is_active = true;
		}		
		
		html += '<div class="flex inline-group">';
			html += '<label for="stock_qu" style="line-height:18px;">Emplacement<br /><small>(Aller, rayon, étagère)</small></label>';
			html += '<input name="location" value="'+$('').text(location).html()+'" />';
		html += '</div>';
		
		html += '<div>';
		html += '	<span class="stock_active_item" data-id="1">Oui</span>';
		html += '	<span class="stock_active_item" data-id="0">Non</span>';
		html += '</div>';
		html += '<input type="hidden" value="'+(is_active?'1':'0')+'" name="is_active" />';
		
		var stock = null;
		$.each($('#productInfos').data('stocks'),function(i,item) {
			if(item.id_warehouse == id_warehouse)
			{
				stock = item;
			}
		});
		return html;
	};
	var interface_add_stock = function($tr,id_warehouse, suppliers) {
		var html ='';
		
		var product = $('#productInfos').data('product');
		
		html += '<form class="stock-form">';
			html += '<input type="hidden" name="action" value="StockAddProduct" />';
			html += '<input type="hidden" name="id_pointshop" value="'+ACAISSE.id_pointshop+'" />';
			html += '<input type="hidden" name="id_warehouse" value="'+id_warehouse+'" />';
			html += '<input type="hidden" name="is_usable" value="1" />';
			html += '<input type="hidden" name="id_employee" value="'+$('#showEmployee').data('id_employee')+'" />';
			html += '<input type="hidden" name="id_product" value="'+parseInt('0' + $('#popupNumeric').data('idobject'))+'" />';
			html += '<input type="hidden" name="id_product_attribute" value="'+parseInt('0' + $('#popupNumeric').data('idobject2'))+'" />';
			
			html += '<div id="supplierBox">';
			if(suppliers != '')
			{
				var table_suppliers = suppliers.split(',');
				html += '<label for="supplier">Fournisseur</label>';
				html +='<select name="supplier" id="supplier">';
					$.each(table_suppliers,function(i,supplier){
						html +='<option value="'+supplier+'">'+supplier+'</option>'
					});
				html +='</select>';
			}
			html += '</div>';
			
			html += '<div id="stockReasonBox">';
			var first_id = 0;
				html += '<label for="mvt_reason">Mouvement</label>';
				html +='<select name="mvt_reason" id="mvt_reason">'; 
					$.each($('#productInfos').data('stockMvtReasons'),function(i,reason){
						
						if(reason.sign == 1)
						{
							if(reason.id_stock_mvt_reason == $('#productInfos').data('PS_STOCK_MVT_INC_REASON_DEFAULT'))
							{
								first_id = reason.id_stock_mvt_reason;
								html += '<option class="stock_mvt_reason_item" value="'+reason.id_stock_mvt_reason+'">'+reason.name+'</option>';
							}
							else
							{
								html += '<option class="stock_mvt_reason_item" value="'+reason.id_stock_mvt_reason+'">'+reason.name+'</option>';
							}
						}
					});
				html +='</select>';
			html += '</div>';
			
			html +='<div id="stockInputsBox">';
				html += '<input type="hidden" value="'+first_id+'" name="id_stock_mvt_reason" />';
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Quantité</label>';
					html += '<span class="stock" data-min="1"><input class="ui-spinner" id="stock_qu" name="stock_qu" value="1" data-min="0" /></span>';
				html += '</div>';
				
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Prix d\'achat unitaire</label>';
					html += '<input name="price_te" value="'+product.wholesale_price+'" />';
					html += '<span class="suffix">€ HT</span>';
				html += '</div>';
			html += '</div>';	
			
			html +='<div class="clearfix"></div>';
			
			html += '<p class="top-spacing">';
				html += '<button class="cancel-stock-action">Fermer</button>';
				html += '<button class="validate-stock-action">Enregistrer le mouvement</button>';
			html += '</p>';
		html += '</form>';
		
		html +='<div class="clearfix"></div>';
		
		return html;
	};
	var interface_remove_stock = function($tr,id_warehouse, suppliers) {
		var html ='';
		
		var product = $('#productInfos').data('product');
		
		html += '<form class="stock-form">';
			html += '<input type="hidden" name="action" value="StockRemoveProduct" />';
			html += '<input type="hidden" name="id_pointshop" value="'+ACAISSE.id_pointshop+'" />';
			html += '<input type="hidden" name="id_warehouse" value="'+id_warehouse+'" />';
			html += '<input type="hidden" name="id_employee" value="'+$('#showEmployee').data('id_employee')+'" />';
			html += '<input type="hidden" name="id_product" value="'+parseInt('0' + $('#popupNumeric').data('idobject'))+'" />';
			html += '<input type="hidden" name="id_product_attribute" value="'+parseInt('0' + $('#popupNumeric').data('idobject2'))+'" />';
			
			html += '<div id="supplierBox">';
			if(suppliers != '')
			{
				var table_suppliers = suppliers.split(',');
				html += '<label for="supplier">Fournisseur</label>';
				html +='<select name="supplier" id="supplier">';
					$.each(table_suppliers,function(i,supplier){
						html +='<option value="'+supplier+'">'+supplier+'</option>'
					});
				html +='</select>';
			}
			html += '</div>';
			
			html += '<div id="stockReasonBox">';
			var first_id = 0;
				html += '<label for="mvt_reason">Mouvement</label>';
				html +='<select name="mvt_reason" id="mvt_reason">'; 
					$.each($('#productInfos').data('stockMvtReasons'),function(i,reason){
						
						if(reason.sign == -1)
						{
							if(reason.id_stock_mvt_reason == $('#productInfos').data('PS_STOCK_MVT_DEC_REASON_DEFAULT'))
							{
								first_id = reason.id_stock_mvt_reason;
								html += '<option class="stock_mvt_reason_item" value="'+reason.id_stock_mvt_reason+'">'+reason.name+'</option>';
							}
							else
							{
								html += '<option class="stock_mvt_reason_item" value="'+reason.id_stock_mvt_reason+'">'+reason.name+'</option>';
							}
						}
					});
				html +='</select>';
			html += '</div>';
			
			html +='<div id="stockInputsBox">';
				html += '<input type="hidden" value="'+first_id+'" name="id_stock_mvt_reason" />';
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Quantité</label>';
					html += '<span class="stock" data-min="1"><input class="ui-spinner" id="stock_qu" name="stock_qu" value="1" data-min="0" /></span>';
				html += '</div>';
				
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Prix d\'achat unitaire</label>';
					html += '<input name="price_te" value="'+product.wholesale_price+'" />';
					html += '<span class="suffix">€ HT</span>';
				html += '</div>';
			html += '</div>';	
			
			html +='<div class="clearfix"></div>';
			
			html += '<p class="top-spacing">';
				html += '<button class="cancel-stock-action">Fermer</button>';
				html += '<button class="validate-stock-action">Enregistrer le mouvement</button>';
			html += '</p>';
		html += '</form>';
		
		html +='<div class="clearfix"></div>';
		
		return html;
	};
	var interface_transfert_stock = function($tr,id_warehouse) {
		var html ='';
		var product = $('#productInfos').data('product');
		
		html += '<form class="stock-form">';
			html += '<input type="hidden" name="action" value="StockTransfertProduct" />';
			html += '<input type="hidden" name="id_warehouse_from" value="'+id_warehouse+'" />';
			html += '<input type="hidden" name="is_usable" value="1" />';
			html += '<input type="hidden" name="id_employee" value="'+$('#showEmployee').data('id_employee')+'" />';
			html += '<input type="hidden" name="id_product" value="'+parseInt('0' + $('#popupNumeric').data('idobject'))+'" />';
			html += '<input type="hidden" name="id_product_attribute" value="'+parseInt('0' + $('#popupNumeric').data('idobject2'))+'" />';
			
			html +='<div id="stockTransertBox">';
				html += '<label>Vers</label> ';
				var first_id = 0;
				html +='<select name="transfert_warehouse" id="transfert_warehouse">';
				$.each($('#productInfos').data('stocks'),function(i,stock){
					if(stock.id_warehouse != id_warehouse)
					{
						if(first_id == 0)
						{
							first_id = stock.id_warehouse;
							html += '<option class="stock_item" value="'+stock.id_warehouse+'">'+stock.name+'</span>';
						}
						else
						{
							html += '<option class="stock_item" value="'+stock.id_warehouse+'">'+stock.name+'</span>';
						}
					}				
				});
				html +='</select>';
			html += '</div>';
			
			html +='<div id="stockInputsBisBox">';
				html += '<div class="inline-group">';
					html += '<label for="stock_qu">Quantité à transférer</label>';
					html += '<span class="ui-spinner-group stock" data-min="1" data-max="'+$('.qty-physique',$tr).text()+'"><input class="ui-spinner" id="stock_qu" name="stock_qu" value="1" /></span>';
				html += '</div>';
			html += '</div>';
			
			html +='<div class="clearfix"></div>';	
				
			html += '<input type="hidden" value="'+first_id+'" name="id_warehouse_to" />';
			
			html += '<p class="top-spacing">';
				html += '<button class="cancel-stock-action">Fermer</button>';
				html += '<button class="validate-stock-action">Enregistrer le transfert</button>';
			html += '</p>';
		html += '</form>';
		
		return html;
	};
	
	
	
	$('#stocksPopup').on('change','#mvt_reason',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on selected mvt reason');
		}
		//console.log($(this).val());
		//$('span.stock_mvt_reason_item').removeClass('checked');
		//$(this).addClass('checked');
		$('[name="id_stock_mvt_reason"]').val($(this).val());
		
	});
	$('#stocksPopup').on('click tap','span.stock_item',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on selected stock');
		}
		$('span.stock_item').removeClass('checked');
		$(this).addClass('checked');
		$('[name="id_warehouse_to"]').val($(this).data('id'));
		
	});
	
	
	$('#stocksPopup').on('click tap','.button.active-warehouse',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on activate warehouse button');
		}
		
		var $tr = $(this).parents('tr');
		var id_warehouse = $tr.data('idwarehouse');
		open_stock_interface($tr,id_warehouse,interface_active_warehouse($tr,id_warehouse));
		
	});
	$('#stocksPopup').on('click tap','.button.add',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on add button');
		}		
		
		var $tr = $(this).parents('tr');
		var id_warehouse = $tr.data('idwarehouse');
		var suppliers = $tr.data('suppliers');	
		window.tr = $tr;
		open_stock_interface($tr,id_warehouse,interface_add_stock($tr,id_warehouse,suppliers));
	});
	$('#stocksPopup').on('click tap','.button.remove',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on remove button');
		}
		
		if($(this).hasClass('disabled')) {
			return;
		}
		
		var $tr = $(this).parents('tr');
		var id_warehouse = $tr.data('idwarehouse');	
		var suppliers = $tr.data('suppliers');	
		open_stock_interface($tr,id_warehouse,interface_remove_stock($tr,id_warehouse,suppliers));
		
	});
	$('#stocksPopup').on('click tap','.button.transfert',function(e){
		if(DEBUG_LEVEL > 1)
		{
			console.log('click on transfert button');
		}
		
		if($(this).hasClass('disabled')) {
			return;
		}
		
		var $tr = $(this).parents('tr');
		var id_warehouse = $tr.data('idwarehouse');		
		open_stock_interface($tr,id_warehouse,interface_transfert_stock($tr,id_warehouse));
		
	});
	
	$('#stocksPopup').on('click tap','.validate-stock-action',function(e){
		send(e);
	});
	
	
	$('#stocksPopup').on('click tap','.cancel-stock-action',function(e){
		e.preventDefault();
		$(this).parents('tr').find('.interface-content').slideUp(function(){ $(this).parents('tr').remove(); });
	});
	
	
	
	
	
});
