
function _recalcAndRefreshTotals() {
	var total_quantity = 0;
	var total_cost = 0;
	var total_cost_ht = 0;
	//total ticket
	if(ACAISSE.ticket.lines)
	{
		for (var i = 0; i < ACAISSE.ticket.lines.length; i++)
		{
			ticket_line = new TicketLine(ACAISSE.ticket.lines[i]);
			total_cost += ticket_line.line_price_ttc;
			total_cost_ht += ticket_line.line_price_ht;
			if(ACAISSE.mode_debogue === true)
			{
				console.log(ticket_line);
			}
			total_quantity += parseInt(ticket_line.quantity);
		}
	}
	ACAISSE.total_cost = total_cost;
	ACAISSE.total_cost_ht = total_cost_ht;
	//ACAISSE.cartModificationLocked = false;
	ACAISSE.total_quantity = total_quantity;
	_refreshTotals();
}

function _refreshTotals() {
	/*
	console.log(['comparaison refreshtot',
		ACAISSE.total_cost,
		ACAISSE.cart_tot_ttc,
		ACAISSE.total_cost_ht,
		ACAISSE.cartModificationLocked,
		ACAISSE.ticket
	]);
	*/
	
	if (ACAISSE.cartModificationLocked == true)
	{
		
		//console.log('TOTO 1');
		
		//console.log('ATTENTION on travail avec un cart');
		//console.log(ACAISSE);
		
		
		//console.log($('#totalCost').html
		//console.log($('#totalCost').html()+' <> '+formatPrice(ACAISSE.cart_tot_ttc) + '€ <small>NET</small>');
		if(ACAISSE.cart_tot_ttc > $('#totalCost').html())
		{
			$('#totalWithoutDiscount').html(ACAISSE.cart_tot).show();
		}
		
		if(ACAISSE.total_cost > ACAISSE.cart_tot_ttc)
		{
			$('#totalWithoutDiscount').html(formatPrice(ACAISSE.total_cost)+'€ <small>TTC (hors remises)</small>').fadeIn();
		}
		else
		{
			$('#totalWithoutDiscount').html('').fadeOut();
		}	
		//si net de tax
		if(ACAISSE.total_cost == ACAISSE.total_cost_ht)
		{
			$('#totalCost').html(formatPrice(ACAISSE.cart_tot_ttc) + '€ <small>NET</small>');
		}
		else
		{	
			$('#totalCost').html(formatPrice(ACAISSE.cart_tot_ttc) + '€ <small>TTC</small>');
		}
		//$('#totalReduction').html('Réduction de ' + formatPrice(ACAISSE.total_cost - ACAISSE.cart_tot_ttc) + '€ TTC incluse');
		$('#totalEstimationMention').hide();
		
		//var module_obj = {action:'reduce',reduce:formatPrice(ACAISSE.total_cost - ACAISSE.cart_tot_ttc),new_tot:(ACAISSE.cart_tot_ttc==0?'?':ACAISSE.cart_tot_ttc)};
		//modules_action(module_obj);
		
		
		var module_obj = {action:'tot',tot:formatPrice(ACAISSE.total_cost),id_ticket:ACAISSE.ticket.ticket_num};
		modules_action(module_obj);
		
		/*
		lcd_write_line('Réduction de ' + formatPrice(ACAISSE.total_cost - ACAISSE.cart_tot_ttc) + '€',2,undefinedValue,undefinedValue,module_obj);
		lcd_write_line(ACAISSE.cart_tot_ttc==0?'':'Total '+ formatPrice(ACAISSE.cart_tot_ttc) + '€',3,'right');
		*/
	}
	else
	{
		
		
		//console.log('TOTO 2');
		
		if(ACAISSE.mode_debogue === true)
			{console.log(modules_action);}
		
		
		
		//si net de tax
		if(ACAISSE.total_cost == ACAISSE.total_cost_ht && parseInt(ACAISSE.total_cost) > 0)
		{
			$('#totalCost').html(formatPrice(ACAISSE.total_cost_ht) + '€ <small>NET</small>'); //il y a ristourne
		}
		else
		{		
			$('#totalCost').html(formatPrice(ACAISSE.total_cost) + '€ <small>TTC</small>');
		}
		
		$('#totalReductionva').html('');
		$('#totalEstimationMention').show();
		
		var module_obj = {action:'tot',tot:formatPrice(ACAISSE.total_cost),id_ticket:ACAISSE.ticket.ticket_num};
		modules_action(module_obj);
		/*
		lcd_write_line(' ',2,undefinedValue,undefinedValue,module_obj);
		lcd_write_line(ACAISSE.total_cost==0?'':'Total '+ formatPrice(ACAISSE.total_cost) + '€',3,'right');
		*/          	

	}
	
	$('#totalItem').html(ACAISSE.total_quantity == 0 ? '0' : ACAISSE.total_quantity > 1 ? ACAISSE.total_quantity + '' : '1');

}
