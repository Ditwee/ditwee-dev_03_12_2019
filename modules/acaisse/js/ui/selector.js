// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

// IMPORTANT remember to change every instance of "uiSelector" to the name of your plugin!
;(function($) {

    // here we go!
    $.uiSelector = function(element, options) {

        // plugin's default options
        // this is private property and is  accessible only from inside the plugin
        var defaults = {
        	multiple:false,
        	onChange:function(new_values) {}
        };

        // to avoid confusions, use "plugin" to reference the current instance of the object
        var plugin = this;

        // this will hold the merged default, and user-provided options
        // plugin's properties will be available through this object like:
        // plugin.settings.propertyName from inside the plugin or
        // element.data('uiSelector').settings.propertyName from outside the plugin, where "element" is the
        // element the plugin is attached to;
        plugin.settings = {};

        var $element = $(element),  // reference to the jQuery version of DOM element the plugin is attached to
             element = element,
             $value = $('input'),
             _value = '';

        // the "constructor" method that gets called when the object is created
        plugin.init = function() {

            // the plugin's final properties are the merged default and user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

            $value = plugin.settings.input == undefined ? $('input',$element) : plugin.settings.input;            
            
            $element.addClass('ui-selector-container');
            
            $element.on('click tap','.item',function(e){
            	
            	if(false && $(this).is('.disabled'))
            	{
            		return;
            	}
            	
            	if(plugin.settings.multiple === false)
            	{
	            	$('.item.selected',$element).removeClass('selected');
	            	$(this).addClass('selected');	       
            	}
            	else
            	{
            		$(this).toggleClass('selected');          		
            	}  
            	_refreshValue();          	           	
            });
            
            $('.item[data-value="'+$value.val()+'"]',$element).click();
        };

    
	    var _refreshValue = function() {    	
			var tmp = [];
			
			$.each($('.item.selected',$element),function(i,elt){
				tmp.push($(this).data('value'));
			});
			_value = tmp.join(';');
			$value.val(_value);
			//console.log(_value);
			plugin.settings.onChange(_value);
	    };


        // fire up the plugin!
        // call the "constructor" method
        plugin.init();

    };

    // add the plugin to the jQuery.fn object
    $.fn.uiSelector = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('uiSelector')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.uiSelector(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('uiSelector').publicMethod(arg1, arg2, ... argn) or
                // element.data('uiSelector').settings.propertyName
                $(this).data('uiSelector', plugin);

            }

        });

    };

})(jQuery);