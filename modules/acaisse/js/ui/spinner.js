/**
 * Travail avec une structure suivante
 *   -  <= en ::before
 *   <input name="...." class="ui-spinner" data-min="" data-max="" [data-increment=""] [data-double-increment=""] [data-allow-press="false"]/>
 * 	 +  <= en ::after
 */


// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

// IMPORTANT remember to change every instance of "uiSpinner" to the name of your plugin!
;(function($) {

    // here we go!
    $.uiSpinner = function(element, options) {
    	
    	var DEBUG_LEVEL = 0;
    	
        var defaults = {
        	align_button : 'right', // 'both-side', 'left' or 'right'
        	'step' : 1,
        	'fast_step' : 10, //when staying press on button
        	'decimal' : false, //only int
        	'min' : null,	//son automatiquement initialiser par $element.data('min')
        	'max' : null,	//son automatiquement initialiser par $element.data('min')
        	'onChange' : function(plugin,$element,value) {},
        	'filter_value' : function(value) { return value==0?'':value; },
			'keyboardInput' : true,
        };

        var plugin = this;
        plugin.settings = {};

        var $element = $(element),  // reference to the jQuery version of DOM element the plugin is attached to
             element = element;        // reference to the actual DOM element
             
        var $minus = $('<span class="ui-spinner-minus">-</span>'),
        	$plus = $('<span class="ui-spinner-plus">+</span>'),
        	$input,
        	value=0;             

        // the "constructor" method that gets called when the object is created
        plugin.init = function() {

            // the plugin's final properties are the merged default and user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

			//init des instances jquery
			$input = $('input',$element);
			$input.prop('readonly',false /*!plugin.settings.keyboardInput*/);
			plugin.settings.min = $element.data('min')!=undefined?$element.data('min'):null;
			plugin.settings.max = $element.data('max')!=undefined?$element.data('max'):null;
			
			value = plugin.settings.decimal === false?parseInt('0'+$input.val()):parseFloat('0'+$input.val());
			
			
			switch(plugin.settings.align_button)
			{
				case 'left':
					$element.prepend($minus);
					$element.prepend($plus);
					break;
				case 'both-side':
					$element.prepend($minus);
					$element.append($plus);
					break;		
				case 'right':
				default:
					$element.append($minus);
					$element.append($plus);
					break;			
			}

            //mise en place des callback sur les bouttons
            $minus.click(function(e){ _decrement(); });
            $minus.mousedown(function(e){ _continious_decrement(); });
            $minus.mouseup(function(e){ _stop_continious_decrement(); });
            
            $plus.click(function(e){ _increment(); });
            $plus.mousedown(function(e){ _continious_increment(); });
            $plus.mouseup(function(e){ _stop_continious_increment(); });
            
            
        
        	$input.keypress(function(e){ 	(e); });
        	$input.keydown(function(e){ _filterKeyDown(e); });
            
        };

        // public methods
        // these methods can be called like:
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // element.data('uiSpinner').publicMethod(arg1, arg2, ... argn) from outside the plugin, where "element"
        // is the element the plugin is attached to;
        plugin.setValue = function(new_value) {
            value = plugin.settings.filter_value(new_value);
            $input
            	.val(value)
            	.change(); //trigger "change" event
            
            //appel du callback de change
        	plugin.settings.onChange(plugin,$element,value);            	
            
        };
        plugin.setMin = function(new_min) {
            plugin.settings.min = new_min;
            if(value<new_min)
            {
            	plugin.setValue(new_min);
            }
        };
        plugin.setMax = function(new_max) {
            plugin.settings.max = new_max;
            if(value>new_max	)
            {
            	plugin.setValue(new_max);
            }
        };

        // private methods
        // these methods can be called only from inside the plugin like:
        // methodName(arg1, arg2, ... argn)
        
        
        var _filterKeyPress = function(e) {        	
        	if(!(
				e.key == 0
				|| e.key == 1
				|| e.key == 2
				|| e.key == 3
				|| e.key == 4
				|| e.key == 5
				|| e.key == 6
				|| e.key == 7
				|| e.key == 8
				|| e.key == 9				
				|| (
					plugin.settings.decimal == true && (						
						e.key == '.'
						|| e.key == ','						
					) 
				)		
			))
			{
				e.preventDefault();			
			}
			else
			{
				value = plugin.settings.decimal === false?parseInt('0'+$input.val()):parseFloat('0'+$input.val());
			}
    	};
		
    	var _filterKeyDown = function(e) {
        	switch(e.keyCode)
        	{
        		case 40: //Down
					e.preventDefault();
        			_decrement();
        			break;
        		case 38: //up
					e.preventDefault();
        			_decrement();
        			break;
        	}
    	};
        

        // a private method. for demonstration purposes only - remove it!
        var _decrement = function(step) {
        	if(step == undefined)
        	{
        		step = plugin.settings.step;
			}
			
			if(DEBUG_LEVEL>6)
			{
				console.log({
					'_decrement' : '',
					'this.value' : value,
					'step' : step,
					'new value' : value-step,
				});
			}
			value = plugin.settings.decimal === false?parseInt('0'+$input.val()):parseFloat('0'+$input.val());
			if(plugin.settings.min !== null && value <= plugin.settings.min)
			{
				return;
			}
			value -= step;
			plugin.setValue(value);
        };
        var _increment = function(step) {
        	if(step == undefined)
        	{
        		step = plugin.settings.step;
        	}
        	
			if(DEBUG_LEVEL>6)
			{
				console.log({
					'_increment' : '',
					'this.value' : value,
					'step' : step,
					'new value' : value+step,
					'max' : plugin.settings.max,
				});
			}
        	value = plugin.settings.decimal === false?parseInt('0'+$input.val()):parseFloat('0'+$input.val());
        	
        	if(plugin.settings.max !== null && value >= plugin.settings.max)
			{
				return;
			}
        	
        	value += step;
			plugin.setValue(value);
        };
        
        var continious_increment_id = null;
        var continious_increment_count = null;
        var _continious_increment = function(step) {
        	continious_increment_id = setInterval(function(){
        		continious_increment_count++;
        		if(continious_increment_count > 10)
        		{        			
        			_increment(plugin.settings.fast_step);
        		}
        		else
        		{
        			_increment(plugin.settings.step);
        		}
        	},200); 	
        };
        
        var _stop_continious_increment = function() {
        	
        	if(continious_increment_id !== null)
        	{
        		clearInterval(continious_increment_id);
        		continious_increment_id = null;
        	}
        	        	
        };
        
        
        
        
        var continious_decrement_id = null;
        var continious_decrement_count = null;
        var _continious_decrement = function(step) {
        	continious_decrement_id = setInterval(function(){
        		continious_decrement_count++;
        		if(continious_decrement_count > 10)
        		{        			
        			_decrement(plugin.settings.fast_step);
        		}
        		else
        		{
        			_decrement(plugin.settings.step);
        		}
        	},200); 	
        };
        
        var _stop_continious_decrement = function() {
        	
        	if(continious_decrement_id !== null)
        	{
        		clearInterval(continious_decrement_id);
        		continious_decrement_id = null;
        	}
        	        	
        };
        

        // fire up the plugin!
        // call the "constructor" method
        plugin.init();
        //plugin.setValue(value);

    };

    // add the plugin to the jQuery.fn object
    $.fn.uiSpinner = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('uiSpinner')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.uiSpinner(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('uiSpinner').publicMethod(arg1, arg2, ... argn) or
                // element.data('uiSpinner').settings.propertyName
                $(this).data('uiSpinner', plugin);

            }

        });

    };

})(jQuery);