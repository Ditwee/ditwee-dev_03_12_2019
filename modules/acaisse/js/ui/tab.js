/**
 * Travail avec une structure suivante
 *   -  <= en ::before
 *   <input name="...." class="ui-spinner" data-min="" data-max="" [data-increment=""] [data-double-increment=""] [data-allow-press="false"]/>
 * 	 +  <= en ::after
 */


// jQuery Plugin Boilerplate
// A boilerplate for jumpstarting jQuery plugins development
// version 1.1, May 14th, 2011
// by Stefan Gabos

// IMPORTANT remember to change every instance of "uiTab" to the name of your plugin!
;(function($) {

    // here we go!
    $.uiTab = function(element, options) {
    	
    	var DEBUG_LEVEL = 0;
    	
        var defaults = {
        	'direction' : 'horizontal', // 'horizontal' or 'vertical' (not implemented)
        	'onChange' : function(plugin,target,$target) {},
        };

        var plugin = this;
        plugin.settings = {};

        var $element = $(element),  // reference to the jQuery version of DOM element the plugin is attached to
             element = element;        // reference to the actual DOM element
             
        var $menu, $content, $barcode_input;             

        // the "constructor" method that gets called when the object is created
        plugin.init = function() {

            // the plugin's final properties are the merged default and user-provided options (if any)
            plugin.settings = $.extend({}, defaults, options);

			//init des instances jquery
			$menu = $('.ui-tab-menu',$element);
			$content = $('.ui-tab-content',$element);
			
			$menu.on('click tap','li',function(){
				
				var target = $(this).data('target');
				if(target == null || target == '')
				{
					alert('You need to define a data-target="selector" on this li');
				}				
				var $target = $(target);
				
				if($target.length == 0)
				{
					alert('No target correspondance for "'+target+'" selector.');
				}
				
				$('.current',$element).removeClass('current');
				$target.addClass('current');
				$(this).addClass('current');
				
				plugin.settings.onChange(this,$(this).data('target'),$target);								
			});		
			
			            
        };

        // public methods
        // these methods can be called like:
        // plugin.methodName(arg1, arg2, ... argn) from inside the plugin or
        // element.data('uiTab').publicMethod(arg1, arg2, ... argn) from outside the plugin, where "element"
        // is the element the plugin is attached to;
        plugin.setValue = function(new_value) {
            value = plugin.settings.filter_value(new_value);
            $input
            	.val(value)
            	.change(); //trigger "change" event
            
            //appel du callback de change
        	plugin.settings.onChange(plugin,$element,value);            	
            
        };

        // private methods
        // these methods can be called only from inside the plugin like:
        // methodName(arg1, arg2, ... argn)
        
        var _toto = function() {
        	
        };
        

        // fire up the plugin!
        // call the "constructor" method
        plugin.init();
        //plugin.setValue(value);

    };

    // add the plugin to the jQuery.fn object
    $.fn.uiTab = function(options) {

        // iterate through the DOM elements we are attaching the plugin to
        return this.each(function() {

            // if plugin has not already been attached to the element
            if (undefined == $(this).data('uiTab')) {

                // create a new instance of the plugin
                // pass the DOM element and the user-provided options as arguments
                var plugin = new $.uiTab(this, options);

                // in the jQuery version of the element
                // store a reference to the plugin object
                // you can later access the plugin and its methods and properties like
                // element.data('uiTab').publicMethod(arg1, arg2, ... argn) or
                // element.data('uiTab').settings.propertyName
                $(this).data('uiTab', plugin);

            }

        });

    };

})(jQuery);