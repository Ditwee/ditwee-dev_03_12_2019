var usb_screen_action = function(p) {};
var action = null;
var total_span = null;
var params = {
	reverse : false
};

$(function() {
  var container = document.querySelector("#container");
  total_span = document.querySelector("#total div span");
  var $div = $('#descript');
  var deletePLV = function(){};
  function formatPrice(price) {
  	var p = price;
  	if (typeof price == 'number') {
  		price=price+'';
  	}
  	while ("0123456789".indexOf(price[price.length-1])==-1) { // on dégage tout les deniers caracères tant que ce ne sont pas des chiffres
  		price = price.substring(0,price.length-1);
  	}
  	if (parseFloat(price)!=price) {
  		throw "Erreur de paramètre à la finction formatPrice(p) avec p=" + p;
  	}
  	var tbl = price.split('.');
  	if (tbl.length==1) {
  		return price;
  	} else if (tbl.length==2) {
  		while (tbl[1].length>2) {
  			tbl[1] = tbl[1].substring(0,tbl[1].length-1);
  		}
  		if (tbl[1].length<2) {
  			tbl[1]+='0';
  		}
  		price = tbl[0]+'.'+tbl[1];
  		return price;
  	} else {
  		return "#numberError";
  	}
  }
  
  function toggleFullScreen() {
    if (!document.mozFullScreen && !document.webkitFullScreen) {
      if (container.mozRequestFullScreen) {
        container.mozRequestFullScreen();
      } else {
        container.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
      }
    } else {
      if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else {
        document.webkitCancelFullScreen();
      }
    }
  }
  
  for (var i=1; i<=5; i++) {
  	$('.item_taille_' + i).css('font-size',(1-(i-1)*0.2)+'em');
  }
  
  document.addEventListener("keydown", function(e) {
    if (e.keyCode == 13) { // entré
      toggleFullScreen();
    }
  }, false);
  
  actions = {
  	payment_exact : function(usb_screen_obj) {
  		deletePLV();
  		$div.html("Payement effectué.<br /><strong>Merci</strong> d'être passé.<br />A bientôt.");
  	},
  	payment_overdue : function(usb_screen_obj) {
  		deletePLV();
  		$div.html("Reçu : " + usb_screen_obj.received + " € | Rendu : " + usb_screen_obj.diff + " €.");
  	},
  	payment_lack : function(usb_screen_obj) {
  		deletePLV();
  		$div.html("Reçu : " + usb_screen_obj.received + " € | Il manque encore : " + usb_screen_obj.diff + " €");
  	},
  	reduce : function(usb_screen_obj) {
  		deletePLV();
  		$div.html("Vous bénéficiez d'une réduction de " + usb_screen_obj.reduce + "€");
  		
  		$('#total span').html(formatPrice(parseFloat($('#total span').html()) - parseFloat(usb_screen_obj.reduce))+'€ TTC');
  	},
  	demo : function(usb_screen_obj) {
  		deletePLV();
  		$div.html("Bonjour");
  	},
  	typed_object : function(usb_screen_obj) { // DEPRECATE NE DEVRAIT PLUS ETRE APPELER EN DIRECT POUR LE MOMENT
  		
  		var line = usb_screen_obj.line;
  		//console.log({line:line});
		var p = line.p;//window.opener._loadProductFromId(line.id_product);
		//console.log({p:p});
		//console.log({'window_opener' : window.opener});
		var id_line = 'screen_line_'+line.id;//+line.id_product + '__' + line.id_attribute;		
		
		var name = line.label;
		var img = line.id_product_attribute == 0 ? (p.image == undefined?null:p.image) : (p.attributes[line.id_product_attribute].image == undefined ? (p.image == undefined?null:p.image) : p.attributes[line.id_product_attribute].image);
		var quantity = line.quantity;
		var price = line.display_prix_final_ttc; //possibilité ici d'optimisé avec prix barré, affichage HT/TTC, etc...'
		
  		//$('#pubBox').html(JSON.stringify(usb_screen_obj));
  		 		
  		deletePLV();
  		$div.html("Ajout de : " + name);
  		  		
  		var unitId = line.unitId;
				
  		//setTimeout(function() {$('.flashed').removeClass('flashed');},2000);
  		
  		var html_content = '<li id="'+id_line+'">';
	    	html_content += '<span><img src="' + (img == undefined || img == null ? '../modules/acaisse/img/noPhoto.jpg' : img) + '" width="50" /></span>';
	    	html_content += '<span class="item_name">'+ name +'</span>';
	    	html_content += '<span class="item_price">' + price + '</span>';
	    	html_content += '<span class="item_qte">' + ('×'+quantity) + '</span>';
    		html_content += '</li>';
    	
    	var $line = $('#'+id_line);
    	
    	if($line.length == 1) //mise à jour d'une ligne existante
    	{
    		$line.html(html_content);
    	}
    	else //insertion d'une nouvelle ligne
    	{
    		if (params.reverse) {
	    		$('#contentUsb ul').prepend(html_content);
	    		$('#contentUsb').animate({scrollTop: 0}, 1000); // on dessend le scroll tout en bas
	    	} else {
	    		$('#contentUsb ul').append(html_content);
	    		setTimeout(function() {
	    			$('#contentUsb').animate({scrollTop: $('#contentUsb')[0].scrollHeight}, 1000); // on dessend le scroll tout en bas
	    		},1);
	    	}
    	}
    	
    	
  	},
  	switch_ticket : function(usb_screen_obj) {
  		deletePLV();
  		$div.html("ticket n°" + usb_screen_obj.id_ticket);
  		
  		$('#total div span').html("0.00€ TTC");
  		$('#contentUsb li').remove();
  		
  		if(usb_screen_obj.ticket.lines != undefined)
  		{
	  		for (var i=0; i<usb_screen_obj.ticket.lines.length; i++) {
	  			actions.typed_object({
	  				action: 'typed_object',
	  				line: usb_screen_obj.ticket.lines[i],
	  				//product: usb_screen_obj.product_list[i],
	  				//id_attribute: usb_screen_obj.product_list[i].id_attribute,
	  				//qte:usb_screen_obj.product_list[i].qte
				});
	  		}
  		}
  	},
  	tot : function(usb_screen_obj) {
  		deletePLV();
  		//$div.html("Ticket n° " + usb_screen_obj.id_ticket + " | Prix total : " + usb_screen_obj.tot + " €");
  		$('#total_usb_screen div span').html(usb_screen_obj.tot + "€ TTC");
  	}
  };
  
  var lastActionStamp = new Date().getTime();
  
  var i=0;
  var scale = 3;
  var interval;
  
  // hauteur et largeur définies en css
  var initialWidth;
  var initialHeight;
  
  var units = {
	from : ['grammes', 'millilitres', 'pièce','pce','pièces',''],
	to : ['/Kg', '/L', '','','',''], // attention rajouter un slash devant les unités
	scale : [1000,1000,1,1,1,1]
  };
  
  function reloadProduct(i) {
		$.ajax({
	        data: {
	        	'action':'getProduct',
	        	'id_plv_screen' : id_plv_screen
	        },
	        type: 'post',
	        dataType: 'json'
	    }).done(function (response) {
	        if (response.success !== false) {
	        	response = response.response;
	            productList[i] = response.product;
            	setProduct(i);
	        }
	    });
  }
  
  function setProduct(i) {
	var percentage = getReductionPercentage(i);
	$('.A'+(i+1))
		.html(
			'<div class="B'+(i+1)+'">'+
				'<div class="productName">'+productList[i].name+'</div>'+
				'<div class="productDescription">'+/*productList[i].desccription_short+*/'</div>'+
				'<div class="productPrice">'+getPrice(i)+'</div>'+
				((percentage!='0%' && percentage!='NaN%')?('<div class="productReduction">-'+getReductionPercentage(i)+'&nbsp;<!--*<span class="reductionValidity">*Offre valable du 00/00/0000<br />au 00/00/0000</span>--></div>'):'')
			+'</div>'
		).css('background',"url(http://"+productList[i].url_image+") no-repeat center center")
		.css('background-size',initialWidth+'px '+initialHeight+'px');
  }
  
  /**
   * @param i : index du tableau productList
   * @return price : prix formaté avec l'€ et l'unité
   */
  function getPrice(i) {
	var product = productList[i];
	
	var unitsIndex = units.from.indexOf(product.unity);
	
	var price = product.price*units.scale[unitsIndex];
	
	if (price==parseInt(price)) {
		price=price+'€'+units.to[unitsIndex];
	} else {
		price=price.toFixed(2)+'€'+units.to[unitsIndex];
	}
	
	return price;
  }

  /**
 * @param i : index du tableau productList
 * @return percentage : reduction formaté avec le %
 */
  function getReductionPercentage(i) {
	var product = productList[i];
	
	var percentage = Math.round((100*product.reduction)/(product.price+product.reduction));
	
	return percentage+'%';
  }
  
	usb_screen_action = function(usb_screen_obj) {
		actions[usb_screen_obj.action](usb_screen_obj);
	};
  
   window.opener.window.hooks.payment_exact.push({window:window,func:'payment_exact'});
   window.opener.window.hooks.payment_overdue.push({window:window,func:'payment_overdue'});
   window.opener.window.hooks.payment_lack.push({window:window,func:'payment_lack'});
   window.opener.window.hooks.reduce.push({window:window,func:'reduce'});
   window.opener.window.hooks.demo.push({window:window,func:'demo'});
   window.opener.window.hooks.typed_object.push({window:window,func:'typed_object'});
   window.opener.window.hooks.switch_ticket.push({window:window,func:'switch_ticket'});
   window.opener.window.hooks.tot.push({window:window,func:'tot'});
  
  
});