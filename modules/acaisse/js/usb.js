var usb_screen; // page html de l'ecran usb client
var windows = [];

var hooks = {
	'payment_exact':[],
  	'payment_overdue':[],
  	'payment_lack':[],
  	'reduce' :[],
  	'demo':[],
  	'typed_object':[],
  	'switch_ticket':[],
  	'tot':[]
};


function modules_action(module_obj) {
	
	//console.log({'modules_action' : module_obj});
	
	var hooksToCall = hooks[module_obj.action];
	for (var i=0; i<hooksToCall.length; i++) {
		if (hooksToCall[i].window.window) { // si la fenetre existe toujours
			
			// console.log('test : ');
			// console.log('test : ' + hooksToCall[i].func);
			
			hooksToCall[i].window.actions[hooksToCall[i].func](module_obj); // on l'utilise
		} else { // sinon 
			hooksToCall.splice(i,1); // on supprime le hook
		}
	}
}

/***
	var module_obj = {action:'payment_exact'};
	var module_obj = {action:'payment_overdue',received:formatPrice(received),diff:formatPrice(diff)};
	var module_obj = {action:'payment_lack',received:formatPrice(received),diff:formatPrice(diff)};
	var module_obj = {action:'demo'};
	MODIFIED AND PUBLIC CALL DEPRECATE var module_obj = {action:'typed_object',product:p,id_attribute:(a!=null?id_product_attribute:null),qte:qte};
	var module_obj = {action:'reduce',reduce:formatPrice(total_cost - ACAISSE.cart_tot),new_tot:(ACAISSE.cart_tot==0?'?':ACAISSE.cart_tot)};
	var module_obj = {action:'tot',tot:formatPrice(total_cost)};
	MODIFIED var module_obj = {action:'switch_ticket',ticket:ticket};
***/