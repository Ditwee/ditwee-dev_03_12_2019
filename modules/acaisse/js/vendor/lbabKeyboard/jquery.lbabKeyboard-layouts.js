var layouts = {
    azerty: [
        {
            normal: ['`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 'backspace'],
            shift: ['~', '!', '@', '#', '%', '^', '&', '*', '(', ')', '_', '+', '=', 'backspace'],
        },
        {
            normal: ['tab', 'a', 'z', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\\'],
            shift: ['tab', 'A', 'Z', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\\'],
        },
        {
            normal: ['capsLock', 'q', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', '\'', 'return'],
            shift: ['capsLock', 'Q', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', '%', 'return'],
        },
        {
            normal: ['shift', 'w', 'x', 'c', 'v', 'b', 'n', ',', '.', ';', '/', 'reset'],
            shift: ['shift', 'W', 'X', 'C', 'V', 'B', 'N', '?', '.', '/', '§', 'reset'],
        },
        {
            normal: ['space'],
            shift: ['space'],
        },
    ],
    qwerty: [
        {
            normal: ['`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 'backspace'],
            shift: ['`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', 'backspace'],
        },
        {
            normal: ['tab', 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\\'],
            shift: ['tab', 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\\'],
        },
        {
            normal: ['capsLock', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', 'return'],
            shift: ['capsLock', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ';', '\'', 'return'],
        },
        {
            normal: ['shift', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', 'shift'],
            shift: ['shift', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.', '/', 'shift'],
        },
        {
            normal: ['space'],
            shift: ['space'],
        },
    ],
    numeric: [
        {
            normal: ['backspace'],
            shift: ['backspace'],
        },
        {
            normal: ['7', '8', '9'],
            shift: ['7', '8', '9'],
        },
        {
            normal: ['4', '5', '6'],
            shift: ['4', '5', '6'],
        },
        {
            normal: ['1', '2', '3'],
            shift: ['1', '2', '3'],
        },
        {
            normal: ['0', 'return'],
            shift: ['0', 'return'],
        },
    ],
    floatNumeric: [
        {
            normal: ['backspace'],
            shift: ['backspace'],
        },
        {
            normal: ['7', '8', '9'],
            shift: ['7', '8', '9'],
        },
        {
            normal: ['4', '5', '6'],
            shift: ['4', '5', '6'],
        },
        {
            normal: ['1', '2', '3'],
            shift: ['1', '2', '3'],
        },
        {
            normal: ['0', '.', 'floatReturn'],
            shift: ['0', '.', 'floatReturn'],
        },
    ],
    money: [
        {
            normal: ['backspace'],
            shift: ['backspace'],
        },
        {
            normal: ['7', '8', '9'],
            shift: ['7', '8', '9'],
        },
        {
            normal: ['4', '5', '6'],
            shift: ['4', '5', '6'],
        },
        {
            normal: ['1', '2', '3'],
            shift: ['1', '2', '3'],
        },
        {
            normal: ['0', '.','reset'],
            shift: ['0', '.','reset'],
        },
    ]
};
