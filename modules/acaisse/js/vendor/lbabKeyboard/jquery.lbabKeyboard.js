(function ($) {
    // Declaration de notre plugin
    $.lbabKeyboard = function (element, options) {

        // Mise en place des options par défaut
        var defaults = {
            'layout' : 'azerty',
            'inputable' : null,
            min : false,
            max : false,
            //'createInputable' : true,
            onType : function(){}, //callback
            onReset : function(){},
            onReturn : function() {},
        };

        //instance de notre plugin
        var plugin = this;
        
        // les options de notre plugin
        plugin.options = {};

        // Référence à l'élément jQuery que le plugin affecte
        var $element = $(element);
        // Référence à l'élément HTML que le plugin affecte
        var element = element;
        
        var keyLook = {
            shift: false,
            capsLock: false,
            uppercase: false,
        };
        
        var functionKeys = {
            backspace: {
                text: 'C',
                class : 'double',
            },
            return: {
                text: 'OK',
                class : 'double',
            },
            floatReturn : {
            	text: 'OK'
            },
            shift: {
                text: 'Shift',
                class : 'triple',
            },
            space: {
                text: 'Espace',
                character: ' ',
            },
            capsLock: {
                text: 'Caps lock',
                class : 'double',
            },
            tab: {
                text: 'Tab',
                class : 'double',
                character: '\t',
            },
            reset: {
                text: 'Reset',
            }
        };
        // La méthode dite "constructeur" qui sera appelée
        // lorsque l'objet sera crée
        plugin.init = function () {

            // on stocke les options dans un objet en fusionnant
            // les options par defaut et celle ajoutée en parametre
            plugin.options = $.extend({}, defaults, options);
            /*private methode*/
            createKeyboard(plugin.options.layout);  
            event();
        };
        
        createKeyboard = function (layout) {
            $element.addClass('lbabK');
            $element.addClass(layout ==='floatNumeric'?'floatNumeric':(layout!=='numeric' && layout!=='money')?'alpha':'numeric');
            $element.addClass(layout);
            var keyboardContainer = $('<ul/>');
            layouts[layout].forEach(function (line, index) {
            //layouts[layout][style].forEach(function (line, index) {
                var lineContainer = $('<li/>').addClass('lbabK-line');
                lineContainer.append(createLine(line));
                keyboardContainer.append(lineContainer);
            });
            $element.html('').append(keyboardContainer);
            
            if(plugin.options.inputable === null){
                createInput();
            }
            else if(plugin.options.inputable !== null && plugin.options.inputable instanceof jQuery){
                //console.log('input dans inputable');
            }
        };
        
        var createLine = function (line) {
            var lineContainer = $('<ul/>');
            
            line['normal'].forEach(function (key, indexKey) {
                //touche symbol
                var shiftKey = line['shift'][indexKey];
                var keyContainer = $('<li/>').addClass('lbabK-key unselectable');
                
                if (functionKeys[key] || functionKeys[shiftKey]) {
                    keyContainer.addClass(key).html(functionKeys[key].text).data('lbabK-command', shiftKey);
                }
                else{
                    var keyCell = $('<span/>').html(key).data('lbabK-command', key);
                    var shiftCell = $('<span/>').addClass('none lbabK-shift').html(shiftKey).data('lbabK-command', shiftKey);
                    if(/[a-zA-Z]/.test(key) || /[a-zA-Z]/.test(shiftKey)){ //si c'est une lettre
                        keyCell.addClass('lbabK-letter');
                        shiftCell.addClass('lbabK-letter');
                    }
                     else{
                        keyCell.addClass('lbabK-symbol');
                        shiftCell.addClass('lbabK-symbol');
                    }
                    keyContainer.append(keyCell, shiftCell);
                }
                lineContainer.append(keyContainer);
            });
            
            return lineContainer;
        };
        
        var createInput = function() {
            var inputable = $('<input/>').addClass('lbabK-inputable ' + plugin.options.layout);
            plugin.options.inputable = inputable;
            $element.prepend(inputable);
        };
        
        var event = function(){
            $element.on('click tap','.lbabK-key',function(){
                var Action = new eventAction();
                if(functionKeys.hasOwnProperty($(this).data('lbabK-command'))){
                    //si on clique sur un touche d'action (functionKeys)
                    //on apelle une fonction dynamic privée pour l'action clique
                    Action[$(this).data('lbabK-command')+"Event"]();
                }
                else{
                    //sinon, c'est une touche normal
                    Action.pressKey($('span:visible',this).html());
                }
            });
        };
        
        var eventAction = function() {
            this.inputable = plugin.options.inputable;
            this.character = '';
            this.addCharacter = function(){
                var sentence = this.inputable.val();
                this.inputable.val(sentence + this.character);
                //this.inputable.focus();
                if(keyLook.shift === true){
                    this.shiftEvent();
                }
                checkErrors();
                //plugin.options.onType.call(this);
                callBack('onType',this.inputable.val());
            };
            this.backspaceEvent = function () {
                //s'il l'input n'est pas vide
                if (this.inputable.val().length > 0) {
                    this.inputable.val(this.inputable.val().substring(0, (this.inputable.val().length - 1)));
                }
                checkErrors();
                callBack('onType',this.inputable.val());
            };
            this.returnEvent = function () {
            	if(!$element.hasClass('error'))
            	{
               	 	callBack('onReturn',this.inputable.val());
               	}
            };
            this.floatReturnEvent = function() {
            	if(!$element.hasClass('error'))
            	{
               	 	callBack('onReturn',this.inputable.val());
               	}
            };
            this.shiftEvent = function(){
                keyLook.uppercase === true ? keyLook.uppercase = false : keyLook.uppercase = true;
                keyLook.shift === true ? keyLook.shift = false : keyLook.shift = true;
                $('.lbabK-key span',$element).toggleClass('none');
                   
            };
            this.spaceEvent = function(){
                this.character = functionKeys.space.character;
                this.addCharacter();
            };
            this.capsLockEvent = function(){
                keyLook.uppercase === true ? keyLook.uppercase = false : keyLook.uppercase = true;
                $('.lbabK-key span',$element).toggleClass('none');
            };
            this.tabEvent = function(){
                this.character = functionKeys.tab.character;
                this.addCharacter();
            };
            this.resetEvent = function(){
                this.inputable.val('');
                callBack('onReset');
            };
            this.pressKey = function(htmlKey){
                this.character = htmlKey;
                if(keyLook.uppercase === true){
                    this.character = this.character.toUpperCase();
                }
                this.addCharacter();
            };
        };
        
        var callBack = function(func,args){
            if (typeof plugin.options[func] === 'function' && plugin.options[func] !== '') {
                 return plugin.options[func](args,$element);
            }
            else{
                alert('bad function');
                return 'bad function';
            }
        };
        
        var checkErrors = function(){
        	var hasError = false;
        	if(
        		plugin.options.min !== false
        		&& plugin.options.inputable.val() < plugin.options.min
        	)
        	{
        		hasError = true;
        	}
        	if(
        		plugin.options.max !== false
        		&& plugin.options.inputable.val() > plugin.options.max
        	)
        	{
        		hasError = true;        		
        	}
        	
        	if(hasError === true)
        	{
        		$element.addClass('error');
        	}
        	else
        	{
        		$element.removeClass('error');
        	}
        };
        
        /*function public*/
        plugin.setMin = function(min) {
            plugin.options.min = min;
        };
        plugin.setMax = function(max) {
            plugin.options.max = max;
        };
        plugin.unsetMin = function() {
            plugin.options.min = false;
        };
        plugin.unsetMax = function() {
            plugin.options.max = false;
        };
        
        plugin.revalidateValue = function() {
        	checkErrors();
        };

        // On appele la méthode publique init
        // qui va se charger de mettre en place
        // toutes les méthodes de notre plugin
        // pour qu'il fonctionne
        plugin.init();

    };

    // On ajoute le plugin à l'objet jQuery $.fn
    $.fn.lbabKeyboard = function (options) {

        // Pour chacuns des élément du dom à qui
        // on a assigné le plugin
        return this.each(function () {
            // Si le plugin n'as pas deja été assigné à l'élément
            if (undefined == $(this).data('lbabKeyboard')) {

                // On crée une instance du plugin
                // avec les options renseignées
                var plugin = new $.lbabKeyboard(this, options);

                // on stocke une référence de notre plugin
                // pour pouvoir accéder à ses méthode publiques
                // (non utilisé dans ce plugin)
                $(this).data('lbabKeyboard', plugin);

            }

        });

    };

})(jQuery);