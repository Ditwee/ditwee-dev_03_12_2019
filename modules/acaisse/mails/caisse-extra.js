/**********************
 * GEstionnaire de l'onglet extra/option du module de caisse 
 */
$(function(){
	
	$('.tab-button').on('click tap',function(){
        $tab=$('#'+$(this).data('tab-id'));
        if($(this).hasClass('unselectable')){
            $('.alert_message', $(this)).fadeIn();
            $('.alert_message', $(this)).delay(1000).fadeOut();
            return;
        }
        else{
			$tab=$('#'+$(this).data('tab-id'));
			$('li',$tab.parent()).removeClass('open');
			$tab.addClass('open');
			$('.tab-button',$(this).parent()).removeClass('open');
			$(this).addClass('open');
        }
	});
});
