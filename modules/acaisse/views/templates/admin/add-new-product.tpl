<div id="addNewProductContainer" class="closed">
	
<script type="text/javascript">
	var noTax,taxesArray,ecotaxTaxRate,ecotax_tax_excl,ecotax_tax_incl,priceTE,priceTI,currency;
	var priceDisplayPrecision = 5;

	var addProductToCatalogValidateDatas = function() {
		
		var resu = true;
		
		//validation du nom du produit
		if($('#product-edit-name').val() == '')
		{
			resu = false;
		}
		
		//validation du prix
		if(isNaN($('#product-edit-price').val()))
		{
			resu = false;
		}
		
		return resu;
	}
	

	var addproductToCatalogHandler = function(){		
		var $bu = $('#product-edit-save');
		var qu = parseInt($('#product-edit-qu-to-add').val().replace(/,/g, '.')); 
		$bu.prop('disabled',false).removeClass('info danger success disabled');
		
		//if($('#remove_after_payment_validation').is(':checked'))
		if($('#remove_after_payment_validation').val() == 0)
		{
			if(qu>0)
			{
				$bu.addClass('success').html('Enregistrer le produit dans le catalogue <br />et l\'ajouter '+qu+' fois au ticket en cours');
			}
			else
			{
				$bu.addClass('info').html('Enregistrer le produit dans le catalogue sans l\'ajouter au ticket en cours');
			}
		}
		else
		{
			if(qu>0)
			{
				$bu.addClass('success').html('Ajouter '+qu+' fois ce produit <strong>éphémère</strong> au ticket en cours');
			}
			else
			{
				$bu.addClass('danger disabled').html('Veuillez choisir la quantité à ajouter au ticket').prop('disabled',true);
			}
		}
		
		if(!addProductToCatalogValidateDatas())
		{
			$bu.addClass('disabled').prop('disabled',true).html('Veuillez renseigner les champs obligatoires');
		}		
	};
	
$(function(){
	
	$('#addNewProductContainer .tab-bar .tab-button').on('click tap',function() {
		$('#addNewProductContainer .content > div').css('display','none');
		$('#addNewProductContainer #'+$(this).data('pageId')).css('display','block');
	});
	
	$('body').on(
		'change keyup tap',
		'#product-edit-eco-tax,#product-edit-price_editor,#product-edit-id_tax_rules_group,#product-edit-tax',
		calcPrice
	);
	
	$('body').on(
		'change keyup tap',
		'#product-edit-eco-tax,#product-edit-tax,#product-edit-price_editor,#product-edit-id_tax_rules_group,#product-edit-name,#remove_after_payment_validation,#product-edit-qu-to-add',
		addproductToCatalogHandler		
	);
	
	$('form#add_new_product').submit(function(e){
		e.preventDefault();
		//validation du formulaire
		if(!addProductToCatalogValidateDatas())
		{
			alert('Une erreur est survenue. Veuillez correctement renseigner les éléments obligatoire');
		}
		else
		{
			addProductController.sendForm();
		}
	});
	
	$('body').on('click tap','#product-edit-save',function(){
		$('form#add_new_product').submit();
		closeAllPanel();
	});
	
	var showFace = 1;
	
	$('#product-edit-middle-panel legend').click(function() {
		if(showFace == 1)
		{
			$('#product-edit-middle-panel .content-face1').css('-webkit-transform','perspective(800) rotateY(180deg)');
			$('#product-edit-middle-panel .content-face2').css('-webkit-transform','perspective(800) rotateY(0deg)');
			$('#product-edit-middle-panel .fa').removeClass('fa-toggle-off').addClass('fa-toggle-on');
			showFace = 2;
		}
		else
		{
			$('#product-edit-middle-panel .content-face2').css('-webkit-transform','perspective(800) rotateY(180deg)');
			$('#product-edit-middle-panel .content-face1').css('-webkit-transform','perspective(800) rotateY(0deg)');
			$('#product-edit-middle-panel .fa').removeClass('fa-toggle-on').addClass('fa-toggle-off');
			showFace = 1;
			
		}
		
	});
	
	addproductToCatalogHandler();
});
	
	
/****** class *******/
function AddProductControllerCore() {
	
	this.sample_property = 'exemple';
	
	var recursiveAddCategoryTree = function(tree,open_id) {
		for(index in tree)
		{
			var cat = tree[index];
			var prefix = '';
			for(var i = 1 ; i < cat.level_depth ; i++)
			{
				prefix += '—';
			}
			$('#product-edit-id_default_category')
				.append('<option'+(cat.id_category == open_id?' selected="selected"':'')+' value="'+cat.id_category+'">'+prefix+' '+cat.name+'</option>');
				
			if(cat.children != undefined && cat.children.length > 0);
			{
				recursiveAddCategoryTree(cat.children,open_id);
			}
			
		}
	};
	
	this.sendForm = function() {
		$.ajax({
	        data: $('form#add_new_product').serialize(),
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	    	if(response.success === true)
	    	{
	    		addToCart(response.response.id_product,0,$('#product-edit-qu-to-add').val());
	    	}
	    	else
	    	{	    		
	    		alert('Error OG120302 : l\'opération n\'a pas pû aboutir, veuillez vérifier vos données.');
	    	}
	    }).fail(function() {
	    	alert('Error OG120300');	    	
	    });
	};
	
	this.resetForm = function() {
		$('form#add_new_product').get(0).reset();
		$.ajax({
	        data: {
	            'action': 'loadAddNewProductFormDatas',	            
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	    	if(response.success === true)
	    	{
	    		var response = response.response;
	    		
	    		//traitements des fournisseurs
	    		var suppliers = response.suppliers;
	    		//on vide le sélecteur sauf le premier choix
	    		$('#product-edit-id_default_supplier option').each(function(i,elt){
	    			if(i>0)
	    			{
	    				$(elt).remove();
	    			}
	    			
	    		});
	    		for(index_supplier in suppliers)
	    		{
	    			var supplier = suppliers[index_supplier];
	    			$('#product-edit-id_default_supplier')
	    				.append('<option value="'+supplier.id_supplier+'">'+supplier.name+'</option>');
	    		}
	    		
	    		
	    		//traitement des marques/fabriquants
	    		var manufacturers = response.manufacturers;
	    		//on vide le sélecteur sauf le premier choix
	    		$('#product-edit-id_manufacturer option').each(function(i,elt){
	    			if(i>0)
	    			{
	    				$(elt).remove();
	    			}
	    			
	    		});
	    		for(index_manufacturer in manufacturers)
	    		{
	    			var manufacturer = manufacturers[index_manufacturer];
	    			$('#product-edit-id_manufacturer')
	    				.append('<option value="'+manufacturer.id_manufacturer+'">'+manufacturer.name+'</option>');
	    		}
	    		
	    		//traitement des groupes de TVA
	    		$('#product-edit-id_tax_rules_group option').remove();
	    		var vat_groups = response.tax_rules_groups;
	    		for(index_vat_group in vat_groups)
	    		{
	    			var vat_group = vat_groups[index_vat_group];
	    			$('#product-edit-id_tax_rules_group')
	    				.append('<option'+(vat_group.id_tax_rules_group == response.default_id_tax_rules_group?' selected="selected"':'')+' value="'+vat_group.id_tax_rules_group+'">'+vat_group.name+'</option>');
	    		}
	    		
	    		ecotaxTaxRate = response.ecotaxTaxRate;
	    		noTax = response.tax_exclude_taxe_option == true;
				taxesArray = response.tax_rates;
				currency = response.currency;
	    		
	    		//traitement des categories
	    		$('#product-edit-id_default_category option').remove();
	    		var categories_tree = response.categories_tree;
	    		recursiveAddCategoryTree(categories_tree,response.id_default_category);
	    	}
	    }).fail(function(){
	    	alert('Error OG120301');
	    });
	};
	
		

}

var addProductController = new AddProductControllerCore();
	
</script>
   <div class="content">
        {*
        <ul class="tab-bar" unselectable="on" style="-webkit-user-select: none;">
            <li class="tab-button open button" data-page-id="pageNewProduct" unselectable="on" style="-webkit-user-select: none;"><i class="fa fa-pencil-square-o"></i> Créer un nouveau produit</li>
            <li class="tab-button button" data-page-id="pageNewProductMemo" unselectable="on" style="-webkit-user-select: none;"><i class="fa fa-bolt"></i> Mémo</li>
        </ul>
        *}
        
        <div id="pageNewProduct" class="tab-content">
        	<h1>Créer un produit à la volée<span class="closeBuPanel">X</span></h1>
        	<form id="add_new_product">
        		<input type="hidden" name="action" value="sendAddNewProductFormDatas" />
	            <fieldset>
	                <legend>Informations obligatoires</legend>
	                
	                {* nom du produit *}
	                <div>
	                    <label for="product-edit-name">Nom * : </label>
	                    <input type="text" id="product-edit-name" name="product-edit-name" value="" />
	                    <span id="product-edit-error-name">Nom invalide</span>
	                </div>
	                
	                {* prix de vente *}
	                <div>
	                    <label for="product-edit-price_editor">Prix de vente * : </label>
	                    <div>
	                    <input type="hidden" id="product-edit-price" name="product-edit-price" value="0" readonly />
	                    <input type="text" id="product-edit-price_editor" name="product-edit-price_editor" value="" style="width:76%; margin-right:0;" />
	                    <select id="product-edit-tax" name="product-edit-tax" style="width:15%; margin-left:0;">
	                    	<option value="TI">TTC</option>
	                    	<option value="TE">HT</option>	                    	
	                    </select>
	                    </div>
	                    <div id="price_conversion">Soit <span id="converted_price_value">0</span><span id="convert_price_currency">&euro;</span> <span id="converted_price_vat">HT</span></div>
	                    <span id="product-edit-error-price">Prix TTC invalide</span>
	                </div>
	                
	                <div>
	                	{*<label for="product-edit-eco-tax"><span id="converted_eco_tax">Eco taxe TTC (inclus dans le prix de vente TTC) : </label>*}
	                	<input type="hidden" id="product-edit-eco-tax" name="product-edit-eco-tax" value="0" />
	                    <span id="product-edit-error-name">Eco-taxe invalide</span>
	                </div> 
	                
	                {* group de TVA applicable à ce produit *}
	                <div>
	                    <label for="product-edit-id_tax_rules_group">TVA applicable : </label>
	                    <select type="text" id="product-edit-id_tax_rules_group" name="product-edit-id_tax_rules_group" value="">
	                    </select>
	                </div>
	                
	                {* Quantité en stock *}
	                <div class="info">
	                    <p>Le produit nouvellement créé ne sera pas visible sur votre site en ligne.</p>
	                    <p>Son stock sera de 0 unité avec la vente hors stock autorisée afin de pouvoir l'ajouter immédiatement à votre ticket en cours.</p>
	                </div>
	            </fieldset>
	            <fieldset id="product-edit-middle-panel">
	            	<div class="content-face1">
		            	<legend>Informations optionnelles <i class="fa fa-toggle-off pull-right"> </i></legend>
	                
		                {* Prix d'achat HT *}
		                <div>
		                    <label for="product-edit-wholesale_price">Prix d'achat : </label>
		                    <input type="text" id="product-edit-wholesale_price" name="product-edit-wholesale_price" value="" placeholder="0" />
		                    <span id="product-edit-error-wholesale_price">Prix d'achat invalide</span>
		                </div>
		                
		                {* EAN *}
		                <div>
		                    <label for="product-edit-ean">Code barre EAN : </label>
		                    <input type="text" id="product-edit-ean" name="product-edit-ean" value="" />
		                    <span id="product-edit-error-ean">Code EAN invalide</span>
		                </div>
		                
		                {* reference *}
		                <div>
		                    <label for="product-edit-reference">Référence : </label>
		                    <input type="text" id="product-edit-reference" name="product-edit-reference" value="" />
		                    <span id="product-edit-error-reference">Référence invalide</span>
		                </div>
		                
		                {* fournisseur *}
		                <div>
		                    <label for="product-edit-id_default_supplier">Fournisseur par défaut : </label>
		                    <select type="text" id="product-edit-id_default_supplier" name="product-edit-id_default_supplier" value="">
		                    	<option value="0">Non renseigné</option>
		                    </select>
		                </div>
		                
		                {* fabricant/marque *}
		                <div>
		                    <label for="product-edit-id_manufacturer">Fabricant/marque : </label>
		                    <select type="text" id="product-edit-id_manufacturer" name="product-edit-id_manufacturer" value="">
		                    	<option value="0">Non renseigné</option>
		                    </select>
		                </div>
	                </div>
	                <div class="content-face2">
		            	<legend>Informations optionnelles <i class="fa fa-toggle-off pull-right"> </i></legend>
	
	                	{* description_courte *}
		                <div>
		                    <label for="product-edit-description_short">Description courte : </label>
		                    <textarea id="product-edit-description_short" name="product-edit-description_short"></textarea>
		                </div>
		                
	                	{* description *}
		                <div>
		                    <label for="product-edit-description">Description : </label>
		                    <textarea id="product-edit-description" name="product-edit-description"></textarea>
		                </div>
	                </div>
	            </fieldset>
	            
	            <fieldset>
	                <legend>Catégorie</legend>
	                <div>
	                    <label for="product-edit-id_default_category">Catégorie principale : </label>
	                    <select type="text" id="product-edit-id_default_category" name="product-edit-id_default_category" value="">
	                    </select>
	                </div>
	            </fieldset>   
	            
                <fieldset>
	                <legend>Sauvegarde</legend>
	                <div>
		                <div>
		                	<input style="width:20%" type="hidden" name="remove_after_payment_validation" value="0" id="remove_after_payment_validation" checked />
		                	{*<label style="width:75%; display:inline-block;">Ajouter ce produit au catalogue</label> *}
	                	</div>
		                
		                <div>Quantité à ajouter au ticket: <input type="number" min="0" name="product-edit-qu-to-add" id="product-edit-qu-to-add" value="1" /></div>
		                
	            		<div class="button" id="product-edit-save">Veuillez renseigner les champs obligatoires</div>
		                
					</div>
					
					
            	</fieldset> 
	            <input type="hidden" id="product-edit-id" name="product-edit-id" value=""/>
	        </form>
       		<!--<span>//@TODO lister les champs qui pourront être éditer ici (pomotion, numéro étudiant...), etc...</span>--> 
        </div>
        
        <div id="pageNewProductMemo" class="tab-content" style="display:none;">
        	<h1>TODO : add-new-product.tpl</h1>
	   		<p>Proposer la création d'un produit à la voléé</p>
	   		<ul>
	   			<li>Soit il s'agit d'un produit temporaire ne servant QUE pour cette vente, sans création du produit dans Prestashop<br />NB: il faudra tout de même créer un vrai produit prestashop, qui sera immédiatement supprimé après la vente</li>
	   			<li>Soit le produit sera réellement crée dans Prestashop de manière pérenne</li>
	   		</ul>
	   		<p>Liste des champs obligatoires</p>
	   		<ul>
	   			
	   		</ul>
	   		<p>Liste des champs optionnels</p>
	   		<ul>
	   			
	   		</ul>
	   		
	   		<p>Dans les deux cas de figure, cette interface doit permettre de choisir la quantité à ajouter au ticker en cours</p>
   		
   		
        </div>
        
    </div>
    
    
    <style>
    	.closeBuPanel {
		    position: absolute;
		    right: 0px;
		    padding: 5px 20px;
		    color: #FFF !important;
		    top: 0;
		    background: red !important;
		    z-index: 2;
		    font-size: 1em;
		    font-weight: bold;
		    cursor: pointer;
    	}
    
    	.content #pageNewProduct .tab-bar {
			float: left;
		    display: block;
		    height: 580px;
		    width: 20%;
		    overflow-y: auto;
    	}
    	.content #pageNewProduct.tab-content {
			margin-left:0%;
			height:100%;
			overflow-y:auto;
    	}
    	.content #pageNewProduct .tab-bar+.tab-content {
			margin-left:20%;
			height:100%;
			overflow-y:auto;
    	}
    	
    	.content #pageNewProduct.tab-content fieldset{
		    width: 24%;
		    float: left;
		    border: none;
	        margin-top: 15px;
		    background: #5D5D5D;
		    width: 31.8%;
		    margin: 1% 0 0 1%;
		    padding: 0;
		}
		
		.content #pageNewProduct.tab-content legend {
			
			font-size: 16px;
		    font-weight: 400;
		    display: block;
		    background: #262626;
		    width: 100%;
		    padding: 4% 0;
		    box-sizing: border-box;
		    margin-bottom: 15px;
	    }
	    
	    .content #pageNewProduct.tab-content label {
		    text-align: left;
		    display: block;
		    margin-left: 4%;
		}
	    .content #pageNewProduct.tab-content input,.content #pageNewProduct.tab-content select {
		    box-sizing: border-box;
		    margin: 1% 4% 4%;
		    width: 92%;
		    height: 35px;
		    border: none;
		}
		
		
	    .content #pageNewProduct.tab-content input[type="button"] {
		    height: 60px;
		    font-size:20px;
		    font-weight:bold;
		}
		
		.content #pageNewProduct.tab-content .info {
			text-align:left;
			font-size: 15px;
    		padding: 8px;
		}
		
		div#product-edit-save.button {
			text-align:center;
			margin:10px;
			padding:10px;
		}
		

		[id*="edit-error-"]{
		    display: none;
		}
		
		.content #pageNewProduct.tab-content .msg-info {
			
		}
		
		.badge {
			border-radius:8px;
			font-size: 12px;
			padding: 2px 5px;
		}
		.badge.badge-beta {
			background-color:red;
			color:#FFF;
		}
		
		#product-edit-middle-panel {
			position:relative;
			min-height:520px;
			overflow:hidden;
			background-color:transparent;
		}
		
		#product-edit-middle-panel legend i.fa {
			margin-right:8px;
		}
		
		#product-edit-middle-panel .content-face1,#product-edit-middle-panel .content-face2 {
			transition:all 1s;
			position:absolute;
			top:0;
			left:0;
			width:100%;
			backface-visibility: hidden;
		    background: #5D5D5D;
		}
		
		#product-edit-middle-panel .content-face1 legend,#product-edit-middle-panel .content-face2 legend {
			margin-top:0;
		}
    
    	#product-edit-description_short ,#product-edit-description{
    		box-sizing: border-box;
		    margin: 1% 4% 4%;
		    width: 92%;
		    border: none;
    		min-height:100px;
		    background: #EEE;
    	}
    	#product-edit-description{
    		min-height:200px;
    	}
		#product-edit-middle-panel .content-face2 {
			-webkit-transform:perspective(800) rotateY(180deg);
		} 
		 
		 
		#add_new_product input {
            width: 92%;
        }
    	
    </style>
    
</div>