<!--<script language="javascript">
var usb_screen_controller_url = '{$usb_screen_controleur_link}';
</script>-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
{* popup integrale full screen *}
<div id="global-loader" class="loader">
	<div>
		<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Veuillez patienter...
	</div>
</div>
<div id="top-loader" class="loader"></div>
<div id="bottom-loader" class="loader"></div>
{*******************************}
<div id="printTicket">
	<div class="printTicketHeader">
		<img class="logo" src="../modules/acaisse/img/logo-ticket.png" />
		<div class="infosPointshop"></div>
		<div class="msgPointshop"></div>
		<div class="infosCaisse"></div>
	</div>
	<br />
	<table class="printTicketLines"></table>
	<br />
	<div class="totaux">
		<table class="recapPayment"></table>
		<br />
		<div class="detailsBons">
			test
		</div>
		<br />
		<table class="recapTaxes"></table>
	</div>
	<br />
	<div class="msgFooter"></div>
</div>
<div id="container">
	<div id="veille">
		<div class="headLine">
			<div id="hourOfTheDay"></div>
		</div>
		<div class="closeBt">
			<i class="fa fa-times-circle"></i>
			<span>fermer</span>
		</div>
		<div id="actuBox" class="col-xs-3">
			ACTUALITES
			{Hook::exec('displayPrestatillNews')}
		</div>
		<div id="veilleEmployee">
			<div class="identification">
				<h2>{l s='Identifiez-vous :' mod='acaisse'}</h2>
			</div>
		</div>
		<div id="veilleContent">
			<div class="col-xs-9">
				<div class="intro">
					{l s='Bienvenue' mod='acaisse'} <span class="firstname"></span>
					<div id="lastOpenCaisse"></div>
				</div>
			</div>
			<ul id="veilleBox" class="col-xs-9">
				<li class="col-xs-4 text-left">
					<i class="fa fa-barcode"></i>
					<span>Caisse</span>
					<ul>
						<li class="prepare_caisse">{l s='Préparer la caisse' mod='acaisse'}</li>
						<li class="open_caisse">{l s='Ouvrir la caisse' mod='acaisse'}</li>
						<li class="caisse_movements">{l s='Mouvements de liquide' mod='acaisse'}</li>
						<li class="close_caisse">{l s='Clôturer la caisse' mod='acaisse'}</li>
					</ul>
				</li>
				<li class="col-xs-4 text-left">
					<i class="fa fa-user"></i>
					<span>Clients</span>
					<ul>
						<li class="create_customer">{l s='Créer un nouveau client' mod='acaisse'}</li>
						<li class="find_customer">{l s='Rechercher un client' mod='acaisse'}</li>
						<li class="see_orders">{l s='Voir les commandes d\'un client' mod='acaisse'}</li>
						{*<li class="see_vouchers">{l s='Voir les bons d\'un client' mod='acaisse'}</li>*}
						<!--new_return--><li class="find_customer">{l s='Retourner un produit' mod='acaisse'}</li>
					</ul>
				</li>
				<li class="col-xs-4 text-left">
					<i class="fa fa-ticket"></i>
					<span>Tickets</span>
					<ul>
						<li class="new_ticket">{l s='Nouveau ticket' mod='acaisse'}</li>
						<li class="find_ticket">{l s='Rechercher un ticket' mod='acaisse'}</li>
						<li class="show_open_tickets">{l s='Voir les tickets en attente' mod='acaisse'}</li>
					</ul>
				</li>
				<div class="clearfix"></div>
				<li class="col-xs-4 text-left">
					<i class="fa fa-shopping-cart"></i>
					<span>Produits</span>
					<ul>
						<li class="view_catalogue">{l s='Consulter le catalogue' mod='acaisse'}</li>
						<li class="create_product" data-action="create_product">{l s='Créer un produit à la volée' mod='acaisse'}</li>
						<li class="search_product">{l s='Rechercher un produit' mod='acaisse'}</li>
						<li class="find_customer">{l s='Retourner un produit' mod='acaisse'}</li>
					</ul>
				</li>
				<li class="col-xs-4 text-left">
					<i class="fa fa-area-chart"></i>
					<span>Comptabilité</span>
					<ul>
						<li class="print_z">{l s='Imprimer la bande Z du jour' mod='acaisse'}</li>
						<li class="find_z">{l s='Retrouver une bande Z' mod='acaisse'}</li>
						<li class="show_cash">{l s='Voir les Encaissements du jour' mod='acaisse'}</li>
					</ul>
				</li>
				<li class="col-xs-4 text-left">
					<i class="fa fa-info"></i>
					<span>Aide</span>
					<ul>
						<li><a href="http://www.prestatill.com/aide-et-tutoriels/" target="blank">{l s='Tous nos tutoriels' mod='acaisse'}</a></li>
						<li><a href="http://www.prestatill.com/aide-et-tutoriels/videos/" target="blank">{l s='Toutes nos vidéos' mod='acaisse'}</a></li>
					</ul>
				</li>
			</ul>
		</div>	
	</div>
	<div id="header">
		<div id="logo">
			<img src="../modules/acaisse/img/logo.png" />
			<div id="showPointshops" class="button toggle-button" >
				<i class="icon-store5"></i>
			</div>
			<div id="showEmployee" class="button toggle-button" >
				<i class="avatar">?</i>
			</div>
			<div id="pointshop-list" class="change-pointshop" style="display: none;">
				<select>
					{if !empty($my_pointshops)}
					{foreach from=$my_pointshops item=shop}
					<option value="{$shop.id_a_caisse_pointshop}">{$shop.name}</option>
					{/foreach}
					{else}
					<option value="-1">{l s='Aucun point de vente'}</option>
					{/if}
				</select>
			</div>
			<div id="notif" class="active">
				<table cellspacing='0' cellpadding='0'>
					<tr>
						<td class="greenFont"><i class="fa fa-check"></i> Connecté </td>
					</tr>
				</table>
			</div>
		</div>
		<div id="userBlock">
			{Hook::exec('displayPrestatillAddHeaderRightButton')}
			<div id="specials" class="button toggle-button">
				<i class="fa fa-cogs"></i>
			</div>
			<div id="fullscreen" class="button">
				<i class="fa fa-arrows-alt"></i>
			</div>
			<!--<div id="discount" class="button"><i class="fa fa-bolt"></i></div>-->
			<!--<div id="viewUsersOrdersButton" class="button"><i class="fa fa-truck"></i>Commandes</div>-->
			<div id="editUserButton" class="button toggle-button">
				<i class="fa fa-user"></i>
			</div>
			<div id="changeUserButton" class="button toggle-button">
				<i class="fa fa-angle-down"></i>
			</div>
			<div id="userName"></div>
			<!-- <div class="separator"></div> -->
		</div>
	</div>
	<div id="middleContainer">
		<div id="middle">
			<!-- liste des différentes popup-->
			{include file=$popupProductTpl}
			{include file=$popupEditCartLineTpl}
			{include file=$popupGlobalCartTpl}
		    {include file=$deliveryPopupTpl}
		    {include file=$returnPopupTpl}
		    {include file=$emptyPopupTpl}
			{Hook::exec('displayPrestatillAddPopup')}
			<!-- popup ou bloc de paiement spécifique -->
			<!--/liste des diff popup -->
			<div id="startScreen" class="panel">
				Ecran de sélection du point de vente
			</div>
			<div id="productList" class="panel">
				<div class="content">
					{include file=$productListTpl}
				</div>
				<div class="switcher show-product-list">
					<div class="boxCart">
						<i class="icon-cart2"></i><span id="totalItem"></span>
						<h6></h6>
					</div>
				</div>
			</div>
			<div id="ticketView" class="panel">
				<div class="content mode-buy">
					<div class="inner-content">
						<table class="lines">
							<tr class="header">
								<th class="img"></th>
								<th class="label">Dénomination</th>
								<th class="cost">P.U. <span class="price_display_method">TTC</span></th>
								<th class="qty">Quantité</th>
								<th class="totalLineCost">P.U. × Qte</th>
								<th colspan="2"></th>
								{Hook::exec('displayPrestatillTicketLineHead')}
								<th class="quantityRemove"></th>
							</tr>
						</table>
					</div>
					<div class="orderButtons">
						<div id="preOrderButtonsPanel">
							<div class="button newTicket">
								<i class="fa fa-ticket" unselectable="on" style="-webkit-user-select: none;"></i>
								<div class="label">
									Ticket en cours
								</div>
								<div class="legend">
									<span class="currentTicketNum">XXX</span>
								</div>
							</div>
							<div class="ticketBox">
								<div class="button isPending">
									<i class="fa fa-hourglass-start" unselectable="on" style="-webkit-user-select: none;"></i>
									<div class="label">
										Mettre en attente
									</div>
								</div>
								<div class="button isCommandeBloquee">
									<i class="fa fa-lock" unselectable="on" style="-webkit-user-select: none;"></i>
									<div class="label">
										Créer une commande
									</div>
								</div>
								<div class="button isDevis">
									<i class="fa fa-file-text" unselectable="on" style="-webkit-user-select: none;"></i>
									<div class="label">
										Créer un devis
									</div>
								</div>
								<div class="button cancelTicket">
									<i class="fa fa-trash"></i>
									<div class="label">
										Annuler le ticket
									</div>
								</div>
							</div>
							<div class="button validTicket">
								<div class="label">
									Encaisser
								</div>
								<div class="legend">
									Enregistrement de la commande dans Prestashop
								</div>
							</div>
						</div>
						{Hook::exec('displayPrestatillCaisseTicketTopButton',array())}
			            <div class="button" id="reductionGlobalOnCart" unselectable="on" style="user-select: none;">
			              <div class="label" unselectable="on" style="user-select: none;">
			                Remise immédiate
			              </div>
			              <div class="legend" unselectable="on" style="user-select: none;">
			                Appliquer une remise immédiate au ticket
			              </div>
			            </div>
			            <div class="clearfix"></div>
			            <div class="button" id="deliveryButton" unselectable="on" style="user-select: none;">
			              <div class="label" unselectable="on" style="user-select: none;">
			                Livraison
			              </div>
			              <div class="legend" unselectable="on" style="user-select: none;">
			                Options de livraison et détaxe
			              </div>
			            </div>
						{Hook::exec('displayPrestatillCaisseTicketBottomButton',array())}			
									
					</div>
				</div>
				<div class="content mode-return">
					{include file=$returnTicketViewTpl}
				</div>
			</div>

			<div id="paymentView" class="panel">
				{include file=$popupPayment}
			</div>
		</div>
		{include file=$searchPreOrderTpl}
		{include file=$searchCustomerTpl}
		{include file=$searchProductTpl}
		{include file=$addNewProductTpl}
		{include file=$specialsTpl}
		{include file=$userEditTpl}
		{include file=$searchTicketsTpl}
	</div>

	{* footer *}
	<div id="footer">

		<div id="preOrderNumber" class="mode-buy">
			Tickets en attente {* n°<span class="currentTicketNum"></span> *}
		</div>
		<div id="info-mode-return" class="mode-return info">
			<i class="fa fa-share-square-o fa-rotate-180 fa-fw"></i> Retour produit
		</div>

		<div id="changeOrderButton" class="button mode-buy">
			<i class="fa fa-angle-up"></i>
		</div>
		<div id="openRootPageCaisse" class="button navActive">
			<i class="fa fa-clone"></i>
		</div>
		<div id="openRootCategorie" class="button">
			<i class="fa fa-folder-open-o"></i>
		</div>
		<div id="productResearch" class="button toggle-button">
			<i class="fa fa-search"></i>
		</div>
		<div id="addNewProduct" class="button toggle-button">
			<i class="fa fa-plus"></i>
		</div>
		<div id="lastPrints" class="button toggle-button">
			<i class="fa fa-ticket"></i>
		</div>
		{Hook::exec('displayPrestatillAddFooterLeftButton')}

		<div id="totalBlock" class="totalBlock mode-buy">
			<div id="totals">
				<span id="totalWithoutDiscount"></span>
				<span id="totalCost"></span>
				<div>
					<span id="totalReduction"></span>
				</div>
			</div>
			<div id="totalTitle">
				Total {* <span id="totalEstimationMention"> (hors réductions)</span> *}
			</div>
			<div class="separator"></div>
			<div id="alertMessage">
				{*<span id="customer-edit-success">Compte client modifié avec succés :)</span>*}
				<span id="declination-inexist"> <!--Déclinaison inexistante :(--> </span>
			</div>
      <div id="print_empty_ticket" class="button">
        <i class="fa fa-2x fa-eject" title="Ouverture manuelle du tiroir caisse"></i>
      </div>
		</div>
		<div id="totalReturnBlock" class="totalBlock mode-return">

		</div>

	</div>
	<div id="ticket_email"></div>
</div>
<script>ACAISSE.employee = {$employee|@json_encode};</script>
