<table class="cash_action_table">
    <thead>
        <tr>
            <th></th>
            <th>Pièces dans la caisse</th>
            <th>Pièces théorique souhaité à l'ouverture</th>
            <th>Pièces demandés</th>
            <th>Pièces retirés</th>
            <th>Total</th>
	</tr>
    </thead>
    <tbody>
        {foreach $moneyTables as $moneyTable}
            <tr data-money="{$moneyTable.data}">
                <td>
                    <img src="../modules/acaisse/img/euroIcon/{$moneyTable.img}.png" alt="{$moneyTable.data} centime"/>
                </td>
                <td>
                    <input type="number" data-cash-on-close="cash_{$moneyTable.cash}" disabled/>
                </td>
                <td>
                    <input type="number" data-theoretical-cash="cash_{$moneyTable.cash}" disabled/>
                </td>
                <td class="requestMoney">
                    <span class="preselectMoney">0</span>
                    <input type="number" min="0" name="cash_{$moneyTable.cash}"/>
                </td>
                <td class="takeOffMoney">
                    <span class="preselectMoney">0</span>
                    <input type="number" min="0" name="cash_{$moneyTable.cash}"/>
                </td>
                <td class="totalLineMoney">
                    <input type="text" disabled/>
                </td>                
            </tr>
        {/foreach}
    </tbody>
</table>
<span class="requestMoney"><input type="hidden" name="total_cash" class="totalRequestMoney"/></span>
<span class="takeOffMoney"><input type="hidden" name="total_cash" class="totalTakeOffMoney"/></span>
<div class="cash_action_right">
    <p>
        <label for="totalCashOnClose">Total à la fermeture</label>
        <input type="text" class="totauxCash" id="totalCashOnClose" disabled placeholder="10"></input>
    </p>
    <p>
        <label for="totalCashOnClose">Reste en caisse</label>
        <input type="text" class="totauxCash" id="totalStayCash" disabled></input>
    </p>
    <p>
        <label for="totalCashOnClose">Total une fois pièces retirées et ajoutées</label>
        <input type="text" class="totauxCash" id="totalRequestMoney" disabled></input>
    </p>
    <p>
        <label for="totalCashOnClose">Total souhaitable</label>
        <input type="text" class="totauxCash" id="totalCashTheoretical" disabled></input>
    </p>
    <h2>Commentaires</h2>
    <p>
        <textarea name="message" placeholder="Commentaire ou note divers"></textarea>
    </p>
    <div class="submitActionPanel"><input type="submit" value="{$cashMessageType}" name="postCashOpen"></div>
</div>
{*include file=$keyboardNumTpl*}