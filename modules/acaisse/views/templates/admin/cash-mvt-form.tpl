<table class="cash_action_table">
    <thead>
	<tr>
            <th colspan="3">Pièces</th>
            <th colspan="3">Billets</th>
	</tr>
	<tr>
            <th>Montant</th>
            <th>Pièces ajoutés</th>
            <th>Pièces ajoutés</th>
            <th>Total</th>
            <th>Montant</th>
            <th>Pièces ajoutés</th>
            <th>Pièces ajoutés</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
	<tr>
            <td><img src="../modules/acaisse/img/euroIcon/1cent.png" alt="1 centime"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_001" data-money="0.01"/></td>
            <td class="totalCash">0 €</td>
            <td><img src="../modules/acaisse/img/euroIcon/5euro.png" alt="5 euros"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_5" data-money="5"/></td>
            <td class="totalCash">0 €</td>
	</tr>
	<tr>
            <td><img src="../modules/acaisse/img/euroIcon/2cent.png" alt="2 centimes"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_002" data-money="0.02"/></td>
            <td class="totalCash">0 €</td>
            <td><img src="../modules/acaisse/img/euroIcon/10euro.png" alt="10 euros"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_10" data-money="10"/></td>
            <td class="totalCash">0 €</td>
	</tr>
	<tr>
            <td><img src="../modules/acaisse/img/euroIcon/5cent.png" alt="5 centimes"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_005" data-money="0.05"/></td>
            <td class="totalCash">0 €</td>
            <td><img src="../modules/acaisse/img/euroIcon/20euro.png" alt="20 euros"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_20" data-money="20"/></td>
            <td class="totalCash">0 €</td>
	</tr>
	<tr>
            <td><img src="../modules/acaisse/img/euroIcon/10cent.png" alt="10 centime"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_01" data-money="0.10"/></td>
            <td class="totalCash">0 €</td>
            <td><img src="../modules/acaisse/img/euroIcon/50euro.png" alt="50 euros"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_50" data-money="50"/></td>
            <td class="totalCash">0 €</td>
	</tr>
	<tr>
            <td><img src="../modules/acaisse/img/euroIcon/20cent.png" alt="20 centimes"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_02" data-money="0.20"/></td>
            <td class="totalCash">0 €</td>
            <td><img src="../modules/acaisse/img/euroIcon/100euro.png" alt="100 euro"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_100" data-money="100"/></td>
            <td class="totalCash">0 €</td>
	</tr>
	<tr>
            <td><img src="../modules/acaisse/img/euroIcon/50cent.png" alt="50 centimes"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_05" data-money="0.50"/></td>
            <td class="totalCash">0 €</td>
            <td><img src="../modules/acaisse/img/euroIcon/200euro.png" alt="200 euros"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_200" data-money="200"/></td>
            <td class="totalCash">0 €</td>
	</tr>
	<tr>
            <td><img src="../modules/acaisse/img/euroIcon/1euro.png" alt="1 euro"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_1" data-money="1"/></td>
            <td class="totalCash">0 €</td>
            <td><img src="../modules/acaisse/img/euroIcon/500euro.png" alt="500 euros"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_500" data-money="500"/></td>
            <td class="totalCash">0 €</td>
	</tr>
	<tr>
            <td><img src="../modules/acaisse/img/euroIcon/2euro.png" alt="2 euros"/></td>
            <td><input{if $onlyPositive=="true"} min="0"{/if} type="number" name="cash_2" data-money="2"/></td>
            <td class="totalCash">0 €</td>
            <td></td>
            <td></td>
            <td></td>
	</tr>
    </tbody>
</table>
<div class="cash_action_right">
    <input type="text" class="fullTotalCash" disabled></input>
    
    <h2>Commentaires</h2>
    <p>
        <textarea name="message" placeholder="Commentaire ou note divers"></textarea>
    </p>
    <div class="submitActionPanel"> <input type="submit" value="{$cashMessageType}" name="postCashOpen"></div>
</div>
    <input type="hidden" name="total_cash"></input>
{*include file=$keyboardNumTpl*}