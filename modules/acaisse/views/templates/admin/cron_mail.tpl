<table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;">
    <tr>
        <td {$style}></td>
        {foreach $paymentMode as $payment}
            <td {$style}>{$payment}</td>
        {/foreach}
        <td {$style}>Total</td>
    </tr>
    {foreach from=$caBetweenDate key=caBetweenDateKey item=caBetweenDateVal}
    <tr>
            {if $caBetweenDateKey != 'pointshop'}
                <td {$style}>{$caBetweenDateKey}<br/>{$caBetweenDateVal.date}</td>
                {foreach from=$paymentMode key=paymentModeKey item=paymentModeVal}
                    <td {$style}>{$caBetweenDateVal.$paymentModeVal}</td>
                {/foreach}
                <td {$style}>{$caBetweenDateVal.total}</td>
            {/if}
    </tr>
    {/foreach}
</table>