<div id="orderDetail">
	<div id="orders_actions" class="">
		<input id="orders_option" name="orders_option" value="0" />
		 <div class="item" data-value="1">
        	<span>
        		<i class="fa fa-eye"></i>
        	</span> Détail de la vente 
        	<sub>Détail de la vente Prestashop</sub>
       	</div>
        <div class="item" data-value="0">
        	<span>
        		<i class="fa fa-money"></i>
        	</span>
        		Paiements
        		<sub>Voir / modifier un paiement</sub>
        </div>
        <div class="item" data-value="2">
        	<span>
        		<i class="fa fa-hourglass-start"></i>
        	</span>
        		Etats 
        		<sub>Voir / modifier l'état de la vente</sub>
        </div>
        <div class="item" data-value="3">
        	<span>
        		<i class="fa fa-file-text"></i>
        	</span>
        		Documents 
        		<sub>Voir les documents disponibles</sub>
        </div>
	</div>
	<div class="oDTable"></div>
	
</div>