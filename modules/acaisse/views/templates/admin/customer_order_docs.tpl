<table cellpadding="0" cellspacing="0" style="background:#FFF;" width="100%">
	<thead>
		<tr>
			<th>
				{l s='Date'}
			</th>
			<th>
				{l s='Document'}
			</th>
			<th>
				{l s='Numéro'}
			</th>
			<th>
				{l s='Actions'}
			</th>
			<th></th>
		</tr>
	</thead>
	<tbody>{*
			<tr>
				<td>{$order->date_add|date_format:'%d/%m/%Y'}</td>
				<td>Ticket de caisse</td>
				<td><a href="#" class="seeTicket" data-id="{$order->id_ticket}">#{$order->id_ticket}</a></td>
				<td>
					<button class="btn btn-primary seeTicket" data-id="{$order->id_ticket}"><i class="fa fa-print"></i> Imprimer</button>
					<button class="btn btn-primary"><i class="fa fa-send"></i> Envoyer</button>
				</td>
			</tr>
			*}
		{if $order->valid == 1}
			{foreach from=$order->getDocuments() item=document}
			
				{if get_class($document) eq 'OrderInvoice'}
					{if isset($document->is_delivery)}
					<tr id="delivery_{$document->id}">
					{else}
					<tr id="invoice_{$document->id}">
					{/if}
				{elseif get_class($document) eq 'OrderSlip'}
					<tr id="orderslip_{$document->id}">
				{/if}
	
						<td>{dateFormat date=$document->date_add}</td>
						<td>
							{if get_class($document) eq 'OrderInvoice'}
								{if isset($document->is_delivery)}
									{l s='Bon de livraison'}
								{else}
									{l s='Facture'}
								{/if}
							{elseif get_class($document) eq 'OrderSlip'}
								{l s='Avoir'}
							{/if}
						</td>
						<td>
							{if get_class($document) eq 'OrderInvoice'}
								{if isset($document->is_delivery)}
									<a class="_blank" title="{l s='See the document'}" href="{$link->getAdminLink('AdminPdf')|escape:'html':'UTF-8'}&amp;submitAction=generateDeliverySlipPDF&amp;id_order_invoice={$document->id}">
								{else}
									<a class="_blank" title="{l s='See the document'}" href="{$link->getAdminLink('AdminPdf')|escape:'html':'UTF-8'}&amp;submitAction=generateInvoicePDF&amp;id_order_invoice={$document->id}">
							   {/if}
							{elseif get_class($document) eq 'OrderSlip'}
								<a class="_blank" title="{l s='See the document'}" href="{$link->getAdminLink('AdminPdf')|escape:'html':'UTF-8'}&amp;submitAction=generateOrderSlipPDF&amp;id_order_slip={$document->id}">
							{/if}
							{if get_class($document) eq 'OrderInvoice'}
								{if isset($document->is_delivery)}
									{Configuration::get('PS_DELIVERY_PREFIX', $current_id_lang, null, $order->id_shop)}{'%06d'|sprintf:$document->delivery_number}
								{else}
									{$document->getInvoiceNumberFormatted($current_id_lang, $order->id_shop)}
								{/if}
							{elseif get_class($document) eq 'OrderSlip'}
								{Configuration::get('PS_CREDIT_SLIP_PREFIX', $current_id_lang)}{'%06d'|sprintf:$document->id}
							{/if}
							</a>
						</td>
						<td>
							{*
							<button class="btn btn-primary"><i class="fa fa-print"></i> Imprimer</button>
							<button class="btn btn-primary"><i class="fa fa-send"></i> Envoyer</button>
							*}
						</td>
					</tr>
			{/foreach}
		{else}
			<tr>
				<td colspan="5">
				{l s='Aucun document disponible pour l\'instant'}
				</td>
			</tr>
		{/if}
	</tbody>
</table>