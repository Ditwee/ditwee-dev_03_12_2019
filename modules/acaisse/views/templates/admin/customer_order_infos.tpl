<ul class="notLoaded">
   <li><i class="fa fa-spinner fa-spin"></i>Chargement en cours...</li>
</ul>
   
<table cellpadding="0" cellspacing="0" style="background:#FFF;" width="100%">
	<thead>
		<tr>
			<th></th>
			<th>Référence</th>
			<th>Produit</th>
			<th>PU HT (avant remise)</th>
			<th>PU HT (après remise)</th>
			<th>PU TTC</th>
			<th>Qté</th>
			<th>Retourné</th>
			<th>total TTC</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		{foreach $order_details as $detail}
		<tr data-id="{$detail.id_order_detail}">
			<td>{if !empty($detail.image_tag)}{$detail.image_tag}{else}<img src="../modules/acaisse/img/noPhoto.jpg" width="45" height="45" />{/if}</td>
			<td>{$detail.product_reference}</td>
			<td class="prod">{$detail.product_name}</td>
			<td class="ht_amount"><span class="{if $detail.original_product_price > 0 && $detail.original_product_price > $detail.unit_price_tax_excl}reduce{/if}">{if $detail.original_product_price > 0}{Tools::displayPrice($detail.original_product_price, $currency)}</span><br />{if $detail.original_product_price > 0 && $detail.original_product_price > $detail.unit_price_tax_excl}<span class="promo">- {Tools::displayPrice($detail.original_product_price-$detail.unit_price_tax_excl, $currency)}</span>{/if}{else}--{/if}</td>
			<td class="ht_amount">{Tools::displayPrice($detail.unit_price_tax_excl|string_format:'%.2f', $currency)}</td>
			<td class="unit_amount"><span>{Tools::displayPrice($detail.unit_price_tax_incl|string_format:'%.2f', $currency)}</span><input class="return" type="number" value="{$detail.unit_price_tax_incl|string_format:'%.2f'}" /></td>
			<td class="qty"><span>{$detail.product_quantity}</span><input class="return" type="number" value="{$detail.product_quantity-$detail.product_quantity_return}" /></td>
			<td class="qty_refund"><span>{$detail.product_quantity_return}</span></td>
			<td class="total_cost"><span>{Tools::displayPrice($detail.total_price_tax_incl|string_format:'%.2f', $currency)}</span><input class="return" type="number" readonly value="{$detail.total_price_tax_incl|string_format:'%.2f'}" /></td>
			<td class="actions"><input type="checkbox" value="" {if $detail.product_quantity_return == $detail.product_quantity}disabled="disabled" style="display:none;"{/if} /></td>
		</tr>
		{/foreach}
	</tbody>
</table>

<ul class="tots">
	<li>
		<span class="label">Total Produits TTC</span>
		<span class="input">{Tools::displayPrice($order->total_products_wt, $currency)}</span>
	</li>
	<li class="discount">
		<span class="label">Réductions</span>
		<span class="input">{Tools::displayPrice($order->total_discounts, $currency)}</span>
	</li>
	<li>
		<span class="label">Livraison</span>
		<span class="input">{Tools::displayPrice($order->total_shipping_tax_incl, $currency)}</span>
	</li>
	<li>
		<span class="label">Total TTC</span>
		<span class="input">{Tools::displayPrice($order->total_paid, $currency)}</span>
	</li>
	<div class="clearfix"></div>
</ul>