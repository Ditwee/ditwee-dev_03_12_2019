<table cellpadding="0" cellspacing="0" style="background:#FFF;" width="100%">
	<thead>
		<tr>
			<th>#</th>
			<th>Date</th>
			<th>Moyen de paiement</th>
			<th>Info. sur la transaction</th>
			<th>Montant</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{foreach $payments as $payment}
		<tr>
			<td>{$payment->id}</td>
			<td>{$payment->date_add|date_format:'%d-%m-%Y %H:%M:%S'}</td>
			<td class="text-center">{$payment->payment_method}</td>
			<td class="text-center">{if $payment->transaction_id == ''}{else}{$payment->transaction_id}{/if}</td>
			<td class="cost {if $payment->amount < 0}negative{/if}">{Tools::displayPrice($payment->amount, $currency)}</td>
			{*<td class=""><button class="btn btn-primary modify-payment" data-value="{$payment->id}">Modifier</button></td>*}
			<td></td>
		</tr>
		{/foreach}
	</tbody>
	
	<tfoot>
		<tr>
			<td colspan="2" class="text-center"><span class="alert alert-info">Aide : Pour modifier un mode de règlement, enregistrez un paiement négatif du règlement à changer, puis un nouveau règlement avec le bon mode de paiement.</span></td>
			<td class="text-center">
				<select id="payment_method" name="payment_method">
					<option value="0">Mode de paiement</option>
					{foreach from=$payment_methods item=method}
						<option value="{$method}">{$method}</option>
					{/foreach}
				</select>
			</td>
			<td class="text-center">
				<input type="text" id="payment_id" name="payment_id" placeholder="Ex: Erreur encaissement..." />
			</td>
			<td class="cost text-center">
				<input type="number" id="payment_amount" name="payment_amount" />
				<span>€</span>
			</td>
			<td><button class="btn btn-primary modify-payment" data-id="{$order->id}">Ajouter</button></td>
		</tr>
	</tfoot>
	
</table>

<ul class="tots col-4 no-before no-after">
	<li {if $total_paid_real <> $total_paid}class="discount"{/if}>
		<span class="label">Total payé TTC</span>
		<span class="input">{Tools::displayPrice($total_paid_real, $currency)}</span>
	</li>
	<li>
		<span class="label">Total vente TTC</span>
		<span class="input">{Tools::displayPrice($total_paid, $currency)}</span>
	</li>
	<li {if $total_paid_real < $total_paid}class="discount"{/if}>
		<span class="label">Reste à payer</span>
		<span class="input">{if $total_paid_real < $total_paid}{Tools::displayPrice($total_paid-$total_paid_real, $currency)}{else}{Tools::displayPrice(0, $currency)}{/if}</span>
	</li>
</ul>