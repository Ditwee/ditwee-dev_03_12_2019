<table cellpadding="0" cellspacing="0" style="background:#FFF;" width="100%">
	<thead>
		<tr>
			<th>Date</th>
			<th>Etats</th>
			<th>Employé</th>
		</tr>
	</thead>
	<tbody>
		{foreach $states as $state}
		<tr>
			<td>{$state.date_add|date_format:'%d/%m/%Y %H:%M:%S'}</td>
			<td class="text-center"><span class="label" style="background:{if $state.color != ''}{$state.color}{else}#777{/if}">{$state.ostate_name}</span></td>
			<td class="text-center">{if $state.employee_firstname != ''}{$state.employee_firstname} {$state.employee_lastname}{else}{/if}</td>
		</tr>
		{/foreach}
	</tbody>
	
	<tfoot>
		<tr>
			<td colspan="2" class="text-center">
				<select id="order_state" name="order_state">
					<option value="0">Etats de ventes</option>
					{foreach from=$order_states item=os}
						{if $os.id_order_state != $states.0.id_order_state}
							<option value="{$os.id_order_state}">{$os.name}</option>
						{/if}
					{/foreach}
				</select>
			</td>
			<td><button class="btn btn-primary modify-state" data-id="{$order->id}">Mettre à jour l'état</button></td>
		</tr>
	</tfoot>
	
	
</table>
