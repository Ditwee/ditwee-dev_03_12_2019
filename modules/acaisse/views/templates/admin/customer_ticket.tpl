{*<div class="closePopup" unselectable="on" style="user-select: none;">
	<span colspan="2" unselectable="on" style="user-select: none;">X</span>
</div>*}
<div id="ticketDetail">
	<div class="content_ticket_toPrint" style="display: none;">
		 <div class="sendTicket infosComp">
			<form id="popup_epgloal__form">
            	<fieldset>
        			<div class="send_form"><div class="label" >Adresse email d\'envoi :</div>
						<input type="text" value="" id="send_form_email" />
						<input type="hidden" value="" id="id_ticket_to_send" />
						<div style="text-align: center;">
							<button id="abort_send" class="button">Annuler</button>
							<button id="confirm_send" class="button">Envoyer</button>
						</div>
					</div>
            	</fieldset> 
            </form>
        </div>
   </div>
   <div class="header_ticket_toPrint">
		<div class="button print_to_ticket">
			<i class="fa fa-print"></i>
			<div class="label">Imprimer</div>
			<div class="legend">(format ticket)</div>
		</div>
		<div class="button send">
			<i class="fa fa-envelope"></i>
			<div class="label">Envoyer</div>
			<div class="legend">par e-mail</div>
		</div>
	</div>
	<div class="ticket toPrint">
		<span class="duplicata">Duplicata édité le {$smarty.now|date_format:'%d-%m-%Y %H:%M:%S'}</span>
		{$ticket_detail.0.htm}
	</div>
</div>