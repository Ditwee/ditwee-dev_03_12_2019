<div id="popupDelivery" class="hidden popup-overlay">
  <div class="container" data-content="">
    <div class="closePopup"><span colspan="2">X</span></div>
    <div class="inner-container">
      <div class="flex flex-vertical flex-info">
          
          <div class="header">
            <div class="header-inner"><h1>Option de livraison<span>Et détaxe</span><small></small></h1></div>
          </div>
          
          <div class="body">
            <form id="popupDeliveryForm" class="flex flex-horizontal">
                <!-- livraison -->
                <div class="flex-item delivery_address_column">
                  {*<h2>Livraison</h2>*}
                  <div id="delivery_option_block" class="">
                    <input id="delivery_option" name="delivery_option" value="0" />
                    <div class="item" data-value="0">
                      <span unselectable="on" style="user-select: none;">
                        <i class="fa fa-map-pin" unselectable="on" style="user-select: none;"></i>
                      </span>
                      En magasin
                      <sub unselectable="on" style="user-select: none;"></sub>
                    </div>
                    <div class="item disabled" data-value="1">
                      <span unselectable="on" style="user-select: none;">
                        <i class="fa fa-home" unselectable="on" style="user-select: none;"></i>
                      </span>
                      A domicile
                      <sub unselectable="on" style="user-select: none;"></sub>                    
                    </div>
                  </div>
                  
                  
                  <p>
                    <input name="id_delivery_address" value="" class="hide" />
                    <ul id="id_delivery_address_home" class="adresse_selector_list hide">
                      Chargement des adresses en cours...
                    </ul>
                    <ul id="id_delivery_address_store" class="adresse_selector_list hide">
                      Chargement des adresses en cours...
                    </ul>
                  </p>
                </div>
                
                <!-- transporteur -->
                <div class="flex-item carrier_address_column">
                  {*<h2>Transporteur</h2>*}
                  <div id="carrier_option_block" class="">
                    <input id="carrier_option" name="carrier_option" value="0" class="hide" />
                    <div class="item" data-value="0">
                      <span unselectable="on" style="user-select: none;">
                        <i class="fa fa-shopping-bag" unselectable="on" style="user-select: none;"></i>
                      </span>
                      Retrait
                      <sub unselectable="on" style="user-select: none;">par le client</sub>                    
                    </div>
                    <div class="item disabled" data-value="1">
                      <span unselectable="on" style="user-select: none;">
                        <i class="fa fa-space-shuttle" unselectable="on" style="user-select: none;"></i>
                      </span>
                      Par transporteur
                      <sub unselectable="on" style="user-select: none;"></sub>
                    </div>
                  </div>
                  
                  <p>                   
                    <input name="id_carrier" value="" class="hide" />
                    <ul id="id_carrier" class="adresse_selector_list hide">
                      Chargement des transporteurs
                    </ul>
                  </p>                
                </div>
                
                <!-- facturation -->
                <div class="flex-item invoice_address_column">
                  {*<h2>Facturation</h2>*}
                  <div id="invoice_option_block" class="">
                    <input id="invoice_option" name="invoice_option" value="0" />
                    <div class="item" data-value="0">
                      <span unselectable="on" style="user-select: none;">
                        <i class="fa fa-ticket" unselectable="on" style="user-select: none;"></i>
                      </span>
                      Sans facture
                      <sub unselectable="on" style="user-select: none;"></sub>                    
                    </div>
                    <div class="item" data-value="1">
                      <span unselectable="on" style="user-select: none;">
                        <i class="fa fa-file" unselectable="on" style="user-select: none;"></i>
                      </span>
                      Avec facture
                      <sub unselectable="on" style="user-select: none;"></sub>                    
                    </div>
                  </div>
                  <p id="invoice_info_block" class="hide">
                    <input name="id_invoice_address" value=""  class="hide" />
                    <ul id="id_invoice_address" class="adresse_selector_list">
                      Chargement des adresses en cours...
                    </ul>
                  </p>                
                </div>
                
                <!-- detaxe -->
                <div class="flex-item detax_address_column">
                  {*<h2>Facturation</h2>*}
                  <div id="detax_option_block" class="">
                    <input id="detax_option" name="detax_option" value="0" />
                    <div class="item" data-value="0">
                      <span unselectable="on" style="user-select: none;">
                        <i class="fa fa-euro" unselectable="on" style="user-select: none;"></i>
                      </span>
                      Sans détaxe
                      <sub unselectable="on" style="user-select: none;"></sub>
                    </div>
                    <div class="item" data-value="1">
                      <span unselectable="on" style="user-select: none;">
                        <i class="fa fa-thumbs-up" unselectable="on" style="user-select: none;"></i>
                      </span>
                      Avec détaxe
                      <sub unselectable="on" style="user-select: none;"></sub>
                    </div>
                  </div>
                  <p id="detax_info_block" class="hide">
                    <input name="detax_info" placeholder="Numéro de pièce d'identité" />
                    <span><strong>Conditions à respecter</strong><br /> - Plus de 175 de produit TTC<br /> - hors résidents des pays suivant :<br />
                    Allemagne, Autriche, Belgique, Bulgarie, Chypre, Danemark, Espagne, Estonie,
Finlande, France, Grèce, Hongrie, Irlande, Italie, Lettonie, Lituanie, Luxembourg,
Malte, Pays-Bas, Pologne, Portugal, Royaume-Uni (y compris l’île de Man),
République Tchèque, Roumanie, Slovaquie, Slovénie, Suède, Principauté de
Monaco, Guadeloupe, Guyane, Martinique et Réunion.
                    </span>                    
                  </p>              
                </div>
                
                
            </form>
          </div>
          
          <div class="footer">
            <div class="footer-inner flex">
              <div id="popupDeliveryPopupValidateButton" class="button info">
                Enregistrer
              </div>
            </div>
          </div>
      </div>
          
    </div>
  </div>
</div>