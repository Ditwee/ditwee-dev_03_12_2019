<div id="emptyPopup"  class="hidden popup-overlay">
  <div class="container" data-content="">
	<div class="closePopup"><span colspan="2">X</span></div>
	<div class="popupActions" style="display: none;"></div>
	<div class="inner-container">
  		 <div class="body">
    		<p><i class="fa fa-loader fa-spin fa-rotate-180"></i>Chargement en cours...</p>
    	</div>
  
	</div>
  </div>
</div>