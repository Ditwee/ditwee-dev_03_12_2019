<div class="keyboard numeric_keyboard">
    <span class="double display"></span><span class="reset"><i class="fa fa-undo"></i></span>
    <span>7</span><span>8</span><span>9</span>
    <span>4</span><span>5</span><span>6</span>
    <span>1</span><span>2</span><span>3</span>
    <span>0</span><span class="double backspace"><i class="fa fa-reply"></i></span>
</div>