<div class="ticket_body">
<style>
todo {
  font-family:FontB21, "Open Sans", "sans-serif" !important;
}
</style>
{* head du ticket *}
<div style="min-width: 600px;">
	{$logo_url}
</div>

<br />

<div style="text-align: center;">
	<h2>JUSTIFICATIF</h2>
	<h3>D'ENTREES / SORTIES D'ESPECES</h3>
	<h3>{$pointshop->name}</h3>
</div>

<br />
<br />

{* détails des totaux *}
<table style="min-width: 600px;margin-left:auto;margin-right:auto;">
  <tbody>
    <tr>
    	<td style="text-align: left;width: 60%;"><b>{if $mvt[16]['value'] > 0}Montant ajouté à la caisse :{else}Montant retiré à la caisse :{/if}</b></td>
    	<td style="text-align: right;width: 40%;font-size:120%;"><b>{number_format($mvt[16]['value']/100,2,',',' ')}€</b></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
     <tr>
    	<td style="text-align: left;width: 60%;"><b>Date :</b></td>
    	<td style="text-align: right;width: 40%;"><b>{$smarty.now|date_format:"%d/%m/%Y %H:%M:%S"}</b></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
    <tr>
    	<td style="text-align: left;width: 60%;"><b>Par :</b></td>
    	<td style="text-align: right;width: 40%;"><b>{$employee->firstname} {$employee->lastname}</b></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: left;"><b>Remarque :</b></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
     <tr>
    	<td colspan="2" style="text-align: left;">{$mvt[15]['value']}</td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
  </tbody>
</table>

<br />
<div style="text-align:center;"><span style="color:#FFF">.</span></div>
<br />
<br /><div style="color:#FFF;">.&nbsp;</div>
</div>