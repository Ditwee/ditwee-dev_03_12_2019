<div id="popupEditCartLine" class="hidden popup-overlay fullImg">
	<div class="container" data-content="">
		<div class="inner-container">
			{* col gauche avec preview produit et eventuel sous-menu *}
		    <div class="popup-col popup-col-menu">
		        <div class="popupNumericOpen popupProductMenu">
		            <div>
		            	<div class="acaisse_productcell_content">
		                    <div class="product_stock">
		                        <div class="product_price" data-price=""></div>   
		                        <div class="product_price_reduction"></div>   
		                        <div class="product_final_price"></div>   
		                        <div class="availability">
		                            <span class="stock_available">xxx</span> 
		                            <span class="stock_real">xx</span> 
		                            <span class="stock_inqueue">xx</span> 
		                        </div>
		                    </div>
		                    <div class="product_name_container">
		                        <div class="product_name_container_box">
		                            <div class="product_name"></div>
		                        </div>
		                    </div>
		                </div>
		                <div id="whole_sale_box"><span>Prix d'achat et marge</span><i class="fa fa-chevron-down"></i></div>
		                <div id="whole_sale">
		                	<span class="label">Prix d'achat</span>
	                        <div class="whole_sale_ht">
	                        	<span class="whole_sale_price_ht"></span>
	                        	<span class="whole_sale_ht_unit">€ HT</span>
	                        </div>
	                        <div class="whole_sale_ttc">
	                        	<span class="whole_sale_price_ttc"></span>
	                        	<span class="whole_sale_ttc_unit">€ TTC</span>
	                        </div>
	                        <span class="label">Taux de marge indicatif</span>
	                        <div class="margin_rate"></div>
	                        <div class="original_margin_rate" style="display:none;"></div>
                        </div>
		            </div>
		        </div>
		    </div>
		    
		    {* col droite : calculatrice, etc... *}
		    <div class="popup-col popup-col-action calcInfos">
		    	<div class="popBox" style="width: 265px;">
		    		<div class="title" style="background:#363636;border-color: #181818;"><div class="discountTitle centerAlign">Réduction par ligne</div></div>
		    		<div class="closePopup"><span colspan="2">X</span></div>
		    		{*<div class="showHideCalc" class="show"><i class="fa fa-calculator"></i></div>*}
		    		<div class="cart_line_reduc_amount">
		    			
		    			<div class="leftAlign chosseDiscountType">
		    				<div class="reductAmountInEuro" data-type="€">
		    					<i class="fa fa-euro"></i>
		    					<small>Réduc.</small>		    					
		    				</div>
		    				<div class="reductAmountInPercent" data-type="%">
		    					<i class="fa fa-percent"></i>
		    					<small>Réduc.</small>
	    					</div>
	    					
	    						{*PROMO DE TYPE dont x produti offert...*}
		    				{*
		    				<div class="reductAmountInGift disabled" data-type="gift">
		    					<span class="badge badge-beta">à venir</span>
		    					<i class="fa fa-gift"></i>
		    					<small>Cadeaux</small>		    					
		    				</div>
		    				*}
		    			</div>
		    		</div>
		    		
		            <div class="selectProductFloatNb" class="none"></div>
		        </div>
		    </div>
		    
		    
		    {* col centrale *}
		    <div class="popup-col popup-col-center productInfos">
		    	<div class="product_name title">Chargement...</div>
		        <div> 
		            <form id="popup_epline__form">
		            	<fieldset>
		            		<span class="alert alert-info">Vous pouvez personnaliser les informations du produit uniquement pour ce ticket</span>
		                    <div>
		                    	<div class="label">Compléter / modifier le nom du produit </div>
		                    	<input id="popup_epline__product_name" name="popup_epline__product_name" type="text" value="" />
		                    </div>
		                    <div class="popup_epline__manual_price_box">
		                    	<span class="label">Modifier le prix de vente original du produit (uniquement pour ce ticket)</span>
		                   		{* <input type="radio" id="popup_epline__original_price_is_forced_by_user_off" name="popup_epline__original_price_is_forced_by_user" value="0" /> 
		                    	<input type="radio" id="popup_epline__original_price_is_forced_by_user_on" name="popup_epline__original_price_is_forced_by_user" value="1" />
		                    	<label for="popup_epline__original_price_is_forced_by_user_on"> Forcer manuellement un tarif de vente</label> *}
		                    </div>
		                    <div id="popup_epline__manual_price_box">
		                   		<div class="col-xs-6 prenumber">
		                    		<input id="popup_epline__product_price_tax_incl" name="popup_epline__product_price_tax_incl" type="text" value="" />
		                    		<span class="unit"></span>
		                    	</div>
		                    	<div class="col-xs-6 prenumber last">
		                    		<input readonly="readonly" id="popup_epline__product_price_tax_excl" name="popup_epline__product_price_tax_excl" type="text" value=""/>
		                    		<span class="unit"></span>
		                    	</div>
		                    	<div style="clear:both;"></div>
		                    	<span id="popup_epline__original price" class="alert alert-warning">
		                    		Le prix de vente original de produit est de 
		                    		<span id="popup_epline__original_tax_incl"></span>€ TTC soit 
		                    		<span id="popup_epline__original_tax_excl"></span>€ HT<br /><small>(remises éventuelles comprises pour un client identifié dans la caisse)</small>
		                    	</span>
		                    </div>
		            	</fieldset>
		            </form>
		        </div>
		    </div>
		    
		</div>
	</div>
</div>