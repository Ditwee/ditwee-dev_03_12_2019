<div id="popupGlobalCart" class="hidden popup-overlay fullImg">
	<div class="container" data-content="">
		<div class="inner-container">
			{* col gauche avec preview produit et eventuel sous-menu *}
		    <div class="popup-col popup-col-menu">
		        <div class="popupNumericOpen popupProductMenu">
		            <ul>
		            	<li></li>
		            </ul>
		        </div>
		    </div>
		    
		    {* col droite : calculatrice, etc... *}
		    <div class="popup-col popup-col-action calcInfos">
		    	<div class="popBox" style="width: 265px;">
		    		<div class="title"><div class="discountTitle centerAlign">Remise immédiate</div></div>
		    		<div class="closePopup"><span colspan="2">X</span></div>
		    		{*<div class="showHideCalc" class="show"><i class="fa fa-calculator"></i></div>*}
		    		<div class="cart_line_reduc_amount">
		    			
		    			<div class="leftAlign chosseDiscountType">
		    				<div class="reductAmountInEuro" data-type="€">
		    					<i class="fa fa-euro"></i>
		    					<small>Réduc.</small>		    					
		    				</div>
		    				<div class="reductAmountInPercent" data-type="%">
		    					<i class="fa fa-percent"></i>
		    					<small>Réduc.</small>
	    					</div>
	    					
	    						{* PROMO DE TYPE dont x produti offert...*}
		    				{*
		    				<div class="reductAmountInGift disabled" data-type="gift">
		    					<span class="badge badge-beta">à venir</span>
		    					<i class="fa fa-gift"></i>
		    					<small>Cadeaux</small>		    					
		    				</div>
		    				*}
		    			</div>
		    		</div>
		    		
		            <div class="selectProductFloatNb" class="none"></div>
		        </div>
		    </div>
		    
		    
		    {* col centrale *}
		    <div id="productInfos" class="popup-col popup-col-center ">
		    	<div class="product_name title">Remise immédiate</div>
		        <div> 
		            <form id="popup_epgloal__form">
		            	<fieldset>
		            		<span class="alert alert-info">Vous pouvez personnaliser les informations uniquement pour ce ticket</span>
		            		<div>
			            		<div class="label">Libellé de la vente</div>
			            		<input type="text" value="" id="popup_global_title" />
		            		</p>
		            		<p>
		            			<div class="label">Informations complémentaires</div>
		            			<textarea id="popup_global_textarea"></textarea>
		            		</p>
		            		<span class="alert alert-info">Exemple : C'est un devis (ref, dates, clauses, etc...)</span>
		            	</fieldset>
		            </form>
		        </div>
		    </div>
		    
		</div>
	</div>
</div>