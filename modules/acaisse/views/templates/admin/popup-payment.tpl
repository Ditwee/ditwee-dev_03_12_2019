<div class="content">
    <div class="flexrow">
     
      {* liste des paiements *}
      {*
      <div class="flexcol col-payments">
       //ici le preview du ticket 
      </div>
      *}
      
      
      {* liste des modules de paiements *}
      <div class="flexcol col-modules">
        {*<h2>Moyens de paiement</h2>*}
      
        
        <!-- hookDisplayPrestatillPaymentButton --> 
        {Hook::exec('displayPrestatillPaymentButton')}
        <!-- /hookDisplayPrestatillPaymentButton -->
      
      </div>
      
      
      {* interface de saisie en mode encaissement *}
      <div class="flexcol col-interface mode-buy">
      	{*<h2>Saisie du montant reçu</h2>*}
      	
      	<div class="display_amount">
          <div class="display_give">Reçu <span class="payment_amount">0</span></div>
          <div class="display_back"><span class="solde_restant"></span></div>
        </div>
      	
        <div class="help box">
          <i class="fa fa-arrow-circle-left fa-3x"></i>
          Veuillez choisir le moyen de paiement ci-contre
          
        </div>
      	
        <!-- hookDisplayPrestatillPaymentInterface --> 
        {Hook::exec('displayPrestatillPaymentInterface')}
        <!-- /hookDisplayPrestatillPaymentInterface -->
      	
        <div class="payment_actions">
            <div class="button" id="launchRegisterPayment">
            	<i class="fa fa-check" ></i>
            	Encaisser <span id="real_payment_amount"></span>
            </div>
        </div>
      </div>
      
      {* interface de saisie en mode retour *}
      <div class="flexcol col-interface mode-return">
      	{*<h2>Saisie du montant reçu</h2>*}
      	
      	<div class="display_amount">
          <div class="display_give">Rembourser <span class="payment_amount">0</span></div>
          <div class="display_back"><span class="solde_restant"></span></div>
        </div>
      	
        <div class="help box">
          <i class="fa fa-arrow-circle-left fa-3x"></i>
          Veuillez choisir le moyen de remboursement ci-contre
          
        </div>
      	
        <!-- hookDisplayPrestatillPaymentInterface --> 
        {Hook::exec('displayPrestatillPaymentInterface')}
        <!-- /hookDisplayPrestatillPaymentInterface -->
      	
        <div class="payment_actions">
            <div class="button" id="launchRegisterRefund">
            	<i class="fa fa-check" ></i>
            	Rembourser <span id="real_payment_amount"></span>
            </div>
        </div>
      </div>
      
      {* liste des paiements *}
      <div class="flexcol col-payments">
          
          <table>
          
          <thead>
              <tr class="montant-total info"><th colspan="2">Montant total</th></<tr>
          </thead>
          <tbody id="details-paiements">
      
          {*
          for (var i=0; i<ACAISSE.payment_parts.length; i++) {
            var label = ACAISSE.payment_parts[i].label;
           
           <tr><td></td><td>'+formatPrice(parseFloat(ACAISSE.payment_parts[i].payment_part_amount))+'€</td></tr>';
           totPaid += parseFloat(formatPrice(parseFloat(ACAISSE.payment_parts[i].payment_part_amount)));
          }
          *}
           
          </tbody>
          <tfoot>
      
              <tr class="solde-restant warning mode-buy">
                <th>Reste à payer</th>
                <th><span id="keepToHaveToPay"></th>
              </tr>
              
              <tr class="solde-restant warning mode-return">
                <th>Reste à rembourser</th>
                <th><span id="keepToHaveToRefund"></th>
              </tr>

      
          </tfoot>
          </table>
          
          
      
      
      </div>
      
      {* colonne d'action' *}
      <div class="flexcol col-payments-last-actions">
      
        {* boutton d'annulation' *}             
          <div class="button danger cancel" unselectable="on" style="-webkit-user-select: none;">
            <i class="fa fa-sign-out" unselectable="on" style="-webkit-user-select: none;"></i>
            <div class="label" unselectable="on" style="-webkit-user-select: none;">Fermer</div>
            <div class="legend" unselectable="on" style="-webkit-user-select: none;">Annuler l'encaissement</div>    
          </div>
          
          {*si le paiement partiel est autorisé dans ce point de vente
          
          if(ACAISSE.current_pointshop.datas.active_partial_paiement == '1')
          {
          
          *}
          
          {* //boutton de sortie en paiement type partiel *}
          <div class="button info partial-validation disabled" unselectable="on" style="-webkit-user-select: none;">
           <i class="fa fa-thumbs-up" unselectable="on" style="-webkit-user-select: none;"></i>
           <div class="label" unselectable="on" style="-webkit-user-select: none;">Paiement partiel</div>
           <div class="legend" unselectable="on" style="-webkit-user-select: none;">Clôturer la commande sans la solder</div>    
          </div>
          
          
          {* } *}
          
          {* //boutton de validation du paiement *}
          {*             
          <div class="button success validation disabled" unselectable="on" style="-webkit-user-select: none;">
              <i class="fa fa-check" unselectable="on" style="-webkit-user-select: none;"></i>
              <div class="label" unselectable="on" style="-webkit-user-select: none;">Commande soldée</div>
              <div class="legend" unselectable="on" style="-webkit-user-select: none;">Créer le ticket suivant</div>    
          </div>
      		*}
      
      </div>
      
    </div>
</div>