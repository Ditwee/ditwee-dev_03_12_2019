<div id="popupNumeric" class="hidden cover popup" data-content="">  
  <div class="container ">
      <div class="popupBox">
          <div id="popupProductMenu" class="cover popupNumericOpen" style="height: 425px; ">
              <div>
                <div class="acaisse_productcell_content" id="cell_popup_0">
                      <div class="product_stock">
                          <div class="product_price"></div> 
                          <div class="availability">
                              <span class="stock_available"></span> 
                              <span class="stock_real"></span> 
                              <span class="stock_inqueue"></span> 
                          </div>
                      </div>
                      <div class="product_name_container">
                          <div class="product_name_container_box">
                              <div class="product_name"></div>
                          </div>
                      </div>
                  </div>
                <button data-to="#popupProduct" class="open" id="firstShow">Fiche produit</button>
                {* <button data-to="#prices">Prix</button> *}
                  <button data-to="#features">Caractéristiques</button>
                  <button data-to="#stocksPopup" id="stockBtn">Stocks</button>
              </div>
          </div>
      </div>
      
      <div id="productInfos" class="popupBox">
        <div class="title"></div>
          <div id="popupProduct" class="cover">
              
              <div id="details">
                <div class="label">Résumé</div>
                <div class="descShort"></div>
                <div class="label">En savoir plus...</div>
                <div class="desc"></div>
              </div>
          </div>
          {*
          <div id="prices" class="prices">
            <ul>
              <li class="wholesale-price">
                
              </li>
              <li class="price">
                
              </li>
              <li class="unit-price">
                
              </li>
            </ul>
          </div>*}
          <div id="features">
            <ul></ul>
          </div>
          <div id="stocksPopup">
          </div>
      </div>
      <div id="calcInfos" class="popupBox">
        <div class="popBox" style="width: 265px;">
          <div class="title">Quantité souhaitée</div>
          <div id="closeKeyboard"><span colspan="2">X</span></div>
          <div id="showHideCalc" class="show"><i class="fa fa-calculator"></i></div>
              <div id="selectProductNb" class="none"></div>
              <div id="selectProductFloatNb" class="none"></div>
          </div>
      </div>
  </div>
</div>