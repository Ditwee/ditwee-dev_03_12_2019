<div id="popupReturn" class="hidden popup-overlay">
  <div class="container" data-content="">
  	<div class="closePopup"><span colspan="2">X</span></div>
    <div class="inner-container">
      <div class="flex flex-vertical flex-info">
          <div class="header">
            <div class="header-inner"><h1><span></span><small></small></h1></div>
          </div>
          
          <div class="body">
            <form id="popupReturnForm">
            <table class="orders_list" cellpadding="0" cellspacing="0">
              <thead>
              	{*
                <tr class="return-sub-detail-line" unselectable="on" style="user-select: none;">
                  <th colspan="2">Commande</th>
                  <th colspan="5">Stock</th>
                  <th colspan="3">Remboursement</th>
                </tr>
                *}
                <tr class="return-sub-detail-line" unselectable="on" style="user-select: none;">
                  <th unselectable="on" style="user-select: none;">Commande</th>
                  <th unselectable="on" style="user-select: none;">Date d'achat</th>
                  <th unselectable="on" style="user-select: none;">Qté commandée</th>
                  <th unselectable="on" style="user-select: none;">Déjà remboursé</th>
                  {*
                  <th unselectable="on" style="user-select: none;">Déjà retourné</th>
                  *}
                  
                  <th unselectable="on" style="user-select: none;">Qté à rembourser</th>
                  {*
                  <th unselectable="on" style="user-select: none;">Qté à retourner</th>
                  <th unselectable="on" style="user-select: none;">Qté à réinjecter en stock</th>
                  *}
                  <th unselectable="on" style="user-select: none;">Prix de vente unitaire (<span class="ttc">TTC</span> / <span class="ht">HT</span>)</th>
                  <th unselectable="on" style="user-select: none;">Montant à rembourser (TTC par produit)</th>
                </tr>
                
              </thead
              <tbody>
                  <tr>
                    <td colspan="{*11*}8">Chargement en cours...</td>
                  </tr>
              </tbody>
              
            </table>
            </form>
          </div>
          
          <div class="footer">
            <div class="footer-inner flex">
             <div class="button total_popup">Total : <b><span></span></b>             
              </div>
              <div id="popupReturnValidateButton" class="button info">
                Enregistrer
              </div>
            </div>
          </div>
      </div>
          
    </div>
  </div>
</div>