<div class="ticket_body">
<style>
todo {
  font-family:FontB21, "Open Sans", "sans-serif" !important;
}
</style>
{* head du ticket *}
<div style="min-width: 600px;">
	{$logo_url}
</div>

<br />

<div style="text-align: center;">
	<h2>JUSTIFICATIF</h2>
	<h3>DE REMBOURSEMENT</h3>
	<h3>{$pointshop->name}</h3>
</div>

<br />
<br />

{* détails des totaux *}
<table style="min-width: 600px;margin-left:auto;margin-right:auto;">
  <tbody>
    <tr>
    	<td style="text-align: left;width: 60%;"><b>REMBOURSEMENT DE : </b></td>
    	<td style="text-align: right;width: 40%;font-size:120%;"><b>{number_format($refund.1.total_price_tax_incl,2,',',' ')}€</b></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
    <tr>
    	<td style="text-align: left;width: 60%;"><b>MODE : </b></td>
    	<td style="text-align: right;width: 40%;font-size:120%;"><b>{$refund.1.method}</b></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
     <tr>
    	<td style="text-align: left;width: 60%;"><b>Ticket concerné :</b></td>
    	<td style="text-align: right;width: 40%;font-size:120%;"><b>n°{$order->id_ticket}</b></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
     <tr>
    	<td style="text-align: left;width: 60%;"><b>Date :</b></td>
    	<td style="text-align: right;width: 40%;"><b>{$smarty.now|date_format:"%d/%m/%Y %H:%M:%S"}</b></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
    <tr>
    	<td style="text-align: left;width: 60%;"><b>Par :</b></td>
    	<td style="text-align: right;width: 40%;"><b>{$employee->firstname} {$employee->lastname}</b></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: center;"><span style="color:#FFF">.</span></td>
    </tr>
  </tbody>
</table>

<br />
<div style="text-align:center;"><span style="color:#FFF">.</span></div>
<br />
<br /><div style="color:#FFF;">.&nbsp;</div>
</div>