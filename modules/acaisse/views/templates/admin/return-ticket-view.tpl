<div class="inner-content">
  {include file="./return-explain.tpl"}
  <table class="return-lines lines">
    <thead>
      <tr class="header">
        <th>Produits</th>
        <th>Total retourné</th>
        <th>Total remboursé</th>
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
    
 </div>
 <div class="orderButtons">
  <div id="returnButtonsPanel">
     <div class="button cancelreturn">
        <i class="fa fa-trash"></i>
        <div class="label">Annuler</div>
        <div class="legend">Abandonner <br />ce retour</div>
     </div>
     <div class="button validateReturn" id="launchRegisterRefundPayment">
     	<i class="fa fa-share-square-o fa-rotate-180 fa-fw"></i>
        <div class="label">Editer un avoir</div>
        <div class="legend">Générer un avoir utilisable dans la boutique</div>
     </div>
     <div class="button validateRefund" id="launchRegisterRefundPaymentRefund">
     	<i class="fa fa-money fa-fw"></i>
        <div class="label">Rembourser</div>
        <div class="legend">Sélectionner un moyen de remboursement</div>
     </div>     
  </div>
               
 </div>