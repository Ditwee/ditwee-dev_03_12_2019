<div id="customerListContainer">
   <div class="preview">
       <div id="defaultCustomersProfils">ICI LA LISTE DES PROFILS PAR DEFAULT</div>
       <div id="customerSearchResults">ici les résultats de recherches</div>
    </div>   
    <div class="searchBox">
       <div id="customerSearchResultsFilter"><input type="text" disabled placeholder="Pour rechercher un client, scannez sa carte, tapez son ID ou son nom." /></div>
   		<div id="searchKeyboard"></div>
   	</div>
   <div class="horizontalCloseOverlay reverse">Fermer / Annuler</div>
</div>