<div id="preOrderListContainer" class="closed">
   <div class="horizontalCloseOverlay">Fermer / Annuler</div>
   <div>
       <div class="left">
       
          {* ajout du witch ticket du vendeur/tous les tickets *}          
          <div id="preorder_option_block" class="">
            <input id="preorder_option" name="preorder_option" value="0" />
            <div class="item" data-value="1">
            	<span></span> Mes tickets en cours 
            	<sub>Tous les tickets du vendeur</sub>
           	</div>
            <div class="item" data-value="0">
            	<span>
            		<i class="fa fa-ticket"></i>
            	</span>
            		Tickets en cours 
            		<sub>Tous les tickets en cours</sub>
            </div>
            <div class="item" data-value="2">
            	<span>
            		<i class="fa fa-ticket"></i>
            	</span>
            		Commandes en cours 
            		<sub>Sur les 30 derniers jours</sub>
            </div>
            <div class="item" data-value="3">
            	<span>
            		<i class="fa fa-file-text"></i>
            	</span>
            		Devis en cours 
            		<sub>Sur les 30 derniers jours</sub>
            </div>
          </div>
       
           <ul id="preList" class="notLoaded">
               <li><i class="fa fa-spinner fa-spin"></i>Chargement en cours...</li>
           </ul>
       </div>
       <div class="right">
       		<div class="keyboardBox">
          		{include file=$keyboardNumTpl}
          		<div class="clearfix"></div>
          	</div>
          	<div id="filterPreOrder">
          		<input type="text" value="" placeholder="Filtrer par client..." />
          	</div>
       </div>
   </div>
</div>