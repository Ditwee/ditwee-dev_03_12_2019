<div id="searchProducts" class="closed">
   <div id="globalSearchBox">
   		<div id="productResearchList"></div>
   		
    <div id="footerSearchBar">	
    	<div class="searchBox">
			
    		<div class="search-col-keyboard">
		    	<div id="productSearchResultsFilter">
		    	   <span id="searchLoadSpinner"><i class="fa fa-search"></i></span>
		    	   <input type="text" disabled placeholder="Recherchez un produit..." />
		    	   <span id="resetSearchInputContent"><i class="fa fa-times"></i></a>
		    	</div>
		    	
		    	<div id="productKeyboardResearch"></div>
	    	</div>
    	</div>
    	<i class="fa fa-keyboard-o"></i>
   </div>
   <div class="horizontalCloseOverlay" style="display: none;">Fermer / Annuler</div>
   
   		
   </div>
	<div id="productResearchListSide">
   			<button id="productResearchButton" class="button" disabled>Elargir la recherche</button>
		<ul class="search-col-filters">
			<li>Filtrer selon</li>
			<li class="button toggle-button selected searchByName">Nom du produit</li>
        <li class="button toggle-button selected searchByRef">Référence</li>
        <li class="button toggle-button selected searchByEAN13">EAN13</li>
        <li class="button toggle-button selected activeProduct">Actif uniquement</li>
        
				{*				
				<li class="button toggle-button">disponible</li>
				*}
			</ul>
	</div>
</div>