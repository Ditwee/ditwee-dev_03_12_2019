<div id="printTickets" class="closed">
	<div class="horizontalCloseOverlay">
		Fermer / Annuler
	</div>
	<div class="panelTitle">
		<i class="fa fa-ticket"></i>{l s='Rechercher un ticket' mod='acaisse'}
	</div>
	<div>
		<div class="left">
			<ul class="notLoaded">
				<li>
					<i class="fa fa-spinner fa-spin"></i>Chargement en cours...
				</li>
			</ul>
			<ul class="loaded">

			</ul>
		</div>
		<div class="right">
			<div class="keyboardBox">
				{include file=$keyboardNumTpl}
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>