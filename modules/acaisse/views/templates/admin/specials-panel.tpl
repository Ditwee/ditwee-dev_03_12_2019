<div id="specialsContainer">
    <div class="content">
        <ul class="tab-bar">
            <li class="tab-button button" data-tab-id="tab0" id="tabOpenCash">Ouvrir la caisse 
            	<i class="fa fa-inbox"></i>
                <span class="alert_message" style="display:none;">
                    <i class="fa fa-exclamation-circle"></i> Veuillez d'abord <strong>préparer</strong> la caisse
            	</span>
            </li>
            <li class="tab-button button" data-tab-id="tab1" id="tabMvtCash">Mouvements de caisse
            	<i class="fa fa-exchange"></i>
                <span class="alert_message" style="display:none;">
                    <i class="fa fa-exclamation-circle"></i> Veuillez d'abord <strong>ouvrir</strong> la caisse
                </span>
            </li>
            <li class="tab-button button" data-tab-id="tab2" id="tabCloseCash">Clôturer la caisse
            	<i class="fa fa-times-circle"></i>
                <span class="alert_message" style="display:none;">
                    <i class="fa fa-exclamation-circle"></i> Veuillez d'abord <strong>ouvrir</strong> la caisse
                </span>
            </li>
            <li class="tab-button button" data-tab-id="tab3" id="tabPrepareCash">Préparer la caisse
                <span class="alert_message" style="display:none;">
                    <i class="fa fa-exclamation-circle"></i> Veuillez d'abord <strong>fermer</strong> la caisse
                </span>
            </li>
            <li class="tab-button button" data-tab-id="tab4" id="tabPrintZList">Imprimer la bande Z
            	<i class="fa fa-print"></i>
                <span class="alert_message" style="display:none;">
                    <i class="fa fa-exclamation-circle"></i> Veuillez d'abord <strong>fermer</strong> la caisse
                </span>
            </li>
            {*
            <li class="tab-button button unselectable" data-tab-id="tab5" id="tabCacheSettings">Gestion du cache</li>
            	<span class="alert_message" style="display:none;">
                    <i class="fa fa-exclamation-circle"></i> En cours de développement
                </span>
            *}
            {Hook::exec('displayPrestatillSpecialButton',array())}
        </ul>        
        
        <ul class="tab-content">
            <li id="tab0">            	
            	{*<div class="cash_open_message"{if $cashOpen!=false} style="display:none;"{/if}>
            		<h1>Ouverture de caisse</h1>
            		<p>La caisse a était ouverte par {$cash_open.employee.firstname} {$cash_open.employee.lastname}</p>
            	</div>*}
            	<div class="cash_open_panel"{if $cashOpen==false} style="display:none;"{/if}>
            		{*<h1>Ouverture de la caisse</h1>
	            	<p>Indiquez ici le décompte de votre caisse lors de l'ouverture du point de vente</p>
	            	<h2>Décompte des pièces et des billets</h2>*}
	            	<form id="cash_open_form">
	            		{assign var="onlyPositive" value="true"}
                                {assign var="cashMessageType" value="Ouvrir la caisse"}
		            	{include file=$cashFormTpl}
	            	</form>
            	</div>
            </li>
            <li id="tab1">            	            	
            	<div class="cash_close_panel"{if $cashOpen!=false} style="display:none;"{/if}>
            		{*<h1>Enregistrer un mouvement de caisse</h1>
	            	<p>Indiquez ici le décompte de votre caisse lors de l'ouverture du point de vente</p>
	            	<h2>Décompte des pièces et des billets</h2>*}
	            	<form id="cash_mvt_form">
	            		{assign var="onlyPositive" value="false"}
                                {assign var="cashMessageType" value="Valider les mouvements"}
		            	{include file=$cashFormTpl}
	            	</form>
            	</div>
            </li>
            <li id="tab2">  	
            	<div class="cash_close_panel"{if $cashOpen!=false} style="display:none;"{/if}>
            		{*<h1>Fermeture de la caisse</h1>
	            	<p>Indiquez ici le décompte de votre caisse à la fermeture du point de vente</p>
	            	<h2>Décompte des pièces et des billets</h2>*}
	            	<form id="cash_close_form">
                            {assign var="onlyPositive" value="true"}
                            {assign var="cashMessageType" value="Fermer la caisse"}
		            {include file=$cashFormTpl}
	            	</form>
            	</div>
           </li>
           <li id="tab3">  	
            	<div class="cash_prepare_panel" style="display:none;">
            		{*<h1>Préparation de la caisse</h1>
	            	<p>Indiquez ici le décompte de votre caisse pour préparer la caisse suivante</p>
	            	<h2>Décompte des pièces et des billets</h2>*}
	            	<form id="cash_prepare_form">
                            {assign var="onlyPositive" value="false"}
                            {assign var="cashMessageType" value="Préparer la caisse"}
		            {include file=$cashFormPrepare}
	            	</form>
            	</div>
           </li>
           <li id="tab-specials-modules-container">
           
           </li>
           
            <li id="tab-cache">              
                
                <div class="cache_panel">
                    Cache settings...
                </div>
            </li>
        </ul>
    </div>
    <div class="horizontalCloseOverlay reverse">Fermer</div>
</div>