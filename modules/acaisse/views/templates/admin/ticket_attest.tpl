<!-- head du ticket -->
<img class="logo" src="{$pointshop->base64_ticket_img}" style="max-width: 50%;">

<div>
	{$pointshop->pointshop_name_on_ticket}<br>
	{$pointshop->adresse}<br>
	{$pointshop->telephone}
</div>

<div>{$pointshop->msg_head_ticket}</div>

<div>Caisse n°1  /  Ticket n°{$ticket->id}<br>Le {date('d/m/Y')} à {date('H:i:s')}</div>

<br />

<!-- details des moyens de paiements -->
<table>
	<tbody>
		<tr style="font-weight: bold;">
			<td colspan="2">Votre paiement</td>
		</tr>
		<tr>
			<td style="text-align: left;">{$payment_part->label} : </td>
			<td style="text-align: right;">{number_format($payment_part->payment_amount,2)}€</td>
		</tr>
	</tbody>
</table>

<br />

<!-- détails des totaux -->
<b><table>
	<tbody>
		<tr>
			<td style="text-align: left;">Total : </td>
			<td style="text-align: right;">{number_format($order->total_products_wt-$order->total_discounts_tax_incl,2,',',' ')}€</td>
		</tr>
		<tr>
			<td style="text-align: left;">Total payé : </td>
			<td style="text-align: right;">{number_format($order->total_paid_real,2,',',' ')}€</td>
		</tr>
		<tr>
			<td style="text-align: left;">Reste à payer : </td>
			<td style="text-align: right;">{number_format($order->total_products_wt-$order->total_discounts_tax_incl-$order->total_paid_real,2,',',' ')}€</td>
		</tr>
	</tbody>
</table></b>

<br />

<div>{$pointshop->msg_foot_ticket}</div>

<br />