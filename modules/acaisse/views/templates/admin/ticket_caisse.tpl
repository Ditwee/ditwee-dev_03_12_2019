<div class="ticket_body">
<style>
todo {
  font-family:FontB21, "Open Sans", "sans-serif" !important;
}
</style>
{* head du ticket *}
<div style="min-width: 600px;">
	{$TICKET_HEADER}
</div>
<h2 class="gift_print">** TICKET CADEAU **</h2>
<h2 class="duplicata_print">** DUPLICATA **</h2>
{*<h3 class="duplicata_print">@@DUPLICATA_INFORMATION@@</h3>*}
<br />
{if isset($extra_datas_global.0->ticket_title)}
  <h3>{$extra_datas_global.0->ticket_title}</h3>
{/if}
{*
<div class="gift_no_print">
{if $ticket_customer_position == Address::PRESTATILL_TICKET_POSITION_TOP}
{$CUSTOMER_INFO}
{/if}
{if $ticket_invoice_position == Address::PRESTATILL_TICKET_POSITION_TOP}
{$INVOICE_INFO}
{/if}
{if $ticket_delivery_position == Address::PRESTATILL_TICKET_POSITION_TOP}
{$DELIVERY_INFO}
{/if}
</div>
*}

{* détails des produits sans reduction *}
<table style="min-width: 600px;margin-left:auto;margin-right:auto;">
  <thead>
    <tr>
      <td style="text-align: left;font-size:90%;"><b>DESCRIPTION / QTE<span class="kdoprint"> x PRIX</span></b></td>
     
      <td class="kdoprint" style="text-align: center;font-size:90%;"><b>TTC</b></td>
      <td class="kdoprint" style="text-align: right;font-size:90%;"><b>TVA</b></td>
      {*
      <td>&nbsp;&nbsp;&nbsp;</td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      *}
    </tr>
  </thead>
  <tbody>
    {assign var='total_without_discounts' value=0}
    {assign var='total_product_pieces' value=0}
    {foreach $detailsProducts as $detailsProduct}        
      {assign var='total_product_pieces' value= $total_product_pieces + $detailsProduct['quantity']}
      <tr>
        <td style="text-align: left;font-size:90%;">{$detailsProduct['product_name']|upper}&nbsp;<span class="gift_print" style="text-align: left;display: inline-block;>"> x {number_format($detailsProduct['quantity'],2,',',' ')}</span></td>
        <td></td>
        <td></td>
      </tr>
      {if $pointshop->print_tickets_with_product_reference == 1 && $detailsProduct['product_object']->reference != ''}
      <tr class="reference">
        <td colspan="3"style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Réf. {$detailsProduct['product_object']->reference}</td>
      </tr>
      {/if}
      <tr class="kdoprint">
        <td style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{number_format($detailsProduct['quantity'],2,',',' ')}{$detailsProduct['quantity_unit']} × {number_format($detailsProduct['product_original_price'],2,',',' ')}{$detailsProduct['price_unit']}</td>
        <td style="text-align: right;">{number_format($detailsProduct['quantity']*$detailsProduct['product_original_price'], 2,',',' ')}€</td>
        <td style="text-align: right;">{$detailsProduct['id_tax_rules_group']}</td>
        
      </tr>
      {if $detailsProduct['ecotax']>0 && 1 == 2}
      <tr class="kdoprint">
        <td colspan="3">Dont {number_format($detailsProduct['ecotax'], 2, ',', ' ')} d'éco-taxe par article</td>
      </tr>
      {/if}
      {assign var='total_without_discounts_ht' value=$total_without_discounts+($detailsProduct['quantity']*$detailsProduct['product_object']->price)}
      {assign var='total_without_discounts' value=$total_without_discounts+($detailsProduct['quantity']*$detailsProduct['product_original_price'])}

      {if isset($ticket->is_detaxed) && $ticket->is_detaxed}
      {else}
      {/if}
    {/foreach}
  </tbody>
</table>

<br />

{* détails des totaux avant remises *}
{if count($detailsReductions)>0 || true}
<b><table style="margin-bottom:20px;min-width: 600px;margin-left:auto;margin-right:auto;" class="kdoprint">
  <tbody>
    <tr>
      <td></td>
      <td style="text-align: right;"><hr /></td>
    </tr>
    <tr>
      <td style="text-align: left;">TOTAL TTC</b> ({$total_product_pieces} article{if $total_product_pieces>1}s{/if}) <b> : </td>
      <td style="text-align: right;">
        {number_format($total_without_discounts,2,',',' ')}€
        </td>
    </tr>
  </tbody>
</table></b>
{/if}
<br />

{* details des réductions *}
{if count($detailsReductions)>0}
<table style="min-width: 600px;margin-left:auto;margin-right:auto;" class="kdoprint">
  <tbody>
    <tr style="font-weight: bold;">
      <td colspan="2" style="font-size:90%;margin-top:10px;margin-bottom:5px;">** VOS REDUCTIONS **</td>
    </tr>
    {assign var='total_discounts' value=0}
    {foreach $detailsReductions as $detailsReduction}
    <tr>
      <td style="text-align: left;padding-right:10px;font-size:90%;">{$detailsReduction['product_name']|upper}</td>
      <td style="text-align: right;">-{number_format($detailsReduction['quantity']*$detailsReduction['reduction'],2,',',' ')}€</td>
    </tr>
    {assign var='total_discounts' value=$total_discounts+$detailsReduction['quantity']*$detailsReduction['reduction']}
    {/foreach}
    <tr style="font-weight: bold;">
      <td colspan="2"><hr /></td>
    </tr>
    <tr>
      <td style="text-align: left;font-size:90%;margin-top:5px;"><b>TOTAL REDUCTIONS : </b></td>
      <td style="text-align: right;">
            <b>-{number_format($total_discounts,2,',',' ')}€</b>   
        </td>
    </tr>
  </tbody>
</table>

<br />

{/if}




{* détails des totaux *}
<b><table class="kdoprint" style="min-width: 600px;margin-left:auto;margin-right:auto;">
  <tbody>
    <tr>
        {assign var="the_tot" value=0}
        {foreach $detailsPaiments as $detailPaiment}
        {assign var="the_tot" value=$the_tot+$detailPaiment->payment_amount}
        {/foreach}
        
      <td style="text-align: left;font-size: 120%;">A PAYER {if $ticket->is_detaxed}NET HT{else}TTC{/if}: </td>
      <td style="text-align: right;font-size: 120%;">
            {number_format($order->getOrdersTotalPaid(),2,',',' ')}€
        </td>
    </tr>
  </tbody>
</table></b>

<br />

{* details des moyens de paiements *}
<table class="kdoprint" style="min-width: 600px;margin-left:auto;margin-right:auto;">
  <tbody>
    <tr style="font-weight: bold;">
      <td style="text-align: left;font-size: 90%;">REGLEMENTS</td>
      <td></td>
    </tr>
    {foreach $detailsPaiments as $detailPaiment}
    <tr>
      <td style="text-align: left;">{$detailPaiment->label} : </td>
      <td style="text-align: right;">{number_format($detailPaiment->payment_amount/100,2,',',' ')}€</td>
    </tr>
    
    
    {if isset($detailPaiment->extra_datas->signature)}
    <tr>
      <td colspan="2"><img src="{$detailPaiment->extra_datas->signature}" border="0" width="100%" /></td>
    </tr>
    {/if}
    
    
    {/foreach}
    {if $order->getOrdersTotalPaid() - $the_tot/100 > 0}
    
    <tr>
      <td style="text-align: left;">Reste à payer : </td>
      <td style="text-align: right;">{number_format($order->getOrdersTotalPaid() - $the_tot/100,2,',',' ')}€</td>
    </tr>
    {/if}
    
  </tbody>
</table>

<br />

{* details des taxes *}
{if count($detailsTaxes)>0}
<table class="kdoprint" style="min-width: 600px;margin-left:auto;margin-right:auto;">
  <tbody>
    <tr>
      <td colspan="2"><hr /></td>
    </tr>
    <tr style="font-weight: bold;">
      <td style="text-align: left;">Taux</td>
      <td style="text-align: right;">Montant</td>
    </tr>
    {foreach $detailsTaxes as $id_tax=>$detailTaxe}
    <tr>
      <td style="text-align: left;">{$id_tax}{if $detailTaxe['tax_rate']!=''} - {$detailTaxe['tax_rate']}%{/if}</td>
      <td style="text-align: right;">{number_format($detailTaxe['amount'],2,',',' ')}€</td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

{*
<pre>
{$common_currency_change|@print_r}
<hr />
{count($common_currency_change)}
</pre>
*}
{* 
{if count($common_currency_change) !=0 }



<table class="kdoprint">
  <tbody>
    <tr>
      <td colspan="2">Taux de change applicable</td>
    </tr>
    
    {foreach from=$common_currency_change item=change key=currency}
    <tr>    
        <td>{$change}{$currency} pour 1€ soit </td>
        <td>{number_format($change*$the_tot/100,2,',',' ')}{$currency}</td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}
*}

<br />

{if isset($extra_datas_global.0->ticket_textarea)}
  <div style="font-size:110%">{$extra_datas_global.0->ticket_textarea}</div>
{/if}

<br />
<div class="kdoprint">
{if $ticket_customer_position == Address::PRESTATILL_TICKET_POSITION_BOTTOM}
{$CUSTOMER_INFO}
{/if}
{if $ticket_invoice_position == Address::PRESTATILL_TICKET_POSITION_BOTTOM}
{$INVOICE_INFO}
{/if}
{if $ticket_delivery_position == Address::PRESTATILL_TICKET_POSITION_BOTTOM}
{$DELIVERY_INFO}
{/if}
</div>

<div style="font-size:110%">{$TICKET_FOOTER}</div>



<br />
<br />
<br /><div style="color:#FFF;">.&nbsp;</div>
</div>