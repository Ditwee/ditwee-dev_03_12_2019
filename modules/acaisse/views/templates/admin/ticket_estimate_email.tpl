<style>
todo {
  font-family:FontB21, "Open Sans", "sans-serif" !important;
}
</style>
<div class="big_format">
	<h2 class="avoirtitle" style="text-align: center;padding-bottom;30px;border:1px solid #000;">&nbsp;<br>** DEVIS n° {$ticket->num_devis} **</h2>
	<br />
	<div style="text-align:center;">
	{if isset($ticket->ref_devis)}
	  <h3>{$ticket->ref_devis}</h3>
	{/if}
	</div>
	<br />
	{* détails des produits sans reduction *}
	
	<table>
	  <thead>
		<tr>
		  <td style="text-align: left;font-size:100%;"><b>DESCRIPTION / QTE x PRIX</b></td>
		  <td style="text-align: right;font-size:100%;"><b>TTC</b></td>
		  <td style="text-align: right;font-size:100%;"><b>TVA</b></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
	  </thead>
	  <tbody>
	    {assign var='total_without_discounts' value=0}
	    {assign var='total_product_pieces' value=0}
	    {foreach $ticket->lines as $key => $detailsProduct} 
	      {assign var='total_product_pieces' value= $total_product_pieces + $detailsProduct->quantity}
	      <tr>
	        <td style="text-align: left;font-size:90%;">{$detailsProduct->label|upper}</td>
	        <td></td>
	        <td></td>
	      </tr>
	      {if $pointshop->print_tickets_with_product_reference == 1 && $detailsProd.$key.reference != ''}
	      <tr class="reference">
	        <td colspan="3"style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Réf. {$detailsProd.$key.reference}</td>
	      </tr>
	      {/if}
	      <tr>
	        <td style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{number_format($detailsProduct->quantity,2,',',' ')} × {number_format($detailsProduct->original_price_with_tax,2,',',' ')}{$detailsProd.$key.price_unit}</td>
	        <td style="text-align: right;">{number_format($detailsProduct->quantity*$detailsProduct->original_price_with_tax, 2,',',' ')}€</td>
	        <td style="text-align: right;">{$detailsProd.$key.id_tax_rules_group}</td>
	        
	      </tr>
	      {if isset($ticket->is_detaxed) && $ticket->is_detaxed}
	      {assign var='total_without_discounts' value=$total_without_discounts+($detailsProduct->quantity*$detailsProduct->price_with_tax)}
	      {else}
	      {assign var='total_without_discounts' value=$total_without_discounts+($detailsProduct->quantity*$detailsProduct->original_price_with_tax)}
	      {/if}
	    {/foreach}
	  </tbody>
	</table>
	
	<br /> {* ({count($total_product_pieces)} article{if count($total_product_pieces)>1}s{/if}) *}
	
	{* détails des totaux avant remises *}
	
	<table style="margin-bottom:20px">
	  <tbody>
	  	 <tr>
	      <td colspan="2">&nbsp;</td>
	    </tr>
	    <tr>
	      <td></td>
	      <td style="text-align: right;"><hr /></td>
	    </tr>
	    <tr>
	      <td style="text-align: left;"><b>TOTAL TTC (Hors réductions) ({$total_product_pieces} article{if $total_product_pieces>1}s{/if}) </b> : </td>
	      <td style="text-align: right;">{number_format($total_without_discounts,2,',',' ')}€</td>
	    </tr>
	  </tbody>
	</table>
	
	<br />
	
	{* details des réductions *}
	{if count($detailsReductions)>0}
	<table>
	  <tbody>
	  	<tr style="font-weight: bold;">
	      <td colspan="2"></td>
	    </tr>
	    <tr style="font-weight: bold;">
	      <td colspan="2" style="font-size:90%;margin-top:10px;margin-bottom:5px;text-align:center">** VOS REDUCTIONS IMMEDIATES **</td>
	    </tr>
	     <tr style="font-weight: bold;">
	      <td colspan="2">&nbsp;</td>
	    </tr>
	    {assign var='total_discounts' value=0}
	    {foreach $detailsReductions as $detailsReduction}
	    <tr>
	      <td style="text-align: left;padding-right:10px;font-size:90%;">{$detailsReduction['product_name']|upper}</td>
	      <td style="text-align: right;">-{number_format($detailsReduction['quantity']*$detailsReduction['reduction'],2,',',' ')}€</td>
	    </tr>
	    {assign var='total_discounts' value=$total_discounts+$detailsReduction['quantity']*$detailsReduction['reduction']}
	    {/foreach}
	    <tr style="font-weight: bold;">
	      <td colspan="2"></td>
	    </tr>
	    <tr style="font-weight: bold;">
	      <td colspan="2"><hr /></td>
	    </tr>
	    <tr>
	      <td style="text-align: left;font-size:90%;margin-top:5px;"><b>TOTAL REDUCTIONS IMMEDIATES : </b></td>
	      <td style="text-align: right;">
	            <b>{number_format($total_discounts,2,',',' ')}€</b>   
	        </td>
	    </tr>
	  </tbody>
	</table>
	
	<br />
	
	{/if}
	
	
	
	
	{* détails des totaux *}
	{if !empty($total_discounts)}
	<table>
	  <tbody>
	  	<tr>
	      <td style="text-align: left;font-size: 120%;"><b>TOTAL {if $ticket->is_detaxed}NET HT{else}TTC{/if}: </b></td>
	      <td style="text-align: right;font-size: 120%;"><b>{number_format($total_without_discounts-$total_discounts,2,',',' ')}€</b></td>
	    </tr>
	  </tbody>
	</table>
	{/if}
	<br />
	
	
	{* details des moyens de paiements *}
	{*
	<table>
	  <tbody>
	    <tr style="font-weight: bold;">
	      <td style="text-align: left;font-size: 90%;">REGLEMENTS</td>
	      <td></td>
	    </tr>
	    {foreach $detailsPaiments as $detailPaiment}
	    <tr>
	      <td style="text-align: left;">{$detailPaiment->label} : </td>
	      <td style="text-align: right;">{number_format($detailPaiment->payment_amount/100,2,',',' ')}€</td>
	    </tr>
	    
	    {/foreach}
	    
	  </tbody>
	</table>
	<br />
	
	*}
	
	{* details des taxes *}
	{if count($detailsTaxes)>0}
	<table>
	  <tbody>
	    <tr>
	      <td colspan="2"><hr /></td>
	    </tr>
	    <tr style="font-weight: bold;">
	      <td style="text-align: left;">Taux</td>
	      <td style="text-align: right;">Montant</td>
	    </tr>
	    {foreach $detailsTaxes as $id_tax=>$detailTaxe}
	    <tr>
	      <td style="text-align: left;">{$id_tax}{if $detailTaxe['tax_rate']!=''} - {$detailTaxe['tax_rate']}%{/if}</td>
	      <td style="text-align: right;">{number_format($detailTaxe['amount'],2,',',' ')}€</td>
	    </tr>
	    {/foreach}
	  </tbody>
	</table>
	{/if}
	
	
	<div style="text-align:center;">
		{if isset($ticket->commentaire_devis)}
		  <div style="font-size:100%"><b>{$ticket->commentaire_devis}</b></div>
		{/if}
	</div>
</div>