<div class="ticket_body">
<style>
todo {
  font-family:FontB21, "Open Sans", "sans-serif" !important;
}
</style>
{* head du ticket *}
<div style="min-width: 600px;">
	{$TICKET_HEADER}
</div>

{* détails des totaux *}
<b><table class="kdoprint" style="min-width: 600px;margin-left:auto;margin-right:auto;">
  <tbody>
    <tr>
        {assign var="the_tot" value=0}
        
      <td style="text-align: left;font-size: 120%;">A PAYER {if $ticket->is_detaxed}NET HT{else}TTC{/if} : </td>
      <td style="text-align: right;font-size: 120%;">
            {number_format($order->getOrdersTotalPaid(),2,',',' ')}€
        </td>
    </tr>
  </tbody>
</table></b>

<br />

{* details des moyens de paiements *}
<table class="kdoprint" style="min-width: 600px;margin-left:auto;margin-right:auto;">
  <tbody>
    <tr style="font-weight: bold;">
      <td colspan="3" style="text-align: left;font-size: 90%;">REGLEMENTS</td>
    </tr>
    {foreach $detailsPaiments as $detailPaiment}
    {assign var="the_tot" value=$the_tot+$detailPaiment->payment_amount}
    <tr>
    	<td colspan="3">&nbsp;<span style="color:#FFF;">.</span></td>
    </tr>
    <tr>
    	<td colspan="2" style="text-align: left;"><b>Le {$detailPaiment->date_add|date_format:'d/m/Y H:i:s'}</b></td>
    </tr>
    <tr>
      <td style="text-align: left;">{$detailPaiment->label} : </td>
      <td style="text-align: right;">{number_format($detailPaiment->payment_amount/100,2,',',' ')}€</td>
    </tr>
    {/foreach}
    <tr>
    	<td colspan="2">&nbsp;<span style="color:#FFF;">.</span></td>
    </tr>
    {*
    <tr style="font-weight: bold;">
      <td colspan="2" style="text-align: left;font-size: 90%;">REGLEMENTS DU JOUR</td>
    </tr>
    {foreach $detailsPaiments[1] as $detailPaiment}
    {assign var="the_tot" value=$the_tot+$detailPaiment->payment_amount}
    <tr>
      <td style="text-align: left;">{$detailPaiment->label} : </td>
      <td style="text-align: right;">{number_format($detailPaiment->payment_amount/100,2,',',' ')}€</td>
      <td style="text-align: right;font-size: 70%;">{$detailPaiment->date_add|date_format:'d/m/Y H:i:s'}</td>
    </tr>
    {/foreach}
     *}
    
    {if $order->getOrdersTotalPaid() - $the_tot/100 > 0}
    <tr>
    	<td colspan="3">&nbsp;<span style="color:#FFF;">.</span></td>
    </tr>
   
    <tr>
      <td style="text-align: left;"><b>RESTE A PAYER : </b></td>
      <td colspan="2" style="text-align: right;"><b>{number_format($order->getOrdersTotalPaid() - $the_tot/100,2,',',' ')}€</b></td>
    </tr>
    {/if}
    
  </tbody>
</table>

<br />
<div class="kdoprint">
{if $ticket_customer_position == Address::PRESTATILL_TICKET_POSITION_BOTTOM}
{$CUSTOMER_INFO}
{/if}
{if $ticket_invoice_position == Address::PRESTATILL_TICKET_POSITION_BOTTOM}
{$INVOICE_INFO}
{/if}
{if $ticket_delivery_position == Address::PRESTATILL_TICKET_POSITION_BOTTOM}
{$DELIVERY_INFO}
{/if}
</div>

<div style="font-size:110%">{$TICKET_FOOTER}</div>

<br />
<br />
<br /><div style="color:#FFF;">.&nbsp;</div>
</div>