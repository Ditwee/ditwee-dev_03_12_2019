<style>
todo {
  font-family:FontB21, "Open Sans", "sans-serif" !important;
}
</style>
{* head du ticket *}
{$TICKET_HEADER}

<br />
<br />
{if isset($extra_datas_global.0->ticket_title)}
  <h3>{$extra_datas_global.0->ticket_title}</h3>
{/if}

{* détails des produits sans reduction *}
<table>
  <thead>
	<tr>
	  <td style="text-align: left;font-size:90%;"><b>DESCRIPTION / QTE x PRIX</b></td>
	 
	  <td class="kdoprint" style="text-align: center;font-size:90%;"><b>TTC</b></td>
	</tr>
  </thead>
  <tbody>
    {assign var='total_without_discounts' value=0}
    {assign var='total_product_pieces' value=0}
    {foreach $ticket->lines as $key => $detailsProduct} 
      {assign var='total_product_pieces' value= $total_product_pieces + $detailsProduct->quantity}
      <tr>
        <td style="text-align: left;font-size:90%;">{$detailsProduct->label|upper}</td>
        <td></td>
        <td></td>
      </tr>
      {if $pointshop->print_tickets_with_product_reference == 1 && $detailsProd.$key.reference != ''}
      <tr class="reference">
        <td colspan="3"style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Réf. {$detailsProd.$key.reference}</td>
      </tr>
      {/if}
      <tr class="kdoprint">
        <td style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{number_format($detailsProduct->quantity,2,',',' ')} × {number_format($detailsProduct->original_price_with_tax,2,',',' ')}{$detailsProd.$key.price_unit}</td>
        <td style="text-align: right;">{number_format($detailsProduct->quantity*$detailsProduct->original_price_with_tax, 2,',',' ')}€</td>
        
      </tr>
      {if isset($ticket->is_detaxed) && $ticket->is_detaxed}
      {assign var='total_without_discounts' value=$total_without_discounts+($detailsProduct->quantity*$detailsProduct->price_with_tax)}
      {else}
      {assign var='total_without_discounts' value=$total_without_discounts+($detailsProduct->quantity*$detailsProduct->original_price_with_tax)}
      {/if}
    {/foreach}
  </tbody>
</table>

<br /> {* ({count($total_product_pieces)} article{if count($total_product_pieces)>1}s{/if}) *}

{* détails des totaux avant remises *}
{*
{if count($detailsReductions)>0 || true}
<b><table style="margin-bottom:20px" class="kdoprint">
  <tbody>
    <tr>
      <td></td>
      <td style="text-align: right;"><hr /></td>
    </tr>
    <tr>
      <td style="text-align: left;">TOTAL TTC (Hors réductions)</b> ({$total_product_pieces} article{if $total_product_pieces>1}s{/if}) <b> : </td>
      <td style="text-align: right;">
        {number_format($total_without_discounts,2,',',' ')}€  
        </td>
    </tr>
  </tbody>
</table></b>
{/if}
*}
<br />

{* details des réductions *}
{*
{if count($detailsReductions)>0}
<table class="kdoprint">
  <tbody>
    <tr style="font-weight: bold;">
      <td colspan="2" style="font-size:90%;margin-top:10px;margin-bottom:5px;">** VOS REDUCTIONS IMMEDIATES **</td>
    </tr>
    {assign var='total_discounts' value=0}
    {foreach $detailsReductions as $detailsReduction}
    <tr>
      <td style="text-align: left;padding-right:10px;font-size:90%;">{$detailsReduction['product_name']|upper}</td>
      <td style="text-align: right;">-{number_format($detailsReduction['quantity']*$detailsReduction['reduction'],2,',',' ')}€</td>
    </tr>
    {assign var='total_discounts' value=$total_discounts+$detailsReduction['quantity']*$detailsReduction['reduction']}
    {/foreach}
    <tr style="font-weight: bold;">
      <td colspan="2"><hr /></td>
    </tr>
    <tr>
      <td style="text-align: left;font-size:90%;margin-top:5px;"><b>TOTAL REDUCTIONS IMMEDIATES : </b></td>
      <td style="text-align: right;">
            <b>{number_format($total_discounts,2,',',' ')}€</b>   
        </td>
    </tr>
  </tbody>
</table>

<br />

{/if}
*}



{* détails des totaux *}
{*
<b><table class="kdoprint">
  <tbody>
      <td style="text-align: left;font-size: 120%;">RESTE A PAYER {if $ticket->is_detaxed}NET HT{else}TTC{/if}: </td>
      <td style="text-align: right;font-size: 120%;">
            XXXXXXX €
        </td>
    </tr>
  </tbody>
</table></b>

<br />
*}

{* details des moyens de paiements *}
{*
<table class="kdoprint">
  <tbody>
    <tr style="font-weight: bold;">
      <td style="text-align: left;font-size: 90%;">REGLEMENTS</td>
      <td></td>
    </tr>
    {foreach $detailsPaiments as $detailPaiment}
    <tr>
      <td style="text-align: left;">{$detailPaiment->label} : </td>
      <td style="text-align: right;">{number_format($detailPaiment->payment_amount/100,2,',',' ')}€</td>
    </tr>
    
    {/foreach}
    
  </tbody>
</table>
*}
<br />

{* details des taxes *}
{if count($detailsTaxes)>0}
<table class="kdoprint">
  <tbody>
    <tr>
      <td colspan="2"><hr /></td>
    </tr>
    <tr style="font-weight: bold;">
      <td style="text-align: left;">Taux</td>
      <td style="text-align: right;">Montant</td>
    </tr>
    {foreach $detailsTaxes as $id_tax=>$detailTaxe}
    <tr>
      <td style="text-align: left;">{$id_tax}{if $detailTaxe['tax_rate']!=''} - {$detailTaxe['tax_rate']}%{/if}</td>
      <td style="text-align: right;">{number_format($detailTaxe['amount'],2,',',' ')}€</td>
    </tr>
    {/foreach}
  </tbody>
</table>
{/if}

{if $ticket_currency_change !== NULL}
{*
<table class="kdoprint">
  <tbody>
    <tr>
      <td colspan="2">Taux de change applicable</td>
    </tr>
    
    {foreach from=$ticket_currency_change item=change key=currency}
    <tr>    
        <td>{$change}{$currency} pour 1€ soit </td>
        <td>{number_format($change*$the_tot/100,2,',',' ')}{$currency}</td>
    </tr>
    {/foreach}
  </tbody>
</table>
*}
{/if}


<br />

{if isset($extra_datas_global.0->ticket_textarea)}
  <div style="font-size:110%">{$extra_datas_global.0->ticket_textarea}</div>
{/if}

<div style="font-size:110%">{$TICKET_FOOTER}</div>



<br />
<br />
<br /><div style="color:#FFF">.</div>&nbsp;