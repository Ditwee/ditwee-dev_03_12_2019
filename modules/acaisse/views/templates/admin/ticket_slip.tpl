<style>
todo {
  font-family:FontB21, "Open Sans", "sans-serif" !important;
}
</style>
{* head du ticket *}
{$TICKET_HEADER}

<br />

<h2 class="avoirtitle">** TICKET D'AVOIR **</h2>
<br />

{if !empty($customer)}
  <h3>Client : {$customer->firstname} {$customer->lastname}</h3>
  <h4>{if $customer->company != null}Société : {$customer->company}{/if}</h4>
  <h4>N° client : {$customer->id}</h4>
  <br />
{/if}


<h2 class="avoirtitle">{$slip->amount}{$currency}</h2>
<br />
<h2 class="avoirtitle">Avoir n°{$slip->id}</h2>
<p>Avoir valide jusqu'au {$end_validite|date_format:'%d/%m/%Y'}</p>

<p>Référence de commande {$order->reference}/{$order->id}</p>
<br />
<img src="data:image/png;base64,{$barcode_png}" width="60%" />

<br />
<p>{$display_barcode}</p>

<br />

<div style="font-size:110%">{$TICKET_FOOTER}</div>

<br />
<br />
<br /><span style="color:#FFF">.</span>&nbsp;