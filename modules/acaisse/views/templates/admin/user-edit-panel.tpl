<div id="userEditContainer">
    <div class="content">
        
        <ul class="tab-bar" unselectable="on" style="-webkit-user-select: none;">
            <li class="tab-button open button" data-page-id="pageUser1" unselectable="on" style="-webkit-user-select: none;"><i class="fa fa-pencil-square-o"></i> Coordonnées du client</li>
            <li class="tab-button button" data-page-id="pageUser2" unselectable="on" style="-webkit-user-select: none;"><i class="fa fa-bolt"></i> Bons du client</li>
            <li class="tab-button button" data-page-id="pageUser3" unselectable="on" style="-webkit-user-select: none;"><i class="fa fa-truck"></i> Commandes du client <span class="pastille"></span></span></li>
            <li class="tab-button button" id="return-switch-mode-button" unselectable="on" style="-webkit-user-select: none;"><i class="fa fa-refresh"></i> Enregistrer un retour produit</span></li>
            
            {Hook::exec('displayPrestatillCaisseUserButton',array())}
            
        </ul>
        
        
        <div id="pageUser1">
          <form id="edit_customer" class="flex flex-vertical">
          
            <div class="ui-tab ui-tab-horizontal" id="user-edit-tab-select">
              <ul class="ui-tab-menu">
                <li data-target=".user-edit-tab-select-general" class="current">Informations générales</li>
                <li data-target=".user-edit-tab-select-address">Adresse</li>
                <li data-target=".user-edit-tab-select-group">Groupe client</li>
                {Hook::exec('displayPrestatillCustommerAdditionnalTab',array())}
              </ul>
              <div class="ui-tab-content">
                <div class="ui-tab-item user-edit-tab-select-general current">
                {*---- GENRAL ----*}
                <fieldset>
                  <legend>Personnel</legend>
                    <div>
                        <label for="customer-edit-id_gender">Civilité * : </label>
                        {assign var="genders" value=Gender::getGenders()->getResults()}
                        <select id="customer-edit-id_gender" name="customer-edit-id_gender">
                        {foreach from=$genders item=gender}
                          <option value="{$gender->id}">{$gender->name}</option>
                        {/foreach}
                        </select>
                        <span id="customer-edit-error-id_gender">Civilité invalide</span>
                    </div>
                    <div>
                        <label for="customer-edit-lastname">Nom * : </label>
                        <input type="text" id="customer-edit-lastname" name="customer-edit-lastname" value="" />
                        <span id="customer-edit-error-lastname">Nom invalide</span>
                    </div>
                    <div>
                        <label for="customer-edit-firstname">Prénom * : </label>
                        <input type="text" id="customer-edit-firstname" name="customer-edit-firstname" value="" />
                        <span id="customer-edit-error-firstname">Prénom invalide</span>
                    </div>
                    <div>
                        <label for="customer-edit-email">Email * : <span id="placehldr">Email générique</span></label>
                        <input type="email" id="customer-edit-email" name="customer-edit-email" value="" />
                        <span id="customer-edit-error-email">Email invalide ou déjà existant</span>
                    </div>
                    <div>
                        <label for="customer-edit-birthday">Date de naissance : </label>
                        <input type="date" id="customer-edit-birthday" name="customer-edit-birthday" value="" />
                        <span id="customer-edit-error-birthday">Date invalide</span>
                    </div>
                    {Hook::exec('displayPrestatillCustommerAdditionnalPersonnalFields',array())}                    
                </fieldset>
                <fieldset>
                  <legend>Société/Association</legend>
                    <div>
                        <label for="customer-edit-company">Organisation (Entreprise, Club...) : </label>
                        <input type="text" id="customer-edit-company" name="customer-edit-company" value="" />
                        <span id="customer-edit-error-company">Nom invalide</span>
                    </div>
                    <div>
                        <label for="customer-edit-siret">N° SIRET : </label>
                        <input type="number" id="customer-edit-siret" name="customer-edit-siret" value="" />
                        <span id="customer-edit-error-siret">Numéro invalide</span>
                    </div>
                    <div>
                        <label for="customer-edit-vat_number">N° TVA Intra : </label>
                        <input type="text" id="customer-edit-vat_number" name="customer-edit-vat_number" value="" />
                        <span id="customer-edit-error-vat_number">Numéro TVA invalide</span>
                    </div>
                    <div>
                        <label for="customer-edit-ape">APE : </label>
                        <input type="text" id="customer-edit-ape" name="customer-edit-ape" value="" />
                        <span id="customer-edit-error-ape">Code APE invalide</span>
                    </div>
                    {Hook::exec('displayPrestatillCustommerAdditionnalProfessionnalFields',array())}   
                  </fieldset> 

                  <fieldset>
                    <legend>Info en +</legend>
                    <div id="customer-edit-more">
                      <div class="col-xs-4">
	                    <label class="center">Newsletter</label>
	                    <input type="checkbox" id="customer-edit-newsletter" name="customer-edit-newsletter" value="" />
                      </div>
                      <div class="col-xs-4">
	                    <label class="center">Opt-in</label>
	                    <input type="checkbox" id="customer-edit-optin" name="customer-edit-optin" value="" />
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-xs-12 padd10">
	                    <label>Note privée</label>
	                    <textarea id="customer-edit-note" name="customer-edit-note"></textarea>
                      </div>
                    </div>
                    {Hook::exec('displayPrestatillCustommerAdditionnalInformationFields',array())}  
                </fieldset> 

                {*/---- GENERAL ----*}
                </div>
                <div class="ui-tab-item user-edit-tab-select-address">
                {*---- ADDRESS ----*}
                
                  <fieldset>
                    <legend>Adresse postale</legend>
                    <span id="customer-edit-error-blank-input">Veuillez renseigner tout les champs</span>
              {*
                      <div>
                          <label for="customer-edit-alias">Alias : </label>
                          <input type="text" id="customer-edit-alias" name="customer-edit-alias" value="MonAdresse" disabled="disabled" />
                          <span id="customer-edit-error-alias">Alias invalide</span>
                      </div>
              *}
              
                      <div>
                          <label for="customer-edit-alias">Pays : </label>
                          <select id="customer-id_country" name="customer-id_country">
                  <option value="8">France</option>
                  <option value="1">Allemagne</option>              
                  <option value="3">Belgique</option>         
                  <option value="17">Royaume-Uni</option>           
                  <option value="12">Luxembourg</option>            
                  <option value="19">Suisse</option>
                </select>
                          <span id="customer-edit-error-alias">Alias invalide</span>
                      </div>
                      <div>
                          <label for="customer-edit-address1">Adresse : </label>
                          <input type="text" id="customer-edit-address1" name="customer-edit-address1" value="" />
                          <span id="customer-edit-error-address1">Adresse invalide</span>
                      </div>
                      <div>
                          <label for="customer-edit-city">Ville : </label>
                          <input type="text" id="customer-edit-city" name="customer-edit-city" value="" />
                          <span id="customer-edit-error-city">Ville invalide</span>
                      </div>
                      <div>
                          <label for="customer-edit-postcode">Code postal : </label>
                          <input type="number" id="customer-edit-postcode" name="customer-edit-postcode" value="" />
                          <span id="customer-edit-error-postcode">Code postale invalide</span>
                      </div>
                  </fieldset>
                  <fieldset>
                    <legend>Coordonnées téléphoniques</legend>
                      
                      <div>
                          <span id="customer-edit-error-phone_both">Veuillez renseigner au moins un numéro</span>
                          <label for="customer-edit-phone">Téléphone fixe : </label>
                          <input type="tel" id="customer-edit-phone" name="customer-edit-phone" value="" />
                          <span id="customer-edit-error-phone">Téléphone fixe invalide</span>
                      </div>
                      <div>
                          <label for="customer-edit-phone_mobile">Téléphone mobile : </label>
                          <input type="tel" id="customer-edit-phone_mobile" name="customer-edit-phone_mobile" value="" />
                          <span id="customer-edit-error-phone_mobile">Téléphone mobile invalide</span>
                      </div>
                  </fieldset>
                
                {*/---- ADDRESS ----*}
                </div>
                <div class="ui-tab-item user-edit-tab-select-group">
                {*---- GROUP ----*}
                
                  
                
                <fieldset class="groupList">
                    <legend>Groupe</legend>
                    <div id="customer-edit-group">
              
                    </div>
                    <fieldset id="legendeEditCustomer">
                      <div><input type="checkbox" checked onclick="return false;" id="editCustomerDefaultGroupLegend" /><label>groupe par défaut</label></div>
              		  <div class="clearfix"></div>
              		  <div><input type="checkbox" checked onclick="return false;" /><label>groupe actif</label></div>
                  	</fieldset>
                </fieldset>    
                <input type="hidden" id="customer-edit-id" name="customer-edit-id" value=""/>
                <input type="hidden" id="customer-edit-id_address" name="customer-edit-id_address" value=""/>
                {*<span id="customer-edit-success">Compte client modifié avec succés :)</span>*}
                
                
                {*/--- GROUP ----*}
                </div>
                
                
                {Hook::exec('displayPrestatillCustommerAdditionnalTabContent',array())}
                
              </div>
            </div>
          
          <div class="customer-edit-save-footer-bar">          
                <input type="button" value="Enregistrer et identifier" id="customer-edit-save-and-log"/>
                <input type="button" value="Enregistrer" id="customer-edit-save"/>
          </div>
          
<script>
  $(function(){
    $('#user-edit-tab-select').uiTab({
      'onChange' : function(plugin,target,$target) {
        
      },
    });
  });
</script>     
          </form>
          <!--<span>//@TODO lister les champs qui pourront être éditer ici (pomotion, numéro étudiant...), etc...</span>--> 
        </div>
        
        <div id="pageUser2">
          <div id="discountContainer"></div>
        </div>
        
        <div id="pageUser3">
          // TODO : lister les commandes du client et mettre en surbrillance les commandes non payés entièrement
        </div>
        
        
        {Hook::exec('displayPrestatillCaisseUserInterface',array())}
        
    </div>
</div>