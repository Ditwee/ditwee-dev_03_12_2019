<div id="zcaisse">
{for $id_service=1 to $report->id_service}
<h1 font->
	{$pointshop->name}
</h1>
<div class="text-center uppercase">
	{$pointshop->adresse}<br />
	{$pointshop->msg_head_ticket}<br />
	TEL : {$pointshop->telephone}
</div>	
	
<h1>	
	TICKET Z {* n° (mettre l'id) *}
</h1>
<!-- mettre ici les sercices -->
<div class="text-left uppercase">
	Début : {$date_from|date_format:'d/m/Y'}<br />
	Fin : {$date_to|date_format:'d/m/Y'}<br />
	Date : {date('d/m/Y')} à {date('H:i:s')}
</div>

<div class="separate_double"></div>
<!-- mettre ici l'employé qui a ouvert et/ou fermé si différent -->

{if $nbVente==0}
	Aucune vente ce jour
{else}

<!-- stats par moyen de paiement -->
<table>
	{foreach $payment_mode.detail.$id_service as $name => $amount}
	<tr>
		<td class="text-left">Total Recette <span class="uppercase">{$name}</span></td>
		<td class="text-right">{number_format($amount,2)}Eur</td>
	</tr>
	{/foreach}
</table>

<!-- stats par moyen de paiement -->
<table>
	{foreach $payment_mode.nbr.$id_service as $name => $nbr}
	<tr>
		<td class="text-left">Nbre Paiement <span class="uppercase">{$name}</span></td>
		<td class="text-right">{$nbr}</td>
	</tr>
	{/foreach}
</table>

<div class="separate"></div>

<!-- stats globaux -->
<div>
<table>
	<tr>
		<td class="text-left uppercase">Recette Totale </td>
		<td class="text-right">{number_format($payment_mode.total.$id_service,2)}Eur</td>
	</tr>
</table>
</div>
<div class="separate_double"></div>
<div>
<table>
	<tr>
		<td class="text-left uppercase">CA TTC </td>
		<td class="text-right">{if !empty($total_cancelled)}{number_format($total_details.$id_service.tot_ttc-$total_cancelled.$id_service.tot_ttc,2)}Eur{else}{number_format($total_details.$id_service.tot_ttc,2)}Eur{/if}</td>
	</tr>
	{if isset($sumHT)}
        <tr>
            <td class="text-left">CA HT</td>
            <td class="text-right">{if !empty($total_cancelled)}{number_format($total_details.$id_service.tot_ht-$total_cancelled.$id_service.tot_ht,2)}Eur{else}{number_format($total_details.$id_service.tot_ht,2)}Eur{/if}</td>
        </tr>
	{/if}
	<tr>
		<td class="text-left uppercase">TOTAL TVA</td>
		<td class="text-right">{if !empty($total_cancelled)}{number_format($total_details.$id_service.tot_vat-$total_cancelled.$id_service.tot_vat,2)}Eur{else}{number_format($total_details.$id_service.tot_vat,2)}Eur{/if}</td>
	</tr>
	{if $total_details.$id_service.tot_shipping_ttc > 0 }
	
	<tr>
		<td class="text-left uppercase">CA PORTS TTC </td>
		<td class="text-right">{number_format($total_details.$id_service.tot_shipping_ttc,2)}Eur</td>
	</tr>
	{/if}
	{if $total_details.$id_service.tot_shipping_ht > 0}
	<tr>
		<td class="text-left uppercase">CA PORTS HT </td>
		<td class="text-right">{number_format($total_details.$id_service.tot_shipping_ht,2)}Eur</td>
	</tr>
	{/if}
	{if $total_details.$id_service.tot_shipping_ht > 0}
	<tr>
		<td class="text-left uppercase">TOTAL TVA PORTS </td>
		<td class="text-right">{number_format($total_details.$id_service.tot_shipping_ttc-$total_details.$id_service.tot_shipping_ht,2)}Eur</td>
	</tr>
	{/if}
</table>
</div>
<div class="separate_double"></div>
<div>
<table>
	<tr>
		<td colspan="2" class="text-left uppercase">DETAIL TVA </td>
	</tr>
</table>
</div>
<div class="separate"></div>
<div>
	<table>
		{foreach from=$report_vat.$id_service item=vat}
		{if $vat.sum_amount > 0}
			<tr>
				<td class="text-left uppercase">TOTAL HT {$vat.tax_name}</td>
				<td class="text-right">{number_format($vat.sum_total_ht,2)}Eur</td>
			</tr>
			<tr>
				<td class="text-left uppercase">MONTANT TVA {$vat.tax_name}</td>
				<td class="text-right">{number_format($vat.sum_amount,2)}Eur</td>
			</tr>
		{/if}
		{/foreach}
	</table>
</div>
<div class="separate"></div>
<div>
	<table>
		<tr>
			<td class="text-left uppercase">TOTAL REMISES TTC</td>
			<td class="text-right">{number_format($total_details.$id_service.tot_discount_ttc,2)}Eur</td>
		</tr>
		<tr>
			<td class="text-left uppercase">TOTAL REMISES HT</td>
			<td class="text-right">{number_format($total_details.$id_service.tot_discount_ht,2)}Eur</td>
		</tr>
		<tr>
			<td class="text-left uppercase">Nbre de Tickets</td>
			<td class="text-right">{$nbVente}</td>
		</tr>
		<tr>
			<td class="text-left uppercase">Nbre d'Articles</td>
			<td class="text-right">{$report->nb_sales}</td>
			{* number_format($sumNbArticle,0) *}
		</tr>
		<tr>
			<td class="text-left uppercase">Panier moyen</td>
			<td class="text-right">{if !empty($total_cancelled)}{number_format(($total_details.$id_service.tot_ttc-$total_cancelled.$id_service.tot_ttc)/$nbVente,2)}Eur{else}{number_format($total_details.$id_service.tot_ttc/$nbVente,2)}Eur{/if}</td>
		</tr>
	</table>
</div>
<div class="separate_double"></div>

{$HOOK_PRESTATILL_FOOTER}
	
	{*
	<tr>
		<td class="text-left">Articles au panier</td>
		<td class="text-right">{number_format($sumNbArticle/$nbVente,2)}</td>
	</tr>
	<tr>
		<td class="text-left">Montant moyen / article</td>
		<td class="text-right">{number_format($sumTTC/$sumNbArticle,2)}Eur</td>
	</tr>
	*}
	{*
	<div>VENTES PAR RAYON</div>
	<!-- stats par categorie -->
	<table>
		{foreach $tot_cat as $cat}
		<tr>
			<td class="text-left" style="text-transform: uppercase;">{$cat['name']} </td>
			<td class="text-right">{$cat['totalPriceSold']}Eur</td>
		</tr>
		{/foreach}
	</table>
	*}
	
	{if !empty($tickets_caisse)}
	
		{foreach $tickets_caisse as $ticket_caisse}
		   {if $ticket_caisse->order->valid == 1}
		       {$ticket_caisse->htm}
		   {/if}
		{/foreach}
	 {/if}
		
	{if !empty($tickets_caisse_canceled)}
	   <h1 style="text-align:center;">*********************</h1><h2>LISTE DES TICKETS ANNULES</h2><h1>*********************</h1>
        {foreach $tickets_caisse_canceled as $ticket_caisse}
               <div>
               {$ticket_caisse->htm}
               </div>
        {/foreach}
    {/if}
	
	<br />
	<br />
	<br />
	.
{/if}
{/for}
</div>