{literal}
<script type="text/javascript">

$(document).ready(function(){
	/**
     * Handle menu tab changement
     */
    $('.menu_tab').click(function(){
        $('.menu_tab').removeClass('active');
         $('.tab-pane').removeClass('active');
   		$(this).addClass('active');
    });

});
	
</script>
{/literal}

{if isset($exportAll)}
	<div class="confirm">Tous les caches ont été mise à jour.</div>
{/if}

{if isset($exportPrices)}
	<div class="confirm">Le cache des prix a été mise à jour.</div>
{/if}

{if isset($exportPointshopPages)}
	<div class="confirm">Le cache des pages de caisse a été mise à jour.</div>
{/if}

{if isset($exportProfiles)}
	<div class="confirm">Le cache des profiles de référence a été mise à jour.</div>
{/if}

{if isset($exportCategories)}
	<div class="confirm">Le cache des catégories a été mise à jour.</div>
{/if}

<div class="clearfix"></div>

<div class="bootstrap">
	<!-- Module content -->
	<div id="modulecontent" class="clearfix">

		<!-- Nav tabs -->
		<div class="col-lg-2">
			<div class="list-group">
				<a href="#infos" class="menu_tab list-group-item active" data-toggle="tab"><i class="icon-cogs"></i> {l s='Paramètres' mod='acaisse'}</a>
				<a href="#payment" class="menu_tab list-group-item" data-toggle="tab"><i class="icon-credit-card"></i> {l s='Modes de règlements' mod='acaisse'}</a>
				<a href="#customer" class="menu_tab list-group-item" data-toggle="tab"><i class="icon-user"></i> {l s='Clients & groupes' mod='acaisse'}</a>
				<a href="#reportfinancial" class="menu_tab list-group-item " data-toggle="tab"><i class="icon-area-chart"></i> {l s='Exports des caches' mod='acaisse'}</a>
				<!-- TU PEUX METTRE UN HOOK TAB ICI SI TU VX -->
			</div>
			<div class="list-group">
				<a class="list-group-item"><i class="icon-info"></i> {l s='Version' mod='acaisse'} {$module_version|escape:'htmlall':'UTF-8'}</a>
			</div>
		</div>
		
		<!-- Tab panes -->
		<div class="tab-content col-lg-10">
			<div class="tab-pane active panel" id="infos">
				{include file="./tabs/infos.tpl"}
			</div>
		</div>
		<div class="tab-content col-lg-10">
			<div class="tab-pane panel" id="payment">
				{include file="./tabs/payment.tpl"}
			</div>
		</div>
		<div class="tab-content col-lg-10">
			<div class="tab-pane panel" id="customer">
				{include file="./tabs/customer.tpl"}
			</div>
		</div>
		<div class="tab-content col-lg-10">
			<div class="tab-pane panel" id="reportfinancial">
				{include file="./tabs/caches.tpl"}
			</div>
		</div>
		<!-- TU PEUX METTRE UN HOOK TAB CONTENT ICI SI TU VX -->
	</div>
</div>

