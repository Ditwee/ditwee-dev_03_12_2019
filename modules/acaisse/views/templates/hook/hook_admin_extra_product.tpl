<style>
a#link-ModuleAcaisse:focus {
	background-color: #1f1f20;
    color: #FF005F;
	border-color:#1f1f20;
}
a#link-ModuleAcaisse:before {
    /* content: "f"; */
    content: "\e900  ";
    font-family: 'lbabcaisse';
}
</style>
<div id="product-ModuleAcaisse" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="ModuleAcaisse">
	<h3>Configuration du produit spécifique au modules de caisse PrestaTill</h3>
	
		
	<h4>Indisponibilité du produit par point de vente</h4>
	
	<div class="alert alert-info">
		Vous pouvez choisir de rendre indisponible le produit dans l'un ou l'autres de vos points de vente.<br />
		Si vous souhaitez rendre ce produit indisponible sur le site web, vous devez configurer sa visibilité sur "Nulle part" sur l'onglet "informations" ci-contre.
	</div>
	
	<table class="table">
		<thead>
			<tr>
				<th><span class="title_box">Point de vente physique</span></th>
				<th><span class="title_box">Rendre indisponible</span></th>
			</tr>
		</thead>

		<tbody>
		
			{foreach from=$pointshops item=pointshop}
			<tr>
				<td>{$pointshop['name']} #{$pointshop['id_a_caisse_pointshop']}</td>
				<td>
					<div class="col-lg-9">
						<span class="switch prestashop-switch fixed-width-lg">
							<input onclick="toggleDraftWarning(false);showOptions(true);" type="radio" name="not_available_in_pointshops_{$pointshop['id_a_caisse_pointshop']}" id="not_available_in_pointshops_{$pointshop['id_a_caisse_pointshop']}_off" value="0" {if !in_array($pointshop['id_a_caisse_pointshop'],$pointshops_exclude_list)}checked="checked"{/if}>
							<label for="not_available_in_pointshops_{$pointshop['id_a_caisse_pointshop']}_off" class="radioCheck">
								Dispo.
							</label>
							<input onclick="toggleDraftWarning(true);showOptions(false);" type="radio" name="not_available_in_pointshops_{$pointshop['id_a_caisse_pointshop']}" id="not_available_in_pointshops_{$pointshop['id_a_caisse_pointshop']}_on" value="1" {if in_array($pointshop['id_a_caisse_pointshop'],$pointshops_exclude_list)}checked="checked"{/if}>
							<label for="not_available_in_pointshops_{$pointshop['id_a_caisse_pointshop']}_on" class="radioCheck">
								Indispo.
							</label>
							<a class="slide-button btn"></a>
						</span>
					</div>				
				</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
	
	
	<h4>Produit générique</h4>
	<div class="alert alert-info">
		En déclarant ce produit générique, celui-ci aura un comportement différent lors de son ajout dans un ticket de caisse.<br />
		En effet lors de l'ajout au tiket l'utilisateur pourra renommer le produit de manière à identifier plus précisément le produit et aussi changer son prix de vente.<br />
		Exemple : un produit "vélo" pourra être renommé avec un nom correpsondant au modèle exacte de vélo vendu au client.
	</div>
	
	{* exemple switch on/off*}
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">
</span></div>
		<label class="control-label col-lg-2">
			Produit générique
		</label>
		<div class="col-lg-3">
			<span class="switch prestashop-switch fixed-width-lg">
				<input onclick="toggleDraftWarning(false);showOptions(true);" type="radio" name="is_generic_product" id="is_generic_product_on" value="1"{if $the_product->is_generic_product == 1} checked="checked"{/if}>
				<label for="is_generic_product_on" class="radioCheck">
					Oui
				</label>
				<input onclick="toggleDraftWarning(true);showOptions(false);" type="radio" name="is_generic_product" id="is_generic_product_off" value="0"{if $the_product->is_generic_product != 1} checked="checked"{/if}>
				<label for="is_generic_product_off" class="radioCheck">
					Non
				</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
	</div>
	
	
{*
	
	<!--exemple input, type numéric-->
	<div class="form-group">
		<label class="control-label col-lg-3" for="wi___dth"> Largeur du colis</label>
		<div class="input-group col-lg-2">
			<span class="input-group-addon">in</span>
			<input maxlength="14" id="width" name="wi___dth" type="text" value="0.000000" onkeyup="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.');">
		</div>
	</div>
	
	<!--exemple switch on/off-->
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">
</span></div>
		<label class="control-label col-lg-2">
			Activé
		</label>
		<div class="col-lg-9">
			<span class="switch prestashop-switch fixed-width-lg">
				<input onclick="toggleDraftWarning(false);showOptions(true);" type="radio" name="exemple_switch" id="exemple_switch_on" value="1" checked="checked">
				<label for="exemple_switch_on" class="radioCheck">
					Oui
				</label>
				<input onclick="toggleDraftWarning(true);showOptions(false);" type="radio" name="exemple_switch" id="exemple_switch_off" value="0">
				<label for="exemple_switch_off" class="radioCheck">
					Non
				</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
	</div>
	
	
	<!--Exemple radiobox-->
	<div class="form-group">
		<label class="control-label col-lg-3" for="simple_product">
			 Type
		</label>
		<div class="col-lg-9">
			<div class="radio">
				<label for="s__imple_pro_duct">
					<input type="radio" name="type_pr___odu__ct" id="s__imple_pro_duct" value="0" checked="checked">
					Produit standard</label>
			</div>
			<div class="radio">
				<label for="pack_p_rod__uct">
					<input type="radio" name="type_pr___odu__ct" id="pack_p_rod__uct" value="1"> Pack de produits existants</label>
			</div>
			<div class="radio">
				<label for="virt__ual_product">
					<input type="radio" name="type_pr___odu__ct" id="virt__ual_product" value="2">
					Produit dématérialisé (services, réservations, produits téléchargeables, etc.)</label>
			</div>
			<div class="row row-padding-top">
				<div id="warn_virtual_combinations" class="alert alert-warning" style="display:none">Vous ne pouvez pas avoir de déclinaisons avec un produit dématérialisé.</div>
				<div id="warn_pack_combinations" class="alert alert-warning" style="display:none">Vous ne pouvez pas utiliser de déclinaisons dans un pack.</div>
			</div>
		</div>
	</div>
	
*}
	

	<div class="panel-footer">
		<a href="{$cancel_link}" class="btn btn-default"><i class="process-icon-cancel"></i> Annuler</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> Enregistrer</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> Enregistrer et rester</button>
	</div>
</div>