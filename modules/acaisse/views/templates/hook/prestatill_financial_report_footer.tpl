{assign var=id_service value=1}
{literal}
	<script type="text/javascript">
		var id_service = $('#prestatill_report_heading').data('id_service');
		
	</script>
{/literal}
{if !empty($report_cash_mvt)}

	<i class="icon-bar-chart"></i> {l s='Etats de caisses / mouvements d\'espèces' mod='prestatillexportcompta'}
	<br />
	<br />
	<span style="display:block;" class="alert alert-warning">{l s='Attention : les mouvements d\'espèces ne sont pas calculés dans les totaux d\'encaissements.' mod='prestatillexportcompta'}</span>
	<div class="table-responsive">
		<table class="table data_table table-striped" id="" data-id_mov="">
			<thead>
				<tr>
					<th class="text-center"><b>{l s='Heure' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='0,01€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='0,02€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='0,05€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='0,10€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='0,20€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='0,50€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='1€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='2' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='5€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='10€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='20€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='50€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='100€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='200€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='500€' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='Total' mod='prestatillexportcompta'}</b></th>
					<th class="text-center"><b>{l s='Commentaire' mod='prestatillexportcompta'}</b></th>
				</tr>
			</thead>
			<tbody>
				{assign var=tot_mvt value=0}
				{foreach from=$report_cash_mvt.$id_service item=mvt}
				<tr>
					<td class="text-center">{$mvt['date_add']|date_format:'%H:%M:%S'}</td>
					<td class="text-center">{$mvt['cash_001']}</td>
					<td class="text-center">{$mvt['cash_002']}</td>
					<td class="text-center">{$mvt['cash_005']}</td>
					<td class="text-center">{$mvt['cash_01']}</td>
					<td class="text-center">{$mvt['cash_02']}</td>
					<td class="text-center">{$mvt['cash_05']}</td>
					<td class="text-center">{$mvt['cash_1']}</td>
					<td class="text-center">{$mvt['cash_2']}</td>
					<td class="text-center">{$mvt['cash_5']}</td>
					<td class="text-center">{$mvt['cash_10']}</td>
					<td class="text-center">{$mvt['cash_20']}</td>
					<td class="text-center">{$mvt['cash_50']}</td>
					<td class="text-center">{$mvt['cash_100']}</td>
					<td class="text-center">{$mvt['cash_200']}</td>
					<td class="text-center">{$mvt['cash_500']}</td>
					<td class="text-right"><b>{Tools::displayPrice($mvt['cash']/100, $currency)}</b></td>
					<td class="text-left">{$mvt['message']}</td>
					{assign var=tot_mvt value=$tot_mvt+$mvt['cash']/100}
				</tr>
				{/foreach}
			</tbody>
			<tfoot>
				<tr>
					<td class="text-right" colspan="17"><b>{l s='TOTAL DES MOUVEMENTS' mod='prestatillexportcompta'}</b></td>
					<td class="text-left" ><b>{Tools::displayPrice($tot_mvt, $currency)}</b></td>
				</tr>
			</tfoot>
		</table>
	</div>
{else}
	<i class="icon-bar-chart"></i> {l s='Sorties d\'espèces' mod='prestatillexportcompta'}
	<br />
	<br />
	<span style="display:block;" class="alert alert-info">{l s='Aucun mouvement d\'espèces' mod='prestatillexportcompta'}</span>
{/if}
							