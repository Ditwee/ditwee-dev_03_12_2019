{*
* 2007-2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*}

<div class="clearfix"></div>
<h3>
    <i class="icon-cogs"></i> {l s='Export des caches' mod='acaisse'} <small>{$module_display_name|escape:'htmlall':'UTF-8'}</small>
</h3>
<form role="form" class="form-horizontal"  action="#" method="POST">
		
<div id="cache_loader">
    <h4>Caches de caisse</h4>
    <a class="generate btn btn-default">Recréer le cache (produits modif. uniquement)</a>
    <a class="generate generate-force btn btn-default">Recréer le cache (tous les produits)</a>
    <a class="consolidate btn btn-default">Consolider le cache</a>
    
    <a class="btn btn-default" href="?controller=AdminModules&token={$token}&exportAll&configure=acaisse&tab_module={$module->tab}&module_name={$module->name}">Export tous les caches (hors produits)</a>

    
</div>


		
    <div class="form-group">
      <label class="control-label col-lg-4">
          <span class="label-tooltip" data-toggle="tooltip"
            title="{l s='Activez cette option pour passer limiter la visibilité des produit uniquement dans leur catégorie principale'}">
            {l s='Produits uniquement dans leur catégorie principale' mod='acaisse'}
          </span>
      </label>
      <div class="col-lg-8">        
        <div class="col-lg-4">
          <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="ACAISSE_export_only_product_in_default_category" id="ACAISSE_export_only_product_in_default_category_on" value="1" {if $ACAISSE_export_only_product_in_default_category==1}checked{/if}>
            <label for="ACAISSE_export_only_product_in_default_category_on" class="radioCheck">
              Oui
            </label>
            <input type="radio" name="ACAISSE_export_only_product_in_default_category" id="ACAISSE_export_only_product_in_default_category_off" value="0" {if $ACAISSE_export_only_product_in_default_category==0}checked{/if}>
            <label for="ACAISSE_export_only_product_in_default_category_off" class="radioCheck">
              Non
            </label>
            <a class="slide-button btn"></a>
          </span>
        </div>
      </div>
    </div>  
    
    
    <div class="form-group">
      <label class="control-label col-lg-4">
          <span class="label-tooltip" data-toggle="tooltip"
            title="{l s='Exclure les produits désactivés du cache'}">
            {l s='Exclure les produits désactivés' mod='acaisse'}
          </span>
      </label>
      <div class="col-lg-8">        
        <div class="col-lg-4">
          <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="ACAISSE_dont_export_inactive_product" id="ACAISSE_dont_export_inactive_product_on" value="1" {if $ACAISSE_dont_export_inactive_product==1}checked{/if}>
            <label for="ACAISSE_dont_export_inactive_product_on" class="radioCheck">
              Oui
            </label>
            <input type="radio" name="ACAISSE_dont_export_inactive_product" id="ACAISSE_dont_export_inactive_product_off" value="0" {if $ACAISSE_dont_export_inactive_product==0}checked{/if}>
            <label for="ACAISSE_dont_export_inactive_product_off" class="radioCheck">
              Non
            </label>
            <a class="slide-button btn"></a>
          </span>
        </div>
      </div>
    </div>  
		
		
    {if isset($is_prestatill)}
    <h4>Fonctions administrateurs</h4>
    <a class="resync-stock btn btn-default">Resynchroniser les niveaux de stock</a>
    {/if}
		<div class="clearfix"></div>
		<div class="panel-footer">
            <div class="btn-group pull-right">
                <button name="submitConfiguration" id="submitConfiguration" type="submit" class="btn btn-default"><i class="process-icon-save"></i> {l s='Save' mod='prestatillexportcompta'}</button>
            </div>
        </div>
        
       
</form>

<!-- script JavaScript pour générer les caches -->
<script>
$("#cache_loader .consolidate").click(function(){
    $.ajax({
        type: "post",
        async:true,
        data: {
          "action": "consolidateProductCache",
        },
        context:$(this),
        dataType: "json",
    });            
});

$('.resync-stock').click(function(){
    $.ajax({
        type: "post",
        async:true,
        data: {
          "action": "loadAllProductsIDS",
          "force_all" : "true",
        },
        context:$(this),
        dataType: "json",
      }).done(function (ids) {
            $("#cache_loader").html("<span class=\"count\">0</span> sur "+ids.length+" produits");
            
            var ids = ids;            
            var _loadNext = function()
            {
                if(ids.length == 0)
                {
                    $("#cache_loader").html("Stock à jour");
                    return;
                }
                var id = ids.shift();
                $.ajax({
                    type: "post",
                    async:true,
                    data: {
                      "action": "resyncProductStock",
                      "id_product": id,
                    },
                    dataType: "json",
                  }).done(function (response) {
                    if (response.success == true) {
                      var $count = $("#cache_loader span.count");
                      var count = parseInt($count.html());
                      count++;
                      $count.html(count);                              
                    }
                    setTimeout(_loadNext,50);
                  });
            }
            
            _loadNext();
            if(ids.length>2) { _loadNext(); }
            if(ids.length>3) { _loadNext(); }
            if(ids.length>4) { _loadNext(); }
            if(ids.length>5) { _loadNext(); }
      });
});

$("#cache_loader .generate,#cache_loader .generate-force").click(function(){
    $.ajax({
        type: "post",
        async:true,
        data: {
          "action": "loadAllProductsIDS",
          "force_all" : $(this).hasClass("generate-force")?"true":"false",
        },
        context:$(this),
        dataType: "json",
      }).done(function (ids) {
            $("#cache_loader").html("<span class=\"count\">0</span> sur "+ids.length+" produits");
            
            var ids = ids;
            
            var _loadNext = function()
            {
                if(ids.length == 0)
                {
                    $("#cache_loader").html("Consolidation du cache");
                    
                    $.ajax({
                        type: "post",
                        data: {
                          "action": "consolidateProductCache",
                        },
                        dataType: "json",                                
                    }).done(function(){
                        $("#cache_loader").html("Cache produit correctement mise à jour.");
                    }).fail(function(){                                
                        $("#cache_loader").html("Erreur lors de la consolidation du cache.");
                    });
                    
                    return;
                }
                var id = ids.shift();
                $.ajax({
                    type: "post",
                    async:true,
                    data: {
                      "action": "loadProduct",
                      "id_product": id,
                    },
                    dataType: "json",
                  }).done(function (response) {
                    if (response.success == true) {
                      var $count = $("#cache_loader span.count");
                      var count = parseInt($count.html());
                      count++;
                      $count.html(count);                              
                    }
                    setTimeout(_loadNext,50);
                  });
            }
            
            _loadNext();
            if(ids.length>2) { _loadNext(); }
            if(ids.length>3) { _loadNext(); }
            if(ids.length>4) { _loadNext(); }
            if(ids.length>5) { _loadNext(); }
      });            
});
</script>


