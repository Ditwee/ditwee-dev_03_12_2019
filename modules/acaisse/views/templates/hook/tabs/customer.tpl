{*
* 2007-2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*}


<div class="clearfix"></div>
<h3>
    <i class="icon-user"></i> {l s='Clients & groupes' mod='acaisse'} <small>{$module_display_name|escape:'htmlall':'UTF-8'}</small>
</h3>
<form role="form" class="form-horizontal"  action="#" method="POST">

		<div class="form-group">
			<label class="control-label col-lg-4" for="profilsID">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Indiquez les id_customer séparés par ;<br/>A noter que le premier ID sera considéré comme celui par défaut si jamais un client ne correspond n\'a aucune correspondance de groupe par défaut.'}">
						{l s='Liste des profils types ' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<input type="text" name="profilsID" value="{$ACAISSE_profilsID}" style="text-align: center" id="profilsID" />
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-4" for="profilsIDCheckingNotAllowed">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Indiquez les id_customer séparés par ;'}">
						{l s='Profils non valables pour une commandes ' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<input type="text" name="profilsIDCheckingNotAllowed" value="{$ACAISSE_profilsIDNoChecking}" style="text-align: center" id="profilsIDCheckingNotAllowed" />
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-4" for="searchActiveCustomer">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Indiquez les id_customer séparés par ;'}">
						{l s='Voir les clients désactivés ' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="searchActiveCustomer" id="searchActiveCustomer_on" value="1" {if $ACAISSE_searchActiveCustomer==1}checked{/if}>
						<label for="searchActiveCustomer_on" class="radioCheck">
							Oui
						</label>
						<input type="radio" name="searchActiveCustomer" id="searchActiveCustomer_off" value="0" {if $ACAISSE_searchActiveCustomer==0}checked{/if}>
						<label for="searchActiveCustomer_off" class="radioCheck">
							Non
						</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-4" for="useForceGroup">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Indiquez les id_customer séparés par ;'}">
						{l s='Forcer les client à passer dans un groupe ' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="useForceGroup" id="useForceGroup_on" value="1" {if $ACAISSE_useForceGroup==1}checked{/if}>
						<label for="useForceGroup_on" class="radioCheck">
							Oui
						</label>
						<input type="radio" name="useForceGroup" id="useForceGroup_off" value="0" {if $ACAISSE_useForceGroup==0}checked{/if}>
						<label for="useForceGroup_off" class="radioCheck">
							Non
						</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>
		</div>
		
		<div id="customer_seach_box" class="form-group">
			<label class="control-label col-lg-4" for="useForceGroup">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Indiquez les id_customer séparés par ;'}">
						{l s='Rechercher un client avec ' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">	
				{foreach from=$clientAttributes key=k item=value}
				<div class="checkbox {if $value}active{/if} ">
					<label for="customerSearchFields{$k}">
						<input type="checkbox" name="customerSearchFields{$k}" id="show_price" value="on" {if $value}checked="checked"{/if}>
						{$k}
					</label>
				</div>
				{/foreach}
			</div>
		</div>
		
		<div class="panel-footer">
            <div class="btn-group pull-right">
                <button name="submitCustomerOptions" id="submitCustomerOptions" type="submit" class="btn btn-default"><i class="process-icon-save"></i> {l s='Save' mod='acaisse'}</button>
            </div>
        </div>
        
       
</form>


<script>

	$('#customer_seach_box .checkbox').click(function(){
		if($(this).hasClass('active'))
		{
			$(this).find('input').prop('checked',false);
			$(this).removeClass('active');
		}
		else
		{
			$(this).find('input').prop('checked','checked');
			$(this).addClass('active');
		}
	});

</script>		