{*
* 2007-2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*}

<div class="clearfix"></div>
<h3>
    <i class="icon-cogs"></i> {l s='Paramètres généraux' mod='acaisse'} <small>{$module_display_name|escape:'htmlall':'UTF-8'}</small>
</h3>
<form role="form" class="form-horizontal"  action="#" method="POST">
		<h4>{l s='Frais de ports' mod='acaisse'}</h4>
		{if $CARRIER_IS_FREE == 0}
		<div class="alert alert-warning">
			{l s='ATTENTION : les transporteur par défaut actuel est PAYANT, cela risque de générer des frais de ports depuis les ventes en caisse. Activez l\'option ci-dessous pour passer outre le transporteur par défaut et supprimer les frais de ports en caisse.' mod='acaisse'}		
		</div>
		{else}
		<div class="alert alert-success">
			{l s='Le transporteur par défaut actuel est GRATUIT, vous n\'êtes pas obligé d\'activer l\'option ci-dessous.' mod='acaisse'}		
		</div>
		{/if}
		<div class="form-group">
			<label class="control-label col-lg-4" for="FORCE_DEFAULT_CARRIER">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Activez cette option pour passer outre les paramètres de frais de port de Prestashop ;'}">
						{l s='Désactiver les frais de transport en point de vente' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="FORCE_DEFAULT_CARRIER" id="FORCE_DEFAULT_CARRIER_on" value="1" {if $ACAISSE_FORCE_DEFAULT_CARRIER==1}checked{/if}>
						<label for="FORCE_DEFAULT_CARRIER_on" class="radioCheck">
							Oui
						</label>
						<input type="radio" name="FORCE_DEFAULT_CARRIER" id="FORCE_DEFAULT_CARRIER_off" value="0" {if $ACAISSE_FORCE_DEFAULT_CARRIER==0}checked{/if}>
						<label for="FORCE_DEFAULT_CARRIER_off" class="radioCheck">
							Non
						</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>
		</div>	
       
		<div class="panel-footer">
            <div class="btn-group pull-right">
                <button name="submitGeneralOptions" id="submitGeneralOptions" type="submit" class="btn btn-default"><i class="process-icon-save"></i> {l s='Save' mod='acaisse'}</button>
            </div>
        </div>
</form>