{*
* 2007-2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*}

<div class="clearfix"></div>
<h3>
    <i class="icon-credit-card"></i> {l s='Modes de règlements' mod='acaisse'} <small>{$module_display_name|escape:'htmlall':'UTF-8'}</small>
</h3>
<form role="form" class="form-horizontal"  action="#" method="POST">
		
		{* PAIEMENTS *}
		<h4>{l s='Paiements' mod='acaisse'}</h4>
		
		{if $ACAISSE_INVOICE_IS_ACTIVE == 0}
		<div class="alert alert-warning">
			{l s='ATTENTION : Actuellement les ventes en magasin ne génèrent aucune facture.' mod='acaisse'}		
		</div>
		<div class="alert alert-danger">
			{l s='ATTENTION : Ne jamais changer de statuts de paiement lorsque vous êtes dans un mode "sans génération de facture", sinon Prestashop va vous duppliquer les paiements déjà réceptionnés si le nouveau statut choisi considère la commande comme payés.' mod='acaisse'}		
		</div>
		{else}
		<div class="alert alert-success">
			{l s='L\'ensemble de vos points de vente dispose d\'une numéroation de facture commune.' mod='acaisse'}		
		</div>
		{/if}
		<div class="form-group">
			<label class="control-label col-lg-4" for="ACAISSE_INVOICE_IS_ACTIVE">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Activez cette option si vous souhaitez avoir une numéroation commune de vos factures pour l\'ensemble de vos points de vente'}">
						{l s='Générer des factures pour chaque vente' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="ACAISSE_INVOICE_IS_ACTIVE" id="ACAISSE_INVOICE_IS_ACTIVE_on" value="1" {if $ACAISSE_INVOICE_IS_ACTIVE==1}checked{/if}>
						<label for="ACAISSE_INVOICE_IS_ACTIVE_on" class="radioCheck">
							Oui
						</label>
						<input type="radio" name="ACAISSE_INVOICE_IS_ACTIVE" id="ACAISSE_INVOICE_IS_ACTIVE_off" value="0" {if $ACAISSE_INVOICE_IS_ACTIVE==0}checked{/if}>
						<label for="ACAISSE_INVOICE_IS_ACTIVE_off" class="radioCheck">
							Non
						</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>
		</div>
		<hr>	
		<div class="alert alert-info">
			{l s='Indiquez les statuts de paiement à utiliser pour la caisse :' mod='acaisse'}		
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-4" for="PS_PRESTATILL_PAYMENT">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Statut principale par lequel passe toutes les commandes en magasin.'}">
						{l s='Passage en caisse' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<select name="PS_PRESTATILL_PAYMENT" style="text-align: center" id="PS_PRESTATILL_PAYMENT" >
						<option value="0">{l s='Choisissez un statut' mod='acaisse'}</option>
						{foreach $ORDER_STATES as $os}
							<option value="{$os.id_order_state}" {if $os.id_order_state == $PS_PRESTATILL_PAYMENT}selected="selected"{/if}>{$os.name}</option> 
						{/foreach}
					</select>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-4" for="ACAISSE_id_partial_state">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Statut principale pour les paiements partiels.'}">
						{l s='Paiement partiel' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<select name="ACAISSE_id_partial_state" style="text-align: center" id="ACAISSE_id_partial_state" >
						<option value="0">{l s='Choisissez un statut' mod='acaisse'}</option>
						{foreach $ORDER_STATES as $os}
							<option value="{$os.id_order_state}" {if $os.id_order_state == $ACAISSE_id_partial_state}selected="selected"{/if}>{$os.name}</option> 
						{/foreach}
					</select>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-4" for="ACAISSE_id_multiple_state">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Statut à utiliser lorsqu\'une commande comporte plusieurs modes de paiement.'}">
						{l s='Paiement multiple' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<select name="ACAISSE_id_multiple_state" style="text-align: center" id="ACAISSE_id_multiple_state" >
						<option value="0">{l s='Choisissez un statut' mod='acaisse'}</option>
						{foreach $ORDER_STATES as $os}
							<option value="{$os.id_order_state}" {if $os.id_order_state == $ACAISSE_id_multiple_state}selected="selected"{/if}>{$os.name}</option> 
						{/foreach}
					</select>
				</div>
			</div>
		</div>
		
		<div class="alert alert-info">
			{l s='Autres statuts de paiement en caisse installés et activés :' mod='acaisse'}		
		</div>
		
		<div class="form-group">
			<label class="control-label col-lg-4" for="ACAISSE_id_multiple_state">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Statut à utiliser lorsqu\'une commande comporte plusieurs modes de paiement.'}">
						{l s='Statuts de paiement en caisse' mod='acaisse'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-12">
					<ul class="payment_method_display">
						{foreach $ORDER_STATES as $os}
							{if substr($os.module_name,0,10) == 'prestatill' && !in_array($os.module_name, array('prestatillshopdefault', 'prestatillpartial', 'prestatillmulti'))}
							<li>{$os.name}</li> 
							{/if}
						{/foreach}
					</ul>
				</div>
			</div>
		</div>
		
		<div class="panel-footer">
            <div class="btn-group pull-right">
                <button name="submitPaymentOptions" id="submitPaymentOptions" type="submit" class="btn btn-default"><i class="process-icon-save"></i> {l s='Save' mod='acaisse'}</button>
            </div>
        </div>
</form>