<?php 
class attributes extends Module {
	function __construct(){
		$this->name = 'attributes';
		$this->tab = 'front_office_features';
        $this->author = 'MyPresta.eu';
		$this->version = '1.0.9';
        $this->dir = '/modules/htmlbox/';
		parent::__construct();
		$this->displayName = $this->l('Product Attributes on List');
		$this->description = $this->l('Module displays product attributes on list');
  
 
	}
  
	function install(){
        if (parent::install() == false 
	    OR $this->registerHook('displayProductOnList') == false
        ){
            return false;
        }
        return true;
	}

	function hookdisplayProductOnList($params){
	    
		$product=new Product($params['id_product']);
        
		$combinations=$product->getAttributeCombinations($this->context->language->id);
		
		$combinations = $product->getAttributesGroups($this->context->language->id);
		
		$temp = array();
		
		foreach($combinations as $combination){
			//On ne garde que la taille
			if($combination['id_attribute_group']==1){
				$temp[$combination['attribute_name']] = $combination;
				
				$temp[$combination['attribute_name']]['id_product']=$product->id;
				
				if(!isset($temp[$combination['attribute_name']]['qte']))
					$temp[$combination['attribute_name']]['qte'] = (int)$combination['quantity'];	
				else
					$temp[$combination['attribute_name']]['qte']+=(int)$combination['quantity'];	
			}
			
		}

        $this->smarty->assign('combinations',$temp);
        return $this->display(__FILE__, 'combinations.tpl');
	}
}
?>