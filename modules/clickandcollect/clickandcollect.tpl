<p class="carrier_title">
{l s="Information concernant la livraison"}
</p>

{if $show_click_and_collect}
{capture "string"}
{l s='« Click & Collect (sous 4h) », essayer et retirer vos produits directement dans les boutiques suivantes :' mod='clickandcollect'}
{/capture} 
{$smarty.capture.string|replace:'«':'<span class="carrier-name">'|replace:'»':'</span>'|replace:'-b-':'<b>'|replace:'-/b-':'</b>'|replace:'-br/-':'<br/>'}

<br/><br/>
{foreach from=$sellers item='seller'}
	{if $seller.show_address}
		<div style="float:left;width:50%">
			<a title="{$seller.name}" href="{$seller.seller_url|escape:'html':'UTF-8'}" target="_blank">
				<i class="ft-icon-shop-1 carrier-name " style="font-size:20px"></i>  {$seller.name}
			</a>
			- {$seller.address}, {$seller.postcode} {$seller.city}
		</div>
		
		{if $seller.workinghours}
			

			<div style="float:left">
				<a href="#workinghours_{$seller.id}" data-toggle="collapse" aria-expanded="false" aria-controls="identityItems">
					<i class="ft-icon-clock  carrier-name " style="font-size:20px"></i><span >{l s='See working hours' mod='clickandcollect'}</span>
				</a>
				
				<div class="collapse" id="workinghours_{$seller.id}">
				{foreach from=$seller.workinghours item=days}
				<p>
					{$days->day} :
					{if $days->isActive}
						{$days->timeFrom}-{$days->timeTill}
						{if $days->isBreak} / {$days->timeFrom2}-{$days->timeTill2}{/if}
					{else}
						{l s='Close' mod='clickandcollect'}
					{/if}
				</p>
				{/foreach}
				</div>
			</div>
			<div class="clearfix"></div>
		{/if}
	{/if}
{/foreach}

<br/><br/>
{/if}

{if $dhl}
{capture "string"}
{l s='« Livraison Express (sous 1 à 2 jours) » : vous serez livré directement à l’adresse de votre choix et si vous n\'êtes pas disponible, le livreur pourra aussi déposer votre colis dans un point relais, c’est vous qui choisissez.' mod='clickandcollect'}
{/capture}
{$smarty.capture.string|replace:'«':'<span class="carrier-name">'|replace:'»':'</span>'|replace:'-b-':'<b>'|replace:'-/b-':'</b>'|replace:'-br/-':'<br/>'}
<br/><br/>
{/if}



{if $colissimo}
{capture "string"}
{l s='« Livraison domicile (sous 2 à 3 jours) » vous serez livré directement à l’adresse de votre choix' mod='clickandcollect'}
{/capture}
{$smarty.capture.string|replace:'«':'<span class="carrier-name">'|replace:'»':'</span>'|replace:'-b-':'<b>'|replace:'-/b-':'</b>'|replace:'-br/-':'<br/>'}
<br/><br/>
{/if}


{if $relais}
{capture "string"}
{l s='« Livraison en relais (sous 3 à 4 jours) » : vous sélectionnez votre point relais et c’est parti.' mod='clickandcollect'}
{/capture}
{$smarty.capture.string|replace:'«':'<span class="carrier-name">'|replace:'»':'</span>'|replace:'-b-':'<b>'|replace:'-/b-':'</b>'|replace:'-br/-':'<br/>'}
<br/><br/>
{/if}



