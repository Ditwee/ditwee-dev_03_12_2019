<div class="tab-pane" id="tab-carrier">
	
	<ul>
		<li>
			<b>{l s='La livraison de ce produit est dipsonible avec les transporteurs suivants :' mod='clickandcollect'}</b>
		</li>
		{foreach $carriers as $carrier}
		<li>
			<img src="{$carrier.img}" alt="{$carrier->name}" width="40"/>
			<b>{$carrier.name}</b> {$carrier.delay}
		</li>
		{foreachelse}
	
			<li>
			<b>{l s='Pas de transporteur pour ce produit' mod='clickandcollect'}</b>
			</li>
		{/foreach}
		<li>
			{l s='Pour en savoir plus :' mod='clickandcollect'}
			<a href="{$link->getCMSLink('11')}" title="{l s='Livraison' mod='clickandcollect'}">{l s='Livraison' mod='clickandcollect'}</a>
		</li>
	</ul>
</div>