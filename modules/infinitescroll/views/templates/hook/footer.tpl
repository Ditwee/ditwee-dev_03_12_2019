{*
*
* Template to include on hook footer front-end; passes module options to javascript function
* @package Infinite_Scroll
*}
<script type="text/javascript">

	infinite_scroll = {$options};
	{if isset($pages_nb)}
	infinite_scroll.maxPage = {$pages_nb};
	{/if}
	infinite_scroll.debug = false;
	infinite_scroll.changeNav = true;
	infinite_scroll.loading.msgText = "{l s='Loading...' mod='infinitescroll'}"
	infinite_scroll.loading.finishedMsg = "{l s='No additional posts.' mod='infinitescroll'}"
	infinite_scroll.textGoto = "{l s='Go to previous page' mod='infinitescroll'}"
	infinite_scroll.loading.msg = "";
	jQuery( infinite_scroll.contentSelector ).infinitescroll( infinite_scroll, function(newElements, data, url) {
		eval(infinite_scroll.callback); 
		
		
	});
	
</script>