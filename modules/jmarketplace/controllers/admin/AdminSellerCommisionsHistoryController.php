<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminSellerCommisionsHistoryController extends ModuleAdminController
{
    //public $asso_type = 'shop';

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'seller_commision_history';
        $this->className = 'SellerCommisionHistory';
        $this->lang = false;
        
        $this->context = Context::getContext();
        
        parent::__construct();

        $states = SellerCommisionHistoryState::getStates((int)$this->context->language->id);
        foreach ($states as $state)
            $this->states_array[$state['id_seller_commision_history_state']] = $state['name'];

        $this->_select = 'a.id_order, o.reference, s.name as seller_name, product_name, a.price, a.quantity, a.commision, schsl.name as state_name, a.date_add';
        $this->_join = 'LEFT JOIN `'._DB_PREFIX_.'seller` s ON (s.`id_seller` = a.`id_seller`) 
                        LEFT JOIN `'._DB_PREFIX_.'seller_commision_history_state` schs ON (schs.`id_seller_commision_history_state` = a.`id_seller_commision_history_state`)
                        LEFT JOIN `'._DB_PREFIX_.'seller_commision_history_state_lang` schsl ON (schsl.`id_seller_commision_history_state` = a.`id_seller_commision_history_state` AND schsl.id_lang = '.(int)$this->context->language->id.')
                        LEFT JOIN `'._DB_PREFIX_.'product` p ON (a.`id_product` = p.`id_product`) 
                        LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.`id_product` = p.`id_product` AND pl.id_lang = '.(int)$this->context->language->id.' AND pl.id_shop = '.(int)$this->context->shop->id.') 
                        LEFT JOIN `'._DB_PREFIX_.'orders` o ON (o.`id_order` = a.`id_order`)';

        $this->_where = 'AND a.id_shop = '.(int)$this->context->shop->id;

        $this->_orderBy = 'date_upd';
        $this->_orderWay = 'DESC';

        if (Tools::getValue('seller_incidenceOrderby')) {  
            $this->_orderBy = pSQL(Tools::getValue('seller_incidenceOrderby'));
            $this->_orderWay = pSQL(Tools::getValue('seller_incidenceOrderway'));
        }

        $this->fields_list = array(
            'id_order' => array(
                'title' => $this->l('Order ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'havingFilter' => true,
            ),
            'reference' => array(
                'title' => $this->l('Order reference'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'havingFilter' => true,
            ),
            'seller_name' => array(
                'title' => $this->l('Seller name'),
                'havingFilter' => true,
            ),
            'product_name' => array(
                'title' => $this->l('Product name'),
                'havingFilter' => true,
            ),
            'price' => array(
                'title' => $this->l('Product price'),
                'havingFilter' => true,
                'align' => 'text-right',
                'type' => 'price',
            ),
            'quantity' => array(
                'title' => $this->l('Product quantity'),
                'havingFilter' => true,
                'align' => 'text-right',
            ),
            'commision' => array(
                'title' => $this->l('Commision (€)'),
                'havingFilter' => true,
                'align' => 'text-right',
                'type' => 'price',
            ),
            'state_name' => array(
                'title' => $this->l('Payment state'),
                'type' => 'select',
                'list' => $this->states_array,
                'filter_key' => 'a!id_seller_commision_history_state',
                'filter_type' => 'int',
                'order_key' => 'state_name',
            ),
            'date_add' => array(
                'title' => $this->l('Date add'),
                'type' => 'datetime',
                'orderby' => true,
                'search' => true,
                'havingFilter' => true,
            )
			
			
        );
		

        $this->bulk_actions = array(
                'delete' => array(
                        'text' => $this->l('Delete selected'),
                        'confirm' => $this->l('Delete selected items?'),
                        'icon' => 'icon-trash'
                ),
                'pay' => array(
                        'text' => $this->l('Pay selected'),
                        'confirm' => $this->l('Pay selected items?'),
                        'icon' => 'icon-money'
                ),
                'cancel' => array(
                        'text' => $this->l('Cancel selected'),
                        'confirm' => $this->l('Cancel selected items?'),
                        'icon' => 'icon-ban'
                )
        );
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();
    }

    public function renderList()
    {
        //$this->addRowAction('view');
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        
        return parent::renderList();
    }

    public function renderForm()
    {
        $title = $this->l('Add new commission');
        if (Tools::getValue('id_seller_commision_history')) {
            $id_seller_commision_history = (int)Tools::getValue('id_seller_commision_history');
            $seller_commision_history = new SellerCommisionHistory($id_seller_commision_history);
            $title = $this->l('Edit commission').' '.$seller_commision_history->product_name;
        }
        
        $orders = array(array('id_order' => 0, 'reference' => $this->l('Not apply')));     
        $orders = array_merge($orders, Order::getOrdersWithInformations());
        
        $products = array(array('id_product' => 0, 'name' => $this->l('Not apply')));     
        $products = array_merge($products, Product::getSimpleProducts($this->context->language->id));
        
        $states = SellerCommisionHistoryState::getStates((int)$this->context->language->id);
        		
        $this->fields_form = array(
            'legend' => array(
                    'title' => $title,
                    'icon' => 'icon-money'
            ),
            'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('Order'),
                        'name' => 'id_order',
                        'required' => false,
                        'options' => array(
                              'query' => $orders,
                              'id' => 'id_order',
                              'name' => 'reference'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Seller'),
                        'name' => 'id_seller',
                        'required' => false,
                        'options' => array(
                              'query' => Seller::getSellers($this->context->shop->id),
                              'id' => 'id_seller',
                              'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Product'),
                        'name' => 'id_product',
                        'required' => false,
                        'options' => array(
                              'query' => $products,
                              'id' => 'id_product',
                              'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Concept'),
                        'name' => 'product_name',
                        'lang' => false,
                        'col' => 3,
                        'required' => false,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Price').' ('.$this->context->currency->sign.')',
                        'name' => 'price',
                        'lang' => false,
                        'col' => 2,
                        'required' => false,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Quantity'),
                        'name' => 'quantity',
                        'lang' => false,
                        'col' => 2,
                        'required' => false,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Commission').' ('.$this->context->currency->sign.')',
                        'name' => 'commision',
                        'lang' => false,
                        'col' => 2,
                        'required' => false,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Payment state'),
                        'name' => 'id_seller_commision_history_state',
                        'required' => false,
                        'options' => array(
                              'query' => $states,
                              'id' => 'id_seller_commision_history_state',
                              'name' => 'name'
                        )
                    )
            )
        );

        $this->fields_form['submit'] = array(
                'title' => $this->l('Save'),
        );

        return parent::renderForm();
    }
    
    public function postProcess() {   
        if (Tools::isSubmit('submitAddseller_commision_history')) {
            $id_order = Tools::getValue('id_order');
            $id_product = Tools::getValue('id_product');
            $id_seller = Tools::getValue('id_seller');
            $concept = Tools::getValue('product_name');
            $price = Tools::getValue('price');
            $quantity = Tools::getValue('quantity');
            $commision = Tools::getValue('commision');
			$ready_ship =  Tools::getValue('ready_ship');
			$delivered_ship =  Tools::getValue('delivered_ship');
            $id_seller_commision_history_state = (int)Tools::getValue('id_seller_commision_history_state');
            
            if (!$concept) 
                $this->errors[] = Tools::displayError('The concept is not valid.', 'AdminSellerCommisionsHistoryController');
            
            if (!Validate::isFloat($price)) 
                $this->errors[] = Tools::displayError('The unit price is not valid.', 'AdminSellerCommisionsHistoryController');
            
            if (!Validate::isInt($quantity)) 
                $this->errors[] = Tools::displayError('The quantity is not valid.', 'AdminSellerCommisionsHistoryController');
            
            if (!Validate::isFloat($commision)) 
                $this->errors[] = Tools::displayError('The commission is not valid.', 'AdminSellerCommisionsHistoryController');
            
            if (count($this->errors) == 0) {
                if (Tools::getValue('id_seller_commision_history'))
                    $seller_commision_history = new SellerCommisionHistory((int)Tools::getValue('id_seller_commision_history'));
                else
                    $seller_commision_history = new SellerCommisionHistory();

                $seller_commision_history->id_order = (int)$id_order;
                $seller_commision_history->id_product = (int)$id_product;
                $seller_commision_history->product_name = (string)$concept;
                $seller_commision_history->id_seller = (int)$id_seller;
                $seller_commision_history->id_shop = $this->context->shop->id;
                $seller_commision_history->price = (float)$price;
                $seller_commision_history->quantity = (int)$quantity;
                $seller_commision_history->commision = (float)$commision;
				$seller_commision_history->ready_ship = (int)$ready_ship;
				$seller_commision_history->delivered_ship = (int)$delivered_ship;
                $seller_commision_history->id_seller_commision_history_state = (int)$id_seller_commision_history_state;

                if (Tools::getValue('id_seller_commision_history'))
                    $seller_commision_history->update();
                else
                    $seller_commision_history->add();
            }
        }
        else if(Tools::isSubmit('submitBulkpayseller_commision_history')) {
            $seller_commissions = Tools::getValue('seller_commision_historyBox');
            if (is_array($seller_commissions)) {
                foreach ($seller_commissions as $id_seller_commision_history) {
					//Nico : Stripe
                    $seller_commision_history = new SellerCommisionHistory($id_seller_commision_history);
					$seller = new Seller($seller_commision_history->id_seller);
					$stripe = Module::getInstanceByName('stripe_official');
					$error_tmp = array();
					
					if($seller_commision_history->stripe_transfer_id)
						$error_tmp[] = Tools::displayError('Commission '.$id_seller_commision_history.' of order '.$seller_commision_history->id_order.' is already paid.', 'AdminSellerCommisionsHistoryController');	

					if($seller_commision_history->id_seller_commision_history_state==SellerCommisionHistoryState::getIdByReference('paid'))
						$error_tmp[] = Tools::displayError('Commission '.$id_seller_commision_history.' of order '.$seller_commision_history->id_order.' is already paid.', 'AdminSellerCommisionsHistoryController');	

					if (!$stripe || !$stripe->active)
						$error_tmp[] = Tools::displayError('Stripe isnt active', 'AdminSellerCommisionsHistoryController');	
	
					if(!$seller->key_stripe)
						$error_tmp[] = Tools::displayError('Seller '.$seller->name." hasnt Stripe Key", 'AdminSellerCommisionsHistoryController');
					
					
					// Set tax_code
					$taxes = OrderDetail::getTaxListStatic($seller_commision_history->id_order);
					$tax = new Tax($taxes[0]['id_tax']);
					$rate = $tax->rate + 0;
					$amount = Tools::ps_round($seller_commision_history->commision*(1+$rate/100), _PS_PRICE_COMPUTE_PRECISION_)*100;
				
					if (count($error_tmp) == 0) {
						try{
							$order = new Order((int)$seller_commision_history->id_order);
							
							\Stripe\Stripe::setApiKey($stripe->getSecretKey());
							$transfer = \Stripe\Transfer::create(array(
								  "amount" => $amount,
								  "currency" => "eur",
								  "destination" => $seller->key_stripe,
								  "description" => $order->reference." - ".$seller_commision_history->product_name,
								  //"transfer_group" => "ORDER_95"
								));
				
							$seller_commision_history->id_seller_commision_history_state = SellerCommisionHistoryState::getIdByReference('paid');
							$seller_commision_history->stripe_transfer_id = $transfer->id;
							$seller_commision_history->update();
							$this->confirmations[] = 'Commission '.$id_seller_commision_history.' of order '.$seller_commision_history->id_order.' payed with success.';
						}
						catch (Exception  $e) {
							$error_tmp[] = Tools::displayError($e->getMessage(), 'AdminSellerCommisionsHistoryController');
						}
					} 
					$this->errors = array_merge($this->errors,$error_tmp);
                }
            }
        }
        else if(Tools::isSubmit('submitBulkcancelseller_commision_history')) {
            $seller_commissions = Tools::getValue('seller_commision_historyBox');
            if (is_array($seller_commissions)) {
                foreach ($seller_commissions as $id_seller_commision_history) {
                    
					$seller_commision_history = new SellerCommisionHistory($id_seller_commision_history);
					$stripe = Module::getInstanceByName('stripe_official');
					$error_tmp = array();
					
					if($seller_commision_history->id_seller_commision_history_state==SellerCommisionHistoryState::getIdByReference('cancel'))
						$error_tmp[] = Tools::displayError('Commission '.$id_seller_commision_history.' of order '.$seller_commision_history->id_order.' is already canceled.', 'AdminSellerCommisionsHistoryController');	

					if($seller_commision_history->stripe_transfer_id){
						
						try{
							\Stripe\Stripe::setApiKey($stripe->getSecretKey());
							$tr = \Stripe\Transfer::retrieve($seller_commision_history->stripe_transfer_id);
							$re = $tr->reversals->create();
							$seller_commision_history->stripe_transfer_id = "";
							$this->confirmations[] = 'Commission '.$id_seller_commision_history.' of order '.$seller_commision_history->id_order.' has been canceled with succes.';
						
						}
						catch (Exception  $e) {
							$error_tmp[] = Tools::displayError($e->getMessage(), 'AdminSellerCommisionsHistoryController');
						}	
					}
					
					
                    $seller_commision_history->id_seller_commision_history_state = SellerCommisionHistoryState::getIdByReference('cancel');
	
                    $seller_commision_history->update();
					
					$this->errors = array_merge($this->errors,$error_tmp);
                }
            }
        }
        else {
            parent::postProcess(); 
        }
    }
}