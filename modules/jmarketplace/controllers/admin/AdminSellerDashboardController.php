<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminSellerDashboardController extends ModuleAdminController
{
    public $asso_type = 'shop';

    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;
        
        $this->context = Context::getContext();

        parent::__construct();
    }
    
    public function setMedia() {
        parent::setMedia();
        $this->addJqueryUI('ui.datepicker');
        $this->context->controller->addJS(_PS_MODULE_DIR_.'jmarketplace/views/js/Chart.bundle.min.js', 'all'); 
        $this->context->controller->addJS(_PS_MODULE_DIR_.'jmarketplace/views/js/admindashboard.js', 'all');
    }

    public function renderList()
    {
        $year = date('Y');
        
        /*$year_array = explode('-', Configuration::get('CONTABE_DATE_FROM'));
        $year = $year_array[0];
        $month = date('m');
        $day = date('d');*/
        
        $from = Configuration::get('JMARKETPLACE_EARNINGS_FROM').' 00:00:00';     
        $to = Configuration::get('JMARKETPLACE_EARNINGS_TO').' 23:59:59';
        
        if (Tools::isSubmit('submitFilterDate')) {
            $from = Tools::getValue('from').' 00:00:00';
            $to = Tools::getValue('to').' 23:59:59';
            Configuration::updateValue('JMARKETPLACE_EARNINGS_FROM', Tools::getValue('from'));
            Configuration::updateValue('JMARKETPLACE_EARNINGS_TO', Tools::getValue('to'));
        }
        
        $commisions_chart = array();
        $from_array = explode(' ', $from);
        $to_array = explode(' ', $to);

        $date_next = $from_array[0];
        $date_end = $to_array[0];
        
        while (Dashboard::compare_dates($date_next, $date_end) > 0) {
            $year_array = explode('-', $date_next);
            $year = $year_array[0];
            $month_array = explode('-', $date_next);
            $month = $month_array[1];
            $commisions_chart[$year.'-'.$month] = 0;
            $date_next = strtotime('+28 day', strtotime($date_next));
            $date_next = date('Y-m-d', $date_next);
        }      
        
        $total_entry_tax_incl = SellerCommisionHistory::getTotalPriceHistory(2, $this->context->shop->id, $from, $to);
        
        if (!$total_entry_tax_incl)
            $total_entry_tax_incl = 0;
        
        $total_spend_tax_incl = SellerCommisionHistory::getTotalCommissionForSellers(2, $this->context->shop->id, $from, $to);
        
        if (!$total_spend_tax_incl)
            $total_spend_tax_incl = 0;
        
        $gross_benefit = $total_entry_tax_incl - $total_spend_tax_incl;

        $total_entry_tax_excl = SellerCommisionHistory::getTotalVariableCommissionsForAdmin(2, $this->context->shop->id, $from, $to);
        
        if (!$total_entry_tax_excl)
            $total_entry_tax_excl = 0;
        
        $total_spend_tax_excl = SellerCommisionHistory::getTotalCommissionForSellers(2, $this->context->shop->id, $from, $to);
        
        if (!$total_spend_tax_excl)
            $total_spend_tax_excl = 0;
        
        $net_benefit = $total_entry_tax_excl - $total_spend_tax_excl;
        
        $month_array = array(
            $this->module->l('Jan', 'AdminSellerDashboard'), 
            $this->module->l('Feb', 'AdminSellerDashboard'), 
            $this->module->l('Mar', 'AdminSellerDashboard'), 
            $this->module->l('Apr', 'AdminSellerDashboard'), 
            $this->module->l('May', 'AdminSellerDashboard'), 
            $this->module->l('Jun', 'AdminSellerDashboard'), 
            $this->module->l('Jul', 'AdminSellerDashboard'), 
            $this->module->l('Aug', 'AdminSellerDashboard'), 
            $this->module->l('Sep', 'AdminSellerDashboard'), 
            $this->module->l('Oct', 'AdminSellerDashboard'), 
            $this->module->l('Nov', 'AdminSellerDashboard'), 
            $this->module->l('Dec', 'AdminSellerDashboard')            
        );
        
        //stats
        $labels = '';
        $entry_string = '';
        $spend_string = '';
        $benefit_string = '';
        
        foreach ($commisions_chart as $key => $value) {
            $entry_month = $value + Tools::ps_round(SellerCommisionHistory::getTotalPriceHistory(2, $this->context->shop->id, $key.'-01', $key.'-31'), 2);
            $spend_month = $value + Tools::ps_round(SellerCommisionHistory::getTotalCommissionForSellers(2, $this->context->shop->id, $key.'-01', $key.'-31'), 2);
            $benefit_month = $value + Tools::ps_round(SellerCommisionHistory::getTotalVariableCommissionsForAdmin(2, $this->context->shop->id, false, $key.'-01', $key.'-31'), 2);
            $benefit_month = $value + $benefit_month - Tools::ps_round(SellerCommisionHistory::getTotalFixCommissionsForAdmin(2, $this->context->shop->id, false, $key.'-01', $key.'-31'), 2);
            
            //$labels .= '"'.$month_array[$i-1].'",';
            $labels .= '"'.$key.'",';

            if (!$entry_month)
                $entry_month = 0;

            if (!$spend_month)
                $spend_month = 0;
            
            //$pyg_string .= '["'.$month.'", '.$entry_month.', '.$spend_month.'],';
            $entry_string .= $entry_month.',';
            $spend_string .= $spend_month.',';

            if (!$benefit_month)
                $benefit_month = 0;

            //$benefit_string .= '["'.$month.'", '.$benefit_month.'],';
            $benefit_string .= $benefit_month.',';
        }
        
        $range_dates = Configuration::get('JMARKETPLACE_EARNINGS_FROM').' - '.Configuration::get('JMARKETPLACE_EARNINGS_TO');
  
        $best_sellers = array();
        $sellers = Seller::getSellers($this->context->shop->id);
        
        foreach ($sellers as $s) {
            $seller = new Seller($s['id_seller']);
            $seller_commissions = SellerCommisionHistory::getBenefitsBySeller(2, $seller->id, $from, $to);
            $admin_commissions = SellerCommisionHistory::getTotalVariableCommissionsForAdmin(2, $this->context->shop->id, $seller->id, $from, $to);
            $admin_commissions = $admin_commissions - SellerCommisionHistory::getTotalFixCommissionsForAdmin(2, $this->context->shop->id, $seller->id, $from, $to);
            
            if (!$seller_commissions) {
                $seller_commissions = 0;
                $admin_commissions = 0;
            }
            
            $best_sellers[] = array(
                'id_seller' => $seller->id,
                'name' => $seller->name,
                'shop' => $seller->shop,
                'commissions' => $seller_commissions,
                'admin_commissions' => $admin_commissions,
            );
        }
        
        $aux = array();
        foreach ($best_sellers as $key => $row) {
            $aux[$key] = $row['admin_commissions'];
        }
        
        array_multisort($aux, SORT_DESC, $best_sellers);
        $best_sellers = array_slice($best_sellers, 0, 10);  

        $this->context->smarty->assign(array(
            'name' => $this->module->name,
            'range_dates' => $range_dates,
            'labels' => $labels,
            'entry_string' => $entry_string,
            'spend_string' => $spend_string,
            'total_entry_tax_incl' => $total_entry_tax_incl,
            'total_spend_tax_incl' => $total_spend_tax_incl,
            'gross_benefit' => $gross_benefit,
            'total_entry_tax_excl' => $total_entry_tax_excl,
            'total_spend_tax_excl' => $total_spend_tax_excl,
            'net_benefit' => $net_benefit,
            'benefit_string' => $benefit_string,
            'year' => $year,
            'from' => $from,
            'to' => $to,
            'best_sellers' => $best_sellers,
            'sign' => $this->context->currency->sign,
            'url_form' => 'index.php?tab=AdminSellerDashboard&token='.Tools::getAdminToken('AdminSellerDashboard'.(int)Tab::getIdFromClassName('AdminSellerDashboard').(int)$this->context->employee->id),
        ));
        
	return $this->context->smarty->fetch($this->module->getLocalPath().'views/templates/admin/dashboard.tpl');
    }
    
    public function compare_dates($date_start, $date_end) {
        $endTimestamp = strtotime($date_end);
        $beginTimestamp = strtotime($date_start);
        return ceil(($endTimestamp - $beginTimestamp) / 86400);
    }
}