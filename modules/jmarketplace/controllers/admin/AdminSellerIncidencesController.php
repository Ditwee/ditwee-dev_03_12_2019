<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include_once dirname(__FILE__).'/../../classes/SellerIncidence.php';
include_once dirname(__FILE__).'/../../classes/SellerIncidenceMessage.php';

class AdminSellerIncidencesController extends ModuleAdminController
{
    //public $asso_type = 'shop';

    public function __construct()
    {
        $this->bootstrap = true;
        $this->table = 'seller_incidence';
        $this->className = 'SellerIncidence';
        $this->lang = false;
        $this->states_array = array();
        $this->types_array = array();
        $this->priorities_array = array();
        
        $this->context = Context::getContext();

        parent::__construct();
    }

    public function initPageHeaderToolbar()
    {
        parent::initPageHeaderToolbar();
    }

    public function renderList()
    {
        $this->addRowAction('view');
        //$this->addRowAction('edit');
        $this->addRowAction('delete');
        
        $this->_select = 'a.reference as incidence_ref, o.reference as order_ref, c.firstname, c.lastname, a.date_add';
        $this->_join = 'LEFT JOIN `'._DB_PREFIX_.'customer` c ON (c.`id_customer` = a.`id_customer`)
                        LEFT JOIN `'._DB_PREFIX_.'orders` o ON (o.`id_order` = a.`id_order`)';
        $this->_where = 'AND a.id_shop = '.(int)$this->context->shop->id;
        
        if (Tools::isSubmit('submitFilter')) {            
            if (Tools::getValue('seller_incidenceFilter_incidence_ref') != '')
                $this->_where = 'AND a.reference LIKE "%'.pSQL(Tools::getValue('seller_incidenceFilter_incidence_ref')).'%"';
            
            if (Tools::getValue('seller_incidenceFilter_order_ref') != '')
                $this->_where = 'AND o.reference LIKE "%'.pSQL(Tools::getValue('seller_incidenceFilter_order_ref')).'%"';
            
            if (Tools::getValue('seller_incidenceFilter_firstname') != '')
                $this->_where = 'AND c.firstname LIKE "%'.pSQL(Tools::getValue('seller_incidenceFilter_firstname')).'%"';
            
            if (Tools::getValue('seller_incidenceFilter_lastname') != '')
                $this->_where = 'AND c.lastname LIKE "%'.pSQL(Tools::getValue('seller_incidenceFilter_lastname')).'%"';
            
            if (Tools::getValue('seller_incidenceFilter_id_incidence_state'))
                $this->_where = 'AND ist.id_incidence_state = '.(int)Tools::getValue('seller_incidenceFilter_id_incidence_state');
            
            if (Tools::getValue('seller_incidenceFilter_id_incidence_type'))
                $this->_where = 'AND it.id_incidence_type = '.(int)Tools::getValue('seller_incidenceFilter_id_incidence_type');
            
            if (Tools::getValue('seller_incidenceFilter_id_incidence_priority'))
                $this->_where = 'AND ip.id_incidence_priority = '.(int)Tools::getValue('seller_incidenceFilter_id_incidence_priority');           
        }
        
        $this->_orderBy = 'date_upd';
        $this->_orderWay = 'DESC';
        
        if (Tools::getValue('seller_incidenceOrderby')) {  
            $this->_orderBy = pSQL(Tools::getValue('seller_incidenceOrderby'));
            $this->_orderWay = pSQL(Tools::getValue('seller_incidenceOrderway'));
        }

        $this->fields_list = array(
            'incidence_ref' => array(
                'title' => $this->l('Incidence reference'),
                'havingFilter' => true,
            ),
            'order_ref' => array(
                'title' => $this->l('Order reference'),
                'havingFilter' => true,
            ),
            'firstname' => array(
                'title' => $this->l('Customer name'),
                'havingFilter' => true,
            ),
            'lastname' => array(
                'title' => $this->l('Customer lastname'),
                'havingFilter' => true,
            ),
            'date_add' => array(
                'title' => $this->l('Date add'),
            ),
            'date_upd' => array(
                'title' => $this->l('Date update'),
            )
        );
        $this->bulk_actions = array(
                'delete' => array(
                        'text' => $this->l('Delete selected'),
                        'confirm' => $this->l('Delete selected items?'),
                        'icon' => 'icon-trash'
                )
        );

        return parent::renderList();
    }
    
    public function postProcess() {
        
        if($this->display == 'view') {
            
            $incidence = new SellerIncidence((int)Tools::getValue('id_seller_incidence'));
            
            $messages = SellerIncidence::getMessages($incidence->id);

            $this->context->smarty->assign(array(
                'incidence' => $incidence, 
                'messages' => $messages, 
                'url_post' => self::$currentIndex.'&id_incidence='.$incidence->id.'&viewincidence&token='.$this->token,
                /*'url_cancel' => self::$currentIndex.'&viewincidence&token='.$this->token,*/
                'token' => $this->token,
            ));
        }

        if (Tools::isSubmit('deleteseller_incidence')) {
            $id_seller_incidence = (int)Tools::getValue('id_seller_incidence');
            $seller_incidence = new SellerIncidence($id_seller_incidence);
            $seller_incidence->delete();
        }
        
        /*if (Tools::isSubmit('submitAddincidence')) {
            $incidence = new SellerIncidence();
            $incidence->reference = SellerIncidence::generateReference();
            $incidence->id_order = (int)Tools::getValue('id_order');
            
            $order = new Order($incidence->id_order);
            $customer = $order->getCustomer();
            
            $incidence->id_customer = (int)$customer->id;
            $incidence->id_state = (int)Tools::getValue('id_incidence_state');
            $incidence->id_type = (int)Tools::getValue('id_incidence_type');
            $incidence->id_priority = (int)Tools::getValue('id_incidence_priority');
            $incidence->id_shop = (int)$this->context->shop->id;
            $incidence->add();
            
            $incidenceMessage = new SellerIncidenceMessage();
            $incidenceMessage->id_incidence = (int)$incidence->id;
            $incidenceMessage->id_customer = (int)$customer->id;
            $incidenceMessage->id_employee = 0;
            $incidenceMessage->description = pSQL(Tools::getValue('description'));
            $incidenceMessage->readed = 0;
            $incidenceMessage->add();
            //mail nueva incidencia
            //parent::postProcess();   
        }*/
    }
}