<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class JmarketplaceDashboardModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    
    public function setMedia() {
        parent::setMedia();
        
        $this->addJqueryUI('ui.datepicker');
        $this->context->controller->addJS(_MODULE_DIR_.$this->module->name.'/views/js/Chart.bundle.min.js', 'all');
        
        $ps_version = $this->module->getPrestaShopVersion();
        
        if ($ps_version == '1.7') 
            $this->context->controller->addJS(_MODULE_DIR_.$this->module->name.'/views/js/sellerdashboard.js', 'all');
        else
            $this->context->controller->addJS(_MODULE_DIR_.$this->module->name.'/views/js/dashboard.js', 'all');
    }

    public function initContent() {
        
        parent::initContent();
        
        if(!$this->context->cookie->id_customer)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        $is_seller = Seller::isSeller($this->context->cookie->id_customer, $this->context->shop->id);
        if (!$is_seller)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        $id_seller = Seller::getSellerByCustomer($this->context->cookie->id_customer);
        
        $seller = new Seller($id_seller);
        
        if ($seller->active == 0)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        $param = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);
        $url_seller_profile = $this->module->getJmarketplaceLink('jmarketplace_seller_rule', $param);
        
        $months = array(
            $this->module->l('Jan', 'dashboard'),
            $this->module->l('Feb', 'dashboard'),
            $this->module->l('Mar', 'dashboard'),
            $this->module->l('Apr', 'dashboard'),
            $this->module->l('May', 'dashboard'),
            $this->module->l('Jun', 'dashboard'),
            $this->module->l('Jul', 'dashboard'),
            $this->module->l('Aug', 'dashboard'),
            $this->module->l('Sept', 'dashboard'),
            $this->module->l('Oct', 'dashboard'),
            $this->module->l('Nov', 'dashboard'),
            $this->module->l('Dec', 'dashboard')
        );
        
        $chart_label = '';
        $chart_data = '';
        $from = explode(' ', $seller->date_add);
        $from = $from[0].' 00:00:00';        
        $to = date('Y-m-d').' 23:59:59';
        
        if (Tools::isSubmit('submitFilterDate')) {
            $from = Tools::getValue('from').' 00:00:00';
            $to = Tools::getValue('to').' 23:59:59';
        }
        
        $commisions_chart = array();
        $from_array = explode(' ', $from);
        $to_array = explode(' ', $to);

        $date_next = $from_array[0];
        $date_end = $to_array[0];
        
        while (Dashboard::compare_dates($date_next, $date_end) > 0) {
            $year_array = explode('-', $date_next);
            $year = $year_array[0];
            $month_array = explode('-', $date_next);
            $month = $month_array[1];
            $commisions_chart[$year.'-'.$month] = 0;
            $date_next = strtotime('+30 day', strtotime($date_next));
            $date_next = date('Y-m-d', $date_next);
        }
        
       //d($commisions_chart);
        
        $commisions = Dashboard::getCommisionHistoryBySeller($id_seller, $this->context->language->id, $this->context->shop->id, $from, $to);
        
        if ($commisions) {
            foreach ($commisions as $c) {
                $date_add_parts = explode(' ', $c['date_add']);
                $date_add_parts = explode('-', $date_add_parts[0]);
                
                /*if ($date_add_parts[1] < 10)
                    $date_add_parts[1] = '0'.$date_add_parts[1];*/
                
                $index = $date_add_parts[0].'-'.($date_add_parts[1]);
                if (array_key_exists($index, $commisions_chart)) 
                    $commisions_chart[$index] = round($commisions_chart[$index] + $c['commision'], 2);
                else 
                    $commisions_chart[$index] = round($c['commision'], 2);
            }
            
            ksort($commisions_chart);

            foreach ($commisions_chart as $key => $value) {
                $date_parts = explode('-', $key);
                $month_value = $months[$date_parts[1]-1];
                $chart_label .= "'".$month_value."-".$date_parts[0]."',";
                $chart_data .= $value.',';
            }
            
            $chart_label = Tools::substr($chart_label, 0, -1);
            $chart_data = Tools::substr($chart_data, 0, -1);
            
            $sales = Dashboard::getSalesBySeller($id_seller, $from, $to);
            $num_orders = Dashboard::getNumOrdersBySeller($id_seller, $from, $to);
            $cart_value = $sales / $num_orders;
            $num_products = Dashboard::getProductQuantityBySeller($id_seller, $from, $to);
            $commision = SellerCommision::getCommisionBySeller($id_seller);
            $benefits = Dashboard::getBenefitsBySeller($id_seller, $from, $to);
        }
        else {
            $sales = 0;
            $num_orders = 0;
            $cart_value = 0;
            $num_products = 0;
            $commision = SellerCommision::getCommisionBySeller($id_seller);
            $benefits = 0;
        }      
        
        $from = explode(' ', $from);
        $from = $from[0];
        $to = explode(' ', $to);
        $to = $to[0];

        $this->context->smarty->assign(array(
            'seller_link' => $url_seller_profile,
            'show_import_product' => Configuration::get('JMARKETPLACE_SELLER_IMPORT_PROD'),
            'show_orders' => Configuration::get('JMARKETPLACE_SHOW_ORDERS'),
            'show_contact' => Configuration::get('JMARKETPLACE_SHOW_CONTACT'),
            'show_edit_seller_account' => Configuration::get('JMARKETPLACE_SHOW_EDIT_ACCOUNT'),
            'show_manage_orders' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS'),
            'show_manage_carriers' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER'),
            'show_dashboard' => Configuration::get('JMARKETPLACE_SHOW_DASHBOARD'),
            'show_seller_invoice' => Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE'),
            'show_menu_top' => Configuration::get('JMARKETPLACE_MENU_TOP'),
            'show_menu_options' => Configuration::get('JMARKETPLACE_MENU_OPTIONS'),
            'sales' => Tools::displayPrice($sales, $this->context->currency->id),
            'num_orders' => $num_orders,
            'cart_value' => Tools::displayPrice($cart_value, $this->context->currency->id),
            'num_products' => $num_products,
            'commision' => $commision,
            'benefits' => Tools::displayPrice($benefits, $this->context->currency->id),
            'chart_label' => $chart_label,
            'chart_data' => $chart_data,
            'currency_iso_code' => $this->context->currency->iso_code,
            'from' => $from,
            'to' => $to,
            'mesages_not_readed' => SellerIncidenceMessage::getNumMessagesNotReadedBySeller($id_seller),
        ));
        
        if (Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE') == 1) {
            $total_funds = 0;
            $orders = SellerTransferCommision::getCommisionHistoryBySeller($id_seller, (int)$this->context->language->id, (int)$this->context->shop->id);
           
            if (is_array($orders) && count($orders) > 0) {
                foreach ($orders as $key => $o) {
                    if (SellerTransferCommision::isSellerTransferCommission($o['id_seller_commision_history']) == 0)
                        $total_funds = $total_funds + $o['commision'];
                }          
            }

            $this->context->smarty->assign('total_funds', Tools::displayPrice($total_funds, $this->context->currency->id));
        }
        
        $ps_version = $this->module->getPrestaShopVersion();
        $this->context->smarty->assign('ps_version', $ps_version);
        
        if ($ps_version == '1.7') {
            $this->context->smarty->assign(array(
                'navigationPipe' => '>',
                'tpl_name' => 'dashboard'
            ));
            
            $this->setTemplate('module:jmarketplace/views/templates/front/page.tpl');
        }
        else {
            $this->setTemplate('dashboard.tpl');
        } 
    }
}