<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class JmarketplaceSellerordersModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    public function initContent() {
        
        parent::initContent();
        
        if(!$this->context->cookie->id_customer)
            Tools::redirect($this->context->link->getPageLink('my-account', true));

        $id_seller = Seller::getSellerByCustomer($this->context->cookie->id_customer);
        $is_seller = Seller::isSeller($this->context->cookie->id_customer, $this->context->shop->id);
        
        if (!$is_seller)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        $seller = new Seller($id_seller);
        
        if ($seller->active == 0)
            Tools::redirect($this->context->link->getPageLink('my-account', true));
        
        if (!Configuration::get('JMARKETPLACE_SHOW_ORDERS')) 
            Tools::redirect($this->context->link->getPageLink('my-account', true));
       
        $orders = SellerCommisionHistory::getCommisionHistoryBySeller($id_seller, $this->context->language->id, $this->context->shop->id);
        
        if ($orders) {
            $i = 0;
            foreach ($orders as $o) {
                $orders[$i]['price'] = Tools::displayPrice($o['price']);
                $orders[$i]['commision'] = Tools::displayPrice($o['commision']);
                $i++;
            }  
        }
        
        $param = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);			
	$url_seller_profile = $this->module->getJmarketplaceLink('jmarketplace_seller_rule', $param);

        $this->context->smarty->assign(array(
            'show_reference' => Configuration::get('JMARKETPLACE_SHOW_REFERENCE'),
            'show_quantity' => Configuration::get('JMARKETPLACE_SHOW_QUANTITY'),
            'show_price' => Configuration::get('JMARKETPLACE_SHOW_PRICE'),
            'show_images' => Configuration::get('JMARKETPLACE_SHOW_IMAGES'),
            'show_import_product' => Configuration::get('JMARKETPLACE_SELLER_IMPORT_PROD'),
            'show_orders' => Configuration::get('JMARKETPLACE_SHOW_ORDERS'),
            'show_manage_orders' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS'),
            'show_manage_carriers' => Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER'),
            'show_dashboard' => Configuration::get('JMARKETPLACE_SHOW_DASHBOARD'),
            'show_seller_invoice' => Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE'),
            'show_edit_seller_account' => Configuration::get('JMARKETPLACE_SHOW_EDIT_ACCOUNT'),
            'show_contact' => Configuration::get('JMARKETPLACE_SHOW_CONTACT'),
            'show_dashboard' => Configuration::get('JMARKETPLACE_SHOW_DASHBOARD'),
            'show_menu_top' => Configuration::get('JMARKETPLACE_MENU_TOP'),
            'show_menu_options' => Configuration::get('JMARKETPLACE_MENU_OPTIONS'),
            'orders' => $orders,
            'benefits' => Tools::displayPrice(SellerCommisionHistory::getBenefitsBySeller(2, $id_seller)),
            'seller_link' => $url_seller_profile,
            'mesages_not_readed' => SellerIncidenceMessage::getNumMessagesNotReadedBySeller($id_seller),
        ));
        
        if (Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE') == 1) {
            $total_funds = 0;
            $orders = SellerTransferCommision::getCommisionHistoryBySeller($id_seller, (int)$this->context->language->id, (int)$this->context->shop->id);
           
            if (is_array($orders) && count($orders) > 0) {
                foreach ($orders as $o) {
                    if (SellerTransferCommision::isSellerTransferCommission($o['id_seller_commision_history']) == 0)
                        $total_funds = $total_funds + $o['commision'];
                }          
            }

            $this->context->smarty->assign('total_funds', Tools::displayPrice($total_funds, $this->context->currency->id));
        }
        
        $ps_version = $this->module->getPrestaShopVersion();
        $this->context->smarty->assign('ps_version', $ps_version);
        
        if ($ps_version == '1.7') {
            $this->context->smarty->assign(array(
                'navigationPipe' => '>',
                'tpl_name' => 'sellerorders'
            ));
            
            $this->setTemplate('module:jmarketplace/views/templates/front/page.tpl');
        }
        else {
            $this->setTemplate('sellerorders.tpl');
        }
    }
}