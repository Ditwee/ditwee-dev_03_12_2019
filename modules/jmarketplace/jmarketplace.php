<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
  exit;
 
class Jmarketplace extends Module
{
    const INSTALL_SQL_FILE = 'install.sql';
    public $output;
    
    public function __construct()
    {
        $this->name = 'jmarketplace';
        $this->tab = 'market_place';
        $this->version = '100.3.7.3';
        $this->author = 'Jose Aguilar';
        $this->need_instance = 0;
        $this->bootstrap = true;
        $this->module_key = "5116f4d7344d9fdd2e57b2bbc885852e";
        $this->controllers = array(
            'addproduct', 
            'addseller', 
            'contactseller', 
            'editproduct', 
            'editseller',
            'favoriteseller',
            'selleraccount',
            'sellercomments',
            'sellermessages',
            'sellerorders',
            'sellerpayment',
            'sellerproductlist',
            'sellerproducts',
            'sellerprofile',
            'sellers',
            'dashboard',
            'orders',
            'carriers',
            'addcarrier',
            'csvproducts',
            'sellerinvoice',
            'sellerinvoicehistory',
        );

        parent::__construct();

        $this->displayName = $this->l('JA Marketplace');
        $this->description = $this->l('Allow to your customers sell in your shop to exchange for a commission.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        
        include_once dirname(__FILE__).'/classes/Seller.php';
        include_once dirname(__FILE__).'/classes/SellerProduct.php';
        include_once dirname(__FILE__).'/classes/SellerCommision.php';
        include_once dirname(__FILE__).'/classes/SellerCommisionHistory.php';
        include_once dirname(__FILE__).'/classes/SellerCommisionHistoryState.php';
        include_once dirname(__FILE__).'/classes/SellerOrder.php';
        include_once dirname(__FILE__).'/classes/SellerTransport.php';
        include_once dirname(__FILE__).'/classes/SellerPayment.php';
        include_once dirname(__FILE__).'/classes/SellerIncidence.php';
        include_once dirname(__FILE__).'/classes/SellerIncidenceMessage.php';
        include_once dirname(__FILE__).'/classes/SellerEmail.php';
        include_once dirname(__FILE__).'/classes/SellerComment.php';
        include_once dirname(__FILE__).'/classes/SellerCommentCriterion.php';
        include_once dirname(__FILE__).'/classes/SellerCategory.php';
        include_once dirname(__FILE__).'/classes/CategoryTree.php';
        include_once dirname(__FILE__).'/classes/Dashboard.php';
        include_once dirname(__FILE__).'/classes/CSVSellerProduct.php';
        include_once dirname(__FILE__).'/classes/CSVSellerProductLog.php';
        include_once dirname(__FILE__).'/classes/SellerTransferCommision.php';
        include_once dirname(__FILE__).'/classes/SellerTransferInvoice.php';
    }
  
    public function install() 
    { 
        //GENERAL SETTINGS
        Configuration::updateValue('JMARKETPLACE_MODERATE_SELLER', 1);
        Configuration::updateValue('JMARKETPLACE_MODERATE_PRODUCT', 1);
        Configuration::updateValue('JMARKETPLACE_CUSTOMER_GROUP_3', 1);
        Configuration::updateValue('JMARKETPLACE_COMMISIONS_ORDER', 0);
        Configuration::updateValue('JMARKETPLACE_COMMISIONS_STATE', 1);
        Configuration::updateValue('JMARKETPLACE_ORDER_STATE_2', 1);
        Configuration::updateValue('JMARKETPLACE_FIXED_COMMISSION', 0);
        Configuration::updateValue('JMARKETPLACE_VARIABLE_COMMISSION', 70);
        Configuration::updateValue('JMARKETPLACE_SHIPPING_COMMISSION', 0);
        Configuration::updateValue('JMARKETPLACE_TAX_COMMISSION', 0);
        Configuration::updateValue('JMARKETPLACE_CANCEL_COMMISSION_6', 1);
        Configuration::updateValue('JMARKETPLACE_CANCEL_COMMISSION_7', 1);
        Configuration::updateValue('JMARKETPLACE_CANCEL_COMMISSION_8', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_CONTACT', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_DASHBOARD', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_SELLER_INVOICE', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_MANAGE_ORDERS', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_MANAGE_CARRIER', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_PROFILE', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_ORDERS', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_EDIT_ACCOUNT', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_EDIT_PRODUCT', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_DELETE_PRODUCT', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_ACTIVE_PRODUCT', 0);
        Configuration::updateValue('JMARKETPLACE_SELLER_FAVORITE', 0);
        Configuration::updateValue('JMARKETPLACE_SELLER_RATING', 0);
        Configuration::updateValue('JMARKETPLACE_NEW_PRODUCTS', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_SELLER_PLIST', 1);
        Configuration::updateValue('JMARKETPLACE_SELLER_IMPORT_PROD', 0);
        
        //SELLER ACCOUNT
        Configuration::updateValue('JMARKETPLACE_SHOW_SHOP_NAME', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_LANGUAGE', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_CIF', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_PHONE', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_FAX', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_ADDRESS', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_COUNTRY', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_STATE', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_CITY', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_POSTAL_CODE', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_DESCRIPTION', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_LOGO', 1);
        
        //SELLER PRODUCT
        Configuration::updateValue('JMARKETPLACE_SHOW_REFERENCE', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_EAN13', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_UPC', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_WIDTH', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_HEIGHT', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_DEPTH', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_WEIGHT', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_SHIP_PRODUCT', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_CONDITION', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_AVAILABLE_ORD', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_SHOW_PRICE', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_ONLINE_ONLY', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_QUANTITY', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_MINIMAL_QTY', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_AVAILABLE_NOW', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_AVAILABLE_LAT', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_AVAILABLE_DATE', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_PRICE', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_WHOLESALEPRICE', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_OFFER_PRICE', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_TAX', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_ON_SALE', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_DESC_SHORT', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_DESC', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_META_KEYWORDS', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_META_TITLE', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_META_DESC', 0);  
        Configuration::updateValue('JMARKETPLACE_SHOW_LINK_REWRITE', 0);  
        Configuration::updateValue('JMARKETPLACE_SHOW_IMAGES', 1);
        Configuration::updateValue('JMARKETPLACE_MAX_IMAGES', 3);
        Configuration::updateValue('JMARKETPLACE_SHOW_SUPPLIERS', 0); 
        Configuration::updateValue('JMARKETPLACE_NEW_SUPPLIERS', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_MANUFACTURERS', 0);
        Configuration::updateValue('JMARKETPLACE_NEW_MANUFACTURERS', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_CATEGORIES', 1);
        Configuration::updateValue('JMARKETPLACE_SHOW_FEATURES', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_ATTRIBUTES', 0);
        Configuration::updateValue('JMARKETPLACE_SHOW_VIRTUAL', 0);
        
        //SELLER PAYMENT
        Configuration::updateValue('JMARKETPLACE_PAYPAL', 1);
        Configuration::updateValue('JMARKETPLACE_BANKWIRE', 1);
        
        //EMAIL
        Configuration::updateValue('JMARKETPLACE_SEND_ADMIN', Configuration::get('PS_SHOP_EMAIL'));
        Configuration::updateValue('JMARKETPLACE_SEND_ADMIN_REGISTER', 1);
		Configuration::updateValue('JMARKETPLACE_SEND_ADMIN_STATUS_UPDATE', 1);
        Configuration::updateValue('JMARKETPLACE_SEND_ADMIN_PRODUCT', 1);
        Configuration::updateValue('JMARKETPLACE_SEND_SELLER_WELCOME', 1);
        Configuration::updateValue('JMARKETPLACE_SEND_SELLER_ACTIVE', 1);
        Configuration::updateValue('JMARKETPLACE_SEND_PRODUCT_ACTIVE', 1);
        Configuration::updateValue('JMARKETPLACE_SEND_PRODUCT_SOLD', 1);
        
        //SELLER COMMENT
        Configuration::updateValue('JMARKETPLACE_MODERATE_COMMENTS', 1);
        Configuration::updateValue('JMARKETPLACE_ALLOW_GUEST_COMMENT', 0);
        Configuration::updateValue('JMARKETPLACE_SEND_COMMENT_SELLER', 1);
        Configuration::updateValue('JMARKETPLACE_SEND_COMMENT_ADMIN', 1);
        
        //THEME FRONT OFFICE
        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            Configuration::updateValue('JMARKETPLACE_THEME', 'default');
            Configuration::updateValue('JMARKETPLACE_TABS', 0);
        }
        else {
            Configuration::updateValue('JMARKETPLACE_THEME', 'default-bootstrap');
            Configuration::updateValue('JMARKETPLACE_TABS', 1);
        }
        
        Configuration::updateValue('JMARKETPLACE_MENU_OPTIONS', 0);
        Configuration::updateValue('JMARKETPLACE_MENU_TOP', 1);
        
        $token = uniqid(rand(), true);
	Configuration::updateValue('JMARKETPLACE_TOKEN', $token);
        
        Configuration::updateValue('JMARKETPLACE_EARNINGS_FROM', date('Y').'-01-01');
        Configuration::updateValue('JMARKETPLACE_EARNINGS_TO', date('Y-m-d'));
        
        if (version_compare(_PS_VERSION_, '1.6', '>')) {
            $menu_jmarketplace = array(
                'en' => 'JA MarketPlace', 
                'es' => 'JA MarketPlace',
                'fr' => 'JA MarketPlace',
                'it' => 'JA MarketPlace',
                'de' => 'JA MarketPlace',
                'br' => 'JA MarketPlace',
            );

            $this->createTab('AdminJmarketplace', $menu_jmarketplace, 'SELL');
        }
        else {
            $menu_jmarketplace = array(
                'en' => 'JA MarketPlace', 
                'es' => 'JA MarketPlace',
                'fr' => 'JA MarketPlace',
                'it' => 'JA MarketPlace',
                'de' => 'JA MarketPlace',
                'br' => 'JA MarketPlace',
            );

            $this->createTab('AdminJmarketplace', $menu_jmarketplace);
        }
            
        $menu_jmarketplace_sellers = array(
            'en' => 'Sellers', 
            'es' => 'Vendedores',
            'fr' => 'Vendeurs',
            'it' => 'Venditori',
            'de' => 'Verkaufer',
            'br' => 'Sellers',
        );

        $this->createTab('AdminSellers', $menu_jmarketplace_sellers, 'AdminJmarketplace');

        $menu_jmarketplace_seller_products = array(
            'en' => 'Seller Products', 
            'es' => 'Productos de los vendedores',
            'fr' => 'Produits des vendeurs',
            'it' => 'Prodotti venditore',
            'de' => 'Produkte Verkaufer',
            'br' => 'Produtos de vendedores',
        );

        $this->createTab('AdminSellerProducts', $menu_jmarketplace_seller_products, 'AdminJmarketplace');

        $menu_jmarketplace_seller_commissions = array(
            'en' => 'Seller Commissions', 
            'es' => 'Comisiones de los vendedores',
            'fr' => 'Commissions des vendeurs',
            'it' => 'Commissioni  dei venditori',
            'de' => 'Provisionen von Anbietern',
            'br' => 'Seller Commissions',
        );

        $this->createTab('AdminSellerCommisions', $menu_jmarketplace_seller_commissions, 'AdminJmarketplace');

        $menu_jmarketplace_seller_commission_history = array(
            'en' => 'Seller Commisions History', 
            'es' => 'Historial de comisiones',
            'fr' => 'Historique de commissions',
            'it' => 'Precedenti delle commissioni',
            'de' => 'Geschichte Kommissionen',
            'br' => 'Seller Commisions History',
        );

        $this->createTab('AdminSellerCommisionsHistory', $menu_jmarketplace_seller_commission_history, 'AdminJmarketplace');

        $menu_jmarketplace_seller_commissiom_history_states = array(
            'en' => 'Seller Payment States', 
            'es' => 'Estado de los pagos',
            'fr' => 'Etats des paiements',
            'it' => 'Stati dei pagamenti',
            'de' => 'Zahlungsstatus',
            'br' => 'Seller Payment States',
        );

        $this->createTab('AdminSellerCommisionsHistoryStates', $menu_jmarketplace_seller_commissiom_history_states, 'AdminJmarketplace');

        $menu_jmarketplace_seller_comments = array(
            'en' => 'Ratings and comments', 
            'es' => 'Valoraciones y comentarios',
            'fr' => 'Notes et commentaires',
            'it' => 'Valutazioni e commenti',
            'de' => 'Bewertungen und Kommentare',
            'br' => 'Ratings and comments',
        );

        $this->createTab('AdminSellerComments', $menu_jmarketplace_seller_comments, 'AdminJmarketplace');

        $menu_jmarketplace_incidences = array(
            'en' => 'Seller Messages', 
            'es' => 'Mensajes',
            'fr' => 'Messages',
            'it' => 'Messaggi',
            'de' => 'Beitrage',
            'br' => 'Seller Messages',
        );

        $this->createTab('AdminSellerIncidences', $menu_jmarketplace_incidences, 'AdminJmarketplace');

        $menu_jmarketplace_seller_emails = array(
            'en' => 'Seller Emails', 
            'es' => 'Emails',
            'fr' => 'Emails',
            'it' => 'Emails',
            'de' => 'E-Mails',
            'br' => 'Emails',
        );
        
        $this->createTab('AdminSellerEmails', $menu_jmarketplace_seller_emails, 'AdminJmarketplace');
        
        $menu_jmarketplace_seller_dashboard = array(
            'en' => 'Earnings', 
            'es' => 'Ganancias',
            'fr' => 'Gains',
            'it' => 'Guadagni',
            'de' => 'Einnahmen',
            'br' => 'Ganhos',
        );

        $this->createTab('AdminSellerDashboard', $menu_jmarketplace_seller_dashboard, 'AdminJmarketplace');
        
        $menu_jmarketplace_seller_invoices = array(
            'en' => 'Transfer Requests', 
            'es' => 'Solicitudes de transferencia',
            'fr' => 'Demandes de transfert',
            'it' => 'Richieste di trasferimento',
            'de' => 'Transfers',
            'br' => 'Transfer Requests',
        );
        
        $this->createTab('AdminSellerInvoices', $menu_jmarketplace_seller_invoices, 'AdminJmarketplace');
        
        $this->addQuickAccess();
        
        if (!parent::install() OR 
                !$this->registerHook('displayHeader') OR 
                !$this->registerHook('backOfficeHeader') OR
                !$this->registerHook('displayCustomerAccount') OR 
                !$this->registerHook('displayProductButtons') OR
                !$this->registerHook('displayProductListReviews') OR
                !$this->registerHook('displayFooter') OR 
                !$this->registerHook('actionValidateOrder') OR
                !$this->registerHook('actionProductDelete') OR
                !$this->registerHook('actionOrderStatusPostUpdate') OR
                !$this->registerHook('adminOrder') OR
                !$this->registerHook('moduleRoutes') OR
                !$this->createImageFolder('sellers') OR
                !$this->createTables() OR
                !$this->addData() OR
                !$this->createHook('displayMarketplaceHeader') OR
                !$this->createHook('displayMarketplaceMenu') OR
                !$this->createHook('displayMarketplaceAfterMenu') OR
                !$this->createHook('displayMarketplaceMenuOptions') OR
                !$this->createHook('displayMarketplaceFooter') OR
                !$this->createHook('displayMarketplaceFormAddProduct') OR
                !$this->createHook('actionMarketplaceAfterAddProduct') OR
                !$this->createHook('actionMarketplaceBeforeAddProduct') OR
                !$this->createHook('displayMarketplaceFormAddSeller') OR
                !$this->createHook('displayMarketplaceHeaderProfile') OR
                !$this->createHook('displayMarketplaceFooterProfile') OR
                !$this->createHook('actionMarketplaceAfterAddSeller') OR
                !$this->createHook('actionMarketplaceBeforeAddSeller') OR
                !$this->createHook('actionMarketplaceAfterUpdateSeller') OR
                !$this->createHook('actionMarketplaceBeforeUpdateSeller') OR
                !$this->createHook('actionMarketplaceAfterUpdateProduct') OR
                !$this->createHook('actionMarketplaceBeforeUpdateProduct') OR
                !$this->createHook('actionMarketplaceSellerProducts') OR
                !$this->createHook('displayMarketplaceTableProfile') OR
				!$this->registerHook('displayProductTab') OR
				!$this->registerHook('displayProductTabContent'))
            return false;
        return true;
    }
  
    public function uninstall() 
    {  
        //GENERAL SETTINGS
        Configuration::deleteByName('JMARKETPLACE_MODERATE_SELLER');
        Configuration::deleteByName('JMARKETPLACE_MODERATE_PRODUCT');
        
        $logged_groups = $this->getGroupsToSeller();
        foreach ($logged_groups as $group) 
            Configuration::deleteByName('JMARKETPLACE_CUSTOMER_GROUP_'.$group['id_group']);
        
        Configuration::deleteByName('JMARKETPLACE_COMMISIONS_ORDER');
        Configuration::deleteByName('JMARKETPLACE_COMMISIONS_STATE');
        //Configuration::deleteByName('JMARKETPLACE_ORDER_STATE');
        Configuration::deleteByName('JMARKETPLACE_VARIABLE_COMMISSION');
        Configuration::deleteByName('JMARKETPLACE_FIXED_COMMISSION');
        Configuration::deleteByName('JMARKETPLACE_SHIPPING_COMMISSION');
        Configuration::deleteByName('JMARKETPLACE_TAX_COMMISSION');
        
        $states = OrderState::getOrderStates($this->context->language->id);
        foreach ($states as $state) {
            Configuration::deleteByName('JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state']);
            Configuration::deleteByName('JMARKETPLACE_ORDER_STATE_'.$state['id_order_state']);
        }
        
        Configuration::deleteByName('JMARKETPLACE_SHOW_CONTACT');
        Configuration::deleteByName('JMARKETPLACE_SHOW_DASHBOARD');
        Configuration::deleteByName('JMARKETPLACE_SHOW_SELLER_INVOICE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_MANAGE_ORDERS');
        Configuration::deleteByName('JMARKETPLACE_SHOW_MANAGE_CARRIER');
        Configuration::deleteByName('JMARKETPLACE_SHOW_PROFILE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_ORDERS');
        Configuration::deleteByName('JMARKETPLACE_SHOW_EDIT_ACCOUNT');
        Configuration::deleteByName('JMARKETPLACE_SHOW_EDIT_PRODUCT');
        Configuration::deleteByName('JMARKETPLACE_SHOW_DELETE_PRODUCT');
        Configuration::deleteByName('JMARKETPLACE_SHOW_ACTIVE_PRODUCT');
        Configuration::deleteByName('JMARKETPLACE_SELLER_FAVORITE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_SELLER_RATING');
        Configuration::deleteByName('JMARKETPLACE_NEW_PRODUCTS');
        Configuration::deleteByName('JMARKETPLACE_SHOW_SELLER_PLIST');
        Configuration::deleteByName('JMARKETPLACE_SELLER_IMPORT_PROD');
        
        //SELLER ACCOUNT
        Configuration::deleteByName('JMARKETPLACE_SHOW_SHOP_NAME');
        Configuration::deleteByName('JMARKETPLACE_SHOW_LANGUAGE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_CIF');
        Configuration::deleteByName('JMARKETPLACE_SHOW_PHONE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_FAX');
        Configuration::deleteByName('JMARKETPLACE_SHOW_ADDRESS');
        Configuration::deleteByName('JMARKETPLACE_SHOW_COUNTRY');
        Configuration::deleteByName('JMARKETPLACE_SHOW_STATE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_CITY');
        Configuration::deleteByName('JMARKETPLACE_SHOW_POSTAL_CODE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_DESCRIPTION');
        Configuration::deleteByName('JMARKETPLACE_SHOW_LOGO');
        
        //SELLER PRODUCT
        Configuration::deleteByName('JMARKETPLACE_SHOW_REFERENCE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_EAN13');
        Configuration::deleteByName('JMARKETPLACE_SHOW_UPC');
        Configuration::deleteByName('JMARKETPLACE_SHOW_WIDTH');
        Configuration::deleteByName('JMARKETPLACE_SHOW_HEIGHT');
        Configuration::deleteByName('JMARKETPLACE_SHOW_DEPTH');
        Configuration::deleteByName('JMARKETPLACE_SHOW_WEIGHT');
        Configuration::deleteByName('JMARKETPLACE_SHOW_SHIP_PRODUCT');
        Configuration::deleteByName('JMARKETPLACE_SHOW_AVAILABLE_ORD');
        Configuration::deleteByName('JMARKETPLACE_SHOW_SHOW_PRICE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_ONLINE_ONLY');
        Configuration::deleteByName('JMARKETPLACE_SHOW_CONDITION');
        Configuration::deleteByName('JMARKETPLACE_SHOW_QUANTITY');
        Configuration::deleteByName('JMARKETPLACE_SHOW_MINIMAL_QTY');
        Configuration::deleteByName('JMARKETPLACE_SHOW_AVAILABLE_NOW');
        Configuration::deleteByName('JMARKETPLACE_SHOW_AVAILABLE_LAT');
        Configuration::deleteByName('JMARKETPLACE_SHOW_AVAILABLE_DATE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_PRICE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_WHOLESALEPRICE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_OFFER_PRICE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_TAX');
        Configuration::deleteByName('JMARKETPLACE_SHOW_ON_SALE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_DESC_SHORT');
        Configuration::deleteByName('JMARKETPLACE_SHOW_DESC');
        Configuration::deleteByName('JMARKETPLACE_SHOW_META_KEYWORDS');
        Configuration::deleteByName('JMARKETPLACE_SHOW_META_TITLE');
        Configuration::deleteByName('JMARKETPLACE_SHOW_LINK_REWRITE'); 
        Configuration::deleteByName('JMARKETPLACE_SHOW_META_DESC');
        Configuration::deleteByName('JMARKETPLACE_SHOW_IMAGES');
        Configuration::deleteByName('JMARKETPLACE_MAX_IMAGES');
        Configuration::deleteByName('JMARKETPLACE_SHOW_SUPPLIERS'); 
        Configuration::deleteByName('JMARKETPLACE_NEW_SUPPLIERS'); 
        Configuration::deleteByName('JMARKETPLACE_SHOW_MANUFACTURERS');
        Configuration::deleteByName('JMARKETPLACE_NEW_MANUFACTURERS');
        Configuration::deleteByName('JMARKETPLACE_SHOW_CATEGORIES'); 
        Configuration::deleteByName('JMARKETPLACE_SHOW_FEATURES');
        Configuration::deleteByName('JMARKETPLACE_SHOW_ATTRIBUTES');
        Configuration::deleteByName('JMARKETPLACE_SHOW_VIRTUAL'); 
        
        //SELLER PAYMENT
        Configuration::deleteByName('JMARKETPLACE_PAYPAL');
        Configuration::deleteByName('JMARKETPLACE_BANKWIRE');
        
        //EMAIL
        Configuration::deleteByName('JMARKETPLACE_SEND_ADMIN');
        Configuration::deleteByName('JMARKETPLACE_SEND_ADMIN_REGISTER');
		Configuration::deleteByName('JMARKETPLACE_SEND_ADMIN_STATUS_UPDATE');
        Configuration::deleteByName('JMARKETPLACE_SEND_ADMIN_PRODUCT');
        Configuration::deleteByName('JMARKETPLACE_SEND_SELLER_WELCOME');
        Configuration::deleteByName('JMARKETPLACE_SEND_SELLER_ACTIVE');
        Configuration::deleteByName('JMARKETPLACE_SEND_PRODUCT_ACTIVE');
        Configuration::deleteByName('JMARKETPLACE_SEND_PRODUCT_SOLD');
        
        //THEME FRONT OFFICE
        Configuration::deleteByName('JMARKETPLACE_THEME');
        Configuration::deleteByName('JMARKETPLACE_TABS');
        Configuration::deleteByName('JMARKETPLACE_MENU_OPTIONS');
        Configuration::deleteByName('JMARKETPLACE_MENU_TOP');
        
        Configuration::deleteByName('JMARKETPLACE_TOKEN');
        
        $this->deleteQuickAccess();
        
        Configuration::deleteByName('JMARKETPLACE_QUICK_ACCESS');
        Configuration::deleteByName('JMARKETPLACE_EARNINGS_FROM');
        Configuration::deleteByName('JMARKETPLACE_EARNINGS_TO');
        
        $this->deleteTab('AdminSellers');
        $this->deleteTab('AdminSellerProducts');
        $this->deleteTab('AdminSellerCommisions');
        $this->deleteTab('AdminSellerCommisionsHistory');
        $this->deleteTab('AdminSellerCommisionsHistoryStates');
        $this->deleteTab('AdminSellerIncidences');
        $this->deleteTab('AdminSellerEmails');
        $this->deleteTab('AdminSellerDashboard');
        $this->deleteTab('AdminJmarketplace');

        if (!parent::uninstall() || !$this->deleteTables() || !Tools::deleteDirectory(_PS_IMG_DIR_.'sellers'))
            return false;
        return true;
    }
    
    public function createTables() 
    {
        if (!file_exists(dirname(__FILE__) . '/' . self::INSTALL_SQL_FILE))
            return false;
        else if (!$sql = Tools::file_get_contents(dirname(__FILE__) . '/' . self::INSTALL_SQL_FILE))
            return false;
        
        $sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql); 
        $sql = preg_split("/;\s*[\r\n]+/", $sql);
        
        foreach ($sql AS $query)
            if ($query)
                if (!Db::getInstance()->execute(trim($query)))
                    return false;              
        return true;
    }
    
    public function addData() 
    {
        $this->createStates();
        $this->addSellerCategories();
        $this->createEmails();
        
        return true;
    }
    
    public function createStates() {
        //states
        $state = new SellerCommisionHistoryState();
        
        $state->active = 1;
        $state->reference = 'pending';
        foreach (Language::getLanguages() as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') 
                $state->name[$lang['id_lang']] = 'Pendiente';
            else
                $state->name[$lang['id_lang']] = 'Pending';
        }    
        $state->add();
        
        $state->active = 1;
        $state->reference = 'paid';
        foreach (Language::getLanguages() as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') 
                $state->name[$lang['id_lang']] = 'Pagado';
            else
                $state->name[$lang['id_lang']] = 'Paid';
        }
        $state->add();
        
        $state->active = 1;
        $state->reference = 'cancel';
        foreach (Language::getLanguages() as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') 
                $state->name[$lang['id_lang']] = 'Cancelado';
            else
                $state->name[$lang['id_lang']] = 'Cancel';
        }
        $state->add();
    }
    
    public function createEmails() 
    {
        if (Configuration::get('PS_SSL_ENABLED') == 1)
            $url_shop = Tools::getShopDomainSsl(true).__PS_BASE_URI__;
        else
            $url_shop = Tools::getShopDomain(true).__PS_BASE_URI__;
        
        //welcome-seller
        $seller_email = new SellerEmail();
        $seller_email->reference = 'welcome-seller';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Bienvenido a {shop_name}';
                $seller_email->description[$lang['id_lang']] = 'Este email es enviado al vendedor cuando su cuenta de vendedor ha sido creada correctamente.';
                $seller_email->content[$lang['id_lang']] = '<p>Bienvenido <strong>{seller_name}</strong>!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Gracias por crear una cuenta de vendedor en {shop_name}!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Su cuenta de vendedor ha sido creada correctamente en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Bienvenue à {shop_name}';
                $seller_email->description[$lang['id_lang']] = 'Ce courriel est envoyé au vendeur lorsque votre compte marchand a été créé avec succès.';
                $seller_email->content[$lang['id_lang']] = '<p>Bienvenue <strong>{seller_name}</strong>!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Merci de créer un compte vendeur dans {shop_name}!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Votre compte vendeur a été créé avec succès dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Benvenuto a {shop_name}';
                $seller_email->description[$lang['id_lang']] = 'Questa email viene inviata al venditore quando il tuo conto commerciante è stato creato con successo.';
                $seller_email->content[$lang['id_lang']] = '<p>Benvenuto <strong>{seller_name}</strong>!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Grazie per creare un account venditore in {shop_name}!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Il tuo account venditore è stato creato con successo in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'Welcome to {shop_name}';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when it has been correctly created your seller account.';
                $seller_email->content[$lang['id_lang']] = '<p>Welcome <strong>{seller_name}</strong>!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Thank you by create a seller account in {shop_name}!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Your seller account has been created successfully in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //new-seller
        $seller_email = new SellerEmail();
        $seller_email->reference = 'new-seller';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Nuevo vendedor registrado';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al administrador cuando un cliente se registra como vendedor.';
                $seller_email->content[$lang['id_lang']] = '<p>Nuevo cliente registrado como vendedor.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Detalles del vendedor:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>NOMBRE: {seller_name}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>TIENDA: {seller_shop}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Puede acceder a su tienda para activar la nueva cuenta de vendedor en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Nouveau vendeur enregistré';
                $seller_email->description[$lang['id_lang']] = 'Ce courrier est envoyé à administrateur lorsqu un client enregistre en tant que vendeur.';
                $seller_email->content[$lang['id_lang']] = '<p>Nouveau client enregistré en tant que vendeur.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Informations sur le vendeur:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Nom: {seller_name}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Boutique: {seller_shop}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accédez à votre boutique pour activer le nouveau compte vendeur <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Nuovo venditore registrato';
                $seller_email->description[$lang['id_lang']] = 'Questa mail viene inviata all amministratore quando un cliente si registra come venditore.';
                $seller_email->content[$lang['id_lang']] = '<p>Nuovo cliente registrato come venditore.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Dettagli del venditore:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Nome: {seller_name}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Shop: {seller_shop}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accedi al tuo negozio per attivare il nuovo account venditore <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'New seller registered';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the administrator when a customer registers as seller.';
                $seller_email->content[$lang['id_lang']] = '<p>New customer registered as a seller.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Seller details:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Name: {seller_name}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Shop: {seller_shop}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can access your shop to activate the new seller account in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //edit-seller
        $seller_email = new SellerEmail();
        $seller_email->reference = 'edit-seller';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" ha editado su cuenta de vendedor';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al administrador Cuando un vendedor ha cambiado la información de su cuenta de vendedor.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> ha modificado su perfil de vendedor.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Puede acceder a su tienda para validar los cambios en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" a changé le profil du vendeur';
                $seller_email->description[$lang['id_lang']] = 'Ce courriel est envoyé à administrateur quand un vendeur a changé les informations de votre compte vendeur.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> a changé le profil du vendeur.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accédez à votre boutique pour accepté les changements dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" ha cambiato il profilo del venditore';
                $seller_email->description[$lang['id_lang']] = 'Questa email viene inviata all amministratore quando un venditore ha cambiato il informazioni del tuo account venditore.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> ha cambiato il profilo del venditore.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accedi al tuo negozio per validare le modifiche <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" has changed your seller account';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the administrator when a seller has changed the information of your seller account.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> has changed your seller account.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can access your shop to validate changes in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //seller-activated
        $seller_email = new SellerEmail();
        $seller_email->reference = 'seller-activated';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Su cuenta de vendedor ha sido activada';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando su cuenta de vendedor ha sido aceptada.';
                $seller_email->content[$lang['id_lang']] = '<p>Su cuenta de vendedor ha sido activada en <strong>{shop_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Ahora puede comenzar a añadir sus productos en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Votre compte vendeur a été activé';
                $seller_email->description[$lang['id_lang']] = 'Ce courriel est envoyé au vendeur lorsque votre compte vendeur a été activé.';
                $seller_email->content[$lang['id_lang']] = '<p>Votre compte vendeur a été activé dans <strong>{shop_name}</strong></p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Maintenant, vous pouvez commencer à ajouter des produits dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Il tuo account venditore è stato attivato';
                $seller_email->description[$lang['id_lang']] = 'Questa email viene inviata al venditore quando è stato attivato il tuo account venditore.';
                $seller_email->content[$lang['id_lang']] = '<p>Il tuo account venditore è stato attivato in <strong>{shop_name}</strong></p>';
                $seller_email->content[$lang['id_lang']] .= '<p>È ora possibile iniziare ad aggiungere prodotti in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'Your seller account has been activated';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when your seller account has been activated.';
                $seller_email->content[$lang['id_lang']] = '<p>Your seller account has been activated in <strong>{shop_name}</strong></p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can now begin to add products in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //seller-desactivated
        $seller_email = new SellerEmail();
        $seller_email->reference = 'seller-desactivated';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Su cuenta de vendedor ha sido rechazada';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando su cuenta de vendedor ha sido aceptada.';
                $seller_email->content[$lang['id_lang']] = '<p>Su cuenta de vendedor ha sido desactivada en <strong>{shop_name}</strong>.</p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Votre compte vendeur a été désactivé';
                $seller_email->description[$lang['id_lang']] = 'Ce courriel est envoyé au vendeur lorsque vous compte vendeur a été refusé.';
                $seller_email->content[$lang['id_lang']] = '<p>Votre compte vendeur a été désactivé dans <strong>{shop_name}</strong></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Il tuo account venditore non è attivato';
                $seller_email->description[$lang['id_lang']] = 'Questa email viene inviata al venditore quando il tuo account venditore è stata rifiutata.';
                $seller_email->content[$lang['id_lang']] = '<p>Il tuo account venditore non è attivato in <strong>{shop_name}</strong></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'Your seller account has been declined';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when your seller account has been desactivated.';
                $seller_email->content[$lang['id_lang']] = '<p>Your seller account has been activated in <strong>{shop_name}</strong></p>';
            }
        }
        
        $seller_email->add();
        
        //new-product
        $seller_email = new SellerEmail();
        $seller_email->reference = 'new-product';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" ha ha añadido un nuevo producto';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al administrador cuando un vendedor añade un nuevo producto.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> ha añadido un nuevo producto <strong>{product_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Puede acceder a su tienda para validarlo en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" a ajouté un nouveau produit';
                $seller_email->description[$lang['id_lang']] = 'Ce courriel est envoyé à administrateur quand un vendeur ajoute un nouveau produit.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> a ajouté un nouveau produit <strong>{product_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Vous pouvez accéder à votre boutique pour gérer <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" ha aggiunto un nuovo prodotto';
                $seller_email->description[$lang['id_lang']] = 'Questa email viene inviata all amministratore Quando un venditore aggiunge un nuovo prodotto.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> ha aggiunto un nuovo prodotto <strong>{product_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Puoi accedere al tuo negozio per gestirlo in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" has added a new product';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the administrator when a seller adds a new product.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> has modified your product <strong>{product_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can access your shop to manage it <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //product-activated
        $seller_email = new SellerEmail();
        $seller_email->reference = 'product-activated';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Su producto "{product_name}" ha sido aceptado';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando su producto ha sido aceptado.';
                $seller_email->content[$lang['id_lang']] = '<p>Su producto <strong>{product_name}</strong> ha sido aceptado.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Ahora está disponible en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Son produit "{product_name}" a ete accepte';
                $seller_email->description[$lang['id_lang']] = 'Ce courriel est envoyé au vendeur lorsque votre produit a été accepté.';
                $seller_email->content[$lang['id_lang']] = '<p>Votre produit <strong>{product_name}</strong> a été accepté dans <strong>{shop_name}</strong></p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Maintenant disponible dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Il suo prodotto "{product_name}" e stato accettato';
                $seller_email->description[$lang['id_lang']] = 'Questa email viene inviata al venditore quando il prodotto è stato accettato.';
                $seller_email->content[$lang['id_lang']] = '<p>Il prodotto <strong>{product_name}</strong> è stato accettato in <strong>{shop_name}</strong></p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Ora disponibile in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'Your product "{product_name}" has been accepted';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when your product has been accepted.';
                $seller_email->content[$lang['id_lang']] = '<p>Your product <strong>{product_name}</strong> has been accepted in <strong>{shop_name}</strong></p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Now available in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //product-desactivated
        $seller_email = new SellerEmail();
        $seller_email->reference = 'product-desactivated';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Su producto "{product_name}" ha sido rechazado';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando su producto ha sido rechazado.';
                $seller_email->content[$lang['id_lang']] = '<p>Su producto <strong>{product_name}</strong> ha sido rechazado.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Contacta ahora en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Son produit "{product_name}" a ete accepte';
                $seller_email->description[$lang['id_lang']] = 'Ce courriel est envoyé au vendeur lorsque votre produit a été refusée.';
                $seller_email->content[$lang['id_lang']] = '<p>Votre produit <strong>{product_name}</strong> na pas été accepté dans <strong>{shop_name}</strong></p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Maintenant disponible dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Il suo prodotto "{product_name}" e stato accettato';
                $seller_email->description[$lang['id_lang']] = 'Questa email viene inviata al venditore quando il prodotto è stato rifiutato.';
                $seller_email->content[$lang['id_lang']] = '<p>Il prodotto <strong>{product_name}</strong> non è stato accettato in <strong>{shop_name}</strong></p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Ora disponibile in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'Your product "{product_name}"  has been declined';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when your product has been declined.';
                $seller_email->content[$lang['id_lang']] = '<p>Your product <strong>{product_name}</strong>  has been declined in <strong>{shop_name}</strong></p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Contact now in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //edit-product
        $seller_email = new SellerEmail();
        $seller_email->reference = 'edit-product';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" ha editado un producto';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al administrador cuando un vendedor ha editado un producto.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> ha editado su producto <strong>{product_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Puede acceder a su tienda para gestionarlo en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" a édité un produit';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando su producto ha sido rechazado.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> a modifié votre produit <strong>{product_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Vous pouvez accéder à votre boutique pour gérer <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" ha modificato il suo prodotto';
                $seller_email->description[$lang['id_lang']] = 'Questa email viene inviata all amministratore quando un venditore ha modificato il suo prodotto.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> ha modificato il prodotto "{product_name}".</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Puoi accedere al tuo negozio per gestirlo in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = '"{seller_name}" has edited a product';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the administrator when a seller has edited a product.';
                $seller_email->content[$lang['id_lang']] = '<p><strong>{seller_name}</strong> has modified your product <strong>{product_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can access your shop to manage it <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();

        //new-order
        $seller_email = new SellerEmail();
        $seller_email->reference = 'new-order';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Nuevo pedido. "{product_name}" - {order_reference}';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando su producto ha sido comprado por un cliente.';
                $seller_email->content[$lang['id_lang']] = '<p>Hola <strong>{seller_name}</strong>!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>¡Enhorabuena! <strong>{product_name}</strong> fue comprado por un cliente en la tienda <strong>{shop_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Visita tu cuenta de vendedor ahora en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Nouvelle commande. "{product_name}" - {order_reference}';
                $seller_email->description[$lang['id_lang']] = 'Ce courriel est envoyé au vendeur Lorsque votre produit a été acheté par un client.';
                $seller_email->content[$lang['id_lang']] = '<p>Bonjour <strong>{seller_name}</strong>!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Félicitations à vous! <strong>{product_name}</strong> a été acheté par un client sur le marché <strong>{shop_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Visitez votre compte marchand maintenant dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Nuevo ordine. "{product_name}" - {order_reference}';
                $seller_email->description[$lang['id_lang']] = 'Questa email viene inviata al venditore quando il prodotto è stato acquistato da un cliente.';
                $seller_email->content[$lang['id_lang']] = '<p>Ciao <strong>{seller_name}</strong>!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Complimenti! <strong>{product_name}</strong> è stato acquistato da un cliente nel mercato <strong>{shop_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Visita il tuo account commerciante <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'New order. "{product_name}" - {order_reference}';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when your product has been purchased by a customer.';
                $seller_email->content[$lang['id_lang']] = '<p>Hello <strong>{seller_name}</strong>!</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>¡Congratulations! <strong>{product_name}</strong> was purchased by a customer in <strong>{shop_name}</strong>.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Visit your seller account now <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //new-incidence
        $seller_email = new SellerEmail();
        $seller_email->reference = 'new-incidence';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Nueva incidencia recibida sobre el pedido "{order_reference}"';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando un cliente tiene problemas con algún pedido.';
                $seller_email->content[$lang['id_lang']] = '<p>Ha habido una nueva incidencia sobre el pedido <strong>{order_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Acceda a su cuenta de vendedor para dar una respuesta en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Nouvelle incidence dans ordre "{order_reference}"';
                $seller_email->description[$lang['id_lang']] = 'Ce courrier est envoyé au vendeur quand un client a un problème avec un ordre.';
                $seller_email->content[$lang['id_lang']] = '<p>Il y a eu un nouvel incident sur demande <strong>{order_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accédez à votre compte marchand pour donner une réponse dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Nuovo incidenza ricevuto su richiesta "{order_reference}"';
                $seller_email->description[$lang['id_lang']] = 'Questa mail viene inviata al venditore quando un cliente ha un problema con un ordine.';
                $seller_email->content[$lang['id_lang']] = '<p>Cè stato un nuovo incidente su richiesta <strong>{order_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accedi al tuo account venditore per rispondere <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'New incidence received about order "{order_reference}"';
                $seller_email->description[$lang['id_lang']] = 'This mail is sent to the seller when a customer has a problem with an order.';
                $seller_email->content[$lang['id_lang']] = '<p>There has been a new incident on order <strong>{order_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Access your seller account to respond in en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //new-message
        $seller_email = new SellerEmail();
        $seller_email->reference = 'new-message';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Nuevo mensaje recibido sobre "{product_name}"';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando un cliente quiere saber más información sobre un producto.';
                $seller_email->content[$lang['id_lang']] = '<p>Ha recibido un nuevo mensaje con referencia <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Acceda a su cuenta de vendedor para dar una respuesta en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Nouveau message reçu sur "{product_name}"';
                $seller_email->description[$lang['id_lang']] = 'Ce courrier est envoyé au vendeur quand un client veut en savoir plus sur un produit.';
                $seller_email->content[$lang['id_lang']] = '<p>Il a reçu un nouveau message en référence <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accédez à votre compte marchand pour donner une réponse dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Nuovo messaggio ricevuto circa "{product_name}"';
                $seller_email->description[$lang['id_lang']] = 'Questa mail viene inviata al venditore quando un cliente vuole sapere di più su un prodotto.';
                $seller_email->content[$lang['id_lang']] = '<p>Ha ricevuto un nuovo messaggio con riferimento <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accedi al tuo account venditore per rispondere <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'New message received about "{product_name}"';
                $seller_email->description[$lang['id_lang']] = 'This mail is sent to the seller when a customer wants to know more about a product.';
                $seller_email->content[$lang['id_lang']] = '<p>You have received a new message reference <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Access your seller account to respond in en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //new-response-seller
        $seller_email = new SellerEmail();
        $seller_email->reference = 'new-response-seller';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Nueva respuesta a su mensaje con referencia "{incidence_reference}"';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al cliente cuando el vendedor responde a un mensaje previo.';
                $seller_email->content[$lang['id_lang']] = '<p>Hay una nueva respuesta sobre su mensaje con referencia <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Acceda a su cuenta para revisar el historial de mensajes <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Nouvelle réponse à votre message en référence "{incidence_reference}"';
                $seller_email->description[$lang['id_lang']] = 'Ce courrier est envoyé au client lorsque le vendeur répond à un message précédent.';
                $seller_email->content[$lang['id_lang']] = '<p>Il y a une nouvelle réponse à votre message en référence <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accédez à votre compte marchand pour donner une réponse dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Nuova risposta al vostro messaggio con riferimento "{incidence_reference}"';
                $seller_email->description[$lang['id_lang']] = 'Questa mail viene inviata al cliente quando il venditore risponde ad un messaggio precedente.';
                $seller_email->content[$lang['id_lang']] = '<p>Cè una nuova risposta al messaggio con riferimento <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accedi al tuo account venditore per rispondere <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'New response to your message with reference "{incidence_reference}"';
                $seller_email->description[$lang['id_lang']] = 'This mail is sent to the customer when the seller responds to a previous message.';
                $seller_email->content[$lang['id_lang']] = '<p>There is a new answer to your message reference <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Access your seller account to respond in en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //new-response-customer
        $seller_email = new SellerEmail();
        $seller_email->reference = 'new-response-customer';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Nueva respuesta del cliente sobre el mensaje con referencia "{incidence_reference}"';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando el cliente responde a un mensaje previo.';
                $seller_email->content[$lang['id_lang']] = '<p>Hay una nueva respuesta del cliente en el mensaje con referencia <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Acceda a su cuenta para dar una respuesta en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Réponse Nouveau client sur le message "{incidence_reference}"';
                $seller_email->description[$lang['id_lang']] = 'Ce courrier est envoyé au vendeur lorsque le client répond à un message précédent.';
                $seller_email->content[$lang['id_lang']] = '<p>Il y a une nouvelle réponse du client dans le message en référence <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accédez à votre compte marchand pour donner une réponse dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Nuova risposta dei clienti sul messaggio con riferimento "{incidence_reference}"';
                $seller_email->description[$lang['id_lang']] = 'Questa mail viene inviata al venditore quando il cliente risponde ad un messaggio precedente.';
                $seller_email->content[$lang['id_lang']] = '<p>Cè una nuova risposta de cliente nel messaggio con riferimento <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accedi al tuo account venditore per rispondere <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'New customer response on the message with reference "{incidence_reference}"';
                $seller_email->description[$lang['id_lang']] = 'This mail is sent to the seller when the customer responds to a previous message.';
                $seller_email->content[$lang['id_lang']] = '<p>There is a new customer response in the message with reference <strong>{incidence_reference}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{description}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Access your seller account to respond in en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //new-comment-admin
        $seller_email = new SellerEmail();
        $seller_email->reference = 'new-comment-admin';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Nuevo comentario recibido sobre "{seller_name}"';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al administrador cuando un cliente valora el grado de satisfacción de un vendedor.';
                $seller_email->content[$lang['id_lang']] = '<p>Nuevo comentario recibido sobre el vendedor <strong>{seller_name}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Grado de satisfacción: {grade}/5</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{comment}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Acceda a su tienda para validarlo <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Nouveau commentaire reçu sur "{seller_name}"';
                $seller_email->description[$lang['id_lang']] = 'Ce courrier est envoyé à administrateur lorsquun client évalue le degré de satisfaction dun vendeur.';
                $seller_email->content[$lang['id_lang']] = '<p>Nouveau commentaire reçu sur vendeur <strong>{seller_name}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Satisfaction: {grade}/5</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{comment}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accédez à votre magasin pour valider <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Nuovo commento ricevuto il "{seller_name}"';
                $seller_email->description[$lang['id_lang']] = 'Questa mail viene inviata all amministratore quando un cliente valuta il grado di soddisfazione di un venditore.';
                $seller_email->content[$lang['id_lang']] = '<p>Nuovo commento ricevuto il venditore <strong>{seller_name}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Soddisfazione: {grade}/5</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{comment}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Accedi al tuo negozio per convalidare <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'New comment received about "{seller_name}"';
                $seller_email->description[$lang['id_lang']] = 'This mail is sent to the administrator when a customer assesses the degree of satisfaction of a seller.';
                $seller_email->content[$lang['id_lang']] = '<p>New comment received about seller <strong>{seller_name}</strong>:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Satisfaction: {grade}/5</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{comment}</p>';
                $seller_email->content[$lang['id_lang']] .= '<pAccess your shop to validate <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //new-comment-seller
        $seller_email = new SellerEmail();
        $seller_email->reference = 'new-comment-seller';
        
        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'ca' || $lang['iso_code'] == 'gl' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Nuevo comentario recibido';
                $seller_email->description[$lang['id_lang']] = 'Este correo se envía al vendedor cuando un cliente valora el grado de satisfacción de un vendedor o cuando el admininistrador valida un comentario.';
                $seller_email->content[$lang['id_lang']] = '<p>Nuevo comentario recibido:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Grado de satisfacción: {grade}/5</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{comment}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Puede verlo en <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Nouveau commentaire reçu';
                $seller_email->description[$lang['id_lang']] = 'Ce courrier est envoyé au vendeur quand un client évalue le degré de satisfaction dun fournisseur ou lorsque le admininistrador valide un commentaire.';
                $seller_email->content[$lang['id_lang']] = '<p>Nouveau commentaire reçu:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Satisfaction: {grade}/5</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{comment}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Vous pouvez le voir dans <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Nuovo commento ricevuto';
                $seller_email->description[$lang['id_lang']] = 'Questa mail viene inviata al venditore quando un cliente valuta il grado di soddisfazione di un venditore o quando il admininistrador convalida un commento.';
                $seller_email->content[$lang['id_lang']] = '<p>Nuovo commento ricevuto:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Soddisfazione: {grade}/5</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{comment}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Lo si può vedere in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'New comment received';
                $seller_email->description[$lang['id_lang']] = 'This mail is sent to the seller when a customer assesses the degree of satisfaction of a vendor or when the admininistrador validates a comment.';
                $seller_email->content[$lang['id_lang']] = '<p>New comment received:</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>Satisfaction: {grade}/5</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>{comment}</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can see it in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }
        
        $seller_email->add();
        
        //seller-transfer-accepted
        $seller_email = new SellerEmail();
        $seller_email->reference = 'seller-transfer-accepted';

        foreach (Language::getLanguages(false) as $lang) {
            if ($lang['iso_code'] == 'es' || $lang['iso_code'] == 'mx' || $lang['iso_code'] == 'co' || $lang['iso_code'] == 'ar') {
                $seller_email->subject[$lang['id_lang']] = 'Su solicitud de pago ha sido aceptada';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when a administrator accept a commissions request transfer.';
                $seller_email->content[$lang['id_lang']] = '<p>Your request for payment has been validated by our accounting team A amount of <strong>{amount}</strong> has been transfered in your {payment} account.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can access your shop to see more information in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'fr') {
                $seller_email->subject[$lang['id_lang']] = 'Your request for payment has been accepted';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when a administrator accept a commissions request transfer.';
                $seller_email->content[$lang['id_lang']] = '<p>Your request for payment has been validated by our accounting team A amount of <strong>{amount}</strong> has been transfered in your {payment} account.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can access your shop to see more information in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else if($lang['iso_code'] == 'it') {
                $seller_email->subject[$lang['id_lang']] = 'Your request for payment has been accepted';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when a administrator accept a commissions request transfer.';
                $seller_email->content[$lang['id_lang']] = '<p>Your request for payment has been validated by our accounting team A amount of <strong>{amount}</strong> has been transfered in your {payment} account.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can access your shop to see more information in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
            else {
                $seller_email->subject[$lang['id_lang']] = 'Your request for payment has been accepted';
                $seller_email->description[$lang['id_lang']] = 'This email is sent to the seller when a administrator accept a commissions request transfer.';
                $seller_email->content[$lang['id_lang']] = '<p>Your request for payment has been validated by our accounting team A amount of <strong>{amount}</strong> has been transfered in your {payment} account.</p>';
                $seller_email->content[$lang['id_lang']] .= '<p>You can access your shop to see more information in <a href="'.$url_shop.'">{shop_name}</a></p>';
            }
        }

        $seller_email->add();
    }
    
    public function addSellerCategories() 
    {
        $data_seller_category = array();
        $categories = Category::getSimpleCategories(Context::getContext()->language->id);
        foreach ($categories as $category) {
            $data_seller_category[] = array(
                'id_category' => (int)$category['id_category'],
                'id_shop' => (int)Context::getContext()->shop->id,
            );
        }
        Db::getInstance()->insert('seller_category', $data_seller_category);
    }
    
    public function deleteTables() 
    {
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_product`');   
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_incidence`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_incidence_message`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_payment`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_commision`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_commision_history`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_commision_history_state`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_commision_history_state_lang`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_favorite`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_category`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_carrier`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_email`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_email_lang`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_comment`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_comment_criterion`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_comment_criterion_lang`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'seller_comment_grade`');
        return true;
    }
    
    public function createTab($class_name, $tab_name, $tab_parent_name = false) 
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = $class_name;
        $tab->name = array();
        
        foreach (Language::getLanguages(true) as $lang) {
            switch($lang['iso_code']) {
                case 'es':
                    $tab->name[$lang['id_lang']] = $tab_name['es'];
                    break;
                case 'co':
                    $tab->name[$lang['id_lang']] = $tab_name['es'];
                    break;
                case 'ar':
                    $tab->name[$lang['id_lang']] = $tab_name['es'];
                    break;
                case 'mx':
                    $tab->name[$lang['id_lang']] = $tab_name['es'];
                    break;
                case 'fr':
                    $tab->name[$lang['id_lang']] = $tab_name['fr'];
                    break;
                case 'it':
                    $tab->name[$lang['id_lang']] = $tab_name['it'];
                    break;
                case 'br':
                    $tab->name[$lang['id_lang']] = $tab_name['br'];
                    break;
                default:
                    $tab->name[$lang['id_lang']] = $tab_name['en'];
                    break;
            }
        }    
        
        if($tab_parent_name) 
            $tab->id_parent = (int)Tab::getIdFromClassName($tab_parent_name);
        else 
            $tab->id_parent = 0;

        $tab->module = $this->name;
        return $tab->add();
    }
    
    public function updateTab($class_name, $tab_name) 
    {
        $tab = Tab::getInstanceFromClassName($class_name);
        
        foreach (Language::getLanguages(true) as $lang) {
            switch($lang['iso_code']) {
                case 'es':
                    $tab->name[$lang['id_lang']] = $tab_name['es'];
                    break;
                case 'fr':
                    $tab->name[$lang['id_lang']] = $tab_name['fr'];
                    break;
                case 'it':
                    $tab->name[$lang['id_lang']] = $tab_name['it'];
                    break;
                case 'br':
                    $tab->name[$lang['id_lang']] = $tab_name['br'];
                    break;
                default:
                    $tab->name[$lang['id_lang']] = $tab_name['en'];
                    break;
            }
        }    

        return $tab->update();
    }
    
    public function deleteTab($class_name) 
    {
        $id_tab = (int)Tab::getIdFromClassName($class_name);
        if ($id_tab) {
            $tab = new Tab($id_tab);
            return $tab->delete();
        }
        else
            return false;
    }
    
    private function createImageFolder($imageFolderName) 
    {
        if(!is_dir(_PS_IMG_DIR_.$imageFolderName))
            if(!mkdir(_PS_IMG_DIR_.$imageFolderName, 0755))
                return false;

        return true;
    }   
    
    public function removeThemeColumnByPage($page) {
        $meta = Meta::getMetaByPage($page, Context::getContext()->language->id);
        return Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'theme_meta` SET `left_column` = 0 WHERE id_meta = '.(int)$meta['id_meta']);
    }
    
    public function createHook($name) 
    {
        $hook = '';
        
        if (version_compare(_PS_VERSION_, '1.6', '<'))
            $hook = Hook::get($name);
        else
            $hook = Hook::getIdByName($name);
        
        if (!$hook) {
            $hook = new Hook();
            $hook->name = $name;
            $hook->save();
        }

        return $this->registerHook($name);
    }
    
    public function addQuickAccess() 
    {
        $quick_access = new QuickAccess();
        $quick_access->link = $this->context->link->getAdminLink('AdminModules').'&configure=jmarketplace';

        $languages = Language::getLanguages(false);
        foreach ($languages as $lang) {
            if ($lang['iso_code'] != 'es')
                $quick_access->name[$lang['id_lang']] = 'ConfiguraciÃ³n JA Marketplace';
            else
                $quick_access->name[$lang['id_lang']] = 'JA Marketplace Setting';
            
        }

        $quick_access->new_window = '0';
        
        if($quick_access->save())
            Configuration::updateValue('JMARKETPLACE_QUICK_ACCESS', $quick_access->id);
    }
    
    public function deleteQuickAccess() 
    {
        $quick_access = new QuickAccess(Configuration::get('JMARKETPLACE_QUICK_ACCESS'));
        $quick_access->delete();
    }
    
    public function addMetas() 
    {
        $this->controllers = array(
            'addproduct', 
            'addseller', 
            'contactseller', 
            'editproduct', 
            'editseller',
            'favoriteseller',
            'sellermessages',
            'sellerorders',
            'sellerpayment',
            'sellerproducts',
            'sellers',
        );
    }
    
    public function postProcess() 
    { 
        $errors = array();
        
        if (Tools::isSubmit('submitGeneralSettings')) {    
            $fixed_commission = Tools::getValue('JMARKETPLACE_FIXED_COMMISSION');
            $variable_commission = Tools::getValue('JMARKETPLACE_VARIABLE_COMMISSION');
            
            if (!Validate::isFloat($fixed_commission)) {
                $errors[] = $this->l('Invalid fixed commission value.');
            }
            
            if ($variable_commission < 0 OR $variable_commission > 100 OR !Validate::isFloat($variable_commission)) {
                $errors[] = $this->l('Invalid variable commission value.');
            }

            if (count($errors) == 0) {
                Configuration::updateValue('JMARKETPLACE_MODERATE_SELLER', Tools::getValue('JMARKETPLACE_MODERATE_SELLER'));
                Configuration::updateValue('JMARKETPLACE_MODERATE_PRODUCT', Tools::getValue('JMARKETPLACE_MODERATE_PRODUCT'));

                $logged_groups = $this->getGroupsToSeller();

                $selected_group = false;
                foreach ($logged_groups as $group) {
                    if (Tools::getValue('JMARKETPLACE_CUSTOMER_GROUP_'.$group['id_group']))
                        $selected_group = true;
                }

                if ($selected_group) {
                    foreach ($logged_groups as $group) {
                        if (Tools::getValue('JMARKETPLACE_CUSTOMER_GROUP_'.$group['id_group']))
                            Configuration::updateValue('JMARKETPLACE_CUSTOMER_GROUP_'.$group['id_group'], 1);
                        else
                            Configuration::updateValue('JMARKETPLACE_CUSTOMER_GROUP_'.$group['id_group'], 0);
                    }
                }
                else {
                    $errors[] = $this->l('You must select at least one group.');
                }

                $states = OrderState::getOrderStates($this->context->language->id);
                $selected_state = false;
                foreach ($states as $state) {
                    if (Tools::getValue('JMARKETPLACE_ORDER_STATE_'.$state['id_order_state']))
                        $selected_state = true;
                }

                if ($selected_state) {
                    foreach ($states as $state) {
                        if (Tools::getValue('JMARKETPLACE_ORDER_STATE_'.$state['id_order_state']))
                            Configuration::updateValue('JMARKETPLACE_ORDER_STATE_'.$state['id_order_state'], 1);
                        else
                            Configuration::updateValue('JMARKETPLACE_ORDER_STATE_'.$state['id_order_state'], 0);
                    }
                }
                else {
                    $errors[] = $this->l('You must select at least one order state for assign commissions.');
                }
                
                $selected_state = false;
                foreach ($states as $state) {
                    if (Tools::getValue('JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state']))
                        $selected_state = true;
                }

                if ($selected_state) {
                    foreach ($states as $state) {
                        if (Tools::getValue('JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state']))
                            Configuration::updateValue('JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state'], 1);
                        else
                            Configuration::updateValue('JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state'], 0);
                    }
                }
                else {
                    $errors[] = $this->l('You must select at least one order state for cancel commissions.');
                }

                if (Tools::getValue('JMARKETPLACE_COMMISIONS_ORDER') == 1 && Configuration::get('JMARKETPLACE_COMMISIONS_ORDER') == 0) {
                    Configuration::updateValue('JMARKETPLACE_COMMISIONS_ORDER', Tools::getValue('JMARKETPLACE_COMMISIONS_ORDER'));
                    Configuration::updateValue('JMARKETPLACE_COMMISIONS_STATE', 0);
                }
                else if (Tools::getValue('JMARKETPLACE_COMMISIONS_STATE') == 1 && Configuration::get('JMARKETPLACE_COMMISIONS_STATE') == 0) {
                    Configuration::updateValue('JMARKETPLACE_COMMISIONS_ORDER', 0);
                    Configuration::updateValue('JMARKETPLACE_COMMISIONS_STATE', Tools::getValue('JMARKETPLACE_COMMISIONS_STATE'));
                }

                //Configuration::updateValue('JMARKETPLACE_ORDER_STATE', Tools::getValue('JMARKETPLACE_ORDER_STATE'));
                Configuration::updateValue('JMARKETPLACE_FIXED_COMMISSION', $fixed_commission);
                Configuration::updateValue('JMARKETPLACE_VARIABLE_COMMISSION', $variable_commission);
                Configuration::updateValue('JMARKETPLACE_SHIPPING_COMMISSION', Tools::getValue('JMARKETPLACE_SHIPPING_COMMISSION'));
                Configuration::updateValue('JMARKETPLACE_TAX_COMMISSION', Tools::getValue('JMARKETPLACE_TAX_COMMISSION'));
                Configuration::updateValue('JMARKETPLACE_SELLER_IMPORT_PROD', Tools::getValue('JMARKETPLACE_SELLER_IMPORT_PROD'));
                Configuration::updateValue('JMARKETPLACE_SHOW_CONTACT', Tools::getValue('JMARKETPLACE_SHOW_CONTACT'));
                Configuration::updateValue('JMARKETPLACE_SHOW_DASHBOARD', Tools::getValue('JMARKETPLACE_SHOW_DASHBOARD'));
                Configuration::updateValue('JMARKETPLACE_SHOW_SELLER_INVOICE', Tools::getValue('JMARKETPLACE_SHOW_SELLER_INVOICE'));
                Configuration::updateValue('JMARKETPLACE_SHOW_MANAGE_ORDERS', Tools::getValue('JMARKETPLACE_SHOW_MANAGE_ORDERS'));
                Configuration::updateValue('JMARKETPLACE_SHOW_MANAGE_CARRIER', Tools::getValue('JMARKETPLACE_SHOW_MANAGE_CARRIER'));
                Configuration::updateValue('JMARKETPLACE_SHOW_PROFILE', Tools::getValue('JMARKETPLACE_SHOW_PROFILE'));
                Configuration::updateValue('JMARKETPLACE_SHOW_ORDERS', Tools::getValue('JMARKETPLACE_SHOW_ORDERS'));
                Configuration::updateValue('JMARKETPLACE_SHOW_EDIT_ACCOUNT', Tools::getValue('JMARKETPLACE_SHOW_EDIT_ACCOUNT'));
                Configuration::updateValue('JMARKETPLACE_SHOW_EDIT_PRODUCT', Tools::getValue('JMARKETPLACE_SHOW_EDIT_PRODUCT'));
                Configuration::updateValue('JMARKETPLACE_SHOW_DELETE_PRODUCT', Tools::getValue('JMARKETPLACE_SHOW_DELETE_PRODUCT'));
                Configuration::updateValue('JMARKETPLACE_SHOW_ACTIVE_PRODUCT', Tools::getValue('JMARKETPLACE_SHOW_ACTIVE_PRODUCT'));
                Configuration::updateValue('JMARKETPLACE_SELLER_FAVORITE', Tools::getValue('JMARKETPLACE_SELLER_FAVORITE'));
                Configuration::updateValue('JMARKETPLACE_SELLER_RATING', Tools::getValue('JMARKETPLACE_SELLER_RATING'));
                Configuration::updateValue('JMARKETPLACE_NEW_PRODUCTS', Tools::getValue('JMARKETPLACE_NEW_PRODUCTS'));
                Configuration::updateValue('JMARKETPLACE_SHOW_SELLER_PLIST', Tools::getValue('JMARKETPLACE_SHOW_SELLER_PLIST'));

                if (Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS') == 1 || Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER') == 1)
                    //Configuration::updateValue('PS_BLOCK_CART_AJAX', 0);
                
                if (Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS') == 0 && Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER') == 0)
                    //Configuration::updateValue('PS_BLOCK_CART_AJAX', 1);

                if (Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER') == 1)
                    Configuration::updateValue('JMARKETPLACE_SHOW_SHIP_PRODUCT', 1);
            }
            
            if (isset($errors) AND sizeof($errors))
                $this->output .= $this->displayError(implode('<br />', $errors));
            else
                $this->output .= $this->displayConfirmation($this->l('General settings updated ok.')); 
        }
        
        if (Tools::isSubmit('submitSellerAccountSettings')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_SHOP_NAME', Tools::getValue('JMARKETPLACE_SHOW_SHOP_NAME'));
            Configuration::updateValue('JMARKETPLACE_SHOW_LANGUAGE', Tools::getValue('JMARKETPLACE_SHOW_LANGUAGE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_CIF', Tools::getValue('JMARKETPLACE_SHOW_CIF'));
            Configuration::updateValue('JMARKETPLACE_SHOW_PHONE', Tools::getValue('JMARKETPLACE_SHOW_PHONE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_FAX', Tools::getValue('JMARKETPLACE_SHOW_FAX'));
            Configuration::updateValue('JMARKETPLACE_SHOW_ADDRESS', Tools::getValue('JMARKETPLACE_SHOW_ADDRESS'));
            Configuration::updateValue('JMARKETPLACE_SHOW_COUNTRY', Tools::getValue('JMARKETPLACE_SHOW_COUNTRY'));
            Configuration::updateValue('JMARKETPLACE_SHOW_STATE', Tools::getValue('JMARKETPLACE_SHOW_STATE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_CITY', Tools::getValue('JMARKETPLACE_SHOW_CITY'));
            Configuration::updateValue('JMARKETPLACE_SHOW_POSTAL_CODE', Tools::getValue('JMARKETPLACE_SHOW_POSTAL_CODE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_DESCRIPTION', Tools::getValue('JMARKETPLACE_SHOW_DESCRIPTION'));
            Configuration::updateValue('JMARKETPLACE_SHOW_LOGO', Tools::getValue('JMARKETPLACE_SHOW_LOGO'));
            Configuration::updateValue('JMARKETPLACE_SHOW_TERMS', Tools::getValue('JMARKETPLACE_SHOW_TERMS'));
            Configuration::updateValue('JMARKETPLACE_CMS_TERMS', Tools::getValue('JMARKETPLACE_CMS_TERMS'));
            $this->output .= $this->displayConfirmation($this->l('Seller account settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitSellerProductTabInformation')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_REFERENCE', Tools::getValue('JMARKETPLACE_SHOW_REFERENCE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_EAN13', Tools::getValue('JMARKETPLACE_SHOW_EAN13'));
            Configuration::updateValue('JMARKETPLACE_SHOW_UPC', Tools::getValue('JMARKETPLACE_SHOW_UPC'));
            Configuration::updateValue('JMARKETPLACE_SHOW_CONDITION', Tools::getValue('JMARKETPLACE_SHOW_CONDITION'));
            Configuration::updateValue('JMARKETPLACE_SHOW_AVAILABLE_ORD', Tools::getValue('JMARKETPLACE_SHOW_AVAILABLE_ORD'));
            Configuration::updateValue('JMARKETPLACE_SHOW_SHOW_PRICE', Tools::getValue('JMARKETPLACE_SHOW_SHOW_PRICE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_ONLINE_ONLY', Tools::getValue('JMARKETPLACE_SHOW_ONLINE_ONLY'));
            Configuration::updateValue('JMARKETPLACE_SHOW_DESC_SHORT', Tools::getValue('JMARKETPLACE_SHOW_DESC_SHORT'));
            Configuration::updateValue('JMARKETPLACE_SHOW_DESC', Tools::getValue('JMARKETPLACE_SHOW_DESC')); 
            $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitSellerProductTabPrices')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_PRICE', Tools::getValue('JMARKETPLACE_SHOW_PRICE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_WHOLESALEPRICE', Tools::getValue('JMARKETPLACE_SHOW_WHOLESALEPRICE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_OFFER_PRICE', Tools::getValue('JMARKETPLACE_SHOW_OFFER_PRICE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_TAX', Tools::getValue('JMARKETPLACE_SHOW_TAX'));
            Configuration::updateValue('JMARKETPLACE_SHOW_ON_SALE', Tools::getValue('JMARKETPLACE_SHOW_ON_SALE'));
            $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitSellerProductTabSeo')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_META_KEYWORDS', Tools::getValue('JMARKETPLACE_SHOW_META_KEYWORDS'));
            Configuration::updateValue('JMARKETPLACE_SHOW_META_TITLE', Tools::getValue('JMARKETPLACE_SHOW_META_TITLE'));
            Configuration::updateValue('JMARKETPLACE_SHOW_META_DESC', Tools::getValue('JMARKETPLACE_SHOW_META_DESC'));
            Configuration::updateValue('JMARKETPLACE_SHOW_LINK_REWRITE', Tools::getValue('JMARKETPLACE_SHOW_LINK_REWRITE'));
            $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitSellerProductTabAssociations')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_SUPPLIERS', Tools::getValue('JMARKETPLACE_SHOW_SUPPLIERS')); 
            
            if (Tools::getValue('JMARKETPLACE_NEW_SUPPLIERS') == 1)
                Configuration::updateValue('JMARKETPLACE_SHOW_SUPPLIERS', 1); 
            
            Configuration::updateValue('JMARKETPLACE_NEW_SUPPLIERS', Tools::getValue('JMARKETPLACE_NEW_SUPPLIERS')); 
            Configuration::updateValue('JMARKETPLACE_SHOW_MANUFACTURERS', Tools::getValue('JMARKETPLACE_SHOW_MANUFACTURERS')); 
            
            if (Tools::getValue('JMARKETPLACE_NEW_MANUFACTURERS') == 1)
                Configuration::updateValue('JMARKETPLACE_SHOW_MANUFACTURERS', 1); 
            
            Configuration::updateValue('JMARKETPLACE_NEW_MANUFACTURERS', Tools::getValue('JMARKETPLACE_NEW_MANUFACTURERS')); 
            Configuration::updateValue('JMARKETPLACE_SHOW_CATEGORIES', Tools::getValue('JMARKETPLACE_SHOW_CATEGORIES'));

            //selected categories
            if (Configuration::get('JMARKETPLACE_SHOW_CATEGORIES') == 1) {
                $selected_categories = Tools::getValue('categories');
                if (is_array($selected_categories) && count($selected_categories) > 0) {
                    SellerCategory::deleteSelectedCategories($this->context->shop->id);
                    foreach ($selected_categories as $sc) {
                        $seller_category = new SellerCategory();
                        $seller_category->id_category = (int)$sc;
                        $seller_category->id_shop = (int)$this->context->shop->id;
                        $seller_category->add();
                    }
                }
                else {
                    $errors[] = $this->l('You must select at least one category.');
                }
            }
            $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitSellerProductTabShipping')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_WIDTH', Tools::getValue('JMARKETPLACE_SHOW_WIDTH'));
            Configuration::updateValue('JMARKETPLACE_SHOW_HEIGHT', Tools::getValue('JMARKETPLACE_SHOW_HEIGHT'));
            Configuration::updateValue('JMARKETPLACE_SHOW_DEPTH', Tools::getValue('JMARKETPLACE_SHOW_DEPTH'));
            Configuration::updateValue('JMARKETPLACE_SHOW_WEIGHT', Tools::getValue('JMARKETPLACE_SHOW_WEIGHT'));
            Configuration::updateValue('JMARKETPLACE_SHOW_SHIP_PRODUCT', Tools::getValue('JMARKETPLACE_SHOW_SHIP_PRODUCT'));
            $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitSellerProductTabCombinations')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_ATTRIBUTES', Tools::getValue('JMARKETPLACE_SHOW_ATTRIBUTES'));
            
            /*if (Configuration::get('JMARKETPLACE_SHOW_ATTRIBUTES') == 1)
                Configuration::updateValue('JMARKETPLACE_SHOW_QUANTITY', 0); */
            
            $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitSellerProductTabQuantities')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_QUANTITY', Tools::getValue('JMARKETPLACE_SHOW_QUANTITY')); 
            
            /*if (Configuration::get('JMARKETPLACE_SHOW_ATTRIBUTES') == 1)
                Configuration::updateValue('JMARKETPLACE_SHOW_QUANTITY', 0);*/
            
            Configuration::updateValue('JMARKETPLACE_SHOW_MINIMAL_QTY', Tools::getValue('JMARKETPLACE_SHOW_MINIMAL_QTY'));
            Configuration::updateValue('JMARKETPLACE_SHOW_AVAILABLE_NOW', Tools::getValue('JMARKETPLACE_SHOW_AVAILABLE_NOW'));
            Configuration::updateValue('JMARKETPLACE_SHOW_AVAILABLE_LAT', Tools::getValue('JMARKETPLACE_SHOW_AVAILABLE_LAT'));
            Configuration::updateValue('JMARKETPLACE_SHOW_AVAILABLE_DATE', Tools::getValue('JMARKETPLACE_SHOW_AVAILABLE_DATE'));
            $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitSellerProductTabImages')) {
            $max_images = Tools::getValue('JMARKETPLACE_MAX_IMAGES');
            if (!$max_images OR $max_images <= 0 OR $max_images > 100 OR !Validate::isInt($max_images)) {
                $errors[] = $this->l('Invalid max images value.');
            }
            else {
                Configuration::updateValue('JMARKETPLACE_SHOW_IMAGES', Tools::getValue('JMARKETPLACE_SHOW_IMAGES'));
                Configuration::updateValue('JMARKETPLACE_MAX_IMAGES', Tools::getValue('JMARKETPLACE_MAX_IMAGES')); 
                $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
            }
        }
        
        if (Tools::isSubmit('submitSellerProductTabFeatures')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_FEATURES', Tools::getValue('JMARKETPLACE_SHOW_FEATURES'));
            $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitSellerProductTabVirtual')) {
            Configuration::updateValue('JMARKETPLACE_SHOW_VIRTUAL', Tools::getValue('JMARKETPLACE_SHOW_VIRTUAL'));
            $this->output .= $this->displayConfirmation($this->l('Seller product settings updated ok.'));
        }
        
        if (isset($errors) AND sizeof($errors))
            $this->output .= $this->displayError(implode('<br />', $errors));
        
        if (Tools::isSubmit('submitEmailSettings')) {
            $email = Tools::getValue('JMARKETPLACE_SEND_ADMIN');
            if (Validate::isEmail($email)) {
                Configuration::updateValue('JMARKETPLACE_SEND_ADMIN', Tools::getValue('JMARKETPLACE_SEND_ADMIN'));
                Configuration::updateValue('JMARKETPLACE_SEND_ADMIN_REGISTER', Tools::getValue('JMARKETPLACE_SEND_ADMIN_REGISTER'));
				
				Configuration::updateValue('JMARKETPLACE_SEND_ADMIN_STATUS_UPDATE', Tools::getValue('JMARKETPLACE_SEND_ADMIN_STATUS_UPDATE'));
                Configuration::updateValue('JMARKETPLACE_SEND_ADMIN_PRODUCT', Tools::getValue('JMARKETPLACE_SEND_ADMIN_PRODUCT'));
                Configuration::updateValue('JMARKETPLACE_SEND_SELLER_WELCOME', Tools::getValue('JMARKETPLACE_SEND_SELLER_WELCOME'));
                Configuration::updateValue('JMARKETPLACE_SEND_SELLER_ACTIVE', Tools::getValue('JMARKETPLACE_SEND_SELLER_ACTIVE'));
                Configuration::updateValue('JMARKETPLACE_SEND_PRODUCT_ACTIVE', Tools::getValue('JMARKETPLACE_SEND_PRODUCT_ACTIVE'));
                Configuration::updateValue('JMARKETPLACE_SEND_PRODUCT_SOLD', Tools::getValue('JMARKETPLACE_SEND_PRODUCT_SOLD'));
                $this->output .= $this->displayConfirmation($this->l('Email settings updated ok.'));
            }
            else {
                $this->output .= $this->displayError($this->l('The email is incorrect.'));
            }
        }
        
        if (Tools::isSubmit('submitThemeSettings')) {
            Configuration::updateValue('JMARKETPLACE_THEME', Tools::getValue('JMARKETPLACE_THEME'));
            Configuration::updateValue('JMARKETPLACE_TABS', Tools::getValue('JMARKETPLACE_TABS'));
            Configuration::updateValue('JMARKETPLACE_MENU_TOP', Tools::getValue('JMARKETPLACE_MENU_TOP'));
            Configuration::updateValue('JMARKETPLACE_MENU_OPTIONS', Tools::getValue('JMARKETPLACE_MENU_OPTIONS'));
            $this->output .= $this->displayConfirmation($this->l('Theme settings updated ok.'));
        }
        
        if (Tools::isSubmit('submitPayments')) {
            if (Tools::getValue('JMARKETPLACE_PAYPAL') == 0 && Tools::getValue('JMARKETPLACE_BANKWIRE') == 0) {
                $this->output .= $this->displayError($this->l('You must select a payment method.'));
            }
            else {
                Configuration::updateValue('JMARKETPLACE_PAYPAL', Tools::getValue('JMARKETPLACE_PAYPAL'));
                Configuration::updateValue('JMARKETPLACE_BANKWIRE', Tools::getValue('JMARKETPLACE_BANKWIRE'));
                $this->output .= $this->displayConfirmation($this->l('Seller payments updated ok.'));
            }  
        }
    }
    
    public function getContent15() 
    {
        $this->output .= '<link href="../modules/'.$this->name.'/css/'.$this->name.'.css" rel="stylesheet" type="text/css" />';
        $this->output .= '<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">';
        $this->output .= '<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>';
        $this->output .= '<script type="text/javascript">
                            $(document).ready(function(){
                                $( "#tabs" ).tabs();
                            });
                            </script>';
        $this->output .= '<div id="tabs">';
        $this->output .= '<ul>
                            <li><a href="#tabs-1">'.$this->l('General settings').'</a></li>
                            <li><a href="#tabs-2">'.$this->l('Seller account').'</a></li>
                            <li><a href="#tabs-3">'.$this->l('Seller product').'</a></li>
                            <li><a href="#tabs-6">'.$this->l('Seller payment').'</a></li> 
                            <li><a href="#tabs-4">'.$this->l('Emails').'</a></li> 
                            <li><a href="#tabs-5">'.$this->l('Front office theme').'</a></li> 
                          </ul>';

        $this->output .= '<div id="tabs-1">';
        $this->output .= $this->displayFormGeneralSettings();
        $this->output .= '</div>';

        $this->output .= '<div id="tabs-2">';
        $this->output .= $this->displayFormSellerAccountSettings();
        $this->output .= '</div>';

        $this->output .= '<div id="tabs-3">';

        $this->output .= $this->displayFormSellerProductSettings();

        $this->output .= '</div>';

        $this->output .= '<div id="tabs-4">';

        $this->output .= $this->displayFormEmailSettings();

        $this->output .= '</div>';

        $this->output .= '<div id="tabs-5">';

        $this->output .= $this->displayFormThemeSettings();

        $this->output .= '</div>';

        $this->output .= '<div id="tabs-6">';

        $this->output .= $this->displayFormPayments();

        $this->output .= '</div>';

        $this->output .= '</div>';

        return $this->output;
    }
    
    public function getContent16() 
    {
        $this->output .= '<div id="modulecontent" class="clearfix">';
        $this->output .= '<div class="col-lg-2">';
        $this->output .= '<div class="list-group">';
        
        $this->output .= '<a href="#information" class="list-group-item active" data-toggle="tab">'.$this->l('Information').'</a>';
        
        $this->output .= '<a href="#general_settings" class="list-group-item" data-toggle="tab">'.$this->l('General settings').'</a>';
        $this->output .= '<a href="#seller_account_settings" class="list-group-item" data-toggle="tab">'.$this->l('Seller account').'</a>';
        $this->output .= '<a href="#seller_product_settings" class="list-group-item" data-toggle="tab">'.$this->l('Seller product').'</a>';
        $this->output .= '<a href="#email_settings" class="list-group-item" data-toggle="tab">'.$this->l('Emails').'</a>';
        $this->output .= '<a href="#seller_payment_settings" class="list-group-item" data-toggle="tab">'.$this->l('Seller payment').'</a>';
        $this->output .= '<a href="#theme_settings" class="list-group-item" data-toggle="tab">'.$this->l('Front office theme').'</a>';
        
        $this->output .= '</div>';
        $this->output .= '</div>';

        
        $this->output .= '<div class="tab-content col-lg-10">';
        
        $this->output .= '<div class="tab-pane active panel" id="information">';
        
        $this->context->smarty->assign(array(
            'module_dir' => $this->_path,
            'name' => $this->displayName, 
            'version' => $this->version, 
            'description' => $this->description,
            'iso_code' => $this->context->language->iso_code,
        ));
	$this->output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/information.tpl');
        
        $this->output .= '</div>';
        
        $this->output .= '<div class="tab-pane panel" id="general_settings">';
        $this->output .= $this->displayFormGeneralSettings();
        $this->output .= '</div>';

        $this->output .= '<div class="tab-pane panel" id="seller_account_settings">';   
        $this->output .= $this->displayFormSellerAccountSettings();   
        $this->output .= '</div>';

        $this->output .= '<div class="tab-pane panel" id="seller_product_settings">';   
        $this->output .= $this->displayFormSellerProductSettings();    
        $this->output .= '</div>';

        $this->output .= '<div  class="tab-pane panel" id="email_settings">';
        $this->output .= $this->displayFormEmailSettings();
        $this->output .= '</div>';

        $this->output .= '<div  class="tab-pane panel" id="seller_payment_settings">';
        $this->output .= $this->displayFormPayments();   
        $this->output .= '</div>';       

        $this->output .= '<div  class="tab-pane panel" id="theme_settings">';
        $this->output .= $this->displayFormThemeSettings();      
        $this->output .= '</div>';            
        
        $this->output .= '</div>';

        $this->output .= '</div>';
        
        $this->output .= '<script type="text/javascript">
                            $(document).ready(function(){
                                $(".list-group-item").on("click", function() {
                                    $(".list-group-item").removeClass("active");
                                    $(this).addClass("active");
                                });
                            });
                            </script>';
        
        return $this->output;
    }
    
    public function getContent() 
    {        
        $this->postProcess();
        
        if (version_compare(_PS_VERSION_, '1.6', '<'))
            return $this->getContent15();
        else
            return $this->getContent16();
        
    }
    
    public function getGroupsToSeller() 
    {
        $logged_groups = array();
        $groups = Group::getGroups($this->context->language->id);
        foreach ($groups as $group) {
            if ($group['id_group'] > 2)
                $logged_groups[] = $group;
        }
        return $logged_groups;
    }
    
    private function displayFormGeneralSettings() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            $type = 'radio';
            $disabled = true;
        }
        else {
            $type = 'switch';
            $disabled = false;
        }
        
        $customer_groups_to_seller = $this->getGroupsToSeller();
        
        $states = OrderState::getOrderStates($this->context->language->id);
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitGeneralSettings';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false,
            'legend' => array(
                'title' => $this->l('General settings')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Moderate sellers'),
                    'name' => 'JMARKETPLACE_MODERATE_SELLER',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Moderate products'),
                    'name' => 'JMARKETPLACE_MODERATE_PRODUCT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Fixed commission'),
                    'suffix' => $this->context->currency->sign,
                    'name' => 'JMARKETPLACE_FIXED_COMMISSION',
                    'desc' => $this->l('Fixed commission for each sale.'),
                    'required' => true,
                    'lang' => false,
                    'col' => 2,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Variable commission'),
                    'suffix' => '%',
                    'name' => 'JMARKETPLACE_VARIABLE_COMMISSION',
                    'desc' => $this->l('This percentage is applied to the total price of products sold. The seller collect').' '.Configuration::get('JMARKETPLACE_VARIABLE_COMMISSION').'% '.$this->l('of sale of your products. Values: 0-100'),
                    'required' => true,
                    'lang' => false,
                    'col' => 2,
                ),
                array(
                    'type' => 'checkbox',
                    'label' => $this->l('Customer group'), 
                    'desc' => $this->l('Select group of customers who may be selling.'),  
                    'name' => 'JMARKETPLACE_CUSTOMER_GROUP', 
                    'values' => array(
                        'query' => $customer_groups_to_seller,
			'id' => 'id_group',
			'name' => 'name'
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Assign commisions when a customer places an order'),
                    'name' => 'JMARKETPLACE_COMMISIONS_ORDER',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('Commissions are awarded when a customer places an order'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Assign commisions when an order status changes'),
                    'name' => 'JMARKETPLACE_COMMISIONS_STATE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('Commissions are awarded when an order status changes'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'checkbox',
                    'label' => $this->l('Order state'), 
                    'desc' => $this->l('Select the order status to send notification to vendors when an order is changed to this state.'),  
                    'name' => 'JMARKETPLACE_ORDER_STATE', 
                    'values' => array(
                        'query' => $states,
			'id' => 'id_order_state',
			'name' => 'name'
                    ),
                ),
                /*array(
		  'type' => 'select',
		  'label' => $this->l('Order state'),
		  'name' => 'JMARKETPLACE_ORDER_STATE',
                  'desc' => $this->l('Select the order status to send notification to vendors when an order is changed to this state.'),
		  'required' => false,
		  'options' => array(
			'query' => $states,
			'id' => 'id_order_state',
			'name' => 'name'
		  )
		),*/
                array(
                    'type' => $type,
                    'label' => $this->l('Seller assumes shipping'),
                    'name' => 'JMARKETPLACE_SHIPPING_COMMISSION',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('The shipping cost is asssumed by seller.'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Seller assumes taxes'),
                    'name' => 'JMARKETPLACE_TAX_COMMISSION',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('The seller pay taxes.'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'checkbox',
                    'label' => $this->l('Cancel commissions'), 
                    'desc' => $this->l('Cancel commissions automatequely when the order changes state.'),  
                    'name' => 'JMARKETPLACE_CANCEL_COMMISSION', 
                    'values' => array(
                        'query' => $states,
			'id' => 'id_order_state',
			'name' => 'name'
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show contact seller in product page'),
                    'name' => 'JMARKETPLACE_SHOW_CONTACT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show dashboard'),
                    'name' => 'JMARKETPLACE_SHOW_DASHBOARD',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller transfer request'),
                    'name' => 'JMARKETPLACE_SHOW_SELLER_INVOICE',
                    'desc' => $this->l('Seller can send a transfer request to collect your commissions.'),  
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller profile'),
                    'name' => 'JMARKETPLACE_SHOW_PROFILE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show orders and commisions to sellers'),
                    'name' => 'JMARKETPLACE_SHOW_ORDERS',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Allow to sellers manage your orders'),
                    'name' => 'JMARKETPLACE_SHOW_MANAGE_ORDERS',
                    'required' => false,
                    'disabled' => $disabled,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Allow to sellers manage your carriers and shipping cost'),
                    'name' => 'JMARKETPLACE_SHOW_MANAGE_CARRIER',
                    'required' => false,
                    'disabled' => $disabled,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show edit sellers account'),
                    'name' => 'JMARKETPLACE_SHOW_EDIT_ACCOUNT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Allow to sellers edit product'),
                    'name' => 'JMARKETPLACE_SHOW_EDIT_PRODUCT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Allow to sellers delete product'),
                    'name' => 'JMARKETPLACE_SHOW_DELETE_PRODUCT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Allow to sellers enable and disable your products'),
                    'name' => 'JMARKETPLACE_SHOW_ACTIVE_PRODUCT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Allow to sellers import and export your products with csv file'),
                    'name' => 'JMARKETPLACE_SELLER_IMPORT_PROD',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Allow favorite seller'),
                    'name' => 'JMARKETPLACE_SELLER_FAVORITE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('Allow customers to add favorite sellers in your account (followers)'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Allow rating seller'),
                    'name' => 'JMARKETPLACE_SELLER_RATING',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('Allow customers to add average grade of sellers.'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show new products in seller profile'),
                    'name' => 'JMARKETPLACE_NEW_PRODUCTS',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('Allow show new products in sellers profile.'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller in product list'),
                    'name' => 'JMARKETPLACE_SHOW_SELLER_PLIST',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('Allow show the seller in all product listings.'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitGeneralSettings',
                'title' => $this->l('Save'),
            ),
        );
        
        if ($customer_groups_to_seller) {
            foreach ($customer_groups_to_seller as $group) {
                if (Configuration::get('JMARKETPLACE_CUSTOMER_GROUP_'.$group['id_group']) == 1)
                    $helper->fields_value['JMARKETPLACE_CUSTOMER_GROUP_'.$group['id_group']] = 1;
                else
                    $helper->fields_value['JMARKETPLACE_CUSTOMER_GROUP_'.$group['id_group']] = 0;
            }
        }
        
        if ($states) {
            foreach ($states as $state) {
                if (Configuration::get('JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state']) == 1)
                    $helper->fields_value['JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state']] = 1;
                else
                    $helper->fields_value['JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state']] = 0;
                
                if (Configuration::get('JMARKETPLACE_ORDER_STATE_'.$state['id_order_state']) == 1)
                    $helper->fields_value['JMARKETPLACE_ORDER_STATE_'.$state['id_order_state']] = 1;
                else
                    $helper->fields_value['JMARKETPLACE_ORDER_STATE_'.$state['id_order_state']] = 0;
            }
        }

        $helper->fields_value['JMARKETPLACE_MODERATE_SELLER'] = Configuration::get('JMARKETPLACE_MODERATE_SELLER');
        $helper->fields_value['JMARKETPLACE_MODERATE_PRODUCT'] = Configuration::get('JMARKETPLACE_MODERATE_PRODUCT');
        $helper->fields_value['JMARKETPLACE_COMMISIONS_ORDER'] = Configuration::get('JMARKETPLACE_COMMISIONS_ORDER');
        $helper->fields_value['JMARKETPLACE_COMMISIONS_STATE'] = Configuration::get('JMARKETPLACE_COMMISIONS_STATE');
        $helper->fields_value['JMARKETPLACE_FIXED_COMMISSION'] = Configuration::get('JMARKETPLACE_FIXED_COMMISSION');
        $helper->fields_value['JMARKETPLACE_VARIABLE_COMMISSION'] = Configuration::get('JMARKETPLACE_VARIABLE_COMMISSION');
        $helper->fields_value['JMARKETPLACE_SHIPPING_COMMISSION'] = Configuration::get('JMARKETPLACE_SHIPPING_COMMISSION');
        $helper->fields_value['JMARKETPLACE_TAX_COMMISSION'] = Configuration::get('JMARKETPLACE_TAX_COMMISSION');
        $helper->fields_value['JMARKETPLACE_SHOW_CONTACT'] = Configuration::get('JMARKETPLACE_SHOW_CONTACT');
        $helper->fields_value['JMARKETPLACE_SHOW_DASHBOARD'] = Configuration::get('JMARKETPLACE_SHOW_DASHBOARD');
        $helper->fields_value['JMARKETPLACE_SHOW_SELLER_INVOICE'] = Configuration::get('JMARKETPLACE_SHOW_SELLER_INVOICE');
        $helper->fields_value['JMARKETPLACE_SHOW_MANAGE_ORDERS'] = Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS');
        $helper->fields_value['JMARKETPLACE_SHOW_MANAGE_CARRIER'] = Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER');
        $helper->fields_value['JMARKETPLACE_SHOW_PROFILE'] = Configuration::get('JMARKETPLACE_SHOW_PROFILE');
        $helper->fields_value['JMARKETPLACE_SHOW_EDIT_ACCOUNT'] = Configuration::get('JMARKETPLACE_SHOW_EDIT_ACCOUNT');
        $helper->fields_value['JMARKETPLACE_SHOW_EDIT_PRODUCT'] = Configuration::get('JMARKETPLACE_SHOW_EDIT_PRODUCT');
        $helper->fields_value['JMARKETPLACE_SHOW_DELETE_PRODUCT'] = Configuration::get('JMARKETPLACE_SHOW_DELETE_PRODUCT');
        $helper->fields_value['JMARKETPLACE_SHOW_ACTIVE_PRODUCT'] = Configuration::get('JMARKETPLACE_SHOW_ACTIVE_PRODUCT');
        $helper->fields_value['JMARKETPLACE_SELLER_IMPORT_PROD'] = Configuration::get('JMARKETPLACE_SELLER_IMPORT_PROD');
        $helper->fields_value['JMARKETPLACE_SHOW_ORDERS'] = Configuration::get('JMARKETPLACE_SHOW_ORDERS');
        $helper->fields_value['JMARKETPLACE_SELLER_FAVORITE'] = Configuration::get('JMARKETPLACE_SELLER_FAVORITE');
        $helper->fields_value['JMARKETPLACE_SELLER_RATING'] = Configuration::get('JMARKETPLACE_SELLER_RATING');
        $helper->fields_value['JMARKETPLACE_NEW_PRODUCTS'] = Configuration::get('JMARKETPLACE_NEW_PRODUCTS');
        $helper->fields_value['JMARKETPLACE_SHOW_SELLER_PLIST'] = Configuration::get('JMARKETPLACE_SHOW_SELLER_PLIST');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerAccountSettings() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<'))
            $type = 'radio';
        else
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerAccountSettings';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Seller account')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller shop name'),
                    'name' => 'JMARKETPLACE_SHOW_SHOP_NAME',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller CIF/NIF'),
                    'name' => 'JMARKETPLACE_SHOW_CIF',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller language'),
                    'name' => 'JMARKETPLACE_SHOW_LANGUAGE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller phone'),
                    'name' => 'JMARKETPLACE_SHOW_PHONE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller fax'),
                    'name' => 'JMARKETPLACE_SHOW_FAX',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller address'),
                    'name' => 'JMARKETPLACE_SHOW_ADDRESS',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller country'),
                    'name' => 'JMARKETPLACE_SHOW_COUNTRY',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller state'),
                    'name' => 'JMARKETPLACE_SHOW_STATE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller city'),
                    'name' => 'JMARKETPLACE_SHOW_CITY',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller postal code'),
                    'name' => 'JMARKETPLACE_SHOW_POSTAL_CODE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller description'),
                    'name' => 'JMARKETPLACE_SHOW_DESCRIPTION',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show seller logo'),
                    'name' => 'JMARKETPLACE_SHOW_LOGO',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show terms of service'),
                    'name' => 'JMARKETPLACE_SHOW_TERMS',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Page CMS Terms'),
                    'name' => 'JMARKETPLACE_CMS_TERMS',
                    'desc' => $this->l('Select page cms to terms of service to sellers.'),
                    'required' => false,
                    'options' => array(
                          'query' => CMS::getCMSPages($this->context->language->id),
                          'id' => 'id_cms',
                          'name' => 'meta_title'
                    )
		),
            ),
            'submit' => array(
                'name' => 'submitSellerAccountSettings',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_SHOP_NAME'] = Configuration::get('JMARKETPLACE_SHOW_SHOP_NAME');
        $helper->fields_value['JMARKETPLACE_SHOW_CIF'] = Configuration::get('JMARKETPLACE_SHOW_CIF');
        $helper->fields_value['JMARKETPLACE_SHOW_LANGUAGE'] = Configuration::get('JMARKETPLACE_SHOW_LANGUAGE');
        $helper->fields_value['JMARKETPLACE_SHOW_PHONE'] = Configuration::get('JMARKETPLACE_SHOW_PHONE');
        $helper->fields_value['JMARKETPLACE_SHOW_FAX'] = Configuration::get('JMARKETPLACE_SHOW_FAX');
        $helper->fields_value['JMARKETPLACE_SHOW_ADDRESS'] = Configuration::get('JMARKETPLACE_SHOW_ADDRESS');
        $helper->fields_value['JMARKETPLACE_SHOW_COUNTRY'] = Configuration::get('JMARKETPLACE_SHOW_COUNTRY');
        $helper->fields_value['JMARKETPLACE_SHOW_STATE'] = Configuration::get('JMARKETPLACE_SHOW_STATE');
        $helper->fields_value['JMARKETPLACE_SHOW_CITY'] = Configuration::get('JMARKETPLACE_SHOW_CITY');
        $helper->fields_value['JMARKETPLACE_SHOW_POSTAL_CODE'] = Configuration::get('JMARKETPLACE_SHOW_POSTAL_CODE');
        $helper->fields_value['JMARKETPLACE_SHOW_DESCRIPTION'] = Configuration::get('JMARKETPLACE_SHOW_DESCRIPTION');
        $helper->fields_value['JMARKETPLACE_SHOW_LOGO'] = Configuration::get('JMARKETPLACE_SHOW_LOGO');
        $helper->fields_value['JMARKETPLACE_SHOW_TERMS'] = Configuration::get('JMARKETPLACE_SHOW_TERMS');
        $helper->fields_value['JMARKETPLACE_CMS_TERMS'] = Configuration::get('JMARKETPLACE_CMS_TERMS');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductInformation() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) 
            $type = 'radio';
        else 
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabInformation';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab information')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show reference'),
                    'name' => 'JMARKETPLACE_SHOW_REFERENCE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show EAN13'),
                    'name' => 'JMARKETPLACE_SHOW_EAN13',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show UPC'),
                    'name' => 'JMARKETPLACE_SHOW_UPC',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show available for order'),
                    'name' => 'JMARKETPLACE_SHOW_AVAILABLE_ORD',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show display price'),
                    'name' => 'JMARKETPLACE_SHOW_SHOW_PRICE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show online only'),
                    'name' => 'JMARKETPLACE_SHOW_ONLINE_ONLY',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show condition'),
                    'name' => 'JMARKETPLACE_SHOW_CONDITION',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show description short'),
                    'name' => 'JMARKETPLACE_SHOW_DESC_SHORT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show description'),
                    'name' => 'JMARKETPLACE_SHOW_DESC',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabInformation',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_REFERENCE'] = Configuration::get('JMARKETPLACE_SHOW_REFERENCE');
        $helper->fields_value['JMARKETPLACE_SHOW_EAN13'] = Configuration::get('JMARKETPLACE_SHOW_EAN13');
        $helper->fields_value['JMARKETPLACE_SHOW_UPC'] = Configuration::get('JMARKETPLACE_SHOW_UPC');
        $helper->fields_value['JMARKETPLACE_SHOW_AVAILABLE_ORD'] = Configuration::get('JMARKETPLACE_SHOW_AVAILABLE_ORD');
        $helper->fields_value['JMARKETPLACE_SHOW_SHOW_PRICE'] = Configuration::get('JMARKETPLACE_SHOW_SHOW_PRICE');
        $helper->fields_value['JMARKETPLACE_SHOW_ONLINE_ONLY'] = Configuration::get('JMARKETPLACE_SHOW_ONLINE_ONLY');
        $helper->fields_value['JMARKETPLACE_SHOW_CONDITION'] = Configuration::get('JMARKETPLACE_SHOW_CONDITION');
        $helper->fields_value['JMARKETPLACE_SHOW_DESC_SHORT'] = Configuration::get('JMARKETPLACE_SHOW_DESC_SHORT');
        $helper->fields_value['JMARKETPLACE_SHOW_DESC'] = Configuration::get('JMARKETPLACE_SHOW_DESC');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductPrices() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) 
            $type = 'radio';
        else 
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabPrices';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab prices')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show price'),
                    'name' => 'JMARKETPLACE_SHOW_PRICE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show wholesale price'),
                    'name' => 'JMARKETPLACE_SHOW_WHOLESALEPRICE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show offer price'),
                    'name' => 'JMARKETPLACE_SHOW_OFFER_PRICE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show tax'),
                    'name' => 'JMARKETPLACE_SHOW_TAX',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show on sale'),
                    'name' => 'JMARKETPLACE_SHOW_ON_SALE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabPrices',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_PRICE'] = Configuration::get('JMARKETPLACE_SHOW_PRICE');
        $helper->fields_value['JMARKETPLACE_SHOW_WHOLESALEPRICE'] = Configuration::get('JMARKETPLACE_SHOW_WHOLESALEPRICE');
        $helper->fields_value['JMARKETPLACE_SHOW_OFFER_PRICE'] = Configuration::get('JMARKETPLACE_SHOW_OFFER_PRICE');
        $helper->fields_value['JMARKETPLACE_SHOW_TAX'] = Configuration::get('JMARKETPLACE_SHOW_TAX');
        $helper->fields_value['JMARKETPLACE_SHOW_ON_SALE'] = Configuration::get('JMARKETPLACE_SHOW_ON_SALE');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductSEO() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) 
            $type = 'radio';
        else 
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabSeo';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab SEO')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show meta keywords'),
                    'name' => 'JMARKETPLACE_SHOW_META_KEYWORDS',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show meta title'),
                    'name' => 'JMARKETPLACE_SHOW_META_TITLE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show meta description'),
                    'name' => 'JMARKETPLACE_SHOW_META_DESC',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Friendly URL'),
                    'name' => 'JMARKETPLACE_SHOW_LINK_REWRITE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabSeo',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_META_KEYWORDS'] = Configuration::get('JMARKETPLACE_SHOW_META_KEYWORDS');
        $helper->fields_value['JMARKETPLACE_SHOW_META_TITLE'] = Configuration::get('JMARKETPLACE_SHOW_META_TITLE');
        $helper->fields_value['JMARKETPLACE_SHOW_META_DESC'] = Configuration::get('JMARKETPLACE_SHOW_META_DESC');
        $helper->fields_value['JMARKETPLACE_SHOW_LINK_REWRITE'] = Configuration::get('JMARKETPLACE_SHOW_LINK_REWRITE');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductAssociations() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            $type = 'radio';
            $type_selector_categories = 'select';
        }
        else {
            $type = 'switch';
            $type_selector_categories = 'categories';
        }
        
        $selected_categories = SellerCategory::getSelectedCategories($this->context->shop->id);
        $finalCategories = array();
            
        foreach ($selected_categories as $category)
            $finalCategories[] = $category['id_category'];
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabAssociations';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab Associations')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show select categories'),
                    'name' => 'JMARKETPLACE_SHOW_CATEGORIES',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                 array(
                    'type' => $type_selector_categories,
                    'label' => $this->l('Selected categories to sellers'), 
                    'name' => 'categories', 
                    'tree' => array(
                        'id' => 'categories', 
                        'title' => $this->l('Choose categories where sellers can add their products.'),
                        'use_search'    => false,
                        'use_checkbox'  => true,
                        'selected_categories' => $finalCategories
                    )
                ), 
                array(
                    'type' => $type,
                    'label' => $this->l('Show select suppliers'),
                    'name' => 'JMARKETPLACE_SHOW_SUPPLIERS',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Add new suppliers'),
                    'name' => 'JMARKETPLACE_NEW_SUPPLIERS',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show select manufacturers'),
                    'name' => 'JMARKETPLACE_SHOW_MANUFACTURERS',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Add new manufacturers'),
                    'name' => 'JMARKETPLACE_NEW_MANUFACTURERS',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabAssociations',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_CATEGORIES'] = Configuration::get('JMARKETPLACE_SHOW_CATEGORIES');
        $helper->fields_value['JMARKETPLACE_SHOW_SUPPLIERS'] = Configuration::get('JMARKETPLACE_SHOW_SUPPLIERS');
        $helper->fields_value['JMARKETPLACE_NEW_SUPPLIERS'] = Configuration::get('JMARKETPLACE_NEW_SUPPLIERS');
        $helper->fields_value['JMARKETPLACE_SHOW_MANUFACTURERS'] = Configuration::get('JMARKETPLACE_SHOW_MANUFACTURERS');
        $helper->fields_value['JMARKETPLACE_NEW_MANUFACTURERS'] = Configuration::get('JMARKETPLACE_NEW_MANUFACTURERS');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductShipping() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) 
            $type = 'radio';
        else 
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabShipping';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab Shipping')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show width'),
                    'name' => 'JMARKETPLACE_SHOW_WIDTH',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show depth'),
                    'name' => 'JMARKETPLACE_SHOW_DEPTH',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show height'),
                    'name' => 'JMARKETPLACE_SHOW_HEIGHT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show weight'),
                    'name' => 'JMARKETPLACE_SHOW_WEIGHT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show shipping by product'),
                    'name' => 'JMARKETPLACE_SHOW_SHIP_PRODUCT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabShipping',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_WIDTH'] = Configuration::get('JMARKETPLACE_SHOW_WIDTH');
        $helper->fields_value['JMARKETPLACE_SHOW_HEIGHT'] = Configuration::get('JMARKETPLACE_SHOW_HEIGHT');
        $helper->fields_value['JMARKETPLACE_SHOW_DEPTH'] = Configuration::get('JMARKETPLACE_SHOW_DEPTH');
        $helper->fields_value['JMARKETPLACE_SHOW_WEIGHT'] = Configuration::get('JMARKETPLACE_SHOW_WEIGHT');
        $helper->fields_value['JMARKETPLACE_SHOW_SHIP_PRODUCT'] = Configuration::get('JMARKETPLACE_SHOW_SHIP_PRODUCT');

        return $helper->generateForm($this->fields_form);
    }

    private function displayFormSellerProductCombinations() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) 
            $type = 'radio';
        else 
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabCombinations';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab Combinations')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show attributes'),
                    'name' => 'JMARKETPLACE_SHOW_ATTRIBUTES',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabFeatures',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_ATTRIBUTES'] = Configuration::get('JMARKETPLACE_SHOW_ATTRIBUTES');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductQuantities() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) 
            $type = 'radio';
        else 
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabQuantities';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab Quantities')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show quantity'),
                    'name' => 'JMARKETPLACE_SHOW_QUANTITY',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show minimal quantity'),
                    'name' => 'JMARKETPLACE_SHOW_MINIMAL_QTY',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show available now'),
                    'name' => 'JMARKETPLACE_SHOW_AVAILABLE_NOW',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show available later'),
                    'name' => 'JMARKETPLACE_SHOW_AVAILABLE_LAT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show available date'),
                    'name' => 'JMARKETPLACE_SHOW_AVAILABLE_DATE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),      
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabQuantities',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_QUANTITY'] = Configuration::get('JMARKETPLACE_SHOW_QUANTITY');
        $helper->fields_value['JMARKETPLACE_SHOW_MINIMAL_QTY'] = Configuration::get('JMARKETPLACE_SHOW_MINIMAL_QTY');
        $helper->fields_value['JMARKETPLACE_SHOW_AVAILABLE_NOW'] = Configuration::get('JMARKETPLACE_SHOW_AVAILABLE_NOW');
        $helper->fields_value['JMARKETPLACE_SHOW_AVAILABLE_LAT'] = Configuration::get('JMARKETPLACE_SHOW_AVAILABLE_LAT');
        $helper->fields_value['JMARKETPLACE_SHOW_AVAILABLE_DATE'] = Configuration::get('JMARKETPLACE_SHOW_AVAILABLE_DATE'); 

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductImages() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) 
            $type = 'radio';
        else 
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabImages';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab Images')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show images'),
                    'name' => 'JMARKETPLACE_SHOW_IMAGES',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Maximum number of images to upload'),
                    'name' => 'JMARKETPLACE_MAX_IMAGES',
                    'required' => false,
                    'lang' => false,
                    'col' => 2,
                ),
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabImages',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_IMAGES'] = Configuration::get('JMARKETPLACE_SHOW_IMAGES');
        $helper->fields_value['JMARKETPLACE_MAX_IMAGES'] = Configuration::get('JMARKETPLACE_MAX_IMAGES');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductFeatures() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) 
            $type = 'radio';
        else 
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabFeatures';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab Features')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show features'),
                    'name' => 'JMARKETPLACE_SHOW_FEATURES',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabFeatures',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_FEATURES'] = Configuration::get('JMARKETPLACE_SHOW_FEATURES');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductVirtual() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<')) 
            $type = 'radio';
        else 
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitSellerProductTabVirtual';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Tab Virtual product')
            ),
            'input' => array(
                array(
                    'type' => $type,
                    'label' => $this->l('Show virtual product'),
                    'name' => 'JMARKETPLACE_SHOW_VIRTUAL',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitSellerProductTabVirtual',
                'title' => $this->l('Save'),
            ),
        );

        $helper->fields_value['JMARKETPLACE_SHOW_VIRTUAL'] = Configuration::get('JMARKETPLACE_SHOW_VIRTUAL');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormSellerProductSettings() 
    {
        return $this->displayFormSellerProductInformation().
                $this->displayFormSellerProductPrices().
                $this->displayFormSellerProductSEO().
                $this->displayFormSellerProductAssociations().
                $this->displayFormSellerProductShipping().
                $this->displayFormSellerProductCombinations().
                $this->displayFormSellerProductQuantities().
                $this->displayFormSellerProductImages().
                $this->displayFormSellerProductFeatures().
                $this->displayFormSellerProductVirtual();
    }
    
    private function displayFormEmailSettings() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<'))
            $type = 'radio';
        else
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitEmailSettings';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false, 
            'legend' => array(
                'title' => $this->l('Emails')
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Email'),
                    'name' => 'JMARKETPLACE_SEND_ADMIN',
                    'desc' => $this->l('This email receives all notifications from the marketplace.'),
                    'required' => false,
                    'lang' => false,
                    'col' => 6,
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Send email to administrator when seller register'),
                    'name' => 'JMARKETPLACE_SEND_ADMIN_REGISTER',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
				array(
                    'type' => $type,
                    'label' => $this->l('Send email to administrator when order status change'),
                    'name' => 'JMARKETPLACE_SEND_ADMIN_STATUS_UPDATE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Send email to admininstrator when selle add new product'),
                    'name' => 'JMARKETPLACE_SEND_ADMIN_PRODUCT',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Send welcome email to seller'),
                    'name' => 'JMARKETPLACE_SEND_SELLER_WELCOME',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Send email to seller when your account has been activated'),
                    'name' => 'JMARKETPLACE_SEND_SELLER_ACTIVE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Send email to seller when your product has been activated'),
                    'name' => 'JMARKETPLACE_SEND_PRODUCT_ACTIVE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Send email to seller when your product has been solded'),
                    'name' => 'JMARKETPLACE_SEND_PRODUCT_SOLD',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitEmailSettings',
                'title' => $this->l('Save'),
            ),
        );
        
        $helper->fields_value['JMARKETPLACE_SEND_ADMIN'] = Configuration::get('JMARKETPLACE_SEND_ADMIN');
		$helper->fields_value['JMARKETPLACE_SEND_ADMIN_STATUS_UPDATE'] = Configuration::get('JMARKETPLACE_SEND_ADMIN_STATUS_UPDATE');
        $helper->fields_value['JMARKETPLACE_SEND_ADMIN_REGISTER'] = Configuration::get('JMARKETPLACE_SEND_ADMIN_REGISTER');
        $helper->fields_value['JMARKETPLACE_SEND_ADMIN_PRODUCT'] = Configuration::get('JMARKETPLACE_SEND_ADMIN_PRODUCT');
        $helper->fields_value['JMARKETPLACE_SEND_SELLER_WELCOME'] = Configuration::get('JMARKETPLACE_SEND_SELLER_WELCOME');
        $helper->fields_value['JMARKETPLACE_SEND_SELLER_ACTIVE'] = Configuration::get('JMARKETPLACE_SEND_SELLER_ACTIVE');
        $helper->fields_value['JMARKETPLACE_SEND_PRODUCT_ACTIVE'] = Configuration::get('JMARKETPLACE_SEND_PRODUCT_ACTIVE');
        $helper->fields_value['JMARKETPLACE_SEND_PRODUCT_SOLD'] = Configuration::get('JMARKETPLACE_SEND_PRODUCT_SOLD');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormThemeSettings() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<'))
            $type = 'radio';
        else
            $type = 'switch';
        
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitThemeSettings';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false,
            'legend' => array(
                'title' => $this->l('Front office theme')
            ),
            'input' => array(  
                array(
                    'type' => 'radio',
                    'label' => $this->l('Select theme to front office'),
                    'name' => 'JMARKETPLACE_THEME',
                    'required' => false,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'default',
                                'value' => 'default',
                                'label' => $this->l('default')
                        ),
                        array(
                                'id' => 'default-bootstrap',
                                'value' => 'default-bootstrap',
                                'label' => $this->l('default-bootstrap')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show tabs'),
                    'name' => 'JMARKETPLACE_TABS',
                    'desc' => $this->l('Page to add products and edit product has tabs.'),
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show top menu'),
                    'name' => 'JMARKETPLACE_MENU_TOP',
                    'desc' => $this->l('Show top menu of options on all page.'),
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Show menu options'),
                    'name' => 'JMARKETPLACE_MENU_OPTIONS',
                    'desc' => $this->l('Show side menu of options on all page.'),
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submitThemeSettings',
                'title' => $this->l('Save'),
            ),
        );
        
        $helper->fields_value['JMARKETPLACE_THEME'] = Configuration::get('JMARKETPLACE_THEME');
        $helper->fields_value['JMARKETPLACE_TABS'] = Configuration::get('JMARKETPLACE_TABS');
        $helper->fields_value['JMARKETPLACE_MENU_TOP'] = Configuration::get('JMARKETPLACE_MENU_TOP');
        $helper->fields_value['JMARKETPLACE_MENU_OPTIONS'] = Configuration::get('JMARKETPLACE_MENU_OPTIONS');

        return $helper->generateForm($this->fields_form);
    }
    
    private function displayFormPayments() 
    {
        if (version_compare(_PS_VERSION_, '1.6', '<'))
            $type = 'radio';
        else
            $type = 'switch';

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language)
            $languages[$k]['is_default'] = (int)$language['id_lang'] == Configuration::get('PS_LANG_DEFAULT');

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $helper->allow_employee_form_lang = true;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'submitPayments';

        $this->fields_form[0]['form'] = array(
            'tinymce' => false,
            'legend' => array(
                'title' => $this->l('Seller payment')
            ),
            'input' => array(  
                array(
                    'type' => $type,
                    'label' => $this->l('Paypal'),
                    'name' => 'JMARKETPLACE_PAYPAL',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('Pay your sellers with Paypal'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => $type,
                    'label' => $this->l('Bankwire'),
                    'name' => 'JMARKETPLACE_BANKWIRE',
                    'required' => false,
                    'is_bool' => true,
                    'class' => 't',
                    'desc' => $this->l('Pay your sellers with Bankwire'),
                    'values' => array(
                        array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                        ),
                        array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                        )
                    ),
                ),    
            ),
            'submit' => array(
                'name' => 'submitPayments',
                'title' => $this->l('Save'),
            ),
        );
        
        $helper->fields_value['JMARKETPLACE_PAYPAL'] = Configuration::get('JMARKETPLACE_PAYPAL');
        $helper->fields_value['JMARKETPLACE_BANKWIRE'] = Configuration::get('JMARKETPLACE_BANKWIRE');

        return $helper->generateForm($this->fields_form);
    }
    
    public function hookDisplayCustomerAccount($params) 
    {
        $customer_can_be_seller = false;
        $customer_groups = Customer::getGroupsStatic($this->context->cookie->id_customer);

        foreach ($customer_groups as $id_group) {
            if (Configuration::get('JMARKETPLACE_CUSTOMER_GROUP_'.$id_group) == 1)
                $customer_can_be_seller = true;
        }
        
        $ps_version = $this->getPrestaShopVersion();
        $this->context->smarty->assign('ps_version', $ps_version);
        
        $this->context->smarty->assign(array(
            'is_seller' => Seller::isSeller($this->context->cookie->id_customer, $this->context->shop->id),
            'is_active_seller' => Seller::isActiveSellerByCustomer($this->context->cookie->id_customer),
            'customer_can_be_seller' => $customer_can_be_seller,
            'id_default_group' => $this->context->customer->id_default_group,
            'show_contact' => Configuration::get('JMARKETPLACE_SHOW_CONTACT'),
            'show_seller_favorite' => Configuration::get('JMARKETPLACE_SELLER_FAVORITE'),
            'ssl_enabled' => Configuration::get('PS_SSL_ENABLED')
        ));

        return $this->display(__FILE__, 'customer-account.tpl');
    }
    
    public function hookDisplayProductButtons($params) 
    {
        $id_product = (int)Tools::getValue('id_product');
        $id_seller = SellerProduct::isSellerProduct($id_product);
        $seller = new Seller($id_seller);
        
        $params = array('id_seller' => $id_seller, 'id_product' => $id_product);	
        $params_seller_profile = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);
        $url_seller_comments = $this->context->link->getModuleLink('jmarketplace', 'sellercomments', $params, true);	
	$url_contact_seller = $this->context->link->getModuleLink('jmarketplace', 'contactseller', $params, true);			
        $url_favorite_seller = $this->context->link->getModuleLink('jmarketplace', 'favoriteseller', $params, true);        
        $url_seller_profile = Jmarketplace::getJmarketplaceLink('jmarketplace_seller_rule', $params_seller_profile);
        $url_seller_products = Jmarketplace::getJmarketplaceLink('jmarketplace_sellerproductlist_rule', $params_seller_profile);

        $this->context->smarty->assign(array(
            'url_contact_seller' => $url_contact_seller,
            'seller_link' => $url_seller_profile,
            'url_seller_comments' => $url_seller_comments,
            'url_seller_products' => $url_seller_products,
            'url_favorite_seller' => $url_favorite_seller,
            'is_product_seller' => $id_seller,
            'show_contact_seller' => Configuration::get('JMARKETPLACE_SHOW_CONTACT'),
            'show_seller_profile' => Configuration::get('JMARKETPLACE_SHOW_PROFILE'),
            'show_seller_favorite' => Configuration::get('JMARKETPLACE_SELLER_FAVORITE'),
            'show_seller_rating' => Configuration::get('JMARKETPLACE_SELLER_RATING'),
            'seller' => $seller,
            
        ));
        
        if(Configuration::get('JMARKETPLACE_SELLER_RATING')) {
            $average = SellerComment::getRatings($id_seller);
            $averageTotal = SellerComment::getCommentNumber($id_seller);
            
            $this->context->smarty->assign(array(
                'averageTotal' => (int)$averageTotal,
                'averageMiddle' => ceil($average['avg']),
            ));       
        }
        
        //return $this->display(__FILE__, 'product-buttons.tpl');
    }
    
    public function hookDisplayProductListReviews($params)
    {
        /*
		$id = 0;
		if(isset($params['product']['seller']['id'])) $id = $this->getCacheId().'|'.$params['product']['seller']['id'];
		
		if (!$this->isCached('product-list.tpl', $id))
		{
			
			$id_product = (int)$params['product']['id_product'];
			$id_seller = SellerProduct::isSellerProduct($id_product);

			if ($id_seller > 0 && Configuration::get('JMARKETPLACE_SHOW_SELLER_PLIST') == 1) {
				$seller = new Seller($id_seller);
				$param = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);			
				$url_seller_profile = $this->getJmarketplaceLink('jmarketplace_seller_rule', $param);

				$this->context->smarty->assign(array(
					'seller' => $seller,
					'seller_link' => $url_seller_profile,
					'product' => $params['product']
				));

			}
		}
		
		return $this->display(__FILE__, 'product-list.tpl',$id);
		*/
		if (Configuration::get('JMARKETPLACE_SHOW_PROFILE') == 1) {
            $id_product = (int)$params['product']['id_product'];
            $id_seller = SellerProduct::isSellerProduct($id_product);

            if ($id_seller > 0 && Configuration::get('JMARKETPLACE_SHOW_SELLER_PLIST') == 1) {
                $seller = new Seller($id_seller);
                $param = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);			
                $url_seller_profile = $this->getJmarketplaceLink('jmarketplace_seller_rule', $param);

                $this->context->smarty->assign(array(
                    'seller' => $seller,
                    'seller_link' => $url_seller_profile,
					'product' => $params['product']
                ));

                return $this->display(__FILE__, 'product-list.tpl');
            }
        }
    }
    
    public function hookDisplayFooter($params)
    {
        if (Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS') == 1 || Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER') == 1) {
            $this->context->smarty->assign('PS_REWRITING_SETTINGS', Configuration::get('PS_REWRITING_SETTINGS'));
            return $this->display(__FILE__, 'footer.tpl');
        }
            
    }
    
    public function sendCommision($params) 
    {     
        if (Validate::isLoadedObject($params['newOrderStatus']))
            $id_order = $params['id_order'];
        else
            $id_order = $params['order']->id;
        
        $order = new Order($id_order);
        
        //mirar si el pedido tiene comisiones
        $order_has_commissions = SellerCommisionHistory::getCommisionHistoryByOrder($id_order, $this->context->language->id, $this->context->shop->id);
        if ($order_has_commissions) {
            //reactivar comisiones
            SellerCommisionHistory::changeStateCommissionsByOrder($id_order, 'pending');
        }
        else {
            $this->createCommissionsForProducts($order);
        }  
    }
    
    public function createCommissionsForProducts($order) {
        $products = $order->getProducts();
        foreach ($products as $p) {
            $id_seller = Seller::getSellerByProduct($p['product_id']);            
            if ($id_seller) {
                $seller = new Seller($id_seller);
                if ($seller->active == 1) {
                    //si quiere que el vendedor asuma la tasa de impuestos
                    if (Configuration::get('JMARKETPLACE_TAX_COMMISSION') != 0) {
                        $unit_price = $p['unit_price_tax_incl'];
                        $total_price = $p['total_price_tax_incl'];
                    }
                    else {
                        $unit_price = $p['unit_price_tax_excl'];
                        $total_price = $p['total_price_tax_excl'];
                    }
                    
                    $sch = new SellerCommisionHistory();
                    $sch->id_order = $order->id;
                    $sch->id_product = $p['product_id'];
                    $sch->product_name = $p['product_name'];
                    $sch->id_seller = $id_seller;
                    $sch->id_shop = $this->context->shop->id;
                    $sch->price = $unit_price;
                    $sch->quantity = (int)$p['product_quantity'];
                    
                    $commision = (float)SellerCommision::getCommisionBySeller($id_seller);

                    $sch->commision = (float)($total_price * $commision) / 100;
                    
                    $sch->id_seller_commision_history_state = SellerCommisionHistoryState::getIdByReference('pending');
                    $sch->add(); 
					
                    $this->createFixedCommission($order, $id_seller);                 
                    //$this->notifyNewOrderToSeller($order, $seller, $sch->id_product);   
                }
            }  
        } 
        
        $this->createCommissionForShipping($order, $id_seller);  
        $this->createCommissionForDiscounts($order, $id_seller);
    }
    
    public function notifyNewOrderToSeller($order, $seller, $id_product) {
        if (Configuration::get('JMARKETPLACE_SEND_PRODUCT_SOLD') == 1) {
            $product = new Product($id_product, false, $seller->id_lang, $this->context->shop->id);
            $id_seller_email = false;
            $to = $seller->email;
            $to_name = $seller->name;
            $from = Configuration::get('PS_SHOP_EMAIL');
            $from_name = Configuration::get('PS_SHOP_NAME');
            
            $template = 'base';
            $reference = 'new-order';
            $id_seller_email = SellerEmail::getIdByReference($reference);

            if ($id_seller_email) {
                $seller_email = new SellerEmail($id_seller_email, $seller->id_lang);
                $vars = array("{shop_name}", "{seller_name}", "{product_name}", "{order_reference}");
                $values = array(Configuration::get('PS_SHOP_NAME'), $seller->name, $product->name, $order->reference);
                $subject_var = $seller_email->subject; 
                $subject_value = str_replace($vars, $values, $subject_var);
                $content_var = $seller_email->content;
                $content_value = str_replace($vars, $values, $content_var);

                $template_vars = array(
                    '{content}' => $content_value,
                    '{shop_name}' => Configuration::get('PS_SHOP_NAME')
                );

                $iso = Language::getIsoById($seller->id_lang);

                if (file_exists(dirname(__FILE__).'/mails/'.$iso.'/'.$template.'.txt') && file_exists(dirname(__FILE__).'/mails/'.$iso.'/'.$template.'.html')) {
                    Mail::Send(
                        $seller->id_lang,
                        $template,
                        $subject_value,
                        $template_vars,
                        $to,
                        $to_name,
                        $from,
                        $from_name,
                        null,
                        null,
                        dirname(__FILE__).'/mails/'
                    );
                }
            }
        }
    }
    
    public function createCommissionForShipping($order, $id_seller) {
		if (Configuration::get('JMARKETPLACE_TAX_COMMISSION'))
			$total_shipping = $order->total_shipping_tax_incl;
		else
			$total_shipping = $order->total_shipping_tax_excl;
			
        if (Configuration::get('JMARKETPLACE_SHIPPING_COMMISSION') == 1 && $order->total_shipping_tax_excl > 0) { 
            
            
            $sch = new SellerCommisionHistory();
            $sch->id_order = $order->id;
            $sch->id_product = 0;
            $sch->product_name = $this->l('Shipping cost for').' '.$order->reference;
            $sch->id_seller = $id_seller;
            $sch->id_shop = $this->context->shop->id;
            $sch->price = $total_shipping;
            $sch->quantity = 1;
            $sch->commision = $total_shipping;
            $sch->id_seller_commision_history_state = SellerCommisionHistoryState::getIdByReference('pending');
            $sch->add(); 
        }
		
		if($order->id_carrier && $order->total_price_tax_incl<=80)
		{
			
			$carrier = new Carrier($order->id_carrier);
			if($carrier->id_reference==89){
				$sch = new SellerCommisionHistory();
				$sch->id_order = $order->id;
				$sch->id_product = 0;
				$sch->product_name = $this->l('Shipping cost for').' '.$order->reference;
				$sch->id_seller = $id_seller;
				$sch->id_shop = $this->context->shop->id;
				$sch->price = $order->total_shipping_tax_excl*0.85;
				$sch->quantity = 1;
				$sch->commision = $order->total_shipping_tax_excl*0.85;
				$sch->id_seller_commision_history_state = SellerCommisionHistoryState::getIdByReference('pending');
				$sch->add(); 
								
			}
			
			
		}
    }
    
    public function createFixedCommission($order, $id_seller) {
        if (Configuration::get('JMARKETPLACE_FIXED_COMMISSION') > 0) {        
            if (!SellerCommisionHistory::getFixedCommissionOfSellerInOrder($id_seller, $order->id)) {
                $sch = new SellerCommisionHistory();
                $sch->id_order = $order->id;
                $sch->id_product = 0;
                $sch->product_name = $this->l('Fixed commission for sale').' '.$order->reference;
                $sch->id_seller = $id_seller;
                $sch->id_shop = $this->context->shop->id;
                $sch->price = -Configuration::get('JMARKETPLACE_FIXED_COMMISSION');
                $sch->quantity = 1;
                $sch->commision = -Configuration::get('JMARKETPLACE_FIXED_COMMISSION');
                $sch->id_seller_commision_history_state = SellerCommisionHistoryState::getIdByReference('pending');
                $sch->add(); 
            }
        }
    }
    
    public function createCommissionForDiscounts($order, $id_seller) {
        if ($order->total_discounts > 0) {             
            $sch = new SellerCommisionHistory();
            $sch->id_order = $order->id;
            $sch->id_product = 0;
            $sch->product_name = $this->l('Total discounts').' '.$order->reference;
            $sch->id_seller = $id_seller;
            $sch->id_shop = $this->context->shop->id;
            $sch->price = -$order->total_discounts;
            $sch->quantity = 1;
            //$sch->commision = -$order->total_discounts;
			//Nico : on passe la commission a 0 lorsque discount
			$sch->commision = 0;
            $sch->id_seller_commision_history_state = SellerCommisionHistoryState::getIdByReference('pending');
            $sch->add(); 
        }
    }
    
    public function hookActionValidateOrder($params) 
    {
        
		if (Configuration::get('JMARKETPLACE_COMMISIONS_ORDER') == 1) {
            $this->sendCommision($params);
        }
		
		//Modification Nico : pour avertir le vendeur au moment de la commande (et non au moement du calcul des commissions)

        $order = $params['order'];
		$id_order = $order->id;
		$cart = $params['cart'];
		
		$products = $order->getProducts();
		
		foreach ($products as $p) {
            
			$id_seller = Seller::getSellerByProduct($p['product_id']); 
			
            if ($id_seller && (int)!$cart->id_pointshop) {
                $seller = new Seller($id_seller);	
				$this->notifyNewOrderToSeller($order, $seller, $p['product_id']);
			}
			//break;
			
		}
		
		
		if(Module::isEnabled('acaisse')){
				
			//Si le pointshop n'est pas déjà défini
			
			if(!$order->id_pointshop && $id_seller){
			
				$pointshops =  $seller->pointshops;
				$arrPointshop = explode(";",$pointshops);
			
				if($seller->pointshops && is_array($arrPointshop)){

					//A améliorer
					//$order->id_pointshop = (int)$arrPointshop[0];
					Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'orders` SET `id_pointshop` = '.(int)$arrPointshop[0].',id_ticket=1 WHERE id_order = '.$id_order);
				
				}
			}
		} 
	
						
		
		
		
    }
    
    public function hookActionOrderStatusPostUpdate($params) 
    {
        $states = OrderState::getOrderStates($this->context->language->id);
       
        $create_commissions = false;
        foreach ($states as $state) {
            if (Configuration::get('JMARKETPLACE_ORDER_STATE_'.$state['id_order_state']) == 1 && $params['newOrderStatus']->id == $state['id_order_state'])
                $create_commissions = true;
        }
        
        if ($create_commissions) {
            $this->sendCommision($params);
        }
        else {
			
			
			
            //update history commissions
            $cancel_commissions = false;
			$recreated_commission = false;
            foreach ($states as $state) {
                if (Configuration::get('JMARKETPLACE_CANCEL_COMMISSION_'.$state['id_order_state']) == 1 && $params['newOrderStatus']->id == $state['id_order_state'])
                    $cancel_commissions = true;
				
            }

			
			
            //si toca cancelar comisiones
            if ($cancel_commissions) 
                SellerCommisionHistory::changeStateCommissionsByOrder($params['id_order'], 'cancel');
			else
			{
				
				//Modification nico
				//On supprime les commissions pour les recréer (si créée), cela permet de gérer la modification des commandes
				$isCreated = SellerCommisionHistory::deleteCommissionsByOrder($params['id_order']);
				if($isCreated) $this->sendCommision($params);
				
			}
        }
		
		$order = new Order((int)$params['id_order']);
		
		//Nico : si le mail de status en active
		if (Configuration::get('JMARKETPLACE_SEND_ADMIN_STATUS_UPDATE') == 1) {
			
			$steps = array(2,3,25,4,26,5,39);
			$order_state = $params['newOrderStatus'];
			
			//Si produit vendu en boutique directement, pas de mail
			if($order->id_pointshop || !$order->current_state) return;
			if(!in_array($order_state,$steps)) return;
			
			
			//Nico
			//On fait un mail a ditwee
			$template = 'base';
			$subject_value = sprintf($this->l('Order %s : Status update : %s'), $order->reference,$order_state->name);
			$template_vars = array(
				'{content}' => sprintf($this->l('The status of order %s has been updated. New status : %s'), $order->reference,$order_state->name),
				'{shop_name}' => Configuration::get('PS_SHOP_NAME')
			);
			$to = Configuration::get('JMARKETPLACE_SEND_ADMIN');
			$to_name = Configuration::get('PS_SHOP_NAME');
			$from = Configuration::get('PS_SHOP_EMAIL');
			$from_name = Configuration::get('PS_SHOP_NAME');
			
			$iso = Language::getIsoById(Configuration::get('PS_LANG_DEFAULT'));
			if (file_exists(dirname(__FILE__).'/mails/'.$iso.'/'.$template.'.txt') && file_exists(dirname(__FILE__).'/mails/'.$iso.'/'.$template.'.html')) {	
				Mail::Send(
					Configuration::get('PS_LANG_DEFAULT'),
					$template,
					$subject_value,
					$template_vars,
					$to,
					$to_name,
					$from,
					$from_name,
					null,
					null,
					dirname(__FILE__).'/../../mails/'
				);
			}
			
		}
		/*
		//Si DHL est activé
		if(Module::isEnabled('dhlexpress')){
			
			require_once(_PS_MODULE_DIR_.'dhlexpress/dhlexpress.php');
			$dhl = new Dhlexpress();

			$products = $order->getProducts();
			$p = reset($products);
			$id_seller = Seller::getSellerByProduct($p['product_id']); 
			$seller = new Seller($id_seller);
		
			$carrier = new Carrier((int) $order->id_carrier);
			$idCarrierReference = $carrier->id_reference;
			
			$idSenderAddress = $seller->id_dhl_address;
			$senderAddress = new DhlAddress((int) $idSenderAddress);
			
			//Si la commande est en DHL et que le vendeur a bien une adresse DHL associée
			
			if (Validate::isLoadedObject($order) && 
				$order->current_state != Configuration::get('PS_OS_ERROR') && 
				$seller->id_dhl_address &&
				Validate::isLoadedObject($senderAddress) &&
				DhlService::isDhlCarrier($idCarrierReference)
				
				) {
				
				//Si la commande est en statut paiement accepte, on fait partir l'etiquette au vendeur
				if($order->current_state == 2){
				
					$dhlPackage = new DhlPackage((int)Configuration::get('DHL_DEFAULT_PACKAGE_TYPE'));
					$sep = "x";
					$formatedDimensions = Configuration::get('DHL_DEFAULT_PACKAGE_TYPE').
						$sep.
						$dhlPackage->weight_value.
						$sep.
						$dhlPackage->length_value.
						$sep.
						$dhlPackage->width_value.
						$sep.
						$dhlPackage->depth_value;
					
					
					$_POST = array();
					   
					$_POST['dhl_label_id_order'] = $order->id;
					//A Améliorer
					$_POST['dhl_label_currency_iso'] = "EUR";
					$_POST['dhl_sender_address'] = $idSenderAddress ;
					$_POST['dhl_customer_address'] = $order->id_address_delivery;
					$_POST['dhl_label_package_type'] = Configuration::get('DHL_DEFAULT_PACKAGE_TYPE');
			
					$_POST['package_dimensions'] =  array($formatedDimensions) ;
					$_POST['dhl_reference_id'] = $order->reference ;
					$_POST['dhl_label_contents'] = Configuration::get('DHL_DEFAULT_SHIPMENT_CONTENT');
					
					$_POST['dhl_sending_doc'] = Configuration::get('DHL_SENDING_DOC');
				
					$_POST['dhl_label_declared_value'] = "";
					$_POST['dhl_label_insured_value'] = "" ;
					$_POST['dhl_label_service'] = "N";
					
					//Nico : a continuer
					//$this->generateDhlLabel();
					
				}
				
				//Si la commande est en statut 'commande préparée pour la livraison' on fait partir la demande d'enlevement
				if($order->current_state == 25){
				
					
					$_POST = array();
					
					$seller_working_day = $seller->formatWorkingHours();
	
					//On ajoutera 1 jour à l'enlevement : pour le lendemain
					$add_day = 1;
					$week_day = date('N');
					//Si vendredi : on ajoutera 2 jours à l'enlevement pour qu'il ne tombe pas le samedi ni le dimanche
					if($week_day==5)
						$add_day=$add_day+2;
					//Si samedi : on ajoutera 1 jours à l'enlevement pour qu'il ne tombe pas le dimanche
					if($week_day==6)
						$add_day=$add_day+1;
					
					$start_day = $add_day;
					$is_ok = false;
					//On cherche une date ok pour l'enlevement
					while(!$is_ok && $add_day<8){
						
						$test_date = date('Y-m-d',strtotime('now +'.$add_day.' days'));

						$day_test_date = date('N',$test_date)-1;
	
						if($seller_working_day[$day_test_date]->isActive){
							$is_ok = true;
							
							$timeClose = explode(':',$seller_working_day[$day_test_date]->timeTill);
							$timePick = explode(':',$seller_working_day[$day_test_date]->timeFrom);
							if($seller_working_day[$day_test_date]->isBreak){
								$timeClose = explode(':',$seller_working_day[$day_test_date]->timeTill2);
							}
							
							$_POST['dhl_closing_time_hour'] = $timeClose[0];
							$_POST['dhl_closing_time_minute'] = $timeClose[1];
							$_POST['dhl_request_time_hour'] = (int)$timePick[0]+1;
							$_POST['dhl_request_time_minute'] = (int)$timePick[1]+30;	
								
								
						}
						
						$add_day++;
						
					}
					
					if(!$is_ok){
						$test_date = date('Y-m-d',strtotime('now +'.$start_day.' days'));
						$_POST['dhl_closing_time_hour'] = "18";
						$_POST['dhl_closing_time_minute'] = "00";
						$_POST['dhl_request_time_hour'] = "10";
						$_POST['dhl_request_time_minute'] = "30";
					}
					
					$_POST['dhl_pickup_location'] = 'Accueil';
					$_POST['dhl_sender_address'] = $idSenderAddress;
					$_POST['dhl_pickup_contact'] = $senderAddress->contact_name;
					$_POST['dhl_pickup_phone'] = $senderAddress->contact_phone;
					$_POST['dhl_pickup_date'] = $test_date;
					$_POST['dhl_pickup_weight'] = 1;
					$_POST['dhl_pickup_packages'] = 1;
					$_POST['dhl_pickup_instructions'] = $order->reference;

				}
			}
            
		}
		*/
		
    }
    
    public function hookActionProductDelete($params) 
    {
        if (Tools::getValue('deleteproduct'))
            $id_product = (int)Tools::getValue('id_product');
        else
            $id_product = (int)$params['id_product'];
            
        Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'seller_product` WHERE id_product = '.$id_product); 
    }
    
    public function hookDisplayHeader($params) 
    {       
        //$this->context->controller->addCSS($this->_path.'views/css/font-awesome.css', 'all');
        $this->context->controller->addCSS($this->_path.'views/css/jmarketplace.css', 'all');
        
        if (Configuration::get('JMARKETPLACE_THEME') == 'default') 
            $this->context->controller->addCSS($this->_path.'views/css/default.css', 'all');
        
        if (Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS') == 1 || Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER') == 1) {
            /*if (version_compare(_PS_VERSION_, '1.7', '>=')) 
                $this->context->controller->registerJavascript('modules-jmarketplace', 'modules/'.$this->name.'/views/js/addsellerproductcart.js', ['position' => 'bottom', 'priority' => 150]);
            else*/
                //$this->context->controller->addJS($this->_path.'views/js/addsellerproductcart.js', 'all');
        }
		
		return $this->hookDisplayFooter($params);
    }
    
    public function hookBackOfficeHeader() 
    {
        $this->context->controller->addCSS($this->_path.'views/css/back.css');
    }
    
    public function hookadminOrder($params)
    {
        $is_order_seller = false;
        $id_order = $params['id_order'];
        $order = new Order($id_order);
        $products = $order->getProducts();
        foreach ($products as $p) {
            if (SellerProduct::existAssociationSellerProduct($p['id_product']))
                $is_order_seller = true;
        }
        
        $commissions = SellerCommisionHistory::getCommisionHistoryByOrder($id_order, $this->context->language->id, $this->context->shop->id);
        $total_commissions = SellerCommisionHistory::getTotalCommissionByOrder($id_order);
        echo '<div class="panel col-lg-12">';
        
        if (is_array($commissions)) {
            echo '<div class="panel-heading"><i class="icon-user"></i> '.$this->l('Information on the commissions to pay the seller').'</div>';
            echo '<div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><span class="title_box">'.$this->l('Concept').'</span></th>
                                <th><span class="title_box">'.$this->l('Seller').'</span></th>
                                <th class="text-right"><span class="title_box">'.$this->l('Quantity').'</span></th>
                                <th class="text-right"><span class="title_box">'.$this->l('Price').'</span></th>
                                <th class="text-right"><span class="title_box">'.$this->l('Commission').'</span></th>
                                <th class="text-center"><span class="title_box">'.$this->l('Status').'</span></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>';
            foreach ($commissions as $c) {
                $seller = new Seller($c['id_seller']);
                echo ' <tr>
                            <td>'.$c['product_name'].'</td>
                            <td>'.$seller->name.'</td>
                            <td class="text-right">'.$c['quantity'].'</td> 
                            <td class="text-right">'.Tools::displayPrice($c['price']).'</td>   
                            <td class="text-right">'.Tools::displayPrice($c['commision']).'</td>
                            <td class="text-center">'.$c['state_name'].'</td>
                            <td class="actions">
                                    <a href="index.php?tab=AdminSellerCommisionsHistory&id_seller_commision_history='.(int)$c['id_seller_commision_history'].'&updateseller_commision_history&token='.Tools::getAdminToken('AdminSellerCommisionsHistory'.(int)Tab::getIdFromClassName('AdminSellerCommisionsHistory').(int)$this->context->employee->id).'" class="btn btn-default">
                                            <i class="icon-search"></i>
                                            Details
                                    </a>
                            </td>
                        </tr>';
            }

            echo '</tbody>';
            
            echo '<tfoot>
                        <tr>
                            <th colspan="4"><span class="title_box">'.$this->l('Total').'</span></th>
                            <th class="text-right">'.Tools::displayPrice($total_commissions).'</th>
                        </tr>
                    </tfoot>
                </table>
            </div>';
        }
        else if ($is_order_seller && !$order->id_pointshop) {
            echo '<div class="panel-heading"><i class="icon-user"></i> '.$this->l('Seller order information').'</div>';
            echo '<div class="table-responsive">
                   <table class="table">
                       <thead>
                           <tr>
                               <th><span class="title_box">'.$this->l('Concept').'</span></th>
                               <th><span class="title_box">'.$this->l('Seller').'</span></th>
                               <th class="text-right"><span class="title_box ">'.$this->l('Quantity').'</span></th>
                               <th class="text-right"><span class="title_box ">'.$this->l('Price').'</span></th> 
                               <th class="text-right"><span class="title_box">'.$this->l('Commission').'</span></th>
                           </tr>
                       </thead>
                       <tbody>';
            foreach ($products as $p) {
                $id_seller_product = SellerProduct::existAssociationSellerProduct($p['id_product']);
                $product = new Product($p['id_product']);
                $owner = Configuration::get('PS_SHOP_NAME');
                
                if (Configuration::get('JMARKETPLACE_TAX_COMMISSION') == 1)
                    $product_price = $product->getPrice(true);
                else
                    $product_price = $product->getPrice(false);
                
                $owner_commission = $p['product_price'];
                if ($id_seller_product) {
                    $seller = new Seller($id_seller_product);
                    $owner = $seller->name;
                    $owner_commission = (SellerCommision::getCommisionBySeller($seller->id) * $product_price) / 100;
                }
                    
                echo ' <tr>
                            <td>'.$p['product_name'].'</td>
                            <td>'.$owner.'</td>
                            <td class="text-right">'.$p['product_quantity'].'</td> 
                            <td class="text-right">'.Tools::displayPrice($product_price).'</td>  
                            <td class="text-right">'.Tools::displayPrice($owner_commission).'</td>
                        </tr>';
            }
            
            echo '</tbody></table></div>';
        }
        else {
            //echo '<div class="panel-heading"><i class="icon-user"></i> '.$this->l('Seller order information').'</div>';
            //echo '<div class="alert alert-info">'.$this->l('This order is by no seller').'</div>';
        }
        
        echo '</div>';
		echo '<div class="clearfix"></div>';
		
    }
    
    public function hookModuleRoutes($params) 
    {
        $my_link = array(
            'jmarketplace_seller_rule' => array(
                'controller' => 'sellerprofile',
                'rule' =>       $this->l('jmarketplace').'/{id_seller}_{link_rewrite}',
                'keywords' => array(
                    'id_seller' =>  array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'id_seller'),
                    'link_rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'jmarketplace',
                ),
            ),
            'jmarketplace_sellerproductlist_rule' => array(
                'controller' => 'sellerproductlist',
                'rule' =>       $this->l('jmarketplace').'/{id_seller}_{link_rewrite}/'.$this->l('products'),
                'keywords' => array(
                    'id_seller' =>  array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'id_seller'),
                    'link_rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'jmarketplace',
                ),
            ),
            'jmarketplace_sellers_rule' => array(
                'controller' => 'sellers',
                'rule' =>       $this->l('jmarketplace').'/'.$this->l('sellers'),
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'jmarketplace',
                ),
            ),
        );

        return $my_link;

    }
    
    public static function getJmarketPlaceUrl() 
    {
        $ssl_enable = Configuration::get('PS_SSL_ENABLED');
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $rewrite_set = (int)Configuration::get('PS_REWRITING_SETTINGS');
        $ssl = null;
        static $force_ssl = null;
        
        if ($ssl === null)  
        {
            if ($force_ssl === null)
                $force_ssl = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'));

            $ssl = $force_ssl;
        }
        
        if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') && $id_shop !== null)
            $shop = new Shop($id_shop);
        else
            $shop = Context::getContext()->shop;

        $base = (($ssl && $ssl_enable) ? 'https://'.$shop->domain_ssl : 'http://'.$shop->domain);   
        $langUrl = Language::getIsoById($id_lang).'/';
        
        if ((!$rewrite_set && in_array($id_shop, array((int)Context::getContext()->shop->id,  null))) || 
                !Language::isMultiLanguageActivated($id_shop) || 
                !(int)Configuration::get('PS_REWRITING_SETTINGS', null, null, $id_shop))
            $langUrl = '';

        return $base.$shop->getBaseURI().$langUrl;
    }
    
    public static function getJmarketplaceLink($rewrite = 'jmarketplace', $params = null, $id_lang = null) 
    {
        $url = Jmarketplace::getJmarketPlaceUrl();
        $dispatcher = Dispatcher::getInstance();
        
        if($params != null)
            return $url.$dispatcher->createUrl($rewrite, $id_lang, $params);
        else
            return $url.$dispatcher->createUrl($rewrite); 
    }
    
    public function getPrestaShopVersion() {
        if (version_compare(_PS_VERSION_, '1.6', '<') && version_compare(_PS_VERSION_, '1.5', '>=')) 
            return '1.5';
        
        if (version_compare(_PS_VERSION_, '1.7', '<') && version_compare(_PS_VERSION_, '1.6', '>=')) 
            return '1.6';
        
        if (version_compare(_PS_VERSION_, '1.7', '>=')) 
            return '1.7';
        
        return false;
    }   

	public function hookdisplayProductTab($params)
	{
		//$this->SizeChartValues("displayProductTabContent");
		return $this->display(__FILE__, 'product-tab.tpl');
	}
	public function hookdisplayProductTabContent($params)
	{
		//$this->SizeChartValues("displayProductTabContent");
		return $this->display(__FILE__, 'product-tab-content.tpl');
	}	
	
	
	public function hookActionProductSave($params) {
		
		//Si l'user connecté est un vendeur de la marketplace
		//FRONTOFFICE
		$id_seller = Seller::getSellerByCustomer((int)$this->context->cookie->id_customer);
		
		//Si l'employee connecté est un vendeur de la marketplace
		//BACKOFFICE
		if(!$id_seller)
			$id_seller = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		
		$id_product = $params['id_product'] ;
	
		if($id_seller && $id_product){
			
			$is_already_associated = SellerProduct::existAssociationSellerProduct($id_product);
			if(!$is_already_associated)
				SellerProduct::associateSellerProduct($id_seller, $id_product);
		
		}
		
		if(Module::isEnabled('acaisse'))
			SellerProduct::setPointshop($id_product);
		
	}
	
	public function hookActionObjectProductUpdateAfter($params){
		
		$id_seller = Seller::getSellerByCustomer((int)Context::getContext()->cookie->id_customer);
		
	
		//Si l'employee connecté est un vendeur de la marketplace
		//BACKOFFICE
		if(!$id_seller){
			$id_seller = Employee::getAssociatedSeller((int)Context::getContext()->employee->id,(int)Context::getContext()->shop->id);
		}
		
		$id_product = $params['object']->id ;
	
	
		if($id_seller && $id_product){
			
			$is_already_associated = SellerProduct::existAssociationSellerProduct($id_product);
			if(!$is_already_associated)
				SellerProduct::associateSellerProduct($id_seller, $id_product);
		
		}
		
	}
	
	public function sendLabelDHLToSeller($order, $dhlLabel){
		
		$order_seller = SellerOrder::getOrderBySeller($order->id, $order->id_lang);

		$seller = new Seller((int)$order_seller[0]['id_seller']);

		if($seller->id){
			
			if (!Validate::isLoadedObject($dhlLabel)) {
				die($this->l('Invalid Label.'));
			}
			$labelType = $dhlLabel->label_format;
			$decoded = base64_decode($dhlLabel->label_string);
			if (in_array($labelType, array('pdf', 'zpl', 'epl'))) {
				$file = $dhlLabel->awb_number.'.'.$labelType;
			} else {
				die($this->l('Invalid Label.'));
			}
			file_put_contents($file, $decoded);
			
			$fileAttachment['content'] = file_get_contents($file); //File path
			$fileAttachment['name'] = $file; //Attachment filename
			$fileAttachment['mime'] = 'application/pdf'; //mime file type

			$customer = new Customer((int) $order->id_customer);
			$templateVars = array(
				'{followup}'        => 'tet',
				'{firstname}'       => $customer->firstname,
				'{lastname}'        => $customer->lastname,
				'{name}'        => $seller->name,
				'{id_order}'        => $order->id,
				'{shipping_number}' => $awbNumber,
				'{order_name}'      => $order->getUniqReference(),
			);

			return @Mail::Send(
				(int) $order->id_lang,
				'dhl_seller_label',
				sprintf($this->l('Commande %s - Etiquette enlèvement DHL','AdminDhlLabel'), $order->getUniqReference()),
				$templateVars,
				$seller->email,
				$seller->name,
				null,
				null,
				$fileAttachment,
				null,
				dirname(__FILE__).'/mails/',
				false,
				(int) $order->id_shop,
				Tools::getValue('JMARKETPLACE_SEND_ADMIN')
			);
		}
	}

	
	
	
}
?>
