[/{shop_url}]

Bonjour {name},

Félicitations pour cette belle vente sur Ditwee.

Vous trouverez ci-joint l’étiquette à coller sur le colis DITWEE {order_name} à l’attention de {firstname} {lastname}

N’oubliez pas que le packaging retranscrira l'image de votre marque, de votre point de vente, et sera un vrai sujet de satisfaction et de ventes additives.  
Un bon packaging communique une image rassurante et de qualité à notre client !

Nous vous souhaitons une belle journée et restons à votre disposition.

{shop_name} [/{shop_url}] réalisé avec amour