{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="panel">
    <h3><i class="icon icon-user"></i> {$name|escape:'html':'UTF-8'} {l s='version' mod='jmarketplace'} {$version|escape:'html':'UTF-8'}</h3>
    <p>
        {l s='This module allows you to incorporate a marketplace in your shop to offer to customers the possibility to sell their products in exchange for a commission.' mod='jmarketplace'}<br /><br/>
        {l s='Thank you very much for installing' mod='jmarketplace'} {$name|escape:'html':'UTF-8'}!<br /><br/>
        {l s='You can configure this module in these tabs and you can manage your content on the JA Marketplace management menu.' mod='jmarketplace'}
    </p>
    <div class="panel-footer">
        <a class="btn btn-default" style="display:none;" href="{$module_dir|escape:'html':'UTF-8'}readme_en.pdf" target="_blank" title="{l s='You can get a PDF documentation to configure this module in English' mod='jmarketplace'}">
            <i class="icon-book"></i> {l s='English' mod='jmarketplace'}
        </a>
        <a class="btn btn-default" style="display:none;" href="{$module_dir|escape:'html':'UTF-8'}readme_es.pdf" target="_blank" title="{l s='You can get a PDF documentation to configure this module in Spanish' mod='jmarketplace'}">
            <i class="icon-book"></i> {l s='Spanish' mod='jmarketplace'}
        </a>
        <a class="btn btn-default" href="https://addons.prestashop.com/{$iso_code|escape:'html':'UTF-8'}/Write-to-developper?id_product=18656" target="_blank">
            <i class="icon-envelope"></i> {l s='Contact with this developer' mod='jmarketplace'}
        </a>
        <a class="btn btn-default" href="http://addons.prestashop.com/{$iso_code|escape:'html':'UTF-8'}/2_community?contributor=343376" target="_blank">
            <i class="icon-eye-open"></i> {l s='View more modules of this developer' mod='jmarketplace'}
        </a>
        <a class="btn btn-default" href="https://addons.prestashop.com/{$iso_code|escape:'html':'UTF-8'}/ratings.php" target="_blank">
            <i class="icon-star"></i> {l s='Rate this module' mod='jmarketplace'}
        </a>
        <a class="btn btn-default" href="{$module_dir|escape:'html':'UTF-8'}changelog.txt" target="_blank">
            <i class="icon-plus"></i> {l s='Changelog' mod='jmarketplace'}
        </a>
    </div>
</div>