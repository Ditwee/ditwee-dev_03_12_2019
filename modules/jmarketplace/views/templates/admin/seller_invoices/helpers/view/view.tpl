{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}
<div class="panel">
    <h3>{l s='Commissions in this transfer request' mod='jmarketplace'}</h3>
    <table class="table">
        <thead>
            <th>{l s='ID Order' mod='jmarketplace'}</th>
            <th>{l s='Order reference' mod='jmarketplace'}</th>
            <th>{l s='Seller name' mod='jmarketplace'}</th>
            <th>{l s='Product name' mod='jmarketplace'}</th>
            <th class="text-right">{l s='Product price' mod='jmarketplace'}</th>
            <th class="text-right">{l s='Product quantity' mod='jmarketplace'}</th>
            <th class="text-right">{l s='Commission' mod='jmarketplace'}</th>
            <th class="text-center">{l s='Payment state' mod='jmarketplace'}</th>
            <th>{l s='Date add' mod='jmarketplace'}</th>
        </thead>
        <tbody>
            {foreach from=$commissions item=commission name=commissions}
                <tr>
                    <td>{$commission.id_order|intval}</td>
                    <td>{$commission.reference|escape:'htmlall':'UTF-8'}</td>
                    <td>{$commission.seller_name|escape:'htmlall':'UTF-8'}</td>
                    <td>{$commission.product_name|escape:'htmlall':'UTF-8'}</td>
                    <td class="text-right">{Tools::displayPrice($commission.price)|escape:'htmlall':'UTF-8'}</td>
                    <td class="text-right">{$commission.quantity|intval}</td>
                    <td class="text-right">{Tools::displayPrice($commission.commision)|escape:'htmlall':'UTF-8'}</td>
                    <td class="text-center">{$commission.state_name|escape:'htmlall':'UTF-8'}</td>
                    <td>{$commission.date_add|escape:'htmlall':'UTF-8'}</td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>
<div class="panel">
    <h3>{l s='Transfer Information'  mod='jmarketplace'}</h3>
    <form action="{$url_post|escape:'html':'UTF-8'}" method="post" class="std">   
        <input type="hidden" name="id_seller_transfer_invoice" value="{$seller_transfer_invoice->id|intval}" />
        <div class="form-group">
            <label for="name">{l s='Seller name' mod='jmarketplace'}</label>
            <input class="form-control" type="text" name="name" id="name" value="{$seller->name|escape:'htmlall':'UTF-8'}" readonly="readonly" />
        </div>
        <div class="form-group">
            <label for="date_request">{l s='Date request payment' mod='jmarketplace'}</label>
            <input class="form-control" type="text" name="date_request" id="date_request" value="{$seller_transfer_invoice->date_add|escape:'htmlall':'UTF-8'}" readonly="readonly" />
        </div>
        <div class="form-group">
            <label for="total">{l s='Total request payment' mod='jmarketplace'}</label>
            <input class="form-control" type="text" name="total" id="total" value="{convertPrice price=$seller_transfer_invoice->total}" readonly="readonly" />
        </div>
        <div class="form-group">
            <a href="{$pdf|escape:'htmlall':'UTF-8'}" target="_blank"><img src="{$pdf_image|escape:'htmlall':'UTF-8'}" /></a>
        </div>
        <div class="form-group">
            <button type="submit" name="submitAcceptedPayment" class="btn btn-default button button-medium">
                <span>{l s='Accept payment' mod='jmarketplace'}<i class="icon-chevron-right right"></i></span>
            </button>
        </div>
    </form>
</div>
{/block}
