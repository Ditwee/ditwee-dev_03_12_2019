{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/form/form.tpl"}

{block name="input"}
    {if $input.type == 'workinghours'}
		<div id="divWorkinghours">
		</div>
		<input type="hidden" value="" name="workinghours" id="workinghours" />
	
	
	<script>
	

	var businessHoursManager = $("#divWorkinghours").businessHours({
		postInit:function(){
			$('.operationTimeFrom, .operationTimeTill').timepicker_for_businessHours({
				'timeFormat': 'H:i',
				'step': 15
				});
		},
		days:{$fields_value['days']},
		{if isset($fields_value[$input.name]) && $fields_value[$input.name]}
		operationTime: {$fields_value[$input.name]},
		{/if}
		dayTmpl:'<div class="dayContainer" style="width: 80px;">' +
			'<div data-original-title="" class="colorBox"><input type="checkbox" class="invisible operationState"></div>' +
			'<div class="weekday"></div>' +
			'<div class="operationDayTimeContainer">' +
			'<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime" class="mini-time form-control operationTimeFrom" value="" ></div>' +
			'<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime" class="mini-time form-control operationTimeTill" value="" ></div>' +
			'<label><input type="checkbox" class="chkBreak">Avec pause</label>' +
			'<div class="divBreak">' +
			'<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime2" class="mini-time form-control operationTimeFrom" value="" ></div>' +
			'<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime2" class="mini-time form-control operationTimeTill" value="" ></div>' +
			'</div></div></div>'
	});

	$('#seller_form').submit(function(e) {
		e.preventDefault();   
		$("#workinghours").val(JSON.stringify(businessHoursManager.serialize()));
		$(this).unbind('submit').submit();
	})
	</script>
	{else}
	    {$smarty.block.parent}
    {/if}
{/block}
{/block}
