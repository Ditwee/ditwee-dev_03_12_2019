{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file="helpers/view/view.tpl"}

{block name="override_tpl"}
<div class="panel">
    <h3>{l s='Seller payment'  mod='jmarketplace'}</h3>
    {foreach from=$payments item=payment name=sellerpayments}
        <div class="form-group{if $payment.active == 0} hidden{/if}">
            {if $payment.payment == 'paypal'}
                <p>{l s='This seller want receive your commissions with Paypal' mod='jmarketplace'}</p>
                <label for="{$payment.payment|escape:'html':'UTF-8'}">{l s='Paypal account (Email)' mod='jmarketplace'}</label>
                <input class="form-control" type="text" name="{$payment.payment|escape:'html':'UTF-8'}" id="{$payment.payment|escape:'html':'UTF-8'}" value="{$payment.account|escape:'html':'UTF-8'}" readonly="readonly" />
            {else if $payment.payment == 'bankwire'}
                <p>{l s='This seller want receive your commissions with Bankwire' mod='jmarketplace'}</p>
                <label for="{$payment.payment|escape:'html':'UTF-8'}">{l s='Number account' mod='jmarketplace'}</label>
                <textarea id="{$payment.payment|escape:'html':'UTF-8'}" name="{$payment.payment|escape:'html':'UTF-8'}" class="form-control" cols="40" rows="4" readonly="readonly">{$payment.account|escape:'html':'UTF-8'}</textarea>   
            {/if}
        </div>
    {/foreach}
    <div class="panel-footer">  
        <a class="btn btn-default" href="{$url_seller_profile|escape:'html':'UTF-8'}" target="_blank">
            <i class="icon-user fa fa-user"></i> {l s='View seller profile' mod='jmarketplace'}
        </a>
        <a class="btn btn-default" href="{$url_seller_products|escape:'html':'UTF-8'}" target="_blank">
            <i class="icon-search fa fa-search"></i> {l s='View all products' mod='jmarketplace'}
        </a>
        <a class="btn btn-default" href="index.php?controller=AdminSellers&amp;token={$token|escape:'html':'UTF-8'}">
            <i class="icon-close fa fa-close"></i> {l s='Cancel' mod='jmarketplace'}
        </a>
    </div>
</div>
<div class="panel">
    <h3>
        {l s='Products'  mod='jmarketplace'} 
        {if $products && count($products)}
            <span class="badge">{count($products)|intval}</span>
        {/if}
    </h3>
    {if $products && count($products)}
        <table id="order-list" class="table table-bordered footab">
            <thead>
                <tr>
                    <th class="first_item" data-sort-ignore="true">{l s='Image'  mod='jmarketplace'}</th>
                    <th class="item">{l s='Product name' mod='jmarketplace'}</th>
                    <th class="item">{l s='Date add' mod='jmarketplace'}</th>
                    <th class="item">{l s='Date update' mod='jmarketplace'}</th>
                    <th class="item">{l s='Status' mod='jmarketplace'}</th>
                    <th class="item">&nbsp;</th>
                    <th class="last_item">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            {foreach from=$products item=product name=sellerproducts}
                <tr>
                    <td class="first_item">
                        {if $product.id_image}
                            <img itemprop="image" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" />
                        {else}
                            <img itemprop="image" src="{$img_prod_dir|escape:'html':'UTF-8'}{$lang_iso|escape:'html':'UTF-8'}-default-small_default.jpg" />
                        {/if}
                    </td>
                    <td class="item"><a href="{$link->getProductLink($product.id_product)|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></td>
                    <td class="item">{$product.date_add|escape:'html':'UTF-8'}</td>
                    <td class="item">{$product.date_upd|escape:'html':'UTF-8'}</td>
                    <td class="item">{if $product.active == 1}{l s='Active' mod='jmarketplace'}{else}{l s='Pending approval' mod='jmarketplace'}{/if}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    {else}
        <p class="alert alert-info">{l s='There are not products.' mod='jmarketplace'} </p>
    {/if}
</div>
{/block}
