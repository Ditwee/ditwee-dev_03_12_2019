{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*} 

{if $show_menu_top == 1}
    {include file="./selleraccount.tpl"}
{/if}

{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {l s='Your account' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <a href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}">
        {l s='Your seller account' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Your shipping and carriers' mod='jmarketplace'}
    </span>
{/capture} 

<div class="row">
    <div class="column col-xs-12 col-sm-3"{if $show_menu_options == 0} style="display:none;"{/if}>
        {include file="./sellermenu.tpl"}
    </div>
    <div class="col-xs-12 col-sm-{if $show_menu_options == 0}12{else}9{/if}">
        {if isset($errors) && $errors}
            {include file="./errors.tpl"}
        {/if}

        <div id="jsellershipping" class="box">
            <h1 class="page-subheading">{l s='Your shipping and carriers' mod='jmarketplace'}</h1>

            {if $carriers && count($carriers) > 0}
                <table id="order-list" class="table table-bordered footab">
                    <thead>
                        <tr>
                            <th class="first_item hidden-xs" data-sort-ignore="true">{l s='Image' mod='jmarketplace'}</th>
                            <th class="item">{l s='Carrier name' mod='jmarketplace'}</th>
                            <th class="item">{l s='Delay' mod='jmarketplace'}</th>
                            <th class="item">{l s='Actions' mod='jmarketplace'}</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach from=$carriers item=carrier name=sellercarriers}
                        <tr>
                            <td class="first_item hidden-xs">
                                {if isset($carrier.logo)}
                                    <img src="{$carrier.logo|escape:'html':'UTF-8'}" width="65" height="65" />
                                {/if}
                            </td>
                            <td class="item">{$carrier.name|escape:'html':'UTF-8'}</td>
                            <td class="item">{$carrier.delay|escape:'html':'UTF-8'}</td>
                            <td class="item">
                                {if $carrier.active == 1}
                                    <a href="{$link->getModuleLink('jmarketplace', 'carriers')|escape:'html':'UTF-8'}?desactivate={$carrier.id_carrier|intval}" title="{l s='Desactivate' mod='jmarketplace'}">
                                        {if $ps_version == '1.7'}
                                            <i class="icon-check fa fa-check"></i>
                                        {else}
                                            <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/status_green.png" />
                                        {/if}
                                    </a>
                                {else}
                                    <a href="{$link->getModuleLink('jmarketplace', 'carriers')|escape:'html':'UTF-8'}?activate={$carrier.id_carrier|intval}" title="{l s='Activate' mod='jmarketplace'}">
                                        {if $ps_version == '1.7'}
                                            <i class="icon-remove fa fa-remove"></i>
                                        {else}
                                            <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/status_red.png" />   
                                        {/if}
                                    </a>
                                {/if}
                                <a class="link-button delete_product" href="{$link->getModuleLink('jmarketplace', 'carriers')|escape:'html':'UTF-8'}?delete={$carrier.id_carrier|intval}" title="{l s='Delete' mod='jmarketplace'}">
                                    {if $ps_version == '1.7'}
                                        <i class="icon-trash fa fa-trash"></i>
                                    {else}
                                        <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/remove.gif" />
                                    {/if}
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            {else}
                <p class="alert alert-info">{l s='There are not carriers.' mod='jmarketplace'}</p><br/>
            {/if}

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group pull-right">
                        <a href="{$link->getModuleLink('jmarketplace', 'addcarrier', array(), true)|escape:'html':'UTF-8'}" class="btn btn-default button button-medium ">
                            <span>{l s='Add new carrier' mod='jmarketplace'}<i class="icon-chevron-right right"></i></span>
                        </a>
                    </div> 
                </div>
            </div>
        </div>
        {include file="./footer.tpl"}
    </div>
</div>