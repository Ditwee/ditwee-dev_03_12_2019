{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {l s='My account' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'htmlall':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Messages' mod='jmarketplace'}
    </span>
{/capture}
{if isset($confirmation) && $confirmation}
    <p class="alert alert-success">{l s='Your issue has been successfully sent.' mod='jmarketplace'}</p>
{else}
    {if isset($errors) && $errors}
        {include file="./errors.tpl"}
    {/if}
{/if}

<div class="box">
    <h1 class="page-subheading">{l s='Messages' mod='jmarketplace'}</h1>
    {if $logged}
        {if $incidences && count($incidences)}
            <table id="order-list" class="table table-bordered footab">
                <thead>
                    <tr>
                        <th class="first_item" data-sort-ignore="true">{l s='Date add' mod='jmarketplace'}</th>
                        <th class="item">{l s='Incidence reference' mod='jmarketplace'}</th>
                        <th class="item_last">{l s='Order ID - Ref' mod='jmarketplace'}</th>
                    </tr>
                </thead>
                <tbody>
                {foreach from=$incidences item=incidence name=jmarketplace}
                    <tr>
                        <td class="first_item{if ($incidence.messages_not_readed > 0)} not_readed{/if}"><i class="icon-calendar fa fa-calendar"></i> - {dateFormat date=$incidence.date_add full=0} - <i class="icon-time fa fa-clock-o"></i> {$incidence.date_add|escape:'htmlall':'UTF-8'|substr:11:5}</td>
                        <td class="item{if ($incidence.messages_not_readed > 0)} not_readed{/if}">{$incidence.reference|escape:'htmlall':'UTF-8'}</td>
                        {if $incidence.id_order == 0}
                            <td class="item{if ($incidence.messages_not_readed > 0)} not_readed{/if}">{l s='No order' mod='jmarketplace'}</td>
                        {else}
                            <td class="item{if ($incidence.messages_not_readed > 0)} not_readed{/if}">{$incidence.id_order|intval} - {$incidence.order_ref|escape:'htmlall':'UTF-8'}</td>
                        {/if}
                        <td class="last_item"><a class="open_incidence" data="{$incidence.id_seller_incidence|intval}" href="#">{l s='View' mod='jmarketplace'}</a></td> 
                    </tr>
                    <tr id="incidence_{$incidence.id_seller_incidence|intval}" style="display:none;">
                        <td class="incidence_messages" colspan="7">
                            <h3>{l s='Messages about' mod='jmarketplace'} {$incidence.product_name|escape:'htmlall':'UTF-8'}</h3>
                            {foreach from=$incidence.messages item=message name=messages}
                                <div class="message{if $message.id_seller == 0} customer{else} employee{/if}">
                                    <div class="author">
                                        {if $message.id_seller == 0}
                                            {$message.customer_firstname|escape:'htmlall':'UTF-8'} {$message.customer_lastname|escape:'htmlall':'UTF-8'} - <i class="icon-calendar fa fa-calendar"></i> - {dateFormat date=$message.date_add full=0} - <i class="icon-time fa fa-clock-o"></i> {$message.date_add|escape:'htmlall':'UTF-8'|substr:11:5}
                                        {else}
                                            {$message.seller_name|escape:'htmlall':'UTF-8'} - <i class="icon-calendar fa fa-calendar"></i> - {dateFormat date=$message.date_add full=0} - <i class="icon-time fa fa-clock-o"></i> {$message.date_add|escape:'htmlall':'UTF-8'|substr:11:5}
                                        {/if}
                                    </div>
                                    <div class="description">{$message.description|nl2br nofilter}</div> {*This html content*}
                                </div>
                            {/foreach}    
                            <form action="{$url_contact_seller|escape:'html':'UTF-8'}" method="post" class="std">
                                <input type="hidden" name="id_incidence" id="id_incidence" value="{$message.id_seller_incidence|intval}" />
                                <fieldset>
                                    <div class="form-group">
                                        <label for="description">
                                            {l s='Add response' mod='jmarketplace'} 
                                        </label>
                                        <textarea class="form-control" name="description" cols="40" rows="7"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" name="submitResponse" class="btn btn-default button button-medium">
                                            <span>{l s='Send' mod='jmarketplace'}<i class="icon-chevron-right right"></i></span>
                                        </button>
                                    </div>
                                </fieldset>
                            </form>
                        </td> 
                    </tr>
                {/foreach}
                </tbody>
            </table>
        {else}
            <p class="alert alert-info">{l s='There is not messages.' mod='jmarketplace'} </p>
        {/if}
        
        <div class="form_incidences" style="width:100%; display:block;">
            <form action="{$url_contact_seller|escape:'html':'UTF-8'}" method="post" class="std">
                <fieldset>
                    {if ($num_orders > 0 AND !isset($id_seller) AND !isset($id_product))}
                        <h2 class="page-subheading">{l s='Add new message' mod='jmarketplace'}</h2>
                        <div class="required form-group">
                            <label for="order">
                                {l s='Order/Product' mod='jmarketplace'} 
                            </label>
                            <select id="id_order_product" class="form-control" name="id_order_product">
                                <option value="0">{l s='Without reference to order' mod='jmarketplace'} </option>
                                {foreach from=$orders_products item=order}
                                    <option value="{$order.id_order_product|escape:'html':'UTF-8'}">{$order.order_reference|escape:'html':'UTF-8'} - {$order.order_date_add|escape:'html':'UTF-8'} - {$order.product_name|escape:'html':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">
                                {l s='Message' mod='jmarketplace'} 
                            </label>
                            <textarea class="form-control" name="description" cols="40" rows="7"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submitAddIncidence" class="btn btn-default button button-medium">
                                <span>{l s='Send' mod='jmarketplace'}<i class="icon-chevron-right right"></i></span>
                            </button>
                        </div>
                    {else if (isset($id_seller) AND isset($id_product))}
                        <h2 class="page-subheading">{l s='Add new message' mod='jmarketplace'} {l s='about' mod='jmarketplace'} "{$product->name|escape:'htmlall':'UTF-8'}"</h2>
                        {if $id_product}<input type="hidden" name="id_product" value="{$id_product|intval}" />{/if} 
                        <div class="form-group">
                            <label for="description">
                                {l s='Message' mod='jmarketplace'} 
                            </label>
                            <textarea class="form-control" name="description" cols="40" rows="7"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submitAddIncidence" class="btn btn-default button button-medium">
                                <span>{l s='Send' mod='jmarketplace'}<i class="icon-chevron-right right"></i></span>
                            </button>
                        </div>
                    {/if}
                </fieldset>
            </form>
        </div>
    {else}
        <p class="alert alert-info">
            {l s='You must be logged in to contact the seller.' mod='jmarketplace'} 
            <a href="{$link->getPageLink('my-account', true)|escape:'htmlall':'UTF-8'}">{l s='Login or register' mod='jmarketplace'}</a>
        </p>
    {/if}
</div>

<ul class="footer_links clearfix">
    {if isset($id_product)}
    <li>
        <a class="btn btn-default button button-small" href="{$link->getProductLink($id_product)|escape:'htmlall':'UTF-8'}">
            <span>
                <i class="icon-chevron-left"></i>{l s='Back to ' mod='jmarketplace'} {$product->name|escape:'htmlall':'UTF-8'}
            </span>
        </a>
    </li>
    {/if}
    <li>
    <a class="btn btn-default button button-small" href="{$base_uri|escape:'htmlall':'UTF-8'}">
        <span>
            <i class="icon-chevron-left"></i>{l s='Home' mod='jmarketplace'}
        </span>
    </a>
</li>
</ul>         
<script type="text/javascript">
var sellermessages_controller_url = '{$link->getModuleLink('jmarketplace', 'contactseller', array(), true)|escape:'html':'UTF-8'}';
</script>          