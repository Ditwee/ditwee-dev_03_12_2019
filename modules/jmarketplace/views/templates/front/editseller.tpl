{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $show_menu_top == 1}
    {include file="./selleraccount.tpl"}
{/if}

{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {l s='Your account' mod='jmarketplace'}
    </a>
    <a href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}">
        {l s='Your seller account' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Edit your seller account' mod='jmarketplace'}
    </span>
{/capture}

<div class="row">
    <div class="column col-xs-12 col-sm-3"{if $show_menu_options == 0} style="display:none;"{/if}>
        {include file="./sellermenu.tpl"}
    </div>
    <div class="col-xs-12 col-sm-{if $show_menu_options == 0}12{else}9{/if}">
        <div class="box">
            <h1 class="page-subheading">{l s='Edit your seller account' mod='jmarketplace'}</h1>
            {if isset($confirmation) && $confirmation}
                {if $moderate}
                    <p class="alert alert-success">{l s='Your seller account has been successfully edited. It is pending approval.' mod='jmarketplace'}</p>
                {else}
                    <p class="alert alert-success">{l s='Your seller account has been successfully edited.' mod='jmarketplace'}</p>
                {/if}
            {else}
                {if isset($errors) && $errors}
                    {if $ps_version != '1.7'}
                        {if isset($errors) && $errors}
                            {include file="./errors.tpl"}
                        {/if}
                    {/if}
                {/if}
                <p class="info-title">{l s='Edit your seller account.' mod='jmarketplace'}</p>
                <form action="{$link->getModuleLink('jmarketplace', 'editseller', array(), true)|escape:'html':'UTF-8'}" method="post" class="std" enctype="multipart/form-data" id="form_edituser">
                    {*<fieldset>*}
                        <div class="required form-group">
                            <label for="name" class="required">{l s='Seller name' mod='jmarketplace'} </label>
                            <input class="is_required validate form-control" type="text" id="name" name="name" value="{$seller->name|escape:'html':'UTF-8'}" />
                        </div>
                        {if $show_shop_name == 1}
                            <div class="form-group">
                                <label for="shop">{l s='Shop name' mod='jmarketplace'}</label>
                                <input class="form-control" type="text" name="shop" id="shop" value="{$seller->shop|escape:'html':'UTF-8'}" />
                            </div>
                        {/if}
                        {if $show_cif == 1}
                            <div class="required form-group">
                                <label for="cif">{l s='CIF/NIF' mod='jmarketplace'}</label>
                                <input class="form-control" type="text" name="cif" id="cif" value="{$seller->cif|escape:'html':'UTF-8'}" />
                            </div>
                        {/if}
                        {if $show_fax == 1}
                            <div class="form-group">
                                <label for="fax">{l s='Fax' mod='jmarketplace'}</label>
                                <input class="form-control" type="text" name="fax" id="fax" value="{$seller->fax|escape:'html':'UTF-8'}" />
                            </div>
                        {/if}
                        <div class="required form-group">
                            <label for="email" class="required">{l s='Seller email' mod='jmarketplace'}</label>
                            <input class="is_required validate form-control" type="text" id="email" name="email" value="{$seller->email|escape:'html':'UTF-8'}" />
                        </div>
                        {if $show_language == 1}
                            <div class="form-group">
                                <label for="seller_lang">{l s='Language' mod='jmarketplace'}</label>
                                <select name="id_lang">
                                    {foreach from=$languages item=language}
                                        <option value="{$language.id_lang|intval}"{if $seller->id_lang == $language.id_lang} selected="selected"{/if}>{$language.name|escape:'html':'UTF-8'} </option>
                                    {/foreach}
                                </select>
                            </div>
                        {/if}
                        {if $show_phone == 1}
                            <div class="required form-group">
                                <label for="phone">{l s='Phone' mod='jmarketplace'}</label>
                                <input class="form-control" type="text" name="phone" id="phone" value="{$seller->phone|escape:'html':'UTF-8'}" />
                            </div>
                        {/if}
                        {if $show_address == 1}
                            <div class="form-group">
                                <label for="address">{l s='Address' mod='jmarketplace'}</label>
                                <input class="form-control" type="text" name="address" id="address" value="{$seller->address|escape:'html':'UTF-8'}" />
                            </div>
                        {/if}
                        {if $show_country == 1}
                            <div class="form-group">
                                <label>{l s='Country' mod='jmarketplace'}</label>
                                <select name="country">
                                    <option value="">{l s='-- Choose --' mod='jmarketplace'}</option>
                                    {foreach from=$countries item=country}
                                        <option value="{$country.name|escape:'html':'UTF-8'}" {if $country.name == $seller->country} selected="selected"{/if}>{$country.name|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            </div>
                        {/if}
                        {if $show_state == 1}
                            <div class="form-group">
                                <label for="state">{l s='State' mod='jmarketplace'} </label>
                                <input class="form-control" type="text" name="state" id="state" value="{$seller->state|escape:'html':'UTF-8'}" />
                            </div>
                        {/if}
                        {if $show_postcode == 1}
                            <div class="form-group">
                                <label for="postcode">{l s='Postal Code' mod='jmarketplace'}</label>
                                <input class="form-control" type="text" name="postcode" id="postcode" value="{$seller->postcode|escape:'html':'UTF-8'}" />
                            </div>
                        {/if}
                        {if $show_city == 1}
                            <div class="form-group">
                                <label for="city">{l s='City' mod='jmarketplace'}</label>
                                <input class="form-control" type="text" name="city" id="city" value="{$seller->city|escape:'html':'UTF-8'}" />
                            </div>
                        {/if}
                        {if $show_description == 1}
                            <div class="form-group required">
                                <label for="description" class="required">{l s='Description' mod='jmarketplace'}</label>
                                <textarea name="description" id="description" cols="40" rows="7" class="form-control is_required validate">{$seller->description} {*This is HTML content*}</textarea>
                            </div>
                        {/if}
                        {if $show_logo == 1}    
                            <div class="form-group">
                                <label for="fileUpload">{l s='Logo or photo' mod='jmarketplace'}</label>
                                <!--<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />-->
                                <input type="file" name="sellerImage" id="fileUpload" class="form-control not_uniform" />
                                {if isset($photo)}
                                    <p><img src="{$img_ps_dir|escape:'html':'UTF-8'}sellers/{$photo|escape:'html':'UTF-8'}" width="70" height="80" /></p>
                                {/if}
                            </div>
							
							<div class="form-group">
                                <label for="fileUpload2">{l s='Logo or photo of seller' mod='jmarketplace'}</label>
                                <!--<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />-->
                                <input type="file" name="sellerImage2" id="fileUpload2" class="form-control not_uniform" />
                                {if isset($photo2)}
                                    <p><img src="{$img_ps_dir|escape:'html':'UTF-8'}sellers/{$photo2|escape:'html':'UTF-8'}" width="1200" height="330" /></p>
                                {/if}
                            </div>
							
                        {/if}
                        {if $show_iban == 1}
                            <div class="form-group">
                                <label for="iban">{l s='Iban' mod='jmarketplace'}</label>
                                <input class="form-control" type="text" name="iban" id="iban" value="{$seller->iban}" style="text-transform: uppercase"/>
                            </div>
                        {/if}

                        {if $show_carriers == 1}
                            {assign var="sellerCarriers" value=";"|explode:$seller->carriers}
                            <div class="form-group">
                                <label>{l s='Carriers' mod='jmarketplace'}</label>
                                <select name="carriers[]" id="carriers[]" multiple="multiple" style="height: auto; width: 100%">
                                    {foreach from=$carriers item=carrier}
                                        <option value="{$carrier.id_reference|escape:'html':'UTF-8'}" {if in_array($carrier.id_reference,$sellerCarriers)} selected="selected"{/if}>{$carrier.name|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                                <div><strong>Click & Collect : </strong><span class="form_info">{l s="Si vous avez un magasin, une boutique ou un atelier, les clients retirent les articles directement depuis ce lieu"}</span></div>
                                <div><strong>Domicile : </strong><span class="form_info">{l s="Vous utilisez votre mode de livraison à domicil habituelle."}</span></div>
                                <div><strong>Point Relais : </strong><span class="form_info">{l s="Ditwee génère l'étiquette de transport, vous l'envoie par mail, vous l'imprimez et la collez sur le colis. Vous déposez le colis dans le point Relais Colis de votre choix et Relais Colis achemine le colis au point Relais Colis de destination."}</span></div>
                                <div><strong>Express : </strong><span class="form_info">{l s="Ditwee génère l'étiquette de transport, vous l'envoie par mail, vous l'imprimez et la collez sur le colis. DHL passe collecter le colis et le livre au client final."}</span></div>
                            </div>
                        {/if}
						<div class="form-group clearfix">{l s='Working hours' mod='jmarketplace'}     
		<div id="divWorkinghours">
		</div>
		<input type="hidden" value="" name="workinghours" id="workinghours" />
		</div>

        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/0.9.0/jquery.mask.min.js'></script>
        <script>

            {*var SPMaskBehavior = function (val, key) {*}
                {*if(val == '' && key.key == '+'){*}
                    {*return '\+00 0 00 00 00 00';*}
                {*}else{*}
                    {*return '00 00 00 00 00';*}
                {*}*}
            {*},*}
            {*spOptions = {*}
                {*onKeyPress: function(val, e, field, options) {*}
                    {*field.mask(SPMaskBehavior.apply({}, arguments), options);*}
                {*}*}
            {*};*}

            {*$('#phone').mask(SPMaskBehavior, spOptions);*}

            $('#iban').mask('SS00 0000 0000 0000 0000 0000 000', {
                placeholder: '____ ____ ____ ____ ____ ____ ___'
            });
            // $('#phone').mask('00 00 00 00 00', {
            //     placeholder: '__ __ __ __ __'
            // });

            var businessHoursManager = $("#divWorkinghours").businessHours({
                postInit:function(){
                    $('.operationTimeFrom, .operationTimeTill').timepicker_for_businessHours({
                        'timeFormat': 'H:i',
                        'step': 15
                        });
                },
                weekdays:{$days},
                {if $seller->workinghours}

                operationTime: {$seller->workinghours|stripslashes},
                {/if}
                dayTmpl:'<div class="dayContainer" style="width: 80px;">' +
                    '<div data-original-title="" class="colorBox"><div class="invisible"><input type="checkbox" class="invisible operationState"></div></div>' +
                    '<div class="weekday"></div>' +
                    '<div class="operationDayTimeContainer">' +
                    '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime" class="mini-time form-control operationTimeFrom" value="" ></div>' +
                    '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime" class="mini-time form-control operationTimeTill" value="" ></div>' +
                    '<label><input type="checkbox" class="chkBreak">Avec pause</label>' +
                    '<div class="divBreak">' +
                    '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-sun-o"></i></span><input type="text" name="startTime2" class="mini-time form-control operationTimeFrom" value="" ></div>' +
                    '<div class="operationTime input-group"><span class="input-group-addon"><i class="fa fa-moon-o"></i></span><input type="text" name="endTime2" class="mini-time form-control operationTimeTill" value="" ></div>' +
                    '</div></div></div>'
            });

            $('#form_edituser').submit(function(e) {
                //e.preventDefault();
                $("#workinghours").val(JSON.stringify(businessHoursManager.serialize()));
                // console.log( JSON.stringify(businessHoursManager.serialize()));
                //$(this).unbind('submit').submit();
            })
        </script>
                        {hook h='displayMarketplaceFormAddSeller'}
                        <div class="form-group">
                            <button type="submit" name="submitEditSeller" class="btn btn-default button button-medium">
                                <span>{l s='Save' mod='jmarketplace'}<i class="icon-chevron-right right"></i></span>
                            </button>
                        </div>
                    {*</fieldset>*}
                </form>
            {/if}
        </div>
        {include file="./footer.tpl"}
    </div>
</div>   

