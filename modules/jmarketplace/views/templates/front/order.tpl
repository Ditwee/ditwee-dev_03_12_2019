{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $show_menu_top == 1}
    {include file="./selleraccount.tpl"}
{/if}

{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {l s='Your account' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <a href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}">
        {l s='Your seller account' mod='jmarketplace'}
    </a>
    <a href="{$link->getModuleLink('jmarketplace', 'orders', array(), true)|escape:'html':'UTF-8'}">
        {l s='Your orders' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Order' mod='jmarketplace'} "{$order->reference|escape:'html':'UTF-8'}" Nº{$order->id|intval}
    </span>
{/capture}

<div class="row">
    <div class="column col-xs-12 col-sm-3"{if $show_menu_options == 0} style="display:none;"{/if}>
        {include file="./sellermenu.tpl"}
    </div>
    <div class="col-xs-12 col-sm-{if $show_menu_options == 0}12{else}9{/if}">
        <div class="row">
            <div class="col-md-12">
                <div class="boxes">
                    <h2 class="page-subheading">{l s='Order' mod='jmarketplace'} "{$order->reference|escape:'html':'UTF-8'}" Nº{$order->id|intval}</h2>
                    <div class="col-md-4">
                        {l s='Date of order' mod='jmarketplace'}: {$order->date_add|escape:'html':'UTF-8'}<br/>
                        {l s='Method of payment' mod='jmarketplace'}: {$order->payment|escape:'html':'UTF-8'}<br/>
						{*
                        {l s='Total weight' mod='jmarketplace'}: {$total_weight|floatval} {$weight_unit|escape:'html':'UTF-8'}<br/>
                        {l s='Total paid' mod='jmarketplace'}: {$total_paid|escape:'htmlall':'UTF-8'}
						*}
                    </div>

                    <div class="col-md-8 pull-right ditwee-space">
                        <a href="javascript:window.print()" class="btn btn-default button button-small">
                            <i class="icon-print"></i> {l s='Print order' mod='jmarketplace'}
                        </a>
						
                        {if $order->invoice_number > 0}
						{*
                            <a href="{$order_invoice_link|escape:'html':'UTF-8'}" class="btn btn-default button button-small">
                                <i class="icon-file"></i> {l s='View customer invoice' mod='jmarketplace'}
                            </a>
						*}
							<a href="{$order_seller_invoice_link|escape:'html':'UTF-8'}" class="btn btn-default button button-small">
                                <i class="icon-file"></i> {l s='View seller invoice' mod='jmarketplace'}
                            </a>
							
							<a href="{$order_seller_commission_link|escape:'html':'UTF-8'}" class="btn btn-default button button-small">
                                <i class="icon-file"></i> {l s='View seller commission' mod='jmarketplace'}
                            </a>
							
                        {/if}
						
                    </div> 
                </div>
            </div>
        </div>    
        <div class="row">
            <div id="order" class="col-md-7">
                <div class="boxes">
                    <h2 class="page-subheading">{l s='Order' mod='jmarketplace'}</h2>
                    <table id="history-status" class="table table-bordered footab">
                        <tbody>
                            {foreach from=$order_state_history item=order_history name=order_history}
                                <tr style="background-color:{$order_history.color|escape:'html':'UTF-8'};color:white;">
                                    <td class="first_item">{$order_history.ostate_name|escape:'html':'UTF-8'}</td>
                                    <td class="item">{$order_history.date_add|escape:'html':'UTF-8'}</td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
				{if $order_state_history[0].id_order_state != 6 && $order_state_history[0].id_order_state != 5  }
                    <form action="{$order_link|escape:'html':'UTF-8'}" method="post" class="std">
                        <div class="form-group">
                            <label>{l s='Order state' mod='jmarketplace'}</label>
                            <select name="id_order_state">
                                {foreach from=$order_states item=order_state}
                                    <option value="{$order_state.id_order_state|intval}"{if isset($order->current_state) && $order->current_state == $order_state.id_order_state} selected="selected"{/if}>{$order_state.name|escape:'html':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submitState" class="btn btn-default button button-medium">
                                <span>{l s='Change status' mod='jmarketplace'}<i class="icon-chevron-right right"></i></span>
                            </button>
                        </div>    
                    </form>
				{/if}
                </div>
            </div>
            <div class="col-md-5">
                <div id="customer" class="boxes">
                    <h2 class="page-subheading">{l s='Customer' mod='jmarketplace'}</h2>
                    {l s='Name' mod='jmarketplace'}: {$customer->firstname|escape:'html':'UTF-8'} {$customer->lastname|escape:'html':'UTF-8'}<br/>
                    {l s='Email' mod='jmarketplace'}: {$customer->email|escape:'html':'UTF-8'}<br/>

                    <div id="address_delivery" class="col-md-6">
                        <h4>{l s='Address delivery' mod='jmarketplace'}</h4>
                        {foreach from=$address_delivery item=concept}
                            {if $concept != ''}
                                {$concept|escape:'html':'UTF-8'}<br/>
                            {/if}
                        {/foreach}
                    </div>

                    <div id="address_invoice" class="col-md-6">
                        <h4>{l s='Address invoice' mod='jmarketplace'}</h4>
                        {foreach from=$address_invoice item=concept}
                            {if $concept != ''}
                                {$concept|escape:'html':'UTF-8'}<br/>
                            {/if}
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="tracking" class="boxes">
                    {if $ps_version != '1.7'}
                        {if isset($errors) && $errors}
                            {include file="./errors.tpl"}
                        {/if}
                    {/if}
                    <div class="table-responsive">
                        <h2 class="page-subheading">{l s='Transport' mod='jmarketplace'}</h2>
                        <form action="{$order_link|escape:'html':'UTF-8'}" method="post" class="std">
                            <table class="table" id="shipping_table">
                                <thead>
                                    <tr>
                                        <th>
                                            <span class="title_box ">{l s='Date' mod='jmarketplace'}</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">&nbsp;</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">{l s='Carrier' mod='jmarketplace'}</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">{l s='Weight' mod='jmarketplace'}</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">{l s='Shipping cost' mod='jmarketplace'}</span>
                                        </th>
                                        <th>
                                            <span class="title_box ">{l s='Tracking number' mod='jmarketplace'}</span>
                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foreach from=$order_shipping item=line}
									
                                        <tr>
                                            <td>{dateFormat date=$line.date_add full=true}</td>
                                            <td>&nbsp;</td>
                                            <td>{$line.carrier_name|escape:'html':'UTF-8'}</td>
                                            <td class="weight">{$line.weight|string_format:"%.3f"} {$ps_weight_unit|escape:'html':'UTF-8'}</td>
                                            <td class="center">
                                                {if $order->getTaxCalculationMethod() == $smarty.const.PS_TAX_INC}
                                                    {Tools::displayPrice($line.shipping_cost_tax_incl)|escape:'html':'UTF-8'}
                                                {else}
                                                    {Tools::displayPrice($line.shipping_cost_tax_excl)|escape:'html':'UTF-8'}
                                                {/if}
                                            </td>
                                            <td>
                                                <input type="hidden" name="id_order_carrier" value="{$line.id_order_carrier|intval}" />
                                                
												{if $order_shipping[0].id_reference==83 || $order_shipping[0].id_reference==89} 
													<input type="text" class="form-control" name="tracking_number" {if $line.tracking_number}value="{$line.tracking_number|escape:'html':'UTF-8'}"{/if}>
													{if $line.tracking_number}
														<a target="_blank"  class="exclusive btn btn-default" href="{$line.tracking_number|escape:'html':'UTF-8'}">{l s="Suivi" mod="jmkarketplace"}</a>
													{/if}
												{else}
													{if $line.url && $line.tracking_number}
														<a target="_blank"  class="exclusive btn btn-default" href="{$line.url|replace:'@':$line.tracking_number|escape:'html':'UTF-8'}">{l s="Suivi" mod="jmkarketplace"}</a>
													{else} {l s="Pas de suivi" mod="jmkarketplace"}
													{/if}
												{/if}
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
							
							{if $order_shipping[0].id_reference==83 || $order_shipping[0].id_reference==89} 
                            <div class="form-group">
                                <button type="submit" name="submitShippingNumber" class="btn btn-default button button-medium">
                                    <span>{l s='Change shipping number' mod='jmarketplace'}<i class="icon-chevron-right right"></i></span>
                                </button>
                            </div>
							{/if}
							{if $order->gift}
							<div>
								<label style="price">{l s="The customer would like his order to be gift wrapped."}</span>
								{if $order->gift_message}
									<div>{$order->gift_message}</div>
								{/if}	
							</div>
							{/if}
                        </form>
                    </div>  
                </div>
            </div>
        </div>
        <div class="row">
            <div id="products" class="col-md-12">
                <div id="products" class="boxes">
                    <h2 class="page-subheading">{l s='Products' mod='jmarketplace'}</h2>
                    {if $products && count($products)}
                        <div class="table-responsive">
                            <table id="order-list" class="table table-bordered footab">
                                <thead>
                                    <tr>
                                        <th class="first_item" data-sort-ignore="true">{l s='Product' mod='jmarketplace'}</th>
                                        <th class="item" style="text-align: right;">{l s='Unit Price' mod='jmarketplace'}</th>
                                        <th class="item" style="text-align: center;">{l s='Qty' mod='jmarketplace'}</th>
                                        <th class="item" style="text-align: center;">{l s='Available quantity' mod='jmarketplace'}</th>
                                        <th class="item" style="text-align: right;">{l s='Total' mod='jmarketplace'}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {foreach from=$products item=product name=product}
                                    <tr>
                                        <td class="first_item">
                                            {$product.product_name|escape:'html':'UTF-8'}<br/>
                                            {l s='Reference' mod='jmarketplace'}: {$product.product_reference|escape:'html':'UTF-8'}
                                        </td>
                                        <td class="item" style="text-align: right;">{$product.unit_price_tax_incl|escape:'html':'UTF-8'}</td>
                                        <td class="item" style="text-align: center;">{$product.product_quantity|intval}</td>
                                        <td class="item" style="text-align: center;">{$product.current_stock|intval}</td>
                                        <td class="item" style="text-align: right;">{$product.total_price_tax_incl|escape:'html':'UTF-8'}</td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4" style="text-align: right;">{l s='Total products' mod='jmarketplace'}</td>
                                        <td style="text-align: right;"><strong>{$total_products|escape:'html':'UTF-8'}</strong></td>
                                    </tr>
                                    {if $total_discounts > 0}
                                        <tr>
                                            <td colspan="4" style="text-align: right;">{l s='Total discounts' mod='jmarketplace'}</td>
                                            <td style="text-align: right;"><strong>-{$total_discounts|escape:'html':'UTF-8'}</strong></td>
                                        </tr>
                                    {/if}
                                    {if $total_shipping > 0}
                                        <tr>
                                            <td colspan="4" style="text-align: right;">{l s='Total shipping' mod='jmarketplace'}</td>
                                            <td style="text-align: right;"><strong>{$total_shipping|escape:'html':'UTF-8'}</strong></td>
                                        </tr>
                                    {/if}
									{if $order->total_wrapping > 0}
                                        <tr>
                                            <td colspan="4" style="text-align: right;">{l s='Total wrapping' mod='jmarketplace'}</td>
                                            <td style="text-align: right;"><strong>{Tools::displayPrice($order->total_wrapping)|escape:'html':'UTF-8'}</strong></td>
                                        </tr>
                                    {/if}
                                    <tr>
                                        <td colspan="4" style="text-align: right;">{l s='Total' mod='jmarketplace'}</td>
                                        <td style="text-align: right;"><strong>{$total_paid|escape:'html':'UTF-8'}</strong></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
        <div class="row">
            <div id="benefits" class="col-md-12">
                <div class="boxes">
                    <h2 class="page-subheading">{l s='Benefits' mod='jmarketplace'}</h2>
                    <p>{l s='You can recollect a' mod='jmarketplace'} {$commision|floatval}% {l s='of the product sale of this order.' mod='jmarketplace'}</p>
                    <p>{l s='This order allows you collect' mod='jmarketplace'} <strong>{$total_commision|escape:'html':'UTF-8'}</strong></p>
                </div>
            </div>
        </div>
        {include file="./footer.tpl"}
    </div>
</div>
