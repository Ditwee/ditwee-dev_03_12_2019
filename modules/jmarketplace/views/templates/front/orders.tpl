{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $show_menu_top == 1}
    {include file="./selleraccount.tpl"}
{/if}

{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {l s='Your account' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <a href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}">
        {l s='Your seller account' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Your orders' mod='jmarketplace'}
    </span>
{/capture} 

<div class="row">
    <div class="column col-xs-12 col-sm-3"{if $show_menu_options == 0} style="display:none;"{/if}>
        {include file="./sellermenu.tpl"}
    </div>
    <div class="col-xs-12 col-sm-{if $show_menu_options == 0}12{else}9{/if}">
        <div class="box">
            <h1 class="page-subheading">{l s='Your orders' mod='jmarketplace'}</h1>
            {if $orders && count($orders)}
                <table id="order-list" class="table table-bordered footab">
                    <thead>
                        <tr>
                            <th class="first_item hidden-xs hidden-sm" data-sort-ignore="true">{l s='Date add' mod='jmarketplace'}</th>
                            <th class="item hidden-xs hidden-sm">{l s='Order ID' mod='jmarketplace'}</th>
                            <th class="item">{l s='Reference Order' mod='jmarketplace'}</th>
                            <th class="item hidden-xs hidden-sm">{l s='Customer' mod='jmarketplace'}</th>
                            <th class="item hidden-xs hidden-sm">{l s='Total' mod='jmarketplace'}</th>
                            <th class="item hidden-xs hidden-sm">{l s='Method of payment' mod='jmarketplace'}</th>
                            <th class="item">{l s='State' mod='jmarketplace'}</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach from=$orders item=order name=sellerorders}
                        <tr>
                            <td class="first_item hidden-xs hidden-sm">{$order.date_add|escape:'html':'UTF-8'}</td>
                            <td class="item hidden-xs hidden-sm">{$order.id_order|intval}</td>
                            <td class="item">{$order.reference|escape:'html':'UTF-8'}</td>
                            <td class="item hidden-xs hidden-sm">{$order.customer|escape:'html':'UTF-8'}</td>
                            <td class="item hidden-xs hidden-sm">{$order.total_paid_tax_incl|escape:'html':'UTF-8'}</td>
                            <td class="item hidden-xs hidden-sm">{$order.payment|escape:'html':'UTF-8'}</td>
                            <td class="last_item">{$order.osname|escape:'html':'UTF-8'}</td>
                            <td class="last_item text-center">
                                <a title="{l s='View' mod='jmarketplace'}" href="{$link->getModuleLink('jmarketplace', 'orders', array(), true)|escape:'html':'UTF-8'}?id_order={$order.id_order|intval}">
                                    {if $ps_version == '1.7'}
                                        <i class="icon-eye fa fa-eye"></i>
                                    {else}
                                        <img src="{$modules_dir|escape:'html':'UTF-8'}jmarketplace/views/img/view.gif" />
                                    {/if} 
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>

                </table>
            {else}
                <p class="alert alert-info">{l s='There are not orders.' mod='jmarketplace'} </p>
            {/if}
        </div>
        {include file="./footer.tpl"}
    </div>
</div>
