{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{extends file='page.tpl'}

{block name='page_content'}
    {if $tpl_name == 'addseller'}
        {include file="modules/jmarketplace/views/templates/front/addseller.tpl"}
    {elseif $tpl_name == 'editseller'}
        {include file="modules/jmarketplace/views/templates/front/editseller.tpl"}
    {elseif $tpl_name == 'selleraccount'}
        {include file="modules/jmarketplace/views/templates/front/selleraccount.tpl"}
    {elseif $tpl_name == 'addproduct'}
        {include file="modules/jmarketplace/views/templates/front/addproduct.tpl"}
    {elseif $tpl_name == 'sellerproducts'}
        {include file="modules/jmarketplace/views/templates/front/sellerproducts.tpl"}
    {elseif $tpl_name == 'editproduct'}
        {include file="modules/jmarketplace/views/templates/front/editproduct.tpl"}
    {elseif $tpl_name == 'csvproducts'}
        {include file="modules/jmarketplace/views/templates/front/csvproducts.tpl"}
    {elseif $tpl_name == 'sellerprofile'}
        {include file="modules/jmarketplace/views/templates/front/sellerprofile.tpl"}
    {elseif $tpl_name == 'sellercomments'}
        {include file="modules/jmarketplace/views/templates/front/sellercomments.tpl"}
    {elseif $tpl_name == 'favoriteseller'}
        {include file="modules/jmarketplace/views/templates/front/favoriteseller.tpl"}
    {elseif $tpl_name == 'sellerproductlist'}
        {include file="modules/jmarketplace/views/templates/front/sellerproductlist17.tpl"}
    {elseif $tpl_name == 'sellerorders'}
        {include file="modules/jmarketplace/views/templates/front/sellerorders.tpl"}
    {elseif $tpl_name == 'orders'}
        {include file="modules/jmarketplace/views/templates/front/orders.tpl"}
    {elseif $tpl_name == 'order'}
        {include file="modules/jmarketplace/views/templates/front/order.tpl"}
    {elseif $tpl_name == 'carriers'}
        {include file="modules/jmarketplace/views/templates/front/carriers.tpl"}
    {elseif $tpl_name == 'addcarrier'}
        {include file="modules/jmarketplace/views/templates/front/addcarrier.tpl"}
    {elseif $tpl_name == 'sellerpayment'}
        {include file="modules/jmarketplace/views/templates/front/sellerpayment.tpl"}
    {elseif $tpl_name == 'sellermessages'}
        {include file="modules/jmarketplace/views/templates/front/sellermessages.tpl"}
    {elseif $tpl_name == 'contactseller'}
        {include file="modules/jmarketplace/views/templates/front/contactseller.tpl"}
    {elseif $tpl_name == 'dashboard'}
        {include file="modules/jmarketplace/views/templates/front/dashboard.tpl"}
    {elseif $tpl_name == 'sellers'}
        {include file="modules/jmarketplace/views/templates/front/sellers.tpl"}
    {elseif $tpl_name == 'sellerinvoice'}
        {include file="modules/jmarketplace/views/templates/front/sellerinvoice.tpl"}
    {elseif $tpl_name == 'sellerinvoicehistory'}
        {include file="modules/jmarketplace/views/templates/front/sellerinvoicehistory.tpl"}
    {/if}
{/block}
