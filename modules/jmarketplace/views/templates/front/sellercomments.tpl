{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$seller_link|escape:'html':'UTF-8'}">
        {$seller->name|escape:'html':'UTF-8'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Ratings and comments' mod='jmarketplace'}
    </span>
{/capture} 

{if isset($confirmation) && $confirmation}
    {if $moderate == 1}
        <p class="alert alert-success">{l s='Your comment has been successfully sent. It is pending approval.' mod='jmarketplace'}</p>
    {else}    
        <p class="alert alert-success">{l s='Your comment has been successfully sent.' mod='jmarketplace'}</p>
    {/if}
{else}
    {if isset($errors) && $errors}
        {if $ps_version != '1.7'}
            {if isset($errors) && $errors}
                {include file="./errors.tpl"}
            {/if}
        {/if}
    {/if}
{/if}

<div class="box">
    <h1 class="page-heading">{$num_comments|intval} {l s='comments about' mod='jmarketplace'} "{$seller->name|escape:'html':'UTF-8'}"</h1>
    {if isset($seller_comments) && $seller_comments}
        <ul class="seller_comments row">
            {foreach from=$seller_comments item=comment name=sellercomments}
                <li class="col-md-12" itemprop="review" itemscope itemtype="https://schema.org/Review">
                    <div class="comment">
                        <div class="row">
                            <div class="comment_header col-md-12">
                                <div class="star_content clearfix"  itemprop="reviewRating" itemscope itemtype="https://schema.org/Rating">
                                    {section name="i" start=0 loop=5 step=1}
                                        {if $comment.grade le $smarty.section.i.index}
                                            <div class="star"></div>
                                        {else}
                                            <div class="star star_on"></div>
                                        {/if}
                                    {/section}
                                    <meta itemprop="worstRating" content = "0" />
                                    <meta itemprop="ratingValue" content = "{$comment.grade|escape:'html':'UTF-8'}" />
                                    <meta itemprop="bestRating" content = "5" />
                                </div>

                                <div itemprop="name" class="title_block">{$comment.title|escape:'html':'UTF-8'}</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="comment_author_infos col-md-12">
                                <span>{l s='By' mod='jmarketplace'}</span>
                                <span itemprop="author" class="author">{$comment.customer_name|escape:'html':'UTF-8'}</span>
                                <span>{l s='on' mod='jmarketplace'}</span>
                                <meta itemprop="datePublished" content="{$comment.date_add|escape:'html':'UTF-8'|substr:0:10}" />
                                <span>{dateFormat date=$comment.date_add|escape:'html':'UTF-8' full=0}</span>
                            </div>
                        </div>
                        <div class="row">    
                            <div class="comment_details col-md-12">
                                <p itemprop="reviewBody">{$comment.content|escape:'html':'UTF-8'|nl2br}</p> 
                            </div>
                        </div>
                    </div>
                </li>
            {/foreach}
        </ul>
    {else}
        <p class="alert alert-info">{l s='There are not comments.' mod='jmarketplace'}</p>
    {/if}
    {if !$logged AND $allow_guests == 0}
        <p class="align_center">
            <a href="{$link->getPageLink('authentication')|escape:'htmlall':'UTF-8'}?back={$url_seller_comments|escape:'htmlall':'UTF-8'}#new_comment_form">
                {l s='Login to send a comment' mod='jmarketplace'}
            </a>
        </p>
    {else}     
        <div id="new_comment_form">
            <form id="id_new_comment_form" action="{$url_seller_comments|escape:'htmlall':'UTF-8'}" method="post">
                <h2 class="page-subheading">{l s='Write a review about' mod='jmarketplace'} "{$seller->name|escape:'html':'UTF-8'}"</h2>
                <div class="row">
                    {if isset($seller) && $seller}
                        <div class="seller clearfix col-xs-12 col-sm-3">
                            {if $show_logo}
                                <img class="img-responsive" src="{$photo|escape:'html':'UTF-8'}" width="200" height="200" />
                            {/if}
                            {if isset($resum_grade) && $resum_grade}
                                <div class="num_comments">{l s='Number of comments' mod='jmarketplace'}: {$num_comments|intval}</div>  
                                <div class="resum">
                                    {foreach from=$resum_grade item=criterion name=criteriongrade}
                                        <div class="resum_criterion">
                                            {section name="i" start=0 loop=5 step=1}
                                                {if $criterion.grade le $smarty.section.i.index}
                                                    <div class="star"></div>
                                                {else}
                                                    <div class="star star_on"></div>
                                                {/if}
                                            {/section}
                                        </div>
                                    {/foreach}   
                                </div>
                            {/if}
                        </div>
                    {/if}
                    <div class="new_comment_form_content col-xs-12 col-sm-9">
                        {if $criterions|@count > 0}
                            <ul id="criterions_list">
                                {foreach from=$criterions item='criterion'}
                                    <li>
                                        <div class="star_content">
                                            <input name="criterion[{$criterion.id_seller_comment_criterion|intval}]" value="1" type="radio" class="star not_uniform"/> 
                                            <input name="criterion[{$criterion.id_seller_comment_criterion|intval}]" value="2" type="radio" class="star not_uniform"/> 
                                            <input name="criterion[{$criterion.id_seller_comment_criterion|intval}]" value="3" type="radio" class="star not_uniform"/> 
                                            <input name="criterion[{$criterion.id_seller_comment_criterion|intval}]" value="4" type="radio" class="star not_uniform" checked="checked"/> 
                                            <input name="criterion[{$criterion.id_seller_comment_criterion|intval}]" value="5" type="radio" class="star not_uniform"/> 
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                {/foreach}
                            </ul>
                        {/if}
                        <label for="comment_title">{l s='Title:' mod='jmarketplace'} <sup class="required">*</sup></label>
                        <input id="comment_title" name="title" type="text" value=""/>

                        <label for="content">{l s='Comment:' mod='jmarketplace'} <sup class="required">*</sup></label>
                        <textarea id="content" name="content"></textarea>
                        
                        {if $allow_guests == 1 && !$logged}
                            <label>{l s='Your name:' mod='jmarketplace'} <sup class="required">*</sup></label>
                            <input name="customer_name" type="text" value=""/>
                        {/if}

                        <div id="new_comment_form_footer">
                            <p class="fl required"><sup>*</sup> {l s='Required fields' mod='jmarketplace'}</p>
                            <p class="fr">
                                <button id="submitNewMessage" name="submitSellerComment" type="submit" class="btn button button-small">
                                    <span>{l s='Submit' mod='jmarketplace'}</span>
                                </button>
                            </p>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    {/if}
</div>   