{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
        {l s='My account' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <a href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}">
        {l s='My seller account' mod='jmarketplace'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Transfer funds' mod='jmarketplace'}
    </span>
{/capture}

{if $show_menu_top == 1}
    {include file="./selleraccount.tpl"}
{/if}

<div class="row">
    <div class="column col-xs-12 col-sm-3"{if $show_menu_options == 0} style="display:none;"{/if}>
        {include file="./sellermenu.tpl"}
    </div>
    
    <div class="col-xs-12 col-sm-{if $show_menu_options == 0}12{else}9{/if}">
        <div class="box">
            <h1 class="page-subheading">{l s='Request for payment of commissions' mod='jmarketplace'} (<strong>{$total_funds|escape:'html':'UTF-8'}</strong>)</h1>
            {if isset($confirmation) && $confirmation}
                <p class="alert alert-success">
                    {l s='Your request has been successfully sent.' mod='jmarketplace'} 
                </p>
            {/if}
            {if isset($errors) && $errors && $ps_version != '1.7'}
                {include file="./errors.tpl"}
            {/if}
            {if $orders && count($orders)}
                <p>{l s='Collect your money by selecting the desired commissions and attaching an invoice to:' mod='jmarketplace'}</p>
                <h2>{l s='Invoice payment information' mod='jmarketplace'}</h2>
                <div class="marketplace_data_invoice">
                    {$shop_name|escape:'html':'UTF-8'}<br/>
                    {$shop_address|escape:'html':'UTF-8'}<br/>
                    {$shop_code|escape:'html':'UTF-8'} {$shop_city|escape:'html':'UTF-8'}<br/>
                    {if $shop_country != false AND $shop_state != false}{$shop_state|escape:'html':'UTF-8'}, {$shop_country|escape:'html':'UTF-8'}<br/>{/if}
                    {$shop_details|escape:'html':'UTF-8'}<br/>
                </div>

                <p>
                    {l s='Method of payment:' mod='jmarketplace'}
                    <strong>
                        {if $active_payment == 'bankwire'}
                            {l s='Bankwire' mod='jmarketplace'}
                        {else}
                            {l s='PayPal' mod='jmarketplace'}
                        {/if}
                    </strong>
                    <a class="btn btn-default button button-small" href="{$link->getModuleLink('jmarketplace', 'sellerpayment', array(), true)|escape:'html':'UTF-8'}">
                        <span>{l s='Change' mod='jmarketplace'}</span>
                    </a>
                </p>

                <h2>{l s='Your commissions pending payment' mod='jmarketplace'}</h2>

                <form action="{$link->getModuleLink('jmarketplace', 'sellerinvoice', array(), true)|escape:'html':'UTF-8'}" method="post" class="std" enctype="multipart/form-data">
                    <table id="sellerfunds-list" class="table table-bordered footab">
                        <thead>
                            <tr>
                                <th class="first_item" data-sort-ignore="true"></th>
                                <th class="item hidden-xs hidden-sm" data-sort-ignore="true">{l s='Date add' mod='jmarketplace'}</th>
                                <th class="item hidden-xs hidden-sm">{l s='Order ID' mod='jmarketplace'}</th>
                                <th class="item">{l s='Reference Order' mod='jmarketplace'}</th>
                                <th class="item hidden-xs hidden-sm">{l s='Product name' mod='jmarketplace'}</th>
                                <th class="item hidden-xs hidden-sm">{l s='Price (tax excl.)' mod='jmarketplace'}</th>
                                <th class="item hidden-xs hidden-sm">{l s='Quantity' mod='jmarketplace'}</th>
                                <th class="item">{l s='Commision' mod='jmarketplace'}</th>
                            </tr>
                        </thead>
                        <tbody>
                        {foreach from=$orders item=order name=sellerorders}
                            <tr>
                                <td class="first_item"><input type="checkbox" name="commisions[]" value="{$order.id_seller_commision_history|intval}" data="{$order.commision}" /></td>
                                <td class="item hidden-xs hidden-sm">{$order.date_add|escape:'html':'UTF-8'}</td>
                                <td class="item hidden-xs hidden-sm">{$order.id_order|intval}</td>
                                <td class="item">{$order.reference|escape:'html':'UTF-8'}</td>
                                <td class="item hidden-xs hidden-sm">{$order.product_name|escape:'html':'UTF-8'}</td>
                                <td class="item hidden-xs hidden-sm">{$order.price|escape:'html':'UTF-8'}</td>
                                <td class="item hidden-xs hidden-sm">{$order.quantity|intval}</td>
                                <td class="item">{$order.final_commision|escape:'html':'UTF-8'}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="first_item hidden-xs hidden-sm" colspan="7"><strong>{l s='Total invoice' mod='jmarketplace'}</strong></td>
                                <td class="last_item hidden-xs hidden-sm">
                                    <strong><span id="total_invoice" data="0">{$initial_price|escape:'html':'UTF-8'}</span></strong>
                                    <input type="hidden" name="total_invoice" value="0" />
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="form-group col-md-5">
                            <label for="fileUpload">{l s='Invoice in PDF' mod='jmarketplace'}</label>
                            <input type="file" name="sellerInvoice" id="sellerInvoice" class="form-control" />
                        </div>
                        <div class="form-group col-md-2">
                            <button type="submit" name="submitInvoice" class="btn btn-default button button-medium" style="margin-top: 15px;">
                                <span>{l s='Send' mod='jmarketplace'}<i class="icon-chevron-right right"></i></span>
                            </button>
                        </div>
                    </div>
                </form>
            {else}
                <p class="alert alert-info">{l s='There are not pending commisions.' mod='jmarketplace'} </p>
            {/if}
        </div>
    </div>
</div>
<ul class="footer_links clearfix">
    <li>
        <a class="btn btn-default button button-small" href="{$link->getModuleLink('jmarketplace', 'sellerinvoicehistory', array(), true)|escape:'html':'UTF-8'}">
            <span>{l s='Transfer history' mod='jmarketplace'}</span>
        </a>
    </li>
</ul>  
<script type="text/javascript">
var sign = "{$sign|escape:'quotes':'UTF-8'}";
</script>