{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$seller_link|escape:'html':'UTF-8'}">
        {$seller->name|escape:'html':'UTF-8'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Products' mod='jmarketplace'}
    </span>
{/capture}
<h1 class="page-heading product-listing">{l s='Products of' mod='jmarketplace'} {$seller->name|escape:'html':'UTF-8'}</h1>

{if isset($products) && $products}
			
    <div class="content_sortPagiBar clearfix">
        <div class="sortPagiBar clearfix">
            {if $ps_version != '1.7'}
				{include file="$tpl_dir./pagination.tpl"}
            	{include file="$tpl_dir./product-sort.tpl" paginationId='top'}
                {include file="$tpl_dir./nbr-product-page.tpl" paginationId='top'}
                {include file="$tpl_dir./product-compare.tpl"}
			{/if}
        </div>
    </div>
    {if $ps_version != '1.7'}    
        {include file="$tpl_dir./product-list.tpl"  colfix=3}
    {else}
        {include file="$tpl_dir./product-list.tpl" class='jmarketplace-products' id='seller_products' products=$products}
    {/if}
	 <div class="content_sortPagiBar clearfix">
        <div class="bottom-pagination-content sortPagiBar  clearfix">
            {if $ps_version != '1.7'}
				{include file="$tpl_dir./pagination.tpl"}
            	{include file="$tpl_dir./product-sort.tpl" paginationId='top'}
                {include file="$tpl_dir./nbr-product-page.tpl" paginationId='top'}
			{/if}
        </div>
    </div>

{else}
<ul class="tab-pane">
    <li class="alert alert-info">{l s='This seller have not products.' mod='jmarketplace'}</li>
</ul>
{/if}