{capture name=path}
    {if $seller_me}
        <a href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}">
            {l s='Your seller account' mod='jmarketplace'}
        </a>
    {else}
        <a href="{$url_sellers|escape:'html':'UTF-8'}">
            {l s='Sellers' mod='jmarketplace'}
        </a>
    {/if}
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {$seller->name|escape:'html':'UTF-8'} 
    </span>
{/capture}


{if $show_logo }
  <img class="img-responsive seller-manager-photo" alt="{$seller->name|escape:'html':'UTF-8'}" src="{$photo|escape:'html':'UTF-8'}" />
{/if}
<div class="row sellerpage">
  <h1 class="page-subheading">{$seller->name|escape:'html':'UTF-8'}</h1>
  {hook h='displayMarketplaceHeaderProfile'}
  {if $show_description}
    <div class="col-lg-12">
		{$seller->description nofilter}
		<div class="clearfix" style="height:20px"></div>
	</div>
		
  {/if}
  <div class="col-lg-12 sidebar">
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-12">
        <h2><i class="ft-icon-shop-1" aria-hidden="true"></i><span class="sr-only">{l s='Seller address' mod='jmarketplace'}</span></h2>
        {if $show_shop_name}
			<p>{$seller->name|escape:'html':'UTF-8'}</p>
			<p>{$seller->shop|escape:'html':'UTF-8'}</p>
        {/if}
		
        {if $show_address && $seller->show_address}
          <p>{$seller->address|escape:'html':'UTF-8'}</p>
        {/if}
        {if $show_postcode  && $seller->show_address}
          {$seller->postcode|escape:'html':'UTF-8'}
        {/if}
        {if $show_city  && $seller->show_address}
          {$seller->city|escape:'html':'UTF-8'}
        {/if}
        {if false && $show_state  && $seller->show_address}
          {$seller->state|escape:'html':'UTF-8'}
        {/if}
        {if $show_country  && $seller->show_address}
          <p>{$seller->country|escape:'html':'UTF-8'}</p>
        {/if}
        
      </div>

      <div class="col-md-4 col-sm-6 col-xs-12">
        <h2><i class="ft-icon-phone" aria-hidden="true"></i><span class="sr-only">{l s='Seller phone' mod='jmarketplace'}</span></h2>
        {if $show_phone}
          <p><a href="tel:{$seller->phone|replace:' ':''|escape:'html':'UTF-8'}">{$seller->phone|escape:'html':'UTF-8'}</a></p>
        {/if}
        {if false && $show_fax}
          <p>{l s='Seller fax' mod='jmarketplace'} {$seller->fax|escape:'html':'UTF-8'}</p>
        {/if}
        {if false && $show_email}
          <p><a href="mailto:{$seller->email|escape:'html':'UTF-8'}">{$seller->email|escape:'html':'UTF-8'}</a></p>
        {/if}
        {if $show_seller_rating}
        <div class="average_rating buttons_bottom_block">
          <a href="{$url_seller_comments|escape:'html':'UTF-8'}" title="{l s='View comments about' mod='jmarketplace'} {$seller->name|escape:'html':'UTF-8'}">
            {section name="i" start=0 loop=5 step=1}
              {if $averageMiddle le $smarty.section.i.index}
                <div class="star"></div>
              {else}
                <div class="star star_on"></div>
              {/if}
            {/section}
            (<span id="average_total">{$averageTotal|intval}</span>)
          </a>
        </div>
        {/if}
        {if $show_seller_favorite}
          <a title="{l s='Add to favorite seller' mod='jmarketplace'}" href="{$url_favorite_seller|escape:'html':'UTF-8'}">
            {l s='Add to favorite seller' mod='jmarketplace'}
            <i class="ft-icon-heart" aria-hidden="true"></i>
          </a>
        {/if}
      </div>
	  
	  <div class="clearfix hidden-lg hidden-md"></div>
	  
	  {if $workinghours && $seller->show_address}
	
		<div  class="col-md-4 col-sm-6 col-xs-12 sidebar">
			<h2><i class="ft-icon-clock"></i><span class="sr-only">{l s='Working hours' mod='jmarketplace'}</span></h2>
			
			{foreach from=$workinghours item=days}
			<p>
				{$days->day} :
				{if $days->isActive}
					{$days->timeFrom}-{$days->timeTill}
					{if $days->isBreak} / {$days->timeFrom2}-{$days->timeTill2}{/if}
				{else}
					{l s='Close' mod='jmarketplace'}
				{/if}
			</p>
			{/foreach}
		</div>
		{/if}
	
	
	 </div>
	</div>

	  
	
    
    {hook h='displayMarketplaceTableProfile'}

{if $show_new_products && isset($products) && $products}
    <div class="box">
      <h2 class="page-heading">{l s='A %s products selection' sprintf={$seller->name|escape:'html':'UTF-8'} mod='jmarketplace'} </h2>
      {if $ps_version == '1.7'}
          {block name='seller_news'}
              {include file="../../../../../../../modules/jmarketplace/views/templates/front/product-list.tpl" class='jmarketplace-products' id='seller_news' products=$products}
          {/block}
      {else}
          {*include file="../../../../../../../modules/jmarketplace/views/templates/front/product-list.tpl" class='tab-pane' id='jmarketplace' colfix=3*}
		  {include file="$tpl_dir/product-list.tpl"  id='module-jmarketplace-sellerproductlist' colfix=3}
      {/if}
    </div>
{/if}
{hook h='displayMarketplaceFooterProfile'}

{if $seller_me}
    {*include file="./footer.tpl"*}
{else}
  <ul class="footer_links clearfix">
    <li style="margin-bottom:1px;">
	
      <a class="btn btn-inverse button button-small" href="javascript: history.go(-1)">
        {l s='Go back' mod='jmarketplace'}
      </a>
    </li>
    <li class="pull-right">
      <a class="btn btn-inverse button button-small" href="{$seller_products_link|escape:'html':'UTF-8'}">
        {l s='View products of' mod='jmarketplace'}
        {$seller->name|escape:'html':'UTF-8'}
      </a>
    </li>
  </ul>
{/if}

	</div>   