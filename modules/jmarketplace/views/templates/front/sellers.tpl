{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <span class="navigation_page">
        {l s='Sellers' mod='jmarketplace'}
    </span>
{/capture}

<div class="box">
    <h1 class="page-heading">{l s='Sellers in Marketplace' mod='jmarketplace'}</h1>
    {if isset($sellers) && $sellers}
		<div class="row">
			<div class="col-xs-12">
				<div id="map" style="width: 100%;height: 350px;margin-bottom:20px" class=""></div>
			</div>
		</div>
        <ul class="seller_list grid row">
        {foreach from=$sellers item=seller name=jmarketplaceSellers}
            <li class="block_seller col-xs-12 col-md-6">
              <div class="jumbotron seller-shop-photo seller-list-shop-photo" style="background-image:url('{$seller.photo2}');">
                <h2 class="seller_name">
                  <a href="{$seller.url|escape:'html':'UTF-8'}">
                    {$seller.name|escape:'html':'UTF-8'}
                  </a>
                </h2>
                {if $show_logo}
                  <a href="{$seller.url|escape:'html':'UTF-8'}" class="seller-list-photo">
                    <img class="img-responsive" src="{$seller.photo|escape:'html':'UTF-8'}" alt="" />
                  </a>
                {/if}
                <a class="seller_product_link pull-right btn btn-inverse" href="{$seller.products_url|escape:'html':'UTF-8'}" title="{l s='View products of' mod='jmarketplace'} {$seller.name|escape:'html':'UTF-8'}">
                  {l s='View products' mod='jmarketplace'}
                </a>
              </div>
            </li>
        {/foreach}
        </ul>
    {else}
        <p class="alert alert-info">{l s='There are not sellers.' mod='jmarketplace'}</p>
    {/if}
</div>



{if isset($sellers) && $sellers}
  
	<script type="text/javascript">
		//<![CDATA[
    var sellers = new Array();
	sellers = {$sellers|@json_encode nofilter}

  
  {literal}
	var activeInfoWindow;
	$(document).ready(function(){
    initMap();
});


	function initMap() {
	
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: 45.757, lng: 4.765},
			zoom: 9,
			gestureHandling: 'greedy'
		});
	
		var myloc = new google.maps.Marker({
			clickable: false,
			icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
				new google.maps.Size(22,22),
				new google.maps.Point(0,18),
				new google.maps.Point(11,11)),
			shadow: null,
			zIndex: 999,
			map: map// your google.maps.Map object
		});
		
		var image = {
		  url: '/img/icon_marker_google_ditwee.png',
		  // This marker is 20 pixels wide by 32 pixels high.
		  //size: new google.maps.Size(32, 32),
		  // The origin for this image is (0, 0).
		  //origin: new google.maps.Point(0, 0),
		  // The anchor for this image is the base of the flagpole at (0, 32).
		  //anchor: new google.maps.Point(0, 32)
		};
		
		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
	
				var me = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				myloc.setPosition(me);
	
				map.setCenter(pos);
			}, function() {
			//handleLocationError(true, infoWindow, map.getCenter());
		});
		}
		else {
			// Browser doesn't support Geolocation
			handleLocationError(false, infoWindow, map.getCenter());
		}

		geocoder = new google.maps.Geocoder();

		for (var x = 0; x < sellers.length; x++) {
		
			var seller = sellers[x];
console.log(seller.name+" , "+ seller.address+ " , "+seller.postcode);
			geocoder.geocode({'address':seller.address + " , " + seller.postcode + " , " + seller.city}, function(seller, x){
				return(function(results, status){
					if (status == google.maps.GeocoderStatus.OK) {
						var latlng = results[0].geometry.location;
			
						var texte = '<div id="iw-container">' +
							'<a href="'+seller.url+'"><img src="'+seller.photo+'" alt="'+seller.name+'" height="115" width="83">'+seller.name+'</a>' +
							'<div class="iw-content">';
							if(seller.show_address==1)
								texte+= seller.address + '<br/>' + seller.postcode + ' ' + seller.city;
							
							texte+= '<br/><a href="tel://'+seller.phone+'">'+seller.phone+'</a>'+
							'<br/>' + seller.description +
							'</div>' +
							'</div>'
								
						var infowindow = new google.maps.InfoWindow({
							content : texte
						});

						var marker = new google.maps.Marker({
							position: latlng,
							title: seller.name,
							map: map,
							icon: image
						});
						
						marker.addListener('click', function() {
							activeInfoWindow&&activeInfoWindow.close();
							infowindow.open(map, marker);
							activeInfoWindow = infowindow;
						});


					}
				});
			}(seller, x));	

		}
	}

	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
		infoWindow.setPosition(pos);
		infoWindow.setContent(browserHasGeolocation ?
			'Error: The Geolocation service failed.' :
			'Error: Your browser doesn\'t support geolocation.');
	}
  {/literal}
//]]>
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDf5h2YzvI6pgjD2DdrJA3HMgISiYo9V94" defer async></script>
{/if}