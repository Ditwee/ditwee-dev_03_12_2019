{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $page_name == 'module-jmarketplace-sellerproducts'} 
    <script type="text/javascript">
    var confirmProductDelete = "{l s='Are you sure you want to delete your product?' mod='jmarketplace'}";
    </script>
{else}
    <script type="text/javascript">
    var addproduct_controller_url = "{$link->getModuleLink('jmarketplace', 'addproduct', array(), true)|escape:'html':'UTF-8'}";
    var editproduct_controller_url = "{$link->getModuleLink('jmarketplace', 'editproduct', array(), true)|escape:'html':'UTF-8'}";
    var image_not_available = "{$image_not_available|escape:'html':'UTF-8'}";
    var has_attributes = "{$has_attributes|intval}";
    var confirmSelectedCategoryTree = "{l s='has been selected ok.' mod='jmarketplace'}";
    var ps_version = "{$ps_version|escape:'html':'UTF-8'}";
    var PS_REWRITING_SETTINGS = "{$PS_REWRITING_SETTINGS|intval}";
    </script>
    {if $show_attributes == 1}   
        <script type="text/javascript">
        var modules_dir = "{$modules_dir|escape:'quotes':'UTF-8'}";
        var token = "{$token|escape:'quotes':'UTF-8'}";
        var errorSaveCombination = "{l s='You must select an option.' mod='jmarketplace'}";
        var errorAttributeGroupEmpty = "{l s='You have not selected attribute group.' mod='jmarketplace'}";
        var errorAttributeEmpty = "{l s='You have not selected attribute.' mod='jmarketplace'}";
        var buttonDelete = "{l s='Delete' mod='jmarketplace'}";
        var errorHasAttributes = "{l s='You cannot use combinations with a virtual product.' mod='jmarketplace'}";
        </script>
    {/if}
{/if}