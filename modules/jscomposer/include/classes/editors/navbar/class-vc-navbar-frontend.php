<?php
require_once vc_path_dir('EDITORS_DIR', 'navbar/class-vc-navbar.php');
/**
 *
 */
Class Vc_Navbar_Frontend extends Vc_Navbar {
	protected $controls = array(
		'add_element',
		'templates',
		'view_post',
		'save_update',
		'screen_size',
		'guides_switch',
		'custom_css'
	);
	protected $controls_filter_name = 'vc_nav_front_controls';
	protected $brand_url = 'http://vc.wpbakery.com/?utm_campaign=VCplugin_header&utm_source=vc_user&utm_medium=frontend_editor';
	public function getControlGuidesSwitch() {
                $vc_manager = vc_manager();
		return '<li class="vc_pull-right">'
		  .'<button id="vc_guides-toggle-button" class="vc_btn vc_btn-primary vc_btn-sm vc_navbar-btn"'
		  .' title="'.$vc_manager->esc_attr( "Toggle editor's guides" ).'">'
		  .$vc_manager->l('Guides ON')
		  .'</button>'
		  .'</li>';
	}
	public function getControlScreenSize() {
		$disable_responsive = vc_settings()->get( 'not_responsive_css' );
                $vc_manager = vc_manager();
		if($disable_responsive !== '1') {
			$screen_sizes = array(
				array(
					'title' => $vc_manager->l('Desktop'),
					'size'  => '100%',
					'key'   => 'default',
					'active' => true
				),
				array(
					'title' => $vc_manager->l('Tablet landscape mode'),
					'size'  => '1024px',
					'key'   => 'landscape-tablets'
				),
				array(
					'title' => $vc_manager->l('Tablet portrait mode'),
					'size'  => '768px',
					'key'   => 'portrait-tablets'
				),
				array(
					'title' => $vc_manager->l('Smartphone landscape mode'),
					'size'  => '480px',
					'key'   => 'landscape-smartphones'
				),
				array(
					'title' => $vc_manager->l('Smartphone portrait mode'),
					'size'  => '320px',
					'key'   => 'portrait-smartphones'
				),
			);
			$output = '<li class="vc_pull-right">'
			  .'<div class="vc_dropdown" id="vc_screen-size-control">'
			  .'<a href="#" class="vc_dropdown-toggle"'
			  .' title="'.$vc_manager->l("Responsive preview").'"><i class="vc_icon default"'
			  .' id="vc_screen-size-current"></i><b class="vc_caret"></b></a>'
			  .'<ul class="vc_dropdown-list">';
			while($screen = current($screen_sizes)) {
				$output .= '<li><a href="#" title="'.$vc_manager->esc_attr($screen['title']).'"'
					.' class="vc_screen-width '.$screen['key']
					.(isset($screen['active']) && $screen['active'] ? ' active' : '')
					.'" data-size="'.$screen['size'].'"></a></li>';
				next($screen_sizes);
			}
			$output .= '</ul></div></li>';
			return $output;
		}
		return '';
	}
	public function getControlSaveUpdate() {

		// if(Tools::getValue('frontend_module_name')){
		// 	$modules_configuration = JsComposer::getModulesConfiguration();		
	 //        $frontend_module_name = Tools::getValue('frontend_module_name');
	 //        $tmp_frontend_module_name = strtolower($frontend_module_name);
	 //        $modult_config = $modules_configuration->$tmp_frontend_module_name;
	 //        $module_name = $tmp_frontend_module_name;
	 //        $module_controller = $modult_config->controller;
	 //        $db_table = $modult_config->table;
	 //        $field_identifier = $modult_config->identifier;
	 //        $field_content = $modult_config->field;

	 //        $val_identifier = Tools::getValue('val_identifier');
	 //    }


     //    if(Tools::getValue('frontend_module_name')){
     //        $modules_configuration = JsComposer::getModulesConfiguration();

     //        $frontend_module_name = Tools::getValue('frontend_module_name');

     //        $module_type = '';
     //        $module_controller = '';
     //        $module_table = '';
     //        $module_identifier = '';
     //        $module_field = '';
     //        $module_status = '';
     //        $module_frontend_status = '';
     //        $module_backend_status = '';

     //        foreach ($modules_configuration as $key => $value) {
     //            if($value->controller == $frontend_module_name){

     //                $module_type = $value->type;
     //                $module_controller = $value->controller;
     //                $field_identifier = $value->identifier;
     //                $field_content = $value->field;
     //                $db_table = $value->dbtable;
     //                $module_status = $value->module_status;
     //                $module_frontend_status = $value->module_frontend_status;
     //                $module_backend_status = $value->module_backend_status;

     //                $back_url = array();
     //                foreach($_GET as $key => $value){
     //                    $back_url[$key] = $value;
     //                }
     //                $back_url = urlencode(serialize($back_url));


     //                $context = Context::getContext();
     //                $id_lang = $context->language->id;


					// $post = JsComposer::getJsControllerValues($db_table,$field_content,$field_identifier,Tools::getValue('val_identifier'),$id_lang);
					// // $post = new $module_controller((int) $val_identifier);



					// $content = $post->$field_content;
     //            }
     //        }
     //    }

		$post = $this->post();
                $vc_manager = vc_manager();
                // $params = array($field_identifier=>$val_identifier,'update'.$module_controller=>'');
		ob_start();
		?>
	<li class="vc_show-mobile vc_pull-right">
            <button data-url="<?php $vc_manager->esc_attr_e(JsComposer::backToAdminLink()) ?>"
				class="vc_btn vc_btn-default vc_btn-sm vc_navbar-btn vc_btn-backend-editor" id="vc_button-cancel"
				title="<?php echo $vc_manager->l("Cancel all changes and return to WP dashboard") ?>"><?php echo $vc_manager->l('Backend editor') ?></button>
		
		<button type="button" class="vc_btn vc_btn-primary vc_btn-sm vc_navbar-btn vc_btn-save" id="vc_button-update"
				title="<?php $vc_manager->esc_attr_e("Update") ?>"><?php echo $vc_manager->l('Update') ?></button>
		
	</li>
	<?php
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}
	public function getControlViewPost() {
                $vc_manager = vc_manager();

		return '<li class="vc_pull-right">'
		  .'<a href="'.$vc_manager->esc_attr(JsComposer::getExitVcLink()).'" class="vc_icon-btn vc_back-button"'
		  .' title="'. $vc_manager->esc_attr( $vc_manager->l( "Exit Visual Composer edit mode")).'"></a>'
		  .'</li>';
	}
}