<?php
class CategoryController extends CategoryControllerCore {
    public function initContent() {
        parent::initContent();
        $description = $this->category->description = JsComposer::do_shortcode($this->category->description);
        if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer')) {
            $this->context->smarty->assign(
                array(
                    'description_short' => JsComposer::do_shortcode($description),
                )
            );
            $this->category->description = JsComposer::do_shortcode($description);
        }
        if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode')) {
            
            $this->context->smarty->assign(
                array(
                    'description_short' => smartshortcode::do_shortcode($description),                
                )
            );
            $this->category->description = smartshortcode::do_shortcode($description);            
        }
    }
}
