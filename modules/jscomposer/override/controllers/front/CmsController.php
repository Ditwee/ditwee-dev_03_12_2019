<?php
class CmsController extends CmsControllerCore
{
    public function display()
	{
	  	    if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer'))
	  	    {
                if(isset($this->cms->content)){
	  	           $this->cms->content = JsComposer::do_shortcode( $this->cms->content );
	  	           if(vc_mode() === 'page_editable'){
                        $this->cms->content = call_user_func(JsComposer::$front_editor_actions['vc_content'],$this->cms->content);
                   }
                }
	  	    }
	  	    if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
	  	    {
                if(isset($this->cms->content)){
	  	           $this->cms->content = smartshortcode::do_shortcode( $this->cms->content );
                }
	  	    }

            return parent::display();

	}
}
