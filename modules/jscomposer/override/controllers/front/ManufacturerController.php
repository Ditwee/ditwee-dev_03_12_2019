<?php
class ManufacturerController extends ManufacturerControllerCore
{
    public function display()
	{
            if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer'))
            {
                   $this->manufacturer->description = JsComposer::do_shortcode( $this->manufacturer->description );
            }
            if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
            {
                   $this->manufacturer->description = smartshortcode::do_shortcode( $this->manufacturer->description );
            }

            return parent::display();
	}
}
