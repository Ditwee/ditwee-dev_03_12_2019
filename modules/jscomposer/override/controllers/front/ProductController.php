<?php
class ProductController extends ProductControllerCore
{
    public function display()
	{

            if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer'))
            {
                   $this->product->description = JsComposer::do_shortcode( $this->product->description );
            }
            if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
            {
                   $this->product->description = smartshortcode::do_shortcode( $this->product->description );
            }

            return parent::display();
	}
}
