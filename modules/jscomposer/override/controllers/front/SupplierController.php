<?php
class SupplierController extends SupplierControllerCore
{
    public function display()
	{
            
            if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer'))
            {
                   $this->supplier->description = JsComposer::do_shortcode( $this->supplier->description );
            }
            if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
            {
                   $this->supplier->description = smartshortcode::do_shortcode( $this->supplier->description );
            }
                    
            return parent::display();
                    
	}
}
