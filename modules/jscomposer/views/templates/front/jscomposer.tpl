{if is_array($results)}
{foreach from=$results item="res"}
	{if $res.content != ''}<span id="vc_inline-anchor_{$res.id_vccontentanywhere}" style="display:none !important;"></span>{$res.content}{/if}
	{assign var=vc_optname value="{Configuration::get("_wpb_vccaw_{$res.id_vccontentanywhere}_{Context::getcontext()->language->id}_css")}"}
	{if $vc_optname}
	<style type="text/css">
		{$vc_optname|html_entity_decode}
	</style>
	{/if}
{/foreach}
{else}
{$results}
{/if}
