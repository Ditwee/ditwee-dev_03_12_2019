<?php
if (!defined('_PS_VERSION_'))
  exit;
 
class med_controlemail_free extends Module
{
	
	public function __construct()
	{
		$this->name = 'med_controlemail_free';
		$this->tab = 'administration';
		$this->version = '1.1';
		$this->author = 'Mediacom87';
		$this->need_instance = 1;
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
		
		$this->_postErrors = array();
		
		parent::__construct();
		
		$this->displayName = $this->l('Free Control email domain');
		$this->description = $this->l('Check the fields of email addresses used by your customers to avoid fraud.');
		
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');
		
		$this->affiliate_id = '?ref=prestashop';
		$this->signup_url = 'http://www.block-disposable-email.com/cms/register/';
		$this->manage_url = 'http://www.block-disposable-email.com/cms/manage/';
		$this->credit_url = 'http://www.block-disposable-email.com/cms/manage/credits/';
		
		$this->key = Configuration::get('MED_CTRLE_API');
		$this->credits = $this->_checkCredit($this->key);
		
		if (!isset($this->key) or empty($this->key))
			$this->warning = $this->l('API key is not define! Take it:').' <a href="'.$this->signup_url.$this->affiliate_id.'" target="_blank"><img src="'.$this->_path.'img/key_notice.png" /></a>';
		if (isset($this->credits) and $this->credits < 20)
			$this->warning = $this->l('You use all your credits! Buy it:').' <a href="'.$this->credit_url.$this->affiliate_id.'" target="_blank"><img src="'.$this->_path.'img/cart.png" /></a>';
	}
	
    function install()
    {
    	if (!parent::install())
			return false;
		return true;
    }
	
	function uninstall()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}

	public function getContent($tab = 'AdminModules')
	{
		global $currentIndex, $cookie;
		
		$token = Tools::getAdminToken($tab.(int)Tab::getIdFromClassName($tab).(int)$cookie->id_employee);
		
		$output = '';
		
		if($this->key)
		{
			if ($this->credits == -1)
				$this->_postErrors[] = $this->l('API key is not valid!');
			elseif ($this->credits == -2)
				$this->_postErrors[] = $this->l('API key is not active!');
			elseif ($this->credits == -3)
				$this->_postErrors[] = $this->l('Error during checking your API key!');
		}
				
        if (Tools::isSubmit('saveconf'))
        {
			$output .= $this->_postProcess();
			
			if (!$this->_displayErrors())
				Tools::redirectAdmin($currentIndex.'&modulename='.$this->name.'&configure='.$this->name.'&conf=6&token='.$token);
        }
        
        if ($this->_displayErrors())
				$output .= $this->_displayErrors();
				
        $output .= $this->displayForm($token);
		return $output;
	}

	private function _displayErrors($tab = 'AdminModules')
	{
		$output = '';
		$nbErrors = sizeof($this->_postErrors);
		if ($nbErrors) {
			$output .= '
			<div class="alert error">
				<h3>'.($nbErrors > 1 ? $this->l('There are') : $this->l('There is')).' '.$nbErrors.' '.($nbErrors > 1 ? $this->l('errors') : $this->l('error')).'</h3>
				<ol>';
			foreach ($this->_postErrors AS $error)
				$output .= '<li>'.$error.'</li>';
			$output .= '
				</ol>
			</div>';
		}
		return $output;
	}
	
	private function _postProcess()
	{
		$key = trim(Tools::getValue('MED_CTRLE_API'));
		if($this->_checkKey($key))
		{
			if(!Configuration::updateValue('MED_CTRLE_API', $key))
				$this->_postErrors[] = $this->l('Error during the recording of the API key!');
		} else {
			$this->_postErrors[] = $this->l('API key is wrong!');
		}
	}

	public function displayForm($token)
	{
		global $cookie;
		return '
		<link rel="stylesheet" type="text/css" href="'.$this->_path.'views/css/'.$this->name.'.css" />
		
		<div class="infos"'.(($this->key and $this->credits >= 0)?' style="display:none"':'').'>
			<p><a href="'.$this->signup_url.$this->affiliate_id.'" title="'.$this->l('Above all things, sign up to get your API Key').'" target="_blank"><b>'.$this->l('Above all things, sign up to get your API Key').'</b></a></p>
		</div>
		
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset>
				<legend><a href="http://www.mediacom87.fr/?utm_source=module&utm_medium=cpc&utm_campaign='.$this->name.'"><img src="'.$this->_path.'logo.gif" alt="" /></a>'.$this->l('Settings').'</legend>
				
				<label>'.$this->l('API Key').'</label>
				<div class="margin-form">
					<input type="text" name="MED_CTRLE_API" value="'.$this->key.'" />'.((!$this->key or $this->credits < 0)?' <a href="'.$this->signup_url.$this->affiliate_id.'" title="'.$this->l('Above all things, sign up to get your API Key').'" target="_blank"><img src="'.$this->_path.'img/key_notice.png" /></a>': '').'
				</div>
				
				'.$this->_nbCredits($this->credits).'
				
				'.$this->_buyCredits($this->credits).'
				
				<center><input type="submit" name="saveconf" value="'.$this->l('Save').'" class="button" /></center>
			</fieldset>
		</form>
		
		<fieldset class="space">
				<legend><a href="https://www.paypal.com/fr/mrb/pal=LG5H4P9K8K6FC" target="_blank"><img src="'.$this->_path.'img/paypal-icon-16x16.png" alt="" /></a>'.$this->l('Donation').'</legend>
				<p><a href="http://www.mediacom87.fr/?utm_source=module&utm_medium=cpc&utm_campaign=zopim" target="_blank" title="'.$this->l('Mediacom87 WebAgency').'">'.$this->l('This module was developed and generously offered to the PrestaShop\'s community by Mediacom87 WebAgency specializing in supporting eCommerce.').'</a></p>
				<p><a href="http://www.mediacom87.fr/?utm_source=module&utm_medium=cpc&utm_campaign=zopim" target="_blank" title="'.$this->l('Mediacom87 WebAgency').'">'.$this->l('If you want to support the Mediacom87\'s process, you can do so by making a donation.').'</a></p>
				<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style="text-align:center">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="3EHD587GS6P46">
				<input type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - la solution de paiement en ligne la plus simple et la plus sécurisée !">
				<img alt="" border="0" src="https://www.paypalobjects.com/fr_FR/i/scr/pixel.gif" width="1" height="1">
				</form>
		</fieldset>
		
		<fieldset class="space">
				<legend><img src="'.$this->_path.'img/google-icon-16x16.png" alt="" height="16" width="16" /> '.$this->l('Ads').'</legend>
				<p><a href="http://www.mediacom87.fr/?utm_source=module&utm_medium=cpc&utm_campaign=skysabar" target="_blank" title="'.$this->l('Mediacom87 WebAgency').'">'.$this->l('You can also support our agency by clicking the advertising below').'.</a></p>
				<p style="text-align:center">
					<script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- Gratuit Control Email 728x90 -->
					<ins class="adsbygoogle"
					     style="display:inline-block;width:728px;height:90px"
					     data-ad-client="ca-pub-1663608442612102"
					     data-ad-slot="5686954358"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</p>
		</fieldset>
		';
	}
	
	function _checkKey($key)
	{
		$request = 'http://status.block-disposable-email.com/status/?apikey='.$key;
		$response = Tools::file_get_contents($request);
		$dea = json_decode($response);
		
		if ($dea->request_status == 'ok')
			if ($dea->apikeystatus == 'active')
				return true;
		return false;
	}
		
	public function _checkMail($email)
	{
		$split = explode('@', $email);
		$domain = array_pop($split);
		$request = 'http://check.block-disposable-email.com/easyapi/json/' . $this->key . '/' . $domain;
		
		$response = Tools::file_get_contents($request);
		
		$dea = json_decode($response);
		
		if ($dea->request_status == 'success')
			if ($dea->domain_status == 'block')
				return false;
			else
				return true;
		return true;
	}
	
	function _checkCredit($key)
	{
		$request = 'http://status.block-disposable-email.com/status/?apikey=' . $key;
		
		$response = Tools::file_get_contents($request);
		
		$dea = json_decode($response);
		
		if ($dea->request_status == 'ok')
			if ($dea->apikeystatus == 'active')
				return $dea->credits;
			else if ($dea->apikeystatus == 'fail')
				return -1;
			else
				return -2;
		else
			return -3;
	}
	
	function _nbCredits($credits)
	{
		if($credits >= 0 && isset($credits))
			return '
				<label>'.$this->l('Number of credits').'</label>
				<div class="margin-form">
					<p>'.$credits.'</p>
				</div>
			';
	}
	
	function _buyCredits($credits)
	{
		if ($credits < 150 && $credits >= 0 && isset($credits))
			return '
				<label>'.$this->l('Buy new credits').'</label>
				<div class="margin-form">
					<a href="'.$this->credit_url.$this->affiliate_id.'" target="_blank" title="'.$this->l('Buy new credits').'"><img src="'.$this->_path.'img/cart.png" /></a>
				</div>
			';
	}
}
?>