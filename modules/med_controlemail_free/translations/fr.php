<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_ff3f06dbaaa24a71d533f46d4327c469'] = 'Contrôle de sécurité des email';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_2699a39f25e53bfa4646359b4c4fdc66'] = 'Vérifiez les email clients lors de leur inscription pour éviter les noms de domaine temporaires ou poubelles';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_bb8956c67b82c7444a80c6b2433dd8b4'] = 'Êtes-vous sûr de que vouloir désinstaller ce module ?';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_d2b8eae1bbf2f01d74e5296fef31b814'] = 'La clé API n\'est pas enregistré, retrouvez là sur :';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_c81a66cb2610d2aef8b487deae71c7d4'] = 'Vous avez consommé tous vos crédits, refaire le plein :';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_0ecf7f8db770908b9b0aa0d6fde49b6d'] = 'Clé API n\'est pas valide !';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_3ac898fc55f36386f97171686738c0cc'] = 'Clé API n\'est pas actif !';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_c813f656850989360156aa87c4a219a7'] = 'Erreur lors de la vérification de votre clé API !';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_6357d3551190ec7e79371a8570121d3a'] = 'Il y a';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_4ce81305b7edb043d0a7a5c75cab17d0'] = 'Il y a';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_07213a0161f52846ab198be103b5ab43'] = 'erreurs';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_cb5e100e5a9a3e7f6d1fd97512215282'] = 'erreur';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_5760bea0954964cdf5c0b094837f9ecd'] = 'Erreur lors de l\'enregistrement de la clé API !';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_3b5c1c32b19c06be16d16b5200a48ab0'] = 'Avant tout choses, inscrivez-vous pour obtenir votre clé API';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_d876ff8da67c3731ae25d8335a4168b4'] = 'Clé API';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_4c5fb985ac3acf87b49c295c67baf03d'] = 'Don';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_03d2854f8732b5b60e72d15bea4d0f4b'] = 'Ce module a été développé et généreusement offert à la communauté de PrestaShop par Mediacom87 WebAgency spécialisée dans le soutien d\'eCommerce.';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_ce3d68e5912e70a5d9e9471324da8260'] = 'Si vous désirez aider Mediacom87 à continuer de proposer des modules gratuits vous pouvez faire un don';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_1aa2e2ff6972016c11c692bfb5c43909'] = 'Annonces';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_80a941307b59f5f17e1fbd31de94848d'] = 'Vous pouvez aussi soutenir notre Agence en cliquant sur la publicité ci-dessous';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_2140f4ff9796dc7621767c1e66dc0f29'] = 'Nombre de crédits';
$_MODULE['<{med_controlemail_free}prestashop>med_controlemail_free_ced3244021e6231e80fdfc79628d5635'] = 'Acheter de nouveaux crédits';
