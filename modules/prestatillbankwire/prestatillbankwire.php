<?php
class PrestatillBankwire extends PaymentModule
{
	public function __construct()
	{
		$this->name = 'prestatillbankwire';
		$this->tab = 'payements_gateways';
		$this->version = '0.1';
		$this->author = 'PrestaTill SAS';
		$this->bootstrap = 'true';
		parent::__construct();
		$this->displayName = $this->l('Module Payment Bankwire for Prestatill');
		$this->description = $this->l('Module Payment Bankwire for Prestatill');	
	}
	
	public function getTicketPaymentLabel($params = NULL)
	{
		return $this->l('Virement');
	}
	
	public function install()
	{
		if(
			!parent::install()
			|| !$this->registerHook('displayPayment')
			|| !$this->registerHook('displayPaymentReturn')
			//hooks Prestatill
			|| !$this->registerHook('displayPrestatillInjectJS')
			|| !$this->registerHook('displayPrestatillInjectCSS')
			|| !$this->registerHook('displayPrestatillPaymentButton')
			|| !$this->registerHook('displayPrestatillPaymentInterface')
			//installation d'un nouvau statut de commande
			|| !$this->_installOrderState()
		)
		{
			return false;
		}
		return true;
	
	}
	
	protected function _installOrderState()
	{
		//on check si ce status n'a pas DEJA était uinstallé
		if(!Configuration::hasKey('PRESTATILL_BANKWIRE_ID_ORDER_STATE'))
		{
			$context = Context::getContext();
			$id_lang = $context->language->id;
			
			$os = new OrderState(null,$id_lang);
			$os->send_email = false;
			$os->module_name = 'prestatillbankwire';
			$os->invoice = true;
			$os->color = '#44bb00';
			$os->logable = true;
			$os->shipped = true;
			$os->unremovable = false;
			$os->delivery = true;
			$os->hidden = false;
			$os->paid = true;
			$os->pdf_delivery = false;
			$os->pdf_invoice = true;
			$os->deleted = false;
			
			$os->name = 'Paiement accepté (virement)';
			$os->save();
			Configuration::updateValue('PRESTATILL_BANKWIRE_ID_ORDER_STATE',$os->id);
			return true;
		}
		return true;
	}
	
	public function getHookController($hook_name)
	{
		require_once(dirname(__FILE__).'/controllers/hook/'.$hook_name.'.php');
		$controller_name = $this->name.$hook_name.'Controller';
		$controller  = new $controller_name($this, __FILE__, $this->_path);
		return $controller;
	}
	
	public function hookDisplayPayment ($params)
	{
		$controller = $this->getHookController('displayPayment');
		return $controller->run($params);
			
	}
	

	/**
	 * Injection JS dans le header de la caisse
	 */
	public function hookDisplayPrestatillInjectJS($params)
	{
		return '<script src="'._MODULE_DIR_.$this->name.'/js/prestatill.js"></script>';
	}
	
	/**
	 * Injection CSS dans le header de la caisse
	 */
	public function hookDisplayPrestatillInjectCSS($params)
	{
		return '<link  href="'._MODULE_DIR_.$this->name.'/css/prestatill.css" rel="stylesheet" type="text/css" media="all" />';		
	}	
	
	/**
	 * Affichage du boutton de paiement
	 */
	public function hookDisplayPrestatillPaymentButton($params)
	{
		//$this->context->smarty->assign($assigns);
	    $html = $this->createTemplate('prestatill_payment_button.tpl')->fetch();
        return $html;
	}	
	
	/**
	 * Affichage d'une interface de saisie du montant (non obligatoire, si pas d'interface de saisie : exemple CB)
	 * l'interface doit soit d'une colonne afin d'etre compatible avec la vue paiement multiple
	 */
	public function hookDisplayPrestatillPaymentInterface($params)
	{	
	    $html = $this->createTemplate('prestatill_payment_interface.tpl')->fetch();
        return $html;
	}
	
	

    //surcharge pour tester aussi le dossier view dans le module
    public function createTemplate($tpl_name,$type = 'hook')
    {
    	//d(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    	//d(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    		
    	
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        return parent::createTemplate($tpl_name);
    }

    public function getTplPath($tpl_name)
    {
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name;
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name;
        }
    }
	

	
}