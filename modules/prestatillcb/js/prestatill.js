$(function(){

	function prestatillcb(caisse_ref) {
		var module = this;
		var module_name = 'prestatillcb';
		
		//propriétés du module
		this.total = 0;
		this.buffer = '';
		this.with_repaid = true;
		
		/**
		 * Indique si une virgule "silencieuse" existe à la fin de la saisie (en attendant une saiie d'un nombre) 
		 */
		this.has_end_point = false;
		
		/**
		 * Indique si il y a déjà une virgule dans la saisie en cours 
		 */
		this.has_point = false;
		
    	
		this.sendAmount = function() {
			PRESTATILL_CAISSE.payment.changePaymentAmount(this.total, this.with_repaid);
		};
		
		this.init = function() {		
			var tot_to_pay = $('#keepToHaveToPay').html();
			var tot_to_pay_res = tot_to_pay.replace(" ","");
			this.total = parseFloat('0'+tot_to_pay_res);	
				
			this.buffer = '';
			PRESTATILL_CAISSE.payment.changePaymentAmount(this.total);	
		};		
		
		$('.prestatill_payment_interface[data-module-name="'+module_name+'"]').on('click tap','.numeric_keyboard>span',function(e){
			
			var key_code = $(this).html();
			
			switch(key_code)
			{
				case '.':
				case ',':
					if(!module.has_end_point)
					{
						module.has_end_point = true;
					}
					module.has_point = true;
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					if(module.has_point && module.has_end_point)
					{
						module.buffer += '.';						
					}
					
					var test = module.buffer.split('.');
					if(!module.has_point || (module.has_point && test[1] != undefined && test[1].length<2))
					{										
						module.buffer += key_code;
						module.has_end_point = false;
					}
					break;
				case 'C':
				case 'R':
					if(module.buffer.length >0)
					{
						module.buffer = '';
					}
					break;
			}
			
			var value = module.buffer;
			if(value == '')
			{
				value = 0;
			}
			module.total = parseFloat(value);
			module.sendAmount();
					
			//console.log(module);
		});
		
		
	};
	
	/*************
	Enregistrement du module de payment dans ll'instance de la caisse
	**************/	
	PRESTATILL_CAISSE.payment.modules.push(new prestatillcb());	
});