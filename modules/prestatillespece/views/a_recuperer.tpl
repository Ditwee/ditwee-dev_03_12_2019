<div id="payByNoCash" style="display:none;">Veuillez ouvrir la caisse pour pouvoir encaisser en liquide</div>
  <div id="payByCash" id="paymentMode0" class="button paymentButton {$paymentsAvailable[0]['class']}" data-id-order-state="{$paymentsAvailable[0]['id_order_state']}" data-module-name="{$paymentsAvailable[0]['module']}">
      <i class="fa fa-money"></i>
      <div class="label">Espèces</div>
      <div class="calc" id="calcPayByCash">
      <div id="calc-keyboard" class="numeric_keyboard">
          <span class="display" id="cashMessage"></span>
          <span class="double display">Reçu <span id="cashReceived"></span> €</span>
          <span class="calc-key-c">C</span>
          <span>7</span>
          <span>8</span>
          <span>9</span>
          <span>4</span>
          <span>5</span>
          <span>6</span>
          <span>1</span>
          <span>2</span>
          <span>3</span>
          <span>0</span>
          <span class="double">.</span>
      </div>
  </div>
</div>   