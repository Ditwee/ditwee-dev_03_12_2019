<div class="prestatill_payment_interface" data-module-name="prestatillespece">
  <div class="cash_open_false danger box" style="__display:none;">
    Veuillez d'abord ouvrir votre caisse et saisir votre fond de caisse avant de pouvoir encaisser en espèces.
  </div>
  <div class="cash_open_true">
  	<div class="numeric_keyboard">
  		<span>7</span>
  		<span>8</span>
  		<span>9</span>
  		<span>4</span>
  		<span>5</span>
  		<span>6</span>
  		<span>1</span>
  		<span>2</span>
  		<span>3</span>
  		<span>0</span>
      <span>.</span>
      <span>C</span>
  	</div>
	</div>
</div>
