<?php

require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');

$action = Tools::getValue('action');

include_once(dirname(__FILE__).'/prestatillexportcompta.php');

$status = 'success';
$message = '';

switch ($action) 
{
		
  case 'myMethod' :
	  
	  $message = PrestatillExportCompta::getData(Tools::getValue('dateFrom'), Tools::getValue('dateTo'), Tools::getValue('id_pointshop'));
	  
    break;
	
  default:
		$status = 'error';
		$message = 'Unknown parameters!';
	exit;
}

$response = new stdClass();
$response->status = $status;
$response->message = $message;
$response->action = $action;
echo json_encode($response);