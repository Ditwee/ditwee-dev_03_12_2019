<?php

class PrestatillFinancialReport extends ObjectModel {
    	
    public $id_financial_report;
	
	/* SINCE 2.0 */
	public $id_service = 1;

	public $id_pointshop;
	
	public $nb_sales;
	
	public $total_tax_incl;
	
	public $total_tax_excl;
	
	public $total_vat;
	
	public $total_payment;
	
	public $total_discounts_tax_incl = 0.000000;
	
	public $total_discounts_tax_excl = 0.000000;
	
	public $total_shipping_tax_incl = 0.000000;
	
	public $total_shipping_tax_excl = 0.000000;
	
	public $conversion_rate;
	
	public $id_currency;
	
	public $day;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'financial_report',
		'primary' => 'id_financial_report',
		'multilang' => false,
		'fields' => array(
			'id_service' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_pointshop' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'nb_sales' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'total_tax_incl' =>				array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_tax_excl' =>				array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_vat' =>					array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_payment' =>				array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_discounts_tax_incl' =>	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_discounts_tax_excl' =>	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_shipping_tax_incl' =>	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_shipping_tax_excl' =>	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'conversion_rate' =>			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'id_currency' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'day' => 					array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
		),
	);
	
	public function __construct($id = null, $id_lang = null)
	{
		parent::__construct($id, $id_lang);
	}
	
	public static function getOrdersDate($date_from = false, $date_to = false)
	{
		if($date_from == false)
			$date_from = date('Y-m-d');	
		
		if($date_to == false)
			$date_to = date('Y-m-d');
			
		$query = 'SELECT LEFT(o.date_add, 10) as date, 
						o.id_order
					FROM `'._DB_PREFIX_.'orders` o
					WHERE o.date_add BETWEEN "'.pSQL($date_from).' 00:00:00" AND "'.pSQL($date_to).' 23:59:59"';

		$query .= ' GROUP BY date';
		
		$query .= ' ORDER BY date';
		
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
		
		return $result;
	}
	
	public static function createDaylyFinancialReport($date_from = false, $date_to = false, $id_pointshops = array())
	{
		// IMPORTANT : on ne génère jamais un rapport le jour même, programmer la tache cron à partir de 00:01 le landemain	
		$yesterday = date('Y-m-d', strtotime("yesterday 12:00"));
		
		if($date_from == false || $date_from == date('Y-m-d'))
			$date_from = $yesterday;	
		
		if($date_to == false || $date_to == date('Y-m-d'))
			$date_to = $yesterday;
			
		if(empty($id_pointshops))
		{
			$id_pointshops = array(0 => 0);
		
			if(Configuration::get('ACAISSE_last_cache_generation_pricelist') != NULL)	
			{
				$all_pointshops = PrestatillExportCompta::getAllPointshops(true);
				foreach($all_pointshops as $key => $pointshop)
				{
					$id_pointshops[] = $pointshop['id_a_caisse_pointshop'];
				}
					
			}
		}	
		
			
			
		foreach($id_pointshops as $id_pointshop)
		{
			//echo("titi");	
			//echo($id_pointshop);
			$totalCAbyVAT = PrestatillFinancialReport::getCAbyVAT($date_from, $date_to,'day', $id_pointshop);	// RENDRE DYNAMIQUE	
			//d($totalCAbyVAT);
			// On récupère le total payé pour chaque point de vente
			$totalCAbyPM = PrestatillFinancialReport::getCAbyPaymentMethod($date_from, $date_to, 'day', $id_pointshop, false);
			//d($totalCAbyPM);
			foreach($totalCAbyVAT['date'] as $day => $infos)
			{
				// On vérifie si un rapport existe pour la date et le pointshop, si non, on le créé. On ne met jamais à jour les rapports créés
				$id_existing_report = PrestatillFinancialReport::ReportExists($day, $id_pointshop, null, true);
				$report = new PrestatillFinancialReport($id_existing_report);
				
				$total_payment = array();
				if(Validate::isLoadedObject($report))
				{
					//On fait rien :)	
					//return false;
				}
				else 
				{
					$total_payment['day'] = 0;
					$total_payment['balance'] = array();	
					// On calcule le total des paiements	
					foreach($totalCAbyPM['details'][$day] as $payment)
					{
						// On distingue les paiements du jour des paiements des commandes anciennes (soldes...)	
						if(substr($payment['date_order'],0,10) === $day)
						{
							$total_payment['day'] += $payment['sales'];
						}
						else 
						{
							$total_payment['balance'][] = $payment;
						}
					}
						
					$report->id_pointshop 				= $id_pointshop; // A dynamiser
					$report->nb_sales 					= (int)$infos['product_qty'];
					$report->total_tax_incl 			= 0;//(float)number_format($infos['total_ttc'],6,'.','');
					$report->total_tax_excl 			= 0;//(float)number_format($infos['total_ht'],6,'.','');
					
					$total_vat 							= $infos['total_ttc']-$infos['total_ht'];
					
					$report->total_vat 					= 0;//(float)number_format($total_vat,6,'.',''); 
					$report->total_payment 				= $total_payment['day']; // REQUETE DES PAIEMENTS AVEC LES REF COMMANDES
					$report->total_discounts_tax_incl 	= 0;//(float)number_format($infos['total_discounts_ttc'],6,'.','');
					$report->total_discounts_tax_excl 	= 0;//(float)number_format($infos['total_discounts_ht'],6,'.','');
					$report->total_shipping_tax_incl 	= 0;//(float)number_format($infos['total_shipping_tax_incl'],6,'.','');
					$report->total_shipping_tax_excl 	= 0;//(float)number_format($infos['total_shipping_tax_excl'],6,'.','');
					$report->conversion_rate 			= 0.000000;
					$report->id_currency 				= 2; // A dynamiser
					$report->day	 					= $day;
					
					$report->id_service 				= 1; // Lorsqu'on génère des ancies rapports
					
					if($report->save())
					{
						foreach($totalCAbyVAT['details'] as $detail)
						{
						 	if($detail['date'] === $day)
							{
								$report_detail 								= new PrestatillFinancialReportDetail();
								$report_detail->id_financial_report 		= $report->id;
								$report_detail->id_service					= $report->id_service;
								$report_detail->id_order 					= $detail['id_order'];
								$report_detail->order_reference 			= $detail['reference'];
								$report_detail->date_order_detail 			= $detail['date_add'];
								
								$tpti 										= $detail['tptinc'];//-$detail['total_discounts_tax_incl'];
								$tpte 										= $detail['tptexc'];//-$detail['total_discounts_tax_excl'];
								
								//$total_vat_detail 							= $tpti-$tpte;
								//$report_detail->total_price_tax_incl 		= $tpti>0?(float)number_format($tpti,6,'.',''):(float)number_format(0,6,'.','');
								//$report_detail->total_price_tax_excl 		= $tpte>0?(float)number_format($tpte,6,'.',''):(float)number_format(0,6,'.','');
								//$report_detail->total_vat 					= (float)number_format($total_vat_detail,6,'.','');
								//$report_detail->total_discounts_tax_incl 	= (float)number_format($detail['total_discounts_tax_incl'],6,'.','');
								//$report_detail->total_discounts_tax_excl 	= (float)number_format($detail['total_discounts_tax_excl'],6,'.','');
								//$report_detail->total_shipping_tax_incl 	= (float)number_format($detail['total_shipping_tax_incl'],6,'.','');
								//$report_detail->total_shipping_tax_excl 	= (float)number_format($detail['total_shipping_tax_excl'],6,'.','');
								$report_detail->product_qty 				= $detail['product_qty'];
								$report_detail->id_currency 				= 2; // A dynamiser
								
								/*
								 * SI ON EST DANS LE CAS D'UNE CARTE CADEAU, ON DEDUIT LE CA DU PRODUIT "CARTE CADEAU"
								 **/
								$info_product = new Product((int)$detail['product_id']);
								if(Validate::isLoadedObject($info_product) && $info_product->prestatillgiftcard_amount > 0)
								{
									$tpti 										= $detail['tptinc']-($info_product->prestatillgiftcard_amount*$detail['product_qty']);
									$tpte 										= $detail['tptexc']-($info_product->prestatillgiftcard_amount*$detail['product_qty']);
								}
								
								$total_vat_detail 							= $tpti-$tpte;
								$report_detail->total_price_tax_incl 		= $tpti>0?(float)number_format($tpti,6,'.',''):(float)number_format(0,6,'.','');
								$report_detail->total_price_tax_excl 		= $tpte>0?(float)number_format($tpte,6,'.',''):(float)number_format(0,6,'.','');
								$report_detail->total_vat 					= (float)number_format($total_vat_detail,6,'.','');
								
								// On répercute les remises par lignes sur les rapports (test : sur 2 chiffres pour éviter les erreurs d'arrondis)
								if(number_format($detail['original_product_price'],2,'.','') > number_format($detail['tptexc'],2,'.','') && $tpte > 0)
								{
									$report_detail->total_discounts_tax_excl 	= number_format($detail['original_product_price'] - $tpte,6,'.','');
									$report_detail->total_discounts_tax_incl 	= number_format($report_detail->total_discounts_tax_excl * (round($tpti / $tpte,3)),6,'.','');
									/*print_r(array(
										'cond_1' =>number_format($detail['original_product_price'],2,'.',''),
										'cond_1_bis' => number_format($detail['tptexc'],2,'.',''),
										'cond_2' => $tpte
									)
										);*/
								} 
								
								$report_detail->total_shipping_tax_incl 	= number_format($detail['total_shipping_tax_incl'],6,'.','');
								$report_detail->total_shipping_tax_excl 	= number_format($detail['total_shipping_tax_excl'],6,'.','');
								
								// On met à jour les totaux des rapports
								$report->total_discounts_tax_incl 	+= (float)number_format($report_detail->total_discounts_tax_incl,6,'.','');
								$report->total_discounts_tax_excl 	+= (float)number_format($report_detail->total_discounts_tax_excl,6,'.','');
								
								// AJOUT DES PORTS
								$report->total_shipping_tax_incl 	+= (float)number_format($report_detail->total_shipping_tax_incl,6,'.','');
								$report->total_shipping_tax_excl 	+= (float)number_format($report_detail->total_shipping_tax_excl,6,'.','');
								
								$report->total_tax_incl 			+= (float)number_format($report_detail->total_price_tax_incl,6,'.','');
								$report->total_tax_excl 			+= (float)number_format($report_detail->total_price_tax_excl,6,'.','');
								$report->total_vat					+= (float)number_format($report_detail->total_vat,6,'.','');
								
								
								$report->save();
								
								
								
								// On récupère la liste des paiements effectués ce jour pour cette commande et on la serialise
								$payments = PrestatillFinancialReportPayment::getPayments($day, $detail['reference']);
								$report_detail->payments = serialize($payments);
								
								$report_detail->save();
							}
						}
						
						// On enregistre les paiements "reliquat" pour pouvoir les afficher sous le tableau principale
						if(!empty($total_payment['balance']))
						{
							foreach($total_payment['balance'] as $other)
							{
								$report_detail 								= new PrestatillFinancialReportDetail();
								$report_detail->id_financial_report 		= $report->id;
								$report_detail->id_order 					= 0;
								$report_detail->order_reference 			= $other['order_reference'];
								$report_detail->date_order_detail 			= $other['date_order'];
								$report_detail->total_price_tax_incl 		= (float)number_format(0,6,'.','');
								$report_detail->total_price_tax_excl 		= (float)number_format(0,6,'.','');
								$report_detail->total_vat 					= (float)number_format(0,6,'.','');
								$report_detail->total_discounts_tax_incl 	= (float)number_format(0,6,'.','');
								$report_detail->total_discounts_tax_excl 	= (float)number_format(0,6,'.','');
								$report_detail->total_shipping_tax_incl 	= (float)number_format(0,6,'.','');
								$report_detail->total_shipping_tax_excl 	= (float)number_format(0,6,'.','');
								$report_detail->product_qty 				= 0;
								$report_detail->id_currency 				= 2; // A dynamiser
								
								// On récupère la liste des paiements effectués ce jour pour cette commande et on la serialise
								$report_detail->payments 					= serialize($other);
								
								$report_detail->save();
								
								// On enregistre le paiement
								$report_payment 						= new PrestatillFinancialReportPayment();
								$report_payment->id_financial_report 	= $report->id;
								$report_payment->id_service				= $report->id_service;
								$report_payment->payment_method 		= $other['payment_method']!==''?$other['payment_method']:'Non renseigné';
								$report_payment->amount 				= (float)number_format($other['sales'],6,'.','');
								$report_payment->balance 				= 1;
								$report_payment->save();
							}
						}
						
						// On enregistre les totaux de TVA par type sur la table concernée
						foreach($infos['vat'] as $id_tax => $total_vat)
						{
							$report_vat 						= new PrestatillFinancialReportVat();
							$report_vat->id_report				= $report->id_service;
							$report_vat->id_financial_report 	= $report->id;
							$report_vat->id_tax_rules_group 	= (int)$id_tax;
							
							$tax 					= PrestatillFinancialReportVat::getTaxName($id_tax);
							$report_vat->tax_name 	= $tax['name'];
							$report_vat->amount 	= $total_vat['total_tax']>0?(float)number_format($total_vat['total_tax'],6,'.',''):(float)number_format(0,6,'.','');
							$report_vat->total_ht 	= $total_vat['total_ht']>0?(float)number_format($total_vat['total_ht'],6,'.',''):(float)number_format(0,6,'.','');
							
							$report_vat->save();
						}
						//d($totalCAbyPM['details'][$day]);
						// On enresgistre les informations de paiement
						foreach($totalCAbyPM['details'][$day] as $payment)
						{
							$report_payment 						= new PrestatillFinancialReportPayment();
							$report_payment->id_financial_report 	= $report->id;
							$report_payment->payment_method 		= $payment['payment_method']!==''?$payment['payment_method']:'Non renseigné';;
							$report_payment->amount 				= (float)number_format($payment['sales'],6,'.','');
							$report_payment->balance 				= 0;
							$report_payment->save();
						} 
						
					}
					else
					{
						return false;
					}
				}
			}
		}
	}

	public static function generateFinancialReportForOrder($date_from, $date_to, $id_pointshop = 0, $id_order = null)
	{
		
		$totalCAbyVAT = PrestatillFinancialReport::getCAbyVAT($date_from, $date_to,'day', $id_pointshop, false, $id_order);	// RENDRE DYNAMIQUE	
		//d($totalCAbyVAT);
		
		// On récupère le total payé pour chaque point de vente
		$totalCAbyPM = PrestatillFinancialReport::getCAbyPaymentMethod($date_from, $date_to, 'day', $id_pointshop, false, $id_order);
		
		foreach($totalCAbyVAT['date'] as $day => $infos)
		{
			$date = $day;	
			// On récupère le dernier service créé du jour (s'il existe)
			$id_service = PrestatillFinancialStates::ServiceExists($day, $id_pointshop);
			
			if($id_service == false && $id_pointshop > 0)
			{
				$old_date = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT MAX(day) FROM `'._DB_PREFIX_.'financial_states`');
				
				// Si on trouve une date, on vérifie si le dernier état du jour est "caisse ouverte"
				if($old_date != false)
				{
					$other_id_sate = PrestatillFinancialStates::ServiceExists($old_date, $id_pointshop);
					
						// On ajoute les informations sur le dernier rapport car la caisse est toujours ouverte	
						$date = $old_date;
						$id_service = $other_id_sate;
				}
			}
			
			// On vérifie si un rapport existe pour la date et le pointshop, si non, on le créé. On ne met jamais à jour les rapports créés
			$id_existing_report = PrestatillFinancialReport::ReportExists($date, $id_pointshop);
			
			$total_payment['day'] = 0;
			$total_payment['balance'] = array();	
			// On calcule le total des paiements	
			foreach($totalCAbyPM['details'][$day] as $payment)
			{
				// On distingue les paiements du jour des paiements des commandes anciennes (soldes...)	
				if(substr($payment['date_order'],0,10) === $day)
				{
					$total_payment['day'] += $payment['sales'];
				}
				else 
				{
					$total_payment['balance'][] = $payment;
				}
			}
			
			if($id_existing_report == false)
			{
				$report = new PrestatillFinancialReport();	
				$report->id_pointshop 				= $id_pointshop; // A dynamiser
				$report->id_service					= 1;
				$report->nb_sales 					= (int)$infos['product_qty'];
				$report->total_tax_incl 			= (float)number_format(0,6,'.','');
				$report->total_tax_excl 			= (float)number_format(0,6,'.','');
				
				$total_vat 							= $infos['total_ttc']-$infos['total_ht'];
				
				$report->total_vat 					= (float)number_format(0,6,'.','');
				$report->total_payment 				= 0;
				$report->total_discounts_tax_incl 	= (float)number_format(0,6,'.','');//(float)number_format($infos['total_discounts_ttc'],6,'.','');
				$report->total_discounts_tax_excl 	= (float)number_format(0,6,'.',''); //(float)number_format($infos['total_discounts_ht'],6,'.','');
				
				// AJOUT DES PORTS
				$report->total_shipping_tax_incl 	= (float)number_format(0,6,'.','');
				$report->total_shipping_tax_excl 	= (float)number_format(0,6,'.','');
				
				$report->conversion_rate 			= 0.000000;
				$report->id_currency 				= 2; // A dynamiser
				$report->day	 					= $day;
			
				$report->save();
				
				$id_existing_report = $report->id;
			}
			else
			{
				$report = new PrestatillFinancialReport($id_existing_report);
				
				if(!Validate::isLoadedObject($report))
					return false;
			
				// On incrémente les informations du rapport
				$report->nb_sales 					+= (int)$infos['product_qty'];
				$report->save();
				
			}
				
			$total_payment['day'] = 0;
			$total_payment['balance'] = array();	
			// On calcule le total des paiements	
			foreach($totalCAbyPM['details'][$day] as $payment)
			{
				// On distingue les paiements du jour des paiements des commandes anciennes (soldes...)	
				if(substr($payment['date_order'],0,10) === $day)
				{
					$total_payment['day'] += $payment['sales'];
				}
				else 
				{
					$total_payment['balance'][] = $payment;
				}
			}
	
			foreach($totalCAbyVAT['details'] as $detail)
			{
			 	if($detail['date'] === $day)
				{
					$report_detail 								= new PrestatillFinancialReportDetail();
					$report_detail->id_financial_report 		= $report->id;
					$report_detail->id_service					= $report->id_service;
					$report_detail->id_order 					= $detail['id_order'];
					$report_detail->order_reference 			= $detail['reference'];
					$report_detail->date_order_detail 			= $detail['date_add'];
					
					$tpti 										= $detail['tptinc'];//-$detail['total_discounts_tax_incl'];
					$tpte 										= $detail['tptexc'];//-$detail['total_discounts_tax_excl'];
					
					/*
					 * SI ON EST DANS LE CAS D'UNE CARTE CADEAU, ON DEDUIT LE CA DU PRODUIT "CARTE CADEAU"
					 **/
					$info_product = new Product((int)$detail['product_id']);
					if(Validate::isLoadedObject($info_product) && $info_product->prestatillgiftcard_amount > 0)
					{
						$tpti 										= $detail['tptinc']-($info_product->prestatillgiftcard_amount*$detail['product_qty']);
						$tpte 										= $detail['tptexc']-($info_product->prestatillgiftcard_amount*$detail['product_qty']);
					}
					
					$total_vat_detail 							= $tpti-$tpte;
					$report_detail->total_price_tax_incl 		= $tpti>0?(float)number_format($tpti,6,'.',''):(float)number_format(0,6,'.','');
					$report_detail->total_price_tax_excl 		= $tpte>0?(float)number_format($tpte,6,'.',''):(float)number_format(0,6,'.','');
					$report_detail->total_vat 					= (float)number_format($total_vat_detail,6,'.','');
					
					// On répercute les remises par lignes sur les rapports (test : sur 2 chiffres pour éviter les erreurs d'arrondis)
					if(number_format($detail['original_product_price'],2,'.','') > number_format($detail['tptexc'],2,'.','') && $tpte > 0)
					{
						$report_detail->total_discounts_tax_excl 	= number_format($detail['original_product_price'] - $tpte,6,'.','');
						$report_detail->total_discounts_tax_incl 	= number_format($report_detail->total_discounts_tax_excl * (round($tpti / $tpte,3)),6,'.','');
						/*print_r(array(
							'cond_1' =>number_format($detail['original_product_price'],2,'.',''),
							'cond_1_bis' => number_format($detail['tptexc'],2,'.',''),
							'cond_2' => $tpte
						)
							);*/
					} 
					
					$report_detail->total_shipping_tax_incl 	= number_format($detail['total_shipping_tax_incl'],6,'.','');
					$report_detail->total_shipping_tax_excl 	= number_format($detail['total_shipping_tax_excl'],6,'.','');
					
					// On met à jour les totaux des rapports
					$report->total_discounts_tax_incl 	+= (float)number_format($report_detail->total_discounts_tax_incl,6,'.','');
					$report->total_discounts_tax_excl 	+= (float)number_format($report_detail->total_discounts_tax_excl,6,'.','');
					
					// AJOUT DES PORTS
					$report->total_shipping_tax_incl 	+= (float)number_format($report_detail->total_shipping_tax_incl,6,'.','');
					$report->total_shipping_tax_excl 	+= (float)number_format($report_detail->total_shipping_tax_excl,6,'.','');
					
					$report->total_tax_incl 			+= (float)number_format($report_detail->total_price_tax_incl,6,'.','');
					$report->total_tax_excl 			+= (float)number_format($report_detail->total_price_tax_excl,6,'.','');
					$report->total_vat					+= (float)number_format($report_detail->total_vat,6,'.','');
					
					if($id_pointshop == 0)
					{
						$payments = PrestatillFinancialReportPayment::getPayments($day, $detail['reference']);
						$report_detail->payments = serialize($payments);
					}
					else
					{
						$report_detail->payments = 0;
					}
					
					$report->save();
					
					// On check l'unité du produit pour voir son ratio

					$unit_scale = 1;
					if(Validate::isLoadedObject($info_product) && $id_pointshop > 0)
					{
						$unit = ACaisseUnit::getUnitFromUnit($info_product->unity);
						
						if(Validate::isLoadedObject($unit))
						{
							$unit_scale = $unit->unit_scale;
						}
					}

					$report_detail->product_qty 				= (float)number_format($detail['product_qty']/$unit_scale,6,'.','');
					$report_detail->id_currency 				= 2; // A dynamiser
					
					//print_r($report_detail);
					$report_detail->save();
				}
			}
			//d('titi');
			// On enregistre les totaux de TVA par type sur la table concernée
			foreach($infos['vat'] as $id_tax => $total_vat)
			{
					
				$id_report_vat_exists = PrestatillFinancialReportVat::ReportVatExists($report->id, (int)$id_tax, $id_service); 	
				
				if($id_report_vat_exists == false)
				{
					$report_vat 						= new PrestatillFinancialReportVat($id_report_vat_exists);

					$report_vat->id_financial_report 	= $report->id;
					$report_vat->id_service				= $report->id_service;
					$report_vat->id_tax_rules_group 	= (int)$id_tax;
					
					$tax 					= PrestatillFinancialReportVat::getTaxName($id_tax);
					$report_vat->tax_name 	= $tax['name'];
					$report_vat->amount 	= $total_vat['total_tax']>0?(float)number_format($total_vat['total_tax'],6,'.',''):(float)number_format(0,6,'.','');
					$report_vat->total_ht 	= $total_vat['total_ht']>0?(float)number_format($total_vat['total_ht'],6,'.',''):(float)number_format(0,6,'.','');
				}
				else 
				{
					$report_vat 			= new PrestatillFinancialReportVat($id_report_vat_exists);
					
					$tax 					= PrestatillFinancialReportVat::getTaxName($id_tax);
					$report_vat->amount 	+= $total_vat['total_tax']>0?(float)number_format($total_vat['total_tax'],6,'.',''):(float)number_format(0,6,'.','');
					$report_vat->total_ht 	+= $total_vat['total_ht']>0?(float)number_format($total_vat['total_ht'],6,'.',''):(float)number_format(0,6,'.','');
				}	
				
				$report_vat->save();
				
			}
			
			// On enregistre les paiement uniquement en cas de commande internet
			/*
			if($id_pointshop == 0)
			{
				// On enresgistre les informations de paiement
				foreach($totalCAbyPM['date'][$day] as $pm => $payment)
				{
					$report_payment 						= new PrestatillFinancialReportPayment();
					$report_payment->id_financial_report 	= $report->id;
					$report_payment->payment_method 		= $pm!==''?$pm:'Non renseigné';;
					$report_payment->amount 				= (float)number_format($payment,6,'.','');
					$report_payment->balance 				= 0;
					$report_payment->save();

					// On met à jour la liste des paiements dans le détail du rapport	
					$payments = PrestatillFinancialReportPayment::getPayments($day, $detail['reference']);
					$report_detail->payments = serialize($payments);
					$report_detail->save();
				
					$report->total_payment 				+= (float)number_format($payment,6,'.',''); // REQUETE DES PAIEMENTS AVEC LES REF COMMANDES
					$report->save();
					
				} 
			}	
			*/
		}
	}

	public static function registerPayment($day, $module_name, $label, $payment_amount, $id_pointshop = 0, $id_order = null, $order_reference = null, $params = array())
	{
			
		if($id_order !== null)
		{
			$order = new Order($id_order);
		}
		else
		{
			$id_order = self::getOrderByRefrence($order_reference);	
			if($id_order !== false)
			{
				$order = new Order($id_order);
			}
		}
		
		$good_date = $day;
		$is_reliquat = false;
		
		if($id_pointshop == 0)
		{
			$id_service = 1;
			if($good_date != substr($order->date_add, 0, 10))
			{
				$is_reliquat = true;
			}
		}
		else 
		{
			// On récupère le dernier service créé du jour (s'il existe)
			$id_service = PrestatillFinancialStates::ServiceExists($day, $id_pointshop);
		}
		
		// Si aucune service n'est renvoyé, on vérifie si une caisse de la veille est encore ouverte
		// On vérifie si un état existe déjà pour la journée, si non on vérifie la veille si la caisse était toujours ouverte
		if($id_service == false)
		{
			$old_date = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT MAX(day) FROM `'._DB_PREFIX_.'financial_states`');
			
			// Si on trouve une date, on vérifie si le dernier état du jour est "caisse ouverte"
			if($old_date != false)
			{
				$other_id_sate = PrestatillFinancialStates::ServiceExists($old_date, $id_pointshop);
				
				// On ajoute les informations sur le dernier rapport car la caisse est toujours ouverte	
				$day = $old_date;
				$id_service = $other_id_sate;
				$is_reliquat = true;
			}
		}
	
		
		$id_existing_report = PrestatillFinancialReport::ReportExists($day, $id_pointshop);	
		//d(array($id_existing_report, $day, $id_pointshop, $module_name, $params));
		
		if($id_existing_report == false)
		{	
			$report = new PrestatillFinancialReport();	
			$report->id_pointshop 				= $id_pointshop; // A dynamiser
			$report_->id_service				= 1;
			$report->nb_sales 					= 0;
			$report->total_tax_incl 			= (float)number_format(0,6,'.','');
			$report->total_tax_excl 			= (float)number_format(0,6,'.','');
			
			$total_vat 							= (float)number_format(0,6,'.','');
			
			$report->total_vat 					= (float)number_format(0,6,'.',''); 
			$report->total_payment 				= (float)number_format($payment_amount,6,'.','');
			$report->total_discounts_tax_incl 	= (float)number_format(0,6,'.','');//(float)number_format($infos['total_discounts_ttc'],6,'.','');
			$report->total_discounts_tax_excl 	= (float)number_format(0,6,'.',''); //(float)number_format($infos['total_discounts_ht'],6,'.','');
			
			// AJOUT DES PORTS
			$report->total_shipping_tax_incl 	= (float)number_format(0,6,'.','');//(float)number_format($infos['total_discounts_ttc'],6,'.','');
			$report->total_shipping_tax_excl 	= (float)number_format(0,6,'.',''); //(float)number_format($infos['total_discounts_ht'],6,'.','');
			
			$report->conversion_rate 			= 0.000000;
			$report->id_currency 				= 2; // A dynamiser
			$report->day	 					= $day;
		
			$report->save();
			
			$id_existing_report = $report->id;
		}
		else
		{
			$report = new PrestatillFinancialReport($id_existing_report);
			
			if(!Validate::isLoadedObject($report))
				return false;
			if($is_reliquat == false)
			{
				// On incrémente uniquement les paiements du jour
				$report->total_payment 				+= (float)number_format($payment_amount,6,'.',''); // REQUETE DES PAIEMENTS AVEC LES REF COMMANDES
				$report->save();
			}
		}
		
		// On vérifie s'il s'agit d'un paiement le jour d'une commande du jour ou non
		$balance = false;
		
		if($report->day != substr($order->date_add,0,10) && $is_reliquat == false)
		{
			// Si on est dans le cas d'un paiement relaiquat on créé un détail de rapport
			$report_detail 								= new PrestatillFinancialReportDetail();
			$report_detail->id_financial_report 		= $report->id;
			$report_detail->id_order 					= 0;
			$report_detail->id_service					= $id_service;
			$report_detail->order_reference 			= $order->reference;
			$report_detail->date_order_detail 			= $order->date_add;
			$report_detail->total_price_tax_incl 		= (float)number_format(0,6,'.','');
			$report_detail->total_price_tax_excl 		= (float)number_format(0,6,'.','');
			$report_detail->total_vat 					= (float)number_format(0,6,'.','');
			$report_detail->total_discounts_tax_incl 	= (float)number_format(0,6,'.','');
			$report_detail->total_discounts_tax_excl 	= (float)number_format(0,6,'.','');
			
			// AJOUT DES PORTS
			$report->total_shipping_tax_incl 	= (float)number_format(0,6,'.','');
			$report->total_shipping_tax_excl 	= (float)number_format(0,6,'.','');
			
			$report_detail->product_qty 				= 0;
			$report_detail->id_currency 				= 2; // A dynamiser
			
			// On récupère la liste des paiements effectués ce jour pour cette commande et on la serialise
			$payment_infos = PrestatillFinancialReportPayment::getPaymentInfos($order->reference); 
			//d($payment_infos);
			if(!empty($payment_infos))
			{
				$report_detail->payments = serialize($payment_infos[0]);
			}
			else
			{
				$report_detail->payments = 0;
			}
			
			$report_detail->save();
			
			$balance = true;
			
		}

		$report_payment 						= new PrestatillFinancialReportPayment();
		$report_payment->id_financial_report 	= $report->id;
		$report_payment->id_service				= $report->id_service;
		$report_payment->payment_method 		= $label!==''?$label:'Non renseigné';;
		$report_payment->amount 				= (float)number_format($payment_amount,6,'.','');
		$report_payment->balance 				= $balance;
		$report_payment->save();
		
		// On met à jour le total des paiements
		//$report->total_payment += (float)number_format($payment_amount,6,'.','');
		$report->save();
		
		// Ajouter les paiements et modifier report_detail->payments (désérialiser puis ajouter les paiements)
		// S'il s'agit d'une commande du jour
		if($balance == false)
		{
			if($order_reference == null)
			{
				$order_reference = $order->reference;
			}		
				
			$id_financial_report_detail = PrestatillFinancialReportDetail::getReport($report->id, $id_order, $order_reference);
				
			$report_detail = new PrestatillFinancialReportDetail($id_financial_report_detail);
			
			if(Validate::isLoadedObject($report_detail))
			{
				// On met à jour la liste des paiements dans le détail du rapport	
				$payments = PrestatillFinancialReportPayment::getPayments($good_date, $order_reference);
				$report_detail->payments = serialize($payments);
				$report_detail->save();
			}
		}
		
	}

	public static function updateReport($day, $id_order, $new_order_status = false, $slips_buying = false, $params = array())
	{
		$order = new Order($id_order);
		
		// On récupère le dernier service créé du jour (s'il existe)
		$id_service = PrestatillFinancialStates::ServiceExists(substr($day, 0, 10), $order->id_pointshop);
		
		// On vérifie si un état existe déjà pour la journée, si non on vérifie la veille si la caisse était toujours ouverte
		if($id_service == false)
		{
			$old_date = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT MAX(day) FROM `'._DB_PREFIX_.'financial_states`');
			
			// Si on trouve une date, on vérifie si le dernier état du jour est "caisse ouverte"
			if($old_date != false)
			{
				$other_id_sate = PrestatillFinancialStates::ServiceExists($old_date, $order->id_pointshop);
				
				// On ajoute les informations sur le dernier rapport car la caisse est toujours ouverte	
				$date = $old_date;
				$id_service = $other_id_sate;
			}
		}
		
		// On vérifie si on est en ligne ou en caisse
		$id_existing_report = PrestatillFinancialReport::ReportExists(substr($day, 0, 10), $order->id_pointshop);	
		
		// @TODO: Si le rapport du jour n'existe pas il faudrait en créer un vierge
		$report = new PrestatillFinancialReport($id_existing_report);
		
		if(Validate::isLoadedObject($report))
		{
			// CAS 1 : la commande est annulée
			if((int)Configuration::get('PS_OS_CANCELED') == (int)$new_order_status)
			{
				$id_financial_report_detail = PrestatillFinancialReportDetail::getReport($report->id, $order->id, $order->reference);
				
				// On créé une commande annulée
				$report_detail = new PrestatillFinancialReportDetail($id_financial_report_detail);
				
				if(Validate::isLoadedObject($report_detail))
				{
					// On met à jour les informations du rapport
					$report_detail->is_cancelled = true;
					$report_detail->date_order_cancelled = $day;
					$report_detail->id_service_cancelled = $report->id_service;
					$report_detail->save();
					
					// On met à jour le CA DU JOUR
					$report_vat = PrestatillFinancialReportVat::getVatList($id_existing_report);
					
					$order_tax_details = $order->getProductTaxesDetails();
					
					foreach($report_vat[$report->id_service] as $vat)
					{
						foreach($order_tax_details as $detail)
						{
							if($detail['id_tax'] == $vat['id_tax_rules_group'])
							{	
								$new_report_vat = new PrestatillFinancialReportVat((int)$vat['id_financial_report_vat']);
								
								if(Validate::isLoadedObject($new_report_vat))
								{
									
									$new_report_vat->amount -= number_format($detail['total_amount'],6,'.','');
									$new_report_vat->total_ht -= number_format($detail['total_tax_base'],6,'.','');
									$new_report_vat->save();
								}
							}
						}
					}
				}
				// On met à jour les totaux du rapport
				$report->total_tax_incl -= number_format($order->total_paid_tax_incl,6,'.','');
				$report->total_tax_excl -= number_format($order->total_paid_tax_excl,6,'.','');
				$report->total_vat -= number_format($order->total_paid_tax_incl-$order->total_paid_tax_excl,6,'.','');
				
				// AJOUT DES PORTS
				$report->total_shipping_tax_incl 	-= (float)number_format($order->total_shipping_tax_incl,6,'.','');
				$report->total_shipping_tax_excl 	-= (float)number_format($order->total_shipping_tax_excl,6,'.','');
				
				$report->total_discounts_tax_incl -= number_format($order->total_discounts_tax_incl,6,'.','');
				$report->total_discounts_tax_excl -= number_format($order->total_discounts_tax_excl,6,'.','');
				$report->save();
			}
			// ON EST DANS LE CAS 2 : GENERATION D'UN AVOIR
			else if($slips_buying == true)
			{					
				// On met à jour le CA DU JOUR
				$report_vat = PrestatillFinancialReportVat::getVatList($id_existing_report);	
				
				foreach($params['product_list'] as $product)
				{
						
					// On créé une commande vide avec un avoir pour chaque produit ?
					$report_detail = new PrestatillFinancialReportDetail();
					$report_detail->order_reference = $order->reference;
					$report_detail->id_order =  $order->id;
					$report_detail->id_financial_report = $report->id;
					$report_detail->date_order_detail = date('Y-m-d H:i:s');
					$report_detail->total_price_tax_incl = -number_format($product['total_price_tax_incl'],6,'.','');
					$report_detail->total_vat = -number_format($product['total_price_tax_incl']-$product['total_price_tax_excl'],6,'.','');
					$report_detail->total_price_tax_excl = -number_format($product['total_price_tax_excl'],6,'.','');
					
					$report_detail->total_discounts_tax_incl = number_format(0,6,'.','');
					$report_detail->total_discounts_tax_excl = number_format(0,6,'.','');
					
					// AJOUT DES PORTS
					$report_detail->total_shipping_tax_incl = number_format(0,6,'.','');
					$report_detail->total_shipping_tax_excl = number_format(0,6,'.','');
					
					
					$report_detail->product_qty = -$product['quantity'];
					$report_detail->id_currency = 2;
					$report_detail->is_cancelled = 0;
					$report_detail->date_order_cancelled = '0000-00-00 00:00:00';
					$report_detail->id_service = $report->id_service;
					$report_detail->id_service_cancelled = 0;
					$report_detail->payments = 0;
					
					/*
					 * SI ON EST DANS LE CADRE D'UN REMBOURSEMENT PARTIEL
					 */
					if(isset($product['amount']))
					{
						$payment = array();
						$payment[0]['id_order_payment'] = 0;
						$payment[0]['id_currency'] = 2;
						$payment[0]['amount'] = (float)number_format(-$product['amount'],6,'.','');;
						$payment[0]['payment_method'] = 'Remboursement via BO'; // @TODO : mettre le mode de remboursement
						
						$report_detail->payments = serialize($payment);
						
						// On soustrait le paiement si on est dans le cas d'un remboursement partiel
						$report_payment 						= new PrestatillFinancialReportPayment();
						$report_payment->id_financial_report 	= $report->id;
						$report_payment->id_service				= $report->id_service;
						$report_payment->payment_method 		= 'Remboursement via BO'; // @TODO : mettre le mode de remboursement
						$report_payment->amount 				= (float)number_format(-$product['amount'],6,'.','');
						$report_payment->balance 				= 0;
						$report_payment->save();
					}
					
					$report_detail->save();
						
					$order_detail = new OrderDetail((int)$product['id_order_detail']);
					
					if(Validate::isLoadedObject($order_detail))
					{
						//d($report_vat);	
						foreach($report_vat[$report->id_service] as $vat)
						{
							if($order_detail->id_tax_rules_group == $vat['id_tax_rules_group'])
							{	
								$new_report_vat = new PrestatillFinancialReportVat((int)$vat['id_financial_report_vat']);
								
								if(Validate::isLoadedObject($new_report_vat))
								{
									$new_report_vat->amount -= number_format($product['total_price_tax_incl']-$product['total_price_tax_excl'],6,'.','');
									$new_report_vat->total_ht -= number_format($product['total_price_tax_excl'],6,'.','');
									$new_report_vat->save();
								}
							}
						}
					}
				}
				
				// On met à jour les totaux du rapport
				$report->total_tax_incl -= number_format($product['total_price_tax_incl'],6,'.','');
				$report->total_tax_excl -= number_format($product['total_price_tax_excl'],6,'.','');
				$report->total_vat -= number_format($product['total_price_tax_incl']-$product['total_price_tax_excl'],6,'.','');
				$report->save();
				//d($params);
			}
		}	
	}

	public static function getCAbyPaymentMethod($date_from, $date_to, $granularity = false, $id_pointshop = array(0 => 0), $compare = false, $id_order = null, $id_service = null)
    {
        $is_compared = false;	// FALSE si on n'est pas dans une comparaison
			
        if($compare !== false AND ($compare['date_compare_from'] != '0000-00-00' AND $compare['date_compare_to'] != '0000-00-00'))
			$is_compared = true;	
			
        if ($granularity !== false)
		{
	        if ($granularity == 'day') 
			{
				$gr = 10;
			} elseif ($granularity == 'month') 
			{
				$gr = 7;
			}elseif ($granularity == 'year') 
			{
				$gr = 4;
			}
			
			// On parse les modes de paiement
			$query = 'SELECT DISTINCT payment_method
						FROM `'._DB_PREFIX_.'order_payment` op
						WHERE op.date_add BETWEEN "'.pSQL($date_from).' 00:00:00" AND "'.pSQL($date_to).' 23:59:59"';
			
			if($is_compared == true)			
				$query.=' OR op.date_add BETWEEN "'.pSQL($compare['date_compare_from']).' 00:00:00" AND "'.pSQL($compare['date_compare_to']).' 23:59:59"
						';
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
			
			//d($result);
			// ON PARCOURS LA DATE 1
			$date_1 = 'SELECT DISTINCT LEFT(op.date_add, '.$gr.') as date, op.id_order_payment, op.date_add as date_payment, op.amount / op.conversion_rate as sales, o.date_add as date_order, op.order_reference, op.payment_method, o.id_pointshop
				FROM `'._DB_PREFIX_.'order_payment` op
				LEFT JOIN '._DB_PREFIX_.'orders o ON (o.reference = op.order_reference)
				WHERE op.date_add BETWEEN "'.pSQL($date_from).' 00:00:00" AND "'.pSQL($date_to).' 23:59:59"
				';
				
				if(is_array($id_pointshop))
				{
					foreach($id_pointshop as $id)
					{
						if($id == reset($id_pointshop))
						{
							$date_1 .= ' AND';
						} else 
						{
							$date_1 .= ' OR';	
						}	
						$date_1 .= ' o.id_pointshop = '.$id;
					}
				}
				else
				{
						
					$date_1 .= ' AND o.id_pointshop = '.$id_pointshop;
				}
				
			if($id_order !== null)
				$date_1 .= ' AND o.id_order = '.$id_order;
					
			$date_1 .= ' GROUP BY op.date_add';
			
            $rest_date_1 = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($date_1);
			//d($rest_date_1);
			
			// SI ON EST DANS UNE COMPARAISON
			if($is_compared == true)
			{
				$date_2 = 'SELECT DISTINCT LEFT(op.date_add, '.$gr.') as date, op.date_add as date_payment, SUM(op.amount / op.conversion_rate) as sales, o.date_add as date_order, op.order_reference, op.payment_method, o.id_pointshop
					FROM `'._DB_PREFIX_.'order_payment` op
					LEFT JOIN '._DB_PREFIX_.'orders o ON (o.reference = op.order_reference)
					WHERE op.date_add BETWEEN "'.pSQL($compare['date_compare_from']).' 00:00:00" AND "'.pSQL($compare['date_compare_to']).' 23:59:59" ';
					
				// IF ID_POINTSHOP
				if(is_array($id_pointshop))
				{
					foreach($id_pointshop as $id)
					{
						if($id == reset($id_pointshop))
						{
							$date_2 .= ' AND';
						} else 
						{
							$date_2 .= ' OR';	
						}	
						$date_2 .= ' o.id_pointshop = '.$id;
					}
				}
				else
				{
					$date_2 .= ' AND o.id_pointshop = '.$id_pointshop;
				}
					
				$date_2 .= ' GROUP BY op.date_add';
					
	            $rest_date_2 = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($date_2);
			}
			
            $sales = array(
				'date' => array(),
				'pm' => array(),
				'total' => array(),
				'details' => array(),
				'total_comp' => array(),
			);
			
	        foreach ($result as $payment) 
	        {
				$sales['pm'][$payment['payment_method']] = 0;
				$sales['total'][$payment['payment_method']] = 0;
				$sales['total_comp'][$payment['payment_method']] = 0;	
					
	            foreach($rest_date_1 as $key => $row)
				{
					if(!isset($sales['date'][$row['date']]))
						$sales['date'][$row['date']] = array();	

					if(!isset($sales['date'][$row['date']][$payment['payment_method']]))
							$sales['date'][$row['date']][$payment['payment_method']] = 0;
						
					$sales['details'][$row['date']][$row['id_order_payment']] = $row;
					
					if($payment['payment_method'] == $row['payment_method'])
					{
						$sales['date'][$row['date']][$payment['payment_method']] += number_format($row['sales'],6,'.','');
						$sales['pm'][$payment['payment_method']] += 1;
						$sales['total'][$payment['payment_method']] += number_format($row['sales'],6,'.','');
					}
					else 
					{
						$sales['date'][$row['date']][$payment['payment_method']] += 0;
						$sales['pm'][$payment['payment_method']] += 0;
						$sales['total'][$payment['payment_method']] += 0;
					}	
				}
				
				if($is_compared == true)
				{
					foreach($rest_date_2 as $key => $row)
					{
						if($payment['payment_method'] == $row['payment_method'])
						{
							$sales['total_comp'][$payment['payment_method']] += $row['sales'];
						}
						else 
						{
							$sales['total_comp'][$payment['payment_method']] += 0;
						}
					}
				}	
	        }
			return $sales;
        } 
    }
	

	public static function getCAbyVAT($date_from, $date_to, $granularity = false, $id_pointshop = array(0 => 0), $compare = false, $id_order = null)
    {
        $is_compared = false;	// FALSE si on n'est pas dans une comparaison
			
        if($compare !== false AND ($compare['date_compare_from'] != '0000-00-00' AND $compare['date_compare_to'] != '0000-00-00'))
			$is_compared = true;	
			
        if ($granularity !== false)
		{
	        if ($granularity == 'day') 
			{
				$gr = 10;
			} elseif ($granularity == 'month') 
			{
				$gr = 7;
			}elseif ($granularity == 'year') 
			{
				$gr = 4;
			}
			 
			$query = 'SELECT LEFT(o.date_add, '.$gr.') as date,
						o.date_add, 
						o.id_order, 
						o.reference,
						o.total_discounts_tax_excl,
						o.total_discounts_tax_incl,
						o.total_shipping_tax_incl,
						o.total_shipping_tax_excl,
						SUM(od.total_price_tax_incl / o.conversion_rate) as tptinc, 
						SUM(od.total_price_tax_excl / o.conversion_rate) as tptexc, 
						SUM(ROUND(od.total_price_tax_incl / o.conversion_rate,2)) as tptinc_round, 
						SUM(ROUND(od.total_price_tax_excl / o.conversion_rate,2)) as tptexc_round, 
						o.id_pointshop, 
						od.id_tax_rules_group,
						od.product_id,
						od.product_attribute_id,
						od.is_giftcard, 
						trg.name as tax_name, 
						SUM(ROUND(od.original_product_price*od.product_quantity,2)) as original_product_price, 
						SUM(od.product_quantity) as product_qty
					FROM `'._DB_PREFIX_.'order_detail` od
					LEFT JOIN '._DB_PREFIX_.'orders o ON (o.id_order = od.id_order)
					LEFT JOIN '._DB_PREFIX_.'tax_rules_group trg ON (od.id_tax_rules_group = trg.id_tax_rules_group)
					LEFT JOIN '._DB_PREFIX_.'order_detail_tax odt ON (odt.id_order_detail = od.id_order_detail)
					WHERE o.date_add BETWEEN "'.pSQL($date_from).' 00:00:00" AND "'.pSQL($date_to).' 23:59:59" ';

			// IF ID_POINTSHOP
			if(is_array($id_pointshop))
			{
				foreach($id_pointshop as $id)
				{
					if($id == reset($id_pointshop))
					{
						$query .= ' AND';
					} else 
					{
						$query .= ' OR';	
					}	
					$query .= ' o.id_pointshop = '.$id;
				}
			}
			else
			{
				$query .= ' AND o.id_pointshop = '.$id_pointshop;
			}

			if($id_order !== null)
				$query .= ' AND o.id_order = '.$id_order;
				
			$query .= ' GROUP BY od.id_order, od.id_tax_rules_group';
			$query .= ' ORDER BY od.id_order, od.id_tax_rules_group';
			
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);	
			//d($result);
			// ON RECUPERE LA LISTE DES REGLES DE TVA ACTIVES 
			$activeTaxeRules = TaxRulesGroup::getTaxRulesGroups(true);
			
			
			// MAJ : on ajoute la notion de "aucune taxe"
			$activeTaxeRules[] = array
									(
							            'id_tax_rules_group' => 0,
							            'name' => 'Aucune taxe',
							           	'active' => 1
							        );
									
			
			// SI ON EST DANS UNE COMPARAISON
			if($is_compared == true)
			{
				$date_2 = 'SELECT LEFT(o.date_add, '.$gr.') as date,
						o.date_add, 
						o.id_order, 
						o.reference,
						o.total_discounts_tax_excl,
						o.total_discounts_tax_incl,
						o.total_shipping_tax_incl,
						o.total_shipping_tax_excl,
						SUM(od.total_price_tax_incl / o.conversion_rate) as tptinc, 
						SUM(od.total_price_tax_excl / o.conversion_rate) as tptexc, 
						o.id_pointshop, 
						od.id_tax_rules_group, 
						trg.name as tax_name, 
						SUM(od.product_quantity) as product_qty
					FROM `'._DB_PREFIX_.'order_detail` od
					LEFT JOIN '._DB_PREFIX_.'orders o ON (o.id_order = od.id_order)
					LEFT JOIN '._DB_PREFIX_.'tax_rules_group trg ON (od.id_tax_rules_group = trg.id_tax_rules_group)
					WHERE o.date_add BETWEEN "'.pSQL($compare['date_compare_from']).' 00:00:00" AND "'.pSQL($compare['date_compare_to']).' 23:59:59"
					AND o.valid = 1 '; // Uniquement les commandes considérées comme validées

				// IF ID_POINTSHOP
				if(is_array($id_pointshop))
				{
					foreach($id_pointshop as $id)
					{
						if($id == reset($id_pointshop))
						{
							$date_2 .= ' AND';
						} else 
						{
							$date_2 .= ' OR';	
						}	
						$date_2 .= ' o.id_pointshop = '.$id;
					}
				}
				else
				{
					$date_2 .= ' AND o.id_pointshop = '.$id_pointshop;
				}
					
				$date_2 .= ' GROUP BY od.id_order, od.id_tax_rules_group';
				$date_2 .= ' ORDER BY od.id_order, od.id_tax_rules_group';
				
	            $rest_date_2 = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($date_2);
				
			}
			
			$sales = array(
				'date' => array(),
				'details' => array(),
				'total' => array(),
				'vat' => array(),
				'total_comp' => array(),
			);
			
			$sales['total']['total_discounts_ttc'] = 0;
			$sales['total']['total_discounts_ht'] = 0;
			$sales['total']['total_shipping_tax_incl'] = 0;
			$sales['total']['total_shipping_tax_excl'] = 0;
			$sales['total']['product_qty'] = 0;
			$sales['total']['total_ttc'] = 0;
			$sales['total']['total_ht'] = 0;
			$sales['total_comp']['vat'] = array();
			
			
			$current_id_order = 0;	
			foreach ($result as $key => $row) 
	        {		
				// TEST 
				$order = new Order((int)$row['id_order']);	
				$detail_taxes = $order->getOrderDetailTaxes();
				//d($detail_taxes);
				if(!empty($detail_taxes))
				{
					foreach($detail_taxes as $detail_tax)
					{
						if(!isset($row['amount_tva']))
							$row['amount_tva'] = 0;	
						
						if(!isset($row['tax_rate']))
							$row['tax_rate'] = 1;	
							
						if($row['id_tax_rules_group'] == $detail_tax['id_tax_rules_group'])
						{
							$row['amount_tva'] += $detail_tax['product_quantity']*$detail_tax['unit_amount'];
							$row['tax_rate'] = $detail_tax['rate'];
						}
						else 
						{
							$row['amount_tva'] += 0;
						}	
					}
					$row['tptexc'] = $row['amount_tva']/($row['tax_rate']/100);	
					$row['tptinc'] = $row['tptexc']+$row['amount_tva'];	
				}				
					
				if(!isset($sales['date'][$row['date']]))
				{
					$sales['date'][$row['date']] = array();
					$sales['date'][$row['date']]['vat'] = array();
					$sales['date'][$row['date']]['total_ttc'] = 0;
					$sales['date'][$row['date']]['total_ht'] = 0;
					$sales['date'][$row['date']]['total_discounts_ttc'] = 0;	
					$sales['date'][$row['date']]['total_discounts_ht'] = 0;
					$sales['date'][$row['date']]['total_shipping_tax_incl'] = 0;
					$sales['date'][$row['date']]['total_shipping_tax_excl'] = 0;
				}	
				
				$sales['details'][$key] = $row;
				
				if( $row['is_giftcard'] == 0)
				{
					$sales['date'][$row['date']]['total_ttc'] += $row['tptinc'];
					$sales['date'][$row['date']]['total_ht'] += $row['tptexc'];
					$sales['total']['total_ttc'] += $row['tptinc'];
					$sales['total']['total_ht'] += $row['tptexc'];
				}
				
				if($current_id_order != $row['id_order']){
						
					if(!isset($sales['date'][$row['date']]['product_qty']))
						$sales['date'][$row['date']]['product_qty'] = 0;
					
					$sales['date'][$row['date']]['total_discounts_ttc'] += $row['total_discounts_tax_incl'];
					$sales['date'][$row['date']]['total_discounts_ht'] += $row['total_discounts_tax_excl'];
					
					$sales['date'][$row['date']]['total_shipping_tax_incl'] += $row['total_shipping_tax_incl'];
					$sales['date'][$row['date']]['total_shipping_tax_excl'] += $row['total_shipping_tax_excl'];
					
					$sales['date'][$row['date']]['product_qty'] += $row['product_qty'];
					$sales['total']['total_discounts_ttc'] += $row['total_discounts_tax_incl'];
					$sales['total']['total_discounts_ht'] += $row['total_discounts_tax_excl'];
					
					$sales['total']['total_shipping_tax_incl'] += $row['total_shipping_tax_incl'];
					$sales['total']['total_shipping_tax_excl'] += $row['total_shipping_tax_excl'];

					// On déduit les remises sur les différents totaux
					/*
					$sales['date'][$row['date']]['total_ttc'] -= $row['total_discounts_tax_incl'];
					$sales['date'][$row['date']]['total_ht'] -= $row['total_discounts_tax_excl'];
					$sales['total']['total_ttc'] -= $row['total_discounts_tax_incl']; 
					$sales['total']['total_ht'] -= $row['total_discounts_tax_excl'];
					$sales['total']['product_qty'] += $row['product_qty'];*/
				}
				else 
				{
					// TEST pour défalquer la remise globale si on a plusieurs taux de TVA
					$sales['details'][$key]['total_discounts_tax_incl'] = number_format(0,6,'.','');
					$sales['details'][$key]['total_discounts_tax_excl'] = number_format(0,6,'.','');
				}
				
				foreach($activeTaxeRules as $id_tax_rule)
				{
					if(!isset($sales['vat'][$id_tax_rule['id_tax_rules_group']]))
						$sales['vat'][$id_tax_rule['id_tax_rules_group']] = 0;

					if(!isset($sales['date'][$row['date']]['vat'][$id_tax_rule['id_tax_rules_group']]))
					{
						$sales['date'][$row['date']]['vat'][$id_tax_rule['id_tax_rules_group']]['total_tax'] = 0;
						$sales['date'][$row['date']]['vat'][$id_tax_rule['id_tax_rules_group']]['total_ht'] = 0;
					}
					
					if(!isset($sales['total']['vat'][$id_tax_rule['id_tax_rules_group']]))
					{
						$sales['total']['vat'][$id_tax_rule['id_tax_rules_group']]['total_tax'] = 0;
						$sales['total']['vat'][$id_tax_rule['id_tax_rules_group']]['total_ht'] = 0;
					}
						
		
					if(($id_tax_rule['id_tax_rules_group'] == $row['id_tax_rules_group']) && ($row['is_giftcard'] == 0))
					{		
						$sales['vat'][$id_tax_rule['id_tax_rules_group']] += 1;	
						$sales['date'][$row['date']]['vat'][$id_tax_rule['id_tax_rules_group']]['total_tax'] += ($row['tptinc'] - $row['tptexc']);
						$sales['date'][$row['date']]['vat'][$id_tax_rule['id_tax_rules_group']]['total_ht'] += $row['tptexc'];
						$sales['total']['vat'][$id_tax_rule['id_tax_rules_group']]['total_tax'] += ($row['tptinc'] - $row['tptexc']);
						$sales['total']['vat'][$id_tax_rule['id_tax_rules_group']]['total_ht'] += $row['tptexc'];
						if($current_id_order != $row['id_order'])
						{
							//$sales['date'][$row['date']]['vat'][$id_tax_rule['id_tax_rules_group']]['total_tax'] -= ($row['total_discounts_tax_incl'] - $row['total_discounts_tax_excl']);
							//$sales['date'][$row['date']]['vat'][$id_tax_rule['id_tax_rules_group']]['total_ht'] -= $row['total_discounts_tax_excl'];
							//$sales['total']['vat'][$id_tax_rule['id_tax_rules_group']]['total_tax'] -= ($row['total_discounts_tax_incl'] - $row['total_discounts_tax_excl']);
							//$sales['total']['vat'][$id_tax_rule['id_tax_rules_group']]['total_ht'] -= $row['total_discounts_tax_excl'];
						}
					}
					else 
					{
						$sales['vat'][$id_tax_rule['id_tax_rules_group']] += 0;	
						$sales['date'][$row['date']]['vat'][$id_tax_rule['id_tax_rules_group']]['total_tax'] += 0;
						$sales['date'][$row['date']]['vat'][$id_tax_rule['id_tax_rules_group']]['total_ht'] += 0;
						$sales['total']['vat'][$id_tax_rule['id_tax_rules_group']]['total_tax'] += 0;						
						$sales['total']['vat'][$id_tax_rule['id_tax_rules_group']]['total_ht'] += 0;						
					}
					
				}
				
				$current_id_order = $row['id_order'];	
			}
			
			if($is_compared == true)
			{
				$current_comp_id_order = 0;
				$sales['total_comp']['total_ttc'] = 0;	
				$sales['total_comp']['total_ht'] = 0;	
				$sales['total_comp']['total_discounts_ttc'] = 0;	
				$sales['total_comp']['total_discounts_ht'] = 0;	
				$sales['total_comp']['product_qty'] = 0;
				$sales['total_comp']['vat'] = array();
				
				foreach($rest_date_2 as $row)
				{
					$sales['total_comp']['total_ttc'] += $row['tptinc'];
					$sales['total_comp']['total_ht'] += $row['tptexc'];
					
					if($current_id_order != $row['id_order']){
						$sales['total_comp']['total_discounts_ttc'] += $row['total_discounts_tax_incl'];
						$sales['total_comp']['total_discounts_ht'] += $row['total_discounts_tax_excl'];
						
						$sales['total_comp']['total_shipping_tax_incl'] += $row['total_shipping_tax_incl'];
						$sales['total_comp']['total_shipping_tax_excl'] += $row['total_shipping_tax_excl'];
	
						// On déduit les remises sur les différents totaux
						$sales['total_comp']['total_ttc'] -= $row['total_discounts_tax_incl']; 
						$sales['total_comp']['total_ht'] -= $row['total_discounts_tax_excl'];
						$sales['total_comp']['product_qty'] += $row['product_qty'];
					}
					
					foreach($activeTaxeRules as $id_tax_rule)
					{
						if(!isset($sales['total_comp']['vat'][$id_tax_rule['id_tax_rules_group']]))	
							$sales['total_comp']['vat'][$id_tax_rule['id_tax_rules_group']] = 0;	
								
						if($id_tax_rule['id_tax_rules_group'] == $row['id_tax_rules_group'])
						{
							$sales['total_comp']['vat'][$id_tax_rule['id_tax_rules_group']] += ($row['tptinc'] - $row['tptexc']);
							if($current_id_order != $row['id_order'])
							{
								$sales['total_comp']['vat'][$id_tax_rule['id_tax_rules_group']] -= ($row['total_discounts_tax_incl'] - $row['total_discounts_tax_excl']);
							}
						}
						else 
						{
							$sales['total_comp']['vat'][$id_tax_rule['id_tax_rules_group']] += 0;						
						}
					}
					$current_comp_id_order = $row['id_order'];	
				}
			}
			//d($sales);
            return $sales;
        } 
    }

	public static function getCAbyCat($date_from, $date_to, $granularity = false, $id_pointshop = array(0 => 0), $id_service = 0, $full_tree = true)
    {
		$id_lang = Context::getContext()->employee->id_lang;
		
		$categories = array();
		if (Shop::getContext() != Shop::CONTEXT_ALL)
		{
			$query = 'SELECT c.nleft, c.nright
					FROM '._DB_PREFIX_.'category c
					WHERE c.id_category IN (
						SELECT s.id_category
						FROM '._DB_PREFIX_.'shop s
						WHERE s.id_shop IN ('.implode(', ', Shop::getContextListShopID()).')
					)';
			if ($result = Db::getInstance()->executeS($query))
			{
				$ntree_restriction = array();
				foreach ($result as $row)
					$ntree_restriction[] = '(nleft >= '.$row['nleft'].' AND nright <= '.$row['nright'].')';

				if ($ntree_restriction)
				{
					$query = 'SELECT id_category
							FROM '._DB_PREFIX_.'category
							WHERE '.implode(' OR ', $ntree_restriction);
					if ($result = Db::getInstance()->executeS($query))
					{
						foreach ($result as $row)
							$categories[] = $row['id_category'];
					}
				}
			}
		}

		// Get best categories
		$query = '
		SELECT SQL_CALC_FOUND_ROWS ca.`id_category`, CONCAT(parent.name, \' > \', calang.`name`) as name,
			IFNULL(SUM(t.`totalQuantitySold`), 0) AS totalQuantitySold,
			ROUND(IFNULL(SUM(t.`totalPriceSold`), 0), 2) AS totalPriceSold
		FROM `'._DB_PREFIX_.'category` ca
		LEFT JOIN `'._DB_PREFIX_.'category_lang` calang ON (ca.`id_category` = calang.`id_category` AND calang.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('calang').')
		LEFT JOIN `'._DB_PREFIX_.'category_lang` parent ON (ca.`id_parent` = parent.`id_category` AND parent.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('parent').')
		LEFT JOIN `'._DB_PREFIX_.'category_product` capr ON ca.`id_category` = capr.`id_category`
		LEFT JOIN (
			SELECT pr.`id_product`, t.`totalQuantitySold`, t.`totalPriceSold`
			FROM `'._DB_PREFIX_.'product` pr
			LEFT JOIN (
				SELECT pr.`id_product`,
					IFNULL(SUM(cp.`product_quantity`), 0) AS totalQuantitySold,
					IFNULL(SUM(cp.`product_price` * cp.`product_quantity`), 0) / o.conversion_rate AS totalPriceSold
				FROM `'._DB_PREFIX_.'product` pr
				LEFT OUTER JOIN `'._DB_PREFIX_.'order_detail` cp ON pr.`id_product` = cp.`product_id`
				LEFT JOIN `'._DB_PREFIX_.'orders` o ON o.`id_order` = cp.`id_order`
				LEFT JOIN `'._DB_PREFIX_.'financial_sales_detail` fs ON o.`id_order` = fs.`id_order`
				'.Shop::addSqlRestriction(Shop::SHARE_ORDER, 'o').'
				WHERE o.valid = 1
				AND o.invoice_date BETWEEN "'.pSQL($date_from).' 00:00:00" AND "'.pSQL($date_to).' 23:59:59"'; 
				
				// IF ID_POINTSHOP
				if(is_array($id_pointshop))
				{
					foreach($id_pointshop as $id)
					{
						if($id == reset($id_pointshop))
						{
							$query .= ' AND';
						} else 
						{
							$query .= ' OR';	
						}	
						$query .= ' o.id_pointshop = '.$id;
					}
				}
				else
				{
					$query .= ' AND o.id_pointshop = '.$id_pointshop;
				}
				
				// IF ID_SERVICE
				if($id_service > 0)
				{
					$query .= ' AND fs.id_service = '.$id_service;
				}
				
				$query .= ' GROUP BY pr.`id_product`
			) t ON t.`id_product` = pr.`id_product`
		) t	ON t.`id_product` = capr.`id_product`
		'.(($categories) ? 'WHERE ca.id_category IN ('.implode(', ', $categories).')' : '').'';
			
		$query .= 'AND totalQuantitySold > 0 GROUP BY ca.`id_category` ';
		$query .= 'HAVING ca.`id_category` != 1 ';
		$query .= 'ORDER BY totalPriceSold DESC ';

		$values = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
		
		foreach ($values as &$value)
		{
			if(!isset($value['totalPriceSold']))	
				$value['totalPriceSold'] = 0;
			if($full_tree == true)
			{	
				$value['totalPriceSold'] = Tools::displayPrice($value['totalPriceSold']);
			}
			else
			{		
				$name_temp = explode('>' , $value['name']);
				$name_temp = array_reverse($name_temp);
				$value['name'] = $name_temp[0];
			}
			
		}
		
		
		//$this->_totalCount = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT FOUND_ROWS()');	
		return $values;
		
    }
    
	public static function getCAbyHour($date_from, $date_to, $granularity = false, $id_pointshop = array(0 => 0))
    {
		$id_lang = Context::getContext()->employee->id_lang;
		
		// Get best categories
		 if ($granularity == 'day') 
			{
				$gr = '10';
			} elseif ($granularity == 'month') 
			{
				$gr = '7';
			}elseif ($granularity == 'year') 
			{
				$gr = '4';
			}
			
			$query = 'SELECT LEFT(o.date_add, '.$gr.') as date,
						RIGHT(o.date_add, 8) as hour, 
						o.id_order, 
						o.total_discounts_tax_excl,
						o.total_discounts_tax_incl, 
						o.total_shipping_tax_excl,
						o.total_shipping_tax_incl,
						SUM(od.total_price_tax_incl / o.conversion_rate) as tptinc, 
						SUM(od.total_price_tax_excl / o.conversion_rate) as tptexc, 
						o.id_pointshop, 
						od.id_tax_rules_group, 
						trg.name as tax_name, 
						SUM(od.product_quantity) as product_qty
					FROM `'._DB_PREFIX_.'order_detail` od
					LEFT JOIN '._DB_PREFIX_.'orders o ON (o.id_order = od.id_order)
					LEFT JOIN '._DB_PREFIX_.'tax_rules_group trg ON (od.id_tax_rules_group = trg.id_tax_rules_group)
					WHERE o.date_add BETWEEN "'.pSQL($date_from).' 00:00:00" AND "'.pSQL($date_to).' 23:59:59"
					AND o.valid = 1 ';

			// IF ID_POINTSHOP
			if(is_array($id_pointshop))
			{
				foreach($id_pointshop as $id)
				{
					if($id == reset($id_pointshop))
					{
						$query .= ' AND';
					} else 
					{
						$query .= ' OR';	
					}	
					$query .= ' o.id_pointshop = '.$id;
				}
			}
			else
			{
				$query .= ' AND o.id_pointshop = '.$id_pointshop;
			}
				
			$query .= ' GROUP BY od.id_order, od.id_tax_rules_group';
			$query .= ' ORDER BY od.id_order, od.id_tax_rules_group';
	
            $values = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
			
			$current_id_order = 0;
			$sales = array();
			foreach ($values as $row) 
	        {
				for($i = 0; $i < 24; $i++) 
				{
					$hour = substr($row['hour'], 0, 2); 
					
					if(!isset($sales['date'][$row['date']][$i]))
						$sales['date'][$row['date']][$i] = 0;
	
					if(!isset($sales['total_ht'][$i]))	
						$sales['total_ht'][$i] = 0;
					
						
					if($i == substr($row['hour'], 0, 2))
					{
						$sales['date'][$row['date']][$i] += $row['tptinc'];
						$sales['total_ht'][$i] += $row['tptinc'];
						
						if($current_id_order != $row['id_order'])
						{
							$sales['date'][$row['date']][$i] -= $row['total_discounts_tax_incl'];
							$sales['total_ht'][$i] -= $row['total_discounts_tax_incl'];
						}
					}
					else 
					{
						$sales['date'][$row['date']][$i] += 0;
						$sales['total_ht'][$i] += 0;
					}
				}
				$current_id_order = $row['id_order'];	
			} 
		
		return $sales;
		
    }
	/*
	 * Vérifie l'existence d'un rapport pour la journée $date et le point de vente $id_pointshop
	 * ATTENTION : si un rapport est créé le jour J, et qu'il n'est pas fermé, il faut qu'on puisse ajouter des 
	 * ventes sur le jour J, puis fermé le rapport.
	 */
	public static function ReportExists($date, $id_pointshop = 0, $cash_type = null, $regenerate = false)
	{
		if($regenerate == false && $id_pointshop > 0)
		{
			$id_service = PrestatillFinancialStates::ServiceExists($date, $id_pointshop);
		}
		else 
		{
			$id_service = 1;
		}
		
		
		// On vérifie si un état existe déjà pour la journée, si non on vérifie la veille si la caisse était toujours ouverte
		if($id_service == false && $cash_type != 1 && $id_pointshop > 0)
		{
			$old_date = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT MAX(day) FROM `'._DB_PREFIX_.'financial_states`');
			
			// Si on trouve une date, on vérifie si le dernier état du jour est "caisse ouverte"
			if($old_date != false)
			{
				$id_state = PrestatillFinancialStates::isState($old_date, $id_pointshop);
				
				if($id_state == 1)
				{
					// On ajoute les informations sur le dernier rapport car la caisse est toujours ouverte	
					$date = $old_date;
				}
			}
		}
		
		if($id_report = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT id_financial_report 
				FROM `'._DB_PREFIX_.'financial_report` 
				WHERE day = "'.$date.'" 
				AND id_pointshop = '.$id_pointshop))
		{
			return $id_report;
		}
			return false;
	}
	
	
	public static function getOrderByRefrence($order_reference)
	{
		if($id_order = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT id_order 
				FROM `'._DB_PREFIX_.'orders` 
				WHERE reference = "'.$order_reference.'" 
				'))
		{
			return $id_order;
		}
			return false;
	}

	public static function getReportAllInfos($id_financial_report = null, $with_balance = false)
	{
		$r_infos = array();	
		if($id_financial_report != null)
		{
			$report = new PrestatillFinancialReport($id_financial_report);
		
			if(Validate::isLoadedObject($report))
			{
				$report_details = PrestatillFinancialReportDetail::getList($id_financial_report);
				$report_vat = PrestatillFinancialReportVat::getVatList($id_financial_report);
				
				$report_payment = PrestatillFinancialReportPayment::getPaymentList($id_financial_report);
				$report_payment_with_balance = PrestatillFinancialReportPayment::getPaymentList($id_financial_report, 0, true);
				
				$report_detail_cancelled = PrestatillFinancialReportDetail::getOrdersCancelled($report->day, $id_financial_report);
				$report_states = PrestatillFinancialStates::getStates($report->id_financial_report, $report->day, $report->id_pointshop);
				//d($report_payment);
				
				$balances = PrestatillFinancialReportDetail::getList($id_financial_report, true);
				
				// On regarde combien de services il y a eu sur la journée
				$services_in_day = PrestatillFinancialStates::ServiceExists($report->day, $report->id_pointshop);

				$total_details = array();
				
				if(isset($report_details['detail']))
				{			
					foreach($report_details['detail'] as $id_service => $service)
					{
						$total_details[$id_service] = array(
										'tot_ttc' => 0,
										'tot_vat' => 0,
										'tot_ht' => 0,
										'tot_discount_ttc' => 0,
										'tot_discount_ht' => 0,
										'tot_shipping_ttc' => 0,
										'tot_shipping_ht' => 0,
										);
							
						foreach($service as $d => $detail)
						{
							$total_details[$id_service]['tot_ttc'] += $detail['total_price_tax_incl'];
							$total_details[$id_service]['tot_vat'] += $detail['total_vat'];
							$total_details[$id_service]['tot_ht'] += $detail['total_price_tax_excl'];
							$total_details[$id_service]['tot_discount_ttc'] += $detail['total_discounts_tax_incl'];
							$total_details[$id_service]['tot_discount_ht'] += $detail['total_discounts_tax_excl'];
							$total_details[$id_service]['tot_shipping_ht'] += $detail['total_shipping_tax_excl'];
							$total_details[$id_service]['tot_shipping_ttc'] += $detail['total_shipping_tax_incl'];
							
							$report_details['detail'][$id_service][$d]['customer'] = 'NC';
							
							$order = new Order($detail['id_order']);
							if(Validate::isLoadedObject($order))
							{
								$customer = new Customer($order->id_customer);
								if(Validate::isLoadedObject($customer))
								{
									$report_details['detail'][$id_service][$d]['customer'] = $customer->firstname.' '.$customer->lastname;
								}
							}
						}
						
					}
				}	
				// TOTAUX ANNULES
				$total_cancelled = array();
				foreach($report_detail_cancelled as $key => $detail_cancelled)
				{
					$total_cancelled[$key] = array(
									'tot_ttc' => 0,
									'tot_vat' => 0,
									'tot_ht' => 0,
									'tot_discount_ttc' => 0,
									'tot_discount_ht' => 0,
									'tot_shipping_ttc' => 0,
									'tot_shipping_ht' => 0,
									);
					
					foreach($detail_cancelled as $detail)
					{
						$total_cancelled[$key]['tot_ttc'] += $detail['total_price_tax_incl'];
						$total_cancelled[$key]['tot_vat'] += $detail['total_vat'];
						$total_cancelled[$key]['tot_ht'] += $detail['total_price_tax_excl'];
						$total_cancelled[$key]['tot_discount_ttc'] += $detail['total_discounts_tax_incl'];
						$total_cancelled[$key]['tot_discount_ht'] += $detail['total_discounts_tax_excl'];
						$total_cancelled[$key]['tot_shipping_ttc'] += $detail['total_shipping_tax_incl'];
						$total_cancelled[$key]['tot_shipping_ht'] += $detail['total_shipping_tax_excl'];
					}
	
				}

				// PAYMENTS
				$report_pm = array();

				foreach($report_payment as $id_service => $service)
				{
					if(!isset($report_pm[$id_service]))
					{
						$report_pm['detail'][$id_service] = array();
						$report_pm['nbr'][$id_service] = array();
						$report_pm['total'][$id_service] = 0;
					}	
						
					foreach($service as $method => $payment)
					{
						if(!isset($report_pm['detail'][$id_service][$payment['payment_method']]))
						{
							$report_pm['detail'][$id_service][$payment['payment_method']] = 0;
							$report_pm['nbr'][$id_service][$payment['payment_method']] 	= 0;
						}
						
						$report_pm['detail'][$id_service][$payment['payment_method']] 	+= $payment['amount'];
						if($payment['amount'] <> 0)
						{
							$report_pm['nbr'][$id_service][$payment['payment_method']] 		+= 1;
						}
						$report_pm['total'][$id_service] 								+= $payment['amount'];
					}
				}
				
				// PAYMENTS WITH BALANCE
				$report_pm_wb = array();

				foreach($report_payment_with_balance as $id_service => $service)
				{
					if(!isset($report_pm_wb[$id_service]))
					{
						$report_pm_wb['detail'][$id_service] = array();
						$report_pm_wb['nbr'][$id_service] = array();
						$report_pm_wb['total'][$id_service] = 0;
					}	
						
					foreach($service as $method => $payment)
					{
						if(!isset($report_pm_wb['detail'][$id_service][$payment['payment_method']]))
						{
							$report_pm_wb['detail'][$id_service][$payment['payment_method']] = 0;
							$report_pm_wb['nbr'][$id_service][$payment['payment_method']] 	= 0;
						}
						
						$report_pm_wb['detail'][$id_service][$payment['payment_method']] 	+= $payment['amount'];
						if($payment['amount'] <> 0)
						{
							$report_pm_wb['nbr'][$id_service][$payment['payment_method']] 		+= 1;
						}
						$report_pm_wb['total'][$id_service] 								+= $payment['amount'];
					}
				}

				//d($report_payment_with_balance);
				$nbVente = $report_details['nbr_sales'];
				
				$r_infos = array(
					'report' => $report,
		            'report_details' => $report_details,
		            'nbVente' => $nbVente,
		            'report_vat' => $report_vat,
		            'report_payment' => $report_pm,
		            'report_payment_wb' => $report_pm_wb,
		            'balances' => $balances,
					'report_detail_cancelled' => $report_detail_cancelled,
					'total_cancelled' => $total_cancelled,
					'total_details' => $total_details,
					'services_in_day' => $services_in_day,
					'report_states' => $report_states,
				);
			}
		}
		
		return $r_infos;
	}
	
	public static function getOrderStates()	
	{		
	
		$query = 'SELECT DISTINCT payment_method 					
					FROM `'._DB_PREFIX_.'financial_report_payment`';
		
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
		
		$rows = array();
		foreach($result as $row)
		{
			$rows[] = $row['payment_method'];
		}
		
		return $rows;	
	}	
}