<?php

class PrestatillFinancialReportDetail extends ObjectModel {
    	
    public $id_financial_report_detail;
	
    public $id_financial_report;
	
	public $id_service = 1;
	
	public $id_service_cancelled = 0;
	
	public $id_order;
	
	public $order_reference;
		public $date_order_detail;
	
	public $total_price_tax_incl;
	
	public $total_vat;
		
	public $total_price_tax_excl;
	
	public $total_discounts_tax_incl = 0.000000;
	
	public $total_discounts_tax_excl = 0.000000;
	
	public $total_shipping_tax_incl = 0.000000;
	
	public $total_shipping_tax_excl = 0.000000;
	
	public $product_qty;
	
	public $payments;
	
	public $id_currency;
	
	public $is_cancelled = 0;
	
	public $date_order_cancelled;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'financial_report_detail',
		'primary' => 'id_financial_report_detail',
		'multilang' => false,
		'fields' => array(
			'id_financial_report' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_service' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_service_cancelled' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_order' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'order_reference' =>			array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'date_order_detail' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
			'total_price_tax_incl' =>		array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_vat' =>					array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_price_tax_excl' =>		array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_discounts_tax_incl' =>	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_discounts_tax_excl' =>	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_shipping_tax_incl' =>	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_shipping_tax_excl' =>	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'product_qty' =>				array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'id_currency' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'is_cancelled' => 				array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			'payments' => 					array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'date_order_cancelled' => 		array('type' => self::TYPE_DATE, 'validate' => 'isCleanHtml', 'required' => false),
		),
	);
	
	public function __construct($id = null, $id_lang = null)
	{
		parent::__construct($id, $id_lang);
		
		
	}
	
	/**
	 * Get a detailed report list of an id_financial_report
	 * @param int $id_financial_report
	 * @return array
	 */
	public static function getList($id_financial_report, $balance = false, $id_service = 0)
	{
		$query = 'SELECT * 
					FROM `'._DB_PREFIX_.'financial_report_detail` 
					WHERE `id_financial_report` = '.(int)$id_financial_report.'';
					
		if($id_service > 0 )
			$query .= ' AND id_service = '.$id_service;				
		
		if($balance == false)
		{
			$query .= ' AND id_order <> 0';
		}
		else
		{
			$query .= ' AND id_order = 0';
		}	

		$result = Db::getInstance()->executeS($query);
		
		$services = array();
		$services['total'] = array();
		$services['nbr_sales'] = 0;
		
		foreach($result as $key => $row)
		{
			if(!empty($row['payments']))
			{
				$row['payments'] = unserialize($row['payments']);	
			}	
			$services['detail'][$row['id_service']][$key] = $row;
			
			// On ajoute la marge de la commande
			$sales_infos = PrestatillFinancialSales::getSalesDetail(substr($row['date_order_detail'], 0,10), substr($row['date_order_detail'], 0,10), -1, $row['id_order']);
			if(!empty($sales_infos))
			{	
				foreach($sales_infos as $info)
				{
					if(!isset($services['total'][0]))
						$services['total'][0] = 0;	
						
					if(!isset($services['total'][$row['id_service']]))
						$services['total'][$row['id_service']] = 0;
					
					$services['detail'][$row['id_service']][$key]['original_wholesale_price'] = $info['original_wholesale_price'];
					$services['detail'][$row['id_service']][$key]['purchase_supplier_price'] = $info['purchase_supplier_price'];
					$services['detail'][$row['id_service']][$key]['margin_rate_ows'] = $info['margin_rate_ows'];
					$services['detail'][$row['id_service']][$key]['margin_rate_psp'] = $info['margin_rate_psp'];
					if($info['original_wholesale_price'] > 0)
					{
						$services['detail'][$row['id_service']][$key]['margin_amount_ht'] = $row['total_price_tax_excl']-($info['original_wholesale_price']*$row['product_qty']);
						$services['total'][$row['id_service']] += $row['total_price_tax_excl']-($info['original_wholesale_price']*$row['product_qty']);
						$services['total'][0] += $row['total_price_tax_excl']-($info['original_wholesale_price']*$row['product_qty']);
					}
					else if($info['purchase_supplier_price'] > 0)  
					{
						$services['detail'][$row['id_service']][$key]['margin_amount_ht'] = $row['total_price_tax_excl']-($info['purchase_supplier_price']*$row['product_qty']);
						$services['total'][$row['id_service']] += $row['total_price_tax_excl']-($info['purchase_supplier_price']*$row['product_qty']);
						$services['total'][0] += $row['total_price_tax_excl']-($info['purchase_supplier_price']*$row['product_qty']);
					}
					else
					{
						$services['detail'][$row['id_service']][$key]['margin_amount_ht'] = 0;
					}
					
				}	
			}
			$services['nbr_sales'] += 1;
		}
		
		return $services;
	}

	/*
	 * Get id_report_detail
	 */
	public static function getReport($id_report, $id_order, $order_reference)
	{
		if($id_report_detail = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT id_financial_report_detail 
				FROM `'._DB_PREFIX_.'financial_report_detail` 				
				WHERE order_reference = "'.$order_reference.'"'))
		{
			return $id_report_detail;
		}
			return false;
	}

	public static function getOrdersCancelled($day, $id_report = null)
	{			
		$query = 'SELECT * 
					FROM `'._DB_PREFIX_.'financial_report_detail` 
					WHERE is_cancelled = 1
					AND LEFT(date_order_cancelled,10) LIKE \''.$day.'\'';
					
		if($id_report != null)			
			$query .= ' AND id_financial_report = '.$id_report;	
		
		$result = Db::getInstance()->executeS($query);
		
		$res_cancelled = array();
		//d($result);
		foreach($result as $key => $row)
		{
			$res_cancelled[$row['id_service_cancelled']][] = $row;
		}

		return $res_cancelled;
	}
}