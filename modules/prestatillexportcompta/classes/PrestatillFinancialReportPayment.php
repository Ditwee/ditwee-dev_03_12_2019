<?php

class PrestatillFinancialReportPayment extends ObjectModel {
    		
	public $id_financial_report_payment;
	
	public $id_financial_report;
	
	public $id_service = 1;
	
	public $payment_method;
	
	public $amount;
	
	public $balance;

	/**
	 * @see ObjectModel::$definition
	 */ 
	public static $definition = array(
		'table' => 'financial_report_payment',
		'primary' => 'id_financial_report_payment',
		'multilang' => false,
		'fields' => array(
			'id_financial_report' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_service' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'payment_method' => 			array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'amount' =>						array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'balance' =>					array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
		),
	);
	
	public function __construct($id = null, $id_lang = null)
	{
		parent::__construct($id, $id_lang);
		
		
	}
	
	public static function getPaymentList($id_financial_report, $id_service = 0, $with_balance = false)
	{
		$query = 'SELECT * 
					FROM `'._DB_PREFIX_.'financial_report_payment` 
					WHERE id_financial_report = "'.$id_financial_report.'"';
					
		if($id_service > 0 )
			$query .= ' AND id_service = '.$id_service;		
		
		if($with_balance == false)
			$query .= ' AND balance = 0';			
					
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
		
		$services = array();
		
		foreach($result as $key => $row)
		{
			$services[$row['id_service']][] = $row;
		}
		
		return $services;
		
	}
	
	public static function getPayments($date, $order_reference)
	{
		$query = 'SELECT * 
					FROM `'._DB_PREFIX_.'order_payment` 
					WHERE LEFT(date_add,10) = "'.$date.'"
					AND order_reference LIKE "'.$order_reference.'"
					AND amount <> 0';	
					
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
		
			return $result;
	}
	
	public static function getPaymentInfos($order_reference)
	{
		$query = 'SELECT * 
					FROM `'._DB_PREFIX_.'order_payment` 
					WHERE order_reference LIKE "'.$order_reference.'"
					AND amount <> 0
					ORDER BY date_add DESC
					LIMIT 1';	
					
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
		
		return $result;
	}
	
}