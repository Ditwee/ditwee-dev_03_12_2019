<?php

class PrestatillFinancialReportVat extends ObjectModel {
    		
	public $id_financial_report_vat;
	
	public $id_financial_report;
	
	public $id_tax_rules_group;
	
	public $id_service = 1;
	
	public $tax_name;
	
	public $amount;
	
	public $total_ht;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'financial_report_vat',
		'primary' => 'id_financial_report_vat',
		'multilang' => false,
		'fields' => array(
			'id_financial_report' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_service' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_tax_rules_group' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'tax_name' => 					array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'amount' =>						array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_ht' =>						array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
		),
	);
	
	public function __construct($id = null, $id_lang = null)
	{
		parent::__construct($id, $id_lang);
		
		
	}
	
	public static function getVatList($id_financial_report, $id_service = 0)
	{
		$query = 'SELECT id_financial_report_vat, id_financial_report, id_tax_rules_group, tax_name, SUM(amount) as sum_amount, SUM(total_ht) as sum_total_ht, id_service 
					FROM `'._DB_PREFIX_.'financial_report_vat` 
					WHERE id_financial_report = "'.$id_financial_report.'"';
					
		if($id_service > 0 )
			$query .= ' AND id_service = '.$id_service;				
					
		$query .=' GROUP BY id_service,id_tax_rules_group';
					
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
		
		$tax_by_service = array();
		
		foreach($result as $key => $row)
		{
			$taxe_rules_group = new TaxRulesGroup($row['id_tax_rules_group']);
			$result[$key]['name'] = $taxe_rules_group->name;
			$tax_by_service[$row['id_service']][$row['id_tax_rules_group']] = $row;
		}
		
		return $tax_by_service;
	}

	public static function getTaxName($id_tax)
	{
		if($id_tax > 0)
		{
			$query = 'SELECT name 
					FROM `'._DB_PREFIX_.'tax_rules_group` 
					WHERE id_tax_rules_group = '.$id_tax;	
					
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query);
		}	
		else 
		{
			$result = array('name' => 'Aucune Taxe');
		}	
		
		return $result;
	}
	
	/*
	 * Vérifie l'existence d'un rapport pour la journée $date et le point de vente $id_pointshop
	 */
	public static function ReportVatExists($id_financial_report, $id_tax_rules, $id_service = 1)
	{
		if($id_service == false)
			$id_service = 1;	
					
		if($id_report = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT id_financial_report_vat 
				FROM `'._DB_PREFIX_.'financial_report_vat` 
				WHERE id_financial_report = "'.$id_financial_report.'" 
				AND id_service = '.$id_service.'
				AND id_tax_rules_group = '.$id_tax_rules))
		{
			return $id_report;
		}
			return false;
	}
	
}