<?php

class PrestatillFinancialSales extends ObjectModel {
    	
	public $id_financial_sales;	
    		
	public $id_pointshop;
	
	public $nb_products;
	
	public $day;
	

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'financial_sales',
		'primary' => 'id_financial_sales',
		'multilang' => false,
		'fields' => array(
			'id_pointshop' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'nb_products' =>			array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
			'day' => 					array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
		),
	);
	
	public function __construct($id = null, $id_lang = null)
	{
		parent::__construct($id, $id_lang);
		
		
	}
	
	public static function getSalesDetail($date_from, $date_to, $id_pointshop = 0, $id_order = null, $id_service = 0)
    {

		$query = 'SELECT DISTINCT LEFT(o.date_add, 10) as date,
					o.date_add, 
					o.id_order,
					o.reference,
					o.total_discounts_tax_incl,
					o.total_discounts_tax_excl,
					o.total_paid_tax_incl,
					od.id_order_detail,
					od.product_id, 
					od.product_attribute_id, 
					od.product_reference,
					od.product_supplier_reference,
					p.id_manufacturer,
					p.id_supplier,
					od.id_warehouse,
					p.id_category_default,
					od.product_ean13,
					od.product_quantity,
					od.product_quantity_in_stock,
					od.product_quantity_refunded,
					od.product_quantity_return,
					od.product_quantity_reinjected,
					od.purchase_supplier_price,
					od.original_product_price,
					od.original_wholesale_price,
					GROUP_CONCAT(DISTINCT pagl.name," : ",al.name SEPARATOR ", ") as attributes,					
					pl.name,
					((od.total_price_tax_incl / o.conversion_rate) - ((od.total_price_tax_incl/(o.total_discounts_tax_incl+o.total_paid_tax_incl))*o.total_discounts_tax_incl)) as total_tax_incl, 
					((od.total_price_tax_excl / o.conversion_rate) - ((od.total_price_tax_excl/(o.total_discounts_tax_excl+o.total_paid_tax_excl))*o.total_discounts_tax_excl)) as total_tax_excl,
					IF(od.original_wholesale_price > 0, ((((od.total_price_tax_excl / o.conversion_rate) - ((od.total_price_tax_excl/(o.total_discounts_tax_excl+o.total_paid_tax_excl))*o.total_discounts_tax_excl))-(od.original_wholesale_price*od.product_quantity))/((od.total_price_tax_excl / o.conversion_rate) - ((od.total_price_tax_excl/(o.total_discounts_tax_excl+o.total_paid_tax_excl))*o.total_discounts_tax_excl))), 0) as margin_rate_ows,  
					IF(od.purchase_supplier_price > 0, ((((od.total_price_tax_excl / o.conversion_rate) - ((od.total_price_tax_excl/(o.total_discounts_tax_excl+o.total_paid_tax_excl))*o.total_discounts_tax_excl))-(od.purchase_supplier_price*od.product_quantity))/((od.total_price_tax_excl / o.conversion_rate) - ((od.total_price_tax_excl/(o.total_discounts_tax_excl+o.total_paid_tax_excl))*o.total_discounts_tax_excl))), 0) as margin_rate_psp,  
					o.id_pointshop, 
					od.product_quantity
				FROM `'._DB_PREFIX_.'order_detail` od
				LEFT JOIN '._DB_PREFIX_.'orders o ON (o.id_order = od.id_order)
				LEFT JOIN '._DB_PREFIX_.'product p ON (p.id_product = od.product_id)
				LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (p.id_product = pl.id_product)
				LEFT JOIN '._DB_PREFIX_.'product_attribute_combination pac ON (od.product_attribute_id = pac.id_product_attribute)
				LEFT JOIN '._DB_PREFIX_.'attribute a ON (a.id_attribute = pac.id_attribute)
				LEFT JOIN '._DB_PREFIX_.'attribute_group_lang pagl ON (pagl.id_attribute_group = a.id_attribute_group)
				LEFT JOIN '._DB_PREFIX_.'attribute_lang al ON (al.id_attribute = pac.id_attribute)
				LEFT JOIN '._DB_PREFIX_.'financial_sales_detail fsd ON (fsd.reference_order = o.reference)
				WHERE o.date_add BETWEEN "'.pSQL($date_from).' 00:00:00" AND "'.pSQL($date_to).' 23:59:59" ';
		
		// IF ID ORDER IS NOT NULL
		if($id_order != NULL)	
		{
			$query .= ' AND o.id_order = '.$id_order;
		}
		
		// IF ID_POINTSHOP
		if(Configuration::get('ACAISSE_last_cache_generation_pricelist') != NULL)	
		{
			if($id_pointshop > -1)	
				$query .= ' AND o.id_pointshop = '.$id_pointshop;
		}

		// IF ID_SERVICE
		if($id_service > 0)	
			$query .= ' AND fsd.id_service = '.$id_service;

		$query .= ' GROUP BY od.id_order_detail, od.product_attribute_id';

			
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
		//d($result);
		
		return $result;
        
    }

	public static function SalesExists($date, $id_pointshop)
	{
		$id_service = PrestatillFinancialStates::ServiceExists($date, $id_pointshop);
		
		// On vérifie si un état existe déjà pour la journée, si non on vérifie la veille si la caisse était toujours ouverte
		if($id_service == false)
		{
			$old_date = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT MAX(day) FROM `'._DB_PREFIX_.'financial_states`');
			
			// Si on trouve une date, on vérifie si le dernier état du jour est "caisse ouverte"
			if($old_date != false)
			{
				$id_state = PrestatillFinancialStates::isState($old_date, $id_pointshop);
				
				if($id_state == 1)
				{
					// On ajoute les informations sur le dernier rapport car la caisse est toujours ouverte	
					$date = $old_date;
				}
			}
		}	
			
		$sql = 'SELECT id_financial_sales 
				FROM `'._DB_PREFIX_.'financial_sales` 
				WHERE day = "'.$date.'"
				AND id_pointshop = '.$id_pointshop;
		
		$id_sales = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);	
						
		if($id_sales != false)
		{
			return $id_sales;
		}
		
		return false;
	}
        
        public static function CountSalesDetail($date, $id_pointshop, $id_service = 0)
	{
		$sql = 'SELECT id_financial_sales 
				FROM `'._DB_PREFIX_.'financial_sales` 
				WHERE day = "'.$date.'"
				AND id_pointshop = '.$id_pointshop;
				
		if($id_service > 0)
		{
			$sql .= 'AND id_service = '.$id_service;
		}		
		
		d($sql);		
		$id_sales = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);	
					
		if($id_sales != false)
		{
			return $id_sales;
		}
			return false;
	}

	public static function generateSaleForOrder($date_from, $date_to, $id_pointshop = 0, $id_order = null, $isFromHook = true, $id_service = 0)
	{

		// On vérifie si la commande est en ligne ou en caisseee	
		$context = Context::getContext();
		if ( _PS_VERSION_ >= '1.7') 
		{
			if($id_pointshop == 0)
				return;
		} 
		else
		{
			if($id_pointshop == 0 && $context->employee->id > 0)
				return;
		}
		
		// On récupère les détails nécessaire pour générer ou compléter le rapport des ventes
		$totalSales = self::getSalesDetail($date_from, $date_to, $id_pointshop, $id_order, $id_service);
		$date = $date_from;
		
		foreach($totalSales as $infos)
		{
                    
			// On vérifie si un rapport existe pour la date et le pointshop, si non, on le créé. 
			$id_existing_sales = self::SalesExists($infos['date'], $id_pointshop, $id_service);
			
			// On récupère le dernier service créé du jour (s'il existe)
			$id_service = PrestatillFinancialStates::ServiceExists($infos['date'], $id_pointshop);
			
			if($id_service == false )
			{
				$old_date = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT MAX(day) FROM `'._DB_PREFIX_.'financial_states`');
				
				// Si on trouve une date, on vérifie si le dernier état du jour est "caisse ouverte"
				if($old_date != false)
				{
					$other_id_sate = PrestatillFinancialStates::ServiceExists($old_date, $id_pointshop);
					
						// On ajoute les informations sur le dernier rapport car la caisse est toujours ouverte	
						$date = $old_date;
						$id_service = $other_id_sate;
				}
			}
			            
			$sales = new PrestatillFinancialSales($id_existing_sales);
			// Si pas de rapport de ventes on le créé à la volée
			if(!Validate::isLoadedObject($sales))
			{
				$sales->id_pointshop 				= $id_pointshop; 
				$sales->nb_products 				= 0;
				$sales->day	 						= $date;
				$sales->save();
			}	
			// On vérifie si la commande n'a pas déjà été enregistrée
			$id_sales_detail = PrestatillFinancialSalesDetail::DetailtExists($infos['id_order_detail']);
			
			$sales_detail = new PrestatillFinancialSalesDetail($id_sales_detail);	
			
			if(Validate::isLoadedObject($sales_detail))
                            return false;
			
			// On met à jour le total des quantités produit
			$sales->nb_products += $infos['product_quantity'];
			$sales->save();	
			
			
			$sales_detail->id_financial_sales = $sales->id;
			$sales_detail->id_service = $id_service;
			$sales_detail->id_order = $infos['id_order'];
			$sales_detail->id_product = $infos['product_id'];
			$sales_detail->id_product_attribute = $infos['product_attribute_id'];
			$sales_detail->reference_order = $infos['reference'];
			$sales_detail->id_order_detail = $infos['id_order_detail'];
			
			// On récupère le nom de la catégory à partir de l'id_category_default
			$cat = new Category($infos['id_category_default']);
			$sales_detail->category_default = Validate::isLoadedObject($cat)?$cat->getName():'-';
			
			$supplier = Supplier::getNameById($infos['id_supplier']);
			$sales_detail->supplier_name = $supplier;
			
			$manufacturer = Manufacturer::getNameById($infos['id_manufacturer']);
			$sales_detail->manufacturer_name = $manufacturer;
			
			$sales_detail->reference = $infos['product_reference'];
			$sales_detail->reference_supplier = $infos['product_supplier_reference'];
			$sales_detail->ean13 = $infos['product_ean13']!=''?$infos['product_ean13']:'';
			
			//$sales_detail->code_analytic = $infos['code_analytic'];
			
			$sales_detail->product_name = empty($infos['name'])?'-':$infos['name'];
			$sales_detail->product_attributes = empty($infos['attributes'])?'-':$infos['attributes'];
			$sales_detail->sales_type = 'VTE'; // PROVISOIRE : VTE, RET...
			$sales_detail->quantity = $infos['product_quantity'];
			$sales_detail->total_tax_incl = (float)number_format($infos['total_tax_incl'],6,'.','');
			$sales_detail->total_tax_excl = (float)number_format($infos['total_tax_excl'],6,'.','');
			
			// ON RECUPERE soit le taux de marge basé sur le pix d'achat fournisseur s'il existe
			// sinon sur le prix d'achat "original"
			if($infos['margin_rate_psp'] <> 0)
			{
				$sales_detail->margin_rate = (float)number_format($infos['margin_rate_psp'],2,'.','');
				$sales_detail->wholesale_price = (float)number_format($infos['purchase_supplier_price'],2,'.','');
			}
			else if($infos['margin_rate_ows'] <> 0)
			{
				$sales_detail->margin_rate = (float)number_format($infos['margin_rate_ows'],2,'.','');
				$sales_detail->wholesale_price = (float)number_format($infos['original_wholesale_price'],2,'.','');
			}
			else if(Configuration::get('CONF_AVERAGE_PRODUCT_MARGIN') > 0)
			{
				// A défaut on se base sur le % de la marge moyenne de Prestashop
				$sales_detail->margin_rate = (float)number_format(Configuration::get('CONF_AVERAGE_PRODUCT_MARGIN')/100,2,'.','');
				$sales_detail->wholesale_price = (float)number_format(0,2,'.','');
			}
			else 
			{
				$sales_detail->margin_rate = (float)number_format(0,2,'.','');
				$sales_detail->wholesale_price = (float)number_format(0,2,'.','');
			}
			// On check la quantité restante au moment de la commande
                        if ($isFromHook === true) {
                            $sales_detail->instant_stock = StockAvailable::getQuantityAvailableByProduct($infos['product_id'],$infos['product_attribute_id']);
                        } else {
                            $sales_detail->instant_stock = -9999;
                        }
			$sales_detail->save();	
		}
		
		// Si on génère le rapport de vente à la volée, on génère ensuite le rapport financier
		if($isFromHook == true)
			$financialReport = PrestatillFinancialReport::generateFinancialReportForOrder($date_from, $date_to, $id_pointshop, $id_order);
		
		
	}
        
    public function createDaylySalesReport($date_from, $date_to)
    {
        // IMPORTANT : on ne génère jamais un rapport le jour même, programmer la tache cron à partir de 00:01 le landemain	
        $yesterday = date('Y-m-d', strtotime("yesterday 12:00"));

        if ($date_from == false || $date_from == date('Y-m-d'))
            $date_from = $yesterday;

        if ($date_to == false || $date_to == date('Y-m-d'))
            $date_to = $yesterday;

        if (empty($id_pointshops)) {
            $id_pointshops = array(0 => 0);

            if (Configuration::get('ACAISSE_last_cache_generation_pricelist') != NULL) {
                $all_pointshops = PrestatillExportCompta::getAllPointshops(true);
                foreach ($all_pointshops as $key => $pointshop) {
                    $id_pointshops[] = $pointshop['id_a_caisse_pointshop'];
                }
            }
        }

        foreach ($id_pointshops as $id_pointshop) {
            self::generateSaleForOrder($date_from, $date_to, $id_pointshop, null, false);
        }
    }
    
    public static function getOrdersDate($date_from = false, $date_to = false) {
        if ($date_from == false)
            $date_from = date('Y-m-d');

        if ($date_to == false)
            $date_to = date('Y-m-d');

        $query = 'SELECT LEFT(o.date_add, 10) as date, 
						o.id_order,
                                                o.id_pointshop
					FROM `' . _DB_PREFIX_ . 'orders` o
					WHERE o.date_add BETWEEN "' . pSQL($date_from) . ' 00:00:00" AND "' . pSQL($date_to) . ' 23:59:59"';

        $query .= ' GROUP BY date';

        $query .= ' ORDER BY date';

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);
        return $result;
    }

}