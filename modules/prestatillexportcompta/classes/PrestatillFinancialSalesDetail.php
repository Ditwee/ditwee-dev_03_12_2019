<?php

class PrestatillFinancialSalesDetail extends ObjectModel {
    	
	public $id_financial_sales_detail;	
    	
    public $id_financial_sales;
	
	public $id_service = 1;
	
	public $id_service_cancelled = 0;
	
	public $category_default;
	
	public $id_order;
	
	public $id_product;
	
	public $id_product_attribute;
	
	public $reference_order;
	
	public $id_order_detail;
	
	public $supplier_name;
	
	public $manufacturer_name;
	
	public $reference;
	
	public $reference_supplier;
	
	public $ean13;
	
	public $code_analytic;
	
	public $product_name;
	
	public $product_attributes;
	
	public $sales_type;
	
	public $quantity;
	
	public $wholesale_price;
	
	public $total_tax_incl;
	
	public $total_tax_excl;
	
	public $margin_rate;
	
	public $instant_stock;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'financial_sales_detail',
		'primary' => 'id_financial_sales_detail',
		'multilang' => false,
		'fields' => array(
			'category_default' => 			array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'id_financial_sales' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_service' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_service_cancelled' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_order' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_product' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_product_attribute' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'reference_order' =>			array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'id_order_detail' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'supplier_name' => 				array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'manufacturer_name' => 			array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'reference' =>				    array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
			'reference_supplier' =>			array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
			'ean13' =>					    array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
			'code_analytic' => 				array('type' => self::TYPE_INT, 'validate' => 'isInt'),
			'product_name' =>				array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'product_attributes' =>			array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
			'sales_type' =>					array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'quantity' =>					array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
			'wholesale_price' =>			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_tax_incl' =>				array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_tax_excl' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'margin_rate' => 				array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'instant_stock' => 				array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
		),
	);
	
	public function __construct($id = null, $id_lang = null)
	{
		parent::__construct($id, $id_lang);
		
		
	}
	
	

	public static function DetailtExists($id_order_detail)
	{		
		if($id_detail = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT id_financial_sales_detail 
				FROM `'._DB_PREFIX_.'financial_sales_detail` 
				WHERE id_order_detail = '.$id_order_detail))
		{
			return $id_detail;
		}
			return false;
	}

	/**
	 * Get a detailed sales list of an id_financial_sales
	 * @param int $id_financial_sales
	 * @return array
	 */
	public static function getList($id_financial_sales, $id_service = 0)
	{
		$query = 'SELECT *, 
					SUM(quantity) as qty,
					SUM(total_tax_incl) as total_ttc,
					ROUND(AVG(margin_rate),2) as avg_margin_rate,
					substring_index(GROUP_CONCAT(instant_stock ORDER BY id_financial_sales_detail DESC), ",", 1) AS stock_minimum
				FROM `'._DB_PREFIX_.'financial_sales_detail` 
				WHERE `id_financial_sales` = '.(int)$id_financial_sales.'
				AND id_service_cancelled = 0
				GROUP BY id_service, `id_product`, `id_product_attribute`
				ORDER BY id_service, qty DESC ';
				
		if($id_service > 0 )
			$query .= ' AND id_service = '.$id_service;				

		$result = Db::getInstance()->executeS($query);
		
		$service = array(
			'detail' => array(),
			'total' => (float)number_format(0,6,'.',''),
			);
		
		foreach($result as $key => $res )
		{
			$service['detail'][$res['id_service']][] = $res;
			$service['total'] += (float)number_format($res['total_ttc'],6,'.','');
		}
		
		return $service;
	}
	
	
}