<?php

class PrestatillFinancialStates extends ObjectModel {
    	
	public $id_financial_state;
	
	public $id_service = 1;	
    	
    public $id_financial_report;

	public $id_pointshop;
	
	public $id_type;
	
	public $id_employee;
	
	public $employee;
	
	public $payment_mode;
	
	public $total_on_open;
	
	public $total_on_closed;
	
	public $total_movements;
	
	public $day;
	
	public $date_add;
	
	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'financial_states',
		'primary' => 'id_financial_state',
		'multilang' => false,
		'fields' => array(
			'id_service' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_financial_report' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_pointshop' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_type' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_employee' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'employee' => 					array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'payment_mode' => 				array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true),
			'message' => 					array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
			'total_on_open' =>				array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_on_closed' =>			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'total_movements' =>			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'day' => 						array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'required' => true),
			'date_add' => 					array('type' => self::TYPE_STRING, 'validate' => 'isDate', 'required' => true),
		),
	);
	
	public function __construct($id = null, $id_lang = null)
	{
		parent::__construct($id, $id_lang);
	}
	
	public static function updateCashState($day, $cash, $employee)
	{
		// On récupère le dernier service créé du jour (s'il existe)
		$id_service = PrestatillFinancialStates::ServiceExists($day, $cash->id_pointshop);
		$date = $day;
		
		if($id_service == false && $cash->type == 2)
		{
			$old_date = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT MAX(day) FROM `'._DB_PREFIX_.'financial_states`');
			
			// Si on trouve une date, on vérifie si le dernier état du jour est "caisse ouverte"
			if($old_date != false)
			{
				$other_id_sate = PrestatillFinancialStates::ServiceExists($old_date, $cash->id_pointshop);
				
				// On ajoute les informations sur le dernier rapport car la caisse est toujours ouverte	
				$date = $old_date;
			}
		}
		
		// On vérifie si la caisse est toujours ouverte de la veille. 
		$id_existing_report = PrestatillFinancialReport::ReportExists($date, $cash->id_pointshop, $cash->type);
		
		if($id_service == false && $cash->type == 1)
		{
			$id_service = 1; // On est dans le premier service
			
			if($id_existing_report == false)
			{
				$report = new PrestatillFinancialReport();	
				$report->id_pointshop 				= $cash->id_pointshop; // A dynamiser
				$report->nb_sales 					= 0;
				$report->id_service 				= $id_service;
				$report->total_tax_incl 			= (float)number_format(0,6,'.','');
				$report->total_tax_excl 			= (float)number_format(0,6,'.','');
				$report->total_vat 					= (float)number_format(0,6,'.',''); 
				$report->total_payment 				= 0;
				$report->total_discounts_tax_incl 	= (float)number_format(0,6,'.','');//(float)number_format($infos['total_discounts_ttc'],6,'.','');
				$report->total_discounts_tax_excl 	= (float)number_format(0,6,'.',''); //(float)number_format($infos['total_discounts_ht'],6,'.','');
				$report->conversion_rate 			= 0.000000;
				$report->id_currency 				= 2; // A dynamiser
				$report->day	 					= $day;
				$report->date_add	 				= date('Y-m-d H:i:s');
				$report->save();
				
				$id_existing_report = $report->id;
				
				// On créé le rapport TVA de base
				$activeTaxeRules = TaxRulesGroup::getTaxRulesGroups(true);
			
				// MAJ : on ajoute la notion de "aucune taxe"
				$activeTaxeRules[] = array
										(
								            'id_tax_rules_group' => 0,
								            'name' => 'Aucune taxe',
								           	'active' => 1
								        );
										
				foreach($activeTaxeRules as $taxe)
				{
					$vat = new PrestatillFinancialReportVat();
					$vat->id_financial_report = $report->id;
					$vat->id_tax_rules_group = $taxe['id_tax_rules_group'];
					$vat->tax_name = $taxe['name'];
					$vat->amount = (float)number_format(0,6,'.','');
					$vat->total_ht = (float)number_format(0,6,'.','');
					$vat->id_service = $report->id_service;
					$vat->save();
				}
			}
		}
		else if($id_service != false && $cash->type == 1)
		{
			$id_service++;
			
			$report = new PrestatillFinancialReport($id_existing_report);
			$report->id_service = $id_service;
			$report->save();
			
		}
		
		//d($id_existing_report)	
		
		// On créé l'état de caisse (uniquement si on est pas dans une préparation)
		if($cash->type !== 5 && $cash->type !== 6)
		{
			if($id_service == false)
			{
				$report = new PrestatillFinancialReport($id_existing_report);
				$id_service = $report->id_service;
			}	
				
			$new_state = new PrestatillFinancialStates();
			$new_state->id_financial_report = $id_existing_report;
			$new_state->id_service = $id_service;
			$new_state->id_pointshop = $cash->id_pointshop;
			$new_state->id_type = $cash->type;
			$new_state->id_employee = $cash->id_employee;
			$new_state->employee = $employee->firstname.' '.$employee->lastname;
			$new_state->payment_mode = 'Espèces'; // A dynamiser avec les autres futurs modes
			$new_state->message = $cash->message;
			$new_state->total_on_open = (float)number_format(0,6,'.','');
			$new_state->total_movements = (float)number_format(0,6,'.','');
			$new_state->total_on_closed = (float)number_format(0,6,'.','');
			$new_state->day = $date;
			$new_state->date_add = date('Y-m-d H:i:s');
			
			// On est dans le cas d'une ouverture
			if($cash->type == 1)
			{
				// On créé le rapport du service (et non plus du jour)
				$new_state->total_on_open = (float)number_format($cash->total_cash/100,6,'.','');
			}
			
			// On est dans le cas d'une fermeture
			if($cash->type == 2)
			{
				// On créé le rapport du service (et non plus du jour)
				$new_state->total_on_closed = (float)number_format($cash->total_cash/100,6,'.','');
			}
			
			// On est dans le cas d'un mouvement
			if($cash->type == 3)
			{
				// On créé le rapport du service (et non plus du jour)
				$new_state->total_movements += (float)number_format($cash->total_cash/100,6,'.','');
			}																
		
			$new_state->save();
		}
	}

	public static function getStates($id_financial_report, $day, $id_pointshop = 0)
	{
		$query = 'SELECT * 
					FROM `'._DB_PREFIX_.'financial_states` 
					WHERE `id_financial_report` = '.(int)$id_financial_report.'
					AND id_pointshop = '.$id_pointshop.'
					AND day LIKE \''.$day.'\'';	
		
		$result = Db::getInstance()->executeS($query);
		
		$services = array();
		foreach($result as $key => $row)
		{
			$services[$row['id_service']][$row['id_type']] = $row;
		}
		
		return $services;
		
	}
	
	public static function ServiceExists($day, $id_pointshop)
	{
		if($id_service = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT MAX(id_service) 
				FROM `'._DB_PREFIX_.'financial_states` 
				WHERE day = "'.$day.'"
				AND id_type <> 2
				AND id_pointshop = '.$id_pointshop.' 
				'))
		{
			return $id_service;
		}
			return false;
	}	
	
	public static function isState($day, $id_pointshop)
	{
		if($id_type = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT id_type
				FROM `'._DB_PREFIX_.'financial_states` 
				WHERE day = "'.$day.'"
				AND id_pointshop = '.$id_pointshop.' 
				'))
		{
			return $id_type;
		}
			return false;
	}	

}