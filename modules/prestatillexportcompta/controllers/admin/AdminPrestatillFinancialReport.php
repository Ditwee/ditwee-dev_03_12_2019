<?php

class AdminPrestatillFinancialReportController extends ModuleAdminController{
			
	public function __construct()
	{
		
		$this->bootstrap = true;	
			
		$this->table = 'financial_report';
		$this->className = 'PrestatillFinancialReport';
		$this->addRowAction('view');
		$this->lang = false;
		$this->deleted = false;
        
        $this->context = Context::getContext();
				
		$this->_select = 'IF(ap.name IS NULL, "En ligne", ap.name) as pointshop_name';
		$this->_join.= '
			LEFT JOIN `'._DB_PREFIX_.'a_caisse_pointshop` ap ON (ap.`id_a_caisse_pointshop` = a.`id_pointshop`)';
		$this->_orderWay = 'a.day';
		$this->_orderWay = 'DESC';	
		
        $this->fields_list = array(
            'day' => array(
                'title' => $this->l('Date'), 
                'align' => 'center', 
                'type' => 'date',
            ),
            'pointshop_name' => array(
                'title' => $this->l('Point de vente'), 
                'align' => 'center', 
				'filter_key' => 'ap!name'
            ),
            /*
            'nb_sales' => array(
                'title' => $this->l('Ventes'), 
                'align' => 'center', 
                'type' => 'int',
            ),*/
            'total_tax_incl' => array(
                'title' => $this->l('Total TTC'), 
                'align' => 'center', 
                'type' => 'price',
                'class' => ''
            ),
            'total_tax_excl' => array(
                'title' => $this->l('Total HT'), 
                'align' => 'center', 
                'type' => 'price'
            ),
            'total_vat' => array(
                'title' => $this->l('Total TVA'), 
                'align' => 'center', 
                'type' => 'price'
            ),
            'total_payment' => array(
                'title' => $this->l('Encaissements'), 
                'align' => 'center', 
                'type' => 'price'
            ),
            'total_discounts_tax_incl' => array(
                'title' => $this->l('Total remises TTC'), 
                'align' => 'center', 
                'type' => 'price'
            ),
            'total_discounts_tax_excl' => array(
                'title' => $this->l('Total remises HT'), 
                'align' => 'center', 
                'type' => 'price'
            ),
		);
		
		
		$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		
		//Si l'employee connecté est un vendeur de la marketplace
		if($sellers){
			
			//unset($this->fields_list['pointshop_name']);
			
			$seller = new Seller((int)$sellers);
			$pointshop = explode(";", $seller->pointshops);
			//$this->_join.= '
			//	LEFT JOIN `'._DB_PREFIX_.'seller` s ON (s.`pointshops` = a.`id_pointshop` AND s.`pointshops` IN ('.implode(', ', $pointshop).'))';
			$this->_where.= ' AND a.`id_pointshop` IN ('.implode(', ', $pointshop).')';
			
		}
		
		
		parent::__construct();
		
	}

	public function renderView()
	{
		$tpl = $this->context->smarty->createTemplate(
			dirname(__FILE__).'/../../views/templates/admin/view_financial_report.tpl'
		);
		
		//$reporrrt = PrestatillFinancialReport::createDaylyFinancialReport('2017-01-22','2017-01-22', $id_pointshop = array(0 => 0, 1 => 1));
		//d($reporrrt);
		
		$id_financial_report = Tools::getValue('id_financial_report');
		
		$report = new PrestatillFinancialReport($id_financial_report);
		
		if(Validate::isLoadedObject($report))
		{
			$report_infos = PrestatillFinancialReport::getReportAllInfos($report->id);
			
			//d($balances);
			$pointshop_name = PrestatillExportCompta::getPointshopName($report->id_pointshop);
			
			$params_hook = array(
				'id_pointshop' => $report_infos['report']->id_pointshop,
				'pointshop_name' => $pointshop_name,
				'day' => $report_infos['report']->day,
				'report' => $report_infos['report'],
				'report_details' => $report_infos['report_details'],
				'report_vat' => $report_infos['report_vat'],
				'report_payment' => $report_infos['report_payment'],
				'report_payment_wb' => $report_infos['report_payment_wb'],
				'report_states' => $report_infos['report_states'],
				'balances' => $report_infos['balances'],
			);
			
			$tpl->assign(array(
	            'pointshop_name' => $pointshop_name,
	            'tocken' => $this->token,
	            'report' => $report_infos['report'],
	            'currency' => $this->context->currency,
	            'report_details' => $report_infos['report_details'],
	            'report_vat' => $report_infos['report_vat'],
	            'report_payment' => $report_infos['report_payment'],
				'report_payment_wb' => $report_infos['report_payment_wb'],
	            'balances' => $report_infos['balances'],
				'report_detail_cancelled' => $report_infos['report_detail_cancelled'],
				'total_cancelled' => $report_infos['total_cancelled'],
				'total_details' => $report_infos['total_details'],
				'services_in_day' => $report_infos['services_in_day'],
				'report_states' => $report_infos['report_states'],
	            'tocken_order' => Tools::getAdminTokenLite('AdminOrders'),
	            'HOOK_PRESTATILL_HEADER' => Hook::exec('displayPrestatillFinancialReportHeader',$params_hook),
	            'HOOK_PRESTATILL_FOOTER' => Hook::exec('displayPrestatillFinancialReportFooter',$params_hook),
	            'HOOK_PRESTATILL_AFTER_REPORT' => Hook::exec('displayPrestatillFinancialReportAfterReport',$params_hook),
	        	)
			);
		
			return $tpl->fetch();
		}
		else 
		{
			return false;
		}	
	}
	
	public function initToolbar()
	{
		parent::initToolbar();
		
		if(0+Tools::getValue('id_financial_report') > 0)
		{
			$this->page_header_toolbar_btn['exportFinancialReport'] = array(
	              'href' => self::$currentIndex.'&exportFinancialReport='.(int)Tools::getValue('id_financial_report').'&token='.$this->token,
	              'desc' => $this->l('Exporter le rapport complet'),
	              'icon' => 'process-icon-hey icon-file-excel-o'
	        );
			/*
			$this->page_header_toolbar_btn['exportFinancialReportMonth'] = array(
	              'href' => self::$currentIndex.'&exportFinancialReportMonth='.(int)Tools::getValue('id_financial_report').'&token='.$this->token,
	              'desc' => $this->l('CSV Export (MONTH)'),
	              'icon' => 'process-icon-hey icon-file-excel-o'
	        ); */
        }
		
		unset($this->toolbar_btn['new']);
	}
	
	
	public function initContent()
	{
		$id_serv = 0;
		
		$id_financial_report = 0;
		if (Tools::isSubmit('id_financial_report') && Tools::getValue('id_financial_report') > 0) {
			$id_financial_report = Tools::getValue('id_financial_report');
		}
		if (Tools::getValue('exportFinancialReport'))
		{
			$id_financial_report = Tools::getValue('exportFinancialReport');
		}
		if (Tools::getValue('exportFinancialReportMonth'))
		{
			$id_financial_report = Tools::getValue('exportFinancialReportMonth');
		}
		
		$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		if($sellers && $id_financial_report){
				
			$financial_report = new PrestatillFinancialReport((int)$id_financial_report);
			
			//unset($this->fields_list['pointshop_name']);
			
			$seller = new Seller((int)$sellers);
			$pointshop = explode(";", $seller->pointshops);
			
			if( !in_array($financial_report->id_pointshop, $pointshop)){
				
				$this->errors[] = Tools::displayError('The order cannot be found within your database.');
				return false;
			
			}
			
		}

		if(Tools::getValue('id_service'))
		{
			$id_serv = (int)Tools::getValue('id_service');
		}	
			
		if (Tools::getValue('exportFinancialReport'))
		{			
			PrestatillExportCompta::exportFinancialReport((int)Tools::getValue('exportFinancialReport'), $id_serv,'csv');
			die();	
		}

		if (Tools::getValue('exportFinancialReportMonth'))
		{						
			PrestatillExportCompta::exportFinancialReportMonth((int)Tools::getValue('exportFinancialReportMonth'),'csv');
			die();	
		}
		

		parent::initContent();
	}
	
}