<?php

class AdminPrestatillFinancialSalesController extends ModuleAdminController{
	
	public $toolbar_title;
		
	public function __construct()
	{
		
		$this->bootstrap = true;	
			
		$this->table = 'financial_sales';
		$this->className = 'PrestatillFinancialSales';
		$this->addRowAction('view');
		$this->lang = false;
		$this->deleted = false;
		$this->identifier='id_'.$this->table;
        
        $this->context = Context::getContext();
		
		
		$this->_select = 'IF(ap.name IS NULL, "En ligne", ap.name) as pointshop_name';
		$this->_join = '
			LEFT JOIN `'._DB_PREFIX_.'a_caisse_pointshop` ap ON (ap.`id_a_caisse_pointshop` = a.`id_pointshop`)';
		
        $this->fields_list = array(
            'day' => array(
                'title' => $this->l('Date'), 
                'align' => 'center', 
                'type' => 'date',
            ),
            'pointshop_name' => array(
                'title' => $this->l('Point de vente'), 
                'align' => 'center',
				'filter_key' => 'ap!name'				
            ),
            'nb_products' => array(
                'title' => $this->l('Nbr. de produits'), 
                'align' => 'center', 
                'type' => 'int',
            ),
            
		);
		
		$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		
		//Si l'employee connecté est un vendeur de la marketplace
		if($sellers){
			
			//unset($this->fields_list['pointshop_name']);
			
			$seller = new Seller((int)$sellers);
			$pointshop = explode(";", $seller->pointshops);
			//$this->_join.= '
			//	LEFT JOIN `'._DB_PREFIX_.'seller` s ON (s.`pointshops` = a.`id_pointshop` AND s.`pointshops` IN ('.implode(', ', $pointshop).'))';
			$this->_where.= ' AND a.`id_pointshop` IN ('.implode(', ', $pointshop).')';
			
		}
		
		parent::__construct();
		
	}

	public function initToolbar()
	{
		parent::initToolbar();
		unset($this->toolbar_btn['new']);
	}
	
	public function renderView()
	{
		$tpl = $this->context->smarty->createTemplate(
			dirname(__FILE__).'/../../views/templates/admin/view_financial_sales.tpl'
		);
		
		$id_financial_sales = Tools::getValue('id_financial_sales');
		
		$sales = new PrestatillFinancialSales($id_financial_sales);
		
		$pointshop_name = PrestatillExportCompta::getPointshopName($sales->id_pointshop);
			
		$sales_details = PrestatillFinancialSalesDetail::getList($sales->id);
		
		//d($sales_details);
		//d($test);
		
		$params_hook = array(
			'id_pointshop' => $sales->id_pointshop,
			'pointshop_name' => $pointshop_name,
			'id_financial_sales' => $id_financial_sales,
			'sales' => $sales,
			'sales_details' => $sales_details,
		);
		
		$tpl->assign(array(
	            'pointshop_name' => $pointshop_name['name'],
	            'sales' => $sales,
	            'currency' => $this->context->currency,
	            'sales_details' => $sales_details,
	            'tocken_product' => Tools::getAdminTokenLite('AdminProducts'),
	            'HOOK_PRESTATILL_HEADER' => Hook::exec('displayPrestatillFinancialSalesHeader',$params_hook),
	            'HOOK_PRESTATILL_FOOTER' => Hook::exec('displayPrestatillFinancialSalesFooter',$params_hook),
	           )
			);
		
		return $tpl->fetch();
	}
	
	public function postProcess()
	{
		if(Tools::isSubmit('generateSales'))
		{
			$id_pointshops = array(0 => 0);	// On définit le pointshop EN LIGNE
		
			if(Configuration::get('ACAISSE_last_cache_generation_pricelist') != NULL)	
			{
				$pointshops = ACaissePointshop::getAllPointshop(true);
				foreach($pointshops as $pointshop)
					$id_pointshops[] = $pointshop['id_a_caisse_pointshop'];
			}
				
			foreach($id_pointshops as $id_pointshop)
			{
				$totalSales = PrestatillFinancialSales::getSalesDetail('2016-01-05','2017-07-05', $id_pointshop);	// RENDRE DYNAMIQUE	
				
				foreach($totalSales as $infos)
				{
					// On vérifie si un rapport existe pour la date et le pointshop, si non, on le créé. 
					$id_existing_sales = PrestatillFinancialSales::SalesExists($infos['date'], $id_pointshop);
					
					$sales = new PrestatillFinancialSales($id_existing_sales);
					
					// Si pas de rapport de ventes on le créé à la volée
					if(!Validate::isLoadedObject($sales))
					{
						$sales->id_pointshop 				= $id_pointshop; 
						$sales->nb_products 				= $infos['product_quantity'];
						$sales->day	 						= $infos['date'];
						$sales->save();
					}
					else
					{
							
						// On vérifie si la commande n'a pas déjà été enregistrée
						$id_sales_detail = PrestatillFinancialSalesDetail::DetailtExists($infos['id_order_detail']);
						
						$sales_detail = new PrestatillFinancialSalesDetail($id_sales_detail);	
						
						if(Validate::isLoadedObject($id_sales_detail))
						{
							return false;
						}
						 
						//$sales_detail = new PrestatillFinancialSalesDetail($id_sales_detail);
						
						$sales_detail->id_financial_sales = $sales->id;
						$sales_detail->id_order = $infos['id_order'];
						$sales_detail->reference_order = $infos['reference'];
						$sales_detail->id_order_detail = $infos['id_order_detail'];
						
						// On récupère le nom de la catégory à partir de l'id_category_default
						$cat = new Category($infos['id_category_default']);
						$sales_detail->category_default = $cat->getName();
						
						$supplier = Supplier::getNameById($infos['id_supplier']);
						$sales_detail->supplier_name = $supplier;
						
						$manufacturer = Manufacturer::getNameById($infos['id_manufacturer']);
						$sales_detail->manufacturer_name = $manufacturer;
						
						$sales_detail->reference = $infos['reference'];
						$sales_detail->reference_supplier = $infos['reference_supplier'];
						$sales_detail->ean13 = $infos['product_ean13'];
						
						//$sales_detail->code_analytic = $infos['code_analytic'];
						
						$sales_detail->product_name = $infos['name'];
						$sales_detail->product_attributes = $infos['attributes'];
						$sales_detail->sales_type = 'VTE'; // PROVISOIRE : VTE, RET...
						$sales_detail->quantity = $infos['product_quantity'];
						$sales_detail->wholesale_price = $infos['product_quantity'];
						$sales_detail->total_tax_incl = number_format($infos['total_tax_incl'], 6);
						$sales_detail->total_tax_excl = number_format($infos['total_tax_excl'], 6);
						
						// ON RECUPERE soit le taux de marge basé sur le pix d'achat fournisseur s'il existe
						// sinon sur le prix d'achat "original"
						if($infos['margin_rate_psp'])
						{
							$sales_detail->margin_rate = number_format($infos['margin_rate_psp'], 2);
						}
						else 
						{
							$sales_detail->margin_rate = number_format($infos['margin_rate_ows'], 2);
						}
						// On check la quantité restante au moment de la commande
						$sales_detail->instant_stock = StockAvailable::getQuantityAvailableByProduct($infos['product_id'],$infos['product_attribute_id']);
						
						$sales_detail->save();
						
					}
				}
			}
		}

		parent::postProcess();
	}

	public function initContent()
	{	
	
		if (Tools::isSubmit('id_financial_sales') && Tools::getValue('id_financial_sales') > 0) {
			$id_financial_sales = Tools::getValue('id_financial_sales');
		}
	
		$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		
		if($sellers && $id_financial_sales){
			
			$financial_report = new PrestatillFinancialSales((int)$id_financial_sales);
			
			//unset($this->fields_list['pointshop_name']);
			
			$seller = new Seller((int)$sellers);
			$pointshop = explode(";", $seller->pointshops);
			
			if( !in_array($financial_report->id_pointshop, $pointshop)){
				
				$this->errors[] = Tools::displayError('The order cannot be found within your database.');
				return false;
			
			}
		}

		parent::initContent();
		
	}
}