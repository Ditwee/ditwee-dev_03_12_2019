<?php
/**
* Prestatill - Advenaced financial repport
*
*
*  @author    La Boîte à Balises lbab.fr contact@lbab.fr
*  @copyright 2013-2018 La Boîte à Balises
*  @license   La Boîte à Balises
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/prestatillexportcompta/prestatillexportcompta.php');

class AdminPrestatillGlobalReportController extends ModuleAdminController
{
    public function __construct()
    {
        $this->table = 'financial_report';
        $this->className = 'PrestatillFinancialReport';
        $this->bootstrap = true;
        $this->lang  = false;
        //$this->addRowAction('view');
        
        /**
SELECT 
		 YEAR(day) as year, MONTH(day) AS month,  sum(fr.nb_sales) as nb_sale , sum(fr.total_tax_incl) as total_tax_incl , sum(fr.total_tax_excl) as total_tax_excl , sum(fr.total_vat) as total_vat , sum(fr.total_payment) as total_payment , sum(fr.total_discounts_tax_incl) as total_discounts_tax_incl , sum(fr.total_discounts_tax_excl) as total_discounts_tax_excl
		  
FROM ps_financial_report fr
GROUP BY year,month ORDER BY year DESC, month DESC
		 */
        
        $id_pointshop = -1;
		$group_by_id_pointshop = false;
        
        $this->_select = '
        					id_pointshop,
        					YEAR(day) as year,
        					MONTH(day) AS month,
        					CONCAT(CONCAT(YEAR(day),"-"),RIGHT(CONCAT("0",MONTH(day)),2)) as the_date,
        					ROUND(sum(a.nb_sales),2) as nb_sale,
        					ROUND(sum(a.total_tax_incl),2) as total_tax_incl,
        					ROUND(sum(a.total_tax_excl),2) as total_tax_excl,
        					ROUND(sum(a.total_vat),2) as total_vat,
        					ROUND(sum(a.total_payment),2) as total_payment,
        					ROUND(sum(a.total_discounts_tax_incl),2) as total_discounts_tax_incl,
        					ROUND(sum(a.total_discounts_tax_excl),2) as total_discounts_tax_excl';
						
		if($id_pointshop != -1)
		{
        	$this->_where .= 'AND id_pointshop = ' . (int)$id_pointshop;
		}
						
		$this->_group = 'GROUP BY year,month'.($group_by_id_pointshop == true && $id_pointshop == -1?',id_pointshop':'');
		$this->_order = 'ORDER BY year DESC,month DESC';
			//d($this);			
        /*
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'customer c ON (c.id_customer = a.id_customer)
                        LEFT JOIN '._DB_PREFIX_.'order_state_lang os ON (os.id_order_state = a.current_state)
                        LEFT JOIN '._DB_PREFIX_.'prestatill_drive_creneau pdc ON (pdc.id_order = a.id_order)';
        */
        $this->fields_list = array(
            'id_pointshop' => array(
                'title' => $this->l('Point de vente'),
                'align' => 'center',
            	'remove_onclick' => true,
            ),
            'the_date' => array(
                'title' => $this->l('Mois'),
                'align' => 'center',
            	'remove_onclick' => true,
            ),
            'nb_sale' => array(
                'title' => $this->l('nb_sale'),
                'align' => 'right',
            	'remove_onclick' => true,
            ),
            'total_tax_incl' => array(
                'title' => $this->l('total_tax_incl'),
                'align' => 'right',
            	'remove_onclick' => true,
            ),
            'total_tax_excl' => array(
                'title' => $this->l('total_tax_excl'),
                'align' => 'right',
            	'remove_onclick' => true,
            ),
            'total_vat' => array(
                'title' => $this->l('total_vat'),
                'align' => 'right',
            	'remove_onclick' => true,
            ),
            'total_payment' => array(
                'title' => $this->l('total_payment'),
                'align' => 'right',
            	'remove_onclick' => true,
            ),
            'total_discounts_tax_incl' => array(
                'title' => $this->l('total_discounts_tax_incl'),
                'align' => 'right',
            	'remove_onclick' => true,
            ),
            'total_discounts_tax_excl' => array(
                'title' => $this->l('total_discounts_tax_excl'),
                'align' => 'right',
            	'remove_onclick' => true,
            ),

        );




        parent::__construct();
    }

	/*
    public function initToolbar()
    {

        if ($this->display == 'view') {
            $id_order = Tools::getValue('id_order');
            $order = new Order($id_order);
            if (Validate::isLoadedObject($order)) {
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminOrders').'&vieworder&id_order='.(int)$id_order);
            }
        }
        return parent::initToolbar();
    }

    public function createTemplate($tpl_name)
    {
        if (file_exists(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name) && $this->viewAccess()) {
            return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name, $this->context->smarty);
        } elseif (file_exists($this->getTemplatePath().$this->override_folder.$tpl_name) && $this->viewAccess()) {
            return $this->context->smarty->createTemplate($this->getTemplatePath().$this->override_folder.$tpl_name, $this->context->smarty);
        }

        return parent::createTemplate($tpl_name);
    }
	 */

    /**
     * Get path to back office templates for the module
     *
     * @return string
     */
    public function getTemplatePath()
    {
        return _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/';
    }
}