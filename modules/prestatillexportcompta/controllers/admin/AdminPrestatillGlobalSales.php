<?php
/**
* Prestatill - Advenaced financial repport
*
*
*  @author    La Boîte à Balises lbab.fr contact@lbab.fr
*  @copyright 2013-2018 La Boîte à Balises
*  @license   La Boîte à Balises
*/

require_once(dirname(_PS_MODULE_DIR_).'/modules/prestatillexportcompta/prestatillexportcompta.php');

class AdminPrestatillGlobalSalesController extends ModuleAdminController
{
    public function __construct()
    {
        $this->table = 'order_detail';
        $this->className = 'OrderDetail';
        $this->bootstrap = true;
        $this->lang  = false;
        //$this->addRowAction('view');
		
		
		$context = Context::getContext();
		$cookie = $context->cookie;
			
			
	    
		$id_pointshop = $cookie->__get('prestatillglobalsalesorder_detailFilter_id_pointshop');
			
		$fields = $this->_loadFieldsFromCookie();

		
		if(empty($fields))
		{		
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_id_product',1);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_id_product_attribute',1);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_pointshop',1);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_order_date',1);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_order_reference',0);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_product_name',1);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_quantity',1);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_quantity_refund',0);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_refund_rate',0);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_reference',1);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_supplier_reference',0);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_unit_price_ttc',0);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_unit_price_ht',0);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_original_price_ht',0);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_supplier_price_ht',0);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_marge',1);
			$cookie->__set('prestatillglobalsalesorder_detailFilter_view_taux_de_marque',0); 
			
			
			$fields = $this->_loadFieldsFromCookie();			
		}

		$this->fields_list = $fields;

		
        parent::__construct();
	}

	private function _loadFieldsFromCookie()
	{
		$context = Context::getContext();
		$cookie = $context->cookie;
		$fields = array();
		
		$this->_filter = '';

		//d($cookie);
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_id_product') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_id_product') == 1)
		{		
        	$fields['product_id'] = array(
                'title' => $this->l('#ID Prod.'),
                'align' => 'center',
           		'class' => 'fixed-width-xs',
           		/*'width' => 10*/
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_id_product_attribute') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_id_product_attribute') == 1)
		{	
            $fields['product_attribute_id'] = array(
                'title' => $this->l('#ID Décl.'),
                'align' => 'center',
            	'class' => 'fixed-width-xs',
            );
		}
    	
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_pointshop') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_pointshop') == 1)
		{				 
		 	$fields['id_pointshop'] = array(
                'title' => $this->l('Point de vente'),	
                'search' => false,	
           		'class' => 'fixed-width-xs', 
           		'callback' => 'getPrettyPointshopDisplay',
			 );		 				
		}	
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_order_date') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_order_date') == 1)
		{
				
		    $fields['date_cmd'] = array(
                'title' => $this->l('Date Cmd.'),
                'align' => 'center',
                'type' => 'date',
                'filter_key' => 'o!date_add',
                'order_key' => 'o!date_add',
                'search' => false,
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_order_reference') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_order_reference') == 1)
		{
				
		    $fields['order_reference'] = array(
                'title' => $this->l('Réf. Cmd.'),
                'align' => 'center',
                'search' => false,
                'filter_key' => 'o!order_reference',
                'order_key' => 'o!order_reference',
            );
		}
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_product_name') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_product_name') == 1)
		{
		
            $fields['product_name'] = array(
                'title' => $this->l('Produit'),
                'align' => 'center',
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_quantity') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_quantity') == 1)
		{
			
            $fields['product_quantity'] = array(
                'title' => $this->l('Qu vendue'),
                'search' => false,	 
           		'class' => 'fixed-width-xs',
            );  
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_quantity_refund') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_quantity_refund') == 1)
		{          
			
            $fields['product_quantity_refunded'] = array(
                'title' => $this->l('Qu Remboursée'),
                'search' => false,	 
           		'class' => 'fixed-width-xs',
            );
		}		

		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_refund_rate') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_refund_rate') == 1)
		{
			
            $fields['refund_rate'] = array(
                'title' => $this->l('Taux de retour'),
                'search' => false,	 
           		'class' => 'fixed-width-xs',
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_reference') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_reference') == 1)
		{
			
            $fields['product_reference'] = array(
                'title' => $this->l('Réf. produit'),
                'align' => 'center',
                'filter_key' => 'product_reference',
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_supplier_reference') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_supplier_reference') == 1)
		{
			
            $fields['product_supplier_reference'] = array(
                'title' => $this->l('Réf. fournisseur'),
                'align' => 'center',
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_unit_price_ttc') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_unit_price_ttc') == 1)
		{
            
            $fields['unit_price_tax_incl'] = array(
                'title' => $this->l('PU TTC moyen'),
                'search' => false,	 
           		'class' => 'fixed-width-xs',
	            'type' => 'price',
	            'align' => 'text-right',
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_unit_price_ht') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_unit_price_ht') == 1)
		{
            
            $fields['unit_price_tax_excl'] = array(
                'title' => $this->l('PU HT moyen'),
                'search' => false,	 
           		'class' => 'fixed-width-xs',
	            'type' => 'price',
	            'align' => 'text-right',
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_original_price_ht') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_original_price_ht') == 1)
		{
            
            $fields['original_product_price'] = array(
                'title' => $this->l('Prix de vente original HT'),
                'search' => false,	 
           		'class' => 'fixed-width-xs',
	            'type' => 'price',
	            'align' => 'text-right',
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_supplier_price_ht') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_supplier_price_ht') == 1)
		{
            
            $fields['original_wholesale_price'] = array(
                'title' => $this->l('Prix achat HT'),
                'search' => false,	 
            );  
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_marge') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_marge') == 1)
		{          
			
            $fields['marge'] = array(
                'title' => $this->l('Marge HT'),
            	'remove_onclick' => true,
                'search' => false,	 
           		'class' => 'fixed-width-xs',
	            'type' => 'price',
	            'align' => 'text-right',
            );
		}		
			
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_view_taux_de_marque') === null || (int)$cookie->__get('prestatillglobalsalesorder_detailFilter_view_taux_de_marque') == 1)
		{
            
            $fields['taux_de_marque'] = array(
                'title' => $this->l('taux_de_marque'),
            	'remove_onclick' => true,
                'search' => false,	 
           		'class' => 'fixed-width-xs',
	            'align' => 'text-right',
        	);
		}
		return $fields;
	}

	static private $_pointshops_name = array();

	public function getPrettyPointshopDisplay($value, $tr)
	{
		$context = Context::getContext();
		$cookie = $context->cookie;
		
		if($cookie->__get('prestatillglobalsalesorder_detailFilter_group_line') !== 'false')
		{
			return _('--');
		}
		
		
		
		if(!isset(self::$_pointshops_name[(int)$value]))
		{
			$name = '' . (int)$value;
			
			if($value == 0)
			{
				$name = _('Vente en ligne');
			}
			else
			{
				$sql = 'SELECT name FROM '._DB_PREFIX_.'a_caisse_pointshop WHERE id_a_caisse_pointshop = '.(int)$value;
				$name = DB::getInstance()->getValue($sql);
			}
			self::$_pointshops_name[(int)$value] = $name;
		}
		return self::$_pointshops_name[(int)$value];		
	}
	
	public function initContent()
	{
		
		
		
		/*
		p($fields);		
		p($cookie);
		p($cookie->__get('prestatillglobalsalesorder_detailFilter_view_taux_de_marque'));
		*/
		
		/**
		 * More options
		 */
		$this->list_no_link = true; //surpression de la ligne clicable
		$this->allow_export = true; //autorise l'export CSV
		//$this->no_filter = true; //autorise l'export CSV


        parent::initContent();
		
		//d($this->_list);
    }


	/*
    public function initToolbar()
    {

        if ($this->display == 'view') {
            $id_order = Tools::getValue('id_order');
            $order = new Order($id_order);
            if (Validate::isLoadedObject($order)) {
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminOrders').'&vieworder&id_order='.(int)$id_order);
            }
        }
        return parent::initToolbar();
    }

    public function createTemplate($tpl_name)
    {
        if (file_exists(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name) && $this->viewAccess()) {
            return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name, $this->context->smarty);
        } elseif (file_exists($this->getTemplatePath().$this->override_folder.$tpl_name) && $this->viewAccess()) {
            return $this->context->smarty->createTemplate($this->getTemplatePath().$this->override_folder.$tpl_name, $this->context->smarty);
        }

        return parent::createTemplate($tpl_name);
    }
	 */

    /**
     * Get path to back office templates for the module
     *
     * @return string
     */
    public function getTemplatePath()
    {
        return _PS_MODULE_DIR_.$this->module->name.'/views/templates/admin/';
    }
}