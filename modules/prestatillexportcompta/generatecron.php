<?php
/*
* 2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*/

if (!defined('_PS_ADMIN_DIR_')) {
    define('_PS_ADMIN_DIR_', getcwd());
}

include(_PS_ADMIN_DIR_.'/../../config/config.inc.php');

if (substr(_COOKIE_KEY_, 34, 8) != Tools::getValue('token')) {
    die;
}

require_once(dirname(__FILE__).'/prestatillexportcompta.php');

//ini_set('max_execution_time', 7200);

PrestatillFinancialReport::createDaylyFinancialReport();

if (Tools::getValue('redirect') && isset($_SERVER['HTTP_REFERER'])) {
    Tools::redirectAdmin($_SERVER['HTTP_REFERER'].'&conf=4');
}
