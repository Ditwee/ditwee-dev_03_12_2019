$(window).load(function() {
    
    var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	
	if(dd<10) {
	    dd = '0'+dd
	} 
	
	if(mm<10) {
	    mm = '0'+mm
	} 
	
	today = yyyy + '-' + mm + '-' + dd;
    
    /**
     * Handle menu tab changement
     */
    $('.menu_tab').click(function(){
        $('.menu_tab').removeClass('active');
         $('.tab-pane').removeClass('active');
   		$(this).addClass('active');
    });
    
    $("#generateFinancialReport").click(function(e){
    	e.preventDefault();
    	
	    $.ajax({
	        type: "post",
	        async:true,
	        data: {
	          "action": "generateFinancialReport",
	          "date_from": $("#date_from").val(),
	          "date_to": $("#date_to").val(),
	        },
	        context:$(this),
	        dataType: "json",
	      }).done(function (dates) {
	      		$('#generateFinancialReport').prop('disabled', true);
	      		
	      		var dates = dates;
	      		console.log(dates);
	      		
	      		if(dates !== 0)
	      		{
	      			$("#cache_loader").html("<span class=\"count\">0</span> / "+dates.length+" rapports financiers créés").show();
	      			
	      			var _loadNext = function()
		            {
		                
		                var date = dates[0];
		                ///////////////////////////		                
		                if(date == today)
		                {
		                	$('#generateFinancialReport').prop('disabled', false);
	                		$("#cache_loader").removeClass('alert-success').addClass('alert-warning');
	                		$("#cache_loader").html("Attention, le rapport financier du jour ne peut être créé que le jour suivant.").show();
	                		return;
		                }
		                ///////////////////////////
		                console.log(dates.length);
		                if(dates.length == 0)
	                	{
	                		$('#generateFinancialReport').prop('disabled', false);
	                		$("#cache_loader").removeClass('alert-warning').addClass('alert-success');
	                		$("#cache_loader").html("Tous les rapport financiers ont été correctement créés.").show();
	                		return;
	                	}
	                	
	                	date = dates.shift();
		                
		                $.ajax({
		                    type: "post",
		                    async:true,
		                    data: {
		                      "action": "generateDaylyReport",
		                      "date": date,
		                    },
		                    dataType: "json",
		                  }).done(function (response) {
		                    if (response.success == true) {
		                      var $count = $("#cache_loader span.count");
		                      var count = parseInt($count.html());
		                      count++;
		                      $count.html(count);                              
		                    }
		                    setTimeout(_loadNext,50);
		                  });
		                 
		            }
		           
		           _loadNext();
		           //if(dates.length>2) { _loadNext(); }
		           //if(dates.length>3) { _loadNext(); }
		      	}
		      	else
		      	{
		      		$("#cache_loader").html("Tous les rapport financiers sont déjà créés.").show();
		      		$(this).prop('disabled', false);
		      	}
	      });            
	});
        
        
        $("#generateSalesReport").click(function(e){
    	e.preventDefault();
    	
	    $.ajax({
	        type: "post",
	        async:true,
	        data: {
	          "action": "generateSalesReport",
	          "date_from": $("#date_sales_from").val(),
	          "date_to": $("#date_sales_to").val(),
	        },
	        context:$(this),
	        dataType: "json",
	      }).done(function (dates) {
	      		$('#generateSalesReport').prop('disabled', true);
	      		
	      		var dates = dates;
	      		console.log(dates);

	      		if(dates !== 0)
	      		{
	      			$("#cache_loader_sales").html("<span class=\"count\">0</span> / "+dates.length+" rapports de ventes créés").show();
	      			
	      			var _loadNext = function()
		            {
		                
		                var date = dates[0];
		                ///////////////////////////		                
		                if(date == today)
		                {
		                	$('#generateSalesReport').prop('disabled', false);
	                		$("#cache_loader_sales").removeClass('alert-success').addClass('alert-warning');
	                		$("#cache_loader_sales").html("Attention, le rapport de vente du jour ne peut être créé que le jour suivant.").show();
	                		return;
		                }
		                ///////////////////////////
		                console.log(dates.length);
		                if(dates.length == 0)
	                	{
	                		$('#generateSalesReport').prop('disabled', false);
	                		$("#cache_loader_sales").removeClass('alert-warning').addClass('alert-success');
	                		$("#cache_loader_sales").html("Tous les rapport de ventes ont été correctement créés.").show();
	                		return;
	                	}
	                	
	                	date = dates.shift();
                                console.log(date);

		                $.ajax({
		                    type: "post",
		                    async:true,
		                    data: {
		                      "action": "generateDaylySalesReport",
		                      "date": date,
		                    },
		                    dataType: "json",
		                  }).done(function (response) {
		                    if (response.success == true) {
		                      var $count = $("#cache_loader_sales span.count");
		                      var count = parseInt($count.html());
		                      count++;
		                      $count.html(count);                              
		                    }
		                    setTimeout(_loadNext,50);
		                  });
		                 
		            }
		           
		           _loadNext();
		           //if(dates.length>2) { _loadNext(); }
		           //if(dates.length>3) { _loadNext(); }
		      	}
		      	else
		      	{
		      		$("#cache_loader_sales").html("Tous les rapport de ventes sont déjà créés.").show();
		      		$(this).prop('disabled', false);
		      	}
	      });            
	});
    
});