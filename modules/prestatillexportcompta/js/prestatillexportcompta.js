$(function(){
	
	$(".submitGranularity").on('click', function(e){
		
		e.preventDefault();
		$('#granularity').val($(this).data('granularity'));
		
		$('button[name="submitGranularity"]').click();
	});
	
	$("#pointshops").on('change', function(e){
		
		e.preventDefault();
		
		$('button[name="submitGranularity"]').click();
		
	});
	
	// STATS
	if($('.compare_line').length > 0)
	{
		getPourcentage('#dash_by_vat');
		getPourcentage('#dash_payement_mod');
	}
	
	// MASQUAGE DES OPTIONS INUTILES EN FONCTION DE LA TABULATION
	$("#dashexportcompta_form .nav-pills li").on('click', function(e){
		if($(this).attr('id') == 'by_cat')
		{
			$("#granularityBox").fadeOut();
		} 
		else
		{
			$("#granularityBox").fadeIn();
		}
	});
});

function getPourcentage(id)
{
	$(id+' .sales_line td.line').each(function(){
			
		var arrow = 'dash_trend_down';
		var line = $(this).data('line');
		var data1 = $(this).data('amount');
		var data2 = $(id+' .compare_line td[data-line='+line+']');
		
		if(data1 !== 0 || data2.data('amount') !== 0)
		{
			var progress = parseFloat((data1/data2.data('amount')*100)-100).toFixed(2);
		}
		else
		{
			var progress = 0;
		}	
		
		var progress_color = '#E08F95';
		
		if(progress > 0)
		{
			arrow = 'dash_trend_up';
			progress_color = '#72C279';
		}
		
		if(progress == 'Infinity')
			progress = '∞';
		
		if(progress == 0)
			progress = '0';
			
		$(data2).prepend('<dd class="dash_trend '+ arrow +'"><span>'+progress+'%</span></dd>');
		$(data2).find('b').css('color',progress_color);
		
	});
}

/*$(function(){
	
	$("#pointshops").on('change', function(e){
	e.preventDefault();
	$.ajax({
		url: '../modules/prestatillexportcompta/ajax.php',
		action: 'myMethod',
		type:'json',
		method:'post',
		data: { 
			  	action: 'myMethod',
			  	dateFrom : $('#datepickerFrom').val(),
			  	dateTo : $('#datepickerTo').val(),
			  	id_pointshop : $(this).val(),
			},
			success: function(result)
			{      
		    	var data = $.parseJSON(result);
				if (data.status == 'success') 
				{		
					refreshDatas(result);
				} 
				else 
				{
					alert(data.message);
				}
			}
		});
	});

});

function refreshDatas(result)
{
	$('#dash_payement_mod .data_table').html('');
	
	var res = $.parseJSON(result);
	
	
	
	
	console.log(res.message.total_ca_by_payment_method);
	
	var html = '';
	
	/*
	 * 
				
					

				{foreach from=$totalCAbyPM['total_ca_by_payment_method']['date'] item=tot key=k}
					<td class="text-center" id="product">{$k}</td>
						{foreach from=$tot item=detail key=d}
							<td class="text-center">{if $detail > 0}<span class="badge badge-success">{Tools::displayPrice($detail, $currency)}</span>{else}{Tools::displayPrice($detail, $currency)}{/if}</td>
						{/foreach}
					</tr>
				{/foreach}
			</tbody>
		</table>
	 * 
	 * 
	 */
	
	/*
	html += '<table class="table data_table" id="">';
		html += '<thead>';
			html += '<tr>';
				html += '<th></th>';
				
				for (var prop in res.message.total_ca_by_payment_method.pm) 
				{
					html += '<th class="text-center" id="">'+`${prop}`+'<br />'+`${res.message.total_ca_by_payment_method.pm[prop]}`+'</th>';	
				}
			html += '</tr>';
		html += '</thead>';
		html += '<tbody>';
			
			for (var prop in res.message.total_ca_by_payment_method.date) 
			{
				html += '<tr>';
					html += '<td class="text-center" id="product">'+`${prop}`+'</td>';
					
					for (var propin in prop)
					{
						html += '<td class="text-center">';
						html += `${prop[propin]}`;
						html += '</td>';
					} 
					//html += '<th class="text-center" id="">'+`${prop}`+'<br />'+`${res.message.total_ca_by_payment_method.pm[prop]}`+'</th>';	
				html += '</tr>';
			}
		html += '</tbody>';
	html += '</table>';
	
	$('#dash_payement_mod .data_table').html(html);	
}
*/