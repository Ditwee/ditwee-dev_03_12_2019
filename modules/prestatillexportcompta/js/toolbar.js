$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

$(function(){
	
	if( decodeURIComponent($.urlParam('id_financial_report') > 0))
	{
		/*
		var tool_csv = '';
		tool_csv += '<li>';
		tool_csv += '<a class="toolbar_btn btn-csv" href="#" title="Exporter en CSV">';
		tool_csv += '<i class="process-icon-hey icon-file-excel-o"></i>';
		tool_csv += '<div>Exporter en CSV</div>';
		tool_csv += '</a></li>';
		*/
		var tool_print = '';
		tool_print += '<li>';
		tool_print += '<a class="toolbar_btn btn-new" href="javascript:window.print()"  title="Imprimer">';
		tool_print += '<i class="process-icon-hey icon-print"></i>';
		tool_print += '<div>Imprimer</div>';
		tool_print += '</a></li>';
		
		//$("#toolbar-nav").prepend(tool_csv);
		$("#toolbar-nav").prepend(tool_print);
		
	}
	
	if($('.panel').length <= 4)
	{
		$('#report_1').slideDown();
	}
	
	$("#toolbar-nav .btn-csv").on('click', function(e){
		e.preventDefault();
		$('button[name="submitExport"]').click();
	});
	
	$(".see_report").on('click', function(e){
		e.preventDefault();
		$($(this).attr('href')).slideToggle();
	});
	
	$(".export_report").on('click', function(e){
		console.log($(this).attr('href'));
	});
	
	
});

