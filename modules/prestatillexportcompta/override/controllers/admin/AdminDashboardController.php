<?php
class AdminDashboardController extends AdminDashboardControllerCore
{
	/*
    * module: prestatillexportcompta
    * date: 2017-07-15 16:00:42
    * version: 1.0
    */
    public function renderView()
	{
		if (Tools::isSubmit('profitability_conf'))
			return parent::renderOptions();
		if ($this->context->cookie->__get('stats_date_update') < strtotime(date('Y-m-d')))
		{
			switch ($this->context->employee->preselect_date_range)
			{
				case 'day':
					$date_from = date('Y-m-d');
					$date_to = date('Y-m-d');
					break;
				case 'prev-day':
					$date_from = date('Y-m-d', strtotime('-1 day'));
					$date_to = date('Y-m-d', strtotime('-1 day'));
					break;
				case 'month':
				default:
					$date_from = date('Y-m-01');
					$date_to = date('Y-m-d');
					break;
				case 'prev-month':
					$date_from = date('Y-m-01', strtotime('-1 month'));
					$date_to = date('Y-m-t', strtotime('-1 month'));
					break;
				case 'year':
					$date_from = date('Y-01-01');
					$date_to = date('Y-m-d');
					break;
				case 'prev-year':
					$date_from = date('Y-m-01', strtotime('-1 year'));
					$date_to = date('Y-12-t', strtotime('-1 year'));
					break;
			}
			$this->context->employee->stats_date_from = $date_from;
			$this->context->employee->stats_date_to = $date_to;
			$this->context->employee->stats_granularity = 'day';
			$this->context->employee->stats_id_pointshop = -1;
			$this->context->employee->update();
			$this->context->cookie->__set('stats_date_update', strtotime(date('Y-m-d')));
			$this->context->cookie->write();
		}
		$calendar_helper = new HelperCalendar();
		$calendar_helper->setDateFrom(Tools::getValue('date_from', $this->context->employee->stats_date_from));
		$calendar_helper->setDateTo(Tools::getValue('date_to', $this->context->employee->stats_date_to));
		$stats_compare_from = $this->context->employee->stats_compare_from;
		$stats_compare_to = $this->context->employee->stats_compare_to;
		if (is_null($stats_compare_from) || $stats_compare_from == '0000-00-00')
			$stats_compare_from = null;
		if (is_null($stats_compare_to) || $stats_compare_to == '0000-00-00')
			$stats_compare_to = null;
		
		if(is_null($this->context->employee->stats_id_pointshop))
		{
			$this->context->employee->stats_id_pointshop = -1;
		}
		
		if(is_null($this->context->employee->stats_granularity))
		{
			$this->context->employee->stats_granularity = "day";
		}
		$calendar_helper->setCompareDateFrom($stats_compare_from);
		$calendar_helper->setCompareDateTo($stats_compare_to);
		$calendar_helper->setCompareOption(Tools::getValue('compare_date_option', $this->context->employee->stats_compare_option));
		$params = array(
			'date_from' => $this->context->employee->stats_date_from,
			'date_to' => $this->context->employee->stats_date_to,
			'id_pointshop' => $this->context->employee->stats_id_pointshop,
			'granularity' => $this->context->employee->stats_granularity
		);
		$this->tpl_view_vars = array(
			'date_from' => $this->context->employee->stats_date_from,
			'date_to' => $this->context->employee->stats_date_to,
			'id_pointshop' => $this->context->employee->stats_id_pointshop,
			'granularity' => $this->context->employee->stats_granularity,
			'hookDashboardZoneOne' => Hook::exec('dashboardZoneOne', $params),
			'hookDashboardZoneTwo' => Hook::exec('dashboardZoneTwo', $params),
			'action' => '#',
			'warning' => $this->getWarningDomainName(),
			'new_version_url' => Tools::getCurrentUrlProtocolPrefix()._PS_API_DOMAIN_.'/version/check_version.php?v='._PS_VERSION_.'&lang='.$this->context->language->iso_code.'&autoupgrade='.(int)(Module::isInstalled('autoupgrade') && Module::isEnabled('autoupgrade')).'&hosted_mode='.(int)defined('_PS_HOST_MODE_'),
			'dashboard_use_push' => Configuration::get('PS_DASHBOARD_USE_PUSH'),
			'calendar' => $calendar_helper->generate(),
			'PS_DASHBOARD_SIMULATION' => Configuration::get('PS_DASHBOARD_SIMULATION'),
			'datepickerFrom' => Tools::getValue('datepickerFrom', $this->context->employee->stats_date_from),
			'datepickerTo' => Tools::getValue('datepickerTo', $this->context->employee->stats_date_to),
			'preselect_date_range' => Tools::getValue('preselectDateRange', $this->context->employee->preselect_date_range)
		);
		return parent::renderView();
	}
	/*
    * module: prestatillexportcompta
    * date: 2017-07-15 16:00:42
    * version: 1.0
    */
    public function postProcess()
	{
		if (Tools::isSubmit('submitDateRealTime'))
		{
			if ($use_realtime = (int)Tools::getValue('submitDateRealTime'))
			{
				$this->context->employee->stats_date_from = date('Y-m-d');
				$this->context->employee->stats_date_to = date('Y-m-d');
				$this->context->employee->stats_compare_option = HelperCalendar::DEFAULT_COMPARE_OPTION;
				$this->context->employee->stats_compare_from = null;
				$this->context->employee->stats_compare_to = null;
				$this->context->employee->update();
			}
			Configuration::updateValue('PS_DASHBOARD_USE_PUSH', $use_realtime);
		}
		if (Tools::isSubmit('submitDateRange'))
		{
			if (!Validate::isDate(Tools::getValue('date_from'))
				|| !Validate::isDate(Tools::getValue('date_to')))
				$this->errors[] = Tools::displayError('The selected date range is not valid.');
			if (Tools::getValue('datepicker_compare'))
				if (!Validate::isDate(Tools::getValue('compare_date_from'))
					|| !Validate::isDate(Tools::getValue('compare_date_to')))
					$this->errors[] = Tools::displayError('The selected date range is not valid.');
			if (!count($this->errors))
			{
				$this->context->employee->stats_date_from = Tools::getValue('date_from');
				$this->context->employee->stats_date_to = Tools::getValue('date_to');
				$this->context->employee->preselect_date_range = Tools::getValue('preselectDateRange');
				if (Tools::getValue('datepicker_compare'))
				{
					$this->context->employee->stats_compare_from = Tools::getValue('compare_date_from');
					$this->context->employee->stats_compare_to = Tools::getValue('compare_date_to');
					$this->context->employee->stats_compare_option = Tools::getValue('compare_date_option');
				}
				else
				{
					$this->context->employee->stats_compare_from = null;
					$this->context->employee->stats_compare_to = null;
					$this->context->employee->stats_compare_option = HelperCalendar::DEFAULT_COMPARE_OPTION;
				}
				$this->context->employee->update();
			}
		}
		
		if(Tools::isSubmit('submitGranularity'))
		{
			$this->context->employee->stats_granularity = Tools::getValue('granularity');	
			$this->context->employee->stats_id_pointshop = Tools::getValue('pointshops');
			
			$this->context->employee->update();
			
			$params = array(
				'granularity' => $this->context->employee->stats_granularity,
				'id_pointshop' => $this->context->employee->stats_id_pointshop,
			);
		}
		
		
		parent::postProcess();
	}
	/*
    * module: prestatillexportcompta
    * date: 2017-07-15 16:00:42
    * version: 1.0
    */
    public function ajaxProcessSaveDashConfig()
	{
		$return = array('has_errors' => false, 'errors' => array());
		$module = Tools::getValue('module');
		$hook = Tools::getValue('hook');
		$configs = Tools::getValue('configs');
		$params = array(
			'date_from' => $this->context->employee->stats_date_from,
			'date_to' => $this->context->employee->stats_date_to
		);
		if (Validate::isModuleName($module) && $module_obj = Module::getInstanceByName($module))
		{
			if (Validate::isLoadedObject($module_obj) && method_exists($module_obj, 'validateDashConfig'))
				$return['errors'] = $module_obj->validateDashConfig($configs);
			if (!count($return['errors']))
			{
				if (Validate::isLoadedObject($module_obj) && method_exists($module_obj, 'saveDashConfig'))
					$return['has_errors'] = $module_obj->saveDashConfig($configs);
				elseif (is_array($configs) && count($configs))
					foreach ($configs as $name => $value)
						if (Validate::isConfigName($name))
							Configuration::updateValue($name, $value);
			}
			else
				$return['has_errors'] = true;
		}
		if (Validate::isHookName($hook) && method_exists($module_obj, $hook))
			$return['widget_html'] = $module_obj->$hook($params);
		die(Tools::jsonEncode($return));
	}
}
