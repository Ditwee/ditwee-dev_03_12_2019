<?php 

if (!defined('_PS_VERSION_'))
	exit;

require_once(dirname(__FILE__).'/classes/PrestatillFinancialReport.php');
require_once(dirname(__FILE__).'/classes/PrestatillFinancialReportDetail.php');
require_once(dirname(__FILE__).'/classes/PrestatillFinancialReportVat.php');
require_once(dirname(__FILE__).'/classes/PrestatillFinancialReportPayment.php');
require_once(dirname(__FILE__).'/classes/PrestatillFinancialSales.php');
require_once(dirname(__FILE__).'/classes/PrestatillFinancialSalesDetail.php');
require_once(dirname(__FILE__).'/classes/PrestatillFinancialStates.php');


class PrestatillExportCompta extends Module
{
	public function __construct()
	{
		$this->name = 'prestatillexportcompta';
		$this->tab = 'administration';
		$this->version = '2.1';
		$this->author = 'Prestatill';
		
		parent::__construct();
		
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap = true;
		$this->displayName = $this->l('Prestatill Rapports Statistiques Avancés');
		$this->description = $this->l('Sales Exports / PointShop / Period / Payment Method / VAT Type / Voucher / Discounts');
		$this->confirmUninstall = $this->l('Confirm uninstall ?');
		
	}
	
	public function install()
	{
		
		if(!parent::install()
			//|| !$this->registerHook('dashboardZoneTwo')
			|| !$this->installSql()
			//|| !$this->registerHook('dashboardData')
			|| !$this->registerHook('actionPrestatillRegisterNewPaymentPart')
			|| !$this->registerHook('actionPrestatillModifyPaymentPart')
			|| !$this->registerHook('actionPaymentCCAdd')
			|| !$this->registerHook('actionOrderStatusUpdate')
			|| !$this->registerHook('actionValidateOrder')
			|| !$this->registerHook('displayBackOfficeHeader') 
			|| !$this->registerHook('displayPrestatillInjectCSS')
			|| !$this->registerHook('displayBackOfficeTop')
			|| !$this->registerHook('actionPrestatillNewCashState')
			|| !$this->registerHook('actionPrestatillAfterOrderAssignToPointshop')
			|| !$this->registerHook('displayPrestatillSpecialButton')
			|| !$this->registerHook('displayPrestatillSpecialView')
			|| !$this->registerHook('displayAdminPrestatillGlobalSalesListBefore')
			|| !$this->registerHook('actionAdminPrestatillGlobalSalesListingFieldsModifier')
			|| !$this->installTab(0,'AdminPrestatillExportCompta' ,$this->l('Exports comptables')) 
			|| !$this->installTab('AdminPrestatillExportCompta','AdminPrestatillFinancialReport' ,$this->l('Rapports financiers'))
			|| !$this->installTab('AdminPrestatillExportCompta','AdminPrestatillFinancialSales' ,$this->l('Rapports des ventes'))
			//|| !$this->installTab('AdminPrestatillExportCompta','AdminPrestatillGlobalReport' ,$this->l('Rapports des ventes cumulées'))
			|| !$this->installTab('AdminPrestatillExportCompta','AdminPrestatillGlobalSales' ,$this->l('Rapports des ventes produits'))			
			
			)
		{
			return false;
		}
		
		Configuration::updateValue('PST_EXPORT_COMPTA_POINTSHOP_NAME', Tools::getValue('PST_EXPORT_COMPTA_POINTSHOP_NAME','En ligne'));
					
		return true;
	}



	/**
     * Affichage du boutton de special
     */
    public function hookDisplayPrestatillSpecialButton($params)
    {
		return $this->display(__FILE__, 'views/templates/hook/prestatill_special_button.tpl');
    }   

    /**
     * Affichage des informations spéciales liées
     */
    public function hookDisplayPrestatillSpecialView($params)
    {
		$id_pointshop = $params['id_pointshop'];
		$date_from = date('Y-m-d');
		$id_service = 1;
		
		$last_caisse_state = ACaisseTicket::getLastCaisseState($id_pointshop);
		if(!empty($last_caisse_state))
		{
			$id_service = $last_caisse_state[0]['id_service'];
			$date_from = substr($last_caisse_state[0]['day'], 0, 10);
		}
		
		// On sélection le rapport du jour 
		$id_existing_report = PrestatillFinancialReport::ReportExists($date_from, $id_pointshop);
		
		// ENCAISSEMENT PAR MODE DE PAIEMENT
		$report_payment = PrestatillFinancialReportPayment::getPaymentList($id_existing_report, $id_service);
		
		$report_pm = array();
			
		foreach($report_payment as $id_serv => $service)
		{
			if(!isset($report_pm[$id_serv]))
			{
				$report_pm['detail'][$id_serv] = array();
				$report_pm['nbr'][$id_serv] = array();
				$report_pm['total'][$id_serv] = 0;
			}	
				
				
			foreach($service as $method => $payment)
			{
					
				if(!isset($report_pm['detail'][$id_serv][$payment['payment_method']]))
				{
					$report_pm['detail'][$id_serv][$payment['payment_method']] = 0;
					$report_pm['nbr'][$id_serv][$payment['payment_method']] 	= 0;
				}
				
				$report_pm['detail'][$id_serv][$payment['payment_method']] 	+= $payment['amount'];
				$report_pm['nbr'][$id_serv][$payment['payment_method']] 	+= 1;
				$report_pm['total'][$id_serv] 								+= $payment['amount'];
			}
		}
		
		//$totalCAbyPM = PrestatillFinancialReport::getCAbyPaymentMethod($date_from, date('Y-m-d'), 'day', $id_pointshop, false, $id_service);

		$this->context->smarty->assign(
			array(
				'report_pm' => $report_pm,
				'id_pointshop' => $id_pointshop,
				'id_service' => $id_service,
				'date_from' => $date_from,
				'gr' => 'day',
				'compare' => false,
				'currency' => $this->context->currency,
	            '_PS_PRICE_DISPLAY_PRECISION_' => _PS_PRICE_DISPLAY_PRECISION_
			)
		);	
			
    	return $this->display(__FILE__, 'views/templates/hook/prestatill_special_view.tpl');
	}  
	
	private function installSql()
    {
        include(dirname(__FILE__).'/sql/install.php');
        $result = true;
	    foreach ($sql_requests as $request){
            if (!empty($request))
			//d($request);
            $result &= Db::getInstance()->execute(trim($request));
        }
        return $result;
    }
	
	public function uninstall()
	{
		if(!parent::uninstall()
			|| !$this->uninstallTab('AdminPrestatillExportCompta')
            || !$this->uninstallTab('AdminFinancialReportList')
		 	|| !$this->uninstallTab('AdminFinancialSalesList')
		 	|| !$this->uninstallTab('AdminPrestatillGlobalReport')
			)
		{
			return false;
		}
				
		return true;
	}
	
	private function installTab($parent, $class_name, $name)
    {
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->class_name = $class_name;
        $tab->module = $this->name;

        $tab->name = array();
		foreach (Language::getLanguages(true) as $lang){
			$tab->name[$lang['id_lang']] = $name;
        }
        
        return $tab->save();
    }

	private function uninstallTab($class_name)
    {
        $idTab = (int)Tab::getIdFromClassName($class_name);
        $tab = new Tab($idTab);
        return $tab->delete();
    }
	
	public function hookDashboardData($params)
	{
		$this->currency = clone $this->context->currency;
		
		

		// Retrieve, refine and add up data for the selected period
		//$tmp_data = $this->getData($params['date_from'], $params['date_to']);
		
		
		
	}
	
	public function hookDashboardZoneTwo($params)
	{
		//$totalCAbyVAT = PrestatillFinancialReport::getCAbyVAT('2018-03-02', '2018-03-02','day', 8, false, 36233);	// RENDRE DYNAMIQUE	
		//d($totalCAbyVAT);		
		// On initialise le point de vente "En Ligne = site internet" = 0	
		$pointshops = array(
			array(
				'id_a_caisse_pointshop' => 0,
				'name' => 'En ligne',
			)
		);
			
		$compare = array();
		$is_compared = false;
		
		$gr = $this->context->employee->stats_granularity;
		$id_pointshop = $this->context->employee->stats_id_pointshop;
		
		// Si aucun id_pointshop n'est coché, on met de base le site
		if($id_pointshop == -1 || empty($id_pointshop))
			$id_pointshop = array(0 => 0);

		// On ajoute les comparaisons
		$compare['date_compare_from'] = $this->context->employee->stats_compare_from;
		$compare['date_compare_to'] = $this->context->employee->stats_compare_to;
		$compare['date_compare_option'] = $this->context->employee->stats_compare_option;
		
		if($compare['date_compare_from'] != '0000-00-00' AND $compare['date_compare_from'] != '')
			$is_compared = true;
		
		// ENCAISSEMENT PAR MODE DE PAIEMENT
		$totalCAbyPM = PrestatillFinancialReport::getCAbyPaymentMethod($params['date_from'], $params['date_to'], $gr, $id_pointshop, $compare);

		// CA PAR TYPE DE TVA
		$totalCAbyVAT = PrestatillFinancialReport::getCAbyVAT($params['date_from'], $params['date_to'], $gr, $id_pointshop, $compare);
		foreach($totalCAbyVAT['vat'] as $id => $vat)
		{
			$tax = PrestatillFinancialReportVat::getTaxName($id);
			$totalCAbyVAT['vat'][$tax['name']] = $vat;
			unset($totalCAbyVAT['vat'][$id]);
		}
			
		// CA PAR RAYON
		$totalCAbyCAT = PrestatillFinancialReport::getCAbyCat($params['date_from'], $params['date_to'], $gr, $id_pointshop);
		
		// CA PAR HEURE
		$totalCAbyHour = PrestatillFinancialReport::getCAbyHour($params['date_from'], $params['date_to'], $gr, $id_pointshop);
		
		// SI CAISSE
		// Liste des points de vente
		if(Configuration::get('ACAISSE_last_cache_generation_pricelist') != NULL)	
		{
			$all_pointshops = ACaissePointshop::getAllPointshop(true);
			foreach($all_pointshops as $key => $pointshop)
			{
				$pointshops[] = $pointshop;
			}	
		}
			
		$this->context->smarty->assign(
			array(
				'totalCAbyPM' => $totalCAbyPM,
				'totalCAbyVAT' => $totalCAbyVAT,
				'totalCAbyCAT' => $totalCAbyCAT,
				'totalCAbyHour' => $totalCAbyHour,
				'id_pointshop' => $id_pointshop,
				'gr' => $gr,
				'is_compared' => $is_compared,
				'compare' => $compare,
				'pointshops' => $pointshops,
				'currency' => $this->context->currency,
	            '_PS_PRICE_DISPLAY_PRECISION_' => _PS_PRICE_DISPLAY_PRECISION_
			)
		);
		
		//return $this->display(__FILE__, 'export.tpl');
	}

	public function loadAsset()
    {
        // Load JS
        $js = array(
            $this->_path.'js/configure.js',
        );
        $css = array(
          //  $this->css_path.'configuration.css',
        );

        $this->context->controller->addJS($js);
       // $this->context->controller->addCSS($css, 'all');
    }

	public function getContent()
	{
        $this->loadAsset();	
		
		if(Tools::isSubmit('submitConfiguration'))
		{
			Configuration::updateValue('PST_EXPORT_COMPTA_POINTSHOP_NAME', Tools::getValue('PST_EXPORT_COMPTA_POINTSHOP_NAME',''));
		}

		$id_pointshops = $this->_getPoinsthops();
		
		// MAJ 
		$exp_id_pointshop = Tools::getValue('id_pointshop');
		
		// si on regénère les caches produits on renvoit d'abors la liste des id de produit à regénérer 
        if(isset($_POST['action']) && $_POST['action'] == 'generateFinancialReport') {
			
			$date_from = Tools::getValue('date_from');
			$date_to = Tools::getValue('date_to');
			
			$dates = PrestatillFinancialReport::getOrdersDate($date_from, $date_to); 
			 
			if(!empty($dates))
			{
	            $dates_results = array();	
	            if(function_exists('array_column'))
	            {
	                $dates_results = array_column($dates,'date');
	            }
	            else
	            {
	                foreach($dates_results as $row)
	                {
	                    $dates_results[] = $row['date']; 
	                }
	            } 
			}
			else 
			{
				$dates_results = 0;
			}
			  
            header('Content-type: application/json');
            echo(json_encode($dates_results));
            die();
        }
			
		if(isset($_POST['action']) && $_POST['action'] == 'generateDaylyReport') {
            $date = $_POST['date'];
			
			$id_pointshops = $this->_getPoinsthops();
            
            if($date !== null) {            
               $report =  PrestatillFinancialReport::createDaylyFinancialReport($date, $date, $id_pointshops);
            }

            $response=array(
                'success' => true,
                'date' => $date,
                'results' => $report === false?false:$report,
            );

            header('Content-type: application/json');
            echo(json_encode($response));
            die();
        }
		
		// LIEN POUR LA TACHE CRON ?
		if(isset($_GET['action']) && ($_GET['action'] == 'generateFinancialReport'  && $_GET['secure_key'] = 'Grgpjf98HdT3Fdf833_R@Ze'))
		{
			$date = date('Y-m-d');
			$report =  PrestatillFinancialReport::createDaylyFinancialReport($date, $date, $id_pointshops);
		}
        
        if (isset($_POST['action']) && $_POST['action'] == 'generateSalesReport') {

            $date_from = Tools::getValue('date_from');
            $date_to = Tools::getValue('date_to');

            $dates = PrestatillFinancialSales::getOrdersDate($date_from, $date_to);

            if (!empty($dates)) {
                $dates_results = array();
                /*if (function_exists('array_column')) {
                    $dates_results = array_column($dates, 'date', 'id_order');
                } else {*/
                    foreach ($dates as $row) {
                        $dates_results[] = $row['date'];
                    //}
                }
            } else {
                $dates_results = 0;
            }
            header('Content-type: application/json');
            echo(json_encode($dates_results));
            die();
        }
        if (isset($_POST['action']) && $_POST['action'] == 'generateDaylySalesReport') {
            $date = $_POST['date'];
            /*$id_order = $_POST['id_order'];
            $id_pointshop = $_POST['id_poinsthop'];
            $id_pointshops = $this->_getPoinsthops();*/

            if ($date !== null) {
                $report = PrestatillFinancialSales::createDaylySalesReport($date, $date);
            }

            $response = array(
                'success' => true,
                'date' => $date,
                'results' => $report === false ? false : $report,
            );

            header('Content-type: application/json');
            echo(json_encode($response));
            die();
        }

		$type = null;
		
		foreach($_POST as $key => $val)
		{
			if(substr($key, 0,7) == 'export_')
			{
				$date_from = $_POST['date_from_export'];
				$date_to = $_POST['date_to_export'];
				$type = $key;
				
				//d($type);
				if(!empty($date_to) && !empty($date_from) && ($type != null))
				{
					
					$filename = $type."-du-".$date_from."-au-".$date_to.".csv";
					$fp = fopen('php://output', 'w');	
						
					//on va chercher
					$sql = 'SELECT id_financial_report
							FROM '._DB_PREFIX_.PrestatillFinancialReport::$definition['table'].' AS fr
							WHERE day BETWEEN "'.pSQL($date_from).' 00:00:00" AND "'.pSQL($date_to).' 23:59:59"
							AND id_pointshop =  '.$exp_id_pointshop.' 
							ORDER BY day ASC;					
					';			
					$rows = DB::getInstance()->executeS($sql);
					
					$count = 0;
					$csv_content = '';
					foreach($rows as $row)
					{
						$csv_content .= self::exportFinancialReport($row['id_financial_report'], 0, 'return', $count == 0, $type);	
						$count++;
					}
					
					$csv_content = mb_convert_encoding($csv_content, 'ISO-8859-15');
					
					header('Content-type: application/csv');
					header('Content-Disposition: attachment; filename='.$filename);
					echo($csv_content);
					die();
							
				}
				return false;
				
			}
			
		}
			

        $config_vars = $this->getConfigFormValues();
		
        $type_date = str_replace('Y', 'yy', $this->context->language->date_format_lite);
		
		$cron_url = Tools::getHttpHost(true, true).__PS_BASE_URI__.basename(_PS_MODULE_DIR_).
            '/'.$this->name.'/generatecron.php?redirect=1&token='.substr(_COOKIE_KEY_, 34, 8);
			
		// On récupère la liste des points de vente 
		$pointshops = self::getAllPointshops(true);
		       
        $this->context->smarty->assign(array(
            'module_active' => (int)$this->active,
            'module_name' => $this->name,
            'pointshops' => $pointshops,
            'module_version' => $this->version,
            'module_display_name' => $this->displayName,
            'type_date' => $type_date,
            'multishop' => (int)Shop::isFeatureActive(),
            'ps_version' => (bool)version_compare(_PS_VERSION_, '1.6', '>'),
            //'guide_link' => 'docs/'.$this->name.'_doc_'.$lang.'.pdf',
            'config_vars' => $config_vars,
            'cron_url' => $cron_url,
        ));

        return $this->display(__FILE__, 'views/templates/admin/configure.tpl');

	}

	private function _getPoinsthops()
	{
		$id_pointshops = array(0 => 0);
		
		if(Configuration::get('ACAISSE_last_cache_generation_pricelist') != NULL)	
		{
			$all_pointshops = self::getAllPointshops(true);
			foreach($all_pointshops as $key => $pointshop)
			{
				$id_pointshops[] = $pointshop['id_a_caisse_pointshop'];
			}
				
		}
		
		return $id_pointshops;
	}

	public function getConfigFormValues()
	{
				
		return array(
			'PST_EXPORT_COMPTA_POINTSHOP_NAME' => 
				Tools::getValue('PST_EXPORT_COMPTA_POINTSHOP_NAME', Configuration::get('PST_EXPORT_COMPTA_POINTSHOP_NAME')),
			);
	}

	public function hookDisplayPrestatillInjectCSS($params)
	{
		return '<link  href="'._MODULE_DIR_.$this->name.'/css/prestatillpreview.css" rel="stylesheet" type="text/css" media="all" />';		
	}
	
	public function hookDisplayBackOfficeTop()
    {
		$this->context->controller->addJquery();	
		$this->context->controller->addJS($this->_path.'js/prestatillexportcompta.js');			
		$this->context->controller->addJS('js/prestatillexportcompta.js');
		$this->context->controller->addCSS($this->_path.'css/prestatillexportcompta.css');
		
		if((Tools::getValue('controller') == 'AdminPrestatillFinancialSales' && Tools::getValue('id_financial_sales'))
		|| (Tools::getValue('controller') == 'AdminPrestatillFinancialReport' && Tools::getValue('id_financial_report')))
		{
			$this->context->controller->addJS($this->_path.'js/toolbar.js');
		}
    }
	
	public function hookActionValidateOrder($params)
	{
				
		if(isset($params['order']))
		{
			if(!isset($params['id_pointshop']))
			{
				$order = $params['order'];
				$date_from = date('Y-m-d');	
				$date_to = $date_from;
				$id_pointshop = 0;
					
				// On ajoute les informations de la vente au rapport des ventes
				$sales = PrestatillFinancialSales::generateSaleForOrder($date_from, $date_to, $id_pointshop, $order->id);
			}	
		}
		
	}
	
	public function hookActionPrestatillAfterOrderAssignToPointshop($params)
	{
		if(isset($params['id_pointshop']) && $params['id_pointshop'] > 0)
		{
			$id_order = $params['id_order'];
			$date_from = date('Y-m-d');	
			$date_to = $date_from;
			
			$id_pointshop = $params['id_pointshop'];
				
			// On ajoute les informations de la vente au rapport des ventes
			$sales = PrestatillFinancialSales::generateSaleForOrder($date_from, $date_to, $id_pointshop, $id_order);
		}	
	}
	
	public function hookActionPrestatillRegisterNewPaymentPart($params)
	{
		$addPayment = PrestatillFinancialReport::registerPayment(
			$day = date('Y-m-d'),
			$params['payment_part']->module_name,
			$params['payment_part']->label,
			$params['payment_part']->payment_amount/100,
			$params['order']->id_pointshop,
			$params['order']->id,
			null,
			$params['payment_part']
		);
	}
	
	public function hookActionPrestatillModifyPaymentPart($params)
	{
		$addPayment = PrestatillFinancialReport::registerPayment(
			$day = date('Y-m-d'),
			null,
			$params['params']['method'],
			$params['params']['amount'],
			$params['order']->id_pointshop,
			null,
			$params['order']->reference
		);
	}
	
	public function hookActionPaymentCCAdd($params)
	{
		// A n'utiliser que si la commande est modifiée depuis le BO (pas en caisse)
		$context = Context::getContext();
		
		if(isset($params['cart']) && (get_class($context->controller) == 'AdminOrdersController' || get_class($context->controller) != 'ACaisseUseController' && get_class($context->controller) != 'PrestatillUseController')) 
		{	
			$addPayment = PrestatillFinancialReport::registerPayment(
				$day = date('Y-m-d'),
				null,
				$params['paymentCC']->payment_method,
				$params['paymentCC']->amount,
				$params['cart']->id_pointshop,
				null,
				$params['paymentCC']->order_reference
			);
		}
	}
	
	public function hookActionOrderStatusUpdate($params)
	{
		$updateReport = PrestatillFinancialReport::updateReport(date('Y-m-d H:i:s'),$params['id_order'], $params['newOrderStatus']->id);
	}
	
	public static function getAllPointshops($activeOnly = false)
	{
        $db=Db::getInstance();
        $sql='SELECT *
        FROM '._DB_PREFIX_.'a_caisse_pointshop';
        if($activeOnly===true) { $sql.=' WHERE active=1'; }
        $rows=$db->executeS($sql,true,false);
        return $rows;
	}
	
	public static function hookActionPrestatillNewCashState($params)
	{
		//d($params);	
		$updateCashState = PrestatillFinancialStates::updateCashState(date('Y-m-d'),$params['cash'],$params['employee']);
		
	}
	
	public static function getPointshopName($id_pointshop = 0)
	{
		if($id_pointshop > 0)
		{
			$query = 'SELECT name
	        FROM '._DB_PREFIX_.'a_caisse_pointshop
	        WHERE id_a_caisse_pointshop = '.$id_pointshop;
			
			$pointshop_name = Db::getInstance()->getRow($query);
		} 
		else
		{
			$pointshop_name['name'] = Configuration::get('PST_EXPORT_COMPTA_POINTSHOP_NAME');
		}
		
		return $pointshop_name;
	}
	
	public static function exportFinancialReportMonth($id_financial_report, $export = 'csv')
	{
		
		$report = new PrestatillFinancialReport($id_financial_report);

		$filename = "export-compta-mois-".substr($report->day,0,7).".csv";
		$fp = fopen('php://output', 'w');		
		
		if(Validate::isLoadedObject($report))
		{
			//on va chercher
			$sql = 'SELECT id_financial_report
					FROM '._DB_PREFIX_.PrestatillFinancialReport::$definition['table'].' AS fr
					WHERE day LIKE \''.substr($report->day,0,7).'%\'
					ORDER BY day ASC;					
			';			
			$rows = DB::getInstance()->executeS($sql);
			
			$count = 0;
			$csv_content = '';
			foreach($rows as $row)
			{
				$csv_content .= self::exportFinancialReport($row['id_financial_report'], 'return', $count == 0);	
				$count++;
			}
			
			$csv_content = mb_convert_encoding($csv_content, 'ISO-8859-15');
			
			switch($export)
			{
				case 'return':
					return $csv_content;					
				case 'csv':
				default:
					header('Content-type: application/csv');
					header('Content-Disposition: attachment; filename='.$filename);
					echo($csv_content);
					die();
			}			
		}
		return false;
	}
	
	public static function exportFinancialReport($id_financial_report, $id_serv = 0, $export = 'csv', $with_header = true, $type = null)
	{	

		$report = new PrestatillFinancialReport($id_financial_report);

		$filename = "export-compta-".$report->day.".csv";

		$fp = fopen('php://output', 'w');		

		if(Validate::isLoadedObject($report))
		{
			$report_infos = PrestatillFinancialReport::getReportAllInfos($report->id);

			$report_period = array();
			$report_period[$report_infos['report']->day]['day'] = $report_infos['report']->day;
			$report_period[$report_infos['report']->day]['tot_ttc'] = $report_infos['report']->total_tax_incl;
			$report_period[$report_infos['report']->day]['tot_ht'] = $report_infos['report']->total_tax_excl;
			$report_period[$report_infos['report']->day]['tot_vat'] = $report_infos['report']->total_vat;
			$report_period[$report_infos['report']->day]['tot_discount_ttc'] = $report_infos['report']->total_discounts_tax_incl;
			$report_period[$report_infos['report']->day]['tot_discount_ht'] = $report_infos['report']->total_discounts_tax_excl;

			// Totaux HT par TVA

			foreach($report_infos['report_vat'] as $id_service => $tot_vats)
			{
				foreach($tot_vats as $vat)
				{
					$report_period[$report_infos['report']->day]['tot_vat_amount'][strtoupper($vat['tax_name'])] += $vat['sum_total_ht'];
				}
			}

			// Totaux TVA par TVA
			foreach($report_infos['report_vat'] as $id_service => $tot_vats)
			{
				foreach($tot_vats as $vat)
				{
					$report_period[$report_infos['report']->day]['tot_vat_vat'][strtoupper($vat['tax_name'])] += $vat['sum_amount'];
				}
			}

			// Totaux par modes de paiement
			//NB: on récupère tous les modes de paiement possible
			$payment_methods = PrestatillFinancialReport::getOrderStates();
			
			//d($report_infos['report_vat']);
			foreach($payment_methods as $pm)
			{
				if(!empty($report_infos['report_payment_wb']['detail']))
				{
					foreach($report_infos['report_payment_wb']['detail'] as $id_service => $tot_payment)
					{
						foreach($tot_payment as $mod => $payment)
						{						
							if(strtoupper($pm)== strtoupper($mod))
							{
								$report_period[$report_infos['report']->day]['tot_pm'][strtoupper($pm)] += $payment;
								// Total des règlements
								$report_period[$report_infos['report']->day]['tot_payment'] += $payment;
							}
							else
							{
								$report_period[$report_infos['report']->day]['tot_pm'][strtoupper($pm)] += 0;
								$report_period[$report_infos['report']->day]['tot_payment'] += 0;								
							}						
						}
					}	
				}
				else
				{
					$report_period[$report_infos['report']->day]['tot_pm'][strtoupper($pm)] += 0;	
				}
				
			}
		}
			$total_payment = $report_period[$report_infos['report']->day]['tot_payment'];
			
			$report_period[$report_infos['report']->day]['tot_ca_vs_payment'] = $report_infos['report']->total_tax_incl-$total_payment;
			
			// Total sur la période : à faire ?
			
			//$report_period);
			
			//d($balances);
			$pointshop_name = PrestatillExportCompta::getPointshopName($report->id_pointshop);
			
			$params_hook = array(
				'id_pointshop' => $report_infos['report']->id_pointshop,
				'pointshop_name' => $pointshop_name,
				'day' => $report_infos['report']->day,
				'report' => $report_infos['report'],
				'report_period' => $report_period,
			);
			
			$context = Context::getContext();
			
			$context->smarty->assign(array(
	            'pointshop_name' => $pointshop_name,
				'date_from' => $with_header==true?$report_infos['report']->day:false,
	            'report' => $report_infos['report'],
	            'report_period' => $report_period,
				'with_header' => $with_header,
	        	)
			);
			 
			
			
			$module = new PrestatillExportCompta();
			//d($type);
	        //$csv_content = $module->display(__FILE__, 'views/templates/admin/view_financial_report_brouillard.csv.tpl');
			if($type != null)
			{
				$csv_content = $module->display(__FILE__, 'views/templates/admin/'.$type.'.csv.tpl');
			}
			else 
			{
				$csv_content = $module->display(__FILE__, 'views/templates/admin/view_financial_report_brouillard.csv.tpl');
			}
			
			switch($export)
			{
				case 'return':
					return $csv_content;					
				case 'csv':
				default:
					$csv_content = mb_convert_encoding($csv_content, 'ISO-8859-15');
					header('Content-type: application/csv');
					header('Content-Disposition: attachment; filename='.$filename);
					echo($csv_content);
					die();
			}
		}



	public function hookDisplayAdminPrestatillGlobalSalesListBefore($params)
	{
		if(method_exists(new Address(), 'getPrestatillPointshop'))
		{
			$this->smarty->assign('addresses', Address::getPrestatillPointshop());
		}
		
		$link = new Link();
		$this->smarty->assign('token', Tools::getAdminTokenLite('AdminPrestatillGlobalSales'));
		
		$context = Context::getContext();
		$cookie = $context->cookie;
		
		$this->smarty->assign(array(		
			'date_from' => $cookie->__get('prestatillglobalsalesorder_detailFilter_date_from'),
			'date_to' => $cookie->__get('prestatillglobalsalesorder_detailFilter_date_to'),
			'group_line' => $cookie->__get('prestatillglobalsalesorder_detailFilter_group_line'),
			'id_supplier' => $cookie->__get('prestatillglobalsalesorder_detailFilter_id_supplier'),
			'id_manufacturer' => $cookie->__get('prestatillglobalsalesorder_detailFilter_id_manufacturer'),
			'id_pointshop_address' => $cookie->__get('prestatillglobalsalesorder_detailFilter_id_pointshop_address'),
			'group_pointshop' => $cookie->__get('prestatillglobalsalesorder_detailFilter_group_pointshop'),
			'cookie' => $cookie,
			
		));
				
		return $this->display(__FILE__, 'views/templates/hook/global_sales_filter.tpl');
	}



	public function hookActionAdminPrestatillGlobalSalesListingFieldsModifier($params)
	{
		//p($params['cookie']);
		
		$date_from = $params['cookie']->__get('prestatillglobalsalesorder_detailFilter_date_from');
		$date_to = $params['cookie']->__get('prestatillglobalsalesorder_detailFilter_date_to');
		$group_line = $params['cookie']->__get('prestatillglobalsalesorder_detailFilter_group_line');
		$id_pointshop = (int)$params['cookie']->__get('prestatillglobalsalesorder_detailFilter_id_pointshop_address');
		$group_pointshop = (bool)$params['cookie']->__get('prestatillglobalsalesorder_detailFilter_group_pointshop');
		$id_supplier = (int)$params['cookie']->__get('prestatillglobalsalesorder_detailFilter_id_supplier');
		$id_manufacturer = (int)$params['cookie']->__get('prestatillglobalsalesorder_detailFilter_id_manufacturer');
		
		$product_reference = $params['cookie']->__get('prestatillglobalsalesorder_detailFilter_product_reference');
		$product_supplier_reference = $params['cookie']->__get('prestatillglobalsalesorder_detailFilter_product_supplier_reference');
		
		$product_id = (int)$params['cookie']->__get('prestatillglobalsales'.'order_detailFilter_product_id');
		$product_attribute_id = (int)$params['cookie']->__get('prestatillglobalsales'.'order_detailFilter_product_attribute_id');
		$product_name = $params['cookie']->__get('prestatillglobalsales'.'order_detailFilter_product_name');
		
		//d($product_reference);
		
		//	d($params['cookie']);		
		
		if($date_from == '')
		{
			$date_from = date('Y-m-d',strtotime('-1 month'));
			$params['cookie']->__set('prestatillglobalsalesorder_detailFilter_date_from',$date_from);
		}
		if($date_to == '')
		{
			$date_to = date('Y-m-d');
			$params['cookie']->__set('prestatillglobalsalesorder_detailFilter_date_to',$date_to);
		}
		
        $params['select'] = '
			a.product_id,
			a.product_attribute_id,
			a.product_name,
			product_reference,
			product_supplier_reference,
			o.id_pointshop as id_pointshop,
			o.date_add as date_cmd,
			o.reference as order_reference,
			o.id_order as id_order,
		';
		
		if($group_line == 'false')
		{
			$params['select'] .= '
				a.product_quantity,
				a.product_quantity_refunded,
				ROUND(a.unit_price_tax_incl,2),
				ROUND(a.unit_price_tax_excl,2),
				ROUND(a.original_product_price,2),
				ROUND(a.original_wholesale_price,2),
				ROUND(((a.unit_price_tax_excl-a.original_wholesale_price)/a.unit_price_tax_excl)*100,1) as taux_de_marque,
				ROUND(a.unit_price_tax_excl-a.original_wholesale_price,2) as marge,
				ROUND(100*a.product_quantity_refunded/a.product_quantity,2) as refund_rate
				';
		}
		else
		{
			$params['select'] .= '
				sum(a.product_quantity) as product_quantity,
				count(a.product_id) as nb_product,
				sum(a.product_quantity_refunded) as product_quantity_refunded,
				ROUND(sum(a.unit_price_tax_incl)/count(a.product_id),2) as unit_price_tax_incl,
				ROUND(sum(a.unit_price_tax_excl)/count(a.product_id),2) as unit_price_tax_excl,
				ROUND(sum(a.original_product_price)/count(a.product_id),2) as original_product_price,
				ROUND(sum(a.original_wholesale_price)/count(a.product_id),2) as original_wholesale_price,
				ROUND((((sum(a.unit_price_tax_excl)/count(a.product_id))-(sum(a.original_wholesale_price)/count(a.product_id)))/(sum(a.unit_price_tax_excl)/count(a.product_id)))*100,1) as taux_de_marque,
				ROUND((sum(a.unit_price_tax_excl)/count(a.product_id))-(sum(a.original_wholesale_price)/count(a.product_id)),2) as marge,
				ROUND(100*sum(a.product_quantity_refunded)/sum(a.product_quantity),2) as refund_rate
				';
		}
				
		switch($group_line)
		{
			case 'attribute':
				$params['group_by'] = 'GROUP BY a.product_id,a.product_attribute_id';
				break;
			case 'false':
			default:			
				$params['group_by'] = 'GROUP BY a.id_order_detail';
				break;				
		}

		$date = 'date_add';

		$params['join'] = 'LEFT JOIN '._DB_PREFIX_.'orders AS o ON (o.id_order = a.id_order)
		LEFT JOIN '._DB_PREFIX_.'a_caisse_pointshop AS p ON (p.id_a_caisse_pointshop = o.id_pointshop)
		LEFT JOIN '._DB_PREFIX_.'address AS ad ON (p.id_address = ad.id_address)
		LEFT JOIN '._DB_PREFIX_.'product AS pp ON (pp.id_product = a.product_id)
		';

		//d($this->_join);
		
		if($group_pointshop == true && $id_pointshop == -1)
		{
			
			//$params['group_by'] .= ',ad.id_address';
		}

		$params['where'] = '';
		$debug = '';

     	if($this->isInstalled('acaisse')){
			switch($id_pointshop)
			{
				case -1 :
					//p('id_pointshop -1');
					$params['where'] .= ' AND o.id_pointshop>=-1';
					break;
				case 0 :
					//p('id_pointshop 0');
					$params['where'] .= ' AND o.id_pointshop='.(int)$id_pointshop;
					break;
				default :
					//p('id_pointshop >0');
					$params['where'] .= ' AND o.id_pointshop IN (SELECT id_a_caisse_pointshop FROM '._DB_PREFIX_.'a_caisse_pointshop WHERE id_address = '.(int)$id_pointshop.')';
					break;
			}
        }
		
		if($product_reference != '')
		{
			$params['where'] .= ' AND pp.reference LIKE "%'.$product_reference.'%"';
		}

		if($product_supplier_reference != '')
		{
			$params['where'] .= ' AND pp.supplier_reference LIKE "%'.$product_supplier_reference.'%"';
		}		
		
		if($product_name != '')
		{
			$params['where'] .= ' AND a.product_name LIKE "%'.$product_name.'%"';
		}

		if((int)$product_id > 0)
		{
			$params['where'] .= ' AND a.product_id  = '.(int)$product_id.'';
		}

		if((int)$product_attribute_id > 0)
		{
			$params['where'] .= ' AND a.product_attribute_id  = '.(int)$product_attribute_id.'';
		}
		
		if((int)$id_manufacturer > 0)
		{
			$params['where'] .= ' AND pp.id_manufacturer  = '.(int)$id_manufacturer.'';			
		}
		
		if((int)$id_supplier > 0)
		{
			$params['where'] .= ' AND pp.id_product IN (SELECT id_product FROM '._DB_PREFIX_.'product_supplier WHERE id_supplier = '.(int)$id_supplier.')';			
		}
		
		
		$params['where'] .= ' AND o.'.$date.'>=\''.$date_from.' 00:00:00\' AND o.'.$date.'<=\''.$date_to.' 23:59:59\'';
		$params['filters'] = '';
		$params['filter'] = '';
		//d($params);
		//d($this);
		//p('SELECT ' .$params['select']. ' FROM ps_rfl_order_detail AS a '.$params['join'].' WHERE 1 '.$params['where'].' '.$params['group_by']);
		return;
	}


}