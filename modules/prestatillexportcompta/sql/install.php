<?php 


$sql_requests = array();
$langs = Language::getLanguages(false);

/**
 * RAPPORT FINANCIER
 */
$sql_requests[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'financial_report`(
    `id_financial_report` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_financial_report_detail` INT(10),
    `id_pointshop` INT(11),
    `nb_sales` INT(11),
    `total_tax_incl` DECIMAL(20,6),
    `total_tax_excl` DECIMAL(20,6),
    `total_vat` DECIMAL(20,6),
    `total_payment` DECIMAL(20,6),
    `total_discounts_tax_incl` DECIMAL(20,6),
    `total_discounts_tax_excl` DECIMAL(20,6),
    `conversion_rate` DECIMAL(13,6),
    `id_currency` INT(11),
    `day` DATE
    )
    ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
    
/*
 * DETAIL DE RAPPORT FINANCIER
 */
 $sql_requests[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'financial_report_detail`(
    `id_financial_report_detail` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_order` INT(11),
    `order_reference` VARCHAR( 128 ),
	`id_financial_report` INT(11),
    `date_order_detail` DATETIME,
    `total_price_tax_incl` DECIMAL(20,6),
    `total_vat` DECIMAL(20,6),
    `total_price_tax_excl` DECIMAL(20,6),
    `total_discounts_tax_incl` DECIMAL(20,6),
    `total_discounts_tax_excl` DECIMAL(20,6),
    `product_qty` INT(11),
    `payments` TEXT,
    `id_currency` INT(11),
    `is_cancelled` INT(11) DEFAULT 0,
    `date_order_cancelled` DATETIME
    )
    ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';    
    
/**
 * DETAIL DE TVA
 */   
 $sql_requests[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'financial_report_vat`(
    `id_financial_report_vat` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_financial_report` INT(11),
    `id_tax_rules_group` INT(11),
    `tax_name` VARCHAR(128),
    `amount` DECIMAL(20,6),
    `total_ht` DECIMAL(20,6) 
    )
    ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';     
    
/**
 * DETAIL DES PAIEMENTS
 */   
 $sql_requests[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'financial_report_payment`(
    `id_financial_report_payment` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_financial_report` INT(11),
    `payment_method` VARCHAR(128),
    `amount` DECIMAL(20,6),
    `balance` TINYINT(1)
    )
    ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';         
    
 /**
 * RAPPORT DES VENTES
 */   
 $sql_requests[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'financial_sales`(
    `id_financial_sales` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_pointshop` INT(11),
    `nb_products` INT(11),
    `day` DATE
    )
    ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';         
   
 /*
 * DETAIL DE RAPPORT FINANCIER
 */
 $sql_requests[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'financial_sales_detail`(
    `id_financial_sales_detail` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_financial_sales` INT(11),
    `id_order` INT(11),
    `id_product` INT(11),
    `id_product_attribute` INT(11) DEFAULT 0,
    `reference_order` VARCHAR( 128 ),
    `id_order_detail` INT(11),
	`category_default` VARCHAR( 128 ),
	`supplier_name` VARCHAR( 128 ),
	`manufacturer_name` VARCHAR( 128 ),
    `reference` VARCHAR( 128 ),
    `reference_supplier` VARCHAR( 128 ),
    `ean13` VARCHAR( 13 ),
    `code_analytic` VARCHAR( 128 ),
    `product_name` VARCHAR( 256 ),
    `product_attributes` VARCHAR( 256 ),
    `sales_type` VARCHAR( 3 ),
    `quantity` INT(11),
    `wholesale_price` DECIMAL(20,6),
    `total_tax_incl` DECIMAL(20,6),
    `total_tax_excl` DECIMAL(20,6),
    `margin_rate` DECIMAL(20,2),
	`instant_stock` INT(11)
    )
    ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';       
	
 /**
 * RAPPORT DES ETATS DE CAISSE (ouverture, mouvements, fermeture...)
 */   
 $sql_requests[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'financial_states`(
    `id_financial_state` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_service` INT(11),
    `id_financial_report` INT(11),
    `id_type` INT(11),
    `id_pointshop` INT(11),
    `id_employee` INT(11),
    `employee` VARCHAR(128),
    `message` VARCHAR(1000),
    `payment_mode` VARCHAR(128),
    `total_on_open` DECIMAL(20,6),
    `total_on_closed` DECIMAL(20,6),
    `total_movements` DECIMAL(20,6),
    `day` DATE,
    `date_add` VARCHAR(19)    
    )
    ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';   

/**
* MISES A JOUR DES CHAMPS DEPUIS LA VERSION 2
* ALTER TABLE UNIQUEMENT SI LE CHAMPS N'EXISTE PAS
*/
$tables = 	array(
				'financial_report' => 
					array(
						'id_service' => 'int(11) NOT NULL DEFAULT 1',
						'total_shipping_tax_incl' => 'DECIMAL(20,6) DEFAULT 0.000000',
    					'total_shipping_tax_excl' => 'DECIMAL(20,6) DEFAULT 0.000000',
					),
				'financial_report_detail' => 
					array(
						'id_service' => 'int(11) NOT NULL DEFAULT 1',
						'id_service_cancelled' => 'int(11) NOT NULL DEFAULT 0',
						'is_cancelled' => 'int(11) NOT NULL DEFAULT 0',
						'date_order_cancelled' => 'DATETIME',
						'total_shipping_tax_incl' => 'DECIMAL(20,6) DEFAULT 0.000000',
    					'total_shipping_tax_excl' => 'DECIMAL(20,6) DEFAULT 0.000000',
					),
				'financial_report_payment' => 
					array(
						'id_service' => 'int(11) NOT NULL DEFAULT 1',
					),
				'financial_report_vat' => 
					array(
						'id_service' => 'int(11) NOT NULL DEFAULT 1',
					),
				'financial_sales_detail' => 
					array(
						'id_service' => 'int(11) NOT NULL DEFAULT 1',
						'id_service_cancelled' => 'int(11) NOT NULL DEFAULT 0',					)					
				); 
						
/*
 * TRAITEMENT DES CHAMPS
 */				
foreach ($tables as $table => $fields)
{
	foreach ($fields as $field => $type)
	{
		$sql_requests[] = 'SET @s = (SELECT IF( (SELECT COUNT(column_name)
			        FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = "' . _DB_PREFIX_ .$table. '"
			        AND table_schema = "'._DB_NAME_.'" AND column_name = "'.$field.'"
			    ) > 0, "SELECT 1", "ALTER TABLE ' . _DB_PREFIX_ .$table. ' ADD '.$field.' '.$type.'"
			)); 
		PREPARE stmt FROM @s; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;';
	}
}	    
    