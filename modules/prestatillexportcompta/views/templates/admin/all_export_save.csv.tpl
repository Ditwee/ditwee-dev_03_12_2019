{* ENCAISSEMENTS *}
{if isset($with_header) && $with_header == true}>> {l s='Ventes et encaissements' mod='prestatillexportcompta'};;;;;;;;;;;{/if}
{assign var=diff_order value='0'}{assign var=num_order value='0'}{assign var=last_day value='0000-00-00'}{if isset($with_header) && $with_header == true}{l s='Ventes' mod='prestatillexportcompta'};{l s='Ref. Commande' mod='prestatillexportcompta'};{l s='Date' mod='prestatillexportcompta'};{l s='TTC' mod='prestatillexportcompta'};{l s='TVA' mod='prestatillexportcompta'};{l s='HT' mod='prestatillexportcompta'};{l s='Remises HT' mod='prestatillexportcompta'};{l s='Remises en %' mod='prestatillexportcompta'};{l s='Mode de paiements' mod='prestatillexportcompta'};{l s='Devise' mod='prestatillexportcompta'};{l s='Montant' mod='prestatillexportcompta'}
{/if}{foreach from=$report_details.detail.$id_service item=detail key=id_serv name=foo}{if $last_day != substr($detail.date_order_detail,0,10)}{assign var=num_order value=0}{assign var=last_day value=substr($detail.date_order_detail,0,10)}{/if}{if $diff_order != $detail.order_reference}{assign var=num_order value=$num_order+1}{$num_order}{else}{/if};{if $diff_order != $detail.order_reference}{$detail.order_reference}{/if};{$detail.date_order_detail|date_format:"%d/%m/%Y %H:%M:%S"};{$detail.total_price_tax_incl|string_format:"%.2f"|replace:".":","};{$detail.total_vat|string_format:"%.2f"|replace:".":","};{$detail.total_price_tax_excl|string_format:"%.2f"|replace:".":","};{if $diff_order != $detail.order_reference}{$detail.total_discounts_tax_excl|string_format:"%.2f"|replace:".":","};{if $total_details.$id_service.tot_discount_ttc > 0}{(($total_details.$id_service.tot_discount_ttc/($total_details.$id_service.tot_ttc+$total_details.$id_service.tot_discount_ttc))*100)|string_format:"%.2f"}{else}0{/if};{if !empty($detail.payments)}{$detail.payments.0.payment_method}{else}{l s='Paiement partiel' mod='prestatillexportcompta'}{/if};{if !empty($detail.payments)}{$currency->iso_code}{else}{$currency->iso_code}{/if};{if !empty($detail.payments)}{$detail.payments.0.amount|string_format:"%.2f"|replace:".":","}{else}{0|string_format:"%.2f"|replace:".":","}{/if};{else};;;;;{/if}{if $detail.payments|@count > 1}{foreach from=$detail.payments item=payment name=pay}{if $smarty.foreach.pay.index > 0};
;;;;;;;;{$payment.payment_method};{$currency->iso_code};{$payment.amount|string_format:"%.2f"|replace:".":","};{/if}{/foreach}{/if}{assign var=diff_order value=$detail.order_reference};
{/foreach}   


{* VENTES ANNULEES *}
{if isset($with_header) && $with_header == true}>> {l s='Ventes annulées' mod='prestatillexportcompta'};;;;;;;;;;;
{l s='Tickets de vente' mod='prestatillexportcompta'};;;{l s='Vente' mod='prestatillexportcompta'};;;;;;;;
{l s='Ventes' mod='prestatillexportcompta'};{l s='Ref. Commande' mod='prestatillexportcompta'};{l s='Date' mod='prestatillexportcompta'};{l s='Annulation' mod='prestatillexportcompta'};{l s='TTC' mod='prestatillexportcompta'};{l s='TVA' mod='prestatillexportcompta'};{l s='HT' mod='prestatillexportcompta'};{l s='Remises HT' mod='prestatillexportcompta'};{l s='Remises en %' mod='prestatillexportcompta'};{/if}
{assign var=diff_order_cancelled value='0'}
{assign var=num_order_cancelled value='0'}
{foreach from=$report_detail_cancelled item=detail  name=foo}
{if $diff_order_cancelled != $detail.order_reference}{assign var=num_order_cancelled value=$num_order_cancelled+1}{$num_order_cancelled}{else}{/if};{if $diff_order_cancelled != $detail.order_reference}{$detail.order_reference}{/if};{$detail.date_order_detail|date_format:"%d/%m/%Y %H:%M:%S"};{$detail.date_order_cancelled|date_format:"%d/%m/%Y %H:%M:%S"};{$detail.total_price_tax_incl|string_format:"%.2f"|replace:".":","};{$detail.total_vat|string_format:"%.2f"|replace:".":","};{$detail.total_price_tax_excl|string_format:"%.2f"|replace:".":","};{$detail.total_discounts_tax_excl|string_format:"%.2f"|replace:".":","};{if $detail.total_discounts_tax_excl > 0}{(($detail.total_discounts_tax_excl/($detail.total_price_tax_excl+$detail.total_discounts_tax_excl))*100)|string_format:"%.2f"}{else}0{/if};
{assign var=diff_order_cancelled value=$detail.order_reference}
{/foreach}
{* ;;;TOTAL;-{$total_cancelled.$id_service.tot_ttc|string_format:"%.2f"|replace:".":","};-{$total_cancelled.$id_service.tot_vat|string_format:"%.2f"|replace:".":","};-{$total_cancelled.$id_service.tot_ht|string_format:"%.2f"|replace:".":","};-{$total_cancelled.$id_service.tot_discount_ht|string_format:"%.2f"|replace:".":","};-{if $total_cancelled.$id_service.tot_discount_ht > 0}{(($total_cancelled.$id_service.tot_discount_ht/($total_cancelled.$id_service.tot_ht+$total_cancelled.$id_service.tot_discount_ht))*100)|string_format:"%.2f"}{else}0{/if} *}
 
 
{* TOTAL PAYMENTS RELIQUATS *}
{if isset($with_header) && $with_header == true}
>> {l s='Encaissements liés à des commandes antérieures' mod='prestatillexportcompta'};;;;;;;;;;
{l s='Tickets de vente' mod='prestatillexportcompta'};;;;{l s='Règlements' mod='prestatillexportcompta'};;;
{l s='Ventes' mod='prestatillexportcompta'};{l s='Réf. Commande' mod='prestatillexportcompta'};{l s='Date de règlement' mod='prestatillexportcompta'};{l s='Date de la commande' mod='prestatillexportcompta'};{l s='Mode de paiements' mod='prestatillexportcompta'};{l s='Devise' mod='prestatillexportcompta'};{l s='Montant' mod='prestatillexportcompta'};{/if}
{assign var=tot_reliquat value=0}
{foreach from=$balances.detail.$id_service item=detail name=foo}{$smarty.foreach.foo.iteration};{$detail.payments.order_reference};{$detail.payments.date_add|date_format:"%d/%m/%Y %H:%M:%S"};{$detail.date_order_detail|date_format:"%d/%m/%Y %H:%M:%S"};{$detail.payments.payment_method};{$currency->iso_code};{$detail.payments.amount|string_format:"%.2f"|replace:".":","};
{assign var=tot_reliquat value=$tot_reliquat+$detail.payments.amount}
{/foreach}
					

{* TOTAL PAYMENTS *}
{if isset($with_header) && $with_header == true}
>> {l s='Totaux de TVA' mod='prestatillexportcompta'};;;;;;;;;;;
{l s='Types de TVA pour le point de vente ' mod='prestatillexportcompta'};{l s='Total HT ' mod='prestatillexportcompta'};{l s='Total TVA' mod='prestatillexportcompta'}{/if}
{foreach from=$report_vat.$id_service item=vat}
{$vat.tax_name};{$vat.sum_total_ht|string_format:"%.2f"|replace:".":","};{$vat.sum_amount|string_format:"%.2f"|replace:".":","}
{/foreach}
TOTAL VENTES {if isset($total_cancelled.$id_service)}{if $total_cancelled.$id_service.tot_ttc > 0} - ANNULATIONS {/if}{/if};{if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)}{$total_details.$id_service.tot_ht-$total_cancelled.$id_service.tot_ht|string_format:"%.2f"|replace:".":","}{/if}{else}{$total_details.$id_service.tot_ht|string_format:"%.2f"|replace:".":","}{/if};{if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)}{$total_details.$id_service.tot_vat-$total_cancelled.$id_service.tot_vat|string_format:"%.2f"|replace:".":","}{/if}{else}{$total_details.$id_service.tot_vat|string_format:"%.2f"|replace:".":","}{/if}


{* TOTAL VAT *}
{if isset($with_header) && $with_header == true}
>> {l s='Totaux des encaissements' mod='prestatillexportcompta'};;;;;;;;;;;
{l s='Total des encaissements pour le point de vente ' mod='prestatillexportcompta'};{l s='Nbre' mod='prestatillexportcompta'};{l s='Total' mod='prestatillexportcompta'};{/if}
{assign var=tot_reliquat value=0}{assign var=tot_nbr value=0}{foreach from=$report_payment.detail.$id_service item=payment key=k}
{$k};{$report_payment.nbr.$id_service.$k};{assign var=tot_nbr value=$tot_nbr+$report_payment.nbr.$id_service.$k}{$payment|string_format:"%.2f"|replace:".":","};
{/foreach}
TOTAL ENCAISSEMENTS {if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)} - ANNULATIONS {/if}{/if}{if $tot_reliquat > 0} + PAIEMENTS RELIQUATS{/if};{$tot_nbr};{if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)}{$report_payment.total.$id_service-$total_cancelled.$id_service.tot_ttc|string_format:"%.2f"|replace:".":","}{/if}{else if $tot_reliquat > 0}{$report_payment.total.$id_service+$tot_reliquat|string_format:"%.2f"|replace:".":","}{else}{$report_payment.total.$id_service|string_format:"%.2f"|replace:".":","}{/if}


