{literal}
<script type="text/javascript">

$(document).ready(function(){
	if ($(".datepicker").length > 0)
		$(".datepicker").datepicker({
			prevText: '',
			nextText: '',
			dateFormat: 'yy-mm-dd'
		});

	if ($(".datetimepicker").length > 0)
	$('.datetimepicker').datetimepicker({
		prevText: '',
		nextText: '',
		dateFormat: 'yy-mm-dd',
		// Define a custom regional settings in order to use PrestaShop translation tools
		currentText: '{l s="Now" js=1}',
		closeText: '{l s="Done" js=1}',
		ampm: false,
		amNames: ['AM', 'A'],
		pmNames: ['PM', 'P'],
		timeFormat: 'hh:mm:ss tt',
		timeSuffix: '',
		timeOnlyTitle: '{l s="Choose Time" js=1}',
		timeText: '{l s="Time" js=1}',
		hourText: '{l s="Hour" js=1}',
		minuteText: '{l s="Minute" js=1}',
	});

});
	
</script>
{/literal}

<div class="clearfix"></div>

<div class="bootstrap">
	<!-- Module content -->
	<div id="modulecontent" class="clearfix">

		<!-- Nav tabs -->
		<div class="col-lg-2">
			<div class="list-group">
				<a href="#infos" class="menu_tab list-group-item active" data-toggle="tab"><i class="icon-cogs"></i> {l s='Paramètres' mod='prestatillexportcompta'}</a>
				<a href="#reportfinancial" class="menu_tab list-group-item " data-toggle="tab"><i class="icon-area-chart"></i> {l s='Rapports financiers' mod='prestatillexportcompta'}</a>
				<a href="#reportsales" class="menu_tab list-group-item" data-toggle="tab"><i class="icon-line-chart"></i> {l s='Rapports des ventes' mod='prestatillexportcompta'}</a>
				<a href="#export" class="menu_tab list-group-item" data-toggle="tab"><i class="icon-file-excel-o"></i> {l s='Exporter' mod='prestatillexportcompta'}</a>
			</div>
			<div class="list-group">
				<a class="list-group-item"><i class="icon-info"></i> {l s='Version' mod='prestatillexportcompta'} {$module_version|escape:'htmlall':'UTF-8'}</a>
			</div>
		</div>
		
		<!-- Tab panes -->
		<div class="tab-content col-lg-10">
			<div class="tab-pane active panel" id="infos">
				{include file="./tabs/infos.tpl"}
			</div>
		</div>
		<div class="tab-content col-lg-10">
			<div class="tab-pane panel" id="reportfinancial">
				{include file="./tabs/reportfinancial.tpl"}
			</div>
		</div>
		<div class="tab-content col-lg-10">
			<div class="tab-pane panel" id="reportsales">
				{include file="./tabs/reportsales.tpl"}
			</div>
		</div>
		<div class="tab-content col-lg-10">
			<div class="tab-pane panel" id="export">
				{include file="./tabs/export.tpl"}
			</div>
		</div>
	</div>
</div>
