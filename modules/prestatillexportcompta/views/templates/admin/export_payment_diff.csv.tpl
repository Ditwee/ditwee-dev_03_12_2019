{* TOTAL PAYMENTS RELIQUATS *}
{if isset($with_header) && $with_header == true}{l s='Ventes' mod='prestatillexportcompta'};{l s='Réf. Commande' mod='prestatillexportcompta'};{l s='Date de règlement' mod='prestatillexportcompta'};{l s='Date de la commande' mod='prestatillexportcompta'};{l s='Mode de paiements' mod='prestatillexportcompta'};{l s='Devise' mod='prestatillexportcompta'};{l s='Montant' mod='prestatillexportcompta'};
{/if}
{assign var=tot_reliquat value=0}{if !empty($balances.detail.$id_service)}{foreach from=$balances.detail.$id_service item=detail name=foo}{$smarty.foreach.foo.iteration};{$detail.payments.order_reference};{$detail.payments.date_add|date_format:"%d/%m/%Y %H:%M:%S"};{$detail.date_order_detail|date_format:"%d/%m/%Y %H:%M:%S"};{$detail.payments.payment_method};{$currency->iso_code};{$detail.payments.amount|string_format:"%.2f"|replace:".":","};
{assign var=tot_reliquat value=$tot_reliquat+$detail.payments.amount}
{/foreach}
{/if}
