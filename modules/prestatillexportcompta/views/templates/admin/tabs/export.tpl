{*
* 2007-2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*}

<div class="clearfix"></div>
<h3>
    <i class="icon-book"></i> {l s='Exporter un rapport' mod='prestatillexportcompta'} <small>{$module_display_name|escape:'htmlall':'UTF-8'}</small>
</h3>
<form role="form" class="form-horizontal"  action="#" method="POST" id="export-form" name="export-form">
	<div class="alert alert-info">
		{l s='Sélectionner une date ou une tranche de date pour exporter les rapports financiers' mod='prestatillexportcompta'}		
	</div>
	
	<div class="form-group">
		<label class="control-label col-lg-3" for="date_from">
				<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='JJ/MM/AAAA'}">
				{l s='Date de début : ' mod='prestatillexportcompta'}
			</span>
		</label>
		<div class="col-lg-9">				
			<div class="col-lg-3">
				<div class="input-group">
					<input class="datepicker " type="text" name="date_from_export" value="{$smarty.now|date_format:'%Y-%m-%d'}" style="text-align: center" id="date_from_export" />
					<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-lg-3" for="date_from">
				<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='JJ/MM/AAAA'}">
				{l s='Date de fin : ' mod='prestatillexportcompta'}
			</span>
		</label>
		<div class="col-lg-9">				
			<div class="col-lg-3">
				<div class="input-group">
					<input class="datepicker" type="text" name="date_to_export" value="{$smarty.now|date_format:'%Y-%m-%d'}" style="text-align: center" id="date_to_export" />
					<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-lg-3" for="id_pointshop">
				<span class="label-tooltip" data-toggle="tooltip">
				{l s='Point de vente : ' mod='prestatillexportcompta'}
			</span>
		</label>
		<div class="col-lg-9">				
			<div class="col-lg-3">
				<div class="input-group">
					<select id="id_pointshop" name="id_pointshop">
						<option value="0">En ligne</option>
						{foreach from=$pointshops item=pointshop}
							<option value="{$pointshop.id_a_caisse_pointshop}">{$pointshop.name}</option>
						{/foreach}
					</select>
				</div>
			</div>
		</div>
	</div>
	 
	
	<div id="cache_loader" class="col-lg-12 alert alert-success" style="display:none">
	</div>
	<div class="clearfix"></div>
	
	<div class="panel-footer">
		<div class="btn-group pull-right">
			<button class="btn btn-default" type="submit" name="export_brouillard" id="export_brouillard">
				<i class="process-icon-o icon-file-excel-o"></i> 
				{l s='Export CA et Encaissements' mod='prestatillexportcompta'}
			</button>
			{*
			<button class="btn btn-default" type="submit" name="export_cancelled" id="export_cancelled">
				<i class="process-icon-o icon-file-excel-o"></i> 
				{l s='Export des ventes annulées' mod='prestatillexportcompta'}
			</button>
			<button class="btn btn-default" type="submit" name="export_payment_diff" id="export_payment_diff">
				<i class="process-icon-o icon-file-excel-o"></i> 
				{l s='Export des paiements reliquats' mod='prestatillexportcompta'}
			</button>
			*}
		</div>
		<div class="clearfix"></div>
	</div>
</form>