{*
* 2007-2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*}

<div class="clearfix"></div>
<h3>
    <i class="icon-cogs"></i> {l s='Paramètres généraux' mod='prestatillexportcompta'} <small>{$module_display_name|escape:'htmlall':'UTF-8'}</small>
</h3>
<form role="form" class="form-horizontal"  action="#" method="POST" id="config_form" name="config_form">

		<div class="form-group">
			<label class="control-label col-lg-3" for="PST_EXPORT_COMPTA_POINTSHOP_NAME">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Indiquez le nom que vous souhaitez donner à votre boutique en ligne dans les rapports'}">
						{l s='Nom de la boutique en ligne : ' mod='prestatillexportcompta'}
					</span>
			</label>
			<div class="col-lg-9">				
				<div class="col-lg-3">
					<input type="text" name="PST_EXPORT_COMPTA_POINTSHOP_NAME" value="{$config_vars.PST_EXPORT_COMPTA_POINTSHOP_NAME}" style="text-align: center" id="PST_EXPORT_COMPTA_POINTSHOP_NAME" />
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<hr>	
		<h4>{l s='Rapports financiers' mod='prestatillexportcompta'}</h4>
		<div class="alert alert-warning">
			{l s='Les rapports financiers sont générés une fois par jour, en fin de journée.' mod='prestatillexportcompta'}		
		</div>
		<h4>{l s='Utilisez cettte URL pour générer une tâche cron quotidienne pour la génération des rapports financier' mod='prestatillexportcompta'}</h4>
		<a href="{$cron_url}">
				<i class="icon-external-link-sign"></i>
				{$cron_url}
		</a>
		<hr>	
		<h4>{l s='Rapports de ventes' mod='prestatillexportcompta'}</h4>
		<div class="alert alert-info">
			{l s='Les rapports de ventes sont générés automatiquement à chaque vente.' mod='prestatillexportcompta'}		
		</div>
		<div class="panel-footer">
            <div class="btn-group pull-right">
                <button name="submitConfiguration" id="submitConfiguration" type="submit" class="btn btn-default"><i class="process-icon-save"></i> {l s='Save' mod='prestatillexportcompta'}</button>
            </div>
        </div>
        
       
</form>
