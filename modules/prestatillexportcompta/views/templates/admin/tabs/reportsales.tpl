{*
* 2007-2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*}

<div class="clearfix"></div>
<h3>
    <i class="icon-book"></i> {l s='Rapports des ventes' mod='prestatillexportcompta'} <small>{$module_display_name|escape:'htmlall':'UTF-8'}</small>
</h3>
<form role="form" class="form-horizontal"  action="#" method="POST" id="sale_form" name="sale_form">
	<div class="alert alert-info">
		{l s='Sélection une date ou une tranche de date pour créer les rapports de ventes' mod='prestatillexportcompta'}		
	</div>
	
	<div class="form-group">
		<label class="control-label col-lg-3" for="date_from">
				<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='JJ/MM/AAAA'}">
				{l s='Date de début : ' mod='prestatillexportcompta'}
			</span>
		</label>
		<div class="col-lg-9">				
			<div class="col-lg-3">
				<div class="input-group">
					<input class="datepicker " type="text" name="date_sales_from" value="{$smarty.now|date_format:'%Y-%m-%d'}" style="text-align: center" id="date_sales_from" />
					<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-lg-3" for="date_from">
				<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='JJ/MM/AAAA'}">
				{l s='Date de fin : ' mod='prestatillexportcompta'}
			</span>
		</label>
		<div class="col-lg-9">				
			<div class="col-lg-3">
				<div class="input-group">
					<input class="datepicker" type="text" name="date_sales_to" value="{$smarty.now|date_format:'%Y-%m-%d'}" style="text-align: center" id="date_sales_to" />
					<span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
				</div>
			</div>
		</div>
	</div>
	
	
	<div id="cache_loader_sales" class="col-lg-12 alert alert-success" style="display:none">
	</div>
	<div class="clearfix"></div>
	
	<div class="panel-footer">
		<div class="btn-group pull-right">
			<button class="btn btn-default" type="submit" name="generateSalesReport" id="generateSalesReport">
				<i class="process-icon-save"></i> 
				{l s='Generate Report' mod='prestatillexportcompta'}
			</button>
		</div>
	</div>
</form>