<div class="col-lg-12">
	<form method="post" action="#" id="exportcsv" name="exportcsv" class="form-inline" style="display: none;">
		<input type="hidden" id="id_report" name="id_report" value="{$report->id}" />
		<button type="submit" name="submitExport" id="submitExport"></button>
	</form>
	<div class="row">
		<div class="col-lg-12 no-print">
			<div class="panel">
				<div class="panel-heading text-center">
					{l s='Bilan du jour ' mod='prestatillexportcompta'}{$report->day|date_format:'d/m/Y'}
				</div>
				<div class="text-center">
					<div class="col-lg-2 col-xs-2 text-left box-stats color4">
						<div class="kpi-content">
							<span class="title">{l s='Total TTC' mod='prestatillexportcompta'}</span>
							<span class="value">{Tools::displayPrice($report->total_tax_incl, $currency)}</span>
						</div>
					</div>
					<div class="col-lg-2 text-left box-stats color4">
						<div class="kpi-content">
							<span class="title">{l s='Total HT' mod='prestatillexportcompta'}</span>
							<span class="value">{Tools::displayPrice($report->total_tax_excl, $currency)}</span></div>
					</div>
					<div class="col-lg-2 text-left box-stats color4">
						<div class="kpi-content">
							<span class="title">{l s='Total TVA' mod='prestatillexportcompta'}</span>
							<span class="value">{Tools::displayPrice($report->total_vat, $currency)}</span>
						</div>
					</div>
					<div class="col-lg-2 text-left box-stats color4">
						<div class="kpi-content">
							<span class="title">{l s='Nombre de ventes' mod='prestatillexportcompta'}</span>
							<span class="value">{$report_details.nbr_sales}</span>
						</div>
					</div>
					<div class="col-lg-2 text-left box-stats color4">
						<div class="kpi-content">
							<span class="title">{l s='Total Encaissements' mod='prestatillexportcompta'}</span>
							<span class="value">{Tools::displayPrice($report->total_payment, $currency)}</span>
						</div>
					</div>
					<div class="col-lg-2 text-left box-stats color4">
						<div class="kpi-content">
							<span class="title">{l s='Total Marge HT' mod='prestatillexportcompta'}</span>
							<span class="value">{if !empty($report_details.total)}{Tools::displayPrice($report_details.total.0, $currency)}{else}{Tools::displayPrice(0, $currency)}{/if}</span>
						</div>
					</div>
					{*
					<div class="col-lg-3 text-left">
						<h4><b>{l s='Caisse' mod='prestatillexportcompta'}</b> : <i>N/C</i></h43>
					</div>
					*}
					<div class="clearfix"></div>
				</div>
			</div>	
		</div>	
		{if isset($report_details.detail)}
		{foreach from=$report_details.detail item=service key=id_service}
			<div id="prestatill_report_heading" class="col-lg-12">
				<div id="" class="panel">
					<div class="panel-heading">
						<i class="icon-credit-card"></i>
						{l s='Rapport financier du ' mod='prestatillexportcompta'}{$report->day|date_format:'d/m/Y'}
						<span class="badge">n°{$id_service}</span>
						<div class="panel-heading-action">
							<div class="btn-group">
								<a class="btn btn-default see_report" href="#report_{$id_service}">
									<i class="icon-eye"></i> VOIR
								</a>
								<a class="btn btn-default export_report {if !isset($report_states.$id_service.2)}disabled{/if}" href="index.php?controller=AdminPrestatillFinancialReport&exportFinancialReport={$report->id}&id_financial_report=197&viewfinancial_report&token={$tocken}&id_service={$id_service}">
									<i class="icon-file-excel-o"></i> Exporter en CSV
								</a>
							</div>
						</div>
					</div>
					<div class="text-center">
						<div class="col-lg-3 text-left box-stats color1">
							<div class="kpi-content">
								<i class="icon-building-o"></i>
								<span class="title">{l s='Magasin' mod='prestatillexportcompta'}</span>
								<span class="value">{$pointshop_name.name}</span>
							</div>
						</div>
						
						<div class="col-lg-3 text-left box-stats color4">
							<div class="kpi-content">
								<i class="icon-clock-o"></i>
								<span class="title">{l s='Ouverture' mod='prestatillexportcompta'}</span>
								<!-- id_type = 1 : ouverture -->
								<span class="value">{if isset($report_states.$id_service.1)}{$report_states.$id_service.1.date_add|date_format:"d/m/Y H:i:s"}{else}--{/if}</span>
							</div>
						</div>
						
						
						<div class="col-lg-3 text-left box-stats color2">
							<div class="kpi-content">
								<i class="icon-clock-o"></i>
								<span class="title">{l s='Fermeture' mod='prestatillexportcompta'}</span>
								<span class="value">{if isset($report_states.$id_service.2)}{$report_states.$id_service.2.date_add|date_format:"d/m/Y H:i:s"}{else}En cours...{/if}</span>
							</div>
						</div>
						<div class="col-lg-3 text-left box-stats color1">
							<div class="kpi-content">
								<i class="icon-user"></i>
								<span class="title">{l s='Utilisateur' mod='prestatillexportcompta'}</span>
								<span class="value">{if isset($report_states.$id_service.1.employee)}{$report_states.$id_service.1.employee}{if isset($report_states.$id_service.2) && ($report_states.$id_service.2.employee != $report_states.$id_service.1.employee)} > {$report_states.$id_service.2.employee}{/if}{else}--{/if}</span>
							</div>
						</div>
						<div class="col-lg-6 text-left box-stats ">
							<div class="kpi-content ">
								<i class="icon-clock-o"></i>
								<span class="title">{l s='Informations à l\'ouverture' mod='prestatillexportcompta'}</span>
								<!-- id_type = 1 : ouverture -->
								<span class="value comment">{if !empty($report_states.$id_service.1.message)}{$report_states.$id_service.1.message}{else}{l s='N/C' mod='prestatillexportcompta'}{/if}</span>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-lg-6 text-left box-stats ">
							<div class="kpi-content ">
								<i class="icon-clock-o"></i>
								<span class="title">{l s='Informations à la fermeture' mod='prestatillexportcompta'}</span>
								<!-- id_type = 1 : ouverture -->
								<span class="value comment">{if !empty($report_states.$id_service.2.message)}{$report_states.$id_service.2.message}{else}{l s='N/C' mod='prestatillexportcompta'}{/if}</span>
							</div>
							<div class="clearfix"></div>
						</div>
						{*
						<div class="col-lg-3 text-left">
							<h4><b>{l s='Caisse' mod='prestatillexportcompta'}</b> : <i>N/C</i></h43>
						</div>
						*}
						<div class="clearfix"></div>
					</div>
					
	 				<div class="table-responsive">
						<table class="table data_table" id="">
							<thead>
								<tr>
									<th class="text-right"><b>{l s='Total TTC' mod='prestatillexportcompta'}</b></th>
									<th class="text-right"><b>{l s='Total HT' mod='prestatillexportcompta'}</b></th>
									<th class="text-right"><b>{l s='Total TVA' mod='prestatillexportcompta'}</b></th>
									<th class="text-right"><b>{l s='Total Encaissements' mod='prestatillexportcompta'}</b></th>
									<th class="text-right"><b>{l s='Total Marge HT' mod='prestatillexportcompta'}</b></th>
									<th class="text-right"><b>{l s='Total Remises HT' mod='prestatillexportcompta'}</b></th>
									<th class="text-right"><b>{l s='Total Remises en %' mod='prestatillexportcompta'}</b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-right">{if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)}{Tools::displayPrice($total_details.$id_service.tot_ttc-$total_cancelled.$id_service.tot_ttc, $currency)}{/if}{else}{Tools::displayPrice($total_details.$id_service.tot_ttc, $currency)}{/if}</td>
									<td class="text-right">{if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)}{Tools::displayPrice($total_details.$id_service.tot_ht-$total_cancelled.$id_service.tot_ht, $currency)}{/if}{else}{Tools::displayPrice($total_details.$id_service.tot_ht, $currency)}{/if}</td>
									<td class="text-right">{if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)}{Tools::displayPrice($total_details.$id_service.tot_vat-$total_cancelled.$id_service.tot_vat, $currency)}{/if}{else}{Tools::displayPrice($total_details.$id_service.tot_vat, $currency)}{/if}</td>
									<td class="text-right">{Tools::displayPrice($report_payment_wb.total.$id_service, $currency)}</td>
									<td class="text-right">{Tools::displayPrice($report_details.total.$id_service, $currency)}</td>
									<td class="text-right">{if $total_details.$id_service.tot_discount_ht > 0.01}{Tools::displayPrice($total_details.$id_service.tot_discount_ht, $currency)}{else}{Tools::displayPrice(0, $currency)}{/if}</td>
									<td class="text-right">{if $total_details.$id_service.tot_discount_ttc > 0.01}{(($total_details.$id_service.tot_discount_ttc/($total_details.$id_service.tot_ttc+$total_details.$id_service.tot_discount_ttc))*100)|string_format:"%.2f"}{else}0{/if}%</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
					<div id="report_{$id_service}" class="report-detail print-together" >
						<br />
						<hr />
						<br />
							<i class="icon-bar-chart"></i> {l s='Ventes & encaissements' mod='prestatillexportcompta'}
						<div class="table-responsive">
							<table class="table data_table table-striped">
								<thead>
									<tr>
										<th class="text-center" colspan="4"><b>{l s='Tickets de vente' mod='prestatillexportcompta'}</b></th>
										<th class="text-center" colspan="5"><b>{l s='Vente' mod='prestatillexportcompta'}</b></th>
										<th class="text-center" colspan="4"><b>{l s='Règlements' mod='prestatillexportcompta'}</b></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center"><b>{l s='Ventes' mod='prestatillexportcompta'}</b></td>
										<td class="text-center no-print"><b>{l s='ID Cmd' mod='prestatillexportcompta'}</b></td>
										{*<td class="text-center no-print"><b>{l s='Ref. Cmd' mod='prestatillexportcompta'}</b></td>*}
										<td class="text-center no-print" style="min-width: 120px"><b>{l s='Client' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Date' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='TTC' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='TVA' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='HT' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Ports HT' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Ports TTC' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Remises HT' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Remises en %' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Mode de paiements' mod='prestatillexportcompta'}</b></td>
										{*<td class="text-center no-print"><b>{l s='Devise' mod='prestatillexportcompta'}</b></td>*}
										<td class="text-center"><b>{l s='Montant' mod='prestatillexportcompta'}</td>
									</tr>
									{assign var=diff_order value='0'}
									{assign var=num_order value='0'}
									
									{foreach from=$service item=detail key=id_serv name=foo}
									
									<tr>
										<td class="text-center">{if $diff_order != $detail.order_reference}{assign var=num_order value=$num_order+1}{$num_order}{else}{/if}</td>
										<td class="text-left no-print">{if $diff_order != $detail.order_reference}<a href="index.php?controller=AdminOrders&id_order={$detail.id_order}&vieworder&token={$tocken_order}">{$detail.id_order}</a>{/if}</td>
										{*<td class="text-left no-print">{if $diff_order != $detail.order_reference}<a href="index.php?controller=AdminOrders&id_order={$detail.id_order}&vieworder&token={$tocken_order}">{$detail.order_reference}</a>{/if}</td>*}
										<td class="text-left no-print" style="min-width: 120px">{$detail.customer}</td>
										<td class="text-center">{$detail.date_order_detail|date_format:"%H:%M:%S"}</td>
										<td class="text-right">{Tools::displayPrice($detail.total_price_tax_incl, $currency)}</td>
										<td class="text-right">{Tools::displayPrice($detail.total_vat, $currency)}</td>
										<td class="text-right">{Tools::displayPrice($detail.total_price_tax_excl, $currency)}</td>
										<td class="text-right">{Tools::displayPrice($detail.total_shipping_tax_excl, $currency)}</td>
										<td class="text-right">{Tools::displayPrice($detail.total_shipping_tax_incl, $currency)}</td>
										{* COMMANDES SLPITEES DONC ON AFFICHE QU'UNE FOIS LE PAIEMENT *}
										{if 1 == 1} {* $diff_order != $detail.order_reference *}
											<td class="text-right">{if $detail.total_discounts_tax_excl > 0.01}{Tools::displayPrice($detail.total_discounts_tax_excl, $currency)}{else}{Tools::displayPrice(0, $currency)}{/if}</td>
											<td class="text-right">{if $detail.total_discounts_tax_excl > 0.01}{(($detail.total_discounts_tax_excl/($detail.total_price_tax_excl+$detail.total_discounts_tax_excl))*100)|string_format:"%.2f"}{else}0{/if}%</td>
											<td class="text-left">{if !empty($detail.payments)}{$detail.payments.0.payment_method}{else}{* <i><span style="color:#bbb">{l s='Paiement partiel' mod='prestatillexportcompta'}</i></span>*}{/if}</td><!-- ATTENTINON PEUT ÊTRE MULTIPLE -->
											{*<td class="text-center">{if !empty($detail.payments)}{$currency->iso_code}{else}<i><span style="color:#bbb">{$currency->iso_code}</i></span>{/if}</td>*}
											<td class="text-right">{if !empty($detail.payments)}{Tools::displayPrice($detail.payments.0.amount, $currency)}{else}<i><span style="color:#bbb">{Tools::displayPrice(0, $currency)}</i></span>{/if}</td>
										{else}
											<td class="text-right">{if $detail.total_discounts_tax_excl > 0.01}{Tools::displayPrice($detail.total_discounts_tax_excl, $currency)}{else}{Tools::displayPrice(0, $currency)}{/if}</td>
											<td class="text-right">{if $detail.total_discounts_tax_excl > 0.01}{(($detail.total_discounts_tax_excl/($detail.total_price_tax_excl+$detail.total_discounts_tax_excl))*100)|string_format:"%.2f"}{else}0{/if}%</td>
											<td class="text-left"></td>
											<td class="text-center"></td>
											<td class="text-right"></td>
										{/if}
									</tr>
									{if $detail.payments|@count > 1}
										{foreach from=$detail.payments item=payment name=pay}
										{if $smarty.foreach.pay.index > 0}
										<tr>
											<td colspan="11" class="no-print"></td>
											<td colspan="9" class="print"></td>
											<td class="text-left">{$payment.payment_method}</td><!-- ATTENTINON PEUT ÊTRE MULTIPLE -->
											{*<td class="text-center no-print">{$currency->iso_code}</td>*}
											<td class="text-right">{Tools::displayPrice($payment.amount, $currency)}</td>
										</tr>
										{/if}
										{/foreach}
									{/if}
									{assign var=diff_order value=$detail.order_reference}
									{/foreach}
								</tbody>
								<tfoot>
									<!-- FIN TABLEAU -->
									<tr>
										<td class="text-center no-print"></td>
										<td class="text-center no-print" colspan="2"></td>
										<td class="text-center print"></td>
										<td class="text-center"><b>TOTAL VENTES</b></td>
										<td class="text-right"><b>{Tools::displayPrice($total_details.$id_service.tot_ttc, $currency)}</b></td>
										<td class="text-right"><b>{Tools::displayPrice($total_details.$id_service.tot_vat, $currency)}</b></td>
										<td class="text-right"><b>{Tools::displayPrice($total_details.$id_service.tot_ht, $currency)}</b></td>
										<td class="text-right"><b>{Tools::displayPrice($total_details.$id_service.tot_shipping_ht, $currency)}</b></td>
										<td class="text-right"><b>{Tools::displayPrice($total_details.$id_service.tot_shipping_ttc, $currency)}</b></td>
										<td class="text-right"><b>{if $total_details.$id_service.tot_discount_ht > 0.01}{Tools::displayPrice($total_details.$id_service.tot_discount_ht, $currency)}{else}{Tools::displayPrice(0, $currency)}{/if}</b></td>
										<td class="text-right"><b>{if $total_details.$id_service.tot_discount_ttc > 0.01}{(($total_details.$id_service.tot_discount_ttc/($total_details.$id_service.tot_ttc+$total_details.$id_service.tot_discount_ttc))*100)|string_format:"%.2f"}{else}0{/if}%</b></td>
										{*<td class="text-center"></td>*}
										<td class="text-center"><b>TOTAL ENCAISSEMENTS</b></td>
										<td class="text-right"><b>{Tools::displayPrice($report_payment.total.$id_service, $currency)}</b></td>
									</tr>
								</tfoot>
							</table>
						</div>
						<br />
						<hr />
						<br />
						<div>
							{$HOOK_PRESTATILL_AFTER_REPORT}
						</div>
						<br />
						<hr />
						<br />
						{if !empty($report_detail_cancelled.$id_service)}
							<i class="icon-bar-chart"></i> {l s='Ventes annulées' mod='prestatillexportcompta'}
							<div class="table-responsive">
								<table class="table data_table table-striped" id="">
									<thead>
										<tr>
											<th class="text-center" colspan="3"><b>{l s='Tickets de vente' mod='prestatillexportcompta'}</b></th>
											<th class="text-center" colspan="8"><b>{l s='Vente' mod='prestatillexportcompta'}</b></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center"><b>{l s='Ventes' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='Ref. Commande' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='Date' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='Annulation' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='TTC' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='TVA' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='HT' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='Ports HT' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='Ports TTC' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='Remises HT' mod='prestatillexportcompta'}</b></td>
											<td class="text-center"><b>{l s='Remises en %' mod='prestatillexportcompta'}</b></td>
										</tr>
										{assign var=diff_order_cancelled value='0'}
										{assign var=num_order_cancelled value='0'}
										{foreach from=$report_detail_cancelled.$id_service item=detail  name=foo}
										<tr>
											<td class="text-center">{if $diff_order_cancelled != $detail.order_reference}{assign var=num_order_cancelled value=$num_order_cancelled+1}{$num_order_cancelled}{else}{/if}</td>
											<td class="text-left">{if $diff_order_cancelled != $detail.order_reference}<a href="index.php?controller=AdminOrders&id_order={$detail.id_order}&vieworder&token={$tocken_order}">{$detail.order_reference}</a>{/if}</td>
											<td class="text-left">{$detail.date_order_detail|date_format:"%d/%m/%Y %H:%M:%S"}</td>
											<td class="text-left">{$detail.date_order_cancelled|date_format:"%d/%m/%Y %H:%M:%S"}</td>
											<td class="text-right">{Tools::displayPrice($detail.total_price_tax_incl, $currency)}</td>
											<td class="text-right">{Tools::displayPrice($detail.total_vat, $currency)}</td>
											<td class="text-right">{Tools::displayPrice($detail.total_price_tax_excl, $currency)}</td>
											<td class="text-right">{Tools::displayPrice($detail.total_shipping_tax_excl, $currency)}</td>
											<td class="text-right">{Tools::displayPrice($detail.total_shipping_tax_incl, $currency)}</td>
											<td class="text-right">{Tools::displayPrice($detail.total_discounts_tax_excl, $currency)}</td>
											<td class="text-right">{if $detail.total_discounts_tax_excl > 0}{(($detail.total_discounts_tax_excl/($detail.total_price_tax_excl+$detail.total_discounts_tax_excl))*100)|string_format:"%.2f"}{else}0{/if}%</td>
										</tr>
										{assign var=diff_order_cancelled value=$detail.order_reference}
										{/foreach}
									</tbody>
									<tfoot>
										<!-- FIN TABLEAU -->
										<tr>
											<td class="text-center" colspan="3"></td>
											<td class="text-center"><b>TOTAL</b></td>
											<td class="text-right"><b>-{Tools::displayPrice($total_cancelled.$id_service.tot_ttc, $currency)}</b></td>
											<td class="text-right"><b>-{Tools::displayPrice($total_cancelled.$id_service.tot_vat, $currency)}</b></td>
											<td class="text-right"><b>-{Tools::displayPrice($total_cancelled.$id_service.tot_ht, $currency)}</b></td>
											<td class="text-right"><b>{if $total_cancelled.$id_service.tot_shipping_ht > 0}-{/if}{Tools::displayPrice($total_cancelled.$id_service.tot_shipping_ht, $currency)}</b></td>
											<td class="text-right"><b>{if $total_cancelled.$id_service.tot_shipping_ttc > 0}-{/if}{Tools::displayPrice($total_cancelled.$id_service.tot_shipping_ttc, $currency)}</b></td>
											<td class="text-right"><b>{if $total_cancelled.$id_service.tot_discount_ht > 0}-{/if}{Tools::displayPrice($total_cancelled.$id_service.tot_discount_ht, $currency)}</b></td>
											<td class="text-right"><b>{if $total_cancelled.$id_service.tot_discount_ht > 0}-{(($total_cancelled.$id_service.tot_discount_ht/($total_cancelled.$id_service.tot_ht+$total_cancelled.$id_service.tot_discount_ht))*100)|string_format:"%.2f"}{else}0{/if}%</b></td>
										</tr>
									</tfoot>
								</table>
							</div>
						{else}
							<i class="icon-bar-chart"></i> {l s='Ventes annulées' mod='prestatillexportcompta'}
							<br />
							<br />
							<span style="display:block;" class="alert alert-info">{l s='Aucune ventes annluées' mod='prestatillexportcompta'}</span>
						{/if}
						<br />
						<hr />
						<br />
						{assign var=tot_reliquat value=0}
						{if !empty($balances['detail'])}
						<i class="icon-bar-chart"></i> {l s='Paiements reliquats (encaissements liés à des commandes antérieures)' mod='prestatillexportcompta'}
						<div class="table-responsive">
							<table class="table data_table table-striped" id="">
								<thead>
									<tr>
										<th class="text-center" colspan="4"><b>{l s='Tickets de vente' mod='prestatillexportcompta'}</b></th>
										<th class="text-center" colspan="3"><b>{l s='Règlements' mod='prestatillexportcompta'}</b></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-center"><b>{l s='Ventes' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Réf. Commande' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Date de règlement' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Date de la commande' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Mode de paiements' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Devise' mod='prestatillexportcompta'}</b></td>
										<td class="text-center"><b>{l s='Montant' mod='prestatillexportcompta'}</td>
									</tr>
									
									{foreach from=$balances.detail.$id_service item=detail name=foo}
									<tr>
										<td class="text-center">{$smarty.foreach.foo.iteration}</td>
										<td class="text-left">{$detail.payments.order_reference}</td>
										<td class="text-left">{$detail.payments.date_add|date_format:"%d/%m/%Y %H:%M:%S"}<i></i></td>
										<td class="text-left" >{$detail.date_order_detail|date_format:"%d/%m/%Y %H:%M:%S"}<i></i></td>
										<td class="text-left">{$detail.payments.payment_method}</td><!-- ATTENTINON PEUT ÊTRE MULTIPLE -->
										<td class="text-center">{$currency->iso_code}</td>
										<td class="text-right">{Tools::displayPrice($detail.payments.amount, $currency)}</td>
									</tr>
										{assign var=tot_reliquat value=$tot_reliquat+$detail.payments.amount}
									{/foreach}
								</tbody>
								<tfoot>
									<!-- FIN TABLEAU -->
									<tr>
										<td class="text-center" colspan="5"></td>
										<td class="text-center"><b>TOTAL ENCAISSEMENTS</b></td>
										<td class="text-right"><b>{Tools::displayPrice($tot_reliquat, $currency)}</b></td>
									</tr>
								</tfoot>
							</table>
						</div>
						{else}
						<i class="icon-bar-chart"></i> {l s='Paiements reliquats (encaissements liés à des commandes antérieures)' mod='prestatillexportcompta'}
						<br />
						<br />
						<span style="display:block;" class="alert alert-info">{l s='Aucun paiement reliquats' mod='prestatillexportcompta'}</span>
						{/if}
						
						<br />
						<hr />
						<br />
						<div class="print-together">
								<div class="col-lg-6">
										<div class="table-responsive">
											<table class="table data_table" id="">
												<thead>
													<tr>
														<th class="text-left"><b>{l s='Types de TVA pour le point de vente ' mod='prestatillexportcompta'}</b></th>
														<th class="text-right"><b>{l s='Total HT ' mod='prestatillexportcompta'}</b></th>
														<th class="text-right"><b>{l s='Total TVA' mod='prestatillexportcompta'}</b></th>
													</tr>
												</thead>
												<tbody>
													{foreach from=$report_vat.$id_service item=vat}
													<tr>
														<td class="text-left">
															{$vat.tax_name}
														</td>
														<td class="text-right">
															{Tools::displayPrice($vat.sum_total_ht, $currency)}
														</td>
														<td class="text-right">
															{Tools::displayPrice($vat.sum_amount, $currency)}
														</td>
													</tr>
													{/foreach}
												</tbody>
												<tfoot>
													<tr>
														<td class="text-left">
															<b>TOTAL VENTES {if isset($total_cancelled.$id_service)}{if $total_cancelled.$id_service.tot_ttc > 0} - ANNULATIONS {/if}{/if}</b>
														</td>
														<td class="text-right">
															<b>{if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)}{Tools::displayPrice($total_details.$id_service.tot_ht-$total_cancelled.$id_service.tot_ht, $currency)}{/if}{else}{Tools::displayPrice($total_details.$id_service.tot_ht, $currency)}{/if}</b>
														</td>
														<td class="text-right">
															<b>{if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)}{Tools::displayPrice($total_details.$id_service.tot_vat-$total_cancelled.$id_service.tot_vat, $currency)}{/if}{else}{Tools::displayPrice($total_details.$id_service.tot_vat, $currency)}{/if}</b>
														</td>
													</tr>
												</tfoot>
											</table>
										</div>
								</div>
								<div class="col-lg-6">
									<div class="table-responsive">
										<table class="table data_table" id="">
											<thead>
												<tr>
													<th class="text-left"><b>{l s='Total des encaissements pour le point de vente ' mod='prestatillexportcompta'}</b></th>
													<th class="text-right"><b>{l s='Nbre' mod='prestatillexportcompta'}</b></th>
													<th class="text-right"><b>{l s='Total' mod='prestatillexportcompta'}</b></th>
												</tr>
											</thead>
											<tbody>
												{assign var=tot_nbr value=0}
												{foreach from=$report_payment_wb.detail.$id_service item=payment key=k}
												{if $payment <> 0}
												<tr>
													<td class="text-left">
														{$k}
													</td>
													<td class="text-right">
														{$report_payment_wb.nbr.$id_service.$k}
														{assign var=tot_nbr value=$tot_nbr+$report_payment_wb.nbr.$id_service.$k}
													</td>
													<td class="text-right">
														{Tools::displayPrice($payment, $currency)}
													</td>
												</tr>
												{/if}
												{/foreach}
											</tbody>
											<tfoot>
												<tr>
													<td class="text-right"><b>TOTAL ENCAISSEMENTS{if $tot_reliquat > 0} + PAIEMENTS RELIQUATS{/if}</b></td>
													<td class="text-right">{$tot_nbr}</td>
													<td class="text-right"><b>{Tools::displayPrice($report_payment_wb.total.$id_service, $currency)}</b></td>
												</tr>
												{* 
												<tr>
													<td class="text-right"><b>TOTAL ENCAISSEMENTS {if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)} - ANNULATIONS {/if}{/if}{if $tot_reliquat > 0} + PAIEMENTS RELIQUATS{/if}</b></td>
													<td class="text-right">{$tot_nbr}</td>
													<td class="text-right"><b>{if isset($total_cancelled.$id_service)}{if !empty($report_detail_cancelled.$id_service)}{Tools::displayPrice($report_payment.total.$id_service-$total_cancelled.$id_service.tot_ttc, $currency)}{/if}{else if $tot_reliquat > 0}{Tools::displayPrice($report_payment.total.$id_service+$tot_reliquat, $currency)}{else}{Tools::displayPrice($report_payment.total.$id_service, $currency)}{/if}</b></td>
												</tr>
												*}
											</tfoot>
										</table>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			{/foreach}
			<!-- ICI LA FIN DU FOREACH ET LE BLOCK "TOTAL DES TOTAUX" -->
			{/if}
			
		</div>
	</div>
	
	
	<div class="row print-together">
		<div class="col-lg-12">
			
		</div>
	</div>
	
	
	{$HOOK_PRESTATILL_FOOTER}
	
</div>