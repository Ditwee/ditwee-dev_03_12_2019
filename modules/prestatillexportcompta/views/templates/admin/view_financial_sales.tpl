<div class="col-lg-12">
	<div class="row no-print" style="display:none;">
		<div class="col-lg-12 text-center">
			<h1>{l s='Rapport des ventes du' mod='prestatillexportcompta'} {$sales->day|date_format:'d/m/Y'}</h1>
			<div class="col-xs-12" style="height:20px;"></div>
		</div>
		<div class="col-lg-12">
			<div id="" class="panel">
				<div class="table-responsive text-center">
					
					<div class="col-lg-6 text-left">
						<h4><b>{l s='Magasin' mod='prestatillexportcompta'}</b> : <i>{$pointshop_name}</i></h4> 
					</div>
					<div class="col-lg-6 text-left">
						<h4><b>{l s='Caisse' mod='prestatillexportcompta'}</b> : <i>{l s='Toutes les caisses' mod='prestatillexportcompta'}</i></h4>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	
	{assign var="tot_qty" value=0}
	
	<div class="row">
		<div class="col-lg-12 text-center">
			<h1>{l s='Rapport des ventes du' mod='prestatillexportcompta'} {$sales->day|date_format:'d/m/Y'}</h1>
			<div class="col-xs-12" style="height:20px;"></div>
		</div>
		<div class="col-lg-12">
			<div id="" class="panel">
				<div class="table-responsive text-center">
					
					<div class="col-lg-6 text-left">
						<h4><b>{l s='Magasin' mod='prestatillexportcompta'}</b> : <i>{$pointshop_name}</i></h4> 
					</div>
					<div class="col-lg-6 text-left">
						<h4><b>{l s='Caisse' mod='prestatillexportcompta'}</b> : <i>{l s='Toutes les caisses' mod='prestatillexportcompta'}</i></h4>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	
	{$HOOK_PRESTATILL_HEADER}
	
	{foreach $sales_details.detail item=sale key=k}
	{assign var="tot_amount" value=0}
	<div class="row">
		<div class="col-lg-12">
			<div id="" class="panel">
				<header class="panel-heading">
					<i class="icon-bar-chart"></i> {l s='Rapport des ventes' mod='prestatillexportcompta'} n°{$k}
				</header>
				<div class="table-responsive">
					<table class="table data_table table-striped" id="XXX">
						<thead>
							<tr>
								<th class="text-left"><b>{l s='Famille' mod='prestatillexportcompta'}</b></th>
								<th class="text-left"><b>{l s='Fournisseur' mod='prestatillexportcompta'}</b></th>
								<th class="text-left"><b>{l s='Marque' mod='prestatillexportcompta'}</b></th>
								<th class="text-left"><b>{l s='Ref. fournisseur' mod='prestatillexportcompta'}</b></th>
								<th class="text-left"><b>{l s='Ref. Article' mod='prestatillexportcompta'}</b></th>
								<th class="text-left"><b>{l s='Article' mod='prestatillexportcompta'}</b></th>
								<th class="text-left"><b>{l s='Déclinaisons' mod='prestatillexportcompta'}</b></th>
								<th class="text-center"><b>{l s='Type' mod='prestatillexportcompta'}</b></th>
								<th class="text-right"><b>{l s='Qté' mod='prestatillexportcompta'}</b></th>
								<th class="text-right"><b>{l s='Montant' mod='prestatillexportcompta'}</b></th>
								<th class="text-right"><b>{l s='Taux de marge' mod='prestatillexportcompta'}</b></th>
								<th class="text-right"><b>{l s='En stock' mod='prestatillexportcompta'}</b></th>
							</tr>
						</thead>
						<tbody>
							{foreach from=$sale item=detail name=foo}
							<tr>
								<td class="text-left">{$detail.category_default}</td>
								<td class="text-left">{$detail.supplier_name}</td>
								<td class="text-left">{$detail.manufacturer_name}</td>
								<td class="text-left">{$detail.reference_supplier}</td>
								<td class="text-left">{$detail.reference}</td>
								<td class="text-left"><a href="index.php?controller=AdminProducts&id_product={$detail.id_product}&updateproduct&token={$tocken_product}">{$detail.product_name}</a></td>
								<td class="text-left">{$detail.product_attributes}</td>
								<td class="text-center">{$detail.sales_type}</td>
								<td class="text-right">{$detail.qty}</td>
								<td class="text-right">{Tools::displayPrice($detail.total_ttc, $currency)}</td>
								<td class="text-right">{if $detail.wholesale_price > 0}{$detail.avg_margin_rate|replace:'.':','}&nbsp;&nbsp;{else}<span style="color:#bbb">{$detail.avg_margin_rate|replace:'.':','}*</span>{/if}</td>
								<td class="text-right">{if $detail.stock_minimum > -9999}{$detail.stock_minimum}{else}--{/if}</td>
							</tr>
							{assign var="tot_qty" value=$tot_qty+$detail.qty}
							{assign var="tot_amount" value=$tot_amount+$detail.total_ttc}
							{/foreach}
						</tbody>
					</table>	
				</div>
				<br>
				<br>
				<hr>
				<br>
				<div class="col-lg-4 pull-right">
					<div class="table-responsive">
						<table class="table data_table table-striped " id="">
							<thead>
								<!-- FIN TABLEAU -->
								<tr>
									<td class="text-center" colspan="9"></td>
									<td class="text-center"><b>Type</b></td>
									<td class="text-right"><b>Qté</b></td>
									<td class="text-right"><b>Montant</b></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center" colspan="9"></td>
									<td class="text-center"><b>VTE</b></td>
									<td class="text-right"><b>{$tot_qty}</b></td>
									<td class="text-right"><b>{Tools::displayPrice($tot_amount, $currency)}</b></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
					<br />
					<div ><i>{l s='(*) Taux de marge indicatif basé sur le taux de marge moyen en l\'absence de prix d\'achat.'  mod='prestatillexportcompta'}</i></div>	
				</div>
				<div style="clear:both"></div>
			</div>
		</div>
	</div>
	{/foreach}
	
	
	<div class="row">
		<div class="col-lg-4 pull-right">
			<div id="" class="panel">
				<div class="table-responsive">
					<table class="table data_table table-striped" id="XXX">
						<thead>
							<tr>
								<th class="text-right">
								<b>{l s='CA Total TTC' mod='prestatillexportcompta'}</b>
								</th>							
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-right">
									<b>{Tools::displayPrice($sales_details.total, $currency)}</b>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
  {$HOOK_PRESTATILL_FOOTER}
	
</div>