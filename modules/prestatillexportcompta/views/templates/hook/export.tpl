	<section id="dashexportcompta" class="panel widget  allow_push loading">
		<header class="panel-heading">
			<i class="icon-bar-chart"></i> {l s='Situation financière avancée' mod='prestatillexportcompta'}
			<span class="panel-heading-action">
				{*<a class="list-toolbar-btn" href="#" onclick="refreshDashboard('dashexportcompta'); return false;" title="Rafraîchir">
					<i class="process-icon-refresh"></i>
				</a>*}
			</span>
		</header>
	
		{* <!-- EXPORT ENCAISSEMENTS --> *}	
		<section>
			<form method="post" action="#" id="dashexportcompta_form" name="dashexportcompta_form" class="form-inline">
				<div>
					<nav id="pointshopBox" class="options">
						<div class="pull-left">
							<div class="label">{l s='Pointshops :' mod='prestatillexportcompta'}</div>
							<input type="hidden" id="granularity" name="granularity" value="{$gr}" />
							{foreach from=$pointshops item=pointshop key=k}
								 {assign var="check" value=false}
								 {foreach from=$id_pointshop item=selected}
								 	{if $selected == $pointshop.id_a_caisse_pointshop}
								 		{$check = true}
								 	{/if}
								 {/foreach}
								<label class="checkbox-inline">
									<input type="checkbox" name=pointshops[] {if $check == true} checked="checked" {/if} value="{$pointshop.id_a_caisse_pointshop}">{$pointshop.name}</label>
							{/foreach}
						</div>
						<div class="pull-right">
							<button class="btn btn-default" type="submit" name="submitGranularity">
								{l s='Filtrer' mod='prestatillexportcompta'}
							</button>
						</div>
						<div class="clearfix"></div>
					</nav>
					<nav class="options">
						<div id="granularityBox" class="pull-left">
							<div class="label">{l s='Vue par : ' mod='prestatillexportcompta'}</div>
							<div class="btn-group">
								
								<button type="button" name"submitDay" data-granularity="day" class="btn btn-default submitGranularity{if $gr == 'day'} active{/if}">
									{l s='Jour' mod='prestatillexportcompta'}
								</button>
								<button type="button" name="submitMonth" data-granularity="month" class="btn btn-default submitGranularity {if $gr == 'month'} active {/if}">
									{l s='Mois' mod='prestatillexportcompta'}
								</button>
								<button type="button" name="submitYear" data-granularity="year" class="btn btn-default submitGranularity {if $gr == 'year'} active {/if}">
									{l s='Année' mod='prestatillexportcompta'}
								</button>
								<!--
								<button type="submit" name="submitDateRealTime" class="hide btn btn-default submitDateRealTime " value="1">
									Temps Réel
								</button> -->
							</div>
						</div>
						<ul class="nav nav-pills pull-right">
							<li id="by_paiement" class="active">
								<a href="#dash_payement_mod" data-toggle="tab">
									{* <i class="icon-fire"></i> *} 
									<span class="hidden-inline-xs">{l s='par mode de paiement'  mod="prestatillexportcompta"}</span>
								</a>
							</li>
							<li id="by_vat">
								<a href="#dash_by_vat" data-toggle="tab">
									{* <i class="icon-fire"></i> *}
									<span class="hidden-inline-xs">{l s='par TVA'  mod="prestatillexportcompta"}</span>
								</a>
							</li>
							<li id="by_cat">
								<a href="#dash_by_cat" data-toggle="tab">
									{* <i class="icon-fire"></i> *}
									<span class="hidden-inline-xs">{l s='par rayon'  mod="prestatillexportcompta"}</span>
								</a>
							</li>
							<li id="by_hour">
								<a href="#dash_by_hour" data-toggle="tab">
									{* <i class="icon-fire"></i> *}
									<span class="hidden-inline-xs">{l s='par heure'  mod="prestatillexportcompta"}</span>
								</a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</nav>
				</div>
			</form>
			<div class="tab-content panel">
				<div class="tab-pane active" id="dash_payement_mod">
					<h3>
						{l s='Encaissements'  mod="prestatillexportcompta"} {if $date_from == $date_to} {l s='le'  mod="prestatillexportcompta"} {$date_from}{else}{l s='entre le'  mod="prestatillexportcompta"} {$date_from} {l s='et le'  mod="prestatillexportcompta"} {$date_to}{/if} {if $is_compared == 1}<span style="color:#00aff0;"> {l s='Comparé aux encaissements'  mod="prestatillexportcompta"} {if $compare.date_compare_from == $compare.date_compare_to} {l s='le'  mod="prestatillexportcompta"} {$compare.date_compare_from|date_format:"%d/%m/%Y"}{else}{l s='entre le'  mod="prestatillexportcompta"} {$compare.date_compare_from|date_format:"%d/%m/%Y"} {l s='et le'  mod="prestatillexportcompta"} {$compare.date_compare_to|date_format:"%d/%m/%Y"}{/if}</span> {/if}
						<span></span>
					</h3>
					{if !empty($totalCAbyPM)}
					<div class="table-responsive">
						<table class="table data_table" id="">
							<thead>
								<tr>
									<th></th>
									{foreach from=$totalCAbyPM['pm'] item=tot key=d}
										<th class="text-center" id="">{$d}<br />({$tot})</th>
									{/foreach}
								</tr>
							</thead>
							<tbody>
								{foreach from=$totalCAbyPM['date'] item=tot key=k}
								<tr>
									<td class="text-center datecol">{$k}</td>
										{foreach from=$tot item=detail key=d}
											<td class="text-center">{if $detail > 0}{Tools::displayPrice($detail, $currency)}{else}<span class="no-sales">{Tools::displayPrice($detail, $currency)}</span>{/if}</td>
										{/foreach}
									</tr>
								{/foreach}
								<tfoot>
									<tr class="sales_line">
										<td class="text-center">{l s='TOTAL' mod='prestatillexportcompta'}</td>
										{foreach from=$totalCAbyPM['total'] item=tot name=foo}
											<td class="text-center line" data-line="{$smarty.foreach.foo.iteration}" data-amount="{$tot}"><b>{Tools::displayPrice($tot, $currency)}</b></td>
										{/foreach}
									</tr>
									{if $is_compared == true}
										{if isset($totalCAbyPM['total_comp'])}
										<tr class="compare_line"> 
											<td class="text-center" style="background:#00aff0;color:#FFF;">{l s='Progression' mod='prestatillexportcompta'}<br />{l s='TOTAL N-1' mod='prestatillexportcompta'}</td>
											{foreach from=$totalCAbyPM['total_comp'] item=tot name=foo}
												<td class="text-center line"  data-line="{$smarty.foreach.foo.iteration}" data-amount="{$tot}"><b>{Tools::displayPrice($tot, $currency)}</b>
													<br />
												</td>
											{/foreach}
										</tr>
										{else}
										<tr class="compare_line"> 
											<td class="text-center" style="background:#00aff0;color:#FFF;">{l s='Progression' mod='prestatillexportcompta'}<br />{l s='TOTAL N-1' mod='prestatillexportcompta'}</td>
											{foreach from=$totalCAbyPM['total'] item=tot name=foo}
												<td class="text-center line"><b style="color:#72C279">{Tools::displayPrice(0, $currency)}</b><dd class="dash_trend dash_trend_up"><span>+∞</span></dd></td>
											{/foreach}
										</tr>
										{/if}
									{/if}
								</tfoot>
						</table>
					</div>
					{else}
						<div class="alert alert-info">
							{l s='Aucunes données pour l\'instant' mod='prestatillexportcompta'}
						</div>
					{/if}
				</div>
				
				<div class="tab-pane" id="dash_by_vat">
					<h3>
						{l s='Ventes à date'  mod="prestatillexportcompta"} {if $date_from == $date_to} {l s='le'  mod="prestatillexportcompta"} {$date_from}{else}{l s='entre le'  mod="prestatillexportcompta"} {$date_from} {l s='et le'  mod="prestatillexportcompta"} {$date_to}{/if} {if $is_compared == 1}<span style="color:#00aff0;"> {l s='Comparé aux ventes'  mod="prestatillexportcompta"} {if $compare.date_compare_from == $compare.date_compare_to} {l s='le'  mod="prestatillexportcompta"} {$compare.date_compare_from|date_format:"%d/%m/%Y"}{else}{l s='entre le'  mod="prestatillexportcompta"} {$compare.date_compare_from|date_format:"%d/%m/%Y"} {l s='et le'  mod="prestatillexportcompta"} {$compare.date_compare_to|date_format:"%d/%m/%Y"}{/if}</span> {/if}
						<span></span>
					</h3>
					{if !empty($totalCAbyVAT)}
					<div class="table-responsive">
						<table class="table data_table" id="">
							<thead>
								<tr>
									<th class="text-center"></th>
									<th class="text-center">{l s='Total TTC' mod='prestatillexportcompta'}</th>
									<th class="text-center">{l s='Total HT' mod='prestatillexportcompta'}</th>
									<th class="text-center">{l s='Total Remises TTC' mod='prestatillexportcompta'}</th>
									<th class="text-center">{l s='Total Remises HT' mod='prestatillexportcompta'}</th>
									{foreach from=$totalCAbyVAT['vat'] item=tot key=d}
										<th class="text-center" id="">{$d}<br />({$tot})</th>
									{/foreach}
								</tr>
							</thead>
							<tbody>
								{foreach from=$totalCAbyVAT['date'] item=tot key=k}
								<tr>
									<td class="text-center datecol">{$k}</td>
									<td class="text-center">{if $tot.total_ttc > 0}{Tools::displayPrice($tot.total_ttc, $currency)}{else}<span class="no-sales">{Tools::displayPrice($tot.total_ttc, $currency)}</span>{/if}</td>
									<td class="text-center">{if $tot.total_ht > 0}{Tools::displayPrice($tot.total_ht, $currency)}{else}<span class="no-sales">{Tools::displayPrice($tot.total_ht, $currency)}</span>{/if}</td>
									<td class="text-center">{if $tot.total_discounts_ttc > 0}{Tools::displayPrice($tot.total_discounts_ttc, $currency)}{else}<span class="no-sales">{Tools::displayPrice($tot.total_discounts_ttc, $currency)}</span>{/if}</td>
									<td class="text-center">{if $tot.total_discounts_ht > 0}{Tools::displayPrice($tot.total_discounts_ht, $currency)}{else}<span class="no-sales">{Tools::displayPrice($tot.total_discounts_ht, $currency)}</span>{/if}</td>
									{foreach from=$tot.vat item=detail key=d}
										<td class="text-center">{if $detail > 0}{Tools::displayPrice($detail.total_tax, $currency)}{else}<span class="no-sales">{Tools::displayPrice($detail, $currency)}</span>{/if}</td>
									{/foreach}
								</tr>
								{/foreach}
								<tfoot>
									<tr class="sales_line">
										<td class="text-center">{l s='TOTAL' mod='prestatillexportcompta'}</td>
										<td class="text-center line" data-line="11" data-amount="{$totalCAbyVAT.total.total_ttc}"><b>{Tools::displayPrice($totalCAbyVAT.total.total_ttc, $currency)}</b></td>
										<td class="text-center line" data-line="12" data-amount="{$totalCAbyVAT.total.total_ht}"><b>{Tools::displayPrice($totalCAbyVAT.total.total_ht, $currency)}</b></td>
										<td class="text-center line" data-line="13" data-amount="{$totalCAbyVAT.total.total_discounts_ttc}"><b>{Tools::displayPrice($totalCAbyVAT.total.total_discounts_ttc, $currency)}</b></td>
										<td class="text-center line" data-line="14" data-amount="{$totalCAbyVAT.total.total_discounts_ht}"><b>{Tools::displayPrice($totalCAbyVAT.total.total_discounts_ht, $currency)}</b></td>
										{foreach from=$totalCAbyVAT['total']['vat'] item=tot name=foo}
											<td class="text-center line"  data-line="{$smarty.foreach.foo.iteration}" data-amount="{$tot.total_tax}"><b>{Tools::displayPrice($tot.total_tax, $currency)}</b></td>
										{/foreach}
									</tr>
									{if $is_compared == true}
										{if isset($totalCAbyVAT['total_comp'])}
											<tr class="compare_line">
												<td class="text-center" style="background:#00aff0;color:#FFF;">{l s='Progression' mod='prestatillexportcompta'}<br />{l s='TOTAL N-1' mod='prestatillexportcompta'}</td>
												<td class="text-center line" data-line="11" data-amount="{$totalCAbyVAT.total_comp.total_ttc}"><b>{Tools::displayPrice($totalCAbyVAT.total_comp.total_ttc, $currency)}</b></td>
												<td class="text-center line" data-line="12" data-amount="{$totalCAbyVAT.total_comp.total_ht}"><b>{Tools::displayPrice($totalCAbyVAT.total_comp.total_ht, $currency)}</b></td>
												<td class="text-center line" data-line="13" data-amount="{$totalCAbyVAT.total_comp.total_discounts_ttc}"><b>{Tools::displayPrice($totalCAbyVAT.total_comp.total_discounts_ttc, $currency)}</b></td>
												<td class="text-center line" data-line="14" data-amount="{$totalCAbyVAT.total_comp.total_discounts_ht}"><b>{Tools::displayPrice($totalCAbyVAT.total_comp.total_discounts_ht, $currency)}</b></td>
												{foreach from=$totalCAbyVAT['total_comp']['vat'] item=tot name=foo}
													<td class="text-center line" data-line="{$smarty.foreach.foo.iteration}" data-amount="{$tot.total_tax}"><b>{Tools::displayPrice($tot.total_tax, $currency)}</b></td>
												{/foreach}
											</tr>
										{else}
										<tr class="compare_line"> 
											<td class="text-center" style="background:#00aff0;color:#FFF;">{l s='Progression' mod='prestatillexportcompta'}<br />{l s='TOTAL N-1' mod='prestatillexportcompta'}</td>
											<td class="text-center line"><b style="color:#72C279">{Tools::displayPrice(0, $currency)}</b><dd class="dash_trend dash_trend_up"><span>+∞</span></dd></td>
											<td class="text-center line"><b style="color:#72C279">{Tools::displayPrice(0, $currency)}</b><dd class="dash_trend dash_trend_up"><span>+∞</span></dd></td>
											<td class="text-center line"><b style="color:#72C279">{Tools::displayPrice(0, $currency)}</b><dd class="dash_trend dash_trend_up"><span>+∞</span></dd></td>
											<td class="text-center line"><b style="color:#72C279">{Tools::displayPrice(0, $currency)}</b><dd class="dash_trend dash_trend_up"><span>+∞</span></dd></td>
											{foreach from=$totalCAbyVAT['total']['vat'] item=tot name=foo}
												<td class="text-center line"><b style="color:#72C279">{Tools::displayPrice(0, $currency)}</b><dd class="dash_trend dash_trend_up"><span>+∞</span></dd></td>
											{/foreach}
										</tr>
										{/if}
									{/if}
								</tfoot>
						</table>
					</div>
					{else}
						<div class="alert alert-info">
							{l s='Aucunes données pour l\'instant' mod='prestatillexportcompta'}
						</div>
					{/if}
				</div>
				
				<div class="tab-pane" id="dash_by_cat">
					<h3>
						{l s='Ventes'  mod="prestatillexportcompta"} {if $date_from == $date_to} {l s='le'  mod="prestatillexportcompta"} {$date_from}{else} {l s='entre le'  mod="prestatillexportcompta"} {$date_from} {l s='et le'  mod="prestatillexportcompta"} {$date_to}{/if}
						<span></span>
					</h3>
					{if !empty($totalCAbyCAT)}
					<div class="table-responsive">
						<table class="table data_table" id="">
							<thead>
								<tr>
									<th class="text-center"></th>
									<th class="text-center">{l s='Nbre produits vendus' mod='prestatillexportcompta'}</th>
									<th class="text-center">{l s='Total HT' mod='prestatillexportcompta'}</th>
								</tr>
							</thead>
							<tbody>
								{foreach from=$totalCAbyCAT item=tot key=k}
								<tr>
									<td class="text-left datecol"><i class="icon-folder-open"></i> {$tot.name}</td>
									<td class="text-center">{$tot.totalQuantitySold}</td>
									<td class="text-center">{$tot.totalPriceSold}</td> 
								</tr>
								{/foreach}
						</table><br />
						<div class="alert alert-info">
								{l s='Attention : un produit pouvant être dans plusieurs catégories, le nombre et produits vendus et les montants peuvent être supérieurs au CA total sur la période.' mod='prestatillexportcompta'}
						</div>
					</div>
					{else}
						<div class="alert alert-info">
							{l s='Aucunes données pour l\'instant' mod='prestatillexportcompta'}
						</div>
					{/if}
				</div>
				
				<div class="tab-pane" id="dash_by_hour">
					<h3>
						{l s='Ventes'  mod="prestatillexportcompta"} {if $date_from == $date_to} {l s='le'  mod="prestatillexportcompta"} {$date_from}{else} {l s='entre le'  mod="prestatillexportcompta"} {$date_from} {l s='et le'  mod="prestatillexportcompta"} {$date_to}{/if}
						<span></span>
					</h3>
					{if !empty($totalCAbyHour)}
					<div class="table-responsive">
						<table class="table data_table" id="">
							<thead>
								<tr>
									<th class="text-center"></th>
									{for $i=0 to 23}
										<th class="text-left">{$i}h</th>
									{/for}
								</tr>
							</thead>
							<tbody>
								{foreach from=$totalCAbyHour['date'] item=tot key=k}
								<tr>
									<td class="text-left datecol">{$k}</td>
									{foreach from=$tot item=details}
										<td class="text-center">{if $details > 0}{Tools::displayPrice($details, $currency)}{else}<span class="no-sales">{Tools::displayPrice($details, $currency)}</span>{/if}</td>
									{/foreach}
								</tr>
								{/foreach}
								<tfoot>
									<tr>
										<td class="text-center">{l s='TOTAL HT' mod='prestatillexportcompta'}</td>
										{foreach from=$totalCAbyHour['total_ht'] item=tot}
											<td class="text-center"><b>{if $tot > 0}<span class="badge badge-success">{Tools::displayPrice($tot, $currency)}</span>{else}{Tools::displayPrice($tot, $currency)}{/if}</b></td>
										{/foreach}
									</tr>
								</tfoot>
						</table>
					</div>
					{else}
						<div class="alert alert-info">
							{l s='Aucunes données pour l\'instant' mod='prestatillexportcompta'}
						</div>
					{/if}
				</div>
				
			</div>
		</section>
				
	</section>
