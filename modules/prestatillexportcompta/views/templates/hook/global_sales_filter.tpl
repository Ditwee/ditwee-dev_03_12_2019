{*
<div class="panel">
  <h3><i class="icon-cogs"></i> Filtres <span style="color:red">EN COURS DE DEV</span></h3>
  <div class="filter-stock-extended">
  *}
  
  <!-- Module content -->
  <div id="modulecontent" class="clearfix">
  
    <form class="form-horizontal">
      
      
      <!-- Nav tabs -->
    <div class="col-lg-2">
      <div class="list-group">
        <a href="#panel-filters" class="menu_tab list-group-item active" data-toggle="tab"><i class="icon-filter"></i> {l s='Filtres' mod='prestatillexportcompta'}</a>
        <a href="#panel-show-hide" class="menu_tab list-group-item " data-toggle="tab"><i class="icon-eye"></i> {l s='Affichage' mod='prestatillexportcompta'}</a>
      </div>
    </div>
      
    <!-- Tab panes -->
    <div class="tab-content col-lg-10">
      <div class="tab-pane active panel" id="panel-filters">


          <h3><i class="icon-filter"></i> Filtres</h3>
      
          <div class="row">
            <div class="col-md-6">        
            
                <input type="hidden" name="controller" value="AdminPrestatillGlobalSales">
                <input type="hidden" name="token" value="{$token}">
                
                <div class="form-group">
                  <label class="control-label col-lg-3" for="date_from">
                      <span class="label-tooltip" data-toggle="tooltip"
                      title="{l s='AAAA-MMM-JJ'}">
                      {l s='Date de début : ' mod='prestatillexportcompta'}
                    </span>
                  </label>
                  <div class="col-lg-9">          
                    <div class="col-lg-3">
                      <div class="input-group">
                        <input class="datepicker" form="form-order_detail" type="text" id="date_from"  name="order_detailFilter_date_from" value="{$date_from}" style="text-align: center" id="date_from" />
                        <span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
                
                
                <div class="form-group">
                  <label class="control-label col-lg-3" for="date_to">
                      <span class="label-tooltip" data-toggle="tooltip"
                      title="{l s='AAAA-MM-JJ'}">
                      {l s='Date de fin : ' mod='prestatillexportcompta'}
                    </span>
                  </label>
                  <div class="col-lg-9">          
                    <div class="col-lg-3">
                      <div class="input-group">
                        <input class="datepicker " form="form-order_detail" type="text" id="date_to"  name="order_detailFilter_date_to" value="{$date_to}" style="text-align: center" id="date_to" />
                        <span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
                      </div>
                    </div>
                  </div>
                </div>      
                
                <div class="form-group">
                  <label for="id_warehouse" class="control-label col-lg-3">Grouper les lignes</label>
                  <div class="col-lg-9">
                    <select id="group_line" name="order_detailFilter_group_line" form="form-order_detail">
                      <option value="attribute"{if !isset($group_line) || $group_line == 'attribute'} selected{/if}>Par produit</option>
                      {*<option value="product"{if isset($group_line) && $group_line == 'product'} selected{/if}>Par produit</option>*}
                      <option value="false"{if isset($group_line) && $group_line == 'false'} selected{/if}>Par vente</option>
                    </select>
                  </div>
                </div>
            
            
            
            </div>
            <div class="col-md-6">
                <div class="form-group">
                  <label for="id_pointshop_address" class="control-label col-lg-4">Filtrer par point de vente</label>
                  <div class="col-lg-8">
                    <select id="id_pointshop_address" name="order_detailFilter_id_pointshop_address" form="form-order_detail">
                      <option value="-1" {if isset($id_pointshop_address) && $id_pointshop_address == -1} selected{/if}>Tous les points de vente</option>
                      <option value="0"{if isset($id_pointshop_address) && $id_pointshop_address == 0} selected{/if}>Vente en ligne</option>
                      {foreach from=$addresses item=address}
                        <option value="{$address->id}"{if isset($id_pointshop_address) && $id_pointshop_address == $address->id} selected{/if}>{$address->alias}</option>
                      {/foreach}
                    </select>
                  </div>
                </div>
                
                {*
                <div class="form-group" id="group_by_pointshop_block">
                  <label for="id_pointshop" class="control-label col-lg-4">Grouper par point de vente</label>
                  <div class="col-lg-8">
                    <select id="group_pointshop" name="order_detailFilter_group_pointshop" form="form-order_detail">
                      <option value="0" {if isset($group_pointshop) && $group_pointshop == 0} selected{/if}>Non</option>
                      <option value="1" {if isset($group_pointshop) && $group_pointshop == 1} selected{/if}>Oui</option>
                      </select>
                  </div>
                </div>
                *}
                
                <div class="form-group">
                  <label for="id_supplier" class="control-label col-lg-4">Filtrer par Fournisseur</label>
                  <div class="col-lg-8">
                    <select id="id_supplier" name="order_detailFilter_id_supplier" form="form-order_detail">
                      <option value="-1">Tous les fournisseurs</option>
                      {foreach from=Supplier::getSuppliers(true,0,false) item=supplier}
                        <option value="{$supplier['id_supplier']}"{if isset($id_supplier) && $id_supplier == $supplier['id_supplier']} selected{/if}>{$supplier['name']} | {$supplier['nb_products']} produit(s)</option>
                      {/foreach}
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="id_manufacturer" class="control-label col-lg-4">Filtrer par fabricant</label>
                  <div class="col-lg-8">
                    <select id="id_manufacturer" name="order_detailFilter_id_manufacturer" form="form-order_detail">
                      <option value="-1">Tous les Fabricants</option>
                      {foreach from=Manufacturer::getManufacturers(true,0,false) item=manufacturer}
                        <option value="{$manufacturer['id_manufacturer']}"{if isset($id_manufacturer) && $id_manufacturer == $manufacturer['id_manufacturer']} selected{/if}>{$manufacturer['name']} | {$manufacturer['nb_products']} produit(s)</option>
                      {/foreach}
                    </select>
                  </div>
                </div>
            
            </div>
            
            
          </div>
            
          
         
          
                
          
      
         <div class="row">
            <div class="col-md-12">            
               <div class="form-group">
                  <div class="col-md-12">
                    <input type="submit" name="order_detailFilter_Filtrer" value="Filtrer" form="form-order_detail" />
                  </div>
                </div>
            </div>
         </div>
      
      
      </div>
    </div>
    <div class="tab-content col-lg-10">
      <div class="tab-pane panel" id="panel-show-hide">
      
         <h3><i class="icon-eye"></i> Affichage</h3>
      
         <div class="row">
         
            <div class="col-md-4">
            
            
               {include file="./global_sales_filters_view_switcher.tpl" checked=true name='id_product' label="ID Produit"}
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='id_product_attribute' label="ID Déclinaison"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='pointshop' label="Point de vente"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='order_date' label="Date de cmd."} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='order_reference' label="Référence de commande"} 
               
               
            </div>   
            <div class="col-md-4">
            
               
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='product_name' label="Nom du produit"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='quantity' label="Quantitée vendue"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='quantity_refund' label="Quantitée remboursée"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='refund_rate' label="Taux de retour"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='reference' label="Référence produit"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='supplier_reference' label="Référence fournisseur"} 
            
            
                
            
            </div>
            
            <div class="col-md-4">
            
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='unit_price_ttc' label="Prix unitaire moyen TTC"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='unit_price_ht' label="Prix unitaire moyen HT"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='original_price_ht' label="Prix original unitaire HT"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='supplier_price_ht' label="Prix d'achat HT"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='marge' label="Marge"} 
               {include file="./global_sales_filters_view_switcher.tpl" checked=false name='taux_de_marque' label="Taux de marque"} 
            
            
            </div>
            
         
         </div>
      
         <div class="row">
            <div class="col-md-12">            
               <div class="form-group">
                  <div class="col-md-12">
                    <input type="submit" name="order_detailFilter_Filtrer" value="Enregistrer" form="form-order_detail" />
                  </div>
                </div>
            </div>
         </div>
                  
          
      </div>
    </div>  
      
    </form>
    </div>
    {*
  </div>
</div>
*}
<script type="text/javascript">

$(function(){

  $('#id_pointshop').change(function(){
    if($(this).val() >= 0)
    {
      $('#group_pointshop').prop('disabled',true);
      $('#group_by_pointshop_block').slideUp();
    }
    else
    {
      $('#group_pointshop').prop('disabled',false);      
      $('#group_by_pointshop_block').slideDown();
    }
        
  });
    
    

  if($('#id_pointshop').val() >= 0)
  {
    $('#group_pointshop').prop('disabled',true);
    $('#group_by_pointshop_block').hide();
  }


  $(".datepicker").datepicker({
    prevText: '',
    nextText: '',
    dateFormat: 'yy-mm-dd'
  });



  /**
   * Handle menu tab changement
   */
  $('.menu_tab').click(function(){
      $('.menu_tab').removeClass('active');
       $('.tab-pane').removeClass('active');
    $(this).addClass('active');
  });

});
  
</script>
