<div class="form-group">                          
    <label class="control-label col-lg-6">{$label}</label>
    <div class="col-lg-3">
        <span class="switch prestashop-switch fixed-width-lg">
        <input form="form-order_detail" type="radio" name="order_detailFilter_view_{$name}" id="order_detailFilter_view_{$name}_on" value="1"{if $cookie->__get("prestatillglobalsalesorder_detailFilter_view_$name") != 0} checked="checked"{/if}>
        <label for="order_detailFilter_view_{$name}_on">Voir</label>
        <input form="form-order_detail" type="radio" name="order_detailFilter_view_{$name}" id="order_detailFilter_view_{$name}_off" value="0"{if $cookie->__get("prestatillglobalsalesorder_detailFilter_view_$name") == 0} checked="checked"{/if}>
        <label for="order_detailFilter_view_{$name}_off">Masquer</label>
        <a class="slide-button btn"></a>
        </span>       
    </div>              
</div>