<div id="prestatill_advanced_stats">
	<div class="tab-pane" id="dash_payement_mod">
		<h3>
			{l s='Encaissements du'  mod='prestatillexportcompta'} {$date_from|date_format:'%d/%m/%Y'} (Service n°{$id_service}) <span>{l s='en TTC' mod='prestatillexportcompta'}</span>
		</h3>
		
		{if !empty($report_pm)}
		<div class="table-responsive">
			<table class="table data_table" id="">
				<thead>
					<tr>
						{foreach from=$report_pm['nbr'][$id_service] item=tot key=d}
							<th class="text-center" id="">{$d}<br />({$tot})</th>
						{/foreach}
					</tr>
				</thead>
				<tbody>
					<tr class="sales_line">
						{foreach from=$report_pm['nbr'][$id_service] item=tot key=d}
							<td class="text-center line"><b>{Tools::displayPrice($report_pm['detail'][$id_service][$d], $currency)}</b></td>
						{/foreach}
						
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td class="h3" colspan="{count($report_pm['nbr'][$id_service])}">{l s='Total des encaissements du jour'  mod='prestatillexportcompta'} <span>{l s='en TTC' mod='prestatillexportcompta'}</span></td>
					</tr>
					<tr>
						<td class="text-center line" colspan="{count($report_pm['nbr'][$id_service])}"><b>{Tools::displayPrice($report_pm['total'][$id_service], $currency)}</b></td>
					</tr>
				</tfoot>
			</table>
		</div>
		{else}
			<h3>
				{l s='Aucunes ventes pour le moment' mod='prestatillexportcompta'}
			</h3>
		{/if}
	</div>
</div>