<?php

class Product extends ProductCore
{
    /**
     * added in module prestatillgiftcard 1.0
     */
    public $prestatillgiftcard_amount = 0;
    public $prestatillgiftcard_minimum_amount = 0;
    public $prestatillgiftcard_partial_usage = 1;
    public $prestatillgiftcard_validity_period = '+1 year';

    public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, \Context $context = null)
    {
    	//added in module prestatillgiftcard 1.0
        self::$definition['fields']['prestatillgiftcard_validity_period'] = array(
            'type' => self::TYPE_STRING,
            'validate' => 'isString'
        );
        self::$definition['fields']['prestatillgiftcard_amount'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );
        self::$definition['fields']['prestatillgiftcard_partial_usage'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );
        self::$definition['fields']['prestatillgiftcard_minimum_amount'] = array(
            'type' => self::TYPE_INT,
            'validate' => 'isInt'
        );
        
        parent::__construct($id_product, $full, $id_lang, $id_shop, $context);        
    }
}