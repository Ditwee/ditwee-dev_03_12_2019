<?php
class PrestatillGiftCardGetGiftCardModuleFrontController extends ModuleFrontController
{
	public function initContent() {
		$secret_tocken = PrestaTillGiftCard::SECRET_TOCKEN;
		require_once(__DIR__.'/../../classes/PrestaTillGiftCardCode.php');
		require_once(__DIR__.'/../../classes/PrestatillGiftCardCodeUsage.php');
		$tocken = Tools::getValue('tocken');
		
		if(Tools::getIsset('action'))
        {
            $action='_ajax'.Tools::getValue('action');
            return $this->{$action}($_POST);
        }
		
		if($secret_tocken == $tocken)
		{
			//require_once(__DIR__.'/../../classes/PrestatillGiftCardUsage.php');
			
			//recherche d'un avoir selon son code ean13
			if(isset($_POST['code128']))
			{
				
				$giftcard = PrestatillGiftCardCode::foundGiftCardByCode($_POST['code128']);
				if(Validate::isLoadedObject($giftcard))
				{
					$ids = array();
					$slips = array();
					$ids[] = $giftcard->id;
					$giftcards[$giftcard->id] = $giftcard;
											
					//on recherche les utilisations des slips
					$usages = PrestatillGiftCardCodeUsage::getFromIdGiftCardCode($ids);
					
					require_once(_PS_MODULE_DIR_.'acaisse/classes/ACaissePointshop.php');
					
					header("Access-Control-Allow-Origin: *");
					header("Content-type:application/json");
					echo(json_encode(array(
						'success' => true,
						'giftcards' => $giftcards,
						'customer' => new Customer($giftcard->from_id_customer),
						//'pointshop' => new ACaissePointshop($giftcard->id_pointshop),	
						'usages' => $usages
					)));
					
				}
				else
				{
					//code ean13 non valdid (vérification du bite de controle 13ème digit)
					header("Access-Control-Allow-Origin: *");
					header("Content-type:application/json");
					echo(json_encode(array(
						'success' => false,
						'msg' => 'Aucun chèque cadeau valide correspondant à ce code'	
					)));							
				}
			}
			
			//recherche de tous les avoirs d'un clients
			if(isset($_POST['id_customer']))
			{			
				$id_customer = (int)Tools::getValue('id_customer');				
				$giftcards = PrestaTillGiftCardCode::getByBuyer($id_customer);			
				
				if(!empty($giftcards))
				{
					$ids = array();
					foreach($giftcards as $i => $giftcard)
					{
						$ids[] = $giftcard->id;
					}
					
					//on recherche les utilisations des slips
					$usages = PrestaTillGiftCardCodeUsage::getFromIdGiftCardCode($ids);		
					
					header("Access-Control-Allow-Origin: *");
					header("Content-type:application/json");
					echo(json_encode(array(
						'success' => true,
						'giftcards' => $giftcards,
						'usages' => $usages
					)));
				}
				else
				{
					header("Access-Control-Allow-Origin: *");
					header("Content-type:application/json");
					echo(json_encode(array(
						'success' => false,
						'msg' => 'Aucun chèque cadeau associé à ce client'			
					)));
				}
			}
			return;			
		}
		else
		{
			header("Access-Control-Allow-Origin: *");
			header("Content-type:application/json");
			echo(json_encode(array(
				'success' => false,
				'msg' => 'Impossible de récupérer la liste des chèques cadeaux depuis le serveur'						
			)));
			return;				
		}		
	}
	
	public function view() {
		return;
	}
	
	public function display()
    {
    	return true;
	}
	
	
	static private function add_ean13_check_digit($digits12){
		//first change digits to a string so that we can access individual numbers
		$digits =(string)$digits12;
		// 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;
		// 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;
		// 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
		$next_ten = (ceil($total_sum/10))*10;
		$check_digit = $next_ten - $total_sum;
		
		//p('$check_digit : '.$check_digit);
		//p($digits12 . $check_digit);
		return $digits12 . $check_digit;
	}
	
	private function _ajaxHasGiftCard($params)
	{
		//UPDATE : on ajoute un bouton pour les chèques cadeau
		$gift_cards = array();
		$has_gift_card = PrestaTillGiftCardCode::getGiftCartFromOrder((int)$params['id_order']);
		if(!empty($has_gift_card))
		{
			foreach ($has_gift_card as $key => $gift) 
			{
				$gift_cards[] = $gift['id_prestatill_gift_card_code'];
			}	
		}
			
		$response = array(
            'action' => 'HasGiftCard',
            'gift_cards' => $gift_cards,
            'success' =>true
        );

        return $this->_returnJSON($response);
        exit();	
	}
	
	private function _ajaxLoadGiftCardsTickets($params) {
        $error='';
        
		$ids = explode(',', $params['id_gift_cards']);
		
        if (empty($ids)) {
            $error = 'Erreur : coommande associée introuvable';
        } else {
            	
			$id_order = $params['id_order'];
		
			$giftcards = PrestatillGiftCardCode::getAllByIdOrder($id_order);
			$html = array();
			//on va boucler sur l'ensemble des cheque cadeau de cette commande
			foreach($giftcards as $giftcard)
			{
				$tpl = 	$this->context->smarty->createTemplate(_PS_MODULE_DIR_.''.$this->module->name.'/views/templates/hook/gift-card-ticket.tpl');
				
				if(!class_exists('ACaissePointshop'))
				{
					require_once(_PS_MODULE_DIR_.'acaisse/core/classes/ACaissePointshop.php');
				}
				
				$pointshop = new ACaissePointshopCore($giftcard->id_pointshop);
				$address = new Address($pointshop->id_address);  		
				
				if(!class_exists('Picqer\Barcode\BarcodeGeneratorPNG'))
				{
					require_once(dirname(__FILE__).'/../../vendor/php-barcode-generator/include.php');
				}
				//PrestaTillGiftCardCode::foundGiftCardByCode($this->code128);
				$generator_png = new Picqer\Barcode\BarcodeGeneratorPNG();
				
				$addr_header = $address->kdo_header;		
				
				// On créé les datas
				$datas_header = array(		
					//logo
					'{{ LOGO }}' => '<img class="logo" src="data:image/png;base64,'.base64_encode(file_get_contents(_PS_MODULE_DIR_.'acaisse/uploads/ticket_logo/'.$address->common_picture_path)).'" style="width: 90%;">',
		            
					//address	
					'{{ COMPANY }}' => $address->company,
					'{{ SHOP_STREET }}' => $address->address1.'<br>'.($address->address2 ==''?'':$address->address2),		
					'{{ SHOP_CP }}' => $address->postcode,
					'{{ SHOP_CITY }}' => $address->city,
					'{{ SHOP_COUNTRY }}' => $address->country,
					'{{ SHOP_PHONE }}' => $address->phone,
					'{{ SIRET }}' => $address->dni,
					'{{ TVA }}' => $address->vat_number,	
					
				);		
						
				$addr_header = str_replace(array_keys($datas_header), array_values($datas_header), $addr_header);
				
				
				// On créé les datas
				$datas_footer = array(		
					
					//customer
					/*
					'{{ CUSTOMER_ID }}' => $customer?$customer->id:'',
					'{{ CUSTOMER_LASTNAME }}' => $customer?$customer->lastname:'',
					'{{ CUSTOMER_FIRSTNAME }}' => $customer?$customer->firstname:'',
					'{{ CUSTOMER_EMAIL }}' => $customer?$customer->email:'',			
					
					'{{ CUSTOMER_COMPANY }}' => $customer?$customer->company:'',
					'{{ CUSTOMER_SIRET }}' => $customer?$customer->siret:'',
					'{{ CUSTOMER_APE }}' => $customer?$customer->ape:'',	
					'{{ CUSTOMER_EAN }}' => $customer_ean,
					'{{ CUSTOMER_EAN_CODE }}' => $display_customer_ean,		*/
					
				);	
				
				//$addr_footer = str_replace(array_keys($datas_footer), array_values($datas_footer), $addr_footer);
				
				$base64_png = $generator_png->getBarcode($giftcard->code128,$generator_png::TYPE_CODE_128,10,160);
				//$this->context->smarty->assign('barcode_png', base64_encode($base64_png));
				//$this->context->smarty->assign('display_end_date_validity',date('d/m/Y',strtotime($this->last_day_validity)));		
				
				$tpl->assign(
					array(
						'giftcard' => $giftcard,
						'currency' => '€',
						'barcode_png' => base64_encode($base64_png),
						'display_end_date_validity' => date('d/m/Y',strtotime($giftcard->last_day_validity)),
						'pointshop' => $pointshop,
						'TICKET_HEADER' => $addr_header,
						//'TICKET_FOOTER' => $addr_footer,
					)
				);
				
				$html[] = $tpl->fetch();
			}		
            	
            if (!empty($html)) {
            	//getNumTickets($('#printTickets .keyboard span.display').html());
            	
                $response = array(
                    'success'=>true,
            		'html' => $html,
                    'error'=>'',
                    'response'=>array(
                        'html'=>$html
                    )
                );
                $this->_returnJSON($response);
            } else {
                $error = 'Erreur : impossible de recharger la commande';
            }
        }
        
        $response = array(
            'success'=>false,
            'error'=>$error,
            'response'=>array()
        );
        $this->_returnJSON($response);
    }
	
	private function _returnJSON($response)
    {
        if(is_array($response))
        {
            $response=json_encode($response);
        }

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        echo $response;
        die();
    }
	
}