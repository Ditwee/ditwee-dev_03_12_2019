<?php
/**
 * A Caisse : Point de vente
 * 
 * Model Caisse
 */
 
class PrestaTillGiftCardCodeCore extends ObjectModel
{     	 
    public $id_prestatill_gift_card_code;
	public $code128;
	public $name;
	public $description;
	public $short_description;
    public $amount;
    public $partial_usage = 1;
	public $from_id_customer = 0;
	public $id_order;
	public $id_product;
	public $id_product_attribute = 0;
	public $num = 0;
	public $id_pointshop = 0;
	
    public $minimum_amount = 0;
    public $period_validity;
    public $last_day_validity;
    public $amount_used;			//solde debiter du chèque
    public $real_amount_used;		//solde reelement utiliser pour payer un panier (en cas de dépassement)
    public $amount_available;		//solde restant
    
    public $date_add;
    public $date_upd;
	
	/* SINCE 2.4.1 */
	public $id_financial_report = 0;
	public $id_service = 1;
	public $day = '0000-00-00';
	
    
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'prestatill_gift_card_code',
        'primary' => 'id_prestatill_gift_card_code',
        'multilang' => false,
        'fields' => array(
            'code128' => array('type' => self::TYPE_STRING, 'required' => false),
            'amount' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
            'partial_usage' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'from_id_customer' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            
            'id_order' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'id_pointshop' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'id_product' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'id_product_attribute' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'num' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),			
			
            'minimum_amount' =>   array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
            'period_validity' => array('type' => self::TYPE_STRING, 'required' => false),
            'name' => array('type' => self::TYPE_STRING, 'required' => false),
            'description' => array('type' => self::TYPE_STRING, 'required' => false),
            'short_description' => array('type' => self::TYPE_STRING, 'required' => false),
            'last_day_validity' =>   array('type' => self::TYPE_DATE, /*'validate' => 'isGenericName',*/ 'required' => true/*, 'size' => 150*/),
            'amount_used' =>   array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
            'real_amount_used' =>   array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
            'amount_available' =>   array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
            'date_add' =>   array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>   array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            
			/* SINCE 2.4.1 */
            'id_service' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'id_financial_report' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'day' =>   array('type' => self::TYPE_STRING),
        ),
    );
	
	public static function getAllByIdOrder($id_order)
	{
		$sql = 'SELECT * FROM '._DB_PREFIX_.self::$definition['table'].' WHERE id_order = '.((int)$id_order).'';
		
		$results = DB::getInstance()->executeS($sql);
		
		$giftcards = array();
		
		foreach($results as $result)
		{
			$giftcards[] = new PrestaTillGiftCardCode($result['id_prestatill_gift_card_code']);
		}
		
		return $giftcards;
		
	}

	public function generatePrintableGiftCardTicket($tpl, $smarty)
	{
		//font qu'on assigne les truc à smarty
		$order = new Order($this->id_order);
		
		if(!class_exists('ACaissePointshop'))
		{
			require_once(_PS_MODULE_DIR_.'acaisse/core/classes/ACaissePointshop.php');
		}
		
		$pointshop = new ACaissePointshopCore($this->id_pointshop);
		$address = new Address($pointshop->id_address);  
		
		//@TODO ici prévoir un substitute si commande en ligne (front)		
		$smarty->assign('pointshop',new ACaissePointshop($order->id_pointshop));
		$smarty->assign('giftcard',$this);
		$smarty->assign('currency','€');
		
		
		if(!class_exists('Picqer\Barcode\BarcodeGeneratorPNG'))
		{
			require_once(dirname(__FILE__).'/../../vendor/php-barcode-generator/include.php');
		}
		//PrestaTillGiftCardCode::foundGiftCardByCode($this->code128);
		$generator_png = new Picqer\Barcode\BarcodeGeneratorPNG();
		
		$addr_header = $address->kdo_header;		
				
		// On créé les datas
		$datas_header = array(		
			//logo
			'{{ LOGO }}' => '<img class="logo" src="data:image/png;base64,'.base64_encode(file_get_contents(_PS_MODULE_DIR_.'acaisse/uploads/ticket_logo/'.$address->common_picture_path)).'" style="width: 90%;">',
            
			//address	
			'{{ COMPANY }}' => $address->company,
			'{{ SHOP_STREET }}' => $address->address1.'<br>'.($address->address2 ==''?'':$address->address2),		
			'{{ SHOP_CP }}' => $address->postcode,
			'{{ SHOP_CITY }}' => $address->city,
			'{{ SHOP_COUNTRY }}' => $address->country,
			'{{ SHOP_PHONE }}' => $address->phone,
			'{{ SIRET }}' => $address->dni,
			'{{ TVA }}' => $address->vat_number,	
			
		);		
				
		$addr_header = str_replace(array_keys($datas_header), array_values($datas_header), $addr_header);
		
		
		
		$base64_png = $generator_png->getBarcode($this->code128,$generator_png::TYPE_CODE_128,10,160);
		$smarty->assign('barcode_png', base64_encode($base64_png));
		$smarty->assign('display_end_date_validity',date('d/m/Y',strtotime($this->last_day_validity)));	
		 
		$smarty->assign('TICKET_HEADER', $addr_header);	
		
		return $tpl->fetch();
	}

	
	public static function getUniqueCode128()
	{
		$code = self::randomString(10);
		//on test si le code existe
		while(false !== self::foundGiftCardByCode($code))
		{
			$code = self::randomString(10);
		}
		
		return $code;
	}
	
	public static function foundGiftCardByCode($code128)
	{
		$sql = 'SELECT * FROM '._DB_PREFIX_.self::$definition['table'].' WHERE code128 = \''.pSQL($code128).'\'';
		
		$result = DB::getInstance()->executeS($sql);
		
		if(empty($result))
		{
			return false;
		}
		else
		{
			return new 	PrestaTillGiftCardCode($result[0]['id_prestatill_gift_card_code']);
		}
	}
	
	public static function getByBuyer($id_customer)
	{
		$sql = 'SELECT * FROM '._DB_PREFIX_.self::$definition['table'].'
			WHERE from_id_customer = '.(int)($id_customer);//.'
			//AND last_day_validity >= \''.date('Y-m-d').'\'';
		$results = DB::getInstance()->executeS($sql);
		$list = array();
		
		if(empty($results))
		{
			return false;
		}
		else
		{
			foreach($results as $result)
			{
				$list[$result['id_prestatill_gift_card_code']] = new PrestaTillGiftCardCode($result['id_prestatill_gift_card_code']);
			}
			return $list;
		}	
	}
	
	
	public static function randomString($length = 10, $flag = 'ALPHANUMERIC', $only_uppercase = true)
    {
        switch ($flag) {
            case 'NUMERIC':
                $str = '0123456789';
                break;
            case 'NO_NUMERIC':
                $str = ($only_uppercase===true?'':'abcdefghjkmnpqrstuwxyz').'ABCDEFGHJKMNPQRSTUWXYZ';
                break;
            default:
                $str = ($only_uppercase===true?'':'abcdefghjkmnpqrstuwxyz').'23456789ABCDEFGHJKMNPQRSTUWXYZ';
                break;
        }
        
        for ($i = 0, $passwd = ''; $i < $length; $i ++)
            $passwd .= Tools::substr($str, mt_rand(0, Tools::strlen($str) - 1), 1);
        
        return $passwd;
    }
	
	/*
	 * RECUPRER LA LISTE DES BONS CADEAU DU JOUR
	 * POUR LE RAPPORT FINANCIER
	 */
	public static function getList($day,$id_pointshop = 0)
	{	
		$query = 'SELECT * 
					FROM `'._DB_PREFIX_.'prestatill_gift_card_code` 
					WHERE day LIKE \''.$day.'%\'
					AND id_pointshop = '.$id_pointshop;

		$result = Db::getInstance()->executeS($query);

		return $result;
	}
	
	/*
	 * MISE A JOUR D'ORDER DETAIL
	 */
	// On met à jour le champs is_giftcard sur orderDetail
	public static function updateOrderDetail($id_order = 0, $id_product = 0)
	{
		if($id_order > 0 && $id_product > 0)
		{
			// On recherche l'id_order_detail
			$query = 'SELECT id_order_detail 
						FROM `'._DB_PREFIX_.'order_detail` 
						WHERE id_order = '.$id_order.'
						AND product_id = '.$id_product;	
						
			$result = Db::getInstance()->getRow($query);		
		
			if(!empty($result))
			{
				$query = 'UPDATE `'._DB_PREFIX_.'order_detail` 
						SET `is_giftcard` = 1
						WHERE `id_order_detail` = '.$result['id_order_detail'];
						
				$result = Db::getInstance()->execute($query);		
			}
		}	
	}
	 
	/*
	 * RECUPRER LA LISTE DES BONS CADEAU D'UNE COMMANDE
	 */
	public static function getGiftCartFromOrder($id_order = 0)
	{	
		$query = 'SELECT id_prestatill_gift_card_code 
					FROM `'._DB_PREFIX_.'prestatill_gift_card_code` 
					WHERE id_order = '.$id_order;

		$result = Db::getInstance()->executeS($query);
		return $result;
	}
    
}