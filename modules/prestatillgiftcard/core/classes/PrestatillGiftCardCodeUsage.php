<?php

class PrestatillGiftCardCodeUsageCore extends ObjectModel
{
	public $id_prestatill_gift_card_code_usage;
    public $id_prestatill_gift_card_code; 
    public $id_employee;
    public $amount;
	public $use_for_id_ticket = 0;
	public $use_for_id_order = 0;
	public $use_for_id_customer = 0;
	public $use_for_id_pointshop = 0;
	public $use_for_id_service = 0;
	public $date_add;
	public $date_upd;
	
	/* SINCE 2.4.1 */
	public $id_financial_report = 0;
	public $day = '0000-00-00';

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'prestatill_gift_card_code_usage',
        'primary' => 'id_prestatill_gift_card_code_usage',
        'multilang' => false,
        'fields' => array(
            'id_prestatill_gift_card_code' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'id_employee' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'amount' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'use_for_id_ticket' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'use_for_id_order' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'use_for_id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'use_for_id_pointshop' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'use_for_id_service' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'date_add' =>   array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>   array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            
			/* SINCE 2.4.1 */
			'id_financial_report' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'day' =>   array('type' => self::TYPE_STRING),
			
        ),
    );
	
	/**
	 * Pour obtenir la liste des utilisations d'avoir d'après une liste d'id_order_slip
	 */
	public static function getFromIdGiftCardCode($ids)
	{
		if(!is_array($ids) || empty($ids))
		{
			return array();
		}
		$sql = 'SELECT sum(amount) as amount, id_prestatill_gift_card_code FROM '._DB_PREFIX_.self::$definition['table'].'
				WHERE id_prestatill_gift_card_code IN ('.implode(',',$ids).')
				GROUP BY id_prestatill_gift_card_code
				';
		$rows = DB::getInstance()->executeS($sql);
		$results = array();
		foreach($rows as $row)
		{
			$results[$row['id_prestatill_gift_card_code']] = $row['amount'];
		}
		return $results;
	} 
	
	/*
	 * RECUPRER LA LISTE DES CHEQUES CADEAUX UTILISES
	 * POUR LE RAPPORT FINANCIER
	 */
	public static function getUsageList($day,$id_pointshop = 0)
	{	
		$query = 'SELECT *, pgu.amount as use_amount
					FROM `'._DB_PREFIX_.'prestatill_gift_card_code_usage` pgu
					LEFT JOIN `'._DB_PREFIX_.'prestatill_gift_card_code` pg ON (pg.id_prestatill_gift_card_code = pgu.id_prestatill_gift_card_code)
					LEFT JOIN `'._DB_PREFIX_.'orders` o ON (o.id_order = pgu.use_for_id_order)
					WHERE pgu.day LIKE \''.$day.'\'
					AND o.id_pointshop = '.$id_pointshop;

		$result = Db::getInstance()->executeS($query);

		return $result;
	}
	
}
