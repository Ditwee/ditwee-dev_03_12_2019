<?php

class ACaissePointshopConfigureControllerCore extends ModuleAdminController
{
    public function initContent()
    {   
        parent::initContent();
        
        if(Tools::getIsset('stats'))
        {
            $pointshop=new ACaissePointshop(Tools::getValue('id_a_caisse_pointshop'));
            if(!Validate::isLoadedObject($pointshop)) {
                die('ERROR : pointshop not found');
            }
            
            $day=Tools::getIsset('day')?Tools::getValue('day'):date('Y-m-d');
            
            $html=$this->_generateStatsBoard($pointshop->id,$day);
            $title='Ventilation des paiements de "'.$pointshop->name.'"';
            $this->_generateContent($html,$title);
            
        }
        /*
        if(Tools::getIsset('permission'))
        {
            $pointshop=new ACaissePointshop(Tools::getValue('id_a_caisse_pointshop'));
            if(!Validate::isLoadedObject($pointshop)) {
                die('ERROR : pointshop not found');
            }
            $html=$this->_displayPermissionsForm();
            $title='Permissions de "'.$pointshop->name.'"';
            $this->_generateContent($html,$title);
        }
		*/
    }
    
    private function _generateStatsBoard($id_pointshop,$day) {
            
        $day2=date('Y-m-d');
        $tmp=explode('-',$day2);
        $start=mktime(0,0,0,$tmp[1],$tmp[2]-31,$tmp[0]);
        $end=mktime(23,59,59,$tmp[1],$tmp[2],$tmp[0]);
        
        $html='Ventilation du '.date('Y-m-d',$start).' au '.date('Y-m-d',$end);
        
        $sqlPaiementsAvailable='SELECT DISTINCT(payment_method)
        FROM '._DB_PREFIX_.'order_payment
        ORDER BY payment_method';
        $paimentsAvailable=DB::getInstance()->executeS($sqlPaiementsAvailable);
        
        $sql='
        SELECT sum(total_paid_real) as tot,payment_method, DATE_SUB(op.date_add,INTERVAL 1 DAY) as day
FROM '._DB_PREFIX_.'a_caisse_ticket AS act
LEFT JOIN '._DB_PREFIX_.'orders AS o ON (act.ps_id_order = o.id_order)
LEFT JOIN '._DB_PREFIX_.'order_payment AS op ON (o.reference = op.order_reference)
WHERE 
    op.date_add BETWEEN CONCAT(\''.date('Y-m-d',$start).'\', \' 00:00:00\') AND CONCAT(\''.date('Y-m-d',$end).'\', \' 23:59:59\')
    AND act.id_pointshop='.(0+$id_pointshop).'
GROUP BY day,op.payment_method
ORDER BY op.payment_method';
        
        $results=DB::getInstance()->executeS($sql);

        $html.='<table>';
        $html.='  <thead><tr>';
        $html.='    <th>'.'Jour'.'</th>';
        
        $joursSem=array('','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche');
                
        foreach($paimentsAvailable as $p)
        {
            $html.='    <th>'.$p['payment_method'].' (TTC)</th>';
        }
        
        $html.='    <th>'.'Total (TTC)'.'</th>';
        $html.='  </tr></thead>';
        
        
        $datas=array();
        
        foreach($results as $result)
        {
            if(!isset($datas[substr($result['day'],0,10)]))
            {
                $datas[substr($result['day'],0,10)]=array();
            }
            $datas[substr($result['day'],0,10)][$result['payment_method']]=$result['tot'];
        }
        $period_tot=0;
        for($t=$start ; $t<$end ; $t+=60*60*24)
        {
            $d=date('Y-m-d',$t);
            $tot=0;
            $html.=' <tr>';
            $html.='    <th>'.$joursSem[date('j',$t)].' '.$d.'</th>';
            foreach($paimentsAvailable as $p)
            {
                $amount = isset($datas[$d][$p['payment_method']])? $datas[$d][$p['payment_method']] : 0;
                $tot+=$amount;
                $html.='    <td>'.$amount.'</td>';
            }
            $period_tot+=$tot;
            $html.='    <th>'.$tot.'</th>';
            $html.=' </tr>';
        }
        $html.='  <tr>
    <th>Totale (TTC)</th>
    <td colspan="'.(1+sizeof($paimentsAvailable)).'">'.$period_tot.'</td>
  </tr>';
        
        $html.='</table>';
        
        return $html;
    }
	/*
    private function _generateContent($html,$title) {
        $smarty = $this->context->smarty;
        $content='<div id="a_caisse_pointshop_toolbar" class="toolbar-placeholder">
	    <div class="toolbarBox toolbarHead">
	            <ul class="cc_button">
	                <li>
	                    <a id="desc-a_caisse_pointshop-back" class="toolbar_btn" href="index.php?controller=ACaissePointshopConfigure&amp;token='.Tools::getAdminTokenLite('ACaissePointshopConfigure').'" title="Retour à la liste">
	                        <span class="process-icon-back "></span>
	                        <div>Retour à la liste</div>
	                    </a>
	                </li>
	            </ul>
	        <div class="pageTitle">
	            <h3>
	                <span id="current_obj" style="font-weight: normal;">
	                    <span class="breadcrumb item-0 ">Caisse <img alt=">" style="margin-right:5px" src="../img/admin/separator_breadcrumb.png"> </span>
	                    <span class="breadcrumb item-1 ">Gestion des points de vente <img alt=">" style="margin-right:5px" src="../img/admin/separator_breadcrumb.png"> </span>
	                    <span class="breadcrumb item-2 ">'.$title.'</span>
	                </span>
	            </h3>
	        </div>
	    </div>
	</div>
	<div class="leadin">'.$html.'</div>
	<div style="clear:both;height:0;line-height:0">&nbsp;</div>';

            $smarty->assign('content', $content);
    }
    
    private function _displayPermissionsForm() {
        $html='';
        $id_pointshop=0+$_GET['id_a_caisse_pointshop'];
        $employees=Employee::getEmployees();
        
        $permissions=array(
            array('name'=>'Accès','id'=>ACaisse::PERMISSION_ACCESS),
            array('name'=>'Ouvrir','id'=>ACaisse::PERMISSION_OPEN),
            array('name'=>'Mouvement','id'=>ACaisse::PERMISSION_MOUVMENT),
            array('name'=>'Fermer','id'=>ACaisse::PERMISSION_CLOSE),
            array('name'=>'Cash reflow','id'=>ACaisse::PERMISSION_CASH_REFLOW),
            array('name'=>'Statitiques','id'=>ACaisse::PERMISSION_STATS),
        );
        
        if(Tools::isSubmit('submitACaissePermissions'))
        {
            foreach($employees as $employee)
            {
                $perm=ACaissePermission::getEmployeePermissions($employee['id_employee'],$id_pointshop);
                foreach($permissions as $permission)
                {
                    $fields='perm'.$permission['id'];
                    $perm->{$fields}=Tools::getIsset('perm_'.$permission['id'].'_'.$employee['id_employee'])?'1':'0';
                }
                $perm->save();
            }            
            $html.='<div class="conf">Nouvelles permissions enregistrées.</div>';
        }        
        
        $html.='<form method="post" action="">';
        $html.='<table>';
        $html.='  <thead>';
        $html.='    <tr>';
        $html.='      <th>Employé</th>';
        foreach($permissions as $permission)
        {
            $html.='      <th>'.$permission['name'].'</th>';
        }
        $html.='    </tr>';
        $html.='  <thead>';
        $html.='  <tbody>';
        foreach($employees as $employee)
        {
            $perm=ACaissePermission::getEmployeePermissions($employee['id_employee'],$id_pointshop);
            $html.='    <tr>';
            $html.='      <td>'.$employee['firstname'].' '.$employee['lastname'].'</td>';
            foreach($permissions as $permission)
            {
                $fields='perm'.$permission['id'];
                $html.='      <td><input'.($perm->{$fields}?' checked="checked"':'').' name="perm_'.$permission['id'].'_'.$employee['id_employee'].'" type="checkbox" /></td>';
            }
            $html.='    </tr>';
        }
        $html.='  </tbody>';
        $html.='</table>';
        
        $html.='<input type="hidden" name="submitACaissePermissions" value="true" />';
        $html.='<input type="submit" value="  Enregistrer  " />';
        $html.='</form>';
        
        return $html;
    }
    */
    public function __construct()
    {
        
        $this->display = 'list'; //view,list or form
        $this->meta_title = $this->l('Points de vente');
        $this->className='ACaissePointshop';
        $this->table='a_caisse_pointshop';
        $this->bootstrap=true;
        $this->identifier='id_'.$this->table;
        
        $this->context = Context::getContext();
        
        if(Tools::getIsset('permission'))
        {
            $this->display='view';
            return parent::__construct();
        }
        
        if(Tools::getIsset('changeCanEditCustomer') && Tools::getIsset('id_a_caisse_pointshop'))
        {
            $pointshop=new ACaissePointshop(Tools::getValue('id_a_caisse_pointshop'));
            if(Validate::isLoadedObject($pointshop))
            {
                $pointshop->editable_customer=!$pointshop->editable_customer;
                $pointshop->save();
            }
        }
        
            
        $this->fields_list = array(
            'id_a_caisse_pointshop' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'width' => 25
            ),
            'name' => array(
                'title' => $this->l('Nom du point de vente'),
                //'filter_key' => 'a!name'
            ),
            'nb_page' => array(
                'title' => $this->l('Nombre de page'),
            ),
            'editable_customer' => array(
                'title' => $this->l('Fiche client éditable'),
                'width' => 70,
                'align' => 'center',
                'type' => 'bool',
                'callback' => 'canEditCustomerIcon',
                'orderby' => false
                ),
            'active' => array(
                'title' => $this->l('Etat'),
                'width' => 70,
                'active' => 'status',
                'filter_key' => 'sa!active',
                'align' => 'center',
                'type' => 'bool',
                'orderby' => false
                )
        );
        
        $this->addRowAction('action');
        
        parent::__construct();
    }

	public function renderForm()
	{
 		$this->fields_form = array(
           //'legend' => array(
             //   'title' => $this->l('Examples'),
             //   'image' => '../img/admin/example.gif'
           //),
           'tabs' => array(
                'Pointshop' => $this->l('Point de vente'),
                'Employees' => $this->l('Employés'),
                'FondDeCaisse' => $this->l('Fond de caisse'),
                'Afficheur' => $this->l('Afficheur client'),
                'Ticket' => $this->l('Ticket de caisse'),
                'Features' => $this->l('Fonctionnalités'),
                'PLV' => $this->l('PLV (Beta)'),
            ),
           'input' => array(
                array(
               		'tab' => 'Pointshop',
                    'type' => 'text',
                    'label' => $this->l('Nom :'),
                    'name' => 'name',
                    'col' => '3',
                    'size' => 33,
                    'required' => true,
                    'desc' => $this->l('Nom du point de vente'),
                ),
               array(
               		'tab' => 'Pointshop',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Point de vente actif'), 
                    'name' => 'active',
                    'col' => '3', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
               array(
               	'tab' => 'Features',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Client éditable'),
                    'name' => 'editable_customer', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
               array(
	               	 'tab' => 'Pointshop',
	                 'type' => 'text',
	                 'label' => $this->l('Email :'),
	                 'name' => 'admin_email',
	                 'size' => 150,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Emails des administrateurs de la caisse, séparés par des points virgules sans espaces (email@mail.com;email@mail.com)'),
	                ),
               array(
	               	 'tab' => 'Pointshop',
	                 'type' => 'text',
	                 'label' => $this->l('Email comptabilité :'),
	                 'name' => 'compta_email',
	                 'size' => 150,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Emails destinés à la comptabilité de la caisse, séparés par des points virgules sans espaces (email@mail.com;email@mail.com)'),
	                ),   
               array(
               		'tab' => 'Features',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Activer le paiement multiple'), 
                    //'desc' => $this->l('xxx'), 
                    'name' => 'active_multi_payment', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
				 array(
               		'tab' => 'Features',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Activier la création de produit à la volée'), 
                    //'desc' => $this->l('xxx'), 
                    'name' => 'activate_add_product', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ), 
               array(
               		'tab' => 'Features',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Autoriser le paiement partiel'), 
                    'desc' => str_replace("\'","'",$this->l("Permet de clôturer une commande sans qu'elle ne soit soldée")), 
                    'name' => 'active_partial_paiement', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
               array(
               		'tab' => 'Features',
                    'type' => 'radio',
                    'label' => $this->l('Prix d\'achat et la marge'),
                    'name' => 'display_wholesale_price',
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_1', 
                            'value' => 0, 
                            'label' => $this->l('Ne jamais afficher')
                        ),
                        array(
                            'id' => 'active_2',
                            'value' => 1,
                            'label' => $this->l('Afficher au clic')
                        ),
                        array(
                            'id' => 'active_3',
                            'value' => 2,
                            'label' => $this->l('Toujours afficher')
                        )
                    ),
                ), 
               array(
               		'tab' => 'Features',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Fermeture automatique pop-up de déclinaison'), 
                    'desc' => str_replace("\'","'",$this->l("Après ajout d'une déclinaison au ticket")), 
                    'name' => 'declinaison_close_popup_after_select', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
               array(
               		'tab' => 'Pointshop',
	                'type' => 'text',
	                'label' => $this->l('Limite de stock bas'),
	                'name' => 'product_STOCK_WARNING_LIMIT',
	                'size' => 10,
                    'col' => '3',
	                'required' => true,
	                'desc' => $this->l('À partir de combien le stock passe en orange'),
	                ),
               array(
               		'tab' => 'Pointshop',
                    'type' => 'select',
                    'label' => $this->l('Image produit'),
                    'name' => 'use_image',
                    'required' => true, 
                    'class' => 't',
                    'col' => '3',
                    'options' => array(
                       'query' => array(array('id'=>0,'name'=>'Ne pas afficher'),array('id'=>1,'name'=>'Afficher la miniature'), array('id'=>2,'name'=>"Afficher l'image")),
                       'id' => 'id',
                       'name' => 'name'
                    ),
                ),
               array(
               		'tab' => 'Features',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Activer la navigation par catégorie'),
                    'name' => 'navigation_by_category', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
                array(
               		'tab' => 'Features',
                    'type' => 'radio',
                    'label' => $this->l('Que faire à la fin d\'une vente ?'),
                    'name' => 'return_to_catalog_after',
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_1', 
                            'value' => 1, 
                            'label' => $this->l('Afficher la page de caisse principale')
                        ),
                        array(
                            'id' => 'active_2',
                            'value' => 2,
                            'label' => $this->l('Afficher les catégories')
                        ),
                        array(
                            'id' => 'active_3',
                            'value' => 0,
                            'label' => $this->l('Afficher le nouveau ticket')
                        )
                    ),
                ),
               array(
               		'tab' => 'Features',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Activer le moteur de recherche produit'),
                    'name' => 'activate_search_product',
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
                array(
               		'tab' => 'Features',
                    'type' => 'text',
                    'label' => $this->l('Nombre de caractères avant de lancer une recherche de produit :'),
                    'name' => 'nb_caract_for_search',
                    'col' => '3',
                    //'size' => 200,
                    'required' => true,
                    'desc' => $this->l('Mettre 0 si vous avez désactivé la recherche des produits'),
                ),
               array(
               		'tab' => 'Features',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Activer la recherche de ticket'),
                    'name' => 'activate_search_ticket',
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),               
               array(
               		'tab' => 'Pointshop',
                     'type' => 'text',
                    'col' => '3',
                     'label' => $this->l('Intervalle de temps entre les rechargements automatiques des caches :'),
                     'name' => 'time_before_reload_caches',
                     //'size' => 200,
                     'required' => true,
                     'desc' => str_replace("\'","'",$this->l("Le temps s'exprime en minute")),
                ),
               
               array(
               		'tab' => 'Pointshop',
	                 'type' => 'text',
                    'col' => '3',
	                 'label' => $this->l('Catégorie par défaut'),
	                 'name' => 'id_root_category',
	                 //'size' => 200,
	                 'required' => true,
	                 'desc' => $this->l('ID de la catégorie par défaut (mode navigation par catégorie)'),
	                ),
               array(
               		'tab' => 'Ticket',
	                 'type' => 'text',
                    'col' => '3',
	                 'label' => $this->l('Nombre de ticket à imprimer :'),
	                 'name' => 'ticket_nbr_copies',
	                 //'size' => 200,
	                 'required' => true,
	                 'desc' => $this->l('Mettre 0 pour ne sortir aucun ticket'),
	                ),
                 
               array(
               		'tab' => 'Ticket',
	                 'type' => 'text',
                    'col' => '3',
	                 'label' => $this->l('Logo sur le ticket de caisse en base 64 :'),
	                 'name' => 'base64_ticket_img',
	                 //'size' => 200,
	                 'required' => true,
	                 'desc' => $this->l('Voir https://www.base64-image.de/ pour conversion.'),
	                ),
               array(
               		'tab' => 'Pointshop',
	                'type' => 'text',
                    'col' => '3',
	                'label' => str_replace("\'","'",$this->l("Mode d'affichage client :")),
	                'name' => 'screen_mode',
	                //'size' => 200,
	                'required' => true,
	                'desc' => $this->l('"usb", "usb,lcd", "lcd" ou laissez vide si aucun afficheur client'),
	                ),
               array(
               		'tab' => 'Pointshop',
	                 'type' => 'text',
                    'col' => '3',
	                 'label' => str_replace("\'","'",$this->l('IP de l\'écran LCD (version arduino) :')),
	                 'name' => 'screen_lcd_ip',
	                 //'size' => 200,
	                 'required' => true,
	                 'desc' => $this->l('Laissez vide si vous n\'utilisez pas d\'afficheur Arduino'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Récupérer les dernières informations depuis le dernier état de caisse'), 
                    'desc' => str_replace("\'","'",$this->l("Aussi bien à l'ouverture qu'à la fermeture")), 
                    'name' => 'retrieve_cash_jnfos', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Pièces de 1 cent :'),
	                 'name' => 'cash_001',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
                 'type' => 'text',
                 'label' => $this->l('Pièces de 2 cents :'),
                 'name' => 'cash_002',
                 'size' => 33,
                    'col' => '3',
                 'required' => true,
                 'desc' => $this->l('Dans le fond de caisse en début de service'),
                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Pièces de 5 cents :'),
	                 'name' => 'cash_005',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Pièces de 10 cents :'),
	                 'name' => 'cash_01',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Pièces de 20 cents :'),
	                 'name' => 'cash_02',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Pièces de 50 cents :'),
	                 'name' => 'cash_05',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Pièces de 1 euro :'),
	                 'name' => 'cash_1',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Pièces de 2 euros :'),
	                 'name' => 'cash_2',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Billets de 5 euros :'),
	                 'name' => 'cash_5',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Billets de 10 euros :'),
	                 'name' => 'cash_10',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Billets de 20 euros :'),
	                 'name' => 'cash_20',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Billets de 50 euros :'),
	                 'name' => 'cash_50',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Billets de 100 euros :'),
	                 'name' => 'cash_100',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Billets de 200 euros :'),
	                 'name' => 'cash_200',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
                array(
               		'tab' => 'FondDeCaisse',
	                 'type' => 'text',
	                 'label' => $this->l('Billets de 500 euros :'),
	                 'name' => 'cash_500',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Dans le fond de caisse en début de service'),
	                ),
               array(
               		'tab' => 'Pointshop',
	                 'type' => 'text',
	                 'label' => $this->l('Lignes maximum :'),
	                 'name' => 'max_row',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Nombre de lignes total des grilles du point de vente'), 
	                ),
                array(
               		'tab' => 'Pointshop',
	                 'type' => 'text',
	                 'label' => $this->l('Lignes maximum à afficher :'),
	                 'name' => 'max_display_row',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Nombre de lignes affichées par les grilles du point de vente'), 
	                ),
               array(
               		'tab' => 'Pointshop',
	                 'type' => 'text',
	                 'label' => $this->l('Colonnes maximum :'),
	                 'name' => 'max_col',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Nombre de colonnes pour la vue du point de vente'), 
	                ),
               array(
               		'tab' => 'Pointshop',
	                 'type' => 'text',
	                 'label' => $this->l('Largeur de la caisse :'),
	                 'name' => 'till_width',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Largeur de votre caisse en pixel'), 
	                ),
               array(
               		'tab' => 'Pointshop',
	                 'type' => 'text',
	                 'label' => $this->l('Hauteur de la caisse :'),
	                 'name' => 'till_height',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Hauteur de votre caisse en pixel'), 
	                ),
               array(
               		'tab' => 'Pointshop',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Envoi email'), 
                    'desc' => $this->l('Envoyer un email au client quand sa commande est validée ?'), 
                    'name' => 'send_mail_to_custumer', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
               array(
               		'tab' => 'Pointshop',
	                 'type' => 'text',
	                 'label' => $this->l('ID entrepôt'),
	                 'name' => 'id_warehouse',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('0 = le point de vente tiendra compte de tous les stocks dispo'), 
	                ),
	            array(
               		'tab' => 'Pointshop',
	                 'type' => 'select',
                     'label' => $this->l('Prix & réductions'),
                     'name' => 'display_original_price',
                     'required' => true, 
                     'class' => 't',
                     'col' => '3',
                     'options' => array(
                       'query' => array(
                       		array(
                       			'id'=>  0,
                       			'name'=>'Afficher uniquement le prix final réduit'),
                       		array(
                       			'id'=>1,
                       			'name'=>'Afficher le prix original barré'),
							),
	                       'id' => 'id',
	                       'name' => 'name',
						),
	                ),
                array(
               		'tab' => 'PLV',
	                 'type' => 'text',
	                 'label' => $this->l('ID de la PVL'),
	                 'name' => 'id_plv_screen',
	                 'size' => 33,
                    'col' => '3',
	                 'required' => true,
	                 'desc' => $this->l('Nécessite le module de PLV ; Nécessite un afficheur en mode usb ; Mettre 0 par défaut'), 
	                ),
                array(
               		'tab' => 'PLV',
	                 'type' => 'text',
	                 'label' => str_replace("\'","'",$this->l("Temps avant l'apparition de la publicité sur l'écran usb :")), /*pourquoi les backslash subsistent sur mes singlequotes ?!*/
	                 'name' => 'time_before_PLV_display',
                    'col' => '3',
	                 //'size' => 200,
	                 'required' => true,
	                 'desc' => $this->l('Nécessite le module de PLV ; Nécessite un afficheur en mode usb ; Mettre 0 par défaut'),
                	),
                
                array(
	               	 'tab' => 'Ticket',
	                 'type' => 'text',
                    'col' => '3',
	                 'label' => $this->l("Nom du point de vente :"),
	                 'name' => 'pointshop_name_on_ticket',
	                 //'size' => 200,
	                 'required' => true,
	                ),
                array(
               		'tab' => 'Ticket',
	                 'type' => 'text',
                    'col' => '3',
	                 'label' => $this->l("Adresse du point de vente :"),
	                 'name' => 'adresse',
	                 //'size' => 200,
	                 'required' => true,
	                ),
                array(
               		'tab' => 'Ticket',
	                 'type' => 'text',
                    'col' => '3',
	                 'label' => $this->l("Numéro de téléphone :"),
	                 'name' => 'telephone',
	                 //'size' => 200,
	                 'required' => true,
	                ),
                array(
               		'tab' => 'Ticket',
	                 'type' => 'text',
                    'col' => '3',
	                 'label' => str_replace("\'","'",$this->l("Message d'accueil :")),
	                 'name' => 'msg_head_ticket',
	                 //'size' => 200,
	                 'required' => true,88///////////////////////////////////////////////////////////////8889
	                ),
                array(
               		'tab' => 'Ticket',
	                 'type' => 'text',
                    'col' => '3',
	                 'label' => $this->l("Message de remerciement :"),
	                 'name' => 'msg_foot_ticket',
	                 //'size' => 200,
	                 'required' => true,
	                ),
	            array(
               		'tab' => 'Ticket',
                    'type' => version_compare(_PS_VERSION_, '1.6') < 0 ?'radio' :'switch',
                    'label' => $this->l('Imrpimer les tickets avec le Z de caisse :'), 
                    'desc' => $this->l('Souhaitez-vous imprimer les tickets de caisse à la suite du Z ?'), 
                    'name' => 'print_tickets_with_z', 
                    'required' => true, 
                    'class' => 't',
                    'is_bool' => true, 
                    'values' => array(
                        array(
                            'id' => 'active_on', 
                            'value' => 1, 
                            'label' => $this->l('Oui')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Non')
                        )
                    ),
                ),
                    
				// Employés
				array(
                    'tab'=> 'Employees',
					'type' => 'checkbox',
					'label' => $this->l('Employees'),
					'name' => 'employee',
                    'values' => array(
                        'query' => Employee::getEmployeesWithFullname(),
                        'id' => 'id_employee',
                        'name' => 'fullname',
                    ),
                ),	
            ),
            'submit' => array(
				'title' => $this->l('Save'),
			),
            'buttons' => array(
                            'save-and-stay' => array(
                            'title' => $this->l('Enregistrer et rester'),
                            'name' => 'submitAdd'.$this->table.'AndStay',
                            'type' => 'submit',
                            'class' => 'btn btn-default pull-right',
                            'icon' => 'process-icon-save',
	             		),
	              ),
          );
		  
		  return parent::renderForm();
	}

	/**
	 * Return the list of fields value
     * on y met la liste des employé rattaché au seller
	 *
	 * @param object $obj Object
	 * @return array
	 */
	public function getFieldsValue($obj)
	{
		
        foreach($this->object->getAssociatedEmployees() as $id_employee){
            $this->fields_value['employee_'.$id_employee] = true;
        }
        
        return parent::getFieldsValue($obj);
    }
	
	protected function afterAdd($object)
	{
		$result = true;
		$this->saveEmployeesPointshop($object);
			
		return $result;
	}
	
	/**
	 * enregistre les employés rattaché au seller
	 *
	 * @param object $obj Object
	 * @return array
	 */
	public function saveEmployeesPointshop($object)
	{
        $associatedEmployees = $object->getAssociatedEmployees();
        //on vérifie si les employé sont toujours dedans
        foreach($associatedEmployees as $id_employee){
            if(!Tools::getIsset('employee_'.$id_employee)){
                $object->removeEmployee($id_employee); 
                unset($associatedEmployees[$id_employee]);
            }
        }

        //on vérifie si les employé envoyé sont bien dans la bdd
        foreach($_POST as $item => $value){
            $item = explode("_", $item);
            if($item[0] == 'employee' && !in_array($item[1], $associatedEmployees)){
                $object->addEmployee($item[1]); 
                $associatedEmployees[] = $item[1];
            }
        }

		return true;
    }

	/**
	 * @see AdminController::afterUpdate()
	 */
	protected function afterUpdate($object)
	{
		$result = true;
		$this->saveEmployeesPointshop($object);
			
		return $result;
	}
	

    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
        $nb_items = count($this->_list);
        for ($i = 0; $i < $nb_items; $i++)
        {
            $nbPages=ACaissePointshop::countPage($this->_list[$i]['id_a_caisse_pointshop']);
            
            switch($nbPages)
            {
                case 0:
                    $this->_list[$i]['nb_page']=$this->l('Aucune page');
                    break;
                case 1:
                    $this->_list[$i]['nb_page']=$this->l('Une page');
                    break;
                default :
                    $this->_list[$i]['nb_page']=$nbPages.' '.$this->l('pages');
                    break;
            }
        }
    }
    
public static function canEditCustomerIcon($value, $pointshop)
    {
        return '<a href="index.php?controller=ACaissePointshopConfigure&id_a_caisse_pointshop='
            .(int)$pointshop['id_a_caisse_pointshop'].'&changeCanEditCustomer&token='.Tools::getAdminTokenLite('ACaissePointshopConfigure').'">
                '.($value ? '<img src="'._PS_IMG_.'admin/enabled.gif" />' : '<img src="'._PS_IMG_.'admin/disabled.gif" />').
            '</a>';
    }
    

public function displayActionLink($token, $id)
{
    $tpl = $this->createTemplate('list_action_export.tpl');
    
    $tpl->assign(array(
         'href_right'=> '?controller=ACaissePointshopConfigure&permission&token='.Tools::getAdminTokenLite('ACaissePointshopConfigure').'&'.$this->identifier.'='.$id,
         'action_right'=>'Permissions',
         'action_icon'=>'../img/admin/htaccess.gif',
         'href_stats'=> '?controller=ACaissePointshopConfigure&stats&token='.Tools::getAdminTokenLite('ACaissePointshopConfigure').'&'.$this->identifier.'='.$id,
         'action_stats'=>'Statistiques',
         'action_iconstats'=>'../img/t/AdminParentStats.gif',
    ));

    return $tpl->fetch();
}
    
    //surcharge pour tester aussi le dossier view dans le module
    public function createTemplate($tpl_name)
    {
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name) && $this->viewAccess()) {
            return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name, $this->context->smarty);
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->module->name.'/views/templates/admin/'.$tpl_name) && $this->viewAccess()) {
            return $this->context->smarty->createTemplate(_PS_MODULE_DIR_.''.$this->module->name.'/views/templates/admin/'.$tpl_name, $this->context->smarty);
        }
        return parent::createTemplate($tpl_name);
    }         

}