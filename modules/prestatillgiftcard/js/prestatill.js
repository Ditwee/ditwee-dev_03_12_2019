$(function(){

	function prestatillgiftcard(caisse_ref) {
		var module = this;
		var module_name = 'prestatillgiftcard';
		
		//options
		var currency = '€';
		
		//propriétés du module
		this.total = 0;
		this.buffer = '';
		this.with_repaid = true;
		this.idOrderSlip = null;
		//pripriete du module pour le retour produit
		this.total_refund = 0;		
		this.refund_has_end_point = false;
		this.refund_has_point = false;
		this.refund_buffer = '';
		
		
		
		var _formater = new Intl.NumberFormat('fr-FR',{minimumFractionDigits:2});
		var _date_formater = new Intl.DateTimeFormat('fr-FR',{});
		var $interface;
		
		/**
		 * Indique si une virgule "silencieuse" existe à la fin de la saisie (en attendant une saiie d'un nombre) 
		 */
		this.has_end_point = false;
		
		/**
		 * Indique si il y a déjà une virgule dans la saisie en cours 
		 */
		this.has_point = false;
		
    	
		this.sendAmount = function() {
			//@TODO : voir receipt pour l'envoi de donner supplémaentaires
			PRESTATILL_CAISSE.payment.changePaymentAmount(this.total, this.with_repaid);
		};
		
		var scanner_customerKeyIntervalProcess = 0;
		var scanner_resetter_process = null;
		var scanner_relauncher_process = null;
		var scanner_buffer = '';
		
		this.init = function() {
			
			$interface = $('.prestatill_payment_interface[data-module-name="'+module_name+'"]');
			$('.ui-tab-content',$interface).css('maxHeight',$('#content').height()-45-64-64-$('.display_amount',$interface).height()-$('.payment_actions',$interface).height()-300);
			
			$('body').find('div[data-module-name=prestatillgiftcard] li:first-child').trigger('click');
			$('body').find('div[data-module-name=prestatillgiftcard] .prestatillgiftcard-by-barcode .results').html('');
			
			var id_customer = (ACAISSE.customer.id == undefined)?0:ACAISSE.customer.id;
			var url_ajax = $interface.data('urlAjax');
			this.idOrderSlip = null;
			this.total = 0;
			$('.prestatillgiftcard-from-client',$interface).html('<div class="info">Chargement...</div>');
			$.ajax({
				url:url_ajax,
				method:'post',
				data:{
					'id_customer':id_customer
				}
			}).done(function(response){
				var html = '';
				
				if(response.success == true)
				{
					html = _generateHTMLCodesList(response.giftcards,response.usages);					
				}
				else
				{
					html = '<div class="danger">' + response.msg + '</div>';	
				}	
				$('.prestatillgiftcard-from-client',$interface).html(html).addClass('current');
				$('.prestatillgiftcard-by-barcode',$interface).removeClass('current');				
			}).fail(function(){
				alert('Error o78965 : Impossible de contacter le serveur pour obtenir la liste des avoirs');
			});			
				
			$('.prestatillgiftcard-by-barcode input',$interface).keyup(function(e){
				var code128 = $(this).val();
				if(code128.length == 10)
				{
					_searchGiftCardFromCode(code128);
				}
			});
			
			this.total = 0;		
			PRESTATILL_CAISSE.payment.changePaymentAmount(this.total);
		};		
		
		var _generateHTMLCodesList = function(giftcards, usages) {
			var html = '<ul class="prestatillgiftcard-list">';
					
			html += '<li class="prestatillgiftcard-code-header">';
					html += '	<span class="prestatillgiftcard-id">n°</span>';
					html += '	<span class="prestatillgiftcard-amount">Montant chèque</span>';
					html += '	<span class="prestatillgiftcard-sold">Solde dispo.</span>';
					html += '	<span class="prestatillgiftcard-date">Date</span>';
					html += '</li>';
			
			
			$.each(giftcards,function(i,giftcard){
				var amount = parseFloat(giftcard.amount);
				var sold = amount;
				
				if(usages[giftcard.id_prestatill_gift_card_code] != undefined)
				{
					sold -= usages[giftcard.id_prestatill_gift_card_code]/100; 
				}	
				
				if(sold>0)
				{
					html += '<li class="prestatillgiftcard-code" data-id-prestatill-gift-card-code="' + giftcard.id + '" data-amount="' + sold + '">';
					html += '	<span class="prestatillgiftcard-id">#'+ giftcard.id + '</span>';
					html += '	<span class="prestatillgiftcard-amount">'+ _formater.format(amount) + currency + '</span>';
					html += '	<span class="prestatillgiftcard-sold">'+ _formater.format(sold) + currency + '</span>';
					html += '	<span class="prestatillgiftcard-date">' + _date_formater.format(new Date(giftcard.date_add)) +'</span>';
					html += '</li>';
				}
			});
			html += '</ul>';
			
			return html;			
		};
		
		this.captureScanner = function(e) {
				clearInterval(scanner_resetter_process);
                if (scanner_relauncher_process !== null) {
                    clearTimeout(scanner_relauncher_process);
                }
                scanner_relauncher_process = setTimeout(function () {
                    scanner_resetter_process = setInterval(function () {
                        scanner_buffer = '';
                    }, 500);
                }, 150);

                if (e.keyCode == 13 && scanner_buffer.length == 10) // touche entrée
                {
                	//ici on lance la recherche
                    $('[data-target=".prestatillgiftcard-by-barcode"]',$interface).click();
                    $('input',$interface).val(scanner_buffer);
                    _searchGiftCardFromCode(scanner_buffer);
                    
                    scanner_buffer = '';
                }
                else
                {
                    scanner_buffer += String.fromCharCode(e.keyCode);
                }
    	};
    	
    	PRESTATILL_CAISSE.registerHook('displayPrestatillCustomerOrdersLine', function(params){ 

			var _addCustomerOrderLineGiftCard = function(url_ajax, id_order ){

				$.ajax({
					url:url_ajax,
					method:'post',
					data:{
					'id_order': id_order
					}
	
				}).done(function(response){
					
					var gift_cards = response.gift_cards;
					
					if(gift_cards != undefined && gift_cards.length > 0)
					{
						var cards = gift_cards.toString();
						var html = '<button class="seeGiftCard" data-id="'+cards+'"><i class="fa fa-gift"></i> <span class="btLabel">BON KDO</span></button>';
						$('#pageUser3 tr[data-id_order='+id_order+'] .actions').prepend(html);
					}
					
				}).fail(function(){
	
				});
			}
			
			var url_ajax = window.location.origin+"/index.php?fc=module&module=prestatillgiftcard&controller=getgiftcard&action=HasGiftCard";
			var id_order = params['id_order'];
			
			_addCustomerOrderLineGiftCard(url_ajax, id_order);
	
		});
		
		var _searchGiftCardFromCode = function(code128) {
			var url_ajax = $interface.data('urlAjax');
			this.idOrderSlip = null;
			this.total = 0;
			$('.prestatillgiftcard-by-barcode .results',$interface).html('<div class="info">Chargement...</div>');
			$.ajax({
				url:url_ajax,
				method:'post',
				data:{
					'code128':code128
				}
			}).done(function(response){
				var html='';
				if(response.success)
				{
					//console.log(response);				
					var html = _generateHTMLCodesList(response.giftcards,response.usages);					
					$('.prestatillgiftcard-by-barcode .results').html(html);
				}
				else
				{					
					alert('Erreur OG203 : '+response.msg);
				}	
				
			}).fail(function(){
				alert('Error OG204 : impossible de communiquer avec le serveur');
			});
		};
		
		this.getExtraDatas = function() {
			var id_prestatill_gift_card_code = parseInt('0'+$('.prestatill_payment_interface[data-module-name="'+module_name+'"] .prestatillgiftcard-code.selected').data('idPrestatillGiftCardCode'));
			return {
				'id_prestatill_gift_card_code' : id_prestatill_gift_card_code,
			};
		};
		
		//handler sur le click sur un chèque cadeau
		$('.prestatill_payment_interface[data-module-name="'+module_name+'"]').on('tap click','.prestatillgiftcard-code',function(e){
			var datas = $(this).data();		
			this.total = Math.min(datas.amount,parseFloat($('#keepToHaveToPay').html().replace(' ','')));
			
			this.id_prestatill_gift_card_code = datas.idPrestatillGiftCardCode;
				
			PRESTATILL_CAISSE.payment.changePaymentAmount(this.total);
			
			$('.prestatill_payment_interface[data-module-name="'+module_name+'"] .prestatillgiftcard-code').removeClass('selected');
			$(this).addClass('selected');		
		});
		
		
		/****************************/
		/** Gestion en mode retour **/
		/****************************/
		
		//]TODO attention
		$('.prestatill_return_interface[data-module-name="'+module_name+'"]').on('click tap','.numeric_keyboard>span',function(e){
			
			var key_code = $(this).html();
			//console.log('buffer before '+module.buffer);
			switch(key_code)
			{
				case '.':
				case ',':
					if(!module.refund_has_end_point)
					{
						module.refund_has_end_point = true;
					}
					module.refund_has_point = true;
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					if(module.refund_has_point && module.refund_has_end_point)
					{
						module.refund_buffer += '.';						
					}
					
					var test = module.refund_buffer.split('.');
					if(!module.refund_has_point || (module.refund_has_point && test[1] != undefined && test[1].length<2))
					{										
						module.refund_buffer += key_code;
						module.refund_has_end_point = false;
					}
					break;
				case 'C':
				case 'R':
					if(module.refund_buffer.length >0)
					{
						module.refund_buffer = '';
						module.refund_has_end_point = false;
						module.refund_has_point = false;
					}
					break;
			}
			
			var value = module.refund_buffer;
			if(value == '')
			{
				value = 0;
			}
			module.total_refund = parseFloat(value);
			module.sendReturnAmount();
			//console.log('buffer after '+module.buffer + ' soit '+ module.total);
			//console.log(module);
		});
		
		
		this.sendReturnAmount = function() {			
			PRESTATILL_CAISSE.return.changeRefundAmount(this.total_refund);
		};
		
		
		
	};
	
	/*
     * AFFICHAGE DU TICKET CADEAU DANS L'INTERFACE CLIENT
     */
    $('body').on('click tap','#pageUser3 .seeGiftCard',function(e) {
    	var id_gift_cards = $(this).data('id');
    	var id_order = $(this).parents('tr').data('id_order');
    	var url_ajax = window.location.origin+"/index.php?fc=module&module=prestatillgiftcard&controller=getgiftcard&action=LoadGiftCardsTickets";
    	$.ajax({
    		url: url_ajax,
	        data: {
	            'action': 'LoadGiftCardsTickets',
	            'id_gift_cards': id_gift_cards,
	            'id_order': id_order
	        },
	        type: 'post',
	        dataType: 'json',
	    }).done(function (response) {
	        if (response.success !== false) {
	        	var response = response.response.html;
	        	
	        	if($('.switcher').hasClass('show-product-list') == false)
	        	{
	        		showEmptyPopup($("#productList"),response);
	        	}
	        	else
	        	{
	        		showEmptyPopup($("#ticketView"),response);
	        	}
	            
	        } else {
	        	alert('Erreur k02011 : impossible de charger le ticket. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	        }
	    }).fail(function () {
	        console.log('Erreur k02010 : impossible de charger le ticket. Si l\'erreur persiste veuillez contacter l\'administrateur du site.');
	    });
    });
    
    $('body').on('click tap','.GiftCard .print',function(e) {
    	printTicket($(this).parents('.GiftCard').html(),1); 
    	$('.closePopup').trigger('click');
    	printQueue();   	
    });
    
	
	/*************
	Enregistrement du module de payment dans l'instance de la caisse
	**************/	
	PRESTATILL_CAISSE.payment.modules.push(new prestatillgiftcard());	
});
