<?php
require_once(dirname(__FILE__).'/classes/PrestaTillGiftCardCode.php');
require_once(dirname(__FILE__).'/classes/PrestatillGiftCardCodeUsage.php');

class PrestaTillGiftCard extends PaymentModule
{
	
	const SECRET_TOCKEN = 'dfsfdsfd65874èAQjdsq45azj324';

    public function __construct() {
        $this->name = 'prestatillgiftcard';
        $this->tab = 'payments_gateways';
        $this->version = '1.0';
        $this->author = 'Prestatill SAS';
        $this->need_instance = 0;
		
        parent::__construct();
		
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap = true;
        $this->displayName = $this->l('Chèque cadeau');
        $this->description = $this->l('Solution de vente et utilisation de chèques cadeaux compatible avec la caisse PrestaTill et Prestashop');

    }
	
	
	public function getTicketPaymentLabel($params = NULL)
	{
		return $this->l('Chèque cadeau');
	}

    public function install() {
        if (
            parent::install() == false
            
            || !$this->_registerNewTab()
			|| !$this->_installSql()
			
			//hooks Prestatill
			|| !$this->registerHook('displayPrestatillInjectJS')
			|| !$this->registerHook('displayPrestatillInjectCSS')
			|| !$this->registerHook('displayPrestatillPaymentButton')
			|| !$this->registerHook('displayPrestatillPaymentInterface')			
			|| !$this->registerHook('actionPrestatillRegisterNewPaymentPart')
			|| !$this->registerHook('displayPrestatillFinancialReportFooter')	
			
			|| !$this->registerHook('actionPrestaTillGenerateExtraTickets')
            
            //hook lié à la fiche produit
            || $this->registerHook('displayAdminProductsExtra') == false
            || $this->registerHook('actionProductUpdate') == false
            
            //hook lié à la validation des paiement
            || $this->registerHook('actionValidateOrder') == false
			|| !$this->_installOrderState()
			
        ) {
            return false;
        }
        /*
        // on créer les paramètres par défaut
        if(Configuration::get('ACAISSE_profilsID') === false) {
        	Configuration::updateValue('ACAISSE_profilsID',1);
		}		
        	
        if(Configuration::get('ACAISSE_profilsIDNoChecking') === false) {
        	Configuration::updateValue('ACAISSE_profilsIDNoChecking','');
		}
        
        if(Configuration::get('ACAISSE_searchActiveCustomer') === false) {
        	Configuration::updateValue('ACAISSE_searchActiveCustomer',0);
		}
        
        Configuration::updateValue('ACAISSE_last_cache_generation_categories', '0000-00-00 00:00:00');
        Configuration::updateValue('ACAISSE_last_cache_generation_pointshoppages', '0000-00-00 00:00:00');
		
		*/
        
        return true;
    }

	public function hookDisplayPrestatillFinancialReportFooter($params)
	{	
		$html = '';
		//$params['day'] = '2018-02-01';
		//$html .= print_r($params,true);
		//d($params['id_pointshop']);
		
		$day = $params['day'];
		
		// On récupère le jour du rapport en cours
		$id_report = PrestatillFinancialReport::ReportExists($day, $params['id_pointshop']);
		if($id_report != false)
		{
			$report = new PrestatillFinancialReport($id_report);
			if(Validate::isLoadedObject($report))
			{
				$day = $report->day;
			}
		}
		
		$gift_buying = PrestaTillGiftCardCode::getList($day,$params['id_pointshop']);
		
		//$gift_usage = ACaisseMultiplePayment::getListByDayAndModuleName($day,$this->name);
		$gift_usage = PrestatillGiftCardCodeUsage::getUsageList($day,$params['id_pointshop']);
		
		foreach($gift_usage as $key => $usage)
		{
			if(!empty($usage['extra_datas']))
			{
				$gift_usage[$key]['extra_datas'] = json_decode($usage['extra_datas']);
			}
		}
		
		
		if((Tools::getValue('controller') == 'ACaisseUse'))
		{
			$tpl = $this->createTemplate('prestatill_z_footer.tpl');
		}
		else
		{
			$tpl = $this->createTemplate('prestatill_financial_report_footer.tpl');
		}
		
		//d($gift_buying);
		
		$tpl->assign(array(
            'gift_usage' => $gift_usage,
            'gift_buying' => $gift_buying,
            'tocken_order' => Tools::getAdminTokenLite('AdminOrders'),
            'currency' => new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
        ));
		
	    $html .= $tpl->fetch();
		return $html;
		
	}
    
	
	/**
	 * Hook s'exécutant lors de l'enregistrement d'un nouveau payement part
	 * avec les paramètres suivant :
	 * 	- order
	 *  - payment_part
	 *  - extra_datas
	 *  
	 */
	public function hookActionPrestatillRegisterNewPaymentPart($params)
	{
		$payment_part = $params['payment_part'];
		$extra_datas = $params['extra_datas'];
		$id_report = false;

		if($payment_part->module_name == $this->name)
		{
			require_once(__DIR__.'/classes/PrestatillGiftCardCodeUsage.php');
			
			$usage = new PrestatillGiftCardCodeUsage();
			$usage->id_prestatill_gift_card_code = $extra_datas['id_prestatill_gift_card_code'];
			$usage->id_employee = Context::getContext()->employee->id;
			$usage->amount = $payment_part->payment_amount;		
			$usage->use_for_id_order = (int)$params['id_order'];
			$usage->use_for_id_ticket = (int)$params['id_ticket'];; 
			$usage->use_for_id_customer = (int)$params['id_customer'];
			$usage->use_for_id_pointshop = (int)$params['id_pointshop'];
			$usage->use_for_id_service = (int)$params['id_service'];
			
			if(Module::isEnabled('prestatillexportcompta'))
			{
				$day = date('Y-m-d');
		
				// On récupère le jour du rapport en cours
				$id_report = PrestatillFinancialReport::ReportExists($day, $params['id_pointshop']);
				if($id_report != false)
				{
					$report = new PrestatillFinancialReport($id_report);
					if(Validate::isLoadedObject($report))
					{
						$usage->use_for_id_service = $report->id_service;
						$usage->id_financial_report = $report->id;
						$usage->day = $report->day;
					}
				}
			}
			
			$usage->save();		
		}		
	}
	
	public function getHookController($hook_name)
	{
		require_once(dirname(__FILE__).'/controllers/hook/'.$hook_name.'.php');
		$controller_name = $this->name.$hook_name.'Controller';
		$controller  = new $controller_name($this, __FILE__, $this->_path);
		return $controller;
	}
	
	public function hookDisplayPayment ($params)
	{
		$controller = $this->getHookController('displayPayment');
		return $controller->run($params);
			
	}
	
	protected function _installOrderState()
	{
		//on check si ce status n'a pas DEJA était uinstallé
		if(!Configuration::hasKey('PRESTATILL_GIFTCARD_ID_ORDER_STATE'))
		{
			$context = Context::getContext();
			$id_lang = $context->language->id;
			
			$os = new OrderState(null,$id_lang);
			$os->send_email = false;
			$os->module_name = 'prestatillgiftcard';
			$os->invoice = true;
			$os->color = '#44bb00';
			$os->logable = true;
			$os->shipped = true;
			$os->unremovable = false;
			$os->delivery = true;
			$os->hidden = false;
			$os->paid = true;
			$os->pdf_delivery = false;
			$os->pdf_invoice = true;
			$os->deleted = false;
			
			$os->name = 'Paiement accepté (chèque cadeau)';
			$os->save();
			Configuration::updateValue('PRESTATILL_GIFTCARD_ID_ORDER_STATE',$os->id);
			return true;
		}
		return true;
	}
	
    private function _prepareNewTab()
	{
	 	$id_product = (int)Tools::getValue('id_product');
	 	$product = new Product($id_product);
	 	$link = new Link();
	    $this->context->smarty->assign(
		    array(
		        'languages' => $this->context->controller->_languages,
		        'default_language' => (int)Configuration::get('PS_LANG_DEFAULT'),
		        'the_product' => $product,
		        'cancel_link' => $link->getAdminLink('AdminProducts'),	
	    	)
		);	 
	}
	
	public function hookDisplayAdminProductsExtra($params)
	{
		$this->_prepareNewTab();		
		return $this->display(__FILE__,'hook_admin_extra_product.tpl');
	}
	    
	public function hookActionProductUpdate($params) {
		
		if(isset($_POST['prestatillgiftcard_amount']))
		{	
		 	$product = &$params['product'];
		 	
		 	if(Validate::isLoadedObject($product))
		 	{	
				$product->prestatillgiftcard_amount = Tools::getValue('prestatillgiftcard_amount');
				$product->prestatillgiftcard_partial_usage = (int)Tools::getValue('prestatillgiftcard_partial_usage');
				$product->prestatillgiftcard_minimum_amount = Tools::getValue('prestatillgiftcard_minimum_amount');
				$product->prestatillgiftcard_validity_period = Tools::getValue('prestatillgiftcard_validity_period');	
				if($product->prestatillgiftcard_amount > 0)
				{
					$product->id_tax_rules_group = 0;
				}			
			}
		}
	}
	
	/**
	 * Hook intervenant après la generation du ticket de caisse, afin de générer des autres tickets
	 * Params contains:
	 * 'ticket' => $ticket,
	 * 'ticket_caisse' => $ticket_caisse,
	 * 'extra_tickets' => &$extra_tickets
	 */
	public function hookActionPrestaTillGenerateExtraTickets($params)
	{
		
		$ticket = $params['ticket'];
		$id_order = $ticket->ps_id_order;
		
		$giftcards = PrestatillGiftCardCode::getAllByIdOrder($id_order);
		
		//on va boucler sur l'ensemble des cheque cadeau de cette commande
		foreach($giftcards as $giftcard)
		{
			$tpl = 	$this->createTemplate('gift-card-ticket.tpl');
			$params['extra_tickets'][] = $giftcard->generatePrintableGiftCardTicket($tpl, $this->context->smarty);
		}
	}
	

    public function uninstall() {
        if (
            parent::uninstall() == false
            /*
            || !$this->_uninstallModuleTab('ACaisseUSBScreen')
            || !$this->_uninstallModuleTab('ACaisseCashView')
            || !$this->_uninstallModuleTab('ACaissePageConfigure')
            || !$this->_uninstallModuleTab('ACaissePointshopConfigure')
            || !$this->_uninstallModuleTab('ACaisseUse')
            || !$this->_uninstallModuleTab('ACaisseMenu')
			*/
        ) {
            return false;
        }
        return true;
    }

    private function _registerNewTab() {
    	//faut trouvé l'id du menu "customer"
    	$id = Tab::getIdFromClassName('customer');
     	PrestaShopLogger::addLog('test install new tab into '.$id);
    	    	
    	/*
        $this->_installModuleTab('ACaisseUse',array($this->context->language->id=>'Ouvrir ma caisse'),$id);
        */

        return true;
    }
    
    /***************************************************/
    /***** Traitement principale de page la config *****/
    /***************************************************/
    public function getContent() {
        $html='';
        $debug='';
        $id_lang = (int)Context::getContext()->language->id;
        
        
        
        
        // si on a envoyé le formulaire de configuration générale du module
        if (Tools::isSubmit('submitCustomerOptions')) {
            //$html.='<pre>'.print_r($_POST,true).'</pre>';
            
            // on traite la configuration des clients
            Configuration::updateValue('ACAISSE_profilsID', Tools::getValue('profilsID'));
            Configuration::updateValue('ACAISSE_profilsIDNoChecking', Tools::getValue('profilsIDCheckingNotAllowed'));
            Configuration::updateValue('ACAISSE_defaultProfilID', Tools::getValue('defaultProfilID'));
            Configuration::updateValue('ACAISSE_searchActiveCustomer', Tools::getValue('searchActiveCustomer'));
            Configuration::updateValue('ACAISSE_useForceGroup', Tools::getValue('useForceGroup'));
			
            $tmp_search=array();
            foreach(array_merge(array('id_customer'=>true),Customer::$definition['fields']) as $key => $value) {
                if (Tools::getIsset('customerSearchFields'.$key)) {
                    $tmp_search[]=$key;
                }
            }

            Configuration::updateValue('ACAISSE_customerSearchFields',implode(';',$tmp_search));
		}
		
		
        
        $token = Tools::getAdminTokenLite('AdminModules');
        $assigns = array();
        $assigns['token'] = $token;
        $assigns['module']=$this;
        
        /*
        $assigns['PRESTATILLGIFTCARD_xxxxxxxxxxxx'] = Configuration::get('PRESTATILLGIFTCARD_xxxxxxxxxxxx');
        */
       
		
		/////////////////// END UPDATE SIREEN 2.0 //////////////////////////
		
        
		$assigns['module_version'] = $this->version;
		$assigns['module_display_name'] = $this->displayName;

        $this->context->smarty->assign($assigns);
		
        $html = $this->display(__FILE__, 'getContent.tpl');

        return $html;
    }

    
    
    /**
    * Hook permettant la suppression des produit éphémère
    */
    public function hookActionValidateOrder($params)
    {
    	$order = $params['order'];
		$cart = $params['cart'];
		$id_report = false;
		
		$id_lang = Context::getContext()->language->id;
		
    	foreach($order->product_list as $product_order)
    	{	
    		$product = new Product($product_order['id_product']);
			//on test si c'est un chèque cadeau
    		if(isset($product->prestatillgiftcard_amount) && $product->prestatillgiftcard_amount > 0)
    		{
				//On génère autant de chèque que de quantité
				//$product_order->quantity
				
				for($i = 0 ; $i < $product_order['quantity'] ; $i++)
				{
	    			$gift_card = new PrestaTillGiftCardCode();				
					$gift_card->amount = $product->prestatillgiftcard_amount;
					$gift_card->partial_usage = $product->prestatillgiftcard_partial_usage;
					$gift_card->minimum_amount = $product->prestatillgiftcard_minimum_amount;
					$gift_card->name = $product->name[$id_lang];
					$gift_card->id_order = $order->id;
					$gift_card->id_product = $product_order['id_product'];
					$gift_card->id_product_attribute = $product_order['id_product_attribute'];
					$gift_card->num = $i;					
					$gift_card->code128 = PrestaTillGiftCardCode::getUniqueCode128();
					$gift_card->from_id_customer = $order->id_customer;
					$gift_card->short_description = $product->description_short[$id_lang];
					$gift_card->description = $product->description[$id_lang];				
					$gift_card->period_validity = $product->prestatillgiftcard_validity_period;
					$gift_card->last_day_validity = date('Y/m/d H:i:s',strtotime($product->prestatillgiftcard_validity_period));
					$gift_card->amount_used = 0;
					$gift_card->real_amount_used = 0;
					$gift_card->amount_available = $product->prestatillgiftcard_amount;
					$gift_card->id_pointshop = $cart->id_pointshop;
					
					if(Module::isEnabled('prestatillexportcompta'))
					{
						$day = date('Y-m-d');
				
						// On récupère le jour du rapport en cours
						$id_report = PrestatillFinancialReport::ReportExists($day, $cart->id_pointshop);
						if($id_report != false)
						{
							$report = new PrestatillFinancialReport($id_report);
							if(Validate::isLoadedObject($report))
							{
								$gift_card->id_service = $report->id_service;
								$gift_card->id_financial_report = $report->id;
								$gift_card->day = $report->day;
							}
						}
					}
					
					$gift_card->save();
				}

				// On met à jour le champs is_giftcard sur orderDetail
				$update_order_detail = PrestaTillGiftCardCode::updateOrderDetail($order->id, $product->id);
    		}    		
    	}
    }
	
	/**
	 * Injection JS dans le header de la caisse
	 */
	public function hookDisplayPrestatillInjectJS($params)
	{
		return '<script src="'._MODULE_DIR_.$this->name.'/js/prestatill.js"></script>';
	}
	
	/**
	 * Injection CSS dans le header de la caisse
	 */
	public function hookDisplayPrestatillInjectCSS($params)
	{
		return '<link  href="'._MODULE_DIR_.$this->name.'/css/prestatill.css" rel="stylesheet" type="text/css" media="all" />';		
	}	
	
	/**
	 * Affichage du boutton de paiement
	 */
	public function hookDisplayPrestatillPaymentButton($params)
	{
		//$this->context->smarty->assign($assigns);
	    $html = $this->createTemplate('prestatill_payment_button.tpl')->fetch();
        return $html;
	}	
	
	/**
	 * Affichage d'une interface de saisie du montant (non obligatoire, si pas d'interface de saisie : exemple CB)
	 * l'interface doit soit d'une colonne afin d'etre compatible avec la vue paiement multiple
	 */
	public function hookDisplayPrestatillPaymentInterface($params)
	{
		$this->context->smarty->assign('secret_tocken',htmlentities(self::SECRET_TOCKEN));	
		$url = $this->context->link->getModuleLink(
			$this->name,
			'getgiftcard',
			array(
				'tocken' => self::SECRET_TOCKEN
			),
			Configuration::get('PS_SSL_ENABLED')
		);
		$this->context->smarty->assign('url_ajax',$url);
					
		
	    $html = $this->createTemplate('prestatill_payment_interface.tpl')->fetch();
        return $html;
	}
	
    

    /**
     * Méthode pour installer un nouvel onglet dans le menu
     * 
     * @param string    $tabClassName
     * @param array     $tabLabel
     * @param int       $parent_tab_id
     * @param string    $module_name
     * @return id_new_tab
     */
    private function _installModuleTab($tabClassName, $tabLabel, $parent_tab_id = 0, $module_name = NULL) {
        $tab = new Tab();
        $tab->name = $tabLabel;
        $tab->class_name = $tabClassName;
        $tab->id_parent = $parent_tab_id;
        $tab->module = $module_name === NULL ? $this->name : $module_name;
        $tab->add();
        return $tab->id;
    }

    /**
     * Méthode pour désinstaller un onglet dans le menu
     * 
     * @param string $tabClass
     * @return boolean
     */
    private function _uninstallModuleTab($tabClass) {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            return $tab->delete();
        }
        return false;
    }

    private function _sendMail($tplVar, $recipients = null, $template, $message) {
        Mail::Send(
            $this->context->language->id, //lang
            $template, //template
            $message, //sujet
            $tplVar, $recipients, //@TODO (doit être un array pour plusieurs destinataire
            '-', //to_name
            strval(Configuration::get('PS_SHOP_NAME')), //from
            strval(Configuration::get('PS_SHOP_NAME')), //from name
            null, //file attachment
            null, //mod smtp
            $this->getLocalPath() . 'mails/' //tpl path
        );
    }

    
    private function _installSql()
    {
        include(dirname(__FILE__).'/sql/install.php');
        $result = true;
	    foreach ($sql as $request){
            if (!empty($request))
            $result &= Db::getInstance()->execute(trim($request));
        }
        return $result;
    }

    //surcharge pour tester aussi le dossier view dans le module
    public function createTemplate($tpl_name,$type = 'hook')
    {
    	//d(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    	//d(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    		
    	
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        return parent::createTemplate($tpl_name);
    }

    public function getTplPath($tpl_name)
    {
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name;
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name;
        }
    }
}
