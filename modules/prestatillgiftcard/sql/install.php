<?php
/**
 * On installe toutes les tables de bases avec les champs jusqu'à la 1.3
 * Puis on teste un par un pour ajouter les nouveaux champs s'ils n'y figurent pas déjà
 */

$sql = array();
	
$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'prestatill_gift_card_code`(
            `id_prestatill_gift_card_code` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
            `name` VARCHAR(250) NOT NULL,
            `description` TEXT NOT NULL,
            `short_description` TEXT NOT NULL,
            `code128` TEXT NOT NULL,
            `amount` VARCHAR(10) NOT NULL DEFAULT "0",
            `partial_usage` INT(10) UNSIGNED NOT NULL DEFAULT "1",           
            `minimum_amount` VARCHAR(10) NOT NULL DEFAULT "0",
            `from_id_customer` INT(10) UNSIGNED NOT NULL,        
            `id_order` INT(10) UNSIGNED NOT NULL,     
            `id_product` INT(10) UNSIGNED NOT NULL,     
            `id_product_attribute` INT(10) UNSIGNED NOT NULL,     
            `num` INT(10) UNSIGNED NOT NULL,            
            `period_validity` VARCHAR(250) NOT NULL DEFAULT "+1 years",
            `last_day_validity` VARCHAR(20) NOT NULL DEFAULT "0000-00-00 00:00:00",
            `amount_used` VARCHAR(10) NOT NULL DEFAULT "0",
            `real_amount_used` VARCHAR(10) NOT NULL DEFAULT "0",
            `amount_available` VARCHAR(10) NOT NULL DEFAULT "0",            
            `date_add` VARCHAR(20) NOT NULL DEFAULT "0000-00-00 00:00:00",
            `date_upd` VARCHAR(20) NOT NULL DEFAULT "0000-00-00 00:00:00"
			)

            ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci';	
			
			
			
			
		$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'prestatill_gift_card_code_usage`(
				  `id_prestatill_gift_card_code_usage` INT NOT NULL AUTO_INCREMENT,
				  `id_prestatill_gift_card_code` INT NOT NULL DEFAULT \'0\',
				  `id_employee` INT NOT NULL DEFAULT \'0\',
				  `amount` VARCHAR(10) NOT NULL DEFAULT \'0\',
				  `use_for_id_ticket` INT NOT NULL DEFAULT \'0\',
				  `use_for_id_order` INT NOT NULL DEFAULT \'0\',
				  `use_for_id_customer` INT NOT NULL DEFAULT \'0\',
				  `use_for_id_pointshop` INT NOT NULL DEFAULT \'0\',
				  `use_for_id_service` INT NOT NULL DEFAULT \'0\',	
				  `date_add` VARCHAR(25) NOT NULL,
				  `date_upd` VARCHAR(25) NOT NULL,
				  PRIMARY KEY(`id_prestatill_gift_card_code_usage`)
				) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_unicode_ci';
			
			
							
		/*				
$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'a_caisse_page`(
            `id_a_caisse_page` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `id_pointshop` INT(11) NOT NULL,
            `is_main` TINYINT(4) NOT NULL DEFAULT "0",
            `name` VARCHAR(255) NOT NULL,
            `description` TEXT NOT NULL,
            `active` INT(11) NOT NULL DEFAULT "1")
            ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci';
*/


			
/**
 * MISES A JOUR DES CHAMPS DEPUIS LA VERSION 1.3
 * ALTER TABLE UNIQUEMENT SI LE CHAMPS N'EXISTE PAS
 */
$tables = 	array(
				'product' => 
					array(
						'prestatillgiftcard_validity_period' => 'VARCHAR(250) NOT NULL DEFAULT \'+1 year\'',
						'prestatillgiftcard_amount' => 'INT(11) NOT NULL DEFAULT 0',
						'prestatillgiftcard_partial_usage' => 'INT(11) NOT NULL DEFAULT 1',
						'prestatillgiftcard_minimum_amount' => 'INT(11) NOT NULL DEFAULT 0',
            			'id_pointshop' => 'INT(10) UNSIGNED NOT NULL DEFAULT 0',
					),
				'prestatill_gift_card_code' => 
					array(
            			'id_pointshop' => 'INT(10) UNSIGNED NOT NULL DEFAULT 0',
            			'id_financial_report' => 'INT(10) UNSIGNED NOT NULL DEFAULT 0',
            			'id_service' => 'INT(10) UNSIGNED NOT NULL DEFAULT 1',
            			'day' => 'VARCHAR(10) NOT NULL DEFAULT \'0000-00-00\'',
					),
				/* SINCE 2.4.1 */
				'prestatill_gift_card_code_usage' => 
					array(
            			'id_financial_report' => 'INT(10) UNSIGNED NOT NULL DEFAULT 0',
            			'day' => 'VARCHAR(10) NOT NULL DEFAULT \'0000-00-00\'',
					),
				); 
						
/*
 * TRAITEMENT DES CHAMPS
 */				
foreach ($tables as $table => $fields)
{
	foreach ($fields as $field => $type)
	{
		$sql[] = 'SET @s = (SELECT IF( (SELECT COUNT(column_name)
			        FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = "' . _DB_PREFIX_ .$table. '"
			        AND table_schema = "'._DB_NAME_.'" AND column_name = "'.$field.'"
			    ) > 0, "SELECT 1", "ALTER TABLE ' . _DB_PREFIX_ .$table. ' ADD '.$field.' '.$type.'"
			)); 
		PREPARE stmt FROM @s; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;';
	}
}			