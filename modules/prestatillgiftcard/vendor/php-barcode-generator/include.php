<?php

require_once('src/Exceptions/BarcodeException.php');
require_once('src/Exceptions/InvalidCharacterException.php');
require_once('src/Exceptions/InvalidCheckDigitException.php');
require_once('src/Exceptions/InvalidFormatException.php');
require_once('src/Exceptions/InvalidLengthException.php');
require_once('src/Exceptions/UnknownTypeException.php');
require_once('src/BarcodeGenerator.php');
require_once('src/BarcodeGeneratorPNG.php');
require_once('src/BarcodeGeneratorJPG.php');
require_once('src/BarcodeGeneratorSVG.php');
require_once('src/BarcodeGeneratorHTML.php');