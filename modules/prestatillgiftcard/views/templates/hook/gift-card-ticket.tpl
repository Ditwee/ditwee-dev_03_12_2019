<div class="GiftCard" style="page-break-before:always; page-break-after:always;">
	<button class="print" style="margin-bottom:20px;"><i class="fa fa-print fa-2x"></i> Imprimer </button>
	<style>
	todo {
	  font-family:FontB21, "Open Sans", "sans-serif" !important;
	}
	
	.GiftCard {
		background: #FFF;
	    position: relative;
	    margin: 20px auto;
	    padding-top:30px;
	}
	</style>
	{* head du ticket *}
	
	<div style="font-size:110%">
	  {$TICKET_HEADER}
	</div>
	
	<br />
	<br />
	
	<br />
	
	<h2 class="avoirtitle">** {l s='CHEQUE CADEAU' mod='prestatillgiftcard'} **</h2>
	
	{if !empty($customer)}
	  <h3>Client : {$customer->firstname} {$customer->lastname}</h3>
	  <h4>{if $customer->company != null}Société : {$customer->company}{/if}</h4>
	  <h4>N° de client : {$customer->id}</h4>
	  <br />
	{/if}
	
	
	<h2 class="avoirtitle extra-bi">{$giftcard->amount}{$currency}</h2>
	<h2 class="avoirtitle big">Code {$giftcard->code128}</h2>
	
	<p>{l s='Date de fin de validité : ' mod='prestatillgiftcard'} {$display_end_date_validity}</p>
	{if $giftcard->minimum_amount > 0}
	<p>{l s='Pour tout achat égal ou supérieur à' mod='prestatillgiftcard'} {$giftcard->minimum_amount}{$currency}</p>
	{/if}
	{if $giftcard->partial_usage == 1}
	<p>{l s='Utilisable en plusieurs fois' mod='prestatillgiftcard'}</p>
	{else}
	<p>{l s='Utilisable en une seule fois' mod='prestatillgiftcard'}</p>
	{/if}
	
	<br />
	<img src="data:image/png;base64,{$barcode_png}" width="70%" />
	
	
	<br />
	
	<div style="font-size:110%">
	  {* $TICKET_FOOTER *}
	</div>
	
	<br />
	<br />
	<br /><span style="color:#FFF;">.&nbsp;</span>
	{*<hr />
	<pre>
	<hr />
	{$barcode_html|@print_r}
	<hr />
	{$order|@print_r}
	<hr />
	{$order|@print_r}
	{$slip|@print_r}
	<hr />
	{$products|@print_r}
	<hr />
	{$order|@print_r}
	*}
</div>