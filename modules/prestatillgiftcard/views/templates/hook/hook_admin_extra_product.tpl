<style>
a#link-ModuleAcaisse:focus {
	background-color: #1f1f20;
    color: #FF005F;
	border-color:#1f1f20;
}
a#link-ModuleAcaisse:before {
    /* content: "f"; */
    content: "\e900  ";
    font-family: 'lbabcaisse';
}
</style>
<div id="product-ModuleAcaisse" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="ModuleAcaisse">
	<h3>Configuration d'un chèque cadeaux</h3>
	
	<h4>Valeur</h4>
	<div class="form-group">
		<label class="control-label col-lg-3" for="prestatillgiftcard_amount">
			<span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="En chèque sera automatiquement créer à chaque valdiation d'un achat de ce produit.
		Pour désactiver la génération d'un chèque mettre comme valeur 0."> Valeur du chèque</span>
		</label>
		<div class="input-group col-lg-2">
			<input maxlength="14" id="width" name="prestatillgiftcard_amount" type="text" value="{$the_product->prestatillgiftcard_amount|escape:'html'}" onkeyup="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.');">
			<span class="input-group-addon"> € TTC</span>
		</div>
	</div>
	
	
	
	<h4>Limites d'utilisation</h4>
	
	<!--exemple switch on/off-->
  <input name="prestatillgiftcard_partial_usage" type="hidden" value="1" />
  <input name="prestatillgiftcard_minimum_amount" type="hidden" value="0" />

	{*
	
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">
</span></div>
		<label class="control-label col-lg-2" for="prestatillgiftcard_partial_usage">
			<span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="En activant cette option, un chèque pourra être utilisé en plusieurs fois, dans la limite des options suivantes"> Autoriser l'utilisation partielle</span>
		</label>
		<div class="col-lg-9">
			<span class="switch prestashop-switch fixed-width-lg">
				<input onclick="" type="radio" name="prestatillgiftcard_partial_usage" id="prestatillgiftcard_partial_usage_on" value="1"{if $the_product->prestatillgiftcard_partial_usage == 1} checked="checked"{/if}>
				<label for="prestatillgiftcard_partial_usage_on" class="radioCheck">
					Oui
				</label>
				<input onclick="" type="radio" name="prestatillgiftcard_partial_usage" id="prestatillgiftcard_partial_usage_off" value="0"{if $the_product->prestatillgiftcard_partial_usage == 0} checked="checked"{/if}>
				<label for="prestatillgiftcard_partial_usage_off" class="radioCheck">
					Non
				</label>
				<a class="slide-button btn"></a>
			</span>
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-lg-3" for="prestatillgiftcard_minimum_amount">
			<span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Laissez 0 pour désactiver la limitation lié au total d'achat"> Achat minimum de</span>
		</label>
		<div class="input-group col-lg-2">
			<input maxlength="14" id="width" name="prestatillgiftcard_minimum_amount" type="text" value="{$the_product->prestatillgiftcard_minimum_amount|escape:'html'}" onkeyup="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.');">
			<span class="input-group-addon"> € TTC (hors frais de port)</span>
		</div>
	</div>
	*}
	
	<div class="form-group">
		<div class="col-lg-1"><span class="pull-right">
</span></div>
		
		<label class="control-label col-lg-2" for="prestatillgiftcard_validity_period">
			<span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Utilisation de la syntaxe de la méthode PHP strtotime (+1years, +3 months ou encore +365 days)"> Durée de validité du chèque</span>
		</label>
		
		<div class="input-group col-lg-2">		
		  <select name="prestatillgiftcard_validity_period">
		    {for $i=1 ; $i<=24 ; $i++}
		    {assign var="key" value="+{$i} month"}
		    <option value="+{$i} month" {if $key == $the_product->prestatillgiftcard_validity_period} selected="selected"{/if}>{$i} mois</option>
		    {/for}
		  </select>
		</div>
	</div>
	
	
	{* exemple switch on/off*}
	
	
{*
	
	
*}
	

	<div class="panel-footer">
		<a href="{$cancel_link}" class="btn btn-default"><i class="process-icon-cancel"></i> Annuler</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> Enregistrer</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> Enregistrer et rester</button>
	</div>
</div>