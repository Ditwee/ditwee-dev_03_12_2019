<div class="prestatill_payment_interface" data-module-name="prestatillgiftcard" data-url-ajax="{$url_ajax}">
  <div class="ui-tab ui-tab-horizontal" id="prestatillgiftcard-tab-select">
    <ul class="ui-tab-menu">
      <li data-target=".prestatillgiftcard-from-client" class="current">{l s='Chèques cadeaux du client' mod='prestatillgiftcard'}</li>
      <li data-target=".prestatillgiftcard-by-barcode">Saisie manuelle</li>
    </ul>
    <div class="ui-tab-content">
      <div class="ui-tab-item prestatillgiftcard-from-client current">
        Loading...
      </div>
      <div class="ui-tab-item prestatillgiftcard-by-barcode">
        Saisi du code barre indiqué sur le {l s='chèque cadeau' mod='prestatillgiftcard'} : 
        <input style="margin:10px;">
        <div class="results"></div>
      </div>
    </div>
  </div>
</div>
<script>
  $(function(){
    $('#prestatillgiftcard-tab-select').uiTab({
      'onChange' : function(plugin,target,$target) {
        switch(target)
        {
          case '.prestatillgiftcard-by-barcode':
            $('input',$target).val('').focus();
            break;
          default:
            
            break;
        }
      
      },
    });
  });
</script>
