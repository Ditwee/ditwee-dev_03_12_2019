	<div style="text-transform: uppercase;text-align: left;">{l s='Chèques cadeau' mod='prestatillexportcompta'}</div>
	<div class="separate"></div>
		{if !empty($gift_usage)}
		<table  style="width:100%;">
			<thead>
				<tr>
					<td class="text-left" colspan="6" style="text-transform: uppercase;">Chèques cadeau utilisés</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="text-left" style="text-transform: uppercase;">Ref. Commande</td>
					<td class="text-right" style="text-transform: uppercase;">Montant</td>
				</tr>
				{assign var=tot_reliquat value=0}
				{foreach from=$gift_usage item=detail name=foo}
				<tr>
					<td class="text-left">{$detail.reference}</td>
					<td class="text-right">{$detail.use_amount/100}Eur</td>
				</tr>
				{assign var=tot_reliquat value=$tot_reliquat+$detail.amount}
				{/foreach}
			</tbody>
		</table>
		{else}
		{l s='Chèques cadeau utilisés' mod='prestatillexportcompta'}
		<br />
		<br />
		<span style="display:block;" class="alert alert-info">{l s='Aucun chèque cadeau utilisé ce jour' mod='prestatillexportcompta'}</span>
		{/if}
	<br />
	<div class="separate"></div>
	<br />
	<div class="table-responsive">
		{if !empty($gift_buying)}
		<table style="width:100%;">
			<thead>
				<tr>
					<td class="text-left" colspan="8" style="text-transform: uppercase;">{l s='Chèques cadeau édités' mod='prestatillexportcompta'}</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="text-left" style="text-transform: uppercase;">{l s='Code du bon' mod='prestatillexportcompta'}</td>
					<td class="text-center" style="text-transform: uppercase;">{l s='N° cmd' mod='prestatillexportcompta'}</td>
					<td class="text-right" style="text-transform: uppercase;">{l s='Montant' mod='prestatillexportcompta'}</td>
				</tr>
				{foreach from=$gift_buying item=detail name=foo}
				<tr>
					<td class="text-left">{$detail.code128}</td>
					<td class="text-center">{$detail.id_order}</td>
					<td class="text-right">{$detail.amount}Eur</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
		{else}
		<div style="text-transform: uppercase;text-align: left;">{l s='Chèques cadeau étidés' mod='prestatillexportcompta'}</div>
		<div class="separate"></div>
		<span style="display:block;" class="alert alert-info">{l s='Aucun chèque cadeau édité ce jour' mod='prestatillexportcompta'}</span>
		{/if}
	</div>
<div class="separate_double"></div>