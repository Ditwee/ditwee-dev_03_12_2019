{*
* 2007-2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*}

<div class="clearfix"></div>
<h3>
    <i class="icon-cogs"></i> {l s='Paramètres généraux' mod='prestatillgiftcard'} <small>{$module_display_name|escape:'htmlall':'UTF-8'}</small>
</h3>
<form role="form" class="form-horizontal"  action="#" method="POST">
		<h4>{l s='ICI un titre' mod='prestatillgiftcard'}</h4>
		<div class="form-group">ici des trucs à configurer
		</div>
		{*
		<div class="form-group">
			<label class="control-label col-lg-4" for="FORCE_DEFAULT_CARRIER">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Activez cette option pour passer outre les paramètres de frais de port de Prestashop ;'}">
						{l s='Désactiver les frais de transport en point de vente' mod='prestatillgiftcard'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="FORCE_DEFAULT_CARRIER" id="FORCE_DEFAULT_CARRIER_on" value="1" {if $ACAISSE_FORCE_DEFAULT_CARRIER==1}checked{/if}>
						<label for="FORCE_DEFAULT_CARRIER_on" class="radioCheck">
							Oui
						</label>
						<input type="radio" name="FORCE_DEFAULT_CARRIER" id="FORCE_DEFAULT_CARRIER_off" value="0" {if $ACAISSE_FORCE_DEFAULT_CARRIER==0}checked{/if}>
						<label for="FORCE_DEFAULT_CARRIER_off" class="radioCheck">
							Non
						</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>
		</div>	
        *}
		<div class="panel-footer">
            <div class="btn-group pull-right">
                <button name="submitGeneralOptions" id="submitGeneralOptions" type="submit" class="btn btn-default"><i class="process-icon-save"></i> {l s='Save' mod='prestatillgiftcard'}</button>
            </div>
        </div>
</form>