<?php

class AdminPrestaTillLabelStoreConfigureController extends ModuleAdminController
{
    public function __construct()
    {  
        $this->display = 'view'; //view,list or form

        $this->meta_title = $this->l('Etiquettes PrestaTill');
        
        parent::__construct();
    }
    
    //surcharge pour tester aussi le dossier view dans le module
    public function createTemplate($tpl_name)
    {
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name) && $this->viewAccess()) {
            return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name, $this->context->smarty);
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->module->name.'/views/templates/admin/'.$tpl_name) && $this->viewAccess()) {
            return $this->context->smarty->createTemplate(_PS_MODULE_DIR_.''.$this->module->name.'/views/templates/admin/'.$tpl_name, $this->context->smarty);
        }
        return parent::createTemplate($tpl_name);
    }
    
    public function initContent()
    {
        parent::initContent();
        
        // là je ne suis pas sûr que ça marche comme pour le renderTPL... A tester
        $this->fields_form = array(
           'input' => array(
                array(
                     'type' => 'number',
                     'name' => 'margin',
                     'required' => true,
                     'label' => $this->l("Marges externes (en px) :"),
                ),
                array(
                     'type' => 'number',
                     'name' => 'padding',
                     'required' => true,
                     'label' => $this->l("Marges internes (en px) :"),
                ),
           ),
           'submit' => array(
                'title' => $this->l('   Enregistrer   '),
                'class' => 'button'
           )
       );
        
       return parent::renderForm();
    }
}
        