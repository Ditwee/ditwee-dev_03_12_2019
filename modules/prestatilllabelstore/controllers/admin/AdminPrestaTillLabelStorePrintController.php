<?php

class AdminPrestaTillLabelStorePrintController extends ModuleAdminController
{
	const DEFAULT_MAX_PRINTABLE_ATTRIBUTS = 2;
	const DEFAULT_NB_COLUMN = 1;
	const DEFAULT_NB_LINE = 1;
	
	
	const DEFAULT_LABEL_WITH_BORDER = false;
	
	const AUTO_CREATE_PRODUCT_EAN13 = true;
	const AUTO_CREATE_ATTRIBUTE_EAN13 = true;
	
    private $_path = '/modules/prestatilllabelstore/';

    public function __construct()
    {
        $this->display = 'form'; //view,list or form

        $this->meta_title = $this->l('Etiquettes PrestaTill');
        
        parent::__construct();
    }


    //surcharge pour tester aussi le dossier view dans le module
    public function createTemplate($tpl_name)
    {
    	if(file_exists(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name) && $this->viewAccess()) {
    		return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->module->name.'/views/templates/admin/'.$tpl_name, $this->context->smarty);
    	}
    	else if(file_exists(_PS_MODULE_DIR_.''.$this->module->name.'/views/templates/admin/'.$tpl_name) && $this->viewAccess()) {
    		return $this->context->smarty->createTemplate(_PS_MODULE_DIR_.''.$this->module->name.'/views/templates/admin/'.$tpl_name, $this->context->smarty);
    	}
    	return parent::createTemplate($tpl_name);
    }
    
    public function postProcess()
    {
		$max_printable_attributs = 2;
		
		$sql = false;
		$sql_updates = array();
		
    	
    	if(Tools::isSubmit('print_label') || Tools::getValue('id_product'))
    	{
    		$datas = array();
    		
    		$id_lang = 1;
    		
    		$select = 'SELECT p.id_product as id_product, p.price, pl.name, p.reference, p.ean13, sa.quantity, m.name as manufacturer';
    		$from = ' FROM '._DB_PREFIX_.'product AS p';
    		$from .= ' LEFT JOIN '._DB_PREFIX_.'product_lang AS pl ON (p.id_product = pl.id_product AND pl.id_lang = '.$id_lang.')';
    		$from .= ' LEFT JOIN '._DB_PREFIX_.'stock_available sa ON (sa.id_product = p.id_product)';
    		$from .= ' LEFT JOIN '._DB_PREFIX_.'manufacturer m ON (p.id_manufacturer = m.id_manufacturer)';
			
    		
    		$where = ' WHERE sa.id_product_attribute = 0 ';
    		
    		if(isset($_POST['date_add']) && Validate::isDate($_POST['date_add']))
    		{
    			$where .= ' AND p.date_add >= \''.$_POST['date_add'].'\'';
    		}
    		
    		
    		if(isset($_POST['date_update']) && Validate::isDate($_POST['date_update']))
    		{
    			
    			$where .= ' AND p.date_upd >= \''.$_POST['date_update'].'\'';
    		}
    		
    		
    		if(isset($_POST['id_category']) && Validate::isInt($_POST['id_category']))
    		{
    			$where .= 'AND p.id_product IN (SELECT id_product FROM '._DB_PREFIX_.'category_product WHERE id_category = '.((int)$_POST['id_category']).')';
    		}
			
			
			if(Tools::getValue('id_product'))
    		{
    			$where .= 'AND p.id_product ='.Tools::getValue('id_product');
    		}			
			
			
			if(isset($_POST['ids_product']) && trim($_POST['ids_product']) != '')
			{
				$ids = str_replace(';',',',trim(''.$_POST['ids_product']));
				$where .= 'AND p.id_product IN ('.$ids.')';
				
			}
			
    		
    		$sql = $select.$from.$where;
			
    		
		}
    		
		if($sql !== false)
		{	
    		
    		//$sql .= ' LIMIT 0,10';
    		
    		$db = DB::getInstance(); 
    		$rows = $db->executeS($sql,null,false);
    		
    		//p($_POST);			p($sql);    		d($rows);
			
    		foreach($rows as $row)
    		{
    			//on cherche les déclinaisons
    			
    			$ids_product_attribute= Db::getInstance()->executeS('
		SELECT pa.id_product_attribute
		FROM `'._DB_PREFIX_.'product_attribute` pa
		WHERE pa.`id_product` = '.(int)$row['id_product'],true,false);
    				
    			$declinaisons = array();
    			
    			if($row['ean13'] == '' && self::AUTO_CREATE_PRODUCT_EAN13 == true)
    			{
    				$row['ean13'] = $this->_add_ean13_check_digit('3'.substr('00000000000'.$row['id_product'],-11));    				
    				$sql_updates[] = 'UPDATE '._DB_PREFIX_.'product SET ean13 = \''.$row['ean13'].'\',date_upd=\''.date('Y-m-d H:i:s').'\' WHERE id_product = '.$row['id_product'];
    			}
    			
    			
    			//il y a des déclinaisons
    			if(sizeof($ids_product_attribute)>0) 
        		{    
		            foreach($ids_product_attribute as $product_attribute)
		            {
		                $id_product_attribute=$product_attribute['id_product_attribute'];
		
		                $combi=new Combination($id_product_attribute);
		                //p($combi);
		                
		                //extraction des libelés d'attribus qui nous intéresse
		                //exemple ici id_attribute_group in [1,3]
		                $sql2 = '
						SELECT sa.quantity,pag.position,a.color,pac.id_product_attribute,al.name,pagl.public_name,pa.reference,pa.ean13,pa.upc
						FROM '._DB_PREFIX_.'product_attribute_combination pac
						LEFT JOIN '._DB_PREFIX_.'attribute a ON (pac.id_attribute = a.id_attribute)
						LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON (pa.id_product_attribute = pac.id_product_attribute)
						LEFT JOIN '._DB_PREFIX_.'stock_available sa ON (sa.id_product_attribute = pac.id_product_attribute)
						LEFT JOIN '._DB_PREFIX_.'attribute_group pag ON (a.id_attribute_group = pag.id_attribute_group)
						LEFT JOIN '._DB_PREFIX_.'attribute_group_lang pagl ON (pagl.id_attribute_group = pag.id_attribute_group AND pagl.id_lang='.(int)$id_lang.')
						LEFT JOIN '._DB_PREFIX_.'attribute_lang al ON (pac.id_attribute = al.id_attribute AND al.id_lang='.(int)$id_lang.')
						WHERE pac.id_product_attribute='.(int)$id_product_attribute.'
						
						ORDER BY pag.position ASC
						';
				                $attributes_name =  Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql2	,true,false);
				                $declinaisons[$id_product_attribute] = $attributes_name;   
					}
				}
				
				if(count($declinaisons) > 0)
				{
					foreach($declinaisons as $id_product_attribute => $declinaison)
					{
						$pretty_attributes = array();
						$pretty_attributes_name = array();
						
						$count_pretty_attribute = 0;
						foreach($declinaison as $tmp)
						{
							$pretty_attributes[] = $tmp['name'];
							$pretty_attributes_name[] = $tmp['public_name'];
							$count_pretty_attribute++;
							
							if($count_pretty_attribute >= self::DEFAULT_MAX_PRINTABLE_ATTRIBUTS) { break; }
						}
							
						$ean = $declinaison[0]['ean13'];

						if($ean == '' && self::AUTO_CREATE_ATTRIBUTE_EAN13 == true)
						{
							$lp = strlen(''.$row['id_product']);
							$la = strlen(''.$id_product_attribute);
							$lmiddle = 11-$lp-$la;
							$ean = $this->_add_ean13_check_digit('4'.$row['id_product'].str_repeat('0',$lmiddle).$id_product_attribute);
							$sql_updates[] = 'UPDATE '._DB_PREFIX_.'product_attribute SET ean13 = \''.$ean.'\' WHERE id_product_attribute = '.$id_product_attribute;
							$sql_updates[] = 'UPDATE '._DB_PREFIX_.'product SET date_upd=\''.date('Y-m-d H:i:s').'\' WHERE id_product = '.$row['id_product'];
							
						}
						elseif($ean == '')
						{
							$ean = $row['ean13'];
						}
						
						
						//calcule ici du prix spéciique en carton
						 //Liste des données servant à calculé les prix de chaque
				        // prix déclinaison
				        $id_shop=1;
				        $usetax = true;
				        //$id_product_attribute = $id_product_attribute;
				        $decimals = 5;
						$id_group = 1;
				        $divisor = null;
				        $only_reduc = false;
				        $usereduc = true;
				        $quantity = 6;
				        $force_associated_tax = false;
				        $id_customer = null;
				        $id_cart = null;
				        $id_address = null;
				        $specific_price_output = null;
				        $with_ecotax = true;
				        $use_group_reduction = true;
				        $context = null;
				        $use_customer_price = true;
				        $id_country=8; //france
				        $id_state=null;
				        $zipcode='67000';
				        $id_currency=1; //EUR
				        $only_reduc=false;
				        $specprice = null;
				        $with_ecotax=true;
				        $use_group_reduction=true;
				        $id_cart=0;
				        $real_quantity=1;
						
						 $price_ttc=Product::priceCalculation(
                            $id_shop,
                            $row['id_product'],
                            $id_product_attribute,
                            $id_country,
                            $id_state,
                            $zipcode,
                            $id_currency,
                            $id_group,
                            $quantity,
                            $usetax,
                            $decimals,
                            $only_reduc,
                            $usereduc,
                            $with_ecotax,
                            $specprice,
                            $use_group_reduction,
                            $id_customer,
                            $use_customer_price,
                            $id_cart,
                            $real_quantity
                         );
						
						
						$declinaison_datas = array(
					        'nom' => $row['name'], //'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
					        'manufacturer' => $row['manufacturer'], //'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
					        'prix' => Tools::displayPrice(1.2*$row['price']),
					        'prix_x6' => Tools::displayPrice($price_ttc),
					        'reference' => $declinaison[0]['reference'] != '' ? $declinaison[0]['reference'] : $row['reference'],
					        /*
					        'taille' => 54,
					        'couleur' => 'Khaki / Blanc',
					        */
					        'ean13' => $ean,//'7612345678900'
					        'qrcode_url' => $this->context->link->getProductLink($row['id_product']),
					        'attributes' => $pretty_attributes,
					        'attributes_name' => $pretty_attributes_name,
					        'nb_tot_print' => (isset($_GET['rayon']) && $_GET['rayon'] == true)?1:(int)$declinaison[0]['quantity'],
					    );
					    
					    if(Tools::isSubmit('manual_quantity'))
					    {
					    	$declinaison_datas['nb_tot_print'] = 0+$_POST['qu_'.$id_product_attribute];
					    }
					    
					    $datas[] = $declinaison_datas;
					    
					}
				}
				else //produits sans déclinaison
				{
					//calcule ici du prix spéciique en carton
					 //Liste des données servant à calculé les prix de chaque
			        // prix déclinaison
			        $id_shop=1;
			        $usetax = true;
			        //$id_product_attribute = $id_product_attribute;
			        $decimals = 5;
					$id_group = 1;
			        $divisor = null;
			        $only_reduc = false;
			        $usereduc = true;
			        $quantity = 6;
			        $force_associated_tax = false;
			        $id_customer = null;
			        $id_cart = null;
			        $id_address = null;
			        $specific_price_output = null;
			        $with_ecotax = true;
			        $use_group_reduction = true;
			        $context = null;
			        $use_customer_price = true;
			        $id_country=8; //france
			        $id_state=null;
			        $zipcode='67000';
			        $id_currency=1; //EUR
			        $only_reduc=false;
			        $specprice = null;
			        $with_ecotax=true;
			        $use_group_reduction=true;
			        $id_cart=0;
			        $real_quantity=1;
					
					 $price_ttc=Product::priceCalculation(
                        $id_shop,
                        $row['id_product'],
                        0,
                        $id_country,
                        $id_state,
                        $zipcode,
                        $id_currency,
                        $id_group,
                        $quantity,
                        $usetax,
                        $decimals,
                        $only_reduc,
                        $usereduc,
                        $with_ecotax,
                        $specprice,
                        $use_group_reduction,
                        $id_customer,
                        $use_customer_price,
                        $id_cart,
                        $real_quantity
                     );
					
	    			$product_datas = array(
				        'nom' => $row['name'], //'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
				        'manufacturer' => $row['manufacturer'], //'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
				        'prix' => Tools::displayPrice(1.2*$row['price']),
				        'prix_x6' => Tools::displayPrice($price_ttc),
				        'reference' => $row['reference'],
				        /*
				        'taille' => 54,
				        'couleur' => 'Khaki / Blanc',
				        */
				        'ean13' => $row['ean13'],//'7612345678900'
				        'qrcode_url' => $this->context->link->getProductLink($row['id_product']),
				        'attributes' => false,
				        'attributes_name' => false,
				        'nb_tot_print' => (isset($_GET['rayon']) && $_GET['rayon'] == true)?1:$row['quantity'],
				    );
				    
				    
				    if(Tools::isSubmit('manual_quantity'))
				    {
				    	$product_datas['nb_tot_print'] = 0+$_POST['qu_0'];
				    }					
					    
					$datas[] = $product_datas;
			    }
    		}
    				
    		
    		//d(implode(';',$sql_updates));
    		if(!empty($sql_updates))
    		{
    			Db::getInstance()->execute(implode(';',$sql_updates));
    			//faudrait redemander la rafraichissement du cache de la caisse...
    			//@TODO : ici faut redemander le rafraîchissement du cache de la caisse...
			}
    		
    		//p($_POST); p($sql); d($datas);

		//d($datas);
    		
    		$this->_generatePDF($datas);
    	}
    }

	public function _generatePDF($datas, $output = 'inline')
	{
		
		$this->_generatePDFWithFormat($datas, $output = 'inline', 'ditwee');
	}

    
    /**
     * @param string $output 'inline' ou 'attachment'
     */
    public function _generatePDFWithFormat($datas, $output = 'inline', $label_ref = '')
    {
    	
		
    	$vendor = _PS_MODULE_DIR_.''.$this->module->name.'/vendor/';
    	require_once($vendor.'fpdf.php');
    	require_once($vendor.'libs/rotate/rotate.php');
    	require_once($vendor.'libs/ean13/ean13.php');
		require_once($vendor.'libs/qrcode/qrcode.class.php');
		
		define('FPDF_FONTPATH',$vendor.'font');
    	
		$format = array(25,54);
		$orientation = 'P';
		switch($label_ref)
		{
			case '54x25.4':	
				$format = array(54,25);
				define('DEFAULT_LABEL_WIDTH',53);//70;
				define('DEFAULT_LABEL_HEIGHT',25);//36;
				define('DEFAULT_PADDING_X',0);
				define('DEFAULT_PADDING_Y',0);
				define('DEFAULT_PRINT_LEFT_MARGIN',-0);
				define('DEFAULT_PRINT_VERTICAL_MARGIN',0);
				define('DEFAULT_PRINT_TOP_MARGIN',0);
				define('DEFAULT_PRINT_HORIZONTAL_MARGIN',0);
				define('DEFAULT_PRINT_ROTATE',0);
				break;
			case 'ditwee':
				define('DEFAULT_LABEL_WIDTH',53);//70;
				define('DEFAULT_LABEL_HEIGHT',25);//36;
				define('DEFAULT_PADDING_X',0);
				define('DEFAULT_PADDING_Y',0);
				define('DEFAULT_PRINT_LEFT_MARGIN',-0);
				define('DEFAULT_PRINT_VERTICAL_MARGIN',0);
				define('DEFAULT_PRINT_TOP_MARGIN',0);
				define('DEFAULT_PRINT_HORIZONTAL_MARGIN',0);
				$format = array(25.4,54);
				$format = array(54,25.4);
				$orientation = 'L';
				define('DEFAULT_PRINT_ROTATE',0);
				
				break;				
			case '28x51':
				$format = array(51,28);
				$orientation = 'P';
				define('DEFAULT_LABEL_WIDTH',50);//70;
				define('DEFAULT_LABEL_HEIGHT',34);//36;
				define('DEFAULT_PADDING_X',0);
				define('DEFAULT_PADDING_Y',0);
				define('DEFAULT_PRINT_LEFT_MARGIN',-27);
				define('DEFAULT_PRINT_VERTICAL_MARGIN',0);
				define('DEFAULT_PRINT_TOP_MARGIN',0);
				define('DEFAULT_PRINT_HORIZONTAL_MARGIN',0);
				define('DEFAULT_PRINT_ROTATE',0);
				break;
			case '46x78':
				$format = array(78,46);
				$orientation = 'P';
				define('DEFAULT_LABEL_WIDTH',78);//70;
				define('DEFAULT_LABEL_HEIGHT',49);//36;
				define('DEFAULT_PADDING_X',0);
				define('DEFAULT_PADDING_Y',0);
				define('DEFAULT_PRINT_LEFT_MARGIN',-53);
				define('DEFAULT_PRINT_VERTICAL_MARGIN',0);
				define('DEFAULT_PRINT_TOP_MARGIN',1);
				define('DEFAULT_PRINT_HORIZONTAL_MARGIN',0);
				define('DEFAULT_PRINT_ROTATE',90);
				break;
			case 'amdv_rayon':
				$format = array(78,46);
				$orientation = 'P';
				define('DEFAULT_LABEL_WIDTH',78);//70;
				define('DEFAULT_LABEL_HEIGHT',49);//36;
				define('DEFAULT_PADDING_X',0);
				define('DEFAULT_PADDING_Y',0);
				define('DEFAULT_PRINT_LEFT_MARGIN',-53);
				define('DEFAULT_PRINT_VERTICAL_MARGIN',0);
				define('DEFAULT_PRINT_TOP_MARGIN',1);
				define('DEFAULT_PRINT_HORIZONTAL_MARGIN',0);
				define('DEFAULT_PRINT_ROTATE',90);
				break;
			case '':
			default:
				$orientation = 'P';
				$format = array(51,28);
		}
		
				
    	$pdf=new PDF_EAN13($orientation,'mm',$format);
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true);		
		$pdf->rotate(DEFAULT_PRINT_ROTATE);
		
		
		$nb_per_ligne = 1;
		$count = 0;		
		$current_line = 0;
		$current_column = 0;
		
		$label_count = 0;
		$label_total = 0;
		foreach($datas as $one_product_datas)
		{		
			for($nb_print = 0 ; $nb_print < $one_product_datas['nb_tot_print'] ; $nb_print++)
			{
				$label_total++;
			}
		}
		
		
		foreach($datas as $one_product_datas)
		{		
			for($nb_print = 0 ; $nb_print < $one_product_datas['nb_tot_print'] ; $nb_print++)
			{
				
				$this->_drawEtiquette(
					$one_product_datas,
					DEFAULT_PRINT_LEFT_MARGIN+$current_column*(DEFAULT_PRINT_HORIZONTAL_MARGIN + DEFAULT_LABEL_WIDTH),
					DEFAULT_PRINT_TOP_MARGIN+$current_line*(DEFAULT_PRINT_VERTICAL_MARGIN + DEFAULT_LABEL_HEIGHT),
					array(),
					$pdf,
					$label_ref
				);
				$label_count++;
				
				$current_column++;
				if($current_column == self::DEFAULT_NB_COLUMN)
				{
					$current_column = 0;
					$current_line++;
					if($current_line == self::DEFAULT_NB_LINE)
					{
						if($label_count<$label_total) //pas la dernière etiquette
						{
							$pdf->AddPage();
							$pdf->rotate(DEFAULT_PRINT_ROTATE);
							$current_line = 0;
							set_time_limit(15);
						}
					}
				}
			}
			
		}
		
		$pdf = $pdf->Output('S');
		header('Content-type:application/pdf');
		header('Content-Disposition:'.$output.';filename=etiquette-'.date('Y-m-d_His').'.pdf');	
		echo($pdf);
    }
    
    private function _drawEtiquette($datas = null,$x,$y,$options,$pdf, $label_ref = '')
	{
		switch($label_ref)
		{
			case '54x25.4':
				$this->_drawEtiquette54x25_4($datas, $x, $y, $options, $pdf);
				break;	
			case 'ditwee':
				$this->_drawEtiquetteDitwee54x25_4($datas, $x, $y, $options, $pdf);
				break;				
			case '28x51':
				$this->_drawEtiquette28x51($datas, $x, $y, $options, $pdf);
				break;
			case '46x78':
				$this->_drawEtiquette46x78($datas, $x, $y, $options, $pdf);
				break;
			case 'amdv_rayon':
				$this->_drawEtiquetteAMDVRayon($datas, $x, $y, $options, $pdf);
				break;
			case '':
			default:
				$this->_drawEtiquette28mm51mm($datas, $x, $y, $options, $pdf);
		}
	}
	
	private function _drawEtiquette54x25_4($datas = null,$x,$y,$options,$pdf)
	{
		$x += DEFAULT_PADDING_X;
		$y += DEFAULT_PADDING_Y;
		
		$with_border = self::DEFAULT_LABEL_WITH_BORDER;			
		
		if($datas === null)
		{
		    $datas = array(
		        'nom' => 'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
		        'prix' => 100.30,
		        'attributes' => false,
		        'couleur' => 'Khaki / Blanc',
		        'ean13' => '1684646846544',//'7612345678900'
		        'qrcode_url' => 'http://slash-store.com/fr/sneakers/362-adidas-gazelle-bleu-argent.html'
		    );
		}
	    $couleur_impression_r = 0;
	    $couleur_impression_v = 0;
	    $couleur_impression_b = 0; 
	    
	    $qr_size = 25-2*DEFAULT_PADDING_X;
	    $qr_bg_color = array(255,255,255);
	    $qr_fg_color=array(0,0,0);
	    $qr_x = 44;
	    $qr_y = 10;
	    
	    $pdf->SetTextColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    $pdf->SetFillColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    
	        
	    //le nom du produit    
	    $pdf->SetFont('arial','B',11);
	    $pdf->Text($x+1,$y+5,iconv('UTF-8', 'windows-1252',$datas['nom']));
	    $pdf->SetFillColor(255,255,255);
	    
	    $pdf->Rect($x+DEFAULT_LABEL_WIDTH,$y,300,DEFAULT_LABEL_HEIGHT,'F');
	    
	    $delta_y_texte = -8;
	    
	    //la reference
	    if(isset($datas['reference']) && !empty($datas['reference']))
	    {
	        $pdf->SetFont('arial','',7);
	        $pdf->Text($x+1,$delta_y_texte+$y+17,iconv('UTF-8', 'windows-1252','Référence'));
	        $pdf->SetFont('arial','',9);
	        $pdf->Text($x+1,$delta_y_texte+$y+20.5,iconv('UTF-8', 'windows-1252',$datas['reference']));
	    }
	    //le prix
	    if(isset($datas['prix']) && !empty($datas['prix']))
	    {
	        $pdf->SetFont('arial','',7);
	        $pdf->Text($x+1,$delta_y_texte+$y+24,iconv('UTF-8', 'windows-1252','Prix'));
	        $pdf->SetFont('arial','',11);
	        $pdf->Text($x+1,$delta_y_texte+$y+28.2,iconv('UTF-8', 'windows-1252',$datas['prix'].' EUR'));
	    }
	    
	    //le code ean13
	    if(isset($datas['ean13']) && !empty($datas['ean13']))
	    {
		    $pdf->SetFillColor(0,0,0);
		    $pdf->SetFont('arial','',3);
		    $pdf->EAN13($x+1,$delta_y_texte+$y+29.5,$datas['ean13'],3.2,0.45);
		}
	       
	    //bordure d'étiquette
	    if($with_border === true)
	    {
	    	$pdf->Rect($x,$y,DEFAULT_LABEL_WIDTH,DEFAULT_LABEL_HEIGHT,'D');
		}
	}
    
	
	
	private function _drawEtiquetteDitwee54x25_4($datas = null,$x,$y,$options,$pdf)
	{
		return $this->_drawEtiquetteDitwee54x25_4_V2($datas, $x, $y, $options, $pdf);
		
		$x += DEFAULT_PADDING_X;
		$y += DEFAULT_PADDING_Y;
		
		$with_border = self::DEFAULT_LABEL_WITH_BORDER;			
		
		//concatenation des attribus
		$attributes_name = ''; 
		foreach($datas['attributes'] as $attribute)
		{
			$attributes_name .= ' ' . $attribute;
		}
		$attributes_name = trim($attributes_name);
	
		if($datas === null)
		{
		    $datas = array(
		        'nom' => 'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
		        'prix' => 100.30,
		        'attributes' => false,
		        'couleur' => 'Khaki / Blanc',
		        'ean13' => '1684646846544',//'7612345678900'
		        'qrcode_url' => 'http://slash-store.com/fr/sneakers/362-adidas-gazelle-bleu-argent.html'
		    );
		}
	    $couleur_impression_r = 0;
	    $couleur_impression_v = 0;
	    $couleur_impression_b = 0; 
	    
	    $qr_size = 25-2*DEFAULT_PADDING_X;
	    $qr_bg_color = array(255,255,255);
	    $qr_fg_color=array(0,0,0);
	    $qr_x = 44;
	    $qr_y = 10;
	    
	    $pdf->SetTextColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    $pdf->SetFillColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    
        $pdf->SetMargins(1.7,1.7,1.7);
	    
	    
	    //le nom du produit
	    $pdf->SetXY(1.7,3.5);    
	    $pdf->SetFont('arial','B',9);
	    //$pdf->Text($x+1,$y+5,iconv('UTF-8', 'windows-1252',$datas['nom']));
		$pdf->Cell(
			0,				//width
			0,				//height
			iconv('UTF-8', 'windows-1252',$datas['nom']),	//txt
			0,				//border
			1,				//1 to the beginning of the next line
			'L'				//align L, C, R
		); 
		$pdf->SetFillColor(255,255,255);
	    $pdf->Rect($x+DEFAULT_LABEL_WIDTH,$y,300,DEFAULT_LABEL_HEIGHT,'F');
	    
	    $delta_y_texte = -8;
	    
		
	    //manufacturer
	    if(isset($datas['manufacturer']) && !empty($datas['manufacturer']))
	    {
	        $pdf->SetFont('arial','',9);
	        //$pdf->Text($x+1,$delta_y_texte+$y+17.2,iconv('UTF-8', 'windows-1252',$datas['manufacturer']));
			$pdf->SetXY(1.7,6.8);   
			$pdf->Cell(
				48,				//width
				0,				//height
				iconv('UTF-8', 'windows-1252',$datas['manufacturer']),	//txt
				0,				//border
				1,				//1 to the beginning of the next line
				'R'				//align L, C, R
			); 
		}
		
		
	    //la PRIX
        $pdf->SetFont('arial','B',10);
        //$pdf->Text($x+1,$delta_y_texte+$y+17.2,iconv('UTF-8', 'windows-1252',$datas['manufacturer']));
		$pdf->SetXY(1.7,20.5);   
		$pdf->Cell(
			48,				//width
			0,				//height
			iconv('UTF-8', 'windows-1252',$datas['prix'].' EUR'),	//txt
			0,				//border
			1,				//1 to the beginning of the next line
			'R'				//align L, C, R
			
		);
		
		
	    //la reference
	    if(isset($datas['reference']) && !empty($datas['reference']))
	    {
	        $pdf->SetFont('arial','',8);
	        //$pdf->Text($x+1,$delta_y_texte+$y+17.2,iconv('UTF-8', 'windows-1252',$datas['manufacturer']));
			$pdf->SetXY(1.7,17.5);   
			$pdf->Cell(
				48,				//width
				0,				//height
				iconv('UTF-8', 'windows-1252',$datas['reference'] . ($attributes_name==''?'':'('.$attributes_name.')')),	//txt
				0,				//border
				1,				//1 to the beginning of the next line
				'L'				//align L, C, R
			);			
	    }
	    //le code ean13
	   	if(isset($datas['ean13']) && !empty($datas['ean13']))
	    {
		    $pdf->SetFillColor(0,0,0);
		    $pdf->SetFont('arial','',3);
		    $pdf->EAN13(3,9.5,$datas['ean13'],3.2,0.45);
		}
		
		return;
	    //bordure d'étiquette
	    if($with_border === true)
	    {
	    	$pdf->Rect($x,$y,DEFAULT_LABEL_WIDTH,DEFAULT_LABEL_HEIGHT,'D');
		}
	}
    
	
	private function _drawEtiquetteDitwee54x25_4_V2($datas = null,$x,$y,$options,$pdf)
	{
		$x += DEFAULT_PADDING_X;
		$y += DEFAULT_PADDING_Y;
		
		$with_border = self::DEFAULT_LABEL_WITH_BORDER;			
		
		//concatenation des attribus
		$attributes_name = ''; 
		foreach($datas['attributes'] as $attribute)
		{
			$attributes_name .= ' ' . $attribute;
		}
		$attributes_name = trim($attributes_name);
	
		if($datas === null)
		{
		    $datas = array(
		        'nom' => 'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
		        'prix' => 100.30,
		        'attributes' => false,
		        'couleur' => 'Khaki / Blanc',
		        'ean13' => '1684646846544',//'7612345678900'
		        'qrcode_url' => 'http://slash-store.com/fr/sneakers/362-adidas-gazelle-bleu-argent.html'
		    );
		}
	    $couleur_impression_r = 0;
	    $couleur_impression_v = 0;
	    $couleur_impression_b = 0; 
	    
	    $qr_size = 25-2*DEFAULT_PADDING_X;
	    $qr_bg_color = array(255,255,255);
	    $qr_fg_color=array(0,0,0);
	    $qr_x = 44;
	    $qr_y = 10;
	    
	    $pdf->SetTextColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    $pdf->SetFillColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    
        $pdf->SetMargins(1.7,1.7,1.7);
	    
	    
	    //le nom du produit
	    $pdf->SetXY(1.7,3.5);    
	    $pdf->SetFont('arial','B',7);
	    //$pdf->Text($x+1,$y+5,iconv('UTF-8', 'windows-1252',$datas['nom']));
		$pdf->Cell(
			0,				//width
			0,				//height
			iconv('UTF-8', 'windows-1252',$datas['nom']),	//txt
			0,				//border
			1,				//1 to the beginning of the next line
			'L'				//align L, C, R
		); 
		$pdf->SetFillColor(255,255,255);
	    $pdf->Rect($x+DEFAULT_LABEL_WIDTH,$y,300,DEFAULT_LABEL_HEIGHT,'F');
	    
	    $delta_y_texte = -8;
	    
		
	    //manufacturer
	    if(isset($datas['manufacturer']) && !empty($datas['manufacturer']))
	    {
	    	
			
	    	$pdf->SetFillColor(255,255,255);
	    	$pdf->Rect(35,1,20,10,'F');
			
	        $pdf->SetFont('arial','',7);
	        //$pdf->Text($x+1,$delta_y_texte+$y+17.2,iconv('UTF-8', 'windows-1252',$datas['manufacturer']));
			$pdf->SetXY(1.7,3.5);   
			$pdf->Cell(
				48,				//width
				0,				//height
				iconv('UTF-8', 'windows-1252',$datas['manufacturer']),	//txt
				0,				//border
				1,				//1 to the beginning of the next line
				'R'				//align L, C, R
			); 
			
		}
		
		
		
	    //la reference
	    if(isset($datas['reference']) && !empty($datas['reference']))
	    {
	        $pdf->SetFont('arial','',7);
	        //$pdf->Text($x+1,$delta_y_texte+$y+17.2,iconv('UTF-8', 'windows-1252',$datas['manufacturer']));
			$pdf->SetXY(1.7,21.5);   
			$pdf->Cell(
				48,				//width
				0,				//height
				iconv('UTF-8', 'windows-1252',$datas['reference']),
				0,				//border
				1,				//1 to the beginning of the next line
				'L'				//align L, C, R
			);			
	    }
		
	    //la PRIX
	    $pdf->SetFillColor(255,255,255);
    	$pdf->Rect(35,15,20,10,'F');
	    
        $pdf->SetFont('arial','B',10);
        //$pdf->Text($x+1,$delta_y_texte+$y+17.2,iconv('UTF-8', 'windows-1252',$datas['manufacturer']));
		$pdf->SetXY(1.7,21.5);   
		$pdf->Cell(
			48,				//width
			0,				//height
			iconv('UTF-8', 'windows-1252',$datas['prix'].''),	//txt
			0,				//border
			1,				//1 to the beginning of the next line
			'R'				//align L, C, R
			
		);
		
		//DECLINAISON COULEUR/TAILLE
		if($attributes_name != '')
	    {
	        $pdf->SetFont('arial','',7);
	        //$pdf->Text($x+1,$delta_y_texte+$y+17.2,iconv('UTF-8', 'windows-1252',$datas['manufacturer']));
			$pdf->SetXY(1.7,18.5);   
			$pdf->Cell(
				48,				//width
				0,				//height
				iconv('UTF-8', 'windows-1252', $attributes_name),	//txt
				0,				//border
				1,				//1 to the beginning of the next line
				'R'				//align L, C, R
			);			
	    }
		
		
	    //le code ean13
	   	if(isset($datas['ean13']) && !empty($datas['ean13']))
	    {
		    $pdf->SetFillColor(0,0,0);
		    $pdf->SetFont('arial','',4);
		    $pdf->EAN13(3,6,$datas['ean13'],10.2,0.45);
		}
		
		return;
	    //bordure d'étiquette
	    if($with_border === true)
	    {
	    	$pdf->Rect($x,$y,DEFAULT_LABEL_WIDTH,DEFAULT_LABEL_HEIGHT,'D');
		}
	}
    
	
	private function _drawEtiquette28x51($datas = null,$x,$y,$options,$pdf)
	{
		
		$x += DEFAULT_PADDING_X;
		$y += DEFAULT_PADDING_Y;
		
		$with_border = self::DEFAULT_LABEL_WITH_BORDER;
			
		
		if($datas === null)
		{
		    $datas = array(
		        'nom' => 'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
		        'prix' => 100.30,
		        'attributes' => false,
		        'couleur' => 'Khaki / Blanc',
		        'ean13' => '1684646846544',//'7612345678900'
		        'qrcode_url' => 'http://slash-store.com/fr/sneakers/362-adidas-gazelle-bleu-argent.html'
		    );
		}
	    $couleur_impression_r = 0;
	    $couleur_impression_v = 0;
	    $couleur_impression_b = 0; 
	    
	    $qr_size = 25-2*DEFAULT_PADDING_X;
	    $qr_bg_color = array(255,255,255);
	    $qr_fg_color=array(0,0,0);
	    $qr_x = 44;
	    $qr_y = 10;
	    
	    $pdf->SetTextColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    $pdf->SetFillColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    
	        
	    //le nom du produit    
	    $pdf->SetFont('arial','B',11);
	    $pdf->Text($x+1,$y+5,iconv('UTF-8', 'windows-1252',$datas['nom']));
	    $pdf->SetFillColor(255,255,255);
	    
	    $pdf->Rect($x+DEFAULT_LABEL_WIDTH,$y,300,DEFAULT_LABEL_HEIGHT,'F');
	    
	    $delta_y_texte = -8;
	    
	    //la reference
	    if(isset($datas['reference']) && !empty($datas['reference']))
	    {
	        $pdf->SetFont('arial','',7);
	        $pdf->Text($x+1,$delta_y_texte+$y+17,iconv('UTF-8', 'windows-1252','Référence'));
	        $pdf->SetFont('arial','',9);
	        $pdf->Text($x+1,$delta_y_texte+$y+20.5,iconv('UTF-8', 'windows-1252',$datas['reference']));
	    }
	
	    //le prix
	    if(isset($datas['prix']) && !empty($datas['prix']))
	    {
	        $pdf->SetFont('arial','',7);
	        $pdf->Text($x+1,$delta_y_texte+$y+24,iconv('UTF-8', 'windows-1252','Prix'));
	        $pdf->SetFont('arial','',11);
	        $pdf->Text($x+1,$delta_y_texte+$y+28.2,iconv('UTF-8', 'windows-1252',$datas['prix'].''));
	    }
	    
	    //le code ean13
	    if(isset($datas['ean13']) && !empty($datas['ean13']))
	    {
		    $pdf->SetFillColor(0,0,0);
		    $pdf->SetFont('arial','',3);
		    $pdf->EAN13($x+1,$delta_y_texte+$y+29.5,$datas['ean13'],3.2,0.45);
		}
	       
	    //bordure d'étiquette
	    if($with_border === true)
	    {
	    	$pdf->Rect($x,$y,DEFAULT_LABEL_WIDTH,DEFAULT_LABEL_HEIGHT,'D');
		}
	}
    
	
	
    private function _drawEtiquette46x78($datas = null,$x,$y,$options,$pdf)
	{
		
		//d(func_get_args());
		
		$x += DEFAULT_PADDING_X;
		$y += DEFAULT_PADDING_Y;
		
		$with_border = self::DEFAULT_LABEL_WITH_BORDER;
			
		
		if($datas === null)
		{
		    $datas = array(
		        'nom' => 'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
		        'prix' => 100.30,
		        'attributes' => false,
		        'couleur' => 'Khaki / Blanc',
		        'ean13' => '1684646846544',//'7612345678900'
		        'qrcode_url' => 'http://slash-store.com/fr/sneakers/362-adidas-gazelle-bleu-argent.html'
		    );
		}
	    $couleur_impression_r = 0;
	    $couleur_impression_v = 0;
	    $couleur_impression_b = 0; 
	    
	    $qr_size = 25-2*DEFAULT_PADDING_X;
	    $qr_bg_color = array(255,255,255);
	    $qr_fg_color=array(0,0,0);
	    $qr_x = 44;
	    $qr_y = 10;
	    
	    $pdf->SetTextColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    $pdf->SetFillColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    
	        
	    //le nom du produit    
	    $pdf->SetFont('arial','B',11);
	    $pdf->Text($x+1,$y+5,iconv('UTF-8', 'windows-1252',$datas['nom']));
	    $pdf->SetFillColor(255,255,255);
	    
	    $pdf->Rect($x+DEFAULT_LABEL_WIDTH,$y,300,DEFAULT_LABEL_HEIGHT,'F');
	    
	    $delta_y_texte = -8;
	    
	    //la reference
	    if(isset($datas['reference']) && !empty($datas['reference']))
	    {
	        //$pdf->SetFont('arial','',7);
	        //$pdf->Text($x+1,$delta_y_texte+$y+17,iconv('UTF-8', 'windows-1252','Référence'));
	        $pdf->SetFont('arial','',11);
	        $pdf->Text($x+1,$delta_y_texte+$y+17,iconv('UTF-8', 'windows-1252',$datas['reference']));
	    }
	
	    //le prix
	    if(isset($datas['prix']) && !empty($datas['prix']))
	    {
	        $pdf->SetFont('arial','',7);
	        $pdf->Text($x+1,$delta_y_texte+$y+24,iconv('UTF-8', 'windows-1252','Prix'));
	        $pdf->SetFont('arial','',11);
	        $pdf->Text($x+1,$delta_y_texte+$y+28.2,iconv('UTF-8', 'windows-1252',$datas['prix'].' EUR'));
	    }
		
	    //le prix carton de 6
	    if($datas['prix_x6'] != $datas['prix'])
	    {
	        $pdf->SetFont('arial','',7);
	        $pdf->Text($x+1,$delta_y_texte+$y+32.2,iconv('UTF-8', 'windows-1252','Prix carton'));
	        $pdf->SetFont('arial','',11);
	        $pdf->Text($x+1,$delta_y_texte+$y+36,iconv('UTF-8', 'windows-1252',$datas['prix_x6'].' EUR'));
	    }
		
	    //le code ean13
	    if(isset($datas['ean13']) && !empty($datas['ean13']))
	    {
		    $pdf->SetFillColor(0,0,0);
		    $pdf->SetFont('arial','',3);
		    $pdf->EAN13($x+1,$delta_y_texte+$y+37.7,$datas['ean13'],3.9,0.45);
		}
	       
	    //bordure d'étiquette
	    if($with_border === true)
	    {
	    	$pdf->Rect($x,$y,DEFAULT_LABEL_WIDTH,DEFAULT_LABEL_HEIGHT,'D');
		}
	}
    

    private function _drawEtiquetteAMDVRayon($datas = null,$x,$y,$options,$pdf)
	{
		
		//d(func_get_args());
		
		$x += DEFAULT_PADDING_X;
		$y += DEFAULT_PADDING_Y;
		
		$with_border = self::DEFAULT_LABEL_WITH_BORDER;
			
		
		if($datas === null)
		{
		    $datas = array(
		        'nom' => 'DEUS EX MACHINA HOUSE TRUCKE BLABLA',
		        'prix' => 100.30,
		        'attributes' => false,
		        'couleur' => 'Khaki / Blanc',
		        'ean13' => '1684646846544',//'7612345678900'
		        'qrcode_url' => 'http://slash-store.com/fr/sneakers/362-adidas-gazelle-bleu-argent.html'
		    );
		}
	    $couleur_impression_r = 0;
	    $couleur_impression_v = 0;
	    $couleur_impression_b = 0; 
	    
	    $qr_size = 25-2*DEFAULT_PADDING_X;
	    $qr_bg_color = array(255,255,255);
	    $qr_fg_color=array(0,0,0);
	    $qr_x = 44;
	    $qr_y = 10;
	    
	    $pdf->SetTextColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    $pdf->SetFillColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
	    
	        
	    //le nom du produit    
	    $pdf->SetFont('arial','',13);
	    $pdf->Text($x+1,$y+5,iconv('UTF-8', 'windows-1252',$datas['nom']));
	    $pdf->SetFillColor(255,255,255);
	    
	    $pdf->Rect($x+DEFAULT_LABEL_WIDTH,$y,300,DEFAULT_LABEL_HEIGHT,'F');
	    
	    $delta_y_texte = -8;
	    
	    //la reference
	    if(isset($datas['reference']) && !empty($datas['reference']))
	    {
	        //$pdf->SetFont('arial','',7);
	        //$pdf->Text($x+1,$delta_y_texte+$y+17,iconv('UTF-8', 'windows-1252','Référence'));
	        $pdf->SetFont('arial','',12);
	        $pdf->Text($x+1,$delta_y_texte+$y+18.5,iconv('UTF-8', 'windows-1252',$datas['reference']));
	    }
	
	    //le prix
	    if(isset($datas['prix']) && !empty($datas['prix']))
	    {
	        //$pdf->SetFont('arial','',7);
	        //$pdf->Text($x+1,$delta_y_texte+$y+24,iconv('UTF-8', 'windows-1252','Prix'));
	        $pdf->SetFont('arial','',15);
	        $pdf->Text($x+1,$delta_y_texte+$y+25.2,iconv('UTF-8', 'windows-1252',$datas['prix'].' A l\'unité'));
	    }
		
	    //le prix carton de 6
	    if($datas['prix_x6'] != $datas['prix'])
	    {
	        //$pdf->SetFont('arial','',7);
	        //$pdf->Text($x+1,$delta_y_texte+$y+32.2,iconv('UTF-8', 'windows-1252','Prix carton'));
	        $pdf->SetFont('arial','',15);
	        $pdf->Text($x+1,$delta_y_texte+$y+34,iconv('UTF-8', 'windows-1252',$datas['prix_x6'].' Au carton'));
	    }
		
	    //le code ean13
	    if(isset($datas['ean13']) && !empty($datas['ean13']))
	    {
		    $pdf->SetFillColor(0,0,0);
		    $pdf->SetFont('arial','',3);
		    $pdf->EAN13($x+1,$delta_y_texte+$y+37.7,$datas['ean13'],6,0.55);
		}
	       
	    //bordure d'étiquette
	    if($with_border === true)
	    {
	    	$pdf->Rect($x,$y,DEFAULT_LABEL_WIDTH,DEFAULT_LABEL_HEIGHT,'D');
		}
	}
    
    
    
    
    public function initContent() {
        $this->context->controller->addJs(_MODULE_DIR_.''.$this->module->name.'/js/index.js'); 
        $this->context->controller->addCSS(_MODULE_DIR_.''.$this->module->name.'/css/index.css');



        $this->toolbar_title = _('Print labels');
        //$this->toolbar_btn = array();
        
        
        $this->context->smarty->assign('prestatilllabelstore_revision', '1.0 Alpha');
        $link = $this->context->link->getAdminLink('AdminPrestaTillLabelStorePrint');
        
        $this->context->smarty->assign('form_link', $link);

        
        
    	$tpl_path = '../../../../modules/'.$this->module->name.'/views/templates/admin/print_label.tpl';
        $this->setTemplate($tpl_path);

        
        
        parent::initContent();
        return;
    }
    
    public function initToolbar() {
    	parent::initToolbar();
		$this->toolbar_btn['button_name'] = array(
		'desc' => $this->l('display button name'),
		'href' =>'www.test.com',
		//'js' => 'window.open(\''.self::$currentIndex.'&add'.$this->table.'&token='.$this->token.'\',\''.'popupwindow \''.',\''.'width=500\',\'height=500\',\'scrollbars\',\'resizable\');',
		);
		return $this->toolbar_btn;
	}
    

    private function _injectJS() {
        $js='<!-- acaisse module JS injection -->';
        
        
        $js_list=array(
            'index.js',
        );
        
        foreach($js_list as $file) {
            if (file_exists('../'._MODULE_DIR_.$this->module->name.'/js/'.$file)) {
                $js.='<script type="text/javascript" src="'._MODULE_DIR_.$this->module->name.'/js/'.$file.'"></script>';
            }
        }
        
        return $js;
    }
	
	public function ajaxProcessPrintLabel($params)
	{
		$this->postProcess();
	}
    
    private function _injectCSS() {
        $css='<!-- acaisse module CSS injection -->';
        
        $css_list=array(
            'index.css',
        );
        
        foreach($css_list as $file) {
            if (file_exists('../'._MODULE_DIR_.$this->module->name.'/css/'.$file)) {
                 $css.='<link href="'._MODULE_DIR_.$this->module->name.'/css/'.$file.'" rel="stylesheet" type="text/css" media="all" />';
            }
        }
        
        return $css;
    }
    
    private function _add_ean13_check_digit($digits12){
		//first change digits to a string so that we can access individual numbers
		$digits =(string)$digits12;
		// 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;
		// 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;
		// 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
		$next_ten = (ceil($total_sum/10))*10;
		$check_digit = $next_ten - $total_sum;
		
		//p('$check_digit : '.$check_digit);
		//p($digits12 . $check_digit);
		return $digits12 . $check_digit;
	}
    
}
