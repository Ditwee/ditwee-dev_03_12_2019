$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
};
$( document ).ready(function() {
	if( decodeURIComponent($.urlParam('controller')) == "AdminProducts" &&
		decodeURIComponent($.urlParam('id_product')) > 0)
	{
		var toolbar = '<li><a id="print_label" class="toolbar_btn pointer" href="'+label_url+'&id_product='+decodeURIComponent($.urlParam('id_product'))+'" title="Imprimer les étiquettes produits (une par quantité en stock)" target="_blank"><i class="process-icon-barcode icon-barcode"></i><div>Etiquettes</div></a></li>';
		$("#toolbar-nav").prepend(toolbar);

		toolbar = '<li><a id="print_label" class="toolbar_btn pointer" href="'+label_url+'&id_product='+decodeURIComponent($.urlParam('id_product'))+'&rayon=true" title="Imprimer une étiquette de rayon" target="_blank"><i class="process-icon-barcode icon-barcode"></i><div>Etiquette rayon</div></a></li>';
		
		$("#toolbar-nav").prepend(toolbar);
	
		$("#choice_print_mode").on('change', function(e){

			e.preventDefault();
			if($(this).val() == 1)
			{
				$('#warehouse_choice').prop('disabled',false);
			}
			else
			{
				$('#warehouse_choice').prop('disabled','disabled');
			}
			console.log($(this).val());
		});
		
		$('#submitManuelPrintLabel').click(function(){
			$form = $('<form action="./'+label_url+'" target="_blank"></form>');
			$form.append($('#product-tab-content-ModulePrestatilllabelstore').clone());
			
			$('body').append($form);
			$form.submit();
			//$form.remove();
			
		});
	
	}
});