<?php
//require_once(dirname(__FILE__).'/classes/PrestaTillLabelStoreScreen.php');
/**
 * DITWEE VERSION / idem DK
 * Etiquette SLP-TP
 * 54 x 25,4mm
 * étiquette sécurisé
 */
class prestatillLabelStore extends Module
{
    /*** Méthode constructeur ***/
    public function __construct()
    {
        $this->name = 'prestatilllabelstore';
        $this->tab = 'content_managment';
        $this->version = '1.0 Alpha';
        $this->author = 'PrestaTill SAS';
		$this->bootstrap = true;
        $this->need_instance = 0;
        //$this->bootstrap = true; // active bootstrap
        
        $sqlMaj = '';
        $currentVersion = Configuration::get('PrestaTillLabelStoreVersion');
        
        if ($this->version > $currentVersion && $currentVersion <= '1.1 Bravo') {
            // $sqlMaj .= 'UPDATE '._DB_PREFIX_.'prestatill_label_store SET ... ;';
        }
        
        if ($this->version > $currentVersion && $currentVersion <= '1.2 Charlie') {
            // $sqlMaj .= 'UPDATE '._DB_PREFIX_.'prestatill_label_store SET ... ;';
        }
        
        if ($sqlMaj!='') {
            Db::getInstance()->Execute($sqlMaj);
        }
        
        parent::__construct();

        $this->displayName = $this->l('Etiquettes PrestaTill');
        $this->description = $this->l('Imprimez les étiquettes des produits de votre catalogues');

        //$this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.5');
        //$this->dependencies = array('aaemsgardes');
    }
    
    /*** Méthode pour écrire la page de configuration ***/
    public function getContent()
    {
        $html = $this->context->smarty->createTemplate(_PS_MODULE_DIR_.$this->name.'/views/templates/admin/index.tpl', $this->context->smarty)->fetch();
        
        return $html;
    }
    
    /*** Méthodes d'installation / désinstallation du module ***/
    public function install()
    {
        return
            parent::install() &&
            Configuration::updateValue('PrestaTillLabelStoreVersion','1.0 Alpha') &&
            $this->_createTable() &&
            $this->_registerPrestaTillLabelStoreTab() &&
			$this->registerHook('displayBackOfficeHeader') &&
			$this->registerHook('actionAdminControllerSetMedia') &&
			$this->registerHook('displayAdminProductsExtra') &&
			$this->registerHook('displayHeader');
    }
    
    public function uninstall()
    {
        $currentVersion = Configuration::get('PrestaTillLabelStoreVersion');
        
        if ($currentVersion >= '1.1 Bravo') {
            // on désinstal les trucs de la version 1.1 Bravo
        }
        
        // on désinstalle les truc de la version 1.0 Alpha
        $uninstallProperly =
            parent::uninstall() &&
            Configuration::deleteByName('PrestaTillLabelStoreVersion') &&
            $this->_deleteTable() &&
            $this->_deletePrestaTillLabelStoreTab();
        
        return $uninstallProperly;
    }
    
    /*** create table ***/
    private function _createTable()
    {
    	return true;
        $sql = '';
        
        if (!Db::getInstance()->Execute($sql)) {
            return false;
        } else {
            return true;
        }
    }
    
    /*** Méthodes pour créer un menu dans le back office ***/
    private function _registerPrestaTillLabelStoreTab()
    {
        $langs = Language::getLanguages();
        
        $translations = array();
        foreach ($langs as $lang) {
            $translations[$lang['id_lang']] = $this->l('Print label');
        }
        $id=$this->_installModuleTab('PrestaTillLabelStoreMenu',$translations);


        $translations = array();
        foreach ($langs as $lang) {
        	$translations[$lang['id_lang']] = $this->l('Print label');
        }
        $this->_installModuleTab('AdminPrestaTillLabelStorePrint',$translations,$id);
        
        $translations = array();
        foreach ($langs as $lang) {
            $translations[$lang['id_lang']] = $this->l('Parameters');
        }
        $this->_installModuleTab('AdminPrestaTillLabelStoreConfigure',$translations,$id);
        
        return true;
    }
    
    /*** delete table ***/
    private function _deleteTable()
    {
        return true; // on ne supprime pas la table au cas ou on a désinstallé le module sans faire exprès qu'on puisse le réinstaller rapidement
        
        $sql='';
        if (!Db::getInstance()->Execute($sql)) {
            return false;
        } else {
            return true;
        }
    }
    
    private function _deletePrestaTillLabelStoreTab()
    {
        $uninstallProperly = true;
        $currentVersion = Configuration::get('PrestaTillLabelStoreVersion');
        
        // on désinstale les tab de la version 1.1 Bravo
        if ($currentVersion >= '1.1 Bravo') {
            
        }
        
        $uninstallProperly =
            $uninstallProperly &&
            $this->_uninstallModuleTab('AdminPrestaTillLabelStorePrint') &&
            $this->_uninstallModuleTab('AdminPrestaTillLabelStoreConfigure') &&
            $this->_uninstallModuleTab('PrestaTillLabelStoreMenu');
        
        return $uninstallProperly;
    }
    
    private function _installModuleTab($tabClassName, $tabLabel, $parent_tab_id = 0, $module_name = NULL)
    {
        @copy(_PS_MODULE_DIR_.$this->name.'/logo.gif', _PS_IMG_DIR_.$this->name.$tabClassName.'.gif'); 
        $tab = new Tab();
        $tab->name = $tabLabel;
        $tab->class_name = $tabClassName;
        $tab->id_parent = $parent_tab_id;
        $tab->module = $module_name === NULL ? $this->name : $module_name;
        $tab->save();
        return $tab->id;
    }
    
    private function _uninstallModuleTab($tabClass)
    {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab) {
            $tab = new Tab($idTab);
            return $tab->delete();
        }
        return false;
    }
	
	/**
     * On ajoute le bouton en JS pour imprimer depuis une fiche produit
     */
    public function hookActionAdminControllerSetMedia()
    {
        if($this->context->controller->controller_name == 'AdminProducts' && Tools::getValue('id_product'))
		{
			$this->context->controller->addJS($this->_path.'js/prestatilllabelstore.js');
		}
    }
	
	/**
     * On ajoute le bouton en JS pour imprimer depuis une fiche produit
     */
    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addCSS($this->_path.'css/admin-theme-label.css');	
        $controller = 'AdminPrestaTillLabelStorePrint';	
        return '<script>
			var token_id = \''.Tools::getAdminTokenLite($controller).'\'; 
			var label_url = \''.$this->context->link->getAdminLink('AdminPrestaTillLabelStorePrint').'\';
		</script>';
    }
	
	public function prepareNewTab()
	{
		$context = Context::getContext();
		$product = new Product((int)Tools::getValue('id_product'),true,$context->language->id);
		$attributes = $product->getAttributeCombinations($context->language->id);
		
		$stocks = 0;
		if($product->advanced_stock_management == 1)
		{
			$stocks = Warehouse::getWarehouses();
			//$stocks = Warehouse::getWarehousesByProductId($product->id); 
			
			if(!empty($attributes))
			{
				foreach($attributes as $k => $attribute)
				{
					foreach($stocks as $key => $stock)
					{
						// On sépare le nom de l'entrepôt de la référence	
						$tmp = explode(" - ", $stock['name']);
						$stocks[$key]['wname'] = '<span class="name"><i class="fa fa-building-o"></i> '.$tmp[1].'</span><span class="ref">'.$tmp[0].'</span>';	
							
						$realQty = new StockManager;
						$attributes[$k]['w']['pQ'] += $realQty->getProductPhysicalQuantities($product->id,$attribute['id_product_attribute'],array($stocks[$key]['id_warehouse']));
						//$attributes[$k]['w'][$stocks[$key]['id_warehouse']]['pU'] = $realQty->getProductPhysicalQuantities($product->id,$attribute['id_product_attribute'],array($stocks[$key]['id_warehouse']),true);
						
					}
				}
			}
		}
		
		$this->context->smarty->assign(array(
			'product_infos' => $product,
			'attributes_infos' => $attributes,
			'warehouse_infos' => $stocks,
			'languages' => $this->context->language->id,
			'default_language' => (int)Configuration::get('PS_LANG_DEFAULT') 
		));

	}

	public function initProcess()
	{
		if (Tools::isSubmit('submitManuelPrintLabel'))
		{
			echo("titi");
		}
	}
	
	public function hookDisplayAdminProductsExtra($params)
	{
		if (Validate::isLoadedObject($product = new Product((int)Tools::getValue('id_product'))))
		{
			$this->prepareNewTab();
			return $this->display(__FILE__, 'views/templates/admin/labelextraproduct.tpl');
		} 
	}

	public function hookDisplayHeader($params)
	{
			$this->prepareNewTab();
	}	
    
}