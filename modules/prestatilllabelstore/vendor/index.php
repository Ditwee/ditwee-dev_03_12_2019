<?php
require_once('ean13.php');
include_once('../../../config/config.inc.php');
include_once('../../../config/settings.inc.php'); 

define('FPDF_FONTPATH',__DIR__.'/font');

$pdf=new PDF_EAN13();
$pdf->AddPage();
$pdf->SetAutoPageBreak(true);

$products = array(
array('2002310006008','Pages de caisse personnalisables'),
//array('2002310006015','Catalogue produits unique'),
array('2002310006022','Fichier clients & fidélisation'),
array('2002310006039','Ergonomie intuitive'),
array('2002310006046','Gain de temps & tranquilité'),
//array('2002310006053','Commerce Phygital'),
array('2002310006060','100% connectée, 100% mobile'),
array('2002310006077','Gestion de stock'),
//array('2002310006084','Promotion'),
//array('2002310006091','site e-commerce'),
//array('2002310006107','mises à jour fréquentes'),
//array('2002310006114','Bande Z'),
//array('2002310006121','Gestion du fond de caisse'),
//array('2002310006138','Multi point de vente'),
array('2002310006145','Tarification transparente'),
//array('2002310006152','Revendeur Oxhoo'),
//array('2002310006169','Afficheur client '),
array('2002310006176','Affichage dynamique'),
//array('2002310006183','Paiement multiple'),
array('2002310006190','Recherche & développement'),
array('2002310006206','Une solution clé en main'),
);

$context = new Context();
$context->controller = new FrontController();


$products = Product::getProducts(1, 0, 200, 'name', 'ASC', false,true, $context);
//p($products);

$hauteur_ticket = 35;
$marge_haute = 25;
$count = 0;
$nb_par_page = 8;


$couleur_impression_r = 0;
$couleur_impression_v = 0;
$couleur_impression_b = 0; 


foreach($products as $i => $product)
{
    $marge_gauche = 110;
    $c = $count;
    if($count%2 == 0) {
        $marge_gauche = 16;
    }
    else {
        $c = $count-1;
    }
        $pdf->SetTextColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
        $pdf->SetFillColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
        $nom = iconv('UTF-8', 'windows-1252', $product['name']);
        
        /*
        $p = Product::getPriceStatic(
            $product['id_product'],
            true, //             $usetax = true,
            null,// $id_product_attribute = null,
            2,// $decimals = 6,
            null,// $divisor = null,
            false, //$only_reduc = false,
            false, //******$usereduc = true,
            1, //$quantity = 1,
            false, //$force_associated_tax = false,
            null, //$id_customer = null,
            null, //$id_cart = null,
            null, //$id_address = null,
            $specific_price_output, //= null,
            true, //$with_ecotax = true,
            true, //$use_group_reduction = true,
            $context, //Context $context = null,
            false //$use_customer_price = true
        );
         
         */
        $p=12.50;
        $prix_barre = iconv('UTF-8', 'windows-1252', $p.' €');
        
        $p_barre = 10;
        $prix = iconv('UTF-8', 'windows-1252', $p_barre.' €');
        $promo = iconv('UTF-8', 'windows-1252', 'PROMO');
        $fictif = ''; //iconv('UTF-8', 'windows-1252', 'Article fictif de démonstration');

        //$pdf->Rect($marge_gauche-3,$marge_haute+15+$c*$hauteur_ticket-$hauteur_ticket,90,$hauteur_ticket,'D');
        $pdf->Rect($marge_gauche-3,$marge_haute+15+$c*$hauteur_ticket,90,$hauteur_ticket,'D');

$pdf->SetFont('Arial','B',14);
        $pdf->Text($marge_gauche,$marge_haute+23+$c*$hauteur_ticket,$nom);
$pdf->SetFont('Arial','',14);
        $pdf->Text($marge_gauche+40,$marge_haute+31+$c*$hauteur_ticket,$prix_barre);
        
$pdf->SetFont('Arial','',10);
    if($fictif !='')
    {
        $pdf->Text($marge_gauche+37.6,$marge_haute+44.75+$c*$hauteur_ticket,$fictif);
    }
        
$pdf->SetFont('Arial','B',18);
        $pdf->Text($marge_gauche+40,$marge_haute+40+$c*$hauteur_ticket,$prix);
        $pdf->EAN13($marge_gauche,$marge_haute+25+$c*$hauteur_ticket,$product['ean13']);

        
        $pdf->SetTextColor(255,255,255);
        $pdf->SetFillColor($couleur_impression_r,$couleur_impression_v,$couleur_impression_b);
        $pdf->Rect($marge_gauche+60,$marge_haute+26+$c*$hauteur_ticket,23,6,'F');
        
$pdf->SetFont('Arial','B',14);
        $pdf->SetTextColor(255,255,255);
        if($promo != '')
        {
            $pdf->Text($marge_gauche+62.5,$marge_haute+30.8+$c*$hauteur_ticket, $promo);
        }

        if($count++ == $nb_par_page-1) { $count=0; $pdf->AddPage(); }
}
$pdf->Output();
?>
