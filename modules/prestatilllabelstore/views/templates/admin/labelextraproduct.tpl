<div id="product-label" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="formations" />
	<h3 class="tab"> <i class="icon-info"></i> {l s='Impression d\'étiquettes' mod='prestatilllabelstore'}</h3>	
	<div class="alert alert-info">
		{l s='En gestion de stocks avancée, vous pouvez choisir le nombre d\'étiquettes à imprimer pour un ou plusieurs entrepôts.' mod='prestatilllabelstore'}
	</div>
	
	<div class="col-xs-12 col-md-6">
		<select id="choice_print_mode">
			<option value="0">Quantités manuelles</option>
			<option value="1">Quantités selon stock avancé</option>
			
		</select>
	</div> 
	<div class="col-xs-12 col-md-6">
		<select id="warehouse_choice" disabled="disabled">
			{foreach from=$warehouse_infos item=stock}
				<option>Tous les entrepôts</option>
				<option value="{$stock.id_warehouse}">{$stock.name}</option>
			{/foreach}
		</select>
	</div> 
	<br /><br />
	<hr/>
	<table class="table">
		<thead style="background: #F4F4F4;">  
			<tr>
				<th style="vertical-align: middle;"><span class="title_box">Déclinaison</span></th>
				<th style="vertical-align: middle;"><span class="title_box">
					Nombre d'étiquettes à imprimer</span></th>
			</tr>
		</thead>
		
		<tbody>
			{if !empty($attributes_infos)}
				{foreach from=$attributes_infos item=attribute}
				<tr>
					<td>{$product_infos->name} - {$attribute.attribute_name}</td>
					<td>
						<div >
							<div class="input-group">
								<input type="text" name="form_date_1" value="{$attribute.w.pQ}" style="text-align: center" id="form_date_1" />
							</div>
						</div>	
					</td>
				</tr>
				
				{/foreach}
			{/if}
		</tbody>
	</table>
	
	{*
	<div class="form-group">
		<label class="control-label col-lg-3" for="form_duration_{$default_language}">
				<span class="label-tooltip" data-toggle="tooltip"
				title="{l s='Public visé'}">
				{l s='Durée'}
			</span>
		</label>
		<div class="col-lg-9">				
			{include file="controllers/products/input_text_lang.tpl"
				languages=$languages
				input_name='form_duration'
				input_value=$form_duration}
		</div>
	</div>
	*}
	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="button" name="submitManuelPrintLabel" id="submitManuelPrintLabel" class="btn btn-default pull-right"><i class="process-icon-barcode icon-barcode"></i> {l s='Imprimer' mod='prestatilllabelstore'}</button>
	</div>
	<script>
	var label_url = '{$link->getAdminLink('AdminPrestaTillLabelStorePrint')}';
	</script>
</div>