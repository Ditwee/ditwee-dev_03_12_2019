$(function(){

	function prestatillreceipt(caisse_ref) {
		var module = this;
		var module_name = 'prestatillreceipt';
		
		//option
		var can_force_receipt = true;
		
		//propriétés du module
		this.total = 0;
		var c = null;
		var ctx = null;
		
		this.sendAmount = function() {
			PRESTATILL_CAISSE.payment.changePaymentAmount(this.total, false);
		};
		
		this.reload = function(){
			console.log('init reload canvas');
			this.init();
		};
		
		this.init = function() {		
			
			console.log('init receipt canvas');
			
			//var autorisation_amount = (ACAISSE.customer.outstanding_allow_amount == undefined)?0:ACAISSE.customer.outstanding_allow_amount;
			//var current_unpaid_amount = (ACAISSE.additionnal_customer_informations.prestatillreceipt.total_unpaid_amount == undefined)?0:ACAISSE.additionnal_customer_informations.prestatillreceipt.total_unpaid_amount;
			//var today_amount = parseFloat('0'+$('#keepToHaveToPay').html());
			var error_msg = false;
			/*
			if(autorisation_amount > 0)
			{
				if(autorisation_amount <= current_unpaid_amount )
				{
					error_msg = 'Ce client dépasse déjà son encours autorisé de '+autorisation_amount+'€ (sommes en attente de règlement '+current_unpaid_amount+'€)';					
				}
				else if(autorisation_amount < current_unpaid_amount + today_amount)
				{
					error_msg = 'Avec cet achat ce client dépasserai son encours autorisé de '+autorisation_amount+'€ (sommes en attente de règlement '+(current_unpaid_amount+today_amount)+'€)';					
				}
				else
				{
					//c'est bon
				}	
			}
			else
			{
				error_msg = 'Ce client n\'a pas d\'autorisation d\'encours';
			}
			*/
								
			//if(error_msg === false || (error_msg !== false && can_force_receipt === true && true === confirm(error_msg+'\n\n'+'Souhaitez-vous forcer le paiement contre-signature malgrès tout?')))
			//{
				this.total = 0;		
				PRESTATILL_CAISSE.payment.changePaymentAmount(this.total);
				$('canvas.prestatillreceipt_area').html('');
				if(c === null)
				{
					c = $('canvas.prestatillreceipt_area')[0];
					ctx = c.getContext("2d");
				}
				ctx.clearRect(0, 0, 420,400);
				ctx.fillStyle = '#FFF';
				ctx.fillRect(0, 0, 420,400);
				
				ctx.strokeStyle = "darkblue"; // Définition de la couleur de contour
				ctx.fillStyle = "darkblue"; // Définition de la couleur de contour
				ctx.lineWidth = 5;   
				
				
			//}
			//else
			//{	
				//if(can_force_receipt !== true) {
				//	alert(error_msg);
				//}
				//this.total = 0;		
				//PRESTATILL_CAISSE.payment.changePaymentAmount(this.total);
			//}
			
			
			// Set up touch events for mobile, etc
			c.addEventListener("touchstart", function (e) {
			      mousePos = getTouchPos(c, e);
				  var touch = e.touches[0];
				  var mouseEvent = new MouseEvent("mousedown", {
				    clientX: touch.clientX,
				    clientY: touch.clientY
				  });
				  c.dispatchEvent(mouseEvent);
			}, false);
			c.addEventListener("touchend", function (e) {
				  var mouseEvent = new MouseEvent("mouseup", {});
				  c.dispatchEvent(mouseEvent);
			}, false);
			c.addEventListener("touchmove", function (e) {
				  var touch = e.touches[0];
				  var mouseEvent = new MouseEvent("mousemove", {
				    clientX: touch.clientX,
				    clientY: touch.clientY
				  });
				  c.dispatchEvent(mouseEvent);
			}, false);	
		
			
			
			
			// Get a regular interval for drawing to the screen
			window.requestAnimFrame = (function (callback) {
			        return window.requestAnimationFrame || 
			           window.webkitRequestAnimationFrame ||
			           window.mozRequestAnimationFrame ||
			           window.oRequestAnimationFrame ||
			           window.msRequestAnimaitonFrame ||
			           function (callback) {
			        window.setTimeout(callback, 1000/60);
			           };
			})();
			
			
			(function drawLoop () {
			  requestAnimFrame(drawLoop);
			  renderCanvas();
			})();
			
			
			
			
		};
		
		this.getExtraDatas = function() {
			var img_jpeg = c.toDataURL('image/jpeg');
			return {
				'signature' : img_jpeg,
			};
		};
		
		
		//TODO gestion de CANVAS	
		
		var mousePos = { x:0, y:0 };
		var lastPos = mousePos;
		
		var x,y,drawing = false;
		$('.prestatill_payment_interface[data-module-name="'+module_name+'"]').on('mousedown','canvas.prestatillreceipt_area',function(e){
			//ctx.beginPath();
			//ctx.moveTo(e.offsetX,e.offsetY);
			
			drawing = true;
			
			
  			lastPos = getMousePos(c, e);
  			mousePos = lastPos;
		});
		
		$('.prestatill_payment_interface[data-module-name="'+module_name+'"]').on('mouseup','canvas.prestatillreceipt_area',function(e){
			//ctx.stroke();
			drawing = false;
		});

		
		var renderCanvas = function(e){
			if(drawing)
			{
				ctx.beginPath();
				ctx.moveTo(lastPos.x, lastPos.y);
			    ctx.lineTo(mousePos.x, mousePos.y);
			    ctx.stroke();
				lastPos = mousePos;
			}			
		};
		
		$('.prestatill_payment_interface[data-module-name="'+module_name+'"]').on('mousemove','canvas.prestatillreceipt_area',function(e){
			mousePos = getMousePos(c, e);		
		});
		
		
		var getMousePos = function(canvasDom, mouseEvent) {
			  var rect = canvasDom.getBoundingClientRect();
			  return {
			    x: mouseEvent.clientX - rect.left,
			    y: mouseEvent.clientY - rect.top
			   };
		};
		
		
		
		
		// Get the position of a touch relative to the canvas
		var getTouchPos = function(canvasDom, touchEvent) {
		  var rect = canvasDom.getBoundingClientRect();
		  return {
		    x: touchEvent.touches[0].clientX - rect.left,
		    y: touchEvent.touches[0].clientY - rect.top
		  };
		};
		
				
		
	};
	
	/*************
	Enregistrement du module de payment dans ll'instance de la caisse
	**************/	
	PRESTATILL_CAISSE.payment.modules.push(new prestatillreceipt());	
});
