<?php

class PrestatillReceipt extends PaymentModule
{
	public function __construct()
	{
		$this->name = 'prestatillreceipt';
		$this->tab = 'payments_gateways';
		$this->version = '0.1';
		$this->author = 'PrestaTill SAS';
		$this->bootstrap = 'true';
		parent::__construct();
		$this->displayName = $this->l('Module Payment Receipt for Prestatill');
		$this->description = $this->l('Module Payment Receipt for Prestatill');	
	}
	
	public function getTicketPaymentLabel($params)
	{
		return $this->l('Contre signature');
	}
	
	public function install()
	{
		if(
			!parent::install()
			|| !$this->registerHook('displayPayment')
			|| !$this->registerHook('displayPaymentReturn')
			//hooks Prestatill
			|| !$this->registerHook('displayPrestatillInjectJS')
			|| !$this->registerHook('displayPrestatillInjectCSS')
			|| !$this->registerHook('displayPrestatillPaymentButton')
			|| !$this->registerHook('displayPrestatillPaymentInterface')
			|| !$this->registerHook('actionPrestaTillCompleteCustomerInformations')
			
			//Hook Prestatill Export Compta
			//rapport financier
			|| !$this->registerHook('displayPrestatillFinancialReportHeader')
			|| !$this->registerHook('displayPrestatillFinancialReportFooter')
			
			
			//installation du status de paiement spécifique
			|| !$this->_installOrderState()
		)
		{
			return false;
		}
		return true;
	
	}
	
	protected function _installOrderState()
	{
		//on check si ce status n'a pas DEJA était uinstallé
		if(!Configuration::hasKey('PRESTATILL_RECEIPT_ID_ORDER_STATE'))
		{
			$context = Context::getContext();
			$id_lang = $context->language->id;
			
			$os = new OrderState(null,$id_lang);
			$os->send_email = false;
			$os->module_name = 'prestatillreceipt';
			$os->invoice = true;
			$os->color = '#44bb00';
			$os->logable = true;
			$os->shipped = true;
			$os->unremovable = false;
			$os->delivery = true;
			$os->hidden = false;
			$os->paid = true;
			$os->pdf_delivery = false;
			$os->pdf_invoice = true;
			$os->deleted = false;
			
			$os->name = 'Paiement accepté (signature)';
			$os->save();
			Configuration::updateValue('PRESTATILL_RECEIPT_ID_ORDER_STATE',$os->id);
			return true;
		}
		return true;
	}
	
	public function getHookController($hook_name)
	{
		require_once(dirname(__FILE__).'/controllers/hook/'.$hook_name.'.php');
		$controller_name = $this->name.$hook_name.'Controller';
		$controller  = new $controller_name($this, __FILE__, $this->_path);
		return $controller;
	}
	
	public function hookDisplayPayment ($params)
	{
		$controller = $this->getHookController('displayPayment');
		return $controller->run($params);
			
	}
	
	
	/**
	 * Injection JS dans le header de la caisse
	 */
	public function hookDisplayPrestatillInjectJS($params)
	{
		return '<script src="'._MODULE_DIR_.$this->name.'/js/prestatill.js"></script>';
	}
	
	/**
	 * Injection CSS dans le header de la caisse
	 */
	public function hookDisplayPrestatillInjectCSS($params)
	{
		return '<link  href="'._MODULE_DIR_.$this->name.'/css/prestatill.css" rel="stylesheet" type="text/css" media="all" />';		
	}	
	
	/**
	 * Affichage du boutton de paiement
	 */
	public function hookDisplayPrestatillPaymentButton($params)
	{
		//$this->context->smarty->assign($assigns);
	    $html = $this->createTemplate('prestatill_payment_button.tpl')->fetch();
        return $html;
	}	
	
	/**
	 * Affichage d'une interface de saisie du montant (non obligatoire, si pas d'interface de saisie : exemple CB)
	 * l'interface doit soit d'une colonne afin d'etre compatible avec la vue paiement multiple
	 */
	public function hookDisplayPrestatillPaymentInterface($params)
	{	
	    $html = $this->createTemplate('prestatill_payment_interface.tpl')->fetch();
        return $html;
	}
	
	/**
	 * Hook appelé lorsque l'on change d'utilisateur dans la caisse.
	 * Ce hook va permettre de compléter la response renvoyée à la caisse.
	 * Exemple ici : on va devoir renvoyer le total de créance de l'utilisateur sélectionné
	 * ce qui permettra au module de bloqué un paiement contre signature si nous dépassons le plafond.
	 */
	public function hookActionPrestaTillCompleteCustomerInformations($params)
	{
		$params['response']['response']['additionnal_customer_informations']['prestatillreceipt'] = array(
			'total_unpaid_amount' => rand(0,5300), //@TODO : à remplacer par la bonne requete pour obtenir le total de l'utilisateur indiqué dans $params['response']['new_customer']
			'list_unpaid_orders' => array()  //@TODO : pour l'instant inutil
		);
		
		return true;
	}
	
	//surcharge pour tester aussi le dossier view dans le module
    public function createTemplate($tpl_name,$type = 'hook')
    {
    	//d(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    	//d(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    		
    	
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        return parent::createTemplate($tpl_name);
    }

    public function getTplPath($tpl_name)
    {
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name;
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name;
        }
    }
	
	public function hookDisplayPrestatillFinancialReportHeader($params)
	{
		return '';	//'<div>FOOTER hookDisplayPrestatillFinancialReportHeader</div>';
	}
	public function hookDisplayPrestatillFinancialReportFooter($params)
	{
		$html = '';
		
		//$html .= '<pre>';
		//$html .= print_r($params,true);
		
		$day = $params['day'];
		
		// On récupère le jour du rapport en cours
		$id_report = PrestatillFinancialReport::ReportExists($day, $params['id_pointshop'], 1);
		if($id_report != false)
		{
			$report = new PrestatillFinancialReport($id_report);
			if(Validate::isLoadedObject($report))
			{
				$day = $report->day;
			}
		}
		
		// On récupère les paiements "en cours" du jour
		//$report_payment = PrestatillFinancialReportPayment::getPaymentList($report->id);
		$report_details = PrestatillFinancialReportDetail::getList($report->id);
		$outstandings = array();
		foreach($report_details['detail'] as $id_service => $details)
		{
			foreach($details as $detail)
			{
				if(!isset($outstandings[$id_service]))
					$outstandings[$id_service] = array();
				
				$order = new Order($detail['id_order']);
				if(Validate::isLoadedObject($order))
				{
					$customer = new Customer($order->id_customer);
					$outstandings[$id_service][$customer->id]['id_customer'] = $customer->id;
					$outstandings[$id_service][$customer->id]['firstname'] = $customer->firstname;
					$outstandings[$id_service][$customer->id]['lastname'] = $customer->lastname;
					//d($customer);
				}
				
				if(!isset($outstandings[$id_service][$customer->id]))
					$outstandings[$id_service][$customer->id] = array();
				
				if(!isset($outstandings[$id_service][$customer->id]['total_to_pay']))
					$outstandings[$id_service][$customer->id]['total_to_pay'] = 0;
				
				if(!isset($outstandings[$id_service][$customer->id]['total_paid']))
					$outstandings[$id_service][$customer->id]['total_paid'] = 0;
				
				//$outstandings[$id_service][$detail['order_reference']]['reference'] = $detail['order_reference'];
				// ON NE DEDUIT QUE LES COMMANDES > 0, LES AUTRES SONT DES AVOIRS / REMBOURSEMENTS
				// ET LES COMMANDES NON ANNULEES
				if($detail['total_price_tax_incl'] > 0 && $detail['is_cancelled'] == 0)
				{
					$outstandings[$id_service][$customer->id]['total_to_pay'] += (float)number_format($detail['total_price_tax_incl'],2,'.','');
					
					if(!empty($detail['payments']))
					{
						foreach($detail['payments'] as $payment)
						{
							$outstandings[$id_service][$customer->id]['total_paid'] += (float)number_format($payment['amount'],2,'.','');
						}
					}
				}
			}	
			//d($outstandings);
			// On supprime du tableau les commandes soldées
			foreach($outstandings[$id_service] as $key => $outstanding)
			{
				if ($outstanding['total_to_pay'] <= $outstanding['total_paid']) 
				{
					unset($outstandings[$id_service][$key]);
				}
			}
		}
		
		if((Tools::getValue('controller') == 'ACaisseUse'))
		{
			$tpl = $this->createTemplate('prestatill_z_footer.tpl');
		}else
		{
			$tpl = $this->createTemplate('prestatill_financial_report_footer.tpl');
		}
		
		
		$tpl->assign(array(
            'outstandings' => $outstandings,
            'report' => $report,
            'tocken_order' => Tools::getAdminTokenLite('AdminOrders'),
            'currency' => new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
        ));
		
	    $html .= $tpl->fetch();
		return $html;
		
	}
	
}
	