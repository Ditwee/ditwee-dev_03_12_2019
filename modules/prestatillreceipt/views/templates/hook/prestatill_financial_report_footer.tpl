<div class="print-together">
	<div class="col-lg-12">
		<div id="" class="panel">
			<header class="panel-heading">
				<i class="icon-user"></i> Encours et comptes clients
			</header>
			<div class="table-responsive">
			{if !empty($outstandings.1)}
				<table class="table data_table table-striped" id="">
					<thead>
						<tr>
							<th class="text-left" colspan="6"><b>Encours du jour</b></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center"><b>ID Client</b></td>
							<td class="text-center"><b>Client</b></td>
							<td class="text-center"><b>Montant de l'encours</b></td>
						</tr>
						{assign var=tot_outstanding value=0}
						{* todo : dynamiser l'id service *}
						{foreach from=$outstandings.1 item=detail key=k name=foo}
						{if $detail.total_to_pay > $detail.total_paid}
						<tr>
							<td class="text-center">{$detail.id_customer}</td>
							<td class="text-center">{$detail.firstname} {$detail.lastname}</td>
							<td class="text-right">{Tools::displayPrice($detail.total_to_pay-$detail.total_paid, $currency)}</td>
						</tr>
						{assign var=tot_outstanding value=$tot_outstanding+($detail.total_to_pay-$detail.total_paid)}
						{/if}
						{/foreach}
					</tbody>
				</table>
			{else}
				<b>{l s='Encours clients' mod='prestatillexportcompta'}</b>
				<br />
				<br />
				<span style="display:block;" class="alert alert-info">{l s='Aucun encours clients ce jour' mod='prestatillexportcompta'}</span>
			{/if}
			</div>
		</div>
	</div>
</div>
