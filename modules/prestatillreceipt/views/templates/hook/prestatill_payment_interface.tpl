<div class="prestatill_payment_interface" data-module-name="prestatillreceipt">
	<canvas class="prestatillreceipt_area" width="420" height="400">LA SIGNATURE ICI</canvas>
	<p>En signant, le client reconnait avoir emporté tout le matériel du bon de commande et accepte sans réserve les conditions générales de ventes.</p>
</div>