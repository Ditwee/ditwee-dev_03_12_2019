<div>
{for $id_service=1 to $report->id_service}	
{if !empty($outstandings.$id_service)}
	<table class="table data_table table-striped" id="">
		<thead>
			<tr>
				<td class="text-left" colspan="2">ENCOURS DU JOUR</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="separate" colspan="2"></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			{assign var=tot_outstanding value=0}
			{foreach from=$outstandings.$id_service item=detail key=k name=foo}
			<tr>
				<td class="text-left">{$detail.firstname} {$detail.lastname}</td>
				<td class="text-right">{Tools::displayPrice($detail.total_to_pay-$detail.total_paid, $currency)}</td>
			</tr>
			{assign var=tot_outstanding value=$tot_outstanding+($detail.total_to_pay-$detail.total_paid)}
			{/foreach}
		</tbody>
	</table>
{else}
	<div style="text-transform: uppercase;text-align: left;">{l s='Encours clients' mod='prestatillexportcompta'}</div>
	<div class="separate"></div>
	<span style="display:block;text-transform: uppercase;" class="alert alert-info">{l s='Aucun encours clients' mod='prestatillexportcompta'}</span>
{/if}
{/for}
</div>
<div class="separate_double"></div>
