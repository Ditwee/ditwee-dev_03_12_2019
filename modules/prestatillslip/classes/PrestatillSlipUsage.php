<?php

class PrestatillSlipUsage extends ObjectModel
{
	public $id_prestatill_slip_usage;
    public $id_order_slip;
    public $id_employee;
    public $amount;
	public $use_for_id_ticket = 0;
	public $use_for_id_order = 0;
	public $use_for_id_customer = 0;
	public $use_for_id_pointshop = 0;
	public $use_for_id_service = 0;
	public $date_add;
	public $date_upd;
	
	/* SINCE 2.4.1 */
	public $id_financial_report = 0;
	public $day = '0000-00-00';

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'prestatill_slip_usage',
        'primary' => 'id_prestatill_slip_usage',
        'multilang' => false,
        'fields' => array(
            'id_order_slip' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'id_employee' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'amount' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'use_for_id_ticket' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'use_for_id_order' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'use_for_id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'use_for_id_pointshop' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'use_for_id_service' =>  array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'date_add' =>   array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            'date_upd' =>   array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
            
			/* SINCE 2.4.1 */
			'id_financial_report' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'day' =>   array('type' => self::TYPE_STRING),
        ),
    );
	
	/**
	 * Pour obtenir la liste des utilisations d'avoir d'après une liste d'id_order_slip
	 */
	public static function getFromIdOrderSlips($ids)
	{
		if(!is_array($ids) || empty($ids))
		{
			return array();
		}
		$sql = 'SELECT sum(amount) as amount, id_order_slip FROM '._DB_PREFIX_.self::$definition['table'].'
				WHERE id_order_slip IN ('.implode(',',$ids).')
				GROUP BY id_order_slip
				';
		$rows = DB::getInstance()->executeS($sql);
		$results = array();
		foreach($rows as $row)
		{
			$results[$row['id_order_slip']] = $row['amount'];
		}
		return $results;
	} 
	
	/*
	 * RECUPRER LA LISTE DES AVOIRS DU JOUR
	 * POUR LE RAPPORT FINANCIER
	 */
	public static function getList($day,$id_pointshop = 0)
	{	
		$query = 'SELECT * 
					FROM `'._DB_PREFIX_.'order_slip` 
					WHERE day LIKE \''.$day.'%\'
					AND id_pointshop = '.$id_pointshop;

		$result = Db::getInstance()->executeS($query);

		return $result;
	}
	
	/*
	 * RECUPRER LA LISTE DES AVOIRS UTILISES
	 * POUR LE RAPPORT FINANCIER
	 */
	public static function getUsageList($day,$id_pointshop = 0)
	{	
		$query = 'SELECT *,psu.amount as use_amount 
					FROM `'._DB_PREFIX_.'prestatill_slip_usage` psu
					LEFT JOIN `'._DB_PREFIX_.'order_slip` os ON (psu.id_order_slip = os.id_order_slip)
					WHERE psu.day LIKE \''.$day.'%\'
					AND psu.use_for_id_pointshop = '.$id_pointshop.'
					AND psu.amount > 0';

		$result = Db::getInstance()->executeS($query);

		return $result;
	}
}
