<?php
class PrestatillSlipGetSlipModuleFrontController extends ModuleFrontController
{
	public function initContent() {
		$secret_tocken = PrestatillSlip::SECRET_TOCKEN;
		$tocken = Tools::getValue('tocken');
		if($secret_tocken == $tocken)
		{
			require_once(__DIR__.'/../../classes/PrestatillSlipUsage.php');
			
			//date péremtion 
			$peremtion_period = Configuration::get('PRESTATILLSLIP_nb_month_validity');
			
			if($peremtion_period == 0 || ''.$peremtion_period == '')
			{
				$peremtion_period = '-1 year';	
			}

			$limit_date = date('Y-m-d', strtotime($peremtion_period));
			//recherche d'un avoir selon son code ean13
			if(isset($_POST['ean13']))
			{
				$ean13 = Tools::getValue('ean13');
				$ean12 = substr($ean13,0,-1);
				if(PrestatillSlipGetSlipModuleFrontController::add_ean13_check_digit($ean12) == $ean13)
				{
					$id_slip = (int)substr($ean12,2);
					$slip = new OrderSlip($id_slip);
					if(Validate::isLoadedObject($slip))
					{
						$ids = array();
						$slips = array();
						$ids[] = $slip->id;
						$slips[$slip->id] = $slip;
												
						//on recherche les utilisations des slips
						$usages = PrestatillSlipUsage::getFromIdOrderSlips($ids);
						
						require_once(_PS_MODULE_DIR_.'acaisse/classes/ACaissePointshop.php');
						
						if($peremtion_period != '0' && substr($slip->date_add,0,10) < $limit_date)
						{
							//trop vieux
							header("Access-Control-Allow-Origin: *");
							header("Content-type:application/json");
							echo(json_encode(array(
								'success' => false,
								'msg' => 'Avoir périmé'
							)));	
							die();							
						}
						
						header("Access-Control-Allow-Origin: *");
						header("Content-type:application/json");
						echo(json_encode(array(
							'success' => true,
							'slips' => $slips,
							'customer' => new Customer($slip->id_customer),
							'pointshop' => new ACaissePointshop($slip->id_pointshop),	
							'usages' => $usages
						)));
						
					}
					else
					{
						//code ean13 non valdid (vérification du bite de controle 13ème digit)
						header("Access-Control-Allow-Origin: *");
						header("Content-type:application/json");
						echo(json_encode(array(
							'success' => false,
							'msg' => 'Avoir inexistant'			
						)));							
					}
				}
				else
				{
					//code ean13 non valdid (vérification du bite de controle 13ème digit)
					header("Access-Control-Allow-Origin: *");
					header("Content-type:application/json");
					echo(json_encode(array(
						'success' => false,
						'msg' => 'Code invalide'			
					)));	
				}
			}
			
			//recherche de tous les avoirs d'un clients
			if(isset($_POST['id_customer']))
			{			
				$id_customer = (int)Tools::getValue('id_customer');				
				$slips = OrderSlip::getOrdersSlip($id_customer);	
				$filtered_slips = array();		
				

				if(!empty($slips))
				{
					$ids = array();
					foreach($slips as $i => $slip)
					{	
						if($peremtion_period == '0' || $slip['date_add'] >= $limit_date)
						{
								$ids[] = $slip['id_order_slip'];
								$slip['id'] = $slip['id_order_slip'];
								$filtered_slips[] = $slip;
						}
					}
					
					//on recherche les utilisations des slips
					$usages = PrestatillSlipUsage::getFromIdOrderSlips($ids);			
					
					header("Access-Control-Allow-Origin: *");
					header("Content-type:application/json");
					echo(json_encode(array(
						'success' => true,
						'slips' => $filtered_slips,
						'usages' => $usages
					)));
				}
				else
				{
					header("Access-Control-Allow-Origin: *");
					header("Content-type:application/json");
					echo(json_encode(array(
						'success' => false,
						'msg' => 'Aucun avoir pour ce client'			
					)));
				}
			}
			return;			
		}
		else
		{
			header("Access-Control-Allow-Origin: *");
			header("Content-type:application/json");
			echo(json_encode(array(
				'success' => false,
				'msg' => 'Impossible de récupérer la liste des avoirs depuis le serveur'						
			)));
			return;				
		}		
	}
	
	public function view() {
		return;
	}
	
	public function display()
    {
    	return true;
	}
	
	
	static private function add_ean13_check_digit($digits12){
		//first change digits to a string so that we can access individual numbers
		$digits =(string)$digits12;
		// 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits{1} + $digits{3} + $digits{5} + $digits{7} + $digits{9} + $digits{11};
		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;
		// 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits{0} + $digits{2} + $digits{4} + $digits{6} + $digits{8} + $digits{10};
		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;
		// 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
		$next_ten = (ceil($total_sum/10))*10;
		$check_digit = $next_ten - $total_sum;
		
		//p('$check_digit : '.$check_digit);
		//p($digits12 . $check_digit);
		return $digits12 . $check_digit;
	}
	
	
	
}