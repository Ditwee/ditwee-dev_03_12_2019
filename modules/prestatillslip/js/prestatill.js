$(function(){

	function prestatillslip(caisse_ref) {
		var module = this;
		var module_name = 'prestatillslip';
		
		//options
		var currency = '€';
		
		//propriétés du module
		this.total = 0;
		this.buffer = '';
		this.with_repaid = true;
		this.idOrderSlip = null;
		//pripriete du module pour le retour produit
		this.total_refund = 0;		
		this.refund_has_end_point = false;
		this.refund_has_point = false;
		this.refund_buffer = '';
		
		
		
		var _formater = new Intl.NumberFormat('fr-FR',{minimumFractionDigits:2});
		var _date_formater = new Intl.DateTimeFormat('fr-FR',{});
		var $interface;
		
		/**
		 * Indique si une virgule "silencieuse" existe à la fin de la saisie (en attendant une saiie d'un nombre) 
		 */
		this.has_end_point = false;
		
		/**
		 * Indique si il y a déjà une virgule dans la saisie en cours 
		 */
		this.has_point = false;
		
    	
		this.sendAmount = function() {
			//@TODO : voir receipt pour l'envoi de donner supplémaentaires
			PRESTATILL_CAISSE.payment.changePaymentAmount(this.total, this.with_repaid);
		};
		
		var scanner_customerKeyIntervalProcess = 0;
		var scanner_resetter_process = null;
		var scanner_relauncher_process = null;
		var scanner_buffer = '';
		
		
		var $interface = $('.prestatill_payment_interface[data-module-name="'+module_name+'"]');
		
		$('.prestatill-slip-by-barcode input',$interface).keyup(function(e){
			//console.log(e);
			var ean13 = $(this).val();
			//console.log('keypress ' + ean13);
			if(ean13.length == 13)
			{
				_searchUniqueSlip(ean13);
			}
		});
		
		this.init = function() {
			
			//$interface.css('maxHeight',$('#content').height()-64-64-$('.display_amount',$interface).height()-$('.payment_actions',$interface).height()-20);
			
			$('.ui-tab-content',$interface).css('maxHeight',$('#content').height()-45-64-64-$('.display_amount',$interface).height()-$('.payment_actions',$interface).height()-300);
			//console.log('je minit'); 
			$('body').find('div[data-module-name=prestatillslip] li:first-child').trigger('click');
			$('body').find('div[data-module-name=prestatillslip] .prestatill-slip-by-barcode .results').html('');
			
			
			var id_customer = (ACAISSE.customer.id == undefined)?0:ACAISSE.customer.id;
			var url_ajax = $interface.data('urlAjax');
			this.idOrderSlip = null;
			this.total = 0;
			$('.prestatill-slip-from-client',$interface).html('<div class="info">Chargement...</div>');
			$.ajax({
				url:url_ajax,
				method:'post',
				data:{
					'id_customer':id_customer
				}
			}).done(function(response){
				var html = '';
				if(response.success == true)
				{
					html = _generateHTMLSlipsList(response.slips,response.usages,false);					
				}
				else
				{
					html = '<div class="danger">' + response.msg + '</div>';	
				}	
				$('.prestatill-slip-from-client',$interface).html(html).addClass('current');
				$('.prestatill-slip-by-barcode',$interface).removeClass('current');
				
				
				
				
			}).fail(function(){
				alert('Error o78965 : Impossible de contacter le serveur pour obtenir la liste des avoirs');
			});
			
			
			
			this.total = 0;		
			PRESTATILL_CAISSE.payment.changePaymentAmount(this.total);
		};		
		
		var _generateHTMLSlipsList = function(slips,usages,unique) {
			var html = '<ul class="prestatillslip-list">';
					
			html += '<li class="prestatillslip-slip-header">';
					html += '	<span class="prestatillslip-id">n°</span>';
					//html += '	<span class="prestatillslip-amount">Tot. avoir</span>';
					html += '	<span class="prestatillslip-sold">Solde dispo.</span>';
					html += '	<span class="prestatillslip-date">Date</span>';
					html += '</li>';
			
			
			$.each(slips,function(i,slip){
				var amount = parseFloat(slip.total_products_tax_incl) + parseFloat(slip.total_shipping_tax_incl);
				var sold = amount;
				
				if(usages[slip.id] != undefined)
				{
					sold -= usages[slip.id]/100; 
				}
				
					if(sold>0)
					{
							html += '<li class="prestatillslip-slip" data-id-order-slip="' + slip.id + '" data-amount="' + sold + '">';
							html += '	<span class="prestatillslip-id">#'+ slip.id + '</span>';
							//html += '	<span class="prestatillslip-amount">'+ _formater.format(amount) + currency + '</span>';
							html += '	<span class="prestatillslip-sold">'+ _formater.format(sold) + currency + '</span>';
							html += '	<span class="prestatillslip-date">' + _date_formater.format(new Date(slip.date_add)) +'</span>';
							html += '</li>';
					}
					else if(sold==0 && (unique != undefined && unique == true))
					{
						html = '<div class="danger">L\'avoir a été utilisé en totalité</div>';
					}
				
			});
			html += '</ul>';
			
			return html;			
		};
		
		this.captureScanner = function(e) {
				clearInterval(scanner_resetter_process);
                if (scanner_relauncher_process !== null) {
                    clearTimeout(scanner_relauncher_process);
                }
                scanner_relauncher_process = setTimeout(function () {
                    scanner_resetter_process = setInterval(function () {
                        scanner_buffer = '';
                    }, 500);
                }, 150);

                if (scanner_buffer.length == 13 || (e.keyCode == 13 && scanner_buffer.length == 13)) // touche entrée
                {
                	//alert('auto lancement de la recherche car 13 caractères ou entrée "' + scanner_buffer + '"');
                	
                	//ici on lance la recherche
                    //$('[data-target=".prestatill-slip-by-barcode"]',$interface).click();
                    $('input',$interface).val(scanner_buffer);
                    _searchUniqueSlip(scanner_buffer);
                    
                    scanner_buffer = '';
                }
                else
                {
                    scanner_buffer += String.fromCharCode(e.keyCode);
                }
    	};
		
		var _searchUniqueSlip = function(ean13) {			
			var url_ajax = $interface.data('urlAjax');
			this.idOrderSlip = null;
			this.total = 0;
			$('.prestatill-slip-by-barcode .results',$interface).html('<div class="info">Chargement...</div>');
			$.ajax({
				url:url_ajax,
				method:'post',
				data:{
					'ean13':ean13
				}
			}).done(function(response){
				var html='';
				if(response.success)
				{
					var html = _generateHTMLSlipsList(response.slips,response.usages,true);					
					$('.prestatill-slip-by-barcode .results').html(html);
				}
				else
				{					
					//alert('Error o_slip0203 : '+response.msg);
					
					html = '<div class="danger">' + response.msg + '</div>';		
					$('.prestatill-slip-by-barcode .results',$interface).html(html);
					
				}	
				
			}).fail(function(){
				alert('Error o_slip0204 : impossible de communiquer avec le serveur');
			});
		};
		
		this.getExtraDatas = function() {
			var id_order_slip = parseInt('0'+$('.prestatill_payment_interface[data-module-name="'+module_name+'"] .prestatillslip-slip.selected').data('idOrderSlip'));
			return {
				'id_order_slip' : id_order_slip,
			};
		};
		
		//handler sur le click sur un avoir
		$('.prestatill_payment_interface[data-module-name="'+module_name+'"]').on('tap click','.prestatillslip-slip',function(e){
			var datas = $(this).data();	
			this.total = Math.min(datas.amount,parseFloat($('#keepToHaveToPay').html().replace(' ','')));
			
			this.id_order_slip = datas.idOrderSlip;		
			PRESTATILL_CAISSE.payment.changePaymentAmount(this.total);
			
			$('.prestatill_payment_interface[data-module-name="'+module_name+'"] .prestatillslip-slip').removeClass('selected');
			$(this).addClass('selected');		
		});
		
		
		/****************************/
		/** Gestion en mode retour **/
		/****************************/
		
		//]TODO attention
		$('.prestatill_return_interface[data-module-name="'+module_name+'"]').on('click tap','.numeric_keyboard>span',function(e){
			
			var key_code = $(this).html();
			//console.log('buffer before '+module.buffer);
			switch(key_code)
			{
				case '.':
				case ',':
					if(!module.refund_has_end_point)
					{
						module.refund_has_end_point = true;
					}
					module.refund_has_point = true;
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					if(module.refund_has_point && module.refund_has_end_point)
					{
						module.refund_buffer += '.';						
					}
					
					var test = module.refund_buffer.split('.');
					if(!module.refund_has_point || (module.refund_has_point && test[1] != undefined && test[1].length<2))
					{										
						module.refund_buffer += key_code;
						module.refund_has_end_point = false;
					}
					break;
				case 'C':
				case 'R':
					if(module.refund_buffer.length >0)
					{
						module.refund_buffer = '';
						module.refund_has_end_point = false;
						module.refund_has_point = false;
					}
					break;
			}
			
			var value = module.refund_buffer;
			if(value == '')
			{
				value = 0;
			}
			module.total_refund = parseFloat(value);
			module.sendReturnAmount();
			//console.log('buffer after '+module.buffer + ' soit '+ module.total);
			//console.log(module);
		});
		
		
		this.sendReturnAmount = function() {			
			PRESTATILL_CAISSE.return.changeRefundAmount(this.total_refund);
		};
		
		
		
	};
	
	/*************
	Enregistrement du module de payment dans l'instance de la caisse
	**************/	
	PRESTATILL_CAISSE.payment.modules.push(new prestatillslip());	
});
