<?php
require_once(dirname(__FILE__).'/classes/PrestatillSlipUsage.php');

class PrestatillSlip extends PaymentModule
{
	
	const SECRET_TOCKEN = 'jhfgds_fdsfdjh5432436-azj324';
	
	public function __construct()
	{
		$this->name = 'prestatillslip';
		$this->tab = 'payments_gateways';
		$this->version = '1.0.1';
		$this->author = 'PrestaTill SAS';
		$this->bootstrap = 'true';
		parent::__construct();
		$this->displayName = $this->l('Module Payment Slip for Prestatill');
		$this->description = $this->l('Module Payment Slip for Prestatill');	
	}
	
	public function getTicketPaymentLabel($params = NULL)
	{
		return $this->l('Avoir');
	}
	
	public function install()
	{
		
		
        $PRESTATILLSLIP_nb_month_validity = Configuration::get('PRESTATILLSLIP_nb_month_validity');
		if(!in_array($PRESTATILLSLIP_nb_month_validity, array(
			'0',
			'-1 month',
			'-2 month',
			'-3 month',
			'-4 month',
			'-5 month',
			'-6 month',
			'-7 month',
			'-8 month',
			'-9 month',
			'-10 month',
			'-11 month',
			'-1 year',
			'-2 year',
			'-3 year',
		)))
		{
			Configuration::updateValue('PRESTATILLSLIP_nb_month_validity', '-1 year');
		}
		if(
			!parent::install()
			|| !$this->registerHook('displayPayment')
			|| !$this->registerHook('displayPaymentReturn')
			//hooks Prestatill
			|| !$this->registerHook('displayPrestatillInjectJS')
			|| !$this->registerHook('displayPrestatillInjectCSS')
			|| !$this->registerHook('displayPrestatillPaymentButton')
			|| !$this->registerHook('displayPrestatillPaymentInterface')			
			|| !$this->registerHook('actionPrestatillRegisterNewPaymentPart')
			
			// Hook Spli
			|| !$this->registerHook('actionPrestatillCreatePartialOrderSlip')
			
			// Hook Prestatill retour
			|| !$this->registerHook('displayPrestatillReturnPaymentButton')			
			|| !$this->registerHook('displayPrestatillReturnPaymentInterface')
			
			
			|| !$this->_installSql() 
			
			//Hook Prestatill Export Compta
			//rapport financier
			|| !$this->registerHook('displayPrestatillFinancialReportHeader')
			|| !$this->registerHook('displayPrestatillFinancialReportFooter')
			//rapport de vente
			|| !$this->registerHook('displayPrestatillFinancialSalesHeader')
			|| !$this->registerHook('displayPrestatillFinancialSalesFooter')			
			
			//onstallation d'un nouvau statut de commande
			|| !$this->_installOrderState()
		)
		{
			return false;
		}
		return true;
	
	}

	private function _installSql()
    {
        include(dirname(__FILE__).'/sql/install.php');
        $result = true;
	    foreach ($sql_requests as $request){
            if (!empty($request))
            $result &= Db::getInstance()->execute(trim($request));
        }
        return $result;
    }
	
	protected function _installOrderState()
	{
		//on check si ce status n'a pas DEJA était uinstallé
		if(!Configuration::hasKey('PRESTATILL_SLIP_ID_ORDER_STATE'))
		{
			$context = Context::getContext();
			$id_lang = $context->language->id;
			
			$os = new OrderState(null,$id_lang);
			$os->send_email = false;
			$os->module_name = 'prestatillslip';
			$os->invoice = true;
			$os->color = '#44bb00';
			$os->logable = true;
			$os->shipped = true;
			$os->unremovable = false;
			$os->delivery = true;
			$os->hidden = false;
			$os->paid = true;
			$os->pdf_delivery = false;
			$os->pdf_invoice = true;
			$os->deleted = false;
			
			$os->name = 'Paiement accepté (avoir)';
			$os->save();
			Configuration::updateValue('PRESTATILL_SLIP_ID_ORDER_STATE',$os->id);
			return true;
		}
		return true;
	}
	
	public function getHookController($hook_name)
	{
		require_once(dirname(__FILE__).'/controllers/hook/'.$hook_name.'.php');
		$controller_name = $this->name.$hook_name.'Controller';
		$controller  = new $controller_name($this, __FILE__, $this->_path);
		return $controller;
	}
	
	public function hookDisplayPayment ($params)
	{
		$controller = $this->getHookController('displayPayment');
		return $controller->run($params);
			
	}
	
	
	/**
	 * Injection JS dans le header de la caisse
	 */
	public function hookDisplayPrestatillInjectJS($params)
	{
		return '<script src="'._MODULE_DIR_.$this->name.'/js/prestatill.js"></script>';
	}
	
	/**
	 * Injection CSS dans le header de la caisse
	 */
	public function hookDisplayPrestatillInjectCSS($params)
	{
		return '<link  href="'._MODULE_DIR_.$this->name.'/css/prestatill.css" rel="stylesheet" type="text/css" media="all" />';		
	}	
	
	/**
	 * Affichage du boutton de paiement
	 */
	public function hookDisplayPrestatillPaymentButton($params)
	{
		//$this->context->smarty->assign($assigns);
	    $html = $this->createTemplate('prestatill_payment_button.tpl')->fetch();
        return $html;
	}	
	
	/**
	 * Affichage d'une interface de saisie du montant (non obligatoire, si pas d'interface de saisie : exemple CB)
	 * l'interface doit soit d'une colonne afin d'etre compatible avec la vue paiement multiple
	 */
	public function hookDisplayPrestatillPaymentInterface($params)
	{
		$this->context->smarty->assign('secret_tocken',htmlentities(self::SECRET_TOCKEN));	
		$url = $this->context->link->getModuleLink(
			$this->name,
			'getslip',
			array(
				'tocken' => self::SECRET_TOCKEN
			),
			Configuration::get('PS_SSL_ENABLED')
		);
		$this->context->smarty->assign('url_ajax',$url);
					
		
	    $html = $this->createTemplate('prestatill_payment_interface.tpl')->fetch();
        return $html;
	}
	
	
	
	public function hookDisplayPrestatillReturnPaymentButton($params)
	{
        return $this->hookDisplayPrestatillPaymentButton($params);
	}
	
	public function hookDisplayPrestatillReturnPaymentInterface($params)
	{
	    $html = $this->createTemplate('prestatill_return_interface.tpl')->fetch();
        return $html;
	}
	
	/**
	 * Hook s'exécutant lors de l'enregistrement d'un nouveau payement part
	 * avec les paramètres suivant :
	 * 	- order
	 *  - payment_part
	 *  - extra_datas
	 *  
	 */
	public function hookActionPrestatillRegisterNewPaymentPart($params)
	{
		$payment_part = $params['payment_part'];
		$extra_datas = $params['extra_datas'];
		if($payment_part->module_name == $this->name)
		{
			require_once(__DIR__.'/classes/PrestatillSlipUsage.php');
			
			$usage = new PrestatillSlipUsage();
			$usage->id_order_slip = $extra_datas['id_order_slip'];
			$usage->id_employee = Context::getContext()->employee->id;
			$usage->amount = (int)$payment_part->payment_amount;
			
			$usage->use_for_id_order = (int)$params['id_order'];
			$usage->use_for_id_ticket = (int)$params['id_ticket'];; 
			$usage->use_for_id_customer = (int)$params['id_customer'];
			$usage->use_for_id_pointshop = (int)$params['id_pointshop'];
			$usage->use_for_id_service = (int)$params['id_service'];
			
			if(Module::isEnabled('prestatillexportcompta'))
			{
				$day = date('Y-m-d');
		
				// On récupère le jour du rapport en cours
				$id_report = PrestatillFinancialReport::ReportExists($day, $params['id_pointshop']);
				if($id_report != false)
				{
					$report = new PrestatillFinancialReport($id_report);
					if(Validate::isLoadedObject($report))
					{
						$usage->use_for_id_service = $report->id_service;
						$usage->id_financial_report = $report->id;
						$usage->day = $report->day;
					}
				}
			}
			
			$usage->save();			
		}		
	}
	
	public function hookActionPrestatillCreatePartialOrderSlip($params)
	{
		if($params['res'] == 1)
		{
			$order = $params['order'];
			$day = date('Y-m-d');
			
			// Récupérer l'order_slip et ajouter le day sur order_slip
			$order_slips = OrderSlip::getOrdersSlip($order->id_customer,$order->id);
			
			$last_caisse_state = ACaisseTicket::getLastCaisseState($order->id_pointshop);
			if(!empty($last_caisse_state))
			{
				$id_service = $last_caisse_state[0]['id_service'];
				$day = substr($last_caisse_state[0]['day'], 0, 10);
			}
		
		
			// On récupère le jour du rapport en cours
			$id_report = PrestatillFinancialReport::ReportExists($day, $order->id_pointshop);
			
			
			$report = new PrestatillFinancialReport($id_report);
			
			if(Validate::isLoadedObject($report))
			{
				$day = $report->day;
			}
			
			//d(array('day' => $day, 'id_report' => $id_report, 'order' => $order->id_pointshop));
			
			foreach($order_slips as $order_slip)
			{
				$os = new OrderSlip((int)$order_slip['id_order_slip']);
				
				if(Validate::isLoadedObject($os))
				{
					$os->day = $day;
					$os->id_service = (int)$report->id_service;
					$os->save();
				}
			}
				
			$updateReport = PrestatillFinancialReport::updateReport(date('Y-m-d H:i:s'),$params['order']->id,false,true,$params);
		}
	}
	
	
	public function hookDisplayPrestatillFinancialReportHeader($params)
	{
		return '';	//'<div>FOOTER hookDisplayPrestatillFinancialReportHeader</div>';
	}
	public function hookDisplayPrestatillFinancialReportFooter($params)
	{
		$html = '';
		
		//$html .= '<pre>';
		//$html .= print_r($params,true);
		
		$day = $params['day'];
		
		// On récupère le jour du rapport en cours
		$id_report = PrestatillFinancialReport::ReportExists($day, $params['id_pointshop']);
		if($id_report != false)
		{
			$report = new PrestatillFinancialReport($id_report);
			if(Validate::isLoadedObject($report))
			{
				$day = $report->day;
			}
		}
		
		$slips_buying = PrestatillSlipUsage::getList($day,$params['id_pointshop']);
		
		$slips_usage = PrestatillSlipUsage::getUsageList($day,$params['id_pointshop']);	
		
		foreach($slips_usage as $key => $usage)
		{
			if(!empty($usage['extra_datas']))
			{
				$slips_usage[$key]['extra_datas'] = json_decode($usage['extra_datas']);
			}
		}
		
		if((Tools::getValue('controller') == 'ACaisseUse'))
		{
			$tpl = $this->createTemplate('prestatill_z_footer.tpl');
		}
		else
		{
			$tpl = $this->createTemplate('prestatill_financial_report_footer.tpl');
		}	
			
		$tpl->assign(array(
            'slips_usage' => $slips_usage,
            'slips_buying' => $slips_buying,
            'tocken_order' => Tools::getAdminTokenLite('AdminOrders'),
            'currency' => new Currency(Configuration::get('PS_CURRENCY_DEFAULT'))
        ));
		
	    $html .= $tpl->fetch();
		return $html;
		
	}
	public function hookDisplayPrestatillFinancialSalesHeader($params)
	{
		return ''; //'<div>HEADER hookDisplayPrestatillFinancialSalesHeader</div>';
		
	}
	public function hookDisplayPrestatillFinancialSalesFooter($params)
	{
		return ''; //'<div>FOOTER hookDisplayPrestatillFinancialSalesFooter</div>';
		
	}
	
	
	
    /***************************************************/
    /***** Traitement principale de page la config *****/
    /***************************************************/
    public function getContent() {
        $html='';
        $debug='';
        $id_lang = (int)Context::getContext()->language->id;
        
        // si on a envoyé le formulaire de configuration générale du module
        if (Tools::isSubmit('PRESTATILLSLIP_nb_month_validity')) {
            //$html.='<pre>'.print_r($_POST,true).'</pre>';
            
            // on traite la configuration des clients
            Configuration::updateValue('PRESTATILLSLIP_nb_month_validity', Tools::getValue('PRESTATILLSLIP_nb_month_validity'));            
		}
        
        $token = Tools::getAdminTokenLite('AdminModules');
        $assigns = array();
        $assigns['token'] = $token;
        $assigns['module']=$this;
		
        $assigns['PRESTATILLSLIP_nb_month_validity'] = Configuration::get('PRESTATILLSLIP_nb_month_validity');
        
		/////////////////// END UPDATE SIREEN 2.0 //////////////////////////
		
        
		$assigns['module_version'] = $this->version;
		$assigns['module_display_name'] = $this->displayName;

        $this->context->smarty->assign($assigns);
		
        $html .= $this->display(__FILE__, 'getContent.tpl');

        return $html;
    }
	
	

    //surcharge pour tester aussi le dossier view dans le module
    public function createTemplate($tpl_name,$type = 'hook')
    {
    	//d(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    	//d(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name);
    		
    	
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name)) {
            return $this->context->smarty->createTemplate(_PS_MODULE_DIR_.''.$this->name.'/views/templates/'.$type.'/'.$tpl_name, $this->context->smarty);
        }
        return parent::createTemplate($tpl_name);
    }

    public function getTplPath($tpl_name)
    {
        if(file_exists(_PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_THEME_DIR_.'modules/'.$this->name.'/views/templates/admin/'.$tpl_name;
        }
        else if(file_exists(_PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name)) {
            return _PS_MODULE_DIR_.''.$this->name.'/views/templates/admin/'.$tpl_name;
        }
    }
}
	