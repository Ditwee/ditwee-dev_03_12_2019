<?php 


$sql_requests = array();
$langs = Language::getLanguages(false);

/**
 * TABLE SLIP_USAGE
 */
$sql_requests[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'prestatill_slip_usage`(
					  `id_prestatill_slip_usage` INT NOT NULL AUTO_INCREMENT,
					  `id_order_slip` INT NOT NULL DEFAULT \'0\',
					  `id_employee` INT NOT NULL DEFAULT \'0\',
					  `amount` INT NOT NULL DEFAULT \'0\',
					  `use_for_id_ticket` INT NOT NULL DEFAULT \'0\',
					  `use_for_id_order` INT NOT NULL DEFAULT \'0\',
					  `use_for_id_customer` INT NOT NULL DEFAULT \'0\',
					  `use_for_id_pointshop` INT NOT NULL DEFAULT \'0\',
					  `use_for_id_service` INT NOT NULL DEFAULT \'0\',	
					  `date_add` VARCHAR(25) NOT NULL,
					  `date_upd` VARCHAR(25) NOT NULL,
					  PRIMARY KEY(`id_prestatill_slip_usage`)
				) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_unicode_ci';

/**
* MISES A JOUR DES CHAMPS DEPUIS LA VERSION 2
* ALTER TABLE UNIQUEMENT SI LE CHAMPS N'EXISTE PAS
*/
$tables = 	array(
				'order_slip' => 
					array(
						'id_service' => 'int(11) NOT NULL DEFAULT 1',
						'day' => 'VARCHAR(10) NOT NULL DEFAULT \'0000-00-00\'',
					),
				'prestatill_slip_usage' => 
					array(
            			'id_financial_report' => 'INT(10) UNSIGNED NOT NULL DEFAULT 0',
            			'day' => 'VARCHAR(10) NOT NULL DEFAULT \'0000-00-00\'',
					),					
				); 
						
/*
 * TRAITEMENT DES CHAMPS
 */				
foreach ($tables as $table => $fields)
{
	foreach ($fields as $field => $type)
	{
		$sql_requests[] = 'SET @s = (SELECT IF( (SELECT COUNT(column_name)
			        FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = "' . _DB_PREFIX_ .$table. '"
			        AND table_schema = "'._DB_NAME_.'" AND column_name = "'.$field.'"
			    ) > 0, "SELECT 1", "ALTER TABLE ' . _DB_PREFIX_ .$table. ' ADD '.$field.' '.$type.'"
			)); 
		PREPARE stmt FROM @s; 
		EXECUTE stmt; 
		DEALLOCATE PREPARE stmt;';
	}
}	    
    