<div class="print-together">
	<div class="col-lg-12">
		<div id="" class="panel">
			<header class="panel-heading">
				<i class="icon-barcode"></i> Avoirs : édition et utilisation
			</header>
			<div class="table-responsive">
			{if !empty($slips_usage)}
				<table class="table data_table table-striped" id="">
					<thead>
						<tr>
							<th class="text-left" colspan="6"><b>Avoirs utilisés</b></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center"><b>#</b></td>
							<td class="text-center"><b>Date</b></td>
							<td class="text-center"><b>{l s='N° de commande' mod='prestatillexportcompta'}</b></td>
							<td class="text-center"><b>Devise</b></td>
							<td class="text-center"><b>Montant</b></td>
						</tr>
						{assign var=tot_reliquat value=0}
						{foreach from=$slips_usage item=detail name=foo}
						<tr>
							<td class="text-center">{$detail.id_order_slip}</td>
							<td class="text-center">{$detail.date_add|date_format:"%d/%m/%Y %H:%M:%S"}<i></i></td>
							<td class="text-center"><a href="index.php?controller=AdminOrders&id_order={$detail.use_for_id_order}&vieworder&token={$tocken_order}">{$detail.use_for_id_order}</a></i></td>
							<td class="text-center">{$currency->iso_code}</td>
							<td class="text-right">{Tools::displayPrice($detail.use_amount/100, $currency)}</td>
						</tr>
						{assign var=tot_reliquat value=$tot_reliquat+$detail.amount}
						{/foreach}
					</tbody>
				</table>
			{else}
				<b>{l s='Avoirs utilisés' mod='prestatillexportcompta'}</b>
				<br />
				<br />
				<span style="display:block;" class="alert alert-info">{l s='Aucun avoir utilisé ce jour' mod='prestatillexportcompta'}</span>
			{/if}
			</div>
			<br />
			<hr />
			<br />
			<div class="table-responsive">
				{if !empty($slips_buying)}
				<table class="table data_table table-striped" id="">
					<thead>
						<tr>
							<th class="text-left" colspan="8"><b>{l s='Avoirs édités' mod='prestatillexportcompta'}</b></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center"><b>{l s='#' mod='prestatillexportcompta'}</b></td>
							<td class="text-center"><b>{l s='Date de l\'avoir' mod='prestatillexportcompta'}</b></td>
							<td class="text-center"><b>{l s='N° de commande' mod='prestatillexportcompta'}</b></td>
							<td class="text-center"><b>{l s='Devise' mod='prestatillexportcompta'}</b></td>
							<td class="text-center"><b>{l s='Montant' mod='prestatillexportcompta'}</td>
						</tr>
						{foreach from=$slips_buying item=detail name=foo}
						<tr>
							<td class="text-center">{$detail.id_order_slip}</td>
							<td class="text-center">{$detail.date_add|date_format:"%d/%m/%Y %H:%M:%S"}</td>
							<td class="text-center"><a href="index.php?controller=AdminOrders&id_order={$detail.id_order}&vieworder&token={$tocken_order}">{$detail.id_order}</a></i></td>
							<td class="text-center">{$currency->iso_code}</td>
							<td class="text-right">{Tools::displayPrice($detail.amount, $currency)}</td>
						</tr>
						{/foreach}
					</tbody>
				</table>
				{else}
				<b>{l s='Avoirs édités' mod='prestatillexportcompta'}</b>
				<br />
				<br />
				<span style="display:block;" class="alert alert-info">{l s='Aucun avoir édité ce jour' mod='prestatillexportcompta'}</span>
				{/if}
			</div>
		</div>
	</div>
</div>
