<div class="prestatill_payment_interface" data-module-name="prestatillslip" data-url-ajax="{$url_ajax}">
  <div class="ui-tab ui-tab-horizontal" id="slip-tab-select">
    <ul class="ui-tab-menu">
      <li data-target=".prestatill-slip-from-client" class="current">Avoir(s) du client</li>
      <li data-target=".prestatill-slip-by-barcode">Saisie manuelle</li>
    </ul>
    <div class="ui-tab-content">
      <div class="ui-tab-item prestatill-slip-from-client current">
        Loading...
      </div>
      <div class="ui-tab-item prestatill-slip-by-barcode">
        Saisi du code barre complet indiqué sur l'avoir : 
        <input style="margin:10px;">
        <div class="results"></div>
      </div>
    </div>
  </div>
</div>
<script>
  $(function(){
    $('#slip-tab-select').uiTab({
      'onChange' : function(plugin,target,$target) {
        switch(target)
        {
          case '.prestatill-slip-by-barcode':
            $('input',$target).val('').focus();
            break;
          default:
            
            break;
        }      
      },
    });
  });
</script>
