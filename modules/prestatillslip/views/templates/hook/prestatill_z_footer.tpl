<div style="text-transform: uppercase;text-align: left;">{l s='Avoirs' mod='prestatillexportcompta'}</div>
	<div class="separate"></div>
		{if !empty($slips_usage)}
			<table style="width:100%;">
				<thead>
					<tr>
						<td class="text-left" colspan="6" style="text-transform: uppercase;">Avoirs utilisés</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="text-left" style="text-transform: uppercase;">{l s='Avoir n°' mod='prestatillexportcompta'}</td>
						<td class="text-center" style="text-transform: uppercase;">{l s='N° cmd' mod='prestatillexportcompta'}</td>
						<td class="text-right" style="text-transform: uppercase;">Montant</td>
					</tr>
					{assign var=tot_reliquat value=0}
					{foreach from=$slips_usage item=detail name=foo}
					<tr>
						<td class="text-left">{$detail.id_order_slip}</td>
						<td class="text-center">{$detail.use_for_id_order}</td>
						<td class="text-right">{$detail.use_amount/100}Eur</td>
					</tr>
					{assign var=tot_reliquat value=$tot_reliquat+$detail.amount}
					{/foreach}
				</tbody>
			</table>
		{else}
			{l s='Avoirs utilisés' mod='prestatillexportcompta'}
			<br />
			<br />
			<span style="display:block;" class="alert alert-info">{l s='Aucun avoir utilisé ce jour' mod='prestatillexportcompta'}</span>
		{/if}
		<br />
		<div class="separate"></div>
		<br />
		{if !empty($slips_buying)}
		<table style="width:100%;" id="">
			<thead>
				<tr>
					<td class="text-left" colspan="8" style="text-transform: uppercase;">{l s='Avoirs édités' mod='prestatillexportcompta'}</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="text-left" style="text-transform: uppercase;">{l s='Avoir n°' mod='prestatillexportcompta'}</td>
					<td class="text-center" style="text-transform: uppercase;">{l s='N° cmd' mod='prestatillexportcompta'}</td>
					<td class="text-right" style="text-transform: uppercase;">{l s='Montant' mod='prestatillexportcompta'}</td>
				</tr>
				{foreach from=$slips_buying item=detail name=foo}
				<tr>
					<td class="text-left">{$detail.id_order_slip}</td>
					<td class="text-center">{$detail.id_order}</td>
					<td class="text-right">{$detail.amount}Eur</td>
				</tr>
				{/foreach}
			</tbody>
		</table>
		{else}
		{l s='Avoirs édités' mod='prestatillexportcompta'}
		<br />
		<br />
		<span style="display:block;" class="alert alert-info">{l s='Aucun avoir édité ce jour' mod='prestatillexportcompta'}</span>
		{/if}
		<br />
<div class="separate_double"></div>

<br />