{*
* 2007-2017 Prestatill
*
* @author    Prestatill SAS <contact@prestatill.com>
* @copyright 2017 Prestatill SAS
* @license   http://store.prestatill.com
* 
*}

<div class="clearfix"></div>
<h3>
    <i class="icon-cogs"></i> {l s='Paramètres généraux' mod='prestatillslip'} <small>{$module_display_name|escape:'htmlall':'UTF-8'}</small>
</h3>
<form role="form" class="form-horizontal"  action="#" method="POST">
		{*<h4>{l s='ICI un titre' mod='prestatillslip'}</h4>*}
		<div class="form-group">
		
		
      <label class="control-label col-lg-4" for="PRESTATILLSLIP_nb_month_validity">
          <span class="label-tooltip" data-toggle="tooltip"
            title="{l s='Les avoirs dépassant la durée de validité ne seront plus utilisables dans la caisse'}">
            {l s='Durée de validité des avoirs en mois'}
          </span>
      </label>
      <div class="col-lg-8">        
        <div class="col-lg-4">
          <select name="PRESTATILLSLIP_nb_month_validity" id="PRESTATILLSLIP_nb_month_validity">          
            <option value="0"{if $PRESTATILLSLIP_nb_month_validity == "0"} selected="selected"{/if}>Sans limite</option>
            {for $i = 1 to 11}
            <option value="-{$i} month"{if $PRESTATILLSLIP_nb_month_validity == "-$i month"} selected="selected"{/if}>{$i} mois</option>
            {/for}
            <option value="-1 year"{if $PRESTATILLSLIP_nb_month_validity == "-1 year"} selected="selected"{/if}>1 an</option>
            <option value="-2 year"{if $PRESTATILLSLIP_nb_month_validity == "-2 year"} selected="selected"{/if}>2 ans</option>
            <option value="-3 year"{if $PRESTATILLSLIP_nb_month_validity == "-3 year"} selected="selected"{/if}>3 ans</option>
          </select>
        </div>
      </div>
		</div>
		{*
		<div class="form-group">
			<label class="control-label col-lg-4" for="FORCE_DEFAULT_CARRIER">
					<span class="label-tooltip" data-toggle="tooltip"
						title="{l s='Activez cette option pour passer outre les paramètres de frais de port de Prestashop ;'}">
						{l s='Désactiver les frais de transport en point de vente' mod='prestatillslip'}
					</span>
			</label>
			<div class="col-lg-8">				
				<div class="col-lg-4">
					<span class="switch prestashop-switch fixed-width-lg">
						<input type="radio" name="FORCE_DEFAULT_CARRIER" id="FORCE_DEFAULT_CARRIER_on" value="1" {if $ACAISSE_FORCE_DEFAULT_CARRIER==1}checked{/if}>
						<label for="FORCE_DEFAULT_CARRIER_on" class="radioCheck">
							Oui
						</label>
						<input type="radio" name="FORCE_DEFAULT_CARRIER" id="FORCE_DEFAULT_CARRIER_off" value="0" {if $ACAISSE_FORCE_DEFAULT_CARRIER==0}checked{/if}>
						<label for="FORCE_DEFAULT_CARRIER_off" class="radioCheck">
							Non
						</label>
						<a class="slide-button btn"></a>
					</span>
				</div>
			</div>
		</div>	
        *}
		<div class="panel-footer">
            <div class="btn-group pull-right">
                <button name="submitGeneralOptions" id="submitGeneralOptions" type="submit" class="btn btn-default"><i class="process-icon-save"></i> {l s='Save' mod='prestatillslip'}</button>
            </div>
        </div>
</form>