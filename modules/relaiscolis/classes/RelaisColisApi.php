<?php
/**
 * 1969-2018 Relais Colis
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@relaiscolis.com so we can send you a copy immediately.
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1969-2018 Relais Colis
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class RelaisColisApi
{
    const TYPE_DELIVERY = 'rc_delivery';
    const TYPE_MAX = 'rc_max';
    const TYPE_HOME = 'home_delivery';
    const TYPE_RETURN = 'return';
    const TYPE_C2C = 'rc_c2c';
    const TYPE_DRIVE = 'drive';
    const TYPE_SCHEDULE = 'schedule';
    const TYPE_BELGIUM = 'belgium';
    const INSEE_DEFAUT = '0';
    const INSEE_HOME = '0';
    const PRODUCT_FAMILY = '08';
    const DELIVERY_TYPE_DEFAULT = '00';
    const DELIVERY_TYPE_MAX = '08';
    const ACTIVITY_CODE = '05';
    const ACTIVITY_CODE_HOME = '08';
    const ACTIVITY_CODE_DRIVE = '07';
    const DELIVERY_TYPE_PAYMENT_DEFAULT = '3';
    const ORDER_TYPE_DEFAULT = '1';
    const ORDER_TYPE_SUB_DEFAULT = '1';
    const SENSITIVE_DEFAULT = '0';
    const PICKING_DEFAULT = '0';
    /**
     * Call Relais Colis REST Webservice and get xml response
     *
     * @param string $entity_method : "entity/method" of the remote WS
     * @param array $post_array :
     * @return SimpleXMLElement
     * @throws PrestaShopException
     */
    protected static function rcWsXmlCall($entity_method, $post_array, $is_zip = false)
    {
        if (!($activation_key = Configuration::get('RC_ACTIVATION_KEY'))) {
            throw new PrestaShopModuleException('No Activation Key');
        }

        $curl = curl_init(Configuration::get('RC_REST_URL').'api/'.$entity_method);
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_USERPWD => $activation_key.":".$activation_key,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => http_build_query($post_array),
                CURLOPT_SSL_VERIFYPEER => false
            )
        );
        $curl_response = curl_exec($curl);

        if ($is_zip) {
            return($curl_response);
        }
        $http_response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($curl_response === false) {
            $curlError = curl_error($curl);
            curl_close($curl);
            return false;
            /*throw new PrestaShopModuleException('Error occured during call of '.$entity_method.'<br/>'.
            ($http_response_code ? 'HTTP code : '.$http_response_code.'<br/>' : '').
            ($curlError ? $curlError.'<br/>' : ''));*/
        }
        curl_close($curl);

        //Check HTTP response code
        switch ($http_response_code) {
            case 200:
                // OK, continue
                break;
            case 401:
                return false;
            case 404:
                return false;
            default:
                throw new PrestaShopModuleException('Error response code received for '.$entity_method.'<br/>'.
                ($http_response_code ? 'HTTP code : '.$http_response_code.'<br/>' : '').
                ($curl_response ? '<pre>'.htmlentities($curl_response).'</pre>' : ''));
        }

        $xml = simplexml_load_string($curl_response, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS);

        if ($xml === false) {
            throw new PrestaShopModuleException('Incorrect data received for '.$entity_method);
        }

        return $xml;
    }

    /**
     * Convert to PHP boolean from xml string :
     * "1", "true", "on", "yes" => true
     * other => false
     *
     * @param mixed $val
     * @return boolean
     */
    protected static function xmlBool($val)
    {
        return is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN) : (bool)$val;
    }

    /**
     * Get the configurations values of the account and update them in PS
     *
     * @param string $activation_key
     * @param string $version
     * @return mixed false if there is no activation key, String message if the account is not active or true if success
     */
    public static function processConfigurationAccount($activation_key = '', $version = '')
    {
        if (!$activation_key) {
            return false;
        }
        if (!$version) {
            $version = Configuration::get('RC_MODULE_VERSION');
        }
        $xml = self::rcWsXmlCall(
            'enseigne/getConfiguration',
            array(
                'activationKey' => Configuration::get('RC_ACTIVATION_KEY'),
                'moduleName' => 'relais colis',
                'moduleVersion' => $version,
                'cmsName' => 'Prestashop',
                'cmsVersion' => _PS_VERSION_
            )
        );

        if (!$xml) {
            return false;
        }
        if (!self::xmlBool($xml->active)) {
            Configuration::updateValue('RC_IS_ACTIVE', 0);
            return 'Votre compte n\'est pas actif';
        }
        $option_value = '';
        if (isset($xml->options->entry)) {
            foreach ($xml->options->entry as $option) {
                $option_value .= $option->value.';';
            }
        }
        Configuration::updateValue('RC_IS_ACTIVE', 1);
        Configuration::updateValue('RC_OPTIONS', $option_value);
        Configuration::updateValue('RC_ENS_ID', (string)$xml->ens_id);
        Configuration::updateValue('RC_ENS_ID_LIGHT', (string)$xml->ens_id_light);
        Configuration::updateValue('RC_LIVEMAP_API', (string)$xml->livemapping_api);
        Configuration::updateValue('RC_LIVEMAP_PID', (string)$xml->livemapping_pid);
        Configuration::updateValue('RC_LIVEMAP_KEY', (string)$xml->livemapping_key);

        // update carriers status in PS
        self::setCarrierState();
        return true;
    }

    /**
     * Call Relais Colis REST Webservice and get xml response as zip file
     *
     * @param string $activation_key
     * @return boolean false if there is no activation key, true if success
     */
    public static function processGetEvts($activation_key = '')
    {
        if (!$activation_key) {
            return false;
        }

        $xml = self::rcWsXmlCall(
            'enseigne/getEvenements',
            array(
                'activationKey' => Configuration::get('RC_ACTIVATION_KEY')
            ),
            true
        );

        $response = json_decode($xml);
        $file = base64_decode($response->file);

        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=".$response->filename);
        header("Content-Length: ".Tools::strlen($file));
        echo $file;

        /*
          if (!self::xmlBool($xml->active)) {
          Configuration::updateValue('RC_IS_ACTIVE', 0);
          return 'Votre compte n\'est pas actif';
          }
         */
        exit;
    }

    /**
     * Call Relais Colis REST Webservice and update the Orders states
     *
     * @param string $activation_key
     * @return boolean false if there is no activation key, true if success
     */
    public static function processGetEvtsData($activation_key = '')
    {
        if (!$activation_key) {
            return false;
        }

        $xml = self::rcWsXmlCall(
            'package/getDataEvts',
            array(
                'activationKey' => $activation_key,
            )
        );

        if (isset($xml->entry)) {
            $array = self::xml2array($xml);

            // manage multiple entry
            if (is_array($array['entry'])) {
                foreach ($array['entry'] as $orders) {
                    self::updateOrderState($orders);
                }
            } else {
                // single entry
                self::updateOrderState($array['entry']);
            }
        }
        return true;
    }

    /**
     * Update the state of an Order
     *
     * @param array $data the order
     */
    public static function updateOrderState($data)
    {
        $extract_number = Tools::substr($data, 1, 14);
        
        // Only the first package of an order modify the state
        if (Tools::substr($extract_number, 13, 2) == "01") {
            $evts_code = Tools::substr($data, 15, 3);
            $evts_justif = Tools::substr($data, 26, 3);
            $evtsDate = Tools::substr($data, 46, 4) .'-'. Tools::substr($data, 50, 2) .'-'. Tools::substr($data, 52, 2) .' '. Tools::substr($data, 54, 2) .':'. Tools::substr($data, 56, 2) .':00';

            // todo implement multiple shipping
            // by default only 1 package by order (01)
            //$pdf_number = $extract_number.'01';

            /*$id_order = self::getRelaisColisOrderId($pdf_number);
            if (!(int)$id_order) {
                // maybe a return order
                $id_order = self::getReturnOrderId($extract_number);
            }*/

            //if (!(int)$id_order) {
            $id_order = self::getRelaisColisOrderIdWithExtractNumber($extract_number);
            //}

            if ((int)$id_order) {
                $order = new Order((int)$id_order);
                $id_order_state = self::getIdstate($evts_code, $evts_justif);
                if ((int)$id_order_state && ((int)$id_order_state != (int)$order->current_state)) {
                    $history = new OrderHistory();
                    $history->id_order = (int)$id_order;
                    $history->id_employee = 1;
                    $history->changeIdOrderState($id_order_state, (int)$id_order);
                    $history->date_add = $evtsDate;//date('Y-m-d H:i:s');
                    $history->addWithemail(false);
                }
            }
        }
    }
    
    /**
     * Check if the Carriers are still active and/or deleted and update them
     *
     * @return boolean
     */
    public static function setCarrierState()
    {
        if ((int)Configuration::getGlobalValue('RELAISCOLIS_ID')) {
            $carrier = new Carrier((int)Configuration::getGlobalValue('RELAISCOLIS_ID'));
            $carrier->deleted = 0;
            if (!self::isFeatureActivated(self::TYPE_DELIVERY)) {
                $carrier->active = 0;
                $carrier->deleted = 1;
            }
            $carrier->save();
        }
        if ((int)Configuration::getGlobalValue('RELAISCOLIS_ID_MAX')) {
            $carrier_max = new Carrier((int)Configuration::getGlobalValue('RELAISCOLIS_ID_MAX'));
            $carrier_max->active = 0;
            $carrier_max->deleted = 1;
            if (self::isFeatureActivated(self::TYPE_MAX)) {
                $carrier_max->active = 1;
                $carrier_max->deleted = 0;

                // if relais max activated, we don't need relais anymore
                $carrier->active = 0;
                $carrier->deleted = 1;
                $carrier->save();
            }
            $carrier_max->save();
        }
        if ((int)Configuration::getGlobalValue('RELAISCOLIS_ID_HOME')) {
            $carrier = new Carrier((int)Configuration::getGlobalValue('RELAISCOLIS_ID_HOME'));
            if (!self::isFeatureActivated(self::TYPE_HOME)) {
                $carrier->active = 0;
            }
            $carrier->save();
        }
        if ((int)Configuration::getGlobalValue('RELAISCOLIS_ID_HOME_PLUS')) {
            $carrier = new Carrier((int)Configuration::getGlobalValue('RELAISCOLIS_ID_HOME_PLUS'));
            if (!self::isFeatureActivated(self::TYPE_HOME)) {
                $carrier->active = 0;
            }
            $carrier->save();
        }
        return true;
    }

    /**
     * Update an Order to be ready for being sent
     *
     * @param int $id_order
     * @return boolean
     */
    public static function processSending($id_order = 0)
    {
        if (!$id_order) {
            return false;
        }

        //load order data
        $order = new Order((int)$id_order);
        $customer = new Customer((int)$order->id_customer);
        $address = new Address((int)$order->id_address_delivery);

        //$address_shipping = new Address((int)$order->id_address_delivery);
        //Load existing Relais Colis Order
        $id_relais_colis_order = RelaisColisOrder::getRelaisColisOrderId((int)$order->id);
        if ($id_relais_colis_order !== false) {
            $relais_colis_order = new RelaisColisOrder((int)$id_relais_colis_order);
            if ((int)$relais_colis_order->order_weight) {
                $datas = array();
                $packages = RelaisColisOrderProduct::getPackagesByRcOrderIdDispatchByPackageNumber($relais_colis_order->id);
                if (!empty($packages)) {
                    foreach ($packages as $package) {
                        $datas[] = self::getPackagesData($order, $customer, $address, $relais_colis_order, $package, $package['package_number']);
                    }
                } else {
                    $datas[] = self::getPackagesData($order, $customer, $address, $relais_colis_order);
                }

                // Call Relais Colis WS
                $xml = self::rcWsXmlCall('package/placeAdvertisement', $datas);
                $pdf_numbers = (array)$xml->entry;
                if (empty($pdf_numbers)) {
                    Context::getContext()->controller->errors[] = 'Pas de numéro de réservation recu';
                    return false;
                }

                $parcel_tracking = true;
                foreach ($pdf_numbers as $package_number => $pdf_number) {
                    if ((string) $pdf_number) {
                        if ($parcel_tracking == true) {
                            //fill data
                            $relais_colis_order->pdf_number = (string) $pdf_number;
                            $tracking_number = (string) $pdf_number;
                            $relais_colis_order->is_send = true;
                        }

                        $relais_colis_pdf = new RelaisColisOrderPdf();
                        $relais_colis_pdf->pdf_number = (string) $pdf_number;
                        $relais_colis_pdf->package_number = (string) $datas[$package_number]['package_number'];
                        $relais_colis_pdf->id_relais_colis_order = $relais_colis_order->id;
                        $relais_colis_pdf->add();
                        $parcel_tracking = false;
                    } else {
                        Context::getContext()->controller->errors[] = 'Une erreur s\'est produite lors de l\'envoi.';
                        return false;
                    }
                }

                //save data
                if (!$relais_colis_order->save()) {
                    Context::getContext()->controller->errors[] = 'Numéro de réservation non mis à jour';
                    return false;
                }
                $id_order_carrier = $order->getIdOrderCarrier();
                $customer_name = str_replace(' ', '', $customer->lastname);
                $customer_name = Tools::strtoupper(Tools::substr($customer_name, 0, 4));
                if ((int)$id_order_carrier) {
                    $order_carrier = new OrderCarrier((int)$id_order_carrier);
                    $order_carrier->tracking_number = Configuration::get('RC_ENS_ID').'&nom_client='.$customer_name.'&typerech=EXP&valeur='.Tools::substr($tracking_number, 2, 10).'&style=RC';
                    $order_carrier->save();
                }

                $order->shipping_number = Configuration::get('RC_ENS_ID').'&nom_client='.$customer_name.'&typerech=EXP&valeur='.Tools::substr($tracking_number, 2, 10).'&style=RC';
                $order->save();
                Context::getContext()->controller->confirmations[] = 'Numéro de réservation recu';

                return true;
            }
            Context::getContext()->controller->errors[] =
                'Vous devez avoir un poids renseigné dans la commande :'.(int)$order->id;
        }
        return false;
    }

    public static function getPackagesData(Order $order, Customer $customer, Address $address, RelaisColisOrder $relais_colis_order, $package = array(), $package_number = null)
    {
        //prepare Data for Relais Colis WS
        $pseudo_rvc = '';
        $agency_code = '';
        $language_order = Language::getIsoById(Context::getContext()->language->id);
        if ($language_order != 'FR' && $language_order != 'NL') {
            $language_order = 'FR';
        }
        $iso_code = Country::getIsoById($address->id_country);
        //$iso_code_shipping = Country::getIsoById($address_shipping->id_country);
        $delivery_type = self::DELIVERY_TYPE_DEFAULT;
        $activity_code = self::ACTIVITY_CODE;
        $delivery_type_payment = self::DELIVERY_TYPE_PAYMENT_DEFAULT;

        $datas = array(
                // Required fields
                'activationKey' => Configuration::get('RC_ACTIVATION_KEY'),
                'orderReference' => $order->reference,
                'customerId' => $order->id_customer,
                //'customerFullname' => substr($customer->lastname.' '.$customer->firstname, 0, 32),
                'customerFullname' => $address->lastname.' '.$address->firstname,
                'customerEmail' => $customer->email,
                'shippingAddress1' => Tools::substr($address->address1, 0, 32),
                'shippingAddress2' => Tools::substr($address->address2, 0, 32),
                'shippingPostcode' => $address->postcode,
                'shippingCity' => $address->city,
                'shippmentWeight' => (float)$relais_colis_order->order_weight,
                'weight' => (isset($package['weight'])) ? (float) ($package['weight'] * 1000) : (float)$relais_colis_order->order_weight,
                'deliveryPaymentMethod' => $delivery_type_payment,
                'pseudoRvc' => $pseudo_rvc,
                //'xeett' => $xeett,
                'agencyCode' => $agency_code,
                'deliveryType' => $delivery_type,
                'activityCode' => $activity_code,
                'language' => $language_order,
                'shippingCountryCode' => $iso_code, // todo check implement
                'customerPhone' => $address->phone,
                'customerMobile' => $address->phone_mobile,
                //'insee' => $insee,
                'pickingSite' => self::PICKING_DEFAULT,
                'productFamily' => self::PRODUCT_FAMILY,
                'orderType' => self::ORDER_TYPE_DEFAULT,
                'orderTypeSub' => self::ORDER_TYPE_SUB_DEFAULT,
                'sensitiveProduct' => self::SENSITIVE_DEFAULT

        );
        if ((int)$relais_colis_order->id_relais_colis_info) {
            // Relais order
            $id_relais_colis_info = RelaisColisInfo::alreadyExists((int)$order->id_cart, (int)$order->id_customer);
            if ((int)$id_relais_colis_info) {
                $relais_colis_info = new RelaisColisInfo($id_relais_colis_info);
                $datas['customerFullname'] = Tools::substr($address->address1, 0, 32);
                $datas['shippingAddress1'] = Tools::substr($address->address2, 0, 32);
                $datas['shippingAddress2'] = '';
                $datas['pseudoRvc'] = $relais_colis_info->pseudo_rvc;
                $datas['xeett'] = $relais_colis_info->rel;
                $datas['agencyCode'] = $relais_colis_info->age_code;

                if ((int)$relais_colis_info->frc_max == 1) {
                    // Relais max
                    $datas['deliveryType'] = self::DELIVERY_TYPE_MAX;
                }
            }
        } else {
            // Home order
            //$datas['insee'] = self::INSEE_HOME;
            $datas['activityCode'] = self::ACTIVITY_CODE_HOME;

            // Home+ order
            if ((int)$id_relais_colis_info_home = RelaisColisInfoHome::getInfoHomeByIdOrder((int)$order->id)) {
                $relais_colis_info_home = new RelaisColisInfoHome($id_relais_colis_info_home);
                $datas['digicode'] = $relais_colis_info_home->digicode;
                $datas['floor'] = $relais_colis_info_home->floor_delivery;
                $datas['housingType'] = $relais_colis_info_home->type_home;
                $datas['lift'] = $relais_colis_info_home->elevator;
                $datas['urgent'] = $relais_colis_info_home->top;
                $datas['sensitiveProduct'] = $relais_colis_info_home->sensible;
                // options

                $datas['cpSchedule'] = (isset($package['schedule']) && $package['schedule'] == true) ? $package['schedule'] : $relais_colis_info_home->schedule;
                $datas['cpRetrieveOldEquipment'] = (isset($package['retrieve_old_equipment']) && $package['retrieve_old_equipment'] == true) ? $package['retrieve_old_equipment'] : $relais_colis_info_home->retrieve_old_equipment;
                $datas['cpDeliveryOnTheFloor'] = (isset($package['delivery_on_floor']) && $package['delivery_on_floor'] == true) ? $package['delivery_on_floor'] : $relais_colis_info_home->delivery_on_floor;
                $datas['cpDeliveryAtTwo'] = (isset($package['delivery_at_two']) && $package['delivery_at_two'] == true) ? $package['delivery_at_two'] : $relais_colis_info_home->delivery_at_two;
                $datas['cpTurnOnHomeAppliance'] = (isset($package['turn_on_home_appliance']) && $package['turn_on_home_appliance'] == true) ? $package['turn_on_home_appliance'] : $relais_colis_info_home->turn_on_home_appliance;
                $datas['cpMountFurniture'] = (isset($package['mount_furniture']) && $package['mount_furniture'] == true) ? $package['mount_furniture'] : $relais_colis_info_home->mount_furniture;
                $datas['cpNonStandard'] = (isset($package['non_standard']) && $package['non_standard'] == true) ? $package['non_standard'] : $relais_colis_info_home->non_standard;
                $datas['cpUnpacking'] = (isset($package['unpacking']) && $package['unpacking'] == true) ? $package['unpacking'] : $relais_colis_info_home->unpacking;
                $datas['cpEvacuationPackaging'] = (isset($package['evacuation_packaging']) && $package['evacuation_packaging'] == true) ? $package['evacuation_packaging'] : $relais_colis_info_home->evacuation_packaging;
                $datas['cpRecovery'] = (isset($package['recovery']) && $package['recovery'] == true) ? $package['recovery'] : $relais_colis_info_home->recovery;
                $datas['cpDeliveryDesiredRoom'] = (isset($package['delivery_desired_room']) && $package['delivery_desired_room'] == true) ? $package['delivery_desired_room'] : $relais_colis_info_home->delivery_desired_room;
                $datas['cpRecoverOldBedding'] = (isset($package['recover_old_bedding']) && $package['recover_old_bedding'] == true) ? $package['recover_old_bedding'] : $relais_colis_info_home->recover_old_bedding;
                $datas['cpDeliveryEco'] = (isset($package['delivery_eco']) && $package['delivery_eco'] == true) ? $package['delivery_eco'] : $relais_colis_info_home->delivery_eco;
                $datas['cpAssembly'] = (isset($package['assembly']) && $package['assembly'] == true) ? $package['assembly'] : $relais_colis_info_home->assembly;

                // recovery 3DE
                if ($relais_colis_info_home->recovery) {
                    $datas['orderType'] = 2;
                    $datas['orderTypeSub'] = 3;
                }
                // recovery 3DE
                if ($relais_colis_info_home->recover_old_bedding) {
                    $datas['orderType'] = 2;
                    $datas['orderTypeSub'] = 2;
                }

                $datas['homePlus'] = 1;
            }
        }
        $datas['package_number'] = 0;
        if ($package_number) {
            $datas['package_number'] = $package_number;
        }
        return $datas;
    }

    /**
     * Update an Order to be ready for being returned (Create or update an OrderReturn bound on the Order)
     *
     * @param int $id_order_return
     * @return bool
     */
    public static function processSendingReturn($id_order_return = 0)
    {
        if (!(int)$id_order_return) {
            return false;
        }
        //load order data
        $order_return = new OrderReturn((int)$id_order_return);
        $order = new Order((int)$order_return->id_order);
        $customer = new Customer((int)$order->id_customer);
        $address = new Address((int)$order->id_address_invoice);
        $country = Country::getNameById((int)Context::getContext()->language->id, (int)$address->id_country);
        $xeett = '';
        $xeett_name = '';
        //is order a relais colis ?
        $id_relais_colis_order = RelaisColisOrder::getRelaisColisOrderId((int)$order->id);
        if ($id_relais_colis_order !== false) {
            $relais_colis_order = new RelaisColisOrder((int)$id_relais_colis_order);
            $relais_colis_info = new RelaisColisInfo((int)$relais_colis_order->id_relais_colis_info);
            $xeett = $relais_colis_info->rel;
            $xeett_name = $relais_colis_info->rel_name;
        } else {
            Context::getContext()->controller->errors[] = 'Cette commande n\'est pas une commande relais colis';
            return false;
        }
        // max 15 days to return with Relais Colis
        $date_day = date('Y-m-d');
        $date_order = date($order->date_add);
        $date_order_max = date('Y-m-d', strtotime($date_order.' +15 days'));

        //$date1 = strtotime($date_day);
        //$date2 = strtotime($date_order_max);

        if ($date_day > $date_order_max) {
            Context::getContext()->controller->errors[] =
                'Cette commande a plus de 15 jours, elle ne peut plus être retournée via relais Colis';
            return false;
        }
        if ($order_return->id) {
            // Call Relais COlis WS
            $xml = self::rcWsXmlCall(
                'return/placeReturn',
                array(
                    'activationKey' => Configuration::get('RC_ACTIVATION_KEY'),
                    'requests' => array(
                        array(
                            'orderId' => $order->id,
                            'customerId' => $order->id_customer,
                            'customerFullname' => $customer->lastname.' '.$customer->firstname,
                            'xeett' => $xeett,
                            'xeettName' => $xeett_name,
                            'customerPhone' => $address->phone,
                            'customerMobile' => $address->phone_mobile,
                            'reference' => $order->reference,
                            'customerCompany' => $address->company,
                            'customerAddress1' => $address->address1,
                            'customerAddress2' => $address->address2,
                            'customerPostcode' => $address->postcode,
                            'customerCity' => $address->city,
                            'customerCountry' => $country,
                        ),
                    ),
                )
            );
            if ((string)$xml->entry->response_status != 'Available') {
                Context::getContext()->controller->errors[] = 'No return number received';
                return false;
            } else {
                if (!$pdf_number = (string)$xml->entry->return_number) {
                    Context::getContext()->controller->errors[] = 'No return number received';
                    return false;
                }
                if (!$token = (string)$xml->entry->token) {
                    Context::getContext()->controller->errors[] = 'No token received';
                    return false;
                }
            }
            //Load existing Relais Colis Return
            if ($id_relais_colis_return = RelaisColisReturn::getRelaisColisReturnId((int)$id_order_return)) {
                $relais_colis_return = new RelaisColisReturn((int)$id_relais_colis_return);
            } else {
                $relais_colis_return = new RelaisColisReturn();
                $relais_colis_return->id_order_return = (int)$id_order_return;
                $relais_colis_return->id_order = (int)$order->id;
                $relais_colis_return->id_customer = (int)$order->id_customer;
            }
            //fill data
            $relais_colis_return->pdf_number = $pdf_number;
            $relais_colis_return->token = $token;
            $relais_colis_return->is_send = true;

            //save data
            if (!$relais_colis_return->save()) {
                Context::getContext()->controller->errors[] = 'Return number not updated';
                return false;
            }
            Context::getContext()->controller->confirmations[] = 'Return number updated';
            return true;
        }
        return false;
    }

    public static function processTestReturn()
    {
        self::rcWsXmlCall(
            'return/testReturn',
            array(
                'activationKey' => Configuration::get('RC_ACTIVATION_KEY'),
                'requests' =>
                array(
                    array(
                        'test' => 'test',
                    ),
                ),
            )
        );
    }

    // Not used anymore ??
    /*public function getIdOrderCarrier($id_order)
    {
        return Db::getInstance()->getValue('SELECT id_order_carrier FROM '._DB_PREFIX_.'relaiscolis_info WHERE id_cart = '.(int)$id_cart.' AND id_customer ='.(int)$id_customer);
    }*/

    /**
     * Check if all the features in the list are activated
     *
     * @param type $feature_list
     * @return boolean
     */
    public static function isFeatureActivated($feature_list)
    {
        if (!is_array($feature_list)) {
            $feature_list = array(
                $feature_list);
        }
        // Get only trigramme
        $keys = explode(';', Configuration::get('RC_OPTIONS'));
        array_pop($keys);
        $found = true;
        foreach ($feature_list as $feature) {
            if (!in_array($feature, $keys)) {
                $found = false;
            }
        }

        return $found;
    }

    /**
     * Transform an XML Object into an array
     *
     * @param type $xmlObject
     * @param array $out
     * @return type
     */
    public static function xml2array($xmlObject, $out = array())
    {
        foreach ((array)$xmlObject as $index => $node) {
            $out[$index] = ( is_object($node) ) ? xml2array($node) : $node;
        }

        return $out;
    }
    
    /**
     * Get the Relais Colis Order's ID with the EXTRACTED Number
     *
     * @param String $extract_number
     * @return mixed Relais Colis Order's ID or false if there is no $extract_number
     */
    public static function getRelaisColisOrderIdWithExtractNumber($extract_number)
    {
        if ($extract_number) {
            return Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'relaiscolis_order WHERE pdf_number LIKE "%'.pSQL($extract_number).'%"');
        }
        return false;
    }

    /**
     * Get the Relais Colis Order's ID with the PDF Number
     *
     * @param String $pdf_number
     * @return mixed Relais Colis Order's ID or false if there is no $pdf_number
     */
    public static function getRelaisColisOrderId($pdf_number)
    {
        if ($pdf_number) {
            return Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'relaiscolis_order WHERE pdf_number = "'.pSQL($pdf_number).'"');
        }
        return false;
    }

    /**
     * Get the ReturnOrder's ID with the PDF Number
     *
     * @param String $pdf_number
     * @return mixed ReturnOrder's ID or false if there is no $pdf_number
     */
    public static function getReturnOrderId($pdf_number)
    {
        if ($pdf_number) {
            return Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'relaiscolis_return WHERE pdf_number = "'.pSQL($pdf_number).'"');
        }
        return false;
    }

    /**
     * Get the status's ID of an Order's event with the event's code and justification's code
     *
     * @param type $evts_code the event's code
     * @param type $evts_justif the justification's code
     * @return mixed status's ID or false if there is no event's code and/or no justification's code
     */
    public static function getIdstate($evts_code, $evts_justif)
    {
        if ($evts_code && $evts_justif) {
            return Db::getInstance()->getValue('SELECT id_status FROM '._DB_PREFIX_.'relaiscolis_evts WHERE code_evenement = "'.pSQL($evts_code).'" and code_justificatif = "'.pSQL($evts_justif).'"');
        }
        return false;
    }
}
