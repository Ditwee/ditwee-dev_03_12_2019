<?php
/**
 * 1969-2018 Relais Colis
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@relaiscolis.com so we can send you a copy immediately.
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1969-2018 Relais Colis
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class RelaisColisInfo extends ObjectModel
{
    
    public $id_relais_colis_info;
    public $id_cart;
    public $id_customer;
    public $rel = '';
    public $rel_name = '';
    public $rel_adr = '';
    public $rel_cp = '';
    public $rel_vil = '';
    public $pseudo_rvc = '';
    public $frc_max = '';
    public $floc_rel = '';
    public $fcod_pays = '';
    public $type_liv = '';
    public $age_code = '';
    public $age_nom = '';
    public $age_adr = '';
    public $age_vil = '';
    public $age_cp = '';
    public $ouvlun = '';
    public $ouvmar = '';
    public $ouvmer = '';
    public $ouvjeu = '';
    public $ouvven = '';
    public $ouvsam = '';
    public $ouvdim = '';
    public $selected_date;
    public $smart;
    public $sending_date;
    
    public static $definition = array(
        'table' => 'relaiscolis_info',
        'primary' => 'id_relais_colis_info',
        'multilang' => false,
        'fields' => array(
            'id_relais_colis_info' => array(
                'type' => ObjectModel::TYPE_INT
            ),
            'id_cart' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => true
            ),
            'id_customer' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => true
            ),
            'rel' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'rel_name' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'rel_adr' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'rel_cp' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'rel_vil' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'pseudo_rvc' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'frc_max' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'floc_rel' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'fcod_pays' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'type_liv' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'age_code' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'age_nom' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'age_adr' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'age_vil' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'age_cp' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'ouvlun' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'ouvmar' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'ouvmer' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'ouvjeu' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'ouvven' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'ouvsam' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'ouvdim' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'selected_date' => array(
                'type' => ObjectModel::TYPE_DATE,
                'required' => true
            ),
            'smart' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'sending_date' => array(
                'type' => ObjectModel::TYPE_DATE,
                'required' => false
            )
        )
    );

    /**
     * Check if a Relais Colis Info already exists for a Cart and Customer IDs tuple
     *
     * @param int $id_cart
     * @param int $id_customer
     * @return mixed Relais Colis Info's ID or false if not exists
     */
    public static function alreadyExists($id_cart = 0, $id_customer = 0)
    {
        if (!$id_cart || !$id_customer) {
            return false;
        }

        return Db::getInstance()->getValue('SELECT id_relais_colis_info FROM '._DB_PREFIX_.'relaiscolis_info WHERE id_cart = '.(int)$id_cart.' AND id_customer ='.(int)$id_customer);
    }

    /**
     * Get the last Relais Colis selected by a Customer for a specific Cart.
     * If there is no id_cart or id_customer, it takes them from context.
     *
     * @param int $id_cart
     * @param int $id_customer
     * @return mixed Relais Colis Info's ID or false if not exists
     */
    public static function lastSelectedPoint($id_cart = 0, $id_customer = 0)
    {
        if (!$id_cart) {
            $id_cart = Context::getContext()->context->cart->id;
        }
        
        if (!$id_customer) {
            $id_customer = Context::getContext()->context->customer->id;
        }
        
        $date_day = date('Y-m-d');
        
        return Db::getInstance()->getValue('SELECT id_relais_colis_info FROM '._DB_PREFIX_.'relaiscolis_info WHERE id_cart <> '.(int)$id_cart.' AND id_customer ='.(int)$id_customer.' AND selected_date >="'.pSQL($date_day).' 00:00:00" ORDER BY id_cart DESC');
    }
    /**
     * Get the total product quantity
     *
     * @param int id_order
     * @return mixed Total product quantity or false if not exists
     */
    public static function getTotalProductsForOrderId($id_order)
    {
        if (!$id_order) {
            return false;
        }

        return Db::getInstance()->getValue('SELECT SUM(product_quantity) FROM '._DB_PREFIX_.'order_detail WHERE id_order = '.pSQL((int)$id_order));
    }
}
