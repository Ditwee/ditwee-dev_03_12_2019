<?php
/**
 * 1969-2018 Relais Colis
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@relaiscolis.com so we can send you a copy immediately.
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1969-2018 Relais Colis
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class RelaisColisInfoHome extends ObjectModel
{
    
    public $id_relais_colis_info_home;
    public $id_cart;
    public $id_customer;
    public $id_order;
    public $schedule;
    public $retrieve_old_equipment;
    public $delivery_on_floor;
    public $delivery_at_two;
    public $turn_on_home_appliance;
    public $mount_furniture;
    public $non_standard;
    public $unpacking;
    public $evacuation_packaging;
    public $recovery;
    public $delivery_desired_room;
    public $recover_old_bedding;
    public $delivery_eco;
    public $assembly;
    public $top;
    public $sensible;
    public $digicode;
    public $floor_delivery;
    public $type_home;
    public $elevator;
    public static $definition = array(
        'table' => 'relaiscolis_infohome',
        'primary' => 'id_relais_colis_info_home',
        'multilang' => false,
        'fields' => array(
            'id_relais_colis_info_home' => array(
                'type' => ObjectModel::TYPE_INT
            ),
            'id_cart' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => true
            ),
            'id_customer' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => true
            ),
            'id_order' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => false
            ),
            'schedule' => array(
                'type' => ObjectModel::TYPE_BOOL,
            ),
            'retrieve_old_equipment' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'delivery_on_floor' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'delivery_at_two' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'turn_on_home_appliance' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'mount_furniture' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'non_standard' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'unpacking' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'evacuation_packaging' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'recovery' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'delivery_desired_room' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'recover_old_bedding' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'delivery_eco' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'assembly' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'top' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'sensible' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'digicode' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'floor_delivery' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => false
            ),
            'type_home' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => false
            ),
            'elevator' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            )
        )
    );

    /**
     * Check if a Relais Colis Info Home already exists for a Cart and Customer IDs tuple
     *
     * @param int $id_cart
     * @param int $id_customer
     * @return mixed Relais Colis Info's ID or false if not exists
     */
    public static function alreadyExists($id_cart = 0, $id_customer = 0)
    {
        if (!$id_cart || !$id_customer) {
            return false;
        }

        return Db::getInstance()->getValue('SELECT id_relais_colis_info_home FROM '._DB_PREFIX_.'relaiscolis_infohome WHERE id_cart = '.(int)$id_cart.' AND id_customer ='.(int)$id_customer);
    }
    /**
     * Get the last Relais Colis Home selected by a Customer for a specific Cart.
     * If there is no id_cart or id_customer, it takes them from context.
     *
     * @param int $id_cart
     * @param int $id_customer
     * @return mixed Relais Colis Info's ID or false if not exists
     */
    public static function getInfoHomeByIdOrder($id_order = 0)
    {
        if (!(int)$id_order) {
            return false;
        }

        return Db::getInstance()->getValue('SELECT id_relais_colis_info_home FROM '._DB_PREFIX_.'relaiscolis_infohome WHERE id_order = '.(int)$id_order);
    }
}
