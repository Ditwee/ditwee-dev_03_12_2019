<?php
/**
 * 1969-2018 Relais Colis
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@relaiscolis.com so we can send you a copy immediately.
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1969-2018 Relais Colis
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class RelaisColisOrderPdf extends ObjectModel
{
    
    public $id_relais_colis_order_pdf;
    public $id_relais_colis_order;
    public $package_number;
    public $pdf_number;
    public static $definition = array(
        'table' => 'relaiscolis_order_pdf',
        'primary' => 'id_relais_colis_order_pdf',
        'multilang' => false,
        'fields' => array(
            'id_relais_colis_order_pdf' => array(
                'type' => ObjectModel::TYPE_INT
            ),
            'id_relais_colis_order' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => true
            ),
            'package_number' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => true
            ),
            'pdf_number' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => true
            )
        )
    );

    public static function getPdfsNumber($id_relais_colis_order)
    {
        if ($id_relais_colis_order) {
            $sql = '
                SELECT rop.*, ROUND(SUM(ropr.`weight`), 2) as weight
                FROM `'._DB_PREFIX_.'relaiscolis_order_pdf` rop
                LEFT JOIN `'._DB_PREFIX_.'relaiscolis_order_product` ropr ON (ropr.`package_number` = rop.`package_number` AND ropr.`id_relais_colis_order` = rop.`id_relais_colis_order`)
                WHERE rop.`id_relais_colis_order` = '.pSQL((int) $id_relais_colis_order).'
                GROUP BY rop.`package_number`
            ';
            $results = Db::getInstance()->executeS($sql);

            return $results;
        }

        return array();
    }
    
    public static function getAllPdfFromNumber($pdf)
    {
        $sql = '
            SELECT `id_relais_colis_order`
            FROM `'._DB_PREFIX_.'relaiscolis_order_pdf`
            WHERE `pdf_number` = "'.pSQL($pdf).'"
        ';
        $id_relais_colis_order = Db::getInstance()->getValue($sql);

        if ($id_relais_colis_order) {
            $sql = '
                SELECT pdf_number
                FROM `'._DB_PREFIX_.'relaiscolis_order_pdf`
                WHERE `id_relais_colis_order` = '.pSQL((int) $id_relais_colis_order).'
            ';

            return Db::getInstance()->executeS($sql);
        }
        return false;
    }
}
