<?php
/**
 * 1969-2018 Relais Colis
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@relaiscolis.com so we can send you a copy immediately.
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1969-2018 Relais Colis
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class RelaisColisReturn extends ObjectModel
{
    
    public $id_relais_colis_return;
    public $id_order_return;
    public $id_order;
    public $id_customer;
    public $is_send = false;
    public $pdf_number;
    public $token;
    public static $definition = array(
        'table' => 'relaiscolis_return',
        'primary' => 'id_relais_colis_return',
        'multilang' => false,
        'fields' => array(
            'id_relais_colis_return' => array(
                'type' => ObjectModel::TYPE_INT
            ),
            'id_order_return' => array(
                'type' => ObjectModel::TYPE_INT
            ),
            'id_order' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => true
            ),
            'id_customer' => array(
                'type' => ObjectModel::TYPE_INT,
                'required' => true
            ),
            'is_send' => array(
                'type' => ObjectModel::TYPE_BOOL,
                'required' => false
            ),
            'pdf_number' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            ),
            'token' => array(
                'type' => ObjectModel::TYPE_STRING,
                'required' => false
            )
        )
    );

    /**
     * Get the PDF number with an OrderReturn's ID
     *
     * @param int $id_order_return
     * @return String
     */
    public static function getPdfNumber($id_order_return)
    {
        return Db::getInstance()->getValue('SELECT pdf_number FROM '._DB_PREFIX_.'relaiscolis_return WHERE id_order_return = '.(int)$id_order_return);
    }

    /**
     * Get the token with an OrderReturn's ID
     *
     * @param int $id_order_return
     * @return String
     */
    public static function getTokenNumber($id_order_return)
    {
        return Db::getInstance()->getValue('SELECT token FROM '._DB_PREFIX_.'relaiscolis_return WHERE id_order_return = '.(int)$id_order_return);
    }

    /**
     * Get the PDF number with a Relais Colis Return's ID
     *
     * @param int $id_order_return
     * @return String
     */
    public static function getPdfNumberByIdRelais($id_relais_colis_return)
    {
        return Db::getInstance()->getValue('SELECT pdf_number FROM '._DB_PREFIX_.'relaiscolis_return WHERE id_relais_colis_return = '.(int)$id_relais_colis_return);
    }

    /**
     * Get the Relais Colis Return's ID with the OrderReturn's ID
     *
     * @param int $id_order_return
     * @return int
     */
    public static function getRelaisColisReturnId($id_order_return)
    {
        if ((int)$id_order_return) {
            return Db::getInstance()->getValue('SELECT id_relais_colis_return FROM '._DB_PREFIX_.'relaiscolis_return WHERE id_order_return = '.(int)$id_order_return);
        }
        return false;
    }
    /**
     * Get the Relais Colis Return with the Customer's ID
     *
     * @param int $id_customer
     * @return array
     */
    public static function getRelaisColisReturnByCustomer($id_customer)
    {
        if ((int)$id_customer) {
            return Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'relaiscolis_return rr
                LEFT JOIN '._DB_PREFIX_.'order_return oret ON oret.id_order_return = rr.id_order_return
                WHERE rr.id_customer = '.(int)$id_customer);
        }
        return false;
    }
}
