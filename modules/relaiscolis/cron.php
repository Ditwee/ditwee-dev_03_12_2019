<?php
/**
 * 1969-2018 Relais Colis
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@relaiscolis.com so we can send you a copy immediately.
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1969-2018 Relais Colis
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

parse_str('id_shop=1', $args);

$_GET = array_merge($args, $_GET);
$_SERVER['QUERY_STRING'] = 'id_shop=1';

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/relaiscolis.php');
$context = Context::getContext();
$shop = $context->shop;

if ((Tools::getValue('token') != Configuration::get('RC_CRON_TOKEN')) || !Configuration::get('RC_CRON_TOKEN')) {
    die('Invalid Token');
}

if (!isset($_SERVER['HTTP_HOST']) || empty($_SERVER['HTTP_HOST'])) {
    $_SERVER['HTTP_HOST'] = $shop->domain;
}
if (!isset($_SERVER['SERVER_NAME']) || empty($_SERVER['SERVER_NAME'])) {
    $_SERVER['SERVER_NAME'] = $shop->domain;
}
if (!isset($_SERVER['REMOTE_ADDR']) || empty($_SERVER['REMOTE_ADDR'])) {
    $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
}



/*
 * BATCH D'IMPORT / EXPORT
 */

// include class
require_once('classes/RelaisColisApi.php');
if (Configuration::get('RC_CRON_ACTIVE')) {
    if (Shop::isFeatureActive()) {
        $shop_list = Shop::getShops();
        if (is_array($shop_list)) {
            foreach ($shop_list as $shop) {
                $activation_key = Configuration::get('RC_ACTIVATION_KEY', null, null, (int)$shop['id_shop']);
                if ($activation_key) {
                    RelaisColisApi::processGetEvtsData($activation_key);
                }
            }
        } else {
            RelaisColisApi::processGetEvtsData(Configuration::get('RC_ACTIVATION_KEY'));
        }
    } else {
        RelaisColisApi::processGetEvtsData(Configuration::get('RC_ACTIVATION_KEY'));
    }
}
