/**
 * 1969-2018 Relais Colis
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@relaiscolis.com so we can send you a copy immediately.
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1969-2018 Relais Colis
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
$(document).ready(function (){
    $('#submitPdfLabel').click(function(){
        $('#rc_form_pdf').submit();
    });
    $('#submitSendingLabel').click(function(){
        $(this).hide();
        $('.waiting_relais').show(1000);
    });
    $('#submitRegularOrder').click(function() {
        if(confirm("Attention !\n\nCeci va supprimer les enregistrements relatifs à RelaisColis en base de données pour cette commande.\n\nCette action est irréversible.")) {
            $('#correct_rc_address').submit();
        }
    });
    
    var tracking_link = $('#tracking_link_rc').val();
    if (tracking_link !== null && tracking_link !== 'undefined') {
        $('.shipping_number_show a').html(tracking_link);
        $('#shipping_number_show a').html(tracking_link);
    }
    
    if(typeof(c2c_activated) !== 'undefined' ) {
        if(c2c_activated === true && $('#shipping_table .shipping_number_show').size()) {
           $('#shipping_table .shipping_number_show').html('Le tracking s\'effectue sur le site RelaisColis');
        }
    }
});
