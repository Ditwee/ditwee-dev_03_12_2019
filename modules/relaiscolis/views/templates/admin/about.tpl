{*
* 1969-2018 Relais Colis
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to contact@relaiscolis.com so we can send you a copy immediately.
*
*  @author    Quadra Informatique <modules@quadra-informatique.fr>
*  @copyright 1969-2018 Relais Colis
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<div class="relais-about">
    <h2>{l s='Relais Colis Offer' mod='relaiscolis'}</h2>

    <h3>{l s='The principles' mod='relaiscolis'}</h3>
    <p><b>{l s='Relais colis philosophy is :' mod='relaiscolis'}</b>
    </p>
    <ul>
        <li>{l s='Speed ​​of delivery accessible to all' mod='relaiscolis'}

        </li><li>{l s='Customer satisfaction and flexibility as the engine of our everyday life' mod='relaiscolis'}

        </li><li>{l s='Innovations to adapt to the needs of tomorrow' mod='relaiscolis'}

        </li>
    </ul>

    <h3>{l s='Know-how and ambitions' mod='relaiscolis'}</h3>

    <p><b> {l s='The proximity asset:' mod='relaiscolis'}</b>{l s='Thanks to an increasingly dense terrain coverage, proximity is one of the major assets of Relais Colis. Nearly 83% of the population is less than 10 minutes from a relay point and 100% within 5 minutes in major cities. The presence of our relays in the most important crossing points guarantees to 60% of the population to pass in front of a relay point every day.' mod='relaiscolis'}
    </p>
    <p><b>{l s='The diversity asset:' mod='relaiscolis'}</b> {l s='Thanks to diversified relay points (Telephony, mass distribution, crafts ...) and innovative partnerships (Bluedistrib instructions, Packcity ...), access to the package is simplified (wide opening hours) and more user-friendly.' mod='relaiscolis'}
    </p>
    <p><b>{l s='Customer satisfaction:' mod='relaiscolis'}</b> {l s='For both our partner brands and our end customers, Relais Colis places customer satisfaction at the heart of its process. Thus, the chains have a web-service, a dedicated account manager, a sales representative and traceability tools to work in perfect synergy with our teams. And the result is reflected on our end customers: 97% of customer satisfaction and 73% of notoriety.' mod='relaiscolis'}
    </p>
    <p><b>{l s='Ambitions :' mod='relaiscolis'}</b> {l s='Faced with the growth of the e-commerce market, Relais Colis has ambitions that live up to this evolution. With the entry of DHL into the capital, a vast transformation plan has been realized since the beginning of 2016: new mechanized hubs, new service offerings, a new website and a new logo are the key elements of success. the new Relais Colis.' mod='relaiscolis'}
    </p>

    <h3>{l s='Offers' mod='relaiscolis'}</h3>

    <p>{l s='Relais Colis adapts to the reality of the e-commerce market by offering offers that meet all customer needs:' mod='relaiscolis'}


        {l s='Professional offer available for e-merchants shipping more than 10 packages / day:' mod='relaiscolis'}
    </p>
    <ul>
        <li>
            {l s='THE STANDARD: Delivery on d + 1 / d + 2 in the 5100 relay points in France and Belgium for packages of less than 20 kilograms and less than 1m70 of developed (l + L + h)' mod='relaiscolis'}
        </li><li>{l s='EXPRESS: Delivery in day + 1 in the 5000 relay points in France and Belgium for packages of less than 20 kilograms and less than 1m70 of developed (l + L + h)' mod='relaiscolis'}

        </li><li>{l s='THE MAX: Delivery day d + 1 / d + 2 in 850 relay points for packages of 20 to 40 kilograms and between 1m70 and 2m50 of developed (l + L + h)' mod='relaiscolis'}

        </li><li>{l s='THE DRIVE: Recovery of your parcels in our 22 agencies on d + 1 / d + 2 for packages of 40 to 70 kilos and more than 2m50 of developed (l + L + h)' mod='relaiscolis'}

        </li><li>{l s='HOME: Delivery at the foot of your door for parcels over 40 kilos and more than 2m50 of developed (l + L + h)' mod='relaiscolis'}

        </li><li>{l s='HOME +: Delivery in the room of your choice with installation for parcels over 40 kilos and more than 2m50 of developed (l + L + h)' mod='relaiscolis'}

        </li>
    </ul>
    <p>{l s='Offer particular to individual and small pro sending less than 10 parcels / day:' mod='relaiscolis'} <br/>



        {l s='C2C: Sending relay relay packages, from individuals to individuals and for small pro on' mod='relaiscolis'} <a href="http://www.relaiscolis.com/" >{l s='www.relaiscolis.com' mod='relaiscolis'}</a> {l s='via our network of 5100 relay points for parcels less than 20 kilos and less than 1m70 of developed (l + L + h).' mod='relaiscolis'}

        {l s='OPTION SMART innovation now allows you to stop printing your label when sending with a QR code.' mod='relaiscolis'} </p>


</div>
