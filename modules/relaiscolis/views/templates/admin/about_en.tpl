{*
* 1969-2017 Relais Colis
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to contact@relaiscolis.com so we can send you a copy immediately.
*
*  @author    Quadra Informatique <modules@quadra-informatique.fr>
*  @copyright 1969-2017 Relais Colis
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
<div class="relais-about">
    <h2>L’offre Relais Colis</h2>

    <h3>Les valeurs</h3>
    <p><b>La philosophie Relais Colis c’est :</b>
    </p>
    <ul>
        <li>
            La rapidité de livraison accessible à tous
        </li><li>
            La satisfaction client et la flexibilité comme moteur de notre quotidien 
        </li><li>
            Des innovations pour s’adapter aux besoins de demain
        </li>
    </ul> 

    <h3>Savoir-faire et ambitions</h3>

    <p><b> L’atout proximité :</b> Grâce à une couverture du terrain de plus en plus dense, la proximité est l’un des atouts majeurs de Relais Colis. Près de 83% de la population se trouve à moins de 10 minutes d’un point Relais et 100% à moins de 5 minutes dans les grandes villes. La présence de nos relais dans les points de passages les plus importants garantie à 60% de la population de passer devant un point Relais chaque jour. 
    </p>
    <p><b>L’atout diversité :</b> Grâce à des points relais diversifiés(Téléphonie, grande distribution, artisanat…) et des partenariats innovants (consigne bluedistrib, Packcity…), l’accés au colis est simplifié (large horaire d’ouverture) et plus convivial.
    </p>
    <p><b>La satisfaction client :</b> Aussi bien pour nos enseignes partenaires que pour nos clients finaux, Relais Colis place la satisfaction client au cœur de son processus. Ainsi, les enseignes disposent d’un web-service, d’un chargé de clientèle dédié, d’un commercial et d’outils de traçabilité afin de travailler en parfaite synergie avec nos équipes. Et le résultat se traduit sur nos clients finaux : 97% de satisfactions clients et 73% de notoriété.
    </p>
    <p><b>Les ambitions :</b> Face à la croissance du marché du e-commerce, Relais Colis affiche des ambitions à la hauteur de cette évolution. Avec l’entrée de DHL au capital, un vaste plan de transformation se concrétise depuis le début de l’année 2016 :de nouveaux hubs mécanisés, de nouvelles offres de service, un nouveau site internet et un nouveau logo sont les éléments clés de succès du nouveau Relais Colis.
    </p>

    <h3>Les offres</h3>

    <p>Relais Colis s’ adapte à la réalité du marché du e-commerce en proposant des offres qui répondent à l’ensemble des besoins clients :



        Offre professionnelle disponible pour les e-marchands expédiant plus de 10 colis/jour :</p>
    <ul>
        <li>
            LE STANDARD : Livraison en j+1/j+2 dans les 5100 points relais en France et en Belgique pour des colis de moins de 20 kilogrammes et de moins 1m70 de développé(l+L+h)
        </li><li>
            L’EXPRESS :  Livraison en j+1 dans les 5000 points relais en France et en Belgique pour des colis de moins de 20 kilogrammes et de moins 1m70 de développé(l+L+h)
        </li><li>
            LE MAX : Livraison en j+1/j+2 dans 850 points relais pour des colis de 20 à 40 kilogrammes  et entre 1m70 et 2m50 de développé(l+L+h)
        </li><li>
            LE DRIVE : Récupération de vos colis dans nos 22 agences en j+1/j+2 pour des colis de 40 à 70 kilos et de plus de 2m50 de développé(l+L+h)
        </li><li>
            HOME : Livraison au pied de votre porte pour des colis de plus de 40 kilos et de plus de 2m50 de développé(l+L+h)
        </li><li>
            HOME+ : Livraison dans la pièce de votre choix avec installation pour des colis de plus de 40 kilos et de plus de 2m50 de développé(l+L+h)
        </li>
    </ul>
    <p>Offre particulier à particulier et petit pro expédiant moins de 10 colis/jour : <br/>



        C2C : Envoi de colis de relais à relais, de particuliers à particuliers et pour les petits pro sur <a href="http://www.relaiscolis.com/" >www.relaiscolis.com</a> via notre réseau de 5100 points relais pour les colis de moins 20 kilos et de moins de 1m70 de développé(l+L+h).

        L’innovation OPTION SMART permet désormais de ne plus imprimer son étiquette lors de l’envoi en disposant d’un QR code. </p>


</div>
