{*
* 1969-2018 Relais Colis
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to contact@relaiscolis.com so we can send you a copy immediately.
*
*  @author    Quadra Informatique <modules@quadra-informatique.fr>
*  @copyright 1969-2018 Relais Colis
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}
{addJsDef is_selected=$is_selected|boolval}
{addJsDef is_selected_home=$is_selected_home|boolval}
{addJsDef is_selected_home_plus=$is_selected_home_plus|boolval}
{addJsDef have_selected_point=$have_selected_point|boolval}
{addJsDef have_selected_last=$have_selected_last|boolval}
{addJsDef url_img=$module_dir}
{addJsDef key_rc_build=$key_build}
{addJsDef relaisColisKey=$relais_colis_key}
{addJsDef msg_order_carrier_relais=$msg_order_carrier_relais}
{addJsDef redirect_link_rc=$redirect_link}
{addJsDef relais_carrier_id=$relais_carrier_id}
{addJsDef home_carrier_id=$home_carrier_id}
{addJsDef relais_carrier_max_id=$relais_carrier_max_id}
{addJsDef max_unit_weight=$max_unit_weight}
{addJsDef is_relais_max=$is_relais_max}
{addJsDef must_unselected=$must_unselected}
{addJsDef have_selected_point=$have_selected_point}
{addJsDef msg_order_carrier_relais=$msg_order_carrier_relais}
{addJsDef have_selected_last=$have_selected_last}
{addJsDef RELAISCOLIS_ID=$RELAISCOLIS_ID}
{addJsDef RELAISCOLIS_ID_MAX=$RELAISCOLIS_ID_MAX}
{addJsDef baseUrl=$baseUrl}
{addJsDef rc_token=$rc_token}
<div id="frame_relais"> 
    {if $have_selected_point || $have_selected_last}
        <div class="col-sm-12 col-xs-12 clearfix choice-info">
            <div class="col-sm-6 col-xs-12">
                <span class="relay-info-intro">
                    {if $have_selected_point}
                        {l s='You have selected that delivery point :' mod='relaiscolis'}
                    {/if}
                    {if $have_selected_last}
                        {l s='In previous order you have selected that delivery point :' mod='relaiscolis'}
                    {/if}
                </span>
                <br/>
                <span class="relay-info-title">{if isset($relay_info.name)}{$relay_info.name|escape:'htmlall':'UTF-8'}{/if}</span>
                <br/>
                <span>{if isset($relay_info.street)}{$relay_info.street|escape:'htmlall':'UTF-8'}{/if}</span>
                <br/>
                <span>{if isset($relay_info.postcode)}{$relay_info.postcode|escape:'htmlall':'UTF-8'}{/if} {if isset($relay_info.city)}{$relay_info.city|escape:'htmlall':'UTF-8'}{/if}</span>
                <br/>
                {if $have_selected_last}
                    <a href="{$redirect_link_last_point|escape:'htmlall':'UTF-8'}" class="red-link button-relais" id="set-last-relay">{l s='Choose that point' mod='relaiscolis'}</a>
                {/if}
                {if $have_selected_point || $have_selected_last}
                    <button class="red-link button-relais" id="set-another-relay" type="button">{l s='Find another point' mod='relaiscolis'}</button>
                {/if}
            </div>
            <div class="col-sm-6 col-xs-12">
                <span class="relay-info-intro">{l s='Opening Hours :' mod='relaiscolis'}</span>
                <br/>
                <table>
                    <tr>
                        <td>{l s='Monday : ' mod='relaiscolis'}</td><td>{if isset($relay_info.ouvlun)}{$relay_info.ouvlun|escape:'htmlall':'UTF-8'}{/if}</td>
                    </tr>
                    <tr>
                        <td>{l s='thuesday : ' mod='relaiscolis'}</td><td>{if isset($relay_info.ouvmar)}{$relay_info.ouvmar|escape:'htmlall':'UTF-8'}{/if}</td>
                    </tr>
                    <tr>
                        <td>{l s='Wednesday : ' mod='relaiscolis'}</td><td>{if isset($relay_info.ouvmer)}{$relay_info.ouvmer|escape:'htmlall':'UTF-8'}{/if}</td>
                    </tr>
                    <tr>
                        <td>{l s='Thursday : ' mod='relaiscolis'}</td><td>{if isset($relay_info.ouvjeu)}{$relay_info.ouvjeu|escape:'htmlall':'UTF-8'}{/if}</td>
                    </tr>
                    <tr>
                        <td>{l s='Friday : ' mod='relaiscolis'}</td><td>{if isset($relay_info.ouvven)}{$relay_info.ouvven|escape:'htmlall':'UTF-8'}{/if}</td>
                    </tr>
                    <tr>
                        <td>{l s='Saturday : ' mod='relaiscolis'}</td><td>{if isset($relay_info.ouvsam)}{$relay_info.ouvsam|escape:'htmlall':'UTF-8'}{/if}</td>
                    </tr>
                    <tr>
                        <td>{l s='Sunday : ' mod='relaiscolis'}</td><td>{if isset($relay_info.ouvdim)}{$relay_info.ouvdim|escape:'htmlall':'UTF-8'}{/if}</td>
                    </tr>
                </table>
            </div>
        </div>
    {/if}


    <div class="col-sm-12 col-xs-12 clearfix" id="divGlobal">
        <div class="relais-title txt-center">
            <span>{l s='Find a delivery point everywhere in France' mod='relaiscolis'}</span>
        </div>
        <div class="col-sm-12 col-xs-12 clearfix">
            <div class="col-md-6 col-xs-6">
                <label class="grey" for="adresse">{l s='Address :' mod='relaiscolis'}</label>
                <input class="small" value="{$street|escape:'htmlall':'UTF-8'}" name="form_address" id="form_address" type="text">
            </div>
            <div class="col-md-2 col-xs-6">
                <label class="grey" for="codePostal">{l s='Postcode :' mod='relaiscolis'}</label>
                <input class="small"value="{$postcode|escape:'htmlall':'UTF-8'}" name="form_CP" id="form_CP" type="text">
            </div>
            <div class="col-md-2 col-xs-6">
                <label class="grey" for="ville">{l s='City :' mod='relaiscolis'}</label>
                <input class="small" value="{$city|escape:'htmlall':'UTF-8'}" name="form_city" id="form_city" type="text">
            </div>
            <div class="col-md-2 col-xs-6">
                <label class="grey" for="ville">{l s='Country :' mod='relaiscolis'}</label>
                <select class="small" name="list_country_rc" id="list_country_rc">
                    {foreach from=$limited_countries item=country}
                        <option value="{$country.iso3|escape:'htmlall':'UTF-8'}" {if $country_selected == $country.iso3}selected{/if}>{$country.name}</option>
                    {/foreach}
                </select>
            </div>
        </div>

        <div class="col-md-12 col-xs-12 txt-center">
            <button class="red-link button-relais" type="button" onclick="launchResearch();
                    return false;">{l s='Ok' mod='relaiscolis'}</button>
            <div class="flag-img">
                <img class="logo-relais" src="{$module_dir|escape:'htmlall':'UTF-8'}views/img/rc_long_logo.png"/>
            </div>
        </div>
        <div id="divEmplacement" style="display: none">
            Choisissez un emplacement :
            <select name="selListeAdresses" id="selListeAdresses">
            </select>
            <input type="button" value="Afficher" name="makeReco" onclick="resolveAmbiguity();" />
        </div>
        <div class="col-sm-12 col-xs-12 clearfix" id="map_rc_wrapper">
            <div class="col-sm-4 col-xs-12" id="divInfohtml">
            </div>
            <div class="col-sm-8 col-xs-12" id="divMapContainer">
            </div>
            <input type="hidden" id="refresh" value ="0"/>
        </div>
    </div>
</div>
<div id="relais_home_options">
    <div class="col-sm-12 col-xs-12 clearfix choice-info">
        <div class="col-sm-6 col-xs-12">
            <p><span class="relay-info-intro">{l s='You will be delivered at this address :' mod='relaiscolis'}</span><p>
            <p><span class="relay-info-title">{if isset($lastname)}{$lastname|escape:'htmlall':'UTF-8'}{/if} {if isset($firstname)}{$firstname|escape:'htmlall':'UTF-8'}{/if}</span></p><br/>
            <p>{if $street}{$street|escape:'htmlall':'UTF-8'}{/if}</p>
            <p>{if $street2}{$street2|escape:'htmlall':'UTF-8'}{/if}</p>
            <p>{if $postcode}{$postcode|escape:'htmlall':'UTF-8'}{/if} {if $city}{$city|escape:'htmlall':'UTF-8'}{/if}</p>
        </div>
        <div class="col-sm-6 col-xs-12" {if !$is_selected_home_plus} style="display:none;"{/if}>
            <p><span class="relay-info-intro">{l s='Complementary informations :' mod='relaiscolis'}</span></p>
            <div class="col-xs-6 text-right">{l s='Digicode :' mod='relaiscolis'}</div><div class="col-xs-6"> <input class="option-home-customer" type="text" name="digicode"  onchange = "submitUpdateOptionHome('digicode');" id="digicode" value="{if $digicode}{$digicode|escape:'htmlall':'UTF-8'}{/if}"/></div>
            <br/><br/>
            <div class="col-xs-6 text-right">{l s='Delivery Floor :' mod='relaiscolis'}</div><div class="col-xs-6"> <select class="small option-home-customer" name="floor_delivery" onchange = "submitUpdateOptionHome('floor_delivery');" id="floor_delivery">
             {foreach from=$list_type_floor key=key item=floor}
                        <option value="{$key|escape:'htmlall':'UTF-8'}" {if $floor_delivery == $key}selected{/if}>{$floor|escape:'htmlall':'UTF-8'}</option>
                    {/foreach}
                </select></div>
            <br/><br/>
            <div class="col-xs-6 text-right">{l s='Home type :' mod='relaiscolis'} </div><div class="col-xs-6"> <select class="small option-home-customer" name="type_home" onchange = "submitUpdateOptionHome('type_home');" id="type_home">
                    {foreach from=$list_type_home key=key item=home}
                        <option value="{$key|escape:'htmlall':'UTF-8'}" {if $type_home_selected == $key}selected{/if}>{$home|escape:'htmlall':'UTF-8'}</option>
                    {/foreach}
                </select></div>
            <br/><br/>
            <div class="col-xs-6 text-right">{l s='Elevator :' mod='relaiscolis'}</div><div class="col-xs-6"><select class="small option-home-customer" name="elevator" onchange = "submitUpdateOptionHome('elevator');" id="elevator">
                    <option value="0" {if !$elevator}selected{/if}>{l s='No' mod='relaiscolis'}</option>
                    <option value="1" {if $elevator}selected{/if}>{l s='Yes' mod='relaiscolis'}</option>
                </select></div>
            <br/><br/>
        </div>
        {if $is_selected_home_plus}
            <input type="hidden" value="{$id_cart_home|escape:'htmlall':'UTF-8'}" id="id_cart_home" />
            <input type="hidden" value="{$id_customer_home|escape:'htmlall':'UTF-8'}" id="id_customer_home" />
            <div class="col-sm-6 col-xs-12">
                {if !empty($customer_option_choice) || $top24}

                    <p><span class="relay-info-intro">{l s='Delivery Options :' mod='relaiscolis'}</span><p>
                        <!-- options list !-->

                        {foreach from=$customer_option_choice item=choice}
                        <div class="col-xs-6 text-left">{$choice.label|escape:'htmlall':'UTF-8'} {if $choice.cost > 0}<span class="rc_price">{$choice.cost|string_format:"%.2f"} {l s='€' mod='relaiscolis'}</span>{else}<span class="free_rc">{l s='Free' mod='relaiscolis'}</span>{/if}</div><input type="hidden" value="{$choice.cost}" id="{$choice.option}_cost" /><div class="col-xs-6"><select class="small option-home-customer" name="{$choice.option|escape:'htmlall':'UTF-8'}" id="{$choice.option|escape:'htmlall':'UTF-8'}" onchange = "submitUpdateOptionHome('{$choice.option}');">
                                <option value="0" {if !$choice.selected}selected{/if}>{l s='No' mod='relaiscolis'}</option>
                                <option value="1" {if $choice.selected}selected{/if}>{l s='Yes' mod='relaiscolis'}</option>
                            </select></div>
                        <br/><br/>
                    {/foreach}
                    {if $top24}
                        <div class="col-xs-6 text-left">{l s='Delivery in 24 Hours :' mod='relaiscolis'} {if $top_cost}<span class="rc_price">{$top_cost|string_format:"%.2f"|escape:'htmlall':'UTF-8'} {l s='€' mod='relaiscolis'}</span>{else}<span class="free_rc">{l s='Free' mod='relaiscolis'}</span>{/if}</div>
                        <input type="hidden" value="{$top_cost|escape:'htmlall':'UTF-8'}" id="top24_cost" /><div class="col-xs-6"><select class="small option-home-customer" name="top24" id="top24" onchange = "submitUpdateOptionHome('top24');">
                                <option value="0" {if !$top_selected}selected{/if}>{l s='No' mod='relaiscolis'}</option>
                                <option value="1" {if $top_selected}selected{/if}>{l s='Yes' mod='relaiscolis'}</option>
                            </select></div>
                        <br/><br/>
                    {/if}
                    <a href="{$order_link|escape:'htmlall':'UTF-8'}" class="button btn btn-default button-medium" >
                        <span>
                            {l s='Update delivery cost' mod='relaiscolis'}
                        </span>
                    </a>
                {/if}
            </div>
            <div class="col-sm-6 col-xs-12">
                {if !empty($mandatory_option)}
                    <p><span class="relay-info-intro">{l s='Details of basic price :' mod='relaiscolis'}</span><p>
                    <div class="col-xs-6 text-left">{l s='Basic cost :' mod='relaiscolis'} {if $basic_home_plus_cost > 0}<span class="rc_price">{$basic_home_plus_cost|string_format:"%.2f"|escape:'htmlall':'UTF-8'} {l s='€' mod='relaiscolis'}</span>{else}<span class="free_rc">{l s='Free' mod='relaiscolis'}</span>{/if}</div> <br/><br/>
                        {foreach from=$mandatory_option item=choice}
                        <div class="col-xs-6 text-left">{$choice.label|escape:'htmlall':'UTF-8'} {if $choice.cost > 0}<span class="rc_price">{$choice.cost|string_format:"%.2f"} {l s='€' mod='relaiscolis'}</span>{else}<span class="free_rc">{l s='Free' mod='relaiscolis'}</span>{/if}</div> <br/><br/>
                        {/foreach}
                    {/if}
                    {if $top24_unselectable}
                    <div class="col-xs-6 text-left">{l s='Delivery in 24 Hours :' mod='relaiscolis'} {if $top_cost}<span class="rc_price">{$top_cost|string_format:"%.2f"|escape:'htmlall':'UTF-8'} {l s='€' mod='relaiscolis'}</span>{else}<span class="free_rc">{l s='Free' mod='relaiscolis'}</span>{/if}</div>
                    <br/><br/>
                {/if}
            </div>
        {/if}
    </div>
</div>
<!--script src="https://secure-apijs.viamichelin.com/apijsv2/api/js?key={$key_build|escape:'htmlall':'UTF-8'}&lang=fra&protocol=https"
type="text/javascript"></script!-->
<script type="text/javascript">
    {literal}
        $(document).ready(function () {
            var is_selected = '{/literal}{$is_selected|escape:'htmlall':'UTF-8'}{literal}';
            var is_selected_home = '{/literal}{$is_selected_home|escape:'htmlall':'UTF-8'}{literal}';
            var is_selected_home_plus = '{/literal}{$is_selected_home_plus|escape:'htmlall':'UTF-8'}{literal}';
            var have_selected_point = '{/literal}{$have_selected_point|escape:'htmlall':'UTF-8'}{literal}';
            var have_selected_last = '{/literal}{$have_selected_last|escape:'htmlall':'UTF-8'}{literal}';
            if (is_selected_home || is_selected_home_plus) {
                $('#relais_home_options').show('1000');
            }
            else {
                $('#relais_home_options').hide('1000');
            }
            if (is_selected) {
                $('#frame_relais').show('1000');
            }
            else {
                $('#frame_relais').hide('1000');
            }
            if (!have_selected_point && !have_selected_last) {
                $('#divGlobal').show('1000');
            }
            else {
                $('#divGlobal').hide('1000');
            }
            $('#set-another-relay').click(function () {
                $('#divGlobal').show('1000');
            });
    {/literal}
    {foreach from=$ids_relais_exclude item=id}
        {literal}
                var unselected = false;
                var input_id = '';
                $('.delivery_option').each(function ( ) {
                    if ($(this).find('input.delivery_option_radio').val() == '{/literal}{$id|escape:'htmlall':'UTF-8'}{literal},') {
                        /*input_id = $(this).find('input.delivery_option_radio').attr('id');
                         $('#' + input_id).prop('checked', false);*/
                        $(this).remove();
                    } /*else {
                     if (must_unselected === true && unselected == false) {
                     input_id = $(this).find('input.delivery_option_radio').attr('id');
                     $('#' + input_id).prop('checked', true);
                     unselected = true;
                     }
                     }*/

                });
        {/literal}
        {literal}
            $('#id_carrier{/literal}{$id|escape:'htmlall':'UTF-8'}{literal}').parent().parent().remove();
        {/literal}
    {/foreach}
    {literal}
        });
    {/literal}
</script>
