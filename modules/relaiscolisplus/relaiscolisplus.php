<?php
/**
 * 1969-2017 Relais Colis
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@relaiscolis.com so we can send you a copy immediately.
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1969-2017 Relais Colis
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
if (!defined('_PS_VERSION_')) {
    exit;
}
require_once _PS_MODULE_DIR_.'relaiscolis/relaiscolis.php';
require_once _PS_MODULE_DIR_.'relaiscolis/classes/RelaisColisInfo.php';
require_once _PS_MODULE_DIR_.'relaiscolis/classes/RelaisColisApi.php';
require_once _PS_MODULE_DIR_.'relaiscolis/classes/RelaisColisOrder.php';
require_once _PS_MODULE_DIR_.'relaiscolis/classes/RelaisColisProduct.php';
require_once _PS_MODULE_DIR_.'relaiscolis/classes/RelaisColisHomeOptions.php';
require_once _PS_MODULE_DIR_.'relaiscolis/classes/RelaisColisInfoHome.php';

class Relaiscolisplus extends CarrierModule
{

    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'relaiscolisplus';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.0';
        $this->author = 'Quadra Informatique';
        $this->need_instance = 1;
        $this->limited_countries = array(
            'france' => array(
                'iso2' => 'FR',
                'iso3' => 'FRA',
                'name' => $this->l('France')),
            'belgique' => array(
                'iso2' => 'BE',
                'iso3' => 'BEL',
                'name' => $this->l('Belgium')),
            'monaco' => array(
                'iso2' => 'MC',
                'iso3' => 'MCO',
                'name' => $this->l('Monaco'))
        );

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Relais Colis Home PLUS');
        $this->description = $this->l('Relais colis Home PLUSModule for prestashop');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall relais colis module ?');
        $this->dependencies = array(
            'relaiscolis');
        $this->ps_versions_compliancy = array(
            'min' => '1.6',
            'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        // adding home + carrier
        $carrier = $this->addCarrierHomePlus();
        $this->addZones($carrier);
        $this->addGroups($carrier);
        $this->addRangesHome($carrier);

        return parent::install() && $this->registerHook('displayCarrierList');
    }

    public function uninstall()
    {
        // delete carrier Home +
        $relais_carrier = new Carrier(Configuration::getGlobalValue('RELAISCOLIS_ID_HOME_PLUS'));
        if ((int)$relais_carrier->id) {
// If relais colis carrier is default set other one as default
            if (Configuration::get('PS_CARRIER_DEFAULT') == (int)$relais_carrier->id) {
                $carriers_d = Carrier::getCarriers($this->context->language->id);
                foreach ($carriers_d as $carrier_d) {
                    if ($carrier_d['active'] && !$carrier_d['deleted'] && ($carrier_d['name'] != $this->config['name'])) {
                        Configuration::updateValue('PS_CARRIER_DEFAULT', $carrier_d['id_carrier']);
                    }
                }
            }
            // Save old carrier id
            Configuration::updateGlobalValue('RC_CARRIER_ID_HIST', Configuration::getGlobalValue('RC_CARRIER_ID_HIST').'|'.(int)$relais_carrier->id);
            $relais_carrier->deleted = 1;
            if (!$relais_carrier->update()) {
                return false;
            }
        }
        return parent::uninstall();
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        $cart = $this->context->cart;

        $home_plus_carrier = new Carrier((int)Configuration::getGlobalValue('RELAISCOLIS_ID_HOME_PLUS'));

        $options_home_available = $this->getOptionsProductCart($cart->getProducts());
        $options_top = $this->hasTopOptions($cart->getProducts());
        $options_sensible = $this->hasSensibleOptions($cart->getProducts());

        // add customer choic price
        if ((int)$result = RelaisColisInfoHome::alreadyExists((int)$cart->id, (int)$cart->id_customer)) {
            $relais_info_home = new RelaisColisInfoHome((int)$result);
        } else {
            $relais_info_home = new RelaisColisInfoHome();
            $relais_info_home->id_cart = $cart->id;
            $relais_info_home->id_customer = $cart->id_customer;
        }

        if ($options_sensible) {
            //$shipping_cost += (float)Configuration::get('RC_SENSIBLE_COST');
            $relais_info_home->sensible = 1;
        }
        if ($options_top) {
            if (Configuration::get('RC_TOP') && $relais_info_home->top) {
                $shipping_cost += (float)Configuration::get('RC_TOP_COST');
            }
            if (!Configuration::get('RC_TOP')) {
                $shipping_cost += (float)Configuration::get('RC_TOP_COST');
                $relais_info_home->top = 1;
            }
        }
        $options_home_list = RelaisColisHomeOptions::getRelaisColisHomeOptionsActive();
        if (is_array($options_home_list)) {
            foreach ($options_home_list as $row) {
                if ((in_array($row['option'], $options_home_available))) {
                    if ($row['customer_choice'] && $relais_info_home->{$row['option']}) {
                        $shipping_cost += (float)$row['cost'];
                        $relais_info_home->{$row['option']} = 1;
                    }
                    if (!$row['customer_choice']) {
                        $shipping_cost += (float)$row['cost'];
                        $relais_info_home->{$row['option']} = 1;
                    }
                } else {
                    $relais_info_home->{$row['option']} = 0;
                }
            }
        }
        $relais_info_home->save();


        return $shipping_cost;
    }

    public function getOrderShippingCostExternal($params)
    {
        return true;
    }

    protected function addCarrierHomePlus()
    {
        $carrier = new Carrier();

        $carrier->name = $this->l('Relais Colis Home +');
        $carrier->is_module = true;
        $carrier->active = 0;
        $carrier->range_behavior = 1;
        $carrier->need_range = 1;
        $carrier->shipping_external = true;
        $carrier->max_height = 0;
        $carrier->max_weight = 0;
        $carrier->url = 'http://www.relaiscolis.com/Tracking/trackandtrace.aspx?ens_id=@';
        $carrier->external_module_name = $this->name;
        $carrier->shipping_method = 2;

        foreach (Language::getLanguages() as $lang) {
            $carrier->delay[$lang['id_lang']] = $this->l('Fast delivery');
        }

        if ($carrier->add() == true) {
            @copy(dirname(__FILE__).'/views/img/carrier_image.jpg', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');
            Configuration::updateGlobalValue('RELAISCOLIS_ID_HOME_PLUS', (int)$carrier->id);
            return $carrier;
        }

        return false;
    }

    protected function addGroups($carrier)
    {
        $groups_ids = array();
        $groups = Group::getGroups(Context::getContext()->language->id);
        foreach ($groups as $group) $groups_ids[] = $group['id_group'];

        $carrier->setGroups($groups_ids);
    }

    protected function addRangesHome($carrier)
    {
        $range_price = new RangePrice();
        $range_price->id_carrier = $carrier->id;
        $range_price->delimiter1 = '0';
        $range_price->delimiter2 = '10000';
        $range_price->add();

        $range_weight = new RangeWeight();
        $range_weight->id_carrier = $carrier->id;
        $range_weight->delimiter1 = '0';
        $range_weight->delimiter2 = '10000';
        $range_weight->add();
    }

    protected function addZones($carrier)
    {
        return true;
    }

    public function hookDisplayCarrierList($params)
    {
        
    }

    public function hasTopOptions($products_list)
    {
        $options_top = false;
        if (is_array($products_list)) {
            foreach ($products_list as $product) {
                $options_home_product = RelaisColisProduct::getProductOptions((int)$product['id_product']);
                if (is_array($options_home_product)) {
                    foreach ($options_home_product as $option) {
                        if ($option['top']) {
                            if ($this->isAvailableTop()) {
                                $options_top = true;
                            }
                        }
                    }
                }
            }
        }
        return $options_top;
    }

    public function hasSensibleOptions($products_list)
    {
        $options_sensible = false;
        if (is_array($products_list)) {
            foreach ($products_list as $product) {
                $options_home_product = RelaisColisProduct::getProductOptions((int)$product['id_product']);
                if (is_array($options_home_product)) {
                    foreach ($options_home_product as $option) {
                        if ($option['sensible']) {
                            $options_sensible = true;
                        }
                    }
                }
            }
        }
        return $options_sensible;
    }

    public function getHomePlusBasicCost()
    {
        if (Configuration::getGlobalValue('RELAISCOLIS_ID_HOME_PLUS')) {
            $carrier = new Carrier((int)Configuration::getGlobalValue('RELAISCOLIS_ID_HOME_PLUS'));
            $address = new Address((int)$this->context->cart->id_address_delivery);
            $id_zone = Address::getZoneById((int)$address->id);
            $products = $this->context->cart->getProducts();
            $additional_shipping_cost = 0;
            //Additional shipping cost on product
            foreach ($products as $product) {
                if (version_compare(_PS_VERSION_, '1.5', '<'))
                    $additional_shipping_cost += (float)$product['additional_shipping_cost'] * $product['quantity'];
                elseif (!$product['is_virtual'])
                    $additional_shipping_cost += (float)$product['additional_shipping_cost'] * $product['quantity'];
            }
            if ($carrier->shipping_handling)
                return $this->getCostByShippingMethod($carrier, $id_zone) + (float)$additional_shipping_cost + (float)Configuration::get('PS_SHIPPING_HANDLING');
            else
                return $this->getCostByShippingMethod($carrier, $id_zone) + (float)$additional_shipping_cost;
        }
        return false;
    }

    public function getCostByShippingMethod($carrier, $id_zone)
    {
        if (version_compare(_PS_VERSION_, '1.5', '<'))
            if (!is_object($this->context->cart))
                $this->context->cart = new Cart();

        if ($carrier->shipping_method) {
            if ($carrier->shipping_method == 1)
                if ($carrier->getDeliveryPriceByWeight($this->context->cart->getTotalWeight(), $id_zone))
                    return $carrier->getDeliveryPriceByWeight($this->context->cart->getTotalWeight(), $id_zone);
            if ($carrier->shipping_method == 2)
                if ($carrier->getDeliveryPriceByPrice(
                        $this->context->cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, $this->context->cart->id_currency))
                    return $carrier->getDeliveryPriceByPrice(
                            $this->context->cart->getOrderTotal(
                                true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, $this->context->cart->id_currency);
        }
        else {
            if (Configuration::get('PS_SHIPPING_METHOD'))
                if ($carrier->getDeliveryPriceByWeight($this->context->cart->getTotalWeight(), $id_zone))
                    return $carrier->getDeliveryPriceByWeight($this->context->cart->getTotalWeight(), $id_zone);
            if (!Configuration::get('PS_SHIPPING_METHOD'))
                if ($carrier->getDeliveryPriceByPrice(
                        $this->context->cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, $this->context->cart->id_currency))
                    return $carrier->getDeliveryPriceByPrice(
                            $this->context->cart->getOrderTotal(
                                true, Cart::BOTH_WITHOUT_SHIPPING), $id_zone, $this->context->cart->id_currency);
        }
        return false;
    }

    public function getOptionsProductCart($products_list)
    {
        $options_home_available = array();
        if (is_array($products_list)) {
            foreach ($products_list as $product) {
                $options_home_product = RelaisColisProduct::getProductOptions((int)$product['id_product']);
                if (is_array($options_home_product)) {
                    foreach ($options_home_product as $option) {
                       if (!in_array('schedule', $options_home_available) && $option['schedule']) {
                            $options_home_available[] = 'schedule';
                        }
                        if (!in_array('retrieve_old_equipment', $options_home_available) && $option['retrieve_old_equipment']) {
                            $options_home_available[] = 'retrieve_old_equipment';
                        }
                        if (!in_array('delivery_on_floor', $options_home_available) && $option['delivery_on_floor']) {
                            $options_home_available[] = 'delivery_on_floor';
                        }
                        if (!in_array('delivery_at_two', $options_home_available) && $option['delivery_at_two']) {
                            $options_home_available[] = 'delivery_at_two';
                        }
                        if (!in_array('turn_on_home_appliance', $options_home_available) && $option['turn_on_home_appliance']) {
                            $options_home_available[] = 'turn_on_home_appliance';
                        }
                        if (!in_array('mount_furniture', $options_home_available) && $option['mount_furniture']) {
                            $options_home_available[] = 'mount_furniture';
                        }
                        if (!in_array('non_standard', $options_home_available) && $option['non_standard']) {
                            $options_home_available[] = 'non_standard';
                        }
                        if (!in_array('unpacking', $options_home_available) && $option['unpacking']) {
                            $options_home_available[] = 'unpacking';
                        }
                        if (!in_array('evacuation_packaging', $options_home_available) && $option['evacuation_packaging']) {
                            $options_home_available[] = 'evacuation_packaging';
                        }
                        if (!in_array('recovery', $options_home_available) && $option['recovery']) {
                            $options_home_available[] = 'recovery';
                        }
                        if (!in_array('delivery_desired_room', $options_home_available) && $option['delivery_desired_room']) {
                            $options_home_available[] = 'delivery_desired_room';
                        }
                        if (!in_array('recover_old_bedding', $options_home_available) && $option['recover_old_bedding']) {
                            $options_home_available[] = 'recover_old_bedding';
                        }
                        if (!in_array('assembly', $options_home_available) && $option['assembly']) {
                            $options_home_available[] = 'assembly';
                        }
                    }
                }
            }
        }
        return $options_home_available;
    }

    public function isAvailableTop()
    {
        $time = date("H:i");
        $time = explode(':', $time);
        $current_hour = $time[0];
        $current_min = $time[1];

        if ($available = Configuration::get('RC_TOP_HOUR')) {
            $available = explode(':', $available);
            if (isset($available[0])) {
                $available_hour = (int)$available[0];
            }
            if (isset($available[0])) {
                $available_min = (int)$available[0];
            }
            if ($available_hour && $available_min) {
                if ((int)$available_hour > (int)$current_hour) {
                    return true;
                }
                if ((int)$available_hour == (int)$current_hour && (int)$available_min >= (int)$current_hour) {
                    return true;
                }
            }
        }
        return false;
    }
}
