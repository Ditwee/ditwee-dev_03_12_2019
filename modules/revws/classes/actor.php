<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


namespace Revws;
use \Context;

class Actor {
  private static $actor = null;

  public static function setActor($actor) {
    self::$actor = $actor;
  }

  public static function getActor() {
    if (! self::$actor) {
      $context = Context::getContext();
      if ($context->employee && $context->employee->id) {
        self::$actor = 'employee';
      } else {
        self::$actor = 'visitor';
      }
    }
    return self::$actor;
  }
}
