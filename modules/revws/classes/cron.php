<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


namespace Revws;
use \Exception;
use \Db;
use \Logger;

interface CronTask {
  function getItems();
  function processItem($item);
}

class Cron {
  private $settings;
  private $url;

  private $tasks = [];

  private $running;
  private $registered;

  public function __construct($module) {
    $settings = $module->getSettings();
    $this->settings = $settings;
    $this->url = $module->getUrl('cron', [
      'secure_key' => $settings->getCronSecret()
    ]);
    $this->running = false;
  }

  public function addTask(CronTask $task) {
    $this->tasks[] = $task;
  }

  public function isActive() {
    return (time() - $this->getLastExecution()) < (1 * 24 * 60 * 60);
  }

  public function getUrl() {
    return $this->url;
  }

  public function getLastExecution() {
    return $this->settings->getLastCronEvent();
  }

  public function process() {
    @set_time_limit(0);
    if (! $this->registered) {
      $this->registered = true;
      register_shutdown_function(array($this, "onShutdown"));
    }
    $this->settings->markCron();
    $this->running = false;
    $orig = set_error_handler(array($this, 'errorHandler'));
    $conn = Db::getInstance();
    $res = $conn->executeS("SELECT GET_LOCK('revws_cron', 5) as `success`");
    try {
      $this->running = ($res && $res[0] && intval($res[0]['success']));
      if (! $this->running) {
        throw new Exception("Failed to obtain lock, another cron is currently in progress");
      }
      foreach ($this->tasks as $task) {
        $items = $task->getItems();
        foreach ($items as $item) {
          $task->processItem($item);
        }
      }
    } catch (Exception $e) {
      $this->logError($e->getMessage());
    }
    $this->releaseLock();
    set_error_handler($orig);
  }

  private function releaseLock() {
    if ($this->running) {
      $this->running = false;
      Db::getInstance()->execute("SELECT RELEASE_LOCK('revws_cron')");
    }
  }

  private function logError($msg) {
    Logger::addLog("Revws module: cron: $msg");
  }

  public function errorHandler($errno, $errstr, $file, $lineNo) {
    if ($errno == E_USER_ERROR) {
      $this->logError($errstr . " in " . $file . " on line " . $lineNo);
      $this->releaseLock();
    }
    return false;
  }

  public function onShutdown() {
    if ($this->running) {
      $error = error_get_last();
      if ($error && $error['type'] === E_ERROR) {
        $this->logError($error['message'] . " in " . $error['file'] . " on line " . $error['line']);
      }
      $this->releaseLock();
    }
  }

}
