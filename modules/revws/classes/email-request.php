<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


namespace Revws;
use \Db;
use \Context;
use \Group;
use \OrderState;
use \Exception;
use \Shop;
use \Product;
use \RevwsEmail;
use \RevwsCriterion;
use \ImageType;

class EmailRequest implements CronTask {
  private $module;

  // statuses
  const TO_BE_SENT = 'TO_BE_SENT';
  const SENT = 'SENT';
  const ERROR = 'ERROR';
  const DATE_THRESHOLD  = 'DATE_THRESHOLD';
  const NO_CONSENT = 'NO_CONSENT';
  const ORDER_TOO_YOUNG = 'ORDER_TOO_YOUNG';
  const ORDER_TOO_OLD = 'ORDER_TOO_OLD';
  const ORDER_NOT_DELIVERED = 'ORDER_NOT_DELIVERED';
  const INVALID_ORDER_STATE = 'INVALID_ORDER_STATE';
  const DELIVERY_DELAY = 'DELIVERY_DELAY';
  const NOT_REVIEWABLE = 'NOT_REVIEWABLE';
  const ALL_PRODUCTS_REVIEWED = 'ALL_PRODUCTS_REVIEWED';
  const INVALID_CUSTOMER_GROUP = 'INVALID_CUSTOMER_GROUP';

  public function __construct($module) {
    $this->module = $module;
  }

  public function process() {
    foreach ($this->getItems() as $orderId) {
      $this->processItem($orderId);
    }
  }

  public function getItems() {
    $items = [];
    $settings = $this->module->getSettings();
    if ($settings->areEmailRequestsActive()) {
      $sql = "SELECT id_order FROM (" . $this->getInnerQuery() .") as `inner` WHERE ".$this->getOrderConditionWhere();
      $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->query($sql);
      if ($result) {
        while ($row = $result->fetch()) {
          $orderId = (int)$row['id_order'];
          $items[] = $orderId;
        }
      }
    }
    return $items;
  }

  public function processItem($orderId) {
    $this->processOrder($orderId, false);
  }

  public function processOrder($orderId, $force=false) {
    $orderId = (int)$orderId;
    $sql = $this->getOrderSql($orderId, $force);
    $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    if (! $result) {
      return false;
    }
    $settings = $this->module->getSettings();
    $email = RevwsEmail::forOrder($orderId, RevwsEmail::REVIEW_REQUEST);
    $customer = $email->getCustomer();
    $products = [];
    foreach ($result as $row) {
      $productId = (int)$row['product_id'];
      $lang = (int)$row['id_lang'];
      if (! isset($products[$productId])) {
        $productData = FrontApp::getProductData($productId, $lang);
        if ((int)$productData['id']) {
          $products[$productId] = [
            'name' => $productData['name'],
            'url' => $email->getProductUrl($settings, $productId),
            'image' => $email->getProductImageUrl($settings, $productData['imageId'], 'home'),
            'rate1' => $email->getRatingUrl($settings, $productId, 1),
            'rate2' => $email->getRatingUrl($settings, $productId, 2),
            'rate3' => $email->getRatingUrl($settings, $productId, 3),
            'rate4' => $email->getRatingUrl($settings, $productId, 4),
            'rate5' => $email->getRatingUrl($settings, $productId, 5),
          ];
        }
      }
    }
    if (! $products) {
      $email->error = 'No products to review';
      $email->save();
      throw new Exception($email->error);
    }
    $items = $this->module->renderTemplate('views/templates/mail/revws-review-request-items.tpl', [ 'products' => $products ]);
    $data = [
      '{empty_star}' => _PS_BASE_URL_ . $this->module->getPath('views/img/empty-star.png'),
      '{filled_star}' => _PS_BASE_URL_ . $this->module->getPath('views/img/filled-star.png'),
      '{unsubscribe}' => $email->getUnsubscribeUrl($settings),
      '{firstname}' => $customer->firstname,
      '{lastname}' => $customer->lastname,
      '{products}' => $items
    ];
    $cnt = 0;
    foreach ($products as $product) {
      $cnt++;
      foreach ($product as $key => $value) {
        $data['{product_'.$cnt.'_'.$key.'}'] = $value;
      }
    }
    if (! $email->send($data)) {
      throw new Exception($email->error ? $email->error : 'Failed to send email');
    }
    return true;
  }

  private function getOrderConditions() {
    $conds = [];
    $settings = $this->module->getSettings();

    // BLOCKED states

    // check if request has been sent
    $conds[] = self::defineCondition("NOT(sent)", self::SENT);

    // check if request has been sent with error
    $conds[] = self::defineCondition("error IS NULL", self::ERROR);

    // do not include orders that were created before threshold date
    // (module installation date is used by default)
    $threshold = (int)$settings->getEmailRequestOrderDateThreshold();
    if ($threshold) {
      $conds[] = self::defineCondition("date_add_ts > $threshold", SELF::DATE_THRESHOLD);
    }

    // check if we have consent
    $conds[] = self::defineCondition("consent", SELF::NO_CONSENT);

    // check if order contains at least one reviewable product
    $conds[] = self::defineCondition("items > 0", self::NOT_REVIEWABLE);

    // check if order contains at least one reviewable product that is not reviewed yet
    $conds[] = self::defineCondition("items > reviewed_items", self::ALL_PRODUCTS_REVIEWED);

    // verify customer access to group
    $conds[] = self::defineCondition("valid_group", self::INVALID_CUSTOMER_GROUP);

    // ACTIVE states
    // verify min order age
    $orderAge = (int)$settings->getEmailRequestOrderMinAge();
    if ($orderAge) {
      $conds[] = self::defineCondition("date_add < DATE_SUB(NOW(), INTERVAL $orderAge DAY)", self::ORDER_TOO_YOUNG);
    }

    // verify max order age
    $maxAge = (int)$settings->getEmailRequestOrderMaxAge();
    if ($maxAge) {
      $conds[] = self::defineCondition("date_add > DATE_SUB(NOW(), INTERVAL $maxAge DAY)", self::ORDER_TOO_OLD);
    }

    // verify curent order status
    $statuses = $settings->getEmailRequestOrderStatuses();
    if (! is_array($statuses) || count($statuses) === 0) {
      $statuses = [ -1 ];
    }
    $statuses = array_map('intval', $statuses);
    $conds[] = self::defineCondition("current_state IN (".join(',', $statuses).")", self::INVALID_ORDER_STATE);

    // verify delivery date delay
    $deliveryDelay = (int)$settings->getEmailRequestOrderDeliveryDelay();
    if ($deliveryDelay) {
      $conds[] = self::defineCondition("delivery_date > '1901-01-01'", self::ORDER_NOT_DELIVERED);
      $conds[] = self::defineCondition("delivery_date < DATE_SUB(NOW(), INTERVAL $deliveryDelay DAY)", self::DELIVERY_DELAY);
    }

    return $conds;
  }

  private function getOrderConditionWhere() {
    $conds = $this->getOrderConditions();
    if ($conds) {
      $ret = [];
      foreach ($conds as $def) {
        $ret[] = $def['condition'];
      }
      return implode(' AND ', $ret);
    }
    return '1';
  }

  private function getOrderConditionCase() {
    $conds = $this->getOrderConditions();
    $send = self::TO_BE_SENT;
    if ($conds) {
      $case = "CASE";
      foreach ($conds as $def) {
        $cond = $def['condition'];
        $desc = $def['description'];
        $case .= " WHEN NOT($cond) THEN '$desc'";
      }
      $case .= " ELSE '$send' END";
    } else {
      $case = "'$send'";
    }
    return $case;
  }

  private function getConsentCondition() {
    $mode = $this->module->getSettings()->getEmailRequestConsentMode();
    return "(c.id_customer IS NOT NULL AND " . Subscription::getSqlExpression($mode, 's', 'c') . ")";
  }

  private static function defineCondition($condition, $description) {
    return [
      'condition' => $condition,
      'description' => $description
    ];
  }

  public function getOrderList($options) {
    $page = isset($options['page']) ? (int)$options['page'] : 0;
    $pageSize = isset($options['pageSize']) ? (int)$options['pageSize'] : 10;
    $order = isset($options['order']) ? $options['order'] : 'id';
    $orderDir = isset($options['orderDir']) ? $options['orderDir'] : 'desc';
    $offset = $page * $pageSize;

    $orderDirection = $orderDir == 'desc' ? 'DESC' : 'ASC';
    $orderField = 'id_order';
    $where = '1';
    if (isset($options['filters']['status'])) {
      $statuses = [];
      foreach ($options['filters']['status'] as $status) {
        $statuses[] = pSql($status);
      }
      $statuses = implode("', '", $statuses);
      $where .= " AND ".$this->getOrderConditionCase()." IN ('$statuses')";
    }
    $sql = "
      SELECT
        SQL_CALC_FOUND_ROWS
        id_order,
        reference,
        current_state,
        date_add_ts,
        total_items,
        items,
        reviewed_items,
        email,
        customer_name,
        id_customer,
        consent,
        error,
        ".$this->getOrderConditionCase()." as `status`
      FROM (" . $this->getInnerQuery() .") as `inner`
      WHERE $where
      ORDER BY $orderField $orderDirection
      LIMIT $pageSize OFFSET $offset
    ";
    $conn = Db::getInstance(_PS_USE_SQL_SLAVE_);
    $result = $conn->query($sql);
    $orders = [];
    $count = 0;
    if ($result) {
      while ($row = $result->fetch()) {
        $orders[] = [
          'id' => (int)$row['id_order'],
          'reference' => $row['reference'],
          'date' => (int)$row['date_add_ts'],
          'orderState' => (int)$row['current_state'],
          'customerName' => $row['customer_name'] ? $row['customer_name'] : 'Unnamed customer',
          'customerId' => (int)$row['id_customer'],
          'email' => $row['email'],
          'totalItems' => (int)$row['total_items'],
          'items' => (int)$row['items'],
          'reviewedItems' => (int)$row['reviewed_items'],
          'consent' => !!(int)$row['consent'],
          'status' => $row['status'],
          'error' => $row['error']
        ];
      }
      $count = (int)$conn->getRow('SELECT FOUND_ROWS() AS r')['r'];
    }
    return [
      'total' => (int)$count,
      'page' => $page,
      'pages' => $pageSize > 0 ? ceil($count / $pageSize) : 1,
      'pageSize' => $pageSize,
      'order' => $order,
      'orderDir' => $orderDir,
      'orders' => $orders
    ];
    return $orders;
  }

  public function getOrdersByStatus($options) {
    $sql = "
      SELECT status, COUNT(1) AS `cnt` FROM (
        SELECT ".$this->getOrderConditionCase()." as `status`
        FROM (" . $this->getInnerQuery() .") as `inner`
      ) AS `inner2`
      GROUP BY status
    ";
    $conn = Db::getInstance(_PS_USE_SQL_SLAVE_);
    $result = $conn->query($sql);
    $statuses = [];
    if ($result) {
      while ($row = $result->fetch()) {
        $status = $row['status'];
        $cnt = (int)$row['cnt'];
        $statuses[$status] = $cnt;
      }
    }
    return $statuses;
  }

  private function getInnerQuery() {
    $settings = $this->module->getSettings();
    $reviewableProductSql = $this->reviewableProductSql();
    $groups = $settings->getEmailRequestCustomerGroups();
    if (is_array($groups) && count($groups) > 0) {
      if (count($groups) === count(self::getAllCustomerGroups())) {
        $customerGroupsSql = "1";
      } else {
        $groups = array_map('intval', $groups);
        $customerGroupsSql = "EXISTS(SELECT 1 FROM "._DB_PREFIX_."customer_group cg WHERE cg.id_customer = c.id_customer AND cg.id_group IN (".join(',', $groups)."))";
      }
    } else {
      $customerGroupsSql = "0";
    }
    return "
      SELECT
        o.id_order,
        o.reference,
        o.id_customer,
        o.date_add,
        UNIX_TIMESTAMP(o.date_add) as `date_add_ts`,
        o.delivery_date as `delivery_date`,
        o.current_state,
        IFNULL(c.newsletter, 0) as `newsletter`,
        c.email,
        (e.id_email IS NOT NULL AND NULLIF(e.error,'') IS NULL) as `sent`,
        NULLIF(e.error, '') as `error`,
        TRIM(CONCAT(c.firstname, ' ', c.lastname)) as `customer_name`,
        (SELECT COUNT(1) FROM "._DB_PREFIX_."order_detail d WHERE d.id_order = o.id_order) as `total_items`,
        (SELECT COUNT(1) FROM "._DB_PREFIX_."order_detail d INNER JOIN "._DB_PREFIX_."product p ON (d.product_id = p.id_product) WHERE d.id_order = o.id_order AND $reviewableProductSql) as `items`,
        (SELECT COUNT(DISTINCT r.id_product) FROM "._DB_PREFIX_."revws_review r, "._DB_PREFIX_."order_detail d WHERE d.id_order=o.id_order AND d.product_id = r.id_product AND o.id_customer = r.id_customer AND r.deleted=0 AND $reviewableProductSql) `reviewed_items`,
        $customerGroupsSql as `valid_group`,
        ".$this->getConsentCondition()." as `consent`
      FROM "._DB_PREFIX_."orders o
      LEFT JOIN "._DB_PREFIX_."customer c ON (c.id_customer = o.id_customer".Shop::addSqlRestriction(Shop::SHARE_CUSTOMER, 'c').")
      LEFT JOIN "._DB_PREFIX_."revws_subscription s ON (s.email = c.email)
      LEFT JOIN "._DB_PREFIX_."revws_email e ON (e.entity_type = 'order' AND e.id_entity = o.id_order AND e.id_email = (SELECT MAX(id_email) FROM "._DB_PREFIX_."revws_email e2 where e2.entity_type = 'order' AND e2.id_entity = o.id_order))
      ORDER BY id_order
    ";
  }

  private function getOrderSql($orderId, $force) {
    $orderId = (int)$orderId;
    $reviewableProductSql = $this->reviewableProductSql();
    $sql = "
      SELECT DISTINCT d.product_id, c.id_lang
      FROM "._DB_PREFIX_."orders o
      INNER JOIN "._DB_PREFIX_."customer c ON (c.id_customer = o.id_customer".Shop::addSqlRestriction(Shop::SHARE_CUSTOMER, 'c').")
      LEFT JOIN "._DB_PREFIX_."order_detail d ON (d.id_order = o.id_order)
      LEFT JOIN "._DB_PREFIX_."revws_review r ON (d.product_id = r.id_product AND o.id_customer = r.id_customer AND r.deleted=0)
      LEFT JOIN "._DB_PREFIX_."revws_email e ON (e.entity_type = 'order' AND e.id_entity = o.id_order)
      WHERE o.id_order = $orderId AND r.id_review IS NULL AND $reviewableProductSql";
    if (! $force) {
      $sql .= " AND e.id_email IS NULL";
    }
    return $sql . " ORDER BY d.total_price_tax_excl DESC";
  }

  private function reviewableProductSql() {
    if (RevwsCriterion::hasGlobalCriterion()) {
      return "1";
    }
    $cc = _DB_PREFIX_ . 'revws_criterion_category';
    $cp = _DB_PREFIX_ . 'revws_criterion_product';
    $product = _DB_PREFIX_ . 'product_shop';
    $criterion = _DB_PREFIX_ . 'revws_criterion';
    return "EXISTS(
      SELECT crit.id_criterion
        FROM $product ps
        INNER JOIN $cc cc ON (cc.id_category = ps.id_category_default)
        INNER JOIN $criterion crit ON (cc.id_criterion = crit.id_criterion)
        WHERE crit.active=1 AND ps.id_product=d.product_id AND ps.id_shop=o.id_shop
      UNION
      SELECT crit.id_criterion
        FROM $product ps
        INNER JOIN $cp cp ON (cp.id_product = ps.id_product)
        INNER JOIN $criterion crit ON (cp.id_criterion = crit.id_criterion)
        WHERE crit.active=1 AND ps.id_product=d.product_id AND ps.id_shop=o.id_shop
    )";
  }

  private function l($string) {
    return $this->module->l($string);
  }

  public static function getDeliveredOrderStatuses() {
    $lang = Context::getContext()->language->id;
    $shipped = [];
    $valid = [];
    foreach (OrderState::getOrderStates($lang) as $state) {
      $id = (int)$state['id_order_state'];
      if (! $state['deleted'] && $state['shipped']) {
        $shipped[] = $id;
      }
      if (! $state['deleted'] && $state['logable']) {
        $valid[] = $id;
      }
    }
    return $shipped ? $shipped : $valid;
  }

  public static function getAllOrderStatuses() {
    $lang = Context::getContext()->language->id;
    $states = [];
    foreach (OrderState::getOrderStates($lang) as $state) {
      $id = (int)$state['id_order_state'];
      if (! $state['deleted']) {
        $states[$id] = $state['name'];
      }
    }
    ksort($states);
    return $states;
  }

  public static function getAllCustomerGroups() {
    $lang = Context::getContext()->language->id;
    $groups = [];
    foreach (Group::getGroups($lang) as $group) {
      $id = (int)$group['id_group'];
      $groups[$id] = $group['name'];
    }
    ksort($groups);
    return $groups;
  }

}
