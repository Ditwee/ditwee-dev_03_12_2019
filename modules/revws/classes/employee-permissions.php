<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


namespace Revws;
use \RevwsReview;

class EmployeePermissions implements Permissions {
  static $instance = null;

  public static function getInstance() {
    if (! self::$instance) {
      self::$instance = new EmployeePermissions();
    }
    return self::$instance;
  }

  public function canCreateReview($productId) {
    return true;
  }

  public function canReportAbuse(RevwsReview $review) {
    return false;
  }

  public function canVote(RevwsReview $review) {
    return false;
  }

  public function canDelete(RevwsReview $review) {
    return true;
  }

  public function canEdit(RevwsReview $review) {
    return true;
  }
}
