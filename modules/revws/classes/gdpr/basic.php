<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


namespace Revws;
use \Hook;
use \Module;
use \Db;

class BasicGDPR implements GDPRInterface {

  public function getConsentMessage(Visitor $visitor) {
    // use build-in message
    return '';
  }

  public function isEnabled(Visitor $visitor) {
    return true;
  }

  public function logConsent(Visitor $visitor) {
    // no-op
  }
}
