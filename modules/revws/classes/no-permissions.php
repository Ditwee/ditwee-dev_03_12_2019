<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


namespace Revws;
use \RevwsReview;

class NoPermissions implements Permissions {

  public function canCreateReview($productId) {
    return false;
  }

  public function canReportAbuse(RevwsReview $review) {
    return false;
  }

  public function canVote(RevwsReview $review) {
    return false;
  }

  public function canDelete(RevwsReview $review) {
    return false;
  }

  public function canEdit(RevwsReview $review) {
    return false;
  }
}
