<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


namespace Revws;
use \RevwsReview;
use \RevwsCriterion;
use \RevwsEmail;
use \Context;
use \Customer;
use \Language;
use \Translate;
use \Validate;
use \Mail;
use \Exception;
use \Logger;

class Notifications {
  private static $instance;
  private $queue = [];

  // cache
  private $review;
  private $customer;

  public static function getInstance() {
    if (! self::$instance) {
      self::$instance = new Notifications();
    }
    return self::$instance;
  }

  // close connection, flush output and continue
  // processing notifications on backend only
  public function closeConnectionAndProcess($module) {
    if ($this->queue) {
      if (ob_get_length() > 0 ) {
        ob_end_flush();
      }
      flush();
      ignore_user_abort(true);
      if (function_exists('fastcgi_finish_request')) {
        fastcgi_finish_request();
      }
      $this->process($module);
    }
    // end request
    die();
  }


  /**
   * process work items
   */
  public function process($module) {
    if ($this->queue) {
      $module->clearCache();
      $settings = $module->getSettings();
      $krona = $module->getKrona();
      foreach ($this->queue as $workItem) {
        $type = $workItem['type'];
        $id = $workItem['id'];
        $actor = $workItem['actor'];
        try {
          call_user_func(array($this, $type), $id, $actor, $settings, $krona);
        } catch (Exception $e) {
          Logger::addLog("Revws module: failed to process $type: " . $e->getMessage());
        }
      }
    }
  }

  private function processReviewCreated($id, $actor, $settings, $krona) {
    if ($actor === 'visitor') {
      $review = $this->getReview($id);

      // send notification to administrator
      if ($settings->emailAdminReviewCreated()) {
        $this->sendToAdmin($settings, $review, RevwsEmail::ADMIN_REVIEW_CREATED);
      }

      // send thank you email
      if ($settings->emailAuthorThankYou()) {
        $this->sendToReviewer($settings, $review, RevwsEmail::AUTHOR_THANK_YOU);
      }

      $krona->reviewCreated($review);
    }
  }

  private function processReviewUpdated($id, $actor, $settings) {
    if ($actor === 'visitor') {
      // send notification to administrator
      if ($settings->emailAdminReviewUpdated()) {
        $review = $this->getReview($id);
        $this->sendToAdmin($settings, $review, RevwsEmail::ADMIN_REVIEW_UPDATED);
      }
    }
  }

  private function processReviewDeleted($id, $actor, $settings, $krona) {
    if ($actor === 'visitor') {
      // send notification to administrator
      $review = $this->getReview($id);
      if ($settings->emailAdminReviewDeleted()) {
        $this->sendToAdmin($settings, $review, RevwsEmail::ADMIN_REVIEW_DELETED);
      }
      $krona->reviewDeleted($review);
    }
    if ($actor === 'employee') {
      $review = $this->getReview($id);
      if ($settings->emailAuthorReviewDeleted()) {
        $this->sendToReviewer($settings, $review, RevwsEmail::AUTHOR_REVIEW_DELETED);
      }
      $krona->reviewRejected($review);
    }
  }

  private function processReviewApproved($id, $actor, $settings, $krona) {
    if ($actor === 'employee') {
      $review = $this->getReview($id);
      if ($settings->emailAuthorReviewApproved()) {
        $this->sendToReviewer($settings, $review, RevwsEmail::AUTHOR_REVIEW_APPROVED);
      }
      $krona->reviewApproved($review);
    }
  }

  private function processNeedsApproval($id, $actor, $settings) {
    if ($actor === 'visitor') {
      if ($settings->moderationEnabled() && $settings->emailAdminReviewNeedsApproval()) {
        $review = $this->getReview($id);
        $this->sendToAdmin($settings, $review, RevwsEmail::ADMIN_NEEDS_APPROVAL);
      }
    }
  }

  private function processReplied($id, $actor, $settings) {
    if ($actor === 'employee') {
      if ($settings->emailAuthorNotifyOnReply()) {
        $review = $this->getReview($id);
        $this->sendToReviewer($settings, $review, RevwsEmail::AUTHOR_REVIEW_REPLIED);
      }
    }
  }

  /**
   * public API
   */
  public function reviewCreated($id, $actor) {
    $this->push('processReviewCreated', $id, $actor);
  }

  public function reviewUpdated($id, $actor) {
    $this->push('processReviewUpdated', $id, $actor);
  }

  public function reviewDeleted($id, $actor) {
    $this->push('processReviewDeleted', $id, $actor);
  }

  public function reviewApproved($id, $actor) {
    $this->push('processReviewApproved', $id, $actor);
  }

  public function needsApproval($id, $actor) {
    $this->push('processNeedsApproval', $id, $actor);
  }

  public function replied($id, $actor) {
    $this->push('processReplied', $id, $actor);
  }

  private function getData(RevwsReview $review, $lang, RevwsEmail $email, Settings $settings) {
    $productData = FrontApp::getProductData($review->id_product, $lang);
    $authorName = $review->display_name;
    if ($review->isCustomer()) {
      $customer = $this->getCustomer($review);
      $customeName = $customer->firstname . ' ' .$customer->lastname;
    }

    return [
      '{product_id}' => $productData['id'],
      '{product_name}' => $productData['name'],
      '{product_image}' => $email->getProductImageUrl($settings, $productData['imageId'], 'home'),
      '{product_url}' => $email->getProductUrl($settings, $productData['id']),
      '{review_id}' => (int)$review->id,
      '{author_type}' => $review->getAuthorType(),
      '{author_email}' => $review->email,
      '{author_name}' => $authorName,
      '{display_name}' => $review->display_name,
      '{title}' => $review->title,
      '{content}' => $this->escapeContent($review->content),
      '{ratings}' => round(Utils::calculateAverage($review->grades), 1),
      '{reply}' => $this->escapeContent($review->reply),
      '{validated}' => $review->validated,
      '{deleted}' => $review->deleted,
      '{approve_link}' => $email->getApproveLink($settings, $review),
      '{reject_link}' => $email->getRejectLink($settings, $review)
    ];
  }

  private function getReviewerEmail(RevwsReview $review) {
    if ($review->isCustomer()) {
      return $this->getCustomer($review)->email;
    } else {
      return $review->email;
    }
  }

  private function getReviewerLanguage(RevwsReview $review) {
    if ($review->isCustomer()) {
      return (int)$this->getCustomer($review)->id_lang;
    } else {
      return (int)$review->getLanguage();
    }
  }

  // utils
  private function push($type, $reviewId, $actor) {
    $this->queue[] = [
      'type' => $type,
      'id' => (int)$reviewId,
      'actor' => $actor
    ];
  }

  private function getReview($id) {
    $id = (int)$id;
    if (! $this->review || !$this->review->id != $id) {
      $review = new RevwsReview($id);
      if (! Validate::isLoadedObject($review)) {
        throw new Exception("Review with id $id not found");
      }
      $review->loadGrades();
      $this->review = $review;
    }
    return $this->review;
  }

  private function getCustomer(RevwsReview $review) {
    if (! $review->isCustomer()) {
      throw new Exception("Review is by guest");
    }
    $id = (int)$review->getAuthorId();
    if (! $this->customer || $this->customer->id != $id) {
      $customer = new Customer($id);
      if (! Validate::isLoadedObject($customer)) {
        throw new Exception("Customer with id $id not found");
      }
      $this->customer = $customer;
    }
    return $this->customer;
  }

  private function sendToReviewer($settings, $review, $template) {
    $to = $this->getReviewerEmail($review);
    if ($to) {
      $lang = $this->getReviewerLanguage($review);
      $email = RevwsEmail::forReview($review, $to, $template, $lang);
      $email->send($this->getData($review, $lang, $email, $settings));
    }
  }

  private function sendToAdmin($settings, $review, $template) {
    $to = $settings->getAdminEmail();
    if ($to) {
      $lang = $settings->getAdminEmailLanguage();
      $email = RevwsEmail::forReview($review, $to, $template, $lang);
      $email->send($this->getData($review, $lang, $email, $settings));
    }
  }

  private function escapeContent($str) {
    if ($str) {
      return nl2br($str);
    }
    return $str;
  }
}
