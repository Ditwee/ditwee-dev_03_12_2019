<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


namespace Revws;

use \Context;
use \RevwsReview;
use \JsonSerializable;

class ReviewList implements JsonSerializable {
  private $id;
  private $conditions;
  private $page;
  private $pageSize;
  private $order;
  private $orderDir;
  private $list = null;

  public function __construct($module, $id, $conditions, $page, $pageSize, $order, $orderDir) {
    $this->module = $module;
    $this->id = $id;
    $this->conditions = $conditions;
    $this->page = $page;
    $this->pageSize = $pageSize;
    $this->order = $order;
    $this->orderDir = $orderDir;
  }

  public function getId() {
    return $this->id;
  }

  public function jsonSerialize() {
    return $this->getData();
  }

  public function isEmpty() {
    $this->ensureLoaded();
    return $this->list['total'] == 0;
  }

  public function getReviews() {
    $this->ensureLoaded();
    return $this->list['reviews'];
  }

  public function getData() {
    $this->ensureLoaded();
    $list = $this->list;
    $list['id'] = $this->id;
    $list['conditions'] = $this->conditions;
    return $list;
  }

  private function ensureLoaded() {
    if (is_null($this->list)) {
      $settings = $this->module->getSettings();
      $visitor = $this->module->getVisitor();
      $permissions = $this->module->getPermissions();
      $options = array_merge($this->conditions, [
        'shop' => Context::getContext()->shop->id,
        'deleted' => false,
        'visitor' => $visitor,
        'validated' => true,
        'pageSize' => $this->pageSize,
        'page' => $this->page,
        'order' => [
          'field' => $this->order,
          'direction' => $this->orderDir
        ]
      ]);
      $list = RevwsReview::findReviews($settings, $options);
      $list['reviews'] = RevwsReview::mapReviews($list['reviews'], $permissions);
      $this->list = $list;
    }
    return $this->list;
  }

  public function getProductEntities($products=[]) {
    $reviews = $this->getReviews();
    $language = $this->module->getVisitor()->getLanguage();
    foreach ($reviews as $review) {
      $productId = (int)$review['productId'];
      if (! isset($products[$productId])) {
        $products[$productId] = FrontApp::getProductData($productId, $language);
      }
    }
    return $products;
  }

  public function getEntitites() {
    return [
      'products' => $this->getProductEntities()
    ];
  }
}
