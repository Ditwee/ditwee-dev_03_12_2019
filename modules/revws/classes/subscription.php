<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


namespace Revws;
use \Db;
use \Customer;
use \Validate;

class Subscription {
  const CONSENT_NOT_REQUIRED = 0;
  const CONSENT_BY_NEWSLETTER = 1;
  const CONSENT_BY_NEWSLETTER_OR_SUBSCRIPTION = 2;
  const CONSET_BY_SUBSCRIPTION = 3;

  private $mode;
  private $customerId;

  private $email = null;
  private $subscribed = null;

  public function __construct(Settings $settings, $customer) {
    if (is_object($customer)) {
      $this->customerId = (int)$customer->id;
      $this->email = $customer->email;
    } else {
      $this->customerId = (int)$customer;
    }
    $this->mode = $settings->getEmailRequestConsentMode();
  }

  public function isSubscribed() {
    if (is_null($this->subscribed)) {
      $sql = "
        SELECT " . self::getSqlExpression($this->mode, 's', 'c') ." AS `subscribed`
        FROM "._DB_PREFIX_."customer c
        LEFT JOIN "._DB_PREFIX_."revws_subscription s ON (s.email = c.email)
        WHERE c.id_customer = ".$this->customerId;
      $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
      if (isset($res[0]['subscribed'])) {
        $this->subscribed = !!$res[0]['subscribed'];
      } else {
        $this->subscribed = false;
      }
    }
    return $this->subscribed;
  }

  public function getEmail() {
    if (is_null($this->email)) {
      $customer = new Customer($this->customerId);
      if (Validate::isLoadedObject($customer)) {
        $this->email = $customer->email;
      }
    }
    return $this->email;
  }

  public function setSubscription($subscribed) {
    $email = $this->getEmail();
    if ($email) {
      $subscribed = $subscribed ? 1 : 0;
      $table = _DB_PREFIX_ . "revws_subscription";
      $time = date('Y-m-d H:i:s');
      $email = pSql($email);
      $sql = "
        INSERT INTO $table(email, subscribed, date_add, date_upd)
        VALUES ('$email', $subscribed, '$time', '$time')
        ON DUPLICATE KEY UPDATE subscribed=VALUES(subscribed), date_upd=values(date_upd)";
      if (Db::getInstance()->execute($sql)) {
        $this->subscribed = !!$subscribed;
        return true;
      }
    }
    return false;
  }

  public static function getSqlExpression($mode, $subscriptionAlias, $customerAlias) {
    switch ($mode) {
      case self::CONSENT_NOT_REQUIRED:
        return "IFNULL($subscriptionAlias.subscribed, 1)";
      case self::CONSENT_BY_NEWSLETTER:
        return "CASE WHEN IFNULL($subscriptionAlias.subscribed, -1) = 0 THEN 0 ELSE IFNULL($customerAlias.newsletter, 0) END";
      case self::CONSENT_BY_NEWSLETTER_OR_SUBSCRIPTION:
        return "COALESCE($subscriptionAlias.subscribed, $customerAlias.newsletter, 0)";
      case self::CONSET_BY_SUBSCRIPTION:
      default:
        return "IFNULL($subscriptionAlias.subscribed, 0)";
    }
  }
}
