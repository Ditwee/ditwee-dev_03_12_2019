<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


use \Revws\Settings;
use \Revws\Visitor;
use \Revws\Permissions;
use \Revws\FrontApp;

class RevwsAllReviewsModuleFrontController extends ModuleFrontController {
  public $module;

  public function __construct() {
    parent::__construct();
    $this->context = Context::getContext();
    $this->php_self = 'module-revws-AllReviews';
  }

  public function initContent() {
    parent::initContent();
    $frontApp = $this->module->getFrontApp();
    $list = $frontApp->addCustomListWidget('all-reviews', [], [
      'reviewStyle' => 'item-with-product'
    ]);
    $this->context->smarty->assign([
      'reviewList' => $list->getData(),
      'visitor' => $frontApp->getVisitorData(),
      'reviewEntities' => $frontApp->getEntitites(),
      'reviewsData' => $frontApp->getStaticData()
    ]);
    $this->setTemplate('all-reviews.tpl');
  }
}
