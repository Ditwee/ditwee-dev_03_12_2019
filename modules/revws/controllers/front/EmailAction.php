<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


use \Revws\Actor;
use \Revws\Settings;
use \Revws\Visitor;
use \Revws\Notifications;
use \Revws\Subscription;

class RevwsEmailActionModuleFrontController extends ModuleFrontController {
  public $module;

  public function __construct() {
    parent::__construct();
    $this->context = Context::getContext();
  }

  public function init() {
    parent::init();
    try {
      $action = $this->getValueOrThrow('action');
      $email = $this->getEmail();
      switch ($action) {
        case 'approve':
          return $this->approval($email->getReview(), true);
        case 'reject':
          return $this->approval($email->getReview(), false);
        case 'get-image':
          return $this->getImage($email);
        case 'open-product':
          return $this->openProduct($email);
        case 'review':
          return $this->reviewProduct($email);
        case 'unsubscribe':
          return $this->unsubscribe($email);
        default:
          throw new Exception("Unknown action $action");
      };
    } catch (Exception $e) {
      $this->context->smarty->assign('error', $e->getMessage());
      $this->setTemplate('email-action-error.tpl');
    } finally {
      Notifications::getInstance()->process($this->module);
    }
  }

  private function approval(RevwsReview $review, $approved) {
    Actor::setActor('employee');
    $review->validated = $approved;
    $review->deleted = !$approved;
    if ($review->save()) {
      $this->context->smarty->assign('review', $review->toJSData($this->module->getPermissions()));
      $this->context->smarty->assign('approved', $approved);
      $this->setTemplate('email-action-approval.tpl');
    } else {
      throw new Exception('Failed to update review');
    }
  }

  private function getImage(RevwsEmail $email) {
    $email->markOpened();
    $imageId = (int)$this->getValueOrThrow('image-id');
    $type = $this->getImageType();
    $file = _PS_PROD_IMG_DIR_ . Image::getImgFolderStatic($imageId) . $imageId . $type . ".jpg";
    if (! file_exists($file)) {
      $file = _PS_PROD_IMG_DIR_ . Image::getImgFolderStatic($imageId) . $imageId . ".jpg";
    }
    if (! file_exists($file)) {
      $lang = Language::getLanguage($email->id_lang)['iso_code'];
      $file = _PS_PROD_IMG_DIR_ . $lang . '-default' . $type . '.jpg';
      if (! file_exists($file)) {
        $file = _PS_PROD_IMG_DIR_ . $lang . 'jpg';
      }
      if (! file_exists($file)) {
        $file = _PS_PROD_IMG_DIR_ . 'en.jpg';
      }
      if (! file_exists($file)) {
        die('file not found');
      }
    }
    header('Content-Type:image/jpeg');
    header('Content-Length: ' . filesize($file));
    readfile($file);
    flush();
    die();
  }

  private function getImageType() {
    $type = Tools::getValue('image-type');
    if ($type) {
      $types = ImageType::getImagesTypes('products');
      foreach ($types as $t) {
        if ($t['name'] === $type) {
          return '-' . $type;
        }
      }
    }
    return null;
  }

  private function reviewProduct(RevwsEmail $email) {
    $email->markClicked();
    $productId = (int)$this->getValueOrThrow('product-id');
    $this->autoLogin($email->getCustomer());
    $url = $this->module->getUrl('MyReviews', [
      'review-product' => $productId ,
      'rating' => Tools::getValue('rating')
    ]);
    Tools::redirect($url);
  }

  private function openProduct(RevwsEmail $email) {
    $email->markClicked();
    $productId = (int)$this->getValueOrThrow('product-id');
    Tools::redirect($this->context->link->getProductLink($productId));
  }

  private function unsubscribe(RevwsEmail $email) {
    $email->markClicked();
    $customer = $email->getCustomer();
    if (! $customer) {
      throw new Exception('Email does not contains customer information');
    }
    $subscription = new Subscription($this->module->getSettings(), $customer);
    $subscription->setSubscription(false);
    $this->setTemplate('email-action-unsubscribed.tpl');
  }

  private function getEmail() {
    $action = $this->getValueOrThrow('action');
    $id = (int)$this->getValueOrThrow('id');
    $hash = $this->getValueOrThrow('secret');
    $email = new RevwsEmail($id);
    if (! Validate::isLoadedObject($email)) {
      throw new Exception('Email not found');
    }
    if (! $email->verifySecretHash($action, $hash, $this->module->getSettings())) {
      throw new Exception('Permission denied');
    }
    return $email;
  }

  private function getValueOrThrow($key) {
    $ret = Tools::getValue($key);
    if (! $ret) {
      throw new Exception("$key not found in request");
    }
    return $ret;
  }

  /* auto-login customer - functionality copied from AuthController */
  private function autoLogin(Customer $customer) {
    if (! $this->context->customer->isLogged() && $customer && $customer->active) {
      Hook::exec('actionBeforeAuthentication');
      $this->context->cookie->id_compare = isset($this->context->cookie->id_compare) ? $this->context->cookie->id_compare: CompareProduct::getIdCompareByIdCustomer($customer->id);
      $this->context->cookie->id_customer = (int) ($customer->id);
      $this->context->cookie->customer_lastname = $customer->lastname;
      $this->context->cookie->customer_firstname = $customer->firstname;
      $this->context->cookie->logged = 1;
      $customer->logged = 1;
      $this->context->cookie->is_guest = $customer->isGuest();
      $this->context->cookie->passwd = $customer->passwd;
      $this->context->cookie->email = $customer->email;

      // Add customer to the context
      $this->context->customer = $customer;

      if (Configuration::get('PS_CART_FOLLOWING') && (empty($this->context->cookie->id_cart) || Cart::getNbProducts($this->context->cookie->id_cart) == 0) && $idCart = (int) Cart::lastNoneOrderedCart($this->context->customer->id)) {
        $this->context->cart = new Cart($idCart);
      } else {
        $idCarrier = (int) $this->context->cart->id_carrier;
        $this->context->cart->id_carrier = 0;
        $this->context->cart->setDeliveryOption(null);
        $this->context->cart->id_address_delivery = (int) Address::getFirstCustomerAddressId((int) ($customer->id));
        $this->context->cart->id_address_invoice = (int) Address::getFirstCustomerAddressId((int) ($customer->id));
      }
      $this->context->cart->id_customer = (int) $customer->id;
      $this->context->cart->secure_key = $customer->secure_key;

      $this->context->cart->save();
      $this->context->cookie->id_cart = (int) $this->context->cart->id;
      $this->context->cookie->write();
      $this->context->cart->autosetProductAddress();
      Hook::exec('actionAuthentication', ['customer' => $this->context->customer]);
      CartRule::autoRemoveFromCart($this->context);
      CartRule::autoAddToCart($this->context);
    }
  }

}
