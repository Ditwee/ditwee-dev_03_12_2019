<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


use \Revws\Settings;
use \Revws\Visitor;
use \Revws\Permissions;
use \Revws\FrontApp;

class RevwsMyReviewsModuleFrontController extends ModuleFrontController {
  public $module;

  public function __construct() {
    parent::__construct();
    $this->context = Context::getContext();
    $this->php_self = 'module-revws-MyReviews';
  }

  public function initContent() {
    parent::initContent();
    if ($this->isLoggedIn()) {
      $this->renderContent($this->module->getVisitor(), $this->module->getPermissions());
    } else {
      Tools::redirect('index.php?controller=authentication&back='.urlencode($this->selfLink()));
    }
  }

  private function renderContent(Visitor $visitor, Permissions $permissions) {
    $frontApp = $this->module->getFrontApp();
    $list = $frontApp->addMyReviewsWidget();
    $params = $this->getParams();
    $reviewProduct = (isset($params['review-product'])) ? (int)$params['review-product'] : null;
    if ($reviewProduct && $permissions->canCreateReview($reviewProduct)) {
      $frontApp->addEntity('product', $reviewProduct);
      $frontApp->addInitAction([
        'type' => 'TRIGGER_CREATE_REVIEW',
        'productId' => $reviewProduct
      ]);
    }

    $this->context->smarty->assign([
      'reviewList' => $list->getData(),
      'visitor' => $frontApp->getVisitorData(),
      'reviewEntities' => $frontApp->getEntitites(),
      'reviewsData' => $frontApp->getStaticData()
    ]);
    $this->setTemplate('my-reviews.tpl');
  }

  private function isLoggedIn() {
    return $this->context->customer->isLogged();
  }

  private function selfLink() {
    return $this->module->getUrl('MyReviews', $this->getParams());
  }

  private function getParams() {
    $params = [];
    if (Tools::getValue('review-product')) {
      $params['review-product'] = (int)Tools::getValue('review-product');
    }
    return $params;
  }

}
