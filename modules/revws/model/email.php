<?php
/**
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*/


use \Revws\Settings;
use \Revws\Utils;
use \Revws\FrontApp;

class RevwsEmail extends ObjectModel {
  const ADMIN_REVIEW_CREATED = 'revws-admin-review-created';
  const ADMIN_REVIEW_UPDATED = 'revws-admin-review-updated';
  const ADMIN_REVIEW_DELETED = 'revws-admin-review-deleted';
  const ADMIN_NEEDS_APPROVAL = 'revws-admin-needs-approval';
  const AUTHOR_THANK_YOU = 'revws-author-thank-you';
  const AUTHOR_REVIEW_DELETED = 'revws-author-review-deleted';
  const AUTHOR_REVIEW_APPROVED = 'revws-author-review-approved';
  const AUTHOR_REVIEW_REPLIED = 'revws-author-review-replied';
  const REVIEW_REQUEST = 'revws-review-request';


  public static $definition = [
    'table'   => 'revws_email',
    'primary' => 'id_email',
    'multilang' => false,
    'fields'  => [
      'id_lang'       => [ 'type' => self::TYPE_INT, 'required' => true ],
      'entity_type'      => [ 'type' => self::TYPE_STRING, 'required' => true ],
      'id_entity'     => [ 'type' => self::TYPE_INT, 'required' => true ],
      'template'      => [ 'type' => self::TYPE_STRING, 'required' => true ],
      'email'         => [ 'type' => self::TYPE_STRING, 'required' => true ],
      'subject'       => [ 'type' => self::TYPE_STRING ],
      'opened'        => [ 'type' => self::TYPE_INT, 'default' => 0 ],
      'clicked'       => [ 'type' => self::TYPE_INT, 'default' => 0 ],
      'error'         => [ 'type' => self::TYPE_STRING, 'required' => false ],
      'date_sent'     => [ 'type' => self::TYPE_DATE ],
      'date_add'      => [ 'type' => self::TYPE_DATE ],
      'date_upd'      => [ 'type' => self::TYPE_DATE ],
    ],
  ];

  public $id_lang;
  public $entity_type;
  public $id_entity;
  public $template;
  public $email;
  public $subject;
  public $opened;
  public $clicked;
  public $error;
  public $date_sent;
  public $date_add;
  public $date_upd;

  private $entity = null;

  public static function forReview(RevwsReview $review, $to, $template, $lang) {
    $email = new RevwsEmail();
    $email->review = $review;
    $email->entity_type = 'review';
    $email->id_entity = (int)$review->id;
    $email->template = $template;
    $email->email = $to;
    $email->id_lang = (int)$lang;
    $email->save();
    return $email;
  }

  public static function forOrder($order, $template) {
    if (is_int($order)) {
      $order = new Order($order);
    }
    if (! Validate::isLoadedObject($order)) {
      throw new Error('Invalid order');
    }
    $customer = new Customer($order->id_customer);
    if (! Validate::isLoadedObject($customer)) {
      throw new Error('Invalid customer');
    }
    $email = new RevwsEmail();
    $email->entity_type = 'order';
    $email->id_entity = (int)$order->id;
    $email->template = $template;
    $email->email = $customer->email;
    $email->id_lang = (int)$customer->id_lang;
    $email->save();
    return $email;
  }

  public function markOpened($save = true) {
    $this->opened++;
    return $save ? $this->save() : true;
  }

  public function markClicked($save = true) {
    $this->clicked++;
    if (! $this->opened) {
      $this->opened++;
    }
    return $save ? $this->save() : true;
  }

  public function send($data) {
    $result = false;
    $error = false;
    try {
      $this->error = null;
      $this->subject = '';
      $lang = (int)$this->id_lang;
      $this->subject = $this->generateSubject($data, $lang);

      // if debug mode is enabled, the whole process can be killed using Tools::dieOrLog
      // and we wouldn't know why -- so let's save it here
      if (defined('_PS_MODE_DEV_') && _PS_MODE_DEV_) {
        $l = Language::getLanguage($lang)['iso_code'];
        $this->error = "Failed to send email {$this->template} in $l language - probably missing email template";
        $this->save();
      }

      $result = Mail::Send($lang, $this->template, $this->subject, $data, $this->email, null, null, null, null, null, Utils::getMailsDirectory(), false);
      if (! $result) {
        $l = Language::getLanguage($lang)['iso_code'];
        throw new Exception("Failed to send email {$this->template} in $l language - probably missing email template");
      }
      $this->error = null;
      $this->date_sent = date('Y-m-d H:i:s');
    } catch (Exception $e) {
      $this->error = $e->getMessage();
      self::log($this->error);
      $result = false;
    }
    $this->save();
    return $result;
  }

  public function getUnsubscribeUrl(Settings $settings) {
    return $this->getUrl($settings, 'unsubscribe', []);
  }

  public function getProductUrl(Settings $settings, $productId) {
    return $this->getUrl($settings, 'open-product', [
      'product-id' => (int)$productId
    ]);
  }

  public function getProductImageUrl(Settings $settings, $imageId, $imageType = null) {
    $params = [
      'image-id' => (int)$imageId
    ];
    if ($imageType) {
      $params['image-type'] = FrontApp::getImageType($imageType);
    }
    return $this->getUrl($settings, 'get-image', $params);
  }

  public function getApproveLink(Settings $settings, RevwsReview $review) {
    return $this->getUrl($settings, 'approve', [
      'review-id' => (int)$review->id
    ]);
  }

  public function getRejectLink(Settings $settings, RevwsReview $review) {
    return $this->getUrl($settings, 'reject', [
      'review-id' => (int)$review->id
    ]);
  }

  public function getRatingUrl(Settings $settings, $productId, $rating) {
    return $this->getUrl($settings, 'review', [
      'product-id' => (int)$productId,
      'rating' => $rating
    ]);
  }

  public function getUrl(Settings $settings, $action, $params) {
    $link = Context::getContext()->link;
    $data['id'] = (int)$this->id;
    $data['action'] = $action;
    $data = array_merge($data, $params);
    $data['secret'] = $this->getSecretHash($action, $settings);
    return $link->getModuleLink('revws', 'EmailAction', $data);
  }

  public function getReview() {
    return ($this->entity_type === 'review') ? $this->getEntity() : null;
  }

  public function getOrder() {
    return ($this->entity_type === 'order') ? $this->getEntity() : null;
  }

  public function getCustomer() {
    if ($this->entity_type === 'order') {
      $order = $this->getEntity();
      $customer = new Customer($order->id_customer);
      if (Validate::isLoadedObject($customer)) {
        return $customer;
      }
    }
    return null;
  }

  public function getEntity() {
    if (is_null($this->entity)) {
      $id = (int)$this->id_entity;
      if ($id) {
        $clazz = ($this->entity_type === 'order') ? 'Order' : 'RevwsReview';
        $obj = new $clazz($id);
        if (Validate::isLoadedObject($obj)) {
          $this->entity = $obj;
        }
      }
    }
    return $this->entity;
  }

  public function getSecretHash($action, Settings $settings) {
    return md5($this->getSignature($action, $settings));
  }

  public function verifySecretHash($action, $hash, Settings $settings) {
    return $this->getSecretHash($action, $settings) === $hash;
  }

  private function getSignature($action, Settings $settings) {
    return (int)$this->id . $action . $settings->getSalt();
  }

  private function generateSubject($data, $lang) {
    switch ($this->template) {
      case self::ADMIN_REVIEW_CREATED:
        return Mail::l('New review has been created', $lang);
      case self::ADMIN_REVIEW_UPDATED:
        return Mail::l('Review has been updated', $lang);
      case self::ADMIN_REVIEW_DELETED:
        return Mail::l('Review has been deleted', $lang);
      case self::ADMIN_NEEDS_APPROVAL:
        return Mail::l('Review needs approval', $lang);
      case self::AUTHOR_THANK_YOU:
        return Mail::l('Thank you for your review', $lang);
      case self::AUTHOR_REVIEW_DELETED:
        return Mail::l('Your review has been deleted', $lang);
      case self::AUTHOR_REVIEW_APPROVED:
        return Mail::l('Your review has been approved', $lang);
      case self::AUTHOR_REVIEW_REPLIED:
        return sprintf(Mail::l('%s replied to your review', $lang), $this->getShopName());
      case self::REVIEW_REQUEST:
        return sprintf(Mail::l('Review your %s', $lang), isset($data['{product_1_name}']) ? $data['{product_1_name}'] : 'product');
      default:
        throw new Exception("Can't generate subject for template '{$this->template}'");
    }
  }

  private static function log($msg) {
    Logger::addLog("Revws module: $msg");
  }

  private function getShopName() {
    return Configuration::get('PS_SHOP_NAME');
  }

  // this function exists only because email translation mechanism is quite stupid.
  // the only purpose is to enumerate all email templates, so the translator can find them
  private function stupidTranslations($lang, $data, $email) {
    Mail::Send($lang, 'revws-admin-needs-approval', Mail::l('Review needs approval', $lang), $data, $email, null, null, null, null, null, Utils::getMailsDirectory(), false);
    Mail::Send($lang, 'revws-admin-review-created', Mail::l('New review has been created', $lang), $data, $email, null, null, null, null, null, Utils::getMailsDirectory(), false);
    Mail::Send($lang, 'revws-admin-review-updated', Mail::l('Review has been updated', $lang), $data, $email, null, null, null, null, null, Utils::getMailsDirectory(), false);
    Mail::Send($lang, 'revws-admin-review-deleted', Mail::l('Review has been deleted', $lang), $data, $email, null, null, null, null, null, Utils::getMailsDirectory(), false);
    Mail::Send($lang, 'revws-author-thank-you', Mail::l('Thank you for your review', $lang), $data, $email, null, null, null, null, null, Utils::getMailsDirectory(), false);
    Mail::Send($lang, 'revws-author-review-deleted', Mail::l('Your review has been deleted', $lang), $data, $email, null, null, null, null, null, Utils::getMailsDirectory(), false);
    Mail::Send($lang, 'revws-author-review-approved', Mail::l('Your review has been approved', $lang), $data, $email, null, null, null, null, null, Utils::getMailsDirectory(), false);
    Mail::Send($lang, 'revws-author-review-replied', sprintf(Mail::l('%s replied to your review', $lang), $this->getShopName()), $data, $email, null, null, null, null, null, Utils::getMailsDirectory(), false);
    Mail::Send($lang, 'revws-review-request', sprintf(Mail::l('Review your %s', $lang), $data['product-1-name']), $data, $email, null, null, null, null, null, Utils::getMailsDirectory(), false);
  }
}
