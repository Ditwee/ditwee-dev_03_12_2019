CREATE TABLE IF NOT EXISTS `PREFIX_revws_email` (
  `id_email`      INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `entity_type`   VARCHAR(30) NOT NULL,
  `id_entity`     INT(11) UNSIGNED NULL,
  `id_lang`       INT(11) UNSIGNED NOT NULL,
  `template`      VARCHAR(127) NOT NULL,
  `email`         VARCHAR(255) NOT NULL,
  `subject`       VARCHAR(255) NOT NULL,
  `opened`        INT(11) UNSIGNED NOT NULL,
  `clicked`       INT(11) UNSIGNED NOT NULL,
  `error`         VARCHAR(512) NULL,
  `date_sent`     DATETIME NULL,
  `date_add`      DATETIME NOT NULL,
  `date_upd`      DATETIME NOT NULL,
  PRIMARY KEY (`id_email`),
  KEY `entity` (`entity_type`, `id_entity`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=CHARSET_TYPE;

CREATE TABLE IF NOT EXISTS `PREFIX_revws_subscription` (
  `email`         varchar(128) NOT NULL,
  `subscribed`    TINYINT(1) NOT NULL,
  `date_add`      DATETIME NOT NULL,
  `date_upd`      DATETIME NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=CHARSET_TYPE;
