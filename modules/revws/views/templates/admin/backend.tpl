{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

<div id="revws-app">
  Please wait...
</div>
<script>
  (function(){
    var started = false;
    var attempt = 0;
    function startRevwsApp() {
      if (started) {
        return;
      }
      if (window.startRevws) {
        started = true;
        startRevws({$revws|json_encode});
      } else {
        attempt++;
        console.log('['+attempt+'] startRevws not loaded yet, waiting...');
        setTimeout(startRevwsApp, 500);
      }
    }
    startRevwsApp();
  })();
</script>
