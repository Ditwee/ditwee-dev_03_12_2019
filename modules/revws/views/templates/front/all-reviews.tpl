{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{capture name=path}
  <span class="navigation-pipe">
    {$navigationPipe}
  </span>
  <span class="navigation_page">
    {l s='Reviews' mod='revws'}
  </span>
{/capture}

<h1>{l s='Reviews' mod='revws'}</h1>
{if $reviewList.total > 0}
<div id="revws-portal-{$reviewList.id}">
  {include
    file='modules/revws/views/templates/widgets/list/list.tpl'
    reviewStyle='item-with-product'
    reviewList=$reviewList
    displayCriteria=$reviewsData.preferences.displayCriteria
    shopName=$reviewsData.shopName
    shape=$reviewsData.theme.shape
    criteria=$reviewsData.criteria
    microdata=false
    allowPaging=true
  }
</div>
{else}
<div class="form-group">{l s='No customer reviews for the moment.' mod='revws'}</div>
{/if}
