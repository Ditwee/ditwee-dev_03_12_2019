{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

<li>
  <a href="{$myReviewsUrl|escape:'html':'UTF-8'}" title="{l s='My reviews' mod='revws'}">
    {if $iconClass}
    <i class="{$iconClass}"></i>
    {/if}
    <span>{l s='My reviews' mod='revws'}</span>
  </a>
</li>
