{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{strip}
<div id="revws-portal-{$reviewList.id}">
  {if $reviewList.reviews}
    {include
      file='modules/revws/views/templates/widgets/list/list.tpl'
      reviewStyle=$reviewStyle
      reviewList=$reviewList
      displayCriteria=$displayCriteria
      shopName=$shopName
      shape=$shape
      criteria=$criteria
      allowPaging=$allowPaging
      microdata=false
    }
  {/if}
</div>
{/strip}
