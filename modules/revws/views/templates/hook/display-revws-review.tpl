{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{strip}
<div class="revws-review-single">
  {include
    file='modules/revws/views/templates/widgets/list/item.tpl'
    review=$review
    shape=$shape
    criteria=$criteria
    shopName=$shopName
    linkToProduct=$linkToProduct
    displayCriteria=$displayCriteria
    microdata=false
  }
</div>
{/strip}
