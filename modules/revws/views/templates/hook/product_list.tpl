{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{if $reviewCount>0 || !$omitEmpty}
<div class="revws-product-list{if $reviewCount == 0} revws-product-list-empty{/if}">
  <a href="{$reviewsUrl}">
    {include file='../widgets/grading/grading.tpl' grade=$grade shape=$shape type='list'}
    <div class="revws-count-text">
      {if $reviewCount == 1}
        {l s='one review' mod='revws'}
      {else}
        {l s='%1$d reviews' sprintf=[$reviewCount] mod='revws'}
      {/if}
    </div>
  </a>
</div>
{/if}
