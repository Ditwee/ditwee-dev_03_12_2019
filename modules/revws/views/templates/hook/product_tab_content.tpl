{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

<div id="idTabRevws">
  {include
    file='modules/revws/views/templates/widgets/product-reviews/product-reviews.tpl'
    reviewList=$reviewList
    productId=$productId
    visitor=$visitor
    reviewsData=$reviewsData
  }
</div>
