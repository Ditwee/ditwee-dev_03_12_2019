{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{strip}
<div class="checkbox">
  <label for="revws-consent">
    <div class="checker" id="uniform-revws-consent">
      <input type="checkbox" name="revws-consent" id="revws-consent" value="1" {if $revwsConsentCheckedByDefault}checked{/if}>
    </div>
    <div class='revws-consent-message'>
      {l s='I agree to receive review requests for purchased products' mod='revws'}
    </div>
  </label>
</div>
{/strip}
