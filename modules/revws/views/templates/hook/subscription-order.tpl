{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{strip}
  <div class="revws-subscribe-section">
    <h2>
      {l s='Please share your experience with other customers' mod='revws'}
    </h2>
    <p>
      {l s="We hope you'll love our products and we would love to hear about your experience. You can share it with other customers by writing a [1]product review[/1]. This can greatly help them to choose only the best merchandise!" tags=['<strong>'] mod='revws'}
    </p>
    <p>
      {l s="%s respects your privacy and we don't want to spam you. So we would like to ask for your [1]permission[/1] to contact you. If you agree, we will send you an email a few days after your order has shipped, asking you for your feedback." tags=['<strong>'] sprintf=[$shop_name] mod='revws'}
    </p>
    <div class="revws-centered">
      <button class='revws-subscribe btn btn-primary' onclick='{$revwsAction}'>
        {l s='Sure, send me feedback email' mod='revws'}
      </button>
      <p class="revws-note">
        {l s="(you can withdraw this consent at any time)" mod='revws'}
      </p>
    </div>
  </div>
{/strip}
