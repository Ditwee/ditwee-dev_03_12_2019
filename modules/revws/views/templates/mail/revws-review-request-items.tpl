{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{foreach from=$products item=product}
<!--[if mso | IE]>
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
<tr>
<td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->
<div style="margin:0 auto;max-width:600px">
  <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0;width:100%" align="center" border="0">
    <tbody>
      <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0;padding:0">
          <!--[if mso | IE]>
          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td style="vertical-align:top;width:300px;">
          <![endif]-->
          <div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%">
            <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
              <tbody>
                <tr>
                  <td style="word-wrap:break-word;font-size:0;padding:10px 25px" align="center">
                    <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0" align="center" border="0">
                      <tbody>
                        <tr>
                          <td style="width:100px">
                            <a href={$product.rate5}>
                              <img alt="{$product.name}" height="auto" src="{$product.image}" style="border:none;border-radius:0;display:block;font-size:13px;outline:0;text-decoration:none;width:100%;height:auto" width="100">
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!--[if mso | IE]>
        </td><td style="vertical-align:top;width:300px;">
        <![endif]-->
        <div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%">
          <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
            <tbody>
              <tr>
                <td style="word-wrap:break-word;font-size:0;padding:10px 25px" align="left">
                  <div style="cursor:auto;color:#919191;font-family:Roboto,Helvetica,sans-serif;font-size:16px;font-weight:300;line-height:24px;text-align:left">
                    <h3 style="font-size:16px;font-weight:700;margin-top:0;margin-bottom:0">{$product.name}</h3>
                    <h4>
                      <a class="rate" style="font-size:20px;font-weight:normal;color:#616161" href={$product.rate5}>
                        {l s='Rate this product' mod='revws'}
                      </a>
                    </h4>
                    <p>
                      <div class="revws-grading">
                        <a href={$product.rate1}>1</a>
                        <a href={$product.rate2}>2</a>
                        <a href={$product.rate3}>3</a>
                        <a href={$product.rate4}>4</a>
                        <a href={$product.rate5}>5</a>
                      </div>
                    </p>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <!--[if mso | IE]>
      </td></tr></table>
      <![endif]-->
    </td>
  </tr>
</tbody>
</table>
</div>
<!--[if mso | IE]>
</td></tr></table>
<![endif]-->


<!--[if mso | IE]>
<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
<tr>
<td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->
<div style="margin:0 auto;max-width:600px">
  <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0;width:100%" align="center" border="0">
    <tbody>
      <tr>
        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0;padding:0">
          <!--[if mso | IE]>
          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td style="vertical-align:top;width:600px;">
          <![endif]-->
          <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%">
            <table role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
              <tbody>
                <tr>
                  <td style="word-wrap:break-word;font-size:0;padding:10px 25px">
                    <p style="font-size:1px;margin:0 auto;border-top:1px solid #e0e0e0;width:100%"></p>
                    <!--[if mso | IE]><table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" style="font-size:1px;margin:0px auto;border-top:1px solid #E0E0E0;width:100%;" width="600"><tr><td style="height:0;line-height:0;"> </td></tr></table><![endif]-->
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!--[if mso | IE]>
        </td></tr></table>
        <![endif]-->
      </td>
    </tr>
  </tbody>
</table>
</div>
<!--[if mso | IE]>
</td></tr></table>
<![endif]-->
{/foreach}
