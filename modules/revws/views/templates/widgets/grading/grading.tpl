{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

<div class="revws-grading revws-grading-{$type}">
  {section name="i" start=0 loop=5 step=1}
    <div class="revws-grade-wrap">
      <svg class="revws-grade revws-grade-{if $grade <= $smarty.section.i.index}off{else}on{/if}" viewBox="{$shape.viewBox}">
        <path d="{$shape.path}" />
      </svg>
    </div>
  {/section}
</div>
