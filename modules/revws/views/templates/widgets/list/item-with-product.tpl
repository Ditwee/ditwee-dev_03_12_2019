{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{assign "product" $reviewEntities.products[$review.productId]}
{strip}
<div class="revws-review-with-product">
  <div>
    <a href="{$product.url}">
      <img src="{$product.image}" alt="{$product.name|escape:'html':'UTF-8'}"></img>
    </a>
  </div>
  <div class="revws-review-wrapper">
    <h2>
      <a href="{$product.url}">{$product.name|escape:'html':'UTF-8'}</a>
    </h2>
    {include
      file='modules/revws/views/templates/widgets/list/item.tpl'
      review=$review
      shopName=$shopName
      criteria=$criteria
      shape=$shape
      displayCriteria=$displayCriteria
      microdata=false
    }
  </div>
</div>
{/strip}
