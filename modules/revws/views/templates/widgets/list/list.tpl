{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{strip}
{if $reviewList}
<div class="revws-review-list">
{foreach from=$reviewList.reviews item=review}
  {if $reviewStyle === 'item'}
    {include
      file='modules/revws/views/templates/widgets/list/item.tpl'
      review=$review
      shopName=$shopName
      shape=$shape
      criteria=$criteria
      displayCriteria=$displayCriteria
      microdata=$microdata
    }
  {else}
    {include
      file='modules/revws/views/templates/widgets/list/item-with-product.tpl'
      review=$review
      shopName=$shopName
      shape=$shape
      criteria=$criteria
      displayCriteria=$displayCriteria
      microdata=$microdata
    }
  {/if}
{/foreach}
</div>
{if $allowPaging && $reviewList.pages > 1}
  {include file='modules/revws/views/templates/widgets/list/paging.tpl' }
{/if}
{/if}
{/strip}
