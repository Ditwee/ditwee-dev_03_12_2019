{*
* Copyright (C) 2017-2018 Petr Hucik <petr@getdatakick.com>
*
* NOTICE OF LICENSE
*
* Licensed under the DataKick Regular License version 1.0
* For more information see LICENSE.txt file
*
* @author    Petr Hucik <petr@getdatakick.com>
* @copyright 2017-2018 Petr Hucik
* @license   Licensed under the DataKick Regular License version 1.0
*}

{strip}
<div id="revws-portal-{$reviewList.id}">
  {if $visitor.productsToReview}
  <h1 class="page-heading">{l s='Could you review these products?' mod='revws'}</h1>
  <div class='revws-review-requests'>
    {foreach from=$visitor.productsToReview item=productId}
      {if $productId@iteration <= $reviewsData.preferences.customerMaxRequests}
      {assign "product" $reviewEntities.products[$productId]}
      <div class='revws-review-request'>
        <img src="{$product.image}" />
        <h3 class='revws-review-request-name'>
          {$product.name|escape:'html':'UTF-8'}
        </h3>
      </div>
      {/if}
    {/foreach}
  </div>
  {/if}
  <h1 class="page-heading">{l s='Your reviews' mod='revws'}</h1>
  {if $reviewList.reviews}
    {include
      file='modules/revws/views/templates/widgets/list/list.tpl'
      reviewStyle='item-with-product'
      reviewList=$reviewList
      displayCriteria=$reviewsData.preferences.displayCriteria
      shopName=$reviewsData.shopName
      shape=$reviewsData.theme.shape
      criteria=$reviewsData.criteria
      microdata=false
      allowPaging=true
    }
  {else}
    <div className="form-group">
    {l s="You haven't written any review yet" mod='revws'}
  </div>
  {/if}
</div>
{/strip}
