{if isset($htmlitems) && $htmlitems}
<div id="htmlcontent_{$hook|escape:'htmlall':'UTF-8'}">
	<ul class="htmlcontent-home clearfix row">
		{foreach name=items from=$htmlitems item=hItem}
			{if $hook == 'left' || $hook == 'right'}
				<li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-12 {if $hItem.fit && $hItem.fit == 1}fit_mobile{/if} {if $hItem.hide && $hItem.hide == 1}hide_mobile{/if}">
			{else}
				<li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-4 {if $hItem.fit && $hItem.fit == 1}fit_mobile{/if} {if $hItem.hide && $hItem.hide == 1}hide_mobile{/if}">
			{/if}
				{if $hItem.url}
					<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
				{/if}
					{if $hItem.title && $hItem.title_use == 1}
						<h2 class="item-title-top">{$hItem.title|escape:'htmlall':'UTF-8'}</h2>
					{/if}
					{if $hItem.image}
						<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img {if $hook == 'left' || $hook == 'right'}img-responsive{/if}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
					{/if}
                        <div class="mask">
                            <div class="content">
                                <h2 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h2>
                                <p>
                                {if $hItem.html}
                                    {$hItem.html}
                                {/if}
                                </p>
                                {if $hItem.url}
                                <span class="btn binfo">{l s='Read More' mod='roybanners'}</span>
                                {/if}
                            </div>
                        </div>
				{if $hItem.url}
					</a>
				{/if}
			</li>
		{/foreach}
	</ul>
</div>
{/if}
