<!-- MODULE Block best sellers -->
<div id="roybestsellers-mini" class="mini_products">
	<h4 class="mini_products_title">
    	<a href="{$link->getPageLink('best-sales')|escape:'html'}" title="{l s='View a top sellers products' mod='roybestsellers'}">{l s='Top sellers' mod='roybestsellers'}</a>
    </h4>
	<div class="mini_products_content">
	{if $best_sellers && $best_sellers|@count > 0}
        <div class="carousel_mini">
			{foreach from=$best_sellers item=product name=myLoop}
            {if $smarty.foreach.myLoop.iteration == 1}<div class="products_box">{/if}
                <div class="products_item clearfix">
				<a href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}" class="products-block-image content_img clearfix">
					<img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html'}" alt="{$product.legend|escape:'html':'UTF-8'}" />
				</a>
				<div class="product-content">
                	<h5>
                    	<a class="product-name" href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}">
                            {$product.name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}
                        </a>
                    </h5>
                    {if isset($roythemes.mini_r) && $roythemes.mini_r == "1"}{hook h='displayProductListReviews' product=$product}{/if}
                    <p class="product-description">{$product.description_short|strip_tags:'UTF-8'|truncate:75:'...'}</p>
                    {if !$PS_CATALOG_MODE}
                        <div class="price-box">
                            <span class="price">{$product.price}</span>
                            {if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}<span class="old-price">{convertPrice price=$product.price_without_reduction}</span>{/if}
                        </div>
                    {/if}
                </div>
                </div>
            {if $smarty.foreach.myLoop.last}
                </div>
            {else}
                {if $smarty.foreach.myLoop.iteration%3 == 0}
                    </div><div class="products_box">
                {/if}
            {/if}
		{/foreach}
        </div>
	{else}
		<p>{l s='No best sellers at this time' mod='roybestsellers'}</p>
	{/if}
	</div>
    {if !$smarty.foreach.myLoop.last && $best_sellers && $best_sellers|@count > 0}
        </div>
    {/if}
</div>
<!-- /MODULE Block best sellers -->