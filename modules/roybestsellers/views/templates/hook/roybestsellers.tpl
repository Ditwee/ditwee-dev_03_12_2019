<!-- MODULE Roy best sellers -->
<div id="best-sellers_block_right" class="block products_block">
	<h4 class="title_block">
    	<a href="{$link->getPageLink('best-sales')|escape:'html'}" title="{l s='View a top sellers products' mod='roybestsellers'}">{l s='Top Sellers' mod='roybestsellers'}</a>
    </h4>
    <div class="block_content products-block">
	{if $best_sellers && $best_sellers|@count > 0}
		<div id="products_top" class="products_slider">
			{foreach from=$best_sellers item=product name=myLoop}
				{if $smarty.foreach.myLoop.iteration == 1}<div class="products_box">{/if}
				<div class="products_item clearfix">
					<a href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}" class="products-block-image content_img clearfix">
						<img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html'}" alt="{$product.legend|escape:'html':'UTF-8'}" />
					</a>
					<div class="product-content">
						<h5>
							<a class="product-name" href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}">
								{$product.name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}
							</a>
						</h5>
						<p class="product-description">{$product.description_short|strip_tags:'UTF-8'|truncate:65:'...'}</p>
						{if !$PS_CATALOG_MODE}
							<div class="price-box">
								<span class="price">{$product.price}</span>
							</div>
						{/if}
					</div>
				</div>
            {if $smarty.foreach.myLoop.last}
                </div>
            {else}
                {if $smarty.foreach.myLoop.iteration%2 == 0}
                    </div><div class="products_box">
                {/if}
            {/if}
		{/foreach}
		</div>
	{else}
		<p>{l s='No best sellers at this time' mod='roybestsellers'}</p>
	{/if}
        <div class="lnk" style="text-align: center">
            <a href="{$link->getPageLink('best-sales')|escape:'html'}" title="{l s='All Best Sellers' mod='roybestsellers'}"  class="btn btn-default button button-small"><span><i class="icon-reorder left"></i>{l s='All Best Sellers' mod='roybestsellers'}</span></a>
        </div>
	</div>
    {if !$smarty.foreach.myLoop.last}
        </div>
    {/if}
</div>
<!-- /MODULE Roy best sellers -->