{if count($currencies) > 1}
	<div class="currencies-block-top">
		<form class="setCurrency" action="{$request_uri}" method="post">
			<div class="current">
				<input type="hidden" name="id_currency" value=""/>
				<input type="hidden" name="SubmitCurrency" value="" />
				<span class="cur-label">{l s='Currency' mod='blockcurrencies'}</span>
				{foreach from=$currencies key=k item=f_currency}
					{if $cookie->id_currency == $f_currency.id_currency}<span class="cur-current">{$f_currency.iso_code}</span>{/if}
				{/foreach}
			</div>
			<ul class="currencies_ul toogle_content">
				{foreach from=$currencies key=k item=f_currency}
					<li {if $cookie->id_currency == $f_currency.id_currency}class="selected"{/if}>
						<a href="javascript:setCurrency({$f_currency.id_currency});" rel="nofollow" title="{$f_currency.name}">
							{$f_currency.name}
						</a>
					</li>
				{/foreach}
			</ul>
		</form>
	</div>
{/if}
<!-- /Block currencies module -->