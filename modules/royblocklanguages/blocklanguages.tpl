<!-- Block languages module -->
{if count($languages) > 1}
	<div class="languages-block-top languages-block">
		{foreach from=$languages key=k item=language name="languages"}
			{if $language.iso_code == $lang_iso}
				
				<div class="current">
				<span class="lang-label">{l s='Language' mod='blocklanguages'}</span>
					<span class="lan-current"><img src="{$img_lang_dir}{$language.id_lang}.jpg" alt="{$language.iso_code}" width="16" height="11" />{$lang_iso|regex_replace:"/\s.*$/":""}</span>
				</div>
			{/if}
		{/foreach}
		<ul class="languages-block_ul toogle_content">
			{foreach from=$languages key=k item=language name="languages"}
				<li {if $language.iso_code == $lang_iso}class="selected"{/if}>
				{if $language.iso_code != $lang_iso}
					{assign var=indice_lang value=$language.id_lang}
					{if isset($lang_rewrite_urls.$indice_lang)}
						<a href="{$lang_rewrite_urls.$indice_lang|escape:'html':'UTF-8'}" title="{$language.name}">
					{else}
						<a href="{$link->getLanguageLink($language.id_lang)|escape:'html':'UTF-8'}" title="{$language.name}">
					{/if}
				{/if}
                            <img src="{$img_lang_dir}{$language.id_lang}.jpg" alt="{$language.iso_code}" width="16" height="11" />
                            <span>{$language.name|regex_replace:"/\s.*$/":""}</span>
				{if $language.iso_code != $lang_iso}
					</a>
				{/if}
				</li>
			{/foreach}
		</ul>
	</div>
{/if}
<!-- /Block languages module -->
