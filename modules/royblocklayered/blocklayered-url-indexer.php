<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/royblocklayered.php');

if (substr(Tools::encrypt('royblocklayered/index'),0,10) != Tools::getValue('token') || !Module::isInstalled('royblocklayered'))
	die('Bad token');

$blockLayered = new RoyBlockLayered();
$cursor = Tools::jsonDecode(Tools::getValue('cursor', '{}'), true);
echo $blockLayered->indexUrl($cursor, (int)Tools::getValue('truncate'));