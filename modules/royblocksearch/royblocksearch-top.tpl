<!-- Block search module TOP -->
<div class="roy_search_call">
	<span class="rs_call_icon"><span class="word">{l s='Search' mod='royblocksearch'}</span></span>
</div>
<div id="search_block_top" class="search_layer">
	<div class="search_close"><span class="search_close_icon"></span></div>
	{if $ajaxsearch}<div class="ajax_note">{l s='Just start typing keyword' mod='royblocksearch'}</div>{/if}
	<form id="searchbox"  {if $ajaxsearch}class="ajax_top"{/if} method="get" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" >
		<input type="hidden" name="controller" value="search" />
		<input type="hidden" name="orderby" value="position" />
		<input type="hidden" name="orderway" value="desc" />
		<input class="search_query form-control" type="text" id="search_query_top" name="search_query" placeholder="{l s='Search' mod='royblocksearch'}" value="{$search_query|escape:'htmlall':'UTF-8'|stripslashes}" />
		<button type="submit" name="submit_search" class="button-search">
			<span></span>
		</button>
	</form>
</div>
<!-- /Block search module TOP -->