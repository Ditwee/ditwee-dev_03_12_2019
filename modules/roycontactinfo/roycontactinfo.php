<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_'))
	exit;
	
class Roycontactinfo extends Module
{
	protected static $contact_fields = array(
		'ROYCONTACTINFOS_COMPANY',
		'ROYCONTACTINFOS_ADDRESS',
		'ROYCONTACTINFOS_ADDRESS2',
		'ROYCONTACTINFOS_PHONE',
		'ROYCONTACTINFOS_PHONE2',
		'ROYCONTACTINFOS_MOB',
		'ROYCONTACTINFOS_FAX',
		'ROYCONTACTINFOS_EMAIL',
	);

	public function __construct()
	{
		$this->name = 'roycontactinfo';
		$this->author = 'RoyThemes';
		$this->tab = 'front_office_features';
		$this->version = '1.0';

		$this->bootstrap = true;
		parent::__construct();	

		$this->displayName = $this->l('Roy Contact information block');
		$this->description = $this->l('This module will allow you to display your contact information in a footer.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		Configuration::updateValue('ROYCONTACTINFOS_COMPANY', Configuration::get('PS_SHOP_NAME'));
		Configuration::updateValue('ROYCONTACTINFOS_ADDRESS', trim(preg_replace('/ +/', ' ', Configuration::get('PS_SHOP_ADDR1').' '.Configuration::get('PS_SHOP_ADDR2')."\n".Configuration::get('PS_SHOP_CODE').' '.Configuration::get('PS_SHOP_CITY')."\n".Country::getNameById(Configuration::get('PS_LANG_DEFAULT'), Configuration::get('PS_SHOP_COUNTRY_ID')))));
		Configuration::updateValue('ROYCONTACTINFOS_ADDRESS2', Configuration::get('PS_SHOP_ADRESS2'));
		Configuration::updateValue('ROYCONTACTINFOS_PHONE', Configuration::get('PS_SHOP_PHONE'));
		Configuration::updateValue('ROYCONTACTINFOS_PHONE2', Configuration::get('PS_SHOP_PHONE2'));
		Configuration::updateValue('ROYCONTACTINFOS_MOB', Configuration::get('PS_SHOP_MOB'));
		Configuration::updateValue('ROYCONTACTINFOS_FAX', Configuration::get('PS_SHOP_FAX'));
		Configuration::updateValue('ROYCONTACTINFOS_EMAIL', Configuration::get('PS_SHOP_EMAIL'));
		$this->_clearCache('roycontactinfo.tpl');
		$this->_clearCache('roycontactinfo_line.tpl');
		$this->_clearCache('roycontactinfo_cu.tpl');
		return (parent::install() && $this->registerHook('header') && $this->registerHook(array('displayFooterLeft', 'displayFooterLine', 'displayContactUsInfo')));
	}

	public function uninstall()
	{
		foreach (Roycontactinfo::$contact_fields as $field)
			Configuration::deleteByName($field);
		return (parent::uninstall());
	}

	public function getContent()
	{
		$html = '';
		if (Tools::isSubmit('submitModule'))
		{	
			foreach (Roycontactinfo::$contact_fields as $field)
				Configuration::updateValue($field, Tools::getValue($field));
			$this->_clearCache('roycontactinfo.tpl');
			$this->_clearCache('roycontactinfo_line.tpl');
			$this->_clearCache('roycontactinfo_cu.tpl');
			$html = $this->displayConfirmation($this->l('Configuration updated'));
		}

		return $html.$this->renderForm();
	}

	public function hookHeader()
	{
		$this->context->controller->addCSS(($this->_path).'roycontactinfo.css', 'all');
	}
	
	public function hookDisplayFooterLeft($params)
	{	
		if (!$this->isCached('roycontactinfo.tpl', $this->getCacheId()))
			foreach (Roycontactinfo::$contact_fields as $field)
				$this->smarty->assign(strtolower($field), Configuration::get($field));
		return $this->display(__FILE__, 'roycontactinfo.tpl', $this->getCacheId());
	}
	public function hookDisplayFooterLine($params)
	{	
		if (!$this->isCached('roycontactinfo_line.tpl', $this->getCacheId()))
			foreach (Roycontactinfo::$contact_fields as $field)
				$this->smarty->assign(strtolower($field), Configuration::get($field));
		return $this->display(__FILE__, 'roycontactinfo_line.tpl', $this->getCacheId());
	}
	public function hookDisplayContactUsInfo($params)
	{	
		if (!$this->isCached('roycontactinfo_cu.tpl', $this->getCacheId()))
			foreach (Roycontactinfo::$contact_fields as $field)
				$this->smarty->assign(strtolower($field), Configuration::get($field));
		return $this->display(__FILE__, 'roycontactinfo_cu.tpl', $this->getCacheId());
	}
	
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Company name'),
						'name' => 'ROYCONTACTINFOS_COMPANY',
					),
					array(
						'type' => 'textarea',
						'label' => $this->l('Address'),
						'name' => 'ROYCONTACTINFOS_ADDRESS',
					),
					array(
						'type' => 'textarea',
						'label' => $this->l('Address Additional'),
						'name' => 'ROYCONTACTINFOS_ADDRESS2',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Phone number'),
						'name' => 'ROYCONTACTINFOS_PHONE',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Phone number additional'),
						'name' => 'ROYCONTACTINFOS_PHONE2',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Mobile Phone'),
						'name' => 'ROYCONTACTINFOS_MOB',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Fax'),
						'name' => 'ROYCONTACTINFOS_FAX',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Email'),
						'name' => 'ROYCONTACTINFOS_EMAIL',
					),
				),
				'submit' => array(
					'title' => $this->l('Save')
				)
			),
		);
		
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitModule';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => array(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		foreach (Roycontactinfo::$contact_fields as $field)
			$helper->tpl_vars['fields_value'][$field] = Tools::getValue($field, Configuration::get($field));
		return $helper->generateForm(array($fields_form));
	}
}