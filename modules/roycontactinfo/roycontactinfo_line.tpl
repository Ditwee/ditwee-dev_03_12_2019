{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if isset($roythemes.footer_lay) && $roythemes.footer_lay == "7"}
<!-- MODULE Roy contact info -->
<div id="roy_contact_infos" class="footer-block col-xs-12">
	<div>
        <ul class="block_content">
            {if $roycontactinfos_phone != ''}
            	<li>
                    <span>{$roycontactinfos_phone|escape:'html':'UTF-8'}</span>
            	</li>
            {/if}
            {if $roycontactinfos_phone2 != ''}
            	<li>
                    <span>{$roycontactinfos_phone2|escape:'html':'UTF-8'}</span>
            	</li>
            {/if}
            {if $roycontactinfos_mob != ''}
                <li>
                    <span>{$roycontactinfos_mob|escape:'html':'UTF-8'}</span>
                </li>
            {/if}
        </ul>
    </div>
</div>
<!-- MODULE Roy contact info -->
{/if}
