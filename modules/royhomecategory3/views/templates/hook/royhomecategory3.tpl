{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<li class="hcc3">
{if isset($products) && $products}
    <div class="home_products_title">
        {if RoyHomeCategory3::displayCategoryId() != 1 && RoyHomeCategory3::displayCategoryId() != 2}
            <a href="{$link->getCategoryLink(RoyHomeCategory3::displayCategoryId(), RoyHomeCategory3::displayCategoryLink())}"><span>{RoyHomeCategory3::displayCategoryName()}</span></a>
        {else}
            <span>{RoyHomeCategory3::displayCategoryName()}</span>
        {/if}
		<h3 class="undertitle">{l s='Check our best products' mod='royhomecategory3'}</h3>
    </div>
    <div class="rv_carousel_container">
	{include file="$tpl_dir./product-list.tpl" class='carousel-home royhomecategory3 tab-pane' id='royhomecategory3'}
    </div>
{else}
<ul id="royhomecategory3" class="carousel-home royhomecategory3 tab-pane">
	<li class="alert alert-info">{l s='No products at this time.' mod='royhomecategory3'}</li>
</ul>
{/if}
</li>