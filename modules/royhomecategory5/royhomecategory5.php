<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class RoyHomeCategory5 extends Module
{
	protected static $cache_products;

	public function __construct()
	{
		$this->name = 'royhomecategory5';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'RoyThemes';
		$this->need_instance = 0;

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Roy Home Custom Category 5');
		$this->description = $this->l('Displays custom category products sliders on your homepage.');
	}

	public function install()
	{
		$this->_clearCache('*');
		Configuration::updateValue('ROY_CC5_NBR', 8);
		Configuration::updateValue('ROY_CC5_CAT', (int)Context::getContext()->shop->getCategory());
		Configuration::updateValue('ROY_CC5_RANDOMIZE', false);

		if (!parent::install()
			|| !$this->registerHook('header')
			|| !$this->registerHook('addproduct')
			|| !$this->registerHook('updateproduct')
			|| !$this->registerHook('deleteproduct')
			|| !$this->registerHook('categoryUpdate')
			|| !$this->registerHook('displayHomeTab')
			|| !$this->registerHook('displayHomeTabContent')
			|| !$this->registerHook('displayMiniProducts')
		)
			return false;

		return true;
	}

	public function uninstall()
	{
		$this->_clearCache('*');

		return parent::uninstall();
	}

	public function getContent()
	{
		$output = '';
		$errors = array();
		if (Tools::isSubmit('submitRoyHomeCategory5'))
		{
			$nbr = Tools::getValue('ROY_CC5_NBR');
			if (!Validate::isInt($nbr) || $nbr <= 0)
			$errors[] = $this->l('The number of products is invalid. Please enter a positive number.');

			$cat = Tools::getValue('ROY_CC5_CAT');
			if (!Validate::isInt($cat) || $cat <= 0)
				$errors[] = $this->l('The category ID is invalid. Please choose an existing category ID.');

			$rand = Tools::getValue('ROY_CC5_RANDOMIZE');
			if (!Validate::isBool($rand))
				$errors[] = $this->l('Invalid value for the "randomize" flag.');
			if (isset($errors) && count($errors))
				$output = $this->displayError(implode('<br />', $errors));
			else
			{
				Configuration::updateValue('ROY_CC5_NBR', (int)$nbr);
				Configuration::updateValue('ROY_CC5_CAT', (int)$cat);
				Configuration::updateValue('ROY_CC5_RANDOMIZE', (bool)$rand);
				Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('royhomecategory5.tpl'));
				$output = $this->displayConfirmation($this->l('Your settings have been updated.'));
			}
		}

		return $output.$this->renderForm();
	}

	public function hookDisplayHeader($params)
	{
		$this->hookHeader($params);
	}

	public function hookHeader($params)
	{
		if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'index')
			$this->context->controller->addCSS(_THEME_CSS_DIR_.'product_list.css');
	}

    public static function displayCategoryName(){
        $category = new Category((int)Configuration::get('ROY_CC5_CAT'), (int)Context::getContext()->language->id);
        return $category->name;
    }
    public static function displayCategoryLink(){
        $category = new Category((int)Configuration::get('ROY_CC5_CAT'), (int)Context::getContext()->language->id);
        return $category->link_rewrite;
    }
    public static function displayCategoryId(){
        $category = new Category((int)Configuration::get('ROY_CC5_CAT'), (int)Context::getContext()->language->id);
        return $category->id;
    }

	public function _cacheProducts()
	{
		if (!isset(RoyHomeCategory5::$cache_products))
		{
			$category = new Category((int)Configuration::get('ROY_CC5_CAT'), (int)Context::getContext()->language->id);
			$nb = (int)Configuration::get('ROY_CC5_NBR');
			if (Configuration::get('ROY_CC5_RANDOMIZE'))
				RoyHomeCategory5::$cache_products = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 8), null, null, false, true, true, ($nb ? $nb : 8));
			else
				RoyHomeCategory5::$cache_products = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 8), 'position');
		}

		if (RoyHomeCategory5::$cache_products === false || empty(RoyHomeCategory5::$cache_products))
			return false;
	}

	public function hookDisplayHomeTab($params)
	{
		if (!$this->isCached('tab.tpl', $this->getCacheId('royhomecategory5-tab')))
			$this->_cacheProducts();

		return $this->display(__FILE__, 'tab.tpl', $this->getCacheId('royhomecategory5-tab'));
	}

	public function hookDisplayHome($params)
	{
		if (!$this->isCached('royhomecategory5.tpl', $this->getCacheId()))
		{
			$this->_cacheProducts();
			$this->smarty->assign(
				array(
					'products' => RoyHomeCategory5::$cache_products,
					'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
					'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
				)
			);
		}

		return $this->display(__FILE__, 'royhomecategory5.tpl', $this->getCacheId());
	}

	public function hookDisplayMiniProducts($params)
	{
		if (!$this->isCached('royhomecategory5-mini.tpl', $this->getCacheId()))
		{
			$this->_cacheProducts();
			$this->smarty->assign(
				array(
					'products' => RoyHomeCategory5::$cache_products,
					'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
                    'smallSize' => Image::getSize(ImageType::getFormatedName('small'))
				)
			);
		}

		return $this->display(__FILE__, 'royhomecategory5-mini.tpl', $this->getCacheId());
	}

	public function hookDisplayHomeTabContent($params)
	{
		return $this->hookDisplayHome($params);
	}

	public function hookAddProduct($params)
	{
		$this->_clearCache('*');
	}

	public function hookUpdateProduct($params)
	{
		$this->_clearCache('*');
	}

	public function hookDeleteProduct($params)
	{
		$this->_clearCache('*');
	}

	public function hookCategoryUpdate($params)
	{
		$this->_clearCache('*');
	}

	public function _clearCache($template, $cache_id = NULL, $compile_id = NULL)
	{
		parent::_clearCache('royhomecategory5.tpl');
		parent::_clearCache('tab.tpl', 'royhomecategory5-tab');
        parent::_clearCache('royhomecategory5-mini.tpl');
	}

	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'description' => $this->l('To add products to your homepage, simply add them to the corresponding product category (default: "Home").'),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Number of products to be displayed'),
						'name' => 'ROY_CC5_NBR',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Set the number of products that you would like to display on homepage (default: 8).'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Category from which to pick products to be displayed'),
						'name' => 'ROY_CC5_CAT',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Choose the category ID of the products that you would like to display on homepage (default: 2 for "Home").'),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Randomly display products'),
						'name' => 'ROY_CC5_RANDOMIZE',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Enable if you wish the products to be displayed randomly (default: no).'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitRoyHomeCategory5';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'ROY_CC5_NBR' => Tools::getValue('ROY_CC5_NBR', (int)Configuration::get('ROY_CC5_NBR')),
			'ROY_CC5_CAT' => Tools::getValue('ROY_CC5_CAT', (int)Configuration::get('ROY_CC5_CAT')),
			'ROY_CC5_RANDOMIZE' => Tools::getValue('ROY_CC5_RANDOMIZE', (bool)Configuration::get('ROY_CC5_RANDOMIZE')),
		);
	}
}
