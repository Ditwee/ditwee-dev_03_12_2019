<?php
/*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
    exit;

include_once _PS_MODULE_DIR_.'royinfoblock/royinfoblockClass.php';

class Royinfoblock extends Module
{
    protected $_html = '';

    public function __construct()
    {
        $this->name = 'royinfoblock';
        $this->tab = 'front_office_features';
        $this->version = '3.0';
        $this->author = 'RoyThemes';
        $this->bootstrap = true;
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Roy Info Block');
        $this->description = $this->l('Adds custom information blocks to your store.');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6.99.99');
    }

    public function install()
    {
        return parent::install() &&
        Configuration::deleteByName('BLOCKCMSINFO_NBBLOCKS') && $this->uninstallDB() && $this->installDB() && Configuration::updateValue('ROYINFOBLOCK_NBBLOCKS', 4) &&
        $this->registerHook('displayInfo') && $this->installFixtures();
    }

    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute('
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'info` (
				`id_info` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_shop` int(10) unsigned DEFAULT NULL,
				PRIMARY KEY (`id_info`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        $return &= Db::getInstance()->execute('
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'info_lang` (
				`id_info` INT UNSIGNED NOT NULL,
				`id_lang` int(10) unsigned NOT NULL ,
				`text` text NOT NULL,
				PRIMARY KEY (`id_info`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;'
        );

        return $return;
    }

    public function uninstall()
    {
        // Delete configuration
        return Configuration::deleteByName('ROYINFOBLOCK_NBBLOCKS') &&
        $this->uninstallDB() &&
        parent::uninstall();
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'info`') && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'info_lang`');
    }

    public function addToDB()
    {
        if (isset($_POST['royblocks']))
        {
            for ($i = 1; $i <= (int)$_POST['royblocks']; $i++)
                Db::getInstance()->execute('
					INSERT INTO `'._DB_PREFIX_.'info` (`text`)
					VALUES ("'.((isset($_POST['info'.$i.'_text']) && $_POST['info'.$i.'_text'] != '') ? pSQL($_POST['info'.$i.'_text']) : '').'")'
                );

            return true;
        }

        return false;
    }

    public function removeFromDB()
    {
        return Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'info`');
    }


    protected function _displayInfoHelp()
    {
        return $this->display(__FILE__, 'infohelp.tpl');
    }

    public function getContent()
    {
        $id_info = (int)Tools::getValue('id_info');

        if (Tools::isSubmit('saveroyinfoblock'))
        {
            if (!Tools::getValue('text_'.(int)Configuration::get('PS_LANG_DEFAULT'), false))
                return $this->html . $this->displayError($this->l('You must fill in all fields.')) . $this->renderForm();
            elseif ($this->processSaveRoyInfo())
                return $this->html . $this->renderList();
            else
                return $this->html . $this->renderForm();
        }
        elseif (Tools::isSubmit('updateroyinfoblock') || Tools::isSubmit('addroyinfoblock'))
        {
            $this->html .= $this->renderForm();
            return $this->html;
        }
        else if (Tools::isSubmit('deleteroyinfoblock'))
        {
            $info = new royinfoblockClass((int)$id_info);
            $info->delete();
            $this->_clearCache('royinfoblock.tpl');
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }
        else
        {
            $this->html .= $this->renderList();
            return $this->html;
        }

    }

    public function processSaveRoyInfo()
    {
        if ($id_info = Tools::getValue('id_info'))
            $info = new royinfoblockClass((int)$id_info);
        else
        {
            $info = new royinfoblockClass();
            if (Shop::isFeatureActive())
            {
                $shop_ids = Tools::getValue('checkBoxShopAsso_configuration');
                if (!$shop_ids)
                {
                    $this->html .= '<div class="alert alert-danger conf error">'.$this->l('You have to select at least one shop.').'</div>';
                    return false;
                }
            }
            else
                $info->id_shop = Shop::getContextShopID();
        }

        $languages = Language::getLanguages(false);
        $text = array();
        foreach ($languages AS $lang)
            $text[$lang['id_lang']] = Tools::getValue('text_'.$lang['id_lang']);
        $info->text = $text;

        if (Shop::isFeatureActive() && !$info->id_shop)
        {
            $saved = true;
            foreach ($shop_ids as $id_shop)
            {
                $info->id_shop = $id_shop;
                $saved &= $info->add();
            }
        }
        else
            $saved = $info->save();

        if ($saved)
            $this->_clearCache('royinfoblock.tpl');
        else
            $this->html .= '<div class="alert alert-danger conf error">'.$this->l('An error occurred while attempting to save.').'</div>';

        return $saved;
    }

    protected function renderForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('New custom CMS block'),
            ),
            'input' => array(
                'id_info' => array(
                    'type' => 'hidden',
                    'name' => 'id_info'
                ),
                'content' => array(
                    'type' => 'textarea',
                    'label' => $this->l('Text'),
                    'lang' => true,
                    'name' => 'text',
                    'cols' => 40,
                    'rows' => 10,
                    'class' => 'rte',
                    'autoload_rte' => true,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            ),
            'buttons' => array(
                array(
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                    'title' => $this->l('Back to list'),
                    'icon' => 'process-icon-back'
                )
            )
        );

        if (Shop::isFeatureActive() && Tools::getValue('id_info') == false)
        {
            $fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso_theme'
            );
        }

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'royinfoblock';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang)
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );

        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'saveroyinfoblock';

        $helper->fields_value = $this->getFormValues();

        return $helper->generateForm(array(array('form' => $fields_form)));
    }

    protected function renderList()
    {
        $this->fields_list = array();
        $this->fields_list['id_info'] = array(
                'title' => $this->l('Block ID'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            );

        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
            $this->fields_list['shop_name'] = array(
                    'title' => $this->l('Shop'),
                    'type' => 'text',
                    'search' => false,
                    'orderby' => false,
                );

        $this->fields_list['text'] = array(
                'title' => $this->l('Block text'),
                'type' => 'text',
                'search' => false,
                'orderby' => false,
            );

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_info';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] = array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new')
        );

        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        $content = $this->getListContent($this->context->language->id);

        return $helper->generateList($content, $this->fields_list);
    }

    protected function getListContent($id_lang = null)
    {
        if (is_null($id_lang))
            $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $sql = 'SELECT r.`id_info`, rl.`text`, s.`name` as shop_name
            FROM `'._DB_PREFIX_.'info` r
            LEFT JOIN `'._DB_PREFIX_.'info_lang` rl ON (r.`id_info` = rl.`id_info`)
            LEFT JOIN `'._DB_PREFIX_.'shop` s ON (r.`id_shop` = s.`id_shop`)
            WHERE `id_lang` = '.(int)$id_lang.' AND (';

        if ($shop_ids = Shop::getContextListShopID())
            foreach ($shop_ids as $id_shop)
                $sql .= ' r.`id_shop` = '.(int)$id_shop.' OR ';

        $sql .= ' r.`id_shop` = 0 )';

        $content = Db::getInstance()->executeS($sql);

        foreach ($content as $key => $value)
            $content[$key]['text'] = substr(strip_tags($value['text']), 0, 200);

        return $content;
    }

    public function getFormValues()
    {
        $fields_value = array();
        $id_info = (int)Tools::getValue('id_info');

        foreach (Language::getLanguages(false) as $lang)
            if ($id_info)
            {
                $info = new royinfoblockClass((int)$id_info);
                $fields_value['text'][(int)$lang['id_lang']] = $info->text[(int)$lang['id_lang']];
            }
            else
                $fields_value['text'][(int)$lang['id_lang']] = Tools::getValue('text_'.(int)$lang['id_lang'], '');

        $fields_value['id_info'] = $id_info;

        return $fields_value;
    }

    public function hookdisplayInfo($params)
    {
        $this->context->controller->addCSS($this->_path.'style.css', 'all');
        if (!$this->isCached('royinfoblock.tpl', $this->getCacheId()))
        {
            $infos = $this->getInfos($this->context->language->id, $this->context->shop->id);
            $this->context->smarty->assign(array('infos' => $infos, 'nbblocks' => count($infos)));
        }

        return $this->display(__FILE__, 'royinfoblock.tpl', $this->getCacheId());
    }

    public function getInfos($id_lang, $id_shop)
    {
        $sql = 'SELECT r.`id_info`, r.`id_shop`, rl.`text`
            FROM `'._DB_PREFIX_.'info` r
            LEFT JOIN `'._DB_PREFIX_.'info_lang` rl ON (r.`id_info` = rl.`id_info`)
            WHERE `id_lang` = '.(int)$id_lang.' AND  `id_shop` = '.(int)$id_shop;

        return Db::getInstance()->executeS($sql);
    }

    public function installFixtures()
    {
        $return = true;
        $tab_texts = array(
            array(
                'text' => '<div class="info-container">
<div class="rt-planetes-phone"> </div>
<h3>Order from your phone</h3>
<p>Phone +01 (234) 55 66 789. Time for calls: 10.00 - 22.00. You can also use contact form for any questions. We are always glad to help you!</p>
</div>'
            ),
            array(
                'text' => '<div class="info-container">
<div class="rt-planetes-secure"> </div>
<h3>Legal and secure service</h3>
<p>Intuitive and secure payment system. We accept a lot of payment methods. You can choose most convenient method and pay safely.</p>
</div>'
            ),
            array(
                'text' => '<div class="info-container">
<div class="rt-planetes-shipping"> </div>
<h3>Free shipping</h3>
<p>If your order total price more than 100$, we will deliver your products to you for free! Some additional info for your customers.</p>
</div>'
            ),
            array(
                'text' => '<div class="info-container">
<div class="rt-planetes-quality"> </div>
<h3>Top products quality</h3>
<p>Our products have top quality on the market. You convinced of it, after we deliver your first order. That is the reason to buy!</p>
</div>',
            ),
            array(
                'text' => '<div class="info-container">
<div class="rt-planetes-loyalty"> </div>
<h3>Loyalty points program</h3>
<p>Do not forget to register your personal account, because every order you earn loyalty points, which can be transform to promo codes!</p>
</div>',
            ),
            array(
                'text' => '<div class="info-container">
<div class="rt-planetes-promo"> </div>
<h3>Promo Codes</h3>
<p>If you have promo code, you can apply it to your order on checkout page. You can apply great discount to your order up to 50%!</p>
</div>'
            )
        );

        $shops_ids = Shop::getShops(true, null, true);
        $return = true;
        foreach ($tab_texts as $tab)
        {
            $info = new royinfoblockClass();
            foreach (Language::getLanguages(false) as $lang)
                $info->text[$lang['id_lang']] = $tab['text'];
            foreach ($shops_ids as $id_shop)
            {
                $info->id_shop = $id_shop;
                $return &= $info->add();
            }
        }

        return $return;
    }
}
