{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="alert alert-info" style="font-size:13px; padding-left:64px;">
<p><strong>{l s='You can change icon, using three icon fonts. Choose from:' mod='royinfoblock'} <a href='http://ionicons.com/' target='_blank'>IonIcons.com</a> {l s='or' mod='royinfoblock'} <a href='http://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>Font Awesome</a></strong></p>

<p>{l s='Go to' mod='royinfoblock'} <strong>{l s='Tools - Source code' mod='royinfoblock'}</strong>{l s='. Then find for example' mod='royinfoblock'} <strong>{l s='<div class="rt-planetes-phone"></div>' mod='royinfoblock'}</strong>{l s=' - change class' mod='royinfoblock'} <strong>{l s='rt-planetes-phone' mod='royinfoblock'}</strong> {l s='to class of icon, that you find by the links above.' mod='royinfoblock'}<br />{l s='Example:' mod='royinfoblock'} <strong>{l s='<div class="ion ion-ios-email-outline"></div>' mod='royinfoblock'}</strong> {l s='or' mod='royinfoblock'} <strong>{l s='<div class="fa fa-shopping-basket"></div>' mod='royinfoblock'}</strong></p>
</div>
