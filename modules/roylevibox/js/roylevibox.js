/* Roy LeviBox */
$(document).ready(function() {
    // Appearance
    $(function(f){
        var element = f('.box-arrow');
        f(window).scroll(function(){
            var sctop = f(this).scrollTop() > 140;
            if (sctop) {
                $('.roy_levibox').css({'padding-bottom':'52px'});
                element.css({'opacity':'0.8', 'bottom':'40px'});
            } else {
                $('.roy_levibox').css({'padding-bottom':'0'});
                element.css({'opacity':'0.4', 'bottom':'-52px'});
            }
        });
    });

    setTimeout(function(){$(window).scroll();}, 1000);

    // Content boxes
    function leviBoxContent()
    {
        var elementClickBox = '.box-current';
        var elementSlideBox =  '.box-content';
        var activeClassBox = 'activebox';

        $(elementClickBox).on('click', function(e){
            e.stopPropagation();
            var boxCont = $(this).next(elementSlideBox);
            if(boxCont.is(':hidden'))
            {
                boxCont.fadeIn('fast');
                $(this).addClass(activeClassBox);
            }
            else
            {
                boxCont.fadeOut('fast');
                $(this).removeClass(activeClassBox);
            }
            $(elementClickBox).not(this).next(elementSlideBox).fadeOut('fast');
            $(elementClickBox).not(this).removeClass(activeClassBox);
            e.preventDefault();
        });

        $(elementSlideBox).on('click', function(e){
            e.stopPropagation();
        });

        $(document).on('click', function(e){
            e.stopPropagation();
            var elementHideBox = $(elementClickBox).next(elementSlideBox);
            $(elementHideBox).fadeOut('fast');
            $(elementClickBox).removeClass('activebox');
        });
    }

    // Up Arrow
    $(".box-arrow").click(function() {
        $("html, body").animate({
            scrollTop: 0 }, {
            duration: 500
        });
        return false;
    });
    $base_dir = 2;

    leviBoxContent();
});
/* /Roy LeviBox */