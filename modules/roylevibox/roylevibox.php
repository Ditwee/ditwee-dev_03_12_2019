<?php
/* RoyThemes Copyright 2015. */

if (!defined('_PS_VERSION_'))
	exit;

class RoyLeviBox extends Module
{
	public function __construct()
	{
		$this->name = 'roylevibox';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'RoyThemes';

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Roy LeviBox');
		$this->description = $this->l('Levitating box on the side with useful icons');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
        return (parent::install() &&
            $this->registerHook('displayHeader') &&
            Configuration::updateValue('BOX_FB', 1) &&
            Configuration::updateValue('BOX_MAIL', 1) &&
            Configuration::updateValue('BOX_ARROW', 1) &&
            Configuration::updateValue('BOX_FB_LAN', 'en_US') &&
            Configuration::updateValue('BOX_ADMIN_MAIL','example@yourdomain.com') &&
            Configuration::updateValue('BOX_FACEBOOK_URL', 'https://www.facebook.com/prestashop') &&
            $this->registerHook('displayLeviBox'));
	}

	public function uninstall()
	{
        Configuration::deleteByName('BOX_FB');
        Configuration::deleteByName('BOX_FB_LAN');
        Configuration::deleteByName('BOX_MAIL');
        Configuration::deleteByName('BOX_ARROW');
        Configuration::deleteByName('BOX_ADMIN_MAIL');
        Configuration::deleteByName('BOX_FACEBOOK_URL');
        $this->_clearCache('roylevibox.tpl');
		return parent::uninstall();
	}

	public function getContent()
	{
        $output = '';
        Context::getContext()->cookie->email;
		if (Tools::isSubmit('submitConfiguration'))
        {
                Configuration::updateValue('BOX_FB', (int)(Tools::getValue('box_fb_sw')));
                Configuration::updateValue('BOX_FB_LAN', Tools::getValue('box_fb_lan', ''));
                Configuration::updateValue('BOX_MAIL', (int)(Tools::getValue('box_mail_sw')));
                Configuration::updateValue('BOX_ARROW', (int)(Tools::getValue('box_arrow_sw')));
                Configuration::updateValue('BOX_ADMIN_MAIL', Tools::getValue('box_admin_mail', ''));
                Configuration::updateValue('BOX_FACEBOOK_URL', Tools::getValue('box_facebook_url', ''));
				$this->_clearCache('roylevibox.tpl');
                $output .= $this->displayConfirmation($this->l('Settings updated'));
        }

        return $output.$this->renderForm();

	}

	/**
	* Returns module content
	*
	* @param array $params Parameters
	* @return string Content
	*/
    public function hookHeader($params)
    {
        if (Configuration::get('PS_CATALOG_MODE'))
            return;
        $this->context->controller->addCSS(($this->_path).'css/roylevibox.css', 'all');
        $this->context->controller->addCSS(($this->_path).'css/contactable.css', 'all');
        $this->context->controller->addJS(($this->_path).'js/roylevibox.js', 'all');
        $this->context->controller->addJS(($this->_path).'js/jquery.contactable.js', 'all');
    }

	public function hookdisplayLeviBox($params)
	{
        if (!$this->isCached('roylevibox.tpl', $this->getCacheId()))
        $this->smarty->assign(
        array(
            'box_fb' => Configuration::get('BOX_FB'),
            'box_fb_lan' => Configuration::get('BOX_FB_LAN'),
            'box_mail' => Configuration::get('BOX_MAIL'),
            'box_arrow' => Configuration::get('BOX_ARROW'),
            'box_admin_mail' => Configuration::get('BOX_ADMIN_MAIL'),
            'box_facebook_url' => Configuration::get('BOX_FACEBOOK_URL'),
            'order_process' => Configuration::get('PS_ORDER_PROCESS_TYPE') ? 'order-opc' : 'order'
        ));

		return $this->display(__FILE__, 'roylevibox.tpl', $this->getCacheId());
	}

	public function renderForm()
	{
        $_fb_languages = array(
            array(
                'id_option' => 'af_ZA',
                'name_option' => 'Afrikaans'
            ),
            array(
                'id_option' => 'sq_AL',
                'name_option' => 'Albanian'
            ),
            array(
                'id_option' => 'ar_AR',
                'name_option' => 'Arabic'
            ),
            array(
                'id_option' => 'hy_AM',
                'name_option' => 'Armenian'
            ),
            array(
                'id_option' => 'ay_BO',
                'name_option' => 'Aymara'
            ),
            array(
                'id_option' => 'az_AZ',
                'name_option' => 'Azeri'
            ),
            array(
                'id_option' => 'eu_ES',
                'name_option' => 'Basque'
            ),
            array(
                'id_option' => 'be_BY',
                'name_option' => 'Belarusian'
            ),
            array(
                'id_option' => 'bn_IN',
                'name_option' => 'Bengali'
            ),
            array(
                'id_option' => 'bs_BA',
                'name_option' => 'Bosnian'
            ),
            array(
                'id_option' => 'bg_BG',
                'name_option' => 'Bulgarian'
            ),
            array(
                'id_option' => 'ca_ES',
                'name_option' => 'Catalan'
            ),
            array(
                'id_option' => 'ck_US',
                'name_option' => 'Cherokee'
            ),
            array(
                'id_option' => 'hr_HR',
                'name_option' => 'Croatian'
            ),
            array(
                'id_option' => 'cs_CZ',
                'name_option' => 'Czech'
            ),
            array(
                'id_option' => 'da_DK',
                'name_option' => 'Danish'
            ),
            array(
                'id_option' => 'nl_NL',
                'name_option' => 'Dutch'
            ),
            array(
                'id_option' => 'nl_BE',
                'name_option' => 'Dutch (Belgi?)'
            ),
            array(
                'id_option' => 'en_PI',
                'name_option' => 'English (Pirate)'
            ),
            array(
                'id_option' => 'en_GB',
                'name_option' => 'English (UK)'
            ),
            array(
                'id_option' => 'en_UD',
                'name_option' => 'English (Upside Down)'
            ),
            array(
                'id_option' => 'en_US',
                'name_option' => 'English (US)'
            ),
            array(
                'id_option' => 'eo_EO',
                'name_option' => 'Esperanto'
            ),
            array(
                'id_option' => 'et_EE',
                'name_option' => 'Estonian'
            ),
            array(
                'id_option' => 'fo_FO',
                'name_option' => 'Faroese'
            ),
            array(
                'id_option' => 'tl_PH',
                'name_option' => 'Filipino'
            ),
            array(
                'id_option' => 'fi_FI',
                'name_option' => 'Finnish'
            ),
            array(
                'id_option' => 'fb_FI',
                'name_option' => 'Finnish (test)'
            ),
            array(
                'id_option' => 'fr_CA',
                'name_option' => 'French (Canada)'
            ),
            array(
                'id_option' => 'fr_FR',
                'name_option' => 'French (France)'
            ),
            array(
                'id_option' => 'gl_ES',
                'name_option' => 'Galician'
            ),
            array(
                'id_option' => 'ka_GE',
                'name_option' => 'Georgian'
            ),
            array(
                'id_option' => 'de_DE',
                'name_option' => 'German'
            ),
            array(
                'id_option' => 'el_GR',
                'name_option' => 'Greek'
            ),
            array(
                'id_option' => 'gn_PY',
                'name_option' => 'Guaran?'
            ),
            array(
                'id_option' => 'gu_IN',
                'name_option' => 'Gujarati'
            ),
            array(
                'id_option' => 'he_IL',
                'name_option' => 'Hebrew'
            ),
            array(
                'id_option' => 'hi_IN',
                'name_option' => 'Hindi'
            ),
            array(
                'id_option' => 'hu_HU',
                'name_option' => 'Hungarian'
            ),
            array(
                'id_option' => 'is_IS',
                'name_option' => 'Icelandic'
            ),
            array(
                'id_option' => 'id_ID',
                'name_option' => 'Indonesian'
            ),
            array(
                'id_option' => 'ga_IE',
                'name_option' => 'Irish'
            ),
            array(
                'id_option' => 'it_IT',
                'name_option' => 'Italian'
            ),
            array(
                'id_option' => 'ja_JP',
                'name_option' => 'Japanese'
            ),
            array(
                'id_option' => 'jv_ID',
                'name_option' => 'Javanese'
            ),
            array(
                'id_option' => 'kn_IN',
                'name_option' => 'Kannada'
            ),
            array(
                'id_option' => 'kk_KZ',
                'name_option' => 'Kazakh'
            ),
            array(
                'id_option' => 'km_KH',
                'name_option' => 'Khmer'
            ),
            array(
                'id_option' => 'tl_ST',
                'name_option' => 'Klingon'
            ),
            array(
                'id_option' => 'ko_KR',
                'name_option' => 'Korean'
            ),
            array(
                'id_option' => 'ku_TR',
                'name_option' => 'Kurdish'
            ),
            array(
                'id_option' => 'la_VA',
                'name_option' => 'Latin'
            ),
            array(
                'id_option' => 'lv_LV',
                'name_option' => 'Latvian'
            ),
            array(
                'id_option' => 'fb_LT',
                'name_option' => 'Leet Speak'
            ),
            array(
                'id_option' => 'li_NL',
                'name_option' => 'Limburgish'
            ),
            array(
                'id_option' => 'lt_LT',
                'name_option' => 'Lithuanian'
            ),
            array(
                'id_option' => 'mk_MK',
                'name_option' => 'Macedonian'
            ),
            array(
                'id_option' => 'mg_MG',
                'name_option' => 'Malagasy'
            ),
            array(
                'id_option' => 'ms_MY',
                'name_option' => 'Malay'
            ),
            array(
                'id_option' => 'ml_IN',
                'name_option' => 'Malayalam'
            ),
            array(
                'id_option' => 'mt_MT',
                'name_option' => 'Maltese'
            ),
            array(
                'id_option' => 'mr_IN',
                'name_option' => 'Marathi'
            ),
            array(
                'id_option' => 'mn_MN',
                'name_option' => 'Mongolian'
            ),
            array(
                'id_option' => 'ne_NP',
                'name_option' => 'Nepali'
            ),
            array(
                'id_option' => 'se_NO',
                'name_option' => 'Northern S?mi'
            ),
            array(
                'id_option' => 'nb_NO',
                'name_option' => 'Norwegian (bokmal)'
            ),
            array(
                'id_option' => 'nn_NO',
                'name_option' => 'Norwegian (nynorsk)'
            ),
            array(
                'id_option' => 'ps_AF',
                'name_option' => 'Pashto'
            ),
            array(
                'id_option' => 'fa_IR',
                'name_option' => 'Persian'
            ),
            array(
                'id_option' => 'pl_PL',
                'name_option' => 'Polish'
            ),
            array(
                'id_option' => 'pt_BR',
                'name_option' => 'Portuguese (Brazil)'
            ),
            array(
                'id_option' => 'pt_PT',
                'name_option' => 'Portuguese (Portugal)'
            ),
            array(
                'id_option' => 'pa_IN',
                'name_option' => 'Punjabi'
            ),
            array(
                'id_option' => 'qu_PE',
                'name_option' => 'Quechua'
            ),
            array(
                'id_option' => 'ro_RO',
                'name_option' => 'Romanian'
            ),
            array(
                'id_option' => 'rm_CH',
                'name_option' => 'Romansh'
            ),
            array(
                'id_option' => 'ru_RU',
                'name_option' => 'Russian'
            ),
            array(
                'id_option' => 'sa_IN',
                'name_option' => 'Sanskrit'
            ),
            array(
                'id_option' => 'sr_RS',
                'name_option' => 'Serbian'
            ),
            array(
                'id_option' => 'zh_CN',
                'name_option' => 'Simplified Chinese (China)'
            ),
            array(
                'id_option' => 'sk_SK',
                'name_option' => 'Slovak'
            ),
            array(
                'id_option' => 'sl_SI',
                'name_option' => 'Slovenian'
            ),
            array(
                'id_option' => 'so_SO',
                'name_option' => 'Somali'
            ),
            array(
                'id_option' => 'es_LA',
                'name_option' => 'Spanish'
            ),
            array(
                'id_option' => 'es_CL',
                'name_option' => 'Spanish (Chile)'
            ),
            array(
                'id_option' => 'es_CO',
                'name_option' => 'Spanish (Colombia)'
            ),
            array(
                'id_option' => 'es_MX',
                'name_option' => 'Spanish (Mexico)'
            ),
            array(
                'id_option' => 'es_ES',
                'name_option' => 'Spanish (Spain)'
            ),
            array(
                'id_option' => 'es_VE',
                'name_option' => 'Spanish (Venezuela)'
            ),
            array(
                'id_option' => 'sw_KE',
                'name_option' => 'Swahili'
            ),
            array(
                'id_option' => 'sv_SE',
                'name_option' => 'Swedish'
            ),
            array(
                'id_option' => 'sy_SY',
                'name_option' => 'Syriac'
            ),
            array(
                'id_option' => 'tg_TJ',
                'name_option' => 'Tajik'
            ),
            array(
                'id_option' => 'ta_IN',
                'name_option' => 'Tamil'
            ),
            array(
                'id_option' => 'tt_RU',
                'name_option' => 'Tatar'
            ),
            array(
                'id_option' => 'te_IN',
                'name_option' => 'Telugu'
            ),
            array(
                'id_option' => 'th_TH',
                'name_option' => 'Thai'
            ),
            array(
                'id_option' => 'zh_HK',
                'name_option' => 'Traditional Chinese (Hong Kong)'
            ),
            array(
                'id_option' => 'zh_TW',
                'name_option' => 'Traditional Chinese (Taiwan)'
            ),
            array(
                'id_option' => 'tr_TR',
                'name_option' => 'Turkish'
            ),
            array(
                'id_option' => 'uk_UA',
                'name_option' => 'Ukrainian'
            ),
            array(
                'id_option' => 'ur_PK',
                'name_option' => 'Urdu'
            ),
            array(
                'id_option' => 'uz_UZ',
                'name_option' => 'Uzbek'
            ),
            array(
                'id_option' => 'vi_VN',
                'name_option' => 'Vietnamese'
            ),
            array(
                'id_option' => 'cy_GB',
                'name_option' => 'Welsh'
            ),
            array(
                'id_option' => 'xh_ZA',
                'name_option' => 'Xhosa'
            ),
            array(
                'id_option' => 'yi_DE',
                'name_option' => 'Yiddish'
            ),
            array(
                'id_option' => 'zu_ZA',
                'name_option' => 'Zulu'
            )
        );

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Facebook box?'),
                        'name' => 'box_fb_sw',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),                    
                    array(
                        'type' => 'select',
                        'label' => $this->l('Language of Facebook content'),
                        'class' => 'fixed-width-xxl',
                        'name' => 'box_fb_lan',                           
                        'options' => array(
                            'query' => $_fb_languages, 
                            'id' => 'id_option', 
                            'name' => 'name_option'  
                            )
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Facebook link'),
                        'name' => 'box_facebook_url',
                        'class' => 'fixed-width-xxl',
                        'desc' => $this->l('Your full facebook page link ( ex. https://facebook.com/roythemes )')
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Mail box?'),
                        'name' => 'box_mail_sw',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Administrator email'),
                        'name' => 'box_admin_mail',
                        'class' => 'fixed-width-xxl',
                        'desc' => $this->l('E-mail that will receive mails through the contact form')
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Up arrow box?'),
                        'name' => 'box_arrow_sw',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    )
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
		$helper->submit_action = 'submitConfiguration';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
            'box_fb_sw' => Tools::getValue('box_fb_sw', Configuration::get('BOX_FB')),
            'box_fb_lan' => Tools::getValue('box_fb_lan', Configuration::get('BOX_FB_LAN')),
            'box_facebook_url' => Tools::getValue('box_facebook_url', Configuration::get('BOX_FACEBOOK_URL')),
            'box_mail_sw' => Tools::getValue('box_mail_sw', Configuration::get('BOX_MAIL')),
            'box_admin_mail' => Tools::getValue('box_admin_mail', Configuration::get('BOX_ADMIN_MAIL')),
            'box_arrow_sw' => Tools::getValue('box_arrow_sw', Configuration::get('BOX_ARROW'))
		);
	}

}


