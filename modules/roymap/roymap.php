<?php
/*
RoyThemes Map block. Do not modify or copy. This module is part of 'Another' theme.
*/

if (!defined('_CAN_LOAD_FILES_'))
	exit;
	
class RoyMap extends Module
{
	public function __construct()
	{
		$this->name = 'roymap';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'RoyThemes';

		$this->bootstrap = true;
		parent::__construct();	

		$this->displayName = $this->l('Roy Map');
		$this->description = $this->l('Allows you to display expandible map block in footer.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	
	public function install()
	{
		return (parent::install() AND 
			$this->registerHook('displayHeader') &&
			$this->registerHook('displayFooterRightMap') &&
			$this->registerHook('displayFooterLeftMap'));
	}
	
	public function uninstall()
	{
		//Delete configuration
		return parent::uninstall();
	}
	
	public function hookDisplayHeader()
	{
		$this->context->controller->addCSS(($this->_path).'css/roymap.css', 'all');
	}
		
	public function hookDisplayFooterRightMap()
	{
		if (!$this->isCached('roymap.tpl', $this->getCacheId()));
		return $this->display(__FILE__, 'roymap.tpl', $this->getCacheId());
	}

	public function hookdisplayFooterLeftMap($params)
	{
		return $this->hookDisplayFooterRightMap($params);
	}
}
