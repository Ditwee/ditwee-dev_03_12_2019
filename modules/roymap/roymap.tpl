{*
RoyThemes Map block. Do not modify or copy. This module is part of "Another" theme.
*}

<div id="roymap" class="roymap">
    {if isset($roythemes.footer_map_en) && $roythemes.footer_map_en == "1"}<div class="map_icon map_switcher"></div>{/if}
	<h4 class="{if isset($roythemes.footer_map_en) && $roythemes.footer_map_en == "0"}map_switcher{/if}">{l s='Find us on map' mod='roymap'}</h4>
    <div class="map_text">{l s='Click on map icon to find our store. Working time 9.00 - 21.00' mod='roymap'}</div>
</div>