<!-- MODULE Block new products -->
<div id="roynewproducts-mini" class="mini_products">
	<h4 class="mini_products_title">
    	<a href="{$link->getPageLink('new-products')|escape:'html'}" title="{l s='New products' mod='blocknewproducts'}">{l s='New products' mod='roynewproducts'}</a>
    </h4>
    <div class="mini_products_content">
        {if $new_products !== false}
            <div class="carousel_mini">
                {foreach from=$new_products item=newproduct name=myLoop}
                {if $smarty.foreach.myLoop.iteration == 1}<div class="products_box">{/if}
                    <div class="products_item clearfix">
                        <a class="products-block-image" href="{$newproduct.link|escape:'html'}" title="{$newproduct.legend|escape:html:'UTF-8'}"><img class="replace-2x img-responsive" src="{$link->getImageLink($newproduct.link_rewrite, $newproduct.id_image, 'small_default')|escape:'html'}" alt="{$newproduct.name|escape:html:'UTF-8'}" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="{$newproduct.link|escape:'html'}" title="{$newproduct.name|escape:html:'UTF-8'}">{$newproduct.name|strip_tags|escape:html:'UTF-8'}</a>
                            </h5>
                            {if isset($roythemes.mini_r) && $roythemes.mini_r == "1"}{hook h='displayProductListReviews' product=$newproduct}{/if}
                        	<p class="product-description">{$newproduct.description_short|strip_tags:'UTF-8'|truncate:75:'...'}</p>
                            {if (!$PS_CATALOG_MODE AND ((isset($newproduct.show_price) && $newproduct.show_price) || (isset($newproduct.available_for_order) && $newproduct.available_for_order)))}
                            	{if isset($newproduct.show_price) && $newproduct.show_price && !isset($restricted_country_mode)}
                                    <div class="price-box">
                                        <span class="price">
                                        	{if !$priceDisplay}{convertPrice price=$newproduct.price}{else}{convertPrice price=$newproduct.price_tax_exc}{/if}
                                        </span>
                                        {if isset($newproduct.specific_prices) && $newproduct.specific_prices && isset($newproduct.specific_prices.reduction) && $newproduct.specific_prices.reduction > 0}<span class="old-price">{convertPrice price=$newproduct.price_without_reduction}</span>{/if}
                                    </div>
                                {/if}
                            {/if}
                        </div>
                    </div>
                {if $smarty.foreach.myLoop.last}
                    </div>
                {else}
                    {if $smarty.foreach.myLoop.iteration%3 == 0}
                        </div><div class="products_box">
                    {/if}
                {/if}
                {/foreach}
            </div>
        {else}
        	<p>&raquo; {l s='Do not allow new products at this time.' mod='roynewproducts'}</p>
        {/if}
    </div>
    {if !$smarty.foreach.myLoop.last && $new_products !== false}
        </div>
    {/if}
</div>
<!-- /MODULE Block new products -->