<?php

if (!defined('_PS_VERSION_'))
	exit;

class RoyPaymentLogo extends Module
{
	public function __construct()
	{
		$this->name = 'roypaymentlogo';
		$this->tab = 'front_office_features';
		$this->version = '2.0';
		$this->author = 'RoyThemes';

		$this->bootstrap = true;
		parent::__construct();	

		$this->displayName = $this->l('Roy Footer payment logos block');
		$this->description = $this->l('Adds a block with payment logos that you accept in footer.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
        return (parent::install() AND Configuration::updateValue('ROY_PAYMENT_LOGO_ID', 1) &&
        $this->registerHook('displayHeader') &&
        $this->registerHook('displayFooterCopyright')
            && Configuration::updateValue('PAY_PAYPAL', 1)
            && Configuration::updateValue('PAY_VISA', 1)
            && Configuration::updateValue('PAY_VISA_EL', 0)
            && Configuration::updateValue('PAY_MASTERCARD', 1)
            && Configuration::updateValue('PAY_MAESTRO', 1)
            && Configuration::updateValue('PAY_CIRRUS', 0)
            && Configuration::updateValue('PAY_DISCOVER', 1)
            && Configuration::updateValue('PAY_WU', 1)
            && Configuration::updateValue('PAY_MB', 1)
            && Configuration::updateValue('PAY_AE', 0)
            && Configuration::updateValue('PAY_SWITCH', 1)
            && Configuration::updateValue('PAY_SOLO', 0)
            && Configuration::updateValue('PAY_ACH', 0)
            && Configuration::updateValue('PAY_JCB', 1)
            && Configuration::updateValue('PAY_2CO', 1)
            && Configuration::updateValue('PAY_DIRECT', 0)
        );
	}

	public function uninstall()
	{
		Configuration::deleteByName('ROY_PAYMENT_LOGO_ID')
            && Configuration::deleteByName('PAY_PAYPAL')
            && Configuration::deleteByName('PAY_VISA')
            && Configuration::deleteByName('PAY_VISA_EL')
            && Configuration::deleteByName('PAY_MASTERCARD')
            && Configuration::deleteByName('PAY_MAESTRO')
            && Configuration::deleteByName('PAY_CIRRUS')
            && Configuration::deleteByName('PAY_DISCOVER')
            && Configuration::deleteByName('PAY_WU')
            && Configuration::deleteByName('PAY_MB')
            && Configuration::deleteByName('PAY_AE')
            && Configuration::deleteByName('PAY_SWITCH')
            && Configuration::deleteByName('PAY_SOLO')
            && Configuration::deleteByName('PAY_ACH')
            && Configuration::deleteByName('PAY_JCB')
            && Configuration::deleteByName('PAY_2CO')
            && Configuration::deleteByName('PAY_DIRECT');
		return parent::uninstall();
	}

	public function getContent()
	{
		$html = '';

		if (Tools::isSubmit('submitConfiguration'))
			if (Validate::isUnsignedInt(Tools::getValue('ROY_PAYMENT_LOGO_ID')))
			{
				Configuration::updateValue('ROY_PAYMENT_LOGO_ID', (int)(Tools::getValue('ROY_PAYMENT_LOGO_ID')));
                Configuration::updateValue('PAY_PAYPAL', (int)(Tools::getValue('PAY_PAYPAL')));
                Configuration::updateValue('PAY_VISA', (int)(Tools::getValue('PAY_VISA')));
                Configuration::updateValue('PAY_VISA_EL', (int)(Tools::getValue('PAY_VISA_EL')));
                Configuration::updateValue('PAY_MASTERCARD', (int)(Tools::getValue('PAY_MASTERCARD')));
                Configuration::updateValue('PAY_MAESTRO', (int)(Tools::getValue('PAY_MAESTRO')));
                Configuration::updateValue('PAY_CIRRUS', (int)(Tools::getValue('PAY_CIRRUS')));
                Configuration::updateValue('PAY_DISCOVER', (int)(Tools::getValue('PAY_DISCOVER')));
                Configuration::updateValue('PAY_WU', (int)(Tools::getValue('PAY_WU')));
                Configuration::updateValue('PAY_MB', (int)(Tools::getValue('PAY_MB')));
                Configuration::updateValue('PAY_AE', (int)(Tools::getValue('PAY_AE')));
                Configuration::updateValue('PAY_SWITCH', (int)(Tools::getValue('PAY_SWITCH')));
                Configuration::updateValue('PAY_SOLO', (int)(Tools::getValue('PAY_SOLO')));
                Configuration::updateValue('PAY_ACH', (int)(Tools::getValue('PAY_ACH')));
                Configuration::updateValue('PAY_JCB', (int)(Tools::getValue('PAY_JCB')));
                Configuration::updateValue('PAY_2CO', (int)(Tools::getValue('PAY_2CO')));
                Configuration::updateValue('PAY_DIRECT', (int)(Tools::getValue('PAY_DIRECT')));
				$this->_clearCache('roypaymentlogo.tpl');
				$html .= $this->displayConfirmation($this->l('The settings have been updated.'));
			}

		$cmss = CMS::listCms($this->context->language->id);

		if (!count($cmss))
			$html .= $this->displayError($this->l('No CMS page is available.'));
		else
			$html .= $this->renderForm();

		return $html;
	}

	/**
	* Returns module content
	*
	* @param array $params Parameters
	* @return string Content
	*/
	public function hookdisplayFooterCopyright($params)
	{
		if (Configuration::get('PS_CATALOG_MODE'))
			return;

		if (!$this->isCached('roypaymentlogo.tpl', $this->getCacheId()))
		{
			if (!Configuration::get('ROY_PAYMENT_LOGO_ID'))
				return;
			$cms = new CMS(Configuration::get('ROY_PAYMENT_LOGO_ID'), $this->context->language->id);
			if (!Validate::isLoadedObject($cms))
				return;
			$this->smarty->assign('cms_payement_logo', $cms);
            $this->smarty->assign(
            array(
                'pay_paypal' => Configuration::get('PAY_PAYPAL'),
                'pay_visa' => Configuration::get('PAY_VISA'),
                'pay_visa_el' => Configuration::get('PAY_VISA_EL'),
                'pay_mastercard' => Configuration::get('PAY_MASTERCARD'),
                'pay_maestro' => Configuration::get('PAY_MAESTRO'),
                'pay_cirrus' => Configuration::get('PAY_CIRRUS'),
                'pay_discover' => Configuration::get('PAY_DISCOVER'),
                'pay_wu' => Configuration::get('PAY_WU'),
                'pay_mb' => Configuration::get('PAY_MB'),
                'pay_ae' => Configuration::get('PAY_AE'),
                'pay_switch' => Configuration::get('PAY_SWITCH'),
                'pay_solo' => Configuration::get('PAY_SOLO'),
                'pay_ach' => Configuration::get('PAY_ACH'),
                'pay_jcb' => Configuration::get('PAY_JCB'),
                'pay_2co' => Configuration::get('PAY_2CO'),
                'pay_direct' => Configuration::get('PAY_DIRECT')
            ));
		}

		return $this->display(__FILE__, 'roypaymentlogo.tpl', $this->getCacheId());
	}

	public function hookHeader($params)
	{
		if (Configuration::get('PS_CATALOG_MODE'))
			return;
		$this->context->controller->addCSS(($this->_path).'roypaymentlogo.css', 'all');
	}
	
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Destination page for the block\'s link'),
						'name' => 'ROY_PAYMENT_LOGO_ID',
						'required' => false,
						'default_value' => (int)$this->context->country->id,
						'options' => array(
							'query' => CMS::listCms($this->context->language->id),
							'id' => 'id_cms',
							'name' => 'meta_title'
						)
					),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display PAYPAL logo?'),
                        'name' => 'PAY_PAYPAL',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display VISA logo?'),
                        'name' => 'PAY_VISA',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display VISA Electron logo?'),
                        'name' => 'PAY_VISA_EL',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display MASTERCARD logo?'),
                        'name' => 'PAY_MASTERCARD',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display MAESTRO logo?'),
                        'name' => 'PAY_MAESTRO',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display CIRRUS logo?'),
                        'name' => 'PAY_CIRRUS',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display DISCOVER logo?'),
                        'name' => 'PAY_DISCOVER',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Western Union logo?'),
                        'name' => 'PAY_WU',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display Money Bookers logo?'),
                        'name' => 'PAY_MB',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display American Express logo?'),
                        'name' => 'PAY_AE',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display SWITCH logo?'),
                        'name' => 'PAY_SWITCH',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display SOLO logo?'),
                        'name' => 'PAY_SOLO',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display ACH logo?'),
                        'name' => 'PAY_ACH',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display JCB logo?'),
                        'name' => 'PAY_JCB',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display 2CO logo?'),
                        'name' => 'PAY_2CO',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display DIRECT DEBIT logo?'),
                        'name' => 'PAY_DIRECT',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('YES')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('NO')
                            )
                        ),
                    )
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitConfiguration';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	
	public function getConfigFieldsValues()
	{		
		return array(
			'ROY_PAYMENT_LOGO_ID' => Tools::getValue('ROY_PAYMENT_LOGO_ID', Configuration::get('ROY_PAYMENT_LOGO_ID')),
            'PAY_PAYPAL' => Tools::getValue('PAY_PAYPAL', Configuration::get('PAY_PAYPAL')),
            'PAY_VISA' => Tools::getValue('PAY_VISA', Configuration::get('PAY_VISA')),
            'PAY_VISA_EL' => Tools::getValue('PAY_VISA_EL', Configuration::get('PAY_VISA_EL')),
            'PAY_MASTERCARD' => Tools::getValue('PAY_MASTERCARD', Configuration::get('PAY_MASTERCARD')),
            'PAY_MAESTRO' => Tools::getValue('PAY_MAESTRO', Configuration::get('PAY_MAESTRO')),
            'PAY_CIRRUS' => Tools::getValue('PAY_CIRRUS', Configuration::get('PAY_CIRRUS')),
            'PAY_DISCOVER' => Tools::getValue('PAY_DISCOVER', Configuration::get('PAY_DISCOVER')),
            'PAY_WU' => Tools::getValue('PAY_WU', Configuration::get('PAY_WU')),
            'PAY_MB' => Tools::getValue('PAY_MB', Configuration::get('PAY_MB')),
            'PAY_AE' => Tools::getValue('PAY_AE', Configuration::get('PAY_AE')),
            'PAY_SWITCH' => Tools::getValue('PAY_SWITCH', Configuration::get('PAY_SWITCH')),
            'PAY_SOLO' => Tools::getValue('PAY_SOLO', Configuration::get('PAY_SOLO')),
            'PAY_ACH' => Tools::getValue('PAY_ACH', Configuration::get('PAY_ACH')),
            'PAY_JCB' => Tools::getValue('PAY_JCB', Configuration::get('PAY_JCB')),
            'PAY_2CO' => Tools::getValue('PAY_2CO', Configuration::get('PAY_2CO')),
            'PAY_DIRECT' => Tools::getValue('PAY_DIRECT', Configuration::get('PAY_DIRECT'))
		);
	}

}


