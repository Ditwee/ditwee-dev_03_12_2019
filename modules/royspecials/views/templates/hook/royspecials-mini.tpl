<div id="roynspecials-mini" class="mini_products">
    <h4 class="mini_products_title">
        <a href="{$link->getPageLink('prices-drop')|escape:'html'}" title="{l s='Big Sale' mod='royspecials'}">{l s='Big Sale!' mod='royspecials'}</a>
    </h4>
    <div class="mini_products_content">
        {if isset($specials) && $specials}
            <div class="carousel_mini">
                {foreach from=$specials item=special name=myLoop}
                {if $smarty.foreach.myLoop.iteration == 1}<div class="products_box">{/if}
                    <div class="products_item clearfix">
                        <a class="products-block-image" href="{$special.link|escape:'html':'UTF-8'}"><img class="replace-2x img-responsive" src="{$link->getImageLink($special.link_rewrite, $special.id_image, 'small_default')|escape:'html':'UTF-8'}" alt="{$special.legend|escape:'html':'UTF-8'}" title="{$special.name|escape:'html':'UTF-8'}" /></a>
                        <div class="product-content">
                            <h5>
                                <a class="product-name" href="{$special.link|escape:'html':'UTF-8'}" title="{$special.name|escape:'html':'UTF-8'}">
                                    {$special.name|escape:'html':'UTF-8'}
                                </a>
                            </h5>
                            {if isset($roythemes.mini_r) && $roythemes.mini_r == "1"}{hook h='displayProductListReviews' product=$special}{/if}
                            {if isset($special.description_short) && $special.description_short}
                                <p class="product-description">
                                    {$special.description_short|strip_tags:'UTF-8'|truncate:140}
                                </p>
                            {/if}
                            <div class="price-box">
                                {if !$PS_CATALOG_MODE}
                                    <span class="price special-price">
                                {if !$priceDisplay}
                                    {displayWtPrice p=$special.price}{else}{displayWtPrice p=$special.price_tax_exc}
                                {/if}
                            </span>
                                    <span class="old-price">
                                {if !$priceDisplay}
                                    {displayWtPrice p=$special.price_without_reduction}{else}{displayWtPrice p=$priceWithoutReduction_tax_excl}
                                    {*LyoHTBOF*}{*DEBUG:ThemeStandard16*}{if Configuration::get('LyoHT_isBlocsSpecials')==1}{if $priceDisplay >= 0 && $priceDisplay <= 1}
                                    <br/>
                                    {displayWtPrice p=($priceDisplay)?$special.price:$special.price_tax_exc}
                                    {*afterPrice*}&nbsp;
                                    {if $priceDisplay}{l s='tax incl.' mod='royspecials'}{else}{l s='tax excl.' mod='royspecials'}{/if}
                                    {*afterTaxTitle*}
                                {/if}{/if}{*LyoHTEOF*}{/if}
                            </span>
                                {/if}
                            </div>
                        </div>
                    </div>
                {if $smarty.foreach.myLoop.last}
                    </div>
                {else}
                    {if $smarty.foreach.myLoop.iteration%3 == 0}
                        </div><div class="products_box">
                    {/if}
                {/if}
                {/foreach}
            </div>
        {else}
            <p>&raquo; {l s='There are no specials at this time.' mod='royspecials'}</p>
        {/if}
    </div>
        {if !$smarty.foreach.myLoop.last && isset($specials) && $specials}
    </div>
{/if}
</div>