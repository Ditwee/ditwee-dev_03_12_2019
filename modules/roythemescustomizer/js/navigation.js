$(document).ready(function(){

    // INSIDE TABS navigation
    $('.nav_inside > li > a').click(function(event){
        event.preventDefault();//stop browser to take action for clicked anchor

        //get displaying tab content jQuery selector
        var active_tab_selector = $('.nav_inside > li.active > a').attr('href');

        //find actived navigation and remove 'active' css
        var actived_nav = $('.nav_inside > li.active');
        actived_nav.removeClass('active');

        //add 'active' css into clicked navigation
        $(this).parents('li').addClass('active');
        localStorage.setItem('lastTabInside', $(this).attr('href')); // Inside local set here

        //hide displaying tab content
        $('.tab-content.active').removeClass('active').addClass('hide');

        //show target tab content
        var target_tab_selector = $(this).attr('href');
        $(target_tab_selector).removeClass('hide');
        $(target_tab_selector).addClass('active');
    });

    // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', $(this).attr('href'));


        // show first inside navigation on click on main
        var first_li_link = $(this).attr('href');
        var first_li_div = $(first_li_link).find('.nav_inside > li:first-child');
        var first_li_target_content = $(first_li_link).find('.nav_inside > li:first-child > a').attr('href');

        $('.tab-content.active').removeClass('active').addClass('hide');
        $('.nav_inside > li.active').removeClass('active');

        $(first_li_div).addClass('active');
        $(first_li_target_content).removeClass('hide').addClass('active');

    });

    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }
    var lastTabInside = localStorage.getItem('lastTabInside');
    if (lastTabInside) {
        $('.tab-content').removeClass('active').addClass('hide');
        $('[href="' + lastTabInside + '"]').tab('show');
        $(lastTabInside).removeClass('hide');
    }

    // Demo select
    $('.demo_apply').click(function() {

        if ($('#select_demo1').is(':checked')) var demo_number = '1';
        if ($('#select_demo2').is(':checked')) var demo_number = '2';
        if ($('#select_demo3').is(':checked')) var demo_number = '3';
        if ($('#select_demo4').is(':checked')) var demo_number = '4';
        if ($('#select_demo5').is(':checked')) var demo_number = '5';
        if ($('#select_demo6').is(':checked')) var demo_number = '6';

        // Base colors massive
        var demo_settings = {
            'body_box_sw': ['1','1','2','1','1','1'],
            'body_te': ['1','1','1','1','1','1'],
            'body_gg': ['15','15','15','15','15','15'],
            'body_bg_repeat': ['0','0','0','0','0','0'],
            'body_bg_position': ['0','0','0','0','0','0'],
            'body_bg_fixed': ['0','0','0','0','0','0'],
            'gradient_scheme': ['1','0','0','0','0','0'],
            'display_gradient': ['1','0','0','0','0','0'],
            'body_bg_pattern': ['0','0','0','0','0','0'],
            'font_size_pp': ['36','36','36','36','36','36'],
            'font_size_bb': ['22','40','22','22','18','22'],
            'font_size_body': ['13','13','13','13','13','13'],
            'font_size_head': ['18','18','18','18','20','18'],
            'font_size_menu': ['16','14','14','14','14','14'],
            'font_size_buttons': ['14','14','14','14','14','14'],
            'font_size_price': ['18','18','18','18','15','15'],
            'font_size_prod': ['24','24','24','24','24','30'],
            'font_size_de': ['13','13','13','13','13','13'],
            'font_size_pn': ['15','15','15','15','14','14'],
            'font_size_hp': ['26','0','18','22','18','22'],
            'nc_font_size_hpd': ['16','16','16','16','14','16'],
            'brand_p_s': ['1','1','1','1','1','1'],
            'brand_p_p': ['1','1','1','1','1','0'],
            'mini_lay': ['4','4','4','4','4','4'],
            'mini_pds': ['1','1','1','1','0','1'],
            'mini_r': ['1','1','1','0','1','1'],
            'mini_b': ['0','0','0','1','1','0'],
            'mini_bw': ['1','1','1','2','2','2'],
            'g_lay': ['1','1','2','1','1','1'],
            'g_tp': ['150','150','150','150','150','150'],
            'g_bp': ['150','150','150','150','150','150'],
            'cs_base': ['1','1','1','1','1','1'],
            'breadcrumb': ['1','1','1','1','1','1'],
            'b_layout': ['1','3','6','1','5','2'],
            'b_padding': ['44','0','50','44','0','0'],
            'g_position_gridster': ['li_p,li_e1,li_e2,li_e3,li_m,li_i,li_b,li_h,li_f1,li_f2,li_f3','li_p,li_e1,li_e2,li_e3,li_m,li_i,li_b,li_h,li_f1,li_f2,li_f3','li_p,li_e1,li_e2,li_e3,li_m,li_i,li_b,li_h,li_f1,li_f2,li_f3','li_p,li_e1,li_e2,li_e3,li_m,li_i,li_b,li_h,li_f1,li_f2,li_f3','li_p,li_e1,li_e2,li_e3,li_m,li_i,li_b,li_h,li_f1,li_f2,li_f3','li_p,li_e1,li_e2,li_e3,li_m,li_i,li_b,li_h,li_f1,li_f2,li_f3'],
            'g_position_p': ['1','1','1','1','1','0'],
            'g_position_m': ['2','2','2','2','2','0'],
            'g_position_i': ['3','3','3','3','3','0'],
            'g_position_b': ['4','4','4','4','4','0'],
            'g_position_h': ['5','5','5','5','5','0'],
            'g_hp1_sw': ['1','1','1','1','2','1'],
            'g_hp1_gg': ['15','15','15','15','135','15'],
            'g_hp1_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_hp1_im_bg_position': ['0','0','0','0','0','0'],
            'g_hp1_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_hp1_te': ['0','1','1','1','1','0'],
            'g_hp1_top': ['0','0','0','0','0','0'],
            'g_hp1_bot': ['0','0','0','0','0','0'],
            'g_hp2_sw': ['0','1','1','1','15','15'],
            'g_hp2_gg': ['15','15','15','15','15','15'],
            'g_hp2_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_hp2_im_bg_position': ['0','0','0','0','0','0'],
            'g_hp2_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_hp2_te': ['1','1','1','1','1','1'],
            'g_hp2_top': ['0','0','0','0','0','0'],
            'g_hp2_bot': ['0','0','0','0','0','30'],
            'g_hp3_sw': ['1','1','1','1','1','1'],
            'g_hp3_gg': ['15','15','15','15','15','15'],
            'g_hp3_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_hp3_im_bg_position': ['0','0','0','0','0','0'],
            'g_hp3_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_hp3_te': ['1','1','1','1','1','1'],
            'g_hp3_top': ['0','0','0','0','20','20'],
            'g_hp3_bot': ['0','0','0','40','20','20'],
            'g_hp4_sw': ['1','1','1','1','1','1'],
            'g_hp4_gg': ['15','15','15','15','15','15'],
            'g_hp4_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_hp4_im_bg_position': ['0','0','0','0','0','0'],
            'g_hp4_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_hp4_te': ['1','1','1','1','1','9'],
            'g_hp4_top': ['25','40','0','0','0','0'],
            'g_hp4_bot': ['25','25','0','0','0','90'],
            'g_hp5_sw': ['1','2','2','1','1','2'],
            'g_hp5_gg': ['7','7','7','7','7','7'],
            'g_hp5_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_hp5_im_bg_position': ['0','0','0','0','0','0'],
            'g_hp5_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_hp5_te': ['0','1','1','1','1','0'],
            'g_hp5_top': ['100','100','0','0','0','0'],
            'g_hp5_bot': ['80','80','0','0','0','0'],
            'g_hp6_sw': ['1','1','1','4','1','1'],
            'g_hp6_gg': ['15','15','15','15','15','15'],
            'g_hp6_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_hp6_im_bg_position': ['0','0','0','0','0','0'],
            'g_hp6_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_hp6_te': ['1','1','1','4','1','1'],
            'g_hp6_top': ['20','40','0','0','0','0'],
            'g_hp6_bot': ['0','0','0','0','0','0'],
            'g_mc_sw': ['1','1','1','1','1','1'],
            'g_mc_gg': ['15','15','15','15','15','15'],
            'g_mc_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_mc_im_bg_position': ['0','0','0','0','0','0'],
            'g_mc_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_mc_te': ['1','1','1','1','1','1'],
            'g_mc_top': ['0','0','0','0','20','0'],
            'g_mc_bot': ['0','0','0','0','0','0'],
            'g_mc_w': ['2','1','1','1','1','2'],
            'g_bc_sw': ['1','1','1','1','1','1'],
            'g_bc_gg': ['15','15','15','15','15','15'],
            'g_bc_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_bc_im_bg_position': ['0','0','0','0','0','0'],
            'g_bc_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_bc_te': ['1','1','1','1','1','1'],
            'g_bc_top': ['0','0','0','0','0','0'],
            'g_bc_bot': ['0','0','0','0','0','0'],
            'g_bc_w': ['0','1','0','0','0','0'],
            'g_fs1_sw': ['1','1','1','1','1','1'],
            'g_fs1_gg': ['15','15','15','15','15','15'],
            'g_fs1_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_fs1_im_bg_position': ['0','0','0','0','0','0'],
            'g_fs1_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_fs1_te': ['1','1','1','1','1','1'],
            'g_fs1_top': ['0','0','0','0','0','0'],
            'g_fs1_bot': ['0','0','0','0','0','0'],
            'g_fs1_w': ['2','0','1','1','1','2'],
            'g_fs2_sw': ['1','1','1','1','1','1'],
            'g_fs2_gg': ['15','15','15','15','15','15'],
            'g_fs2_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_fs2_im_bg_position': ['0','0','0','0','0','0'],
            'g_fs2_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_fs2_te': ['1','1','1','1','1','1'],
            'g_fs2_top': ['0','0','0','0','0','0'],
            'g_fs2_bot': ['0','0','0','0','0','0'],
            'g_fs2_w': ['2','1','1','1','1','1'],
            'g_fs3_sw': ['1','1','1','1','1','1'],
            'g_fs3_gg': ['15','15','15','15','15','15'],
            'g_fs3_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_fs3_im_bg_position': ['0','0','0','0','0','0'],
            'g_fs3_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_fs3_te': ['1','1','1','1','1','1'],
            'g_fs3_top': ['0','0','0','0','0','0'],
            'g_fs3_bot': ['0','0','0','0','0','0'],
            'g_fs3_w': ['2','1','1','1','1','1'],
            'g_hp1_w': ['2','2','2','2','2','2'],
            'g_hp2_w': ['3','1','1','1','1','2'],
            'g_hp3_w': ['2','2','2','2','1','2'],
            'g_hp4_w': ['2','2','2','2','2','2'],
            'g_hp5_w': ['2','1','1','1','1','2'],
            'g_hp6_w': ['2','1','1','1','1','2'],
            'g_foot_w': ['2','1','1','1','1','2'],
            'g_pro_w': ['2','1','1','1','1','2'],
            'g_blog_w': ['2','1','1','1','1','2'],
            'g_info_w': ['2','1','1','1','1','2'],
            'g_mini_w': ['2','2','2','2','2','2'],
            'g_bra_w': ['2','1','1','1','1','2'],
            'g_b_w': ['2','2','1','1','1','2'],
            'g_c_w': ['2','2','1','1','1','2'],
            'g_op1_sw': ['1','4','1','1','2','1'],
            'g_op1_gg': ['15','15','15','15','178','15'],
            'g_op1_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_op1_im_bg_position': ['0','0','0','0','0','0'],
            'g_op1_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_op1_te': ['1','5','1','1','1','5'],
            'g_op1_top': ['0','200','0','0','190','160'],
            'g_op1_bot': ['0','80','0','0','30','40'],
            'g_op2_sw': ['1','1','0','1','1','1'],
            'g_op2_gg': ['15','15','15','15','15','15'],
            'g_op2_im_bg_repeat': ['0','0','0','0','0','0'],
            'g_op2_im_bg_position': ['0','0','0','0','0','0'],
            'g_op2_im_bg_fixed': ['0','0','0','0','0','0'],
            'g_op2_te': ['1','1','1','1','0','1'],
            'g_op2_top': ['30','60','30','30','30','60'],
            'g_op2_bot': ['30','30','0','30','30','30'],
            'f_headings': ['Poppins','Montserrat','Montserrat','Montserrat','Montserrat','Montserrat'],
            'f_buttons': ['Poppins','Montserrat,','Montserrat','Montserrat','Montserrat','Montserrat'],
            'f_text': ['Montserrat','Montserrat','Montserrat','Montserrat','Montserrat','Montserrat'],
            'f_price': ['Montserrat','Montserrat','Montserrat','Montserrat','Montserrat','Montserrat'],
            'f_pn': ['Montserrat','Montserrat','Montserrat','Montserrat','Montserrat','Montserrat'],
            'latin_ext': ['0','0','0','0','0','0'],
            'cyrillic': ['0','0','0','0','0','0'],
            'nc_f_weight': ['0','0','0','0','0','0'],
            'b_hover': ['1','2','1','1','1','1'],
            'i_b_width': ['2','2','2','2','2','2'],
            'i_b_radius': ['4','4','4','4','4','0'],
            'nc_fw_menu': ['600','600','600','600','600','600'],
            'nc_fw_heading': ['600','600','600','600','600','400'],
            'nc_fw_but': ['600','600','600','600','600','600'],
            'nc_fw_hp': ['600','600','600','600','600','400'],
            'nc_fw_bb': ['600','400','700','400','400','400'],
            'nc_fw_pn': ['600','400','400','400','400','400'],
            'nc_fw_add': ['600','600','600','600','600','600'],
            'nc_fw_f': ['600','600','600','600','600','600'],
            'nc_fw_ct': ['600','600','600','600','600','600'],
            'nc_b_speed': ['240','240','300','300','180','180'],
            'nc_b_radius': ['4','4','4','4','4','1'],
            'nc_b_b_width': ['2','2','2','2','2','2'],
            'nc_ls': ['0.2','0.3','0.4','0.3','0.3','0.3'],
            'nc_ls_h': ['0.2','1.2','0.4','1.2','0.3','0.1'],
            'nc_ls_m': ['0.5','0.4','0.4','0.4','0.6','0.4'],
            'nc_ls_p': ['0.2','0.2','0.2','0.2','0.2','0.1'],
            'nc_ls_t': ['0.2','0.9','0.9','0.9','0.3','0.4'],
            'nc_ls_b': ['0.5','0.1','0.1','0.1','0.1','0.1'],
            'cl_popup_b': ['0','0','1','0','0','0'],
            'cl_popup_b1': ['0','0','3','0','0','0'],
            'cl_popup_b2': ['0','0','0','0','0','0'],
            'cl_popup_b3': ['0','0','0','0','0','0'],
            'cl_popup_b4': ['0','0','0','0','0','0'],
            'cl_popup_br': ['0','0','0','0','0','0'],
            'cl_popup_shadow': ['1','1','1','1','1','1'],
            'cl_popup_wel': ['1','1','1','1','1','1'],
            'cl_popup_name': ['1','1','1','1','1','1'],
            'pa_pro': ['3','3','3','3','3','3'],
            'pa_mini': ['3','3','3','3','1','3'],
            'pa_info': ['3','3','3','3','3','2'],
            'pa_bra': ['3','3','3','3','3','3'],
            'pa_blog': ['3','3','3','3','3','3'],
            'pa_ban_top': ['1','1','1','1','1','1'],
            'pa_ban_pro': ['3','1','1','1','1','1'],
            'pa_ban_mini': ['1','1','1','1','1','1'],
            'pa_ban_info': ['1','1','1','1','1','1'],
            'pa_ban_bra': ['1','1','1','1','1','1'],
            'pa_ban_home': ['1','1','1','1','1','1'],
            'pa_ban_foot': ['1','1','1','1','1','1'],
            'pa_ban_foott': ['1','1','1','1','1','1'],
            'pa_ban_footb': ['1','1','1','1','1','1'],
            'pa_ban_s1': ['1','1','1','1','1','1'],
            'pa_ban_s2': ['1','1','1','1','1','1'],
            'pa_ban_s3': ['1','1','1','1','1','1'],
            'ban_row_top': ['1','3','1','1','1','1'],
            'ban_spa_top': ['1','1','1','1','1','1'],
            'ban_wid_top': ['1','1','1','1','1','1'],
            'ban_ts_top': ['0','80','0','0','0','80'],
            'ban_bs_top': ['0','0','0','0','0','0'],
            'ban_row_pro': ['3','1','1','1','1','1'],
            'ban_spa_pro': ['1','2','2','2','2','2'],
            'ban_wid_pro': ['2','1','1','1','2','2'],
            'ban_ts_pro': ['60','20','20','60','40','20'],
            'ban_bs_pro': ['40','20','0','90','0','20'],
            'ban_row_mini': ['1','1','1','1','1','1'],
            'ban_spa_mini': ['1','1','1','2','1','1'],
            'ban_wid_mini': ['2','2','2','2','1','2'],
            'ban_ts_mini': ['50','0','0','0','0','0'],
            'ban_bs_mini': ['80','70','80','80','50','70'],
            'ban_row_info': ['3','3','3','3','3','2'],
            'ban_spa_info': ['2','2','2','2','2','2'],
            'ban_wid_info': ['2','2','2','2','2','2'],
            'ban_ts_info': ['10','10','10','10','10','0'],
            'ban_bs_info': ['80','80','80','80','80','150'],
            'ban_row_bra': ['1','1','1','1','1','1'],
            'ban_spa_bra': ['1','1','1','1','1','1'],
            'ban_wid_bra': ['1','1','1','1','1','1'],
            'ban_ts_bra': ['30','30','30','30','30','30'],
            'ban_bs_bra': ['0','0','0','0','0','0'],
            'ban_row_home': ['1','1','1','2','1','1'],
            'ban_spa_home': ['1','1','1','2','1','1'],
            'ban_wid_home': ['1','1','1','1','1','1'],
            'ban_ts_home': ['30','30','30','120','30','30'],
            'ban_bs_home': ['90','90','90','120','90','90'],
            'ban_row_foot': ['1','1','1','1','1','1'],
            'ban_spa_foot': ['1','1','1','1','1','1'],
            'ban_wid_foot': ['1','1','1','1','1','1'],
            'ban_ts_foot': ['30','30','30','30','30','30'],
            'ban_bs_foot': ['0','0','0','0','0','0'],
            'ban_ts_left': ['0','0','0','0','0','0'],
            'ban_bs_left': ['0','0','0','0','0','0'],
            'ban_ts_right': ['0','0','0','0','0','0'],
            'ban_bs_right': ['0','0','0','0','0','0'],
            'ban_row_foott': ['1','1','1','1','1','1'],
            'ban_spa_foott': ['1','1','1','1','1','1'],
            'ban_wid_foott': ['1','1','1','1','1','1'],
            'ban_ts_foott': ['0','0','0','0','0','0'],
            'ban_bs_foott': ['50','50','50','50','50','50'],
            'ban_row_footb': ['1','1','1','1','1','1'],
            'ban_spa_footb': ['1','1','1','1','1','1'],
            'ban_wid_footb': ['1','1','1','1','1','1'],
            'ban_ts_footb': ['30','30','30','30','30','30'],
            'ban_bs_footb': ['0','0','0','0','0','0'],
            'ban_row_s1': ['1','1','1','1','1','1'],
            'ban_spa_s1': ['1','1','1','1','1','1'],
            'ban_wid_s1': ['1','1','1','1','1','1'],
            'ban_ts_s1': ['0','0','0','0','0','0'],
            'ban_bs_s1': ['0','0','0','0','0','0'],
            'ban_row_s2': ['1','1','1','1','1','1'],
            'ban_spa_s2': ['1','1','1','1','1','1'],
            'ban_wid_s2': ['1','1','1','1','1','1'],
            'ban_ts_s2': ['0','0','0','0','0','0'],
            'ban_bs_s2': ['0','0','0','0','0','0'],
            'ban_row_s3': ['1','1','1','1','1','1'],
            'ban_spa_s3': ['1','1','1','1','1','1'],
            'ban_wid_s3': ['1','1','1','1','1','1'],
            'ban_ts_s3': ['0','0','0','0','0','0'],
            'ban_bs_s3': ['0','0','0','0','0','0'],
            'info_lay': ['3','1','3','3','3','1'],
            'info_row': ['3','3','3','3','3','4'],
            'info_iconbgs': ['0','1','0','0','1','0'],
            'logo_normal': ['png','png','png','png','png','png'],
            'logo_trans': ['png','png','png','png','png','png'],
            'logo_sticky': ['png','png','png','png','png','png'],
            'logo_footer': ['png','png','png','png','png','png'],
            'hp_lay': ['4','4','3','2','3','1'],
            'm_link_bgs': ['0','0','0','0','1','0'],
            'nc_m_under': ['1','0','0','0','0','1'],
            'nc_mh_width': ['0','1','1','1','1','0'],
            'nc_mo_width': ['0','1','1','1','1','0'],
            'nc_m_align': ['1','1','1','1','1','3'],
            'nc_m_br': ['0','0','0','0','4','0'],
            'm_popup_b': ['1','1','1','1','1','1'],
            'm_popup_b1': ['1','1','3','1','1','1'],
            'm_popup_b2': ['1','1','0','1','1','1'],
            'm_popup_b3': ['1','1','0','1','1','1'],
            'm_popup_b4': ['1','1','0','1','1','1'],
            'm_popup_br': ['0','0','0','0','0','0'],
            'submenu_shadow': ['1','1','1','1','1','1'],
            'search_lay': ['1','2','1','2','1','1'],
            'levi_position': ['2','2','2','2','2','2'],
            'levi_br': ['3','0','0','3','0','30'],
            'sidebar_bg': ['0','0','0','1','0','1'],
            'sidebar_title': ['0','0','0','0','0','0'],
            'sidebar_title_b': ['1','1','1','1','0','1'],
            'sidebar_title_br': ['0','0','0','0','0','2'],
            'sidebar_title_b1': ['0','0','0','0','0','0'],
            'sidebar_title_b2': ['0','4','0','8','0','0'],
            'sidebar_title_b3': ['2','0','4','0','2','2'],
            'sidebar_title_b4': ['0','10','0','8','0','0'],
            'sidebar_block_content_qbg': ['0','0','0','0','0','0'],
            'sidebar_content_b': ['0','1','1','1','1','0'],
            'sidebar_content_b1': ['1','2','0','2','2','0'],
            'sidebar_content_b2': ['1','2','2','2','2','0'],
            'sidebar_content_b3': ['1','2','2','2','2','2'],
            'sidebar_content_b4': ['1','2','2','2','2','0'],
            'sidebar_content_br': ['0','4','0','0','4','0'],
            'sidebar_bet': ['0','0','0','0','0','4'],
            'sidebar_categories_b': ['1','1','0','0','1','1'],
            'sidebar_categories_b1': ['2','2','2','2','2','2'],
            'sidebar_categories_b2': ['2','2','2','2','2','2'],
            'sidebar_categories_b3': ['2','2','2','2','2','2'],
            'sidebar_categories_b4': ['2','2','2','2','2','2'],
            'sidebar_categories_br': ['0','0','0','0','4','0'],
            'pl_nav_top_b': ['1','1','1','1','1','0'],
            'pl_nav_top_b1': ['0','2','2','0','1','2'],
            'pl_nav_top_b2': ['0','2','2','0','1','2'],
            'pl_nav_top_b3': ['2','2','2','2','1','2'],
            'pl_nav_top_b4': ['0','2','2','0','1','2'],
            'pl_nav_top_br': ['0','0','0','0','4','0'],
            'pl_nav_bot_b': ['1','1','1','1','1','1'],
            'pl_nav_bot_b1': ['2','2','2','2','2','2'],
            'pl_nav_bot_b2': ['2','2','2','2','2','0'],
            'pl_nav_bot_b3': ['2','2','2','2','2','0'],
            'pl_nav_bot_b4': ['2','2','2','2','2','0'],
            'pl_nav_bot_br': ['3','0','0','3','3','0'],
            'nc_hover4_g': ['1','1','1','1','1','0'],
            'nc_pls_cat': ['1','1','1','1','1','1'],
            'pl_filter_b': ['1','1','1','1','1','0'],
            'pl_filter_b1': ['2','2','2','2','2','2'],
            'pl_filter_b2': ['2','2','2','2','2','2'],
            'pl_filter_b3': ['2','2','2','2','2','2'],
            'pl_filter_b4': ['2','2','2','2','2','2'],
            'pl_filter_br': ['0','4','0','0','4','4'],
            'pl_item_bgs': ['0','0','0','0','0','0'],
            'nc_pl_red': ['0','0','0','0','0','0'],
            'pl_i_qw': ['1','1','1','1','6','3'],
            'pl_i_w': ['1','3','1','1','3','1'],
            'pl_i_c': ['1','1','4','3','1','1'],
            'pl_i_a': ['3','4','5','4','1','2'],
            'pl_i_s': ['2','2','2','2','2','2'],
            'pp_imgb': ['0','0','1','1','1','1'],
            'nc_pp_ip': ['1','1','1','1','1','1'],
            'nc_pp_col': ['1','1','1','1','1','5'],
            'nc_pp_tabs': ['1','1','1','1','1','2'],
            'nc_pp_image': ['2','1','1','1','1','2'],
            'nc_pp_qq3': ['1','1','1','1','1','1'],
            'nc_pp_lo3': ['true','true','true','true','true','true'],
            'nc_pos': ['1','1','1','1','1','1'],
            'nc_pp_st': ['1','1','0','0','0','1'],
            'nc_pp_q': ['1','1','1','1','1','2'],
            'nc_pp_qm': ['1','1','1','1','1','1'],
            'pp_z': ['1','7','1','1','1','1'],
            'pp_zio': ['0.25','0.45','1','1','1','0.85'],
            'pp_display_print': ['0','0','0','0','0','0'],
            'pp_li': ['1','1','1','1','1','0'],
            'pp_display_refer': ['1','1','1','1','1','1'],
            'pp_display_cond': ['1','1','1','1','0','0'],
            'pp_display_avail': ['1','1','1','1','1','0'],
            'pp_display_q': ['1','1','1','1','1','0'],
            'pp_reviews_display_top': ['1','1','1','1','1','1'],
            'header_lay': ['1','3','3','2','5','3'],
            'nc_header_fw': ['0','1','0','0','0','1'],
            'nc_icons_spacing': ['0','0','0','0','0','1'],
            'header_trah': ['0','1','1','0','1','1'],
            'header_trao': ['0','1','0','0','1','1'],
            'header_sc': ['1','1','1','1','1','1'],
            'acc_lay': ['1','2','1','2','1','1'],
            'acc_icon': ['1','2','1','3','3','1'],
            'cart_lay': ['1','4','1','2','1','1'],
            'cart_icon': ['6','1','6','4','1','2'],
            'c_popup_b': ['1','1','1','1','1','1'],
            'c_popup_b1': ['1','1','3','1','1','1'],
            'c_popup_b2': ['1','1','0','1','1','1'],
            'c_popup_b3': ['1','1','0','1','1','1'],
            'c_popup_b4': ['1','1','0','1','1','1'],
            'c_popup_br': ['0','0','0','0','0','0'],
            'c_popup_shadow': ['1','1','1','1','1','1'],
            'c_popup_title': ['1','1','1','1','1','1'],
            'ma_login': ['1','10','1','9','1','1'],
            'ma_reg': ['1','2','1','10','1','3'],
            'bl_titlelay': ['4','4','4','4','4','1'],
            'bl_dd': ['0','0','0','0','0','1'],
            'bl_row': ['3','2','2','2','3','3'],
            'bl_date': ['1','1','1','1','1','2'],
            'bl_date_pad': ['16','16','16','16','16','16'],
            'bl_rm': ['5','1','5','5','3','5'],
            'bl_rm_bg': ['0','1','0','0','0','1'],
            'bl_rm_border': ['1','0','1','1','1','0'],
            'footer_pay': ['0.3','0.3','0.3','0.3','0.3','0.3'],
            'footer_copyright_display': ['1','1','1','1','1','1'],
            'footer_lay': ['2','5','5','6','2','8'],
            'footer_map_en': ['1','1','1','0','1','1'],
            'footer_map_enbb': ['1','1','0','0','1','1'],
            'footer_map_bw': ['2','2','2','2','2','2'],
            'footer_map_br': ['4','0','0','0','40','40'],
            'footer_map_i': ['7','1','7','7','6','7'],
            'footer_map_sub': ['1','1','1','1','1','1'],
            'nc_subcat': ['0','0','0','0','0','0'],
            'nc_cat': ['1','1','1','1','1','1'],
            'nc_loader': ['0','0','0','0','0','0'],
            'nc_loader_logo': ['1','1','1','1','1','1'],
            'nc_loader_lay': ['5','1','1','1','1','1'],
            'nc_sale': ['1','1','2','2','1','2'],
            'nc_label': ['1','2','1','3','1','2'],
            'nc_second_img': ['1','1','1','1','1','1'],
            'nc_qwrotate': ['0','0','0','0','0','0'],
            'nc_ai': ['1','0','1','1','1','0'],
            'nc_pp_att_right': ['0','0','0','0','0','0'],
            'nc_man_text': ['1','1','1','1','1','1'],
            'nc_man_logo': ['1','1','1','1','1','1'],
            'nc_rgrid': ['1','1','1','1','1','1'],
            'nc_startype': ['1','2','1','2','2','2'],
            'nc_rlist': ['1','1','1','1','1','1'],
            'nc_rhide': ['1','1','1','1','0','1'],
            'nc_count': ['1','1','1','1','1','1'],
            'nc_count_days': ['0','0','1','0','0','1'],
            'nc_ital_pn': ['1','1','1','1','1','1'],
            'nc_italic_pp': ['1','1','1','1','1','1'],
            'nc_italic_pd': ['1','1','1','1','1','1'],
            'nc_italic_s': ['1','1','1','1','1','1'],
            'nc_italic_it': ['1','1','1','1','1','1'],
            'nc_italic_id': ['1','1','1','1','1','1'],
            'nc_italic_lt': ['1','1','1','1','1','1'],
            'nc_italic_pc': ['1','1','1','1','1','1'],
            'nc_italic_ppl': ['1','1','1','1','1','1'],
            'nc_up_hp': ['1','2','2','2','2','1'],
            'nc_up_bb': ['1','2','2','1','1','2'],
            'nc_up_nc': ['1','1','1','1','1','1'],
            'nc_up_np': ['1','1','1','1','1','1'],
            'nc_up_f': ['1','2','1','2','2','2'],
            'nc_up_bp': ['1','2','1','2','2','2'],
            'nc_up_add': ['1','2','2','2','2','1'],
            'nc_up_mi': ['1','2','2','2','2','1'],
            'nc_up_menu': ['1','2','2','2','2','2'],
            'nc_up_head': ['1','1','1','1','1','1'],
            'nc_up_but': ['1','2','2','2','2','1'],
            'nc_product_switch': ['4','2','3','4','3','4'],
            'nc_pc_layout': ['1','1','1','2','3','1'],
            'nc_pc_b': ['1','1','2','1','2','1'],
            'nc_p_hover': ['1','4','2','3','3','4'],
            'nc_carousel_featured': ['1','2','1','1','1','2'],
            'nc_auto_featured': ['true','false','true','true','true','true'],
            'nc_items_featured': ['5','3','5','5','3','5'],
            'nc_carousel_best': ['1','1','1','1','1','1'],
            'nc_auto_best': ['true','true','true','true','true','true'],
            'nc_items_best': ['5','4','5','4','4','5'],
            'nc_carousel_new': ['1','1','1','1','1','1'],
            'nc_auto_new': ['true','true','true','true','true','true'],
            'nc_items_new': ['5','4','5','5','3','5'],
            'nc_carousel_sale': ['1','1','1','1','1','1'],
            'nc_auto_sale': ['true','true','true','true','true','true'],
            'nc_items_sale': ['5','4','5','4','3','5'],
            'nc_carousel_custom1': ['1','1','1','1','1','1'],
            'nc_auto_custom1': ['true','true','true','true','true','true'],
            'nc_items_custom1': ['5','3','5','4','3','5'],
            'nc_carousel_custom2': ['1','1','1','1','1','1'],
            'nc_auto_custom2': ['true','true','true','true','true','true'],
            'nc_items_custom2': ['5','4','5','4','3','5'],
            'nc_carousel_custom3': ['1','1','1','1','1','1'],
            'nc_auto_custom3': ['true','true','true','true','true','true'],
            'nc_items_custom3': ['5','4','5','4','3','5'],
            'nc_carousel_custom4': ['1','1','1','1','1','1'],
            'nc_auto_custom4': ['true','true','true','true','true','true'],
            'nc_items_custom4': ['5','4','4','4','3','5'],
            'nc_carousel_custom5': ['1','1','1','1','1','1'],
            'nc_auto_custom5': ['true','true','true','true','true','true'],
            'nc_items_custom5': ['5','4','4','4','3','5'],
            'nc_pls_f': ['1','1','1','1','1','1'],
            'nc_pls_b': ['1','1','1','1','1','0'],
            'nc_pls_n': ['1','1','1','1','1','1'],
            'nc_pls_s': ['1','1','1','1','1','1'],
            'nc_pls_cc1': ['1','1','1','1','1','0'],
            'nc_pls_cc2': ['1','1','1','1','1','1'],
            'nc_pls_cc3': ['1','1','1','1','1','1'],
            'nc_pls_cc4': ['1','1','1','1','1','1'],
            'nc_pls_cc5': ['1','1','1','1','1','1'],
            'nc_plc_f': ['1','1','1','2','3','1'],
            'nc_plc_b': ['1','1','1','2','3','4'],
            'nc_plc_n': ['1','1','1','2','3','1'],
            'nc_plc_s': ['1','1','1','2','3','1'],
            'nc_plc_cc1': ['1','1','1','2','3','4'],
            'nc_plc_cc2': ['1','1','1','2','3','1'],
            'nc_plc_cc3': ['1','1','1','2','3','1'],
            'nc_plc_cc4': ['1','1','1','2','3','1'],
            'nc_plc_cc5': ['1','1','1','2','3','1'],
            'nc_cat_title': ['1','1','1','1','1','2'],
            'nc_cat_under': ['0','0','0','0','0','1'],
            'nc_footer_bwh': ['0','0','0','0','0','0'],
            'nc_footer_bwo': ['0','0','0','0','0','60'],
            'nc_colors': ['0','0','0','0','0','0'],
            'nc_mobadrag': ['1','1','1','1','1','1'],
            'nc_mobadots': ['1','1','1','1','1','1'],
            'nc_mobaarrows': ['1','1','1','1','1','1'],
            "nc_opc_title": ['2','2','2','2','2','2'],
            "nc_opc_date": ['2','2','2','2','2','2'],
            "nc_opc_address": ['1','1','1','1','1','1'],
            "nc_opc_info": ['2','2','2','2','2','2'],
            "nc_opc_inv": ['2','2','2','2','2','2'],
            "nc_opc_mob": ['1','1','1','1','1','1'],
            "nc_opc_hide_country": ['1','1','1','1','1','1'],
            "nc_opc_halfcity": ['1','1','1','1','1','1'],
            "nc_opc_halfstate": ['1','1','1','1','1','1'],
            "nc_guest_only": ['2','2','2','2','2','2'],
            "nc_guest_add": ['1','1','1','1','1','1'],
            "nc_guest_show": ['1','1','1','1','1','1'],
            "nc_guest_text": ['Default','Default','Default','Default','Default','Default'],
            "nc_guest_zip": ['00000','00000','00000','00000','00000','00000'],
            "nc_opc_width": ['1','1','1','1','1','1']
        }

        var keys = Object.keys(demo_settings);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var value = demo_settings[key][demo_number - 1];
            var name = $("[name='" + key + "']");

            $(name).each(function() {
                $(this).val(value)
            });
        }

        $('.demo_apply').html('<i></i>Demo settings changed, click save changes to save it');

    });


    // Color schemes
    $('.colors_apply').click(function() {

        if ($('#select_scheme1').is(':checked')) var scheme_number = '1';
        if ($('#select_scheme2').is(':checked')) var scheme_number = '2';
        if ($('#select_scheme3').is(':checked')) var scheme_number = '3';
        if ($('#select_scheme4').is(':checked')) var scheme_number = '4';
        if ($('#select_scheme5').is(':checked')) var scheme_number = '5';
        if ($('#select_scheme6').is(':checked')) var scheme_number = '6';

        // Base colors massive
        var scheme_colors = {
            'main_background_color': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5'],
            'body_gs': ['#00c091','#00c091','#00c091','#00c091','#389290','#00c091'],
            'body_ge': ['#8480df','#8480df','#8480df','#8480df','#8480df','#8480df'],
            'brand_p_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'brand_p_text': ['#a7a7a7','#a7a7a7','#a7a7a7','#a7a7a7','#a7a7a7','#bbbbbb'],
            'brand_p_name': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'brand_p_con': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'mini_titles': ['#1d1d1d','#00c091','#6264ff','#f2f2f2','#000000','#5e6363'],
            'mini_sep': ['#ffffff','#ffffff','#ffffff','#363636','#ffffff','#ffffff'],
            'mini_con': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'mini_pn': ['#323232','#323232','#323232','#ffffff','#323232','#323232'],
            'mini_bc': ['#e5e5e5','#e5e5e5','#e5e5e5','#cccccc','#ffd000','#e5e5e5'],
            'mini_pd': ['#c7c7c7','#c7c7c7','#c7c7c7','#828282','#c7c7c7','#c7c7c7'],
            'mini_pp': ['#444444','#444444','#444444','#ff8e32','#444444','#444444'],
            'mini_pop': ['#cccccc','#cccccc','#cccccc','#9e4700','#cccccc','#cccccc'],
            'cs_main': ['#00bda0','#00bda0','#00bda0','#00bda0','#00bda0','#00bda0'],
            'cs_second': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'g_label': ['#333333','#333333','#333333','#333333','#333333','#333333'],
            'g_checkbox_label': ['#777777','#777777','#777777','#777777','#777777','#777777'],
            'g_form': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_form_grey': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'search_close': ['#313743','#313743','#313743','#ff8e32','#ffd000','#626262'],
            'b_title': ['#323232','#000000','#6264ff','#323232','#ffffff','#ffffff'],
            'b_text': ['#ababab','#555555','#ababab','#ababab','#f3f3f3','#ffffff'],
            'b_link': ['#888888','#555555','#888888','#888888','#ffffff','#ffffff'],
            'b_link_hover': ['#323232','#000000','#323232','#323232','#ffd000','#ffffff'],
            'b_separator': ['#dddddd','#aaaaaa','#dddddd','#dddddd','#dddddd','#cd2746'],
            'b_border': ['#f2f2f2','#ededed','#f2f2f2','#f2f2f2','#f2f2f2','#cd2746'],
            'g_body_link': ['#323232','#323232','#6264ff','#323232','#323232','#323232'],
            'g_body_link_hover': ['#000000','#000000','#ff1b71','#000000','#000000','#f93d60'],
            'g_dark': ['#000000','#000000','#000000','#000000','#000000','#000000'],
            'g_table_title_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_table_title_color': ['#999999','#999999','#999999','#999999','#999999','#999999'],
            'g_table_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_table_b': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'g_table_lbg': ['#fafafa','#fafafa','#fafafa','#fafafa','#fafafa','#fafafa'],
            'g_table_lc': ['#6d6d6d','#6d6d6d','#6d6d6d','#6d6d6d','#000000','#6d6d6d'],
            'g_table_rc': ['#777777','#777777','#777777','#777777','#323232','#777777'],
            'g_cc': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_ch': ['#323232','#323232','#323232','#323232','#323232','#f93d60'],
            'g_hb': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'g_hc': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_fancy_bg': ['#1d1d1d','#999999','#999999','#999999','#177ec4','#177ec4'],
            'g_fancy_cibg': ['#1d1d1d','#555555','#555555','#555555','#ffd000','#f93d60'],
            'g_fancy_cic': ['#ffffff','#ffffff','#ffffff','#ffffff','#000000','#ffffff'],
            'g_fancy_nbg': ['#323232','#323232','#323232','#323232','#ffffff','#ffffff'],
            'g_fancy_nc': ['#ffffff','#ffffff','#ffffff','#ffffff','#000000','#000000'],
            'g_box': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_boxb': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5'],
            'g_box_title': ['#222222','#222222','#222222','#222222','#222222','#222222'],
            'g_box_title_border': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'g_hp1_bg': ['#ffffff','#ffffff','#ffffff','#292929','#ffffff','#ffffff'],
            'g_hp1_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#53cdde','#f8f8f8'],
            'g_hp1_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#299ae6','#d6d6d6'],
            'g_hp2_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_hp2_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_hp2_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_hp3_bg': ['#ffffff','#ffffff','#ffffff','#292929','#ffffff','#ffffff'],
            'g_hp3_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_hp3_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_hp4_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffd83a'],
            'g_hp4_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_hp4_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_hp5_bg': ['#1d1d1d','#fafafa','#fafafa','#fafafa','#fafafa','#fafafa'],
            'g_hp5_gs': ['#1d1d1d','#f4f4f4','#f4f4f4','#eeeeee','#eeeeee','#f4f4f4'],
            'g_hp5_ge': ['#1d1d1d','#fafafa','#fafafa','#fcfcfc','#fcfcfc','#fafafa'],
            'g_hp6_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#fcda3a'],
            'g_hp6_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_hp6_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_mc_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_mc_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_mc_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_bc_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_bc_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_bc_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_fs1_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_fs1_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_fs1_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_fs2_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_fs2_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_fs2_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_fs3_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_fs3_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_fs3_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'g_op1_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#f93d60'],
            'g_op1_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#53cdde','#f8f8f8'],
            'g_op1_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#299ae6','#d6d6d6'],
            'g_op2_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'g_op2_gs': ['#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8','#f8f8f8'],
            'g_op2_ge': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'b_normal_bg': ['#ffffff','#ffffff','#6264ff','#ffffff','#ffffff','#ffffff'],
            'b_normal_border': ['#e5e5e5','#e5e5e5','#6264ff','#ff8e32','#ffd000','#e5e5e5'],
            'b_normal_color': ['#323232','#323232','#ffffff','#323232','#323232','#000000'],
            'b_normal_bg_hover': ['#323232','#ffffff','#ff1b71','#ff6926','#299ae6','#f93d60'],
            'b_normal_border_hover': ['#323232','#1fc4b2','#ff1b71','#ff6926','#299ae6','#f93d60'],
            'b_normal_color_hover': ['#ffffff','#009383','#ffffff','#ffffff','#ffffff','#ffffff'],
            'b_ex_bg': ['#323232','#ffffff','#00ba7b','#ff8e32','#ffd000','#000000'],
            'b_ex_border': ['#323232','#1fc4b2','#00ba7b','#ff8e32','#ffd000','#000000'],
            'b_ex_color': ['#ffffff','#009383','#ffffff','#ffffff','#323232','#ffffff'],
            'b_ex_bg_hover': ['#525252','#1fc4b2','#ff1b71','#ff6926','#299ae6','#f93d60'],
            'b_ex_border_hover': ['#525252','#009383','#ff1b71','#ff6926','#299ae6','#f93d60'],
            'b_ex_color_hover': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'b_to': ['#323232','#323232','#6264ff','#323232','#323232','#323232'],
            'b_toh': ['#000000','#000000','#ff1b71','#ff6926','#299ae6','#f93d60'],
            'i_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'i_color': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'i_b_color': ['#dddddd','#dddddd','#dddddd','#dddddd','#cccccc','#dddddd'],
            'i_bg_focus': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'i_color_focus': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'i_b_focus': ['#000000','#000000','#6264ff','#000000','#299ae6','#000000'],
            'rc_bg': ['#ffffff','#ffffff','#ffffff','#525252','#ffffff','#ffffff'],
            'rc_border': ['#e5e5e5','#e5e5e5','#e5e5e5','#777777','#e5e5e5','#e5e5e5'],
            'rc_bg_active': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'rc_border_active': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'cl_text': ['#cccccc','#cccccc','#777777','#cccccc','#cccccc','#aaaaaa'],
            'cl_sep': ['#555555','#555555','#f2f2f2','#555555','#555555','#ededed'],
            'ban_title': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'ban_text': ['#999999','#999999','#999999','#999999','#999999','#bbbbbb'],
            'ban_title_over': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'ban_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'cl_popup_bg': ['#323232','#323232','#ffffff','#323232','#323232','#ffffff'],
            'cl_popup_bc': ['#323232','#323232','#ff1b71','#323232','#323232','#323232'],
            'cl_link': ['#dddddd','#dddddd','#000000','#dddddd','#dddddd','#000000'],
            'cl_link_hover': ['#ffffff','#ffffff','#6264ff','#ffffff','#ffffff','#f93d60'],
            'cl_lc': ['#dddddd','#dddddd','#000000','#dddddd','#dddddd','#000000'],
            'cl_lc_hover': ['#ffffff','#ffffff','#6264ff','#ffffff','#ffffff','#f93d60'],
            'ta_welcome': ['#cccccc','#cccccc','#aaaaaa','#cccccc','#cccccc','#aaaaaa'],
            'ta_name': ['#ffffff','#4ef1cd','#6264ff','#ff8e32','#ffd000','#000000'],
            'info_stitle': ['#000000','#000000','#000000','#000000','#000000','#000000'],
            'info_icon': ['#dfdfdf','#ffffff','#6264ff','#ff8e32','#ffffff','#f93d60'],
            'info_iconbg': ['#fafafa','#1fc4b2','#fafafa','#fafafa','#299ae6','#ffffff'],
            'info_title': ['#000000','#000000','#000000','#000000','#000000','#000000'],
            'info_text': ['#999999','#999999','#999999','#999999','#999999','#999999'],
            'hp_dec': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'hp_desc': ['#a8a8a8','#a8a8a8','#a8a8a8','#a8a8a8','#a8a8a8','#a8a8a8'],
            'hp_con': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'hp_conho': ['#323232','#323232','#6264ff','#323232','#299ae6','#323232'],
            'hp_title': ['#323232','#323232','#323232','#323232','#323232','#5e6363'],
            'hp_title_hover': ['#000000','#000000','#6264ff','#000000','#299ae6','#000000'],
            'm_link_bg_hover': ['#fafafa','#fafafa','#fafafa','#fafafa','#ffd000','#fafafa'],
            'm_link': ['#323232','#323232','#323232','#ffffff','#323232','#323232'],
            'm_link_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#000000','#f93d60'],
            'm_links': ['#ffffff','#323232','#323232','#ffffff','#323232','#323232'],
            'm_links_hover': ['#f2f2f2','#1fc4b2','#6264ff','#ff8e32','#000000','#f93d60'],
            'm_linkh': ['#ffffff','#323232','#323232','#ffffff','#323232','#ffffff'],
            'm_linkh_hover': ['#c7f3ea','#1fc4b2','#6264ff','#ff8e32','#000000','#ffffff'],
            'm_linko': ['#ffffff','#323232','#ffffff','#ffffff','#323232','#ffffff'],
            'm_linko_hover': ['#c7f3ea','#1fc4b2','#c7f3ea','#c7f3ea','#000000','#ffffff'],
            'm_bg': ['#fafafa','#fafafa','#fafafa','#fafafa','#ffffff','#fafafa'],
            'm_icon': ['#ffffff','#323232','#6264ff','#ffffff','#323232','#323232'],
            'm_popup_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'm_popup_bc': ['#eeeeee','#eeeeee','#ff1b71','#eeeeee','#eeeeee','#eeeeee'],
            'm_popup_link': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'm_popup_link_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'm_popup_chevron': ['#555555','#555555','#555555','#555555','#555555','#555555'],
            'm_popup_llink': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'm_popup_llink_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'm_popup_lbg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'm_popup_lbg_hover': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'm_popup_lchevron': ['#555555','#555555','#555555','#555555','#555555','#555555'],
            'm_popup_lborder': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5'],
            'm_popup_mlink': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'm_popup_mlink_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'm_popup_mtitle': ['#555555','#555555','#555555','#555555','#555555','#555555'],
            'm_popup_mtitle_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'm_popup_mchevron': ['#555555','#555555','#555555','#555555','#555555','#555555'],
            'm_popup_mname': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'm_popup_mprice': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'search_i': ['#323232','#323232','#292929','#ffffff','#323232','#323232'],
            'search_t': ['#323232','#323232','#292929','#ffffff','#323232','#323232'],
            'search_i_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'search_t_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'search_si': ['#ffffff','#323232','#292929','#ffffff','#323232','#323232'],
            'search_st': ['#ffffff','#323232','#292929','#ffffff','#323232','#323232'],
            'search_si_hover': ['#ffffff','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'search_st_hover': ['#ffffff','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'search_ti': ['#ffffff','#323232','#292929','#ffffff','#ffffff','#ffffff'],
            'search_tt': ['#ffffff','#323232','#292929','#ffffff','#ffffff','#ffffff'],
            'search_ti_hover': ['#4de3dd','#1fc4b2','#6264ff','#4de3dd','#ffd000','#ffffff'],
            'search_tt_hover': ['#4de3dd','#1fc4b2','#6264ff','#4de3dd','#ffd000','#ffffff'],
            'search_oi': ['#ffffff','#323232','#ffffff','#ffffff','#ffffff','#ffffff'],
            'search_ot': ['#ffffff','#323232','#ffffff','#ffffff','#ffffff','#ffffff'],
            'search_oi_hover': ['#4de3dd','#1fc4b2','#4de3dd','#4de3dd','#ffd000','#ffffff'],
            'search_ot_hover': ['#4de3dd','#1fc4b2','#4de3dd','#4de3dd','#ffd000','#ffffff'],
            'search_bg': ['#ffffff','#ffffff','#ffffff','#363636','#299ae6','#323232'],
            'search_type': ['#1d1d1d','#cccccc','#292929','#777777','#ffd000','#f93d60'],
            'search_res': ['#555555','#555555','#555555','#f2f2f2','#f2f2f2','#cccccc'],
            'search_res_h': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#ffffff','#f93d60'],
            'search_res_b': ['#1d1d1d','#fafafa','#ffffff','#292929','#299ae6','#323232'],
            'search_input': ['#555555','#555555','#6264ff','#ff8e32','#ffffff','#ffffff'],
            'search_inputh': ['#323232','#323232','#6264ff','#ff8e32','#ffffff','#ffffff'],
            'search_line': ['#555555','#555555','#6264ff','#ff8e32','#ffffff','#f93d60'],
            'search_lineh': ['#323232','#323232','#6264ff','#ff8e32','#ffffff','#f93d60'],
            'search_icon': ['#323232','#323232','#6264ff','#ff8e32','#ffffff','#f93d60'],
            'search_iconh': ['#1d1d1d','#1fc4b2','#6264ff','#ff6926','#ffffff','#f93d60'],
            'levi_up': ['#ffffff','#ffffff','#ffffff','#ffffff','#000000','#555555'],
            'levi_up_bg': ['#000000','#1fc4b2','#00e295','#ff8e32','#f2f2f2','#f2f2f2'],
            'levi_up_b': ['#000000','#1fc4b2','#00e295','#ff8e32','#f2f2f2','#f2f2f2'],
            'levi_up_hover': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#000000'],
            'levi_up_bg_hover': ['#323232','#323232','#00e295','#ff6926','#299ae6','#e5e5e5'],
            'levi_up_b_hover': ['#323232','#323232','#00e295','#ff6926','#299ae6','#e5e5e5'],
            'levi_m': ['#323232','#323232','#323232','#323232','#323232','#fcda3a'],
            'levi_m_bg': ['#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','transparent'],
            'levi_m_b': ['#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','transparent'],
            'levi_m_hover': ['#ffffff','#ffffff','#ffffff','#ffffff','#000000','#323232'],
            'levi_m_bg_hover': ['#000000','#ffc018','#ffc018','#ff8e32','#ffd000','#fcda3a'],
            'levi_m_b_hover': ['#000000','#ffc018','#ffc018','#ff8e32','#ffd000','#fcda3a'],
            'levi_m_p_bg': ['#f2f2f2','#f2f2f2','#f2f2f2','#424442','#f2f2f2','#fafafa'],
            'levi_m_p_text': ['#323232','#323232','#323232','#f2f2f2','#323232','#777777'],
            'levi_m_p_ibg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ebebeb'],
            'levi_m_p_ib': ['#e5e5e5','#e5e5e5','#e5e5e5','#ff8e32','#ffffff','#ebebeb'],
            'levi_m_p_ip': ['#cccccc','#cccccc','#cccccc','#cccccc','#cccccc','#777777'],
            'levi_m_p_it': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'levi_m_p_sbg': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#323232'],
            'levi_m_p_sb': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#323232'],
            'levi_m_p_sc': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'levi_m_p_sbg_hover': ['#000000','#000000','#000000','#ff6926','#ffd000','#000000'],
            'levi_m_p_sb_hover': ['#000000','#000000','#000000','#ff6926','#ffd000','#000000'],
            'levi_m_p_sc_hover': ['#ffffff','#ffffff','#ffffff','#ffffff','#000000','#fcda3a'],
            'levi_f_bg': ['#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','transparent'],
            'levi_f_b': ['#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','transparent'],
            'levi_f_c': ['#323232','#323232','#323232','#323232','#323232','#3B5998'],
            'levi_f_bg_hover': ['#3B5998','#3B5998','#3B5998','#3B5998','#3B5998','#3B5998'],
            'levi_f_b_hover': ['#3B5998','#3B5998','#3B5998','#3B5998','#3B5998','#3B5998'],
            'levi_f_c_hover': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'levi_f_p_bg': ['#3B5998','#3B5998','#3B5998','#3B5998','#3B5998','#3B5998'],
            'levi_f_p_text': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'mob_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#323232'],
            'mob_text': ['#777777','#777777','#777777','#777777','#777777','#f2f2f2'],
            'mob_links': ['#323232','#323232','#323232','#323232','#323232','#ffffff'],
            'mob_title': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#fcda3a'],
            'mob_sep': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#555555'],
            'mob_check': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'mob_checkbg': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'mob_checkb': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'mob_close': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'mob_closebg': ['#323232','#323232','#323232','#323232','#323232','#626262'],
            'mob_m_t': ['#323232','#323232','#323232','#323232','#323232','#fafafa'],
            'mob_m_tbg': ['#fafafa','#fafafa','#fafafa','#fafafa','#fafafa','#323232'],
            'mob_m_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#323232'],
            'mob_m_link': ['#000000','#000000','#000000','#000000','#000000','#fafafa'],
            'mob_m_linkb': ['#eeeeee','#eeeeee','#eeeeee','#eeeeee','#eeeeee','#555555'],
            'mob_m_linke': ['#323232','#323232','#323232','#323232','#323232','#f93d60'],
            'sidebar_bg_color': ['#ffffff','#f4f4f4','#ffffff','#424442','#ffffff','#ffffff'],
            'sidebar_title_bg': ['#fafafa','#fafafa','#fafafa','#424442','#fafafa','#ffffff'],
            'sidebar_title_border': ['#ededed','#ffffff','#ff1b71','#424442','#ededed','#ededed'],
            'sidebar_title_color': ['#323232','#323232','#323232','#f2f2f2','#323232','#323232'],
            'sidebar_title_link': ['#323232','#323232','#323232','#f2f2f2','#323232','#323232'],
            'sidebar_title_link_hover': ['#626262','#626262','#626262','#ff8e32','#626262','#626262'],
            'sidebar_block_content_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'sidebar_block_content_border': ['#e5e5e5','#ededed','#ededed','#525252','#323232','#ededed'],
            'sidebar_block_text_color': ['#777777','#777777','#777777','#e2e2e2','#777777','#bbbbbb'],
            'sidebar_block_link': ['#323232','#323232','#323232','#f2f2f2','#323232','#323232'],
            'sidebar_block_link_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'sidebar_c': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'sidebar_hc': ['#323232','#323232','#ff1b71','#ff6926','#299ae6','#323232'],
            'sidebar_tags_bg': ['#ffffff','#ffffff','#ffffff','#525252','#ffffff','#ffffff'],
            'sidebar_tags_border': ['#e5e5e5','#e5e5e5','#e5e5e5','#aaaaaa','#e5e5e5','#e5e5e5'],
            'sidebar_tags_color': ['#323232','#323232','#323232','#f2f2f2','#323232','#323232'],
            'sidebar_tags_hbg': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'sidebar_tags_hborder': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'sidebar_tags_hcolor': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'sidebar_button_bg': ['#ffffff','#ffffff','#6264ff','#ff8e32','#299ae6','#ffffff'],
            'sidebar_button_border': ['#ededed','#e5e5e5','#6264ff','#ff8e32','#299ae6','#e5e5e5'],
            'sidebar_button_color': ['#323232','#323232','#ffffff','#ffffff','#ffffff','#323232'],
            'sidebar_button_hbg': ['#323232','#323232','#ff1b71','#ff6926','#323232','#323232'],
            'sidebar_button_hborder': ['#323232','#323232','#ff1b71','#ff6926','#323232','#323232'],
            'sidebar_button_hcolor': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'sidebar_item_separator': ['#f2f2f2','#f2f2f2','#f2f2f2','#626262','#f2f2f2','#ffffff'],
            'sidebar_product_desc': ['#bbbbbb','#bbbbbb','#bbbbbb','#aaaaaa','#bbbbbb','#bbbbbb'],
            'sidebar_product_price': ['#444444','#444444','#444444','#ff8e32','#444444','#444444'],
            'sidebar_product_oprice': ['#bbbbbb','#bbbbbb','#bbbbbb','#aaaaaa','#bbbbbb','#bbbbbb'],
            'sidebar_categories_bg': ['#ffffff','#ffffff','#ffffff','#525252','#ffffff','#ffffff'],
            'sidebar_categories_title': ['#323232','#323232','#323232','#f2f2f2','#000000','#323232'],
            'sidebar_categories_titlebg': ['#ffffff','#ffffff','#ffffff','#424442','#ffd000','#ffffff'],
            'sidebar_categories_bc': ['#323232','#323232','#6264ff','#525252','#323232','#ededed'],
            'sidebar_categories_ex': ['#cccccc','#cccccc','#cccccc','#777777','#cccccc','#cccccc'],
            'sidebar_categories_exa': ['#323232','#323232','#323232','#ff8e32','#299ae6','#323232'],
            'sidebar_categories_item': ['#323232','#323232','#323232','#f2f2f2','#000000','#323232'],
            'sidebar_categories_item_hover': ['#555555','#555555','#555555','#ff8e32','#299ae6','#000000'],
            'sidebar_categories_separator': ['#ededed','#ededed','#ededed','#525252','#ededed','#ffffff'],
            'cms_title': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'cms_title_border': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'page_text_color': ['#4a4e51','#4a4e51','#4a4e51','#4a4e51','#4a4e51','#4a4e51'],
            'page_headings': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'page_link': ['#888888','#888888','#888888','#888888','#888888','#000000'],
            'page_link_hover': ['#515151','#515151','#515151','#515151','#515151','#f93d60'],
            'page_bq_title': ['#525252','#525252','#525252','#525252','#525252','#525252'],
            'page_bq_name': ['#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa'],
            'page_bq_q': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'contact_form_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'contact_form_border': ['#323232','#323232','#323232','#323232','#323232','#ffffff'],
            'contact_form_title': ['#323232','#323232','#6264ff','#323232','#299ae6','#323232'],
            'contact_form_under': ['#e5e5e5','#e5e5e5','#e5e5e5','#ff8e32','#e5e5e5','#e5e5e5'],
            'contact_icon': ['#777777','#777777','#777777','#777777','#ffd000','#fcda3a'],
            'contact_icon_sec': ['#cccccc','#cccccc','#cccccc','#cccccc','#ffd000','#ffffff'],
            'warning_message_color': ['#e7b918','#e7b918','#e7b918','#e7b918','#e7b918','#e7b918'],
            'success_message_color': ['#48b151','#48b151','#48b151','#48b151','#48b151','#48b151'],
            'danger_message_color': ['#DB7769','#DB7769','#DB7769','#DB7769','#DB7769','#f93d60'],
            'alert_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'alert_border': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5'],
            'alert_color': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'alert_link': ['#555555','#555555','#555555','#555555','#555555','#555555'],
            'alert_hlink': ['#000000','#000000','#000000','#000000','#000000','#f93d60'],
            'pl_nav_top_bg': ['#ffffff','#f4f4f4','#ffffff','#ffffff','#2ea0ed','#f4f4f4'],
            'pl_nav_top_border': ['#ededed','#f4f4f4','#ededed','#ededed','#2ea0ed','#f4f4f4'],
            'pl_nav_bot_bg': ['#ffffff','#f4f4f4','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pl_nav_bot_border': ['#ededed','#f4f4f4','#ededed','#ededed','#e5e5e5','#f4f4f4'],
            'nc_hover4_gr1': ['#d474ff','#00d398','#d474ff','#d474ff','#d474ff','#ffffff'],
            'nc_hover4_gr2': ['#00a39a','#00a39a','#00a39a','#00a39a','#00a39a','#ffffff'],
            'nc_hover4_bg': ['#323232','#323232','#323232','#323232','#323232','#ffffff'],
            'nc_hover4_c': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#000000'],
            'pl_nav_grid': ['#323232','#323232','#cccccc','#525252','#c2e6ff','#cccccc'],
            'pl_nav_grid_bg': ['#ffffff','#f4f4f4','#ffffff','#ffffff','#2ea0ed','#ffffff'],
            'pl_nav_grid_b': ['#fafafa','#f4f4f4','#ffffff','#e5e5e5','#2ea0ed','#ffffff'],
            'pl_nav_grida': ['#1d1d1d','#1fc4b2','#ff1b71','#ff6926','#ffd000','#323232'],
            'pl_nav_grid_bga': ['#ffffff','#f4f4f4','#ffffff','#ffffff','#1e85ca','#ffffff'],
            'pl_nav_grid_ba': ['#eeeeee','#f4f4f4','#ffffff','#ff8e32','#2ea0ed','#ffffff'],
            'pl_nav_compare_border': ['#fafafa','#f4f4f4','#fafafa','#fafafa','#2ea0ed','#f4f4f4'],
            'pl_nav_sort': ['#888888','#888888','#888888','#888888','#ffffff','#bbbbbb'],
            'pl_nav_sorti': ['#ededed','#ededed','#ededed','#ededed','#ffd000','#ededed'],
            'pl_number_bg': ['#ffffff','#f4f4f4','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pl_number_bg_hover': ['#1d1d1d','#f4f4f4','#6264ff','#ffffff','#ffd000','#ffffff'],
            'pl_number_color': ['#323232','#323232','#6264ff','#323232','#323232','#323232'],
            'pl_number_color_hover': ['#ffffff','#1fc4b2','#ffffff','#323232','#323232','#f93d60'],
            'pl_number_b': ['#ffffff','#f4f4f4','#ffffff','#ffffff','#ffffff','#ededed'],
            'pl_number_b_hover': ['#1d1d1d','#f4f4f4','#6264ff','#ff8e32','#ffd000','#f93d60'],
            'pl_show_per_page': ['#888888','#888888','#888888','#888888','#888888','#bbbbbb'],
            'pl_show_items': ['#ababab','#ababab','#ababab','#ababab','#fafafa','#bbbbbb'],
            'pl_filter_separator': ['#ededed','#ededed','#ededed','#626262','#ededed','#ffffff'],
            'pl_filter_t': ['#323232','#323232','#000000','#f2f2f2','#323232','#323232'],
            'pl_filter_e': ['#cccccc','#cccccc','#cccccc','#777777','#cccccc','#cccccc'],
            'pl_filter_range': ['#dddddd','#dddddd','#b3b3dd','#ff8e32','#dddddd','#dddddd'],
            'pl_filter_brange': ['#dddddd','#dddddd','#b3b3dd','#ff8e32','#dddddd','#dddddd'],
            'pl_filter_range_out': ['#dddddd','#dddddd','#dddddd','#777777','#dddddd','#dddddd'],
            'pl_filter_handle_bg': ['#1d1d1d','#ffffff','#6264ff','#ff6926','#ffd000','#323232'],
            'pl_filter_handle_border': ['#ffffff','#ffffff','#ffffff','#ffc286','#ffffff','#525252'],
            'pl_filter_bg': ['#ffffff','#ffffff','#ffffff','#424442','#ffffff','#ffffff'],
            'pl_filter_bc': ['#ededed','#ededed','#ededed','#525252','#323232','#ededed'],
            'pl_item_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pl_item_border': ['#e5e5e5','#e5e5e5','#ededed','#e5e5e5','#e5e5e5','#e5e5e5'],
            'nc_pl_item_borderh': ['#e5e5e5','#e5e5e5','#ededed','#e5e5e5','#ffd000','#e5e5e5'],
            'pl_product_name': ['#323232','#323232','#323232','#323232','#323232','#5c6666'],
            'pl_product_price': ['#444444','#444444','#444444','#444444','#299ae6','#5c6666'],
            'pl_product_oldprice': ['#cccccc','#cccccc','#cccccc','#cccccc','#cccccc','#cccccc'],
            'pl_product_percent': ['#f13340','#f13340','#ff1b71','#f13340','#f13340','#f93d60'],
            'pl_product_cart': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffd000','#323232'],
            'pl_product_cart_border': ['#e5e5e5','#e5e5e5','#e5e5e5','#ff8e32','#ffd000','#323232'],
            'pl_product_cart_color': ['#323232','#323232','#6264ff','#ff6926','#323232','#ffffff'],
            'pl_product_cw_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#323232'],
            'pl_product_cw_border': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#323232'],
            'pl_product_cw_color': ['#bababa','#323232','#bababa','#bababa','#bababa','#ffffff'],
            'pl_product_cw_bg_hover': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#323232'],
            'pl_product_cw_border_hover': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#323232'],
            'pl_product_cw_color_hover': ['#323232','#323232','#323232','#323232','#299ae6','#fcda3a'],
            'pl_product_cw_color_hover_active': ['#999999','#1fc4b2','#999999','#999999','#299ae6','#f93d60'],
            'pl_product_wish_icon_active': ['#999999','#1fc4b2','#999999','#999999','#299ae6','#f93d60'],
            'pl_product_cart_hover': ['#323232','#323232','#323232','#ff6926','#299ae6','#323232'],
            'pl_product_cart_hover_border': ['#323232','#323232','#323232','#ff6926','#299ae6','#323232'],
            'pl_product_cart_hover_color': ['#ffffff','#1fc4b2','#ff1b71','#ffffff','#ffffff','#fcda3a'],
            'pl_product_quickview': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#323232'],
            'pl_product_quickview_hover': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#323232'],
            'pl_product_quickview_color': ['#909090','#323232','#909090','#909090','#909090','#ffffff'],
            'pl_product_quickview_color_hover': ['#323232','#323232','#323232','#323232','#299ae6','#fcda3a'],
            'pl_product_view_border': ['#ededed','#ededed','#ededed','#ededed','#ffd000','#323232'],
            'pl_product_view_border_hover': ['#ededed','#ededed','#ededed','#ededed','#ffd000','#323232'],
            'pl_product_new_bg': ['#1d1d1d','#1fc4b2','#ffc100','#fdfdfd','#299ae6','#ffffff'],
            'pl_product_new_border': ['#1d1d1d','#1fc4b2','#ffc100','#e5e5e5','#299ae6','#ffffff'],
            'pl_product_new_color': ['#ffffff','#ffffff','#ffffff','#555555','#ffffff','#323232'],
            'pl_product_sale_bg': ['#323232','#323232','#ff1b71','#ff6926','#ffd000','#ffffff'],
            'pl_product_sale_border': ['#323232','#323232','#ff1b71','#ff6926','#ffd000','#f93d60'],
            'pl_product_sale_color': ['#ffffff','#ffffff','#ffffff','#ffffff','#000000','#f93d60'],
            'pl_list_separator': ['#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2'],
            'pl_list_description': ['#777777','#777777','#777777','#777777','#777777','#869090'],
            'pp_img_border': ['#ffffff','#ffffff','#ffe048','#525252','#ffd000','#ffffff'],
            'pp_zi': ['#323232','#323232','#6264ff','#323232','#323232','#323232'],
            'pp_zih': ['#ffffff','#009383','#ffffff','#ffffff','#ffffff','#fcda3a'],
            'pp_zihbg': ['#323232','#323232','#6264ff','#ff8e32','#299ae6','#323232'],
            'pp_icon_border': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#ffd000','#e5e5e5'],
            'pp_icon_border_hover': ['#323232','#323232','#6264ff','#323232','#299ae6','#ffffff'],
            'pp_loyt': ['#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa'],
            'pp_loys': ['#777777','#777777','#777777','#777777','#777777','#777777'],
            'pp_loyb': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'pp_social_color': ['#888888','#888888','#888888','#888888','#888888','#888888'],
            'pp_useful_color_hover': ['#333333','#333333','#333333','#333333','#333333','#333333'],
            'pp_name': ['#323232','#000000','#323232','#323232','#323232','#000000'],
            'pp_desc': ['#999999','#999999','#999999','#999999','#999999','#525252'],
            'pp_nav': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'pp_navb': ['#ededed','#ededed','#ededed','#525252','#ffd000','#ededed'],
            'pp_navbg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pp_info_label': ['#999999','#999999','#999999','#999999','#999999','#aaaaaa'],
            'pp_info_value': ['#6d6d6d','#6d6d6d','#6d6d6d','#6d6d6d','#6d6d6d','#424242'],
            'pp_lic': ['#cccccc','#cccccc','#cccccc','#cccccc','#ffd000','#cccccc'],
            'pp_libg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pp_lib': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pp_att_label': ['#333333','#333333','#333333','#999999','#ffffff','#323232'],
            'pp_att_color': ['#eeeeee','#eeeeee','#eeeeee','#6d6d6d','#eeeeee','#eeeeee'],
            'pp_att_color_active': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pp_att_quan_input_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pp_att_quan_input_b': ['#e5e5e5','#e5e5e5','#e5e5e5','#ff8e32','#e5e5e5','#e5e5e5'],
            'pp_att_quan_input_c': ['#323232','#323232','#323232','#323232','#323232','#000000'],
            'pp_att_wc': ['#323232','#323232','#ff1b71','#ff6926','#299ae6','#000000'],
            'pp_att_wb': ['#ededed','#e5e5e5','#ededed','#ededed','#299ae6','#ffffff'],
            'pp_att_wbg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pp_att_wch': ['#323232','#f05a88','#ff1b71','#ff6926','#ffffff','#000000'],
            'pp_att_wbh': ['#323232','#f05a88','#ff1b71','#ff6926','#de2666','#f93d60'],
            'pp_att_wbgh': ['#ffffff','#ffffff','#ffffff','#ffffff','#de2666','#ffffff'],
            'pp_att_quan_s': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'pp_att_quan_pm_b': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5'],
            'pp_att_quan_pm_bh': ['#323232','#323232','#6264ff','#ff6926','#323232','#f93d60'],
            'pp_att_quan_pm_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pp_att_quan_pm_color': ['#cccccc','#cccccc','#cccccc','#cccccc','#cccccc','#cccccc'],
            'pp_att_quan_pm_bg_hover': ['#ffffff','#ffffff','#ffffff','#fafafa','#ffffff','#fafafa'],
            'pp_att_quan_pm_color_hover': ['#323232','#323232','#323232','#323232','#323232','#000000'],
            'pp_att_bg': ['#f7f7f7','#f7f7f7','#ffffff','#f7f7f7','#299ae6','#ffffff'],
            'pp_att_b': ['#f7f7f7','#f7f7f7','#ededed','#f7f7f7','#299ae6','#ededed'],
            'pp_price_color': ['#323232','#323232','#323232','#ff6926','#299ae6','#323232'],
            'pp_price_coloro': ['#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb'],
            'pp_tabs_sb': ['#ededed','#ededed','#ededed','#ededed','#e5e5e5','#ededed'],
            'pp_tabs_sheets_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'pp_tabs_sheets_color': ['#777777','#777777','#777777','#777777','#777777','#777777'],
            'pp_tabs_ca': ['#323232','#009383','#323232','#000000','#299ae6','#323232'],
            'pp_tabs_color': ['#6d6d6d','#323232','#6264ff','#6d6d6d','#6d6d6d','#989898'],
            'pp_tabs_color_hover': ['#1d1d1d','#009383','#ff1b71','#ff6926','#299ae6','#000000'],
            'pp_reviews_staron': ['#face00','#face00','#face00','#face00','#face00','#525252'],
            'pp_reviews_staroff': ['#c0c0c0','#c0c0c0','#c0c0c0','#c0c0c0','#c0c0c0','#aaaaaa'],
            'pp_reviews_name': ['#333333','#333333','#333333','#333333','#333333','#333333'],
            'pp_reviews_date': ['#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa'],
            'pp_reviews_use_separator': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'pp_reviews_post_bg': ['#eeeeee','#eeeeee','#eeeeee','#eeeeee','#eeeeee','#eeeeee'],
            'pp_reviews_useful': ['#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb'],
            'pp_reviews_color': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'pp_reviews_tc': ['#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa'],
            'pp_qw_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'header_bg': ['#ffffff','#ffffff','#ffffff','#424442','#transparent','#ffffff'],
            'header_bgs': ['#222222','#ffffff','#ffffff','#424442','#ffffff','#ffffff'],
            'header_nbg': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'header_nt': ['#cccccc','#cccccc','#cccccc','#cccccc','#cccccc','#cccccc'],
            'header_nl': ['#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2','#f2f2f2'],
            'header_nlh': ['#4ef1cd','#4ef1cd','#4ef1cd','#4ef1cd','#ffd000','#4ef1cd'],
            'header_hnt': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'header_hnl': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'header_ont': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'header_onl': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'header_nb': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'header_ns': ['#888888','#888888','#888888','#888888','#ededed','#ededed'],
            'header_nbh': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'header_nsh': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'header_nbo': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'header_nso': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'acc_i': ['#323232','#323232','#292929','#ffffff','#323232','#323232'],
            'acc_t': ['#323232','#323232','#292929','#ffffff','#323232','#323232'],
            'acc_i_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'acc_t_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'acc_cbg': ['#323232','#1fc4b2','#ff1b71','#ff8e32','#ffd000','#f93d60'],
            'acc_si': ['#ffffff','#323232','#292929','#ffffff','#323232','#323232'],
            'acc_st': ['#ffffff','#323232','#292929','#ffffff','#323232','#323232'],
            'acc_si_hover': ['#ffffff','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'acc_st_hover': ['#ffffff','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'acc_scbg': ['#ffffff','#1fc4b2','#ff1b71','#ff8e32','#ffd000','#f93d60'],
            'acc_hi': ['#ffffff','#323232','#292929','#ffffff','#ffffff','#ffffff'],
            'acc_ht': ['#ffffff','#323232','#292929','#ffffff','#ffffff','#ffffff'],
            'acc_hi_hover': ['#4de3dd','#1fc4b2','#6264ff','#4de3dd','#ffd000','#ffffff'],
            'acc_ht_hover': ['#4de3dd','#1fc4b2','#6264ff','#4de3dd','#ffd000','#ffffff'],
            'acc_hcbg': ['#1d1d1d','#1fc4b2','#ff1b71','#00bda0','#ffd000','#ffffff'],
            'acc_oi': ['#ffffff','#323232','#ffffff','#ffffff','#ffffff','#ffffff'],
            'acc_ot': ['#ffffff','#323232','#ffffff','#ffffff','#ffffff','#ffffff'],
            'acc_oi_hover': ['#4de3dd','#1fc4b2','#4de3dd','#4de3dd','#ffd000','#ffffff'],
            'acc_ot_hover': ['#4de3dd','#1fc4b2','#4de3dd','#4de3dd','#ffd000','#ffffff'],
            'acc_ocbg': ['#1d1d1d','#1fc4b2','#00bda0','#00bda0','#ffd000','#ffffff'],
            'cart_i': ['#323232','#323232','#292929','#ffffff','#323232','#323232'],
            'cart_t': ['#323232','#323232','#292929','#ffffff','#323232','#323232'],
            'cart_i_hover': ['#1d1d1d','#1fc4b2','#ff1b71','#ff8e32','#299ae6','#f93d60'],
            'cart_t_hover': ['#1d1d1d','#1fc4b2','#ff1b71','#ff8e32','#299ae6','#f93d60'],
            'cart_qbg': ['#323232','#1fc4b2','#ff1b71','#ff8e32','#ffd000','#f93d60'],
            'cart_qc': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#f93d60'],
            'cart_si': ['#ffffff','#323232','#292929','#ffffff','#323232','#323232'],
            'cart_st': ['#ffffff','#323232','#292929','#ffffff','#323232','#323232'],
            'cart_si_hover': ['#ffffff','#1fc4b2','#ff1b71','#ff8e32','#299ae6','#f93d60'],
            'cart_st_hover': ['#ffffff','#1fc4b2','#ff1b71','#ff8e32','#299ae6','#f93d60'],
            'cart_sqbg': ['#ffffff','#1fc4b2','#ff1b71','#ff8e32','#ffd000','#f93d60'],
            'cart_sqc': ['#000000','#ffffff','#ffffff','#ffffff','#ffffff','#f93d60'],
            'cart_hi': ['#ffffff','#323232','#292929','#ffffff','#ffffff','#ffffff'],
            'cart_ht': ['#ffffff','#323232','#292929','#ffffff','#ffffff','#ffffff'],
            'cart_hi_hover': ['#4de3dd','#1fc4b2','#ff1b71','#4de3dd','#ffd000','#ffffff'],
            'cart_ht_hover': ['#4de3dd','#1fc4b2','#ff1b71','#4de3dd','#ffd000','#ffffff'],
            'cart_hqbg': ['#1d1d1d','#1fc4b2','#ff1b71','#00bda0','#ffd000','#ffffff'],
            'cart_hqc': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'cart_oi': ['#ffffff','#323232','#ffffff','#ffffff','#ffffff','#ffffff'],
            'cart_ot': ['#ffffff','#323232','#ffffff','#ffffff','#ffffff','#ffffff'],
            'cart_oi_hover': ['#4de3dd','#1fc4b2','#4de3dd','#4de3dd','#ffd000','#ffffff'],
            'cart_ot_hover': ['#4de3dd','#1fc4b2','#4de3dd','#4de3dd','#ffd000','#ffffff'],
            'cart_oqbg': ['#1d1d1d','#1fc4b2','#00bda0','#00bda0','#ffd000','#ffffff'],
            'cart_oqc': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'c_popup_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'c_popup_bc': ['#eeeeee','#eeeeee','#ff1b71','#eeeeee','#eeeeee','#eeeeee'],
            'c_title_color': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'c_product_name': ['#000000','#000000','#000000','#000000','#000000','#000000'],
            'c_product_name_hover': ['#1d1d1d','#1fc4b2','#6264ff','#ff8e32','#299ae6','#f93d60'],
            'c_product_price': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'c_product_remove': ['#cccccc','#cccccc','#cccccc','#cccccc','#cccccc','#cccccc'],
            'c_product_remove_hover': ['#000000','#000000','#000000','#000000','#000000','#000000'],
            'c_product_separator': ['#eeeeee','#eeeeee','#eeeeee','#eeeeee','#eeeeee','#eeeeee'],
            'c_product_summary_title': ['#cccccc','#cccccc','#cccccc','#cccccc','#cccccc','#cccccc'],
            'c_product_summary_value': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'c_product_checkout': ['#323232','#323232','#6264ff','#323232','#323232','#323232'],
            'c_product_checkout_hover': ['#1d1d1d','#1fc4b2','#ff1b71','#ff8e32','#299ae6','#f93d60'],
            'lc_bg': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#177ec4','#000000'],
            'lc_c': ['#ffffff','#e5e5e5','#323232','#323232','#f3bb10','transparent'],
            'lc_cdone': ['#323232','#1fc4b2','#00bda0','#323232','#ffffff','#f93d60'],
            'lc_check': ['#323232','#999999','#999999','#999999','#ffffff','#ffffff'],
            'lc_checkdone': ['#ffffff','#ffffff','#ffffff','#ffffff','#299ae6','#ffffff'],
            'lc_close': ['#323232','#323232','#323232','#323232','#ffffff','#ffffff'],
            'o_ad_bg': ['#f7f7f7','#f7f7f7','#f7f7f7','#f7f7f7','#299ae6','#f7f7f7'],
            'o_ad_c': ['#323232','#323232','#323232','#292929','#ffffff','#323232'],
            'o_ad_sb': ['#1d1d1d','#00bda0','#00bda0','#ff8e32','#299ae6','#fcda3a'],
            'o_ad_boxbg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'o_ad_boxb': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#ffd000','#e5e5e5'],
            'o_ad_text': ['#aaaaaa','#aaaaaa','#aaaaaa','#cccccc','#aaaaaa','#aaaaaa'],
            'o_r_t': ['#ffffff','#ffffff','#ffffff','#292929','#ffffff','#000000'],
            'o_r_tu': ['#1d1d1d','#00bda0','#00bda0','#f7f7f7','#299ae6','#f7f7f7'],
            'o_del_bg': ['#ffffff','#ffffff','#ffffff','#424442','#ffffff','#ffffff'],
            'o_del_b': ['#e5e5e5','#e5e5e5','#e5e5e5','#424442','#ffd000','#e5e5e5'],
            'o_del_s': ['#323232','#323232','#323232','#f2f2f2','#323232','#323232'],
            'o_del_sep': ['#ededed','#ededed','#ededed','#525252','#ededed','#ededed'],
            'o_del_cbg': ['#ffffff','#ffffff','#ffffff','#525252','#ffffff','#ffffff'],
            'o_del_cb': ['#e5e5e5','#e5e5e5','#e5e5e5','#525252','#e5e5e5','#e5e5e5'],
            'o_del_cc': ['#323232','#323232','#323232','#f2f2f2','#323232','#323232'],
            'o_del_car': ['#000000','#000000','#000000','#f2f2f2','#000000','#000000'],
            'o_del_text': ['#aaaaaa','#aaaaaa','#aaaaaa','#cccccc','#aaaaaa','#aaaaaa'],
            'ma_required': ['#f13340','#f13340','#f13340','#f13340','#f13340','#f13340'],
            'ma_form': ['#fafafa','#fafafa','#fafafa','#323232','#2ea0ed','#fafafa'],
            'nc_ma_form_text': ['#323232','#323232','#323232','#ffffff','#ffffff','#525252'],
            'ma_title': ['#6d6d6d','#6d6d6d','#6d6d6d','#6d6d6d','#6d6d6d','#6d6d6d'],
            'ma_title_hover': ['#323232','#323232','#323232','#323232','#299ae6','#f93d60'],
            'ma_info': ['#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa'],
            'o_number_bg': ['#fafafa','#fafafa','#fafafa','#fafafa','#fafafa','#fafafa'],
            'o_number_color': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'o_number_border': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5'],
            'o_number_border_active': ['#1d1d1d','#00bda0','#00bda0','#ff8e32','#ffd000','#fcda3a'],
            'o_number_border_done': ['#1d1d1d','#00bda0','#00bda0','#ff8e32','#299ae6','#fcda3a'],
            'o_number_border_done_hover': ['#515151','#515151','#515151','#515151','#515151','#515151'],
            'o_number_title': ['#888888','#888888','#888888','#888888','#888888','#888888'],
            'o_number_bg_active': ['#1d1d1d','#00bda0','#00bda0','#ff8e32','#ffd000','#fcda3a'],
            'o_number_color_active': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'o_number_title_active': ['#000000','#000000','#000000','#000000','#000000','#000000'],
            'o_number_bg_done': ['#1d1d1d','#00bda0','#00bda0','#ff8e32','#299ae6','#fcda3a'],
            'o_number_color_done': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'o_number_title_done': ['#888888','#888888','#888888','#888888','#888888','#888888'],
            'o_number_bg_done_hover': ['#6d6d6d','#6d6d6d','#6d6d6d','#6d6d6d','#6d6d6d','#6d6d6d'],
            'o_number_color_done_hover': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'o_number_title_done_hover': ['#1d1d1d','#00bda0','#00bda0','#ff8e32','#299ae6','#000000'],
            'o_img_border': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'o_product_name': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'o_product_atts': ['#777777','#777777','#777777','#777777','#777777','#777777'],
            'o_remove': ['#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb'],
            'o_remove_hover': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'o_av_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'o_av_c': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'o_av_b': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'o_total_b': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'o_total_c': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'o_pay_item_bg': ['#ffffff','#ffffff','#ffffff','#424442','#ffffff','#ffffff'],
            'o_pay_item_b': ['#e5e5e5','#e5e5e5','#e5e5e5','#323232','#ffd000','#e5e5e5'],
            'o_pay_item_i': ['#1d1d1d','#00bda0','#00bda0','#ff8e32','#299ae6','#f93d60'],
            'nc_o_pay_item_bg_hover': ['#ffffff','#ffffff','#ffffff','#323232','#ffffff','#ffffff'],
            'nc_o_pay_item_b_hover': ['#1d1d1d','#00bda0','#00bda0','#ff6926','#299ae6','#000000'],
            'o_pay_item_title': ['#000000','#000000','#000000','#f2f2f2','#000000','#000000'],
            'o_pay_item_desc': ['#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa'],
            'o_pay_item_chevron': ['#000000','#000000','#000000','#ff8e32','#000000','#000000'],
            'bl_head': ['#323232','#323232','#323232','#323232','#323232','#000000'],
            'bl_htitle': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'bl_head_hover': ['#000000','#000000','#000000','#000000','#000000','#f93d60'],
            'bl_title_desc': ['#a8a8a8','#a8a8a8','#a8a8a8','#a8a8a8','#a8a8a8','#a8a8a8'],
            'bl_con': ['#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6','#d6d6d6'],
            'bl_conho': ['#323232','#323232','#323232','#323232','#323232','#f93d60'],
            'bl_dec': ['#ededed','#ededed','#ededed','#ededed','#ededed','#fcda3a'],
            'bl_info': ['#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#dddddd'],
            'bl_info_val': ['#777777','#777777','#777777','#777777','#777777','#777777'],
            'bl_info_hval': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'bl_desc': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'bl_date_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'bl_date_border': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'bl_date_color': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'bl_rm_icon': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'bl_rm_bg_icon': ['#ffffff','#1fc4b2','#ffffff','#ffffff','#ffffff','#f93d60'],
            'bl_rm_border_icon': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'bl_rm_color': ['#1d1d1d','#009383','#6264ff','#ff6926','#299ae6','#f93d60'],
            'bl_rm_hover': ['#323232','#323232','#323232','#323232','#299ae6','#ffffff'],
            'bl_title': ['#323232','#323232','#323232','#323232','323232#','#323232'],
            'bl_title_hover': ['#555555','#555555','#555555','#555555','#299ae6','#f93d60'],
            'bl_bg_underline': ['#ebebeb','#ebebeb','#ebebeb','#ebebeb','#ebebeb','#ebebeb'],
            'bl_meta': ['#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb'],
            'bl_metal': ['#777777','#777777','#777777','#777777','#777777','#777777'],
            'bl_meta_hover': ['#000000','#000000','#000000','#000000','#299ae6','#f93d60'],
            'bl_info_title': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'bl_c_title': ['#555555','#555555','#555555','#555555','#555555','#555555'],
            'bl_c_underline': ['#ebebeb','#ebebeb','#ebebeb','#ebebeb','#ebebeb','#ebebeb'],
            'bl_c_text': ['#777777','#777777','#777777','#777777','#777777','#777777'],
            'bl_c_meta': ['#999999','#999999','#999999','#999999','#999999','#999999'],
            'bl_c_reply': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'bl_c_hreply': ['#777777','#777777','#777777','#777777','#299ae6','#f93d60'],
            'bl_pdesc': ['#555555','#555555','#555555','#555555','#555555','#555555'],
            'footer_bg_sec': ['#1d1d1d','#323232','#ffffff','#323232','#292929','#ffffff'],
            'footer_b_sec': ['#323232','#474747','#f2f2f2','#474747','#474747','#fafafa'],
            'footer_news_input_colorp': ['#575757','#575757','#aaaaaa','#575757','#575757','#575757'],
            'footer_news_input_color': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'footer_news_input': ['#ffffff','#ffffff','#f2f2f2','#ffffff','#ffffff','#ffffff'],
            'footer_news_button': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#000000'],
            'footer_news_button_bg': ['#1d1d1d','#1fc4b2','#00e295','#ff6926','#299ae6','#fcda3a'],
            'footer_news_button_border': ['#323232','#323232','#00e295','#323232','#299ae6','#fcda3a'],
            'footer_news_buttonh': ['#1d1d1d','#ffffff','#ffffff','#ffffff','#000000','#fcda3a'],
            'footer_news_button_bgh': ['#ffffff','#009383','#ff1b71','#ff8e32','#ffd000','#000000'],
            'footer_news_button_borderh': ['#323232','#323232','#ff1b71','#323232','#292929','#000000'],
            'footer_background_color': ['#1d1d1d','#323232','#ffffff','#323232','#292929','#ffffff'],
            'footer_txt_color': ['#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb','#bbbbbb'],
            'footer_link_color': ['#bbbbbb','#bbbbbb','#292929','#bbbbbb','#bbbbbb','#737373'],
            'footer_hover_color': ['#ffffff','#ffffff','#6264ff','#ffffff','#ffd000','#000000'],
            'footer_slash': ['#888888','#888888','#888888','#888888','#888888','#f2f2f2'],
            'footer_social': ['#ffffff','#ffffff','#6264ff','#ff8e32','#ffffff','#676767'],
            'footer_bottom_text': ['#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#323232'],
            'footer_map_bg': ['#1d1d1d','#464646','#464646','#464646','#ffd000','#fafafa'],
            'footer_map_bc': ['#323232','#282828','#282828','#282828','#ffd000','#fafafa'],
            'footer_map_ic': ['#e5e5e5','#e5e5e5','#6264ff','#ff8e32','#323232','#000000'],
            'nc_pp_add_bg': ['#323232','#ffffff','#6264ff','#ff6926','#ffd000','#000000'],
            'nc_pp_add_border': ['#323232','#1fc4b2','#6264ff','#ff6926','#ffd000','#000000'],
            'nc_pp_add_color': ['#ffffff','#000000','#ffffff','#ffffff','#323232','#ffffff'],
            'nc_footer_news_border': ['#323232','#323232','#f2f2f2','#323232','#ffffff','#e5e5e5'],
            'nc_loader_bg': ['#ffffff','#ffffff','#ffffff','#323232','#299ae6','#f93d60'],
            'nc_loader_color': ['#323232','#009383','#6264ff','#000000','#ffffff','#ffffff'],
            'nc_loader_color2': ['#323232','#1fc4b2','#6264ff','#ff8e32','#ffd000','#ffffff'],
            'nc_loader_colors': ['#000000','#000000','#000000','#000000','#000000','#000000'],
            'nc_c_title_hover': ['#4b5363','#4b5363','#4b5363','#4b5363','#4b5363','#4b5363'],
            'nc_soldout_bg': ['#323232','#323232','#f2f2f2','#323232','#323232','#323232'],
            'nc_soldout_border': ['#323232','#323232','#f2f2f2','#323232','#323232','#323232'],
            'nc_soldout_color': ['#ffffff','#ffffff','#000000','#ffffff','#ffffff','#ffffff'],
            'nc_g_body_text': ['#4a4e51','#4a4e51','#4a4e51','#4a4e51','#4a4e51','#4a4e51'],
            'nc_footer_headings': ['#e1e1e1','#e1e1e1','#292929','#e1e1e1','#e1e1e1','#bbbbbb'],
            'nc_f_underline': ['#424242','#555555','#f2f2f2','#555555','#525252','#fafafa'],
            'nc_man_logobg': ['#323232','#fafafa','#ffffff','#fafafa','#fafafa','#fafafa'],
            'nc_count_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'nc_count_color': ['#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa','#aaaaaa'],
            'nc_count_time': ['#323232','#323232','#000000','#323232','#000000','#323232'],
            'nc_count_pr_title': ['#ffffff','#ffffff','#ffffff','#323232','#000000','#000000'],
            'nc_count_pr_titlebg': ['#1d1d1d','#1fc4b2','#6264ff','#fafafa','#ffd000','#fcda3a'],
            'nc_count_pr_bg': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'nc_count_pr_color': ['#888888','#888888','#888888','#888888','#000000','#888888'],
            'nc_count_pr_sep': ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5'],
            'nc_m_under_color': ['#424242','#323232','#323232','#323232','#323232','#ffffff'],
            'nc_m_under_colors': ['#222222','#ffffff','#ffffff','#323232','#ffffff','#ffffff'],
            'nc_m_under_coloro': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'nc_m_under_colorh': ['#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff'],
            'nc_footer_bch': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'nc_colors_b': ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            'nc_colors_bh': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            "nc_colors_b": ['#ededed','#ededed','#ededed','#ededed','#ededed','#ededed'],
            "nc_colors_bh": ['#323232','#323232','#323232','#323232','#323232','#323232'],
            "nc_guest_bc": ['#323232','#323232','#323232','#323232','#323232','#323232'],
            "nc_guest_bb": ['#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5','#e5e5e5'],
            "nc_guest_bch": ['#000000','#000000','#000000','#000000','#000000','#000000'],
            "nc_guest_bbh": ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'nc_mobadotsc': ['#323232','#323232','#323232','#323232','#323232','#323232'],
            'nc_mobaarrowsc': ['#cccccc','#cccccc','#cccccc','#cccccc','#cccccc','#cccccc']
        }

        var keys = Object.keys(scheme_colors);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var value = scheme_colors[key][scheme_number - 1];
            var name = $("[name='" + key + "']");

            $(name).each(function() {
                $(this).val(value)
            });
        }

        $('.colors_apply').html('<i></i>Color scheme changed, click save changes to save it');

    });






    // custom scheme

    var csbase = function(){
        if($('#cs_base1').is(':checked')) {
            $('.cs_circle_base').css('background','#ffffff');
        }
        if($('#cs_base2').is(':checked')) {
            $('.cs_circle_base').css('background','#fafafa');
        }
        if($('#cs_base3').is(':checked')) {
            $('.cs_circle_base').css('background','#31302f');
        }
        if($('#cs_base4').is(':checked')) {
            $('.cs_circle_base').css('background','#24282a');
        }
    };
    csbase();

    $('.cs_base').click(function() {
        csbase();
    });

    $('.cs_apply').click(function() {

        var mainvalue = $('.cs_circle_main').val();
        var secvalue = $('.cs_circle_second').val();
        if ($('#cs_base1').is(':checked')) var basevalue = '1';
        if ($('#cs_base2').is(':checked')) var basevalue = '2';
        if ($('#cs_base3').is(':checked')) var basevalue = '3';
        if ($('#cs_base4').is(':checked')) var basevalue = '4';

        $('.cs_mc').each(function() {
            $(this).val(mainvalue)
        });
        $('.cs_sc').each(function() {
            $(this).val(secvalue)
        });

        // Base colors massive
        var basecolors = {
         'header_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'header_bgs': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'm_link': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'm_links': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'm_icon': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'm_popup_bg': ['#ffffff', '#323232', '#ffffff', '#ffffff'],
         'm_popup_bc': ['#ffffff', '#323232', '#ffffff', '#ffffff'],
         'm_popup_link': ['#323232', '#ffffff', '#323232', '#000000'],
         'm_popup_chevron': ['#555555', '#e5e5e5', '#555555', '#555555'],
         'm_popup_llink': ['#323232', '#ffffff', '#323232', '#000000'],
         'm_popup_lchevron': ['#555555', '#e5e5e5', '#555555', '#555555'],
         'm_popup_lbg': ['#ffffff', '#323232', '#ffffff', '#ffffff'],
         'm_popup_lbg_hover': ['#ffffff', '#323232', '#ffffff', '#ffffff'],
         'm_popup_lborder': ['#e5e5e5', '#000000', '#e5e5e5', '#e5e5e5'],
         'm_popup_lborder': ['#e5e5e5', '#000000', '#e5e5e5', '#e5e5e5'],
         'm_popup_mtitle': ['#555555', '#fafafa', '#555555', '#555555'],
         'm_popup_mlink': ['#323232', '#ffffff', '#323232', '#000000'],
         'm_popup_mchevron': ['#555555', '#e5e5e5', '#555555', '#555555'],
         'm_popup_mname': ['#323232', '#ffffff', '#323232', '#000000'],
         'm_popup_mprice': ['#323232', '#ffffff', '#323232', '#000000'],
         'search_i': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'search_t': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'search_si': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'search_st': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'search_bg': ['#ffffff', '#ffffff', '#ffffff', '#1e262a'],
         'search_type': ['#cccccc', '#cccccc', '#cccccc', '#9ca3a6'],
         'search_input': ['#555555', '#555555', '#555555', '#eeeeee'],
         'search_res': ['#555555', '#555555', '#555555', '#dddddd'],
         'search_res_b': ['#f2f2f2', '#323232', '#f2f2f2', '#39464c'],
         'search_line': ['#555555', '#555555', '#555555', '#dddddd'],
         'search_line': ['#555555', '#555555', '#555555', '#dddddd'],
         'search_icon': ['#323232', '#323232', '#323232', '#eeeeee'],
         'search_inputh': ['#323232', '#323232', '#323232', '#ffffff'],
         'search_lineh': ['#323232', '#323232', '#323232', '#ffffff'],
         'search_close': ['#313743', '#313743', '#313743', '#dddddd'],
         'cart_i': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'cart_t': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'cart_si': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'cart_si': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'c_popup_bg': ['#ffffff', '#323232', '#ffffff', '#ffffff'],
         'c_title_color': ['#323232', '#eeeeee', '#323232', '#000000'],
         'c_product_summary_title': ['#323232', '#eeeeee', '#323232', '#000000'],
         'c_product_summary_value': ['#323232', '#eeeeee', '#323232', '#000000'],
         'c_product_checkout': ['#323232', '#eeeeee', '#323232', '#000000'],
         'c_product_price': ['#323232', '#dddddd', '#323232', '#000000'],
         'c_product_name': ['#777777', '#cccccc', '#777777', '#777777'],
         'c_product_name_hover': ['#000000', '#ffffff', '#000000', '#000000'],
         'c_product_remove': ['#999999', '#cccccc', '#999999', '#999999'],
         'c_product_remove_hover': ['#000000', '#ffffff', '#000000', '#000000'],
         'c_product_separator': ['#e5e5e5', '#555555', '#e5e5e5', '#e5e5e5'],
         'acc_i': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'acc_t': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'acc_si': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'acc_si': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'cl_popup_bg': ['#ffffff', '#323232', '#ffffff', '#ffffff'],
         'ta_welcome': ['#777777', '#cccccc', '#777777', '#777777'],
         'cl_text': ['#777777', '#cccccc', '#777777', '#777777'],
         'cl_link': ['#555555', '#dddddd', '#555555', '#555555'],
         'cl_lc': ['#555555', '#dddddd', '#555555', '#555555'],
         'cl_sep': ['#e5e5e5', '#555555', '#e5e5e5', '#e5e5e5'],
         'cl_link_hover': ['#000000', '#ffffff', '#000000', '#000000'],
         'cl_lc_hover': ['#000000', '#ffffff', '#000000', '#000000'],
         'mob_bg': ['#ffffff', '#323232', '#323232', '#1e262a'],
         'mob_text': ['#777777', '#cccccc', '#cccccc', '#cccccc'],
         'mob_links': ['#323232', '#dddddd', '#dddddd', '#dddddd'],
         'mob_sep': ['#e5e5e5', '#555555', '#555555', '#39464c'],
         'mob_m_t': ['#323232', '#cccccc', '#cccccc', '#cccccc'],
         'mob_m_link': ['#000000', '#ffffff', '#ffffff', '#ffffff'],
         'mob_m_linke': ['#323232', '#dddddd', '#dddddd', '#dddddd'],
         'mob_m_linkb': ['#eeeeee', '#555555', '#555555', '#39464c'],
         'mob_m_tbg': ['#fafafa', '#555555', '#555555', '#39464c'],
         'mob_m_bg': ['#ffffff', '#323232', '#323232', '#1e262a'],
         'levi_up_bg_hover': ['#323232', '#323232', '#fafafa', '#ffffff'],
         'levi_up_b_hover': ['#323232', '#323232', '#fafafa', '#ffffff'],
         'levi_up': ['#ffffff', '#ffffff', '#000000', '#ffffff'],
         'levi_up_hover': ['#ffffff', '#ffffff', '#000000', '#1e262a'],
         'levi_f_p_bg': ['#3B5998', '#3B5998', '#3B5998', '#3B5998'],
         'levi_f_p_text': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'levi_f_bg': ['#f2f2f2', '#f2f2f2', '#fafafa', '#6d777c'],
         'levi_f_b': ['#f2f2f2', '#f2f2f2', '#fafafa', '#6d777c'],
         'levi_f_c': ['#323232', '#323232', '#000000', '#ffffff'],
         'levi_f_bg_hover': ['#3B5998', '#3B5998', '#3B5998', '#3B5998'],
         'levi_f_b_hover': ['#3B5998', '#3B5998', '#3B5998', '#3B5998'],
         'levi_f_c_hover': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'levi_m_bg': ['#f2f2f2', '#f2f2f2', '#fafafa', '#6d777c'],
         'levi_m_b': ['#f2f2f2', '#f2f2f2', '#fafafa', '#6d777c'],
         'levi_m': ['#323232', '#323232', '#000000', '#ffffff'],
         'levi_m_bg_hover': ['#ffc018', '#ffc018', '#ffc018', '#ffc018'],
         'levi_m_b_hover': ['#ffc018', '#ffc018', '#ffc018', '#ffc018'],
         'levi_m_hover': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'nc_g_body_text': ['#4a4e51', '#4a4e51', '#dddddd', '#dddddd'],
         'g_body_link': ['#323232', '#323232', '#f2f2f2', '#f2f2f2'],
         'g_body_link_hover': ['#000000', '#000000', '#ffffff', '#ffffff'],
         'g_table_bg': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'g_table_lbg': ['#fafafa', '#fafafa', '#323232', '#1e262a'],
         'g_table_title_bg': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'g_table_b': ['#ededed', '#f2f2f2', '#555555', '#39464c'],
         'g_table_rc': ['#777777', '#555555', '#dddddd', '#cccccc'],
         'g_table_lc': ['#6d6d6d', '#323232', '#eeeeee', '#eeeeee'],
         'g_table_title_color': ['#999999', '#999999', '#999999', '#bcc7cc'],
         'g_box': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'g_boxb': ['#e5e5e5', '#dddddd', '#777777', '#4c5a60'],
         'g_box_title': ['#222222', '#222222', '#fafafa', '#fafafa'],
         'g_box_title_border': ['#ededed', '#ededed', '#555555', '#39464c'],
         'g_label': ['#323232', '#323232', '#dddddd', '#dddddd'],
         'g_checkbox_label': ['#777777', '#777777', '#cccccc', '#cccccc'],
         'g_form': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'g_form_grey': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'g_cc': ['#d6d6d6', '#d6d6d6', '#999999', '#5e7c8b'],
         'g_ch': ['#323232', '#323232', '#dddddd', '#dddddd'],
         'g_hb': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'g_hc': ['#ffffff', '#ffffff', '#323232', '#000000'],
         'g_fancy_bg': ['#999999', '#999999', '#555555', '#39464c'],
         'g_fancy_nbg': ['#323232', '#323232', '#222222', '#1e262a'],
         'g_fancy_nc': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'g_fancy_cibg': ['#555555', '#555555', '#222222', '#1e262a'],
         'g_fancy_cic': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'g_mc_bg': ['#ffffff', '#f7f7f7', '#323232', '#1e262a'],
         'g_bc_bg': ['#ffffff', '#f7f7f7', '#323232', '#1e262a'],
         'g_hp1_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'g_hp2_bg': ['#ffffff', '#f7f7f7', '#323232', '#1e262a'],
         'g_hp3_bg': ['#ffffff', '#f7f7f7', '#323232', '#1e262a'],
         'g_hp4_bg': ['#f7f7f7', '#ffffff', '#222222', '#39464c'],
         'g_hp5_bg': ['#ffffff', '#f7f7f7', '#323232', '#1e262a'],
         'g_hp6_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'g_fs1_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'g_fs2_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'g_fs3_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'g_op1_bg': ['#ffffff', '#fafafa', '#222222', '#39464c'],
         'g_op2_bg': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'g_hp5_gs': ['#eeeeee', '#fcfcfc', '#525252', '#39464c'],
         'g_hp5_ge': ['#eeeeee', '#fcfcfc', '#525252', '#39464c'],
         'nc_loader_bg': ['#ffffff', '#f7f7f7', '#323232', '#1e262a'],
         'nc_loader_color': ['#323232', '#323232', '#f2f2f2', '#f2f2f2'],
         'nc_loader_color2': ['#323232', '#323232', '#f2f2f2', '#f2f2f2'],
         'nc_loader_colors': ['#000000', '#000000', '#ffffff', '#ffffff'],
         'hp_title': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'hp_title_hover': ['#000000', '#000000', '#ffffff', '#ffffff'],
         'hp_desc': ['#a8a8a8', '#a8a8a8', '#999999', '#81949d'],
         'hp_con': ['#d6d6d6', '#d6d6d6', '#999999', '#5e7c8b'],
         'hp_conho': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'hp_dec': ['#ededed', '#ededed', '#555555', '#39464c'],
         'mini_titles': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'mini_sep': ['#ededed', '#ededed', '#555555', '#39464c'],
         'mini_con': ['#d6d6d6', '#d6d6d6', '#999999', '#5e7c8b'],
         'mini_pn': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'mini_pd': ['#c7c7c7', '#c7c7c7', '#999999', '#81949d'],
         'mini_pop': ['#cccccc', '#cccccc', '#999999', '#81949d'],
         'info_iconbg': ['#fafafa', '#fafafa', '#525252', '#1e262a'],
         'info_title': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'info_stitle': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'info_text': ['#676767', '#676767', '#999999', '#81949d'],
         'brand_p_con': ['#d6d6d6', '#d6d6d6', '#999999', '#5e7c8b'],
         'b_title': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'b_text': ['#ababab', '#ababab', '#999999', '#81949d'],
         'b_link': ['#888888', '#888888', '#bbbbbb', '#adb7bc'],
         'b_link_hover': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'b_separator': ['#dddddd', '#dddddd', '#999999', '#5e7c8b'],
         'b_border': ['#f2f2f2', '#e5e5e5', '#222222', '#39464c'],
         'cms_title': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'cms_title_border': ['#ededed', '#e5e5e5', '#555555', '#39464c'],
         'page_headings': ['#555555', '#555555', '#dddddd', '#d9e2e6'],
         'page_text_color': ['#4a4e51', '#4a4e51', '#eeeeee', '#ebf6fc'],
         'page_link': ['#888888', '#888888', '#cccccc', '#adb7bc'],
         'page_link_hover': ['#525252', '#525252', '#dddddd', '#ebf6fc'],
         'page_bq_title': ['#525252', '#525252', '#dddddd', '#ebf6fc'],
         'page_bq_name': ['#aaaaaa', '#888888', '#bbbbbb', '#96a2a8'],
         'page_bq_q': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'warning_message_color': ['#e7b918', '#e7b918', '#e7b918', '#e7b918'],
         'success_message_color': ['#48b151', '#48b151', '#48b151', '#48b151'],
         'danger_message_color': ['#DB7769', '#DB7769', '#DB7769', '#DB7769'],
         'danger_message_color': ['#DB7769', '#DB7769', '#DB7769', '#DB7769'],
         'alert_bg': ['#fafafa', '#fefefe', '#525252', '#39464c'],
         'alert_border': ['#e5e5e5', '#e1e1e1', '#323232', '#1e262a'],
         'alert_color': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'alert_link': ['#555555', '#555555', '#eeeeee', '#d2dee4'],
         'alert_hlink': ['#000000', '#000000', '#ffffff', '#ffffff'],
         'alert_icon': ['#cccccc', '#cccccc', '#cccccc', '#ffffff'],
         'alert_icon_bg': ['#ffffff', '#ffffff', '#777777', '#6b787e'],
         'contact_form_bg': ['#fafafa', '#ffffff', '#525252', '#39464c'],
         'contact_form_border': ['#fafafa', '#ffffff', '#525252', '#39464c'],
         'contact_form_title': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'contact_form_under': ['#e5e5e5', '#e5e5e5', '#424242', '#546065'],
         'contact_icon': ['#777777', '#555555', '#f2f2f2', '#ebf6fc'],
         'contact_icon_sec': ['#cccccc', '#cccccc', '#525252', '#39464c'],
         'sidebar_title_border': ['#ededed', '#ededed', '#525252', '#39464c'],
         'sidebar_title_color': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_title_link': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_title_link_hover': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_block_content_bg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'sidebar_block_content_border': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'sidebar_block_text_color': ['#777777', '#555555', '#cccccc', '#87949a'],
         'sidebar_block_link': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_block_link_hover': ['#000000', '#000000', '#ffffff', '#ffffff'],
         'sidebar_item_separator': ['#f2f2f2', '#eeeeee', '#626262', '#4c5a60'],
         'sidebar_product_desc': ['#bbbbbb', '#bbbbbb', '#999999', '#81949d'],
         'sidebar_product_price': ['#444444', '#444444', '#dedede', '#ebf6fc'],
         'sidebar_product_oprice': ['#bbbbbb', '#bbbbbb', '#999999', '#81949d'],
         'sidebar_button_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'sidebar_button_border': ['#ededed', '#323232', '#777777', '#4c5a60'],
         'sidebar_button_color': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_button_hbg': ['#323232', '#323232', '#525252', '#39464c'],
         'sidebar_button_hborder': ['#323232', '#323232', '#525252', '#39464c'],
         'sidebar_button_hcolor': ['#ffffff', '#ffffff', '#f2f2f2', '#ebf6fc'],
         'sidebar_c': ['#d6d6d6', '#d6d6d6', '#999999', '#5e7c8b'],
         'sidebar_hc': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_categories_bg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'sidebar_categories_title': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_categories_item': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_categories_ex': ['#cccccc', '#cccccc', '#999999', '#81949d'],
         'sidebar_categories_titlebg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'sidebar_categories_item_hover': ['#555555', '#555555', '#ffffff', '#ffffff'],
         'sidebar_categories_exa': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_categories_separator': ['#ededed', '#ededed', '#525252', '#39464c'],
         'pl_filter_bg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'pl_filter_bc': ['#ededed', '#e5e5e5', '#525252', '#39464c'],
         'pl_filter_t': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pl_filter_range': ['#efefef', '#efefef', '#676767', '#49555b'],
         'pl_filter_handle_border': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'pl_filter_separator': ['#ededed', '#ededed', '#777777', '#4c5a60'],
         'pl_filter_brange': ['#dddddd', '#dddddd', '#676767', '#49555b'],
         'pl_filter_range_out': ['#e5e5e5', '#e5e5e5', '#676767', '#49555b'],
         'pl_filter_e': ['#cccccc', '#cccccc', '#999999', '#81949d'],
         'sidebar_tags_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'sidebar_tags_border': ['#ededed', '#323232', '#777777', '#4c5a60'],
         'sidebar_tags_border': ['#ededed', '#323232', '#777777', '#4c5a60'],
         'sidebar_tags_color': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'sidebar_tags_hbg': ['#323232', '#323232', '#525252', '#39464c'],
         'sidebar_tags_hborder': ['#323232', '#323232', '#525252', '#39464c'],
         'sidebar_tags_hcolor': ['#ffffff', '#ffffff', '#f2f2f2', '#ebf6fc'],
         'pl_item_bg': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'pl_item_border': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'nc_pl_item_borderh': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'pl_product_name': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pl_list_description': ['#c7c7c7', '#c7c7c7', '#999999', '#81949d'],
         'pl_product_oldprice': ['#cccccc', '#cccccc', '#999999', '#81949d'],
         'pl_product_percent': ['#f13340', '#f13340', '#ffcc00', '#ffcc00'],
         'pl_list_separator': ['#f2f2f2', '#f2f2f2', '#525252', '#39464c'],
         'pl_product_cart': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'pl_product_cart_color': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'pl_product_cart_hover_color': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'pl_product_sale_bg': ['#323232', '#323232', '#525252', '#39464c'],
         'pl_product_sale_border': ['#323232', '#323232', '#525252', '#39464c'],
         'pl_nav_top_bg': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'pl_nav_top_border': ['#ededed', '#e5e5e5', '#525252', '#39464c'],
         'pl_nav_grid': ['#aaaaaa', '#aaaaaa', '#aaaaaa', '#81949d'],
         'pl_nav_grid_bg': ['#ffffff', '#ffffff', '#424242', '#39464c'],
         'pl_nav_grid_b': ['#eeeeee', '#eeeeee', '#777777', '#4c5a60'],
         'pl_nav_grid_bga': ['#fafafa', '#fafafa', '#525252', '#4c5a60'],
         'pl_nav_grid_ba': ['#eeeeee', '#eeeeee', '#777777', '#4c5a60'],
         'pl_nav_compare_border': ['#fafafa', '#fafafa', '#525252', '#39464c'],
         'pl_show_items': ['#ababab', '#ababab', '#999999', '#81949d'],
         'pl_nav_sort': ['#888888', '#888888', '#cccccc', '#adb7bc'],
         'pl_nav_sorti': ['#ededed', '#e5e5e5', '#525252', '#39464c'],
         'pl_nav_bot_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'pl_nav_bot_border': ['#ededed', '#e5e5e5', '#525252', '#39464c'],
         'pl_number_color': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pl_number_bg': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'pl_number_b': ['#ededed', '#e5e5e5', '#525252', '#39464c'],
         'pl_show_per_page': ['#888888', '#888888', '#cccccc', '#adb7bc'],
         'pl_number_color_hover': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'pp_img_border': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'pp_icon_border': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'pp_icon_border_hover': ['#323232', '#323232', '#555555', '#81949d'],
         'pp_social_color': ['#888888', '#777777', '#cccccc', '#adb7bc'],
         'pp_loyt': ['#aaaaaa', '#888888', '#bbbbbb', '#96a2a8'],
         'pp_loys': ['#777777', '#777777', '#cccccc', '#adb7bc'],
         'pp_loyb': ['#ededed', '#e5e5e5', '#525252', '#39464c'],
         'pp_useful_color_hover': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pp_name': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pp_desc': ['#999999', '#999999', '#cccccc', '#adb7bc'],
         'pp_nav': ['#bbbbbb', '#bbbbbb', '#eeeeee', '#81949d'],
         'pp_navbg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'pp_navb': ['#ededed', '#ededed', '#525252', '#39464c'],
         'pp_price_coloro': ['#bbbbbb', '#bbbbbb', '#eeeeee', '#81949d'],
         'pp_info_label': ['#999999', '#999999', '#cccccc', '#adb7bc'],
         'pp_info_value': ['#6d6d6d', '#6d6d6d', '#aaaaaa', '#6e7e86'],
         'pp_lic': ['#cccccc', '#cccccc', '#999999', '#81949d'],
         'pp_libg': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'pp_lib': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'pp_att_bg': ['#f7f7f7', '#ffffff', '#525252', '#39464c'],
         'pp_att_b': ['#f7f7f7', '#e5e5e5', '#525252', '#39464c'],
         'pp_att_label': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pp_att_color': ['#e5e5e5', '#e5e5e5', '#777777', '#4c5a60'],
         'pp_att_color_active': ['#323232', '#323232', '#cccccc', '#adb7bc'],
         'pp_att_quan_s': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'nc_pp_add_color': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'pp_att_quan_input_bg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'pp_att_quan_input_b': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'pp_att_quan_input_c': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pp_att_quan_pm_bg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'pp_att_quan_pm_b': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'pp_att_quan_pm_color': ['#cccccc', '#cccccc', '#999999', '#81949d'],
         'pp_att_quan_pm_bg_hover': ['#ffffff', '#ffffff', '#626262', '#4c5a60'],
         'pp_att_quan_pm_bh': ['#323232', '#323232', '#999999', '#81949d'],
         'pp_att_quan_pm_color_hover': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pp_att_wbg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'pp_att_wb': ['#ededed', '#e5e5e5', '#777777', '#4c5a60'],
         'pp_att_wc': ['#ababab', '#ababab', '#999999', '#81949d'],
         'pp_att_wbgh': ['#ffffff', '#ffffff', '#424242', '#4c5a60'],
         'pp_att_wbh': ['#323232', '#323232', '#999999', '#81949d'],
         'pp_att_wch': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'nc_count_pr_title': ['#ffffff', '#ffffff', '#eeeeee', '#adb7bc'],
         'nc_count_pr_titlebg': ['#323232', '#323232', '#525252', '#39464c'],
         'nc_count_pr_bg': ['#f7f7f7', '#ffffff', '#525252', '#39464c'],
         'nc_count_pr_color': ['#888888', '#777777', '#cccccc', '#adb7bc'],
         'nc_count_pr_sep': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'pp_reviews_tbg': ['#f7f7f7', '#ffffff', '#525252', '#39464c'],
         'pp_reviews_tb': ['#f7f7f7', '#ffffff', '#525252', '#39464c'],
         'pp_reviews_tc': ['#aaaaaa', '#888888', '#bbbbbb', '#96a2a8'],
         'pp_reviews_name': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pp_reviews_date': ['#aaaaaa', '#888888', '#bbbbbb', '#96a2a8'],
         'pp_reviews_post_bg': ['#f7f7f7', '#ffffff', '#525252', '#39464c'],
         'pp_reviews_color': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'pp_reviews_use_separator': ['#ededed', '#e5e5e5', '#525252', '#39464c'],
         'pp_reviews_useful': ['#bbbbbb', '#bbbbbb', '#eeeeee', '#81949d'],
         'pp_tabs_bg': ['#f2f2f2', '#f2f2f2', '#525252', '#39464c'],
         'pp_tabs_b': ['#eeeeee', '#eeeeee', '#525252', '#39464c'],
         'pp_tabs_color': ['#6d6d6d', '#6d6d6d', '#aaaaaa', '#6e7e86'],
         'pp_tabs_bg_hover': ['#ffffff', '#ffffff', '#525252', '#4c5a60'],
         'pp_tabs_bh': ['#e5e5e5', '#e5e5e5', '#525252', '#4c5a60'],
         'pp_tabs_color_hover': ['#555555', '#323232', '#cccccc', '#adb7bc'],
         'pp_tabs_bga': ['#ffffff', '#ffffff', '#525252', '#4c5a60'],
         'pp_tabs_ba': ['#e5e5e5', '#e5e5e5', '#525252', '#4c5a60'],
         'pp_tabs_ca': ['#323232', '#000000', '#f2f2f2', '#ebf6fc'],
         'pp_tabs_sheets_bg': ['#ffffff', '#ffffff', '#525252', '#4c5a60'],
         'pp_tabs_sb': ['#e5e5e5', '#e5e5e5', '#525252', '#4c5a60'],
         'pp_tabs_sheets_color': ['#777777', '#777777', '#f2f2f2', '#ebf6fc'],
         'pp_qw_bg': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'o_number_bg': ['#fafafa', '#ffffff', '#525252', '#39464c'],
         'o_number_border': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'o_number_color': ['#323232', '#000000', '#f2f2f2', '#ebf6fc'],
         'o_number_title': ['#888888', '#777777', '#cccccc', '#adb7bc'],
         'o_number_title_active': ['#000000', '#000000', '#f2f2f2', '#ebf6fc'],
         'o_number_title_done': ['#888888', '#777777', '#cccccc', '#adb7bc'],
         'o_number_bg_done_hover': ['#6d6d6d', '#6d6d6d', '#aaaaaa', '#6e7e86'],
         'o_number_border_done_hover': ['#515151', '#515151', '#aaaaaa', '#6e7e86'],
         'lc_c': ['#323232', '#323232', '#323232', '#1e262a'],
         'lc_cdone': ['#13c500', '#13c500', '#13c500', '#13c500'],
         'lc_bg': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'lc_check': ['#999999', '#999999', '#999999', '#81949d'],
         'lc_checkdone': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'lc_close': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'o_product_name': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'o_product_atts': ['#777777', '#777777', '#cccccc', '#adb7bc'],
         'o_remove': ['#bbbbbb', '#bbbbbb', '#777777', '#81949d'],
         'o_remove_hover': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'o_total_b': ['#323232', '#323232', '#525252', '##39464c'],
         'o_img_border': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'o_av_bg': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'o_av_b': ['#ffffff', '#fafafa', '#323232', '#1e262a'],
         'o_av_c': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'o_total_c': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'o_ad_bg': ['#fafafa', '#ffffff', '#525252', '#39464c'],
         'o_ad_c': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'o_ad_text': ['#aaaaaa', '#888888', '#bbbbbb', '#96a2a8'],
         'o_ad_boxbg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'o_ad_boxb': ['#e5e5e5', '#e5e5e5', '#525252', '#39464c'],
         'o_r_t': ['#ffffff', '#ffffff', '#cccccc', '#ebf6fc'],
         'o_del_bg': ['#ffffff', '#ffffff', '#525252', '#4c5a60'],
         'o_del_b': ['#e5e5e5', '#e5e5e5', '#525252', '#4c5a60'],
         'o_del_s': ['#323232', '#323232', '#eeeeee', '#ebf6fc'],
         'o_del_sep': ['#ededed', '#ededed', '#626262', '#6e7e86'],
         'o_del_car': ['#000000', '#000000', '#ffffff', '#ffffff'],
         'o_del_text': ['#aaaaaa', '#aaaaaa', '#bbbbbb', '#96a2a8'],
         'o_del_cbg': ['#ffffff', '#ffffff', '#626262', '#39464c'],
         'o_del_cb': ['#e5e5e5', '#e5e5e5', '#626262', '#39464c'],
         'o_del_cc': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'o_pay_item_bg': ['#ffffff', '#ffffff', '#424242', '#4c5a60'],
         'o_pay_item_b': ['#e5e5e5', '#e5e5e5', '#424242', '#4c5a60'],
         'nc_o_pay_item_bg_hover': ['#ffffff', '#ffffff', '#323232', '#1e262a'],
         'o_pay_item_title': ['#000000', '#000000', '#ffffff', '#ffffff'],
         'o_pay_item_desc': ['#aaaaaa', '#888888', '#bbbbbb', '#96a2a8'],
         'o_pay_item_chevron': ['#000000', '#000000', '#cccccc', '#adb7bc'],
         'ma_form': ['#fafafa', '#ffffff', '#525252', '#39464c'],
         'nc_ma_form_text': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'ma_required': ['#f13340', '#f13340', '#ffd200', '#ffd200'],
         'ma_title': ['#6d6d6d', '#6d6d6d', '#aaaaaa', '#6e7e86'],
         'ma_title_hover': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'ma_info': ['#aaaaaa', '#888888', '#bbbbbb', '#96a2a8'],
         'bl_head': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'bl_title_desc': ['#a8a8a8', '#a8a8a8', '#999999', '#81949d'],
         'bl_con': ['#d6d6d6', '#d6d6d6', '#999999', '#5e7c8b'],
         'bl_conho': ['#323232', '#323232', '#ffffff', '#ffffff'],
         'bl_dec': ['#ededed', '#ededed', '#555555', '#39464c'],
         'bl_htitle': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'bl_desc': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'bl_info': ['#bbbbbb', '#bbbbbb', '#777777', '#81949d'],
         'bl_info_val': ['#777777', '#777777', '#cccccc', '#adb7bc'],
         'bl_info_hval': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'bl_rm_color': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'bl_title': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'bl_title_hover': ['#555555', '#323232', '#cccccc', '#adb7bc'],
         'bl_bg_underline': ['#ededed', '#ededed', '#555555', '#39464c'],
         'bl_pdesc': ['#555555', '#323232', '#cccccc', '#adb7bc'],
         'bl_meta': ['#bbbbbb', '#bbbbbb', '#777777', '#81949d'],
         'bl_metal': ['#777777', '#777777', '#cccccc', '#adb7bc'],
         'bl_meta_hover': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'bl_info_title': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'bl_c_title': ['#555555', '#323232', '#cccccc', '#adb7bc'],
         'bl_c_underline': ['#ededed', '#ededed', '#555555', '#39464c'],
         'bl_c_text': ['#777777', '#777777', '#cccccc', '#adb7bc'],
         'bl_c_meta': ['#999999', '#999999', '#cccccc', '#adb7bc'],
         'bl_c_reply': ['#323232', '#323232', '#f2f2f2', '#ebf6fc'],
         'bl_c_hreply': ['#777777', '#777777', '#cccccc', '#adb7bc'],
         'footer_background_color': ['#323232', '#323232', '#222222', '#39464c'],
         'nc_f_underline': ['#555555', '#555555', '#555555', '#39464c'],
         'footer_b_sec': ['#474747', '#474747', '#474747', '#39464c'],
         'footer_bg_sec': ['#323232', '#323232', '#323232', '#1e262a'],
         'nc_footer_headings': ['#e1e1e1', '#e1e1e1', '#e1e1e1', '#ebf6fc'],
         'footer_txt_color': ['#bbbbbb', '#bbbbbb', '#bbbbbb', '#a3b0b6'],
         'footer_link_color': ['#bbbbbb', '#bbbbbb', '#bbbbbb', '#a3b0b6'],
         'footer_hover_color': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'footer_hover_color': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'footer_slash': ['#888888', '#888888', '#888888', '#6d777c'],
         'footer_social': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'footer_news_input': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'nc_footer_news_border': ['#323232', '#323232', '#323232', '#1e262a'],
         'footer_news_input_colorp': ['#575757', '#575757', '#575757', '#39464c'],
         'footer_news_input_color': ['#323232', '#323232', '#323232', '#1e262a'],
         'footer_news_button_border': ['#323232', '#323232', '#323232', '#1e262a'],
         'footer_news_button_borderh': ['#323232', '#323232', '#323232', '#1e262a'],
         'footer_news_button': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'footer_bottom_text': ['#aaaaaa', '#aaaaaa', '#aaaaaa', '#a3b0b6'],
         'footer_map_bg': ['#464646', '#464646', '#464646', '#474e52'],
         'footer_map_bc': ['#282828', '#282828', '#282828', '#2f3436'],
         'footer_map_ic': ['#e5e5e5', '#e5e5e5', '#e5e5e5', '#ebf6fc'],
         'b_normal_bg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'b_normal_border': ['#e5e5e5', '#dddddd', '#676767', '#6d777c'],
         'b_normal_color': ['#323232', '#323232', '#ffffff', '#ebf6fc'],
         'b_normal_color_hover': ['#ffffff', '#ffffff', '#ffffff', '#ffffff'],
         'i_bg': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'i_b_color': ['#dddddd', '#cccccc', '#676767', '#6d777c'],
         'i_color': ['#777777', '#777777', '#f2f2f2', '#d1dde4'],
         'i_bg_focus': ['#ffffff', '#ffffff', '#525252', '#39464c'],
         'i_b_focus': ['#000000', '#000000', '#999999', '#96a2a8'],
         'i_color_focus': ['#323232', '#323232', '#ffffff', '#ebf6fc'],
         'rc_bg': ['#ffffff', '#ffffff', '#676767', '#474e52'],
         'rc_border': ['#e5e5e5', '#dddddd', '#999999', '#81949d'],
         'g_dark': ['#000000', '#000000', '#ffffff', '#ffffff'],
         'b_to': ['#323232', '#323232', '#e5e5e5', '#ebf6fc'],
         'b_toh': ['#000000', '#000000', '#ffffff', '#ffffff']
        }

        var keys = Object.keys(basecolors);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var value = basecolors[key][basevalue - 1];
            var name = $("[name='" + key + "']");

            $(name).each(function() {
                $(this).val(value)
            });
        }

        $('.cs_apply').html('<i></i>Colors changed, click save changes to save it');

    });



    //positions
    $(function(){ //DOM Ready

        var order = $("#g_position_gridster").val();
        if (order && order.length) {
            var arr = order.split(",");
            var parent = $(".gridster.s_positions").find("ul");
            for (var i = arr.length - 1; i >= 0; i--) {
                var el = $("." + arr[i]);
                el.attr("data-row", i + 1);
                parent.prepend(el);
            }
        }

        $(".gridster ul").gridster({
            widget_base_dimensions: [600, 60],
            widget_margins: [0, 8],
            max_cols:1
        });

        $(".gridster ul li").draggable({
            stop: function( event, ui ) {
                var arr = [];
                var elements = $(".gridster.s_positions").find("li");
                $.each(elements, function(key, entry) {
                    var className = $(entry).attr('data-pos');
                    var row = parseInt($(entry).attr('data-row') - 1);
                    arr[row] = className;
                });

                var str = "";
                for (var i = 0; i < arr.length; i++) {
                    var s = arr[i];
                    str = str + s;
                    if (i != arr.length - 1)
                        str = str + ",";
                }
                $("#g_position_gridster").val(str);
            }
        });

    });


    // reset popup prevent
    $(".reset_container").click(function() {
        $(".reset_popup").css('top','0');
    });

    $(".no-button").click(function() {
        $(".reset_popup").css('top','100px');
    });

    //slider
    var nc_b_speed = $( "#nc_b_speed" ).val()
    $( "#slider_nc_b_speed" ).slider({
      range: "min",
      min: 50,
      max: 1000,
      step: 5,
      value: nc_b_speed,
      slide: function( event, ui ) {
        $( "#nc_b_speed" ).val( ui.value );
      }
    });
    $( "#nc_b_speed" ).val( $( "#slider_nc_b_speed" ).slider( "value" ) );


    //slider
    var nc_fw_menu = $( "#nc_fw_menu" ).val()
    $( "#slider_nc_fw_menu" ).slider({
      range: "min", min: 300, max: 700, step: 100, value: nc_fw_menu, slide: function( event, ui ) {
        $( "#nc_fw_menu" ).val( ui.value );
      }
    });
    var nc_fw_heading = $( "#nc_fw_heading" ).val()
    $( "#slider_nc_fw_heading" ).slider({
      range: "min", min: 300, max: 700, step: 100, value: nc_fw_heading, slide: function( event, ui ) {
        $( "#nc_fw_heading" ).val( ui.value );
      }
    });
    var nc_fw_but = $( "#nc_fw_but" ).val()
    $( "#slider_nc_fw_but" ).slider({
      range: "min", min: 300, max: 700, step: 100, value: nc_fw_but, slide: function( event, ui ) {
        $( "#nc_fw_but" ).val( ui.value );
      }
    });
    var nc_fw_hp = $( "#nc_fw_hp" ).val()
    $( "#slider_nc_fw_hp" ).slider({
      range: "min", min: 300, max: 700, step: 100, value: nc_fw_hp, slide: function( event, ui ) {
        $( "#nc_fw_hp" ).val( ui.value );
      }
    });
    var nc_fw_pn = $( "#nc_fw_pn" ).val()
    $( "#slider_nc_fw_pn" ).slider({
      range: "min", min: 300, max: 700, step: 100, value: nc_fw_pn, slide: function( event, ui ) {
        $( "#nc_fw_pn" ).val( ui.value );
      }
    });
    var nc_fw_bb = $( "#nc_fw_bb" ).val()
    $( "#slider_nc_fw_bb" ).slider({
      range: "min", min: 300, max: 700, step: 100, value: nc_fw_bb, slide: function( event, ui ) {
        $( "#nc_fw_bb" ).val( ui.value );
      }
    });
    var nc_fw_add = $( "#nc_fw_add" ).val()
    $( "#slider_nc_fw_add" ).slider({
      range: "min", min: 300, max: 700, step: 100, value: nc_fw_add, slide: function( event, ui ) {
        $( "#nc_fw_add" ).val( ui.value );
      }
    });
    var nc_fw_f = $( "#nc_fw_f" ).val()
    $( "#slider_nc_fw_f" ).slider({
      range: "min", min: 300, max: 700, step: 100, value: nc_fw_f, slide: function( event, ui ) {
        $( "#nc_fw_f" ).val( ui.value );
      }
    });
    var nc_fw_ct = $( "#nc_fw_ct" ).val()
    $( "#slider_nc_fw_ct" ).slider({
      range: "min", min: 300, max: 700, step: 100, value: nc_fw_ct, slide: function( event, ui ) {
        $( "#nc_fw_ct" ).val( ui.value );
      }
    });


    $( "#nc_b_speed" ).val( $( "#slider_nc_b_speed" ).slider( "value" ) );

    if($('#body_box_sw1').is(':checked')) { $('.if_body_box_bg').slideDown(500); } else { $('.if_body_box_bg').slideUp(500); }
    if($('#body_box_sw2').is(':checked')) { $('.if_body_box_gr').slideDown(500); } else { $('.if_body_box_gr').slideUp(500); }
    if($('#body_box_sw3').is(':checked')) { $('.if_body_box_im').slideDown(500); } else { $('.if_body_box_im').slideUp(500); }
    if($('#body_box_sw4').is(':checked')) { $('.if_body_box_te').slideDown(500); } else { $('.if_body_box_te').slideUp(500); }

    if($('#g_hp1_sw1').is(':checked')) { $('.if_g_hp1_bg').slideDown(500); } else { $('.if_g_hp1_bg').slideUp(500); }
    if($('#g_hp1_sw2').is(':checked')) { $('.if_g_hp1_gr').slideDown(500); } else { $('.if_g_hp1_gr').slideUp(500); }
    if($('#g_hp1_sw3').is(':checked')) { $('.if_g_hp1_im').slideDown(500); } else { $('.if_g_hp1_im').slideUp(500); }
    if($('#g_hp1_sw4').is(':checked')) { $('.if_g_hp1_te').slideDown(500); } else { $('.if_g_hp1_te').slideUp(500); }

    if($('#g_hp2_sw1').is(':checked')) { $('.if_g_hp2_bg').slideDown(500); } else { $('.if_g_hp2_bg').slideUp(500); }
    if($('#g_hp2_sw2').is(':checked')) { $('.if_g_hp2_gr').slideDown(500); } else { $('.if_g_hp2_gr').slideUp(500); }
    if($('#g_hp2_sw3').is(':checked')) { $('.if_g_hp2_im').slideDown(500); } else { $('.if_g_hp2_im').slideUp(500); }
    if($('#g_hp2_sw4').is(':checked')) { $('.if_g_hp2_te').slideDown(500); } else { $('.if_g_hp2_te').slideUp(500); }

    if($('#g_hp3_sw1').is(':checked')) { $('.if_g_hp3_bg').slideDown(500); } else { $('.if_g_hp3_bg').slideUp(500); }
    if($('#g_hp3_sw2').is(':checked')) { $('.if_g_hp3_gr').slideDown(500); } else { $('.if_g_hp3_gr').slideUp(500); }
    if($('#g_hp3_sw3').is(':checked')) { $('.if_g_hp3_im').slideDown(500); } else { $('.if_g_hp3_im').slideUp(500); }
    if($('#g_hp3_sw4').is(':checked')) { $('.if_g_hp3_te').slideDown(500); } else { $('.if_g_hp3_te').slideUp(500); }

    if($('#g_hp4_sw1').is(':checked')) { $('.if_g_hp4_bg').slideDown(500); } else { $('.if_g_hp4_bg').slideUp(500); }
    if($('#g_hp4_sw2').is(':checked')) { $('.if_g_hp4_gr').slideDown(500); } else { $('.if_g_hp4_gr').slideUp(500); }
    if($('#g_hp4_sw3').is(':checked')) { $('.if_g_hp4_im').slideDown(500); } else { $('.if_g_hp4_im').slideUp(500); }
    if($('#g_hp4_sw4').is(':checked')) { $('.if_g_hp4_te').slideDown(500); } else { $('.if_g_hp4_te').slideUp(500); }

    if($('#g_hp5_sw1').is(':checked')) { $('.if_g_hp5_bg').slideDown(500); } else { $('.if_g_hp5_bg').slideUp(500); }
    if($('#g_hp5_sw2').is(':checked')) { $('.if_g_hp5_gr').slideDown(500); } else { $('.if_g_hp5_gr').slideUp(500); }
    if($('#g_hp5_sw3').is(':checked')) { $('.if_g_hp5_im').slideDown(500); } else { $('.if_g_hp5_im').slideUp(500); }
    if($('#g_hp5_sw4').is(':checked')) { $('.if_g_hp5_te').slideDown(500); } else { $('.if_g_hp5_te').slideUp(500); }

    if($('#g_hp6_sw1').is(':checked')) { $('.if_g_hp6_bg').slideDown(500); } else { $('.if_g_hp6_bg').slideUp(500); }
    if($('#g_hp6_sw2').is(':checked')) { $('.if_g_hp6_gr').slideDown(500); } else { $('.if_g_hp6_gr').slideUp(500); }
    if($('#g_hp6_sw3').is(':checked')) { $('.if_g_hp6_im').slideDown(500); } else { $('.if_g_hp6_im').slideUp(500); }
    if($('#g_hp6_sw4').is(':checked')) { $('.if_g_hp6_te').slideDown(500); } else { $('.if_g_hp6_te').slideUp(500); }

    if($('#g_fs1_sw1').is(':checked')) { $('.if_g_fs1_bg').slideDown(500); } else { $('.if_g_fs1_bg').slideUp(500); }
    if($('#g_fs1_sw2').is(':checked')) { $('.if_g_fs1_gr').slideDown(500); } else { $('.if_g_fs1_gr').slideUp(500); }
    if($('#g_fs1_sw3').is(':checked')) { $('.if_g_fs1_im').slideDown(500); } else { $('.if_g_fs1_im').slideUp(500); }
    if($('#g_fs1_sw4').is(':checked')) { $('.if_g_fs1_te').slideDown(500); } else { $('.if_g_fs1_te').slideUp(500); }

    if($('#g_fs2_sw1').is(':checked')) { $('.if_g_fs2_bg').slideDown(500); } else { $('.if_g_fs2_bg').slideUp(500); }
    if($('#g_fs2_sw2').is(':checked')) { $('.if_g_fs2_gr').slideDown(500); } else { $('.if_g_fs2_gr').slideUp(500); }
    if($('#g_fs2_sw3').is(':checked')) { $('.if_g_fs2_im').slideDown(500); } else { $('.if_g_fs2_im').slideUp(500); }
    if($('#g_fs2_sw4').is(':checked')) { $('.if_g_fs2_te').slideDown(500); } else { $('.if_g_fs2_te').slideUp(500); }

    if($('#g_fs3_sw1').is(':checked')) { $('.if_g_fs3_bg').slideDown(500); } else { $('.if_g_fs3_bg').slideUp(500); }
    if($('#g_fs3_sw2').is(':checked')) { $('.if_g_fs3_gr').slideDown(500); } else { $('.if_g_fs3_gr').slideUp(500); }
    if($('#g_fs3_sw3').is(':checked')) { $('.if_g_fs3_im').slideDown(500); } else { $('.if_g_fs3_im').slideUp(500); }
    if($('#g_fs3_sw4').is(':checked')) { $('.if_g_fs3_te').slideDown(500); } else { $('.if_g_fs3_te').slideUp(500); }

    if($('#g_op1_sw1').is(':checked')) { $('.if_g_op1_bg').slideDown(500); } else { $('.if_g_op1_bg').slideUp(500); }
    if($('#g_op1_sw2').is(':checked')) { $('.if_g_op1_gr').slideDown(500); } else { $('.if_g_op1_gr').slideUp(500); }
    if($('#g_op1_sw3').is(':checked')) { $('.if_g_op1_im').slideDown(500); } else { $('.if_g_op1_im').slideUp(500); }
    if($('#g_op1_sw4').is(':checked')) { $('.if_g_op1_te').slideDown(500); } else { $('.if_g_op1_te').slideUp(500); }

    if($('#g_op2_sw1').is(':checked')) { $('.if_g_op2_bg').slideDown(500); } else { $('.if_g_op2_bg').slideUp(500); }
    if($('#g_op2_sw2').is(':checked')) { $('.if_g_op2_gr').slideDown(500); } else { $('.if_g_op2_gr').slideUp(500); }
    if($('#g_op2_sw3').is(':checked')) { $('.if_g_op2_im').slideDown(500); } else { $('.if_g_op2_im').slideUp(500); }
    if($('#g_op2_sw4').is(':checked')) { $('.if_g_op2_te').slideDown(500); } else { $('.if_g_op2_te').slideUp(500); }

    if($('#g_mc_sw1').is(':checked')) { $('.if_g_mc_bg').slideDown(500); } else { $('.if_g_mc_bg').slideUp(500); }
    if($('#g_mc_sw2').is(':checked')) { $('.if_g_mc_gr').slideDown(500); } else { $('.if_g_mc_gr').slideUp(500); }
    if($('#g_mc_sw3').is(':checked')) { $('.if_g_mc_im').slideDown(500); } else { $('.if_g_mc_im').slideUp(500); }
    if($('#g_mc_sw4').is(':checked')) { $('.if_g_mc_te').slideDown(500); } else { $('.if_g_mc_te').slideUp(500); }

    if($('#g_bc_sw1').is(':checked')) { $('.if_g_bc_bg').slideDown(500); } else { $('.if_g_bc_bg').slideUp(500); }
    if($('#g_bc_sw2').is(':checked')) { $('.if_g_bc_gr').slideDown(500); } else { $('.if_g_bc_gr').slideUp(500); }
    if($('#g_bc_sw3').is(':checked')) { $('.if_g_bc_im').slideDown(500); } else { $('.if_g_bc_im').slideUp(500); }
    if($('#g_bc_sw4').is(':checked')) { $('.if_g_bc_te').slideDown(500); } else { $('.if_g_bc_te').slideUp(500); }


    if($('#g_pro_w2').is(':checked')) { $('.if_pa_pro').slideDown(500); } else { $('.if_pa_pro').slideUp(500); }
    if($('#g_mini_w2').is(':checked')) { $('.if_pa_mini').slideDown(500); } else { $('.if_pa_mini').slideUp(500); }
    if($('#g_info_w2').is(':checked')) { $('.if_pa_info').slideDown(500); } else { $('.if_pa_info').slideUp(500); }
    if($('#g_bra_w2').is(':checked')) { $('.if_pa_bra').slideDown(500); } else { $('.if_pa_bra').slideUp(500); }
    if($('#g_blog_w2').is(':checked')) { $('.if_pa_blog').slideDown(500); } else { $('.if_pa_blog').slideUp(500); }
    if($('#ban_wid_top2').is(':checked')) { $('.if_pa_ban_top').slideDown(500); } else { $('.if_pa_ban_top').slideUp(500); }
    if($('#ban_wid_pro2').is(':checked')) { $('.if_pa_ban_pro').slideDown(500); } else { $('.if_pa_ban_pro').slideUp(500); }
    if($('#ban_wid_mini2').is(':checked')) { $('.if_pa_ban_mini').slideDown(500); } else { $('.if_pa_ban_mini').slideUp(500); }
    if($('#ban_wid_info2').is(':checked')) { $('.if_pa_ban_info').slideDown(500); } else { $('.if_pa_ban_info').slideUp(500); }
    if($('#ban_wid_bra2').is(':checked')) { $('.if_pa_ban_bra').slideDown(500); } else { $('.if_pa_ban_bra').slideUp(500); }
    if($('#ban_wid_home2').is(':checked')) { $('.if_pa_ban_home').slideDown(500); } else { $('.if_pa_ban_home').slideUp(500); }
    if($('#ban_wid_foot2').is(':checked')) { $('.if_pa_ban_foot').slideDown(500); } else { $('.if_pa_ban_foot').slideUp(500); }
    if($('#ban_wid_foott2').is(':checked')) { $('.if_pa_ban_foott').slideDown(500); } else { $('.if_pa_ban_foott').slideUp(500); }
    if($('#ban_wid_footb2').is(':checked')) { $('.if_pa_ban_footb').slideDown(500); } else { $('.if_pa_ban_footb').slideUp(500); }
    if($('#ban_wid_s12').is(':checked')) { $('.if_pa_ban_s1').slideDown(500); } else { $('.if_pa_ban_s1').slideUp(500); }
    if($('#ban_wid_s22').is(':checked')) { $('.if_pa_ban_s2').slideDown(500); } else { $('.if_pa_ban_s2').slideUp(500); }
    if($('#ban_wid_s32').is(':checked')) { $('.if_pa_ban_s3').slideDown(500); } else { $('.if_pa_ban_s3').slideUp(500); }


    if($('#nc_pp_image3').is(':checked')) {
        $('.if_pp_image3').slideDown(500);
    } else { $('.if_pp_image3').slideUp(500); }

    if($('#nc_pc_layout2').is(':checked') || $('#nc_pc_layout3').is(':checked')) {
        $('.if_pc_layout23').slideDown(500);
    } else { $('.if_pc_layout23').slideUp(500); }

    if($('#header_lay5').is(':checked') || $('#header_lay6').is(':checked')) {
        $('.if_nav').slideDown(500);
    } else { $('.if_nav').slideUp(500); }

    if($('#pl_filter_b1').is(':checked')) {
        $('.if_pl_filter_b').slideDown(500);
    } else { $('.if_pl_filter_b').slideUp(500); }

    if($('#bl_dd1').is(':checked')) {
        $('.if_bl_dd').slideDown(500);
    } else { $('.if_bl_dd').slideUp(500); }

    if($('#header_trah1').is(':checked')) {
        $('.if_trah').slideDown(500);
    } else { $('.if_trah').slideUp(500); }

    if($('#header_trao1').is(':checked')) {
        $('.if_trao').slideDown(500);
    } else { $('.if_trao').slideUp(500); }

    if($('#cl_popup_b1').is(':checked')) {
        $('.if_cl_popup_b').slideDown(500);
    } else { $('.if_cl_popup_b').slideUp(500); }

    if($('#c_popup_b1').is(':checked')) {
        $('.if_c_popup_b').slideDown(500);
    } else { $('.if_c_popup_b').slideUp(500); }

    if($('#m_popup_b1').is(':checked')) {
        $('.if_m_popup_b').slideDown(500);
    } else { $('.if_m_popup_b').slideUp(500); }

    if($('#m_link_bgs1').is(':checked')) {
        $('.if_m_bg').slideDown(500);
    } else { $('.if_m_bg').slideUp(500); }

    if($('#nc_loader_logo_1').is(':checked')) {
        $('.if_loader_logo').slideDown(500);
    } else { $('.if_loader_logo').slideUp(500); }

    if($('#pl_nav_bot_b0').is(':checked')) {
        $('.if_pl_nav_bot_b').slideUp(500);
    }
    if($('#pl_nav_bot_b1').is(':checked')) {
        $('.if_pl_nav_bot_b').slideDown(500);
    }
    if($('#pl_nav_top_b0').is(':checked')) {
        $('.if_pl_nav_top_b').slideUp(500);
    }
    if($('#pl_nav_top_b1').is(':checked')) {
        $('.if_pl_nav_top_b').slideDown(500);
    }
    if($('#pp_li0').is(':checked')) {
        $('.if_pp_li').slideUp(500);
    }
    if($('#pp_li1').is(':checked')) {
        $('.if_pp_li').slideDown(500);
    }
    if($('#pp_imgb0').is(':checked')) {
        $('.if_pp_imgb').slideUp(500);
    }
    if($('#pp_imgb1').is(':checked')) {
        $('.if_pp_imgb').slideDown(500);
    }
    if($('#footer_map_enbb0').is(':checked')) {
        $('.if_footer_map_enbb').slideUp(500);
    }
    if($('#footer_map_enbb1').is(':checked')) {
        $('.if_footer_map_enbb').slideDown(500);
    }
    if($('#footer_map_en0').is(':checked')) {
        $('.if_footer_map_en').slideUp(500);
    }
    if($('#footer_map_en1').is(':checked')) {
        $('.if_footer_map_en').slideDown(500);
    }
    if($('#sidebar_categories_b0').is(':checked')) {
        $('.if_sidebar_categories_b').slideUp(500);
    }
    if($('#sidebar_categories_b1').is(':checked')) {
        $('.if_sidebar_categories_b').slideDown(500);
    }
    if($('#sidebar_content_b0').is(':checked')) {
        $('.if_sidebar_content_b').slideUp(500);
    }
    if($('#sidebar_content_b1').is(':checked')) {
        $('.if_sidebar_content_b').slideDown(500);
    }
    if($('#sidebar_block_content_qbg0').is(':checked')) {
        $('.if_sidebar_qbg').slideUp(500);
    }
    if($('#sidebar_block_content_qbg1').is(':checked')) {
        $('.if_sidebar_qbg').slideDown(500);
    }
    if($('#sidebar_title_b0').is(':checked')) {
        $('.if_sidebar_title_b').slideUp(500);
    }
    if($('#sidebar_title_b1').is(':checked')) {
        $('.if_sidebar_title_b').slideDown(500);
    }
    if($('#sidebar_title0').is(':checked')) {
        $('.if_sidebar_title').slideUp(500);
    }
    if($('#sidebar_title1').is(':checked')) {
        $('.if_sidebar_title').slideDown(500);
    }
    if($('#sidebar_bg0').is(':checked')) {
        $('.if_sidebar_bg').slideUp(500);
    }
    if($('#sidebar_bg1').is(':checked')) {
        $('.if_sidebar_bg').slideDown(500);
    }
    if($('#bl_rm_bg0').is(':checked')) {
        $('.if_rmbg').slideUp(500);
    }
    if($('#bl_rm_bg1').is(':checked')) {
        $('.if_rmbg').slideDown(500);
    }
    if($('#bl_rm_border0').is(':checked')) {
        $('.if_rmborder').slideUp(500);
    }
    if($('#bl_rm_border1').is(':checked')) {
        $('.if_rmborder').slideDown(500);
    }
    if($('#g_lay1').is(':checked')) {
        $('.if_boxed').slideUp(500);
    }
    if($('#g_lay2').is(':checked')) {
        $('.if_boxed').slideDown(500);
    }
    if($('#display_gradient_1').is(':checked')) {
        $('.if_image').slideDown(500);
    }
    if($('#display_gradient_0').is(':checked')) {
        $('.if_image').slideUp(500);
    }

    //if boxed
    $('.nc_m_under').click(function() {
        if($('#nc_m_under0').is(':checked')) {
            $('.if_under').slideUp(500);
        }
        if($('#nc_m_under1').is(':checked')) {
            $('.if_under').slideDown(500);
        }
    });
    //if boxed
    $('.glay').click(function() {
        if($('#g_lay1').is(':checked')) {
            $('.if_boxed').slideUp(500);
        }
        if($('#g_lay2').is(':checked')) {
            $('.if_boxed').slideDown(500);
        }
    });
    //if image
    $('.if_img').click(function() {
        if($('#display_gradient_1').is(':checked')) {
            $('.if_image').slideDown(500);
        }
        if($('#display_gradient_0').is(':checked')) {
            $('.if_image').slideUp(500);
        }
    });
    //if blog readmore bg
    $('.bl_rm_bg').click(function() {
        if($('#bl_rm_bg1').is(':checked')) {
            $('.if_rmbg').slideDown(500);
        }
        if($('#bl_rm_bg0').is(':checked')) {
            $('.if_rmbg').slideUp(500);
        }
    });
    //if blog readmore border
    $('.bl_rm_border').click(function() {
        if($('#bl_rm_border1').is(':checked')) {
            $('.if_rmborder').slideDown(500);
        }
        if($('#bl_rm_border0').is(':checked')) {
            $('.if_rmborder').slideUp(500);
        }
    });
    //if sidebar bg
    $('.sidebar_bg').click(function() {
        if($('#sidebar_bg1').is(':checked')) {
            $('.if_sidebar_bg').slideDown(500);
        }
        if($('#sidebar_bg0').is(':checked')) {
            $('.if_sidebar_bg').slideUp(500);
        }
    });
    //if sidebar title bg
    $('.sidebar_title').click(function() {
        if($('#sidebar_title1').is(':checked')) {
            $('.if_sidebar_title').slideDown(500);
        }
        if($('#sidebar_title0').is(':checked')) {
            $('.if_sidebar_title').slideUp(500);
        }
    });
    //if sidebar title border
    $('.sidebar_title_b').click(function() {
        if($('#sidebar_title_b1').is(':checked')) {
            $('.if_sidebar_title_b').slideDown(500);
        }
        if($('#sidebar_title_b0').is(':checked')) {
            $('.if_sidebar_title_b').slideUp(500);
        }
    });
    //if sidebar content bg
    $('.sidebar_block_content_qbg').click(function() {
        if($('#sidebar_block_content_qbg1').is(':checked')) {
            $('.if_sidebar_qbg').slideDown(500);
        }
        if($('#sidebar_block_content_qbg0').is(':checked')) {
            $('.if_sidebar_qbg').slideUp(500);
        }
    });
    //if sidebar content border
    $('.sidebar_content_b').click(function() {
        if($('#sidebar_content_b1').is(':checked')) {
            $('.if_sidebar_content_b').slideDown(500);
        }
        if($('#sidebar_content_b0').is(':checked')) {
            $('.if_sidebar_content_b').slideUp(500);
        }
    });
    //if sidebar categories border
    $('.sidebar_categories_b').click(function() {
        if($('#sidebar_categories_b1').is(':checked')) {
            $('.if_sidebar_categories_b').slideDown(500);
        }
        if($('#sidebar_categories_b0').is(':checked')) {
            $('.if_sidebar_categories_b').slideUp(500);
        }
    });
    //if map icon
    $('.footer_map_en').click(function() {
        if($('#footer_map_en1').is(':checked')) {
            $('.if_footer_map_en').slideDown(500);
        }
        if($('#footer_map_en0').is(':checked')) {
            $('.if_footer_map_en').slideUp(500);
        }
    });
    //if map icon bb
    $('.footer_map_enbb').click(function() {
        if($('#footer_map_enbb1').is(':checked')) {
            $('.if_footer_map_enbb').slideDown(500);
        }
        if($('#footer_map_enbb0').is(':checked')) {
            $('.if_footer_map_enbb').slideUp(500);
        }
    });
    //if product image border
    $('.pp_imgb').click(function() {
        if($('#pp_imgb1').is(':checked')) {
            $('.if_pp_imgb').slideDown(500);
        }
        if($('#pp_imgb0').is(':checked')) {
            $('.if_pp_imgb').slideUp(500);
        }
    });
    //if product label icon
    $('.pp_li').click(function() {
        if($('#pp_li1').is(':checked')) {
            $('.if_pp_li').slideDown(500);
        }
        if($('#pp_li0').is(':checked')) {
            $('.if_pp_li').slideUp(500);
        }
    });
    //if top nav b
    $('.pl_nav_top_b').click(function() {
        if($('#pl_nav_top_b1').is(':checked')) {
            $('.if_pl_nav_top_b').slideDown(500);
        }
        if($('#pl_nav_top_b0').is(':checked')) {
            $('.if_pl_nav_top_b').slideUp(500);
        }
    });
    //if bot nav b
    $('.pl_nav_bot_b').click(function() {
        if($('#pl_nav_bot_b1').is(':checked')) {
            $('.if_pl_nav_bot_b').slideDown(500);
        } else { $('.if_pl_nav_bot_b').slideUp(500); }
    });
    //if loader logo
    $('.nc_loader_logo').click(function() {
        if($('#nc_loader_logo_1').is(':checked')) {
            $('.if_loader_logo').slideDown(500);
        } else { $('.if_loader_logo').slideUp(500); }
    });
    //if cl popup border
    $('.cl_popup_b').click(function() {
        if($('#cl_popup_b1').is(':checked')) {
            $('.if_cl_popup_b').slideDown(500);
        } else { $('.if_cl_popup_b').slideUp(500); }
    });
    //if c popup border
    $('.c_popup_b').click(function() {
        if($('#c_popup_b1').is(':checked')) {
            $('.if_c_popup_b').slideDown(500);
        } else { $('.if_c_popup_b').slideUp(500); }
    });
    //if m popup border
    $('.m_popup_b').click(function() {
        if($('#m_popup_b1').is(':checked')) {
            $('.if_m_popup_b').slideDown(500);
        } else { $('.if_m_popup_b').slideUp(500); }
    });
    //if m bg
    $('.m_link_bgs').click(function() {
        if($('#m_link_bgs1').is(':checked')) {
            $('.if_m_bg').slideDown(500);
        } else { $('.if_m_bg').slideUp(500); }
    });
    //if pl_filter_b
    $('.pl_filter_b').click(function() {
        if($('#pl_filter_b1').is(':checked')) {
            $('.if_pl_filter_b').slideDown(500);
        } else { $('.if_pl_filter_b').slideUp(500); }
    });
    //if bl date
    $('.bl_dd').click(function() {
        if($('#bl_dd1').is(':checked')) {
            $('.if_bl_dd').slideDown(500);
        } else { $('.if_bl_dd').slideUp(500); }
    });
    //if trah
    $('.header_trah').click(function() {
        if($('#header_trah1').is(':checked')) {
            $('.if_trah').slideDown(500);
        } else { $('.if_trah').slideUp(500); }
    });
    //if trao
    $('.header_trao').click(function() {
        if($('#header_trao1').is(':checked')) {
            $('.if_trao').slideDown(500);
        } else { $('.if_trao').slideUp(500); }
    });
    //if nav
    $('.header_lay').click(function() {
        if($('#header_lay5').is(':checked') || $('#header_lay6').is(':checked')) {
            $('.if_nav').slideDown(500);
        } else { $('.if_nav').slideUp(500); }
    });
    //if nav
    $('.nc_pc_layout').click(function() {
        if($('#nc_pc_layout2').is(':checked') || $('#nc_pc_layout3').is(':checked')) {
            $('.if_pc_layout23').slideDown(500);
        } else { $('.if_pc_layout23').slideUp(500); }
    });
    //if hover 4
    if($('#nc_p_hover4').is(':checked')) {
        $('.if_hover4').slideDown(500);
    } else {
        $('.if_hover4').slideUp(500);
    }
    $('.nc_p_hover').click(function() {
        setTimeout(function(){
            if($('#nc_p_hover4').is(':checked')) {
                $('.if_hover4').slideDown(500);
            } else {
                $('.if_hover4').slideUp(500);
            }
        }, 200);
    });
    //if container 4 then hover 4
    if($('#nc_pc_layout4').is(':checked')) {
        $('#nc_p_hover4').prop("checked", true);
    }
    $('.nc_pc_layout').click(function() {
        setTimeout(function(){
            if($('#nc_pc_layout4').is(':checked')) {
                $('#nc_p_hover4').prop("checked", true);
                $('.if_hover4').slideDown(500);
            }
        }, 200);
    });
    // sections start
    $('.body_box_sw').click(function() { if($('#body_box_sw1').is(':checked')) { $('.if_body_box_bg').slideDown(500); } else { $('.if_body_box_bg').slideUp(500); } });
    $('.body_box_sw').click(function() { if($('#body_box_sw2').is(':checked')) { $('.if_body_box_gr').slideDown(500); } else { $('.if_body_box_gr').slideUp(500); } });
    $('.body_box_sw').click(function() { if($('#body_box_sw3').is(':checked')) { $('.if_body_box_im').slideDown(500); } else { $('.if_body_box_im').slideUp(500); } });
    $('.body_box_sw').click(function() { if($('#body_box_sw4').is(':checked')) { $('.if_body_box_te').slideDown(500); } else { $('.if_body_box_te').slideUp(500); } });

    $('.g_hp1_sw').click(function() { if($('#g_hp1_sw1').is(':checked')) { $('.if_g_hp1_bg').slideDown(500); } else { $('.if_g_hp1_bg').slideUp(500); } });
    $('.g_hp1_sw').click(function() { if($('#g_hp1_sw2').is(':checked')) { $('.if_g_hp1_gr').slideDown(500); } else { $('.if_g_hp1_gr').slideUp(500); } });
    $('.g_hp1_sw').click(function() { if($('#g_hp1_sw3').is(':checked')) { $('.if_g_hp1_im').slideDown(500); } else { $('.if_g_hp1_im').slideUp(500); } });
    $('.g_hp1_sw').click(function() { if($('#g_hp1_sw4').is(':checked')) { $('.if_g_hp1_te').slideDown(500); } else { $('.if_g_hp1_te').slideUp(500); } });

    $('.g_hp2_sw').click(function() { if($('#g_hp2_sw1').is(':checked')) { $('.if_g_hp2_bg').slideDown(500); } else { $('.if_g_hp2_bg').slideUp(500); } });
    $('.g_hp2_sw').click(function() { if($('#g_hp2_sw2').is(':checked')) { $('.if_g_hp2_gr').slideDown(500); } else { $('.if_g_hp2_gr').slideUp(500); } });
    $('.g_hp2_sw').click(function() { if($('#g_hp2_sw3').is(':checked')) { $('.if_g_hp2_im').slideDown(500); } else { $('.if_g_hp2_im').slideUp(500); } });
    $('.g_hp2_sw').click(function() { if($('#g_hp2_sw4').is(':checked')) { $('.if_g_hp2_te').slideDown(500); } else { $('.if_g_hp2_te').slideUp(500); } });

    $('.g_hp3_sw').click(function() { if($('#g_hp3_sw1').is(':checked')) { $('.if_g_hp3_bg').slideDown(500); } else { $('.if_g_hp3_bg').slideUp(500); } });
    $('.g_hp3_sw').click(function() { if($('#g_hp3_sw2').is(':checked')) { $('.if_g_hp3_gr').slideDown(500); } else { $('.if_g_hp3_gr').slideUp(500); } });
    $('.g_hp3_sw').click(function() { if($('#g_hp3_sw3').is(':checked')) { $('.if_g_hp3_im').slideDown(500); } else { $('.if_g_hp3_im').slideUp(500); } });
    $('.g_hp3_sw').click(function() { if($('#g_hp3_sw4').is(':checked')) { $('.if_g_hp3_te').slideDown(500); } else { $('.if_g_hp3_te').slideUp(500); } });

    $('.g_hp4_sw').click(function() { if($('#g_hp4_sw1').is(':checked')) { $('.if_g_hp4_bg').slideDown(500); } else { $('.if_g_hp4_bg').slideUp(500); } });
    $('.g_hp4_sw').click(function() { if($('#g_hp4_sw2').is(':checked')) { $('.if_g_hp4_gr').slideDown(500); } else { $('.if_g_hp4_gr').slideUp(500); } });
    $('.g_hp4_sw').click(function() { if($('#g_hp4_sw3').is(':checked')) { $('.if_g_hp4_im').slideDown(500); } else { $('.if_g_hp4_im').slideUp(500); } });
    $('.g_hp4_sw').click(function() { if($('#g_hp4_sw4').is(':checked')) { $('.if_g_hp4_te').slideDown(500); } else { $('.if_g_hp4_te').slideUp(500); } });

    $('.g_hp5_sw').click(function() { if($('#g_hp5_sw1').is(':checked')) { $('.if_g_hp5_bg').slideDown(500); } else { $('.if_g_hp5_bg').slideUp(500); } });
    $('.g_hp5_sw').click(function() { if($('#g_hp5_sw2').is(':checked')) { $('.if_g_hp5_gr').slideDown(500); } else { $('.if_g_hp5_gr').slideUp(500); } });
    $('.g_hp5_sw').click(function() { if($('#g_hp5_sw3').is(':checked')) { $('.if_g_hp5_im').slideDown(500); } else { $('.if_g_hp5_im').slideUp(500); } });
    $('.g_hp5_sw').click(function() { if($('#g_hp5_sw4').is(':checked')) { $('.if_g_hp5_te').slideDown(500); } else { $('.if_g_hp5_te').slideUp(500); } });

    $('.g_hp6_sw').click(function() { if($('#g_hp6_sw1').is(':checked')) { $('.if_g_hp6_bg').slideDown(500); } else { $('.if_g_hp6_bg').slideUp(500); } });
    $('.g_hp6_sw').click(function() { if($('#g_hp6_sw2').is(':checked')) { $('.if_g_hp6_gr').slideDown(500); } else { $('.if_g_hp6_gr').slideUp(500); } });
    $('.g_hp6_sw').click(function() { if($('#g_hp6_sw3').is(':checked')) { $('.if_g_hp6_im').slideDown(500); } else { $('.if_g_hp6_im').slideUp(500); } });
    $('.g_hp6_sw').click(function() { if($('#g_hp6_sw4').is(':checked')) { $('.if_g_hp6_te').slideDown(500); } else { $('.if_g_hp6_te').slideUp(500); } });

    $('.g_fs1_sw').click(function() { if($('#g_fs1_sw1').is(':checked')) { $('.if_g_fs1_bg').slideDown(500); } else { $('.if_g_fs1_bg').slideUp(500); } });
    $('.g_fs1_sw').click(function() { if($('#g_fs1_sw2').is(':checked')) { $('.if_g_fs1_gr').slideDown(500); } else { $('.if_g_fs1_gr').slideUp(500); } });
    $('.g_fs1_sw').click(function() { if($('#g_fs1_sw3').is(':checked')) { $('.if_g_fs1_im').slideDown(500); } else { $('.if_g_fs1_im').slideUp(500); } });
    $('.g_fs1_sw').click(function() { if($('#g_fs1_sw4').is(':checked')) { $('.if_g_fs1_te').slideDown(500); } else { $('.if_g_fs1_te').slideUp(500); } });

    $('.g_fs2_sw').click(function() { if($('#g_fs2_sw1').is(':checked')) { $('.if_g_fs2_bg').slideDown(500); } else { $('.if_g_fs2_bg').slideUp(500); } });
    $('.g_fs2_sw').click(function() { if($('#g_fs2_sw2').is(':checked')) { $('.if_g_fs2_gr').slideDown(500); } else { $('.if_g_fs2_gr').slideUp(500); } });
    $('.g_fs2_sw').click(function() { if($('#g_fs2_sw3').is(':checked')) { $('.if_g_fs2_im').slideDown(500); } else { $('.if_g_fs2_im').slideUp(500); } });
    $('.g_fs2_sw').click(function() { if($('#g_fs2_sw4').is(':checked')) { $('.if_g_fs2_te').slideDown(500); } else { $('.if_g_fs2_te').slideUp(500); } });

    $('.g_fs3_sw').click(function() { if($('#g_fs3_sw1').is(':checked')) { $('.if_g_fs3_bg').slideDown(500); } else { $('.if_g_fs3_bg').slideUp(500); } });
    $('.g_fs3_sw').click(function() { if($('#g_fs3_sw2').is(':checked')) { $('.if_g_fs3_gr').slideDown(500); } else { $('.if_g_fs3_gr').slideUp(500); } });
    $('.g_fs3_sw').click(function() { if($('#g_fs3_sw3').is(':checked')) { $('.if_g_fs3_im').slideDown(500); } else { $('.if_g_fs3_im').slideUp(500); } });
    $('.g_fs3_sw').click(function() { if($('#g_fs3_sw4').is(':checked')) { $('.if_g_fs3_te').slideDown(500); } else { $('.if_g_fs3_te').slideUp(500); } });

    $('.g_op1_sw').click(function() { if($('#g_op1_sw1').is(':checked')) { $('.if_g_op1_bg').slideDown(500); } else { $('.if_g_op1_bg').slideUp(500); } });
    $('.g_op1_sw').click(function() { if($('#g_op1_sw2').is(':checked')) { $('.if_g_op1_gr').slideDown(500); } else { $('.if_g_op1_gr').slideUp(500); } });
    $('.g_op1_sw').click(function() { if($('#g_op1_sw3').is(':checked')) { $('.if_g_op1_im').slideDown(500); } else { $('.if_g_op1_im').slideUp(500); } });
    $('.g_op1_sw').click(function() { if($('#g_op1_sw4').is(':checked')) { $('.if_g_op1_te').slideDown(500); } else { $('.if_g_op1_te').slideUp(500); } });

    $('.g_op2_sw').click(function() { if($('#g_op2_sw1').is(':checked')) { $('.if_g_op2_bg').slideDown(500); } else { $('.if_g_op2_bg').slideUp(500); } });
    $('.g_op2_sw').click(function() { if($('#g_op2_sw2').is(':checked')) { $('.if_g_op2_gr').slideDown(500); } else { $('.if_g_op2_gr').slideUp(500); } });
    $('.g_op2_sw').click(function() { if($('#g_op2_sw3').is(':checked')) { $('.if_g_op2_im').slideDown(500); } else { $('.if_g_op2_im').slideUp(500); } });
    $('.g_op2_sw').click(function() { if($('#g_op2_sw4').is(':checked')) { $('.if_g_op2_te').slideDown(500); } else { $('.if_g_op2_te').slideUp(500); } });

    $('.g_mc_sw').click(function() { if($('#g_mc_sw1').is(':checked')) { $('.if_g_mc_bg').slideDown(500); } else { $('.if_g_mc_bg').slideUp(500); } });
    $('.g_mc_sw').click(function() { if($('#g_mc_sw2').is(':checked')) { $('.if_g_mc_gr').slideDown(500); } else { $('.if_g_mc_gr').slideUp(500); } });
    $('.g_mc_sw').click(function() { if($('#g_mc_sw3').is(':checked')) { $('.if_g_mc_im').slideDown(500); } else { $('.if_g_mc_im').slideUp(500); } });
    $('.g_mc_sw').click(function() { if($('#g_mc_sw4').is(':checked')) { $('.if_g_mc_te').slideDown(500); } else { $('.if_g_mc_te').slideUp(500); } });

    $('.g_bc_sw').click(function() { if($('#g_bc_sw1').is(':checked')) { $('.if_g_bc_bg').slideDown(500); } else { $('.if_g_bc_bg').slideUp(500); } });
    $('.g_bc_sw').click(function() { if($('#g_bc_sw2').is(':checked')) { $('.if_g_bc_gr').slideDown(500); } else { $('.if_g_bc_gr').slideUp(500); } });
    $('.g_bc_sw').click(function() { if($('#g_bc_sw3').is(':checked')) { $('.if_g_bc_im').slideDown(500); } else { $('.if_g_bc_im').slideUp(500); } });
    $('.g_bc_sw').click(function() { if($('#g_bc_sw4').is(':checked')) { $('.if_g_bc_te').slideDown(500); } else { $('.if_g_bc_te').slideUp(500); } });

    $('.g_pro_w').click(function() { if($('#g_pro_w2').is(':checked')) { $('.if_pa_pro').slideDown(500); } else { $('.if_pa_pro').slideUp(500); } });
    $('.g_mini_w').click(function() { if($('#g_mini_w2').is(':checked')) { $('.if_pa_mini').slideDown(500); } else { $('.if_pa_mini').slideUp(500); } });
    $('.g_info_w').click(function() { if($('#g_info_w2').is(':checked')) { $('.if_pa_info').slideDown(500); } else { $('.if_pa_info').slideUp(500); } });
    $('.g_bra_w').click(function() { if($('#g_bra_w2').is(':checked')) { $('.if_pa_bra').slideDown(500); } else { $('.if_pa_bra').slideUp(500); } });
    $('.g_blog_w').click(function() { if($('#g_blog_w2').is(':checked')) { $('.if_pa_blog').slideDown(500); } else { $('.if_pa_blog').slideUp(500); } });
    $('.ban_wid_top').click(function() { if($('#ban_wid_top2').is(':checked')) { $('.if_pa_ban_top').slideDown(500); } else { $('.if_pa_ban_top').slideUp(500); } });
    $('.ban_wid_pro').click(function() { if($('#ban_wid_pro2').is(':checked')) { $('.if_pa_ban_pro').slideDown(500); } else { $('.if_pa_ban_pro').slideUp(500); } });
    $('.ban_wid_mini').click(function() { if($('#ban_wid_mini2').is(':checked')) { $('.if_pa_ban_mini').slideDown(500); } else { $('.if_pa_ban_mini').slideUp(500); } });
    $('.ban_wid_info').click(function() { if($('#ban_wid_info2').is(':checked')) { $('.if_pa_ban_info').slideDown(500); } else { $('.if_pa_ban_info').slideUp(500); } });
    $('.ban_wid_bra').click(function() { if($('#ban_wid_bra2').is(':checked')) { $('.if_pa_ban_bra').slideDown(500); } else { $('.if_pa_ban_bra').slideUp(500); } });
    $('.ban_wid_home').click(function() { if($('#ban_wid_home2').is(':checked')) { $('.if_pa_ban_home').slideDown(500); } else { $('.if_pa_ban_home').slideUp(500); } });
    $('.ban_wid_foot').click(function() { if($('#ban_wid_foot2').is(':checked')) { $('.if_pa_ban_foot').slideDown(500); } else { $('.if_pa_ban_foot').slideUp(500); } });
    $('.ban_wid_foott').click(function() { if($('#ban_wid_foott2').is(':checked')) { $('.if_pa_ban_foott').slideDown(500); } else { $('.if_pa_ban_foott').slideUp(500); } });
    $('.ban_wid_footb').click(function() { if($('#ban_wid_footb2').is(':checked')) { $('.if_pa_ban_footb').slideDown(500); } else { $('.if_pa_ban_footb').slideUp(500); } });
    $('.ban_wid_s1').click(function() { if($('#ban_wid_s12').is(':checked')) { $('.if_pa_ban_s1').slideDown(500); } else { $('.if_pa_ban_s1').slideUp(500); } });
    $('.ban_wid_s2').click(function() { if($('#ban_wid_s22').is(':checked')) { $('.if_pa_ban_s2').slideDown(500); } else { $('.if_pa_ban_s2').slideUp(500); } });
    $('.ban_wid_s3').click(function() { if($('#ban_wid_s32').is(':checked')) { $('.if_pa_ban_s3').slideDown(500); } else { $('.if_pa_ban_s3').slideUp(500); } });

    $('.nc_m_under').click(function() { if($('#nc_m_under1').is(':checked')) { $('.if_under').slideDown(500); } else { $('.if_under').slideUp(500); } });
    $('.nc_pp_image').click(function() { if($('#nc_pp_image3').is(':checked')) { $('.if_pp_image3').slideDown(500); } else { $('.if_pp_image3').slideUp(500); } });

});
