<style type="text/css">@import url(<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/
 echo SC_PLUPLOAD;?>js/vault/vault.min.css);</style>
<script type="text/javascript" src="<?php echo SC_JQUERY;?>"></script>
<script type="text/javascript" src="<?php echo SC_JSFUNCTIONS;?>"></script>
<script type="text/javascript" src="<?php echo SC_PLUPLOAD;?>js/vault/vault.min.js"></script>
<?php
$id_lang=Tools::getValue('id_lang',null);
$product_list = $original_product_list =Tools::getValue('product_list',null);
$attr_list=Tools::getValue('attr_list',null);
$is_attr=Tools::getValue('is_attr',null);
$is_combination_multiproduct=Tools::getValue('multi',null);
if($is_combination_multiproduct) {
    $attr_arr = explode(',',$attr_list);
    $cache = array();
    foreach($attr_arr as $row) {
        list($id_product,$id_product_attribute) = explode('_',$row);
        $cache['product'][$id_product] = (int)$id_product;
        $cache['attribute'][$id_product_attribute] = (int)$id_product_attribute;
    }
    if(!empty($cache)) {
        $product_list = implode(',', $cache['product']);
        $attr_list = implode(',', $cache['attribute']);
    }
}
?>
<body style="margin:0;">
<div id="file_uploader"></div>
<script>
    <?php require_once SC_PLUPLOAD.'js/vault/vault_lang.php'; ?>
    let window_id = "<?php echo $original_product_list; ?>";
    let autosend = top.ll_toolbar.getItemState('auto_upload');
    let authorized_extensions = ["JPG","JPEG","jpg","jpeg","PNG","png","GIF","gif","BMP","bmp"];
    let vaultObject = new dhx.Vault("file_uploader", {
        uploader:{
            target: 'index.php?ajax=1&act=all_upload&obj=image&product_list=<?php echo $product_list;?>&attr_list=<?php echo $attr_list; ?>&id_lang=<?php echo (int)$id_lang.($is_combination_multiproduct ? '&is_multiproduct=1' : '');?>',
            autosend:autosend
        },
        mode:"grid",
    });
    vaultObject.events.on("BeforeAdd", function(item){
        let extension = item.file.name.split('.').pop();
        if(authorized_extensions.indexOf(extension) >= 0) {
            let fileSize = item.file.size;
            let fileSizeMo = fileSize / 1024 / 1024;
            <?php $limitSizeMo = (Configuration::get('PS_PRODUCT_PICTURE_MAX_SIZE')/1024/1024); ?>
            let limitSize = <?php echo $limitSizeMo;?>;
            if (fileSizeMo > limitSize) {
                dhx.message({
                    text: "<?php echo _l('The file is too large. Maximum size allowed is: %1$d Mo. The file you are trying to upload is ', 1, array($limitSizeMo));?> " + fileSizeMo.toFixed(2) + " Mo",
                    css: "dhx-error",
                    expire: 4000
                });
                return false;
            }
        } else {
            dhx.message({
                text: "<?php echo _l("Wrong file format",1);?> ("+authorized_extensions.join(',')+" <?php echo _l('only',1);?>)",
                css: "dhx-error",
                expire: 4000
            });
            return false;
        }
    });
    vaultObject.events.on("AfterAdd", function(item) {
        this.uploader.config.autosend = top.ll_toolbar.getItemState('auto_upload');
        this.paint();
    });
    vaultObject.events.on("UploadBegin", function(files){
        top.prop_tb._imagesUploadWindow[window_id].park();
    });
    var file_list_id = [];
    vaultObject.events.on("UploadComplete", function(files){
        var error = 0;
        files.forEach(function(item){
            let file_response  = JSON.parse(item.request.response);
            file_list_id.push(file_response.id);
            if(file_response.error !== null) {
                dhx.message({
                    text: "code:"+file_response.error.code+" "+file_response.error.message,
                    css: "dhx-error",
                    expire: 4000
                });
                error = error+1;
            }
        });
        if(error === 0) {
            $.post("index.php?ajax=1&act=cat_image_update&action=position_after_upload&id_lang=<?php echo (int)$id_lang;?>&"+new Date().getTime(),{'product_list':'<?php echo $product_list;?>','total_file_uploaded':files.length,'list_id_image':file_list_id.join(',')},function(res){
                if(res === 'OK') {
                    top.prop_tb._imagesUploadWindow[window_id].hide();
                    <?php
                    if(!$is_attr) {
                        echo 'top.displayImages();';
                    } else {
                        if($is_combination_multiproduct) {
                            echo 'top.getCombinationMultiProductImages();';
                        } else {
                            echo 'top.getCombinationsImages();';
                        }
                    }
                    ?>
                }
            });
        }
    });
</script>
</body>
