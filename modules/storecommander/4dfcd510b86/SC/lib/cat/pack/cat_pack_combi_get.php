<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/

	$id_pack=Tools::getValue('id_pack',0);
	$id_product=Tools::getValue('id_product',0);
	$id_lang=intval(Tools::getValue('id_lang'));

	function getPdtCombiPack()
	{
		global $id_pack,$id_lang,$id_product;

        $sql = "SELECT pa.id_product_attribute,pa.reference, pk.*
                FROM "._DB_PREFIX_."product_attribute pa
                    LEFT JOIN "._DB_PREFIX_."pack pk ON (pk.id_product_attribute_item=pa.id_product_attribute AND pk.id_product_pack ='".intval($id_pack)."')
                WHERE 
                    pa.id_product='".(int)$id_product."'
                ORDER BY pa.id_product_attribute ASC";
        $res = Db::getInstance()->ExecuteS($sql);

		foreach($res as $row){
            $name = "";
            $sql_attr ="SELECT agl.name as gp, al.name
						FROM "._DB_PREFIX_."product_attribute_combination pac
							INNER JOIN "._DB_PREFIX_."attribute a ON pac.id_attribute = a.id_attribute
								INNER JOIN "._DB_PREFIX_."attribute_group_lang agl ON a.id_attribute_group = agl.id_attribute_group
							INNER JOIN "._DB_PREFIX_."attribute_lang al ON pac.id_attribute = al.id_attribute
						WHERE pac.id_product_attribute = '".$row['id_product_attribute']."'
							AND agl.id_lang = '".$id_lang."'
							AND al.id_lang = '".$id_lang."'
						GROUP BY a.id_attribute
						ORDER BY agl.name";
            $res_attr = Db::getInstance()->executeS($sql_attr);
            foreach($res_attr as $attr)
            {
                if(!empty($attr["gp"]) && !empty($attr["name"]))
                {
                    if(!empty($name))
                        $name .= ", ";
                    $name .= $attr["gp"]." : ".$attr["name"];
                }
            }

			echo "<row id=\"".$row['id_product_attribute']."\">";
			echo 		"<cell>".$row['id_product_attribute']."</cell>";
			echo 		"<cell><![CDATA[".$row['reference']."]]></cell>";
			echo 		"<cell><![CDATA[".$name."]]></cell>";
            echo 		"<cell><![CDATA[".(!empty($row['id_product_attribute_item'])?"1":"0")."]]></cell>";
			echo 		"<cell  type=\"".(!empty($row['id_product_attribute_item'])?'edn':'ro')."\"><![CDATA[".$row['quantity']."]]></cell>";
			echo "</row>";
		}
	}

	if(stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml")){
	 		header("Content-type: application/xhtml+xml");
	}else{
	 		header("Content-type: text/xml");
	}
	echo("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
?>
<rows>
<head>
<beforeInit>
<call command="attachHeader"><param><![CDATA[#text_filter,#text_filter,#text_filter,,#numeric_filter]]></param></call>
</beforeInit>
<column id="id" width="50" type="ro" align="right" sort="int"><?php echo _l('ID')?></column>
<column id="reference" width="120" type="ro" align="left" sort="str"><?php echo _l('Reference')?></column>
<column id="name" width="200" type="ro" align="left" sort="str"><?php echo _l('Name')?></column>
<column id="present" width="80" type="ch" align="center" sort="int"><?php echo _l('Present')?></column>
<column id="quantity" width="80" type="edn" align="right" sort="int"><?php echo _l('Quantity in pack')?></column>
</head>
<?php
	echo '<userdata name="uisettings">'.uisettings::getSetting('cat_combi_pack').'</userdata>'."\n";
getPdtCombiPack();
	//echo '</rows>';
?>
</rows>