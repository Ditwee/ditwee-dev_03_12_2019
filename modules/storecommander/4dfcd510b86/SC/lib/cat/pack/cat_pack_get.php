<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/

	$id_pack=Tools::getValue('id_pack',0);
	$id_lang=intval(Tools::getValue('id_lang'));

	function getPdtPack()
	{
		global $id_pack,$id_lang;

        $sql = "SELECT pk.*,p.reference,pl.name ".(version_compare(_PS_VERSION_, '1.5.0.0', '>=')?", ps.active, p.id_shop_default":", p.active").", img.id_image
                FROM "._DB_PREFIX_."pack pk
                    LEFT JOIN "._DB_PREFIX_."product p ON (pk.id_product_item=p.id_product)
                    LEFT JOIN "._DB_PREFIX_."product_lang pl ON (pk.id_product_item=pl.id_product AND pl.id_lang=".intval($id_lang)." ".(SCMS?(SCI::getSelectedShop()>0?' AND pl.id_shop='.(int)SCI::getSelectedShop():' AND pl.id_shop=p.id_shop_default '):'').")
                    ".(version_compare(_PS_VERSION_, '1.5.0.0', '>=')?"INNER JOIN "._DB_PREFIX_."product_shop ps ON (p.id_product=ps.id_product AND ps.id_shop=".(SCI::getSelectedShop()>0?(int)SCI::getSelectedShop():'p.id_shop_default').")":"")."
                    LEFT JOIN "._DB_PREFIX_."image img ON img.id_product = p.id_product AND img.cover = 1
                WHERE pk.id_product_pack ='".intval($id_pack)."'
                GROUP BY pk.id_product_item
                ORDER BY pl.name ASC";
        $res = Db::getInstance()->ExecuteS($sql);

		foreach($res as $row){
            $has_combi = false;
            $combis = Product::getProductAttributesIds($row['id_product_item']);
            if(count($combis)>0)
                $has_combi = true;


			echo "<row id=\"".$row['id_product_item']."\">";
			echo 		"<cell>".$row['id_product_item']."</cell>";
            echo 		"<cell style=\"background-color:".($row['active']?'':'#888888')."\"><![CDATA[".($row['active']?_l('Yes'):_l('No'))."]]></cell>";

                $defaultimg='lib/img/i.gif';
                if (!empty($row['id_image']))
                {
                    if (file_exists(SC_PS_PATH_REL."img/p/".getImgPath((int)$row['id_product_item'],(int)$row['id_image'],_s('CAT_PROD_GRID_IMAGE_SIZE')))) {
                        echo "<cell><![CDATA[<img src='".SC_PS_PATH_REL."img/p/".getImgPath((int)$row['id_product_item'],(int)$row['id_image'],_s('CAT_PROD_GRID_IMAGE_SIZE'))."'/>]]></cell>";
                    } else {
                        echo '<cell><![CDATA['.((_s('APP_USE_NEW_ICONS')) ? '<i class="'.getIcon('i.gif').'" ></i>' : '<img src="'.$defaultimg.'"/>').'--]]></cell>';
                    }
                }else{
                    echo '<cell><![CDATA['.((_s('APP_USE_NEW_ICONS')) ? '<i class="'.getIcon('i.gif').'" ></i>' : '<img src="'.$defaultimg.'"/>').'--]]></cell>';
                }

			echo 		"<cell><![CDATA[".$row['reference']."]]></cell>";
			echo 		"<cell><![CDATA[".$row['name']."]]></cell>";
			echo 		"<cell style=\"background-color:".($has_combi?'#888888':'')."\"  type=\"".($has_combi?'ro':'edn')."\"><![CDATA[".($has_combi?'-':$row['quantity'])."]]></cell>";
			echo "</row>";
		}
	}

	if(stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml")){
	 		header("Content-type: application/xhtml+xml");
	}else{
	 		header("Content-type: text/xml");
	}
	echo("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
?>
<rows>
<head>
<beforeInit>
<call command="attachHeader"><param><![CDATA[#text_filter,#select_filter,,#text_filter,#text_filter,#numeric_filter]]></param></call>
</beforeInit>
<column id="id" width="50" type="ro" align="right" sort="int"><?php echo _l('ID')?></column>
<column id="active" width="45" type="ro" align="left" sort="str"><?php echo _l('Active')?></column>
<column id="used" width="100" type="ro" align="center" sort="int"><?php echo _l('Image')?></column>
<column id="reference" width="120" type="ro" align="left" sort="str"><?php echo _l('Reference')?></column>
<column id="name" width="200" type="ro" align="left" sort="str"><?php echo _l('Name')?></column>
<column id="quantity" width="80" type="edn" align="right" sort="int"><?php echo _l('Quantity in pack')?></column>
</head>
<?php
	echo '<userdata name="uisettings">'.uisettings::getSetting('cat_pack').'</userdata>'."\n";
getPdtPack();
	//echo '</rows>';
?>
</rows>