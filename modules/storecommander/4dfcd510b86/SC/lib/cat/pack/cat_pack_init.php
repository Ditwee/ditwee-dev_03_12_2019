<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/
if(version_compare(_PS_VERSION_, '1.6.1.20', '>=')) { ?>
    prop_tb.addListOption('panel', 'pack', 15, "button", '<?php echo _l('Pack',1)?>', "<?php echo getIcon("box.png"); ?>");
    allowed_properties_panel[allowed_properties_panel.length] = "pack";

    needInitpack = 1;
    var lastPdtPackSelectedId = null;
    function initpack()
    {
        if (needInitpack)
        {
            lastPdtPackSelectedId = null;
            prop_tb._packLayout = dhxLayout.cells('b').attachLayout('2E');
            dhxLayout.cells('b').showHeader();

            // PRODUCTS
            prop_tb._packProduct = prop_tb._packLayout.cells('a');
            prop_tb._packProduct.setText('<?php echo _l('Products',1)?>');

            prop_tb._packProduct_tb = prop_tb._packProduct.attachToolbar();
            <?php echo(_s('APP_USE_NEW_ICONS') ? "prop_tb._packProduct_tb.setIconset('awesome');"."\r\n" : ''); ?>
            prop_tb._packProduct_tb.addButton("pack_refresh", 100, "", "<?php echo getIcon("arrow_refresh.png"); ?>", "<?php echo getIcon("arrow_refresh.png"); ?>");
            prop_tb._packProduct_tb.setItemToolTip('pack_refresh','<?php echo _l('Refresh grid',1)?>');
            prop_tb._packProduct_tb.addButton("pack_pdt_delete", 100, "", "<?php echo getIcon("delete.gif"); ?>", "<?php echo getIcon("delete.gif"); ?>");
            prop_tb._packProduct_tb.setItemToolTip('pack_pdt_delete','<?php echo _l('Remove the selected product from the pack',1)?>');
            prop_tb._packProduct_tb.addButton("exportcsv", 100, "", "<?php echo getIcon("page_excel.png"); ?>", "<?php echo getIcon("page_excel.png"); ?>");
            prop_tb._packProduct_tb.setItemToolTip('exportcsv','<?php echo _l('Export grid to clipboard in CSV format for MSExcel with tab delimiter.')?>');
            prop_tb._packProduct_tb.addInput("search",100,'#searchPdtForPack',200);
            prop_tb._packProduct_tb.setItemToolTip('search','<?php echo _l('Search a product/combination to add in pack')?>');
            prop_tb._packProduct_tb.attachEvent("onClick", function(id){
                if (id=='pack_refresh')
                {
                    displayPackProduct();
                }
                else if (id=='pack_pdt_delete')
                {
                    if(lastProductSelID!=undefined && lastProductSelID!=null && lastProductSelID!="" && lastProductSelID!=0)
                    {
                        if(lastPdtPackSelectedId!=undefined && lastPdtPackSelectedId!=null && lastPdtPackSelectedId!="" && lastPdtPackSelectedId!=0)
                        {
                            if (confirm('<?php echo _l('Do you want to remove this product from this pack?',1)?>')){
                                $.post("index.php?ajax=1&act=cat_pack_update&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),
                                {
                                    'id_pack': lastProductSelID,
                                    'id_product': lastPdtPackSelectedId,
                                    'action': 'delete'
                                },
                                function(data)
                                {
                                    displayPackProduct();
                                });
                            }
                        }
                    }
                }
                else if (id=='exportcsv'){
                    displayQuickExportWindow(prop_tb._packProductGrid,1);
                }
            });

            var inputSearch = $("input[value=#searchPdtForPack]");
            inputSearch.val("");
            inputSearch.attr("placeholder",'<?php echo _l('Search a product/combination to add in pack')?>');
            inputSearch.autocomplete("index.php?ajax=1&act=cat_pack_search",{
                forceUrl: true,
                minChars: 1,
                max: 20,
                width: 500,
                cacheLength:0,
                selectFirst: false,
                scroll: false,
                blockSubmit:true,
                dataType: "json",
                formatItem: function(data, i, max, value, term){
                    return value;
                },
                parse: function(data){
                    var mytab = new Array();
                    for (var i = 0; i < data.length; i++){
                        mytab[mytab.length]={
                            data: data[i],
                            value: data[i].pname
                        };
                    }
                    return mytab;
                },
                extraParams:{
                    ajaxSearch: 1
                }
            })
                .result(function(event, data, formatted){
                    if(lastProductSelID!=undefined && lastProductSelID!=null && lastProductSelID!="" && lastProductSelID!=0)
                    {
                        $.post("index.php?ajax=1&act=cat_pack_update&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),
                        {
                            'id_pack': lastProductSelID,
                            'id_product': data.id_product,
                            'id_product_attribute': data.id_product_attribute,
                            'action': 'insert'
                        },
                        function(data)
                        {
                            displayPackProduct();
                        });
                    }
                    return false;
                })


            prop_tb._packProductGrid = prop_tb._packProduct.attachGrid();
            prop_tb._packProductGrid._name='_packProductGrid';
            prop_tb._packProductGrid.setImagePath("lib/js/imgs/");
            prop_tb._packProductGrid.enableDragAndDrop(false);
            prop_tb._packProductGrid.enableMultiselect(false);
            
            // UISettings
            prop_tb._packProductGrid._uisettings_prefix='cat_pack';
            prop_tb._packProductGrid._uisettings_name=prop_tb._packProductGrid._uisettings_prefix;
            prop_tb._packProductGrid._first_loading=1;

            // UISettings
            initGridUISettings(prop_tb._packProductGrid);

            prop_tb._packProductGrid.attachEvent("onEditCell",function(stage,rId,cInd,nValue,oValue)
            {
                if(lastProductSelID!=undefined && lastProductSelID!=null && lastProductSelID!="" && lastProductSelID!=0)
                {
                    idxQuantity=prop_tb._packProductGrid.getColIndexById('quantity');
                    if(stage==2 && idxQuantity==cInd)
                    {
                        var action = "quantity";
                        var value = nValue;

                        $.post("index.php?ajax=1&act=cat_pack_update&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),
                            {
                                'id_pack': lastProductSelID,
                                'id_product': rId,
                                'id_product_attribute': 0,
                                'value': value,
                                'action': action
                            },
                            function(data)
                            {});
                    }
                }
                return true;
            });

            prop_tb._packProductGrid.attachEvent("onRowSelect",function (idpdt){
                before = lastPdtPackSelectedId;
                lastPdtPackSelectedId = idpdt;
                if (before != lastPdtPackSelectedId){
                    displayPackCombi();
                }
            });

            // COMBI
            prop_tb._PackCombi = prop_tb._packLayout.cells('b');
            prop_tb._PackCombi.setText('<?php echo _l('Combinations',1)?>');

            prop_tb._PackCombi_tb = prop_tb._PackCombi.attachToolbar();
             <?php echo(_s('APP_USE_NEW_ICONS') ? "prop_tb._PackCombi_tb.setIconset('awesome');"."\r\n" : ''); ?>
            prop_tb._PackCombi_tb.addButton("pack_combi_refresh", 100, "", "<?php echo getIcon("arrow_refresh.png"); ?>", "<?php echo getIcon("arrow_refresh.png"); ?>");
            prop_tb._PackCombi_tb.setItemToolTip('pack_combi_refresh','<?php echo _l('Refresh grid',1)?>');
            prop_tb._PackCombi_tb.addButton("exportcsv", 100, "", "<?php echo getIcon("page_excel.png"); ?>", "<?php echo getIcon("page_excel.png"); ?>");
            prop_tb._PackCombi_tb.setItemToolTip('exportcsv','<?php echo _l('Export grid to clipboard in CSV format for MSExcel with tab delimiter.')?>');
            prop_tb._PackCombi_tb.attachEvent("onClick", function(id){
                if (id=='pack_combi_refresh')
                {
                    displayPackCombi();
                }
                else if (id=='exportcsv'){
                    displayQuickExportWindow(prop_tb._PackCombiGrid,1);
                }
            });

            prop_tb._PackCombiGrid = prop_tb._PackCombi.attachGrid();
            prop_tb._PackCombiGrid._name='_PackCombiGrid';
            prop_tb._PackCombiGrid.setImagePath("lib/js/imgs/");
            prop_tb._PackCombiGrid.enableDragAndDrop(false);
            prop_tb._PackCombiGrid.enableMultiselect(false);

            prop_tb._PackCombiGrid.attachEvent("onEditCell",function(stage,rId,cInd,nValue,oValue)
            {
                if(lastProductSelID!=undefined && lastProductSelID!=null && lastProductSelID!="" && lastProductSelID!=0)
                {
                    if(lastPdtPackSelectedId!=undefined && lastPdtPackSelectedId!=null && lastPdtPackSelectedId!="" && lastPdtPackSelectedId!=0)
                    {
                        idxPresent=prop_tb._PackCombiGrid.getColIndexById('present');
                        idxQuantity=prop_tb._PackCombiGrid.getColIndexById('quantity');
                        if(stage==1 && idxPresent==cInd)
                        {
                            var action = "combi_present";
                            var value = prop_tb._PackCombiGrid.cells(rId,cInd).isChecked();

                            $.post("index.php?ajax=1&act=cat_pack_update&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),
                            {
                                'id_pack': lastProductSelID,
                                'id_product': lastPdtPackSelectedId,
                                'id_product_attribute': rId,
                                'value': value,
                                'action': action
                            },
                            function(data)
                            {
                                displayPackCombi();
                            });
                        }
                        else if(stage==2 && idxQuantity==cInd)
                        {
                            var action = "quantity";
                            var value = nValue;

                            $.post("index.php?ajax=1&act=cat_pack_update&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),
                            {
                                'id_pack': lastProductSelID,
                                'id_product': lastPdtPackSelectedId,
                                'id_product_attribute': rId,
                                'value': value,
                                'action': action
                            },
                            function(data)
                            {});
                        }
                    }
                }
                return true;
            });

            // UISettings
            prop_tb._PackCombiGrid._uisettings_prefix='cat_pack_combi';
            prop_tb._PackCombiGrid._uisettings_name=prop_tb._PackCombiGrid._uisettings_prefix;
            prop_tb._PackCombiGrid._first_loading=1;

            // UISettings
            initGridUISettings(prop_tb._PackCombiGrid);

            needInitpack=0;
        }
    }

    function setPropertiesPanel_pack(id){
        if (id=='pack')
        {
            if(lastProductSelID!=undefined && lastProductSelID!="")
            {
                idxProductName=cat_grid.getColIndexById('name');
                dhxLayout.cells('b').setText('<?php echo _l('Properties',1).' '._l('of',1)?> '+cat_grid.cells(lastProductSelID,idxProductName).getValue());
            }
            hidePropTBButtons();
            prop_tb.setItemText('panel', '<?php echo _l('Pack',1)?>');
            prop_tb.setItemImage('panel', '<?php echo getIcon("box.png"); ?>');
            needInitpack = 1;
            initpack();
            propertiesPanel='pack';
            if (lastProductSelID!=0)
            {
                displayPackProduct();
            }
        }
    }
    prop_tb.attachEvent("onClick", setPropertiesPanel_pack);

    function displayPackProduct()
    {
        if(lastProductSelID!=undefined && lastProductSelID!=null && lastProductSelID!="" && lastProductSelID!=0)
        {
            prop_tb._packProductGrid.clearAll(true);
            $.post("index.php?ajax=1&act=cat_pack_get&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),{'id_pack': lastProductSelID},function(data)
            {
                prop_tb._packProductGrid.parse(data);
                nb=prop_tb._packProductGrid.getRowsNum();
                prop_tb._packProductGrid._rowsNum=nb;

                // UISettings
                loadGridUISettings(prop_tb._packProductGrid);
                prop_tb._packProductGrid._first_loading=0;
            });
        }
    }

    function displayPackCombi()
    {
        prop_tb._PackCombiGrid.clearAll(true);
        if(lastProductSelID!=undefined && lastProductSelID!=null && lastProductSelID!="" && lastProductSelID!=0)
        {
            if(lastPdtPackSelectedId!=undefined && lastPdtPackSelectedId!=null && lastPdtPackSelectedId!="" && lastPdtPackSelectedId!=0)
            {
                $.post("index.php?ajax=1&act=cat_pack_combi_get&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),{'id_pack': lastProductSelID, 'id_product':lastPdtPackSelectedId},function(data)
                {
                    prop_tb._PackCombiGrid.parse(data);
                    nb=prop_tb._PackCombiGrid.getRowsNum();
                    prop_tb._PackCombiGrid._rowsNum=nb;

                    // UISettings
                    loadGridUISettings(prop_tb._PackCombiGrid);
                    prop_tb._PackCombiGrid._first_loading=0;
                });
            }
        }
    }


    cat_grid.attachEvent("onRowSelect",function (idproduct){
        if (propertiesPanel=='pack'){
            displayPackProduct();
        }
    });


<?php }
