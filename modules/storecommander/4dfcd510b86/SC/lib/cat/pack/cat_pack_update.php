<?php


$id_lang=intval(Tools::getValue('id_lang'));
$action=Tools::getValue('action','');

if(!empty($action))
{
    switch ($action){
        case 'combi_present':

            $id_pack=intval(Tools::getValue('id_pack'));
            $id_product=intval(Tools::getValue('id_product'));
            $id_product_attribute=intval(Tools::getValue('id_product_attribute'));
            $value=Tools::getValue('value');
            if($value=="true")
                $value = 1;
            else
                $value = 0;

            if(!empty($value))
            {
                $sql = "SELECT * FROM "._DB_PREFIX_."pack WHERE id_product_pack='".$id_pack."' AND id_product_item='".$id_product."' AND id_product_attribute_item='".$id_product_attribute."'";
                $res = Db::getInstance()->ExecuteS($sql);
                if(empty($res[0]["id_product_attribute_item"]))
                {
                    $sql = "INSERT INTO "._DB_PREFIX_."pack (id_product_pack,id_product_item,id_product_attribute_item,quantity)
                        VALUES ('".$id_pack."','".$id_product."','".$id_product_attribute."','1')";
                    Db::getInstance()->Execute($sql);
                }
            }
            else
            {
                $sql = "DELETE FROM "._DB_PREFIX_."pack WHERE id_product_pack='".$id_pack."' AND id_product_item='".$id_product."' AND id_product_attribute_item='".$id_product_attribute."'";
                Db::getInstance()->Execute($sql);
            }

            break;
        case 'quantity':
            $id_pack=intval(Tools::getValue('id_pack'));
            $id_product=intval(Tools::getValue('id_product'));
            $id_product_attribute=intval(Tools::getValue('id_product_attribute',0));
            $value=(int)Tools::getValue('value');

            $sql = "SELECT id_product_item FROM "._DB_PREFIX_."pack WHERE id_product_pack='".$id_pack."' AND id_product_item='".$id_product."' AND id_product_attribute_item='".$id_product_attribute."'";
            $res = Db::getInstance()->ExecuteS($sql);
            if(!empty($res[0]["id_product_item"]))
            {
                $sql = "UPDATE "._DB_PREFIX_."pack SET quantity='".(int)$value."' WHERE id_product_pack='".$id_pack."' AND id_product_item='".$id_product."' AND id_product_attribute_item='".$id_product_attribute."'";
                Db::getInstance()->Execute($sql);
            }

            break;
        case 'delete':
            $id_pack=intval(Tools::getValue('id_pack'));
            $id_product=intval(Tools::getValue('id_product'));


            $sql = "DELETE FROM "._DB_PREFIX_."pack WHERE id_product_pack='".$id_pack."' AND id_product_item='".$id_product."'";
            Db::getInstance()->Execute($sql);

            break;
        case 'insert':
            $id_pack=intval(Tools::getValue('id_pack'));
            $id_product=intval(Tools::getValue('id_product'));
            $id_product_attribute=intval(Tools::getValue('id_product_attribute'));

            if(!empty($id_pack) && !empty($id_product))
            {
                $sql = "INSERT INTO "._DB_PREFIX_."pack (id_product_pack,id_product_item,id_product_attribute_item,quantity)
                        VALUES ('".$id_pack."','".$id_product."','".$id_product_attribute."','1')";
                Db::getInstance()->Execute($sql);
            }

            break;
    }
}