<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/

$id_attribute = explode(',', Tools::getValue('id_attribute', '0'));
$action = Tools::getValue('action', '0');
if ($id_attribute == 0) exit;
if ($action == 'delete') {
    foreach ($id_attribute AS $v) {
        if (file_exists(_PS_COL_IMG_DIR_ . $v . '.jpg')) {
            @unlink(_PS_COL_IMG_DIR_ . $v . '.jpg');
        }
    }
}
if ($action == 'duplicate') {
    $attributes = explode(',', Tools::getValue('attributes', 0));
    $id_group = Tools::getValue('id_group', 0);
    if ($id_group == 0 || $attributes == 0) exit;
    foreach ($attributes AS $a) {
        $srcAttr = new Attribute($a);
        $newAttr = new Attribute();
        $newAttr->id_attribute_group = $id_group;
        foreach ($languages AS $lang) {
            $newAttr->name[$lang['id_lang']] = ($srcAttr->name[$lang['id_lang']] != '' ? $srcAttr->name[$lang['id_lang']] : ' ');
        }
        $newAttr->color = $srcAttr->color;
        $newAttr->add();
        if (file_exists(_PS_COL_IMG_DIR_ . $a . '.jpg'))
            @copy(_PS_COL_IMG_DIR_ . $a . '.jpg', _PS_COL_IMG_DIR_ . $newAttr->id . '.jpg');
    }
}
if ($action == 'add') {
    $id_attribute = $id_attribute[0];
    ?>
    <style type="text/css">@import url(<?php echo SC_PLUPLOAD;?>js/vault/vault.min.css);</style>
    <script type="text/javascript" src="<?php echo SC_JQUERY; ?>"></script>
    <script type="text/javascript" src="<?php echo SC_JSFUNCTIONS; ?>"></script>
    <script type="text/javascript" src="<?php echo SC_PLUPLOAD; ?>js/vault/vault.min.js"></script>
    <body style="margin:0;">
    <div id="file_uploader"></div>
    <script>
        <?php require_once SC_PLUPLOAD.'js/vault/vault_lang.php'; ?>
        let authorized_extensions = ["jpg"];
        let vaultObject = new dhx.Vault("file_uploader", {
            uploader: {
                target: 'index.php?ajax=1&act=all_upload&obj=attrtexture&id_attribute=<?php echo $id_attribute;?>',
                autosend: false
            },
            mode: "grid",

        });
        vaultObject.events.on("BeforeAdd", function (item) {
            let extension = item.file.name.split('.').pop();
            if (authorized_extensions.indexOf(extension) >= 0) {
                let fileSize = item.file.size;
                let fileSizeMo = fileSize / 1024 / 1024;
                <?php $limitSizeMo = (Configuration::get('PS_PRODUCT_PICTURE_MAX_SIZE') / 1024 / 1024); ?>
                let limitSize = <?php echo $limitSizeMo;?>;
                if (fileSizeMo > limitSize) {
                    dhx.message({
                        text: "<?php echo _l('The file is too large. Maximum size allowed is: %1$d Mo. The file you are trying to upload is ', 1, array($limitSizeMo));?> " + fileSizeMo.toFixed(2) + " Mo",
                        css: "dhx-error",
                        expire: 4000
                    });
                    return false;
                }
            } else {
                dhx.message({
                    text: "<?php echo _l("Wrong file format", 1);?> (" + authorized_extensions.join(',') + " <?php echo _l('only',1);?>)",
                    css: "dhx-error",
                    expire: 4000
                });
                return false;
            }
        });
        vaultObject.events.on("UploadComplete", function (files) {
            var error = 0;
            files.forEach(function (item) {
                let file_response = JSON.parse(item.request.response);
                if (file_response.error !== null) {
                    dhx.message({
                        text: "code:" + file_response.error.code + " " + file_response.error.message,
                        css: "dhx-error",
                        expire: 4000
                    });
                    error = error + 1;
                }
            });
            if (error === 0) {
                top.displayAttributes();
                top.wAttributeTexture.hide();
            }
        });
    </script>
    </body>
    <?php
}
