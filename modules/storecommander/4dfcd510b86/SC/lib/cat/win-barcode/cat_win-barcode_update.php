<?php
$type = Tools::getValue('type',null);
$config = Tools::getValue('config',null);
$error = false;
$res = array(
    'error' => 0,
    'message' => '',
    'pdt_updated' => array()
);

if(!empty($config) && !empty($type)){
    switch($type){
        case 'ref':
            $id_product_attribute = null;
            $sql = 'SELECT id_product,id_product_attribute
                    FROM ' . _DB_PREFIX_ . 'product_attribute 
                    WHERE ean13 = "' . pSQL($config['code']) . '"';
            $res = Db::getInstance()->getRow($sql);
            if (!empty($res)) {
                $id_product = (int)$res['id_product'];
                $id_product_attribute = (int)$res['id_product_attribute'];
            } else {
                $sql = 'SELECT id_product
                        FROM ' . _DB_PREFIX_ . 'product 
                        WHERE ean13 = "' . pSQL($config['code']) . '"';
                $id_product = Db::getInstance()->getValue($sql);
            }
            if(!empty($id_product)) {
                if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
                    $product = new Product((int)$id_product, false, null, (int)SCI::getSelectedShop());
                } else {
                    $product = new Product((int)$id_product);
                }
                $process_value = str_replace(' ','',$config['process_value']);
                if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
                    $process = SCI::updateQuantity($id_product, $id_product_attribute, $process_value, (int)SCI::getSelectedShop());
                } else {
                    $process = SCI::updateQuantity($id_product, $id_product_attribute, $process_value, null);
                }
                if($process){
                    $prd = implode('_',array($id_product,$id_product_attribute,$product->id_category_default));
                    $res['pdt_updated'][] = $prd;
                } else {
                    $error = 1;
                }
            } else {
                $error = 2;
            }
            break;
        default:
            foreach($config as $row) {
                list($id_product,$id_product_attribute,$id_category_default) = explode('_',$row['code']);
                $id_product_attribute = ($id_product_attribute > 0 ? $id_product_attribute : null);
                $process_value = str_replace(' ','',$row['process_value']);
                if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
                    $process = SCI::updateQuantity($id_product, $id_product_attribute, $process_value, (int)SCI::getSelectedShop());
                } else {
                    $process = SCI::updateQuantity($id_product, $id_product_attribute, $process_value);
                }
                if($process){
                    $res['pdt_updated'][] = $row['code'];
                } else {
                    $error = 3;
                    break;
                }
            }
    }
    if(empty($error))
    {
        $res['error'] = 0;
        $res['message'] = _l('Products updated successfully');
    } else {
        $res['error'] = $error;
        $res['message'] = _l('Error during product update');
    }
    die(json_encode($res));
}
