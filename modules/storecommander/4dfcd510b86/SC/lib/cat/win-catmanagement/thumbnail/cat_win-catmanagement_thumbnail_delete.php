<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/
$id_lang = (int)Tools::getValue('id_lang');
$ids = Tools::getValue('ids', 0);
$idlist = explode(",", $ids);


foreach ($idlist AS $id_category) {
    $filename = '/' . (int)$id_category . '_thumb.jpg';
    $filepath = _PS_CAT_IMG_DIR_ . $filename;
    $image = "";
    if (file_exists($filepath)){
        unlink($filepath);
    }
}