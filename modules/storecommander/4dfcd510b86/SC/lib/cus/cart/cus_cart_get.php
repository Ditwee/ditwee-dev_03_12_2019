<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/

$id_customer = (int)Tools::getValue('id_customer', null);
$id_lang = (int)Tools::getValue('id_lang', Configuration::get('PS_LANG_DEFAULT'));

$ui_setting_name = 'cus_cart';
$colSettings = array();
$grids = array();
// SETTINGS, FILTERS AND COLONNES
include($ui_setting_name . "_data_fields.php");
include($ui_setting_name . "_data_views.php");
$cols = explode(',', $grids);


function getFooterColSettings()
{
    global $cols, $colSettings;

    $footer = '';
    foreach ($cols AS $id => $col) {
        if (sc_array_key_exists($col, $colSettings) && sc_array_key_exists('footer', $colSettings[$col]))
            $footer .= $colSettings[$col]['footer'] . ',';
        else
            $footer .= ',';
    }
    return $footer;
}

function getFilterColSettings()
{
    global $cols, $colSettings;

    $filters = '';
    foreach ($cols AS $id => $col) {
        if ($colSettings[$col]['filter'] == "na")
            $colSettings[$col]['filter'] = "";
        $filters .= $colSettings[$col]['filter'] . ',';
    }
    $filters = trim($filters, ',');
    return $filters;
}

function getColSettingsAsXML()
{
    global $cols, $colSettings, $ui_setting_name;

    $uiset = uisettings::getSetting($ui_setting_name);
    $tmp = explode('|', $uiset);
    $tmp = explode('-', $tmp[2]);
    $sizes = array();
    foreach ($tmp AS $v) {
        $s = explode(':', $v);
        $sizes[$s[0]] = $s[1];
    }
    $tmp = explode('|', $uiset);
    $tmp = explode('-', $tmp[0]);
    $hidden = array();
    foreach ($tmp AS $v) {
        $s = explode(':', $v);
        $hidden[$s[0]] = $s[1];
    }
    $xml = '';
    foreach ($cols AS $id => $col) {
        $xml .= '<column id="' . $col . '"' . (sc_array_key_exists('format', $colSettings[$col]) ?
                ' format="' . $colSettings[$col]['format'] . '"' : '') .
            ' width="' . (sc_array_key_exists($col, $sizes) ? $sizes[$col] : $colSettings[$col]['width']) . '"' .
            ' hidden="' . (sc_array_key_exists($col, $hidden) ? $hidden[$col] : 0) . '"' .
            ' align="' . $colSettings[$col]['align'] . '" 
                        type="' . $colSettings[$col]['type'] . '" 
                        sort="' . $colSettings[$col]['sort'] . '" 
                        color="' . $colSettings[$col]['color'] . '">' . $colSettings[$col]['text'];
        if (!empty($colSettings[$col]['options'])) {
            foreach ($colSettings[$col]['options'] AS $k => $v) {
                $xml .= '<option value="' . str_replace('"', '\'', $k) . '"><![CDATA[' . $v . ']]></option>';
            }
        }
        $xml .= '</column>' . "\n";
    }
    return $xml;
}

function getData()
{
    global $id_customer, $cols, $id_lang;

    $xml = '';

    $sql = 'SELECT c.`id_cart`
            FROM ' . _DB_PREFIX_ . 'cart c
            WHERE NOT EXISTS (SELECT 1 FROM ' . _DB_PREFIX_ . 'orders o 
                                WHERE o.`id_cart` = c.`id_cart`
                                AND o.`id_customer` = ' . (int)$id_customer . ')
            AND c.`id_customer` = ' . (int)$id_customer;
    if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
        $sql .= ' AND c.id_shop = ' . (int)SCI::getSelectedShop();
    }
    $sql .= ' ORDER BY c.`date_upd` DESC';
    $id_cart = Db::getInstance()->getValue($sql);

    if (!empty($id_cart)) {
        $sql = 'SELECT id_product,id_product_attribute,date_add
            FROM ' . _DB_PREFIX_ . 'cart_product 
            WHERE id_cart = ' . (int)$id_cart;
        $info_cart_product = Db::getInstance()->executeS($sql);
        $tmp = array();
        if (!empty($info_cart_product)) {
            foreach ($info_cart_product as $row) {
                $tmp[$row['id_product'] . '_' . $row['id_product_attribute']] = $row['date_add'];
            }
            $info_cart_product = $tmp;
        }
        $cart = new Cart($id_cart);
        $cart_products = $cart->getProducts();
        if (!empty($cart_products)) {
            foreach ($cart_products as $product) {
                $xml .= '<row id="' . $product['id_product'] . '_' . $product['id_product_attribute'] . '">';
                foreach ($cols as $col) {
                    switch ($col) {
                        case 'id_cart':
                            $xml .= '<cell>' . (int)$id_cart . '</cell>';
                            break;
                        case 'product_date_add':
                            $date = '';
                            if (array_key_exists($product['id_product'] . '_' . $product['id_product_attribute'], $info_cart_product)) {
                                $date = $info_cart_product[$product['id_product'] . '_' . $product['id_product_attribute']];
                            }
                            $xml .= '<cell><![CDATA[' . $date . ']]></cell>';
                            break;
                        case 'product_name':
                            $xml .= '<cell><![CDATA[' . $product['name'] . ': ' . $product['attributes_small'] . ']]></cell>';
                            break;
                        default:
                            $xml .= '<cell><![CDATA[' . $product[$col] . ']]></cell>';
                    }
                }
                $xml .= '</row>';
            }
        }
    }


    return $xml;
}

//XML HEADER
if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
    header("Content-type: application/xhtml+xml");
} else {
    header("Content-type: text/xml");
}
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
echo '<rows><head>';
echo getColSettingsAsXML();
echo '<afterInit>
        <call command="attachHeader"><param>' . getFilterColSettings() . '</param></call>
        <call command="attachFooter"><param><![CDATA[' . getFooterColSettings() . ']]></param></call>
    </afterInit>';
echo '</head>' . "\n";

echo '<userdata name="uisettings">' . uisettings::getSetting($ui_setting_name) . '</userdata>' . "\n";
sc_ext::readCustomPropSpePriceGridConfigXML('gridUserData');

echo getData();
?>
</rows>