<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/
$title = _l('Cart',1);
$icon = getIcon('basket.png');
?>
prop_tb.addListOption('panel', 'cart', 3, "button", '<?php echo $title; ?>', "<?php echo $icon; ?>");
allowed_properties_panel[allowed_properties_panel.length] = "cart";

prop_tb.addButton("cart_refresh", 100, "", "<?php echo getIcon("arrow_refresh.png"); ?>", "<?php echo getIcon("arrow_refresh.png"); ?>");
prop_tb.setItemToolTip('cart_refresh','<?php echo _l('Refresh grid', 1) ?>');
prop_tb.addButton("exportcsv", 100, "", "<?php echo getIcon("page_excel.png"); ?>", "<?php echo getIcon("page_excel.png"); ?>");
prop_tb.setItemToolTip('exportcsv','<?php echo _l('Export grid to clipboard in CSV format for MSExcel with tab delimiter.', 1)?>');


needinitcart = 1;
function initcart(){
    if (needinitcart)
    {
        prop_tb._cartLayout = dhxLayout.cells('b').attachLayout('1C');
        prop_tb._cartLayout.cells('a').hideHeader();
        dhxLayout.cells('b').showHeader();
        prop_tb._cartGrid = prop_tb._cartLayout.cells('a').attachGrid();
        prop_tb._cartGrid.setImagePath("lib/js/imgs/");

        // UISettings
        prop_tb._cartGrid._uisettings_prefix='cus_cart';
        prop_tb._cartGrid._uisettings_name=prop_tb._cartGrid._uisettings_prefix;
        prop_tb._cartGrid._first_loading=1;

        // UISettings
        initGridUISettings(prop_tb._cartGrid);

        needinitcart=0;
    }
}


function setPropertiesPanel_cart(id){
    if (id=='cart')
    {
        if(lastCustomerSelID!=undefined && lastCustomerSelID!="")
        {
            idxLastname=cus_grid.getColIndexById('lastname');
            idxFirstname=cus_grid.getColIndexById('firstname');
            dhxLayout.cells('b').setText('<?php echo _l('Properties', 1) . ' ' . _l('of', 1) ?> '+cus_grid.cells(lastCustomerSelID,idxFirstname).getValue()+" "+cus_grid.cells(lastCustomerSelID,idxLastname).getValue());
        }
        hidePropTBButtons();
        prop_tb.showItem('exportcsv');
        prop_tb.showItem('cart_refresh');
        prop_tb.setItemText('panel', '<?php echo $title; ?>');
        prop_tb.setItemImage('panel', '<?php echo $icon; ?>');
        needinitcart = 1;
        initcart();
        propertiesPanel='cart';
        if (lastCustomerSelID!=0)
        {
            displayCart();
        }
    }
    if (id=='cart_refresh')
    {
        displayCart();
    }
    if (id=='exportcsv'){
        displayQuickExportWindow(prop_tb._cartGrid,1);
    }
}
prop_tb.attachEvent("onClick", setPropertiesPanel_cart);


function displayCart()
{
    var id_customer = "";
    if(gridView!="grid_address") {
        id_customer = lastCustomerSelID;
    } else {
        idxIdCustomer=cus_grid.getColIndexById('id_customer');
        id_customer = cus_grid.cells(lastCustomerSelID,idxIdCustomer).getValue();
    }
    prop_tb._cartGrid.clearAll(true);
    prop_tb._cartGrid.load("index.php?ajax=1&act=cus_cart_get&id_customer="+id_customer+"&id_lang="+SC_ID_LANG,function()
    {
        nb=prop_tb._cartGrid.getRowsNum();
        prop_tb._sb.setText(nb+' '+(nb>1?'<?php echo _l('products',1)?>':'<?php echo _l('product',1)?>'));

        // UISettings
        loadGridUISettings(prop_tb._cartGrid);

        // UISettings
        prop_tb._cartGrid._first_loading=0;
    });
}


cus_grid.attachEvent("onRowSelect",function (idproduct){
    if (propertiesPanel=='cart' && !dhxLayout.cells('b').isCollapsed()){
        displayCart();
    }
});