<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/

$id_lang=(int)Tools::getValue('id_lang');
$id_customer=(int)Tools::getValue('id_customer');
$id_shop=(int)Tools::getValue('id_shop');

$id_cart = 0;

function getRowsFromDB(){
    global $id_lang,$id_customer,$id_shop,$id_cart;

    if(version_compare(_PS_VERSION_, '1.5.0.0', '>='))
    {
        $shop_where = $id_shop;
    }
    if (version_compare(_PS_VERSION_, '1.6.0.10', '>='))
    {
        $inner = "";

        if (SCMS && $shop_where>0)
            $inner = " INNER JOIN "._DB_PREFIX_."tax_rules_group_shop trgs ON (trgs.id_tax_rules_group = trg.id_tax_rules_group AND trgs.id_shop = '".(int)$shop_where."')";

        $sql='SELECT trg.name, trg.id_tax_rules_group,t.rate, trg.deleted
			FROM `'._DB_PREFIX_.'tax_rules_group` trg
			LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (trg.`id_tax_rules_group` = tr.`id_tax_rules_group` AND tr.`id_country` = '.(int)SCI::getDefaultCountryId().' AND tr.`id_state` = 0)
  	  			LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
  	  		'.$inner.'
	    	WHERE 1
  	  		ORDER BY trg.deleted ASC, trg.name ASC';
        $res=Db::getInstance()->ExecuteS($sql);
        foreach($res as $row){
            if ($row['name']=='') $row['name']=' ';

            if($row['deleted']=="1")
                $row['name'] = _l("(deleted)")." ".$row['name'];

            $tax[$row['id_tax_rules_group']]=$row['rate'];
        }
    }
    elseif (version_compare(_PS_VERSION_, '1.4.0.0', '>='))
    {
        $inner = "";

        if (version_compare(_PS_VERSION_, '1.6.0.0', '>=') && SCMS && $shop_where>0)
            $inner = " INNER JOIN "._DB_PREFIX_."tax_rules_group_shop trgs ON (trgs.id_tax_rules_group = trg.id_tax_rules_group AND trgs.id_shop = '".(int)$shop_where."')";

        $sql='SELECT trg.name, trg.id_tax_rules_group,t.rate
			FROM `'._DB_PREFIX_.'tax_rules_group` trg
			LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (trg.`id_tax_rules_group` = tr.`id_tax_rules_group` AND tr.`id_country` = '.(int)SCI::getDefaultCountryId().' AND tr.`id_state` = 0)
  	  			LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
  	  		'.$inner.'
	    	WHERE trg.active=1';
        $res=Db::getInstance()->ExecuteS($sql);
        foreach($res as $row){
            if ($row['name']=='') $row['name']=' ';
            $tax[$row['id_tax_rules_group']]=$row['rate'];
        }
    }else{
        $sql = "SELECT id_tax,rate FROM "._DB_PREFIX_."tax";
        $res=Db::getInstance()->ExecuteS($sql);
        foreach($res as $row){
            $tax[$row['id_tax']]=$row['rate'];
        }
    }

    $decimal = (_s("CAT_PROD_PRICEWITHOUTTAX4DEC")=="1"?4:2);

    $sql = '
			SELECT c.id_cart, o.id_order, o.valid
			FROM '._DB_PREFIX_.'cart c
			    LEFT JOIN '._DB_PREFIX_.'orders o ON (o.id_cart=c.id_cart)
			WHERE c.id_customer = "'.(int)$id_customer.'"
			    '.(SCMS && !empty($id_shop)?' AND c.id_shop = "'.(int)$id_shop.'" ':'').'
			ORDER BY c.date_add DESC';
    $cart=Db::getInstance()->getRow($sql);
    $xml='';
    if(!empty($cart["id_cart"]) && (empty($cart["id_order"]) || (!empty($cart["id_order"]) && empty($cart["valid"]))))
    {
        $id_cart = $cart["id_cart"];
        $sql = '
			UPDATE '._DB_PREFIX_.'cart SET date_upd = "'.date("Y-m-d H:i:s").'"
			WHERE id_cart = "'.(int)$cart["id_cart"].'"';
        Db::getInstance()->Execute($sql);

        $sql = '
			SELECT cp.id_product, cp.id_product_attribute, cp.quantity, pl.name as p_name,
			        p.id_category_default,
			        p.reference, pa.reference as pa_reference,
			        p.price, '.(version_compare(_PS_VERSION_, '1.4.0.0', '>=')?'p.id_tax_rules_group':'p.id_tax').", ".(version_compare(_PS_VERSION_, '1.3.0.0', '>=')?'p.ecotax,':'').'
                    pa.price AS pa_price, pa.ecotax AS pa_ecotax
                     '.(SCMS ? " ,ps.id_category_default,ps.price, ps.id_tax_rules_group, ps.ecotax,pas.price AS pa_price, pas.ecotax AS pa_ecotax":"").'
			FROM '._DB_PREFIX_.'cart_product cp    
			    LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (cp.id_product = pl.id_product AND pl.id_lang='.intval($id_lang).(version_compare(_PS_VERSION_, '1.5.0.0', '>=')?" AND pl.id_shop=".(int)$shop_where:"").')
			    INNER JOIN '._DB_PREFIX_.'product p ON (cp.id_product = p.id_product)
			    '.(SCMS?" LEFT JOIN `"._DB_PREFIX_."product_shop` ps ON (ps.id_product = p.id_product AND ps.id_shop=".(int)$shop_where.") ":"").'
			    LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (cp.id_product_attribute=pa.id_product_attribute)
			        '.(SCMS?" LEFT JOIN `"._DB_PREFIX_."product_attribute_shop` pas ON (pas.id_product_attribute = pa.id_product_attribute AND pas.id_shop=".$shop_where.") ":"").'
            WHERE cp.id_cart = "'.(int)$cart["id_cart"].'"
			ORDER BY cp.date_add DESC';
        $res=Db::getInstance()->ExecuteS($sql);

        foreach ($res AS $row)
        {
            $combination_detail = null;
            if(!empty($row['id_product_attribute'])) {
                $prod = new Product($row['id_product']);
                if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
                    $attributes = $prod->getAttributesResume($id_lang);
                    if(!empty($attributes)) {
                        foreach($attributes as $attr) {
                            if($attr['id_product_attribute'] == $row['id_product_attribute']) {
                                $combination_detail = $attr['attribute_designation'];
                                break;
                            }
                        }
                    }
                } else {
                    $detail = array();
                    $attributes = $prod->getAttributeCombinaisons($id_lang);
                    if(!empty($attributes)) {
                        foreach($attributes as $attr) {
                            if($attr['id_product_attribute'] == $row['id_product_attribute']) {
                                $detail[] = $attr['group_name'].' : '.$attr['attribute_name'];
                            }
                        }
                        $combination_detail = implode(', ', $detail);
                    }
                }
            }

            $price = $row['price']+(!empty($row['pa_price'])?$row['pa_price']:0);
            $price = number_format($price, $decimal, '.', '');
            if (version_compare(_PS_VERSION_, '1.4.0.0', '>='))
                $row['id_tax']=$row['id_tax_rules_group'];
            $taxrate = $tax[intval($row['id_tax'])];
            if(!empty($row['pa_price']))
            {
                if(version_compare(_PS_VERSION_, '1.6.0.0', '>=') || (version_compare(_PS_VERSION_, '1.3.0.0', '>=') && ($row['pa_ecotax']*1)==0))
                    $row['pa_ecotax']=$row['ecotax'];
                $ecotax = (_s('CAT_PROD_ECOTAXINCLUDED') ? ( version_compare(_PS_VERSION_, '1.3.0.0', '>=') ? $row['pa_ecotax']*SCI::getEcotaxTaxRate() : $row['pa_ecotax'] ) : 0);

                if (version_compare(_PS_VERSION_, '1.4.0.0', '>='))
                {
                    if(!empty($taxrate))
                        $price_it = number_format(($row['price']+$row['pa_price'])*($taxrate/100+1) + $ecotax, $decimal, '.', '');
                    else
                        $price_it = number_format($row['price']+$row['pa_price'] + $ecotax, $decimal, '.', '');
                }
                elseif (version_compare(_PS_VERSION_, '1.3.0.0', '>='))
                {
                    if(!empty($taxrate))
                        $price_it = number_format($row['price']+$row['pa_price']*($taxrate/100+1)+$ecotax, $decimal, '.', '');
                    else
                        $price_it = number_format($row['price']+$row['pa_price']+$ecotax, $decimal, '.', '');
                }else{
                    if(!empty($taxrate))
                        $price_it = number_format($row['price']+$row['pa_price']*($taxrate/100+1), $decimal, '.', '');
                    else
                        $price_it = number_format($row['price']+$row['pa_price'], $decimal, '.', '');
                }
            }
            else
            {
                $ecotax = (_s('CAT_PROD_ECOTAXINCLUDED') ? ( version_compare(_PS_VERSION_, '1.3.0.0', '>=') ? $row['ecotax']*SCI::getEcotaxTaxRate() : $row['ecotax'] ) : 0);

                $price_it = number_format($row['price']*($taxrate/100+1)+$ecotax, $decimal, '.', '');
            }

            $xml.=("<row id='".$row['id_product']."_".$row['id_product_attribute']."'>");
            $xml.='  	<userdata name="path_pdt">'.$row["id_category_default"].'-'.$row["id_product"].(!empty($row["id_product_attribute"])?'-'.$row["id_product_attribute"]:'').'</userdata>';
            $xml.=("<cell>".$row['id_product']."</cell>");
            $xml.=("<cell>".$row['id_product_attribute']."</cell>");
            $xml.=("<cell>".(!empty($row["pa_reference"])?$row["pa_reference"]:$row["reference"])."</cell>");
            $xml.=("<cell><![CDATA[".$row['p_name'].(!empty($combination_detail) ? " ".$combination_detail : '')."]]></cell>");
            $xml.=("<cell>".$price."</cell>");
            $xml.=("<cell>".$price_it."</cell>");
            $xml.=("<cell><![CDATA[".$row['quantity']."]]></cell>");
            $xml.=("<cell><![CDATA[".number_format($row['quantity']*$price_it, $decimal, '.', '')."]]></cell>");
            $xml.=("</row>");
        }
    }
    else
    {
        $customer = new Customer((int)$id_customer);

        $cart = new Cart();
        $cart->id_customer = (int)$id_customer;
        $cart->id_lang = (int)$id_lang;
        $cart->id_currency = (int)Configuration::get('PS_CURRENCY_DEFAULT');
        $cart->secure_key = $customer->secure_key;
        if(SCMS && !empty($id_shop))
            $cart->id_shop = (int)$id_shop;
        $cart->add();

        if(!empty($cart->id))
            $id_cart = $cart->id;
    }


    return $xml;
}

//XML HEADER
if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) {
    header("Content-type: application/xhtml+xml"); } else {
    header("Content-type: text/xml");
}
echo("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

$xml=getRowsFromDB();
?>
<rows id="0">
    <head>
        <beforeInit>
            <call command="attachHeader"><param><![CDATA[#numeric_filter,#numeric_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#numeric_filter,#numeric_filter]]></param></call>
        </beforeInit>
        <column id="id_product" width="60" type="ro" align="right" sort="int"><?php echo _l('id prod.')?></column>
        <column id="id_product_attribute" width="60" type="ro" align="right" sort="int"><?php echo _l('id prod. attr.')?></column>
        <column id="reference" width="100" type="ro" align="left" sort="str"><?php echo _l('Reference')?></column>
        <column id="product" width="360" type="ro" align="left" sort="str"><?php echo _l('Product')?></column>
        <column id="price" width="80" type="ro" align="right" sort="int"><?php echo _l('Price excl. Tax')?></column>
        <column id="price_it" width="80" type="ro" align="right" sort="int"><?php echo _l('Price incl. Tax')?></column>
        <column id="quantity" width="80" type="edn" align="right" sort="int"><?php echo _l('Quantity')?></column>
        <column id="total_product" width="80" type="ro" align="right" sort="int"><?php echo _l('Total produit')?></column>
    </head>
    <?php
    echo '<userdata name="uisettings">'.uisettings::getSetting('makeOrder_cart_grid').'</userdata>'."\n";
    echo '<userdata name="id_cart">'.$id_cart.'</userdata>'."\n";
    echo $xml;

    $cart = new Cart((int)$id_cart);
    echo '<userdata name="total_et">'.Tools::displayPrice($cart->getOrderTotal(false, Cart::BOTH), Currency::getCurrencyInstance((int)$cart->id_currency), false).'</userdata>'."\n";
    echo '<userdata name="total_it">'.Tools::displayPrice($cart->getOrderTotal(true, Cart::BOTH), Currency::getCurrencyInstance((int)$cart->id_currency), false).'</userdata>'."\n";
    ?>
</rows>
