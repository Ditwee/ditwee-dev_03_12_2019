<?php
/**
 * Store Commander
 *
 * @category administration
 * @author Store Commander - support@storecommander.com
 * @version 2015-09-15
 * @uses Prestashop modules
 * @since 2009
 * @copyright Copyright &copy; 2009-2015, Store Commander
 * @license commercial
 * All rights reserved! Copying, duplication strictly prohibited
 *
 * *****************************************
 * *           STORE COMMANDER             *
 * *   http://www.StoreCommander.com       *
 * *            V 2015-09-15               *
 * *****************************************
 *
 * Compatibility: PS version: 1.1 to 1.6.1
 *
 **/

$id_lang=(int)Tools::getValue('id_lang');
$id_customer=(int)Tools::getValue('id_customer');
$id_shop=(int)Tools::getValue('id_shop',1);
$id_cart=(int)Tools::getValue('id_cart');

$action=Tools::getValue('action');

if(!empty($action) && !empty($id_customer) && !empty($id_cart))
{
    switch ($action) {
        case"update_qty":
            $id_product=Tools::getValue('id_product');
            $quantity=(int)Tools::getValue('quantity');

            list($id_product,$id_product_attribute) = explode("_",$id_product);

            if(!empty($id_product))
            {
                if(!empty($quantity) && is_numeric($quantity))
                {
                    $sql = '
                    UPDATE '._DB_PREFIX_.'cart_product SET quantity = "'.(int)$quantity.'"
                    WHERE id_cart = "'.(int)$id_cart.'" AND id_product="'.(int)$id_product.'" AND id_product_attribute="'.(int)$id_product_attribute.'"';
                    Db::getInstance()->Execute($sql);
                }
                elseif(empty($quantity))
                {
                    $sql = '
                    DELETE FROM '._DB_PREFIX_.'cart_product
                    WHERE id_cart = "'.(int)$id_cart.'" AND id_product="'.(int)$id_product.'" AND id_product_attribute="'.(int)$id_product_attribute.'"';
                    Db::getInstance()->Execute($sql);
                }
            }

            break;
        case"add_product":
            $id_product=Tools::getValue('id_product');
            $quantity=(int)Tools::getValue('quantity','1');

            if(!empty($id_product))
            {
                $ids = explode(",", $id_product);
                foreach ($ids as $id)
                {
                    list($id_product,$id_product_attribute) = explode("_",$id);
                    if(!empty($id_product))
                    {
                        $sql = '
                        SELECT id_cart FROM '._DB_PREFIX_.'cart_product
                        WHERE id_cart = "'.(int)$id_cart.'" AND id_product="'.(int)$id_product.'" AND id_product_attribute="'.(int)$id_product_attribute.'"';
                        $exist = Db::getInstance()->getRow($sql);
                        if(!empty($exist["id_cart"]))
                        {
                            $sql = '
                            UPDATE '._DB_PREFIX_.'cart_product SET quantity = quantity+'.(int)$quantity.'
                            WHERE id_cart = "'.(int)$id_cart.'" AND id_product="'.(int)$id_product.'" AND id_product_attribute="'.(int)$id_product_attribute.'"';;
                            Db::getInstance()->Execute($sql);
                        }
                        else
                        {
                            $sql = '
                            INSERT INTO '._DB_PREFIX_.'cart_product (id_cart,id_product,id_product_attribute,quantity,date_add'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=')?",id_shop":"").')
                            VALUES ("'.(int)$id_cart.'","'.(int)$id_product.'","'.(int)$id_product_attribute.'","'.(int)$quantity.'","'.date("Ym-d H:i:s").'"'.(version_compare(_PS_VERSION_, '1.5.0.0', '>=')?',"'.(int)$id_shop.'"':"").')';
                            Db::getInstance()->Execute($sql);
                        }
                    }
                }
            }

            break;
        case"remove_product":
            $id_product=Tools::getValue('id_product');

            if(!empty($id_product))
            {
                $ids = explode(",", $id_product);
                foreach ($ids as $id)
                {
                    list($id_product,$id_product_attribute) = explode("_",$id);
                    if(!empty($id_product))
                    {
                        $sql = '
                        DELETE FROM '._DB_PREFIX_.'cart_product
                        WHERE id_cart = "'.(int)$id_cart.'" AND id_product="'.(int)$id_product.'" AND id_product_attribute="'.(int)$id_product_attribute.'"';
                        Db::getInstance()->Execute($sql);
                    }
                }
            }
            break;
        case"update_address":
            $type=Tools::getValue('type');
            $id_address=(int)Tools::getValue('id_address');

            if(!empty($id_address) && !empty($type))
            {
                $sql = '
                   UPDATE '._DB_PREFIX_.'cart SET id_address_'.pSQL($type).' = "'.(int)$id_address.'"
                   WHERE id_cart = "'.(int)$id_cart.'" ';
                Db::getInstance()->Execute($sql);
            }
            break;
    }
}