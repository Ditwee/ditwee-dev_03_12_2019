<script type="text/javascript">
    dhxlMakeOrderLayout=wMakeOrder.attachLayout("3E");

    var makeOrder_selected_customer = null;
    var makeOrder_selected_cart = null;

    var makeOrderRow1 = dhxlMakeOrderLayout.cells('a');
    var makeOrderRow1Layout=makeOrderRow1.attachLayout("3W");

    /*var makeOrderRow1Col1 = makeOrderRow1Layout.cells('a');
    var makeOrderRow1Col1Layout=makeOrderRow1Col1.attachLayout("2E");
    var makeOrder_cell_customer = makeOrderRow1Col1Layout.cells('a');
    var makeOrder_cell_customerInfo = makeOrderRow1Col1Layout.cells('b');
    var makeOrder_cell_lastPdtOrdered = makeOrderRow1Layout.cells('b');*/

    var makeOrder_cell_customer = makeOrderRow1Layout.cells('a');
    var makeOrder_cell_customerInfo = makeOrderRow1Layout.cells('b');
    var makeOrder_cell_coupon = makeOrderRow1Layout.cells('c');

    var makeOrderRow2 = dhxlMakeOrderLayout.cells('b');

    var makeOrderRow2Col1Layout=makeOrderRow2.attachLayout("2U");
    var makeOrder_cell_searchProduct = makeOrderRow2Col1Layout.cells('a');
    var makeOrder_cell_cart = makeOrderRow2Col1Layout.cells('b');

    var makeOrderRow3Layout=dhxlMakeOrderLayout.cells('c').attachLayout("3W");
    var makeOrder_cell_crossSelling = makeOrderRow3Layout.cells('a');
    var makeOrder_cell_addresses = makeOrderRow3Layout.cells('b');
    // var makeOrder_cell_addressform = makeOrderRow3Layout.cells('c');
    var makeOrder_cell_validationForm= makeOrderRow3Layout.cells('c');

    /*
    CUSTOMER
     */
    //makeOrder_cell_customer.setText('<?php //echo _l("Customer",1); ?>//');
    makeOrder_cell_customer.hideHeader();

    var makeOrder_customer_tb = makeOrder_cell_customer.attachToolbar();
      <?php echo(_s('APP_USE_NEW_ICONS') ? "makeOrder_customer_tb.setIconset('awesome');"."\r\n" : ''); ?>
    makeOrder_customer_tb.addText("title", 1, '<b><?php echo _l("Customer",1); ?><b/>');

    makeOrder_customer_tb.addButton('makeOrder_customer_refresh',100,'','<?php echo getIcon("arrow_refresh.png"); ?>','<?php echo getIcon("arrow_refresh.png"); ?>');
    makeOrder_customer_tb.setItemToolTip('makeOrder_customer_refresh','<?php echo _l('Refresh',1)?>');

    var makeOrder_shop = '1';
    <?php if(SCMS) {
        $name = "";
        $default = Configuration::get('PS_SHOP_DEFAULT');
        ?>
        var opts = [
            <?php
            $shops = Shop::getShops(false);
            foreach($shops as $shop) { ?>
            ['shop-<?php echo $shop["id_shop"]; ?>', 'obj', '<?php echo str_replace("'", "\'", $shop['name']);?>', ''],
            <?php
            if($default==$shop["id_shop"])
                $name = str_replace("'", "\'", $shop['name']);
            } ?>
        ];
        makeOrder_shop = '<?php echo $default; ?>';
        makeOrder_customer_tb.addButtonSelect("makeOrder_shop", 100, '<?php echo $name; ?>', opts, "","",false,true);
        makeOrder_customer_tb.setItemToolTip('makeOrder_shop','<?php echo _l('Shop')?>');
    <?php } ?>
    makeOrder_customer_tb.addButton("makeOrder_add_ps", 100, "", "<?php echo getIcon("add_ps.png"); ?>", "<?php echo getIcon("add_ps.png"); ?>");
    makeOrder_customer_tb.setItemToolTip('makeOrder_add_ps','<?php echo _l('Create new customer with the PrestaShop form', 1)?>');
    makeOrder_customer_tb.addButton("makeOrder_view_customer_ps", 100, "", "<?php echo getIcon("user_ps.png"); ?>", "<?php echo getIcon("user_ps.png"); ?>");
    makeOrder_customer_tb.setItemToolTip('makeOrder_view_customer_ps','<?php echo _l('View selected customer in Prestashop', 1)?>');
    <?php if(_r("ACT_CUS_LOGIN_AS_CUSTOMER")) { ?>
    makeOrder_customer_tb.addButton("makeOrder_user_go", 100, "", "<?php echo getIcon("user_orange_go.png"); ?>", "<?php echo getIcon("user_orange_go.png"); ?>");
    makeOrder_customer_tb.setItemToolTip('makeOrder_user_go','<?php echo _l('login as selected customer on the front office', 1)?>');
    <?php } ?>
    makeOrder_customer_tb.addButton('makeOrder_customer_select',100,'','<?php echo getIcon("tick.png"); ?>','<?php echo getIcon("tick.png"); ?>');
    makeOrder_customer_tb.setItemToolTip('makeOrder_customer_select','<?php echo _l('Load customer',1)?>');

    makeOrder_customer_tb.attachEvent("onClick", function (id) {
        <?php if(SCMS) { ?>
        var shop = id.split("-");
        if(shop[0]!=undefined && shop[0]=="shop")
        {
            if(shop[1]!=undefined && shop[1]=="all")
            {
                makeOrder_customer_tb.setItemText('makeOrder_shop','<?php echo _l('All shops',1)?>');
                makeOrder_shop = 'all';
            }
            <?php foreach($shops as $shop) { ?>
            if(shop[1]!=undefined && shop[1]=="<?php echo $shop["id_shop"]; ?>")
            {
                makeOrder_customer_tb.setItemText('makeOrder_shop','<?php echo str_replace("'", "\'", $shop['name']);?>');
                makeOrder_shop = "<?php echo $shop["id_shop"]; ?>";
            }
            <?php } ?>
            makeOrder_customer_tb.setListOptionSelected('makeOrder_shop',id);
            displayMOCustomers();
        }
        <?php } ?>
        if(id=="makeOrder_add_ps")
        {
            if (!dhxWins.isWindow("wNewCustomer"))
            {
                <?php  if(version_compare(_PS_VERSION_, '1.6.0.0', '>=')) { ?>
                wNewCustomer = dhxWins.createWindow("wNewCustomer", 50, 50, 1250, $(window).height()-75);
                <?php  } else { ?>
                wNewCustomer = dhxWins.createWindow("wNewCustomer", 50, 50, 1000, $(window).height()-75);
                <?php  } ?>
                wNewCustomer.setText('<?php echo _l('Create the new customer and close this window to refresh the grid',1)?>');
                wNewCustomer.attachURL("<?php echo SC_PS_PATH_ADMIN_REL;?>index.php?tab=AdminCustomers&addcustomer&token=<?php echo $sc_agent->getPSToken('AdminCustomers');?>");
                wNewCustomer.attachEvent("onClose", function(win){
                    displayMOCustomers();
                    return true;
                });
            }
        }
        if(id=="makeOrder_view_customer_ps")
        {
            let selectedId = makeOrder_customer_grid.getSelectedRowId();
            if(selectedId!=undefined && selectedId!=null && selectedId!="" && selectedId!=0)
            {
                <?php  if(version_compare(_PS_VERSION_, '1.6.0.0', '>=')) { ?>
                wViewCustomer = dhxWins.createWindow(i+"wViewCustomer"+new Date().getTime(), 50+i*40, 50+i*40, 1250, $(window).height()-75);
                <?php  } else { ?>
                wViewCustomer = dhxWins.createWindow(i+"wViewCustomer"+new Date().getTime(), 50+i*40, 50+i*40, 1000, $(window).height()-75);
                <?php  } ?>
                wViewCustomer.setText('<?php echo _l('Customer',1)?> '+selectedId);
                wViewCustomer.attachURL("<?php echo SC_PS_PATH_ADMIN_REL;?>index.php?tab=AdminCustomers&viewcustomer&id_customer="+selectedId+"&token=<?php echo $sc_agent->getPSToken('AdminCustomers');?>");
            }
        }
        if(id=="makeOrder_user_go")
        {
            let selectedId = makeOrder_customer_grid.getSelectedRowId();
            if(selectedId!=undefined && selectedId!=null && selectedId!="" && selectedId!=0)
            {
                connectAsUser("<?php echo SCI::getConfigurationValue('SC_FOLDER_HASH');?>","<?php echo $sc_agent->id_employee; ?>",selectedId,makeOrder_shop);
            }
        }
        if(id=="makeOrder_customer_refresh")
        {
            displayMOCustomers();
        }
        if(id=="makeOrder_customer_select")
        {
            var selectedId = makeOrder_customer_grid.getSelectedRowId();
            if(selectedId!=undefined && selectedId!=null && selectedId!="" && selectedId!=0)
            {
                makeOrder_selected_customer = selectedId;
                makeOrder_selected_cart = null;
                //displayMOlastPdtOrdered();
                displayMOCart();
                displayMOsearchProduct();
                displayMOaddresses();
                displayMOCustomerInfo();
                displayMOcoupons();
                displayMOvalidationForm();
            }
        }
    });

    makeOrder_customer_grid = makeOrder_cell_customer.attachGrid();
    makeOrder_customer_grid._name='makeOrder_customer_grid';
    makeOrder_customer_grid.setImagePath("lib/js/imgs/");
    makeOrder_customer_grid.enableDragAndDrop(false);
    makeOrder_customer_grid.enableMultiselect(false);

    var makeOrder_customer_old_filter_params = "";
    var makeOrder_customer_filter_params = "";
    var makeOrder_customer_oldFilters = new Object();
    makeOrder_customer_grid.attachEvent("onRowDblClicked", function(rId){
        if(rId!=undefined && rId!=null && rId!="" && rId!=0)
        {
            makeOrder_selected_customer = rId;
            makeOrder_selected_cart = null;
            displayMOCart();
            displayMOsearchProduct();
            displayMOaddresses();
            displayMOCustomerInfo();
            displayMOcoupons();
            displayMOvalidationForm();
        }
        return true;
    });
    makeOrder_customer_grid.attachEvent("onFilterEnd", function(elements){
        makeOrder_customer_old_filter_params = makeOrder_customer_filter_params;
        makeOrder_customer_filter_params = "";
        var nb_cols = makeOrder_customer_grid.getColumnsNum();
        var need_refresh = false;
        if(nb_cols>0)
        {
            for(var i=0; i<nb_cols; i++)
            {
                var colId=makeOrder_customer_grid.getColumnId(i);
                if(makeOrder_customer_grid.getFilterElement(i)!=null
                    && ( colId =="id_customer"
                        || colId =="firstname"
                        || colId =="lastname"
                        || colId =="email"
                        || colId =="company"
                    )
                )
                {
                    var colValue = makeOrder_customer_grid.getFilterElement(i).value;
                    if((colValue!=null && colValue!="") || (makeOrder_customer_oldFilters[colId]!=null && makeOrder_customer_oldFilters[colId]!=""))
                    {
                        if(makeOrder_customer_filter_params!="")
                            makeOrder_customer_filter_params = makeOrder_customer_filter_params + ",";
                        makeOrder_customer_filter_params = makeOrder_customer_filter_params + colId+"|||"+colValue;
                        makeOrder_customer_oldFilters[colId] = makeOrder_customer_grid.getFilterElement(i).value;
                        var need_refresh = true;
                    }
                }
            }
        }
        if(need_refresh && makeOrder_customer_filter_params!="" && makeOrder_customer_filter_params!=makeOrder_customer_old_filter_params)
        {
            displayMOCustomers();
        }
    });

    function displayMOCustomers()
    {
        makeOrder_customer_oldFilters=new Array();
        for(var i=0,l=makeOrder_customer_grid.getColumnsNum();i<l;i++)
        {
            if (makeOrder_customer_grid.getFilterElement(i)!=null && makeOrder_customer_grid.getFilterElement(i).value!='')
                makeOrder_customer_oldFilters[makeOrder_customer_grid.getColumnId(i)]=makeOrder_customer_grid.getFilterElement(i).value;

        }

        makeOrder_cell_customer.progressOn();
        makeOrder_customer_grid.clearAll(true);
        $.post("index.php?ajax=1&act=ord_win-makeorder_customer_get&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),{'id_shop': makeOrder_shop, "filter_params": makeOrder_customer_filter_params},function(data)
        {
            makeOrder_cell_customer.progressOff();
            makeOrder_customer_grid.parse(data);

            for(var i=0;i<makeOrder_customer_grid.getColumnsNum();i++)
            {
                if (makeOrder_customer_grid.getFilterElement(i)!=null && makeOrder_customer_oldFilters[makeOrder_customer_grid.getColumnId(i)]!=undefined)
                {
                    makeOrder_customer_grid.getFilterElement(i).value=makeOrder_customer_oldFilters[makeOrder_customer_grid.getColumnId(i)];
                }
            }
            makeOrder_customer_grid.filterByAll();
        });
    }
    displayMOCustomers();

    /*
    INFO CUSTOMER
     */
    makeOrder_cell_customerInfo.setText('<?php echo _l("Customer information",1); ?>');
    // makeOrder_cell_customerInfo.collapse();
    makeOrder_cell_customerInfo.setWidth(340);

    function displayMOCustomerInfo()
    {
        if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
        {
            makeOrder_cell_customerInfo.attachURL("index.php?ajax=1&act=ord_win-makeorder_customerinfo_get&id_lang=" + SC_ID_LANG + "&id_customer="+makeOrder_selected_customer+"&" + new Date().getTime());
        }
    }

    /*
    LAST ORDERED PRODUCTS
     */
    /*makeOrder_cell_lastPdtOrdered.setText('<?php echo _l("Last ordered products",1); ?>');

    var makeOrder_lastPdtOrdered_tb = makeOrder_cell_lastPdtOrdered.attachToolbar();
      <?php echo(_s('APP_USE_NEW_ICONS') ? "makeOrder_lastPdtOrdered_tb.setIconset('awesome');"."\r\n" : ''); ?>

    makeOrder_lastPdtOrdered_tb.addButton('makeOrder_lastPdtOrdered_refresh',100,'','<?php echo getIcon("arrow_refresh.png"); ?>','<?php echo getIcon("arrow_refresh.png"); ?>');
    makeOrder_lastPdtOrdered_tb.setItemToolTip('makeOrder_lastPdtOrdered_refresh','<?php echo _l('Refresh',1)?>');
    makeOrder_lastPdtOrdered_tb.addButton('makeOrder_lastPdtOrdered_selectall',100,'','<?php echo getIcon("lightning.png"); ?>','<?php echo getIcon("lightning.png"); ?>');
    makeOrder_lastPdtOrdered_tb.setItemToolTip('makeOrder_lastPdtOrdered_selectall','<?php echo _l('Select all',1)?>');
    makeOrder_lastPdtOrdered_tb.addButton('makeOrder_lastPdtOrdered_addincart',100,'','<?php echo getIcon("cart_add.png"); ?>','<?php echo getIcon("cart_add.png"); ?>');
    makeOrder_lastPdtOrdered_tb.setItemToolTip('makeOrder_lastPdtOrdered_addincart','<?php echo _l('Add to cart',1)?>');
    makeOrder_lastPdtOrdered_tb.addButton("gotocatalog", 100, "", "<?php echo getIcon("table_go.png"); ?>", "<?php echo getIcon("table_go.png"); ?>");
    makeOrder_lastPdtOrdered_tb.setItemToolTip('gotocatalog','<?php echo _l('Open the product in Sc catalog')?>');

    makeOrder_lastPdtOrdered_tb.attachEvent("onClick", function (id) {
        if(id=="makeOrder_lastPdtOrdered_refresh")
        {
            displayMOlastPdtOrdered();
        }
        if(id=="makeOrder_lastPdtOrdered_selectall")
        {
            makeOrder_lastPdtOrdered_grid.selectAll();
        }
        if(id=="makeOrder_lastPdtOrdered_addincart")
        {
            var selectedIds = makeOrder_lastPdtOrdered_grid.getSelectedRowId();
            if(selectedIds!=undefined && selectedIds!=null && selectedIds!='' && selectedIds!=0)
            {
                addInCart(selectedIds,"1");
            }
        }
        if (id=='gotocatalog')
        {
            var selectedIds = makeOrder_lastPdtOrdered_grid.getSelectedRowId();
            if(selectedIds!=undefined && selectedIds!=null && selectedIds!='' && selectedIds!=0)
            {
                var ids = selectedIds.split(",");
                $.each(ids, function(num,rId){
                    var path = makeOrder_lastPdtOrdered_grid.getUserData(rId, "path_pdt");
                    let url = "?page=cat_tree&open_cat_grid="+path;
                    window.open(url,'_blank');
                });
            }
        }
    });


    makeOrder_lastPdtOrdered_grid = makeOrder_cell_lastPdtOrdered.attachGrid();
    makeOrder_lastPdtOrdered_grid._name='makeOrder_lastPdtOrdered_grid';
    makeOrder_lastPdtOrdered_grid.setImagePath("lib/js/imgs/");
    makeOrder_lastPdtOrdered_grid.enableDragAndDrop(false);
    makeOrder_lastPdtOrdered_grid.enableMultiselect(true);

    function displayMOlastPdtOrdered()
    {
        if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
        {
            makeOrder_cell_lastPdtOrdered.progressOn();
            makeOrder_lastPdtOrdered_grid.clearAll(true);
            $.post("index.php?ajax=1&act=ord_win-makeorder_lastpdtordered_get&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),{"id_customer": makeOrder_selected_customer},function(data)
            {
                makeOrder_cell_lastPdtOrdered.progressOff();
                makeOrder_lastPdtOrdered_grid.parse(data);
            });
        }
    }*/

    /*
    COUPONS
     */
    //makeOrder_cell_coupon.setText('<?php //echo _l("Available coupons",1); ?>//');
    makeOrder_cell_coupon.hideHeader();
    var makeOrder_cell_coupon_tb = makeOrder_cell_coupon.attachToolbar();
      <?php echo(_s('APP_USE_NEW_ICONS') ? "makeOrder_cell_coupon_tb.setIconset('awesome');"."\r\n" : ''); ?>
    makeOrder_cell_coupon_tb.addText("title",1,'<b><?php echo _l("Available discount vouchers",1); ?></b>');
    makeOrder_cell_coupon_tb.addButton('makeOrder_coupon_refresh',100,'','<?php echo getIcon("arrow_refresh.png"); ?>','<?php echo getIcon("arrow_refresh.png"); ?>');
    makeOrder_cell_coupon_tb.setItemToolTip('makeOrder_coupon_refresh','<?php echo _l('Refresh',1)?>');
    makeOrder_cell_coupon_tb.addButton("manage_coupon_add_bo", 100, "", "<?php echo getIcon("add_ps.png"); ?>", "<?php echo getIcon("add_ps.png"); ?>");
    makeOrder_cell_coupon_tb.setItemToolTip('manage_coupon_add_bo','<?php echo _l('Create new voucher with the PrestaShop form')?>');
    makeOrder_cell_coupon_tb.addButton("manage_coupon_bo", 100, "", "<?php echo getIcon("zoom.png"); ?>", "<?php echo getIcon("zoom.png"); ?>");
    makeOrder_cell_coupon_tb.setItemToolTip('manage_coupon_bo','<?php echo _l('Edit voucher in PrestaShop')?>');
    makeOrder_cell_coupon_tb.attachEvent("onClick", function (id) {
        if(id=="makeOrder_coupon_refresh")
        {
            displayMOcoupons();
        }
        if(id=="manage_coupon_add_bo")
        {
            if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
            {
                if (!dhxWins.isWindow("wNewCoupon"))
                {
                    <?php  if(version_compare(_PS_VERSION_, '1.7.0.0', '>=')) { ?>
                    wNewCoupon = dhxWins.createWindow("wNewCoupon", 50, 50, 1460, $(window).height()-75);
                    <?php  } else { ?>
                    wNewCoupon = dhxWins.createWindow("wNewCoupon", 50, 50, 1000, $(window).height()-75);
                    <?php  } ?>
                    wNewCoupon.setText('<?php echo _l('Create the new coupon and close this window to refresh the grid',1)?>');
                    wNewCoupon.attachURL("<?php echo SC_PS_PATH_ADMIN_REL;?>index.php?<?php echo(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? 'controller=AdminCartRules' : 'tab=AdminCartRules'); ?>&addcart_rule&token=<?php echo $sc_agent->getPSToken('AdminCartRules');?>");
                    wNewCoupon.attachEvent("onClose", function(win){
                        displayMOcoupons();
                        return true;
                    });
                }
            }
        }
        if(id=="manage_coupon_bo")
        {
            if (!dhxWins.isWindow("wViewCoupon"))
            {
                <?php  if(version_compare(_PS_VERSION_, '1.7.0.0', '>=')) { ?>
                wViewCoupon = dhxWins.createWindow("wViewCoupon", 50, 50, 1460, $(window).height()-75);
                <?php  } else { ?>
                wViewCoupon = dhxWins.createWindow("wViewCoupon", 50, 50, 1000, $(window).height()-75);
                <?php  } ?>
                wViewCoupon.setText('<?php echo _l('View coupon in back office',1)?>');
                let id_cart_rule = makeOrder_coupon_grid.getSelectedRowId();
                wViewCoupon.attachURL("<?php echo SC_PS_PATH_ADMIN_REL;?>index.php?<?php echo(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? 'controller=AdminCartRules' : 'tab=AdminCartRules'); ?>&id_cart_rule="+id_cart_rule+"&updatecart_rule&token=<?php echo $sc_agent->getPSToken('AdminCartRules');?>");
                wViewCoupon.attachEvent("onClose", function(win){
                    displayMOcoupons();
                    return true;
                });
            }
        }
    });

    makeOrder_coupon_grid = makeOrder_cell_coupon.attachGrid();
    makeOrder_coupon_grid._name='makeOrder_coupon_grid';
    makeOrder_coupon_grid.setImagePath("lib/js/imgs/");
    makeOrder_coupon_grid.enableDragAndDrop(false);
    makeOrder_coupon_grid.enableMultiselect(false);

    function displayMOcoupons(){
        makeOrder_coupon_grid.clearAll(true);
        makeOrder_cell_coupon.progressOn();
        $.post("index.php?ajax=1&act=ord_win-makeorder_coupon_get&id_lang=" + SC_ID_LANG + "&" + new Date().getTime(),
            {
                "id_customer": makeOrder_selected_customer,
                "id_lang":SC_ID_LANG,
                "id_shop": makeOrder_shop,
                "id_cart": makeOrder_selected_cart,
            }
            , function (data) {
                makeOrder_cell_coupon.progressOff();
                makeOrder_coupon_grid.parse(data);
            });
    }

    makeOrder_coupon_grid.attachEvent("onEditCell",function(stage,rId,cInd){
        let coltype=ord_grid.getColType(cInd);
        if (stage==1 && this.editor && this.editor.obj && coltype!='txt' && coltype!='txttxt') this.editor.obj.select();

        if(stage == 2){
            return false;
        }
        return true;
    });

    /*
    PRODUCT SEARCH
     */
    //makeOrder_cell_searchProduct.setText('<?php //echo _l("Products search",1); ?>//');
    makeOrder_cell_searchProduct.hideHeader();

    var makeOrder_searchProduct_tb = makeOrder_cell_searchProduct.attachToolbar();
      <?php echo(_s('APP_USE_NEW_ICONS') ? "makeOrder_searchProduct_tb.setIconset('awesome');"."\r\n" : ''); ?>
    makeOrder_searchProduct_tb.addText("title",1,'<b><?php echo _l("Search for products",1); ?><b/>');
    makeOrder_searchProduct_tb.addButton('makeOrder_searchProduct_refresh',100,'','<?php echo getIcon("arrow_refresh.png"); ?>','<?php echo getIcon("arrow_refresh.png"); ?>');
    makeOrder_searchProduct_tb.setItemToolTip('makeOrder_searchProduct_refresh','<?php echo _l('Refresh',1)?>');
    makeOrder_searchProduct_tb.addButton('makeOrder_searchProduct_addincart',100,'','<?php echo getIcon("cart_add.png"); ?>','<?php echo getIcon("cart_add.png"); ?>');
    makeOrder_searchProduct_tb.setItemToolTip('makeOrder_searchProduct_addincart','<?php echo _l('Add to cart',1)?>');
    makeOrder_searchProduct_tb.addButton("gotocatalog", 100, "", "<?php echo getIcon("table_go.png"); ?>", "<?php echo getIcon("table_go.png"); ?>");
    makeOrder_searchProduct_tb.setItemToolTip('gotocatalog','<?php echo _l('Open the product in Sc catalog')?>');

    makeOrder_searchProduct_tb.attachEvent("onClick", function (id) {
        if(id=="makeOrder_searchProduct_refresh")
        {
            displayMOsearchProduct();
        }
        if(id=="makeOrder_searchProduct_addincart")
        {
            var selectedIds = makeOrder_searchProduct_grid.getSelectedRowId();
            if(selectedIds!=undefined && selectedIds!=null && selectedIds!='' && selectedIds!=0)
            {
                addInCart(selectedIds,"1");
            }
        }
        if (id=='gotocatalog')
        {
            var selectedIds = makeOrder_searchProduct_grid.getSelectedRowId();
            if(selectedIds!=undefined && selectedIds!=null && selectedIds!='' && selectedIds!=0)
            {
                var path = makeOrder_searchProduct_grid.getUserData(selectedIds, "path_pdt");
                let url = "?page=cat_tree&open_cat_grid="+path;
                window.open(url,'_blank');
            }
        }
    });

    makeOrder_searchProduct_grid = makeOrder_cell_searchProduct.attachGrid();
    makeOrder_searchProduct_grid._name='makeOrder_searchProduct_grid';
    makeOrder_searchProduct_grid.setImagePath("lib/js/imgs/");
    makeOrder_searchProduct_grid.enableDragAndDrop(false);
    makeOrder_searchProduct_grid.enableMultiselect(false);

    makeOrder_searchProduct_grid.attachEvent("onSelectStateChanged", function(rId){
        displayMOcrossSelling();
    });

    var makeOrder_searchProduct_old_filter_params = "";
    var makeOrder_searchProduct_filter_params = "";
    var makeOrder_searchProduct_oldFilters = new Object();
    makeOrder_searchProduct_grid.attachEvent("onFilterEnd", function(elements){
        makeOrder_searchProduct_old_filter_params = makeOrder_searchProduct_filter_params;
        makeOrder_searchProduct_filter_params = "";
        var nb_cols = makeOrder_searchProduct_grid.getColumnsNum();
        var need_refresh = false;
        if(nb_cols>0)
        {
            for(var i=0; i<nb_cols; i++)
            {
                var colId=makeOrder_searchProduct_grid.getColumnId(i);
                if(makeOrder_searchProduct_grid.getFilterElement(i)!=null
                    && ( colId =="id_product"
                        || colId =="id_product_attribute"
                        || colId =="reference"
                        || colId =="ean13"
                        || colId =="product"
                    )
                )
                {
                    var colValue = makeOrder_searchProduct_grid.getFilterElement(i).value;
                    if((colValue!=null && colValue!="") || (makeOrder_searchProduct_oldFilters[colId]!=null && makeOrder_searchProduct_oldFilters[colId]!=""))
                    {
                        if(makeOrder_searchProduct_filter_params!="")
                            makeOrder_searchProduct_filter_params = makeOrder_searchProduct_filter_params + ",";
                        makeOrder_searchProduct_filter_params = makeOrder_searchProduct_filter_params + colId+"|||"+colValue;
                        makeOrder_searchProduct_oldFilters[colId] = makeOrder_searchProduct_grid.getFilterElement(i).value;
                        var need_refresh = true;
                    }
                }
            }
        }
        if(need_refresh && makeOrder_searchProduct_filter_params!="" && makeOrder_searchProduct_filter_params!=makeOrder_searchProduct_old_filter_params)
        {
            displayMOsearchProduct();
        }
    });

    function displayMOsearchProduct()
    {
        if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
        {
            makeOrder_searchProduct_oldFilters=new Array();
            for(var i=0,l=makeOrder_searchProduct_grid.getColumnsNum();i<l;i++)
            {
                if (makeOrder_searchProduct_grid.getFilterElement(i)!=null && makeOrder_searchProduct_grid.getFilterElement(i).value!='')
                    makeOrder_searchProduct_oldFilters[makeOrder_searchProduct_grid.getColumnId(i)]=makeOrder_searchProduct_grid.getFilterElement(i).value;

            }

            makeOrder_cell_searchProduct.progressOn();
            makeOrder_searchProduct_grid.clearAll(true);
            makeOrder_crossSelling_grid.clearAll(true);
            $.post("index.php?ajax=1&act=ord_win-makeorder_product_get&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),{"id_customer": makeOrder_selected_customer,"id_shop": makeOrder_shop, 'filter_params': makeOrder_searchProduct_filter_params},function(data)
            {
                makeOrder_cell_searchProduct.progressOff();
                makeOrder_searchProduct_grid.parse(data);

                for(var i=0;i<makeOrder_searchProduct_grid.getColumnsNum();i++)
                {
                    if (makeOrder_searchProduct_grid.getFilterElement(i)!=null && makeOrder_searchProduct_oldFilters[makeOrder_searchProduct_grid.getColumnId(i)]!=undefined)
                    {
                        makeOrder_searchProduct_grid.getFilterElement(i).value=makeOrder_searchProduct_oldFilters[makeOrder_searchProduct_grid.getColumnId(i)];
                    }
                }
                makeOrder_searchProduct_grid.filterByAll();
            });
        }
    }

    /*
    CROSS SELLING
     */
    //makeOrder_cell_crossSelling.setText('<?php //echo _l("Cross-selling: accessories to suggest",1); ?>//');
    makeOrder_cell_crossSelling.hideHeader();

    var makeOrder_crossSelling_tb = makeOrder_cell_crossSelling.attachToolbar();
      <?php echo(_s('APP_USE_NEW_ICONS') ? "makeOrder_crossSelling_tb.setIconset('awesome');"."\r\n" : ''); ?>

    makeOrder_crossSelling_tb.addText("title",1,'<b><?php echo _l("Cross-selling: suggest accessories",1); ?><b/>');
    makeOrder_crossSelling_tb.addButton('makeOrder_crossSelling_refresh',100,'','<?php echo getIcon("arrow_refresh.png"); ?>','<?php echo getIcon("arrow_refresh.png"); ?>');
    makeOrder_crossSelling_tb.setItemToolTip('makeOrder_crossSelling_refresh','<?php echo _l('Refresh',1)?>');
    makeOrder_crossSelling_tb.addButton('makeOrder_crossSelling_addincart',100,'','<?php echo getIcon("cart_add.png"); ?>','<?php echo getIcon("cart_add.png"); ?>');
    makeOrder_crossSelling_tb.setItemToolTip('makeOrder_crossSelling_addincart','<?php echo _l('Add to cart',1)?>');
    makeOrder_crossSelling_tb.addButton("gotocatalog", 100, "", "<?php echo getIcon("table_go.png"); ?>", "<?php echo getIcon("table_go.png"); ?>");
    makeOrder_crossSelling_tb.setItemToolTip('gotocatalog','<?php echo _l('Open the product in Sc catalog')?>');

    makeOrder_crossSelling_tb.attachEvent("onClick", function (id) {
        if(id=="makeOrder_crossSelling_refresh")
        {
            displayMOcrossSelling();
        }
        if(id=="makeOrder_crossSelling_addincart")
        {
            var selectedIds = makeOrder_crossSelling_grid.getSelectedRowId();
            if(selectedIds!=undefined && selectedIds!=null && selectedIds!='' && selectedIds!=0)
            {
                addInCart(selectedIds,"1");
            }
        }
        if (id=='gotocatalog')
        {
            var selectedIds = makeOrder_crossSelling_grid.getSelectedRowId();
            if(selectedIds!=undefined && selectedIds!=null && selectedIds!='' && selectedIds!=0)
            {
                var path = makeOrder_crossSelling_grid.getUserData(selectedIds, "path_pdt");
                let url = "?page=cat_tree&open_cat_grid="+path;
                window.open(url,'_blank');
            }
        }
    });

    makeOrder_crossSelling_grid = makeOrder_cell_crossSelling.attachGrid();
    makeOrder_crossSelling_grid._name='makeOrder_crossSelling_grid';
    makeOrder_crossSelling_grid.setImagePath("lib/js/imgs/");
    makeOrder_crossSelling_grid.enableDragAndDrop(false);
    makeOrder_crossSelling_grid.enableMultiselect(false);

    function displayMOcrossSelling()
    {
        var selectedIds = makeOrder_searchProduct_grid.getSelectedRowId();
        if(selectedIds!=undefined && selectedIds!=null && selectedIds!='' && selectedIds!=0)
        {
            makeOrder_cell_crossSelling.progressOn();
            makeOrder_crossSelling_grid.clearAll(true);
            $.post("index.php?ajax=1&act=ord_win-makeorder_crossselling_get&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),{"id_shop": makeOrder_shop, "id_product":selectedIds},function(data)
            {
                makeOrder_cell_crossSelling.progressOff();
                makeOrder_crossSelling_grid.parse(data);
            });
        }
    }

    /*
    CART
     */
    //makeOrder_cell_cart.setText('<?php //echo _l("Cart",1); ?>//');
    makeOrder_cell_cart.hideHeader();

    var makeOrder_cart_tb = makeOrder_cell_cart.attachToolbar();
      <?php echo(_s('APP_USE_NEW_ICONS') ? "makeOrder_cart_tb.setIconset('awesome');"."\r\n" : ''); ?>

    makeOrder_cart_tb.addText("title",1,'<b><?php echo _l("Cart",1); ?><b/>');
    makeOrder_cart_tb.addButton('makeOrder_cart_refresh',100,'','<?php echo getIcon("arrow_refresh.png"); ?>','<?php echo getIcon("arrow_refresh.png"); ?>');
    makeOrder_cart_tb.setItemToolTip('makeOrder_cart_refresh','<?php echo _l('Refresh',1)?>');
    makeOrder_cart_tb.addButton('makeOrder_cart_selectall',100,'','<?php echo getIcon("application_lightning.png"); ?>','<?php echo getIcon("application_lightning.png"); ?>');
    makeOrder_cart_tb.setItemToolTip('makeOrder_cart_selectall','<?php echo _l('Select all',1)?>');
    makeOrder_cart_tb.addButton('makeOrder_cart_remove',100,'','<?php echo getIcon("cart_delete.png"); ?>','<?php echo getIcon("cart_delete.png"); ?>');
    makeOrder_cart_tb.setItemToolTip('makeOrder_cart_remove','<?php echo _l('Remove selected products from cart',1)?>');
    makeOrder_cart_tb.addButton("gotocatalog", 100, "", "<?php echo getIcon("table_go.png"); ?>", "<?php echo getIcon("table_go.png"); ?>");
    makeOrder_cart_tb.setItemToolTip('gotocatalog','<?php echo _l('Open the product in Sc catalog')?>');

    makeOrder_cart_tb.attachEvent("onClick", function (id) {
        if(id=="makeOrder_cart_refresh")
        {
            displayMOCart();
        }
        if(id=="makeOrder_cart_selectall")
        {
            makeOrder_cart_grid.selectAll();
        }
        if(id=="makeOrder_cart_remove")
        {
            var selectedIds = makeOrder_cart_grid.getSelectedRowId();
            if(selectedIds!=undefined && selectedIds!=null && selectedIds!='' && selectedIds!=0)
            {
                if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
                {
                    if(makeOrder_selected_cart!=undefined && makeOrder_selected_cart!=null && makeOrder_selected_cart!='' && makeOrder_selected_cart!=0)
                    {
                        makeOrder_cell_cart.progressOn();
                        $.post("index.php?ajax=1&act=ord_win-makeorder_cart_update&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),
                        {
                            "id_customer": makeOrder_selected_customer,
                            'id_cart': makeOrder_selected_cart,
                            'id_shop': makeOrder_shop,
                            'action': 'remove_product',
                            'id_product': selectedIds
                        },
                        function(data)
                        {
                            makeOrder_cell_cart.progressOff();
                            displayMOCart();
                        });
                    }
                }
            }
        }
        if (id=='gotocatalog')
        {
            var selectedIds = makeOrder_cart_grid.getSelectedRowId();
            if(selectedIds!=undefined && selectedIds!=null && selectedIds!='' && selectedIds!=0)
            {
                var ids = selectedIds.split(",");
                $.each(ids, function(num,rId){
                    var path = makeOrder_cart_grid.getUserData(rId, "path_pdt");
                    let url = "?page=cat_tree&open_cat_grid="+path;
                    window.open(url,'_blank');
                });
            }
        }
    });

    makeOrder_cart_sb=makeOrder_cell_cart.attachStatusBar();

    makeOrder_cart_grid = makeOrder_cell_cart.attachGrid();
    makeOrder_cart_grid._name='makeOrder_cart_grid';
    makeOrder_cart_grid.setImagePath("lib/js/imgs/");
    makeOrder_cart_grid.enableDragAndDrop(false);
    makeOrder_cart_grid.enableMultiselect(true);

    function onEditCell(stage,rId,cInd,nValue,oValue){
        var coltype=ord_grid.getColType(cInd);
        if (stage==1 && this.editor && this.editor.obj && coltype!='txt' && coltype!='txttxt') this.editor.obj.select();

        if(stage==2 && makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
        {
            if(makeOrder_selected_cart!=undefined && makeOrder_selected_cart!=null && makeOrder_selected_cart!='' && makeOrder_selected_cart!=0)
            {
                makeOrder_cell_cart.progressOn();
                $.post("index.php?ajax=1&act=ord_win-makeorder_cart_update&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),
                    {
                        "id_customer": makeOrder_selected_customer,
                        'id_cart': makeOrder_selected_cart,
                        'id_shop': makeOrder_shop,
                        'quantity': nValue,
                        'action': 'update_qty',
                        'id_product': rId
                    },
                    function(data)
                {
                    makeOrder_cell_cart.progressOff();
                    displayMOCart();
                });
            }
        }

        return true;
    }
    makeOrder_cart_grid.attachEvent("onEditCell",onEditCell);

    function displayMOCart()
    {
        if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
        {
            makeOrder_cell_cart.progressOn();
            makeOrder_cart_grid.clearAll(true);
            $.post("index.php?ajax=1&act=ord_win-makeorder_cart_get&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),{"id_customer": makeOrder_selected_customer, 'id_shop': makeOrder_shop},function(data)
            {
                makeOrder_cell_cart.progressOff();
                makeOrder_cart_grid.parse(data);

                var id_cart = makeOrder_cart_grid.getUserData("","id_cart");
                if(id_cart!=undefined && id_cart!=null && id_cart!='' && id_cart!=0)
                {
                    var old = makeOrder_selected_cart;
                    makeOrder_selected_cart = id_cart;
                    if(old!=makeOrder_selected_cart)
                        displayMOaddresses();
                }


                var total_et = makeOrder_cart_grid.getUserData("","total_et");
                var total_it = makeOrder_cart_grid.getUserData("","total_it");
                makeOrder_cart_sb.setText('<div style="text-align: right"><?php echo _l("Totals:",1); ?> <span style="margin-left: 20px;">'+total_et+' <?php echo _l("Tax excl.",1); ?></span> <span style="margin-left: 20px;">'+total_it+' <?php echo _l("Tax incl.",1); ?></span></div>');
            });
        }
    }

    /*
     ADDRESSES
     */
    //makeOrder_cell_addresses.setText('<?php //echo _l("Addresses",1); ?>//');
    makeOrder_cell_addresses.hideHeader();

    var makeOrder_addresses_tb = makeOrder_cell_addresses.attachToolbar();
      <?php echo(_s('APP_USE_NEW_ICONS') ? "makeOrder_addresses_tb.setIconset('awesome');"."\r\n" : ''); ?>

    makeOrder_addresses_tb.addText("title",1,'<b><?php echo _l("Addresses",1); ?><b/>');
    makeOrder_addresses_tb.addButton('makeOrder_addresses_refresh',100,'','<?php echo getIcon("arrow_refresh.png"); ?>','<?php echo getIcon("arrow_refresh.png"); ?>');
    makeOrder_addresses_tb.setItemToolTip('makeOrder_addresses_refresh','<?php echo _l('Refresh',1)?>');
    makeOrder_addresses_tb.addButton("makeOrder_addresses_add_ps", 100, "", "<?php echo getIcon("add_ps.png"); ?>", "<?php echo getIcon("add_ps.png"); ?>");
    makeOrder_addresses_tb.setItemToolTip('makeOrder_addresses_add_ps','<?php echo _l('Create new address with the PrestaShop form')?>');

    makeOrder_addresses_tb.attachEvent("onClick", function (id) {
        if(id=="makeOrder_addresses_refresh")
        {
            displayMOaddresses();
        }
        if(id=="makeOrder_addresses_add_ps")
        {
            if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
            {
                if (!dhxWins.isWindow("wNewAddress"))
                {
                    <?php  if(version_compare(_PS_VERSION_, '1.7.0.0', '>=')) { ?>
                    wNewAddress = dhxWins.createWindow("wNewAddress", 50, 50, 1460, $(window).height()-75);
                    <?php  } else { ?>
                    wNewAddress = dhxWins.createWindow("wNewAddress", 50, 50, 1000, $(window).height()-75);
                    <?php  } ?>
                    wNewAddress.setText('<?php echo _l('Create the new address and close this window to refresh the grid',1)?>');
                    wNewAddress.attachURL("<?php echo SC_PS_PATH_ADMIN_REL;?>index.php?<?php echo(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? 'controller=AdminAddresses' : 'tab=AdminAddresses'); ?>&addaddress=1&token=<?php echo $sc_agent->getPSToken('AdminAddresses');?>&id_customer="+makeOrder_selected_customer);

                    wNewAddress.attachEvent("onClose", function(win){
                        displayMOaddresses();
                        return true;
                    });
                }
            }
        }
    });

    makeOrder_addresses_grid = makeOrder_cell_addresses.attachGrid();
    makeOrder_addresses_grid._name='makeOrder_addresses_grid';
    makeOrder_addresses_grid.setImagePath("lib/js/imgs/");
    makeOrder_addresses_grid.enableDragAndDrop(false);
    makeOrder_addresses_grid.enableMultiselect(false);

    makeOrder_addresses_grid.attachEvent("onCheck", function(rId,cInd,state){
        var colIdinvoice=makeOrder_addresses_grid.getColIndexById("invoice");
        var colIddelivery=makeOrder_addresses_grid.getColIndexById("delivery");
        if(cInd==colIddelivery || cInd==colIdinvoice)
        {
            if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
            {
                if(makeOrder_selected_cart!=undefined && makeOrder_selected_cart!=null && makeOrder_selected_cart!='' && makeOrder_selected_cart!=0)
                {
                    makeOrder_cell_addresses.progressOn();
                    $.post("index.php?ajax=1&act=ord_win-makeorder_cart_update&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),
                        {
                            "id_customer": makeOrder_selected_customer,
                            'id_cart': makeOrder_selected_cart,
                            'id_shop': makeOrder_shop,
                            'action': 'update_address',
                            'type': (cInd==colIddelivery?'delivery':'invoice'),
                            'id_address': rId
                        },
                        function(data)
                        {
                            makeOrder_cell_addresses.progressOff();
                        });
                }
            }
        }
    });

    function displayMOaddresses()
    {
        if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
        {
            if(makeOrder_selected_cart!=undefined && makeOrder_selected_cart!=null && makeOrder_selected_cart!='' && makeOrder_selected_cart!=0)
            {
                makeOrder_cell_addresses.progressOn();
                makeOrder_addresses_grid.clearAll(true);
                $.post("index.php?ajax=1&act=ord_win-makeorder_addresses_get&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),{"id_customer": makeOrder_selected_customer, 'id_shop': makeOrder_shop, 'id_cart': makeOrder_selected_cart},function(data)
                {
                    makeOrder_cell_addresses.progressOff();
                    makeOrder_addresses_grid.parse(data);
                });
            }
        }
    }

    /*
     ADDRESS FORM
     */
    //makeOrder_cell_addressform.setText('<?php //echo _l("Add address",1); ?>//');

    /*
     VALIDATION FORM
     */
    makeOrder_cell_validationForm.setWidth(320);
    makeOrder_cell_validationForm.hideHeader();

    function displayMOvalidationForm() {
        if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0) {
            let idxFirstname = makeOrder_customer_grid.getColIndexById('firstname');
            let idxLastname = makeOrder_customer_grid.getColIndexById('lastname');
            let idxEmail = makeOrder_customer_grid.getColIndexById('email');
            let makeOrder_selected_customer_detail = makeOrder_customer_grid.cells(makeOrder_selected_customer, idxFirstname).getValue() + ' ' + makeOrder_customer_grid.cells(makeOrder_selected_customer, idxLastname).getValue();
            let makeOrder_selected_customer_email = makeOrder_customer_grid.cells(makeOrder_selected_customer, idxEmail).getValue();
            let makeOrder_cell_validationForm_structure = [
                {type: "settings", position: "label-left"},
                {
                    type: "button",
                    className: "btn_order_as",
                    name: "order_as",
                    value: "<?php echo (_s('APP_USE_NEW_ICONS') ? '<i class=\"'.getIcon('user_orange_go.png').'\"></i> ': '')._l('Order as', 1); ?> " + makeOrder_selected_customer_detail
                },
                {
                    type: "button",
                    className: "btn_order_sendmail",
                    name: "order_sendmail",
                    value: "<?php echo (_s('APP_USE_NEW_ICONS') ? '<i class=\"'.getIcon('email_go.png').'\"></i> ': '')._l('Send validation order mail to:', 1); ?><br/>" + makeOrder_selected_customer_email
                }
            ];
            //let wManageFields_properties_form = makeOrder_cell_validationForm.attachForm(makeOrder_cell_validationForm_structure);
            let htmlFormString = '<div id="order_form_container"></div>';
            makeOrder_cell_validationForm.attachHTMLString(htmlFormString);
            let wManageFields_properties_form = new dhtmlXForm("order_form_container",makeOrder_cell_validationForm_structure);
            wManageFields_properties_form.attachEvent("onButtonClick", function (id) {
                if(id=='order_as'){
                    <?php if(!SCMS){?>
                    makeOrder_shop = 0;
                    <?php } ?>
                    connectAsUser("<?php echo SCI::getConfigurationValue('SC_FOLDER_HASH');?>","<?php echo $sc_agent->id_employee; ?>",makeOrder_selected_customer,makeOrder_shop);
                }
                if(id=='order_sendmail'){
                    if (confirm("<?php echo _l('Would you really want send a validation order email to ', 1); ?> "+makeOrder_selected_customer_email+"?") )
                    {
                        makeOrder_cell_validationForm.progressOn();
                        $.post("<?php echo SC_PS_PATH_ADMIN_REL;?>index.php?<?php echo(version_compare(_PS_VERSION_, '1.5.0.0', '>=') ? 'controller=AdminOrders' : 'tab=AdminOrders'); ?>",
                        {
                            'ajax':1,
                            'token':"<?php echo $sc_agent->getPSToken('AdminOrders');?>",
                            'tab': "AdminOrders",
                            'action': "sendMailValidateOrder",
                            'id_customer': makeOrder_selected_customer,
                            'id_cart': makeOrder_selected_cart
                        },
                        function(res)
                        {
                            if (res.errors){
                                dhtmlx.message({text:res.result,type:'error',expire:10000});
                            } else {
                                dhtmlx.message({text:res.result,type:'success',expire:5000});
                            }
                            makeOrder_cell_validationForm.progressOff();
                        },"json");
                    }
                }
            });
        }
    }

    /*
    FUNCTIONS
     */
    function addInCart(id_product,quantity)
    {
        if(makeOrder_selected_customer!=undefined && makeOrder_selected_customer!=null && makeOrder_selected_customer!='' && makeOrder_selected_customer!=0)
        {
            if(makeOrder_selected_cart!=undefined && makeOrder_selected_cart!=null && makeOrder_selected_cart!='' && makeOrder_selected_cart!=0)
            {
                if(id_product!=undefined && id_product!=null && id_product!='' && id_product!=0)
                {
                    makeOrder_cell_cart.progressOn();
                    $.post("index.php?ajax=1&act=ord_win-makeorder_cart_update&id_lang="+SC_ID_LANG+"&"+new Date().getTime(),
                        {
                            "id_customer": makeOrder_selected_customer,
                            'id_cart': makeOrder_selected_cart,
                            'id_shop': makeOrder_shop,
                            'quantity': quantity,
                            'action': 'add_product',
                            'id_product': id_product
                        },
                        function(data)
                        {
                            makeOrder_cell_cart.progressOff();
                            displayMOCart();
                        });
                }
            }
        }
    }
</script>