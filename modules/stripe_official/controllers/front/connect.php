<?php
/**
 * 2007-2017 PrestaShop
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
 * International Registered Trademark & Property of PrestaShop SA
 */

class Stripe_officialConnectModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    private $stripe;

    public function __construct()
    {
        parent::__construct();
        $this->context = Context::getContext();
        $this->stripe = Module::getInstanceByName('stripe_official');
    }

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        if ($this->stripe && $this->stripe->active) {
            $this->context = Context::getContext();

				
			if (!$this->context->customer->isLogged() ) {
				Tools::redirect('index.php?controller=authentication&back='.Context::getContext()->link->getModuleLink('stripe_official', 'connect', array(), true)."?code=".Tools::getValue('code'));
			}
		
            if(Tools::getValue('code')) {
              
				$id_seller = Seller::getSellerByCustomer($this->context->cookie->id_customer);
				$seller = new Seller($id_seller);
				
				//Recuperation du compte Stripe Connect
				$code = Tools::getValue('code');
				$connect = $this->stripe->get_credential_from_stripe($code);
				$seller->key_stripe = $connect->stripe_user_id;
				
				if(!$seller->key_customer_stripe && Validate::isIBAN($seller->iban)){
					//Creation du compte Stripe Customer
					$customer = $this->stripe->set_customer_account($seller->email);
					
					//Creation du SEPA
					$sepa = $this->stripe->set_sepa_source($seller->iban, $seller->name);
					
					//Link du customer et du sepa
					$this->stripe->link_sepa_to_customer($customer->id, $sepa->id);
					$seller->key_customer_stripe = $customer->id;
				}
					
				$seller->save();
				
				Tools::redirect(Context::getContext()->link->getModuleLink('jmarketplace', 'selleraccount', array(), true));
				
			}
			else{
				
				$this->errors[] = Tools::displayError('Invalid code.');	
				$this->context->smarty->assign(array(
					'errors' => $this->errors));
			}
		}
    }
}