{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* @author    UPELA
* @copyright 2017-2018 MPG Upela
* @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}


<div class="row">
    <div class="panel section8">
        <h5 style="padding:5px;margin-top: 10px" ><img style="width: 20px;" src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/icon-carriers.png"/> {l s='Select carrier explanation' mod='upela'}</h5>
        <h5 style="padding:5px;" ><img style="width: 20px;" src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/poids-volume.png"/> {l s='Select carrier explanation volume' mod='upela'}</h5>

        <a id="pricer" style="padding:5px" class="btn btn-primary text-center part__button button--white" href="#">
            {l s='Show pricer' mod='upela'}</a>
        <div class="row hidden" id="pricer-form">
            <h5 style="padding:5px;margin-top: 10px" >{l s='Pricer explanation' mod='upela'}</h5>
            <hr>
            <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="form-group col-md-4">
                    <select class="form-control" id="ct-to" placeholder="country to">
                        {foreach from=$countryListe key=o item=info}
                            <option value="{$info.code}">{$info.name}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <input type="text" class="form-control" id="cdp-to" placeholder="{l s='postal code' mod='upela'}">
                </div>
                <div class="form-group col-md-4">
                    <input type="text" class="form-control" id="vl-to" placeholder="{l s='city' mod='upela'}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-3">
                    <label><small>{l s='Length' mod='upela'}</small>
                    <input type="text" class="form-control" id="cl-x" placeholder="{l s='Length' mod='upela'}" value="{$upela_length|escape:'htmlall':'UTF-8'}">
                    </label>
                </div>
                <div class="form-group col-md-3">
                    <label><small>{l s='Width' mod='upela'}</small>
                    <input type="text" class="form-control" id="cl-y" placeholder="{l s='Width' mod='upela'}" value="{$upela_width|escape:'htmlall':'UTF-8'}">
                    </label>
                </div>
                <div class="form-group col-md-3">
                    <label><small>{l s='Height' mod='upela'}</small>
                    <input type="text" class="form-control" id="cl-z" placeholder="{l s='Height' mod='upela'}" value="{$upela_height|escape:'htmlall':'UTF-8'}">
                    </label>
                </div>
                <div class="form-group col-md-3">
                    <label class="col-md-12"><small>{l s='Weight (Kg)' mod='upela'}</small>
                    <input type="text" class="form-control" id="cl-pd" placeholder="{l s='Weight (Kg)' mod='upela'}" value="{$upela_weight|escape:'htmlall':'UTF-8'}">
                    </label>
                </div>
            </div>
                <div class="col-md-12">
                    <div id="info-pricer" class="alert alert-info hidden col-md-6"><small>{l s='Please wait untill, we get you the best offers' mod='upela'}</small></div>
                    <div id="info-pricer-error" class="alert alert-warning hidden col-md-6"></div>
                    <a id="rate-it" class="btn btn-primary text-center part__button button--white pull-right" href="#">{l s='Estimate' mod='upela'}</a>
                </div>

            </div>
        </div>
        <hr>
        <div class="row">
            <form method="POST" action="{$upela_update_carrier_link|escape:'htmlall':'UTF-8'}">
                <table class="table">
                    <thead>
                    <tr>

                        <th class="carrier">{l s='Carrier' mod='upela'} </th>
                        <th class="offer">{l s='Offer' mod='upela'}</th>
                        <th class="from">{l s='From' mod='upela'}</th>
                        <th class="to">{l s='To' mod='upela'}</th>
                        <th class="delay">{l s='Delay' mod='upela'}</th>
                        <th class="status">{l s='Status' mod='upela'}</th>
                        <th class="edit">{l s='Edit' mod='upela'}</th>
                        <th class="edit"></th>
                    </tr>
                    </thead>
                    <tbody id="list-carrier-1">
                    {if isset($carriersListRelay) && $carriersListRelay && sizeof($carriersListRelay)}
                        <tr>
                            <td colspan=9><span class="accent-font"
                                                style="font-size: 16px">{l s='Relay' mod='upela'}</span></td>
                        </tr>
                        {foreach from=$carriersListRelay key=o item=offer}

                            <tr class="filter-offer">
                                <td class="operator">
                                    <img style="width: 20px;" src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/{if $offer.pricing_type == 1}poids-volume{else}icon-carriers{/if}.png"/> {$offer.label|escape:'htmlall':'UTF-8'}
                                </td>
                                <td class="offer">
                                    {$offer.desc_store|escape:'htmlall':'UTF-8'}
                                    <span class="label label-success" id ="{$offer.up_code_carrier|escape:'htmlall':'UTF-8'}-{$offer.up_code_service|escape:'htmlall':'UTF-8'}"></span>
                                </td>

                                {if $offer.is_pickup_point == 1}
                                    <td class="from">
                                        <span class="orange fa fa-home"></span>
                                        <span class="orange">{l s='On-site' mod='upela'}</span></td>
                                {else}
                                    <td class="from">
                                        <span class="blue fa fa-map-marker"></span>
                                        <span class="blue">{l s='Dropoff' mod='upela'}</span></td>
                                {/if}

                                {if $offer.is_relay == 0}
                                    <td class="from">
                                        <span class="orange fa fa-home"></span>
                                        <span class="orange">{l s='On-site' mod='upela'}</span></td>
                                {else}
                                    <td class="from">
                                        <span class="blue fa fa-map-marker"></span>
                                        <span class="blue">{l s='Dropoff' mod='upela'}</span></td>
                                {/if}


                                <td class="delay">{$offer.delay_text|escape:'htmlall':'UTF-8'}</td>

                                {if $offer.is_active == 1}
                                    <td class="status">
                                        <div class="hide">
                                            <input type="checkbox" name="offers3[]"
                                                   value="{$offer.id_service|escape:'htmlall':'UTF-8'}"
                                                   id="offer{$offer.id_service|escape:'htmlall':'UTF-8'}" {if $offer.is_active > 0} checked="checked"{/if}/>
                                        </div>
                                        <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/checkbox-checked.png" alt="true" class="toggleCarrier"
                                             onclick="UpelatoggleCarrier($(this))"></td>
                                {else}
                                    <td class="status">
                                        <div class="hide">
                                            <input type="checkbox" name="offers3[]"
                                                   value="{$offer.id_service|escape:'htmlall':'UTF-8'}"
                                                   id="offer{$offer.id_service|escape:'htmlall':'UTF-8'}" {if $offer.is_active > 0} checked="checked"{/if}/>
                                        </div>
                                        <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/unchecked-checkbox.png" alt="done" class="toggleCarrier"
                                             onclick="UpelatoggleCarrier($(this))">
                                    </td>
                                {/if}

                                <td class="edit">
                                    {if $offer.is_active == 1}
                                        <div class="btn-group-action">
                                            <div class="btn-group">
                                                <a href="{$carrierControllerUrl|escape:'htmlall':'UTF-8'}&id_carrier={$offer.id_carrier|escape:'htmlall':'UTF-8'}"
                                                   title="{l s='Edit' mod='upela'}" class="edit part__button show-parameters"
                                                   target="_blank" style="margin:0px">
                                                    {l s='Parametrer' mod='upela'}
                                                </a>
                                            </div>
                                        </div>
                                    {else}
                                        <div class="disable-edit">-</div>
                                    {/if}
                                </td>
                            </tr>

                        {/foreach}
                    {/if}
                    {if isset($carriersListOthers) && $carriersListOthers && sizeof($carriersListOthers)}
                        <tr>
                            <td colspan=9><span class="accent-font"
                                                style="font-size: 16px">{l s='Standard' mod='upela'}</span></td>
                        </tr>
                        {foreach from=$carriersListOthers key=o item=offer}

                            <tr  class="filter-offer">
                                <td class="operator"><img style="width: 20px;" src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/{if $offer.pricing_type == 1}poids-volume{else}icon-carriers{/if}.png"/> {$offer.label|escape:'htmlall':'UTF-8'}</td>
                                <td class="offer">{$offer.desc_store|escape:'htmlall':'UTF-8'}
                                    <span class="label label-success" id ="{$offer.up_code_carrier|escape:'htmlall':'UTF-8'}-{$offer.up_code_service|escape:'htmlall':'UTF-8'}"></span>
                                </td>

                                {if $offer.is_pickup_point == 1}
                                    <td class="from">
                                        <span class="orange fa fa-home"></span>
                                        <span class="orange">{l s='On-site' mod='upela'}</span></td>
                                {else}
                                    <td class="from">
                                        <span class="blue fa fa-map-marker"></span>
                                        <span class="blue">{l s='Dropoff' mod='upela'}</span></td>
                                {/if}

                                {if $offer.is_relay == 0}
                                    <td class="from">
                                        <span class="orange fa fa-home"></span>
                                        <span class="orange">{l s='On-site' mod='upela'}</span></td>
                                {else}
                                    <td class="from">
                                        <span class="blue fa fa-map-marker"></span>
                                        <span class="blue">{l s='Dropoff' mod='upela'}</span></td>
                                {/if}


                                <td class="delay">{$offer.delay_text|escape:'htmlall':'UTF-8'}</td>

                                {if $offer.is_active == 1}
                                    <td class="status">
                                        <div class="hide">
                                            <input type="checkbox" name="offers1[]"
                                                   value="{$offer.id_service|escape:'htmlall':'UTF-8'}"
                                                   id="offer{$offer.id_service|escape:'htmlall':'UTF-8'}" {if $offer.is_active > 0} checked="checked"{/if}/>
                                        </div>
                                        <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/checkbox-checked.png" alt="true" class="toggleCarrier"
                                             onclick="UpelatoggleCarrier($(this))">
                                    </td>
                                {else}
                                    <td class="status">
                                        <div class="hide">
                                            <input type="checkbox" name="offers1[]"
                                                   value="{$offer.id_service|escape:'htmlall':'UTF-8'}"
                                                   id="offer{$offer.id_service|escape:'htmlall':'UTF-8'}" {if $offer.is_active > 0} checked="checked"{/if}/>
                                        </div>
                                        <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/unchecked-checkbox.png" alt="done" class="toggleCarrier"
                                             onclick="UpelatoggleCarrier($(this))"></td>
                                {/if}

                                <td class="edit">
                                    {if $offer.is_active == 1}
                                        <div class="btn-group-action">
                                            <div class="btn-group">
                                                <a href="{$carrierControllerUrl|escape:'htmlall':'UTF-8'}&id_carrier={$offer.id_carrier|escape:'htmlall':'UTF-8'}"
                                                   title="{l s='Edit' mod='upela'}" class="edit part__button show-parameters"
                                                   target="_blank" style="margin:0px">
                                                    {l s='Parametrer' mod='upela'}
                                                </a>
                                            </div>
                                        </div>
                                    {else}
                                        <div class="disable-edit">-</div>
                                    {/if}
                                </td>
                            </tr>

                        {/foreach}
                    {/if}

                    {if isset($carriersListExpress) && $carriersListExpress && sizeof($carriersListExpress)}
                        <tr>
                            <td colspan=9><span class="accent-font"
                                                style="font-size: 16px">{l s='Express' mod='upela'}</span></td>
                        </tr>
                        {foreach from=$carriersListExpress key=o item=offer}

                            <tr  class="filter-offer">
                                <td class="operator"><img style="width: 20px;" src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/{if $offer.pricing_type == 1}poids-volume{else}icon-carriers{/if}.png"/> {$offer.label|escape:'htmlall':'UTF-8'}</td>
                                <td class="offer">{$offer.desc_store|escape:'htmlall':'UTF-8'}
                                    <span class="label label-success" id ="{$offer.up_code_carrier|escape:'htmlall':'UTF-8'}-{$offer.up_code_service|escape:'htmlall':'UTF-8'}"></span>
                                </td>

                                {if $offer.is_pickup_point == 1}
                                    <td class="from">
                                        <span class="orange fa fa-home"></span>
                                        <span class="orange">{l s='On-site' mod='upela'}</span></td>
                                {else}
                                    <td class="from">
                                        <span class="blue fa fa-map-marker"></span>
                                        <span class="blue">{l s='Dropoff' mod='upela'}</span></td>
                                {/if}

                                {if $offer.is_relay == 0}
                                    <td class="from">
                                        <span class="orange fa fa-home"></span>
                                        <span class="orange">{l s='On-site' mod='upela'}</span></td>
                                {else}
                                    <td class="from">
                                        <span class="blue fa fa-map-marker"></span>
                                        <span class="blue">{l s='Dropoff' mod='upela'}</span></td>
                                {/if}


                                <td class="delay">{$offer.delay_text|escape:'htmlall':'UTF-8'}</td>

                                {if $offer.is_active == 1}
                                    <td class="status">
                                        <div class="hide">
                                            <input type="checkbox" name="offers2[]"
                                                   value="{$offer.id_service|escape:'htmlall':'UTF-8'}"
                                                   id="offer{$offer.id_service|escape:'htmlall':'UTF-8'}" {if $offer.is_active > 0} checked="checked"{/if}/>
                                        </div>
                                        <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/checkbox-checked.png" alt="true" class="toggleCarrier" style="width: 16px"
                                             onclick="UpelatoggleCarrier($(this))"></td>
                                {else}
                                    <td class="status">
                                        <div class="hide">
                                            <input type="checkbox" name="offers2[]"
                                                   value="{$offer.id_service|escape:'htmlall':'UTF-8'}"
                                                   id="offer{$offer.id_service|escape:'htmlall':'UTF-8'}" {if $offer.is_active > 0} checked="checked"{/if}/>
                                        </div>
                                        <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/unchecked-checkbox.png" alt="done" class="toggleCarrier"
                                             onclick="UpelatoggleCarrier($(this))">
                                    </td>
                                {/if}

                                <td class="edit">
                                    {if $offer.is_active == 1}
                                        <div class="btn-group-action">
                                            <div class="btn-group">
                                                <a href="{$carrierControllerUrl|escape:'htmlall':'UTF-8'}&id_carrier={$offer.id_carrier|escape:'htmlall':'UTF-8'}"
                                                   title="{l s='Edit' mod='upela'}" class="edit part__button show-parameters"
                                                   target="_blank" style="margin:0px">
                                                    {l s='Parametrer' mod='upela'}
                                                </a>
                                            </div>
                                        </div>
                                    {else}
                                        <div class="disable-edit">-</div>
                                    {/if}
                                </td>
                            </tr>

                        {/foreach}
                    {/if}
                    </tbody>
                </table>
                <div class="margin-form submit" style="position: fixed;bottom: 5%;right: 15px;">
                    <div class="pull-right">
                        <button name="processParameters" type="submit"
                                class="btn btn-primary text-center part__button button--white pull -right">
                            {l s='Save' mod='upela'}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


</div>