{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* @author    UPELA
* @copyright 2017-2018 MPG Upela
* @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

    {if $upela_user_connected}
        <div class="panel">
            <div class="row">
                <div class="panel-content part__content">
                    <div class="col-lg-6">
                        <h2 >{l s='Store information' mod='upela'}</h2>
                        <small>{l s='To define store info' mod='upela'}</small>
                        <br>
                        <br>
                        <form method='POST' action="{$upela_store_update_link|escape:'htmlall':'UTF-8'}"
                              class="form-horizontal">
                            <div class="col-lg-6">
                                <div class="form-group col-lg-10">
                                        <label for="store_name" >{l s='Store name' mod='upela'}*</label>
                                        <input name="store_name" required type="text" class="form-control" id="store_name"
                                               placeholder="{l s='Store name' mod='upela'}"
                                               value="{$storeInfos['upela_store_name']|escape:'htmlall':'UTF-8'}">
                                </div>
                                <div class="form-group col-lg-10">
                                    <label for="store_firstname"
                                           class="col-sm-4">{l s='Firstname' mod='upela'}*</label>
                                        <input name="store_firstname" required type="text" class="form-control" id="store_firstname"
                                               placeholder="{l s='Firstname' mod='upela'}"
                                               value="{$storeInfos['upela_store_firstname']|escape:'htmlall':'UTF-8'}">
                                </div>
                                <div class="form-group col-lg-10">
                                    <label for="store_lastname">{l s='Lastname' mod='upela'}*</label>
                                        <input name="store_lastname" type="text" required class="form-control" id="store_lastname"
                                               placeholder="{l s='Lastname' mod='upela'}"
                                               value="{$storeInfos['upela_store_lastname']|escape:'htmlall':'UTF-8'}">
                                </div>
                                <div class="form-group col-lg-10">
                                    <label for="store_phone" >{l s='Phone' mod='upela'}*</label>
                                        <input name="store_phone" type="text" required class="form-control" id="store_phone"
                                               placeholder="{l s='Phone' mod='upela'}"
                                               value="{$storeInfos['upela_store_phone']|escape:'htmlall':'UTF-8'}">
                                </div>
                                <div class="form-group col-lg-10">
                                    <label for="store_email" >{l s='Email' mod='upela'}*</label>
                                        <input name="store_email" type="email" required class="form-control" id="store_email"
                                               placeholder="{l s='Email' mod='upela'}"
                                               value="{$storeInfos['upela_store_email']|escape:'htmlall':'UTF-8'}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group col-lg-12">
                                    <label for="store_address1">{l s='Address 1' mod='upela'}*</label>
                                        <input name="store_address1" type="text" required class="form-control" id="store_address1"
                                               placeholder="{l s='Address 1' mod='upela'}"
                                               value="{$storeInfos['upela_store_address1']|escape:'htmlall':'UTF-8'}">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="store_address2">{l s='Address 2' mod='upela'}</label>
                                        <input name="store_address2" type="text" class="form-control" id="store_address2"
                                               placeholder="{l s='Address 2' mod='upela'}"
                                               value="{$storeInfos['upela_store_address2']|escape:'htmlall':'UTF-8'}">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="store_zipcode" >{l s='ZIP Code' mod='upela'}*</label>
                                        <input name="store_zipcode" type="text" required class="form-control" id="store_zipcode"
                                               placeholder="{l s='Zip code' mod='upela'}"
                                               value="{$storeInfos['upela_store_zipcode']|escape:'htmlall':'UTF-8'}">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="store_city">{l s='City' mod='upela'}*</label>
                                        <input name="store_city" type="text" class="form-control" required id="store_city"
                                               placeholder="{l s='city' mod='upela'}"
                                               value="{$storeInfos['upela_store_city']|escape:'htmlall':'UTF-8'}">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="store_country">{l s='Country' mod='upela'}*</label>
                                    <select class="form-control" name="store_country" disabled="disabled">
                                        {foreach from=$countryListe key=o item=info}
                                            <option value="{$info.code}">{$info.name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>

                                <div class="col-sm-12">
                                    <button name="processStoreUpdate" type="submit"
                                            class="btn btn-primary text-center part__button button--white pull-right">
                                        {l s='Save' mod='upela'}
                                    </button>
                                </div>

                        </form>
                    </div>

                    <div class="col-lg-6">
                        <h2 >{l s='Payment information' mod='upela'}</h2>
                        <small>{l s='To define payement info' mod='upela'}</small>
                        <br>
                        <br>
                        {if {$paymentInfos['info']|escape:'htmlall':'UTF-8'} == false}
                            <h4>{l s='No payment informations avalaible !' mod='upela'}</h4>
                        {else}
                            {if {$paymentInfos['avalaible']|escape:'htmlall':'UTF-8'} == false}
                                <h4 class="text-orange">
                                    {l s='You can not ship your orders directly. You must switch your account to SEPA payment or credit your account!' mod='upela'}
                                </h4>
                                <br>
                            {/if}

                            <table class="table">
                                <tr>
                                    <td style="font-size: 16px">
                                        {l s='Payment method' mod='upela'}
                                    </td>
                                    <td style="font-size: 16px">
                                        {$paymentInfos['method']|escape:'htmlall':'UTF-8'}
                                    </td>
                                </tr>
                                {if {$paymentInfos['method']|escape:'htmlall':'UTF-8'}=={l s='Credit card' mod='upela'}}
                                    <tr>
                                        <td style="font-size: 16px">
                                            {l s='Amount avalaible' mod='upela'}
                                        </td>
                                        <td style="font-size: 16px">
                                            {$paymentInfos['amount']|escape:'htmlall':'UTF-8'}
                                        </td>
                                    </tr>
                                {/if}
                                {if {$paymentInfos['voucher']|escape:'htmlall':'UTF-8'}==true}
                                    <tr>
                                        <td style="font-size: 16px">
                                            {l s='Voucher amount' mod='upela'}
                                        </td>
                                        <td style="font-size: 16px">
                                            {$paymentInfos['vamount']|escape:'htmlall':'UTF-8'}
                                        </td>
                                    </tr>
                                {/if}
                            </table>
                                    <div class="col-lg-12">
                                        <a href="{$upela_param_link|escape:'htmlall':'UTF-8'}"
                                           class="part__button button--white pull-right"
                                           target="_blank"
                                           style="text-align: center;">
                                            {l s='Go to payment update' mod='upela'}
                                        </a>
                                    </div>

                        {/if}

                    </div>
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="row">
                <div class="panel-content part__content">
                    <div class="col-lg-12">
                        <h2 class="">{l s='Shipment information' mod='upela'}</h2>
                        <small>{l s='Default parcel informations' mod='upela'}</small>
                        <br>
                        <br>
                        <form method='POST' action="{$upela_parameters_link|escape:'htmlall':'UTF-8'}">
                            <div class="row">
                            <div class="form-group col-md-6">
                                <label for="ship_content" >{l s='Shipments content' mod='upela'}</label>
                                    <input name="ship_content" type="text" class="form-control" id="ship_content"
                                           placeholder="{l s='Shipment content' mod='upela'}"
                                           value="{$upela_ship_content|escape:'htmlall':'UTF-8'}">
                            </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">

                            <div class="form-group col-sm-3">
                                <label for="wt" >{l s='Weight (Kg)' mod='upela'}</label>
                                    <input name="upela_weight" type="text" class="form-control col-md-10" id="upela_weight"
                                           placeholder="{l s='Weight (Kg)' mod='upela'}"
                                           value="{$upela_weight|escape:'htmlall':'UTF-8'}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="wt">{l s='Length' mod='upela'} {l s='(cm)' mod='upela'}</label>
                                    <input name="upela_length" type="text" class="form-control col-md-10" id="upela_length"
                                           placeholder="{l s='Length' mod='upela'}"
                                           value="{$upela_length|escape:'htmlall':'UTF-8'}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="wt" >{l s='Width' mod='upela'} {l s='(cm)' mod='upela'}</label>
                                    <input name="upela_width" type="text" class="form-control col-md-10" id="upela_width"
                                           placeholder="{l s='Width' mod='upela'}"
                                           value="{$upela_width|escape:'htmlall':'UTF-8'}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="wt">{l s='Height' mod='upela'} {l s='(cm)' mod='upela'}</label>
                                    <input name="upela_height" type="text" class="form-control col-md-10" id="upela_height"
                                           placeholder="{l s='Height' mod='upela'}"
                                           value="{$upela_height|escape:'htmlall':'UTF-8'}">
                            </div>

                            </div>


                            <div class="form-group pull-right">
                                    <button name="processParameters" type="submit"
                                            class="btn btn-primary text-center part__button button--white pull -right">
                                        {l s='Save' mod='upela'}
                                    </button>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

    {else}
        <div class="panel">
        <div class="row">
        <div class="panel-content part__content">
            <h2>{l s='Connection parameters to your Upela account' mod='upela'}</h2>
            <br>
            <br>
        <div class="col-lg-8">
            <form method='POST' action="{$upela_login_link|escape:'htmlall':'UTF-8'}"
                  class="form-horizontal">
                <div class="form-group">
                    <label for="email" class="col-sm-4">{l s='Email' mod='upela'}</label>
                    <div class="col-sm-8">
                        <input name="upela_email" type="email" class="form-control" id="email"
                               placeholder="{l s='Email' mod='upela'}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-4">{l s='Password' mod='upela'}</label>
                    <div class="col-sm-8">
                        <input name="upela_password" type="password" class="form-control"
                               id="password"
                               placeholder="{l s='Password' mod='upela'}">
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group pull-right">
                        <button name="processLogin" type="submit"
                                class="btn btn-primary text-center part__button">
                            {l s='Log Into your Account' mod='upela'}
                        </button>
                    </div>
                    <div class="form-group pull-right" style="margin-right:20px">
                        <a href="{$upela_register_link|escape:'htmlall':'UTF-8'}"
                           class="part__button button--white">
                            {l s='Create a Business Account' mod='upela'}
                        </a>
                    </div>
                </div>

            </form>

        </div>
        </div>
        </div>
        </div>
    {/if}