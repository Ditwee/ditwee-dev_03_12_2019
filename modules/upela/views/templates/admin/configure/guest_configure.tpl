{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* @author    UPELA
* @copyright 2017-2018 MPG Upela
* @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*}

{if isset($postSuccess)}
    {foreach from=$postSuccess item=ps}
        <div class="alert alert-success">{$ps|escape:'htmlall':'UTF-8'}</div>
    {/foreach}
{/if}

{if isset($postErrors)}
    {foreach from=$postErrors item=pe}
        <div class="alert alert-danger">{$pe|escape:'htmlall':'UTF-8'}</div>
    {/foreach}
{/if}
{if isset($postInfos)}
    {foreach from=$postInfos item=pe}
        <div class="alert alert-info">{$pe|escape:'htmlall':'UTF-8'}</div>
    {/foreach}
{/if}

{if isset($modeform) && !$upela_user_connected}
    {$modeform}
{/if}

<div id="upela_mnu">
    <ul class="nav nav-tabs" id="upelaTabs">
        {if {$param_select|escape:'htmlall':'UTF-8'} ==true || {$carrier_select|escape:'htmlall':'UTF-8'}==true}
        <li class="nav-item">
            {else}
        <li class="nav-item active">
            {/if}
            <a href="#home_form" data-toggle="tab" role="tab">
                <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/logo-upela.png"/>
                {l s='The Upela solution' mod='upela'}
            </a>
        </li>

        {if $upela_user_connected}
        <li class="nav-item {if {$param_select|escape:'htmlall':'UTF-8'}==true}active{/if}">
            <a href="#settings_form" data-toggle="tab" role="tab">
                <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/icon-config.png"/>
                {l s='Parameters' mod='upela'}
            </a>
        </li>
        {/if}

        {if {$upela_user_connected|escape:'htmlall':'UTF-8'}==true}
            <li class="nav-item {if {$carrier_select|escape:'htmlall':'UTF-8'}==true}active{/if}">
                <a href="#carriers_form" data-toggle="tab" role="tab">
                    <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/icon-carriers.png"/>
                    {l s='Carriers Weight' mod='upela'}
                </a>
            </li>
        {/if}
        {if {$isnotpsready|escape:'htmlall':'UTF-8'}==true}
            <li class="nav-item">
                <a href="#guide_form" data-toggle="tab" role="tab">
                    <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/icon-guide.png"/>
                    {l s='User manual' mod='upela'}
                </a>
            </li>
            <li class="nav-item">
                <a href="#contact_form" data-toggle="tab" role="tab">
                    <img src="{$_path|escape:'htmlall':'UTF-8'}views/img/icons/icon-contact.png"/>
                    {l s='Contact' mod='upela'}
                </a>
            </li>
        {/if}
    </ul>
</div>

<div id="upela_content" class="tab-content">
    <div class="tab-pane {if {$param_select|escape:'htmlall':'UTF-8'} ==false && {$carrier_select|escape:'htmlall':'UTF-8'}==false}active{/if}"
         id="home_form"
         role="tabpanel">
        {include file="$tpl_home"}
    </div>

    <div class="tab-pane {if {$carrier_select|escape:'htmlall':'UTF-8'}==true}active{/if}" id="carriers_form"
         role="tabpanel">
        {include file="$tpl_carriers"}
    </div>
    <div class="tab-pane" id="guide_form" role="tabpanel">
        {include file="$tpl_guide"}
    </div>
    <div class="tab-pane {if {$param_select|escape:'htmlall':'UTF-8'}==true}active{/if}" id="settings_form"
         role="tabpanel">
        {include file="$tpl_params"}
    </div>

    <div class="tab-pane" id="contact_form" role="tabpanel">
        {include file="$tpl_contact"}
    </div>

</div>


<script type="text/javascript">


    $('.show-parameters').on('click',function(e){
        $('#upelaTabs a[href="#settings_form"]').tab('show');
    });


    function UpelatoggleCarrier(carrier) {
        Upela_modify = true;
        var value = carrier.attr('alt');


        var prices = carrier.parents('tr').find('.price').children('div');
        var checkbox = carrier.parent('td').find('input');

        if (value === 'true') {
            //prices.fadeOut();
            carrier.attr('alt', 'false');
            checkbox.attr('checked', false);
            carrier.attr('src', '{$_path|escape:'htmlall':'UTF-8'}views/img/icons/unchecked-checkbox.png');
        } else {
            carrier.attr('alt', 'true');
            checkbox.attr('checked', true);
            carrier.attr('src', '{$_path|escape:'htmlall':'UTF-8'}views/img/icons/checkbox-checked.png');
        }


    }


    $('#pricer').on('click', function () {
        $('#pricer-form').toggleClass('hidden');
    });

    $('#rate-it').on('click', function () {

        $('#info-pricer-error').addClass('hidden', '');
        $('#info-pricer').toggleClass('hidden', '');
        $('.filter-offer').addClass('hidden', '');

        var base = '{$url_site}';
        var backwardDirectory  = base.substr(-1) === '/' ? '':'/';
        var url = base+backwardDirectory+'index.php?fc=module&module=upela&controller=ajax&option=rate';

        // get base uri of the module



        var toct = $('#ct-to').val();
        var tocdp = $('#cdp-to').val();
        var tovl = $('#vl-to').val();
        var clx = $('#cl-x').val();
        var cly = $('#cl-y').val();
        var clz = $('#cl-z').val();

        var clp = $('#cl-pd').val();

        var dateObj = new Date();
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        newdate = year + "-" + month + "-" + day;

        var data = {
            to: {
                country: toct,
                cp: tocdp,
                city: tovl,
                pro: 0
            },
            parcels: {
                number: 1,
                weight: clp,
                length: clx,
                width: cly,
                height: clz
            }
        };

        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (s) {
                //console.log(s);
                var result = JSON.parse(s);
                var offers = result.offers;

                if (result.success !== false) {
                    $(offers).each(function () {
                        var id = this.carrier_code.replace(/ /g,'') + '-' + this.service_code;
                        var price_ti = this.price_excl_tax;
                        $('#' + id).text(price_ti + '€ HT');
                        $('#' + id).closest('tr').removeClass('hidden');
                    });
                }
                else {
                    var bufferErrors = '';
                    for (var key in result.errors) {
                        bufferErrors += result.errors[key];
                    }
                    $('#info-pricer-error').text(bufferErrors);
                    $('#info-pricer-error').removeClass('hidden', '');
                }

                $('#info-pricer').addClass('hidden', '');
            }
        });
    });
</script>
