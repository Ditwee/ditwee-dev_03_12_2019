{if isset($xipblogcategories)}
<div class="xipblogcategories block">
	<h4 class="title_block">
		{if isset($xipbc_title)}{$xipbc_title}{/if}
	</h4>
	<div class="block_content">
		<ul class="tree">
			{foreach from=$xipblogcategories item=xipblogcategory}
				<li>
					<a href="{$xipblogcategory.link}" title="{$xipblogcategory.name}">
						{$xipblogcategory.name}
					</a>
				</li>
			{/foreach}
		</ul>
	</div>
</div>
{/if}