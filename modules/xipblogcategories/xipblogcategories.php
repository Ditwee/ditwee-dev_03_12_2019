<?php

if (!defined('_CAN_LOAD_FILES_')) {
	exit;
}

class xipblogcategories extends Module {
	public $css_array = array("xipblogcategories.css", "xipblogcategories.css");
	public $js_array = array("xipblogcategories.js", "xipblogcategories.js");
	public function __construct(){
		$this->name = 'xipblogcategories';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'xpert-idea';
		$this->bootstrap = true;
		$this->dependencies = array('xipblog');
		parent::__construct();
		$this->displayName = $this->l('XipBlog Categories');
		$this->description = $this->l('Display Categories Module');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	// For installation service
	public function install() {
		if (!parent::install()
			// || !$this->registerHook('displayheader')
			// || !$this->registerHook('displayhome')
			// || !$this->registerHook('LeftColumn')
			// || !$this->registerHook('RightColumn')
			// || !$this->registerHook('displayxipblogleft')
			|| !$this->registerHook('displayxipblogright')
			)
			return false;
			$languages = Language::getLanguages(false);
           	foreach($languages as $lang){
        		Configuration::updateValue('xipbc_title_'.$lang['id_lang'],"Blog Categories");
           	}
        	Configuration::updateValue('xipbc_tagcount',4);
			return true;
	}
	// For uninstallation service
	public function uninstall() {
		if (!parent::uninstall()
			)
			return false;
		else
			return true;
	}
	// Helper Form for Html markup generate
	public function SettingForm() {

		$default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Setting'),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button',
			),
		);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Title'),
				'name' => 'xipbc_title',
				'lang' => true,
			);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('How Many Category You Want To Display'),
				'name' => 'xipbc_tagcount',
			);
		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
		foreach (Language::getLanguages(false) as $lang) {
			$helper->languages[] = array(
				'id_lang' => $lang['id_lang'],
				'iso_code' => $lang['iso_code'],
				'name' => $lang['name'],
				'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0),
			);
		}
		$helper->toolbar_btn = array(
			'save' => array(
				'desc' => $this->l('Save'),
				'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name . 'token=' . Tools::getAdminTokenLite('AdminModules'),
			),
		);
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->title = $this->displayName;
		$helper->show_toolbar = true;
		$helper->toolbar_scroll = true;
		$helper->submit_action = 'save' . $this->name;
		$languages = Language::getLanguages(false);
           	foreach($languages as $lang){
           		$helper->fields_value['xipbc_title'][$lang['id_lang']] = Configuration::get('xipbc_title_'.$lang['id_lang']);
           	}
           	$helper->fields_value['xipbc_tagcount'] = Configuration::get('xipbc_tagcount');
		return $helper;
	}
	// All Functional Logic here.
	public function getContent() {
		$html = '';
		if (Tools::isSubmit('save' . $this->name)) {
			$languages = Language::getLanguages(false);
               foreach($languages as $lang){
            		Configuration::updateValue('xipbc_title_'.$lang['id_lang'],Tools::getvalue('xipbc_title_'.$lang['id_lang']));
               }
            	Configuration::updateValue('xipbc_tagcount',Tools::getvalue('xipbc_tagcount'));
		}
		$helper = $this->SettingForm();
		$html .= $helper->generateForm($this->fields_form);
		return $html;
	}
	// Display Header Hook Execute Functions
	public function hookdisplayheader($params) {
		if(isset($this->css_array))
			foreach ($this->css_array as $css) {
				$this->context->controller->addCSS($this->_path.'css/'.$css);
			}
		if(isset($this->js_array))
			foreach ($this->js_array as $js) {
				$this->context->controller->addJS($this->_path.'js/'.$js);
			}
	}
	// Display Header Hook Execute Functions
	public function ExecuteHook(){
		$id_lang = (int)$this->context->language->id;
		$xipbc_title = Configuration::get('xipbc_title_'.$id_lang);
		$xipbc_tagcount = Configuration::get('xipbc_tagcount');
		$xipblogcategories = array();
		if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog')){
			$xipblogcategories = xippostsclass::GetBlogTags($xipbc_tagcount,'category');
		}
		$this->smarty->assign(
			array(
				'xipbc_title' => $xipbc_title,
				'xipbc_tagcount' => $xipbc_tagcount,
				'xipblogcategories' => $xipblogcategories,
			)
		);
	}
	public function hookdisplayhome($params) {
		$this->ExecuteHook();
		return $this->display(__FILE__,'views/templates/front/xipblogcategories.tpl');	
	}
	public function hookLeftColumn($params) {
		$this->ExecuteHook();
		return $this->display(__FILE__,'views/templates/front/xipblogcategoriesleft.tpl');	
	}
	public function hookRightColumn($params) {
		$this->ExecuteHook();
		return $this->display(__FILE__,'views/templates/front/xipblogcategoriesleft.tpl');	
	}
	public function hookdisplayxipblogleft($params) {
		$this->ExecuteHook();
		return $this->display(__FILE__,'views/templates/front/xipblogcategoriesleft.tpl');	
	}
	public function hookdisplayxipblogright($params) {
		$this->ExecuteHook();
		return $this->display(__FILE__,'views/templates/front/xipblogcategoriesleft.tpl');	
	}
	// Display Header Hook Execute Functions
	public function hookdisplayhomefullwidthmiddle($params) {
		$this->ExecuteHook();
		return $this->display(__FILE__,'views/templates/front/xipblogcategories.tpl');
	}
}