<div class="home_blog_post_area {$xipbdp_designlayout}">
	<div class="home_blog_post">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xipbdp_title)}
				<h3 class="page-heading">
					<em>{$xipbdp_title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xipbdp_subtext)}
				<p class="page_subtitle d_none">{$xipbdp_subtext}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		<div class="row home_blog_post_inner carousel">
		{foreach from=$xipblogposts item=xipblgpst}
			<article class="blog_post col-xs-12 col-sm-4">
				<div class="blog_post_content">
					<div class="blog_post_content_top">
						<div class="post_thumbnail">
							{if $xipblgpst.post_format == 'video'}
								{assign var="postvideos" value=','|explode:$xipblgpst.video}
								{include file="./post-video.tpl" videos=$postvideos width='570' height="316" class="{if $postvideos|@count > 1 }carousel{/if}"}
							{elseif $xipblgpst.post_format == 'audio'}
								{assign var="postaudio" value=','|explode:$xipblgpst.audio}
								{include file="./post-audio.tpl" audios=$postaudio width='570' height="316" class="{if $postaudio|@count > 1 }carousel{/if}"}
							{elseif $xipblgpst.post_format == 'gallery'}
								{include file="./post-gallery.tpl" gallery=$xipblgpst.gallery_lists imagesize="home_default" class="{if $xipblgpst.gallery_lists|@count > 1 }carousel{/if}"}
							{else}
								<img class="img-responsive" src="{$xipblgpst.post_img_home_default}" alt="{$xipblgpst.post_title}">
								<div class="blog_mask">
									<div class="blog_mask_content">
										<a class="thumbnail_lightbox" href="{$xipblgpst.post_img_large}">
											<i class="icon-expand"></i>
										</a>
									</div>
								</div>
							{/if}
						</div>
					</div>
					<div class="post_content">
						<div class="post_meta clearfix">
							<p class="meta_date">
								<i class="icon-calendar"></i>
								{$xipblgpst.post_date|date_format:"%b %d, %Y"}
							</p>
							<p class="meta_author">
								<i class="icon-user"></i>
								<span>{$xipblgpst.post_author_arr.firstname} {$xipblgpst.post_author_arr.lastname}</span>
							</p>
							<p class="meta_category">
								<i class="icon-tag"></i>
									<a href="{$xipblgpst.category_default_arr.link}">{$xipblgpst.category_default_arr.name}</a>
							</p>
						</div>
						<h3 class="post_title"><a href="{$xipblgpst.link}">{$xipblgpst.post_title}</a></h3>
						<div class="post_description">
							{if isset($xipblgpst.post_excerpt) && !empty($xipblgpst.post_excerpt)}
								<p>{$xipblgpst.post_excerpt|truncate:100:' { ... }'|escape:'html':'UTF-8'}</p>
							{else}
								<p>{$xipblgpst.post_content|truncate:100:' { ... }'|escape:'html':'UTF-8'}</p>
							{/if}
						</div>
						<div class="read_more d_none">
							<a class="more" href="{$xipblgpst.link}">{l s='Read More..' mod='xipblogdisplayposts'}</a>
						</div>
					</div>
				</div>
			</article>
		{/foreach}
		</div>
	</div>
</div>
{addJsDef xipbdp_numcolumn=$xipbdp_numcolumn|intval}