{if $xipblogposts !== false}
<div class="xipblogrecentposts block blog_block blog_recentpost_footer col-sm-3">
	<h4 class="title_block">
    	{if isset($xipbrp_title)}{$xipbrp_title}{/if}
    </h4>
    <div class="block_content list-block">
        <ul>
        	{foreach from=$xipblogposts item=xipblgpst}
        		<li class="clearfix">
					<a class="product-name" href="{$xipblgpst.link}">
						{$xipblgpst.post_title|truncate:40:'..'}
					</a>
                     <p>{$xipblgpst.post_date|date_format:"%e %b %Y"}</p>    
        		</li>
        	{/foreach}
        </ul>
    </div>
</div>
{else}
	<p>{l s='No recent post at this time.' mod='xipblogrecentposts'}</p>
{/if}