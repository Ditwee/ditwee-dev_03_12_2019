{if $xipblogposts !== false}
<div class="xipblogrecentposts block blog_block">
	<h4 class="title_block">
    	{if isset($xipbrp_title)}{$xipbrp_title}{/if}
    </h4>
    <div class="block_content products-block">
        <ul>
        	{foreach from=$xipblogposts item=xipblgpst}
        		<li class="clearfix">
        			{if $xipblgpst.post_img != 'noimage.jpg'}
        			<a href="{$xipblgpst.link}" title="{$xipblgpst.post_title}" class="products-block-image" style="background-image:url('{$xipblgpst.post_img_small}')">
        			</a>
        			{else}
						<a href="{$xipblgpst.link}" title="{$xipblgpst.post_title}" class="products-block-image" style="background-image:url('{$img_dir}bg/no_bg.jpg');background-size:100%;">
						</a>
        			{/if}
        			<div class="product-content">
                    	<h5>
                        	<a class="product-name" href="{$xipblgpst.link}">{$xipblgpst.post_title|truncate:40:'..'}</a>
                        </h5>
                        <p>{$xipblgpst.post_author_arr.firstname} {$xipblgpst.post_author_arr.lastname}</p>
                        <div class="price-box">
                        	{$xipblgpst.post_date|date_format:"%e %b %Y"}
                        </div>
                    </div>
        		</li>
        	{/foreach}
        </ul>
    </div>
</div>
{else}
	<p>{l s='No recent post at this time.' mod='xipblogrecentposts'}</p>
{/if}