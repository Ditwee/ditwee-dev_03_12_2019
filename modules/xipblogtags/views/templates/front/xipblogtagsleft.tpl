
{if isset($xipblogtags)}
<!-- Block categories module -->
<div class="xipblogtags block tags_block">
	<h2 class="title_block">
		{if isset($xipbt_title)}{$xipbt_title}{/if}
	</h2>
	<div class="block_content">
		<ul class="clearfix">
			{foreach from=$xipblogtags item=xipblogtag}
				<li>

					<a href="{$xipblogtag.link}" title="{$xipblogtag.name}">
						{$xipblogtag.name}
					</a>
							
				</li>
			{/foreach}
		</ul>
	</div>
</div>
{else}
	<p>{l s='No tags have been specified yet.' mod='xipblogtags'}</p>
{/if}





{* {if isset($xipbt_title)}{$xipbt_title}{/if}

{if isset($xipblogtags)}

	{foreach from=$xipblogtags item=xipblogtag}
			{$xipblogtag.id_xipcategory}<br>
			{$xipblogtag.id_tag}<br>
			{$xipblogtag.name}<br>
			{$xipblogtag.link}<br>
	{/foreach}

{/if} *}