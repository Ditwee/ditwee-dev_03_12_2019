<?php
$productblock_dummy_data = array(
    array(
    	'lang' => array(
		    'title' => 'Accessories',
		    'sub_title' => '',
		),
	    'notlang' => array(
			'count_prd'	=>	'10',
			'transition_period'	=>	'',
			'product_scroll'	=>	'per_item',
			'product_rows'	=>	'1',
			'play_again'	=>	0,
			'navigation_dots'	=>	0,
			'navigation_arrow'	=>	0,
			'autoplay'	=>	0,
			'hook'	=>	'displayProductRightSidebar',
			'image'	=>	'',
			'pages'	=>	'product',
			'layout'	=>	'left-sidebar',
			'truck_identify'	=>	'',
			'device'	=>	'{"device_xs":"1","device_sm":"2","device_md":"3","device_lg":"3"}',
			'active'	=>	'1',
			'content'	=>	'{"section_margin":"0px 0px 30px 0px","order_by":"id_product","order_way":"ASC","enable_carousel":"0","nav_arrow_style":"arrow_top"}',
			'position'	=>	0,
		),
    ),
);