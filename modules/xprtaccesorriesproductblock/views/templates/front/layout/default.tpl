{if isset($xprtaccesorriesproductblock) && !empty($xprtaccesorriesproductblock) && !empty($xprtaccesorriesproductblock.products)}
	{if isset($xprtaccesorriesproductblock.device)}
		{assign var=device_data value=$xprtaccesorriesproductblock.device|json_decode:true}
	{/if}
	<div id="xprtaccesorriesproductblock_{$xprtaccesorriesproductblock.id_xprtaccesorriesproductblock}" class="xprtaccesorriesproductblock xprt_default_products_block" style="margin:{$xprtaccesorriesproductblock.section_margin};">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtaccesorriesproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtaccesorriesproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtaccesorriesproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtaccesorriesproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		{if isset($xprtaccesorriesproductblock) && $xprtaccesorriesproductblock}
			<div id="xprt_relatedproductsblock" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtaccesorriesproductblock.products id='' class="xprt_relatedproductsblock {if $xprtaccesorriesproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtaccesorriesproductblock'}</p>
			</div>
		{/if}
	</div>
<script type="text/javascript">
	var xprtaccesorriesproductblock = $("#xprtaccesorriesproductblock_{$xprtaccesorriesproductblock.id_xprtaccesorriesproductblock}");
	var sliderSelect = xprtaccesorriesproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtaccesorriesproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtaccesorriesproductblock.nav_arrow_style) && ($xprtaccesorriesproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}
	
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtaccesorriesproductblock.play_again)}{$xprtaccesorriesproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtaccesorriesproductblock.autoplay)}{$xprtaccesorriesproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtaccesorriesproductblock.pause_on_hover)}{$xprtaccesorriesproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtaccesorriesproductblock.navigation_dots)}{$xprtaccesorriesproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtaccesorriesproductblock.navigation_arrow)}{$xprtaccesorriesproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtaccesorriesproductblock.product_rows)}{$xprtaccesorriesproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtaccesorriesproductblock.product_scroll) && ($xprtaccesorriesproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtaccesorriesproductblock.product_scroll) && ($xprtaccesorriesproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtaccesorriesproductblock.product_scroll) && ($xprtaccesorriesproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtaccesorriesproductblock.product_scroll) && ($xprtaccesorriesproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtaccesorriesproductblock.product_scroll) && ($xprtaccesorriesproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}