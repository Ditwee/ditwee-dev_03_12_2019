{if isset($xprtaccesorriesproductblock) && !empty($xprtaccesorriesproductblock) && !empty($xprtaccesorriesproductblock.products)}
	{if isset($xprtaccesorriesproductblock.device)}
		{assign var=device_data value=$xprtaccesorriesproductblock.device|json_decode:true}
	{/if}
	<div class="xprt_product_home_small col-sm-4">
		<div class="xprtaccesorriesproductblock block carousel">
			<h4 class="title_block">
		    	{$xprtaccesorriesproductblock.title}
		    </h4>
		    <div class="block_content products-block">
		        {if isset($xprtaccesorriesproductblock) && $xprtaccesorriesproductblock}
		        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtaccesorriesproductblock.products}
		        {else}
	        		<p class="alert alert-info">{l s='No products at this time.' mod='xprtaccesorriesproductblock'}</p>
		        {/if}
		    </div>
		</div>
	</div>
{/if}