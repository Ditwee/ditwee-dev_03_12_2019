{if isset($xprtaccesorriesproductblock) && !empty($xprtaccesorriesproductblock)}
	{if isset($xprtaccesorriesproductblock.device)}
		{assign var=device_data value=$xprtaccesorriesproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtaccesorriesproductblock.products) && !empty($xprtaccesorriesproductblock.products)}
		<div id="xprt_accessoriesproductsblock_tab_{if isset($xprtaccesorriesproductblock.id_xprtaccesorriesproductblock)}{$xprtaccesorriesproductblock.id_xprtaccesorriesproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtaccesorriesproductblock.products class='xprt_relatedproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_accessoriesproductsblock_tab_{if isset($xprtaccesorriesproductblock.id_xprtaccesorriesproductblock)}{$xprtaccesorriesproductblock.id_xprtaccesorriesproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No products at this time.' mod='xprtaccesorriesproductblock'}</p>
		</div>
	{/if}
{/if}