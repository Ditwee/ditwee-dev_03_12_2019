{if isset($xprtbestratedproductblock) && !empty($xprtbestratedproductblock)}
	{if isset($xprtbestratedproductblock.device)}
		{assign var=device_data value=$xprtbestratedproductblock.device|json_decode:true}
	{/if}
	<div class="xprt_product_home_small col-sm-4">
		<div class="xprtbestratedproductblock block carousel">
			<h4 class="title_block">
		    	<em>{$xprtbestratedproductblock.title}</em>
		    </h4>
		    <div class="block_content products-block">
		        {if isset($xprtbestratedproductblock) && $xprtbestratedproductblock}
		        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtbestratedproductblock.products}
		        {else}
	        		<p class="alert alert-info">{l s='No products at this time.' mod='xprtbestratedproductblock'}</p>
		        {/if}
		    </div>
		</div>
	</div>
{/if}