{if isset($xprtbestratedproductblock) && !empty($xprtbestratedproductblock)}
	{if isset($xprtbestratedproductblock.device)}
		{assign var=device_data value=$xprtbestratedproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtbestratedproductblock.products) && !empty($xprtbestratedproductblock.products)}
		<div id="xprt_bestratedproductsblock_tab_{if isset($xprtbestratedproductblock.id_xprtbestratedproductblock)}{$xprtbestratedproductblock.id_xprtbestratedproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtbestratedproductblock.products class='xprt_bestratedproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_bestratedproductsblock_tab_{if isset($xprtbestratedproductblock.id_xprtbestratedproductblock)}{$xprtbestratedproductblock.id_xprtbestratedproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No products at this time.' mod='xprtbestratedproductblock'}</p>
		</div>
	{/if}
{/if}