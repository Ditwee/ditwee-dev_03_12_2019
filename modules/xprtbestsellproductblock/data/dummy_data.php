<?php
$productblock_dummy_data = array(
    array(
    	'lang' => array(
		    'title' => 'Popular',
		    'sub_title' => 'Updated daily with our latest pieces',
		),
	    'notlang' => array(
			'count_prd'	=>	'10',
			'transition_period'	=>	'',
			'product_scroll'	=>	'per_item',
			'product_rows'	=>	'1',
			'play_again'	=>	0,
			'navigation_dots'	=>	0,
			'navigation_arrow'	=>	0,
			'autoplay'	=>	0,
			'hook'	=>	'displayHomeTabContent',
			'image'	=>	'',
			'pages'	=>	'index',
			'layout'	=>	'default',
			'truck_identify'	=>	'demo_2',
			'device'	=>	'{"device_xs":"1","device_sm":"2","device_md":"3","device_lg":"4"}',
			'active'	=>	0,
			'content'	=>	'{"section_margin":"0px 0px 60px 0px ","order_by":"id_product","order_way":"ASC","enable_carousel":"0"}',
			'position'	=>	0,
		),
    ),
    array(
    	'lang' => array(
		    'title' => 'Best sell',
		    'sub_title' => 'Updated daily with latest price',
		),
	    'notlang' => array(
			'count_prd'	=>	'',
			'transition_period'	=>	'',
			'product_scroll'	=>	'per_item',
			'product_rows'	=>	'1',
			'play_again'	=>	0,
			'navigation_dots'	=>	0,
			'navigation_arrow'	=>	0,
			'autoplay'	=>	0,
			'hook'	=>	'displayHomeTabContent',
			'image'	=>	'',
			'pages'	=>	'all_page',
			'layout'	=>	'default',
			'truck_identify'	=>	'demo_3',
			'device'	=>	'{"device_xs":"1","device_sm":"2","device_md":"3","device_lg":"3"}',
			'active'	=>	0,
			'content'	=>	'{"section_margin":"","order_by":"id_product","order_way":"ASC","enable_carousel":"0"}',
			'position'	=>	1,
		),
    ),
    array(
    	'lang' => array(
		    'title' => 'The Best Products',
		    'sub_title' => 'Updated daily with latest price',
		),
	    'notlang' => array(
			'count_prd'	=>	'10',
			'transition_period'	=>	'333',
			'product_scroll'	=>	'per_item',
			'product_rows'	=>	'2',
			'play_again'	=>	1,
			'navigation_dots'	=>	0,
			'navigation_arrow'	=>	1,
			'autoplay'	=>	1,
			'hook'	=>	'displayHomeTopRight',
			'image'	=>	'',
			'pages'	=>	'all_page',
			'layout'	=>	'default',
			'truck_identify'	=>	'demo_8',
			'device'	=>	'{"device_xs":"1","device_sm":"1","device_md":"2","device_lg":"2"}',
			'active'	=>	0,
			'content'	=>	'{"section_margin":"0px 0px 30px 0px","order_by":"id_product","order_way":"ASC","enable_carousel":"1","nav_arrow_style":"arrow_top"}',
			'position'	=>	2,
		),
    ),
    array(
    	'lang' => array(
		    'title' => 'Popular collection',
		    'sub_title' => 'Updated daily with latest price',
		),
	    'notlang' => array(
			'count_prd'	=>	'',
			'transition_period'	=>	'300',
			'product_scroll'	=>	'per_item',
			'product_rows'	=> '1',
			'play_again'	=>	1,
			'navigation_dots'	=>	0,
			'navigation_arrow'	=>	1,
			'autoplay'	=>	0,
			'hook'	=>	'displayHomeFullWidthMiddle',
			'image'	=>	'',
			'pages'	=>	'index',
			'layout'	=>	'fullwidth-samll-padding',
			'truck_identify'	=>	'demo_9',
			'device'	=>	'{"device_xs":"1","device_sm":"3","device_md":"4","device_lg":"4"}',
			'active'	=>	0,
			'content'	=>	'{"section_margin":"0px 0px 30px 0px","order_by":"id_product","order_way":"ASC","enable_carousel":"1"}',
			'position'	=>	3,
		),
    ),
    array(
    	'lang' => array(
		    'title' => 'Popular collection',
		    'sub_title' => 'Updated daily with latest price',
		),
	    'notlang' => array(
			'count_prd'	=>	'10',
			'transition_period'	=>	'300',
			'product_scroll'	=>	'per_item',
			'product_rows'	=>	'1',
			'play_again'	=>	1,
			'navigation_dots'	=>	0,
			'navigation_arrow'	=>	1,
			'autoplay'	=>	1,
			'hook'	=>	'displayFooterProduct',
			'image'	=>	'',
			'pages'	=>	'all_page',
			'layout'	=>	'default',
			'truck_identify'	=>	'demo_4',
			'device'	=>	'{"device_xs":"1","device_sm":"2","device_md":"3","device_lg":"4"}',
			'active'	=>	0,
			'content'	=>	'{"section_margin":"0px 0px 30px 0px","order_by":"id_product","order_way":"ASC","enable_carousel":"1","nav_arrow_style":"arrow_top"}',
			'position'	=>	4,
		),
    ),
    array(
    	'lang' => array(
		    'title' => 'Best Selling',
		    'sub_title' => '',
		),
	    'notlang' => array(
			'count_prd'	=>	'10',
			'transition_period'	=>	'',
			'product_scroll'	=>	'per_item',
			'product_rows'	=>	'1',
			'play_again'	=>	0,
			'navigation_dots'	=>	0,
			'navigation_arrow'	=>	0,
			'autoplay'	=>	0,
			'hook'	=>	'displayLeftColumn',
			'image'	=>	'',
			'pages'	=>	'all_page',
			'layout'	=>	'left-sidebar',
			'truck_identify'	=>	'demo_6',
			'device'	=>	'{"device_xs":"1","device_sm":"1","device_md":"1","device_lg":"1"}',
			'active'	=>	0,
			'content'	=>	'{"section_margin":"","order_by":"id_product","order_way":"ASC","enable_carousel":"0","nav_arrow_style":"arrow_top"}',
			'position'	=>	5,
		),
    ),
);