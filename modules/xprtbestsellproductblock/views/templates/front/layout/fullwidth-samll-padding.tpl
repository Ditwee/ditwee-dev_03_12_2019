{if isset($xprtbestsellproductblock) && !empty($xprtbestsellproductblock)}
	{if isset($xprtbestsellproductblock.device)}
		{assign var=device_data value=$xprtbestsellproductblock.device|json_decode:true}
	{/if}
	<div id="xprtbestsellproductblock_{$xprtbestsellproductblock.id_xprtbestsellproductblock}" class="xprtbestsellproductblock xprt_default_products_block" style="margin:{$xprtbestsellproductblock.section_margin};">
		
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtbestsellproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtbestsellproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtbestsellproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtbestsellproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>

		{if isset($xprtbestsellproductblock) && $xprtbestsellproductblock}
			<div id="xprt_bestproductsblock_{$xprtbestsellproductblock.id_xprtbestsellproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtbestsellproductblock.products id='' class="xprt_bestproductsblock product_small_padding {if $xprtbestsellproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No best sale products at this time.' mod='xprtbestsellproductblock'}</p>
			</div>
		{/if}
	</div>
	<script type="text/javascript">
	var xprtbestsellproductblock = $("#xprtbestsellproductblock_{$xprtbestsellproductblock.id_xprtbestsellproductblock}");
	var sliderSelect = xprtbestsellproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtbestsellproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtbestsellproductblock.nav_arrow_style) && ($xprtbestsellproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}
	
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtbestsellproductblock.play_again)}{$xprtbestsellproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtbestsellproductblock.autoplay)}{$xprtbestsellproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtbestsellproductblock.pause_on_hover)}{$xprtbestsellproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtbestsellproductblock.navigation_dots)}{$xprtbestsellproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtbestsellproductblock.navigation_arrow)}{$xprtbestsellproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtbestsellproductblock.product_rows)}{$xprtbestsellproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtbestsellproductblock.product_scroll) && ($xprtbestsellproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			{ 
				breakpoint: 1921,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{($device_data.device_lg|intval)+1}{else}5{/if},
					slidesToScroll : {if isset($xprtbestsellproductblock.product_scroll) && ($xprtbestsellproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{($device_data.device_lg|intval)+1}{else}5{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 1441,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtbestsellproductblock.product_scroll) && ($xprtbestsellproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtbestsellproductblock.product_scroll) && ($xprtbestsellproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtbestsellproductblock.product_scroll) && ($xprtbestsellproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtbestsellproductblock.product_scroll) && ($xprtbestsellproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}