{if isset($xprtbestsellproductblock) && !empty($xprtbestsellproductblock)}
	{if isset($xprtbestsellproductblock.device)}
		{assign var=device_data value=$xprtbestsellproductblock.device|json_decode:true}
	{/if}
	<div class="xprtbestsellproductblock block carousel">
		<h4 class="title_block">
	    	<em>{$xprtbestsellproductblock.title}</em>
	    </h4>
	    <div class="block_content">
	        {if isset($xprtbestsellproductblock) && $xprtbestsellproductblock}
	        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtbestsellproductblock.products class="bestsellproduct" }
	        {else}
	        	<ul id="bestsellproduct_{$xprtbestsellproductblock.id_xprtbestsellproductblock}" class="bestsellproduct">
	        		<li class="alert alert-info">{l s='No products at this time.' mod='xprtbestsellproductblock'}</li>
	        	</ul>
	        {/if}
	    </div>
	</div>
{/if}