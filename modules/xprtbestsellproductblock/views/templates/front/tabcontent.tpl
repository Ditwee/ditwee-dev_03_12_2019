{if isset($xprtbestsellproductblock) && !empty($xprtbestsellproductblock)}
	{if isset($xprtbestsellproductblock.device)}
		{assign var=device_data value=$xprtbestsellproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtbestsellproductblock) && $xprtbestsellproductblock}
		<div id="xprt_bestproductsblock_tab_{if isset($xprtbestsellproductblock.id_xprtbestsellproductblock)}{$xprtbestsellproductblock.id_xprtbestsellproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtbestsellproductblock.products class='xprt_bestproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_bestproductsblock_tab_{if isset($xprtbestsellproductblock.id_xprtbestsellproductblock)}{$xprtbestsellproductblock.id_xprtbestsellproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No best sale products at this time.' mod='xprtbestsellproductblock'}</p>
		</div>
	{/if}
{/if}
