{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $blockCategTree && $blockCategTree.children|@count}
<!-- Block categories module -->
<div id="home_categories_list" class="block home_categories_list">
	<h4 class="title_block current">
		{if isset($currentCategory)}{$currentCategory->name|escape}{else}{l s='Categories' mod='xprtblockcategories'}{/if}
		<i class="icon-th-list f_right f_size_15 m_top_15"></i>
	</h4>
	<div class="block_content toogle_content">
		<ul class="tree dynamics">
		{foreach from=$blockCategTree.children item=child name=blockCategTree}
			{if $smarty.foreach.blockCategTree.last}
				{include file="$branche_tpl_path" node=$child last='true'}
			{else}
				{include file="$branche_tpl_path" node=$child}
			{/if}
		{/foreach}

		</ul>
		{* Javascript moved here to fix bug #PSCFI-151 *}
		{literal}
		<script type="text/javascript">
		// <![CDATA[
		$(document).ready(function(){

			// we hide the tree only if JavaScript is activated
			//$('div#home_categories_list ul.dhtml').hide();
			function responsWidth(){
				var containerWidth = $('#columns .container').width();
				//console.log(containerWidth);
				if(containerWidth == 1170 ){
					$('#home_categories_list ul.tree > li').children('ul.sub_tree').css({'width': '800px'});
				}
				else if(containerWidth == 940){
					$('#home_categories_list ul.tree > li').children('ul.sub_tree').css({'width': '700px'});
				}
				else if(containerWidth == 720){
					$('#home_categories_list ul.tree > li').children('ul.sub_tree').css({'width': '550px'});
				}
			};
			
			$(window).load(responsWidth);
			$(window).resize(responsWidth);
			//$(window).resize(responsiveResize);

			if($('ul.tree').hasClass('dynamics')){

				//add growers to each ul.tree elements
				$('ul.tree.dynamics >li > ul').prev().before("<span class='grower OPEN'> </span>");
				
				//dynamically add the '.last' class on each last item of a branch
				$('ul.tree.dynamics ul li:last-child, ul.tree li:last-child').addClass('last');
				
				//collapse every expanded branch
				//$('ul.tree span.grower.OPEN').addClass('CLOSE').removeClass('OPEN');
				//$('ul.tree.dhtml').show();
			}

			//load more
			var toShowCatList = 10;

			var totalCatList = $('#home_categories_list ul.tree').children('li').length;

			if(totalCatList > toShowCatList){

				$('#home_categories_list ul.tree').children('li:gt('+(toShowCatList - 1)+')').hide();

				$('#home_categories_list ul.tree').append('<li class="btn_more">View More Categories</li>');

				 //hiddenList = 1;
				$('#home_categories_list ul.tree').children('.btn_more').click(function(){

					 
					$('#home_categories_list ul.tree').children('li:hidden:lt('+(toShowCatList)+')').slideDown();

					var hiddenList = $('#home_categories_list ul.tree').children('li:hidden').length;

					if(hiddenList == 0){
						$('#home_categories_list ul.tree').children('li').remove('.btn_more');
					};
					
					//return hiddenList;
					
				});

			};

			
			//console.log(totalCatList);
			
		});
		// ]]>
		</script>
		{/literal}
	</div>
</div>
<!-- /Block categories module -->
{/if}
