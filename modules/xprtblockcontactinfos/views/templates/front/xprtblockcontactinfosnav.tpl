{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- MODULE Block contact infos -->
<div class="xprtblockcontactinfos contact_infos_nav f_left">
	<div class="block_content">
		<ul>
			{if $xprtblckcntctnfs_PHN != ''}
				<li>
					<i class="icon-phone"></i>
					{l s='Call us:' mod='xprtblockcontactinfos'} 
					{$xprtblckcntctnfs_PHN}
				</li>
			{/if}
			{if $xprtblckcntctnfs_EML != ''}
				<li>
					<i class="icon-envelope-o"></i>
					{l s='Email:' mod='xprtblockcontactinfos'} 
					<span>{$xprtblckcntctnfs_EML}</span>
				</li>
			{/if}
		</ul>
	</div>
</div>
<!-- /MODULE Block contact infos -->