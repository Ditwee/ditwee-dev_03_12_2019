<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_PS_VERSION_'))
	exit;
class xprtblockfacebook extends Module
{
	public function __construct()
	{
		$this->name = 'xprtblockfacebook';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Facebook Like Box block');
		$this->description = $this->l('Great Store Facebook Like Box block by Xpert-Idea');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->registerHook('DisplayFooter')
			|| !$this->registerHook('displayHeader')
			)
		return false;
			Configuration::updateValue('xprtblockfacebook_url','https://www.facebook.com/prestashop');
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall())
			return false;
		Configuration::deleteByName('xprtblockfacebook_url');
			return true;
	}
	public function getContent()
	{
		$html = '';
		if (Tools::isSubmit('submitModule'))
		{
			Configuration::updateValue('xprtblockfacebook_url', Tools::getValue('xprtblockfacebook_url'));
			$html .= $this->displayConfirmation($this->l('Configuration updated'));
			Tools::redirectAdmin('index.php?tab=AdminModules&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
		}
		$html .= $this->renderForm();
		$facebookurl = Configuration::get('xprtblockfacebook_url');
		if(!strstr($facebookurl, "facebook.com")) $facebookurl="https://www.facebook.com/".$facebookurl;
		$this->context->smarty->assign('facebookurl', $facebookurl);
		$this->context->smarty->assign('facebook_js_url', $this->_path.'xprtblockfacebook.js');
		$this->context->smarty->assign('facebook_css_url', $this->_path.'css/xprtblockfacebook.css');
		$html .= $this->context->smarty->fetch($this->local_path.'views/admin/_configure/preview.tpl');
		return $html;
	}
	public function hookDisplayHome()
	{
		$facebookurl = Configuration::get('xprtblockfacebook_url');
			if (!strstr($facebookurl, 'facebook.com'))
				$facebookurl = 'https://www.facebook.com/'.$facebookurl;
			$this->context->smarty->assign('facebookurl', $facebookurl);
		return $this->display(__FILE__, '/views/front/xprtblockfacebook.tpl');
	}
	public function hookDisplayLeftColumn()
	{
			$this->_assignMedia();
		return $this->hookDisplayHome();
	}
	public function hookDisplayRightColumn()
	{
			$this->_assignMedia();
		return $this->hookDisplayHome();
	}
	public function hookDisplayFooter()
	{
		$this->_assignMedia();
		return $this->hookDisplayHome();
	}
	public function hookHeader()
	{
		$this->page_name = Dispatcher::getInstance()->getController();
			$this->_assignMedia();
	}
	protected function _assignMedia()
	{
		$this->context->controller->addCss(($this->_path).'css/xprtblockfacebook.css');
		$this->context->controller->addJS(($this->_path).'js/xprtblockfacebook.js');
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Facebook Block Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Facebook link (full URL is required)'),
						'name' => 'xprtblockfacebook_url',
					),
				),
				'submit' => array(
					'title' => $this->l('Save')
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitModule';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		return array(
			'xprtblockfacebook_url' => Tools::getValue('xprtblockfacebook_url', Configuration::get('xprtblockfacebook_url')),
		);
	}
}