<div class="footer_logo_contact_info col-sm-3">
	{if isset($xprtlogo_img)}
	<div class="xprtblockfooterlogo block footer_logo">
		<div class="block_content">
			<div class="logo">
				<a href="{$base_dir}" title="">
					<img class="img-responsive" {if isset($xprtlogo_height)} height="{$xprtlogo_height}" {/if} {if isset($xprtlogo_width)} width="{$xprtlogo_width}" {/if} src="{$xprtlogo_img|escape:'htmlall':'UTF-8'}" alt="" title=""/>
				</a>
			</div>
			{if ($xprtlogo_desc !='')}
				<div class="logo_desc">
					{$xprtlogo_desc}
				</div>
			{/if}
		</div>
	</div>
	{/if}
	{hook h="displayFooterLogoContact"}
</div>