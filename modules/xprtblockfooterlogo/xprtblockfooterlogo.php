<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class xprtblockfooterlogo extends Module
{
	public function __construct()
	{
		$this->name = 'xprtblockfooterlogo';
		$this->tab = 'front_office_features';
		$this->version = '2.0';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store theme Footer Logo block');
		$this->description = $this->l('Displays Logo at the Footer of the shop.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->registerHook('displayFooter')
			// || !$this->registerHook('displayMaintenance')
			|| !$this->xpertsampledata()
			)
			return false;
		else
			return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
			|| !Configuration::deleteByName('xprtBLOCKFOOTERLOGO_IMG')
			|| !Configuration::deleteByName('xprtBLOCKFOOTERLOGO_DESC')
			)
			return false;
		else
			return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
        	$this->{$func}();
        }
        return true;
	}
	public function xpertsample_demo_2(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_1(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_3(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_4(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_5(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_6(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_7(){
		$this->LogoInsert("logo-w.png");
	}
	public function xpertsample_demo_8(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_9(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_10(){
		$logo = "logo-w.png";
		$languages = Language::getLanguages(false);
		$imgname = array();
		$DESC = array();
		foreach ($languages as $lang)
		{
			$imgname[$lang['id_lang']] = $logo;
			$DESC[$lang['id_lang']] = '';
		}
		Configuration::updateValue('xprtBLOCKFOOTERLOGO_IMG',$imgname);
		Configuration::updateValue('xprtBLOCKFOOTERLOGO_DESC',$DESC,true);
	}
	public function LogoInsert($logo = "logo.png")
	{
		$languages = Language::getLanguages(false);
		$imgname = array();
		$DESC = array();
		foreach ($languages as $lang)
		{
			$imgname[$lang['id_lang']] = $logo;
			$DESC[$lang['id_lang']] = '<p>Great Store is an Premium Prestashop Template which is the most perfect solution for your online shop website.</p><p>This is a custom block edited from admin panel.You can insert any content here.</p>';
		}
		Configuration::updateValue('xprtBLOCKFOOTERLOGO_IMG',$imgname);
		Configuration::updateValue('xprtBLOCKFOOTERLOGO_DESC',$DESC,true);
		return true;
	}
 //    public function xpertsampledata($demo=NULL)
	// {
		
	// }
	public function hookDisplayFooter($params)
	{
		$imgname = Configuration::get('xprtBLOCKFOOTERLOGO_IMG', $this->context->language->id);
		if($imgname && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgname)){
			$this->smarty->assign(array(
				'xprtlogo_img' => $this->context->link->protocol_content.Tools::getMediaServer($imgname).$this->_path.'img/'.$imgname,
				'xprtlogo_desc' => Configuration::get('xprtBLOCKFOOTERLOGO_DESC', $this->context->language->id)
			));
		}
		return $this->display(__FILE__, 'views/templates/front/xprtblockfooterlogo.tpl');
	}

	public function hookdisplayNav($params)
	{
		return $this->hookDisplayTop($params);
	}
	
	public function hookDisplayBanner($params)
	{
		return $this->hookDisplayTop($params);
	}

	public function hookDisplayMainMenu($params)
	{
		return $this->hookDisplayTop($params);
	}

	public function hookdisplayFooterTop($params)
	{
		return $this->hookDisplayFooter($params);
	}
	
	public function hookdisplayFooterBottom($params)
	{
		return $this->hookDisplayFooter($params);
	}
	public function hookdisplayMaintenance($params)
	{
		return $this->hookDisplayFooter($params);
	}


	public function postProcess()
	{
		if (Tools::isSubmit('submit'.$this->name))
		{
			$languages = Language::getLanguages(false);
			$values = array();
			$update_images_values = false;

			foreach ($languages as $lang)
			{
				if (isset($_FILES['xprtBLOCKFOOTERLOGO_IMG_'.$lang['id_lang']])
					&& isset($_FILES['xprtBLOCKFOOTERLOGO_IMG_'.$lang['id_lang']]['tmp_name'])
					&& !empty($_FILES['xprtBLOCKFOOTERLOGO_IMG_'.$lang['id_lang']]['tmp_name']))
				{
					if ($error = ImageManager::validateUpload($_FILES['xprtBLOCKFOOTERLOGO_IMG_'.$lang['id_lang']], 4000000))
						return $error;
					else
					{
						$ext = substr($_FILES['xprtBLOCKFOOTERLOGO_IMG_'.$lang['id_lang']]['name'], strrpos($_FILES['xprtBLOCKFOOTERLOGO_IMG_'.$lang['id_lang']]['name'], '.') + 1);
						$file_name = md5($_FILES['xprtBLOCKFOOTERLOGO_IMG_'.$lang['id_lang']]['name']).'.'.$ext;

						if (!move_uploaded_file($_FILES['xprtBLOCKFOOTERLOGO_IMG_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name))
							return $this->displayError($this->l('An error occurred while attempting to upload the file.'));
						else
						{
							if (Configuration::hasContext('xprtBLOCKFOOTERLOGO_IMG', $lang['id_lang'], Shop::getContext())
								&& Configuration::get('xprtBLOCKFOOTERLOGO_IMG', $lang['id_lang']) != $file_name)
								@unlink(dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.Configuration::get('xprtBLOCKFOOTERLOGO_IMG', $lang['id_lang']));

							$values['xprtBLOCKFOOTERLOGO_IMG'][$lang['id_lang']] = $file_name;
						}
					}

					$update_images_values = true;
				}
				$values['xprtBLOCKFOOTERLOGO_DESC'][$lang['id_lang']] = Tools::getValue('xprtBLOCKFOOTERLOGO_DESC_'.$lang['id_lang']);
			}
			if ($update_images_values)
				Configuration::updateValue('xprtBLOCKFOOTERLOGO_IMG', $values['xprtBLOCKFOOTERLOGO_IMG']);
			Configuration::updateValue('xprtBLOCKFOOTERLOGO_DESC', $values['xprtBLOCKFOOTERLOGO_DESC'], true);
			return $this->displayConfirmation($this->l('The settings have been updated.'));
		}
		return '';
	}
	public function getContent()
	{
		return $this->postProcess().$this->renderForm();
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Logo Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'file_lang',
						'label' => $this->l('Logo image'),
						'name' => 'xprtBLOCKFOOTERLOGO_IMG',
						'desc' => $this->l('Upload an Logo image for your shop.'),
						'lang' => true,
					),
					array(
						'type' => 'textarea',
						'lang' => true,
						'label' => $this->l('Logo description for footer'),
						'name' => 'xprtBLOCKFOOTERLOGO_DESC',
						'desc' => $this->l('Please enter a short description for your company.')
					)
				),
				'submit' => array(
					'title' => $this->l('Save')
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'uri' => $this->getPathUri(),
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		$languages = Language::getLanguages(false);
		$fields = array();
		foreach ($languages as $lang)
		{
			$fields['xprtBLOCKFOOTERLOGO_IMG'][$lang['id_lang']] = Tools::getValue('xprtBLOCKFOOTERLOGO_IMG_'.$lang['id_lang'], Configuration::get('xprtBLOCKFOOTERLOGO_IMG', $lang['id_lang']));
			$fields['xprtBLOCKFOOTERLOGO_DESC'][$lang['id_lang']] = Tools::getValue('xprtBLOCKFOOTERLOGO_DESC_'.$lang['id_lang'], Configuration::get('xprtBLOCKFOOTERLOGO_DESC', $lang['id_lang']));
		}
		return $fields;
	}
}
