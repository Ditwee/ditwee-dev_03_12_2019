<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
class xprtblockfootertext extends Module
{
	public function __construct()
	{
		$this->name = 'xprtblockfootertext';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Footer Text Block');
		$this->description = $this->l('Great Store Theme Theme Footer Text Block.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->registerHook('displayFooterBottom')
			|| !$this->xpertsampledata())
			return false;
			Configuration::updateValue('xprtfootercopycolumn','4');
			Configuration::updateValue('xprtfootercopyfloat','disable');
			$langs = Language::getLanguages();
			foreach($langs as $l)
			{
				Configuration::updateValue('xprtblockfootertext_'.$l['id_lang'],'Copyright @ 2017 <a href="#">GREAT STORE</a>. All rights reserved.', true);
			}
			return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
        	$this->{$func}();
        }
        return true;
	}
	public function xpertsample_demo_10()
	{
		Configuration::updateValue('xprtfootercopycolumn','12');
		Configuration::updateValue('xprtfootercopyfloat','disable');
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue('xprtblockfootertext_'.$l['id_lang'],'Copyright @ 2017 <a href="#">GREAT STORE</a>. All rights reserved.', true);
		}
	}
	public function uninstall()
	{
		if(!parent::uninstall()
			|| !Configuration::deleteByName('xprtfootercopycolumn')
			|| !Configuration::deleteByName('xprtfootercopyfloat')
			)
			return false;
			$langs = Language::getLanguages();
			foreach($langs as $l)
			{
					Configuration::deleteByName('xprtblockfootertext_'.$l['id_lang']);
			}
			return true;
	}
	public function hookDisplayFooter($params)
	{
		$id_lang = (int)$this->context->language->id;
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$xprtblockfootertext = Configuration::get('xprtblockfootertext_'.$id_lang);

		if(empty($xprtblockfootertext)){
			$xprtblockfootertext = Configuration::get('xprtblockfootertext_'.$default_lang);
		}

		$this->smarty->assign(array(
				'xprtblockfootertext' => $xprtblockfootertext,
				'xprtfootercopycolumn' => Configuration::get('xprtfootercopycolumn'),
				'xprtfootercopyfloat' => Configuration::get('xprtfootercopyfloat'),
			));
		return $this->display(__FILE__, 'views/templates/front/xprtblockfootertext.tpl');
	}
	public function hookDisplayFooterTop($params)
	{
		return $this->hookDisplayFooter($params);
	}
	public function hookDisplayFooterBottom($params)
	{
		return $this->hookDisplayFooter($params);
	}
	public function postProcess()
	{
		if (Tools::isSubmit('submit'.$this->name))
		{
			$langs = Language::getLanguages();
			foreach($langs as $l)
			{
				Configuration::updateValue('xprtblockfootertext_'.$l['id_lang'],Tools::getValue('xprtblockfootertext_'.$l['id_lang']), true);
			}
			Configuration::updateValue('xprtfootercopycolumn',Tools::getValue('xprtfootercopycolumn'), true);
			Configuration::updateValue('xprtfootercopyfloat',Tools::getValue('xprtfootercopyfloat'), true);
			return $this->displayConfirmation($this->l('The settings have been updated.'));
		}
		return '';
	}
	public function getContent()
	{
		return $this->postProcess().$this->renderForm();
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'tinymce' => true,
				'legend' => array(
					'title' => $this->l('Footer Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'textarea',
						'lang' => true,
						'label' => $this->l('Description'),
						'name' => 'xprtblockfootertext',
						'desc' => $this->l('Please enter a short but meaningful description for the footer.'),
						'cols' => 40,
						'rows' => 10,
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Select column'),
					    'name' => 'xprtfootercopycolumn',
					    'default_val' => '4',
					    'desc' => $this->l('Choose colum for this block'),
					    'options' => array(
					    	'id' => 'id',
					    	'name' => 'name',
					    	'query' => array(
					    		array(
					    			'id' => '6',
					    			'name' => 'Column two (col-sm-6)'
					    			),
					    		array(
					    			'id' => '4',
					    			'name' => 'Column three (col-sm-4)'
					    			),
					    		array(
					    			'id' => '3',
					    			'name' => 'Column four (col-sm-3)'
					    			),
					    		array(
					    			'id' => '12',
					    			'name' => 'Column full (col-sm-12)'
					    			),
					    		array(
					    			'id' => 'none',
					    			'name' => 'None'
					    			),
					    		)
					    	)
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Float Align'),
					    'name' => 'xprtfootercopyfloat',
					    'default_val' => 'disable',
					    'desc' => $this->l('Choose colum for this block'),
					    'options' => array(
					    	'id' => 'id',
					    	'name' => 'name',
					    	'query' => array(
					    		array(
					    			'id' => 'disable',
					    			'name' => 'Disable'
					    			),
					    		array(
					    			'id' => 'f_left',
					    			'name' => 'Float Left'
					    			),
					    		array(
					    			'id' => 'f_right',
					    			'name' => 'Float Right'
					    			),
					    		array(
					    			'id' => 'f_none',
					    			'name' => 'Float none'
					    			),
					    		)
					    	)
					),
				),
				'submit' => array(
					'title' => $this->l('Save')
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'uri' => $this->getPathUri(),
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		$fields = array();
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			$fields['xprtblockfootertext'][$l['id_lang']] = Tools::getValue('xprtblockfootertext_'.$l['id_lang'], Configuration::get('xprtblockfootertext_'.$l['id_lang']));
		}
		$fields['xprtfootercopycolumn'] = Tools::getValue('xprtfootercopycolumn', Configuration::get('xprtfootercopycolumn'));
		$fields['xprtfootercopyfloat'] = Tools::getValue('xprtfootercopyfloat', Configuration::get('xprtfootercopyfloat'));
		return $fields;
	}
}
