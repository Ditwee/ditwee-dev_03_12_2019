<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/xprtblocklayered.php');

if (substr(Tools::encrypt('xprtblocklayered/index'),0,10) != Tools::getValue('token') || !Module::isInstalled('xprtblocklayered'))
	die('Bad token');

$xprtblocklayered = new xprtblocklayered();
$cursor = Tools::jsonDecode(Tools::getValue('cursor', '{}'), true);
echo $xprtblocklayered->indexUrl($cursor, (int)Tools::getValue('truncate'));