{if (isset($xprtlinkdemo_mode) && ($xprtlinkdemo_mode == "1"))}
	<!-- Block links module -->
	<div id="links_block_left" class="xprtblocklink block footer_custom_link col-sm-3">
		<h4 class="title_block">
			<a href="#" title="{l s='Our Offer' mod='xprtblocklink'}">{l s='Our Offer' mod='xprtblocklink'}</a>
		</h4>
		<div class="block_content list-block">
			<ul class="clearfix">
				<li> <a href="#" title="{l s='Envato' mod='xprtblocklink'}">{l s='Our Offer' mod='Envato'}</a></li>
				<li> <a href="#" title="{l s='Returns' mod='xprtblocklink'}" onclick="window.open(this.href);return false;">{l s='Returns' mod='xprtblocklink'}</a></li>
				<li> <a href="#" title="{l s='Privacy Policy' mod='xprtblocklink'}" onclick="window.open(this.href);return false;">{l s='Privacy Policy' mod='xprtblocklink'}</a></li>
				<li> <a href="#" title="{l s='Amazone' mod='xprtblocklink'}" onclick="window.open(this.href);return false;">{l s='Amazone' mod='xprtblocklink'}</a></li>
				<li> <a href="#" title="{l s='E-bay' mod='xprtblocklink'}" onclick="window.open(this.href);return false;">{l s='E-bay' mod='xprtblocklink'}</a></li>
				<li> <a href="#" title="{l s='Wallmart' mod='xprtblocklink'}" onclick="window.open(this.href);return false;">{l s='Wallmart' mod='xprtblocklink'}</a></li>
			</ul>
		</div>
	 </div>
	<!-- /Block links module -->
{else}
<!-- Block links module -->
<div id="links_block_left" class="xprtblocklink block footer_custom_link col-sm-3">
	<h4 class="title_block">
	{if $url}
		<a href="{$url|escape}" title="{$title|escape}">{$title|escape}</a>
	{else}
		{$title|escape}
	{/if}
	</h4>
	<div class="block_content list-block">
		<ul class="clearfix">
		{foreach from=$xprtblocklink_links item=xprtblocklink_link}
			{if isset($xprtblocklink_link.$lang)} 
				<li>
					<a href="{$xprtblocklink_link.url|escape}" title="{$xprtblocklink_link.$lang|escape}" {if $xprtblocklink_link.newWindow} onclick="window.open(this.href);return false;"{/if}>{$xprtblocklink_link.$lang|escape}</a></li>
			{/if}
		{/foreach}
		</ul>
	</div>
</div>
<!-- /Block links module -->
{/if}
