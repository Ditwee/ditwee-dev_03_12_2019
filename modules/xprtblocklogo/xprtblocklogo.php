<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class xprtblocklogo extends Module
{
	public function __construct()
	{
		$this->name = 'xprtblocklogo';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Great Store Theme Logo block');
		$this->description = $this->l('Displays Logo at the top of the shop.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		if(!parent::install()
			|| !$this->registerHook('displayTopRightOne')
			|| !$this->registerHook('displayNav')
			|| !$this->registerHook('displayTopLeft')
			|| !$this->xpertsampledata()
		)
			return false;
		return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
        	$this->{$func}();
        }
        return true;
	}
	public function xpertsample_demo_2(){
		$this->LogoInsert("logo-b.png");
	}
	public function xpertsample_demo_1(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_3(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_4(){
		$this->LogoInsert("logo-b.png");
	}
	public function xpertsample_demo_5(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_6(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_7(){
		$this->LogoInsert("logo-b.png");
	}
	public function xpertsample_demo_8(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_9(){
		$this->LogoInsert("logo.png");
	}
	public function xpertsample_demo_10(){
		$this->LogoInsert("logo.png");
	}
	public function LogoInsert($logo = "logo.png")
	{
		$languages = Language::getLanguages(false);
		$imgname = array();
		$DESC = array();
		foreach ($languages as $lang)
		{
			$imgname[$lang['id_lang']] = $logo;
			$DESC[$lang['id_lang']] = 'Great Store is an Premium Prestashop Template which is the most perfect solution for your online shop website.';
		}
		Configuration::updateValue('xprtBLOCKLOGO_IMG',$imgname);
		Configuration::updateValue('xprtBLOCKLOGO_DESC',$DESC);
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
			|| !Configuration::deleteByName('xprtBLOCKLOGO_IMG')
			|| !Configuration::deleteByName('xprtBLOCKLOGO_Height')
			|| !Configuration::deleteByName('xprtBLOCKLOGO_Width')
			|| !Configuration::deleteByName('xprtBLOCKLOGO_DESC')
			)
			return false;
		else
			return true;
	}
	public function hookDisplayTop($params)
	{
		// $this->xpertsample_demo_1();
		$imgname = Configuration::get('xprtBLOCKLOGO_IMG', $this->context->language->id);
		if($imgname && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgname)){
			$this->smarty->assign(array(
				'xprtlogo_img' => $this->context->link->protocol_content.Tools::getMediaServer($imgname).$this->_path.'img/'.$imgname,
				'xprtlogo_desc' => Configuration::get('xprtBLOCKLOGO_DESC', $this->context->language->id),
				'xprtlogo_height' => Configuration::get('xprtBLOCKLOGO_Height'),
				'xprtlogo_width' => Configuration::get('xprtBLOCKLOGO_Width'),
			));
		}
		return $this->display(__FILE__, 'views/templates/front/xprtblocklogotop.tpl');
	}
	public function hookDisplayFooter($params)
	{
		$imgname = Configuration::get('xprtBLOCKLOGO_IMG', $this->context->language->id);
		if($imgname && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$imgname)){
			$this->smarty->assign(array(
				'xprtlogo_img' => $this->context->link->protocol_content.Tools::getMediaServer($imgname).$this->_path.'img/'.$imgname,
				'xprtlogo_desc' => Configuration::get('xprtBLOCKLOGO_DESC', $this->context->language->id),
				'xprtlogo_height' => Configuration::get('xprtBLOCKLOGO_Height'),
				'xprtlogo_width' => Configuration::get('xprtBLOCKLOGO_Width'),
			));
		}
		return $this->display(__FILE__, 'views/templates/front/xprtblocklogofooter.tpl');
	}

	public function hookdisplayNav($params)
	{
		return $this->hookDisplayTop($params);
	}
	
	public function hookDisplayBanner($params)
	{
		return $this->hookDisplayTop($params);
	}

	public function hookDisplayMainMenu($params)
	{
		return $this->hookDisplayTop($params);
	}
	
	public function hookdisplaySidebarPanel($params)
	{
		return $this->hookDisplayTop($params);
	}
	
	public function hookdisplayTopLeft($params)
	{
		return $this->hookDisplayTop($params);
	}
	
	public function hookdisplayTopRightOne($params)
	{
		return $this->hookDisplayTop($params);
	}
	
	public function hookdisplayTopRightTwo($params)
	{
		return $this->hookDisplayTop($params);
	}

	public function hookdisplayFooterTop($params)
	{
		return $this->hookDisplayFooter($params);
	}
	
	public function hookdisplayFooterBottom($params)
	{
		return $this->hookDisplayFooter($params);
	}
	
	public function hookdisplayMaintenance($params)
	{
		return $this->hookDisplayFooter($params);
	}
	
	public function postProcess()
	{
		if (Tools::isSubmit('submit'.$this->name))
		{
			$languages = Language::getLanguages(false);
			$values = array();
			$update_images_values = false;
			foreach ($languages as $lang)
			{
				if (isset($_FILES['xprtBLOCKLOGO_IMG_'.$lang['id_lang']])
					&& isset($_FILES['xprtBLOCKLOGO_IMG_'.$lang['id_lang']]['tmp_name'])
					&& !empty($_FILES['xprtBLOCKLOGO_IMG_'.$lang['id_lang']]['tmp_name']))
				{
					if ($error = ImageManager::validateUpload($_FILES['xprtBLOCKLOGO_IMG_'.$lang['id_lang']], 4000000))
						return $error;
					else
					{
						$ext = substr($_FILES['xprtBLOCKLOGO_IMG_'.$lang['id_lang']]['name'], strrpos($_FILES['xprtBLOCKLOGO_IMG_'.$lang['id_lang']]['name'], '.') + 1);
						$file_name = Tools::link_rewrite($_FILES['xprtBLOCKLOGO_IMG_'.$lang['id_lang']]['name']).'.'.$ext;

						if (!move_uploaded_file($_FILES['xprtBLOCKLOGO_IMG_'.$lang['id_lang']]['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$file_name))
							return $this->displayError($this->l('An error occurred while attempting to upload the file.'));
						else
						{
							if (Configuration::hasContext('xprtBLOCKLOGO_IMG', $lang['id_lang'], Shop::getContext())
								&& Configuration::get('xprtBLOCKLOGO_IMG', $lang['id_lang']) != $file_name)
								@unlink(dirname(__FILE__).DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.Configuration::get('xprtBLOCKLOGO_IMG', $lang['id_lang']));
							$values['xprtBLOCKLOGO_IMG'][$lang['id_lang']] = $file_name;
						}
					}

					$update_images_values = true;
				}
				$values['xprtBLOCKLOGO_DESC'][$lang['id_lang']] = Tools::getValue('xprtBLOCKLOGO_DESC_'.$lang['id_lang']);
				$values['xprtBLOCKLOGO_Height'] = Tools::getValue('xprtBLOCKLOGO_Height');
				$values['xprtBLOCKLOGO_Width'] = Tools::getValue('xprtBLOCKLOGO_Width');
			}
			if ($update_images_values)
				Configuration::updateValue('xprtBLOCKLOGO_IMG', $values['xprtBLOCKLOGO_IMG']);
			Configuration::updateValue('xprtBLOCKLOGO_DESC', $values['xprtBLOCKLOGO_DESC']);
			Configuration::updateValue('xprtBLOCKLOGO_Height', $values['xprtBLOCKLOGO_Height']);
			Configuration::updateValue('xprtBLOCKLOGO_Width', $values['xprtBLOCKLOGO_Width']);
			return $this->displayConfirmation($this->l('The settings have been updated.'));
		}
		return '';
	}
	public function getContent()
	{
		return $this->postProcess().$this->renderForm();
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Logo Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'file_lang',
						'label' => $this->l('Logo image'),
						'name' => 'xprtBLOCKLOGO_IMG',
						'desc' => $this->l('Upload an Logo image for your shop. Dimention (173x69)'),
						'lang' => true,
					),
					array(
						'type' => 'text',
						'label' => $this->l('Logo Height'),
						'name' => 'xprtBLOCKLOGO_Height',
						'class' => 'fixed-width-md',
						'suffix' => 'pixels',
						'desc' => $this->l('Please enter a Logo Image Height value : 150.')
					),
					array(
						'type' => 'text',
						'label' => $this->l('Logo Width'),
						'name' => 'xprtBLOCKLOGO_Width',
						'class' => 'fixed-width-md',
						'suffix' => 'pixels',
						'desc' => $this->l('Please enter a Logo Image Width value : 150.')
					)
				),
				'submit' => array(
					'title' => $this->l('Save')
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'uri' => $this->getPathUri(),
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		$languages = Language::getLanguages(false);
		$fields = array();
		foreach ($languages as $lang)
		{
			$fields['xprtBLOCKLOGO_IMG'][$lang['id_lang']] = Tools::getValue('xprtBLOCKLOGO_IMG_'.$lang['id_lang'], Configuration::get('xprtBLOCKLOGO_IMG', $lang['id_lang']));
			$fields['xprtBLOCKLOGO_DESC'][$lang['id_lang']] = Tools::getValue('xprtBLOCKLOGO_DESC_'.$lang['id_lang'], Configuration::get('xprtBLOCKLOGO_DESC', $lang['id_lang']));
		}
		$fields['xprtBLOCKLOGO_Height'] = Tools::getValue('xprtBLOCKLOGO_Height',Configuration::get('xprtBLOCKLOGO_Height'));
		$fields['xprtBLOCKLOGO_Width'] = Tools::getValue('xprtBLOCKLOGO_Width',Configuration::get('xprtBLOCKLOGO_Width'));
		return $fields;
	}
}
