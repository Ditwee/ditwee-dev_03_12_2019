<!-- Block Newsletter module-->
<div class="xprtblocknewsletter newsletter_block_area xprt_parallax_section">
	<div id="newsletter_block_left" class="newsletter_block block">
		<div class="block_title">
			<h4>{$xprt.newsletter_title}</h4>
			<div class="heading-line"><span></span></div>
			<p>{$xprt.newsletter_text}</p>
		</div>
		<div class="block_content">
			<form action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}" method="post">
				<div class="form-group{if isset($msg) && $msg } {if $nw_error}form-error{else}form-ok{/if}{/if}" >
					<input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" value="{if isset($msg) && $msg}{$msg}{elseif isset($value) && $value}{$value}{else}{l s='Enter your e-mail' mod='xprtblocknewsletter'}{/if}" />
	                <button type="submit" name="submitNewsletter" class="btn btn-default button button-small btn-inverse">
	                    <span>{l s='Subscribe' mod='xprtblocknewsletter'}</span>
	                </button>
					<input type="hidden" name="action" value="0" />
				</div>
			</form>
		</div>
	</div>
	{hook h="displayxprtblocknewsletterBottom" from='xprtblocknewsletter'}
</div>
<!-- /Block Newsletter module-->
{strip}
{if isset($msg) && $msg}
{addJsDef msg_newsl=$msg|@addcslashes:'\''}
{/if}
{if isset($nw_error)}
{addJsDef nw_error=$nw_error}
{/if}
{addJsDefL name=placeholder_xprtblocknewsletter}{l s='Enter your e-mail' mod='xprtblocknewsletter' js=1}{/addJsDefL}
{if isset($msg) && $msg}
	{addJsDefL name=alert_xprtblocknewsletter}{l s='Newsletter : %1$s' sprintf=$msg js=1 mod="xprtblocknewsletter"}{/addJsDefL}
{/if}
{/strip}