<div class="xprtblockpaymenticon block footer_payment_block {if isset($xprtblockpayment_column)}col-sm-{$xprtblockpayment_column}{/if} {$xprtblockpayment_float}">
	<div class="footer_payment_logo clearfix">
		<ul>
			{if $xprt_PAYPAL_URL != ''}
				<li><a href="{$xprt_PAYPAL_URL}" target="_blank">
					<img src="{$pi_path}paypal.png" alt="paypal" /></a>
				</li>
			{/if}
			{if $xprt_VISA_URL != ''}
				<li><a href="{$xprt_VISA_URL}" target="_blank">
					<img src="{$pi_path}visa.png" alt="visa" /></a>
				</li>
			{/if}
			{if $xprt_DISCOVER_URL != ''}
				<li><a href="{$xprt_DISCOVER_URL}" target="_blank">
					<img src="{$pi_path}discover.png" alt="discover" /></a>
				</li>
			{/if}
			{if $xprt_MASTERCART_URL != ''}
				<li><a href="{$xprt_MASTERCART_URL}" target="_blank">
					<img src="{$pi_path}mastercard.png" alt="mastercard" /></a>
				</li>
			{/if}
			{if $xprt_AMERICANEXPRESS_URL != ''}
				<li><a href="{$xprt_AMERICANEXPRESS_URL}" target="_blank">
					<img src="{$pi_path}american-express.png" alt="american-express" /></a>
				</li>
			{/if}
			{if $xprt_MAESTRO_URL != ''}
				<li><a href="{$xprt_MAESTRO_URL}" target="_blank">
					<img src="{$pi_path}maestro.png" alt="maestro" /></a>
				</li>
			{/if}
			{if $xprt_VISAELECTRON_URL != ''}
				<li><a href="{$xprt_VISAELECTRON_URL}" target="_blank">
					<img src="{$pi_path}visa-electron.png" alt="visa-electron" /></a>
				</li>
			{/if}
			{if $xprt_CIRRUS_URL != ''}
				<li><a href="{$xprt_CIRRUS_URL}" target="_blank">
					<img src="{$pi_path}cirrus.png" alt="cirrus" /></a>
				</li>
			{/if}
		</ul>
	</div>
</div>