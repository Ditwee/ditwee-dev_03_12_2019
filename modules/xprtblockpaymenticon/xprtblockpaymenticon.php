<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
class xprtblockpaymenticon extends Module
{
	public $PI_fields_name;
	public $hooks_url = '/hooks/hooks.php';
	public $css_files_url = '/css/css.php';
	public $js_files_url = '/js/js.php';
	public $tabs_files_url = '/tabs/tabs.php';
	public $mysql_files_url = '/mysqlquery/mysqlquery.php';
	public $data_url = '/data/data.php';
	public $icon_url = '/icon/icon.php';
	public function __construct()
	{
		$this->name = 'xprtblockpaymenticon';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		$this->PI_fields_name = array(
				'xprt_PAYPAL_URL',
				'xprt_VISA_URL',
				'xprt_DISCOVER_URL',
				'xprt_MASTERCART_URL',
				'xprt_AMERICANEXPRESS_URL',
				'xprt_MAESTRO_URL',
				'xprt_VISAELECTRON_URL',
				'xprt_CIRRUS_URL',
				'xprtblockpayment_column',
				'xprtblockpayment_float');
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Payment Icon');
		$this->description = $this->l('Great Store Theme Payment Icon Show in your Shop');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
		 || !$this->Register_SQL()
		 || !$this->Register_Config()
		 || !$this->Register_Hooks()
		 || !$this->xpertsampledata()
		)
			return false;
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
		 || !$this->UnRegister_Hooks()
		 || !$this->UnRegister_Config()
		 || !$this->UnRegister_SQL()
		 //|| !$this->UnRegister_Tabs()
		)
			return false;
		return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
        	$this->{$func}();
        }
        return true;
	}
	public function xpertsample_demo_2()
	{
		Configuration::updateValue("xprt_PAYPAL_URL","http://www.paypal.com");
		Configuration::updateValue("xprt_VISA_URL","http://www.visa.com");
		Configuration::updateValue("xprt_DISCOVER_URL","http://www.discover.com");
		Configuration::updateValue("xprt_MASTERCART_URL","http://www.mastercard.com");
		Configuration::updateValue("xprtblockpayment_column","4");
		Configuration::updateValue("xprtblockpayment_float","disable");
		return true;
	}
	public function xpertsample_demo_1()
	{
		Configuration::updateValue("xprt_PAYPAL_URL","http://www.paypal.com");
		Configuration::updateValue("xprt_VISA_URL","http://www.visa.com");
		Configuration::updateValue("xprt_DISCOVER_URL","http://www.discover.com");
		Configuration::updateValue("xprt_MASTERCART_URL","http://www.mastercard.com");
		Configuration::updateValue("xprtblockpayment_column","6");
		Configuration::updateValue("xprtblockpayment_float","f_right");
		return true;
	}
	public function xpertsample_demo_3()
	{
		Configuration::updateValue("xprt_PAYPAL_URL","http://www.paypal.com");
		Configuration::updateValue("xprt_VISA_URL","http://www.visa.com");
		Configuration::updateValue("xprt_DISCOVER_URL","http://www.discover.com");
		Configuration::updateValue("xprt_MASTERCART_URL","http://www.mastercard.com");
		Configuration::updateValue("xprtblockpayment_column","6");
		Configuration::updateValue("xprtblockpayment_float","f_right");
		return true;
	}
	public function xpertsample_demo_4()
	{
		Configuration::updateValue("xprt_PAYPAL_URL","http://www.paypal.com");
		Configuration::updateValue("xprt_VISA_URL","http://www.visa.com");
		Configuration::updateValue("xprt_DISCOVER_URL","http://www.discover.com");
		Configuration::updateValue("xprt_MASTERCART_URL","http://www.mastercard.com");
		Configuration::updateValue("xprtblockpayment_column","");
		Configuration::updateValue("xprtblockpayment_float","");
		return true;
	}
	public function xpertsample_demo_5()
	{
		Configuration::updateValue("xprt_PAYPAL_URL","http://www.paypal.com");
		Configuration::updateValue("xprt_VISA_URL","http://www.visa.com");
		Configuration::updateValue("xprt_DISCOVER_URL","http://www.discover.com");
		Configuration::updateValue("xprt_MASTERCART_URL","http://www.mastercard.com");
		Configuration::updateValue("xprtblockpayment_column","6");
		Configuration::updateValue("xprtblockpayment_float","f_right");
		return true;
	}
	public function xpertsample_demo_6()
	{
		Configuration::updateValue("xprt_PAYPAL_URL","http://www.paypal.com");
		Configuration::updateValue("xprt_VISA_URL","http://www.visa.com");
		Configuration::updateValue("xprt_DISCOVER_URL","http://www.discover.com");
		Configuration::updateValue("xprt_MASTERCART_URL","http://www.mastercard.com");
		Configuration::updateValue("xprtblockpayment_column","");
		Configuration::updateValue("xprtblockpayment_float","");
		return true;
	}
	public function xpertsample_demo_7()
	{
		Configuration::updateValue("xprt_PAYPAL_URL","http://www.paypal.com");
		Configuration::updateValue("xprt_VISA_URL","http://www.visa.com");
		Configuration::updateValue("xprt_DISCOVER_URL","http://www.discover.com");
		Configuration::updateValue("xprt_MASTERCART_URL","http://www.mastercard.com");
		Configuration::updateValue("xprtblockpayment_column","none");
		Configuration::updateValue("xprtblockpayment_float","disable");
		return true;
	}
	public function Register_Config()
	{
			$data = array();
	        require_once(dirname(__FILE__).$this->data_url);
        if(isset($data) && !empty($data))
        	foreach($data as $data_key => $data_val):
        		Configuration::updateValue($data_key,$data_val);
        	endforeach;
        	return true;
        return false;
	}
	public function UnRegister_Config()
	{
			$data = array();
	        require_once(dirname(__FILE__).$this->data_url);
        if(isset($data) && !empty($data))
        	foreach($data as $data_key => $data_val):
        		Configuration::deleteByName($data_key);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
        		$this->registerHook($hook);
        	endforeach;
        	return true;
        return false;
	}
	public function UnRegister_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
	        		$hook_id = Module::getModuleIdByName($hook);
	        	if(isset($hook_id) && !empty($hook_id))
	        		$this->unregisterHook((int)$hook_id);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Css()
	{
		$css_files = array();
        require_once(dirname(__FILE__).$this->css_files_url);
        if(isset($css_files) && !empty($css_files))
        	foreach($css_files as $css_file):
        		$this->context->controller->addCSS($css_file);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Js()
	{
		$js_files = array();
        require_once(dirname(__FILE__).$this->js_files_url);
        if(isset($js_files) && !empty($js_files))
        	foreach($js_files as $js_file):
        		$this->context->controller->addJS($js_file);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_SQL()
	{
		$mysqlquery = array();
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($mysqlquery) && !empty($mysqlquery))
				foreach($mysqlquery as $query){
					if(!Db::getInstance()->Execute($query))
					    return false;
				}
        return true;
	}
	public function UnRegister_SQL()
	{
		$mysqlquery_u = array();
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($mysqlquery_u) && !empty($mysqlquery_u))
				foreach($mysqlquery_u as $query_u){
					if(!Db::getInstance()->Execute($query_u))
					    return false;
				}
        return true;
	}
	public function getContent()
	{
		if(Tools::isSubmit('submit'.$this->name))
		{
			$arr_val = array();
			foreach($this->PI_fields_name as $PI_fields_name)
			{
				Configuration::updateValue($PI_fields_name, Tools::getValue($PI_fields_name));
			}
		}
		return $this->renderForm();
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('xprt Payment Icon Settings'),
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('PayPal URL'),
						'name' => 'xprt_PAYPAL_URL',
						'desc' => $this->l('Enter PayPal URL.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('VISA URL'),
						'name' =>  'xprt_VISA_URL',
						'desc' => $this->l('Enter VISA URL.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('DISCOVER URL'),
						'name' =>  'xprt_DISCOVER_URL',
						'desc' => $this->l('Enter DISCOVER URL.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('MASTERCART URL'),
						'name' =>  'xprt_MASTERCART_URL',
						'desc' => $this->l('Enter MASTERCART URL.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('AMERICAN EXPRESS URL'),
						'name' =>  'xprt_AMERICANEXPRESS_URL',
						'desc' => $this->l('Enter AMERICAN EXPRESS URL.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('MAESTRO URL'),
						'name' =>  'xprt_MAESTRO_URL',
						'desc' => $this->l('Enter MAESTRO URL.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('VISAELECTRON URL'),
						'name' =>  'xprt_VISAELECTRON_URL',
						'desc' => $this->l('Enter VISAELECTRON URL.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('CIRRUS URL'),
						'name' =>  'xprt_CIRRUS_URL',
						'desc' => $this->l('Enter CIRRUS URL.'),
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Select column'),
					    'name' => 'xprtblockpayment_column',
					    'default_val' => '4',
					    'desc' => $this->l('Choose colum for this block'),
					    'options' => array(
					    	'id' => 'id',
					    	'name' => 'name',
					    	'query' => array(
					    		array(
					    			'id' => '6',
					    			'name' => 'Column two (col-sm-6)'
					    			),
					    		array(
					    			'id' => '4',
					    			'name' => 'Column three (col-sm-4)'
					    			),
					    		array(
					    			'id' => '3',
					    			'name' => 'Column four (col-sm-3)'
					    			),
					    		array(
					    			'id' => '12',
					    			'name' => 'Column full (col-sm-12)'
					    			),
					    		array(
					    			'id' => 'none',
					    			'name' => 'None'
					    			),
					    		)
					    	)
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Float Align'),
					    'name' => 'xprtblockpayment_float',
					    'default_val' => 'disable',
					    'desc' => $this->l('Choose colum for this block'),
					    'options' => array(
					    	'id' => 'id',
					    	'name' => 'name',
					    	'query' => array(
					    		array(
					    			'id' => 'disable',
					    			'name' => 'Float Disable'
					    			),
					    		array(
					    			'id' => 'f_left',
					    			'name' => 'Float Left'
					    			),
					    		array(
					    			'id' => 'f_right',
					    			'name' => 'Float Right'
					    			),
					    		array(
					    			'id' => 'f_none',
					    			'name' => 'Float none'
					    			),
					    		)
					    	)
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		$arr_val = array();
		foreach($this->PI_fields_name as $PI_fields_name){
			$arr_val[$PI_fields_name] = Tools::getValue($PI_fields_name, Configuration::get($PI_fields_name));
		}
		return $arr_val;
	}
	public function hookDisplayFooter($params)
	{
		$arr_val = array();
		foreach($this->PI_fields_name as $PI_fields_name)
		{
			$arr_val[$PI_fields_name] = Configuration::get($PI_fields_name);
		}
		$arr_val['pi_path'] = $this->_path.'img/';
		$this->smarty->assign($arr_val);
		return $this->display(__FILE__, 'views/templates/front/xprtblockpaymenticon.tpl');
	}
	public function hookdisplayFooterBottom($params){
		return $this->hookDisplayFooter($params);
	}
	public function hookdisplayFooterLogoContact($params){
		return $this->hookDisplayFooter($params);
	}
}
