<?php

if (!defined('_PS_VERSION_'))
	exit;
function upgrade_module_2_0_1($object)
{
	Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'xprtblckpopuptbl` ADD dontshow longtext DEFAULT NULL');
	return true;
}