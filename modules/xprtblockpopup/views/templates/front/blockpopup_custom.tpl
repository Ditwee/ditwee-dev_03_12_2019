<!-- newsletter popup -->
<div class="blockpopup_custom">
	<div class="blockpopup_custom_content">
		<p class="sub_title">{$result.subtitle}</p>
		<h4 class="title">{$result.title}</h4>
		<p class="content">{$result.description}</p>
		<div class="newsletter_popup_bottom"> 
			<input type="checkbox" id="newsletter_popup_dont_show_again">
			<label for="newsletter_popup_dont_show_again">{l s='Don\'t show this popup again' mod='xprtblockpopup'}</label>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function($){

		$(".newsletter_popup_bottom input:checkbox").on("click",function(){

			var data = {
				'action_type':'dontshow',
				'id_newsletter':{$result.id_xprtblckpopuptbl}
			};
			$.ajax({
				url: baseDir + 'modules/xprtblockpopup/ajax.php',
				data: data,
				dataType: 'json',
				success: function(result){
		        	$(".fancybox-close").trigger("click");
				}
			});
		});
	});
</script>