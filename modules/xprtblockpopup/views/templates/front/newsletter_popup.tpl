<!-- newsletter popup -->
<div class="newsletter_popup">
	<div class="newsletter_popup_content">
		<h4 class="title">{$result.title}</h4>
		<p class="sub_title">{$result.subtitle}</p>
		<p class="content">{$result.description}</p>
		<form action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}" method="post" id="form-ns-{$result.id_xprtblckpopuptbl}">
			<div class="form-group" >

				<input class="inputNew form-control grey newsletter-input" id="newsletter-input-{$result.id_xprtblckpopuptbl}" required type="email" name="email" size="18" placeholder="{l s='Your Email' mod='xprtblockpopup'}"/>
                <button type="submit" name="submitNewsletter" id="submitNewsletter-{$result.id_xprtblckpopuptbl}" class="btn btn-default button button-small">

                    <span>{l s='Subscribe' mod='xprtblockpopup'}</span>
                </button>
				<input type="hidden" name="action" value="0" />
			</div>
		</form>
		<div class="newsletter_popup_bottom"> 
			<input type="checkbox" id="newsletter_popup_dont_show_again">
			<label for="newsletter_popup_dont_show_again">{l s='Don\'t show this popup again' mod='xprtblockpopup'}</label>
		</div>
	</div>
</div>


<script type="text/javascript">

	$(document).ready(function(){
		$(".newsletter_popup_bottom input:checkbox").on("change",function(){
		

			var data = {
				'action_type':'dontshow',
				'id_newsletter':{$result.id_xprtblckpopuptbl}
			};
			$.ajax({
				url: baseDir + 'modules/xprtblockpopup/ajax.php',
				data: data,
				type: 'post',
				dataType: 'json',
				success: function(result){
		        	$(".fancybox-close").trigger("click");
				}
			});
		});
		$(".newsletter_popup #submitNewsletter-{$result.id_xprtblckpopuptbl}").on("click",function(e){
			e.preventDefault();
			var form = $(this).closest("form");
			
			if( !form[0].checkValidity() ){
				form[0].reportValidity();
				return;
			}

			var data = {
				'action_type':'submit_newsletter',
				'action': $("[name=action]").val(),
				'email': $("#newsletter-input-{$result.id_xprtblckpopuptbl}").val(),
				'id_newsletter':{$result.id_xprtblckpopuptbl}
			};
			$.ajax({
				url: baseDir + 'modules/xprtblockpopup/ajax.php',
				data: data,
				type: 'post',
				dataType: 'json',
				success: function(result){
		        	$(".fancybox-close").trigger("click");
		        	$.fancybox(result.msg,{
        	            minWidth: 'auto',
        	            minHeight: 'auto'
        	        });
				}
			});
		});
	});
</script>