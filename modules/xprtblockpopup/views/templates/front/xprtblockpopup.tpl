{if !empty($results)}
	{foreach from=$results item=result}
		<div id="xprtblockpopup_{$result.id_xprtblckpopuptbl}" class="xprtblockpopup d_none" style="background-image:url({$modules_dir}xprtblockpopup/img/{$result.image})">
			<div class="xprtblockpopup_content">
		
			
				{if $result.popuptype == 'newsletter'}
					{include file="./newsletter_popup.tpl"}
				{/if}
				{if $result.popuptype == 'custom'}
					{include file="./blockpopup_custom.tpl"}
				{/if}
				
			</div>
		</div>
		
		{* <style type="text/css">
			#xprtblockpopup_{$result.id_xprtblckpopuptbl} .xprtblockpopup_content { 
				/*width: 100%;*/
				max-width: {$result.width};
				height: {$result.height};
			 } 
		</style> *}

		{literal}
		<script type="text/javascript">
			jQuery(document).ready(function($){
				if ($(document.body).width() > 767){

					var popupWidth = parseInt({/literal}{$result.width|intval}{literal});
					var popupHeight = parseInt({/literal}{$result.height|intval}{literal});
					var starttime = parseInt({/literal}{$result.starttime|intval}{literal});
					var staytime = parseInt({/literal}{$result.staytime|intval}{literal});
					if(staytime==0) staytime = 10000000;
setTimeout(function () {
					$.fancybox.open($('#xprtblockpopup_{/literal}{$result.id_xprtblckpopuptbl}{literal}'), {
						padding: 0,
						margin: 0,
						titleShow     : false,
						openEffect	:	'fade',
						closeEffect	:	'fade',
						transitionIn	:	'none',
						transitionOut	:	'none',
						fitToView: false, //
						autoResize: true,
					    maxWidth: "90%", // 
						width: popupWidth, 
						height: popupHeight,
					    // type: 'html',
					    autoSize: false,
			            scrolling: false
						
						// 'speedIn'		:	600, 
						// 'speedOut'		:	200, 
					});
					
					setTimeout(function(){$.fancybox.close();}, staytime);
},starttime);
				};
			});
		</script>
		{/literal}

	{/foreach}
{/if}