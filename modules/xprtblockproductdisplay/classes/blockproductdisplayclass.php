<?php

class blockproductdisplayclass extends ObjectModel
{
	public $id;
	public $title;
    public $subtitle;
	public $description;
    public $btn_lbl;
    public $btn_link;
    public $btn_link_type;
	public $id_xprtblckprdctdisplaytbl;
	public $product_type;
	public $product_item;
	public $limit;
	public $order_by;
	public $order_way;
    public $image_type;
	public $image;
	public $layout_style;
	public $slider_style;
	public $column;
	public $pages;
	public $hook;
	public $position=0;
	public $active=1;
	public static $definition = array(
		'table' => 'xprtblckprdctdisplaytbl',
		'primary' => 'id_xprtblckprdctdisplaytbl',
		'multilang' => true,
		'fields' => array(
				'title' =>			array('type' => self::TYPE_STRING, 'validate' => 'isString','lang' => true),
                'subtitle' =>       array('type' => self::TYPE_STRING, 'validate' => 'isString','lang' => true),
				'description' =>	array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml','lang' => true),
				'product_type' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString','required'=>true),
				'product_item' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'limit' =>			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'order_by' =>		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'order_way' =>		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                'image_type' =>     array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                'image' =>          array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                'btn_lbl' =>        array('type' => self::TYPE_STRING, 'validate' => 'isString','lang' => true),
                'btn_link' =>       array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'btn_link_type' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'layout_style' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString','required'=>true),
				'slider_style' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'column' =>			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'pages' =>			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'hook' =>			array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml','required'=>true),
				'position' =>		array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
				'active' =>			array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
		),
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
	{
        Shop::addTableAssociation('xprtblckprdctdisplaytbl', array('type' => 'shop'));
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function update($null_values = false)
    {
        if (isset($_FILES['image']) && isset($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name']))
            $this->image = $this->processImage($_FILES);
        $product_type = Tools::getValue('product_type');
        $product_item_cat = Tools::getValue('product_item_cat');
        $product_item_man = Tools::getValue('product_item_man');
        $product_item_sup = Tools::getValue('product_item_sup');
        $product_item_prd = Tools::getValue('product_item_prd');
        if($product_type == 'category_product'){
            $this->product_item = $product_item_cat;
        }elseif($product_type == 'manufacturer_product'){
            $this->product_item = $product_item_man;
        }elseif($product_type == 'supplier_product'){
            $this->product_item = $product_item_sup;
        }elseif($product_type == 'selected_product'){
            $this->product_item = $product_item_prd;
        }elseif($product_type == 'all_product' || $product_type == 'new_product' || $product_type == 'featured_product' || $product_type == 'best_product' || $product_type == 'specials_product'){
            $this->product_item = "";
        }
        if(!parent::update($null_values))
            return false;
        return true;
    }
    public function add($autodate = true, $null_values = false)
    {
        if($this->position <= 0)
            $this->position = self::getTopPosition() + 1;
        if(isset($this->image) && !empty($this->image)){
        	$this->image = $this->image;
        }else{
        	$this->image = $this->processImage($_FILES);
        }
        $this->image = $this->processImage($_FILES);
        if(empty($this->pages)){
            $this->pages = 'all_page';
        }

        if(isset($this->product_item) && !empty($this->product_item)){
        	$this->product_item = $this->product_item;
        }else{
        	$product_type = Tools::getValue('product_type');
			$product_item_cat = Tools::getValue('product_item_cat');
			$product_item_man = Tools::getValue('product_item_man');
			$product_item_sup = Tools::getValue('product_item_sup');
			$product_item_prd = Tools::getValue('product_item_prd');
	        if($product_type == 'category_product'){
	            $this->product_item = $product_item_cat;
	        }elseif($product_type == 'manufacturer_product'){
	            $this->product_item = $product_item_man;
	        }elseif($product_type == 'supplier_product'){
	            $this->product_item = $product_item_sup;
	        }elseif($product_type == 'selected_product'){
	            $this->product_item = $product_item_prd;
	        }elseif($product_type == 'all_product' || $product_type == 'new_product' || $product_type == 'featured_product' || $product_type == 'best_product' || $product_type == 'specials_product'){
	            $this->product_item = "";
	        }
        }
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public function processImage($FILES) {

        if (isset($FILES['image']) && isset($FILES['image']['tmp_name']) && !empty($FILES['image']['tmp_name'])) {
                $ext = substr($FILES['image']['name'], strrpos($FILES['image']['name'], '.') + 1);
                $id = time();
                $file_name = $id . '.' . $ext;
                $path = _PS_MODULE_DIR_ .'xprtblockproductdisplay/img/' . $file_name;
                if (!move_uploaded_file($FILES['image']['tmp_name'], $path))
                    return false;         
                else
                    return $file_name;   
        }
    }
    public static function getTopPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtblckprdctdisplaytbl`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS('
            SELECT `id_xprtblckprdctdisplaytbl`, `position`
            FROM `'._DB_PREFIX_.'xprtblckprdctdisplaytbl`
            ORDER BY `position` ASC'
        ))
            return false;
        if(!empty($res))
        foreach($res as $xprtblckprdctdisplaytbl)
            if((int)$xprtblckprdctdisplaytbl['id_xprtblckprdctdisplaytbl'] == (int)$this->id)
        $moved_xprtblckprdctdisplaytbl = $xprtblckprdctdisplaytbl;
        if(!isset($moved_xprtblckprdctdisplaytbl) || !isset($position))
            return false;
        $queryx = ' UPDATE `'._DB_PREFIX_.'xprtblckprdctdisplaytbl`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
        ? '> '.(int)$moved_xprtblckprdctdisplaytbl['position'].' AND `position` <= '.(int)$position
        : '< '.(int)$moved_xprtblckprdctdisplaytbl['position'].' AND `position` >= '.(int)$position.'
        ');
        $queryy = ' UPDATE `'._DB_PREFIX_.'xprtblckprdctdisplaytbl`
        SET `position` = '.(int)$position.'
        WHERE `id_xprtblckprdctdisplaytbl` = '.(int)$moved_xprtblckprdctdisplaytbl['id_xprtblckprdctdisplaytbl'];
        return (Db::getInstance()->execute($queryx,false)
        && Db::getInstance()->execute($queryy,false));
    }
    // START ALL SELECTS VALUE
    public function SimpleProductS()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $sql = 'SELECT p.`id_product`, pl.`name`
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }
    public function AllProductS()
    {
        $rs = array();
        $rslt = array();
        $rs =  $this->SimpleProductS();
        $i = 0;
        foreach($rs as $r){
            $rslt[$i]['id'] = 'prd_'.$r['id_product'];
            $rslt[$i]['name'] = $r['name'];
            $i++;
        }
        return $rslt;
    }
    public function AllCategorieS()
    {
        $rs = array();
        $rslt = array();
        $id_lang = Context::getContext()->language->id;
        $rs =  Category::getCategories($id_lang,true,false);
        $i = 0;
        foreach($rs as $r){
            $rslt[$i]['id'] = 'cat_'.$r['id_category'];
            $rslt[$i]['name'] = $r['name'];
            $i++;
        }
        return $rslt;
    }
    public function AllSupplierS()
    {
        $rs = array();
        $rslt = array();
        $rs =  Supplier::getSuppliers();
        $i = 0;
        foreach($rs as $r){
            $rslt[$i]['id'] = 'sup_'.$r['id_supplier'];
            $rslt[$i]['name'] = $r['name'];
            $i++;
        }
        return $rslt;
    }
    public function GetManufacturers()
    {
        $rs = array();
        $rslt = array();
        $rs =  Manufacturer::getManufacturers();
        $i = 0;
        foreach($rs as $r){
            $rslt[$i]['id'] = 'man_'.$r['id_manufacturer'];
            $rslt[$i]['name'] = $r['name'];
            $i++;
        }
        return $rslt;
    }
    public static function TestGetAllBlockProductS()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtblckprdctdisplaytbl` pb 
                INNER JOIN `'._DB_PREFIX_.'xprtblckprdctdisplaytbl_lang` pbl ON (pb.`id_xprtblckprdctdisplaytbl` = pbl.`id_xprtblckprdctdisplaytbl` AND pbl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.'xprtblckprdctdisplaytbl_shop` pbs ON (pb.`id_xprtblckprdctdisplaytbl` = pbs.`id_xprtblckprdctdisplaytbl` AND pbs.`id_shop` = '.$id_shop.')
                ';
        $sql .= ' WHERE pb.`active` = 1 ORDER BY pb.`position` ASC';
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }
    public static function GetAllBlockProductS($hook = NULL)
    {
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtblckprdctdisplaytbl` pb 
                INNER JOIN `'._DB_PREFIX_.'xprtblckprdctdisplaytbl_lang` pbl ON (pb.`id_xprtblckprdctdisplaytbl` = pbl.`id_xprtblckprdctdisplaytbl` AND pbl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.'xprtblckprdctdisplaytbl_shop` pbs ON (pb.`id_xprtblckprdctdisplaytbl` = pbs.`id_xprtblckprdctdisplaytbl` AND pbs.`id_shop` = '.$id_shop.')
                ';
        $sql .= ' WHERE pb.`active` = 1 ';

        if($hook != NULL)
            $sql .= ' AND pb.`hook` = "'.$hook.'" ';

        $sql .= ' ORDER BY pb.`position` ASC';
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }
    public static function GetProductBlock($hook = NULL)
    {
        if($hook == NULL)
            return false;
        $results = array();
        $theme_tpl_path = _PS_THEME_DIR_."modules/xprtblockproductdisplay/views/templates/front/layout/";
        $mod_tpl_path = _PS_MODULE_DIR_."xprtblockproductdisplay/views/templates/front/layout/";
        
        $allproducts = self::GetAllBlockProductS($hook);
        if(isset($allproducts) && !empty($allproducts)){
            $i = 0;
            foreach ($allproducts as $allprds){
                if(xprtblockproductdisplay::PageException($allprds['pages'])){
                    $results[$i]['id_xprtblckprdctdisplaytbl']    =   isset($allprds['id_xprtblckprdctdisplaytbl'])?$allprds['id_xprtblckprdctdisplaytbl']:0;
                    $results[$i]['product_type']  =   isset($allprds['product_type'])?$allprds['product_type']:"all_product";
                    $results[$i]['product_item']  =   isset($allprds['product_item'])?$allprds['product_item']:"";
                    $results[$i]['limit'] =   isset($allprds['limit'])?$allprds['limit']:4;
                    $results[$i]['order_by']  =   isset($allprds['order_by'])?$allprds['order_by']:"id_product";
                    $results[$i]['order_way'] =   isset($allprds['order_way'])?$allprds['order_way']:"DESC";
                    $results[$i]['image_type']    =   isset($allprds['image_type'])?$allprds['image_type']:"medium_default";
                    
                    $xprthvb_image = isset($allprds['image'])?$allprds['image']:"";
                    if(!file_exists(_PS_MODULE_DIR_.'xprtblockproductdisplay/img/'.$xprthvb_image)){
						$results[$i]['image'] = null;
                    }else{
                    	$results[$i]['image'] = _MODULE_DIR_.'xprtblockproductdisplay/img/'.$xprthvb_image;
                    }
                    // $results[$i]['image']    =   isset($allprds['image'])?$allprds['image']:"";
                    if(file_exists($theme_tpl_path.$allprds['layout_style'].".tpl")){
                        $results[$i]['layout_style']  =   $allprds['layout_style'];
                        self::AddMediaFile($allprds['layout_style'],true);
                        self::AddMediaFile($allprds['layout_style'],false);
                    }else{
                        if(file_exists($mod_tpl_path.$allprds['layout_style'].".tpl")){
                            $results[$i]['layout_style']  =   $allprds['layout_style'];
                            self::AddMediaFile($allprds['layout_style'],true);
                            self::AddMediaFile($allprds['layout_style'],false);
                        }else{
                            $results[$i]['layout_style']  =   'default';
                            self::AddMediaFile('default',true);
                            self::AddMediaFile('default',false);
                        }
                    }
                    $results[$i]['layout_style']  =   isset($allprds['layout_style'])?$allprds['layout_style']:"default";
                    $results[$i]['slider_style']  =   isset($allprds['slider_style'])?$allprds['slider_style']:"";
                    $results[$i]['column']    =   isset($allprds['column'])?$allprds['column']:"";
                    $results[$i]['pages'] =   isset($allprds['pages'])?$allprds['pages']:"";
                    $results[$i]['hook']  =   isset($allprds['hook'])?$allprds['hook']:"";
                    $results[$i]['active']    =   $allprds['active'];
                    $results[$i]['position']  =   $allprds['position'];
                    $results[$i]['title'] =   isset($allprds['title'])?$allprds['title']:"";
                    $results[$i]['subtitle']  =   isset($allprds['subtitle'])?$allprds['subtitle']:"";
                    $results[$i]['description']  =   isset($allprds['description'])?$allprds['description']:"";
                    $results[$i]['btn_lbl']  =   isset($allprds['btn_lbl'])?$allprds['btn_lbl']:"";
                    $results[$i]['btn_link']  =   isset($allprds['btn_link'])?$allprds['btn_link']:"";
                    $results[$i]['btn_link_type']  =   isset($allprds['btn_link_type'])?$allprds['btn_link_type']:"";
                    $results[$i]['products'] = self::GetProductProperties($allprds['product_type'],$allprds['product_item'],$allprds['limit'],$allprds['order_by'],$allprds['order_way']);
                $i++;
                }
            }
        }
        return $results;
    }
    public static function AddMediaFile($layout,$css=true)
    {
        if($css){
            $theme_media_path = _PS_THEME_DIR_."css/modules/xprtblockproductdisplay/css/";
            $mod_media_path = _PS_MODULE_DIR_."xprtblockproductdisplay/css/";
            $postfix = ".css";
        }else{
            $theme_media_path = _PS_THEME_DIR_."js/modules/xprtblockproductdisplay/js/";
            $mod_media_path = _PS_MODULE_DIR_."xprtblockproductdisplay/js/";
            $postfix = ".js";
        }
        if(file_exists($theme_media_path.$layout.$postfix)){
            if($css){
                Context::getContext()->controller->addCSS(_THEME_CSS_DIR_."modules/xprtblockproductdisplay/css/".$layout.$postfix);
            }else{
                Context::getContext()->controller->addJS(_THEME_JS_DIR_."modules/xprtblockproductdisplay/js/".$layout.$postfix);
            }
        }else{
            if(file_exists($mod_media_path.$layout.$postfix)){
                if($css){
                    Context::getContext()->controller->addCSS(_MODULE_DIR_."xprtblockproductdisplay/css/".$layout.$postfix);
                }else{
                    Context::getContext()->controller->addJS(_MODULE_DIR_."xprtblockproductdisplay/js/".$layout.$postfix);
                }
            }
        }
    }
    public static function GetProductProperties($product_type='new_product',$product_item=NULL,$limit=10,$order_by='id_product',$order_way='DESC')
    {
        $productproperties = array();
        $context = Context::getcontext();
            if($product_type == 'all_product'){
                $prd_ids = self::GetAllPrdExclude($order_by,$order_way);
                $productproperties = self::GetProductsByID($prd_ids);
            }elseif($product_type == 'featured_product'){
                $productproperties = self::GetProductsByCatID((int)Configuration::get('HOME_FEATURED_CAT'),(int)$limit,(int)$context->language->id, null, false, $order_by ,$order_way);
            }elseif($product_type == 'new_product'){
                $productproperties = Product::getNewProducts((int)$context->language->id, 0, (int)$limit);
            }elseif($product_type == 'best_product'){
                $productproperties = self::GetBestSellProducts((int)$limit);
            }elseif($product_type == 'specials_product'){
                $productproperties = Product::getPricesDrop((int)$context->language->id,0,(int)$limit);
            }elseif($product_type == 'category_product'){
                if(isset($product_item) && !empty($product_item)){
                    $category_arr = self::GetIndividualItem($product_item,"cat",false);
                    foreach($category_arr as $category_ar){
                        $cat_products = self::GetProductsByCatID($category_ar,(int)$limit,(int)$context->language->id, null, false, $order_by ,$order_way);
                        if(is_array($cat_products) && !empty($cat_products))
                            $productproperties = array_merge($productproperties,$cat_products);
                    }
                }
            }elseif($product_type == 'selected_product'){
                if(isset($product_item) && !empty($product_item)){
                    $prd_ids = self::GetIndividualItem($product_item,"prd",true);
                    $productproperties = self::GetProductsByID($prd_ids);
                }
            }elseif($product_type == 'manufacturer_product'){
                if(isset($product_item) && !empty($product_item)){
                    $brand_ids_arr = self::GetIndividualItem($product_item,"man",false);
                    foreach($brand_ids_arr as $brand_ids_ar){
                        $manufacturer = new Manufacturer((int)$brand_ids_ar, $context->language->id);
                        $man_products = $manufacturer->getProducts($brand_ids_ar, $context->language->id,1,(int)$limit, $order_by, $order_way);
                        if(is_array($man_products) && !empty($man_products))
                            $productproperties = array_merge($productproperties,$man_products);
                    }
                }
            }elseif($product_type == 'supplier_product'){
                if(isset($product_item) && !empty($product_item)){
                    $supplier_ids_arr = self::GetIndividualItem($product_item,"sup",false);
                    foreach($supplier_ids_arr as $supplier_ids_ar){
                        $sup_products = Supplier::getProducts($supplier_ids_ar, $context->language->id,1, $limit, $order_by, $order_way, false);
                        if(is_array($sup_products) && !empty($sup_products))
                            $productproperties = array_merge($productproperties,$sup_products);
                    }
                }
            }
        return $productproperties;
    }
    public static function GetAllPrdExclude($orderby = 'id_product',$order = 'DESC')
    {
        $excludeprd = '';
        $excludeprds = '';
        if($orderby == 'id_product'){
            $order_init = ' p.`id_product` '.$order;
        }elseif($orderby == 'price'){
            $order_init = ' p.`price` '.$order;
        }elseif($orderby == 'date_add'){
            $order_init = ' p.`date_add` '.$order;
        }elseif($orderby == 'date_upd'){
            $order_init = ' p.`date_upd` '.$order;
        }else{
            $order_init = ' p.`id_product` DESC ';
        }
        $sql = 'SELECT p.`id_product` as id
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                WHERE pl.`id_lang` = '.(int)Context::getContext()->language->id.' ORDER BY '.$order_init;
        $allproducts =  Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if(isset($allproducts) && !empty($allproducts)){
            foreach($allproducts as $allprd){
                $excludeprd .= $allprd['id'].',';
            }
            if(isset($excludeprd) && !empty($excludeprd)){
                $excludeprds = substr($excludeprd,0,-1);
            }
        }
        return $excludeprds;
    }
    public static function GetProductsByID($ids = NULL)
    {
        if($ids == NULL)
            return false;
            $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
             pl.`description`, pl.`description_short`, product_attribute_shop.id_product_attribute, pl.`link_rewrite`,
              pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, image_shop.`id_image`, il.`legend`,
               m.`name` AS manufacturer_name FROM `' . _DB_PREFIX_ . 'product` p
            ' . Shop::addSqlAssociation('product', 'p') . ' LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute pa ON (pa.id_product = p.id_product) 
            ' . Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.default_on=1') . '
            ' . Product::sqlStock('p', 0, false) . ' LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON ( p.`id_product` = pl.`id_product` AND pl.`id_lang` = 
                ' . (int) Context::getContext()->language->id . Shop::addSqlRestrictionOnLang('pl') . '
            ) LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_product` = p.`id_product`)' . Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1') . 
            ' LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int) Context::getContext()->language->id . ')
            LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`) WHERE  p.`id_product` IN(' . $ids . ') AND product_shop.`active` = 1  
            AND product_shop.`show_price` = 1 AND ((image_shop.id_image IS NOT NULL OR i.id_image IS NULL) OR (image_shop.id_image IS NULL AND i.cover=1)) AND (pa.id_product_attribute IS NULL OR 
                product_attribute_shop.default_on = 1)';
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
            return Product::getProductsProperties((int) Context::getContext()->language->id,$result);
    }
    public static function GetBestSellProducts($limit = 10)
    {
        if(Configuration::get('PS_CATALOG_MODE'))
            return false;
        $context = Context::getcontext();
        if(!($result = ProductSale::getBestSalesLight((int)$context->language->id,0,$limit)))
            return false;
        $currency = new Currency((int)$context->cookie->id_currency);
        $usetax = Product::getTaxCalculationMethod();
        if(isset($result) && !empty($result)){
                foreach ($result as &$row){
                    $row['price'] = Tools::displayPrice(Product::getPriceStatic((int)$row['id_product'], $usetax), $currency);
                }
            return $result;
        }else
            return false;
    }
    public static function GetProductsByCatID($category_id,$limit=4, $id_lang = null, $id_shop = null, $child_count = false, $order_by = 'id_product', $order_way = "DESC")
    {
        $context = Context::getContext(); 
        $id_lang = is_null($id_lang) ? $context->language->id : $id_lang ;
        $id_shop = is_null($id_shop) ? $context->shop->id : $id_shop ;
        $id_supplier = '';
        $active = true;
        $front = true;
        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, MAX(product_attribute_shop.id_product_attribute) id_product_attribute, 
        product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`,
         pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, MAX(image_shop.`id_image`) id_image, il.`legend`, m.`name` AS manufacturer_name,
          cl.`name` AS category_default, DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
                    DAY)) > 0 AS new, product_shop.price AS orderprice FROM `'._DB_PREFIX_.'category_product` cp LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product` 
        '.Shop::addSqlAssociation('product', 'p').' LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product`)
            '.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
            '.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).' LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (product_shop.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').') LEFT JOIN `'._DB_PREFIX_.'image` i
                ON (i.`id_product` = p.`id_product`)'. Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').' LEFT JOIN `'._DB_PREFIX_.'image_lang` il
                ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.') LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
                ON m.`id_manufacturer` = p.`id_manufacturer` WHERE product_shop.`id_shop` = '.(int)$context->shop->id.' AND cp.`id_category` = '.(int)$category_id
                .($active ? ' AND product_shop.`active` = 1' : '') .($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '') .($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
                .' GROUP BY product_shop.id_product';
        if (empty($order_by) || $order_by == 'position') $order_by = 'price';
        if (empty($order_way)) $order_way = 'DESC';
        if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add'  || $order_by == 'date_upd')
                $order_by_prefix = 'p';
        else if ($order_by == 'name')
                $order_by_prefix = 'pl';
        $sql .= " ORDER BY {$order_by_prefix}.{$order_by} {$order_way}";
        $sql .= ' LIMIT '.$limit.' '; 
       $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);  
        return Product::getProductsProperties($id_lang,$result);
    }
    public static function GetIndividualItem($items=NULL,$pref=NULL,$string=false)
    {
        if($pref == NULL)
            return false; 
        if($items == NULL)
            return false;  
        $results = array();
        $results_str = '';
        $items_arr = explode(",",$items);
        if(isset($items_arr) && !empty($items_arr)){
            foreach($items_arr as $item_ar){
                if(strpos($item_ar,$pref) !== false){
                    $results[] = str_replace($pref.'_',"",$item_ar);
                }
            }
            $results_str = implode(",",$results);
        }
        if($string == false)
            return $results;
        else
            return $results_str;
    }
}