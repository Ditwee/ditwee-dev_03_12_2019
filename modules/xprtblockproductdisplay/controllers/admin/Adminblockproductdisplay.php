<?php
class AdminblockproductdisplayController extends ModuleAdminController {

    public function __construct() {
        $this->table = 'xprtblckprdctdisplaytbl';
        $this->className = 'blockproductdisplayclass';
        $this->lang = true;
        $this->deleted = false;
        $this->module = 'xprtblockproductdisplay';
        $this->explicitSelect = true;
        $this->_defaultOrderBy = 'position';
        $this->allow_export = false;
        $this->_defaultOrderWay = 'DESC';
        $this->bootstrap = true;
            if(Shop::isFeatureActive())
            Shop::addTableAssociation($this->table, array('type' => 'shop'));
            parent::__construct();
        $this->fields_list = array(
            'id_xprtblckprdctdisplaytbl' => array(
                    'title' => $this->l('Id'),
                    'width' => 100,
                    'type' => 'text',
            ),
            'title' => array(
                    'title' => $this->l('Title'),
                    'width' => 60,
                    'type' => 'text',
            ),
            'product_type' => array(
                    'title' => $this->l('Products'),
                    'width' => 60,
                    'type' => 'text',
            ),
            'hook' => array(
                    'title' => $this->l('Hook'),
                    'width' => 220,
                    'type' => 'text',
            ),
            'layout_style' => array(
                    'title' => $this->l('Layout Style'),
                    'width' => 220,
                    'type' => 'text',
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'width' => 60,
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false
            )
        );
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        parent::__construct();
    }
    public function init()
    {
        parent::init();
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'xprtblckprdctdisplaytbl_shop sbp ON a.id_xprtblckprdctdisplaytbl=sbp.id_xprtblckprdctdisplaytbl && sbp.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
        $this->_select = 'sbp.id_shop';
        $this->_defaultOrderBy = 'a.position';
        $this->_defaultOrderWay = 'DESC';
        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
        $this->_group = 'GROUP BY a.id_xprtblckprdctdisplaytbl';
        $this->_select = 'a.position position';
    }
    public function setMedia()
    {          
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
        $this->addJqueryPlugin('select2');
    }
    public static function AllPageExceptions()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $sql = 'SELECT p.`id_product`, pl.`name`
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
        $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        $id_lang = Context::getContext()->language->id;
        $categories =  Category::getCategories($id_lang,true,false);
        $controllers = Dispatcher::getControllers(_PS_FRONT_CONTROLLER_DIR_);
        if(isset($controllers))
            ksort($controllers);
        $Manufacturers =  Manufacturer::getManufacturers();
        $Suppliers =  Supplier::getSuppliers();
        $rslt = array();
        $rslt[0]['id'] = 'all_page';
        $rslt[0]['name'] = 'All Pages';
        $i = 1;
        if(isset($controllers))
            foreach($controllers as $r => $v){
                $rslt[$i]['id'] = $r;
                $rslt[$i]['name'] = 'Page : '.ucwords($r);
                $i++;
            }
        if(isset($Manufacturers))
            foreach($Manufacturers as $r){
                $rslt[$i]['id'] = 'man_'.$r['id_manufacturer'];
                $rslt[$i]['name'] = 'Manufacturer : '.$r['name'];
                $i++;
            }
        if(isset($Suppliers))
            foreach($Suppliers as $r){
                $rslt[$i]['id'] = 'sup_'.$r['id_supplier'];
                $rslt[$i]['name'] = 'Supplier : '.$r['name'];
                $i++;
            }
        if(isset($categories))
            foreach($categories as $cats){
                $rslt[$i]['id'] = 'cat_'.$cats['id_category'];
                $rslt[$i]['name'] = 'Category : '.$cats['name'];
                $i++;
            }
        if(isset($products))
            foreach($products as $r){
                $rslt[$i]['id'] = 'prd_'.$r['id_product'];
                $rslt[$i]['name'] = 'Product : '. $r['name'];
                $i++;
            }
        if(isset($categories))
            foreach($categories as $cats){
                $rslt[$i]['id'] = 'prdcat_'.$cats['id_category'];
                $rslt[$i]['name'] = 'Category Product: '.$cats['name'];
                $i++;
            }
        if(isset($Manufacturers))
            foreach($Manufacturers as $r){
                $rslt[$i]['id'] = 'prdman_'.$r['id_manufacturer'];
                $rslt[$i]['name'] = 'Manufacturer Product : '.$r['name'];
                $i++;
            }
        if(isset($Suppliers))
            foreach($Suppliers as $r){
                $rslt[$i]['id'] = 'prdsup_'.$r['id_supplier'];
                $rslt[$i]['name'] = 'Supplier Product : '.$r['name'];
                $i++;
            }
        return $rslt;
    }
	public static function order_by_val()
    {
    	$order_by_val = array(
    			array(
    				'id' => 'id_product',
    				'name' => 'Product ID'
    			),
    			array(
    				'id' => 'price',
    				'name' => 'Price'
    			),
    			array(
    				'id' => 'date_add',
    				'name' => 'Created Date'
    			),
    			array(
    				'id' => 'date_upd',
    				'name' => 'Update Date'
    			)
    		);
    	return $order_by_val;
    }
    public static function order_way_val()
    {
    	$order_way_val = array(
    			array(
    				'id' => 'ASC',
    				'name' => 'Assending'
    			),
    			array(
    				'id' => 'DESC',
    				'name' => 'Desending'
    			),
    		);
    	return $order_way_val;
    }
    public static function image_type_val()
    {
    	$image_type_val = array();
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'image_type` WHERE `products` = 1 ORDER BY `name` ASC';
		$results = Db::getInstance()->executeS($sql);
		if(isset($results) && !empty($results)){
			$i = 0;
			foreach($results as $result)
			{
			  $image_type_val[$i]['id'] = $result['name'];
			  $image_type_val[$i]['name'] = ucwords(str_replace("_"," ",$result['name']));
			  $i++;
			}
		}
    	return $image_type_val;
    }
    public static function layout_style_val()
    {
        $layout_style_val = array();
        $theme_path =  _PS_THEME_DIR_.'modules/xprtblockproductdisplay/views/templates/front/layout/';
        $mod_path =  _PS_MODULE_DIR_.'xprtblockproductdisplay/views/templates/front/layout/';
        if(file_exists($theme_path.'default.tpl')){
            $file_lists = array_diff(scandir($theme_path), array('..', '.'));
        }else{
    	    $file_lists = array_diff(scandir($mod_path), array('..', '.'));
        }
        if(isset($file_lists) && !empty($file_lists)){
            $i = 0;
            foreach ($file_lists as $key => $value) {
                $layout_style_val[$i]['id'] = str_replace(".tpl","",$value);
                $layout_style_val[$i]['name'] = ucwords(str_replace(".tpl","",$value))." Style";
                $i++;
            }
        }
    	return $layout_style_val;
    }
    public static function slider_style_val()
    {
    	$slider_style_val = array(
			array(
				'id' => 'general',
				'name' => 'General'
			),
			// array(
			// 	'id' => 'slider',
			// 	'name' => 'Slider'
			// ),
			array(
				'id' => 'carousel',
				'name' => 'Carousel'
			),
		);
    	return $slider_style_val;
    }
    public static function hook_val()
    {
    	require_once(dirname(__FILE__) . '/../../hooks/hooks.php');
		$hook_val = $hooks;
    	return $hook_val;
    }
    public function renderForm()
    {
        $this->displayInformation($this->l('Does Not Work all Fields For All Layout Styles.(*) is Required Fields. Other Fields Value Are Depend on Your Layout Style.'));
        $image_url = false;
        $image_size = file_exists($image_url) ? filesize($image_url) / 1000 : false;
        $prdtypes[] = array('id' => 'all_product','name' => 'All Products');
        $prdtypes[] = array('id' => 'new_product','name' => 'New Products');
        $prdtypes[] = array('id' => 'featured_product','name' => 'Featured Products');
        $prdtypes[] = array('id' => 'best_product','name' => 'Best Sells Products');
        $prdtypes[] = array('id' => 'specials_product','name' => 'Specials Products');
        $prdtypes[] = array('id' => 'category_product','name' => 'Category Wise Products');
        $prdtypes[] = array('id' => 'manufacturer_product','name' => 'Manufacturer Wise Products');
        $prdtypes[] = array('id' => 'supplier_product','name' => 'Supplier Wise Products');
        $prdtypes[] = array('id' => 'selected_product','name' => 'Selected Products');
        $bpdc = new blockproductdisplayclass();
		$hook_val = self::hook_val();
		$order_by_val = self::order_by_val();
		$order_way_val = self::order_way_val();
		$image_type_val = self::image_type_val();
		$layout_style_val = self::layout_style_val();
		$slider_style_val = self::slider_style_val();
        if(Tools::getvalue('id_xprtblckprdctdisplaytbl')){
            $clsobj = new blockproductdisplayclass(Tools::getvalue('id_xprtblckprdctdisplaytbl'));
            $product_item_defvalues = $clsobj->product_item;
        }else{
            $product_item_defvalues = '';
        }
        $this->fields_form = array(
            'legend' => array(
          		'title' => $this->l('Display Products'),
            ),
            'input' => array(
            	array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'name' => 'title',
                    'desc' => $this->l('Enter Your Block Title'),
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Sub Title'),
                    'name' => 'subtitle',
                    'desc' => $this->l('Enter Your Block subtitle'),
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description'),
                    'name' => 'Description',
                    'desc' => $this->l('Enter Your Block Description'),
                    'lang' => true,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Product Type: '),
                    'name' => 'product_type',
                    'required'=>true,
                    'options' => array(
                        'query' => $prdtypes,
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'selectchange',
                    'name' => 'snt_selectchange',
                    'hideclass' => 'selecttwotypeclass',
                    'dependency' => array(
                    	'category_product' => 'product_item_cat',
                    	'manufacturer_product' => 'product_item_man',
                    	'supplier_product' => 'product_item_sup',
                    	'selected_product' => 'product_item_prd',
                    	)
                ),
                array(
                    'type' => 'selecttwotype',
                    'label' => $this->l('Select Products'),
                    'placeholder' => $this->l('Select Products'),
                    'initvalues' => $bpdc->AllProductS(),
                    'defvalues' => $product_item_defvalues,
                    'name' => 'product_item_prd',
                    'hideclass' => 'selecttwotypeclass',
                    'dependency' => 'new_product',
                    'desc' => $this->l('Please Type Your Product Name And Select. You Can Select Multiple')
                ),
                array(
                    'type' => 'selecttwotype',
                    'label' => $this->l('Select Categories'),
                    'hideclass' => 'selecttwotypeclass',
                    'placeholder' => $this->l('Select Categories'),
                    'initvalues' => $bpdc->AllCategorieS(),
                    'defvalues' => $product_item_defvalues,
                    'name' => 'product_item_cat',
                    'dependency' => 'manufacturer_product',
                    'desc' => $this->l('Please Type Your Category Name And Select. You Can Select Multiple')
                ),
                array(
                    'type' => 'selecttwotype',
                    'label' => $this->l('Select Manufacturers'),
                    'hideclass' => 'selecttwotypeclass',
                    'placeholder' => $this->l('Select Manufacturers'),
                    'initvalues' => $bpdc->GetManufacturers(),
                    'defvalues' => $product_item_defvalues,
                    'name' => 'product_item_man',
                    'dependency' => 'category_product',
                    'desc' => $this->l('Please Type Your Manufacturer Name And Select. You Can Select Multiple')
                ),
                array(
                    'type' => 'selecttwotype',
                    'label' => $this->l('Select Suppliers'),
                    'hideclass' => 'selecttwotypeclass',
                    'placeholder' => $this->l('Select Suppliers'),
                    'initvalues' => $bpdc->AllSupplierS(),
                    'defvalues' => $product_item_defvalues,
                    'name' => 'product_item_sup',
                    'dependency' => 'best_product',
                    'desc' => $this->l('Please Type Your Supplier Name And Select. You Can Select Multiple')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('How Many Product You Want To Display'),
                    'name' => 'limit',
                    'desc' => $this->l('How Many Product You Want To Display'),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Product Sorting Order By'),
                    'name' => 'order_by',
                    'options' => array(
                        'query' => $order_by_val,
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Product Sorting Order Way'),
                    'name' => 'order_way',
                    'options' => array(
                        'query' => $order_way_val,
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Product Image Type'),
                    'name' => 'image_type',
                    'options' => array(
                        'query' => $image_type_val,
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Layout Style'),
                    'name' => 'layout_style',
                    'required'=>true,
                    'options' => array(
                        'query' => $layout_style_val,
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Slider Style'),
                    'name' => 'slider_style',
                    'options' => array(
                        'query' => $slider_style_val,
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Where You Want to Display'),
                    'name' => 'hook',
                    'required'=>true,
                    'options' => array(
                        'query' => $hook_val,
                        'id' => 'id',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'selecttwotype',
                    'label' => $this->l('Which Page You Want to Display'),
                    'placeholder' => $this->l('Please Type Your Controller Name.'),
                    'initvalues' => self::AllPageExceptions(),
                    'name' => 'pages',
                    'desc' => $this->l('Please Type Your Specific Page Name,Category name,Product Name,For All Product specific Category select category product: category name.<br>For showing All page Type: All Page. For Home Page Type:index.')
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Upload Image'),
                    'name' => 'image',
                    'display_image' => true,
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    // 'delete_url' => self::$currentIndex.'&'.$this->identifier.'='.$this->_category->id.'&token='.$this->token.'&deleteImage=1',
                    // 'format' => $format['category']
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Button Lavel'),
                    'name' => 'btn_lbl',
                    'desc' => $this->l('Enter Your Button Lavel'),
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Button Link'),
                    'name' => 'btn_link',
                    'desc' => $this->l('Enter Your Button Link'),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Button Link Target'),
                    'name' => 'btn_link_type',
                    'options' => array(
                        'query' => array(
                            array(
                                'id'=>'_blank',
                                'name'=>'New Window or Tab',
                            ),
                            array(
                                'id'=>'_self',
                                'name'=>'Same Frame as it Was Clicked',
                            ),
                            array(
                                'id'=>'_parent',
                                'name'=>'Parent Frame',
                            ),
                            array(
                                'id'=>'_top',
                                'name'=>'Full Body of The Window',
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Status'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
	                        'id' => 'active',
	                        'value' => 1,
	                        'label' => $this->l('Enabled')
                        ),
                        array(
	                        'id' => 'active',
	                        'value' => 0,
	                        'label' => $this->l('Disabled')
                        )
                    )
               	)
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        if(Shop::isFeatureActive())
			$this->fields_form['input'][] = array(
				'type' => 'shop',
				'label' => $this->l('Shop association:'),
				'name' => 'checkBoxShopAsso',
			);
        if(!($blockproductdisplayclass = $this->loadObject(true)))
            return;
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save   '),
            'class' => 'button'
        );
        return parent::renderForm();
    }
    public function renderList()
    {
        if(isset($this->_filter) && trim($this->_filter) == '')
            $this->_filter = $this->original_filter;
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();
    }
    public function initToolbar(){
          parent::initToolbar();
    }
    public function processPosition()
    {
        if($this->tabAccess['edit'] !== '1')
            $this->errors[] = Tools::displayError('You do not have permission to edit this.');
        else if(!Validate::isLoadedObject($object = new blockproductdisplayclass((int)Tools::getValue($this->identifier, Tools::getValue('id_xprtblckprdctdisplaytbl', 1)))))
        $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.').' <b>'.
        $this->table.'</b> '.Tools::displayError('(cannot load object)');
        if(!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position')))
        $this->errors[] = Tools::displayError('Failed to update the position.');
        else
        {
            $object->regenerateEntireNtree();
            Tools::redirectAdmin(self::$currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_xprtblckprdctdisplaytbl = (int)Tools::getValue($this->identifier)) ? ('&'.$this->identifier.'='.$id_xprtblckprdctdisplaytbl) : '').'&token='.Tools::getAdminTokenLite('Adminblockproductdisplay'));
        }
    }
    public function ajaxProcessUpdatePositions()
    {
      $id_xprtblckprdctdisplaytbl = (int)(Tools::getValue('id'));
      $way = (int)(Tools::getValue('way'));
      $positions = Tools::getValue($this->table);
      if (is_array($positions))
        foreach ($positions as $key => $value)
        {
          $pos = explode('_', $value);
          if ((isset($pos[1]) && isset($pos[2])) && ($pos[2] == $id_xprtblckprdctdisplaytbl))
          {
            $position = $key + 1;
            break;
          }
        }
      $blockproductdisplayclass = new blockproductdisplayclass($id_xprtblckprdctdisplaytbl);
      if (Validate::isLoadedObject($blockproductdisplayclass))
      {
        if (isset($position) && $blockproductdisplayclass->updatePosition($way, $position))
        {
          Hook::exec('action'.$this->className.'Update');
          die(true);
        }
        else
          die('{"hasError" : true, errors : "Can not update blockproductdisplayclass position"}');
      }
      else
        die('{"hasError" : true, "errors" : "This blockproductdisplayclass can not be loaded"}');
    }
}
