<?php

$querys = array();

$querys[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtblckprdctdisplaytbl` (
				`id_xprtblckprdctdisplaytbl` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`product_type` VARCHAR(55) NULL,
				`product_item` text NULL,
				`limit` VARCHAR(50) NULL,
				`order_by` VARCHAR(50) NULL,
				`order_way` VARCHAR(50) NULL,
				`image_type` VARCHAR(100) NULL,
				`image` VARCHAR(150) NULL,
				`layout_style` VARCHAR(100) NULL,
				`slider_style` VARCHAR(100) NULL,
				`column` VARCHAR(50) NULL,
				`pages` VARCHAR(500) NULL,
				`btn_link` VARCHAR(500) NULL,
				`btn_link_type` VARCHAR(100) NULL,
				`hook` VARCHAR(150) NULL,
				`active` int(10) NOT NULL,
				`position`int(10) NOT NULL ,
				PRIMARY KEY (`id_xprtblckprdctdisplaytbl`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$querys[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtblckprdctdisplaytbl_lang` (
				`id_xprtblckprdctdisplaytbl` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NULL ,
				`title` VARCHAR(300) NULL,
				`subtitle` VARCHAR(300) NULL,
				`description` text NULL,
				`btn_lbl` VARCHAR(100) NULL,
				PRIMARY KEY (`id_xprtblckprdctdisplaytbl`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$querys[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtblckprdctdisplaytbl_shop` (
			  `id_xprtblckprdctdisplaytbl` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprtblckprdctdisplaytbl`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$querys_u = array();

$querys_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtblckprdctdisplaytbl`';

$querys_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtblckprdctdisplaytbl_lang`';

$querys_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtblckprdctdisplaytbl_shop`';
