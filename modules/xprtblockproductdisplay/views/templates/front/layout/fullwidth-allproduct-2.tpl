<div class="kr_product_layout_fullwidth">
	<div class="kr_product_layout_fullwidth_inner">
		{if isset($blockproducts.title) && !empty($blockproducts.title)}
			<div class="kr_product_layout_fullwidth_heading">
				<h3 class="page-heading">
					<em>{$blockproducts.title}</em>
				</h3>
				<p class="page_subtitle">{$blockproducts.subtitle}</p>
			</div>
		{/if}
		{if $blockproducts.products && $blockproducts.products|@count > 0}
		<div class="kr_product_layout_fullwidth_content">
			{include file="$tpl_dir./product-list.tpl" products=$blockproducts.products class="blockproduct" }
		</div>
		{/if}
	</div>
</div>