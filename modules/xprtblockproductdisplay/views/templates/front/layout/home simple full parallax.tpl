{if !empty($blockproducts)}
    {if $blockproducts.products && $blockproducts.products|@count > 0}
		{include file="$tpl_dir./product-list/product-list-simple-paralax.tpl" products=$blockproducts.products parallaxbg=$blockproducts.image }
	{else}
		<p>{l s='No products at this time' mod='xprtblockproductdisplay'}</p>
	{/if}
{/if}