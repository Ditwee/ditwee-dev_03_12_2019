<div class="kr_lookbook_layout_area">
	<div class="kr_lookbook_layout_inner">
		{if isset($blockproducts.title) && !empty($blockproducts.title)}
			<div class="kr_lookbook_layout_heading">
				<h3 class="page-heading">
					<em>{$blockproducts.title}</em>
				</h3>
				<p class="page_subtitle">{$blockproducts.subtitle}</p>
			</div>
		{/if}
		{if $blockproducts.products && $blockproducts.products|@count > 0}
		<ul class="product_list grid {$blockproducts.slider_style} p_left_15 p_right_15 {if $xprt.gray_image_bg == 1}gray_image_bg{/if} {$xprt.prod_grid_qw_style} clearfix">
			{foreach from=$blockproducts.products item=product name=myLoop}
			<li class="ajax_block_product col-sm-6 col-sm-3 col-md-4 col-lg-4">
				<div class="product-container">
					<div class="left-block">
						<div class="product-image-container">
							<div class="product_img_link">
								<a href="{$product.link|escape:'html':'UTF-8'}">
							        <img class="{if $xprt.prod_grid_img_style == 'prod_grid_img_style_multi'}multi_img{elseif $xprt.prod_grid_img_style == 'prod_grid_img_style_hover'}hover_img{/if} replace-2x img-responsive lazy" data-imgs-attr="{hook h='productgridmultiimgs' product=$product count="{if $xprt.prod_grid_img_style == 'prod_grid_img_style_multi'}false{else}true{/if}" img_type=$blockproducts.image_type}" 
							        data-echo="{$link->getImageLink($product.link_rewrite, $product.id_image, $blockproducts.image_type)|escape:'html':'UTF-8'}" src="{$img_dir}kr_loader.gif"
							        alt="{$product.legend|escape:'html':'UTF-8'}" 
							        title="{$product.name|escape:'html':'UTF-8'}" />
							    </a>
							</div>
							{if isset($product.new) && $product.new == 1}
								<a class="new-box" href="{$product.link|escape:'html':'UTF-8'}">
									<span class="new-label">{l s='New' mod='xprtblockproductdisplay'}</span>
								</a>
							{/if}
							{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
								<a class="sale-box" href="{$product.link|escape:'html':'UTF-8'}">
									<span class="sale-label">{l s='Sale!' mod='xprtblockproductdisplay'}</span>
								</a>
							{/if}
							<div class="functional-buttons clearfix">
								{if isset($quick_view) && $quick_view}
									<div class="quick-view-wrapper">
										<a class="quick-view {if $xprt.prod_grid_qw_style == 'quickwish_square'}tooltiptop" title="{l s='Quick view' mod='xprtblockproductdisplay'}{/if}" href="{$product.link|escape:'html':'UTF-8'}" rel="{$product.link|escape:'html':'UTF-8'}">
											<span>{l s='Quick view' mod='xprtblockproductdisplay'}</span>
										</a>
									</div>
								{/if}
								{* wishlist hook *}
								{hook h='displayProductListFunctionalButtons' product=$product}
							</div>
						</div>
					</div>
					<div class="right-block">
						<h5>
							<a class="product-name" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}">
					            {$product.name|escape:'html':'UTF-8'}
					        </a>
					    </h5>
					    {if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
					    <div class="content_price">
					    	{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
					    		{hook h="displayProductPriceBlock" product=$product type='before_price'}
					    		<span class="price product-price">
					    			{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
					    		</span>
					    		{if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
					    			{hook h="displayProductPriceBlock" product=$product type="old_price"}
					    			<span class="old-price product-price">
					    				{displayWtPrice p=$product.price_without_reduction}
					    			</span>
					    			{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
					    			{if $product.specific_prices.reduction_type == 'percentage'}
					    				<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}%</span>
					    			{/if}
					    		{/if}
					    		{hook h="displayProductPriceBlock" product=$product type="price"}
					    		{hook h="displayProductPriceBlock" product=$product type="unit_price"}
					    		{hook h="displayProductPriceBlock" product=$product type='after_price'}
					    	{/if}
					    </div>
					    {/if}
						
						{if $xprt.prd_list_addtocart == 1}
					    <div class="button-container">
					    	{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}
					    		{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}
					    			{capture}add=1&amp;id_product={$product.id_product|intval}{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
					    			<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='xprtblockproductdisplay'}" data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
					    				<span>{l s='Add to cart' mod='xprtblockproductdisplay'}</span>
					    			</a>
					    		{else}
					    			<span class="button ajax_add_to_cart_button btn btn-default disabled">
					    				<span>{l s='Add to cart' mod='xprtblockproductdisplay'}</span>
					    			</span>
					    		{/if}
					    	{/if}
					    	<a class="button lnk_view btn btn-default" href="{$product.link|escape:'html':'UTF-8'}" title="{l s='View' mod='xprtblockproductdisplay'}">
					    		<span>{if (isset($product.customization_required) && $product.customization_required)}{l s='Customize' mod='xprtblockproductdisplay'}{else}{l s='More' mod='xprtblockproductdisplay'}{/if}</span>
					    	</a>
					    </div>
						{/if}
					</div>
				</div>
			</li>
			{/foreach}
		</ul>
		{/if}
	</div>
</div>