<div class="block carousel">
	<h4>{$blockproducts.title}</h4>
	<div class="block_content products-block">
	{if $blockproducts.products && $blockproducts.products|@count > 0}
		<ul>
			{foreach from=$blockproducts.products item=product name=myLoop}
				<li class="clearfix">
					<a href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}" class="products-block-image content_img clearfix">
						<img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'cart_default')|escape:'html'}" alt="{$product.legend|escape:'html':'UTF-8'}" />
					</a>
					<div class="product-content">
	                	<h5>
	                    	<a class="product-name" href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}">
	                            {$product.name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}
	                        </a>
	                    </h5>
	                    <p class="product_category">{$product.category|truncate:45:'...'|escape:'html':'UTF-8'}</p>
	                    {if !$PS_CATALOG_MODE}
	                        <div class="price-box">
	                            <span class="price">{$product.price}</span>
	                        </div>
	                    {/if}
	                </div>
				</li>
			{/foreach}
		</ul>
	{else}
		<p>{l s='No products at this time' mod='xprtblockproductdisplay'}</p>
	{/if}
	</div>
</div>
<!-- /MODULE Block best sellers -->