<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
include_once _PS_MODULE_DIR_.'xprtblockproductdisplay/classes/blockproductdisplayclass.php';
class xprtblockproductdisplay extends Module
{
	public $tabs_files_url = '/tabs/tabs.php';
	public $mysql_files_url = '/querys/querys.php';
	public $hooks_url = '/hooks/hooks.php';
	public function __construct()
	{
		$this->name = 'xprtblockproductdisplay';
		$this->tab = 'front_office_features';
		$this->version = '2.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		parent::__construct();	
		$this->displayName = $this->l('Great Store Theme Products Display Block');
		$this->description = $this->l('Adds an Products Display Block in any where.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
		 || !$this->Register_Hooks()
		 || !$this->Register_Tabs()
		 || !$this->Register_SQL()
		 || !$this->xpertsampledata()
		)
			return false;
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
		 || !$this->UnRegister_Hooks()
		 || !$this->UnRegister_Tabs()
		 || !$this->UnRegister_SQL()
		)
			return false;
		return true;
	}
	public function Register_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
        		$this->registerHook($hook);
        	endforeach;
        	return true;
	}
	public function UnRegister_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
	        		$hook_id = Module::getModuleIdByName($hook);
	        	if(isset($hook_id) && !empty($hook_id))
	        		$this->unregisterHook((int)$hook_id);
        	endforeach;
        	return true;
	}
	public function Register_SQL()
	{
		$querys = array();
		if(file_exists(dirname(__FILE__).$this->mysql_files_url)){
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($querys) && !empty($querys))
				foreach($querys as $query){
					if(!Db::getInstance()->Execute($query,false))
					    return false;
				}
		}
        return true;
	}
	public function UnRegister_SQL()
	{
		$querys_u = array();
		if(file_exists(dirname(__FILE__).$this->mysql_files_url)){
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($querys_u) && !empty($querys_u))
				foreach($querys_u as $query_u){
					if(!Db::getInstance()->Execute($query_u,false))
					    return false;
				}
		}
        return true;
	}
	public function UnRegister_Tabs()
    {
        $tabs_lists = array();
        require_once(dirname(__FILE__) .$this->tabs_files_url);
        if(isset($tabs_lists) && !empty($tabs_lists)){
        	foreach($tabs_lists as $tab_list){
        	    $tab_list_id = Tab::getIdFromClassName($tab_list['class_name']);
        	    if(isset($tab_list_id) && !empty($tab_list_id)){
        	        $tabobj = new Tab($tab_list_id);
        	        $tabobj->delete();
        	    }
        	}
        } 
        $save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
        if($save_tab_id != 0){
        	$count = Tab::getNbTabs($save_tab_id);
        	if($count == 0){
        		if(isset($save_tab_id) && !empty($save_tab_id)){
        		    $tabobjs = new Tab($save_tab_id);
        		    $tabobjs->delete();
        		}
        	}
        }
        return true;
    }
	public function RegisterParentTabs(){
    	$langs = Language::getLanguages();
    	$save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
    	if($save_tab_id != 0){
    		return $save_tab_id;
    	}else{
    		$tab_listobj = new Tab();
    		$tab_listobj->class_name = 'Adminxprtdashboard';
    		$tab_listobj->id_parent = 0;
    		$tab_listobj->module = $this->name;
    		foreach($langs as $l)
    		{
    		    $tab_listobj->name[$l['id_lang']] = $this->l("Theme Settings");
    		}
    		if($tab_listobj->save())
    			return (int)$tab_listobj->id;
    		else
    			return (int)$save_tab_id;
    	}
    }
    public function Register_Tabs()
    {
        $tabs_lists = array();
        $langs = Language::getLanguages();
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $save_tab_id = $this->RegisterParentTabs();
            require_once(dirname(__FILE__) .$this->tabs_files_url);
            if(isset($tabs_lists) && !empty($tabs_lists))
	            foreach ($tabs_lists as $tab_list)
	            {
	                $tab_listobj = new Tab();
	                $tab_listobj->class_name = $tab_list['class_name'];
	                if($tab_list['id_parent'] == 'parent'){
	                    $tab_listobj->id_parent = $save_tab_id;
	                }else{
	                    $tab_listobj->id_parent = $tab_list['id_parent'];
	                }
	                if(isset($tab_list['module']) && !empty($tab_list['module'])){
	                    $tab_listobj->module = $tab_list['module'];
	                }else{
	                    $tab_listobj->module = $this->name;
	                }
	                foreach($langs as $l)
	                {
	                    $tab_listobj->name[$l['id_lang']] = $this->l($tab_list['name']);
	                }
	                $tab_listobj->save();
	            }
        return true;
    }
    public function hookexecute($hook)
	{
		$blckprdctdisplay = blockproductdisplayclass::GetProductBlock($hook);
		$this->context->smarty->assign(array('blckprdctdisplay' => $blckprdctdisplay));
		return $this->display(__FILE__,'views/templates/front/xprtblockproductdisplay.tpl');
	}
	public static function PageException($exceptions = NULL)
	{
		if($exceptions == NULL)
			return false;
		$exceptions = explode(",",$exceptions);
		$page_name = Context::getContext()->controller->php_self;
		$this_arr = array();
		$this_arr[] = 'all_page';
		$this_arr[] = $page_name;
		if($page_name == 'category'){
			$id_category = Tools::getvalue('id_category');
			$this_arr[] = 'cat_'.$id_category;
		}elseif($page_name == 'product'){
			$id_product = Tools::getvalue('id_product');
			$this_arr[] = 'prd_'.$id_product;
			// Start Get Product Category
			$prd_cat_sql = 'SELECT cp.`id_category` AS id
			    FROM `'._DB_PREFIX_.'category_product` cp
			    LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = cp.id_category)
			    '.Shop::addSqlAssociation('category', 'c').'
			    WHERE cp.`id_product` = '.(int)$id_product;
			$prd_catresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_cat_sql);
			if(isset($prd_catresults) && !empty($prd_catresults))
			{
			    foreach($prd_catresults as $prd_catresult)
			    {
			        $this_arr[] = 'prdcat_'.$prd_catresult['id'];
			    }
			}
			// END Get Product Category
			// Start Get Product Manufacturer
			$prd_man_sql = 'SELECT `id_manufacturer` AS id FROM `'._DB_PREFIX_.'product` WHERE `id_product` = '.(int)$id_product;
			$prd_manresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_man_sql);
			if(isset($prd_manresults) && !empty($prd_manresults))
			{
			    foreach($prd_manresults as $prd_manresult)
			    {
			        $this_arr[] = 'prdman_'.$prd_manresult['id'];
			    }
			}
			// END Get Product Manufacturer
			// Start Get Product SupplierS
			$prd_sup_sql = "SELECT `id_supplier` AS id FROM `"._DB_PREFIX_."product_supplier` WHERE `id_product` = ".(int)$id_product." GROUP BY `id_supplier`";
			$prd_supresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_sup_sql);
			if(isset($prd_supresults) && !empty($prd_supresults))
			{
			    foreach($prd_supresults as $prd_supresult)
			    {
			        $this_arr[] = 'prdsup_'.$prd_supresult['id'];
			    }
			}
			// END Get Product SupplierS
		}elseif($page_name == 'cms'){
			$id_cms = Tools::getvalue('id_cms');
			$this_arr[] = 'cms_'.$id_cms;
		}elseif($page_name == 'manufacturer'){
			$id_manufacturer = Tools::getvalue('id_manufacturer');
			$this_arr[] = 'man_'.$id_manufacturer;
		}elseif($page_name == 'supplier'){
			$id_supplier = Tools::getvalue('id_supplier');
			$this_arr[] = 'sup_'.$id_supplier;
		}
		if(isset($this_arr)){
			foreach ($this_arr as $this_arr_val) {
				if(in_array($this_arr_val,$exceptions))
					return true;
			}
		}
		return false;
	}
	public function hookdisplayTopColumn($params)
	{
		return $this->hookexecute('displayTopColumn');
	}
	public function hookdisplayBanner($params)
	{
		return $this->hookexecute('displayBanner');
	}
	public function hookdisplayFooter($params)
	{
		return $this->hookexecute('displayFooter');
	}
	public function hookdisplayFooterProduct($params)
	{
		return $this->hookexecute('displayFooterProduct');
	}
	public function hookdisplayHome($params)
	{
		return $this->hookexecute('displayHome');
	}
	public function hookdisplayLeftColumn($params)
	{
		return $this->hookexecute('displayLeftColumn');
	}
	public function hookdisplayLeftColumnProduct($params)
	{
		return $this->hookexecute('displayLeftColumnProduct');
	}
	public function hookdisplayMaintenance($params)
	{
		return $this->hookexecute('displayMaintenance');
	}
	public function hookdisplayMyAccountBlock($params)
	{
		return $this->hookexecute('displayMyAccountBlock');
	}
	public function hookdisplayMyAccountBlockfooter($params)
	{
		return $this->hookexecute('displayMyAccountBlockfooter');
	}
	public function hookdisplayNav($params)
	{
		return $this->hookexecute('displayNav');
	}
	public function hookdisplayRightColumn($params)
	{
		return $this->hookexecute('displayRightColumn');
	}
	public function hookdisplayRightColumnProduct($params)
	{
		return $this->hookexecute('displayRightColumnProduct');
	}
	public function hookdisplayTop($params)
	{
		return $this->hookexecute('displayTop');
	}
	public function hookdisplayhometopleft($params)
	{
		return $this->hookexecute('displayhometopleft');
	}
	public function hookdisplayhometopright($params)
	{
		return $this->hookexecute('displayhometopright');
	}
	public function hookdisplayhomefullwidthmiddle($params)
	{
		return $this->hookexecute('displayhomefullwidthmiddle');
	}
	public function hookdisplayHomeFullWidthBottom($params)
	{
		return $this->hookexecute('displayHomeFullWidthBottom');
	}
	public function xpertsampledata($demo=NULL)
	{
		// if(($demo==NULL) || (empty($demo)))
		// 	$demo = "demo_1";
		// $func = 'xpertsample_'.$demo;
		// if(method_exists($this,$func)){
  //       	$this->alldisabled();
  //       	$this->{$func}();
  //       }else{
  //       	$this->alldisabled();
  //       }
        return true;
	}
	public function alldisabled(){
		Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'xprtblckprdctdisplaytbl`;',false);
		Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'xprtblckprdctdisplaytbl_shop`;',false);
		Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'xprtblckprdctdisplaytbl_lang`;',false);
		return true;
	}
	private function xpertsample_demo_6()
	{
		$dummy_datas = array(
		        array(
		            'product_type' => 'new_product',
		            'limit' => '10',
		            'order_by' => 'id_product',
		            'order_way' => 'ASC',
		            'image_type' => 'home_default',
		            'image' => '1.jpg',
		            'layout_style' => 'default',
		            'slider_style' => 'general',
		            'column' => '',
		            'pages' => 'index',
		            'btn_link' => '#',
		            'btn_link_type' => '_blank',
		            'hook' => 'displayhomefullwidthmiddle',
		            'active' => 1,
		            'position' => 0,
		            'title' => 'hot trending in this week',
		            'subtitle' => 'mens clothing',
		            'description' => '',
		            'btn_lbl' => 'shop now',
		        ),
		        array(
		            'product_type' => 'specials_product',
		            'limit' => '10',
		            'order_by' => 'id_product',
		            'order_way' => 'ASC',
		            'image_type' => 'home_default',
		            'image' => '2.jpg',
		            'layout_style' => 'default-reverse',
		            'slider_style' => 'general',
		            'column' => '',
		            'pages' => 'index',
		            'btn_link' => '#',
		            'btn_link_type' => '_blank',
		            'hook' => 'displayhomefullwidthmiddle',
		            'active' => 1,
		            'position' => 1,
		            'title' => 'hot deals with sale up to 50% off from barley',
		            'subtitle' => 'special sale',
		            'description' => '',
		            'btn_lbl' => 'View all',
		        ),
		    );
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
		if(isset($dummy_datas) && !empty($dummy_datas)){
		    $languages = Language::getLanguages(false);
		    $i = 1;
		    foreach($dummy_datas as $valu){

		        $sqldumi2 = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl(`id_xprtblckprdctdisplaytbl`,`product_type`,`limit`,`order_by`,`order_way`,`image_type`,`image`,`layout_style`,`slider_style`,`column`,`pages`,`btn_link`,`btn_link_type`,`hook`,`active`,`position`)VALUES(".$i.",'".$valu['product_type']."','".$valu['limit']."','".$valu['order_by']."','".$valu['order_way']."','".$valu['image_type']."','".$valu['image']."','".$valu['layout_style']."','".$valu['slider_style']."','".$valu['column']."','".$valu['pages']."','".$valu['btn_link']."','".$valu['btn_link_type']."','".$valu['hook']."',".(int)$valu['active'].",".(int)$valu['position'].");";
		            Db::getInstance()->execute($sqldumi2,false);

		            // Start Lang
		        foreach($languages as $language)
		        {
		            $sqldumi = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl_lang(id_xprtblckprdctdisplaytbl,id_lang,title,subtitle,description,btn_lbl)VALUES(".(int)$i.",".(int)$language['id_lang'].",'".$valu['title']."','".$valu['subtitle']."','".$valu['description']."','".$valu['btn_lbl']."');";
		            Db::getInstance()->execute($sqldumi,false);
		        }
		            // End Lang
		            // Start shop
		        $damisqs1 = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl_shop(id_xprtblckprdctdisplaytbl,id_shop)VALUES(".$i.",".$id_shop.");";
		        Db::getInstance()->execute($damisqs1,false); 
		            // End shop
		    $i = $i + 1;    
		    }
		}
		return true;
	}
	private function xpertsample_demo_7()
	{
		$dummy_datas = array(
		        array(
		            'product_type' => 'new_product',
		            'limit' => '8',
		            'order_by' => 'id_product',
		            'order_way' => 'ASC',
		            'image_type' => 'large_default',
		            'image' => '',
		            'layout_style' => 'fullwidth-allproduct',
		            'slider_style' => 'general',
		            'column' => '',
		            'pages' => 'index',
		            'btn_link' => '',
		            'btn_link_type' => '_blank',
		            'hook' => 'displayhomefullwidthmiddle',
		            'active' => 1,
		            'position' => 2,
		            'title' => 'create a difference with jakirop style',
		            'subtitle' => 'Check out the best offers to stay on trend with 1protheme',
		            'description' => '',
		            'btn_lbl' => '',
		        )
		    );
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
		if(isset($dummy_datas) && !empty($dummy_datas)){
		    $languages = Language::getLanguages(false);
		    $i = 1;
		    foreach($dummy_datas as $valu){

		        $sqldumi2 = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl(`id_xprtblckprdctdisplaytbl`,`product_type`,`limit`,`order_by`,`order_way`,`image_type`,`image`,`layout_style`,`slider_style`,`column`,`pages`,`btn_link`,`btn_link_type`,`hook`,`active`,`position`)VALUES(".$i.",'".$valu['product_type']."','".$valu['limit']."','".$valu['order_by']."','".$valu['order_way']."','".$valu['image_type']."','".$valu['image']."','".$valu['layout_style']."','".$valu['slider_style']."','".$valu['column']."','".$valu['pages']."','".$valu['btn_link']."','".$valu['btn_link_type']."','".$valu['hook']."',".(int)$valu['active'].",".(int)$valu['position'].");";
		            Db::getInstance()->execute($sqldumi2,false);

		            // Start Lang
		        foreach($languages as $language)
		        {
		            $sqldumi = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl_lang(id_xprtblckprdctdisplaytbl,id_lang,title,subtitle,description,btn_lbl)VALUES(".(int)$i.",".(int)$language['id_lang'].",'".$valu['title']."','".$valu['subtitle']."','".$valu['description']."','".$valu['btn_lbl']."');";
		            Db::getInstance()->execute($sqldumi,false);
		        }
		            // End Lang
		            // Start shop
		        $damisqs1 = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl_shop(id_xprtblckprdctdisplaytbl,id_shop)VALUES(".$i.",".$id_shop.");";
		        Db::getInstance()->execute($damisqs1,false); 
		            // End shop
		    $i = $i + 1;    
		    }
		}
		return true;
	}
	private function xpertsample_demo_11()
	{
		$dummy_datas = array(
		        array(
		            'product_type' => 'all_product',
		            'limit' => '20',
		            'order_by' => 'id_product',
		            'order_way' => 'ASC',
		            'image_type' => 'thickbox_default',
		            'image' => '',
		            'layout_style' => 'lookbook',
		            'slider_style' => 'carousel',
		            'column' => '',
		            'pages' => 'index',
		            'btn_link' => '',
		            'btn_link_type' => '_blank',
		            'hook' => 'displayhomefullwidthmiddle',
		            'active' => 1,
		            'position' => 1,
		            'title' => '',
		            'subtitle' => '',
		            'description' => '',
		            'btn_lbl' => '',
		        )
		    );
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
		if(isset($dummy_datas) && !empty($dummy_datas)){
		    $languages = Language::getLanguages(false);
		    $i = 1;
		    foreach($dummy_datas as $valu){

		        $sqldumi2 = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl(`id_xprtblckprdctdisplaytbl`,`product_type`,`limit`,`order_by`,`order_way`,`image_type`,`image`,`layout_style`,`slider_style`,`column`,`pages`,`btn_link`,`btn_link_type`,`hook`,`active`,`position`)VALUES(".$i.",'".$valu['product_type']."','".$valu['limit']."','".$valu['order_by']."','".$valu['order_way']."','".$valu['image_type']."','".$valu['image']."','".$valu['layout_style']."','".$valu['slider_style']."','".$valu['column']."','".$valu['pages']."','".$valu['btn_link']."','".$valu['btn_link_type']."','".$valu['hook']."',".(int)$valu['active'].",".(int)$valu['position'].");";
		            Db::getInstance()->execute($sqldumi2,false);
		            // Start Lang
		        foreach($languages as $language)
		        {
		            $sqldumi = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl_lang(id_xprtblckprdctdisplaytbl,id_lang,title,subtitle,description,btn_lbl)VALUES(".(int)$i.",".(int)$language['id_lang'].",'".$valu['title']."','".$valu['subtitle']."','".$valu['description']."','".$valu['btn_lbl']."');";
		            Db::getInstance()->execute($sqldumi,false);
		        }
		            // End Lang
		            // Start shop
		        $damisqs1 = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl_shop(id_xprtblckprdctdisplaytbl,id_shop)VALUES(".$i.",".$id_shop.");";
		        Db::getInstance()->execute($damisqs1,false); 
		            // End shop
		    $i = $i + 1;    
		    }
		}
		return true;
	}
	private function xpertsample_demo_12()
	{
		$dummy_datas = array(
		        array(
		            'product_type' => 'new_product',
		            'limit' => 3,
		            'order_by' => 'id_product',
		            'order_way' => 'ASC',
		            'image_type' => 'cart_default',
		            'image' => '',
		            'layout_style' => 'sidebar',
		            'slider_style' => 'carousel',
		            'column' => '',
		            'pages' => 'index',
		            'btn_link' => '',
		            'btn_link_type' => '_blank',
		            'hook' => 'displayhometopleft',
		            'active' => 1,
		            'position' => 1,
		            'title' => 'Todays Deal',
		            'subtitle' => '',
		            'description' => '',
		            'btn_lbl' => '',
		        ),array(
		            'product_type' => 'new_product',
		            'limit' => 3,
		            'order_by' => 'id_product',
		            'order_way' => 'ASC',
		            'image_type' => 'cart_default',
		            'image' => '',
		            'layout_style' => 'sidebar',
		            'slider_style' => 'carousel',
		            'column' => '',
		            'pages' => 'index',
		            'btn_link' => '',
		            'btn_link_type' => '_blank',
		            'hook' => 'displayhometopleft',
		            'active' => 1,
		            'position' => 2,
		            'title' => 'Treanding',
		            'subtitle' => '',
		            'description' => '',
		            'btn_lbl' => '',
		        )
		    );
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
		if(isset($dummy_datas) && !empty($dummy_datas)){
		    $languages = Language::getLanguages(false);
		    $i = 1;
		    foreach($dummy_datas as $valu){
		        $sqldumi2 = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl(`id_xprtblckprdctdisplaytbl`,`product_type`,`limit`,`order_by`,`order_way`,`image_type`,`image`,`layout_style`,`slider_style`,`column`,`pages`,`btn_link`,`btn_link_type`,`hook`,`active`,`position`)VALUES(".$i.",'".$valu['product_type']."','".$valu['limit']."','".$valu['order_by']."','".$valu['order_way']."','".$valu['image_type']."','".$valu['image']."','".$valu['layout_style']."','".$valu['slider_style']."','".$valu['column']."','".$valu['pages']."','".$valu['btn_link']."','".$valu['btn_link_type']."','".$valu['hook']."',".(int)$valu['active'].",".(int)$valu['position'].");";
		            Db::getInstance()->execute($sqldumi2,false);
		            // Start Lang
		        foreach($languages as $language)
		        {
		            $sqldumi = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl_lang(id_xprtblckprdctdisplaytbl,id_lang,title,subtitle,description,btn_lbl)VALUES(".(int)$i.",".(int)$language['id_lang'].",'".$valu['title']."','".$valu['subtitle']."','".$valu['description']."','".$valu['btn_lbl']."');";
		            Db::getInstance()->execute($sqldumi,false);
		        }
		            // End Lang
		            // Start shop
		        $damisqs1 = "INSERT INTO "._DB_PREFIX_."xprtblckprdctdisplaytbl_shop(id_xprtblckprdctdisplaytbl,id_shop)VALUES(".$i.",".$id_shop.");";
		        Db::getInstance()->execute($damisqs1,false); 
		            // End shop
		    $i = $i + 1;    
		    }
		}
		return true;
	}
}