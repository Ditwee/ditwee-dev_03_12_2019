<?php
class xprtblocksearchsearchModuleFrontController extends ModuleFrontController
{
	public $instant_search;
	public $ajax_search;
	public $id_category;
	public function init()
	{
		parent::init();
		$this->id_category = (Tools::getValue('id_category')) ? Tools::getValue('id_category') : '';
		$this->instant_search = Tools::getValue('instantSearch');
		$this->ajax_search = Tools::getValue('ajaxSearch');
		if($this->instant_search || $this->ajax_search)
		{
			$this->display_header = false;
			$this->display_footer = false;
		}
	}
	public function initContent()
	{
		parent::initContent();
		$query = Tools::replaceAccentedChars(urldecode(Tools::getValue('q')));
		$original_query = Tools::getValue('q');
		if($this->ajax_search)
		{
			$searchResults = Search::find((int)(Tools::getValue('id_lang')), $query, 1, 10, 'position', 'desc', true);
			foreach ($searchResults as &$product):
				// if(isset($this->id_category)){
				// 		$procats = Product::getProductCategories($product['id_product']);
				// 		if(in_array($this->id_category,$procats) || $this->id_category == 0){
				// 			$product['product_link'] = $this->context->link->getProductLink($product['id_product'], $product['prewrite'], $product['crewrite']);
				// 			$id_image = Product::getCover($product['id_product'] );
				// 	        if (sizeof($id_image) > 0)
				// 	        {
				// 	            $image = new Image($id_image['id_image']);
				// 	            $img_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath()."-small_default".".jpg";
				// 	        }
				// 	    	$product['product_cover'] =  $img_url;
				// 		}
				// }else{
							$product['product_link'] = $this->context->link->getProductLink($product['id_product'], $product['prewrite'], $product['crewrite']);
							$id_image = Product::getCover($product['id_product'] );
					        if (sizeof($id_image) > 0)
					        {
					            $image = new Image($id_image['id_image']);
					            $img_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath()."-small_default".".jpg";
					        }
					    	$product['product_cover'] =  $img_url;
					    	$prdprce = Product::getPriceStatic((int)$product['id_product'], true, null, 6, null, false, true, 1, true, null, null, null);
					    	$product['product_price'] =  Tools::displayPrice($prdprce, Context::getContext()->currency);
				// }
			endforeach;
			die(Tools::jsonEncode($searchResults));
		}
		if ($this->instant_search && !is_array($query))
		{
			$this->productSort();
			$this->n = abs((int)(Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'))));
			$this->p = abs((int)(Tools::getValue('p', 1)));
			$search = Search::find($this->context->language->id, $query, 1, 10, 'position', 'desc');
			Hook::exec('actionSearch', array('expr' => $query, 'total' => $search['total']));
			$nbProducts = $search['total'];
			$this->pagination($nbProducts);
			$this->addColorsToProductList($search['result']);
			$this->context->smarty->assign(array(
				'products' => $search['result'], // DEPRECATED (since to 1.4), not use this: conflict with block_cart module
				'search_products' => $search['result'],
				'nbProducts' => $search['total'],
				'search_query' => $original_query,
				'instant_search' => $this->instant_search,
				'homeSize' => Image::getSize(ImageType::getFormatedName('home'))));
		}
		elseif (($query = Tools::getValue('search_query', Tools::getValue('ref'))) && !is_array($query))
		{
			$this->productSort();
			$this->n = abs((int)(Tools::getValue('n', Configuration::get('PS_PRODUCTS_PER_PAGE'))));
			$this->p = abs((int)(Tools::getValue('p', 1)));
			$original_query = $query;
			$query = Tools::replaceAccentedChars(urldecode($query));			
			$search = Search::find($this->context->language->id, $query, $this->p, $this->n, $this->orderBy, $this->orderWay);
			foreach ($search['result'] as &$product)
				$product['link'] .= (strpos($product['link'], '?') === false ? '?' : '&').'search_query='.urlencode($query).'&results='.(int)$search['total'];
			Hook::exec('actionSearch', array('expr' => $query, 'total' => $search['total']));
			$nbProducts = $search['total'];
			$this->pagination($nbProducts);
			$this->addColorsToProductList($search['result']);
			$this->context->smarty->assign(array(
				'products' => $search['result'], // DEPRECATED (since to 1.4), not use this: conflict with block_cart module
				'search_products' => $search['result'],
				'nbProducts' => $search['total'],
				'search_query' => $original_query,
				'homeSize' => Image::getSize(ImageType::getFormatedName('home'))));
		}
		elseif (($tag = urldecode(Tools::getValue('tag'))) && !is_array($tag))
		{
			$nbProducts = (int)(Search::searchTag($this->context->language->id, $tag, true));
			$this->pagination($nbProducts);
			$result = Search::searchTag($this->context->language->id, $tag, false, $this->p, $this->n, $this->orderBy, $this->orderWay);
			Hook::exec('actionSearch', array('expr' => $tag, 'total' => count($result)));
			$this->addColorsToProductList($result);
			$this->context->smarty->assign(array(
				'search_tag' => $tag,
				'products' => $result, // DEPRECATED (since to 1.4), not use this: conflict with block_cart module
				'search_products' => $result,
				'nbProducts' => $nbProducts,
				'homeSize' => Image::getSize(ImageType::getFormatedName('home'))));
		}
		else
		{
			$this->context->smarty->assign(array(
				'products' => array(),
				'search_products' => array(),
				'pages_nb' => 1,
				'nbProducts' => 0));
		}
		$this->context->smarty->assign(array('add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'), 'comparator_max_item' => Configuration::get('PS_COMPARATOR_MAX_ITEM')));
		$this->setTemplate('search.tpl');
	}
	public function displayHeader($display = true)
	{
		if (!$this->instant_search && !$this->ajax_search)
			parent::displayHeader();
		else
			$this->context->smarty->assign('static_token', Tools::getToken(false));
	}
	public function displayFooter($display = true)
	{
		if (!$this->instant_search && !$this->ajax_search)
			parent::displayFooter();
	}
	public function setMedia()
	{
		parent::setMedia();
		if (!$this->instant_search && !$this->ajax_search)
			$this->addCSS(_THEME_CSS_DIR_.'product_list.css');
	}
}
