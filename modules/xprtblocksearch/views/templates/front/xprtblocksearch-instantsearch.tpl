{if $instantsearch}
	<script type="text/javascript">
	// <![CDATA[
		function tryToCloseInstantSearch() {
			if ($('#old_center_column').length > 0)
			{
				$('#center_column').remove();
				$('#old_center_column').attr('id', 'center_column');
				$('#center_column').show();
				return false;
			}
		}
		
		instantSearchQueries = new Array();
		function stopInstantSearchQueries(){
			for(i=0;i<instantSearchQueries.length;i++) {
				instantSearchQueries[i].abort();
			}
			instantSearchQueries = new Array();
		}
		
		$("#search_query_{$xprtblocksearch_type}").keyup(function(){
			if($(this).val().length > 0){
				stopInstantSearchQueries();
				instantSearchQuery = $.ajax({
					url: '{$link->getModuleLink("xprtblocksearch","search")|addslashes}',
					data: {
						instantSearch: 1,
						id_lang: {$cookie->id_lang},
						q: $(this).val()
					},
					dataType: 'html',
					type: 'POST',
					success: function(data){
						if($("#search_query_{$xprtblocksearch_type}").val().length > 0)
						{
							tryToCloseInstantSearch();
							$('#center_column').attr('id', 'old_center_column');
							$('#old_center_column').after('<div id="center_column" class="' + $('#old_center_column').attr('class') + '">'+data+'</div>');
							$('#old_center_column').hide();
							ajaxCart.overrideButtonsInThePage();
							$("#instant_search_results a.close").click(function() {
								$("#search_query_{$xprtblocksearch_type}").val('');
								return tryToCloseInstantSearch();
							});
							return false;
						}
						else
							tryToCloseInstantSearch();
					}
				});
				instantSearchQueries.push(instantSearchQuery);
			}
			else
				tryToCloseInstantSearch();
		});
	// ]]>
	</script>
{/if}
{if $ajaxsearch}
	<script type="text/javascript">
	// <![CDATA[
		$('document').ready( function() {
			$("#search_query_{$xprtblocksearch_type}").autocomplete('{$link->getModuleLink("xprtblocksearch","search")|addslashes}',{
						minChars: 3,
						max: 10,
						width: 236,
						selectFirst: false,
						scroll: false,
						// data: { 'id_category': $('#sid_category').find('option:selected').text() } ,
						dataType: "json",
						formatItem: function(data, i, max, value, term) {
							return value;
						},
						parse: function(data) {
							var mytab = new Array();
							for (var i = 0; i < data.length; i++)
								if(data[i].product_cover != undefined && data[i].product_price != undefined) 
								mytab[mytab.length] = { data: data[i], value: ' <img src="'+ data[i].product_cover + '" /><span class="psrname">' + data[i].pname + ' </span> <span class="psrprice">' + data[i].product_price + ' </span> '};
                                    else
						 		mytab[mytab.length] = { data: data[i], value: ' <span class="psrname">' + data[i].pname + ' </span> '};
							return mytab;
						},
						extraParams: {
							ajaxSearch: 1,
							// id_category: $('#sid_category').find('option:selected').text(),
							id_lang: {$cookie->id_lang}
						}
					}
				)
				.result(function(event, data, formatted) {
					$('#search_query_{$xprtblocksearch_type}').val(data.pname);
					document.location.href = data.product_link;
				})
		});
	// ]]>
	</script>
{/if}