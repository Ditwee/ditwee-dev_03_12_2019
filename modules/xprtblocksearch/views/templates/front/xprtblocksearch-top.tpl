{if isset($hook_mobile)}
	<div class="input_search" data-role="fieldcontain">
		<form method="get" action="{$link->getModuleLink('xprtblocksearch','search')|escape:'html'}" id="searchbox">
			<input type="hidden" name="controller" value="search" />
			<input type="hidden" name="orderby" value="position" />
			<input type="hidden" name="orderway" value="desc" />
			<input class="search_query" type="search" id="search_query_top" name="search_query" placeholder="{l s='Search' mod='xprtblocksearch'}" value="{$search_query_key|escape:'html':'UTF-8'|stripslashes}" />
		</form>
	</div>
{else}
	<div class="xprtblocksearch header_block search_block_top">
		<div class="search_icon"><i class="{if isset($xprt.header_search_icon)}{$xprt.header_search_icon}{else}icon_search{/if}"></i></div>
		<form class="header_block_content" method="get" action="{$link->getModuleLink('xprtblocksearch','search')|escape:'html'}" id="searchbox">
			<div class="search_block_content">
				<input type="hidden" name="controller" value="search" />
				<input type="hidden" name="orderby" value="position" />
				<input type="hidden" name="orderway" value="desc" />
				<input class="search_query" type="text" id="search_query_top" name="search_query" value="{$search_query_key|escape:'html':'UTF-8'|stripslashes}" placeholder="{l s='Search here'  mod='xprtblocksearch'}" />
				<button type="submit" name="submit_search"><i class="arrow_right"></i></button>
			</div>
		</form>
	</div>
{include file="$self_uri./xprtblocksearch-instantsearch.tpl"}
{/if}
