<div id="search_block_left" class="block exclusive">
	<h4 class="title_block">{l s='Search' mod='xprtblocksearch'}</h4>
	<form method="get" action="{$link->getModuleLink('xprtblocksearch','search', true)|escape:'html'}" id="searchbox">
		<p class="block_content">
			<label for="search_query_block">{l s='Search products:' mod='xprtblocksearch'}</label>
			<input type="hidden" name="controller" value="search" />
			<input type="hidden" name="orderby" value="position" />
			<input type="hidden" name="orderway" value="desc" />
			<input class="search_query" type="text" id="search_query_block" name="search_query" value="{$search_query_key|escape:'html':'UTF-8'|stripslashes}" />
			<input type="submit" id="search_button" class="button_mini" value="{l s='Go' mod='xprtblocksearch'}" />
		</p>
	</form>
</div>
{include file="$self_uri./xprtblocksearch-instantsearch.tpl"}