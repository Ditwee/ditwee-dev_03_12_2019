<?php
if (!defined('_PS_VERSION_'))
	exit;
class xprtblocksearch extends Module
{
	public $hooks_url = '/hooks/hooks.php';
	public $css_files_url = '/css/css.php';
	public $js_files_url = '/js/js.php';
	public function __construct()
	{
		$this->name = 'xprtblocksearch';
		$this->tab = 'search_filter';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->controllers = array('search');
		$this->need_instance = 0;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Quick search block');
		$this->description = $this->l('Adds a quick search field to your website.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install() || !$this->Register_Hooks())
			return false;
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall() || !$this->UnRegister_Hooks())
			return false;
		return true;
	}
	public function Register_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
        		$this->registerHook($hook);
        	endforeach;
        	return true;
        return false;
	}
	public function UnRegister_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
	        		$hook_id = Module::getModuleIdByName($hook);
	        	if(isset($hook_id) && !empty($hook_id))
	        		$this->unregisterHook((int)$hook_id);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Css()
	{
		$css_files = array();
        require_once(dirname(__FILE__).$this->css_files_url);
        if(isset($css_files) && !empty($css_files))
        	foreach($css_files as $css_file):
        		$this->context->controller->addCSS($css_file);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Js()
	{
		$js_files = array();
        require_once(dirname(__FILE__).$this->js_files_url);
        if(isset($js_files) && !empty($js_files))
        	foreach($js_files as $js_file):
        		$this->context->controller->addJS($js_file);
        	endforeach;
        	return true;
        return false;
	}
	public function hookdisplayMobileTopSiteMap($params)
	{
		$this->smarty->assign(array('hook_mobile' => true, 'instantsearch' => false));
		$params['hook_mobile'] = true;
		return $this->hookTop($params);
	}
	public function hookHeader($params)
	{
		$this->Register_Css();
		if(Configuration::get('PS_SEARCH_AJAX'))
		{
			$this->context->controller->addJqueryPlugin('autocomplete');
			Media::addJsDef(array('search_url' => $this->context->link->getPageLink('search', Tools::usingSecureMode())));
			$this->Register_Js();
		}
	}
	public function hookLeftColumn($params)
	{
		return $this->hookRightColumn($params);
	}
	public function hookRightColumn($params)
	{
		if(Tools::getValue('search_query_key'))
		{
			$this->calculHookCommon($params);
			$this->smarty->assign(array(
				'xprtblocksearch_type' => 'block',
				'search_query_key' => (string)Tools::getValue('search_query_key')
				)
			);
		}
		Media::addJsDef(array('xprtblocksearch_type' => 'block'));
		return $this->display(__FILE__, 'views/templates/front/xprtblocksearch.tpl');
	}
	public function hookTop($params)
	{
			$this->calculHookCommon($params);
			$this->smarty->assign(array(
				'xprtblocksearch_type' => 'top',
				'search_query_key' => (string)Tools::getValue('search_query_key')
				)
			);
		Media::addJsDef(array('xprtblocksearch_type' => 'top'));
		return $this->display(__FILE__,'views/templates/front/xprtblocksearch-top.tpl');
	}
	public function hookDisplayNav($params)
	{
		return $this->hookTop($params);
	}
	public function hookdisplayTopRightTwo($params)
	{
		return $this->hookTop($params);
	}
	public function hookdisplayTopRightOne($params)
	{
		return $this->hookTop($params);
	}
	private function calculHookCommon($params)
	{
		$id_lang = (int)Context::getcontext()->language->id;
		$cats = Category::getCategories($id_lang,true,false);
		$this->smarty->assign(array(
			'ENT_QUOTES' =>		ENT_QUOTES,
			'allcats' =>		$cats,
			'search_ssl' =>		Tools::usingSecureMode(),
			'ajaxsearch' =>		Configuration::get('PS_SEARCH_AJAX'),
			'instantsearch' =>	Configuration::get('PS_INSTANT_SEARCH'),
			'self_uri' =>			dirname(__FILE__).'/views/templates/front/',
		));
		return true;
	}
}

