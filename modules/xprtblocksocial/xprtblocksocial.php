<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_CAN_LOAD_FILES_'))
	exit;
class xprtblocksocial extends Module
{
	public function __construct()
	{
		$this->name = 'xprtblocksocial';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		parent::__construct();	
		$this->displayName = $this->l('Great Store Social networking block');
		$this->description = $this->l('Allows you to add information about your brand\'s social networking accounts.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			// || !$this->registerHook('displayHeader')
			|| !$this->registerHook('displayNewsletterSocialBlock')
			|| !$this->xpertsampledata()
			)
			return false;
			return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
			)
			return false;
		Configuration::deleteByName('xprtblocksocial_FACEBOOK');
		Configuration::deleteByName('xprtblocksocial_TWITTER');
		Configuration::deleteByName('xprtblocksocial_RSS');
		Configuration::deleteByName('xprtblocksocial_YOUTUBE');
		Configuration::deleteByName('xprtblocksocial_GOOGLE_PLUS');
		Configuration::deleteByName('xprtblocksocial_PINTEREST');
		Configuration::deleteByName('xprtblocksocial_VIMEO');
		Configuration::deleteByName('xprtblocksocial_INSTAGRAM');
		Configuration::deleteByName('xprtblocksocial_column');
		Configuration::deleteByName('xprtblocksocial_float');
			return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
    		$this->{$func}();
	    }else{
	    	$this->xpertsample_demo();
	    }
	    return true;
	}
	public function xpertsample_demo()
	{
		Configuration::updateValue('xprtblocksocial_FACEBOOK', '#');
		Configuration::updateValue('xprtblocksocial_TWITTER', '#');
		Configuration::updateValue('xprtblocksocial_RSS', '#');
		Configuration::updateValue('xprtblocksocial_YOUTUBE', '#');
		Configuration::updateValue('xprtblocksocial_GOOGLE_PLUS', '#');
		Configuration::updateValue('xprtblocksocial_PINTEREST', '#');
		Configuration::updateValue('xprtblocksocial_VIMEO', '#');
		Configuration::updateValue('xprtblocksocial_INSTAGRAM', '#');
		Configuration::updateValue('xprtblocksocial_column', '');
		Configuration::updateValue('xprtblocksocial_float', '');
	}
	public function xpertsample_demo_8()
	{
		Configuration::updateValue('xprtblocksocial_FACEBOOK', '#');
		Configuration::updateValue('xprtblocksocial_TWITTER', '#');
		Configuration::updateValue('xprtblocksocial_RSS', '#');
		Configuration::updateValue('xprtblocksocial_YOUTUBE', '#');
		Configuration::updateValue('xprtblocksocial_GOOGLE_PLUS', '#');
		Configuration::updateValue('xprtblocksocial_PINTEREST', '#');
		Configuration::updateValue('xprtblocksocial_VIMEO', '#');
		Configuration::updateValue('xprtblocksocial_INSTAGRAM', '#');
		Configuration::updateValue('xprtblocksocial_column', '3');
		Configuration::updateValue('xprtblocksocial_float', '');
	}
	public function getContent()
	{
		
		// If we try to update the settings
		$output = '';
		if (Tools::isSubmit('submitModule'))
		{	
			Configuration::updateValue('xprtblocksocial_FACEBOOK', Tools::getValue('xprtblocksocial_facebook', ''));
			Configuration::updateValue('xprtblocksocial_TWITTER', Tools::getValue('xprtblocksocial_twitter', ''));
			Configuration::updateValue('xprtblocksocial_RSS', Tools::getValue('xprtblocksocial_rss', ''));
			Configuration::updateValue('xprtblocksocial_YOUTUBE', Tools::getValue('xprtblocksocial_youtube', ''));
			Configuration::updateValue('xprtblocksocial_GOOGLE_PLUS', Tools::getValue('xprtblocksocial_google_plus', ''));
			Configuration::updateValue('xprtblocksocial_PINTEREST', Tools::getValue('xprtblocksocial_pinterest', ''));
			Configuration::updateValue('xprtblocksocial_VIMEO', Tools::getValue('xprtblocksocial_vimeo', ''));
			Configuration::updateValue('xprtblocksocial_INSTAGRAM', Tools::getValue('xprtblocksocial_instagram', ''));
			Configuration::updateValue('xprtblocksocial_column', Tools::getValue('xprtblocksocial_column', ''));
			Configuration::updateValue('xprtblocksocial_float', Tools::getValue('xprtblocksocial_float', ''));
			$this->_clearCache('xprtblocksocial.tpl');
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&conf=4&module_name='.$this->name);
		}
		
		return $output.$this->renderForm();
	}
	
	public function hookDisplayHeader()
	{
		$this->context->controller->addCSS(($this->_path).'xprtblocksocial.css', 'all');
	}
		
	public function hookDisplayFooter()
	{
		if (!$this->isCached('xprtblocksocial.tpl', $this->getCacheId()))
			$this->smarty->assign(array(
				'facebook_url' => Configuration::get('xprtblocksocial_FACEBOOK'),
				'twitter_url' => Configuration::get('xprtblocksocial_TWITTER'),
				'rss_url' => Configuration::get('xprtblocksocial_RSS'),
				'youtube_url' => Configuration::get('xprtblocksocial_YOUTUBE'),
				'google_plus_url' => Configuration::get('xprtblocksocial_GOOGLE_PLUS'),
				'pinterest_url' => Configuration::get('xprtblocksocial_PINTEREST'),
				'vimeo_url' => Configuration::get('xprtblocksocial_VIMEO'),
				'instagram_url' => Configuration::get('xprtblocksocial_INSTAGRAM'),
				'xprtblocksocial_column' => Configuration::get('xprtblocksocial_column'),
				'xprtblocksocial_float' => Configuration::get('xprtblocksocial_float'),
			));

		return $this->display(__FILE__, 'xprtblocksocial.tpl', $this->getCacheId());
	}
		
	public function hookdisplayNewsletterSocialBlock()
	{
		return $this->hookDisplayFooter();
	}
	
	public function hookdisplaySidebarPanel()
	{
		return $this->hookDisplayFooter();
	}

	public function hookdisplayFooterBottom()
	{
		return $this->hookDisplayFooter();
	}
	public function hookdisplayFooterTop()
	{
		return $this->hookDisplayFooter();
	}
	public function hookdisplayTop()
	{
		return $this->hookDisplayFooter();
	}
	public function hookdisplayNav()
	{
		return $this->hookDisplayFooter();
	}
	public function hookdisplayxprtblocknewsletterBottom()
	{
		return $this->hookDisplayFooter();
	}
	
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Facebook URL'),
						'name' => 'xprtblocksocial_facebook',
						'desc' => $this->l('Your Facebook fan page. Ex: http://facebook.com/{your profile name}'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Twitter URL'),
						'name' => 'xprtblocksocial_twitter',
						'desc' => $this->l('Your official Twitter account. Ex: http://twitter.com/{your profile name}'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('RSS URL'),
						'name' => 'xprtblocksocial_rss',
						'desc' => $this->l('The RSS feed of your choice (your blog, your store, etc.).Ex: http://rss.com/{your profile name}'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('YouTube URL'),
						'name' => 'xprtblocksocial_youtube',
						'desc' => $this->l('Your official YouTube account.Ex: http://youtube.com/{your profile name}'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Google+ URL:'),
						'name' => 'xprtblocksocial_google_plus',
						'desc' => $this->l('Your official Google+ page. Ex: http://plus.google.com/{your profile name}'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Pinterest URL:'),
						'name' => 'xprtblocksocial_pinterest',
						'desc' => $this->l('Your official Pinterest account. Ex: http://pinterest.com/{your profile name}'),
					),
					array(						
						'type' => 'text',						
						'label' => $this->l('Vimeo URL:'),						
						'name' => 'xprtblocksocial_vimeo',						
						'desc' => $this->l('Your official Vimeo account.Ex: http://vimeo.com/{your profile name}'),					
					),
					array(						
						'type' => 'text',						
						'label' => $this->l('Instagram URL:'),						
						'name' => 'xprtblocksocial_instagram',						
						'desc' => $this->l('Your official Instagram account.Ex: http://instagram.com/{your profile name}'),					
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Select column'),
					    'name' => 'xprtblocksocial_column',
					    'default_val' => '4',
					    'desc' => $this->l('Choose colum for this block'),
					    'options' => array(
					    	'id' => 'id',
					    	'name' => 'name',
					    	'query' => array(
					    		array(
					    			'id' => '6',
					    			'name' => 'Column two (col-sm-6)'
					    			),
					    		array(
					    			'id' => '4',
					    			'name' => 'Column three (col-sm-4)'
					    			),
					    		array(
					    			'id' => '3',
					    			'name' => 'Column four (col-sm-3)'
					    			),
					    		array(
					    			'id' => '12',
					    			'name' => 'Column full (col-sm-12)'
					    			),
					    		array(
					    			'id' => 'none',
					    			'name' => 'None'
					    			),
					    		)
					    	)
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Float Align'),
					    'name' => 'xprtblocksocial_float',
					    'default_val' => 'disable',
					    'desc' => $this->l('Choose colum for this block'),
					    'options' => array(
					    	'id' => 'id',
					    	'name' => 'name',
					    	'query' => array(
					    		array(
					    			'id' => 'disable',
					    			'name' => 'Float Disable'
					    			),
					    		array(
					    			'id' => 'f_left',
					    			'name' => 'Float Left'
					    			),
					    		array(
					    			'id' => 'f_right',
					    			'name' => 'Float Right'
					    			),
					    		array(
					    			'id' => 'f_none',
					    			'name' => 'Float none'
					    			),
					    		)
					    	)
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitModule';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		return array(
			'xprtblocksocial_facebook' => Tools::getValue('xprtblocksocial_facebook', Configuration::get('xprtblocksocial_FACEBOOK')),
			'xprtblocksocial_twitter' => Tools::getValue('xprtblocksocial_twitter', Configuration::get('xprtblocksocial_TWITTER')),
			'xprtblocksocial_rss' => Tools::getValue('xprtblocksocial_rss', Configuration::get('xprtblocksocial_RSS')),
			'xprtblocksocial_youtube' => Tools::getValue('xprtblocksocial_youtube', Configuration::get('xprtblocksocial_YOUTUBE')),
			'xprtblocksocial_google_plus' => Tools::getValue('xprtblocksocial_google_plus', Configuration::get('xprtblocksocial_GOOGLE_PLUS')),
			'xprtblocksocial_pinterest' => Tools::getValue('xprtblocksocial_pinterest', Configuration::get('xprtblocksocial_PINTEREST')),
			'xprtblocksocial_vimeo' => Tools::getValue('xprtblocksocial_vimeo', Configuration::get('xprtblocksocial_VIMEO')),
			'xprtblocksocial_instagram' => Tools::getValue('xprtblocksocial_instagram', Configuration::get('xprtblocksocial_INSTAGRAM')),
			'xprtblocksocial_column' => Tools::getValue('xprtblocksocial_column', Configuration::get('xprtblocksocial_column')),
			'xprtblocksocial_float' => Tools::getValue('xprtblocksocial_float', Configuration::get('xprtblocksocial_float')),
		);
	}
}