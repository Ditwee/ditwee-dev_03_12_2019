<!-- Block tags module -->
<div id="tags_block_left" class="xprtblocktags block tags_block col-sm-3">
	<h4 class="title_block">{l s='Popular Tags' mod='xprtblocktags'}</h4>
	<div class="block_content">
		{if $tags}
			<ul>
				{foreach from=$tags item=tag name=myLoop}
					<li class="{if isset($tag.class)}{$tag.class}{/if} {if $smarty.foreach.myLoop.last}last_item{elseif $smarty.foreach.myLoop.first}first_item{else}item{/if}"><a href="{$link->getPageLink('search', true, NULL, "tag={$tag.name|urlencode}")|escape:'html'}" title="{l s='More about' mod='xprtblocktags'} {$tag.name|escape:html:'UTF-8'}">{$tag.name|escape:html:'UTF-8'}</a></li>
				{/foreach}
			</ul>
		{else}
			<p>{l s='No tags have been specified yet.' mod='xprtblocktags'}</p>
		{/if}
	</div>
</div>
<!-- /Block tags module -->