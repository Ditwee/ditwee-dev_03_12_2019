{if $MENU != ''}
	<!-- Menu -->
	<div class="block_top_menu sf-contener clearfix">
		<div class="cat-title hidden-md hidden-lg">{l s="Menu" mod="xprtblocktopmenu"}<i class="icon_menu-square_alt2"></i></div>
		<ul class="sf-menu clearfix menu-content">
			{$MENU}
		</ul>
	</div>
	<!--/ Menu -->
{/if}