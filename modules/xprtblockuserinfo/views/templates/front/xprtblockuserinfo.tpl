<!-- Block user information module NAV  -->
<div class="header_user_info">
	<div class="current">
		<i class="icon-user"></i>
	</div>
	<ul class="toogle_content">
		<li>
			{if isset($is_logged) && $is_logged}
				<a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='xprtblockuserinfo'}">
					{l s='Sign out' mod='xprtblockuserinfo'}
				</a>
			{else}
				<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='xprtblockuserinfo'}">
					{l s='Sign in' mod='xprtblockuserinfo'}
				</a>
			{/if}
		</li>
		<li>
			<a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='View my customer account' mod='xprtblockuserinfo'}" rel="nofollow">{l s='My Account' mod='xprtblockuserinfo'}</a>
		</li>
		<li>
			<a href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='My orders' mod='xprtblockuserinfo'}" rel="nofollow">{l s='My orders' mod='xprtblockuserinfo'}</a>
		</li>
		{if isset($returnAllowed) && $returnAllowed}
			<li>
				<a href="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" title="{l s='My merchandise returns' mod='xprtblockuserinfo'}" rel="nofollow">{l s='My merchandise returns' mod='xprtblockuserinfo'}</a>
			</li>
		{/if}
		<li>
			<a href="{$link->getPageLink('order-slip', true)|escape:'html':'UTF-8'}" title="{l s='My credit slips' mod='xprtblockuserinfo'}" rel="nofollow">{l s='My credit slips' mod='xprtblockuserinfo'}</a>
		</li>
		<li>
			<a href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='My addresses' mod='xprtblockuserinfo'}" rel="nofollow">{l s='My addresses' mod='xprtblockuserinfo'}</a>
		</li>
		{if isset($voucherAllowed) && $voucherAllowed}
			<li>
				<a href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}" title="{l s='My vouchers' mod='xprtblockuserinfo'}" rel="nofollow">{l s='My vouchers' mod='xprtblockuserinfo'}</a>
			</li>
		{/if}
		<li>
			<a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="{l s='Manage my personal information' mod='xprtblockuserinfo'}" rel="nofollow">{l s='My personal info' mod='xprtblockuserinfo'}</a>
		</li>
	</ul>
</div>