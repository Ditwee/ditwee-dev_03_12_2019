<!-- Block user information module NAV xprt -->
<div class="xprtblockuserinfo header_nav_element header_user_info">
	<h4 class="title">{l s='Account' mod='xprtblockuserinfo'}</h4>
	<div class="header_nav_element_content">
		<ul>
			<li>
				<a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='View my customer account' mod='xprtblockuserinfo'}" rel="nofollow">
					<i class="icon-user"></i>
					{l s='My account' mod='xprtblockuserinfo'}
				</a>
			</li>
			<li>
				<a 	href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|escape:'html':'UTF-8'}" title="{l s='My wishlists' mod='blockwishlist'}">
					<i class="icon-heart"></i>
					{l s='My wishlist' mod='xprtblockuserinfo'}
				</a>
			</li>
			<li>
				<a 	href="{$link->getPageLink('products-comparison')|escape:'html':'UTF-8'}" title="{l s='Product Comparison' mod='xprtblockuserinfo'}">
					<i class="icon-retweet"></i>
					{l s='Product Comparison' mod='xprtblockuserinfo'}
				</a>
			</li>
			<li>
				<a href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='My orders' mod='xprtblockuserinfo'}" rel="nofollow">
					<i class="icon-paper-plane-o"></i>
					{l s='Track orders' mod='xprtblockuserinfo'}
				</a>
			</li>
			{if isset($returnAllowed) && $returnAllowed}
				<li>
					<a href="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" title="{l s='My merchandise returns' mod='xprtblockuserinfo'}" rel="nofollow">
						<i class="icon-credit-card"></i>
						{l s='My merchandise returns' mod='xprtblockuserinfo'}
					</a>
				</li>
			{/if}
			<li>
				<a href="{$link->getPageLink('order-slip', true)|escape:'html':'UTF-8'}" title="{l s='My credit slips' mod='xprtblockuserinfo'}" rel="nofollow">
					<i class="icon-money"></i>
					{l s='My credit slips' mod='xprtblockuserinfo'}
				</a>
			</li>
			<li>
				<a href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='My addresses' mod='xprtblockuserinfo'}" rel="nofollow">
					<i class="icon-thumb-tack"></i>
					{l s='My addresses' mod='xprtblockuserinfo'}
				</a>
			</li>
			{if isset($voucherAllowed) && $voucherAllowed}
				<li>
					<a href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}" title="{l s='My vouchers' mod='xprtblockuserinfo'}" rel="nofollow">
						<i class="icon-share"></i>
						{l s='My vouchers' mod='xprtblockuserinfo'}
					</a>
				</li>
			{/if}
			<li>
				{if isset($is_logged) && $is_logged}
					<a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='xprtblockuserinfo'}">
						<i class="icon-lock"></i>
						{l s='Sign out' mod='xprtblockuserinfo'}
					</a>
				{else}
					<a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log in to your customer account' mod='xprtblockuserinfo'}">
						<i class="icon-unlock-alt"></i>
						{l s='Sign in' mod='xprtblockuserinfo'}
					</a>
				{/if}
			</li>
		</ul>
	</div>
</div>