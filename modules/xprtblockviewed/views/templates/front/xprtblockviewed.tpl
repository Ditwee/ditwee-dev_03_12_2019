<div id="xprtblockviewed" class="xprtblockviewed kr_blockviewed kr_products_block m_bottom_30">
	<div class="page_title_area {$xprt.home_title_style}">
		{if isset($xprt_products_viewed_title)}
			<h3 class="page-heading">
				<em>{$xprt_products_viewed_title}</em>
				<div class="heading_carousel_arrow"></div>
			</h3>
		{/if}
		{if isset($xprt_products_viewed_subtitle)}
			<p class="page_subtitle d_none">{$xprt_products_viewed_subtitle}</p>
		{/if}
		<div class="heading-line d_none"><span></span></div>
	</div>
	{if isset($productsViewedObj) && $productsViewedObj}
	    {include file="$tpl_dir./product-list.tpl" products=$productsViewedObj class="xprtblockviewed carousel"}
	{/if}
</div>
<script type="text/javascript">
	var xprtblockviewedproductblock = $("#xprtblockviewed");
	var sliderSelect = xprtblockviewedproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtblockviewedproductblock.find('.heading_carousel_arrow'); 
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: true,
		autoplay: false,
		pauseOnHover: true,
		dots: false,
		arrows: true,
		appendArrows: arrowSelect,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: 1,
		// slidesPerRow: 3,
		slidesToShow : {$xprt_products_slider_nbr},
		slidesToScroll : 1,
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {$xprt_products_slider_nbr},
					slidesToScroll : 1,
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: 4,
					slidesToScroll : 1,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: 3,
					slidesToScroll : 1,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: 2,
					slidesToScroll : 1,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>