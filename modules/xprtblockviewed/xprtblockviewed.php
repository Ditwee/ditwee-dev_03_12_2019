<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class xprtblockviewed extends Module
{
	public function __construct()
	{
		$this->name = 'xprtblockviewed';
		$this->tab = 'front_office_features';
		$this->version = '1.2.3';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Viewed products block');
		$this->description = $this->l('Adds a block displaying recently viewed products.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->registerHook('header')
			|| !$this->registerHook('displayCatBottom')
			)
		return false;
			Configuration::updateValue('xprt_PRODUCTS_VIEWED_NBR',6);
			Configuration::updateValue('xprt_PRODUCTS_VIEWED_TITLE', 'Recently Viewed');
			Configuration::updateValue('xprt_PRODUCTS_VIEWED_SUBTITLE', 'Updated recent products daily');
			Configuration::updateValue('xprt_PRODUCTS_SLIDER_NBR', 5);
			Configuration::updateValue('xprt_PRODUCTS_VIEWED_DESIGN', 'normal');
		return true;
	}
	public function getContent()
	{
		$output = '';
		if (Tools::isSubmit('submitxprtblockviewed'))
		{
			Configuration::updateValue('xprt_PRODUCTS_VIEWED_NBR', (int)Tools::getValue('xprt_PRODUCTS_VIEWED_NBR'));
			Configuration::updateValue('xprt_PRODUCTS_VIEWED_TITLE', Tools::getValue('xprt_PRODUCTS_VIEWED_TITLE'));
			Configuration::updateValue('xprt_PRODUCTS_VIEWED_SUBTITLE', Tools::getValue('xprt_PRODUCTS_VIEWED_SUBTITLE'));
			Configuration::updateValue('xprt_PRODUCTS_SLIDER_NBR', Tools::getValue('xprt_PRODUCTS_SLIDER_NBR'));
			Configuration::updateValue('xprt_PRODUCTS_VIEWED_DESIGN', Tools::getValue('xprt_PRODUCTS_VIEWED_DESIGN'));
			$output .= $this->displayConfirmation($this->l('Settings updated.'));
		}
		return $output.$this->renderForm();
	}
	public function PrepHook()
	{
		if(isset(Context::getcontext()->cookie->viewed))
		{
			$prd_ids = Context::getcontext()->cookie->viewed;
			$productsViewedObj = self::getProductsByIDS($prd_ids);
			$this->smarty->assign(array(
				'productsViewedObj' => $productsViewedObj,
				'mediumSize' => Image::getSize('medium'),
				'xprt_products_viewed_title' => Configuration::get('xprt_PRODUCTS_VIEWED_TITLE'),
				'xprt_products_viewed_subtitle' => Configuration::get('xprt_PRODUCTS_VIEWED_SUBTITLE'),
				'xprt_products_slider_nbr' => Configuration::get('xprt_PRODUCTS_SLIDER_NBR'),
				'xprt_products_viewed_design' => Configuration::get('xprt_PRODUCTS_VIEWED_DESIGN'),
				)
			);
			return $this->display(__FILE__, 'views/templates/front/xprtblockviewed.tpl');
		}else
			return false;
	}
	public function hookRightColumn($params)
	{
		$this->PrepHook();
	}
	public function hookLeftColumn($params)
	{
		return $this->PrepHook();
	}
	public function hookdisplayCatBottom($params)
	{
		return $this->PrepHook();
	}
	public function hookFooter($params)
	{
		$this->PrepHook();
	}
	public function hookHeader($params)
	{
		$id_product = (int)Tools::getValue('id_product');
		$productsViewed = (isset($params['cookie']->viewed) && !empty($params['cookie']->viewed)) ? array_slice(array_reverse(explode(',', $params['cookie']->viewed)), 0, Configuration::get('xprt_PRODUCTS_VIEWED_NBR')) : array();
		if ($id_product && !in_array($id_product, $productsViewed))
		{
			$product = new Product((int)$id_product);
			if ($product->checkAccess((int)$this->context->customer->id))
			{
				if (isset($params['cookie']->viewed) && !empty($params['cookie']->viewed))
					$params['cookie']->viewed .= ','.(int)$id_product;
				else
					$params['cookie']->viewed = (int)$id_product;
			}
		}
		$this->context->controller->addCSS(($this->_path).'xprtblockviewed.css', 'all');
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Products to display'),
						'name' => 'xprt_PRODUCTS_VIEWED_NBR',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Define the number of products displayed in this block.')
					),
					array(
						'type' => 'text',
						'label' => $this->l('Title'),
						'name' => 'xprt_PRODUCTS_VIEWED_TITLE',
						// 'class' => 'fixed-width-xs',
						'desc' => $this->l('Enter title for product viewed')
					),
					array(
						'type' => 'text',
						'label' => $this->l('Sub Title'),
						'name' => 'xprt_PRODUCTS_VIEWED_SUBTITLE',
						// 'class' => 'fixed-width-xs',
						'desc' => $this->l('Enter subtitle for product viewed')
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Design style'),
					    'name' => 'xprt_PRODUCTS_VIEWED_DESIGN',
					    'default_val' => 'normal',
					    'options' => array(
					        'query' => array(
					        	array(
					        	    'id'=>'normal',
					        	    'name'=>'Normal',
					        	),
					            array(
					                'id'=>'regular',
					                'name'=>'Regular',
					            ),
					        ),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Number of product show on slider'),
						'name' => 'xprt_PRODUCTS_SLIDER_NBR',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Define the number of products displayed in this block.')
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitxprtblockviewed';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		return array(
			'xprt_PRODUCTS_VIEWED_NBR' => Tools::getValue('xprt_PRODUCTS_VIEWED_NBR', Configuration::get('xprt_PRODUCTS_VIEWED_NBR')),
			'xprt_PRODUCTS_VIEWED_TITLE' => Tools::getValue('xprt_PRODUCTS_VIEWED_TITLE', Configuration::get('xprt_PRODUCTS_VIEWED_TITLE')),
			'xprt_PRODUCTS_VIEWED_SUBTITLE' => Tools::getValue('xprt_PRODUCTS_VIEWED_SUBTITLE', Configuration::get('xprt_PRODUCTS_VIEWED_SUBTITLE')),
			'xprt_PRODUCTS_SLIDER_NBR' => Tools::getValue('xprt_PRODUCTS_SLIDER_NBR', Configuration::get('xprt_PRODUCTS_SLIDER_NBR')),
			'xprt_PRODUCTS_VIEWED_DESIGN' => Tools::getValue('xprt_PRODUCTS_VIEWED_DESIGN', Configuration::get('xprt_PRODUCTS_VIEWED_DESIGN')),
		);
	}
	public static function getProductsByIDS($prd_ids = '')
	{
        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`, product_attribute_shop.id_product_attribute,
			pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`,
			pl.`name`, image_shop.`id_image`, il.`legend`, m.`name` AS manufacturer_name
		FROM `' . _DB_PREFIX_ . 'product` p
		' . Shop::addSqlAssociation('product', 'p') . '
		LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute pa ON (pa.id_product = p.id_product)
		' . Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.default_on=1') . '
		' . Product::sqlStock('p', 0, false) . '
		LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
			p.`id_product` = pl.`id_product`
			AND pl.`id_lang` = ' . (int) Context::getContext()->language->id . Shop::addSqlRestrictionOnLang('pl') . '
		)
		LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_product` = p.`id_product`)' .
                Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1') . '
		LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int) Context::getContext()->language->id . ')
		LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
		WHERE  p.`id_product` IN(' . $prd_ids . ')
                        AND product_shop.`active` = 1
		AND product_shop.`show_price` = 1
		AND ((image_shop.id_image IS NOT NULL OR i.id_image IS NULL) OR (image_shop.id_image IS NULL AND i.cover=1))
		AND (pa.id_product_attribute IS NULL OR product_attribute_shop.default_on = 1) LIMIT '.(int)Configuration::get('xprt_PRODUCTS_VIEWED_NBR');
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        $allproducts = Product::getProductsProperties((int) Context::getContext()->language->id,$result);
	    return $allproducts;
	}
}
