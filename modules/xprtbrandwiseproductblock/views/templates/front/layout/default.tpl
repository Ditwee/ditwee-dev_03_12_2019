{if isset($xprtbrandwiseproductblock) && !empty($xprtbrandwiseproductblock)}
	{if isset($xprtbrandwiseproductblock.device)}
		{assign var=device_data value=$xprtbrandwiseproductblock.device|json_decode:true}
	{/if}
	<div id="xprtbrandwiseproductblock_{$xprtbrandwiseproductblock.id_xprtbrandwiseproductblock}" class="xprtbrandwiseproductblock xprt_default_products_block" style="margin:{$xprtbrandwiseproductblock.section_margin};">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtbrandwiseproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtbrandwiseproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtbrandwiseproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtbrandwiseproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		{if isset($xprtbrandwiseproductblock) && $xprtbrandwiseproductblock}
			<div id="xprt_brandwiseproductsblock_{$xprtbrandwiseproductblock.id_xprtbrandwiseproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtbrandwiseproductblock.products id='' class="xprt_specialproductsblock {if $xprtbrandwiseproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No special products at this time.' mod='xprtbrandwiseproductblock'}</p>
			</div>
		{/if}
	</div>
	<script type="text/javascript">
	var xprtbrandwiseproductblock = $("#xprtbrandwiseproductblock_{$xprtbrandwiseproductblock.id_xprtbrandwiseproductblock}");
	var sliderSelect = xprtbrandwiseproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtbrandwiseproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtbrandwiseproductblock.nav_arrow_style) && ($xprtbrandwiseproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}

	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtbrandwiseproductblock.play_again)}{$xprtbrandwiseproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtbrandwiseproductblock.autoplay)}{$xprtbrandwiseproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtbrandwiseproductblock.pause_on_hover)}{$xprtbrandwiseproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtbrandwiseproductblock.navigation_dots)}{$xprtbrandwiseproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtbrandwiseproductblock.navigation_arrow)}{$xprtbrandwiseproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtbrandwiseproductblock.product_rows)}{$xprtbrandwiseproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtbrandwiseproductblock.product_scroll) && ($xprtbrandwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtbrandwiseproductblock.product_scroll) && ($xprtbrandwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtbrandwiseproductblock.product_scroll) && ($xprtbrandwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtbrandwiseproductblock.product_scroll) && ($xprtbrandwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtbrandwiseproductblock.product_scroll) && ($xprtbrandwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}