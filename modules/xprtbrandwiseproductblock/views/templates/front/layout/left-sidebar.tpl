{if isset($xprtbrandwiseproductblock) && !empty($xprtbrandwiseproductblock)}
	{if isset($xprtbrandwiseproductblock.device)}
		{assign var=device_data value=$xprtbrandwiseproductblock.device|json_decode:true}
	{/if}
	<div class="xprtbrandwiseproductblock block carousel">
		<h4 class="title_block">
	    	{$xprtbrandwiseproductblock.title}
	    </h4>
	    <div class="block_content">
	        {if isset($xprtbrandwiseproductblock) && $xprtbrandwiseproductblock}
	        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtbrandwiseproductblock.products }
	        {else}
	        	<p class="alert alert-info">{l s='No products at this time.' mod='xprtbrandwiseproductblock'}</p>
	        {/if}
	    </div>
	</div>
{/if}