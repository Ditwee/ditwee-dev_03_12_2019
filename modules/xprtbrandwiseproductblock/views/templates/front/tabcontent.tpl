{if isset($xprtbrandwiseproductblock) && !empty($xprtbrandwiseproductblock)}
	{if isset($xprtbrandwiseproductblock.device)}
		{assign var=device_data value=$xprtbrandwiseproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtbrandwiseproductblock) && $xprtbrandwiseproductblock}
		<div id="xprt_brandwiseproductsblock_tab_{if isset($xprtbrandwiseproductblock.id_xprtbrandwiseproductblock)}{$xprtbrandwiseproductblock.id_xprtbrandwiseproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtbrandwiseproductblock.products class='xprt_specialproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_brandwiseproductsblock_tab_{if isset($xprtbrandwiseproductblock.id_xprtbrandwiseproductblock)}{$xprtbrandwiseproductblock.id_xprtbrandwiseproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No products at this time.' mod='xprtbrandwiseproductblock'}</p>
		</div>
	{/if}
{/if}