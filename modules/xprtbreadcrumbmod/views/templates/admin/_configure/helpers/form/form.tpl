{extends file="helpers/form/form.tpl"}
{block name="description"}
	<div class="help-block-container">
		{if isset($input.type) && $input.type == "file"}
			{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
				<img src="{$image_baseurl}{$fields_value[$input.name]}" height="200px" width="auto"/>
			{/if}
		{/if}
		{$smarty.block.parent}
	</div>
{/block}