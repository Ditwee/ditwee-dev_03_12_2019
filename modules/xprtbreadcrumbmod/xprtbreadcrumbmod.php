<?php

if (!defined('_CAN_LOAD_FILES_')) {
	exit;
}

class xprtbreadcrumbmod extends Module {
	public $css_array = array("xprtbreadcrumbmod.css", "xprtbreadcrumbmod.css");
	public $js_array = array("xprtbreadcrumbmod.js", "xprtbreadcrumbmod.js");
	public $inclue_pages = array("product", "blog", "cms", "others", "404", "sellers", "category");
	public function __construct() {
		$this->name = 'xprtbreadcrumbmod';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme BreadCrumb Block');
		$this->description = $this->l('Prestashop Most Powerfull Great Store Theme BreadCrumb Block Module by Xpert-Idea.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	// For installation service
	public function install() {
		if(!parent::install())
			return false;
		else	
			return true;
	}
	// For uninstallation service
	public function uninstall() {
		if(!parent::uninstall())
			return false;
		else	
			return true;
	}
	// Helper Form for Html markup generate
	public function SettingForm() {
		$default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Setting'),
			),
			'input' => array(
				array(
		    	    'type' => 'text',
		    	    'label' => $this->l('Breadcrumb area height'),
		    	    'desc' => $this->l('Select heigt for breadcrumb area. (default 293px)'),
		    	    'name' => 'bread_height',
		    	    'default_val' => '293',
		    	    'size' => 10,
		    	    'required' => true
		    	),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button',
			),
		);
		foreach ($this->inclue_pages as $k) {
			$kl = ucwords($k);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'file',
				'label' => $this->l($kl . ' BreadCrumb Image'),
				'name' => 'image_' . $k,
				'display_image' => false,
			);
		}
		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
		foreach (Language::getLanguages(false) as $lang) {
			$helper->languages[] = array(
				'id_lang' => $lang['id_lang'],
				'iso_code' => $lang['iso_code'],
				'name' => $lang['name'],
				'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0),
			);
		}
		$helper->tpl_vars = array(
			'uri' => $this->getPathUri(),
			'fields_value' => $this->getConfigFieldsValues(),
			'image_baseurl' => $this->_path.'img/',
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		$helper->toolbar_btn = array(
			'save' => array(
				'desc' => $this->l('Save'),
				'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name . 'token=' . Tools::getAdminTokenLite('AdminModules'),
			),
		);
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->title = $this->displayName;
		$helper->show_toolbar = true;
		$helper->toolbar_scroll = true;
		$helper->submit_action = 'save' . $this->name;
		foreach ($this->inclue_pages as $k) {
			$helper->fields_value['image_' . $k] = Configuration::get('image_' . $k);
		}
		return $helper;
	}
	// All Functional Logic here.
	public function getContent() {
		$html = '';
		if (Tools::isSubmit('save' . $this->name)) {
			Configuration::updateValue('bread_height',Tools::getValue('bread_height'));
			foreach ($this->inclue_pages as $k) {
				if (isset($_FILES['image_' . $k]) && isset($_FILES['image_' . $k]['tmp_name']) && !empty($_FILES['image_' . $k]['tmp_name'])) {
					$image = $this->processImage($k);
					Configuration::updateValue('image_' . $k, $image);
				}
			}
		}
		$helper = $this->SettingForm();
		$html .= $helper->generateForm($this->fields_form);
		return $html;
	}
	// Upload Image
	public function processImage($name) {
		$file_name = false;
		if (isset($_FILES['image_' . $name]) && isset($_FILES['image_' . $name]['tmp_name']) && !empty($_FILES['image_' . $name]['tmp_name'])) {
			$ext = substr($_FILES['image_' . $name]['name'], strrpos($_FILES['image_' . $name]['name'], '.') + 1);
			$id = $name;
			$file_name = $id . '.' . $ext;
			$path = _PS_MODULE_DIR_ . 'xprtbreadcrumbmod/img/' . $file_name;
			if (!move_uploaded_file($_FILES['image_' . $name]['tmp_name'], $path)) {
				return false;
			}else{
				return $file_name;
			}
		} else {
			return $file_name;
		}
	}
	public function getConfigFieldsValues()
	{
		$fields = array();
		$fields['bread_height'] = Tools::getValue('bread_height', Configuration::get('bread_height'));
		foreach ($this->inclue_pages as $k){
			$filename = Configuration::get('image_' . $k);
			if($filename && file_exists(_PS_MODULE_DIR_.$this->name.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.$filename)){
				$fields['image_'.$k] =  $filename;
			}else{
				$fields['image_'.$k] =  '';
			}
		}
		return $fields;
	}
}