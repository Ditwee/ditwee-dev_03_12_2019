<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
/**
 * @since 1.6.0
 */
class AdminxprtcategoryextraimgController extends ModuleAdminController
{
	public function postProcess()
	{
		if(($id_thumb = Tools::getValue('DeleteExtraimage', false)) !== false)
		{
			$id_category = (int)Tools::getValue('id_category');
			$image_path = _PS_MODULE_DIR_.'xprtcategoryextraimg/images/'.$id_category.'_extrathumb.jpg';
			if(file_exists($image_path)
				&& !unlink($image_path)){
				$this->context->controller->errors[] = Tools::displayError('Error while delete');
			}else{
				$img_fillname = 'catextra'.$id_category.'extrathumb';
				Configuration::deleteByName($img_fillname);
			}
			if(empty($this->context->controller->errors))
				Tools::clearSmartyCache();
			Tools::redirectAdmin(Context::getContext()->link->getAdminLink('AdminCategories').'&id_category='
				.(int)Tools::getValue('id_category').'&updatecategory');
		}
		parent::postProcess();
	}
	public function ajaxProcessUploadExtraimages()
	{
		if(isset($_FILES['catextraimagenameid']))
		{
			$id_category = Tools::getValue('id_category');
			$image_path = _PS_MODULE_DIR_.'xprtcategoryextraimg/images/'.$id_category.'_extrathumb.jpg';
			$img_name = $id_category.'_extrathumb.jpg';
			$img_fillname = 'xprtcatextra'.$id_category.'extrathumb';
			ImageManager::resize($_FILES['catextraimagenameid']['tmp_name'],$image_path);
			Configuration::updateValue($img_fillname,$img_name);
		}
		die(1);
	}
	public function ProcessExtraimages()
	{

	}
}
