{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="form-group">
	<label class="control-label col-lg-3">
			{l s='Category Page Extra Thumbnails' mod='xprtcategoryextraimg'}
	</label>
	<div class="col-lg-9">
		<div>
			{if isset($image_path_exists) && $image_path_exists == true}
			<img src="{$image_url}" alt="" class="imgm img-thumbnail" width="300">
			<p>
				<a class="btn btn-default" href="index.php?controller=Adminxprtcategoryextraimg&amp;id_category={$id_category}&amp;token={$admin_token}&amp;DeleteExtraimage=1">
					<i class="icon-trash"></i> {l s='Delete' mod='xprtcategoryextraimg'}
				</a>
			</p>
			{/if}
			<p>
				<button class="btn btn-default" data-style="expand-right" data-size="s" type="button" id="catextraimageid">
					<span class="ladda-label">
						<i class="icon-folder-open"></i>
						{l s='Add file' mod='xprtcategoryextraimg'}
					</span>
					<span class="ladda-spinner">
					</span>
				</button>
				<input type="file" name="catextraimagenameid" data-url="{$upload_url}" id="catextraimagenameid" style="display:none;" />
			</p>
		</div>
	</div>
</div>
