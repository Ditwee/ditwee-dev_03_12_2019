<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_PS_VERSION_'))
	exit;
class xprtcategoryextraimg extends Module
{
	public function __construct()
	{
		$this->name = 'xprtcategoryextraimg';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Sub Category Images Upload Module');
		$this->description = $this->l('Great Store Sub Category Images Upload Module by Innotech');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		$tab = new Tab();
		$tab->active = 1;
		$tab->class_name = 'Adminxprtcategoryextraimg';
		$tab->name = array();
		foreach (Language::getLanguages(true) as $lang)
			$tab->name[$lang['id_lang']] = 'xprtcategoryextraimg';
		$tab->id_parent = -1;
		$tab->module = $this->name;
		if(!$tab->add() ||
			!parent::install() ||
			!$this->registerHook('header') ||
			!$this->registerHook('displayBackOfficeheader') ||
			!$this->registerHook('displayBackOfficeCategory'))
				return false;
		return true;
	}
	public function uninstall()
	{
		$id_tab = (int)Tab::getIdFromClassName('Adminxprtcategoryextraimg');
		if ($id_tab)
		{
			$tab = new Tab($id_tab);
			$tab->delete();
		}
		if(!parent::uninstall())
			return false;
		return true;
	}
	public function hookDisplayBackOfficeCategory($params)
	{
		$id_category = (int)Tools::getValue('id_category');
		$image_path_exists = false;
		$admin_token = Tools::getAdminTokenLite('Adminxprtcategoryextraimg');
		$image_path = _PS_MODULE_DIR_.$this->name.'/images/'.$id_category.'_extrathumb.jpg';
		$image_url = _MODULE_DIR_.$this->name.'/images/'.$id_category.'_extrathumb.jpg';
		if(file_exists($image_path))
			$image_path_exists = true;
		$upload_url = $this->context->link->getAdminLink('Adminxprtcategoryextraimg').'&ajax=1&action=UploadExtraimages&id_category='.$id_category;
		$this->smarty->assign('image_path_exists',$image_path_exists);
		$this->smarty->assign('image_path',$image_path);
		$this->smarty->assign('image_url',$image_url);
		$this->smarty->assign('id_category',$id_category);
		$this->smarty->assign('admin_token',$admin_token);
		$this->smarty->assign('upload_url',$upload_url);
		return $this->display(__FILE__, 'views/templates/admin/xprtcategoryextraimg_admin.tpl');
	}
	public function hookdisplayBackOfficeheader()
	{
		if((Tools::getValue('controller') == 'AdminCategories') && (Tools::getIsset('addcategory') || Tools::getIsset('updatecategory')))
		$this->context->controller->addJS($this->_path.'js/imageuploadjs.js');
	}
}