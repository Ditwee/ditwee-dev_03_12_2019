<?php

class xprtcategoryprdclass extends ObjectModel
{
      public $id_xprtcategoryproducts;
      public $cate_lists;
      public $layout_view;
      public $display_style;
      public $limit;
      public $active;
      public $orderby;
      public $order_way;
      public $hook;
      public $code;
      public static $definition = array(
        		'table' => 'xprtcategoryproducts',
        		'primary' => 'id_xprtcategoryproducts',
            'multilang'=>false,
        		'fields' => array(
                 'cate_lists' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                 'layout_view' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                 'display_style' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                 'limit' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required'=>true),
                 'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                 'orderby' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                 'order_way' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                 'hook' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                 'code' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
        		),
        	);
    public function __construct($id = null, $id_shop = null)
    {
        Shop::addTableAssociation('xprtcategoryproducts', array('type' => 'shop'));
            parent::__construct($id, $id_shop);
    }
}
