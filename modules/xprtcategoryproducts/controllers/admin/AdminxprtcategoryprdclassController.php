<?php
require_once (dirname(__FILE__) . '/../../xprtcategoryproducts.php');
class AdminxprtcategoryprdclassController extends ModuleAdminController {
    public $asso_type = 'shop';
    public function __construct(){
        $this->table = 'xprtcategoryproducts';
        $this->className = 'xprtcategoryprdclass';
        $this->module = 'xprtcategoryproducts';
        $this->context = Context::getContext();
        $this->bootstrap = true;
        $this->allow_export = false;
        if(Shop::isFeatureActive())
            Shop::addTableAssociation($this->table, array('type' => 'shop'));
		parent::__construct();
        $this->fields_list = array(
                    'id_xprtcategoryproducts' => array(
                            'title' => $this->l('Id'),
                            'width' => 60,
                            'type' => 'text',
                    ),
                    'category' => array(
                            'title' => $this->l('Category'),
                            'width' => 100,
                            'type' => 'text'
                    ),
                    'hook' => array(
                            'title' => $this->l('Hook'),
                            'width' => 60,
                            'type' => 'text'
                    ),
                    'limit' => array(
                        'title' => $this->l('Limit'),
                        'width' => 70,
                    ),
                    'active' => array(
                        'title' => $this->l('Status'),
                        'width' => 70,
                        'align' => 'center',
                        'active' => 'status',
                        'type' => 'bool',
                        'orderby' => false
                    )
                );
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'xprtcategoryproducts_shop sbs ON a.id_xprtcategoryproducts=sbs.id_xprtcategoryproducts && sbs.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
        $this->_select = 'cl.`name` as category,';
        $this->_select .= ' sbs.id_shop ';
        $this->_defaultOrderBy = 'a.id_xprtcategoryproducts';
        $this->_defaultOrderWay = 'DESC';
        $this->_join .= 'LEFT JOIN '._DB_PREFIX_.'category_lang cl ON a.cate_lists = cl.id_category and cl.id_lang = '.$this->context->language->id.' and cl.id_shop = '.$this->context->shop->id;
        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
        {
           $this->_group = 'GROUP BY a.id_xprtcategoryproducts';
        }
        parent::__construct();
    }
    public function renderList() {
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();
    }
    public function renderForm()
    {
        $category_lists = array();
        $xprt = new xprtcategoryproducts();
        $category_lists = $xprt->generateCategoriesOption(
        Category::getNestedCategories(null, (int)Context::getContext()->language->id, true));
        $this->fields_form = array(
          'legend' => array(
          'title' => $this->l('xprt Category Wise Display Products'),
            ),
            'input' => array(
                array(
                        'type' => 'select',
                        'label' => $this->l('Select Category'),
                        'name' => 'cate_lists',
                        'options' => array(
                            'query' =>$category_lists,
                            'id' => 'id_category',
                            'name' => 'name'
                        ),
                        'desc' => $this->l('Please Select Your Category, Which Category Products You Want To Display.')
                    ),
                array(
                        'type' => 'select',
                        'label' => $this->l('Display Style'),
                        'name' => 'display_style',
                        'desc' => $this->l('Please Select Your Display Style.'),
                        'options' => array(
                            'query' =>array(
                                    array('id'=>0,'name'=>'General Style'),
                                    array('id'=>1,'name'=>'General Style For Each Subcategory'),
                                    array('id'=>2,'name'=>'Tab Style'),
                                    array('id'=>3,'name'=>'Tab style For Each Subcategory')
                                ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                array(
                        'type' => 'select',
                        'label' => $this->l('Layout View'),
                        'name' => 'layout_view',
                        'options' => array(
                            'query' =>array(
                                    array('id'=>0,'name'=>'General View'),
                                    array('id'=>1,'name'=>'Carousel View'),
                                ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'desc' => $this->l('Please Select Your Layout Style.')
                    ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Product Display Limit'),
                    'name' => 'limit',
                    'class' => 'fixed-width-xl',
                    'required' => true,
                    'desc' => $this->l('How Many Products You Want to Display for Each Category.')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Order By'),
                    'name' => 'orderby',
                    'desc' => $this->l('Please Select Order By To Display Products.'),
                    'options' => array(
                            'query' =>array(
                                    array('id'=>'id_product','name'=>'Product Id'),
                                    array('id'=>'price','name'=>'Price'),
                                    array('id'=>'date_add','name'=>'DateTime')
                                ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Order Way'),
                    'name' => 'order_way',
                    'desc' => $this->l('Please Select Order Way To Display Products.'),
                    'options' => array(
                            'query' =>array(
                                    array('id'=>'asc','name'=>'ASC'),
                                    array('id'=>'desc','name'=>'DESC')
                                ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                ),
                array(
                        'type' => 'select',
                        'label' => $this->l('Display Hook'),
                        'name' => 'hook',
                        'options' => array(
                            'query' =>array(
                                    array('id'=>'displayFooterProduct','name'=>'displayFooterProduct'),
                                    array('id'=>'displayHome','name'=>'displayHome'),
                                    array('id'=>'displayHomeTopRight','name'=>'displayHomeTopRight'),
                                    array('id'=>'displayHomeTopLeft','name'=>'displayHomeTopLeft'),
                                    array('id'=>'displayTopColumn','name'=>'displayTopColumn'),
                                    array('id'=>'displayHomeMiddle','name'=>'displayHomeMiddle')
                                ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'desc' => $this->l('Where You Want To Display This Block')
                    ),
                array(
                       'type' => 'switch',
                       'label' => $this->l('Status'),
                       'name' => 'active',
                       'required' => false,
                       'class' => 't',
                       'is_bool' => true,
                       'values' => array(
	                       array(
	                       'id' => 'active',
	                       'value' => 1,
	                       'label' => $this->l('Enabled')
	                       ),
	                       array(
	                       'id' => 'active',
	                       'value' => 0,
	                       'label' => $this->l('Disabled')
	                         )
                       )
                  )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
		if(Shop::isFeatureActive())
			$this->fields_form['input'][] = array(
				'type' => 'shop',
				'label' => $this->l('Shop association:'),
				'name' => 'checkBoxShopAsso',
			);
        if (!($xprtcategoryprdclass = $this->loadObject(true)))
            return;
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save'),
            'class' => 'button'
        );
        return parent::renderForm();
    }
}