<?php

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtcategoryproducts` (
  `id_xprtcategoryproducts` int(11) NOT NULL auto_increment,
  `cate_lists`int(11) DEFAULT NULL,
  `layout_view` int(11) DEFAULT NULL,
  `display_style` int(11) DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `orderby` varchar(150) DEFAULT NULL,
  `order_way` varchar(150) DEFAULT NULL,
  `hook` varchar(150) DEFAULT NULL,
  `code` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_xprtcategoryproducts`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtcategoryproducts_shop` (
  `id_xprtcategoryproducts` int(11) NOT NULL,
  `id_shop` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_xprtcategoryproducts`,`id_shop`)
)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

