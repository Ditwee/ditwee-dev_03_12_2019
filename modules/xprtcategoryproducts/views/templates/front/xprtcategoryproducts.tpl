{* Start for general style *}
{if isset($dataresults_general) && !empty($dataresults_general)}
	{include file="./xprtcategoryproducts_general.tpl" dataresult_general=$dataresults_general}
{/if}
{* End for general style *}
{* Start for general style With Each SubCategory *}
{if isset($dataresults_sub_general) && !empty($dataresults_sub_general)}
	{include file="./xprtcategoryproducts_subgeneral.tpl" dataresults_sub_general=$dataresults_sub_general}
{/if}
{* End for general style With Each SubCategory *}
{* Start for Tab style *}
{if isset($dataresults_tabstyle) && !empty($dataresults_tabstyle)}
	{include file="./xprtcategoryproducts_tab.tpl" dataresults_tabstyle=$dataresults_tabstyle}
{/if}
{* End for Tab style *}
{* Start for Tab style With Each SubCategory*}
{if isset($dataresults_sub_tabstyle) && !empty($dataresults_sub_tabstyle)}
	{include file="./xprtcategoryproducts_subtab.tpl" dataresults_sub_tabstyle=$dataresults_sub_tabstyle}
{/if}
{* End for Tab style With Each SubCategory*}