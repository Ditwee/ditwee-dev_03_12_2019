{foreach from=$dataresults_sub_general item=catvalue name=catgeneral}
	{foreach from=$catvalue.children item=value name=catgeneral}
	 	<section class="kr_categoryproduct">
			<h3 class="page-heading {$xprt.page_heading_style} {$xprt.page_heading_position}">
				<em>{$value.categoy_obj->name}</em>
				{* {if $xprt.page_heading_style != 'default' && $xprt.home_product_style == 'carousel'} *}
					{* <div class="heading_carousel_arrow"></div> *}
				{* {/if} *}
			</h3>
			{if isset($value.products) AND $value.products}
				{include file="$tpl_dir./product-list.tpl" products=$value.products id ="categorysubgenid_{$value.categoy_obj->id_category}"}
				{if ($value.layout_view == 1)}
					{literal}
						<script>
							jQuery(document).ready(function($){
								var variable = "categorysubgenid_{/literal}{$value.categoy_obj->id_category}{literal}";
								Call SLider
							});
						</script>
					{/literal}
				{/if}
			{else}
	 			<p>{l s='No category products' mod='xprtcategoryproducts'}</p>
			{/if}
	 	</section>
	{/foreach}
{/foreach}