	{$i = 0}
	{foreach from=$dataresults_sub_tabstyle item=catprodsubtab name=categoryProduct}
		<ul id="home-page-tabs_subcat_{$i}" class="nav nav-tabs clearfix">
				{$count = 0}
				{* start all block *}
				<li {if ($count == 0)}class="active"{/if}>
					<a data-toggle="tab" href="#categorysubtabid_{$i}_{$catprodsubtab.categoy_obj->id_category}" class="categorysubtabclass_{$catprodsubtab.categoy_obj->id_category}">{l s='All' mod='xprtcategoryproducts'}</a>
				</li>
				{$count = 1}
				{* End all block *}
				{* start Sub Category Block *}
				{foreach from=$catprodsubtab.children item=childrencatprodsubtab name=categoryProduct}
					<li {if ($count == 0)}class="active"{/if}>
						<a data-toggle="tab" href="#categorysubtabid_{$i}_{$childrencatprodsubtab.categoy_obj->id_category}" class="categorysubtabclass_{$childrencatprodsubtab.categoy_obj->id_category}">{$childrencatprodsubtab.categoy_obj->name}</a>
					</li>
				{$count = $count+1}
				{/foreach}
				{* End Sub Category Block*}
		</ul>
		<div id="home-page-tab-content_subcat_{$i}" class="tab-content">
					{$countx = 0}
					{* start all block *}					
						{if isset($catprodsubtab.total_products) AND $catprodsubtab.total_products}
							{include file="$tpl_dir./product-list.tpl" products=$catprodsubtab.total_products class="tab-pane {if ($countx == 0)} active{/if}" id="categorysubtabid_{$i}_{$catprodsubtab.categoy_obj->id_category}"}
								{if ($catprodsubtab.layout_view == 1)}
									{literal}
										<script>
											jQuery(document).ready(function($){
												var variable = "categorysubtabid_{/literal}{$i}_{$catprodsubtab.categoy_obj->id_category}{literal}";
												Call SLider
											});
										</script>
									{/literal}
								{/if}
						{else}
							<ul id="categorysubtabid_{$i}_{$catprodsubtab.categoy_obj->id_category}" class="tab-pane {if ($countx == 0)} active{/if}">
								<p>{l s='No Products On This Category' mod='xprtcategoryproducts'}</p>
							</ul>
						{/if}
						{$countx = 1}
					{* End all block *}
					{* start Sub Category Block *}
						{foreach from=$catprodsubtab.children item=childrencatprodsubtab name=categoryProduct}
							{if isset($childrencatprodsubtab.products) AND $childrencatprodsubtab.products}
								{include file="$tpl_dir./product-list.tpl" products=$childrencatprodsubtab.products class="tab-pane {if ($countx == 0)} active{/if}" id="categorysubtabid_{$i}_{$childrencatprodsubtab.categoy_obj->id_category}"}
									{if ($childrencatprodsubtab.layout_view == 1)}
										{literal}
											<script>
												jQuery(document).ready(function($){
													var variable = "categorysubtabid_{/literal}{$i}_{$childrencatprodsubtab.categoy_obj->id_category}{literal}";
													Call SLider
												});
											</script>
										{/literal}
									{/if}
							{else}
								<ul id="categorysubtabid_{$i}_{$childrencatprodsubtab.categoy_obj->id_category}" class="tab-pane {if ($countx == 0)} active{/if}">
									<p>{l s='No Products On This Category' mod='xprtcategoryproducts'}</p>
								</ul>
							{/if}
							{$countx = $countx+1}
						{/foreach}
					{* End Sub Category Block*}
		</div>
		{$i = $i+1}
	{/foreach}

