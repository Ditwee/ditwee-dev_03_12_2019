<ul id="home-page-tabs" class="nav nav-tabs clearfix">
	{foreach from=$dataresults_tabstyle item=catprodtab name=categoryProduct}
		<li>
			<a data-toggle="tab" href="#categorytabid_{$catprodtab.categoy_obj->id_category}" class="categorytabclass_{$catprodtab.categoy_obj->id_category}">{$catprodtab.categoy_obj->name}</a>
		</li>
	{/foreach}
</ul>
<div id="home-page-tab-content" class="tab-content">
	{foreach from=$dataresults_tabstyle item=catprodtabcon name=categoryProduct}
			{if isset($catprodtabcon.products) AND $catprodtabcon.products}
				<div id="categorytabid_{$catprodtabcon.categoy_obj->id_category}" class="tab-pane">
					{include file="$tpl_dir./product-list.tpl" products=$catprodtabcon.products id="" class="categorytabid_{$catprodtabcon.categoy_obj->id_category}"}

					{if ($catprodtabcon.layout_view == 1)}
						{literal}
							<script>
								jQuery(document).ready(function($){
									var variable = "categorytabid_{/literal}{$catprodtabcon.categoy_obj->id_category}{literal}";
								});
							</script>
						{/literal}
					{/if}
					<div class="view_all">
						<a href="{$link->getCategoryLink($catprodtabcon.categoy_obj->id_category, $catprodtabcon.categoy_obj->link_rewrite)|escape:'html':'UTF-8'}">{l s='View All Products' mod='xprtcategoryproducts'}</a>
					</div>
				</div>
					
			{else}
				<p>{l s='No featured products normal' mod='xprtcategoryproducts'}</p>
			{/if}
	{/foreach}
</div>