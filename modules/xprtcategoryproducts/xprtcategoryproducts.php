<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
require_once (dirname(__FILE__) . '/classes/xprtcategoryprdclass.php');
class xprtcategoryproducts extends Module
{
	public $xpertcat =array();
	var $data, $sortdata;
	public $element_index = 0;
	public function __construct()
	{
		$this->name = 'xprtcategoryproducts';
		$this->tab = 'front_office_features';
		$this->author = 'Xpert-Idea';
		$this->version = '2.0';
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Category Products Display');
		$this->description = $this->l('Display Category products Display Module By Xpert-Idea.');
	}
	public function install()
	{
		$sql = array();
        require_once(dirname(__FILE__) . '/sql/install.php');
        foreach ($sql as $sq) :
            if (!Db::getInstance()->Execute($sq,false))
                return false;
        endforeach;
        $this->RegisterTabs();
        if(!parent::install()
        	|| !$this->registerHook('displayHome')
        	|| !$this->registerHook('displayHeader')
        	|| !$this->xpertsampledata())
        	return false;
        return true;
	}
	public function uninstall()
	{
		require_once(dirname(__FILE__) . '/sql/uninstall_tab.php');
        foreach ($idtabs as $tabid):
            if ($tabid) {
                $tab = new Tab($tabid);
                $tab->delete();
            }
        endforeach;
                $sql = array();
        require_once(dirname(__FILE__) . '/sql/uninstall.php');
        foreach ($sql as $s) :
            if (!Db::getInstance()->Execute($s,false))
                return false;
        endforeach;
		Configuration::deleteByName('xprtwc_saved_conf');
		return parent::uninstall();
	}
	private function RegisterTabs()
	{
        $langs = Language::getLanguages();
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        require_once(dirname(__FILE__) . '/sql/install_tab.php');
        foreach ($tabvalue as $tab){
            $newtab = new Tab();
            $newtab->class_name = $tab['class_name'];
            $newtab->id_parent = $tab['id_parent'];
            $newtab->module = $tab['module'];
            foreach ($langs as $l) {
                $newtab->name[$l['id_lang']] = $this->l($tab['name']);
            }
            $newtab->save();
        }
        return true;
    }
	public function GetDataByHook($hook='displayhome')
	{
		$sql = 'SELECT * FROM '._DB_PREFIX_.'xprtcategoryproducts xprt INNER JOIN `'._DB_PREFIX_.'xprtcategoryproducts_shop` xprtl ON xprt.id_xprtcategoryproducts = xprtl.id_xprtcategoryproducts AND xprtl.id_shop = '.(int)Context::getContext()->shop->id.' where xprt.hook = "'.$hook.'"';
		$cats = Db::getInstance()->executeS($sql);
			return $cats;		
	}
	public function GetHookDataByHook($hook='displayhome')
	{
		$results = array();
		$dataresults = array();
		$dataresults_one = array();
		$dataresults_two = array();
		$dataresults_three = array();
		$dataresults_four = array();
		$sub_category = array();
		$total_products = array();
		$sortdata = $this->GetDataByHook($hook);
	    $this->context->controller->addJS($this->_path.'js/custom.js');
		$id_lang = $this->context->language->id ? (int) $this->context->language->id : (int)Context::getContext()->language->id;
		$id_shop = $this->context->shop->id ? (int) $this->context->shop->id : (int)Context::getContext()->shop->id;
        $root_categories = Category::getRootCategories($this->context->language->id, true);
		$i=0;
		if(isset($sortdata) && !empty($sortdata)){
			foreach($sortdata as $i=>$data){
				if($data['active'] == 1){
					$category = new Category((int)$data['cate_lists'],(int)$id_lang);
					$id_shop =(int)Shop::getContextShopID();
					$products = $this->getProductsByCategoryID($data['cate_lists'],$data['limit'],$data['orderby'],$data['order_way']);
	                $total_products = $products;
					$children = Category::getChildren((int)$data['cate_lists'],(int)$id_lang, true,(int)$id_shop);
	                $dataresults[$i]['cate_lists'] = $data['cate_lists'];
	                $dataresults[$i]['layout_view'] = $data['layout_view'];
	                $dataresults[$i]['display_style'] = $data['display_style'];
	                $dataresults[$i]['limit'] = $data['limit'];
	                $dataresults[$i]['active'] = $data['active'];
	                $dataresults[$i]['orderby'] = $data['orderby'];
	                $dataresults[$i]['order_way'] = $data['order_way'];
	                $dataresults[$i]['hook'] = $data['hook'];
	                $dataresults[$i]['code'] = $data['code'];
	                $dataresults[$i]['products'] = $products;
	                $dataresults[$i]['categoy_obj'] = $category;
					$catextraextrathumb = "catextra".$data['cate_lists']."extrathumb";
					$catextraextrathumbimg = Configuration::get($catextraextrathumb);
					if(isset($catextraextrathumbimg) && !empty($catextraextrathumbimg)){
						$dataresults[$i]['categoy_image'] = _MODULE_DIR_."xprtcategoryextraimg/images/".$catextraextrathumbimg;
					}else{
						$dataresults[$i]['categoy_image'] = _THEME_IMG_DIR_."no-image.jpg";
					}
	                $j=0;
					if(isset($children)){
						foreach($children as $ch){
							$temp_cat = new Category($ch['id_category'],$id_lang);
	                        $sub_category_products = $this->getProductsByCategoryID($ch['id_category'],$data['limit'],$data['orderby'],$data['order_way']);
	                        $sub_category[$j]['products'] = $sub_category_products;
	                        $sub_category[$j]['layout_view'] = $data['layout_view'];
							$sub_category[$j]['categoy_obj'] = $temp_cat;
							$total_products = array_merge($total_products,$sub_category_products);
							$catextraextrathumb = "catextra".$ch['id_category']."extrathumb";
							$catextraextrathumbimg = Configuration::get($catextraextrathumb);
							if(isset($catextraextrathumbimg) && !empty($catextraextrathumbimg)){
								$sub_category[$j]['categoy_image'] = _MODULE_DIR_."xprtcategoryextraimg/images/".$catextraextrathumbimg;
							}else{
								$sub_category[$j]['categoy_image'] = _THEME_IMG_DIR_."no-image.jpg";
							}
	                        $j++;
						}
						$dataresults[$i]['children'] = $sub_category;
					}
						$dataresults[$i]['total_products'] = $total_products;

					if($data['display_style'] == 3){
						$dataresults_four[] = $dataresults[$i];
					}elseif($data['display_style'] == 2){
						$dataresults_three[] = $dataresults[$i];
					}elseif($data['display_style'] == 1){
						$dataresults_two[] = $dataresults[$i];
					}else{
						$dataresults_one[] = $dataresults[$i];
					}
	            }
				$i++;
			}
			$results['all'] = $dataresults;
			$results['one'] = $dataresults_one;
			$results['two'] = $dataresults_two;
			$results['three'] = $dataresults_three;
			$results['four'] = $dataresults_four;
		}
		return $results;
	}
	public function getProductsByCategoryID($category_id,$limit=4,$orderby = 'id_product',$orderway = 'desc', $id_lang = null, $id_shop = null, $child_count = false)
	{
		$id_lang = is_null($id_lang) ? $this->context->language->id : $id_lang ;
        $id_shop = is_null($id_shop) ? $this->context->shop->id : $id_shop ;
        $id_supplier = (int)Tools::getValue('id_supplier');
        $active = true;
        	$front = true;
            $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, MAX(product_attribute_shop.id_product_attribute) id_product_attribute, product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, pl.`description`, pl.`description_short`, pl.`available_now`,
				pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, MAX(image_shop.`id_image`) id_image,
				il.`legend`, m.`name` AS manufacturer_name, cl.`name` AS category_default,
				DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),
				INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
					DAY)) > 0 AS new, product_shop.price AS orderprice
			FROM `'._DB_PREFIX_.'category_product` cp
			LEFT JOIN `'._DB_PREFIX_.'product` p
				ON p.`id_product` = cp.`id_product`
			'.Shop::addSqlAssociation('product', 'p').'
			LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
			ON (p.`id_product` = pa.`id_product`)
			'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
			'.Product::sqlStock('p', 'product_attribute_shop', false, $this->context->shop).'
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
				ON (product_shop.`id_category_default` = cl.`id_category`
				AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
				ON (p.`id_product` = pl.`id_product`
				AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
			LEFT JOIN `'._DB_PREFIX_.'image` i
				ON (i.`id_product` = p.`id_product`)'.
			Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il
				ON (image_shop.`id_image` = il.`id_image`
				AND il.`id_lang` = '.(int)$id_lang.')
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
				ON m.`id_manufacturer` = p.`id_manufacturer`
			WHERE product_shop.`id_shop` = '.(int)$this->context->shop->id.'
				AND cp.`id_category` = '.(int)$category_id
				.($active ? ' AND product_shop.`active` = 1' : '')
				.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
				.($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
				.' GROUP BY product_shop.id_product';
            $sql .= ' ORDER BY p.'.$orderby.' '.$orderway.' '; 
            $sql .= '  LIMIT '.$limit.' '; 
           $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if(!$result)
            return array();    
		return Product::getProductsProperties($id_lang,$result);
	}
	public function generatesubCategoriesOption($categories,$items_to_skip = null)
	{
		$subcatvals = array();
		$spacer_size = '5';
	 	$this->element_index++;
		foreach ($categories as $key => $category)
		{
			$this->xpertcat[$this->element_index]['id_category'] = $category['id_category'];
			$this->xpertcat[$this->element_index]['name'] = str_repeat('&nbsp;', $spacer_size * (int)$category['level_depth']).$category['name'];
			if (isset($category['children']))
				  $this->generatesubCategoriesOption($category['children']);
		$this->element_index++;
		}
		return true;
	}
	public function generateCategoriesOption($categories, $items_to_skip = null)
	{
			$subcatvals = array();
			$spacer_size = '3';
			$this->element_index = 1;
			foreach ($categories as $key => $category)
			{
					$this->xpertcat[$this->element_index]['id_category'] = $category['id_category'];
					$this->xpertcat[$this->element_index]['name'] = str_repeat('&nbsp;', $spacer_size * (int)$category['level_depth']).$category['name'];
				if (isset($category['children']))
					  $this->generatesubCategoriesOption($category['children']);
				$this->element_index++;
			}
		return $this->xpertcat;
	}
	public function hookdisplayHeader($params)
	{  
		$this->context->controller->addJqueryPlugin(array('jquery-ui','bxslider'));
		$this->context->controller->addCSS($this->_path . 'bxslider/jquery.bxslider.css');
	}
	public function hookshoppingCart($params)
	{
		$this->AssignSmartyData('shoppingcart');
		return  $this->display(__FILE__, 'views/templates/front/xprtcategoryproducts.tpl');
	}
	public function hookdisplayFooterProduct($params)
	{
		$this->AssignSmartyData('displayfooterproduct');
		return  $this->display(__FILE__,'views/templates/front/xprtcategoryproducts.tpl');  
	}
	public function hookdisplayHome($params)
	{
		$this->AssignSmartyData('displayHome');
		return  $this->display(__FILE__,'views/templates/front/xprtcategoryproducts.tpl');  
	}
	public function hookdisplayHomeMiddle($params)
	{
		$this->AssignSmartyData('displayHomeMiddle');
		return  $this->display(__FILE__,'views/templates/front/xprtcategoryproducts.tpl');  
	}
	public function hookdisplayHomeTopLeft($params)
	{
		$this->AssignSmartyData('displayHomeTopLeft');
		return  $this->display(__FILE__,'views/templates/front/xprtcategoryproducts.tpl');  
	}
	public function hookdisplayHomeTopRight($params)
	{
		$this->AssignSmartyData('displayHomeTopRight');
		return  $this->display(__FILE__,'views/templates/front/xprtcategoryproducts.tpl');  
	}
	public function hookdisplayTopColumn($params)
	{
		if('index' == Context::getContext()->controller->php_self){
			$this->AssignSmartyData('displaytopcolumn');
			return  $this->display(__FILE__,'views/templates/front/xprtcategoryproducts.tpl');  
		}
	}
	public function AssignSmartyData($hook='displayhome')
	{
			$dataresults = $this->GetHookDataByHook($hook);
			if(isset($dataresults['all']))
				$this->smarty->assign('dataresults',$dataresults['all']);  
			if(isset($dataresults['one']))
				$this->smarty->assign('dataresults_general',$dataresults['one']);  
			if(isset($dataresults['three']))
				$this->smarty->assign('dataresults_tabstyle',$dataresults['three']);  
			if(isset($dataresults['two']))
				$this->smarty->assign('dataresults_sub_general',$dataresults['two']);  
			if(isset($dataresults['four']))
				$this->smarty->assign('dataresults_sub_tabstyle',$dataresults['four']);  
	}
	public function xpertsampledata($demo=NULL)
	{
		// if(($demo==NULL) || (empty($demo)))
		// 	$demo = "demo_1";
		// $func = 'xpertsample_'.$demo;
		// if(method_exists($this,$func)){
	 //    	$this->alldisabled();
	 //    	$this->{$func}();
	 //    }else{
	 //    	$this->alldisabled();
	 //    }
	    return true;
	}
	public function alldisabled(){
		// Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'xprtcategoryproducts`;',false);
		// Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'xprtcategoryproducts_shop`;',false);
		return true;
	}
}