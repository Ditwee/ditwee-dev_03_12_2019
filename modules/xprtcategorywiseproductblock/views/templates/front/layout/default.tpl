{if isset($xprtcategorywiseproductblock) && !empty($xprtcategorywiseproductblock)}
	{if isset($xprtcategorywiseproductblock.device)}
		{assign var=device_data value=$xprtcategorywiseproductblock.device|json_decode:true}
	{/if}
	<div id="xprtcategorywiseproductblock_{$xprtcategorywiseproductblock.id_xprtcategorywiseproductblock}" class="xprtcategorywiseproductblock xprt_default_products_block" style="margin:{$xprtcategorywiseproductblock.section_margin};">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtcategorywiseproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtcategorywiseproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtcategorywiseproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtcategorywiseproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		{if isset($xprtcategorywiseproductblock) && $xprtcategorywiseproductblock}
			<div id="xprt_categoryproductsblock_{$xprtcategorywiseproductblock.id_xprtcategorywiseproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtcategorywiseproductblock.products id='' class="xprt_categoryproductsblock {if $xprtcategorywiseproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtcategorywiseproductblock'}</p>
			</div>
		{/if}
	</div>
	<script type="text/javascript">
	var xprtcategorywiseproductblock = $("#xprtcategorywiseproductblock_{$xprtcategorywiseproductblock.id_xprtcategorywiseproductblock}");
	var sliderSelect = xprtcategorywiseproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtcategorywiseproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtcategorywiseproductblock.nav_arrow_style) && ($xprtcategorywiseproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}
	
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtcategorywiseproductblock.play_again)}{$xprtcategorywiseproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtcategorywiseproductblock.autoplay)}{$xprtcategorywiseproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtcategorywiseproductblock.pause_on_hover)}{$xprtcategorywiseproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtcategorywiseproductblock.navigation_dots)}{$xprtcategorywiseproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtcategorywiseproductblock.navigation_arrow)}{$xprtcategorywiseproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtcategorywiseproductblock.product_rows)}{$xprtcategorywiseproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtcategorywiseproductblock.product_scroll) && ($xprtcategorywiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtcategorywiseproductblock.product_scroll) && ($xprtcategorywiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtcategorywiseproductblock.product_scroll) && ($xprtcategorywiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtcategorywiseproductblock.product_scroll) && ($xprtcategorywiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtcategorywiseproductblock.product_scroll) && ($xprtcategorywiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}