{if isset($xprtcategorywiseproductblock) && !empty($xprtcategorywiseproductblock)}
	{if isset($xprtcategorywiseproductblock.device)}
		{assign var=device_data value=$xprtcategorywiseproductblock.device|json_decode:true}
	{/if}
	<div class="xprtcategorywiseproductblock block carousel">
		<h4 class="title_block">
	    	{$xprtcategorywiseproductblock.title}
	    </h4>
	    <div class="block_content">
	        {if isset($xprtcategorywiseproductblock) && $xprtcategorywiseproductblock}
	        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtcategorywiseproductblock.products }
	        {else}
	        	<p class="alert alert-info">{l s='No products at this time.' mod='xprtcategorywiseproductblock'}</p>
	        {/if}
	    </div>
	</div>
{/if}