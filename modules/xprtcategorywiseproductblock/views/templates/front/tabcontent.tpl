{if isset($xprtcategorywiseproductblock) && !empty($xprtcategorywiseproductblock)}
	{if isset($xprtcategorywiseproductblock.device)}
		{assign var=device_data value=$xprtcategorywiseproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtcategorywiseproductblock) && $xprtcategorywiseproductblock}
		<div id="xprt_categoryproductsblock_tab_{if isset($xprtcategorywiseproductblock.id_xprtcategorywiseproductblock)}{$xprtcategorywiseproductblock.id_xprtcategorywiseproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtcategorywiseproductblock.products class='xprt_specialproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_categoryproductsblock_tab_{if isset($xprtcategorywiseproductblock.id_xprtcategorywiseproductblock)}{$xprtcategorywiseproductblock.id_xprtcategorywiseproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No products at this time.' mod='xprtcategorywiseproductblock'}</p>
		</div>
	{/if}
{/if}