<?php
class xprtcountdownclass extends ObjectModel
{
	public $id;
	public $id_xprtcountdown;
	public $position;
	public $hook;
	public $truck_identify;
	public $content;
	public $title;
	public $sub_title;
	public $active;
	public $image;
	public $pages;
	private static $module_name = 'xprtcountdown';
	private static $tablename = 'xprtcountdown';
	private static $classname = 'xprtcountdownclass';
	public static $definition = array(
		'table' => 'xprtcountdown',
		'primary' => 'id_xprtcountdown',
		'multilang' => true,
		'fields' => array(
			'title' =>			array('type' => self::TYPE_STRING,'validate' => 'isString','lang' => true),
			'sub_title' =>		array('type' => self::TYPE_STRING,'validate' => 'isString','lang' => true),
			'hook' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'truck_identify' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'content' =>		array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'image' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'pages' =>			array('type' => self::TYPE_STRING,'validate' => 'isCleanHtml'),
			'position' =>			array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'active' =>			array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation(self::$tablename, array('type' => 'shop'));
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = self::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.self::$tablename.'`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetProductBlock($hook = NULL)
    {
        if($hook == NULL)
            return false;
        $module_name = self::$module_name;
        $allproducts = self::GetAllBlockProductS($hook);
        $results = array();
        if(isset($allproducts) && !empty($allproducts)){
        	$i=0;
            foreach ($allproducts as $allprds){
            	if(empty($allprds['pages'])){
					$allprds['pages'] = 'all_page';
				}
                if($module_name::PageException($allprds['pages'])){
                    if(!file_exists(_PS_MODULE_DIR_.$module_name.'/images/'.$allprds['image'])){
						$allprds['image'] = null;
                    }else{
                    	$allprds['image'] = _MODULE_DIR_.$module_name.'/images/'.$allprds['image'];
                    }
                    if(!file_exists(_PS_MODULE_DIR_.$module_name.'/images/'.$allprds['bgimage'])){
						$allprds['bgimage'] = null;
                    }else{
                    	$allprds['bgimage'] = _MODULE_DIR_.$module_name.'/images/'.$allprds['bgimage'];
                    }
                    $allprds['pages'] =   isset($allprds['pages'])?$allprds['pages']:"";
                    $allprds['hook']  =   isset($allprds['hook'])?$allprds['hook']:"";
                    $allprds['active']    =   $allprds['active'];
                    $allprds['position']  =   $allprds['position'];
                    $results[$i] = $allprds;
                    $i++;
                }
            }
        }
        return $results;
    }
    public static function GetAllBlockProductS($hook = NULL)
    {
    	$results = array();
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.self::$tablename.'` pb 
                INNER JOIN `'._DB_PREFIX_.self::$tablename.'_lang` pbl ON (pb.`id_'.self::$tablename.'` = pbl.`id_'.self::$tablename.'` AND pbl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.self::$tablename.'_shop` pbs ON (pb.`id_'.self::$tablename.'` = pbs.`id_'.self::$tablename.'` AND pbs.`id_shop` = '.$id_shop.')
                ';
        $sql .= ' WHERE pb.`active` = 1 ';
        if($hook != NULL)
            $sql .= ' AND pb.`hook` = "'.$hook.'" ';
        $sql .= ' ORDER BY pb.`position` ASC';
        $results = Db::getInstance()->executeS($sql);
        if(isset($results) && !empty($results) && is_array($results)){
        	foreach ($results as &$result) {
        		if(isset($result['content']) && !empty($result['content'])){
        			$content = Tools::jsonDecode($result['content']);
        			if(isset($content) && !empty($content)){
        				foreach ($content as $content_key => $content_value) {
        					if(isset($content_value) && !is_string($content_value) && is_object($content_value)){
        						// foreach ($content_value as $content_value_key => $content_value_value) {
        						// 	$result[$content_key][$content_value_key] = $content_value_value;
        						// }
        						$result[$content_key] = (isset($content_value->{$id_lang}) && !empty($content_value->{$id_lang})) ? $content_value->{$id_lang} : "";
        					}else{
        						$result[$content_key] = $content_value;
        					}
        				}
        			}
        		}
        	}
        }
        return $results;
    }
}