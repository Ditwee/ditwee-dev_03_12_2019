<?php
$countdown_dummy_data = array(
    array( // 1
    	'lang' => array(
		    'title' => 'Bags fashion  just 30% off',
		    'sub_title' => 'wow! super deal only today',
		),
	    'notlang' => array(
			'hook'	=>	'displayHomeFullWidthMiddle',
			'image'	=>	'untitled-2.png',
			'pages'	=>	'all_page',
			'active'	=>	0,
			'content'	=>	'{"description":{"1":"Inspired by New York. Crafted in Switzerland. <br />\r\nFrom the inventors of the New York Minute.","3":"Inspired by New York. Crafted in Switzerland. <br />\r\nFrom the inventors of the New York Minute.","5":"Inspired by New York. Crafted in Switzerland. <br />\r\nFrom the inventors of the New York Minute."},"counter":"June 7, 2087 15:03:26","height":"600px","section_margin":"0px 0px 2px 0px","bgimage":"blog1.jpg","bgimage_enable":"1","is_parallax":"1","btnlabel":{"1":"Shop now","3":"Shop now","5":"Shop now"},"btnlnk":{"1":"#","3":"#","5":"#"},"btntarget":"_blank"}',
			'position'	=>	0,
			'truck_identify'	=>	'demo_5',
		),
    ),
);