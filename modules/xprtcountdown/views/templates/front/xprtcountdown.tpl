{if isset($xprtcountdown) && !empty($xprtcountdown)}
	<div class="home_promo_countdown_block_area xprt_parallax_section" style="background:url({$xprtcountdown.bgimage}) no-repeat scroll center center/ cover; height:{$xprtcountdown.height|replace:px:''}px; margin: {$xprtcountdown.section_margin}">
		<div class="container home_promo_countdown_block">
			<div class="row">
				<div class="col-sm-12">
					<div class="home_promo_countdown_block_left">
						<div class="home_promo_countdown_block_inner">
							{if isset($xprtcountdown.sub_title) && !empty($xprtcountdown.sub_title)}
								<h4>{$xprtcountdown.sub_title}</h4>
							{/if}
							{if isset($xprtcountdown.title) && !empty($xprtcountdown.title)}
								<h2>{$xprtcountdown.title}</h2>
							{/if}
							{if isset($xprtcountdown.description) && !empty($xprtcountdown.description)}
								<p>{$xprtcountdown.description}</p>
							{/if}
							{if isset($xprtcountdown.counter) && !empty($xprtcountdown.counter)}
								<div class="home_promo_countdown_block_counter">
									<div id="home_promo_countdown_block_counter" class="promo_countdown_block_counter" data-date="{$xprtcountdown.counter}"></div>
									{literal}
										<script type="text/javascript">
											$(function() {
												$('#home_promo_countdown_block_counter').countdown({
													render: function(data) {
														$(this.el).html("<div class='countdown_list days'><span class='countdown_digit'>" + this.leadingZeros(data.days, 3) + "</span><span class='countdown_label'>{/literal}{l s='days' mod='xprtcountdown'}{literal}</span></div><div class='countdown_list hrs'><span class='countdown_digit'>" + this.leadingZeros(data.hours, 2) + "</span><span class='countdown_label'>{/literal}{l s='hrs' mod='xprtcountdown'}{literal}</span></div><div class='countdown_list min'><span class='countdown_digit'>" + this.leadingZeros(data.min, 2) + "</span><span class='countdown_label'>{/literal}{l s='min' mod='xprtcountdown'}{literal}</span></div><div class='countdown_list sec'><span class='countdown_digit'>" + this.leadingZeros(data.sec, 2) + "</span><span class='countdown_label'>{/literal}{l s='sec' mod='xprtcountdown'}{literal}</span></div>");
													}
												});
											});
										</script>
									{/literal}
								</div> <!-- home_promo_countdown_block_counter -->
							{/if}
							{if isset($xprtcountdown.btnlabel) && !empty($xprtcountdown.btnlabel)}
								<p class="home_promo_countdown_link"><a class="btn btn-default" target="{$xprtcountdown.btnlnk}" href="{$xprtcountdown.btnlnk}">{$xprtcountdown.btnlabel}</a></p>
							{/if}
						</div>
					</div>
					<div class="home_promo_countdown_block_right">
						<img class="img-responsive" src="{$xprtcountdown.image}" alt="{$xprtcountdown.title}">
					</div>
				</div>
			</div>
		</div>
	</div>
{/if}