<?php

if (!defined('_PS_VERSION_'))
	exit;
function upgrade_module_1_0_1($object)
{
	$name = $object->name;
	$table_name = $name::$tablename;
	Db::getInstance()->Execute("ALTER TABLE `"._DB_PREFIX_.$name::$tablename."` ADD `truck_identify` VARCHAR(150) NOT NULL ;");
	return true;
}