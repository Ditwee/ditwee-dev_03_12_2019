{if isset($xprtcrosssellingproductblock) && !empty($xprtcrosssellingproductblock) && !empty($xprtcrosssellingproductblock.products)}
	{if isset($xprtcrosssellingproductblock.device)}
		{assign var=device_data value=$xprtcrosssellingproductblock.device|json_decode:true}
	{/if}
	<div id="xprtcrosssellingproductblock_{$xprtcrosssellingproductblock.id_xprtcrosssellingproductblock}" class="xprtcrosssellingproductblock xprt_default_products_block" style="margin:{$xprtcrosssellingproductblock.section_margin};">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtcrosssellingproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtcrosssellingproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtcrosssellingproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtcrosssellingproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		{if isset($xprtcrosssellingproductblock) && $xprtcrosssellingproductblock}
			<div id="xprt_crosssellproductsblock_{$xprtcrosssellingproductblock.id_xprtcrosssellingproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtcrosssellingproductblock.products id='' class="xprt_crosssellproductsblock {if $xprtcrosssellingproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtcrosssellingproductblock'}</p>
			</div>
		{/if}
	</div>
<script type="text/javascript">
	var xprtcrosssellingproductblock = $("#xprtcrosssellingproductblock_{$xprtcrosssellingproductblock.id_xprtcrosssellingproductblock}");
	var sliderSelect = xprtcrosssellingproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtcrosssellingproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtcrosssellingproductblock.nav_arrow_style) && ($xprtcrosssellingproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}
	
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtcrosssellingproductblock.play_again)}{$xprtcrosssellingproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtcrosssellingproductblock.autoplay)}{$xprtcrosssellingproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtcrosssellingproductblock.pause_on_hover)}{$xprtcrosssellingproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtcrosssellingproductblock.navigation_dots)}{$xprtcrosssellingproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtcrosssellingproductblock.navigation_arrow)}{$xprtcrosssellingproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtcrosssellingproductblock.product_rows)}{$xprtcrosssellingproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtcrosssellingproductblock.product_scroll) && ($xprtcrosssellingproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtcrosssellingproductblock.product_scroll) && ($xprtcrosssellingproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtcrosssellingproductblock.product_scroll) && ($xprtcrosssellingproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtcrosssellingproductblock.product_scroll) && ($xprtcrosssellingproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtcrosssellingproductblock.product_scroll) && ($xprtcrosssellingproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}