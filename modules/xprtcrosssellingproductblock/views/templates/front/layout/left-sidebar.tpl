{if isset($xprtcrosssellingproductblock) && !empty($xprtcrosssellingproductblock) && !empty($xprtcrosssellingproductblock.products)}
	{if isset($xprtcrosssellingproductblock.device)}
		{assign var=device_data value=$xprtcrosssellingproductblock.device|json_decode:true}
	{/if}
	<div class="xprtcrosssellingproductblock block carousel">
		<h4 class="title_block">
	    	{$xprtcrosssellingproductblock.title}
	    </h4>
	    <div class="block_content">
	        {if isset($xprtcrosssellingproductblock) && $xprtcrosssellingproductblock}
	        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtcrosssellingproductblock.products }
	        {else}
	        	<p class="alert alert-info">{l s='No products at this time.' mod='xprtcrosssellingproductblock'}</p>
	        {/if}
	    </div>
	</div>
{/if}