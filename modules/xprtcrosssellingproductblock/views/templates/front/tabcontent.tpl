{if isset($xprtcrosssellingproductblock) && !empty($xprtcrosssellingproductblock)}
	{if isset($xprtcrosssellingproductblock.device)}
		{assign var=device_data value=$xprtcrosssellingproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtcrosssellingproductblock) && $xprtcrosssellingproductblock}
		<div id="xprt_crosssellproductsblock_tab_{if isset($xprtcrosssellingproductblock.id_xprtcrosssellingproductblock)}{$xprtcrosssellingproductblock.id_xprtcrosssellingproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtcrosssellingproductblock.products class='xprt_crosssellproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_crosssellproductsblock_tab_{if isset($xprtcrosssellingproductblock.id_xprtcrosssellingproductblock)}{$xprtcrosssellingproductblock.id_xprtcrosssellingproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No products at this time.' mod='xprtcrosssellingproductblock'}</p>
		</div>
	{/if}
{/if}