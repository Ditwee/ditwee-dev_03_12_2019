{if isset($xprtcrossviewproductblock) && !empty($xprtcrossviewproductblock)}
	{if isset($xprtcrossviewproductblock.device)}
		{assign var=device_data value=$xprtcrossviewproductblock.device|json_decode:true}
	{/if}
	<div id="xprtcrossviewproductblock_{$xprtcrossviewproductblock.id_xprtcrossviewproductblock}" class="xprtcrossviewproductblock xprt_default_products_block" style="margin:{$xprtcrossviewproductblock.section_margin};">
		<div class="page_title_area {*$xprt.home_title_style*}">
			{if isset($xprtcrossviewproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtcrossviewproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtcrossviewproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtcrossviewproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		{if isset($xprtcrossviewproductblock) && $xprtcrossviewproductblock}
			<div id="xprt_crossviewedproductsblock_{$xprtcrossviewproductblock.id_xprtcrossviewproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtcrossviewproductblock.products id='' class="xprt_crossviewedproductsblock {if $xprtcrossviewproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtcrossviewproductblock'}</p>
			</div>
		{/if}
	</div>
<script type="text/javascript">
	var xprtcrossviewproductblock = $("#xprtcrossviewproductblock_{$xprtcrossviewproductblock.id_xprtcrossviewproductblock}");
	var sliderSelect = xprtcrossviewproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtcrossviewproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtcrossviewproductblock.nav_arrow_style) && ($xprtcrossviewproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}
	
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtcrossviewproductblock.play_again)}{$xprtcrossviewproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtcrossviewproductblock.autoplay)}{$xprtcrossviewproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtcrossviewproductblock.pause_on_hover)}{$xprtcrossviewproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtcrossviewproductblock.navigation_dots)}{$xprtcrossviewproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtcrossviewproductblock.navigation_arrow)}{$xprtcrossviewproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtcrossviewproductblock.product_rows)}{$xprtcrossviewproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtcrossviewproductblock.product_scroll) && ($xprtcrossviewproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtcrossviewproductblock.product_scroll) && ($xprtcrossviewproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtcrossviewproductblock.product_scroll) && ($xprtcrossviewproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtcrossviewproductblock.product_scroll) && ($xprtcrossviewproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtcrossviewproductblock.product_scroll) && ($xprtcrossviewproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtcrossviewproductblock.product_scroll) && ($xprtcrossviewproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}