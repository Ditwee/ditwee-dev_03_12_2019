{if isset($xprtcrossviewproductblock) && !empty($xprtcrossviewproductblock)}
{if isset($xprtcrossviewproductblock.device)}
	{assign var=device_data value=$xprtcrossviewproductblock.device|json_decode:true}
{/if}
	<div class="xprtcrossviewproductblock block carousel">
		<h4 class="title_block">
	    	{$xprtcrossviewproductblock.title}
	    </h4>
	    <div class="block_content">
	        {if isset($xprtcrossviewproductblock) && $xprtcrossviewproductblock}
	        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtcrossviewproductblock.products }
	        {else}
	        	<p class="alert alert-info">{l s='No products at this time.' mod='xprtcrossviewproductblock'}</p>
	        {/if}
	    </div>
	</div>
{/if}