{if isset($xprtcrossviewproductblock) && !empty($xprtcrossviewproductblock)}
{if isset($xprtcrossviewproductblock.device)}
	{assign var=device_data value=$xprtcrossviewproductblock.device|json_decode:true}
{/if}
		{if isset($xprtcrossviewproductblock) && $xprtcrossviewproductblock}
			<div id="xprt_crossviewedproductsblock_tab_{if isset($xprtcrossviewproductblock.id_xprtcrossviewproductblock)}{$xprtcrossviewproductblock.id_xprtcrossviewproductblock}{/if}" class="tab-pane fade">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtcrossviewproductblock.products class='xprt_crossviewedproductsblock ' id=''}
			</div>
		{else}
			<div id="xprt_crossviewedproductsblock_tab_{if isset($xprtcrossviewproductblock.id_xprtcrossviewproductblock)}{$xprtcrossviewproductblock.id_xprtcrossviewproductblock}{/if}" class="tab-pane fade">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtcrossviewproductblock'}</p>
			</div>
		{/if}
{/if}