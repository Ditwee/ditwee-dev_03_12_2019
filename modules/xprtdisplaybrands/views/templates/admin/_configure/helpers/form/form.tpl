{extends file="helpers/form/form.tpl"}
{block name="description"}
	{if $input.type == 'file'}
		{if isset($input.type) && $input.type == "file"}
			
			{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
				<img src="{$image_baseurl}{$fields_value[$input.name]}" height="200px" width="auto"/>
			{/if}
			{if isset($input.desc) && !empty($input.desc)}
				<p>{$input.desc}</p>
			{/if}
		{/if}
	{else}
		{$smarty.block.parent}
	{/if}
{/block}