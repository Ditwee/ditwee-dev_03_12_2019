<div class="kr_brands_area m_bottom_35 xprt_parallax_section kr_parallax_section {if isset($xprtDB_layout)}{$xprtDB_layout}{/if}" style="background: url({$image_baseurl}{$xprtDB_bgimage}) no-repeat scroll center center/ cover;height:{$xprtDB_height};margin:{if isset($xprtDB_margin)}{$xprtDB_margin}{/if};">
	<div class="container">
		<div class="row">
			<div class="kr_brands_left col-sm-4">
				<h2 class="kr_brands_title">{$xprtDB_title}</h2>
				<h4 class="kr_brands_subtitle">{$xprtDB_subtitle}</h4>
				<p>{$xprtDB_content}</p>
			</div>
			<div class="kr_brands_right col-sm-8">
				<ul class="row kr_brands {if $xprtDB_DS == 'slider' || $xprtDB_DS == 'container_width_slider'}carousel{/if}">
					{foreach $brands as $brand}
					<li class="brand_list col-sm-{if !empty($xprtDB_Col)}{12/$xprtDB_Col}{else}3{/if} m_bottom_30">
						<div class="brand_list_content">
							<a class="brand_thumbnail" href="{$link->getmanufacturerLink($brand.id_manufacturer, $brand.link_rewrite)}" title="{$brand.name|escape:html:'UTF-8'}">
						        <img src="{$img_manu_dir}{$brand.id_manufacturer|escape:'htmlall':'UTF-8'}-{$imagetype.name}.jpg" alt="{$brand.name|escape:html:'UTF-8'}" class="img-responsive" height="{$imagetype.height}" width="{$imagetype.width}" />
						    </a>
						</div>
					</li>
					{/foreach}
				</ul>
			</div>
		</div>
	</div>
</div> <!-- kr_brands_area -->
{if isset($xprtDB_Col)}
	{addJsDef xprtBrandsColumn=$xprtDB_Col|intval}
{/if}