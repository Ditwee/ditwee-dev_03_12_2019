<div class="kr_brands_area m_bottom_35 {if isset($xprtDB_DS)}{$xprtDB_DS}{/if}" style="margin:{if isset($xprtDB_margin)}{$xprtDB_margin}{/if};">
	<ul class="row kr_brands {if $xprtDB_DS == 'slider' || $xprtDB_DS == 'container_width_slider'}carousel{/if}">
		{foreach $brands as $brand}
		<li class="brand_list col-sm-{if !empty($xprtDB_Col)}{12/$xprtDB_Col}{else}3{/if} m_bottom_30">
			<div class="brand_list_content">
				<a class="brand_thumbnail" href="{$link->getmanufacturerLink($brand.id_manufacturer, $brand.link_rewrite)}" title="{$brand.name|escape:html:'UTF-8'}">
			        <img src="{$img_manu_dir}{$brand.id_manufacturer|escape:'htmlall':'UTF-8'}-{$imagetype.name}.jpg" alt="{$brand.name|escape:html:'UTF-8'}" class="img-responsive" height="{$imagetype.height}" width="{$imagetype.width}" />
			    </a>
			</div>
		</li>
		{/foreach}
	</ul>
</div>