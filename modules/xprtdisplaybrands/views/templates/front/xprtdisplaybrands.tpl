<div class="kr_brands_area">
	<h3 class="page-heading {$xprt.page_heading_style} {$xprt.page_heading_position}">
		<em>{l s='Brands' mod='xprtdisplaybrands'}</em>
			<span class="heading_carousel_arrow"></span>
	</h3>
	<ul class="row kr_brands {if $xprtDB_DS == 'slider'}carousel{/if}">
		{foreach $brands as $brand}
		<li class="brand_list col-sm-2">
			<div class="brand_list_content">
				<a class="brand_thumbnail" href="{$link->getmanufacturerLink($brand.id_manufacturer, $brand.link_rewrite)}" title="{$brand.name|escape:html:'UTF-8'}">
			        <img src="{$img_manu_dir}{$brand.id_manufacturer|escape:'htmlall':'UTF-8'}-{$imagetype.name}.jpg" alt="{$brand.name|escape:html:'UTF-8'}" class="img-responsive" height="{$imagetype.height}" width="{$imagetype.width}" />
			    </a>
			</div>
		</li>
		{/foreach}
	</ul>
</div>