<div class="prod_brand_image">
	<a class="brand_thumbnail" href="{$link->getmanufacturerLink($product->id_manufacturer, $product->link_rewrite)}" title="{$product->name|escape:html:'UTF-8'}">
		<img class="img-responsive" src="{$img_manu_dir}{$product->id_manufacturer}-medium_default.jpg">
	</a>
</div>