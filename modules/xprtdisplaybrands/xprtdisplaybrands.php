<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
class xprtdisplaybrands extends Module
{
	public $DB_fields_name;
	public static $module_name = "xprtdisplaybrands";
	public function __construct()
	{
		$this->name = 'xprtdisplaybrands';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		$this->DB_fields_name = array('xprtDB_title','xprtDB_subtitle','xprtDB_content','xprtDB_COUNT','xprtDB_Col','xprtDB_height','xprtDB_margin','xprtDB_bgimage','xprtDB_bgimage_enable','xprtDB_DS','xprtDB_IT','xprtDB_parallax','xprtDB_layout');
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Brands');
		$this->description = $this->l('Great Store Display Brands Show In xprt Theme.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->registerHook('displayHeader')
			|| !$this->registerHook('displayHomeBottom')
			|| !$this->xpertsampledata()
		)
			return false;
		return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
    		$this->{$func}();
	    }
	    return true;
	}
	public function xpertsample_demo_2()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue("xprtDB_title_".$l['id_lang'],"Shop By Brands");
			Configuration::updateValue("xprtDB_subtitle_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_content_".$l['id_lang'],"");
		}
			Configuration::updateValue("xprtDB_COUNT",8);
			Configuration::updateValue("xprtDB_Col",3);
			Configuration::updateValue("xprtDB_height","");
			Configuration::updateValue("xprtDB_margin","");
			Configuration::updateValue("xprtDB_bgimage","");
			Configuration::updateValue("xprtDB_bgimage_enable","0");
			Configuration::updateValue("xprtDB_DS","slider");
			Configuration::updateValue("xprtDB_IT","manufacture_default");
			Configuration::updateValue("xprtDB_parallax","0");
			Configuration::updateValue("xprtDB_layout","default");
	}
	public function xpertsample_demo_1()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue("xprtDB_title_".$l['id_lang'],"Shop By Brands");
			Configuration::updateValue("xprtDB_subtitle_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_content_".$l['id_lang'],"");
		}
			Configuration::updateValue("xprtDB_COUNT",8);
			Configuration::updateValue("xprtDB_Col",3);
			Configuration::updateValue("xprtDB_height","");
			Configuration::updateValue("xprtDB_margin","");
			Configuration::updateValue("xprtDB_bgimage","");
			Configuration::updateValue("xprtDB_bgimage_enable","0");
			Configuration::updateValue("xprtDB_DS","slider");
			Configuration::updateValue("xprtDB_IT","manufacture_default");
			Configuration::updateValue("xprtDB_parallax","0");
			Configuration::updateValue("xprtDB_layout","default");
	}
	public function xpertsample_demo_3()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue("xprtDB_title_".$l['id_lang'],"Shop By Brands");
			Configuration::updateValue("xprtDB_subtitle_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_content_".$l['id_lang'],"");
		}
			Configuration::updateValue("xprtDB_COUNT",8);
			Configuration::updateValue("xprtDB_Col",3);
			Configuration::updateValue("xprtDB_height","");
			Configuration::updateValue("xprtDB_margin","");
			Configuration::updateValue("xprtDB_bgimage","");
			Configuration::updateValue("xprtDB_bgimage_enable","0");
			Configuration::updateValue("xprtDB_DS","slider");
			Configuration::updateValue("xprtDB_IT","manufacture_default");
			Configuration::updateValue("xprtDB_parallax","0");
			Configuration::updateValue("xprtDB_layout","default");
	}
	public function xpertsample_demo_4()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue("xprtDB_title_".$l['id_lang'],"Shop By Brands");
			Configuration::updateValue("xprtDB_subtitle_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_content_".$l['id_lang'],"");
		}
			Configuration::updateValue("xprtDB_COUNT",8);
			Configuration::updateValue("xprtDB_Col",3);
			Configuration::updateValue("xprtDB_height","");
			Configuration::updateValue("xprtDB_margin","40px auto");
			Configuration::updateValue("xprtDB_bgimage","");
			Configuration::updateValue("xprtDB_bgimage_enable","0");
			Configuration::updateValue("xprtDB_DS","container_width_slider");
			Configuration::updateValue("xprtDB_IT","manufacture_default");
			Configuration::updateValue("xprtDB_parallax","0");
			Configuration::updateValue("xprtDB_layout","default");
	}
	public function xpertsample_demo_5()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue("xprtDB_title_".$l['id_lang'],"Shop By Brands");
			Configuration::updateValue("xprtDB_subtitle_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_content_".$l['id_lang'],"");
		}
			Configuration::updateValue("xprtDB_COUNT",8);
			Configuration::updateValue("xprtDB_Col",3);
			Configuration::updateValue("xprtDB_height","");
			Configuration::updateValue("xprtDB_margin","");
			Configuration::updateValue("xprtDB_bgimage","");
			Configuration::updateValue("xprtDB_bgimage_enable","0");
			Configuration::updateValue("xprtDB_DS","slider");
			Configuration::updateValue("xprtDB_IT","manufacture_default");
			Configuration::updateValue("xprtDB_parallax","0");
			Configuration::updateValue("xprtDB_layout","default");
	}
	public function xpertsample_demo_6()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue("xprtDB_title_".$l['id_lang'],"Shop By Brands");
			Configuration::updateValue("xprtDB_subtitle_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_content_".$l['id_lang'],"");
		}
			Configuration::updateValue("xprtDB_COUNT",8);
			Configuration::updateValue("xprtDB_Col",6);
			Configuration::updateValue("xprtDB_height","");
			Configuration::updateValue("xprtDB_margin","0px 0px 60px 0px");
			Configuration::updateValue("xprtDB_bgimage","");
			Configuration::updateValue("xprtDB_bgimage_enable","0");
			Configuration::updateValue("xprtDB_DS","slider");
			Configuration::updateValue("xprtDB_IT","manufacture_default");
			Configuration::updateValue("xprtDB_parallax","0");
			Configuration::updateValue("xprtDB_layout","default");
	}
	public function xpertsample_demo_7()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue("xprtDB_title_".$l['id_lang'],"Shop By Brands");
			Configuration::updateValue("xprtDB_subtitle_".$l['id_lang'],"Check out the best brands to stay on trend with us");
			Configuration::updateValue("xprtDB_content_".$l['id_lang'],"Lorem ipsum dolor sit amete, consectetur adipisicing sed do eiusmod tempor icididunt ut labore etel dolore magna aliqua.
enim adminim veniam, done quisnostrud exercitation ullommodo.");
		}
			Configuration::updateValue("xprtDB_COUNT",8);
			Configuration::updateValue("xprtDB_Col",3);
			Configuration::updateValue("xprtDB_height","320px");
			Configuration::updateValue("xprtDB_margin","0px 0px 30px 0px");
			Configuration::updateValue("xprtDB_bgimage","newsletter-bg.jpg");
			Configuration::updateValue("xprtDB_bgimage_enable","1");
			Configuration::updateValue("xprtDB_DS","slider");
			Configuration::updateValue("xprtDB_IT","manufacture_default");
			Configuration::updateValue("xprtDB_parallax","1");
			Configuration::updateValue("xprtDB_layout","parallax-style-one");
	}
	public function xpertsample_demo_8()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue("xprtDB_title_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_subtitle_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_content_".$l['id_lang'],"");
		}
			Configuration::updateValue("xprtDB_COUNT",8);
			Configuration::updateValue("xprtDB_Col",3);
			Configuration::updateValue("xprtDB_height","");
			Configuration::updateValue("xprtDB_margin","");
			Configuration::updateValue("xprtDB_bgimage","");
			Configuration::updateValue("xprtDB_bgimage_enable","0");
			Configuration::updateValue("xprtDB_DS","slider");
			Configuration::updateValue("xprtDB_IT","manufacture_default");
			Configuration::updateValue("xprtDB_parallax","0");
			Configuration::updateValue("xprtDB_layout","default");
	}
	public function xpertsample_demo_12()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue("xprtDB_title_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_subtitle_".$l['id_lang'],"");
			Configuration::updateValue("xprtDB_content_".$l['id_lang'],"");
		}
			Configuration::updateValue("xprtDB_COUNT",8);
			Configuration::updateValue("xprtDB_Col",4);
			Configuration::updateValue("xprtDB_height","");
			Configuration::updateValue("xprtDB_margin","");
			Configuration::updateValue("xprtDB_bgimage","");
			Configuration::updateValue("xprtDB_bgimage_enable","0");
			Configuration::updateValue("xprtDB_DS","slider");
			Configuration::updateValue("xprtDB_IT","manufacture_default");
			Configuration::updateValue("xprtDB_parallax","0");
			Configuration::updateValue("xprtDB_layout","default");
	}
	public function uninstall()
	{
		if(!parent::uninstall()
		)
			return false;
		return true;
	}
	public function getManufacturers($limit=NULL,$id_lang){
		$sql = 'SELECT m.*, ml.`description`, ml.`short_description`
		FROM `'._DB_PREFIX_.'manufacturer` m
		'.Shop::addSqlAssociation('manufacturer', 'm').'
		INNER JOIN `'._DB_PREFIX_.'manufacturer_lang` ml ON (m.`id_manufacturer` = ml.`id_manufacturer` AND ml.`id_lang` = '.(int)$id_lang.')
		WHERE m.`active` = 1 ORDER BY m.`name` ASC ';
		if($limit != NULL)
			$sql .= ' LIMIT '.$limit;
		$manufacturers = Db::getInstance()->executeS($sql);
		return $manufacturers;
	}
	public function getBrands()
	{
		$count = (int)Configuration::get('xprtDB_COUNT');
		$id_lang = (int)$this->context->language->id;
		$brands = self::getManufacturers((int)$count,$id_lang);
		if($brands === false)
			return false;
		$count_brands = count($brands);
		$rewrite_set = (int)Configuration::get('PS_REWRITING_SETTINGS');
		if(isset($brands) && !empty($brands))
			foreach($brands as &$brand){
				if($rewrite_set){
					$brand['link_rewrite'] = Tools::link_rewrite($brand['name']);
				}
				else{
					$brand['link_rewrite'] = 0;
				}
			}
		return $brands;
	}
	public function GetImageSize()
	{
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'image_type`  WHERE `manufacturers` = 1';
		$image_types = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if($image_types === false)
			return false;
		return $image_types;
	}
	public function GetImageTypeSize()
	{
		$type = Configuration::get('xprtDB_IT');
		$sizearr = array();
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'image_type`  WHERE `manufacturers` = 1';
		$image_types = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		if($image_types === false)
			return false;
		if(!empty($image_types) && isset($image_types)){
			foreach($image_types as $key => $image_type){
				if($image_type['name'] == $type){
					$sizearr['name'] = $image_type['name'];
					$sizearr['height'] = $image_type['height'];
					$sizearr['width'] = $image_type['width'];
				}	
			}
		}
		return $sizearr;
	}
	public function getContent()
	{
		if(Tools::isSubmit('submit'.$this->name))
		{
			$languages = Language::getLanguages(false);
			$arr_val = array();
			foreach($this->DB_fields_name as $DB_fields_name)
			{
				if($DB_fields_name == 'xprtDB_bgimage'){
					if(isset($_FILES[$DB_fields_name]) && isset($_FILES[$DB_fields_name]['tmp_name']) && !empty($_FILES[$DB_fields_name]['tmp_name'])){
						$xprtDB_bgimage = $this->processImage($DB_fields_name);
						Configuration::updateValue($DB_fields_name,$xprtDB_bgimage);
					}
				}elseif($DB_fields_name == 'xprtDB_title' || $DB_fields_name == 'xprtDB_subtitle' || $DB_fields_name == 'xprtDB_content'){
					foreach ($languages as $lang)
					{
						Configuration::updateValue($DB_fields_name."_".$lang['id_lang'],Tools::getValue($DB_fields_name."_".$lang['id_lang']));
					}
				}else{
					Configuration::updateValue($DB_fields_name, Tools::getValue($DB_fields_name));
				}
			}
		}
		return $this->renderForm();
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('xprt DisplayBrands Settings'),
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Brand Title'),
						'name' => 'xprtDB_title',
						'lang' => true,
						'desc' => $this->l('Please Enter Brands Title'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Brand SubTitle'),
						'name' => 'xprtDB_subtitle',
						'lang' => true,
						'desc' => $this->l('Please Enter Brands SubTitle'),
					),
					array(
						'type' => 'textarea',
						'label' => $this->l('Brand Description'),
						'name' => 'xprtDB_content',
						'lang' => true,
						'desc' => $this->l('Please Enter Brand Description'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Number of Brands You Want to Display'),
						'name' => 'xprtDB_COUNT',
						'suffix' => 'Number',
						'class' => 'fixed-width-lg',
						'desc' => $this->l('Number Of Brands You Want to Display.(Must be Input Integer Number)'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Number of column You Want to Display'),
						'name' => 'xprtDB_Col',
						'suffix' => 'Number',
						'class' => 'fixed-width-lg',
						'desc' => $this->l('Number Of column You Want to Display.(Must be Input Integer Number like: 2, 3, 4, 6)'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Maximum Height Of Brands Block'),
						'name' => 'xprtDB_height',
						'class' => 'fixed-width-lg',
						'suffix' => 'pixels',
						'desc' => $this->l('Maximum Height Of Brands Block'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Margin'),
						'name' => 'xprtDB_margin',
						'class' => 'fixed-width-lg',
						'desc' => $this->l('Please Enter Margin. sample values structure 0px 0px 0px 0px .(top right bottom left)'),
					),
					array(
						'type' => 'file',
						'label' => $this->l('Select a Background Image File From Computer'),
						'name' => 'xprtDB_bgimage',
						'desc' => $this->l(sprintf('Maximum image size: %s.', ini_get('upload_max_filesize')))
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enabled Background Image ?'),
						'name' => 'xprtDB_bgimage_enable',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'xprtDB_bgimage_enable_on',
								'value' => 1,
								'label' => $this->l('Enable')
							),
							array(
								'id' => 'xprtDB_bgimage_enable_off',
								'value' => 0,
								'label' => $this->l('Disable')
							)
						),
					),
					array(
	                    'type' => 'select',
	                    'label' => $this->l('Display Style'),
	                    'name' => 'xprtDB_DS',
	                    'options' => array(
	                        		'query' => array(
										array('id' => 'general', 'name' => 'General'),
										array('id' => 'slider', 'name' => 'Slider'),
										array('id' => 'container_width_slider', 'name' => 'Container Width slider'),
								    ),
	                        'id' => 'id',
	                        'name' => 'name',
	                    )
                	),
                	array(
	                    'type' => 'select',
	                    'label' => $this->l('Layout Style'),
	                    'name' => 'xprtDB_layout',
	                    'options' => array(
	                        		'query' => array(
										array('id' => 'default', 'name' => 'Default'),
										array('id' => 'parallax-style-one', 'name' => 'Parallax style one'),
										array('id' => 'parallax-style-two', 'name' => 'Parallax style two'),
										// array('id' => 'container_width_slider', 'name' => 'Container Width slider'),
								    ),
	                        'id' => 'id',
	                        'name' => 'name',
	                    )
                	),
                	array(
	                    'type' => 'select',
	                    'label' => $this->l('Select Image Type:'),
	                    'name' => 'xprtDB_IT',
	                    'options' => array(
	                        		'query' => $this->GetImageSize(),
	                        'id' => 'name',
	                        'name' => 'name',
	                    )
                	),
                	array(
						'type' => 'switch',
						'label' => $this->l('Is Parallax In Background Image ? '),
						'name' => 'xprtDB_parallax',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'xprtDB_parallax_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'xprtDB_parallax_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->module = $this;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
			'image_baseurl' => $this->_path.'images/',
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		$arr_val = array();
		$languages = Language::getLanguages(false);
		foreach($this->DB_fields_name as $DB_fields_name)
		{
			if($DB_fields_name == 'xprtDB_title' || $DB_fields_name == 'xprtDB_subtitle' || $DB_fields_name == 'xprtDB_content')
			{
				foreach ($languages as $lang)
				{
					$arr_val[$DB_fields_name][$lang['id_lang']] = Tools::getValue($DB_fields_name."_".$lang['id_lang'], Configuration::get($DB_fields_name."_".$lang['id_lang']));
				}
			}
			else
			{
				$arr_val[$DB_fields_name] = Tools::getValue($DB_fields_name, Configuration::get($DB_fields_name));
			}
		}
		return $arr_val;
	}
	public function processImage($file_name = null)
	{
		if($file_name == null)
			return false;
	    if(isset($_FILES[$file_name]) && isset($_FILES[$file_name]['tmp_name']) && !empty($_FILES[$file_name]['tmp_name']))
	    {
            $ext = substr($_FILES[$file_name]['name'], strrpos($_FILES[$file_name]['name'], '.') + 1);
            $basename_file_name = basename($_FILES[$file_name]["name"]);
            $strlen = strlen($basename_file_name);
            $strlen_ext = strlen($ext);
            $basename_file_name = substr($basename_file_name,0,($strlen-$strlen_ext));
            $link_rewrite_file_name = Tools::link_rewrite($basename_file_name);
            $file_orgname = $link_rewrite_file_name.'.'.$ext;
            $path = _PS_MODULE_DIR_.self::$module_name.'/images/' . $file_orgname;
            if(!move_uploaded_file($_FILES[$file_name]['tmp_name'],$path))
                return false;         
            else
                return $file_orgname;   
	    }
	    else
	    {
	    	return false;
	    }
	}
	public function AssignSmartyValues($design = null)
	{
		$arr_val = array();
		$id_lang_def = (int)Configuration::get("PS_LANG_DEFAULT");
		$languages = Language::getLanguages(false);
		if(!empty($this->DB_fields_name) && isset($this->DB_fields_name))
		{
			foreach($this->DB_fields_name as $DB_fields_name)
			{
				if($DB_fields_name == 'xprtDB_title' || $DB_fields_name == 'xprtDB_subtitle' || $DB_fields_name == 'xprtDB_content')
				{
					foreach ($languages as $lang)
					{
						if(Configuration::get($DB_fields_name."_".$lang['id_lang'])){
							$arr_val[$DB_fields_name] = Configuration::get($DB_fields_name."_".$lang['id_lang']);
						}else{
							$arr_val[$DB_fields_name] = Configuration::get($DB_fields_name."_".$id_lang_def);
						}
					}
				}
				else
				{
					$arr_val[$DB_fields_name] = Configuration::get($DB_fields_name);
				}
			}
		}
		$arr_val['imagetype'] = $this->GetImageTypeSize();
		$arr_val['brands'] = $this->getBrands();
		$arr_val['image_baseurl'] = $this->_path.'images/';
		$this->smarty->assign($arr_val);
		$tpl = 'default';
		if($design == null){
			$xprtDB_layout = Configuration::get('xprtDB_layout');
			if(isset($xprtDB_layout) && !empty($xprtDB_layout)){
				$tpl = $xprtDB_layout;
			}
		}else{
			$tpl = $design;
		}
		return $this->display(__FILE__, 'views/templates/front/'.$tpl.'.tpl');
	}
	public function PreHookEXE($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayHome($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayHomeMiddle($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayhometopright($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayHomeTopLeftRightBottom($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayFooterTop($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayFooterBottom($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayHomeBottom($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayCatBottom($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayFooterTopFullwidth($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayProductRightSidebar($params)
	{
		return $this->AssignSmartyValues('xprtdisplaybrandsproductsidebar');
	}
	public function hookdisplayFooterProduct($params)
	{
		return $this->AssignSmartyValues();
	}
	public function hookdisplayHeader($params)
	{
		// $this->context->controller->addCSS();
		// $this->context->controller->addJS();
	}
}