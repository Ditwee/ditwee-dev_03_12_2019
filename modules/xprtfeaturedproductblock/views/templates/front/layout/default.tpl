{if isset($xprtfeaturedproductblock) && !empty($xprtfeaturedproductblock)}
	{if isset($xprtfeaturedproductblock.device)}
		{assign var=device_data value=$xprtfeaturedproductblock.device|json_decode:true}
	{/if}
	<div id="xprtfeaturedproductblock_{$xprtfeaturedproductblock.id_xprtfeaturedproductblock}" class="xprtfeaturedproductblock xprt_default_products_block" style="margin:{$xprtfeaturedproductblock.section_margin};">
		
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtfeaturedproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtfeaturedproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtfeaturedproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtfeaturedproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>

		{if isset($xprtfeaturedproductblock) && $xprtfeaturedproductblock}
			<div id="xprt_featuredproductsblock_{$xprtfeaturedproductblock.id_xprtfeaturedproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtfeaturedproductblock.products id='' class="xprt_featuredproductsblock {if $xprtfeaturedproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtfeaturedproductblock'}</p>
			</div>
		{/if}
	</div>
<script type="text/javascript">
	var xprtfeaturedproductblock = $("#xprtfeaturedproductblock_{$xprtfeaturedproductblock.id_xprtfeaturedproductblock}");
	var sliderSelect = xprtfeaturedproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtfeaturedproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtfeaturedproductblock.nav_arrow_style) && ($xprtfeaturedproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}

	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtfeaturedproductblock.play_again)}{$xprtfeaturedproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtfeaturedproductblock.autoplay)}{$xprtfeaturedproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtfeaturedproductblock.pause_on_hover)}{$xprtfeaturedproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtfeaturedproductblock.navigation_dots)}{$xprtfeaturedproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtfeaturedproductblock.navigation_arrow)}{$xprtfeaturedproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtfeaturedproductblock.product_rows)}{$xprtfeaturedproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtfeaturedproductblock.product_scroll) && ($xprtfeaturedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtfeaturedproductblock.product_scroll) && ($xprtfeaturedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtfeaturedproductblock.product_scroll) && ($xprtfeaturedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtfeaturedproductblock.product_scroll) && ($xprtfeaturedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtfeaturedproductblock.product_scroll) && ($xprtfeaturedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}