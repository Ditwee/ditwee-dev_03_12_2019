{if isset($xprtfeaturedproductblock) && !empty($xprtfeaturedproductblock)}
	{if isset($xprtfeaturedproductblock.device)}
		{assign var=device_data value=$xprtfeaturedproductblock.device|json_decode:true}
	{/if}
	<div class="xprt_product_home_small col-sm-4">
		<div class="xprtfeaturedproductblock block carousel">
			<h4 class="title_block">
		    	{$xprtfeaturedproductblock.title}
		    </h4>
		    <div class="block_content products-block">
		        {if isset($xprtfeaturedproductblock) && $xprtfeaturedproductblock}
		        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtfeaturedproductblock.products}
		        {else}
		        	<p class="alert alert-info">{l s='No products at this time.' mod='xprtfeaturedproductblock'}</p>
		        {/if}
		    </div>
		</div>
	</div>
{/if}