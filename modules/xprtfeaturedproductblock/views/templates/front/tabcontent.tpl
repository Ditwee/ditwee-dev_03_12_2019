{if isset($xprtfeaturedproductblock) && !empty($xprtfeaturedproductblock)}
	{if isset($xprtfeaturedproductblock.device)}
		{assign var=device_data value=$xprtfeaturedproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtfeaturedproductblock.products) && !empty($xprtfeaturedproductblock.products)}
		<div id="xprt_featuredproductsblock_tab_{if isset($xprtfeaturedproductblock.id_xprtfeaturedproductblock)}{$xprtfeaturedproductblock.id_xprtfeaturedproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtfeaturedproductblock.products class='xprt_featuredproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_featuredproductsblock_tab_{if isset($xprtfeaturedproductblock.id_xprtfeaturedproductblock)}{$xprtfeaturedproductblock.id_xprtfeaturedproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No products at this time.' mod='xprtfeaturedproductblock'}</p>
		</div>
	{/if}
{/if}