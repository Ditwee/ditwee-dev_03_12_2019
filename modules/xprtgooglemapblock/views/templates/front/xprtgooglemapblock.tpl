<div id="xprtgmaps" class="xprtgmaps">
	 <h4 class="d_none">{l s='Find our store' mod='xprtgooglemapblock'}</h4>
	 <div class="bock_content">
	 	<div id="{$xprtGM_KEY}"></div>
	 </div>
</div>
{if !isset($xprtGM_CNTRL) || empty($xprtGM_CNTRL)} 
    {$xprtGM_CNTRL='false'}
{/if}
{if !isset($xprtGM_SCRLWHL) || empty($xprtGM_SCRLWHL)} 
    {$xprtGM_SCRLWHL='false'}
{/if}
<script type="text/javascript">
    jQuery(function($) { 
    	$(window).load(function() {
            var mapelem = $("#{$xprtGM_KEY}");
            mapelem.css( { width:{$xprtGM_WDTH},height:'{$xprtGM_HGHT}' } );
            var myLatLng = { lat: parseFloat({$xprtGM_LAT}), lng: parseFloat({$xprtGM_LNG}) };
            var map = new google.maps.Map(document.getElementById("{$xprtGM_KEY}"), {
            	zoom: {$xprtGM_ZOOM},
            	center: myLatLng,
            	disableDefaultUI: true,
            	scrollwheel: {$xprtGM_SCRLWHL},
            	navigationControl: false,
            	mapTypeControl: {$xprtGM_CNTRL},
            	scaleControl: false,
            	draggable: true,
            	mapTypeId: google.maps.MapTypeId.{$xprtGM_TYPE}
            });

             var marker = new google.maps.Marker({
             	position: myLatLng,
             	map: map,
             	draggable: true,
             	icon:'{$modules_dir}xprtgooglemapblock/img/map_icon.png'
             });
         } ); 
    } ); 
</script>