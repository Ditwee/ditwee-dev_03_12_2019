<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
class xprtgooglemapblock extends Module
{
	public $GM_fields_name;
	public $hooks_url = '/hooks/hooks.php';
	public $css_files_url = '/css/css.php';
	public $js_files_url = '/js/js.php';
	public $tabs_files_url = '/tabs/tabs.php';
	public $mysql_files_url = '/mysqlquery/mysqlquery.php';
	public $data_url = '/data/data.php';
	public $icon_url = '/icon/icon.php';
	public function __construct()
	{
		$this->name = 'xprtgooglemapblock';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		$this->GM_fields_name = array('xprtGM_LAT','xprtGM_LNG','xprtGM_HGHT','xprtGM_ZOOM','xprtGM_TYPE','xprtGM_CNTRL','xprtGM_SCRLWHL','xprtGM_WDTH','xprtGM_API');
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Google Maps');
		$this->description = $this->l('Great Store Google Maps Show In xprt Theme.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
		 || !$this->Register_SQL()
		 || !$this->Register_Config()
		 || !$this->Register_Hooks()
		 || !$this->Register_Tabs()
		 || !$this->xpertsampledata()
		)
			return false;
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
		 || !$this->UnRegister_Hooks()
		 || !$this->UnRegister_Config()
		 || !$this->UnRegister_SQL()
		 || !$this->UnRegister_Tabs()
		)
			return false;
		return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		Configuration::updateValue("xprtGM_LAT","48.8762495");
		Configuration::updateValue("xprtGM_LNG","2.3249656");
		Configuration::updateValue("xprtGM_HGHT","418");
		Configuration::updateValue("xprtGM_ZOOM","17");
		Configuration::updateValue("xprtGM_TYPE","TERRAIN");
		Configuration::updateValue("xprtGM_CNTRL","1");
		Configuration::updateValue("xprtGM_SCRLWHL","0");
		Configuration::updateValue("xprtGM_WDTH",'"100%"');
		Configuration::updateValue("xprtGM_API","AIzaSyCfYfGJEPizt6YtNwuP7jU-oAY2vhQCXnE");
	}
	public function Register_Config()
	{
			$data = array();
	        require_once(dirname(__FILE__).$this->data_url);
        if(isset($data) && !empty($data))
        	foreach($data as $data_key => $data_val):
        		Configuration::updateValue($data_key,$data_val);
        	endforeach;
        	return true;
        return false;
	}
	public function UnRegister_Config()
	{
			$data = array();
	        require_once(dirname(__FILE__).$this->data_url);
        if(isset($data) && !empty($data))
        	foreach($data as $data_key => $data_val):
        		Configuration::deleteByName($data_key);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
        		$this->registerHook($hook);
        	endforeach;
        	return true;
        return false;
	}
	public function UnRegister_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
	        		$hook_id = Module::getModuleIdByName($hook);
	        	if(isset($hook_id) && !empty($hook_id))
	        		$this->unregisterHook((int)$hook_id);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Css()
	{
		$css_files = array();
        require_once(dirname(__FILE__).$this->css_files_url);
        if(isset($css_files) && !empty($css_files))
        	foreach($css_files as $css_file):
        		$this->context->controller->addCSS($css_file);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Js()
	{
		$js_files = array();
        require_once(dirname(__FILE__).$this->js_files_url);
        if(isset($js_files) && !empty($js_files))
        	foreach($js_files as $js_file):
        		$this->context->controller->addJS($js_file);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_SQL()
	{
		$mysqlquery = array();
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($mysqlquery) && !empty($mysqlquery))
				foreach($mysqlquery as $query){
					if(!Db::getInstance()->Execute($query))
					    return false;
				}
        return true;
	}
	public function UnRegister_SQL()
	{
		$mysqlquery_u = array();
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($mysqlquery_u) && !empty($mysqlquery_u))
				foreach($mysqlquery_u as $query_u){
					if(!Db::getInstance()->Execute($query_u))
					    return false;
				}
        return true;
	}
	public function UnRegister_Tabs()
	{
		$tabs_lists = array();
			require_once(dirname(__FILE__) .$this->tabs_files_url);
			if(isset($tabs_lists) && !empty($tabs_lists))
	        foreach($tabs_lists as $tab_list){
	        	$tab_list_id = Tab::getIdFromClassName($tab_list['class_name']);
	            if(isset($tab_list_id) && !empty($tab_list_id)){
	                $tabobj = new Tab($tab_list_id);
	                $tabobj->delete();
	            }
	        }
        return true;
	}
	public function Register_Tabs()
	{
		$tabs_lists = array();
        $langs = Language::getLanguages();
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $save_tab_id = Configuration::get('xprtxpert_idea_tab');
        if(!isset($save_tab_id) && empty($save_tab_id)){
        	$tabobj = new Tab();
        	$tabobj->class_name = "Adminxprtcontentbox";
        	$tabobj->module = $this->name;
        	$tabobj->id_parent = 0;
        	foreach($langs as $l)
        	{
        	    $tabobj->name[$l['id_lang']] = $this->l('Xpert Settings');
        	}
        	$tabobj->save();
        	$save_tab_id = $tabobj->id;
        	Configuration::updateValue('xprtxpert_idea_tab',$save_tab_id);
        }
        	require_once(dirname(__FILE__) .$this->tabs_files_url);
        	if(isset($tabs_lists) && !empty($tabs_lists))
        	foreach ($tabs_lists as $tab_list)
        	{
        	    $tab_listobj = new Tab();
        	    $tab_listobj->class_name = $tab_list['class_name'];
        	    if($tab_list['id_parent'] == 'parent'){
        	    	$tab_listobj->id_parent = $save_tab_id;
        	    }else{
        	    	$tab_listobj->id_parent = $tab_list['id_parent'];
        	    }
        	    if(isset($tab_list['module']) && !empty($tab_list['module'])){
        	    	$tab_listobj->module = $tab_list['module'];
        	    }else{
        	    	$tab_listobj->module = $this->name;
        	    }
        	    foreach($langs as $l)
        	    {
        	    	$tab_listobj->name[$l['id_lang']] = $this->l($tab_list['name']);
        	    }
        	    $tab_listobj->save();
        	}
        return true;
    }
	public function getContent()
	{
		if(Tools::isSubmit('submit'.$this->name))
		{
			$arr_val = array();
			foreach($this->GM_fields_name as $GM_fields_name)
			{
				Configuration::updateValue($GM_fields_name, Tools::getValue($GM_fields_name));
			}
		}
		return $this->renderForm();
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('xprt Google Maps'),
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Latitude'),
						'name' => 'xprtGM_LAT',
						'desc' => $this->l('Latitude Must Be Numeric value'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Longitude'),
						'name' => 'xprtGM_LNG',
						'desc' => $this->l('Longitude Must Be Numeric value.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Height'),
						'name' => 'xprtGM_HGHT',
						'desc' => $this->l('Height in a Pixel Value.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Width'),
						'name' => 'xprtGM_WDTH',
						'desc' => $this->l('Width in a Pixel Value.(For Full Width: "100%"). Must be use double Quoto.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('API'),
						'name' => 'xprtGM_API',
						'desc' => $this->l('Enter Api key'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Zoom'),
						'name' => 'xprtGM_ZOOM',
						'desc' => $this->l('Zoom Must Be Numeric Value.'),
					),array(
                        'type' => 'select',
                        'label' => $this->l('Map Type'),
                        'name' => 'xprtGM_TYPE',
                        'required' => true,
                        'options' => array(
                            'query' => array(
                                array(
                                'id' => 'ROADMAP',
                                'name' => 'ROADMAP'
                                ),
                                array(
                                'id' => 'SATELLITE',
                                'name' => 'SATELLITE'
                                ),
                                array(
                                'id' => 'HYBRID',
                                'name' => 'HYBRID'
                                ),
                                array(
                                'id' => 'TERRAIN',
                                'name' => 'TERRAIN'
                                )
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),array(
	                    'type' => 'switch',
	                    'label' => $this->l('Controlling Map Type '),
	                    'name' => 'xprtGM_CNTRL',
	                    'class' => 't',
	                    'is_bool' => true,
	                    'values' => array(
	                        array(
		                        'id' => 'active',
		                        'value' => 1,
		                        'label' => $this->l('Enabled')
	                        ),
	                        array(
		                        'id' => 'active',
		                        'value' => 0,
		                        'label' => $this->l('Disabled')
	                        )
	                    )
	                ),array(
	                    'type' => 'switch',
	                    'label' => $this->l('Scroll Wheel'),
	                    'name' => 'xprtGM_SCRLWHL',
	                    'class' => 't',
	                    'is_bool' => true,
	                    'values' => array(
	                        array(
		                        'id' => 'active',
		                        'value' => 1,
		                        'label' => $this->l('Enabled')
	                        ),
	                        array(
		                        'id' => 'active',
		                        'value' => 0,
		                        'label' => $this->l('Disabled')
	                        )
	                    )
	                )
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		$arr_val = array();
		foreach($this->GM_fields_name as $GM_fields_name){
			$arr_val[$GM_fields_name] = Tools::getValue($GM_fields_name, Configuration::get($GM_fields_name));
		}
		return $arr_val;
	}
	public function hookDisplayHeader()
	{
		$this->context->controller->addJS('http'.((Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ? 's' : '').'://maps.google.com/maps/api/js?libraries=places&key='.Configuration::get('xprtGM_API'));
		$this->Register_Css();
		$this->Register_Js();
	}
	public function hookDisplayFooter($params)
	{
		$arr_val = array();
		$arr_val['xprtGM_HGHT'] = Configuration::get('xprtGM_HGHT');
		$arr_val['xprtGM_WDTH'] = Configuration::get('xprtGM_WDTH');
		$arr_val['xprtGM_API'] = Configuration::get('xprtGM_API');
		$arr_val['xprtGM_LAT'] = Configuration::get('xprtGM_LAT');
		$arr_val['xprtGM_LNG'] = Configuration::get('xprtGM_LNG');
		$arr_val['xprtGM_TYPE'] = Configuration::get('xprtGM_TYPE');
		$arr_val['xprtGM_ZOOM'] = Configuration::get('xprtGM_ZOOM');
		$arr_val['xprtGM_CNTRL'] = (bool)Configuration::get('xprtGM_CNTRL');
		$arr_val['xprtGM_SCRLWHL'] = (bool)Configuration::get('xprtGM_SCRLWHL');
		$arr_val['xprtGM_KEY'] = "xprtgmap_".rand(00000,99999);
		$this->smarty->assign($arr_val);
		return $this->display(__FILE__, 'views/templates/front/xprtgooglemapblock.tpl');
	}
	public function hookdisplayFooterTopFullwidth($params)
	{
		return $this->hookDisplayFooter($params);
	}
	public function hookdisplayFooterTop($params)
	{
		return $this->hookDisplayFooter($params);
	}
	public function hookdisplayFooterBottom($params)
	{
		return $this->hookDisplayFooter($params);
	}
	public function hookdisplayFloatLeftRight($params)
	{
		return $this->hookDisplayFooter($params);
	}
}