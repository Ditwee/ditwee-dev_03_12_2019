{capture name='displayHeaderNavIcon'}{hook h='displayHeaderNavIcon'}{/capture}
<div class="xprtheadernavigation header_icon_block">
	<div {if $xprt.header_top_nav_content == 'header_nav_sidebarpanel'}id="header_nav_icon"{/if} class="header_icon {if $xprt.header_top_nav_content == 'header_nav_regular' || empty($xprt.header_top_nav_content)}nav_icon_regular{/if}" data-action="toggle" data-side="left" data-id="header_nav_sidebarpanel">
		<i class="{if isset($xprt.header_nav_icon)}{$xprt.header_nav_icon}{else}icon-bars{/if}"></i>
	</div>
	{if $smarty.capture.displayHeaderNavIcon && $xprt.header_top_nav_content == 'header_nav_regular'}
		<div class="header_icon_content">
			{$smarty.capture.displayHeaderNavIcon}
		</div>
	{/if}
</div>