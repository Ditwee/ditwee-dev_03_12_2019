<div class="xprtheadernavigation header_block">
	<div class="title current">
		<i class="icon-cog"></i>
	</div>
	<ul class="header_block_content toogle_content">
		<li>
			<a 	href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|escape:'html':'UTF-8'}" title="{l s='My wishlists' mod='xprtheadernavigation'}">
				{l s='My wishlist' mod='xprtheadernavigation'}
			</a>
		</li>
		<li>
			<a 	href="{$link->getPageLink('products-comparison')|escape:'html':'UTF-8'}" title="{l s='Product Comparison' mod='xprtheadernavigation'}">
				<i class="icon-retweet"></i>
				{l s='Product Comparison' mod='xprtheadernavigation'}
			</a>
		</li>
		<li id="header_link_contact"><a href="{$link->getPageLink('contact', true)|escape:'html'}" title="{l s='contact' mod='xprtheadernavigation'}">{l s='Contact us' mod='xprtheadernavigation'}</a></li>
		<li id="header_link_sitemap"><a href="{$link->getPageLink('sitemap')|escape:'html'}" title="{l s='sitemap' mod='xprtheadernavigation'}">{l s='Sitemap' mod='xprtheadernavigation'}</a></li>
		<li id="header_link_bookmark">
			<script type="text/javascript">writeBookmarkLink('{$come_from}', '{$meta_title|addslashes|addslashes}', '{l s='Bookmark' mod='xprtheadernavigation' js=1}');</script>
		</li>
	</ul>
</div>