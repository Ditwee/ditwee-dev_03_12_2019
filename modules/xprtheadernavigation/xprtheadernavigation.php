<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_PS_VERSION_'))
	exit;
class xprtheadernavigation extends Module
{
	public function __construct()
	{
		$this->name = 'xprtheadernavigation';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		parent::__construct();
		$this->displayName = $this->l('Great Store header Navigation Block Module.');
		$this->description = $this->l('Great Store header Navigation Block Module by Xpert-Idea.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->registerHook('displayTopLeft')
			|| !$this->registerHook('header')
			)
		return false;
			return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall())
		return false;
			return true;
	}
	public function hookTop($params)
	{
		return $this->display(__FILE__, '/views/templates/front/xprtheadernavigation-header.tpl');
	}
	public function hookDisplayNav($params)
	{
		return $this->display(__FILE__, '/views/templates/front/xprtheadernavigation.tpl');
	}
	public function hookdisplayTopLeft($params)
	{
		return $this->hookTop($params);
	}
	public function hookdisplayTopRightOne($params)
	{
		return $this->hookTop($params);
	}
	public function hookdisplayTopRightTwo($params)
	{
		return $this->hookTop($params);
	}
	public function hookLeftColumn($params)
	{
		return $this->display(__FILE__, '/views/templates/front/xprtheadernavigation.tpl');
	}
	public function hookRightColumn($params)
	{
		return $this->hookLeftColumn($params);
	}
	public function hookFooter($params)
	{
		return $this->display(__FILE__, '/views/templates/front/xprtheadernavigation-footer.tpl');
	}
	public function hookHeader($params)
	{
		$this->context->controller->addCSS(($this->_path).'css/xprtheadernavigation.css', 'all');
		$this->context->controller->addJS(($this->_path).'js/xprtheadernavigation.js', 'all');
	}
}