<?php

if (!defined('_PS_VERSION_'))
	exit;
class xprthomeblockcategory extends Module
{
	public function __construct()
	{
		$this->name = 'xprthomeblockcategory';
		$this->tab = 'front_office_features';
		$this->version = '2.0';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Home Block Category');
		$this->description = $this->l('Great Store Theme Home Block Category');
	}
	public function install()
	{
		if (!parent::install()
			|| !$this->registerHook('displayHomeMiddle')
		)
			return false;
		Configuration::updateValue('xprtCATEGORY_NBR',20);
		Configuration::updateValue('xprtSUBCATEGORY_NBR',20);
		Configuration::updateValue('xprtsection_margin', '0px 0px 30px 0px');
		Configuration::updateValue('xprtnumber_col', 3);
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue('xprthomecat_title_'.$l['id_lang'],'Popular categories');
			Configuration::updateValue('xprthomecat_subtitle_'.$l['id_lang'],'Shop by most popular categories');
		}
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall())
			return false;
		return true;
	}
	public function getContent()
	{
		$output = '';
		$errors = array();
		if(Tools::isSubmit('submitxprthomeblockcategory'))
		{
				Configuration::updateValue('xprtCATEGORY_NBR', (int)Tools::getvalue('xprtCATEGORY_NBR'));
				Configuration::updateValue('xprtSUBCATEGORY_NBR', (int)Tools::getvalue('xprtSUBCATEGORY_NBR'));
				Configuration::updateValue('xprtnumber_col', (int)Tools::getvalue('xprtnumber_col'));
				Configuration::updateValue('xprtsection_margin', Tools::getvalue('xprtsection_margin'));

				$langs = Language::getLanguages();
				foreach($langs as $l)
				{
					Configuration::updateValue('xprthomecat_title_'.$l['id_lang'],Tools::getvalue('xprthomecat_title_'.$l['id_lang']));
					Configuration::updateValue('xprthomecat_subtitle_'.$l['id_lang'],Tools::getvalue('xprthomecat_title_'.$l['id_lang']));
				}
				$output = $this->displayConfirmation($this->l('Your settings have been updated.'));
		}
		return $output.$this->renderForm();
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Title'),
						'name' => 'xprthomecat_title',
						'lang' => true,
						'desc' => $this->l('Enter the title.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Subtitle'),
						'name' => 'xprthomecat_subtitle',
						'lang' => true,
						'desc' => $this->l('Enter Subtitle'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Section margin'),
						'name' => 'xprtsection_margin',
						'class' => 'fixed-width-lg',
						'desc' => $this->l('Enter section margin: Exaple: (0px 0px 30px 0px) top right bottom left'),
					),
					array(
						'type' => 'select',
						'label' => $this->l('Number of column to show'),
						'name' => 'xprtnumber_col',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Enter number of column to show'),
							'options' => array(
								'id' => 'id',
							    'name' => 'name',
							    'query' => array(
							        array(
							            'id'=>'12',
							            'name'=>'one',
							        ),
							        array(
							            'id'=>'6',
							            'name'=>'two',
							        ),
							        array(
							            'id'=>'4',
							            'name'=>'three',
							        ),
							        array(
							            'id'=>'3',
							            'name'=>'four',
							        ),
							    ),
							),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Number of Parent Category to be displayed'),
						'name' => 'xprtCATEGORY_NBR',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Number of Parent Category to be displayed.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Number of Sub Category to be displayed'),
						'name' => 'xprtSUBCATEGORY_NBR',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Number of Sub Category to be displayed.'),
					),
					
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		$langs = Language::getLanguages();
		$xprthomecat_title = array();
		$xprthomecat_subtitle = array();
		foreach($langs as $l)
		{
			$xprthomecat_title[$l['id_lang']] = Tools::getValue('xprthomecat_title_'.$l['id_lang'], Configuration::get('xprthomecat_title_'.$l['id_lang']));
			$xprthomecat_subtitle[$l['id_lang']] = Tools::getValue('xprthomecat_subtitle_'.$l['id_lang'], Configuration::get('xprthomecat_subtitle_'.$l['id_lang']));
		}
		return array(
			'xprtCATEGORY_NBR' => Tools::getValue('xprtCATEGORY_NBR', (int)Configuration::get('xprtCATEGORY_NBR')),
			'xprtSUBCATEGORY_NBR' => Tools::getValue('xprtSUBCATEGORY_NBR', (int)Configuration::get('xprtSUBCATEGORY_NBR')),
			'xprtnumber_col' => Tools::getValue('xprtnumber_col', (int)Configuration::get('xprtnumber_col')),
			'xprtsection_margin' => Tools::getValue('xprtsection_margin', Configuration::get('xprtsection_margin')),
			'xprthomecat_title' => $xprthomecat_title,
			'xprthomecat_subtitle' => $xprthomecat_subtitle,
		);
	}
	public static function NumberOfProduct($id = NULL)
	{
		if($id==NULL)
			return false;
		$sql = 'SELECT COUNT(id_category) FROM `'._DB_PREFIX_.'category_product` WHERE `id_category` = '.$id;
		$results = Db::getInstance()->getValue($sql);
		return $results;
	}
	public static function GetCategory()
	{
		$results = array();
		$context = Context::getContext();
		$PS_HOME_CATEGORY = Configuration::get('PS_HOME_CATEGORY');
		$xprtCATEGORY_NBR = (int)Configuration::get('xprtCATEGORY_NBR');
		$limit = 'LIMIT '.$xprtCATEGORY_NBR;
		$extraimage_uri = _THEME_IMG_DIR_."no-image.jpg";
		$categories = Category::getNestedCategories($PS_HOME_CATEGORY,(int)$context->language->id,true,null,false,'',' ORDER BY c.`id_category` ASC ',$limit);
		if(isset($categories[$PS_HOME_CATEGORY]['children']) && !empty($categories[$PS_HOME_CATEGORY]['children'])){
			$results = $categories[$PS_HOME_CATEGORY]['children'];
			foreach($results as &$catego){
				$catego['productcount'] =  self::NumberOfProduct($catego['id_category']);
				$catextraextrathumbimg = Configuration::get("xprtcatextra".$catego['id_category']."extrathumb");
				if(isset($catextraextrathumbimg) && !empty($catextraextrathumbimg) && Module::isInstalled('xprtcategoryextraimg')){
					$extraimage_uri = _MODULE_DIR_."xprtcategoryextraimg/images/{$catextraextrathumbimg}";
				}
				$catego['catgegor_image'] =  $extraimage_uri;
				$catego['id_image'] =  ($catego['id_category'] && file_exists(_PS_CAT_IMG_DIR_.(int)$catego['id_category'].'.jpg')) ? (int)$catego['id_category'] : $context->language->iso_code.'-default';
			}
		}
		return $results;
	}
	public function HookExecute()
	{
		$id_lang = (int)$this->context->language->id;
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		$xprthomecat_title = Configuration::get('xprthomecat_title_'.$id_lang);
		$xprthomecat_subtitle = Configuration::get('xprthomecat_subtitle_'.$id_lang);
		if(empty($xprthomecat_title)){
			$xprthomecat_title = Configuration::get('xprthomecat_title_'.$default_lang);
		}
		if(empty($xprthomecat_title)){
			$xprthomecat_title = Configuration::get('xprthomecat_title');
		}
		if(empty($xprthomecat_subtitle)){
			$xprthomecat_subtitle = Configuration::get('xprthomecat_subtitle_'.$default_lang);
		}
		if(empty($xprthomecat_subtitle)){
			$xprthomecat_subtitle = Configuration::get('xprthomecat_subtitle');
		}
		$this->smarty->assign(array(
				'categories' => self::GetCategory(),
				'xprtSUBCATEGORY_NBR' => (int)Configuration::get('xprtSUBCATEGORY_NBR'),
				'xprtnumber_col' => (int)Configuration::get('xprtnumber_col'),
				'xprtsection_margin' => Configuration::get('xprtsection_margin'),
				'xprthomecat_title' => $xprthomecat_title,
				'xprthomecat_subtitle' => $xprthomecat_subtitle,
			));

		return $this->display(__FILE__,'views/templates/front/xprthomeblockcategory.tpl');	
	}
	public function hookDisplayHome($params)
	{
		return $this->HookExecute();
	}
	public function hookdisplayHomeMiddle($params)
	{
		return $this->HookExecute();
	}
}
