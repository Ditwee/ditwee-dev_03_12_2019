<div class="kr_home_video_block_area xprt_parallax_section" style="background-image:url({$xprthvb_image}); height:{$xprthvb_height}">
	<div class="kr_home_video_block container" >
		<div class="kr_home_video_block_content">
			<div class="kr_home_video_block_top">
				<a id="kr_video_popup" class="fancybox.iframe" href="{$xprthvb_videourl}"><i class="icon-play"></i></a>
			</div>
			<div class="kr_home_video_block_bottom">
				{if isset($xprthvb_title) && !empty($xprthvb_title)}
					<h2>{$xprthvb_title}</h2>
				{/if}
				{if isset($xprthvb_content) && !empty($xprthvb_content)}
					<p>{$xprthvb_content}</p>
				{/if}
				{if isset($xprthvb_linklavel) && !empty($xprthvb_linklavel)}
					<a class="btn btn-default btn-white" href="{$xprthvb_linkurl}">{$xprthvb_linklavel}</a>
				{/if}
			</div>
		</div>
	</div>
</div>