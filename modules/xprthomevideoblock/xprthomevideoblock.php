<?php

if (!defined('_CAN_LOAD_FILES_')) {
	exit;
}

class xprthomevideoblock extends Module {
	public $css_array = array("xprthomevideoblock.css", "xprthomevideoblock.css");
	public $js_array = array("xprthomevideoblock.js", "xprthomevideoblock.js");
	public function __construct() {
		$this->name = 'xprthomevideoblock';
		$this->tab = 'front_office_features';
		$this->version = '2.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Home Video Block');
		$this->description = $this->l('Prestashop Most Powerfull Great Store Theme Home Video Block Module by Xpert-Idea.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	// For installation service
	public function install() {
		if (!parent::install()
			|| !$this->registerHook('displayheader')
			|| !$this->registerHook('displayhomefullwidthmiddle')
			)
			return false;
		$langs = Language::getLanguages();
			foreach($langs as $l)
			{
				Configuration::updateValue('xprthvb_title_'.$l['id_lang'],"Great Store");
				Configuration::updateValue('xprthvb_content_'.$l['id_lang'],"Great Store theme is a modern, clean and professional Prestashop theme, it comes with a lot of useful features. Great Store theme is fully responsive, it looks stunning on all types of screens and devices.");
				Configuration::updateValue('xprthvb_linklavel_'.$l['id_lang'],"See more");
			}
		Configuration::updateValue('xprthvb_linkurl',"#");
		Configuration::updateValue('xprthvb_height',"350px");
		Configuration::updateValue('xprthvb_videourl',"https://player.vimeo.com/video/25475500");
		Configuration::updateValue('xprthvb_image',"sample.jpg");
			return true;
	}
	// For uninstallation service
	public function uninstall() {
		if (!parent::uninstall()
			)
			return false;
		else
			return true;
	}
	// Helper Form for Html markup generate
	public function SettingForm() {

		$default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Setting'),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button',
			),
		);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'text',
				'lang' => true,
				'label' => $this->l('Title'),
				'name' => 'xprthvb_title',
			);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'textarea',
				'label' => $this->l('Content'),
				'lang' => true,
				'name' => 'xprthvb_content',
			);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Link Lavel'),
				'lang' => true,
				'name' => 'xprthvb_linklavel',
			);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Link Url'),
				'name' => 'xprthvb_linkurl',
			);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Video Url'),
				'name' => 'xprthvb_videourl',
				'desc' => 'Youtube or Vimeo Iframe Url Put Here.',
			);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'text',
				'label' => $this->l('Section Height'),
				'name' => 'xprthvb_height',
				'desc' => 'Section height Ex: (300px)',
			);
			$this->fields_form[0]['form']['input'][] = array(
				'type' => 'file',
				'label' => $this->l('Video Frame Image'),
				'name' => 'image',
				'display_image' => false,
			);
		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
		foreach (Language::getLanguages(false) as $lang) {
			$helper->languages[] = array(
				'id_lang' => $lang['id_lang'],
				'iso_code' => $lang['iso_code'],
				'name' => $lang['name'],
				'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0),
			);
		}
		$helper->toolbar_btn = array(
			'save' => array(
				'desc' => $this->l('Save'),
				'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name . 'token=' . Tools::getAdminTokenLite('AdminModules'),
			),
		);
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->title = $this->displayName;
		$helper->show_toolbar = true;
		$helper->toolbar_scroll = true;
		$helper->submit_action = 'save' . $this->name;
			$langs = Language::getLanguages();
			foreach($langs as $l)
			{
				$helper->fields_value['xprthvb_title'][$l['id_lang']] = Configuration::get('xprthvb_title_'.$l['id_lang']);
				$helper->fields_value['xprthvb_content'][$l['id_lang']] = Configuration::get('xprthvb_content_'.$l['id_lang']);
				$helper->fields_value['xprthvb_linklavel'][$l['id_lang']] = Configuration::get('xprthvb_linklavel_'.$l['id_lang']);
			}

		$helper->fields_value['xprthvb_linkurl'] = Configuration::get('xprthvb_linkurl');
		$helper->fields_value['xprthvb_videourl'] = Configuration::get('xprthvb_videourl');
		$helper->fields_value['xprthvb_height'] = Configuration::get('xprthvb_height');
		$helper->fields_value['image'] = Configuration::get('xprthvb_image');
		return $helper;
	}
	// All Functional Logic here.
	public function getContent() {
		$html = '';
		if (Tools::isSubmit('save' . $this->name)) {
			$langs = Language::getLanguages();
			foreach($langs as $l)
			{
				Configuration::updateValue('xprthvb_title_'.$l['id_lang'],Tools::getvalue('xprthvb_title_'.$l['id_lang']));
				Configuration::updateValue('xprthvb_content_'.$l['id_lang'],Tools::getvalue('xprthvb_content_'.$l['id_lang']));
				Configuration::updateValue('xprthvb_linklavel_'.$l['id_lang'],Tools::getvalue('xprthvb_linklavel_'.$l['id_lang']));
			}

			Configuration::updateValue('xprthvb_linkurl',Tools::getvalue('xprthvb_linkurl'));
			Configuration::updateValue('xprthvb_videourl',Tools::getvalue('xprthvb_videourl'));
			Configuration::updateValue('xprthvb_height',Tools::getvalue('xprthvb_height'));
			$image = $this->processImage('image');
			Configuration::updateValue('xprthvb_image',$image);
		}
		$helper = $this->SettingForm();
		$html .= $helper->generateForm($this->fields_form);
		return $html;
	}
	// Upload Image
	public function processImage($name) {
		$file_name = false;
		if (isset($_FILES[$name]) && isset($_FILES[$name]['tmp_name']) && !empty($_FILES[$name]['tmp_name'])) {
			$ext = substr($_FILES[$name]['name'], strrpos($_FILES[$name]['name'], '.') + 1);
			$id = time();
			$file_name = $id . '.' . $ext;
			$path = _PS_MODULE_DIR_ . 'xprthomevideoblock/img/' . $file_name;
			if (!move_uploaded_file($_FILES[$name]['tmp_name'], $path)) {
				return false;
			}else{
				return $file_name;
			}
		} 
		else {
			return $file_name;
		}

	}
	// Display Header Hook Execute Functions
	public function hookdisplayheader($params) {
		if(isset($this->css_array))
			foreach ($this->css_array as $css) {
				$this->context->controller->addCSS($this->_path.'css/'.$css);
			}
		if(isset($this->js_array))
			foreach ($this->js_array as $js) {
				$this->context->controller->addJS($this->_path.'js/'.$js);
			}
	}
	// Display Header Hook Execute Functions
	public function hookdisplayhome($params) {
		$id_lang = (int)$this->context->language->id;
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

		$xprthvb_title = Configuration::get('xprthvb_title_'.$id_lang);
		$xprthvb_content = Configuration::get('xprthvb_content_'.$id_lang);
		$xprthvb_linklavel = Configuration::get('xprthvb_linklavel_'.$id_lang);
		if(empty($xprthvb_title)){
			$xprthvb_title = Configuration::get('xprthvb_title_'.$default_lang);
		}
		if(empty($xprthvb_content)){
			$xprthvb_content = Configuration::get('xprthvb_content_'.$default_lang);
		}
		if(empty($xprthvb_linklavel)){
			$xprthvb_linklavel = Configuration::get('xprthvb_linklavel_'.$default_lang);
		}
		
		$xprthvb_linkurl = Configuration::get('xprthvb_linkurl');
		$xprthvb_videourl = Configuration::get('xprthvb_videourl');
		$xprthvb_height = Configuration::get('xprthvb_height');
		$xprthvb_image = Configuration::get('xprthvb_image');
		if(!file_exists(_PS_MODULE_DIR_.'xprthomevideoblock/img/'.$xprthvb_image))
			$xprthvb_image = null;
		else
			$xprthvb_image = _MODULE_DIR_.'xprthomevideoblock/img/'.$xprthvb_image;
		$this->smarty->assign(
			array(
				'xprthvb_title' => $xprthvb_title,
				'xprthvb_content' => $xprthvb_content,
				'xprthvb_linklavel' => $xprthvb_linklavel,
				'xprthvb_linkurl' => $xprthvb_linkurl,
				'xprthvb_videourl' => $xprthvb_videourl,
				'xprthvb_height' => $xprthvb_height,
				'xprthvb_image' => $xprthvb_image,
			)
		);
		return $this->display(__FILE__,'views/templates/front/xprthomevideoblock.tpl');	
	}
	// Display Header Hook Execute Functions
	public function hookdisplayhomefullwidthmiddle($params) {
		return $this->hookdisplayhome($params);
	}
	public function hookdisplayFooterProduct($params) {
		return $this->hookdisplayhome($params);
	}
}