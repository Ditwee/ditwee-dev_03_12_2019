<?php
$htmlblock_dummy_data = array(
array
(
    'notlang' => array
        (
            'count_prd' => '',
            'transition_period' => '',
            'product_scroll' => '',
            'product_rows' => '',
            'play_again' => 0,
            'navigation_dots' => 0,
            'navigation_arrow' => 0,
            'autoplay' => 0,
            'hook' => 'displayHomeTopLeft',
            'image' => '',
            'pages' => 'all_page',
            'layout' => '',
            'device' => '',
            'active' => 0,
            'content' => '{"html_sub_title":"Lookbook fashion collection","section_margin":"0px 0px 30px 0px"}',
            'position' => 0,
            'truck_identify' => 'demo_8',
        ),
    'lang' => array
        (
            'title' => 'Shop the collection',
            'sub_title' => '',
        ),
),
array
(
    'notlang' => array
        (
            'count_prd' => '',
            'transition_period' => '',
            'product_scroll' => '',
            'product_rows' => '',
            'play_again' => 0,
            'navigation_dots' => 0,
            'navigation_arrow' => 0,
            'autoplay' => 0,
            'hook' => 'displayProductRightSidebar',
            'image' => '',
            'pages' => 'product',
            'layout' => 'sidebar',
            'device' => '',
            'active' => 0,
            'content' => '{"html_sub_title":"This is a custom sub-title.","section_margin":"0px 0px 30px 0px"}',
            'position' => 1,
            'truck_identify' => 'demo_7',
        ),
    'lang' => array
        (
            'title' => 'Custom HTML Block',
            'sub_title' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non placerat mi. Etiam non tellus </p>',
        ),
),
array
(
    'notlang' => array
        (
            'count_prd' => '',
            'transition_period' => '',
            'product_scroll' => '',
            'product_rows' => '',
            'play_again' => 0,
            'navigation_dots' => 0,
            'navigation_arrow' => 0,
            'autoplay' => 0,
            'hook' => 'displayLeftColumn',
            'image' => '',
            'pages' => 'all_page',
            'layout' => 'sidebar',
            'device' => '',
            'active' => 0,
            'content' => '{"html_sub_title":"","section_margin":"0px 0px 30px 0px"}',
            'position' => 2,
            'truck_identify' => 'demo_8',
        ),
    'lang' => array
        (
            'title' => 'Custom HTML Block',
            'sub_title' => '<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled.</p>
            <p></p>
            <p><img src="http://localhost/prestashop_showcase/ps-greatstore/demo-eight/img/cms/Untitled-1.jpg" width="270" height="240" alt="Untitled-1.jpg" /></p>',
        ),
),
array
(
    'notlang' => array
        (
            'count_prd' => '',
            'transition_period' => '',
            'product_scroll' => '',
            'product_rows' => '',
            'play_again' => 0,
            'navigation_dots' => 0,
            'navigation_arrow' => 0,
            'autoplay' => 0,
            'hook' => 'displayBanner',
            'image' => '',
            'pages' => 'index',
            'layout' => 'banner',
            'device' => '',
            'active' => 0,
            'content' => '{"html_sub_title":"","section_margin":"10px 0px 0px 0px"}',
            'position' => 3,
            'truck_identify' => 'demo_9',
        ),
    'lang' => array
        (
            'title' => 'This is Banner Html',
            'sub_title' => '<p style="text-align:center;"><span style="color:#f1f1f1;">* FREE DELIVERY FROM <strong>$50</strong> AND EASY RETURNS !</span></p>',
        ),
),
array
(
    'notlang' => array
        (
            'count_prd' => '',
            'transition_period' => '',
            'product_scroll' => '',
            'product_rows' => '',
            'play_again' => 0,
            'navigation_dots' => 0,
            'navigation_arrow' => 0,
            'autoplay' => 0,
            'hook' => 'displayFooterProduct',
            'image' => '',
            'pages' => 'product',
            'layout' => 'product_desc',
            'device' => '',
            'active' => 0,
            'content' => '{"html_sub_title":"SED UT PERSPICIATIS UNDE OMNIS ISTE NATUS ERROR SIT VOLUPTATEM ACCUSANTIUM DOLOREMQUE LAUDANTI UM","section_margin":"0px 0px 30px 0px"}',
            'position' => 4,
            'truck_identify' => 'demo_8',
        ),
    'lang' => array
        (
            'title' => 'Additional Description',
            'sub_title' => '<p style="text-align:center;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
            <p style="text-align:center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>',
        ),
),
array
(
    'notlang' => array
        (
            'count_prd' => '',
            'transition_period' => '',
            'product_scroll' => '',
            'product_rows' => '',
            'play_again' => 0,
            'navigation_dots' => 0,
            'navigation_arrow' => 0,
            'autoplay' => 0,
            'hook' => 'displayFooter',
            'image' => '',
            'pages' => 'all_page',
            'layout' => 'footer',
            'device' => '',
            'active' => 0,
            'content' => '{"html_sub_title":"","section_margin":""}',
            'position' => 3,
            'truck_identify' => 'demo_9',
        ),
    'lang' => array
        (
            'title' => 'Custom Block',
            'sub_title' => '<p><img src="http://localhost/prestashop_showcase/ps-greatstore/demo-nine/img/cms/footer-cutom.jpg" alt="" width="270" height="83" /></p>
<p><span>This is custom html block. You can inset text, image or edit this from backend. It is a long established fact that a reader will be.</span></p>',
        ),
), //new item
array
(
    'notlang' => array
        (
            'count_prd' => '',
            'transition_period' => '',
            'product_scroll' => '',
            'product_rows' => '',
            'play_again' => 0,
            'navigation_dots' => 0,
            'navigation_arrow' => 0,
            'autoplay' => 0,
            'hook' => 'displayHomeTopLeft',
            'image' => '',
            'pages' => 'all_page',
            'layout' => 'sidebar',
            'device' => '',
            'active' => 0,
            'content' => '{"html_sub_title":"","section_margin":"0px 0px 30px 0px"}',
            'position' => 1,
            'truck_identify' => 'demo_12',
        ),
    'lang' => array
        (
            'title' => 'Custom block',
            'sub_title' => '<p><img src="http://xpert-idea.com/prestashop/great-store/demos/six/modules/xprtpromotionalblock/images/1466413716.jpg" class="img-responsive" alt="1466413716.jpg" /></p>
<p><span>Great Store theme is a modern, clean and professional Prestashop theme, it comes with a lot of useful features. Great Store theme is fully responsive, it looks stunning on all types of screens and devices.</span></p>',
        ),
),
);