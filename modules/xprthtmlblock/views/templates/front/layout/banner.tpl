{if isset($xprthtmlblock) && !empty($xprthtmlblock)}
	<div class="xprthtmlblock" {if isset($xprthtmlblock.section_margin) && $xprthtmlblock.section_margin}style="margin:{$xprthtmlblock.section_margin}"{/if}>
		{if isset($xprthtmlblock.sub_title) && $xprthtmlblock.sub_title}
	        <div class="xprt_html_block_content">
	        	{$xprthtmlblock.sub_title}
	        </div>
        {/if}
	</div>
{/if}