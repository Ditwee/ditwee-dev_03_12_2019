{if isset($xprthtmlblock) && !empty($xprthtmlblock)}
	<div class="xprthtmlblock sidebar_style block col-sm-3">
		{if isset($xprthtmlblock.title)}
			<h4>
				<em>{$xprthtmlblock.title}</em>
			</h4>
		{/if}	
		{if isset($xprthtmlblock.sub_title) && $xprthtmlblock.sub_title}
	        <div class="block_content">
	        	{$xprthtmlblock.sub_title}
	        </div>
        {/if}
	</div>
{/if}