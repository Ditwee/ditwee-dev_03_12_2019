{if isset($xprthtmlblock) && !empty($xprthtmlblock)}
	<div class="xprthtmlblock" {if isset($xprthtmlblock.section_margin) && $xprthtmlblock.section_margin}style="margin:{$xprthtmlblock.section_margin}"{/if}>
    	{if isset($xprthtmlblock.title) && $xprthtmlblock.title}
    		<div class="page_title_area {$xprt.home_title_style}">
    			{if isset($xprthtmlblock.title)}
    				<h3 class="page-heading">
    					<em>{$xprthtmlblock.title}</em>
    					<span class="heading_carousel_arrow"></span>
    				</h3>
    			{/if}
    			{if isset($xprthtmlblock.html_sub_title) && $xprthtmlblock.html_sub_title}
    				<p class="page_subtitle d_none">{$xprthtmlblock.html_sub_title}</p>
    			{/if}
    			<div class="heading-line d_none"><span></span></div>
    		</div>
        {/if}
		{if isset($xprthtmlblock.sub_title) && $xprthtmlblock.sub_title}
	        <div class="xprt_html_block_content">
	        	{$xprthtmlblock.sub_title}
	        </div>
        {/if}
	</div>
{/if}