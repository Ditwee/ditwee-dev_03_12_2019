{if isset($xpertresults) && !empty($xpertresults)}
<div id="xprtinstagram" class="xprtinstagram instragram_block {if $xprtinsta_style == 'slider'}carousel{/if}">
	<ul class="clearfix instagram_col_{$xprtinsta_nocolumn}">
		{foreach from=$xpertresults item=xpertresult}
			<li>
				<a class="fancy_instragram" data-fancybox-group="fancy_instragram" href="{$xpertresult.high}" data-title="<a target='_blank' href='{$xpertresult.link}'><b>{if isset($xpertresult.caption)}{$xpertresult.caption}{/if}</b>. {l s='Link to Instagram' mod='xprtinstagram'}</a>">
					<div class="instagram_image" style="background:url({$xpertresult.url}) no-repeat scroll center center/ cover;width:100%; height:{if !empty($xprtinsta_customheight)}{$xprtinsta_customheight}{else}{$xpertresult.width}{/if}px;"></div>
				</a>
			</li>
		{/foreach}
	</ul>
</div>
{else}
<h3>{l s='Please Configure Instagram API. ' mod='xprtinstagram'}</h3>
{/if}
{addJsDef xprtInstaNumColumn=$xprtinsta_nocolumn|intval}