{if isset($xpertresults)}
<div id="xprtinstagram" class="xprtinstagram instragram_block block col-sm-3 {if $xprtinsta_style == 'slider'}carousel{/if}">
	<h4>{l s='Follow us on Instagram' mod="xprtinstagram"}</h4>
	<ul class="block_content instagram_col_{$xprtinsta_nocolumn} clearfix">
		{foreach from=$xpertresults item=xpertresult}
			<li>
				<a class="fancy_instragram" data-fancybox-group="fancy_instragram" href="{$xpertresult.high}" data-title="<a target='_blank' href='{$xpertresult.link}'><b>{if isset($xpertresult.caption)}{$xpertresult.caption}{/if}</b>. {l s='Link to Instagram' mod='xprtinstagram'}</a>">
					<div class="instagram_image" style="background:url({$xpertresult.url}) no-repeat scroll center center/ cover;width:100%; height:{if !empty($xprtinsta_customheight)}{$xprtinsta_customheight}{else}{$xpertresult.width}{/if}px;"></div>
				</a>
			</li>
		{/foreach}
	</ul>
</div>
{else}
<p>{l s='Please Configure Instagram API. ' mod='xprtinstagram'}</p>
{/if}
{addJsDef xprtInstaNumColumn=$xprtinsta_nocolumn|intval}