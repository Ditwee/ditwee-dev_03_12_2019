<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_PS_VERSION_'))
	exit;
class xprtinstagram extends Module
{
	public function __construct()
	{
		$this->name = 'xprtinstagram';
		$this->tab = 'front_office_features';
		$this->version = '2.0';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Instagram Block');
		$this->description = $this->l('Great Store Theme Theme Instagram Block.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->registerHook('displayheader')
			|| !$this->registerHook('displayFooterTopFullwidth')
			)
		return false;
			Configuration::updateValue('xprtinsta_userid','');
			Configuration::updateValue('xprtinsta_token','');
			Configuration::updateValue('xprtinsta_count','');
			Configuration::updateValue('xprtinsta_style','');
			Configuration::updateValue('xprtinsta_imgsize','');
			Configuration::updateValue('xprtinsta_nocolumn','');
			Configuration::updateValue('xprtinsta_customheight','');
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall())
			return false;
		Configuration::deleteByName('xprtinsta_userid');
		Configuration::deleteByName('xprtinsta_token');
		Configuration::deleteByName('xprtinsta_count');
		Configuration::deleteByName('xprtinsta_style');
		Configuration::deleteByName('xprtinsta_imgsize');
		Configuration::deleteByName('xprtinsta_nocolumn');
		Configuration::deleteByName('xprtinsta_customheight');
			return true;
	}
	public function hookexecute(){
		$data = false;
		$results = array();
		$xprtinsta_userid = Configuration::get('xprtinsta_userid');
		$xprtinsta_token  = Configuration::get('xprtinsta_token');
		$xprtinsta_count  = Configuration::get('xprtinsta_count');
		$xprtinsta_style  = Configuration::get('xprtinsta_style');
		$xprtinsta_imgsize  = Configuration::get('xprtinsta_imgsize');
		$xprtinsta_nocolumn  = Configuration::get('xprtinsta_nocolumn');
		$xprtinsta_customheight  = Configuration::get('xprtinsta_customheight');
		if($xprtinsta_userid && $xprtinsta_token){
			$url = "https://api.instagram.com/v1/users/".$xprtinsta_userid."/media/recent/?access_token=".$xprtinsta_token."&count=".$xprtinsta_count;
			$data = @file_get_contents($url);
		}
		if($data){
			$data = Tools::jsonDecode($data);

		}
		if(isset($data->data)){
			$i = 0;
			foreach ($data->data as $datavalue) {
				
				$results[$i]['link'] = $datavalue->link;
				if (is_object($datavalue->caption)){
					$results[$i]['caption'] = $datavalue->caption->text;
				}
				$results[$i]['low'] = $datavalue->images->low_resolution->url;
				$results[$i]['high'] = $datavalue->images->standard_resolution->url;
				$results[$i]['thumb'] = $datavalue->images->thumbnail->url;
				$results[$i]['url'] = $datavalue->images->{$xprtinsta_imgsize}->url;
				$results[$i]['height'] = $datavalue->images->{$xprtinsta_imgsize}->height;
				$results[$i]['width'] = $datavalue->images->{$xprtinsta_imgsize}->width;
				$i++;
			}
		}
		$this->context->smarty->assign(array(
				'xpertresults' => $results,
				'xprtinsta_userid' => Configuration::get('xprtinsta_userid'),
				'xprtinsta_token' => Configuration::get('xprtinsta_token'),
				'xprtinsta_count' => Configuration::get('xprtinsta_count'),
				'xprtinsta_style' => Configuration::get('xprtinsta_style'),
				'xprtinsta_imgsize' => Configuration::get('xprtinsta_imgsize'),
				'xprtinsta_nocolumn' => Configuration::get('xprtinsta_nocolumn'),
				'xprtinsta_customheight' => Configuration::get('xprtinsta_customheight'),
			));
	}
	public function hookdisplayHeader($params)
	{
		$this->context->controller->addCSS($this->_path.'css/xprtinstagram.css');
		$this->context->controller->addJS($this->_path.'js/xprtinstagram.js');
	}
	public function hookDisplayFooter($params)
	{
		$this->hookexecute();
		return $this->display(__FILE__,'views/templates/front/xprtinstagramfooter.tpl');
	}
	public function hookDisplayFooterTop($params)
	{
		$this->hookexecute();
		return $this->display(__FILE__,'views/templates/front/xprtinstagram.tpl');
	}
	public function hookDisplayFooterBottom($params)
	{
		$this->hookexecute();
		return $this->display(__FILE__,'xprtinstagram.tpl');
	}
	public function hookdisplayhomefullwidthmiddle($params)
	{
		$this->hookexecute();
		return $this->display(__FILE__,'xprtinstagram.tpl');
	}
	public function hookdisplayHomeFullWidthBottom($params)
	{
		$this->hookexecute();
		return $this->display(__FILE__,'xprtinstagram.tpl');
	}
	public function hookdisplayFooterTopFullwidth($params)
	{
		$this->hookexecute();
		return $this->display(__FILE__,'xprtinstagram.tpl');
	}

	
	public function postProcess()
	{
		if (Tools::isSubmit('submit'.$this->name))
		{
			Configuration::updateValue('xprtinsta_userid',Tools::getValue('xprtinsta_userid'));
			Configuration::updateValue('xprtinsta_token',Tools::getValue('xprtinsta_token'));
			Configuration::updateValue('xprtinsta_count',Tools::getValue('xprtinsta_count'));
			Configuration::updateValue('xprtinsta_style',Tools::getValue('xprtinsta_style'));
			Configuration::updateValue('xprtinsta_nocolumn',Tools::getValue('xprtinsta_nocolumn'));
			Configuration::updateValue('xprtinsta_customheight',Tools::getValue('xprtinsta_customheight'));
			Configuration::updateValue('xprtinsta_imgsize',Tools::getValue('xprtinsta_imgsize'));
			return $this->displayConfirmation($this->l('The settings have been updated.'));
		}
		return '';
	}
	public function getContent()
	{
		return $this->postProcess().$this->renderForm();
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'tinymce' => true,
				'legend' => array(
					'title' => $this->l('Instagram Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('User ID'),
						'name' => 'xprtinsta_userid',
						'desc' => $this->l('Please Enter Your Instagram User ID. Get User Id : https://smashballoon.com/instagram-feed/token/'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Access Token'),
						'name' => 'xprtinsta_token',
						'desc' => $this->l('Please Enter Your Instagram Access Token. Get Access Token : https://smashballoon.com/instagram-feed/token/'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Post Count'),
						'name' => 'xprtinsta_count',
						'desc' => $this->l('Please Enter How Many Post You Want To Display.'),
					),
					array(
	                    'type' => 'select',
	                    'label' => $this->l('Display noumber of column per row'),
	                    'name' => 'xprtinsta_nocolumn',
	                    'options' => array(
                        		'query' => array(
									array('id' => '3', 'name' => '3'),
									array('id' => '4', 'name' => '4'),
									array('id' => '5', 'name' => '5'),
									array('id' => '6', 'name' => '6'),
									array('id' => '7', 'name' => '7'),
									array('id' => '8', 'name' => '8'),
									array('id' => '9', 'name' => '9'),
									array('id' => '10', 'name' => '10'),
							    ),
	                        'id' => 'id',
	                        'name' => 'name',
	                    )
                	),
					array(
	                    'type' => 'text',
	                    'label' => $this->l('Custom Height'),
	                    'name' => 'xprtinsta_customheight',
						'desc' => $this->l('You can enter custom height for image if you want. optional'),
                	),
					array(
	                    'type' => 'select',
	                    'label' => $this->l('Display Style'),
	                    'name' => 'xprtinsta_style',
	                    'options' => array(
	                        		'query' => array(
										array('id' => 'general', 'name' => 'General'),
										array('id' => 'slider', 'name' => 'Slider'),
								    ),
	                        'id' => 'id',
	                        'name' => 'name',
	                    )
                	),
                	array(
	                    'type' => 'select',
	                    'label' => $this->l('Image Size'),
	                    'name' => 'xprtinsta_imgsize',
	                    'options' => array(
                    		'query' => array(
								array('id' => 'low_resolution', 'name' => 'Low Resolution'),
								array('id' => 'thumbnail', 'name' => 'Thumbnail'),
								array('id' => 'standard_resolution', 'name' => 'Standard Resolution'),
						    ),
	                        'id' => 'id',
	                        'name' => 'name',
	                    )
                	),
				),
				'submit' => array(
					'title' => $this->l('Save')
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->module = $this;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'uri' => $this->getPathUri(),
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		$fields = array();
		$fields['xprtinsta_userid'] = Tools::getValue('xprtinsta_userid', Configuration::get('xprtinsta_userid'));
		$fields['xprtinsta_token'] = Tools::getValue('xprtinsta_token', Configuration::get('xprtinsta_token'));
		$fields['xprtinsta_count'] = Tools::getValue('xprtinsta_count', Configuration::get('xprtinsta_count'));
		$fields['xprtinsta_style'] = Tools::getValue('xprtinsta_style', Configuration::get('xprtinsta_style'));
		$fields['xprtinsta_nocolumn'] = Tools::getValue('xprtinsta_nocolumn', Configuration::get('xprtinsta_nocolumn'));
		$fields['xprtinsta_customheight'] = Tools::getValue('xprtinsta_customheight', Configuration::get('xprtinsta_customheight'));
		$fields['xprtinsta_imgsize'] = Tools::getValue('xprtinsta_imgsize', Configuration::get('xprtinsta_imgsize'));
		return $fields;
	}
}