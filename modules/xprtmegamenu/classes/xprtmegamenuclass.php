<?php
class xprtmegamenuclass extends ObjectModel
{
	public $id;
	public $id_xprtmegamenu;
	public $menu_name;
	public $is_horizontal;
	public $is_sticky;
	public $hook;
	public $custom_class;
	public $pages;
	public $custom_css;
	public $gcontent;
	public $truck_identify;
	public $id_shop;
	public $active;
	public $position;
	private static $module_name = 'xprtmegamenu';
	private static $tablename = 'xprtmegamenu';
	private static $classname = 'xprtmegamenuclass';
	private static $tablenameitem = 'xprtmegamenuitem';
	private static $classnameitem = 'xprtmegamenuitemclass';
	public static $definition = array(
		'table' => 'xprtmegamenu',
		'primary' => 'id_xprtmegamenu',
		'multilang' => false,
		'fields' => array(
			'menu_name' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'is_horizontal' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'is_sticky' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'custom_css' =>			array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'custom_class' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'truck_identify' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'gcontent' =>		array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'hook' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'pages' =>			array('type' => self::TYPE_STRING,'validate' => 'isCleanHtml'),
			'id_shop' =>			array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'position' =>			array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'active' =>			array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
    	if(isset($this->truck_identify) && !empty($this->truck_identify)){
			$this->truck_identify = $this->truck_identify;
        }else{
        	$this->truck_identify = "";
        }
        $this->id_shop = (int)Context::getContext()->shop->id;
        if ($this->position <= 0)
            $this->position = self::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.self::$tablename.'`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetMenuBlock($hook = NULL)
    {
        if($hook == NULL)
            return false;
        $results = array();
        $module_name = self::$module_name;
        $allproducts = self::GetAllBlockMenuS($hook);
        if(isset($allproducts) && !empty($allproducts))
        {
            $i = 0;
            foreach ($allproducts as $allprds)
            {
            	if(empty($allprds['pages']))
            	{
					$allprds['pages'] = 'all_page';
				}
                if($module_name::PageException($allprds['pages']))
                {
                	$allprds['imageurl'] =  _MODULE_DIR_.$module_name.'/images/';
                    $allprds['pages'] =  isset($allprds['pages'])?$allprds['pages']:"";
                    $allprds['hook'] =  isset($allprds['hook'])?$allprds['hook']:"";
                    $allprds['active'] = $allprds['active'];
                    $allprds['position'] = $allprds['position'];
                    $allprds['menuitems'] = self::GetAllBlockMenuItemS($allprds['id_'.self::$tablename]);
                    $results[$i] = $allprds;
                $i++;
                }
            }
        }
        return $results;
    }
    public static function GetAllBlockMenuS($hook = NULL)
    {
		$results = array();
	    $id_lang = (int)Context::getContext()->language->id;
	    $id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT * FROM `'._DB_PREFIX_.self::$tablename.'` pb ';
	    $sql .= ' WHERE pb.`active` = 1 and pb.`id_shop` = '.$id_shop;
	    if($hook != NULL)
	        $sql .= ' AND pb.`hook` = "'.$hook.'" ';
	    $sql .= ' ORDER BY pb.`position` ASC';
	    $results = Db::getInstance()->executeS($sql);
	    if(isset($results) && !empty($results) && is_array($results))
	    {
	    	foreach ($results as &$result)
	    	{
	    		if(isset($result['gcontent']) && !empty($result['gcontent']))
	    		{
	    			$content = Tools::jsonDecode($result['gcontent']);
	    			if(isset($content) && !empty($content))
	    			{
	    				foreach ($content as $content_key => $content_value)
	    				{
	    					if(isset($content_value) && !is_string($content_value) && is_object($content_value))
	    					{
	    						foreach ($content_value as $content_value_key => $content_value_value)
	    						{
	    							$result[$content_key][$content_value_key] = $content_value_value;
	    						}
	    					}else{
	    						$result[$content_key] = $content_value;
	    					}
	    				}
	    			}
	    		}
	    		$result['gcontent'] = '';
	    	}
	    }
	    return $results;
    }
    public static function GetAllBlockMenuItemS($id_parent = null)
    {
    	if($id_parent == null)
    		return false;
		$results = array();
	    $id_lang = (int)Context::getContext()->language->id;
	    $id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT * FROM `'._DB_PREFIX_.self::$tablenameitem.'` pb INNER JOIN `'._DB_PREFIX_.self::$tablenameitem.'_lang` pbl ON (pb.`id_'.self::$tablenameitem.'` = pbl.`id_'.self::$tablenameitem.'` AND pbl.`id_lang` = '.$id_lang.') ';
	    $sql .= ' WHERE pb.`active` = 1 and pb.`id_'.self::$tablename.'` = '.(int)$id_parent;
	    $sql .= ' ORDER BY pb.`position` ASC';
	    $results = Db::getInstance()->executeS($sql);
	    if(isset($results) && !empty($results) && is_array($results))
	    {
	    	foreach ($results as &$result)
	    	{
	    		if(isset($result['gcontent']) && !empty($result['gcontent']))
	    		{
	    			$content = Tools::jsonDecode($result['gcontent']);
	    			if(isset($content) && !empty($content))
	    			{
	    				foreach ($content as $content_key => $content_value)
	    				{
	    					if(isset($content_value) && !is_string($content_value) && is_object($content_value))
	    					{
	    						foreach ($content_value as $content_value_key => $content_value_value)
	    						{
	    							$result[$content_key][$content_value_key] = $content_value_value;
	    						}
	    					}else{
	    						$result[$content_key] = $content_value;
	    					}
	    				}
	    			}
	    			$result['gcontent'] = '';
	    		}
	    	}
	    }
	    return $results;
    }
}