<?php
class xprtmegamenuitemclass extends ObjectModel
{
	public $id;
	public $id_xprtmegamenuitem;
	public $icon;
	public $icon_pos;
	public $link_type;
	public $link_value;
	public $is_submenu;
	public $is_megamenu;
	public $is_fullwidth;
	public $row_ids;
	public $custom_class;
	public $gcontent;
	public $active;
	public $position;
	public $id_xprtmegamenu;
	public $name;
	public $badge;
	public $link_value_lang;
	public $submenu_level;
	public $submenu_value;
	private static $module_name = 'xprtmegamenu';
	private static $tablename = 'xprtmegamenuitem';
	private static $classname = 'xprtmegamenuitemclass';
	public static $definition = array(
		'table' => 'xprtmegamenuitem',
		'primary' => 'id_xprtmegamenuitem',
		'multilang' => true,
		'fields' => array(
			'name' =>	array('type' => self::TYPE_STRING,'validate' => 'isString','lang' => true),
			'badge' =>	array('type' => self::TYPE_STRING,'validate' => 'isString','lang' => true),
			'link_value_lang' =>	array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml','lang' => true),
			'submenu_level' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'submenu_value' =>	array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'icon' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'icon_pos' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'link_type' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'link_value' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'is_submenu' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'is_megamenu' =>array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'is_fullwidth' =>array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'row_ids' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'custom_class' =>array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'gcontent' =>	array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'position' =>	array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'id_xprtmegamenu' =>array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'active' =>			array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = self::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.self::$tablename.'`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
}