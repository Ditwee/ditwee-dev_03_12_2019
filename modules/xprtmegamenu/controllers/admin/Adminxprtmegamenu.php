<?php
class AdminxprtmegamenuController extends ModuleAdminController
{
	private static $module_name = 'xprtmegamenu';
    public function ajaxProcessuploadmenuimage()
	{
		$results = array();
		if(isset($_FILES['imagedata']) && !empty($_FILES['imagedata'])){
			$results['imagedata'] = $this->processImage('imagedata');
			$results['imagelink'] = _MODULE_DIR_.self::$module_name.'/images/';
		}else{
			$results['imagedata'] = '';
			$results['imagelink'] = _MODULE_DIR_.self::$module_name.'/images/';
		}
		die(Tools::jsonEncode($results));
	}
	public function processImage($file_name = null){
		if($file_name == null)
			return false;
	    if(isset($_FILES[$file_name]) && isset($_FILES[$file_name]['tmp_name']) && !empty($_FILES[$file_name]['tmp_name'])){
            $ext = substr($_FILES[$file_name]['name'], strrpos($_FILES[$file_name]['name'], '.') + 1);
            $basename_file_name = basename($_FILES[$file_name]["name"]);
            $strlen = strlen($basename_file_name);
            $strlen_ext = strlen($ext);
            $basename_file_name = substr($basename_file_name,0,($strlen-$strlen_ext));
            $link_rewrite_file_name = Tools::link_rewrite($basename_file_name);
            $file_orgname = $link_rewrite_file_name.'.'.$ext;
            $path = _PS_MODULE_DIR_.self::$module_name.'/images/' . $file_orgname;
            if(!move_uploaded_file($_FILES[$file_name]['tmp_name'],$path))
                return false;         
            else
                return $file_orgname;   
	    }else{
	    	return false;
	    }
	}
}