/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

var responsiveflagMenu = false;
var categoryMenu = $('ul.sf-menu');
var mCategoryGrover = $('.sf-contener .cat-title');

$(document).ready(function(){
	categoryMenu = $('ul.sf-menu');
	mCategoryGrover = $('.sf-contener .cat-title');
	//responsiveMenu();
	//menuFullWidth();
	$(window).load(responsiveMenu);
	$(window).resize(responsiveMenu);
	// $(window).resize(menuChange);
});

// check resolution
function responsiveMenu()
{
   if ($(document.body).width() <= 991 && responsiveflagMenu == false)
	{
		menuChange('enable');
		responsiveflagMenu = true;
	}
	else if ($(document.body).width() >= 992)
	{
		menuChange('disable');
		responsiveflagMenu = false;
	};
	
}


function mobileInit()
{
	if(typeof xprtmegamenu_label != 'undefined'){
		var xprtmegamenulabel = xprtmegamenu_label;
	}else{
		var xprtmegamenulabel = 'Menu';
	}
	categoryMenu.superfish('destroy');
	$('ul.sf-menu').removeAttr('style');


	$('#header .xprt_mega_menu_area ul.sf-menu').slicknav({
		'appendTo': '#header .main_menu_area',
		'closedSymbol': '&#9655;', // Character after collapsed parents.
		'label': xprtmegamenulabel, // Character after collapsed parents.
		'openedSymbol': '&#9661;', // Character after expanded parents.
	});




	// mCategoryGrover.on('click', function(e){
	// 	$(this).toggleClass('active').parent().find('ul.menu-content').stop().slideToggle('medium');
	// 	return false;
	// });

	// $('.sf-menu > li ul').addClass('menu-mobile clearfix').parent().prepend('<span class="menu-mobile-grover"></span>');

	// //$('.sf-menu .menu-mobile > li').children('ul').parent().addClass('has_child').prepend('<span class="menu-mobile-grover"></span>');

	// $(".sf-menu .menu-mobile-grover").on('click', function(e){
	// 	var catSubUl = $(this).next().next('.menu-mobile');
	// 	if (catSubUl.is(':hidden'))
	// 	{
	// 		catSubUl.slideDown();
	// 		$(this).addClass('active');
	// 	}
	// 	else
	// 	{
	// 		catSubUl.slideUp();
	// 		$(this).removeClass('active');
	// 	}
	// 	return false;
	// });


	// $('#header .xprt_mega_menu > ul.sf-menu > li > a').on('click', function(e){
	// 	console.log(this);
	// 	var parentOffset = $(this).prev().offset();
	//    	var relX = parentOffset.left - e.pageX;
	// 	if ($(this).parent('li').find('ul').length && relX >= 0 && relX <= 20)
	// 	{
	// 		e.preventDefault();
	// 		var mobCatSubUl = $(this).next('.menu-mobile');
	// 		var mobMenuGrover = $(this).prev();
	// 		if (mobCatSubUl.is(':hidden'))
	// 		{
	// 			mobCatSubUl.slideDown();
	// 			mobMenuGrover.addClass('active');
	// 		}
	// 		else
	// 		{
	// 			mobCatSubUl.slideUp();
	// 			mobMenuGrover.removeClass('active');
	// 		}
	// 	}
	// });

}


// init Super Fish Menu for 767px+ resolution
function desktopInit()
{
	mCategoryGrover.off();
	mCategoryGrover.removeClass('active');
	$('.sf-menu > li > ul').removeClass('menu-mobile').parent().find('.menu-mobile-grover').remove();
	$('.sf-menu').removeAttr('style');
	$('.sf-menu > li > ul.submenu-regular').find('ul').addClass('submenu-regular'); // for regular menu to addclass

	categoryMenu.superfish({
		onBeforeShow: function(){
			var el = this;
			if( $(this).is('.sf_mega_menu_fixedwidth') ){
				megaMenuFixedWidth(el);
			}
			else if( $(this).is('.sf_mega_menu_fullwidth') ){
				menuContainerWidth(el);
			}
			else{
				// console.log(el);
				$(el).parent('li').addClass('sf_regular_with_ul');
			}
		}
	});

	//add class for width define
	// $('.sf-menu > li > ul').addClass('submenu-container clearfix');
	//var childArrow = $('.sf-menu > li > a').siblings('ul.submenu-container');
	$('ul.sf-menu > li > ul.submenu-container, ul.sf-menu > li > ul.submenu-regular').siblings('a').addClass('sf-with-ul');
	


	// if(typeof childArrow != 'undefined'){
	// }

	 // loop through each sublist under each top list item
    // $('.sf-menu > li > ul').each(function(){
    //     i = 0;
    //     //add classes for clearing
    //     $(this).each(function(){
    //         if ($(this).attr('class') != "category-thumbnail"){
    //             i++;
    //             if(i % 2 == 1)
    //                 $(this).addClass('first-in-line-xs');
    //             else if (i % 5 == 1)
    //                 $(this).addClass('first-in-line-lg');
    //         }
    //     });
    // });


    // if( $('#header .sf_mega_menu.submenu-container').is('.sf_mega_menu_fullwidth') ){
    // 	menuContainerWidth();
    // }
    // else{
    // 	megaMenuFixedWidth();
    // }



    
    //$(window).resize(menuFullWidth);
}


function megaMenuFixedWidth(el){
	
	var submenuContainer = $(el);

	var windowWidth = $(window).width();

	var posContainerMenu = $('#header .xprt_mega_menu').offset();

	var extraPaddingAdjust = 0;

 	var submenuFixedWidth = parseInt($(el).attr('data-megawidth'));

 	var megaMenuExtraWidth = Math.ceil(parseInt(posContainerMenu.left + submenuFixedWidth));

 	if(submenuFixedWidth < windowWidth){
 		if( megaMenuExtraWidth < windowWidth){
 			submenuContainer.css({'width': submenuFixedWidth +'px'});
 		}else{
 			extraPaddingAdjust = parseInt(megaMenuExtraWidth - windowWidth);
 			submenuContainer.css({'width': (submenuFixedWidth - 5) +'px', 'left': -extraPaddingAdjust + 'px'});
 		}
 	}
 	else{
 		submenuContainer.css({'width': (windowWidth - 10) +'px', 'left': -(posContainerMenu.left - 5) + 'px'});
 	}


} //megaMenuFixedWidth




function menuContainerWidth(el){

	var submenuContainer = $(el);

	var containerWidth = $('#header .container:visible').width();

	var posContainer = $('#header .container:visible').offset();

	var posContainerMenu = $('#header .xprt_mega_menu').offset();

	if(typeof posContainerMenu != 'undefined' && typeof posContainer != 'undefined'){
		var newPositionLeft = Math.ceil(posContainerMenu.left - posContainer.left);
	}

 	
	var centerColumn = $('#columns > .container:visible').width();
	
	if(typeof newPositionLeft != 'undefined'){
		
		if(containerWidth <= centerColumn){
			submenuContainer.css({'width':containerWidth, 'left': -newPositionLeft + 'px'});
		}
		else{
			submenuContainer.css({'width':containerWidth, 'left': -newPositionLeft + 'px', 'padding-left': (containerWidth - centerColumn)/2 + 'px', 'padding-right': (containerWidth - centerColumn)/2 + 'px'});
		}
	}


};





function menuFullWidth(){
	//full width menu
	//$(window).load(function(){
	var submenuContainer = $('.sf-menu > li > ul');

	var windowWidth = $(window).width();

	var posContainer = $('.sf-menu').offset();

	submenuContainer.css({'width':windowWidth, 'left': -(posContainer.left)});

	//console.log(posContainer);

	//});
}

//menu header style two
function headerMenuStyleTwo(){

	if($('#header .header_top_right_bottom .xprt_mega_menu').length > 0){

		var headerContainer = $('#header .header_top > .container').width();

		var positionContainer = $('#header .header_top > .container').offset();

		var menuPosition = $('#header .header_top_right_bottom .xprt_mega_menu').offset();

		var positionNewCon = parseInt(menuPosition.left) - parseInt(positionContainer.left);

		$('#header .xprt_mega_menu .sf-menu > li > ul.submenu-container').css({'width':headerContainer, 'left': -(positionNewCon)+'px'});
		
	}
}




// change the menu display at different resolutions
function menuChange(status)
{
	status == 'enable' ? mobileInit(): desktopInit();
}
