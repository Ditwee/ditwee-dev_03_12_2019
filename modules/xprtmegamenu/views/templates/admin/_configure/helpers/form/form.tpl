{extends file="helpers/form/form.tpl"}
{block name="after"}
	<div class="store_each_element_column_container" style="display: none;">
		<div class="column_item_content column_item_content_XPRTCOLUMNROWID_XPRTCOLUMNID_EACHCOLUMNCOUNT">
			<div class="element_title">
				<a href="#" data-toggle="modal" data-target="#column_inner_data_popup_XPRTCOLUMNROWID_XPRTCOLUMNID_EACHCOLUMNCOUNT" class="column_items xprtmegamenu_par_level"><i class="icon-edit"></i></a>
				<span class="elementtitle"> Element: EACHCOLUMNCOUNT</span>
				<a href="#" data-classname="column_item_content_XPRTCOLUMNROWID_XPRTCOLUMNID_EACHCOLUMNCOUNT" class="xprtmegamenu_del_elm"><i class="icon-trash"></i></a>
			</div>
			<div class=" modal fade in column_inner_data_popup_XPRTCOLUMNROWID_XPRTCOLUMNID_EACHCOLUMNCOUNT" id="column_inner_data_popup_XPRTCOLUMNROWID_XPRTCOLUMNID_EACHCOLUMNCOUNT" data-row_id="XPRTCOLUMNROWID" data-column_id="XPRTCOLUMNID" data-element_id="EACHCOLUMNCOUNT">
				<div class="modal-dialog modal-md">
				    <div class="modal-content">
				        <div class="modal-header xprt_drag_controll">
				            <button type="button" data-identify="XPRTCOLUMNROWID_XPRTCOLUMNID_EACHCOLUMNCOUNT"  class="icon_popup_cls close column_content_exit" data-dismiss="modal">×</button>
				            <h4 class="modal-title">Element: EACHCOLUMNCOUNT Content</h4>
				        </div>
				        <div class="modal-body">
				            <div class="column_inner_data">
								<div class="form-group">
									<label class="control-label col-lg-3">
									Show level
									</label>
									<div class="col-lg-9">
										<select name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][shwlvl]"  class="xprt_shwlvl">
											<option value="hide">Hide</option>
											<option value="show">Show</option>
										</select>
									</div>
								</div>
								{* start language work *}
				            	<div class="form-group xprt_shwlvl_group xprt_shwlvl_show">
				            		<label class="control-label col-lg-3">
				            			Level
				            		</label>
				            		<div class="col-lg-9">
				            		{foreach from=$languages item=language}
				            			{if $languages|count > 1}
				            			<div class="translatable-field row lang-{$language.id_lang}">
				            				<div class="col-lg-9">
				            			{/if}
				            			<input type="text" class="xprtmegamenu_level xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][level][{$language.id_lang}]" class="fixed-width-lg" value="">
				            				{if $languages|count > 1}
				            					</div>
				            					<div class="col-lg-3">
				            						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
				            							{$language.iso_code}
				            							<span class="caret"></span>
				            						</button>
				            						<ul class="dropdown-menu">
				            							{foreach from=$languages item=language}
				            							<li>
				            								<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
				            							</li>
				            							{/foreach}
				            						</ul>
				            					</div>
				            				</div>
				            				{/if}
				            			{/foreach}
				            		</div>
				            	</div>
				            	<div class="form-group xprt_shwlvl_group xprt_shwlvl_show">
				            		<label class="control-label col-lg-3">
				            			Badge
				            		</label>
				            		<div class="col-lg-9">
				            		{foreach from=$languages item=language}
				            			{if $languages|count > 1}
				            			<div class="translatable-field row lang-{$language.id_lang}">
				            				<div class="col-lg-9">
				            			{/if}
				            			<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][badge][{$language.id_lang}]" class="fixed-width-lg" value="">
				            				{if $languages|count > 1}
				            					</div>
				            					<div class="col-lg-3">
				            						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
				            							{$language.iso_code}
				            							<span class="caret"></span>
				            						</button>
				            						<ul class="dropdown-menu">
				            							{foreach from=$languages item=language}
				            							<li>
				            								<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
				            							</li>
				            							{/foreach}
				            						</ul>
				            					</div>
				            				</div>
				            				{/if}
				            			{/foreach}
				            		</div>
				            	</div>
				            	<div class="form-group xprt_shwlvl_group xprt_shwlvl_show">
				            		<label class="control-label col-lg-3">
				            			Level Link
				            		</label>
				            		<div class="col-lg-9">
				            		{foreach from=$languages item=language}
				            			{if $languages|count > 1}
				            			<div class="translatable-field row lang-{$language.id_lang}">
				            				<div class="col-lg-9">
				            			{/if}
				            			<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][lvllink][{$language.id_lang}]" class="fixed-width-lg" value="">
				            				{if $languages|count > 1}
				            					</div>
				            					<div class="col-lg-3">
				            						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
				            							{$language.iso_code}
				            							<span class="caret"></span>
				            						</button>
				            						<ul class="dropdown-menu">
				            							{foreach from=$languages item=language}
				            							<li>
				            								<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
				            							</li>
				            							{/foreach}
				            						</ul>
				            					</div>
				            				</div>
				            				{/if}
				            			{/foreach}
				            		</div>
				            	</div>
								{* end language work *}
				            	<div class="form-group">
				            		<label class="control-label col-lg-3">
				            			Type
				            		</label>
				            		<div class="col-lg-9">
				            			<select name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][type]" class="xprt_type">
				            				<option value="none">None</option>
				            				<option value="external">External Link</option>
				            				<option value="system">System Link</option>
				            				<option value="custom">Custom Html</option>
				            				<option value="hook">Hook</option>
				            				<option value="product">Product</option>
				            				<option value="image">Image</option>
				            				<option value="video">Video</option>
				            			</select>
				            		</div>
				            	</div>
				            	{* start language work for external *}
								<div class="form-group xprt_type_group xprt_type_external">
									<label class="control-label col-lg-3">
										External Level
									</label>
									<div class="col-lg-9">
										{foreach from=$languages item=language}
										{if $languages|count > 1}
										<div class="translatable-field row lang-{$language.id_lang}">
											<div class="col-lg-6">
										{/if}
										<input type="text" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][xtrllvl][{$language.id_lang}]" class="">
										{if $languages|count > 1}
											</div>
											<div class="col-lg-2">
												<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
													{$language.iso_code}
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
													{foreach from=$languages item=language}
													<li>
														<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
													</li>
													{/foreach}
												</ul>
											</div>
										</div>
										{/if}
										{/foreach}
									</div>
								</div>

								<div class="form-group xprt_type_group xprt_type_external">
									<label class="control-label col-lg-3">
										External URL
									</label>
									<div class="col-lg-9">
										{foreach from=$languages item=language}
										{if $languages|count > 1}
										<div class="translatable-field row lang-{$language.id_lang}">
											<div class="col-lg-6">
										{/if}
										<input type="text" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][xtrlurl][{$language.id_lang}]" class="">
										{if $languages|count > 1}
											</div>
											<div class="col-lg-2">
												<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
													{$language.iso_code}
													<span class="caret"></span>
												</button>
												<ul class="dropdown-menu">
													{foreach from=$languages item=language}
													<li>
														<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
													</li>
													{/foreach}
												</ul>
											</div>
										</div>
										{/if}
										{/foreach}
									</div>
								</div>
								{* start language work for external *}
				            	<div class="form-group xprt_type_group xprt_type_system">
				            		<label class="control-label col-lg-3">
				            			Link
				            		</label>
				            		<div class="col-lg-9">
				            			<select name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][link][]" class="xprtmultipleclass" multiple="multiple">
				            			<optgroup label="Categories">
				            			{if (isset($xprt_categories) && !empty($xprt_categories))}
					            			{foreach from=$xprt_categories item=cate}
					            				<option value="cat_{$cate.id}">{$cate.name}</option>
					            			{/foreach}
										{/if}
				            			</optgroup>
				            			<optgroup label="Manufacturers">
				            			{if (isset($xprt_manufacturers) && !empty($xprt_manufacturers))}
					            				<option value="man_0">All Manufacturer</option>
					            			{foreach from=$xprt_manufacturers item=man}
					            				<option value="man_{$man.id_manufacturer}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$man.name}</option>
					            			{/foreach}
										{/if}
				            			</optgroup>
				            			<optgroup label="Suppliers">
				            			{if (isset($xprt_suppliers) && !empty($xprt_suppliers))}
					            				<option value="sup_0">All Suppliers</option>
					            			{foreach from=$xprt_suppliers item=sup}
					            				<option value="sup_{$sup.id_supplier}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$sup.name}</option>
					            			{/foreach}
										{/if}
				            			</optgroup>
				            			<optgroup label="CMS Categories">
				            			{if (isset($xprt_cmscategories) && !empty($xprt_cmscategories))}
					            				<option value="cmscat_0">All CMS Categories</option>
					            			{foreach from=$xprt_cmscategories item=cmscat}
					            				<option value="{$cmscat.id}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$cmscat.name}</option>
					            			{/foreach}
										{/if}
				            			</optgroup>
				            			<optgroup label="Products">
				            			{if (isset($xprt_products) && !empty($xprt_products))}
					            			{foreach from=$xprt_products item=cms}
					            				<option value="prd_{$cms.id}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$cms.name}</option>
					            			{/foreach}
										{/if}
				            			</optgroup>
				            			<optgroup label="Pages">
				            			{if (isset($xprt_pages) && !empty($xprt_pages))}
				            				{foreach from=$xprt_pages item=page}
				            					<option value="page_{$page.id}">{$page.name}</option>
				            				{/foreach}
				            			{/if}
				            			</optgroup>
				            			<optgroup label="Modules Pages">
				            				{if (isset($xprt_modulepages) && !empty($xprt_modulepages))}
				            					{foreach from=$xprt_modulepages item=xprtmodulepage key=modulename}
				            						<option disabled="disabled" style="font-weight: bold;"><strong>&nbsp;{$modulename}</strong></option>
				            						{foreach from=$xprtmodulepage item=modulepage}
				            							<option value="module_{$modulepage.id}">{$modulepage.name}</option>
				            						{/foreach}
				            					{/foreach}
				            				{/if}
				            			</optgroup>
				            			{if (isset($getxipblogcategory) && !empty($getxipblogcategory)) || (isset($getxipblogposts) && !empty($getxipblogposts))}
				            			<optgroup label="XipBlog Modules Pages">
				            				{if (isset($getxipblogcategory) && !empty($getxipblogcategory))}
				            					<option disabled="disabled" style="font-weight: bold;"><strong>&nbsp;Blog Category</strong></option>
				            					{foreach from=$getxipblogcategory item=XIPCAT}
				            						<option value="XIPCAT_{$XIPCAT.id}">{$XIPCAT.name}</option>
				            					{/foreach}
				            				{/if}
				            				{if (isset($getxipblogposts) && !empty($getxipblogposts))}
				            					<option disabled="disabled" style="font-weight: bold;"><strong>&nbsp;Blog Post</strong></option>
				            					{foreach from=$getxipblogposts item=XIPPOST}
				            						<option value="XIPPOST_{$XIPPOST.id}">{$XIPPOST.name}</option>
				            					{/foreach}
				            				{/if}
				            			</optgroup>
				            			{/if}
				            			{if isset($xprtmultishop) && !empty($xprtmultishop) && $xprtmultishop == 'enable'}
					            			{if isset($xprt_shoppages) && !empty($xprt_shoppages)}
					            				<optgroup label="MultiShop: All Shops">
					            					{foreach from=$xprt_shoppages item=xprtshop}
					            						<option value="xprtshop_{$xprtshop.id}">{$xprtshop.name}</option>
					            					{/foreach}
					            				</optgroup>
					            			{/if}
				            			{/if}
				            			</select>
				            		</div>
				            	</div>

				            	<div class="form-group xprt_type_group xprt_type_hook">
				            		<label class="control-label col-lg-3">
				            			Hook
				            		</label>
				            		<div class="col-lg-9">
				            			<select name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][hook]" class="">
				            				{if (isset($xprt_hooks) && !empty($xprt_hooks))}
				            					{foreach from=$xprt_hooks item=hook}
				            						<option value="{$hook.id}">{$hook.name}</option>
				            					{/foreach}
				            				{/if}
				            			</select>
				            		</div>
				            	</div>

				            	<div class="form-group xprt_type_group xprt_type_custom">
				            		<label class="control-label col-lg-3">
				            			Custom
				            		</label>
				            		<div class="col-lg-9">
				            		{foreach from=$languages item=language}
				            			{if $languages|count > 1}
				            			<div class="translatable-field row lang-{$language.id_lang}">
				            				<div class="col-lg-6">
				            			{/if}
				            			<textarea name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][customhtml][{$language.id_lang}]" class="" cols="30" rows="10"></textarea>
				            			{if $languages|count > 1}
				            						</div>
				            						<div class="col-lg-2">
				            							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
				            								{$language.iso_code}
				            								<span class="caret"></span>
				            							</button>
				            							<ul class="dropdown-menu">
				            								{foreach from=$languages item=language}
				            								<li>
				            									<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
				            								</li>
				            								{/foreach}
				            							</ul>
				            						</div>
				            					</div>
				            					{/if}
				            				{/foreach}
				            		</div>
				            	</div>

				            	<div class="form-group xprt_type_group xprt_type_product">
				            		<label class="control-label col-lg-3">
				            			Products
				            		</label>
				            		<div class="col-lg-9">
				            			<select name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][products][]" class="xprtmultipleclass" multiple="multiple">
				            				{if (isset($xprt_products) && !empty($xprt_products))}
	    			            				{foreach from=$xprt_products item=prd}
	    				            				<option value="prd_{$prd.id}">{$prd.name}</option>
	    				            			{/foreach}
    				            			{/if}
				            			</select>
				            		</div>
				            	</div>

				            	<div class="form-group xprt_type_group xprt_type_image">
				            		<label class="control-label col-lg-3">
				            			Image
				            		</label>
									<div class="col-lg-9 xprtmegamenu_row_upload_parent">
										<input type="file" name="xprtmegamenu_row_temp" class="xprtmegamenu_row_temp btn btn-default">
										<input type="hidden" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][conimage]" class="xprtmegamenu_row_val" value="">
										<div class="xprtmegamenu_row_show"></div>
										<div name="xprtmegamenu_row_upload" class="xprtmegamenu_row_upload btn btn-default m_top_5"><i class="icon-folder-open"></i> Upload</div>
									</div>
				            	</div>

				            	<div class="form-group xprt_type_group xprt_type_image xprt_type_video">
				            		<label class="control-label col-lg-3">
				            			Height
				            		</label>
				            		<div class="col-lg-9">
				            			<input type="text" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][height]" class="">
				            		</div>
				            	</div>

				            	<div class="form-group xprt_type_group xprt_type_image xprt_type_video">
				            		<label class="control-label col-lg-3">
				            			Width
				            		</label>
				            		<div class="col-lg-9">
				            			<input type="text" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][width]" class="">
				            		</div>
				            	</div>

				            	<div class="form-group xprt_type_group xprt_type_video">
				            		<label class="control-label col-lg-3">
				            			Video
				            		</label>
				            		<div class="col-lg-9">
				            			<input type="text" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][eachcolumn][EACHCOLUMNCOUNT][video]" class="">
				            		</div>
				            	</div>
				            </div>
				        </div>
				        <div class="modal-footer">
				        	<div class="col-sm-3 pull-right">
				            	<button type="button" data-identify="XPRTCOLUMNROWID_XPRTCOLUMNID_EACHCOLUMNCOUNT" class="btn btn-primary btn-block btn-sm icon_popup_cls column_content_exit" data-dismiss="modal">Submit</button>
				        	</div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="store_element_row_container" style="display: none;">
		<div class="element_row_container element_row_container_XPRTROWID" data-row_id="XPRTROWID">
			<div class="row_heder_container">
				<a href="#" class="row_controll_elem row_move"><i class="icon-arrows"></i></a>
				<span class="column_controll_elem">  Row: XPRTROWID  </span>
				<a href="#" class="row_controll_elem row_excol pull-right"><i class="icon-caret-down"></i></a>
				<a href="#" class="row_controll_elem row_delete pull-right"><i class="icon-trash"></i></a>
				<a href="#" data-toggle="modal" data-target="#row_inner_container_popup_XPRTROWID" class="row_controll_elem row_setting pull-right"><i class="icon-edit"></i></a>
				<a href="#" class="row_controll_elem row_add_column pull-right"><i class="process-icon-new"></i></a>
			</div>
			<div class="row_inner_container">
				
			</div>
			<div class="row_inner_container_popup_container xprt_drag_elm modal fade in" data-row_id="XPRTROWID" id="row_inner_container_popup_XPRTROWID" style="display: none;">
				<div class="modal-dialog modal-md">
				    <div class="modal-content">
				        <div class="modal-header xprt_drag_controll">
				            <button type="button" class="icon_popup_cls close" data-dismiss="modal">×</button>
				            <h4 class="modal-title">Row: XPRTROWID Settings</h4>
				        </div>
				        <div class="modal-body">
				            <div class="row_inner_container_popup">
            					<div class="form-group">
            						<label class="control-label col-lg-3">
            							Row Padding
            						</label>
            						<div class="col-lg-9">
            							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[XPRTROWID][settings][padding]" class="fixed-width-lg" value="">
            							<div class="help-block-container">
            								<p class="help-block">
            									Please Enter Row Padding (0px 0px 0px 0px) (top right bottom left)
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group">
            						<label class="control-label col-lg-3">
            							Row Border
            						</label>
            						<div class="col-lg-9">
            							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[XPRTROWID][settings][border]" class="fixed-width-lg" value="">
            							<div class="help-block-container">
            								<p class="help-block">
            									Please Enter Row Border Size In Pixel
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group">
            						<label class="control-label col-lg-3">
            							Row Border Color
            						</label>
            						<div class="col-lg-9">
            								<input type="color" data-hex="true" class="fixed-width-lg color mColorPickerInput" name="xprtmegamenu[XPRTROWID][settings][borderclr]" value="" />	
            							<div class="help-block-container">
            								<p class="help-block">
            									Please Enter Row Border Color
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group">
            						<label class="control-label col-lg-3">
            							Row Background
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld row_bg" name="xprtmegamenu[XPRTROWID][settings][bgtype]">
            								<option value="none">None</option>
            								<option value="color">Color</option>
            								<option value="image">Image</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Please Select Row Background Type.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group row_bg_group row_bg_color">
            						<label class="control-label col-lg-3">
            							Background Color
            						</label>
            						<div class="col-lg-9">
            							<input type="color" data-hex="true" class="fixed-width-lg color mColorPickerInput" name="xprtmegamenu[XPRTROWID][settings][bgclr]" class="fixed-width-lg" value="">
            							<div class="help-block-container">
            								<p class="help-block">
            									Please Select Row Background Color.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group row_bg_group row_bg_image">
            						<label class="control-label col-lg-3">
            							Upload Background Image
            						</label>
            						<div class="col-lg-9 xprtmegamenu_row_upload_parent">
										<input type="file" name="xprtmegamenu_row_temp" class="xprtmegamenu_row_temp btn btn-default">
										<input type="hidden" name="xprtmegamenu[XPRTROWID][settings][bgimg]" class="xprtmegamenu_row_val" value="">
										<div class="xprtmegamenu_row_show"></div>
										<div name="xprtmegamenu_row_upload" class="xprtmegamenu_row_upload btn btn-default m_top_5"><i class="icon-folder-open"></i> Upload</div>
            						</div>
            					</div>
            					<div class="form-group row_bg_group row_bg_image">
            						<label class="control-label col-lg-3">
            							Background Reapet
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld" name="xprtmegamenu[XPRTROWID][settings][bgrepeat]">
            								<option value="repeat">Repeat</option>
            								<option value="no-repeat">No repeat</option>
            								<option value="repeat-x">Repeat-X</option>
            								<option value="repeat-y">Repeat-Y</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Select Row background repeat/no-repeat.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group row_bg_group row_bg_image">
            						<label class="control-label col-lg-3">
            							Background Position
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld" name="xprtmegamenu[XPRTROWID][settings][bgpos]">
            								<option value="left top">left top</option>
            								<option value="left center">left center</option>
            								<option value="left bottom">left bottom</option>
            								<option value="right top">right top</option>
            								<option value="right center">right center</option>
            								<option value="right bottom">right bottom</option>
            								<option value="center top">center top</option>
            								<option value="center center">center center</option>
            								<option value="center bottom">center bottom</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Select Row background position.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group row_bg_group row_bg_image">
            						<label class="control-label col-lg-3">
            							Background Size
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld" name="xprtmegamenu[XPRTROWID][settings][bgsize]">
            								<option value="cover">Cover</option>
            								<option value="contain">Contain</option>
            								<option value="initial">initial</option>
            								<option value="100% 100%">100% 100%</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Select Row background size.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group row_bg_group row_bg_image">
            						<label class="control-label col-lg-3">
            							Background Attachment
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld" name="xprtmegamenu[XPRTROWID][settings][bgattach]">
            								<option value="scroll">scroll</option>
            								<option value="fixed">fixed</option>
            								<option value="local">local</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Select Row background attachment.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group">
            						<label class="control-label col-lg-3">
            							Custom Class
            						</label>
            						<div class="col-lg-9">
            							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[XPRTROWID][settings][customcls]" class="fixed-width-lg" value="">
            							<div class="help-block-container">
            								<p class="help-block">
		    									Please Enter a Custom Class.
		    								</p>
            							</div>
            						</div>
            					</div>

				            </div>
				        </div>
				        <div class="modal-footer">
				        	<div class="col-sm-3 pull-right">
				            	<button type="button" class="btn btn-primary btn-block btn-sm icon_popup_cls " data-dismiss="modal">Submit</button>
				        	</div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="store_element_column_container" style="display: none;">
		<div class="element_column_container element_column_container_XPRTCOLUMNID ui-widget-content" data-row_id="XPRTCOLUMNROWID" data-column_id="XPRTCOLUMNID">
			<div class="column_heder_container">
				<a href="#" class="ui-widget-header column_controll_elem column_move"><i class="icon-arrows-alt"></i></a>
				<span class="column_controll_elem">  Column: XPRTCOLUMNID  </span>
				<a href="#" class="column_controll_elem column_excol pull-right"><i class="icon-caret-down"></i></a>
				<a href="#" class="column_controll_elem column_delete pull-right"><i class="icon-trash"></i></a>
				<a href="#" data-toggle="modal" data-target="#column_inner_container_popup_XPRTCOLUMNROWID_XPRTCOLUMNID" class="column_controll_elem column_setting pull-right"><i class="icon-edit"></i></a>
			</div>
			<div class="column_inner_container">
				<div class="column_inner_container_content">
					{* start column_inner_container_content *}
					
					{* end column_inner_container_content *}
				</div>
				<div class="add_column_elem_parent">
					<a href="#" class="add_column_elem" data-row_id="XPRTCOLUMNROWID" data-column_id="XPRTCOLUMNID"><i class="process-icon-new"></i> Add Element </a>
				</div>
			</div>
			<div class="column_inner_container_popup_container xprt_drag_elm modal fade in" data-row_id="XPRTCOLUMNROWID" data-column_id="XPRTCOLUMNID" id="column_inner_container_popup_XPRTCOLUMNROWID_XPRTCOLUMNID">
				<div class="modal-dialog modal-md">
				    <div class="modal-content">
				        <div class="modal-header xprt_drag_controll">
				            <button type="button" class="icon_popup_cls close" data-dismiss="modal">×</button>
				            <h4 class="modal-title">Column: XPRTCOLUMNID Settings</h4>
				        </div>
				        <div class="modal-body">
				            <div class="column_inner_container_popup">

            					<div class="form-group">
            						<label class="control-label col-lg-3">
            							Width
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][width]">
            								<option value="12">12 column</option>
            								<option value="11">11 column</option>
            								<option value="10">10 column</option>
            								<option value="9">9 column</option>
            								<option value="8">8 column</option>
            								<option value="7">7 column</option>
            								<option value="6">6 column</option>
            								<option value="5">5 column</option>
            								<option value="4">4 column</option>
            								<option value="3">3 column</option>
            								<option value="2">2 column</option>
            								<option value="1">1 column</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Please Enter Width in pixel Value.(200px;)
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group">
            						<label class="control-label col-lg-3">
            							Padding
            						</label>
            						<div class="col-lg-9">
            							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][padding]" class="fixed-width-lg" value="">
            							<div class="help-block-container">
            								<p class="help-block">
            									Please Enter Padding (0px 0px 0px 0px)(top right bottom left)
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group">
            						<label class="control-label col-lg-3">
            							Column Background
            						</label>
            						<div class="col-lg-9">
            							<select class="xprt_clm_bg xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][bgtype]">
            								<option value="none">None</option>
            								<option value="image">Image</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Please Select Column Background Type.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
            						<label class="control-label col-lg-3">
            							Upload Background Image
            						</label>
            						<div class="col-lg-9 xprtmegamenu_row_upload_parent">
										<input type="file" name="xprtmegamenu_row_temp" class="xprtmegamenu_row_temp btn btn-default">
										<input type="hidden" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][colimage]" class="xprtmegamenu_row_val" value="">
										<div class="xprtmegamenu_row_show"></div>
										<div name="xprtmegamenu_row_upload" class="xprtmegamenu_row_upload btn btn-default m_top_5"><i class="icon-folder-open"></i> Upload</div>
            						</div>
            					</div>

            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
            						<label class="control-label col-lg-3">
            							Background Reapet
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][bgrepeat]">
            								<option value="repeat">Repeat</option>
            								<option value="no-repeat">No repeat</option>
            								<option value="repeat-x">Repeat-X</option>
            								<option value="repeat-y">Repeat-Y</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Select Column background repeat/no-repeat.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
            						<label class="control-label col-lg-3">
            							Background Position
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][bgpos]">
            								<option value="left top">left top</option>
            								<option value="left center">left center</option>
            								<option value="left bottom">left bottom</option>
            								<option value="right top">right top</option>
            								<option value="right center">right center</option>
            								<option value="right bottom">right bottom</option>
            								<option value="center top">center top</option>
            								<option value="center center">center center</option>
            								<option value="center bottom">center bottom</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Select Column background position.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
            						<label class="control-label col-lg-3">
            							Background Size
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][bgsize]">
            								<option value="cover">Cover</option>
            								<option value="contain">Contain</option>
            								<option value="initial">initial</option>
            								<option value="100% 100%">100% 100%</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Select Column background size.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
            						<label class="control-label col-lg-3">
            							Background Attachment
            						</label>
            						<div class="col-lg-9">
            							<select class="xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][bgattach]">
            								<option value="scroll">scroll</option>
            								<option value="fixed">fixed</option>
            								<option value="local">local</option>
            							</select>
            							<div class="help-block-container">
            								<p class="help-block">
            									Select Column background attachment.
            								</p>
            							</div>
            						</div>
            					</div>

            					<div class="form-group">
            						<label class="control-label col-lg-3">
            							Custom Class
            						</label>
            						<div class="col-lg-9">
            							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[XPRTCOLUMNROWID][XPRTCOLUMNID][customcls]" class="fixed-width-lg" value="">
            							<div class="help-block-container">
            								<p class="help-block">
		    									Please Enter a Custom Class.
		    								</p>
            							</div>
            						</div>
            					</div>

				            </div>
				        </div>
				        <div class="modal-footer">
				        	<div class="col-sm-3 pull-right">
				            	<button type="button" class="btn btn-primary btn-block btn-sm icon_popup_cls " data-dismiss="modal">Submit</button>
				        	</div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">

$(function() {
	function mrowssettingload() { 
		$(".row_bg").each(function(index,element){
			var column_inner_data = $(this).parent().parent().parent(".row_inner_container_popup");
			column_inner_data.children(".row_bg_group").hide();
			var val = $(this).val();
			column_inner_data.children(".row_bg_"+val).show();
		});
	 } 
	 function mrowssetting() { 
		$(".row_bg").live("change",function(){
			var column_inner_data = $(this).parent().parent().parent(".row_inner_container_popup");
			column_inner_data.children(".row_bg_group").hide();
			var val = $(this).val();
			column_inner_data.children(".row_bg_"+val).show();
		});
	 }
	mrowssettingload();
	mrowssetting();
	function mainrowssettingload() { 
		$(".mainrow_bg").each(function(index,element){
			var column_inner_data = $(this).parent().parent().parent(".mainrow_inner_container_popup");
			column_inner_data.children(".mainrow_bg_group").hide();
			var val = $(this).val();
			column_inner_data.children(".mainrow_bg_"+val).show();
		});
	 } 
	 function mainrowssetting() { 
		$(".mainrow_bg").live("change",function(){
			var column_inner_data = $(this).parent().parent().parent(".mainrow_inner_container_popup");
			column_inner_data.children(".mainrow_bg_group").hide();
			var val = $(this).val();
			column_inner_data.children(".mainrow_bg_"+val).show();
		});
	 }
	mainrowssettingload();
	mainrowssetting();
	function columnssettingload() { 
		$(".xprt_clm_bg").each(function(index,element){
			var column_inner_data = $(this).parent().parent().parent(".column_inner_container_popup");
			column_inner_data.children(".xprt_clm_bg_group").hide();
			var val = $(this).val();
			column_inner_data.children(".xprt_clm_bg_"+val).show();
		});
	 } 
	 function columnssetting() { 
		$(".xprt_clm_bg").live("change",function(){
			var column_inner_data = $(this).parent().parent().parent(".column_inner_container_popup");
			column_inner_data.children(".xprt_clm_bg_group").hide();
			var val = $(this).val();
			column_inner_data.children(".xprt_clm_bg_"+val).show();
		});
	 }
	columnssettingload();
	columnssetting();
	function xprtshwlvlload() { 
		$(".xprt_shwlvl").each(function(index,element){
			hideOtherLanguage(default_language);
			var column_inner_data = $(this).parent().parent().parent(".column_inner_data");
			column_inner_data.children(".xprt_shwlvl_group").hide();
			var val = $(this).val();
			column_inner_data.children(".xprt_shwlvl_"+val).show();
		});
	 } 
	 function xprtshwlvlsetting() { 
		$(".xprt_shwlvl").live("change",function(){
			javascript:hideOtherLanguage(default_language);
			var column_inner_data = $(this).parent().parent().parent(".column_inner_data");
			column_inner_data.children(".xprt_shwlvl_group").hide();
			var val = $(this).val();
			column_inner_data.children(".xprt_shwlvl_"+val).show();
		});
	 }
	xprtshwlvlload();
	xprtshwlvlsetting();
	 function eachsettingload() { 
		$(".xprt_type").each(function(index,element){
			var column_inner_data = $(this).parent().parent().parent(".column_inner_data");
			column_inner_data.children(".xprt_type_group").hide();
			var val = $(this).val();
			column_inner_data.children(".xprt_type_"+val).show();
		});
	 } 
	 function eachsetting() { 
		$(".xprt_type").live("change",function(){
			var column_inner_data = $(this).parent().parent().parent(".column_inner_data");
			column_inner_data.children(".xprt_type_group").hide();
			var val = $(this).val();
			column_inner_data.children(".xprt_type_"+val).show();
		});
	 } 
	eachsettingload();
	eachsetting();
	// start setting show/hide system
	function is_fullwidth_type_hide() { 
		$("[name=megawidth]").parent().parent(".form-group").hide();
	}
	function is_fullwidth_type_set() { 
		$("[name=megawidth]").parent().parent(".form-group").show();
	}
	function is_fullwidth_settings() { 
		$("[name=is_fullwidth]").live("change",function(){
			is_fullwidth_settings_live();
		});
	 }
	function is_fullwidth_settings_live() { 
		var valc = $("[name=is_fullwidth]:checked").val();
		if(valc == 1){
			is_fullwidth_type_hide();
		}else{
			is_fullwidth_type_set();
		}
	 }
	function is_megamenu_type_set() { 
		$("[name=megawidth]").parent().parent(".form-group").show();
		$(".mainrow_items").parent().parent(".form-group").show();
		$("[name=is_fullwidth]").parent().parent().parent(".form-group").show();
		is_fullwidth_settings_live();
		$(".element_inject_parentcontainer").show();
		$(".row_inject_parentcontainer").show();
	}
	function is_megamenu_type_hide() { 
		$("[name=megawidth]").parent().parent(".form-group").hide();
		$(".mainrow_items").parent().parent(".form-group").hide();
		$("[name=is_fullwidth]").parent().parent().parent(".form-group").hide();
		$(".element_inject_parentcontainer").hide();
		$(".row_inject_parentcontainer").hide();
	}
	function is_megamenu_settings_live() { 
		var valc = $("[name=is_megamenu]:checked").val();
		if(valc == 1){
			is_megamenu_type_set();
		}else{
			is_megamenu_type_hide();
		}
	 }
	 function is_megamenu_settings() { 
		$("[name=is_megamenu]").live("change",function(){
			is_megamenu_settings_live();
		});
		hideOtherLanguage(default_language);
	 }
	function submenu_type_hide() { 
		$("[name=submenu_type]").parent().parent(".form-group").hide();
		all_submenu_type_hide();
	}
	function all_submenu_type_set() { 
	    var thisc = $("[name=submenu_type]");
		var sval = thisc.val();
		all_submenu_type_hide();
		$("."+sval).parents(".form-group").show();
		hideOtherLanguage(default_language);
	 } 
	function all_submenu_type_hide() { 
		$("[name=submenu_type]").find("option").each(function(index, element) { 
	    	var ecval = $(this).val();
	    	$("."+ecval).parents(".form-group").hide();
	     } );
	}
	function submenu_settings() { 
		$("[name=is_submenu]").live("change",function(){
			submenu_settings_live();
		});
	 }
	function submenu_settings_live() { 
		var valc = $("[name=is_submenu]:checked").val();
		if(valc == 1){
			$("[name=submenu_level]").parent().parent(".form-group").show();
			$("[name=submenu_type]").parent().parent(".form-group").show();
			all_submenu_type_set();
		}else{
			$("[name=submenu_level]").parent().parent(".form-group").hide();
			submenu_type_hide();
		}
	 }
	 $("[name=submenu_type]").live("change",function(){
		all_submenu_type_set();
	 });

	function all_link_type_hide() { 
	    $("#link_type").find("option").each(function(index, element) { 
	    	var ecval = $(this).val();
	    	$("."+ecval).parent().parent(".form-group").hide();
	     } );
	 } 

	 function all_link_type_set() { 
	    var thisc = $("#link_type");
		var sval = thisc.val();
		all_link_type_hide();
		$("."+sval).parent().parent(".form-group").show();
	 } 
	$("#link_type").live("change",function(){
		all_link_type_set();
	});
	is_megamenu_settings_live();
	is_megamenu_settings();
	is_fullwidth_settings();
	is_fullwidth_settings_live();
	submenu_settings();
	submenu_settings_live();
	all_link_type_set();
	// end setting show/hide system
	$(".add_column_elem").live("click",function(e){		
		e.preventDefault();
		var rowid = $(this).data("row_id");
		var columnid = $(this).data("column_id");
		var rcont = $(".store_each_element_column_container").html();
		var ec_par = $(this).parent().parent(".column_inner_container").children(".column_inner_container_content");
		var lnth = $(this).parent().parent().children(".column_inner_container_content").children().length;
		rcont = rcont.replace(/\XPRTCOLUMNROWID/g,rowid);
		rcont = rcont.replace(/\XPRTCOLUMNID/g,columnid);
		rcont = rcont.replace(/\EACHCOLUMNCOUNT/g,lnth);
		ec_par.append(rcont);
	});
	$(".row_inject_element").on("click",function(e){		
		e.preventDefault();
		var rcont = $(".store_element_row_container").html();
		var lnth = $(".element_inject_parentcontainer").children().length;
		rcont = rcont.replace(/\XPRTROWID/g,lnth);
		$(".element_inject_parentcontainer").append(rcont);
	});
	$(".row_add_column").live('click', function(e){
		e.preventDefault();
		var ccont = $(".store_element_column_container").html();
		var rootx = $(this).parent().parent();
		var row_id = rootx.data("row_id");
		var main_elm = rootx.find(".row_inner_container");
		var lnth = main_elm.children().length;
		ccont = ccont.replace(/\XPRTCOLUMNID/g,lnth);
		ccont = ccont.replace(/\XPRTCOLUMNROWID/g,row_id);
		main_elm.append(ccont);
		$(".column_inner_container_content").sortable({
	    	axis: "y",cursor: 'move',opacity: 0.5
		});
	});
	$(".row_delete").live("click",function(e){
		e.preventDefault();
		$(this).parent().parent().children().remove();
	});
	$(".column_delete").live("click",function(e){
		e.preventDefault();
		var elmn = $(this).parent().parent();
		elmn.children().remove();
		elmn.hide();
	});
	$(".row_excol").live("click",function(e){
		e.preventDefault();
		$(this).parent().parent().children(".row_inner_container").toggle();
	});
	$(".column_excol").live("click",function(e){
		e.preventDefault();
		$(this).parent().parent().children(".column_inner_container").toggle();
	});
	$(".column_inner_container_content").sortable({
    	axis: "y",cursor: 'move',opacity: 0.5,handle: ".column_items",
	}); 
	$("#element_inject_parentcontainer").sortable({
	    connectWith: '#element_inject_parentcontainer',
	    cursor: 'move',
	    opacity: 0.5,
	    handle: ".row_move",
	    cancel: ".portlet-toggle",
	    cursorAt: { 
	        top: 0, left: 0 
	    },
	    placeholder: 'ui-sortable-placeholder',
	    tolerance: 'pointer',
	    update: function( event, ui ) {

	    }
	}); 
	$(".row_inner_container").sortable({
	    connectWith: '.row_inner_container',
	    cursor: 'move',
    	opacity: 0.5,
    	axis: "x",
	    handle: ".column_move",
	    cancel: ".portlet-toggle",
	    cursorAt: { 
	        top: 0, left: 0 
	    },
	    placeholder: 'ui-sortable-placeholder',
	    tolerance: 'pointer',
	    update: function( event, ui ) {

	    }
	});
	$(".row_add_column").live("click",function(e){
				e.preventDefault();
		$(".row_inner_container").sortable({
		    connectWith: '.row_inner_container',
		    cursor: 'move',
	    	opacity: 0.5,
	    	axis: "x",
		    handle: ".column_move",
		    cancel: ".portlet-toggle",
		    cursorAt: { 
		        top: 0, left: 0 
		    },
		    placeholder: 'ui-sortable-placeholder',
		    tolerance: 'pointer',
		    update: function( event, ui ) {

		    }
		}); 
	});
	// $(".xprtmegamenu_row_upload").live("click",function() {
	// 	console.log($(this).parent(".xprtmegamenu_row_upload_parent").children(".xprtmegamenu_row_val").val("I am print"));
	//  });
	$(".xprtmegamenu_row_upload").live("click",function(e) { 
				e.preventDefault();
		var parentp = $(this).parent(".xprtmegamenu_row_upload_parent");
		var file_data = parentp.children(".xprtmegamenu_row_temp").prop("files")[0]; 
		var form_data = new FormData(); 
		form_data.append("imagedata", file_data);
		  $.ajax({
		    url: "{$controllers_link}&action=uploadmenuimage&ajax=1",
		    dataType: 'json',
		    cache: false,
		    contentType: false,
		    processData: false,
		    data: form_data, 
		    type: 'post',
		    success: function(data) {
				if(data.imagedata != ''){
					parentp.children(".xprtmegamenu_row_val").val(data.imagedata);
					parentp.children(".xprtmegamenu_row_show").html("<img class='xprtmegamenu_row_img_show' src='"+data.imagelink+data.imagedata+"' width='auto' height='70'>");
				}
		    },
		    error: function(data) {
		    	$.growl.error({ title: "Error", message: "Something Wrong to upload image" });
		    },
		  });
	 });
	$(".column_content_exit").live("click",function(e){
		e.preventDefault();
		var thisc = $(this);
		var org_par = thisc.parents(".column_item_content");
		var this_lbl = org_par.children().find(".xprtmegamenu_level").val();
		if (this_lbl) {
		   org_par.children().find(".elementtitle").text("");
		   org_par.children().find(".elementtitle").append(this_lbl);
		}
	});
	$(".xprtmegamenu_del_elm").live("click",function(e){
		e.preventDefault();
		var thisc = $(this);
		var classname = thisc.data("classname");
		$("."+classname).children().remove();
	});
});
</script>
<style>
.mColorPickerTrigger{
	width:160px !important;
}
.column_excol,.row_excol{
	margin-left: 10px;
}
.element_inject_parentcontainer {
    border: 1px dotted #999;
    padding: 10px;
    display: block;
}
.element_column_container{
	width:23%;
	margin-right: 10px;
}
.element_row_container,.element_column_container{
	margin-bottom: 10px; 
}
a.row_controll_elem:hover,a.column_controll_elem:hover{
 text-decoration: none;
}
a.row_controll_elem.row_move:hover,a.column_controll_elem.column_move:hover{
 color:#00aff0;
}
.icon-arrows,.icon-trash,.icon-edit,.process-icon-new, .icon-arrows-alt {
    font-size: 18px;
    padding: 0px 5px;
    display: inline-block;
    vertical-align: middle;
    width: auto;
    height: auto;
}
.row_inject_parentcontainer,.column_inject_parentcontainer{
	padding:5px;
	border:1px solid #999;
}
.row_inject_container,.column_inject_container{
	border:1px solid #999;
	padding: 10px;
}
.row_inject_element,.column_inject_element {
    display: block;
    width: 150px;
    margin: 0 auto;
    text-align: center;
}
.row_heder_container,.column_heder_container{
	position: relative;
    height: 45px;
    width: 100%;
    border: 1px solid #999;
    padding: 12px 10px;
    display: block;
    overflow: hidden;
    clear: both;
    line-height: 1;
}
.row_inner_container,.column_inner_container{
	width: 100%;
	border: 1px solid #999;
	padding: 10px 10px 40px;
	min-height: 100px;
	position: relative;
}

.add_column_elem_parent{
	vertical-align: bottom;
	position: absolute;
	bottom: 10px;
}

.column_item_content .element_title{
	padding:5px 0px;
}

.element_column_container {
    display: inline-block;
    clear: both;
    vertical-align: top;
}
.ui-widget-content a {
    color: #00aff0;
    background: none;
    border: none;
}
..ui-widget-content{
    border: none;
}
#sub_system_link.xprtmultipleclass,.column_inner_data .xprtmultipleclass{
    min-height: 250px;
}
</style>
{/block}
{block name="field"}
	{if $input.type == 'device'}
		<div class="col-lg-{if isset($input.col)}{$input.col|intval}{else}9{/if}{if !isset($input.label)} col-lg-offset-3{/if}">
			{if isset($input.device_desc) && !empty($input.device_desc)}
				{foreach $input.device_desc AS $dev}
					<div class="col-xs-4 col-sm-3">
						<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="{if isset($dev.tooltip)}{$dev.tooltip}{/if}">{if isset($dev.title)}{$dev.title}{/if}</label>
						{$devicess = "{$input.name}_{$dev.name}"}
						<select name="{$input.name}_{$dev.name}" id="{$input.name}_{$dev.name}" class="fixed-width-md">
							{if isset($dev.column) && !empty($dev.column)}
			        			{foreach $dev.column AS $column}
			                        <option value="{$column}" {if isset($fields_value[$input['name']]->{$devicess}) && ($fields_value[$input['name']]->{$devicess} == $column)} selected="selected"  {/if} >{$column}</option>
			                    {/foreach}
		                    {/if}
	        			</select>
	        			{$devicess = ""}
					</div>
				{/foreach}
			{/if}
		</div>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
{block name="description"}
{if !isset($input.expandstyle_class)}{$input.expandstyle_class = true}{/if}
{if !isset($input.expandstyle_margin)}{$input.expandstyle_margin = true}{/if}
{if !isset($input.expandstyle_padding)}{$input.expandstyle_padding = true}{/if}
{if (isset($input.expandstyle) && $input.expandstyle == true) || isset($input.desc) && !empty($input.desc)}
		<div class="help-block-container">
			{if isset($input.expandstyle) && $input.expandstyle == true}
					<a class="click-to-put-cls clk-put-cls-{$input.name}" href="#">Click Here To Use Expand Style {if isset($input.label) && !empty($input.label)} In {$input.label} {/if}</a>
					<div class="col-xs-12 col-sm-12 classnamecontainerclass">
						{if $input.expandstyle_class}
							<div class="col-xs-4 col-sm-3">
								<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="Please Enter a custom class for custom design">Custom CSS Class</label>
								<input type="text" class="fixed-width-md classnameclass put-cls-{$input.name}" id="put-cls-{$input.name}" name="put-cls-{$input.name}" value="{if isset($fields_value["put-cls-{$input.name}"])}{$fields_value["put-cls-{$input.name}"]}{/if}">
							</div>
						{/if}
						{if $input.expandstyle_margin}
						<div class="col-xs-4 col-sm-3">
							<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="top right bottom left">Custom Margin</label>
							<input type="text" class="fixed-width-md classnamemar put-mar-{$input.name}" id="put-mar-{$input.name}" name="put-mar-{$input.name}" value="{if isset($fields_value["put-mar-{$input.name}"])}{$fields_value["put-mar-{$input.name}"]}{/if}">
						</div>
						{/if}
						{if $input.expandstyle_padding}
						<div class="col-xs-4 col-sm-3">
							<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="top right bottom left">Custom Padding</label>
							<input type="text" class="fixed-width-md classnamepad put-pad-{$input.name}" id="put-pad-{$input.name}" name="put-pad-{$input.name}" value="{if isset($fields_value["put-pad-{$input.name}"])}{$fields_value["put-pad-{$input.name}"]}{/if}">
						</div>
						{/if}
					</div>
			{/if}
			{if isset($input.type) && $input.type == "file"}
				{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
					<img src="{$image_baseurl}{$fields_value[$input.name]}" height="200px" width="auto"/>
				{/if}
			{/if}
			{$smarty.block.parent}
		</div>
	{/if}
{/block}
{block name="script"}
		$(document).ready(function(){
			$(".classnamecontainerclass").hide();
			$(".click-to-put-cls").on("click",function(){
				$(this).parent().find(".classnamecontainerclass").toggle(500);
			});
		});
{/block}
{block name="input"}
	{if ($input.type == 'selecttwotype')}
			<div class="{if isset($input.hideclass)}{$input.hideclass}{/if} {$input.name} {$input.name}_class" id="{$input.name}_id">
			<select name="selecttwotype_{$input.name}" class="selecttwotype_{$input.name}_cls" id="selecttwotype_{$input.name}_id" multiple="true">
				{if (isset($input.initvalues) && !empty($input.initvalues))}
				    {foreach from=$input.initvalues item=initval}
				        {if isset($fields_value[$input.name])}
				            {assign var=settings_def_value value=","|explode:$fields_value[$input.name]}
				            {if $initval['id']|in_array:$settings_def_value}
				                {$selected = 'selected'}
				            {else}
				                {$selected = ''}
				            {/if}
				        {else}
				            {$selected = ''}
				        {/if}
				        <option {$selected} value="{$initval['id']}">{$initval['name']}</option>
				    {/foreach}
				{/if}
			</select>
			<input type="hidden" name="{$input.name}" id="{$input.name}" value="{if isset($input.defvalues)}{$input.defvalues}{else}{$fields_value[$input.name]}{/if}" class=" {$input.name} {$input.type}_field">
			</div>
			<script type="text/javascript">
			    // START SELECT TWO CALLING
			    $(function(){
			        var defVal = $("input#{$input.name}").val();
			        if(defVal.length){
			            var ValArr = defVal.split(',');
			            for(var n in ValArr){
			                $( "select#selecttwotype_{$input.name}_id" ).children('option[value="'+ValArr[n]+'"]').attr('selected','selected');
			            }
			        }
			        $( "select#selecttwotype_{$input.name}_id" ).select2( { placeholder: "{$input.placeholder}", width: 200, tokenSeparators: [',', ' '] } ).on('change',function(){
			            var data = $(this).select2('data');
			            var select = $(this);
			            var field = select.next("input#{$input.name}");
			            var saved = '';
			            select.children('option').attr('selected',null);
			            if(data.length)
			                $.each(data, function(k,v){
			                    var selected = v.id;   
			                    select.children('option[value="'+selected+'"]').attr('selected','selected');
			                    if(k > 0)
			                        saved += ',';
			                    saved += selected;                                
			                });
			             field.val(saved);   
			        });
			    });
 			// END SELECT TWO CALLING
			</script>
			<style type="text/css">
				.select2-container.select2-container-multi
				{ 
					width: 100% !important;
				}
			</style>
	{elseif ($input.type == 'rowset')}
	<a href="#" data-toggle="modal" data-target="#mainrow_inner_container_popup_main" class="mainrow_items"><i class="icon-edit"></i></a>
	<div class="mainrow_inner_container_popup_container xprt_drag_elm modal fade in" data-row_id="main" id="mainrow_inner_container_popup_main" style="display: none;">
		<div class="modal-dialog modal-md">
		    <div class="modal-content">
		        <div class="modal-header xprt_drag_controll">
		            <button type="button" class="icon_popup_cls close" data-dismiss="modal">×</button>
		            <h4 class="modal-title">Main Row Settings</h4>
		        </div>
		        <div class="modal-body">
		            <div class="mainrow_inner_container_popup">
						<div class="form-group">
							<label class="control-label col-lg-3">
								Row Padding
							</label>
							<div class="col-lg-9">
								<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[main][settings][padding]" class="fixed-width-lg" value="{if isset($mainrowsettings->main->settings->padding)}{$mainrowsettings->main->settings->padding}{/if}">
								<div class="help-block-container">
									<p class="help-block">
										Please Enter Row Padding (0px 0px 0px 0px) (top right bottom left)
									</p>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-lg-3">
								Row Border
							</label>
							<div class="col-lg-9">
								<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[main][settings][border]" class="fixed-width-lg" value="{if isset($mainrowsettings->main->settings->border)}{$mainrowsettings->main->settings->border}{/if}">
								<div class="help-block-container">
									<p class="help-block">
										Please Enter Row Border Size In Pixel
									</p>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-lg-3">
								Row Border Color
							</label>
							<div class="col-lg-9">
									<input type="color" data-hex="true" class="fixed-width-lg color mColorPickerInput" name="xprtmegamenu[main][settings][borderclr]" value="{if isset($mainrowsettings->main->settings->borderclr)}{$mainrowsettings->main->settings->borderclr}{/if}" />	
								<div class="help-block-container">
									<p class="help-block">
										Please Enter Row Border Color
									</p>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-lg-3">
								Row Background
							</label>
							<div class="col-lg-9">
								<select class="xprtmegamenu_fld mainrow_bg" name="xprtmegamenu[main][settings][bgtype]">
									<option value="none" {if isset($mainrowsettings->main->settings->bgtype) && ($mainrowsettings->main->settings->bgtype == 'none')}selected="selected"{/if}>None</option>
									<option value="color" {if isset($mainrowsettings->main->settings->bgtype) && ($mainrowsettings->main->settings->bgtype == 'color')}selected="selected"{/if}>Color</option>
									<option value="image" {if isset($mainrowsettings->main->settings->bgtype) && ($mainrowsettings->main->settings->bgtype == 'image')}selected="selected"{/if}>Image</option>
								</select>
								<div class="help-block-container">
									<p class="help-block">
										Please Select Row Background Type.
									</p>
								</div>
							</div>
						</div>

						<div class="form-group mainrow_bg_group mainrow_bg_color">
							<label class="control-label col-lg-3">
								Background Color
							</label>
							<div class="col-lg-9">
								<input type="color" data-hex="true" class="fixed-width-lg color mColorPickerInput" name="xprtmegamenu[main][settings][bgclr]" class="fixed-width-lg color mColorPickerInput" value="{if isset($mainrowsettings->main->settings->bgclr)}{$mainrowsettings->main->settings->bgclr}{/if}">
								<div class="help-block-container">
									<p class="help-block">
										Please Select Row Background Color.
									</p>
								</div>
							</div>
						</div>

						<div class="form-group mainrow_bg_group mainrow_bg_image">
							<label class="control-label col-lg-3">
								Upload Background Image
							</label>
							<div class="col-lg-9 xprtmegamenu_row_upload_parent">
								<input type="file" name="xprtmegamenu_row_temp" class="xprtmegamenu_row_temp btn btn-default">
								<input type="hidden" name="xprtmegamenu[main][settings][bgimg]" class="xprtmegamenu_row_val" value="{if isset($mainrowsettings->main->settings->bgimg)}{$mainrowsettings->main->settings->bgimg}{/if}">
								<div class="xprtmegamenu_row_show">
									{if (isset($mainrowsettings->main->settings->bgimg) && !empty($mainrowsettings->main->settings->bgimg))}
										<img class="xprtmegamenu_row_img_show" src="{$xprt_imagelink}{$mainrowsettings->main->settings->bgimg}" width="auto" height="70">
									{/if}
								</div>
								<div name="xprtmegamenu_row_upload" class="xprtmegamenu_row_upload btn btn-default m_top_5"><i class="icon-folder-open"></i> Upload</div>
							</div>
						</div>
						<div class="form-group mainrow_bg_group mainrow_bg_image">
							<label class="control-label col-lg-3">
								Background Reapet
							</label>
							<div class="col-lg-9">
								<select class="xprtmegamenu_fld" name="xprtmegamenu[main][settings][bgrepeat]">
									<option value="repeat" {if isset($mainrowsettings->main->settings->bgrepeat) && ($mainrowsettings->main->settings->bgrepeat == 'repeat')}selected="selected"{/if}>Repeat</option>
									<option value="no-repeat" {if isset($mainrowsettings->main->settings->bgrepeat) && ($mainrowsettings->main->settings->bgrepeat == 'no-repeat')}selected="selected"{/if}>No repeat</option>
									<option value="repeat-x" {if isset($mainrowsettings->main->settings->bgrepeat) && ($mainrowsettings->main->settings->bgrepeat == 'repeat-x')}selected="selected"{/if}>Repeat-X</option>
									<option value="repeat-y" {if isset($mainrowsettings->main->settings->bgrepeat) && ($mainrowsettings->main->settings->bgrepeat == 'repeat-y')}selected="selected"{/if}>Repeat-Y</option>
								</select>
								<div class="help-block-container">
									<p class="help-block">
										Select Row background repeat/no-repeat.
									</p>
								</div>
							</div>
						</div>

						<div class="form-group mainrow_bg_group mainrow_bg_image">
							<label class="control-label col-lg-3">
								Background Position
							</label>
							<div class="col-lg-9">
								<select class="xprtmegamenu_fld" name="xprtmegamenu[main][settings][bgpos]">
									<option value="left top" {if isset($mainrowsettings->main->settings->bgpos) && ($mainrowsettings->main->settings->bgpos == 'left top')}selected="selected"{/if}>left top</option>
									<option value="left center" {if isset($mainrowsettings->main->settings->bgpos) && ($mainrowsettings->main->settings->bgpos == 'left center')}selected="selected"{/if}>left center</option>
									<option value="left bottom" {if isset($mainrowsettings->main->settings->bgpos) && ($mainrowsettings->main->settings->bgpos == 'left bottom')}selected="selected"{/if}>left bottom</option>
									<option value="right top" {if isset($mainrowsettings->main->settings->bgpos) && ($mainrowsettings->main->settings->bgpos == 'right top')}selected="selected"{/if}>right top</option>
									<option value="right center" {if isset($mainrowsettings->main->settings->bgpos) && ($mainrowsettings->main->settings->bgpos == 'right center')}selected="selected"{/if}>right center</option>
									<option value="right bottom" {if isset($mainrowsettings->main->settings->bgpos) && ($mainrowsettings->main->settings->bgpos == 'right bottom')}selected="selected"{/if}>right bottom</option>
									<option value="center top" {if isset($mainrowsettings->main->settings->bgpos) && ($mainrowsettings->main->settings->bgpos == 'center top')}selected="selected"{/if}>center top</option>
									<option value="center center" {if isset($mainrowsettings->main->settings->bgpos) && ($mainrowsettings->main->settings->bgpos == 'center center')}selected="selected"{/if}>center center</option>
									<option value="center bottom" {if isset($mainrowsettings->main->settings->bgpos) && ($mainrowsettings->main->settings->bgpos == 'center bottom')}selected="selected"{/if}>center bottom</option>
								</select>
								<div class="help-block-container">
									<p class="help-block">
										Select Row background position.
									</p>
								</div>
							</div>
						</div>

						<div class="form-group mainrow_bg_group mainrow_bg_image">
							<label class="control-label col-lg-3">
								Background Size
							</label>
							<div class="col-lg-9">
								<select class="xprtmegamenu_fld" name="xprtmegamenu[main][settings][bgsize]">
									<option value="cover" {if isset($mainrowsettings->main->settings->bgsize) && ($mainrowsettings->main->settings->bgsize == 'cover')}selected="selected"{/if}>Cover</option>
									<option value="contain" {if isset($mainrowsettings->main->settings->bgsize) && ($mainrowsettings->main->settings->bgsize == 'contain')}selected="selected"{/if}>Contain</option>
									<option value="initial" {if isset($mainrowsettings->main->settings->bgsize) && ($mainrowsettings->main->settings->bgsize == 'initial')}selected="selected"{/if}>initial</option>
									<option value="100% 100%" {if isset($mainrowsettings->main->settings->bgsize) && ($mainrowsettings->main->settings->bgsize == '100% 100%')}selected="selected"{/if}>100% 100%</option>
								</select>
								<div class="help-block-container">
									<p class="help-block">
										Select Row background size.
									</p>
								</div>
							</div>
						</div>

						<div class="form-group mainrow_bg_group mainrow_bg_image">
							<label class="control-label col-lg-3">
								Background Attachment
							</label>
							<div class="col-lg-9">
								<select class="xprtmegamenu_fld" name="xprtmegamenu[main][settings][bgattach]">
									<option value="scroll" {if isset($mainrowsettings->main->settings->bgattach) && ($mainrowsettings->main->settings->bgattach == 'scroll')}selected="selected"{/if}>scroll</option>
									<option value="fixed" {if isset($mainrowsettings->main->settings->bgattach) && ($mainrowsettings->main->settings->bgattach == 'fixed')}selected="selected"{/if}>fixed</option>
									<option value="local" {if isset($mainrowsettings->main->settings->bgattach) && ($mainrowsettings->main->settings->bgattach == 'local')}selected="selected"{/if}>local</option>
								</select>
								<div class="help-block-container">
									<p class="help-block">
										Select Row background attachment.
									</p>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-lg-3">
								Custom Class
							</label>
							<div class="col-lg-9">
								<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[main][settings][customcls]" class="fixed-width-lg" value="{if isset($mainrowsettings->main->settings->customcls)}{$mainrowsettings->main->settings->customcls}{/if}">
								<div class="help-block-container">
									<p class="help-block">
										Please Enter a Custom Class.
									</p>
								</div>
							</div>
						</div>
		            </div>
		        </div>
		        <div class="modal-footer">
		        	<div class="col-sm-3 pull-right">
		            	<button type="button" class="btn btn-primary btn-block btn-sm icon_popup_cls " data-dismiss="modal">Submit</button>
		        	</div>
		        </div>
		    </div>
		</div>
	</div>
	{elseif ($input.type == 'systemlink')}
		<select name="{$input.name}{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}[]{/if}" class="{if (isset($input.class) && !empty($input.class))}{$input.class}{/if} {if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}xprtmultipleclass{/if} {$input.name}_class" id="{$input.name}" {if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}multiple{/if}>
			<optgroup label="Categories">
				{if (isset($xprt_categories) && !empty($xprt_categories))}
    			{foreach from=$xprt_categories item=cate}
    				{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
						{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
							{$arrr = $fields_value[$input.name]}
							{if $arrr|is_array}
								{$current_item = "cat_{$cate.id}"}
								<option value="cat_{$cate.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$cate.name}</option>
								{$current_item = ""}
							{else}
								<option value="cat_{$cate.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "cat_{$cate.id}")}selected="selected"{/if}>{$cate.name}</option>
							{/if}
						{else}
							<option value="cat_{$cate.id}">{$cate.name}</option>
						{/if}
					{else}
						<option value="cat_{$cate.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "cat_{$cate.id}")}selected="selected"{/if}>{$cate.name}</option>
    				{/if}
    			{/foreach}
    			{/if}
			</optgroup>
			<optgroup label="Manufacturers">
				{if (isset($xprt_manufacturers) && !empty($xprt_manufacturers))}
    			{foreach from=$xprt_manufacturers item=man}
    				{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
						{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
							{$arrr = $fields_value[$input.name]}
							{if $arrr|is_array}
								{$current_item = "man_{$man.id_manufacturer}"}
								<option value="man_{$man.id_manufacturer}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$man.name}</option>
								{$current_item = ""}
							{else}
								<option value="man_{$man.id_manufacturer}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "man_{$man.id_manufacturer}")}selected="selected"{/if}>{$man.name}</option>
							{/if}
						{else}
							<option value="man_{$man.id_manufacturer}">{$man.name}</option>
						{/if}
					{else}
						<option value="man_{$man.id_manufacturer}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "man_{$man.id_manufacturer}")}selected="selected"{/if}>{$man.name}</option>
    				{/if}
    			{/foreach}
    			{/if}
			</optgroup>
			<optgroup label="Suppliers">
			{if (isset($xprt_suppliers) && !empty($xprt_suppliers))}
    			{foreach from=$xprt_suppliers item=sup}
	    			{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
	    				{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
	    					{$arrr = $fields_value[$input.name]}
	    					{if $arrr|is_array}
	    						{$current_item = "sup_{$sup.id_supplier}"}
	    						<option value="sup_{$sup.id_supplier}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$sup.name}</option>
	    						{$current_item = ""}
	    					{else}
	    						<option value="sup_{$sup.id_supplier}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "sup_{$sup.id_supplier}")}selected="selected"{/if}>{$sup.name}</option>
	    					{/if}
	    				{else}
	    					<option value="sup_{$sup.id_supplier}">{$sup.name}</option>
	    				{/if}
	    			{else}
	    				<option value="sup_{$sup.id_supplier}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "sup_{$sup.id_supplier}")}selected="selected"{/if}>{$sup.name}</option>
	    			{/if}
    			{/foreach}
    			{/if}
			</optgroup>
			<optgroup label="CMS Categories">
				{if (isset($xprt_cmscategories) && !empty($xprt_cmscategories))}
	    			{foreach from=$xprt_cmscategories item=cmscat}
	    				{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
	    					{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
	    						{$arrr = $fields_value[$input.name]}
	    						{if $arrr|is_array}
	    							{$current_item = "{$cmscat.id}"}
	    							<option value="{$cmscat.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$cmscat.name}</option>
	    							{$current_item = ""}
	    						{else}
	    							<option value="{$cmscat.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "{$cmscat.id}")}selected="selected"{/if}>{$cmscat.name}</option>
	    						{/if}
	    					{else}
	    						<option value="{$cmscat.id}">{$cmscat.name}</option>
	    					{/if}
	    				{else}
	    					<option value="{$cmscat.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "{$cmscat.id}")}selected="selected"{/if}>{$cmscat.name}</option>
	    				{/if}
	    			{/foreach}
    			{/if}
			</optgroup>
			<optgroup label="Products">
				{if (isset($xprt_products) && !empty($xprt_products))}
	    			{foreach from=$xprt_products item=prd}
	    				{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
	    					{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
	    						{$arrr = $fields_value[$input.name]}
	    						{if $arrr|is_array}
	    							{$current_item = "prd_{$prd.id}"}
	    							<option value="prd_{$prd.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$prd.name}</option>
	    							{$current_item = ""}
	    						{else}
	    							<option value="prd_{$prd.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "prd_{$prd.id}")}selected="selected"{/if}>{$prd.name}</option>
	    						{/if}
	    					{else}
	    						<option value="prd_{$prd.id}">{$prd.name}</option>
	    					{/if}
	    				{else}
	    					<option value="prd_{$prd.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "prd_{$prd.id}")}selected="selected"{/if}>{$prd.name}</option>
	    				{/if}
	    			{/foreach}
    			{/if}
			</optgroup>
			<optgroup label="Pages">
				{if (isset($xprt_pages) && !empty($xprt_pages))}
					{foreach from=$xprt_pages item=page}
						{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
							{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
								{$arrr = $fields_value[$input.name]}
								{if $arrr|is_array}
									{$current_item = "page_{$page.id}"}
									<option value="page_{$page.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$page.name}</option>
									{$current_item = ""}
								{else}
									<option value="page_{$page.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "page_{$page.id}")}selected="selected"{/if}>{$page.name}</option>
								{/if}
							{else}
								<option value="page_{$page.id}">{$page.name}</option>
							{/if}
						{else}
							<option value="page_{$page.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "page_{$page.id}")}selected="selected"{/if}>{$page.name}</option>
						{/if}
					{/foreach}
				{/if}
			</optgroup>
			<optgroup label="Modules Pages">
				{if (isset($xprt_modulepages) && !empty($xprt_modulepages))}
					{foreach from=$xprt_modulepages item=xprtmodulepage key=modulename}
						<option disabled="disabled" style="font-weight: bold;"><strong>&nbsp;{$modulename}</strong></option>
						{foreach from=$xprtmodulepage item=modulepage}
							{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
								{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
									{$arrr = $fields_value[$input.name]}
									{if $arrr|is_array}
										{$current_item = "module_{$modulepage.id}"}
										<option value="module_{$modulepage.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$modulepage.name}</option>
										{$current_item = ""}
									{else}
										<option value="module_{$modulepage.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "modulepage_{$modulepage.id}")}selected="selected"{/if}>{$modulepage.name}</option>
									{/if}
								{else}
									<option value="module_{$modulepage.id}">{$modulepage.name}</option>
								{/if}
							{else}
								<option value="module_{$modulepage.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "module_{$modulepage.id}")}selected="selected"{/if}>{$modulepage.name}</option>
							{/if}
						{/foreach}
					{/foreach}
				{/if}
			</optgroup>
			{if (isset($getxipblogcategory) && !empty($getxipblogcategory)) || (isset($getxipblogposts) && !empty($getxipblogposts))}
			<optgroup label="XipBlog Modules Pages">
				{if (isset($getxipblogcategory) && !empty($getxipblogcategory))}
					<option disabled="disabled" style="font-weight: bold;"><strong>&nbsp;Blog Category</strong></option>
					{foreach from=$getxipblogcategory item=XIPCAT}
						{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
							{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
								{$arrr = $fields_value[$input.name]}
								{if $arrr|is_array}
									{$current_item = "XIPCAT_{$XIPCAT.id}"}
									<option value="XIPCAT_{$XIPCAT.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$XIPCAT.name}</option>
									{$current_item = ""}
								{else}
									<option value="XIPCAT_{$XIPCAT.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "XIPCAT_{$XIPCAT.id}")}selected="selected"{/if}>{$XIPCAT.name}</option>
								{/if}
							{else}
								<option value="XIPCAT_{$XIPCAT.id}">{$XIPCAT.name}</option>
							{/if}
						{else}
							<option value="XIPCAT_{$XIPCAT.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "XIPCAT_{$XIPCAT.id}")}selected="selected"{/if}>{$XIPCAT.name}</option>
						{/if}
					{/foreach}
				{/if}
				{if (isset($getxipblogposts) && !empty($getxipblogposts))}
					<option disabled="disabled" style="font-weight: bold;"><strong>&nbsp;Blog Post</strong></option>
					{foreach from=$getxipblogposts item=XIPPOST}
						{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
							{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
								{$arrr = $fields_value[$input.name]}
								{if $arrr|is_array}
									{$current_item = "XIPPOST_{$XIPPOST.id}"}
									<option value="XIPPOST_{$XIPPOST.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$XIPPOST.name}</option>
									{$current_item = ""}
								{else}
									<option value="XIPPOST_{$XIPPOST.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "XIPPOST_{$XIPPOST.id}")}selected="selected"{/if}>{$XIPPOST.name}</option>
								{/if}
							{else}
								<option value="XIPPOST_{$XIPPOST.id}">{$XIPPOST.name}</option>
							{/if}
						{else}
							<option value="XIPPOST_{$XIPPOST.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "XIPPOST_{$XIPPOST.id}")}selected="selected"{/if}>{$XIPPOST.name}</option>
						{/if}
					{/foreach}
				{/if}
			</optgroup>
			{/if}
			{if isset($xprtmultishop) && !empty($xprtmultishop) && $xprtmultishop == 'enable'}
				{if isset($xprt_shoppages) && !empty($xprt_shoppages)}
					<optgroup label="MultiShop: All Shops">
						{foreach from=$xprt_shoppages item=xprtshop}
							{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
								{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
									{$arrr = $fields_value[$input.name]}
									{if $arrr|is_array}
										{$current_item = "xprtshop_{$xprtshop.id}"}
										<option value="xprtshop_{$xprtshop.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$xprtshop.name}</option>
										{$current_item = ""}
									{else}
										<option value="xprtshop_{$xprtshop.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "xprtshop_{$xprtshop.id}")}selected="selected"{/if}>{$xprtshop.name}</option>
									{/if}
								{else}
									<option value="xprtshop_{$xprtshop.id}">{$xprtshop.name}</option>
								{/if}
							{else}
								<option value="xprtshop_{$xprtshop.id}" {if isset($fields_value[$input.name]) && ($fields_value[$input.name] == "xprtshop_{$xprtshop.id}")}selected="selected"{/if}>{$xprtshop.name}</option>
							{/if}
						{/foreach}
					</optgroup>
				{/if}
			{/if}
		</select>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
{block name="label"}
    {if $input.type == 'xprticon_type'}
<div class="form-group">
<label class="control-label col-lg-3">
{$input.label}
</label>
<div class="col-lg-9 ">
<input name="{$input.name}" type="text" value="{if isset($fields_value[$input.name])}{$fields_value[$input.name]|escape:'html':'UTF-8'}{/if}" id="icon_type_{$input.name}" class="xprticon_type_cls fixed-width-lg">
<a href="#" class="toolbar_btn" data-toggle="modal" data-target="#icon_{$input.name}_id" title="Addons">Select icon</a>
</div>
</div>
<div class="bootstrap">
    <div class="modal fade in" id="icon_{$input.name}_id" tabindex="-1" aria-hidden="false" style="display: none;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="icon_popup_cls close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Select Icon</h4>
                </div>
                <div class="modal-body">
                    <ul class="icon_type_list_{$input.name}">
	                    {if (isset($icon_list) && !empty($icon_list))}
	                        {foreach $icon_list as $icon_class}
	                            <li data-icon_class="{$icon_class}"><i class="{$icon_class}"></i></li>
	                        {/foreach}
						{/if}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
	ul.icon_type_list_{$input.name} { 
		list-style:none;
		padding:0;
		overflow: hidden;
		display: block;
		text-align: center;
	 } 
	ul.icon_type_list_{$input.name}  li { 
		display: inline-block;
		border: 1px solid #eee;
		margin: 0px 10px 10px 0px;
		width: 40px;
		height: 40px;
		line-height: 44px;
		text-align: center;
		cursor: pointer;
	 } 
	ul.icon_type_list_{$input.name} li i { 
		font-size: 20px;
	 } 
</style>

<script type="text/javascript">
$("#icon_{$input.name}_id").hide();
$("ul.icon_type_list_{$input.name} li").on('click',function(){
var icc =  $(this).attr("data-icon_class");
$("#icon_type_{$input.name}").val(icc);
$(".icon_popup_cls").trigger( "click" );
$(".modal-backdrop").remove();
});
</script>
	{else}
        {if isset($input.label)}
			<label class="control-label col-lg-3{if isset($input.required) && $input.required && $input.type != 'radio'} required{/if}">
				{if isset($input.hint)}
				<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{if is_array($input.hint)}
							{foreach $input.hint as $hint}
								{if is_array($hint)}
									{$hint.text|escape:'html':'UTF-8'}
								{else}
									{$hint|escape:'html':'UTF-8'}
								{/if}
							{/foreach}
						{else}
							{$input.hint|escape:'html':'UTF-8'}
						{/if}">
				{/if}
				{$input.label}
					{if isset($input.doc)}
						<span class="doc_class">
							<a target="_blank" href="{$input.doc}">?</a>
						</span>
					{/if}
				{if isset($input.hint)}
				</span>
				{/if}
			</label>
		{/if}
    {/if}
{/block}

{block name="input_row"}
	{if $input.type == 'rowcolumntype'}
	
	<div id="element_inject_parentcontainer" class="form-group element_inject_parentcontainer"> {* start total element wrapper *}
		{* Start full visual content predefind values *}
		{if (isset($rowcolumnsettings) && !empty($rowcolumnsettings))}
			{$ai = 0}
			{foreach from=$rowcolumnsettings item=rowcolumnsetting}
			<div class="element_row_container element_row_container_{$ai}" data-row_id="{$ai}">
				<div class="row_heder_container">
					<a href="#" class="row_controll_elem row_move"><i class="icon-arrows"></i></a>
					<span class="column_controll_elem">  Row: {$ai}  </span>
					<a href="#" class="row_controll_elem row_excol pull-right"><i class="icon-caret-down"></i></a>
					<a href="#" class="row_controll_elem row_delete pull-right"><i class="icon-trash"></i></a>
					<a href="#" data-toggle="modal" data-target="#row_inner_container_popup_{$ai}" class="row_controll_elem row_setting pull-right"><i class="icon-edit"></i></a>
					<a href="#" class="row_controll_elem row_add_column pull-right"><i class="process-icon-new"></i></a>
				</div>
				<div class="row_inner_container">
					{* start row content *}
						{if (isset($rowcolumnsetting->columnvalues) && !empty($rowcolumnsetting->columnvalues))}
							{$aci = 0}
							{foreach from=$rowcolumnsetting->columnvalues item=columnvalue}
								<div class="element_column_container element_column_container_{$aci} ui-widget-content" data-row_id="{$ai}" data-column_id="{$aci}">
									<div class="column_heder_container">
										<a href="#" class="ui-widget-header column_controll_elem column_move"><i class="icon-arrows-alt"></i></a>
										<span class="column_controll_elem">  Column: {$aci}  </span>
										<a href="#" class="column_controll_elem column_excol pull-right"><i class="icon-caret-down"></i></a>
										<a href="#" class="column_controll_elem column_delete pull-right"><i class="icon-trash"></i></a>
										<a href="#" data-toggle="modal" data-target="#column_inner_container_popup_{$ai}_{$aci}" class="column_controll_elem column_setting pull-right"><i class="icon-edit"></i></a>
									</div>
									<div class="column_inner_container">
										<div class="column_inner_container_content">
											{* start edit column_inner_container_content *}
	
												{if (isset($columnvalue->eachcolumn) && !empty($columnvalue->eachcolumn))}
													{$eec = 0}
													{foreach from=$columnvalue->eachcolumn item=eachcolumnvalue}
														<div class="column_item_content column_item_content_{$ai}_{$aci}_{$eec}">
															<div class="element_title">
																<a href="#" data-toggle="modal" data-target="#column_inner_data_popup_{$ai}_{$aci}_{$eec}" class="column_items xprtmegamenu_par_level"><i class="icon-edit"></i></a>
																<span class="elementtitle"> Element: {if (isset($eachcolumnvalue->level->$id_language) && !empty($eachcolumnvalue->level->$id_language))}{$eachcolumnvalue->level->$id_language}{else}{$eec}{/if}</span>
																<a href="#" data-classname="column_item_content_{$ai}_{$aci}_{$eec}" class="xprtmegamenu_del_elm"><i class="icon-trash"></i></a>
															</div>
														<div class=" modal fade in column_inner_data_popup_{$ai}_{$aci}_{$eec}" id="column_inner_data_popup_{$ai}_{$aci}_{$eec}" data-row_id="{$ai}" data-column_id="{$aci}" data-element_id="{$eec}">
															<div class="modal-dialog modal-md">
															    <div class="modal-content">
															        <div class="modal-header xprt_drag_controll">
															            <button type="button" data-identify="{$ai}_{$aci}_{$eec}" class="icon_popup_cls close column_content_exit" data-dismiss="modal">×</button>
															            <h4 class="modal-title">Element: {$eec} Content</h4>
															        </div>
															        <div class="modal-body">
															            <div class="column_inner_data">
															            	<div class="form-group">
																            	<label class="control-label col-lg-3">
																            		Show level
																            	</label>
																            	<div class="col-lg-9">
																            		<select name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][shwlvl]" class="xprtmegamenu_fld xprt_shwlvl">
																            			<option value="hide" {if isset($eachcolumnvalue->shwlvl) && ($eachcolumnvalue->shwlvl == 'hide')}selected="selected"{/if}>Hide</option>
																            			<option value="show" {if isset($eachcolumnvalue->shwlvl) && ($eachcolumnvalue->shwlvl == 'show')}selected="selected"{/if}>Show</option>
																            		</select>
																            	</div>
															            	</div>
															            	{* start language work *}
															            	<div class="form-group xprt_shwlvl_group xprt_shwlvl_show">
															            	<label class="control-label col-lg-3">
															            		Level
															            	</label>
															            	<div class="col-lg-9">
															            	{foreach from=$languages item=language}
															            		{if $languages|count > 1}
															            		<div class="translatable-field row lang-{$language.id_lang}">
															            			<div class="col-lg-9">
															            		{/if}
															            		<input type="text" class="xprtmegamenu_level xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][level][{$language.id_lang}]" class="fixed-width-lg" value="{if (isset($eachcolumnvalue->level->$language.id_lang) && !empty($eachcolumnvalue->level->$language.id_lang))}{$eachcolumnvalue->level->$language.id_lang}{/if}">
															            		{if $languages|count > 1}
															            			</div>
															            			<div class="col-lg-3">
															            				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
															            					{$language.iso_code}
															            					<span class="caret"></span>
															            				</button>
															            				<ul class="dropdown-menu">
															            					{foreach from=$languages item=language}
															            					<li>
															            						<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
															            					</li>
															            					{/foreach}
															            				</ul>
															            			</div>
															            		</div>
															            		{/if}
															            	{/foreach}
															            	</div>
															            	</div>

															            	<div class="form-group xprt_shwlvl_group xprt_shwlvl_show">
															            	<label class="control-label col-lg-3">
															            		Badge
															            	</label>
															            	<div class="col-lg-9">
															            		{foreach from=$languages item=language}
															            			{if $languages|count > 1}
															            			<div class="translatable-field row lang-{$language.id_lang}">
															            				<div class="col-lg-9">
															            			{/if}
															            			<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][badge][{$language.id_lang}]" class="fixed-width-lg" value="{if (isset($eachcolumnvalue->badge->$language.id_lang) && !empty($eachcolumnvalue->badge->$language.id_lang))}{$eachcolumnvalue->badge->$language.id_lang}{/if}">
															            			{if $languages|count > 1}
															            				</div>
															            				<div class="col-lg-3">
															            					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
															            						{$language.iso_code}
															            						<span class="caret"></span>
															            					</button>
															            					<ul class="dropdown-menu">
															            						{foreach from=$languages item=language}
															            						<li>
															            							<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
															            						</li>
															            						{/foreach}
															            					</ul>
															            				</div>
															            			</div>
															            			{/if}
															            		{/foreach}
															            	</div>
															            	</div>

															            	<div class="form-group xprt_shwlvl_group xprt_shwlvl_show">
															            	<label class="control-label col-lg-3">
															            		Level Link
															            	</label>
															            	<div class="col-lg-9">
															            		{foreach from=$languages item=language}
															            			{if $languages|count > 1}
															            			<div class="translatable-field row lang-{$language.id_lang}">
															            				<div class="col-lg-9">
															            			{/if}
															            			<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][lvllink][{$language.id_lang}]" class="fixed-width-lg" value="{if (isset($eachcolumnvalue->lvllink->$language.id_lang) && !empty($eachcolumnvalue->lvllink->$language.id_lang))}{$eachcolumnvalue->lvllink->$language.id_lang}{/if}">
															            			{if $languages|count > 1}
															            				</div>
															            				<div class="col-lg-3">
															            					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
															            						{$language.iso_code}
															            						<span class="caret"></span>
															            					</button>
															            					<ul class="dropdown-menu">
															            						{foreach from=$languages item=language}
															            						<li>
															            							<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
															            						</li>
															            						{/foreach}
															            					</ul>
															            				</div>
															            			</div>
															            			{/if}
															            		{/foreach}
															            	</div>
															            	</div>
{* end language work *}
															            	<div class="form-group">
															            		<label class="control-label col-lg-3">
															            			Type
															            		</label>
															            		<div class="col-lg-9">
															            			<select name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][type]" class="xprt_type">
															            				<option value="none" {if isset($eachcolumnvalue->type) && ($eachcolumnvalue->type == 'none')}selected="selected"{/if}>None</option>
															            				<option value="external" {if isset($eachcolumnvalue->type) && ($eachcolumnvalue->type == 'external')}selected="selected"{/if}>External Link</option>
															            				<option value="system" {if isset($eachcolumnvalue->type) && ($eachcolumnvalue->type == 'system')}selected="selected"{/if}>System Link</option>
															            				<option value="custom" {if isset($eachcolumnvalue->type) && ($eachcolumnvalue->type == 'custom')}selected="selected"{/if}>Custom Html</option>
															            				<option value="hook" {if isset($eachcolumnvalue->type) && ($eachcolumnvalue->type == 'hook')}selected="selected"{/if}>Hook</option>
															            				<option value="product" {if isset($eachcolumnvalue->type) && ($eachcolumnvalue->type == 'product')}selected="selected"{/if}>Product</option>
															            				<option value="image" {if isset($eachcolumnvalue->type) && ($eachcolumnvalue->type == 'image')}selected="selected"{/if}>Image</option>
															            				<option value="video" {if isset($eachcolumnvalue->type) && ($eachcolumnvalue->type == 'video')}selected="selected"{/if}>Video</option>
															            			</select>
															            		</div>
															            	</div>
															            	{* start language work for external *}
																			<div class="form-group xprt_type_group xprt_type_external">
																				<label class="control-label col-lg-3">
																				External Level
																				</label>
																				<div class="col-lg-9">
																					{foreach from=$languages item=language}
																					{if $languages|count > 1}
																					<div class="translatable-field row lang-{$language.id_lang}">
																						<div class="col-lg-9">
																					{/if}
																					<input type="text" value="{if isset($eachcolumnvalue->xtrllvl->$language.id_lang)}{$eachcolumnvalue->xtrllvl->$language.id_lang}{/if}" name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][xtrllvl][{$language.id_lang}]" class="">
																					{if $languages|count > 1}
																						</div>
																						<div class="col-lg-3">
																							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
																								{$language.iso_code}
																								<span class="caret"></span>
																							</button>
																							<ul class="dropdown-menu">
																								{foreach from=$languages item=language}
																								<li>
																									<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
																								</li>
																								{/foreach}
																							</ul>
																						</div>
																					</div>
																					{/if}
																					{/foreach}
																				</div>
																			</div>
																			<div class="form-group xprt_type_group xprt_type_external">
																				<label class="control-label col-lg-3">
																				External URL
																				</label>
																				<div class="col-lg-9">
																					{foreach from=$languages item=language}
																					{if $languages|count > 1}
																					<div class="translatable-field row lang-{$language.id_lang}">
																						<div class="col-lg-9">
																					{/if}
																					<input type="text" value="{if isset($eachcolumnvalue->xtrlurl->$language.id_lang)}{$eachcolumnvalue->xtrlurl->$language.id_lang}{/if}" name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][xtrlurl][{$language.id_lang}]" class="">
																					{if $languages|count > 1}
																						</div>
																						<div class="col-lg-3">
																							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
																								{$language.iso_code}
																								<span class="caret"></span>
																							</button>
																							<ul class="dropdown-menu">
																								{foreach from=$languages item=language}
																								<li>
																									<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
																								</li>
																								{/foreach}
																							</ul>
																						</div>
																					</div>
																					{/if}
																					{/foreach}
																				</div>
																			</div>
																			{* end language work for external *}
																			{* start link options group *}
															            	<div class="form-group xprt_type_group xprt_type_system">
															            	<label class="control-label col-lg-3">
															            		Link
															            	</label>
															            	<div class="col-lg-9">

															            		<select name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][link][]" class="xprtmultipleclass" multiple="multiple">
															            		<optgroup label="Categories">
															            			{if (isset($xprt_categories) && !empty($xprt_categories))}
															            				{foreach from=$xprt_categories item=cate}
															            					{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
															            						{$arrr = $eachcolumnvalue->link}
															            						{if $arrr|is_array}
															            							{$current_item = "cat_{$cate.id}"}
															            							<option value="cat_{$cate.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$cate.name}</option>
															            							{$current_item = ""}
															            						{else}
															            							<option value="cat_{$cate.id}" {if ((isset($eachcolumnvalue->link)) && ($eachcolumnvalue->link == "cat_{$cate.id}"))}selected="selected"{/if}>{$cate.name}</option>
															            						{/if}
															            					{else}
															            						<option value="cat_{$cate.id}">{$cate.name}</option>
															            					{/if}
															            				{/foreach}
															            			{/if}
															            		</optgroup>
															            		<optgroup label="Manufacturers">
															            			{if (isset($xprt_manufacturers) && !empty($xprt_manufacturers))}
															            			{foreach from=$xprt_manufacturers item=man}
															            				{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
															            					{$arrr = $eachcolumnvalue->link}
															            					{if $arrr|is_array}
															            						{$current_item = "man_{$man.id_manufacturer}"}
															            						<option value="man_{$man.id_manufacturer}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$man.name}</option>
															            						{$current_item = ""}
															            					{else}
															            						<option value="man_{$man.id_manufacturer}" {if ((isset($eachcolumnvalue->link)) && ($eachcolumnvalue->link == "man_{$man.id_manufacturer}"))}selected="selected"{/if}>{$man.name}</option>
															            					{/if}
															            				{else}
															            					<option value="man_{$man.id_manufacturer}">{$man.name}</option>
															            				{/if}
															            			{/foreach}
															            			{/if}
															            		</optgroup>
															            		<optgroup label="Suppliers">
															            			{if (isset($xprt_suppliers) && !empty($xprt_suppliers))}
															            			{foreach from=$xprt_suppliers item=sup}
															            				{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
															            					{$arrr = $eachcolumnvalue->link}
															            					{if $arrr|is_array}
															            						{$current_item = "sup_{$sup.id_supplier}"}
															            						<option value="sup_{$sup.id_supplier}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$sup.name}</option>
															            						{$current_item = ""}
															            					{else}
															            						<option value="sup_{$sup.id_supplier}" {if ((isset($eachcolumnvalue->link)) && ($eachcolumnvalue->link == "sup_{$sup.id_supplier}"))}selected="selected"{/if}>{$sup.name}</option>
															            					{/if}
															            				{else}
															            					<option value="sup_{$sup.id_supplier}">{$sup.name}</option>
															            				{/if}
															            			{/foreach}
															            			{/if}
															            		</optgroup>
															            		<optgroup label="CMS Categories">
															            			{if (isset($xprt_cmscategories) && !empty($xprt_cmscategories))}
															            			{foreach from=$xprt_cmscategories item=cmscat}
															            				{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
															            					{$arrr = $eachcolumnvalue->link}
															            					{if $arrr|is_array}
															            						{$current_item = "{$cmscat.id}"}
															            						<option value="{$cmscat.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$cmscat.name}</option>
															            						{$current_item = ""}
															            					{else}
															            						<option value="{$cmscat.id}" {if ((isset($eachcolumnvalue->link)) && ($eachcolumnvalue->link == "{$cmscat.id}"))}selected="selected"{/if}>{$cmscat.name}</option>
															            					{/if}
															            				{else}
															            					<option value="{$cmscat.id}">{$cmscat.name}</option>
															            				{/if}
															            			{/foreach}
															            			{/if}
															            		</optgroup>
															            		<optgroup label="Products">
															            			{if (isset($xprt_products) && !empty($xprt_products))}
															            			{foreach from=$xprt_products item=prd}
															            				{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
															            					{$arrr = $eachcolumnvalue->link}
															            					{if $arrr|is_array}
															            						{$current_item = "prd_{$prd.id}"}
															            						<option value="prd_{$prd.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$prd.name}</option>
															            						{$current_item = ""}
															            					{else}
															            						<option value="prd_{$prd.id}" {if ((isset($eachcolumnvalue->link)) && ($eachcolumnvalue->link == "prd_{$prd.id}"))}selected="selected"{/if}>{$prd.name}</option>
															            					{/if}
															            				{else}
															            					<option value="prd_{$prd.id}">{$prd.name}</option>
															            				{/if}
															            			{/foreach}
															            			{/if}
															            		</optgroup>
															            		<optgroup label="Pages">
															            			{if (isset($xprt_pages) && !empty($xprt_pages))}
															            				{foreach from=$xprt_pages item=page}
															            					{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
															            						{$arrr = $eachcolumnvalue->link}
															            						{if $arrr|is_array}
															            							{$current_item = "page_{$page.id}"}
															            							<option value="page_{$page.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$page.name}</option>
															            							{$current_item = ""}
															            						{else}
															            							<option value="page_{$page.id}" {if ((isset($eachcolumnvalue->link)) && ($eachcolumnvalue->link == "page_{$page.id}"))}selected="selected"{/if}>{$page.name}</option>
															            						{/if}
															            					{else}
															            						<option value="page_{$page.id}">{$page.name}</option>
															            					{/if}
															            				{/foreach}
															            			{/if}
															            		</optgroup>
															            		<optgroup label="Modules Pages">
															            			{if (isset($xprt_modulepages) && !empty($xprt_modulepages))}
															            				{foreach from=$xprt_modulepages item=xprtmodulepage key=modulename}
															            					<option disabled="disabled" style="font-weight: bold;"><strong>&nbsp;{$modulename}</strong></option>
															            					{foreach from=$xprtmodulepage item=modulepage}
															            							{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
															            								{$arrr = $eachcolumnvalue->link}
															            								{if $arrr|is_array}
															            									{$current_item = "module_{$modulepage.id}"}
															            									<option value="module_{$modulepage.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$modulepage.name}</option>
															            									{$current_item = ""}
															            								{else}
															            									<option value="module_{$modulepage.id}" {if isset($eachcolumnvalue->link) && ($eachcolumnvalue->link == "modulepage_{$modulepage.id}")}selected="selected"{/if}>{$modulepage.name}</option>
															            								{/if}
															            							{else}
															            								<option value="module_{$modulepage.id}">{$modulepage.name}</option>
															            							{/if}
															            					{/foreach}
															            				{/foreach}
															            			{/if}
															            		</optgroup>
															            		{if (isset($getxipblogcategory) && !empty($getxipblogcategory)) || (isset($getxipblogposts) && !empty($getxipblogposts))}
															            		<optgroup label="XipBlog Modules Pages">
															            			{if (isset($getxipblogcategory) && !empty($getxipblogcategory))}
															            				<option disabled="disabled" style="font-weight: bold;"><strong>&nbsp;Blog Category</strong></option>
															            				{foreach from=$getxipblogcategory item=XIPCAT}
															            						{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
															            							{$arrr = $eachcolumnvalue->link}
															            							{if $arrr|is_array}
															            								{$current_item = "XIPCAT_{$XIPCAT.id}"}
															            								<option value="XIPCAT_{$XIPCAT.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$XIPCAT.name}</option>
															            								{$current_item = ""}
															            							{else}
															            								<option value="XIPCAT_{$XIPCAT.id}" {if isset($eachcolumnvalue->link) && ($eachcolumnvalue->link == "XIPCAT_{$XIPCAT.id}")}selected="selected"{/if}>{$XIPCAT.name}</option>
															            							{/if}
															            						{else}
															            							<option value="XIPCAT_{$XIPCAT.id}">{$XIPCAT.name}</option>
															            						{/if}
															            				{/foreach}
															            			{/if}
															            			{if (isset($getxipblogposts) && !empty($getxipblogposts))}
															            				<option disabled="disabled" style="font-weight: bold;"><strong>&nbsp;Blog Post</strong></option>
															            				{foreach from=$getxipblogposts item=XIPPOST}
															            						{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
															            							{$arrr = $eachcolumnvalue->link}
															            							{if $arrr|is_array}
															            								{$current_item = "XIPPOST_{$XIPPOST.id}"}
															            								<option value="XIPPOST_{$XIPPOST.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$XIPPOST.name}</option>
															            								{$current_item = ""}
															            							{else}
															            								<option value="XIPPOST_{$XIPPOST.id}" {if isset($eachcolumnvalue->link) && ($eachcolumnvalue->link == "XIPPOST_{$XIPPOST.id}")}selected="selected"{/if}>{$XIPPOST.name}</option>
															            							{/if}
															            						{else}
															            							<option value="XIPPOST_{$XIPPOST.id}">{$XIPPOST.name}</option>
															            						{/if}
															            				{/foreach}
															            			{/if}
															            		</optgroup>
															            		{/if}
															            		{if isset($xprtmultishop) && !empty($xprtmultishop) && $xprtmultishop == 'enable'}
																            		{if isset($xprt_shoppages) && !empty($xprt_shoppages)}
																            			<optgroup label="MultiShop: All Shops">
																            				{foreach from=$xprt_shoppages item=xprtshop}
																            					{if isset($input.is_multiple) && !empty($input.is_multiple) && $input.is_multiple == true}
																            						{if isset($eachcolumnvalue->link) && !empty($eachcolumnvalue->link)}
																            							{$arrr = $eachcolumnvalue->link}
																            							{if $arrr|is_array}
																            								{$current_item = "xprtshop_{$xprtshop.id}"}
																            								<option value="xprtshop_{$xprtshop.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$xprtshop.name}</option>
																            								{$current_item = ""}
																            							{else}
																            								<option value="xprtshop_{$xprtshop.id}" {if isset($eachcolumnvalue->link) && ($eachcolumnvalue->link == "xprtshop_{$xprtshop.id}")}selected="selected"{/if}>{$xprtshop.name}</option>
																            							{/if}
																            						{else}
																            							<option value="xprtshop_{$xprtshop.id}">{$xprtshop.name}</option>
																            						{/if}
																            					{else}
																            						<option value="xprtshop_{$xprtshop.id}" {if isset($eachcolumnvalue->link) && ($eachcolumnvalue->link == "xprtshop_{$xprtshop.id}")}selected="selected"{/if}>{$xprtshop.name}</option>
																            					{/if}
																            				{/foreach}
																            			</optgroup>
																            		{/if}
															            		{/if}
															            		</select>
															            	</div>
															            	</div>
{* end link options group *}
															            	<div class="form-group xprt_type_group xprt_type_hook">
															            		<label class="control-label col-lg-3">
															            			Hook
															            		</label>
															            		<div class="col-lg-9">
															            			<select name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][hook]" class="">
																					{if (isset($xprt_hooks) && !empty($xprt_hooks))}
																						{foreach from=$xprt_hooks item=hook}
																							<option value="{$hook.id}" {if isset($eachcolumnvalue->hook) && ($eachcolumnvalue->hook == $hook.name)}selected="selected"{/if}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$hook.name}</option>
																						{/foreach}
																					{/if}
															            			</select>
															            		</div>
															            	</div>

															            	<div class="form-group xprt_type_group xprt_type_custom">
															            		<label class="control-label col-lg-3">
															            			Custom
															            		</label>
															            		<div class="col-lg-9">
															            		{foreach from=$languages item=language}
															            					{if $languages|count > 1}
															            					<div class="translatable-field row lang-{$language.id_lang}">
															            						<div class="col-lg-10">
															            					{/if}
															            			 <textarea name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][customhtml][{$language.id_lang}]" class="" cols="30" rows="10">{if isset($eachcolumnvalue->customhtml->$language.id_lang)}{$eachcolumnvalue->customhtml->$language.id_lang}{/if}</textarea>
															            			 {if $languages|count > 1}
															            						</div>
															            						<div class="col-lg-2">
															            							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
															            								{$language.iso_code}
															            								<span class="caret"></span>
															            							</button>
															            							<ul class="dropdown-menu">
															            								{foreach from=$languages item=language}
															            								<li>
															            									<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
															            								</li>
															            								{/foreach}
															            							</ul>
															            						</div>
															            					</div>
															            					{/if}
															            				{/foreach}
															            		</div>
															            	</div>
{* start products block *}
															            	<div class="form-group xprt_type_group xprt_type_product">
															            		<label class="control-label col-lg-3">
															            			Products
															            		</label>
															            		<div class="col-lg-9">
															            			<select name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][products][]" class="xprtmultipleclass" multiple="multiple">
															            			{if (isset($xprt_products) && !empty($xprt_products))}
															            				{foreach from=$xprt_products item=prd}
															            					{if isset($eachcolumnvalue->products) && !empty($eachcolumnvalue->products)}
															            						{$arrr = $eachcolumnvalue->products}
															            						{if $arrr|is_array}
															            							{$current_item = "prd_{$prd.id}"}
															            							<option value="prd_{$prd.id}" {if in_array($current_item,$arrr)}selected="selected"{/if}>{$prd.name}</option>
															            							{$current_item = ""}
															            						{else}
															            							<option value="prd_{$prd.id}" {if ((isset($eachcolumnvalue->products)) && ($eachcolumnvalue->products == "prd_{$prd.id}"))}selected="selected"{/if}>{$prd.name}</option>
															            						{/if}
															            					{else}
															            						<option value="prd_{$prd.id}">{$prd.name}</option>
															            					{/if}
															            				{/foreach}
															            				{/if}
															            			</select>
															            		</div>
															            	</div>
{* end products block *}
															            	<div class="form-group xprt_type_group xprt_type_image">
															            		<label class="control-label col-lg-3">
															            			Image
															            		</label>
																				<div class="col-lg-9 xprtmegamenu_row_upload_parent">
																					<input type="file" name="xprtmegamenu_row_temp" class="xprtmegamenu_row_temp btn btn-default">
																					<input type="hidden" name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][conimage]" class="xprtmegamenu_row_val" value="{if isset($eachcolumnvalue->conimage)}{$eachcolumnvalue->conimage}{/if}">
																					<div class="xprtmegamenu_row_show">
																						{if (isset($eachcolumnvalue->conimage) && !empty($eachcolumnvalue->conimage))}
																							<img class="xprtmegamenu_row_img_show" src="{$xprt_imagelink}{$eachcolumnvalue->conimage}" width="auto" height="70">
																						{/if}
																					</div>
																					<div name="xprtmegamenu_row_upload" class="xprtmegamenu_row_upload btn btn-default m_top_5"><i class="icon-folder-open"></i> Upload</div>
																				</div>
															            	</div>

															            	<div class="form-group xprt_type_group xprt_type_image xprt_type_video">
															            		<label class="control-label col-lg-3">
															            			Height
															            		</label>
															            		<div class="col-lg-9">
															            			<input type="text" name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][cheight]" class="" value="{if isset($eachcolumnvalue->height)}{$eachcolumnvalue->height}{/if}">
															            		</div>
															            	</div>

															            	<div class="form-group xprt_type_group xprt_type_image xprt_type_video">
															            		<label class="control-label col-lg-3">
															            			Width
															            		</label>
															            		<div class="col-lg-9">
															            			<input type="text" name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][cwidth]" class="" value="{if isset($eachcolumnvalue->width)}{$eachcolumnvalue->width}{/if}">
															            		</div>
															            	</div>

															            	<div class="form-group xprt_type_group xprt_type_video">
															            		<label class="control-label col-lg-3">
															            			Video
															            		</label>
															            		<div class="col-lg-9">
															            			<input type="text" name="xprtmegamenu[{$ai}][{$aci}][eachcolumn][{$eec}][video]" class="" value="{if isset($eachcolumnvalue->video)}{$eachcolumnvalue->video}{/if}">
															            		</div>
															            	</div>
															            </div>
															        </div>
															        <div class="modal-footer">
															        	<div class="col-sm-3 pull-right">
															            	<button type="button" data-identify="{$ai}_{$aci}_{$eec}" class="btn btn-primary btn-block btn-sm icon_popup_cls column_content_exit" data-dismiss="modal">Submit</button>
															        	</div>
															        </div>
															    </div>
															</div>
														</div>
														</div>
														{$eec = $eec+1}
													{/foreach}
												{/if}
											{* end edit column_inner_container_content *}
										</div>
										<div class="add_column_elem_parent">
											<a href="#" class="add_column_elem" data-row_id="{$ai}" data-column_id="{$aci}"><i class="process-icon-new"></i> Add Element </a>
										</div>
									</div>
									<div class="column_inner_container_popup_container xprt_drag_elm modal fade in" data-row_id="{$ai}" data-column_id="{$aci}" id="column_inner_container_popup_{$ai}_{$aci}">
										<div class="modal-dialog modal-md">
										    <div class="modal-content">
										        <div class="modal-header xprt_drag_controll">
										            <button type="button" class="icon_popup_cls close" data-dismiss="modal">×</button>
										            <h4 class="modal-title">Column: {$aci} Settings</h4>
										        </div>
										        <div class="modal-body">
										            <div class="column_inner_container_popup">

						            					<div class="form-group">
						            						<label class="control-label col-lg-3">
						            							Width
						            						</label>
						            						<div class="col-lg-9">
																<select class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][width]">
																<option value="12" {if isset($columnvalue->width) && ($columnvalue->width == '12')}selected="selected"{/if}>12 column</option>
																<option value="11" {if isset($columnvalue->width) && ($columnvalue->width == '11')}selected="selected"{/if}>11 column</option>
																<option value="10" {if isset($columnvalue->width) && ($columnvalue->width == '10')}selected="selected"{/if}>10 column</option>
																<option value="9" {if isset($columnvalue->width) && ($columnvalue->width == '9')}selected="selected"{/if}>9 column</option>
																<option value="8" {if isset($columnvalue->width) && ($columnvalue->width == '8')}selected="selected"{/if}>8 column</option>
																<option value="7" {if isset($columnvalue->width) && ($columnvalue->width == '7')}selected="selected"{/if}>7 column</option>
																<option value="6" {if isset($columnvalue->width) && ($columnvalue->width == '6')}selected="selected"{/if}>6 column</option>
																<option value="5" {if isset($columnvalue->width) && ($columnvalue->width == '5')}selected="selected"{/if}>5 column</option>
																<option value="4" {if isset($columnvalue->width) && ($columnvalue->width == '4')}selected="selected"{/if}>4 column</option>
																<option value="3" {if isset($columnvalue->width) && ($columnvalue->width == '3')}selected="selected"{/if}>3 column</option>
																<option value="2" {if isset($columnvalue->width) && ($columnvalue->width == '2')}selected="selected"{/if}>2 column</option>
																<option value="1" {if isset($columnvalue->width) && ($columnvalue->width == '1')}selected="selected"{/if}>1 column</option>
																</select>
						            							<div class="help-block-container">
						            								<p class="help-block">
						            									Please Enter Width in pixel Value.(200px;)
						            								</p>
						            							</div>
						            						</div>
						            					</div>

						            					<div class="form-group">
						            						<label class="control-label col-lg-3">
						            							Padding
						            						</label>
						            						<div class="col-lg-9">
						            							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][padding]" class="fixed-width-lg" value="{if isset($columnvalue->padding)}{$columnvalue->padding}{/if}">
						            							<div class="help-block-container">
						            								<p class="help-block">
						            									Please Enter Padding (0px 0px 0px 0px)(top right bottom left)
						            								</p>
						            							</div>
						            						</div>
						            					</div>

						            					<div class="form-group">
						            						<label class="control-label col-lg-3">
						            							Column Background
						            						</label>
						            						<div class="col-lg-9">
						            							<select class="xprt_clm_bg xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][bgtype]">
						            								<option value="none" {if isset($columnvalue->bgtype) && ($columnvalue->bgtype == 'none')}selected="selected"{/if}>None</option>
						            								<option value="image" {if isset($columnvalue->bgtype) && ($columnvalue->bgtype == 'image')}selected="selected"{/if}>Image</option>
						            							</select>
						            							<div class="help-block-container">
						            								<p class="help-block">
						            									Please Select Column Background Type.
						            								</p>
						            							</div>
						            						</div>
						            					</div>

						            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
						            						<label class="control-label col-lg-3">
						            							Upload Background Image
						            						</label>
						            						<div class="col-lg-9 xprtmegamenu_row_upload_parent">
																<input type="file" name="xprtmegamenu_row_temp" class="xprtmegamenu_row_temp btn btn-default">
																<input type="hidden" name="xprtmegamenu[{$ai}][{$aci}][colimage]" class="xprtmegamenu_row_val" value="{if isset($columnvalue->colimage)}{$columnvalue->colimage}{/if}">
																<div class="xprtmegamenu_row_show">
																{if (isset($columnvalue->colimage) && !empty($columnvalue->colimage))}
																	<img class="xprtmegamenu_row_img_show" src="{$xprt_imagelink}{$columnvalue->colimage}" width="auto" height="70">
																{/if}
																</div>
																<div name="xprtmegamenu_row_upload" class="xprtmegamenu_row_upload btn btn-default m_top_5"><i class="icon-folder-open"></i> Upload</div>
						            						</div>
						            					</div>

						            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
						            						<label class="control-label col-lg-3">
						            							Background Reapet
						            						</label>
						            						<div class="col-lg-9">
						            							<select class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][bgrepeat]">
						            								<option value="repeat" {if isset($columnvalue->bgrepeat) && ($columnvalue->bgrepeat == 'repeat')}selected="selected"{/if}>Repeat</option>
						            								<option value="no-repeat" {if isset($columnvalue->bgrepeat) && ($columnvalue->bgrepeat == 'no-repeat')}selected="selected"{/if}>No repeat</option>
						            								<option value="repeat-x" {if isset($columnvalue->bgrepeat) && ($columnvalue->bgrepeat == 'repeat-x')}selected="selected"{/if}>Repeat-X</option>
						            								<option value="repeat-y" {if isset($columnvalue->bgrepeat) && ($columnvalue->bgrepeat == 'repeat-y')}selected="selected"{/if}>Repeat-Y</option>
						            							</select>
						            							<div class="help-block-container">
						            								<p class="help-block">
						            									Select Column background repeat/no-repeat.
						            								</p>
						            							</div>
						            						</div>
						            					</div>

						            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
						            						<label class="control-label col-lg-3">
						            							Background Position
						            						</label>
						            						<div class="col-lg-9">
						            							<select class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][bgpos]">
						            								<option value="left top" {if isset($columnvalue->bgpos) && ($columnvalue->bgpos == 'left top')}selected="selected"{/if}>left top</option>
						            								<option value="left center" {if isset($columnvalue->bgpos) && ($columnvalue->bgpos == 'left center')}selected="selected"{/if}>left center</option>
						            								<option value="left bottom" {if isset($columnvalue->bgpos) && ($columnvalue->bgpos == 'left bottom')}selected="selected"{/if}>left bottom</option>
						            								<option value="right top" {if isset($columnvalue->bgpos) && ($columnvalue->bgpos == 'right top')}selected="selected"{/if}>right top</option>
						            								<option value="right center" {if isset($columnvalue->bgpos) && ($columnvalue->bgpos == 'right center')}selected="selected"{/if}>right center</option>
						            								<option value="right bottom" {if isset($columnvalue->bgpos) && ($columnvalue->bgpos == 'right bottom')}selected="selected"{/if}>right bottom</option>
						            								<option value="center top" {if isset($columnvalue->bgpos) && ($columnvalue->bgpos == 'center top')}selected="selected"{/if}>center top</option>
						            								<option value="center center" {if isset($columnvalue->bgpos) && ($columnvalue->bgpos == 'center center')}selected="selected"{/if}>center center</option>
						            								<option value="center bottom" {if isset($columnvalue->bgpos) && ($columnvalue->bgpos == 'center bottom')}selected="selected"{/if}>center bottom</option>
						            							</select>
						            							<div class="help-block-container">
						            								<p class="help-block">
						            									Select Column background position.
						            								</p>
						            							</div>
						            						</div>
						            					</div>

						            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
						            						<label class="control-label col-lg-3">
						            							Background Size
						            						</label>
						            						<div class="col-lg-9">
						            							<select class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][bgsize]">
						            								<option value="cover" {if isset($columnvalue->bgsize) && ($columnvalue->bgsize == 'cover')}selected="selected"{/if}>Cover</option>
						            								<option value="contain" {if isset($columnvalue->bgsize) && ($columnvalue->bgsize == 'contain')}selected="selected"{/if}>Contain</option>
						            								<option value="initial" {if isset($columnvalue->bgsize) && ($columnvalue->bgsize == 'initial')}selected="selected"{/if}>initial</option>
						            								<option value="100% 100%" {if isset($columnvalue->bgsize) && ($columnvalue->bgsize == '100% 100%')}selected="selected"{/if}>100% 100%</option>
						            							</select>
						            							<div class="help-block-container">
						            								<p class="help-block">
						            									Select Column background size.
						            								</p>
						            							</div>
						            						</div>
						            					</div>

						            					<div class="form-group xprt_clm_bg_group xprt_clm_bg_image">
						            						<label class="control-label col-lg-3">
						            							Background Attachment
						            						</label>
						            						<div class="col-lg-9">
						            							<select class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][bgattach]">
						            								<option value="scroll" {if isset($columnvalue->bgattach) && ($columnvalue->bgattach == 'scroll')}selected="selected"{/if}>scroll</option>
						            								<option value="fixed" {if isset($columnvalue->bgattach) && ($columnvalue->bgattach == 'fixed')}selected="selected"{/if}>fixed</option>
						            								<option value="local" {if isset($columnvalue->bgattach) && ($columnvalue->bgattach == 'local')}selected="selected"{/if}>local</option>
						            							</select>
						            							<div class="help-block-container">
						            								<p class="help-block">
						            									Select Column background attachment.
						            								</p>
						            							</div>
						            						</div>
						            					</div>

						            					<div class="form-group">
						            						<label class="control-label col-lg-3">
						            							Custom Class
						            						</label>
						            						<div class="col-lg-9">
						            							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][{$aci}][customcls]" class="fixed-width-lg" value="{if isset($columnvalue->customcls)}{$columnvalue->customcls}{/if}">
						            							<div class="help-block-container">
						            								<p class="help-block">
								    									Please Enter a Custom Class.
								    								</p>
						            							</div>
						            						</div>
						            					</div>

										            </div>
										        </div>
										        <div class="modal-footer">
										        	<div class="col-sm-3 pull-right">
										            	<button type="button" class="btn btn-primary btn-block btn-sm icon_popup_cls " data-dismiss="modal">Submit</button>
										        	</div>
										        </div>
										    </div>
										</div>
									</div>
								</div>
							{$aci = $aci+1}
							{/foreach}
						{/if}
					{* End row content *}
				</div>
				<div class="row_inner_container_popup_container xprt_drag_elm modal fade in" data-row_id="{$ai}" id="row_inner_container_popup_{$ai}" style="display: none;">
					<div class="modal-dialog modal-md">
					    <div class="modal-content">
					        <div class="modal-header xprt_drag_controll">
					            <button type="button" class="icon_popup_cls close" data-dismiss="modal">×</button>
					            <h4 class="modal-title">Row: {$ai} Settings</h4>
					        </div>
					        <div class="modal-body">
					            <div class="row_inner_container_popup">
			    					<div class="form-group">
			    						<label class="control-label col-lg-3">
			    							Row Padding
			    						</label>
			    						<div class="col-lg-9">
			    							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][settings][padding]" class="fixed-width-lg" value="{if isset($rowcolumnsetting->settings->padding)}{$rowcolumnsetting->settings->padding}{/if}">
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Please Enter Row Padding (0px 0px 0px 0px) (top right bottom left)
			    								</p>
			    							</div>
			    						</div>
			    					</div>

			    					<div class="form-group">
			    						<label class="control-label col-lg-3">
			    							Row Border
			    						</label>
			    						<div class="col-lg-9">
			    							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][settings][border]" class="fixed-width-lg" value="{if isset($rowcolumnsetting->settings->border)}{$rowcolumnsetting->settings->border}{/if}">
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Please Enter Row Border Size In Pixel
			    								</p>
			    							</div>
			    						</div>
			    					</div>

			    					<div class="form-group">
			    						<label class="control-label col-lg-3">
			    							Row Border Color
			    						</label>
			    						<div class="col-lg-9">
			    								<input type="color" data-hex="true" class="fixed-width-lg color mColorPickerInput" name="xprtmegamenu[{$ai}][settings][borderclr]" value="{if isset($rowcolumnsetting->settings->borderclr)}{$rowcolumnsetting->settings->borderclr}{/if}"/>	
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Please Enter Row Border Color
			    								</p>
			    							</div>
			    						</div>
			    					</div>

			    					<div class="form-group">
			    						<label class="control-label col-lg-3">
			    							Row Background
			    						</label>
			    						<div class="col-lg-9">
			    							<select class="row_bg xprtmegamenu_fld" name="xprtmegamenu[{$ai}][settings][bgtype]">
			    								<option value="none" {if isset($rowcolumnsetting->settings->bgtype) && ($rowcolumnsetting->settings->bgtype == 'none')}selected="selected"{/if}>None</option>
			    								<option value="color" {if isset($rowcolumnsetting->settings->bgtype) && ($rowcolumnsetting->settings->bgtype == 'color')}selected="selected"{/if}>Color</option>
			    								<option value="image" {if isset($rowcolumnsetting->settings->bgtype) && ($rowcolumnsetting->settings->bgtype == 'image')}selected="selected"{/if}>Image</option>
			    							</select>
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Please Select Row Background Type.
			    								</p>
			    							</div>
			    						</div>
			    					</div>

			    					<div class="form-group row_bg_group row_bg_color">
			    						<label class="control-label col-lg-3">
			    							Background Color
			    						</label>
			    						<div class="col-lg-9">
			    							<input type="color" data-hex="true" class="fixed-width-lg color mColorPickerInput" name="xprtmegamenu[{$ai}][settings][bgclr]" class="fixed-width-lg" value="{if isset($rowcolumnsetting->settings->bgclr)}{$rowcolumnsetting->settings->bgclr}{/if}">
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Please Select Row Background Color.
			    								</p>
			    							</div>
			    						</div>
			    					</div>

			    					<div class="form-group row_bg_group row_bg_image">
			    						<label class="control-label col-lg-3">
			    							Upload Background Image
			    						</label>
	            						<div class="col-lg-9 xprtmegamenu_row_upload_parent">
											<input type="file" name="xprtmegamenu_row_temp" class="xprtmegamenu_row_temp btn btn-default">
											<input type="hidden" name="xprtmegamenu[{$ai}][settings][bgimg]" class="xprtmegamenu_row_val" value="{if isset($rowcolumnsetting->settings->bgimg)}{$rowcolumnsetting->settings->bgimg}{/if}">
											<div class="xprtmegamenu_row_show">
												{if (isset($rowcolumnsetting->settings->bgimg) && !empty($rowcolumnsetting->settings->bgimg))}
													<img class="xprtmegamenu_row_img_show" src="{$xprt_imagelink}{$rowcolumnsetting->settings->bgimg}" width="auto" height="70">
												{/if}
											</div>
											<div name="xprtmegamenu_row_upload" class="xprtmegamenu_row_upload btn btn-default m_top_5"><i class="icon-folder-open"></i> Upload</div>
	            						</div>
			    					</div>

			    					<div class="form-group row_bg_group row_bg_image">
			    						<label class="control-label col-lg-3">
			    							Background Reapet
			    						</label>
			    						<div class="col-lg-9">
			    							<select class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][settings][bgrepeat]">
			    								<option value="repeat" {if isset($rowcolumnsetting->settings->bgrepeat) && ($rowcolumnsetting->settings->bgrepeat == 'repeat')}selected="selected"{/if}>Repeat</option>
			    								<option value="no-repeat" {if isset($rowcolumnsetting->settings->bgrepeat) && ($rowcolumnsetting->settings->bgrepeat == 'no-repeat')}selected="selected"{/if}>No repeat</option>
			    								<option value="repeat-x" {if isset($rowcolumnsetting->settings->bgrepeat) && ($rowcolumnsetting->settings->bgrepeat == 'repeat-x')}selected="selected"{/if}>Repeat-X</option>
			    								<option value="repeat-y" {if isset($rowcolumnsetting->settings->bgrepeat) && ($rowcolumnsetting->settings->bgrepeat == 'repeat-y')}selected="selected"{/if}>Repeat-Y</option>
			    							</select>
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Select Row background repeat/no-repeat.
			    								</p>
			    							</div>
			    						</div>
			    					</div>

			    					<div class="form-group row_bg_group row_bg_image">
			    						<label class="control-label col-lg-3">
			    							Background Position
			    						</label>
			    						<div class="col-lg-9">
			    							<select class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][settings][bgpos]">
			    								<option value="left top" {if isset($rowcolumnsetting->settings->bgpos) && ($rowcolumnsetting->settings->bgpos == 'left top')}selected="selected"{/if}>left top</option>
			    								<option value="left center" {if isset($rowcolumnsetting->settings->bgpos) && ($rowcolumnsetting->settings->bgpos == 'left center')}selected="selected"{/if}>left center</option>
			    								<option value="left bottom" {if isset($rowcolumnsetting->settings->bgpos) && ($rowcolumnsetting->settings->bgpos == 'left bottom')}selected="selected"{/if}>left bottom</option>
			    								<option value="right top" {if isset($rowcolumnsetting->settings->bgpos) && ($rowcolumnsetting->settings->bgpos == 'right top')}selected="selected"{/if}>right top</option>
			    								<option value="right center" {if isset($rowcolumnsetting->settings->bgpos) && ($rowcolumnsetting->settings->bgpos == 'right center')}selected="selected"{/if}>right center</option>
			    								<option value="right bottom" {if isset($rowcolumnsetting->settings->bgpos) && ($rowcolumnsetting->settings->bgpos == 'right bottom')}selected="selected"{/if}>right bottom</option>
			    								<option value="center top" {if isset($rowcolumnsetting->settings->bgpos) && ($rowcolumnsetting->settings->bgpos == 'center top')}selected="selected"{/if}>center top</option>
			    								<option value="center center" {if isset($rowcolumnsetting->settings->bgpos) && ($rowcolumnsetting->settings->bgpos == 'center center')}selected="selected"{/if}>center center</option>
			    								<option value="center bottom" {if isset($rowcolumnsetting->settings->bgpos) && ($rowcolumnsetting->settings->bgpos == 'center bottom')}selected="selected"{/if}>center bottom</option>
			    							</select>
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Select Row background position.
			    								</p>
			    							</div>
			    						</div>
			    					</div>

			    					<div class="form-group row_bg_group row_bg_image">
			    						<label class="control-label col-lg-3">
			    							Background Size
			    						</label>
			    						<div class="col-lg-9">
			    							<select class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][settings][bgsize]">
			    								<option value="cover" {if isset($rowcolumnsetting->settings->bgsize) && ($rowcolumnsetting->settings->bgsize == 'cover')}selected="selected"{/if}>Cover</option>
			    								<option value="contain" {if isset($rowcolumnsetting->settings->bgsize) && ($rowcolumnsetting->settings->bgsize == 'contain')}selected="selected"{/if}>Contain</option>
			    								<option value="initial" {if isset($rowcolumnsetting->settings->bgsize) && ($rowcolumnsetting->settings->bgsize == 'initial')}selected="selected"{/if}>initial</option>
			    								<option value="100% 100%" {if isset($rowcolumnsetting->settings->bgsize) && ($rowcolumnsetting->settings->bgsize == '100% 100%')}selected="selected"{/if}>100% 100%</option>
			    							</select>
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Select Row background size.
			    								</p>
			    							</div>
			    						</div>
			    					</div>

			    					<div class="form-group row_bg_group row_bg_image">
			    						<label class="control-label col-lg-3">
			    							Background Attachment
			    						</label>
			    						<div class="col-lg-9">
			    							<select class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][settings][bgattach]">
			    								<option value="scroll" {if isset($rowcolumnsetting->settings->bgattach) && ($rowcolumnsetting->settings->bgattach == 'scroll')}selected="selected"{/if}>scroll</option>
			    								<option value="fixed" {if isset($rowcolumnsetting->settings->bgattach) && ($rowcolumnsetting->settings->bgattach == 'fixed')}selected="selected"{/if}>fixed</option>
			    								<option value="local" {if isset($rowcolumnsetting->settings->bgattach) && ($rowcolumnsetting->settings->bgattach == 'local')}selected="selected"{/if}>local</option>
			    							</select>
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Select Row background attachment.
			    								</p>
			    							</div>
			    						</div>
			    					</div>

			    					<div class="form-group">
			    						<label class="control-label col-lg-3">
			    							Custom Class
			    						</label>
			    						<div class="col-lg-9">
			    							<input type="text" class="xprtmegamenu_fld" name="xprtmegamenu[{$ai}][settings][customcls]" class="fixed-width-lg" value="{if isset($rowcolumnsetting->settings->customcls)}{$rowcolumnsetting->settings->customcls}{/if}">
			    							<div class="help-block-container">
			    								<p class="help-block">
			    									Please Enter a Custom Class.
			    								</p>
			    							</div>
			    						</div>
			    					</div>

					            </div>
					        </div>
					        <div class="modal-footer">
					        	<div class="col-sm-3 pull-right">
					            	<button type="button" class="btn btn-primary btn-block btn-sm icon_popup_cls " data-dismiss="modal">Submit</button>
					        	</div>
					        </div>
					    </div>
					</div>
				</div>
			</div>
			{$ai = $ai+1}
			{/foreach}
		{/if}
		{* End full visual content predefind values *}
	</div> {* end total element wrapper *}
	<div class="form-group row_inject_parentcontainer"> 
		<div class="row_inject_container"><a class="row_inject_element" href="#">Add New Row</a></div>
	</div>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}