<div class="panel"><h3><i class="icon-list-ul"></i> {l s='Menu Item list' mod='{$xprtmodulename}'}
	<span class="panel-heading-action">
		<a id="desc-product-new" class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')}&configure={$xprtmodulename}">
			<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Back To Menu List" data-html="true">
				<i class="process-icon-back"></i>
			</span>
		</a>
		<a id="desc-product-new" class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')}&configure={$xprtmodulename}&additem{$xprtmodulename}=1&id_{$xprtmodulename}={$identity}">
			<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Add New Item" data-html="true">
				<i class="process-icon-new "></i>
			</span>
		</a>
	</span>
	</h3>
	<div id="{$xprtmodulename}Content">
			{if isset($block_lists) && !empty($block_lists)}
		<div id="itemblock_lists">
			{foreach from=$block_lists item=block_list}
				<div id="itemblock_lists_{$block_list.id}" class="panel">
					<div class="row">
						<div class="col-md-1">
							<div><i class="icon-arrows"></i></div>
						</div>
						<div class="col-md-3">
							{$block_list.id}
						</div>
						<div class="col-md-8">
							<h4 class="pull-left">
								{$block_list.name}
							</h4>
							<div class="btn-group-action pull-right">
								{$block_list.status}
								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')}&configure={$xprtmodulename}&id_{$xprtmodulename}item={$block_list.id}&updateitem{$xprtmodulename}=1&id_{$xprtmodulename}={$identity}">
									<i class="icon-edit"></i> {l s='Edit'}
								</a>
								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')}&configure={$xprtmodulename}&deleteitem_id_{$xprtmodulename}={$block_list.id}">
									<i class="icon-trash"></i> {l s='Delete'}
								</a>
							</div>
						</div>
					</div>
				</div>
			{/foreach}
		</div>
			{else}
				<center><p style="font-size:15px;"><a id="desc-product-new2" class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')}&configure={$xprtmodulename}&additem{$xprtmodulename}=1&id_{$xprtmodulename}={$identity}">You Don't Have Any Menu Items. Please  Click Here  To Add New Menu Item.</a></p></center>
			{/if}
	</div>
</div>
<style type="text/css">
	#itemblock_lists .panel{
	    padding: 0px !important;
	    padding-left: 15px !important;
	    padding-right: 15px !important;
	    margin-bottom: 10px !important;
	}
</style>