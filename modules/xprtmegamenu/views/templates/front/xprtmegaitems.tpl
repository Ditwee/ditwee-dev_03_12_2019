{if isset($xprtmegaitems) && !empty($xprtmegaitems)}
	{* start main ul tag *}
	<ul class="submenu-container sf_mega_menu {if isset($xprtmegaitems.mainrowsetting.main->settings->customcls) && !empty($xprtmegaitems.mainrowsetting.main->settings->customcls)}{$xprtmegaitems.mainrowsetting.main->settings->customcls}{/if} {if $xprtmegaitems.is_fullwidth == 1}sf_mega_menu_fullwidth{else}sf_mega_menu_fixedwidth{/if}" style="{if isset($xprtmegaitems.mainrowsetting.main->settings->padding) && !empty($xprtmegaitems.mainrowsetting.main->settings->padding)}padding:{$xprtmegaitems.mainrowsetting.main->settings->padding};{/if}{if isset($xprtmegaitems.mainrowsetting.main->settings->border) && !empty($xprtmegaitems.mainrowsetting.main->settings->border)}border:{$xprtmegaitems.mainrowsetting.main->settings->border}px solid {if isset($xprtmegaitems.mainrowsetting.main->settings->borderclr) && !empty($xprtmegaitems.mainrowsetting.main->settings->borderclr)}{$xprtmegaitems.mainrowsetting.main->settings->borderclr};{else}#000;{/if}{/if}{if isset($xprtmegaitems.mainrowsetting.main->settings->bgtype) && !empty($xprtmegaitems.mainrowsetting.main->settings->bgtype)}{if ($xprtmegaitems.mainrowsetting.main->settings->bgtype == 'color')}{if isset($xprtmegaitems.mainrowsetting.main->settings->bgclr) && !empty($xprtmegaitems.mainrowsetting.main->settings->bgclr)}background-color:{$xprtmegaitems.mainrowsetting.main->settings->bgclr};{/if}
	{elseif ($xprtmegaitems.mainrowsetting.main->settings->bgtype == 'image')}{if isset($xprtmegaitems.mainrowsetting.main->settings->bgimg) && !empty($xprtmegaitems.mainrowsetting.main->settings->bgimg)}background-image: url('{$xprtimageurl}{$xprtmegaitems.mainrowsetting.main->settings->bgimg}');{else}background-image: none;{/if}	{if isset($xprtmegaitems.mainrowsetting.main->settings->bgrepeat) && !empty($xprtmegaitems.mainrowsetting.main->settings->bgrepeat)}background-repeat:{$xprtmegaitems.mainrowsetting.main->settings->bgrepeat};{/if}{if isset($xprtmegaitems.mainrowsetting.main->settings->bgpos) && !empty($xprtmegaitems.mainrowsetting.main->settings->bgpos)}background-position:{$xprtmegaitems.mainrowsetting.main->settings->bgpos};{/if}{if isset($xprtmegaitems.mainrowsetting.main->settings->bgsize) && !empty($xprtmegaitems.mainrowsetting.main->settings->bgsize)}background-size:{$xprtmegaitems.mainrowsetting.main->settings->bgsize};{/if}{if isset($xprtmegaitems.mainrowsetting.main->settings->bgattach) && !empty($xprtmegaitems.mainrowsetting.main->settings->bgattach)}background-attachment:{$xprtmegaitems.mainrowsetting.main->settings->bgattach};{/if}{/if}{/if}" {if $xprtmegaitems.is_fullwidth == 0}data-megawidth="{$xprtmegaitems.megawidth|intval}"{/if} >
	 {if isset($xprtmegaitems.rowcolumnsetting) && !empty($xprtmegaitems.rowcolumnsetting)} {* start per row *}
	 	{foreach from=$xprtmegaitems.rowcolumnsetting item=rowcolumnsetting}
			<li class="clearfix {if isset($rowcolumnsetting->settings->customcls) && !empty($rowcolumnsetting->settings->customcls)}{$rowcolumnsetting->settings->customcls}{/if}" style="{if isset($rowcolumnsetting->settings->padding) && !empty($rowcolumnsetting->settings->padding)}padding:{$rowcolumnsetting->settings->padding};{/if}{if isset($rowcolumnsetting->settings->border) && !empty($rowcolumnsetting->settings->border)}border:{$rowcolumnsetting->settings->border}px solid {if isset($rowcolumnsetting->settings->borderclr) && !empty($rowcolumnsetting->settings->borderclr)}{$rowcolumnsetting->settings->borderclr};{else}#000;{/if}{/if}{if isset($rowcolumnsetting->settings->bgtype) && !empty($rowcolumnsetting->settings->bgtype)}{if ($rowcolumnsetting->settings->bgtype == 'color')}{if isset($rowcolumnsetting->settings->bgclr) && !empty($rowcolumnsetting->settings->bgclr)}background-color:{$rowcolumnsetting->settings->bgclr};{/if}
		{elseif ($rowcolumnsetting->settings->bgtype == 'image')}{if isset($rowcolumnsetting->settings->bgimg) && !empty($rowcolumnsetting->settings->bgimg)}background-image: url('{$xprtimageurl}{$rowcolumnsetting->settings->bgimg}');{else}background-image: none;{/if}	{if isset($rowcolumnsetting->settings->bgrepeat) && !empty($rowcolumnsetting->settings->bgrepeat)}background-repeat:{$rowcolumnsetting->settings->bgrepeat};{/if}{if isset($rowcolumnsetting->settings->bgpos) && !empty($rowcolumnsetting->settings->bgpos)}background-position:{$rowcolumnsetting->settings->bgpos};{/if}{if isset($rowcolumnsetting->settings->bgsize) && !empty($rowcolumnsetting->settings->bgsize)}background-size:{$rowcolumnsetting->settings->bgsize};{/if}{if isset($rowcolumnsetting->settings->bgattach) && !empty($rowcolumnsetting->settings->bgattach)}background-attachment:{$rowcolumnsetting->settings->bgattach};{/if}{/if}{/if}">
				{if isset($rowcolumnsetting->columnvalues) && !empty($rowcolumnsetting->columnvalues)} {* start per column each row *}
				 	<div class="row" style="">
					 	{foreach from=$rowcolumnsetting->columnvalues item=columnvalues}
							<div class="{if isset($columnvalues->width) && !empty($columnvalues->width)}col-md-{$columnvalues->width} {else}col-md-12 {/if} {if isset($columnvalues->customcls) && !empty($columnvalues->customcls)}{$columnvalues->customcls}{/if}" style="{if isset($columnvalues->padding) && !empty($columnvalues->padding)}padding:{$columnvalues->padding};{/if}{if isset($columnvalues->border) && !empty($columnvalues->border)}border:{$columnvalues->border}px solid {if isset($columnvalues->borderclr) && !empty($columnvalues->borderclr)}{$columnvalues->borderclr};{else}#000;{/if}{/if}{if isset($columnvalues->bgtype) && !empty($columnvalues->bgtype)}{if ($columnvalues->bgtype == 'color')}{if isset($columnvalues->bgclr) && !empty($columnvalues->bgclr)}background-color:{$columnvalues->bgclr};{/if}
					{elseif ($columnvalues->bgtype == 'image')}{if isset($columnvalues->bgimg) && !empty($columnvalues->bgimg)}background-image: url('{$xprtimageurl}{$columnvalues->bgimg}');{else}background-image: none;{/if}	{if isset($columnvalues->bgrepeat) && !empty($columnvalues->bgrepeat)}background-repeat:{$columnvalues->bgrepeat};{/if}{if isset($columnvalues->bgpos) && !empty($columnvalues->bgpos)}background-position:{$columnvalues->bgpos};{/if}{if isset($columnvalues->bgsize) && !empty($columnvalues->bgsize)}background-size:{$columnvalues->bgsize};{/if}{if isset($columnvalues->bgattach) && !empty($columnvalues->bgattach)}background-attachment:{$columnvalues->bgattach};{/if}{/if}{/if}">
								{* start each column item *}
									{if (isset($columnvalues->eachcolumn) && !empty($columnvalues->eachcolumn))}
											<ul class="list_item">
										{foreach from=$columnvalues->eachcolumn item=eachcolumn}
												<li>
												{if isset($eachcolumn->shwlvl) && $eachcolumn->shwlvl == 'show'}
													<h4 class="title">
														<a href="{if isset($eachcolumn->lvllink->$id_lang)}{$eachcolumn->lvllink->$id_lang}{else}#{/if}" class="">
															{if isset($eachcolumn->level->$id_lang)}{$eachcolumn->level->$id_lang}{/if}
															{if isset($eachcolumn->badge->$id_lang) && !empty($eachcolumn->badge->$id_lang)}
																<span class="badge">
																	{$eachcolumn->badge->$id_lang}
																</span>
															{/if}
														</a>
													</h4>
												{/if}
												{if (isset($eachcolumn->type) && $eachcolumn->type == 'external')}
													{if isset($eachcolumn->xtrllvl->$id_lang) && !empty($eachcolumn->xtrllvl->$id_lang)}
															<a href="{if isset($eachcolumn->xtrlurl->$id_lang)}{$eachcolumn->xtrlurl->$id_lang}{else}#{/if}" class="" target="">{if isset($eachcolumn->xtrllvl->$id_lang)}{$eachcolumn->xtrllvl->$id_lang}{/if}</a>
													{/if}
												{elseif (isset($eachcolumn->type) && $eachcolumn->type == 'custom')}
													{if isset($eachcolumn->customhtml->$id_lang) && !empty($eachcolumn->customhtml->$id_lang)}
														{$eachcolumn->customhtml->$id_lang}
													{/if}
												{elseif (isset($eachcolumn->type) && $eachcolumn->type == 'hook')}
													{if isset($eachcolumn->hook) && !empty($eachcolumn->hook)}
														{hook h=$eachcolumn->hook}
													{/if}
												{elseif (isset($eachcolumn->type) && $eachcolumn->type == 'image')}
													{if isset($eachcolumn->conimage) && !empty($eachcolumn->conimage)}
														<img class="img-responsive" src="{$xprtimageurl}{$eachcolumn->conimage}"  {if isset($eachcolumn->cheight) && !empty($eachcolumn->cheight)} height="{$eachcolumn->cheight}" {/if} {if isset($eachcolumn->cwidth) && !empty($eachcolumn->cwidth)} width="{$eachcolumn->cwidth}" {/if} alt="">
													{/if}
												{elseif (isset($eachcolumn->type) && $eachcolumn->type == 'video')}
													{if isset($eachcolumn->video) && !empty($eachcolumn->video)}
														<div class="embed-responsive embed-responsive-16by9">
															<iframe class="embed-responsive-item" src="{$eachcolumn->video}" {if isset($eachcolumn->cwidth) && !empty($eachcolumn->cwidth)} width="{$eachcolumn->cwidth}" {/if} {if isset($eachcolumn->cheight) && !empty($eachcolumn->cheight)} height="{$eachcolumn->cheight}" {/if} frameborder="0"></iframe>
														</div>
													{/if}
												{elseif (isset($eachcolumn->type) && $eachcolumn->type == 'system')}
													{if isset($eachcolumn->link) && !empty($eachcolumn->link)}
														{foreach from=$eachcolumn->link item=link}
															{if isset($link) && !empty($link)}
																<a href="{xprtmegamenu::GetLinkByAllLink($link)}" class="">{xprtmegamenu::GetNameByAllLink($link)}</a>
															{/if}
														{/foreach}
													{/if}
												{elseif (isset($eachcolumn->type) && $eachcolumn->type == 'product')}
													{if isset($eachcolumn->products) && !empty($eachcolumn->products)}
														{xprtmegamenu::GetProductsMarkup($eachcolumn->products)}
													{/if}
												{/if}
												</li>
										{/foreach}
											</ul>
									{/if}
								{* end each column item *}
							</div>
					 	{/foreach}
				 	</div>
				{/if} {* end per column each row *}
			</li>
	 	{/foreach}
	 {/if} {* end per row *}
	</ul> {* end main ul tag *}
{/if}


	{* 
shwlvl
type
link
hook
products
conimage
cheight
cwidth
video


customhtml
xtrllvl
xtrlurl
level
badge
lvllink 
 *}