{if !empty($xprtmegamenu)}
	<div class="xprt_mega_menu_area clearfix {if (isset($xprtmegamenu.is_sticky) && ($xprtmegamenu.is_sticky == 'yes'))}sticky{/if}">
	    <div class="xprt_mega_menu sf-contener clearfix {if (isset($xprtmegamenu.is_horizontal) && ($xprtmegamenu.is_horizontal == 'yes'))}xprt_horizontal_menu{else}xprt_vertical_menu{/if} {if isset($xprtmegamenu.hook)}{$xprtmegamenu.hook}{/if} {if isset($xprtmegamenu.custom_class)}{$xprtmegamenu.custom_class}{/if}">
	        <ul class="sf-menu clearfix menu-content">
	            {if (isset($xprtmegamenu.menuitems) && !empty($xprtmegamenu.menuitems))}
	                {foreach from=$xprtmegamenu.menuitems item=xprt_menuitems}
						<li class="{if isset($xprt_menuitems.system_link)}{$xprt_menuitems.system_link}{/if}">
							{if isset($xprt_menuitems.badge) && !empty($xprt_menuitems.badge)}
								<span class="xprt_badge">
									{$xprt_menuitems.badge}
								</span>
							{/if}
{$menulinkarr=[]}
{if isset($xprt_menuitems.link_type)}{$menulinkarr.link_type = $xprt_menuitems.link_type}{/if}
{if isset($xprt_menuitems.link_value_lang)}{$menulinkarr.link_value_lang = $xprt_menuitems.link_value_lang}{/if}
{if isset($xprt_menuitems.external_link)}{$menulinkarr.external_link = $xprt_menuitems.external_link}{/if}
{if isset($xprt_menuitems.system_link)}{$menulinkarr.system_link = $xprt_menuitems.system_link}{/if}
{if isset($xprt_menuitems.html_link)}{$menulinkarr.html_link = $xprt_menuitems.html_link}{/if}
						    <a href="{xprtmegamenu::getMenuLink($menulinkarr)}" title="{if isset($xprt_menuitems.name)}{$xprt_menuitems.name}{/if}">
						    {if isset($xprt_menuitems.icon) && !empty($xprt_menuitems.icon)}
						    	<i class="{if isset($xprt_menuitems.icon_pos)} {$xprt_menuitems.icon_pos} {/if} {$xprt_menuitems.icon}"></i>
						    {/if}
						    {if isset($xprt_menuitems.name)}{$xprt_menuitems.name}{/if}
						    </a>
						    {if (isset($xprt_menuitems.is_submenu) && ($xprt_menuitems.is_submenu == 1) && ($xprt_menuitems.is_megamenu != 1))}
{$submenulinkarr=[]}
{if isset($xprt_menuitems.submenu_level)}{$submenulinkarr.submenu_level = $xprt_menuitems.submenu_level}{/if}
{if isset($xprt_menuitems.submenu_type)}{$submenulinkarr.submenu_type = $xprt_menuitems.submenu_type}{/if}
{if isset($xprt_menuitems.sub_external_link)}{$submenulinkarr.sub_external_link = $xprt_menuitems.sub_external_link}{/if}
{if isset($xprt_menuitems.sub_external_level)}{$submenulinkarr.sub_external_level = $xprt_menuitems.sub_external_level}{/if}
{if isset($xprt_menuitems.sub_system_link)}{$submenulinkarr.sub_system_link = $xprt_menuitems.sub_system_link}{/if}
{if isset($xprt_menuitems.sub_html_link)}{$submenulinkarr.sub_html_link = $xprt_menuitems.sub_html_link}{/if}
								{xprtmegamenu::getSubMenuLink($submenulinkarr)}
						    {/if}
						    {if (isset($xprt_menuitems.is_megamenu) && ($xprt_menuitems.is_megamenu == 1))}
								{xprtmegamenu::generateMegaMenu($xprt_menuitems)}
						    {/if}
						</li>
	                {/foreach}
	            {/if}
	        </ul>
	    </div> <!-- //xprt_mega_menu -->
	</div> <!-- //xprt_mega_menu_area -->	            
{/if}
{strip}
	{addJsDefL name='xprtmegamenu_label'}{l s='Menu' mod='xprtmegamenu' js=1}{/addJsDefL}
{/strip}