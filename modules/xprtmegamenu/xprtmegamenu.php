<?php

if (!defined('_PS_VERSION_'))
	exit;
include_once(_PS_MODULE_DIR_.'xprtmegamenu/classes/xprtmegamenuclass.php');
include_once(_PS_MODULE_DIR_.'xprtmegamenu/classes/xprtmegamenuitemclass.php');
class xprtmegamenu extends Module
{
	private $_html = '';
	private $_hooks = '';
	private $_category_nested = array();
	private $_cmscategory_nested = array();
	private $_blogcategory_nested = array();
	private $_category_count = 0;
	private $_blogcategory_count = 0;
	private $_cmscategory_count = 0;
	public static $module_name = 'xprtmegamenu';
	public static $tablename = 'xprtmegamenu';
	public static $classname = 'xprtmegamenuclass';
	public static $xiplinkobj;
	public $icon_url = '/icon/icon.php';
	public $link_type = array('external_link'=>'External Link','system_link'=>'System Link','html_link'=>'HTML');
	public $parentlink_type = array('external_link'=>'External Link','system_link'=>'System Link');
	public function __construct()
	{
		$this->name = 'xprtmegamenu';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'xpert-idea.com';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;
		parent::__construct();
		$this->initializeHooks();
		$this->displayName = $this->l('Great Store Theme Mega Menu Modules For Prestashop');
		$this->description = $this->l('Great Store Theme Mega Menu Modules For Prestashop by Xpert Idea.');
		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => '1.6.99.99');
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->HookRegister()
			|| !$this->CreateTables()
			|| !$this->Register_Tabs()
		)
			return false;	
		else	
			return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
			|| !$this->deleteTables()
			|| !$this->UnRegister_Tabs()
		)
			return false;	
		else	
			return true;
	}
	private function HookRegister(){
		if(isset($this->_hooks) && !empty($this->_hooks)){
			foreach ($this->_hooks as $_hooks_values) {
				if(isset($_hooks_values) && !empty($_hooks_values)){
					foreach ($_hooks_values as $_hook) {
						$this->registerHook($_hook);
					}
				}
			}
		}
		return true;
	}
	private function GetParentLinkType(){
		$results = array();
		if(isset($this->parentlink_type) && !empty($this->parentlink_type)){
			$i = 0;
			foreach ($this->parentlink_type as $linkkey => $linkvalue) {
				$results[$i]['id'] = $linkkey;
				$results[$i]['name'] = $linkvalue;
				$i++;
			}
		}
		return $results;
	}
	private function GetLinkType(){
		$results = array();
		if(isset($this->link_type) && !empty($this->link_type)){
			$i = 0;
			foreach ($this->link_type as $linkkey => $linkvalue) {
				$results[$i]['id'] = $linkkey;
				$results[$i]['name'] = $linkvalue;
				$i++;
			}
		}
		return $results;
	}
	public static function GetProductsByID($ids = NULL)
    {
        if($ids == NULL)
            return false;
            $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
             pl.`description`, pl.`description_short`, product_attribute_shop.id_product_attribute, pl.`link_rewrite`,
              pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, image_shop.`id_image`, il.`legend`,
               m.`name` AS manufacturer_name FROM `' . _DB_PREFIX_ . 'product` p
            ' . Shop::addSqlAssociation('product', 'p') . ' LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute pa ON (pa.id_product = p.id_product) 
            ' . Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.default_on=1') . '
            ' . Product::sqlStock('p', 0, false) . ' LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON ( p.`id_product` = pl.`id_product` AND pl.`id_lang` = 
                ' . (int) Context::getContext()->language->id . Shop::addSqlRestrictionOnLang('pl') . '
            ) LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_product` = p.`id_product`)' . Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1') . 
            ' LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int) Context::getContext()->language->id . ')
            LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`) WHERE  p.`id_product` IN(' . $ids . ') AND product_shop.`active` = 1  
            AND product_shop.`show_price` = 1 AND ((image_shop.id_image IS NOT NULL OR i.id_image IS NULL) OR (image_shop.id_image IS NULL AND i.cover=1)) AND (pa.id_product_attribute IS NULL OR 
                product_attribute_shop.default_on = 1)';
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
            return Product::getProductsProperties((int) Context::getContext()->language->id,$result);
    }
	public static function GetProductsMarkup($products = null)
	{
		if($products == null)
			return false;
		$arr = array();
		if(isset($products) && !empty($products)){
			foreach($products as $product) {
				if(isset($product) && !empty($product)){
					$arr[] = str_replace("prd_","",$product);
				}
			}
		}
		if(isset($arr) && !empty($arr)){
			$prd_str = @implode(",",$arr);
			$products = self::GetProductsByID($prd_str);
			$context = Context::getContext();
			$id_lang = $context->language->id;
			$data = $context->smarty->createData();
			$data->assign(array(
				'id_lang' => $id_lang,
				'xprtproducts' => $products,
				'xprtimageurl' => _MODULE_DIR_.'xprtmegamenu/images/',
			));
			return $context->smarty->createTemplate(_PS_ROOT_DIR_.'/modules/xprtmegamenu/views/templates/front/xprtproducts.tpl', $data)->fetch();
		}else{
			return false;
		}
	}
	public static function generateMegaMenu($params = null)
	{
		if($params == null)
			return false;
		$context = Context::getContext();
		$id_lang = $context->language->id;
		$data = $context->smarty->createData();
		$data->assign(array(
			'id_lang' => $id_lang,
			'xprtmegaitems' => $params,
			'xprtimageurl' => _MODULE_DIR_.'xprtmegamenu/images/'
		));
		return $context->smarty->createTemplate(_PS_ROOT_DIR_.'/modules/xprtmegamenu/views/templates/front/xprtmegaitems.tpl', $data)->fetch();
	}
	public static function getSubMenuLink($params = null)
	{
		$html = '';
		if($params == null)
			return false;
		if(isset($params['submenu_level']) && !empty($params['submenu_level'])){
			$submenu_level = $params['submenu_level'];
		}else{
			$submenu_level = 'one';
		}
		if(isset($params['submenu_type']) && !empty($params['submenu_type'])){
			$submenu_type = $params['submenu_type'];
		}else{
			$submenu_type = 'sub_html_link';
		}
		if(isset($params['sub_external_link']) && !empty($params['sub_external_link'])){
			$sub_external_link = $params['sub_external_link'];
		}else{
			$sub_external_link = "#";
		}
		if(isset($params['sub_external_level']) && !empty($params['sub_external_level'])){
			$id_lang = (int)Context::getContext()->language->id;
			$sub_external_level = $params['sub_external_level'][$id_lang];
		}else{
			$sub_external_level = "Sample Link";
		}
		if(isset($params['sub_system_link']) && !empty($params['sub_system_link'])){
			$sub_system_link = $params['sub_system_link'];
		}else{
			$sub_system_link = array("cat_2");
		}
		if(isset($params['sub_html_link']) && !empty($params['sub_html_link'])){
			$id_lang = (int)Context::getContext()->language->id;
			$sub_html_link = $params['sub_html_link'][$id_lang];
		}else{
			$sub_html_link = "";
		}
		if($submenu_type == 'sub_external_link'){
			$html .= '<ul class="submenu-regular sf_regular_menu clearfix">';
			$html .= '<li class=""><a title="'.$sub_external_level.'" href="'.$sub_external_link.'">'.$sub_external_level.'</a></li>';
			$html .= '</ul>';
		}elseif($submenu_type == 'sub_html_link'){
			$html .= $sub_html_link;
		}elseif($submenu_type == 'sub_system_link'){
			// submenu_level
			if(isset($sub_system_link) && !empty($sub_system_link)){
				$html .= '<ul class="submenu-regular sf_regular_menu clearfix">';
					foreach ($sub_system_link as $sub_system_val) {
						$name = self::GetNameByAllLink($sub_system_val);
						$link = self::GetLinkByAllLink($sub_system_val);
						$html .= '<li class=""><a title="'.$name.'" href="'.$link.'">'.$name.'</a>';
						$html .= self::GetEachSubMenu($sub_system_val,$submenu_level);
						$html .= '</li>';
					}
				$html .= '</ul>';
			}
		}
		return $html;
	}
	public static function GetFrontChildCategory($id_parent = null)
	{
		if($id_parent == null)
			return false;
		$context = Context::getContext();
		$id_shop = (int)$context->shop->id;
		$id_lang = (int)$context->language->id;
        $sql = 'SELECT `id_category` FROM `'._DB_PREFIX_.'category` WHERE active = 1 AND id_parent = '.$id_parent;
        $results = Db::getInstance()->executeS($sql);
        return $results;
	}
	public static function GetEachSubMenu($syslnk = '',$level='one')
	{
		$html = '';
		if(empty($syslnk)){
			$html = '';
		}else{
			if(strpos($syslnk,'cat_') !== false){
				$id_category = str_replace("cat_","",$syslnk);
				$results = self::GetFrontChildCategory($id_category);
				if(isset($results) && !empty($results)){
					$html .= '<ul>';
					foreach ($results as $result) {
						$name = self::GetCategoryName($result['id_category']);
						$link = self::GetLinkObject()->getCategoryLink($result['id_category']);
						$html .= '<li><a href="'.$link.'" title="'.$name.'">'.$name.'</a>';
							if($level == 'multi'){
								$childresults = self::GetFrontChildCategory($result['id_category']);
								if(isset($childresults) && !empty($childresults)){
										$html .= '<ul>';
									foreach ($childresults as $childresult) {
											$namec = self::GetCategoryName($childresult['id_category']);
											$linkc = self::GetLinkObject()->getCategoryLink($childresult['id_category']);
											$html .= '<li><a href="'.$linkc.'" title="'.$namec.'">'.$namec.'</a>';
												$childchildresults = self::GetFrontChildCategory($childresult['id_category']);
												if(isset($childchildresults) && !empty($childchildresults)){
														$html .= '<ul>';
													foreach ($childchildresults as $childchildresult) {
															$namecc = self::GetCategoryName($childchildresult['id_category']);
															$linkcc = self::GetLinkObject()->getCategoryLink($childchildresult['id_category']);
															$html .= '<li><a href="'.$linkcc.'" title="'.$namecc.'">'.$namecc.'</a>';
															$html .= '</li>';
													}
														$html .= '</ul>';
												}
											$html .= '</li>';
									}
										$html .= '</ul>';
								}
							}
						$html .= '</li>';
					}
					$html .= '</ul>';
				}
			}elseif(strpos($syslnk,'page_') !== false){
				$pagess = str_replace("page_","",$syslnk);
				$link = self::getPagesLink($pagess);
				$name = self::getPagesLevel($pagess);
				$html = '<li><a href="'.$link.'" title="'.$name.'">'.$name.'</a></li>';
			}elseif(strpos($syslnk,'module_') !== false){
				$module = str_replace("module_","",$syslnk);
				$link = self::getModulePagesLink($module);
				$name = self::getModulePagesLevel($module);
				$html = '<li><a href="'.$link.'" title="'.$name.'">'.$name.'</a></li>';
			// }elseif(strpos($syslnk,'xprtshop_') !== false){
			// 	$id_shop = str_replace("xprtshop_","",$syslnk);
			// 	$shop = new Shop((int)$id_shop);
			// 	$name = $shop->name;
			// 	$link = $shop->getBaseURL();
			// 	$html = '<li><a href="'.$link.'" title="'.$name.'">'.$name.'</a></li>';
			}elseif(strpos($syslnk,'XIPPOST_') !== false){
		    	if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
		    	{
			    	if(class_exists("xipblog") && class_exists("xippostsclass"))
			    	{
			    		// $id_lang = (int)Context::getContext()->language->id;
			    		// $id_blog_post = str_replace("XIPPOST_","",$syslnk);
			    		// $blog_post_obj = new xippostsclass((int)$id_blog_post);
			    		// $params = array();
			    		// $params['id'] = @$blog_post_obj->id;
			    		// $params['rewrite'] = @$blog_post_obj->link_rewrite[$id_lang];
			    		// $params['page_type'] = 'post';
			    		// $link = xipblog::XipBlogPostLink($params);
			    		// $name = @$blog_post_obj->post_title[$id_lang];
			    		// $html = '<li><a href="'.$link.'" title="'.$name.'">'.$name.'</a></li>';
			    		$html = '';
			    	}else{
			    		$html = '';
			    	}
			    }else{
			    	$html = '';
			    }
			}elseif(strpos($syslnk,'XIPCAT_') !== false){
		    	if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
		    	{
			    	if(class_exists("xipblog") && class_exists("xipcategoryclass"))
			    	{
			    		$id_lang = (int)Context::getContext()->language->id;
			    		$id_blog_category = str_replace("XIPCAT_","",$syslnk);
			    		$blog_category_obj = new xipcategoryclass((int)$id_blog_category);
			    		$params = array();
			    		$params['id'] = @$blog_category_obj->id;
			    		$params['rewrite'] = @$blog_category_obj->link_rewrite[$id_lang];
			    		$params['page_type'] = 'category';
			    		$params['subpage_type'] = 'post';
			    		$link = xipblog::XipBlogCategoryLink($params);
			    		$name = @$blog_category_obj->name[$id_lang];
			    		$html = '<li><a href="'.$link.'" title="'.$name.'">'.$name.'</a></li>';
			    	}else{
			    		$html = '';
			    	}
			    }else{
			    	$html = '';
			    }
			}elseif(strpos($syslnk,'cmscat_') !== false){
				$id_cms_categoryy = str_replace("cmscat_","",$syslnk);
				$html = '';
			}else{
				$html = '';
			}
		}
		return $html;
	}
	public static function getMenuLink($params = null)
	{
		if($params == null)
			return false;
		$link = '#';
		if(isset($params['link_type']) && !empty($params['link_type'])){
			if($params['link_type'] == 'external_link'){
				if(isset($params['external_link'])){
					$link = $params['external_link'];
				}else{
					$link = '#';
				}
			}elseif($params['link_type'] == 'system_link'){
				if(isset($params['system_link']) && !empty($params['system_link'])){
					$syslnk = $params['system_link'];
				}else{
					$syslnk = '';
				}
				$link = self::GetLinkByAllLink($syslnk);
			}
		}
		return $link;
	}
	public static function GetCategoryName($id = null)
	{
		if($id == null)
			return false;
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT `name` FROM `'._DB_PREFIX_.'category_lang` WHERE id_lang = '.$id_lang.' AND id_shop = '.$id_shop.' AND id_category = '.$id;
	    $name = DB::getInstance()->getValue($sql);
	    return (isset($name)) ? $name : false;
	}
	public static function GetBrandName($id = null)
	{
		if($id == null)
			return false;
		$id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT `meta_title` FROM `'._DB_PREFIX_.'manufacturer_lang` WHERE id_lang = '.$id_lang.' AND id_manufacturer = '.$id;
	    $name = DB::getInstance()->getValue($sql);
	    if(empty($name)){
	    	$sql = 'SELECT `name` FROM `'._DB_PREFIX_.'manufacturer` WHERE id_manufacturer = '.$id;
	    	$name = DB::getInstance()->getValue($sql);
	    }
	    return (isset($name)) ? $name : false;
	}
	public static function GetSupplierName($id = null)
	{
		if($id == null)
			return false;
		$id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT `meta_title` FROM `'._DB_PREFIX_.'supplier_lang` WHERE id_lang = '.$id_lang.' AND id_supplier = '.$id;
	    $name = DB::getInstance()->getValue($sql);
	    if(empty($name)){
	    	$sql = 'SELECT `name` FROM `'._DB_PREFIX_.'supplier` WHERE id_supplier = '.$id;
	    	$name = DB::getInstance()->getValue($sql);
	    }
	    return (isset($name)) ? $name : false;
	}
	public static function GetCMSCategoryName($id = null)
	{
		if($id == null)
			return false;
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT `name` FROM `'._DB_PREFIX_.'cms_category_lang` WHERE id_lang = '.$id_lang.' AND id_shop = '.$id_shop.' AND id_cms_category = '.$id;
	    $name = DB::getInstance()->getValue($sql);
	    return (isset($name)) ? $name : false;
	}
	public static function GetCMSName($id = null)
	{
		if($id == null)
			return false;
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT `meta_title` FROM `'._DB_PREFIX_.'cms_lang` WHERE id_lang = '.$id_lang.' AND id_shop = '.$id_shop.' AND id_cms = '.$id;
	    $name = DB::getInstance()->getValue($sql);
	    return (isset($name)) ? $name : false;
	}
	public static function GetProductName($id = null)
	{
		if($id == null)
			return false;
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT `name` FROM `'._DB_PREFIX_.'product_lang` WHERE id_lang = '.$id_lang.' AND id_shop = '.$id_shop.' AND id_product = '.$id;
	    $name = DB::getInstance()->getValue($sql);
	    return (isset($name)) ? $name : false;
	}
	public static function GetNameByAllLink($syslnk = '')
	{
		if(empty($syslnk)){
			$name = '#';
		}else{
			if(strpos($syslnk,'cat_') !== false){
				$id_category = str_replace("cat_","",$syslnk);
				$name = self::GetCategoryName($id_category);
			}elseif(strpos($syslnk,'man_') !== false){
				$id_manufacturer = str_replace("man_","",$syslnk);
				$name = self::GetBrandName($id_manufacturer);
			}elseif(strpos($syslnk,'sup_') !== false){
				$id_supplier = str_replace("sup_","",$syslnk);
				$name = self::GetSupplierName($id_supplier);
			}elseif(strpos($syslnk,'prd_') !== false){
				$id_product = str_replace("prd_","",$syslnk);
				$name = self::GetProductName($id_product);
			}elseif(strpos($syslnk,'cms_') !== false){
				$id_cms = str_replace("cms_","",$syslnk);
				$name = self::GetCMSName($id_cms);
			}elseif(strpos($syslnk,'cmscat_') !== false){
				$id_cms_categoryy = str_replace("cmscat_","",$syslnk);
				$name = self::GetCMSCategoryName($id_cms_categoryy);
			}elseif(strpos($syslnk,'page_') !== false){
				$pagess = str_replace("page_","",$syslnk);
				$name = self::getPagesLevel($pagess);
			}elseif(strpos($syslnk,'xprtshop_') !== false){
				$id_shop = str_replace("xprtshop_","",$syslnk);
				$shop = new Shop((int)$id_shop);
				$name = $shop->name;
			}elseif(strpos($syslnk,'XIPPOST_') !== false){
		    	if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
		    	{
			    	if(class_exists("xipblog") && class_exists("xippostsclass"))
			    	{
			    		$id_lang = (int)Context::getContext()->language->id;
			    		$id_blog_post = str_replace("XIPPOST_","",$syslnk);
			    		$blog_post_obj = new xippostsclass((int)$id_blog_post);
			    		$name = @$blog_post_obj->post_title[$id_lang];
			    	}else{
			    		$name = '';
			    	}
			    }else{
			    	$name = '';
			    }
			}elseif(strpos($syslnk,'XIPCAT_') !== false){
		    	if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
		    	{
			    	if(class_exists("xipblog") && class_exists("xipcategoryclass"))
			    	{
			    		$id_lang = (int)Context::getContext()->language->id;
			    		$id_blog_category = str_replace("XIPCAT_","",$syslnk);
			    		$blog_category_obj = new xipcategoryclass((int)$id_blog_category);				
			    		$name = @$blog_category_obj->name[$id_lang];
			    	}else{
			    		$name = '';
			    	}
			    }else{
			    	$name = '';
			    }
			}elseif(strpos($syslnk,'module_') !== false){
				$module = str_replace("module_","",$syslnk);
				// $link = self::getModulePagesLink($module);
				$name = self::getModulePagesLevel($module);
			}else{
				$name = '';
			}
		}
		return $name;
	}
	public static function GetLinkByAllLink($syslnk = ''){
		if(empty($syslnk)){
			$link = '#';
		}else{
			if(strpos($syslnk,'cat_') !== false){
				$id_category = str_replace("cat_","",$syslnk);
				$link = self::GetLinkObject()->getCategoryLink($id_category);
			}elseif(strpos($syslnk,'man_') !== false){
				$id_manufacturer = str_replace("man_","",$syslnk);
				$link = self::GetLinkObject()->getManufacturerLink($id_manufacturer);
			}elseif(strpos($syslnk,'sup_') !== false){
				$id_supplier = str_replace("sup_","",$syslnk);
				$link = self::GetLinkObject()->getSupplierLink($id_supplier);
			}elseif(strpos($syslnk,'prd_') !== false){
				$id_product = str_replace("prd_","",$syslnk);
				$link = self::GetLinkObject()->getProductLink($id_product);
			}elseif(strpos($syslnk,'cms_') !== false){
				$id_cms = str_replace("cms_","",$syslnk);
				$link = self::GetLinkObject()->getCMSLink($id_cms);
			}elseif(strpos($syslnk,'cmscat_') !== false){
				$id_cms_categoryy = str_replace("cmscat_","",$syslnk);
				$link = self::GetLinkObject()->getCMSCategoryLink($id_cms_categoryy);
			}elseif(strpos($syslnk,'page_') !== false){
				$pagess = str_replace("page_","",$syslnk);
				$link = self::getPagesLink($pagess);
			}elseif(strpos($syslnk,'xprtshop_') !== false){
				$id_shop = str_replace("xprtshop_","",$syslnk);
				$shop = new Shop((int)$id_shop);
				$link = $shop->getBaseURL();
			}elseif(strpos($syslnk,'XIPPOST_') !== false){
				if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
		    	{
			    	if(class_exists("xipblog") && class_exists("xippostsclass"))
			    	{
			    		$id_lang = (int)Context::getContext()->language->id;
			    		$id_blog_post = str_replace("XIPPOST_","",$syslnk);
			    		$blog_post_obj = new xippostsclass((int)$id_blog_post);
			    		$params = array();
			    		$params['id'] = @$blog_post_obj->id;
			    		$params['rewrite'] = @$blog_post_obj->link_rewrite[$id_lang];
			    		$params['page_type'] = 'post';
			    		$link = xipblog::XipBlogPostLink($params);
			    	}else{
			    		$link = '';
			    	}
			    }else{
			    	$link = '';
			    }
			}elseif(strpos($syslnk,'XIPCAT_') !== false){
				if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
				{
					if(class_exists("xipblog") && class_exists("xipcategoryclass"))
					{
						$id_lang = (int)Context::getContext()->language->id;
						$id_blog_category = str_replace("XIPCAT_","",$syslnk);
						$blog_category_obj = new xipcategoryclass((int)$id_blog_category);
						$params = array();
						$params['id'] = @$blog_category_obj->id;
						$params['rewrite'] = @$blog_category_obj->link_rewrite[$id_lang];
						$params['page_type'] = 'category';
						$params['subpage_type'] = 'post';
						$link = xipblog::XipBlogCategoryLink($params);
					}else{
						$link = '#';
					}
				}else{
					$link = '#';
				}
			}elseif(strpos($syslnk,'module_') !== false){
				$module = str_replace("module_","",$syslnk);
				$link = self::getModulePagesLink($module);
			}else{
				$link = '#';
			}
		}
		return $link;
	}
	public static function GetLinkObject()
	{
    	if(!isset(self::$xiplinkobj) || empty(self::$xiplinkobj))
    	{
    		$ssl = false;
    		if (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) {
    		    $ssl = true;
    		}
    		$protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
    		$useSSL = ((isset($ssl) && $ssl && Configuration::get('PS_SSL_ENABLED')) || Tools::usingSecureMode()) ? true : false;
    		$protocol_content = ($useSSL) ? 'https://' : 'http://';
    		self::$xiplinkobj = new Link($protocol_link, $protocol_content);
    	}
    	return self::$xiplinkobj;
    }
	private function GetSubLinkType(){
		$results = array();
		if(isset($this->link_type) && !empty($this->link_type)){
			$i = 0;
			foreach ($this->link_type as $linkkey => $linkvalue) {
				$results[$i]['id'] = "sub_".$linkkey;
				$results[$i]['name'] = $linkvalue;
				$i++;
			}
		}
		return $results;
	}
	private function GetHookarray(){
		$results = array();
		if(isset($this->_hooks['display']) && !empty($this->_hooks['display'])){
			$i = 0;
			foreach ($this->_hooks['display'] as $_hook) {
				$results[$i]['id'] = $_hook;
				$results[$i]['name'] = $_hook;
				$i++;
			}
		}
		return $results;
	}
	private function initializeHooks()
    {
        $this->_hooks = array(
        	'display' => array(
        		'DisplayTop',
        		'displayMainMenu',
        		'DisplayTopColumn',
        		'displayTopLeft',
        	),
        	'action' => array(
        		'displayHeader',
        	),
        );
    }
	public function processImage($file_name = null){
		if($file_name == null)
			return false;
	    if(isset($_FILES[$file_name]) && isset($_FILES[$file_name]['tmp_name']) && !empty($_FILES[$file_name]['tmp_name'])){
            $ext = substr($_FILES[$file_name]['name'], strrpos($_FILES[$file_name]['name'], '.') + 1);
            $basename_file_name = basename($_FILES[$file_name]["name"]);
            $strlen = strlen($basename_file_name);
            $strlen_ext = strlen($ext);
            $basename_file_name = substr($basename_file_name,0,($strlen-$strlen_ext));
            $link_rewrite_file_name = Tools::link_rewrite($basename_file_name);
            $file_orgname = $link_rewrite_file_name.'.'.$ext;
            $path = _PS_MODULE_DIR_ .self::$module_name.'/images/' . $file_orgname;
            if(!move_uploaded_file($_FILES[$file_name]['tmp_name'],$path))
                return false;         
            else
                return $file_orgname;   
	    }
	}
	public static function PageException($exceptions = NULL)
	{
		if($exceptions == NULL)
			return false;
		$exceptions = explode(",",$exceptions);
		$page_name = Context::getContext()->controller->php_self;
		$this_arr = array();
		$this_arr[] = 'all_page';
		$this_arr[] = $page_name;
		if($page_name == 'category'){
			$id_category = Tools::getvalue('id_category');
			$this_arr[] = 'cat_'.$id_category;
		}elseif($page_name == 'product'){
			$id_product = Tools::getvalue('id_product');
			$this_arr[] = 'prd_'.$id_product;
			// Start Get Product Category
			$prd_cat_sql = 'SELECT cp.`id_category` AS id
			    FROM `'._DB_PREFIX_.'category_product` cp
			    LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = cp.id_category)
			    '.Shop::addSqlAssociation('category', 'c').'
			    WHERE cp.`id_product` = '.(int)$id_product;
			$prd_catresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_cat_sql);
			if(isset($prd_catresults) && !empty($prd_catresults))
			{
			    foreach($prd_catresults as $prd_catresult)
			    {
			        $this_arr[] = 'prdcat_'.$prd_catresult['id'];
			    }
			}
			// END Get Product Category
			// Start Get Product Manufacturer
			$prd_man_sql = 'SELECT `id_manufacturer` AS id FROM `'._DB_PREFIX_.'product` WHERE `id_product` = '.(int)$id_product;
			$prd_manresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_man_sql);
			if(isset($prd_manresults) && !empty($prd_manresults))
			{
			    foreach($prd_manresults as $prd_manresult)
			    {
			        $this_arr[] = 'prdman_'.$prd_manresult['id'];
			    }
			}
			// END Get Product Manufacturer
			// Start Get Product SupplierS
			$prd_sup_sql = "SELECT `id_supplier` AS id FROM `"._DB_PREFIX_."product_supplier` WHERE `id_product` = ".(int)$id_product." GROUP BY `id_supplier`";
			$prd_supresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_sup_sql);
			if(isset($prd_supresults) && !empty($prd_supresults))
			{
			    foreach($prd_supresults as $prd_supresult)
			    {
			        $this_arr[] = 'prdsup_'.$prd_supresult['id'];
			    }
			}
			// END Get Product SupplierS
		}elseif($page_name == 'cms'){
			$id_cms = Tools::getvalue('id_cms');
			$this_arr[] = 'cms_'.$id_cms;
		}elseif($page_name == 'manufacturer'){
			$id_manufacturer = Tools::getvalue('id_manufacturer');
			$this_arr[] = 'man_'.$id_manufacturer;
		}elseif($page_name == 'supplier'){
			$id_supplier = Tools::getvalue('id_supplier');
			$this_arr[] = 'sup_'.$id_supplier;
		}
		if(isset($this_arr)){
			foreach ($this_arr as $this_arr_val) {
				if(in_array($this_arr_val,$exceptions))
					return true;
			}
		}
		return false;
	}
	public function Register_Tabs()
    {
        $langs = Language::getLanguages();
        $tab_listobj = new Tab();
        $tab_listobj->class_name = "Adminxprtmegamenu";
        $tab_listobj->id_parent = -1;
        $tab_listobj->module = $this->name;
        foreach($langs as $l)
        {
            $tab_listobj->name[$l['id_lang']] = $this->l("Admin Xpert Mega Menu");
        }
        $tab_listobj->save();
        return true;
    }
    public function UnRegister_Tabs()
    {
        $save_tab_id = (int)Tab::getIdFromClassName("Adminxprtmegamenu");
        if($save_tab_id != 0){
    		if(isset($save_tab_id) && !empty($save_tab_id)){
    		    $tabobjs = new Tab($save_tab_id);
    		    $tabobjs->delete();
    		}
        }
        return true;
    }
    public function getCategories($categories = null)
    {
    	$space = "";
    	if($categories == null){
        	$categories =  Category::getNestedCategories();
    	}
        if(isset($categories) && !empty($categories)){
        	foreach ($categories as $key => $categorie) {
        		$spacer = str_repeat('&nbsp;',4*(int)$categorie['level_depth']);
        		$this->_category_nested[$this->_category_count]['id'] = $categorie['id_category'];
        		$this->_category_nested[$this->_category_count]['name'] = $spacer.$categorie['name'];
        		$this->_category_count++;
        		if(isset($categorie['children']) && !empty($categorie['children'])){
        			$this->getCategories($categorie['children']);
        		}
        	}
        }
        return $this->_category_nested;
    }
    public function getCMSCategoriesList($id_parent = 0)
    {
    	$cmscategories =  self::getCMSCategories($id_parent);
    	if(isset($cmscategories) && !empty($cmscategories)){
    		foreach ($cmscategories as $cmscategorie) {
    			$spacer = str_repeat('&nbsp;',4*(int)$cmscategorie['level_depth']);
    			$this->_cmscategory_nested[$this->_cmscategory_count]['id'] = 'cmscat_'.$cmscategorie['id_cms_category'];
    			$this->_cmscategory_nested[$this->_cmscategory_count]['name'] = $spacer.'<strong>'.$cmscategorie['name'].'</strong>';
    			$this->_cmscategory_count++;
    			$this->getCMSByCategory($cmscategorie['id_cms_category'],$cmscategorie['level_depth']);
    			$this->getCMSCategoriesList($cmscategorie['id_cms_category']);
    		}
    		return $this->_cmscategory_nested;
    	}else{
    		return false;
    	}
    }
    public function getCMSByCategory($id_cms_category = 0,$level_depth = 1)
    {
    	if($id_cms_category == 0)
    		return false;
    	$level_depth = $level_depth+1;
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
        $query = 'SELECT * FROM `'._DB_PREFIX_.'cms` pb INNER JOIN `'._DB_PREFIX_.'cms_lang` pbl ON (pb.`id_cms` = pbl.`id_cms` AND pbl.`id_lang` = '.$id_lang.' AND pbl.`id_shop` = '.$id_shop.') INNER JOIN `'._DB_PREFIX_.'cms_shop` pbs ON (pb.`id_cms` = pbs.`id_cms` AND pbs.`id_shop` = '.$id_shop.') WHERE pb.`id_cms_category` = '.(int)$id_cms_category;
        $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        if(isset( $results) && !empty( $results)){
        	foreach ($results as $result) {
        		$spacer = str_repeat('&nbsp;',4*$level_depth);
        		$this->_cmscategory_nested[$this->_cmscategory_count]['id'] = 'cms_'.$result['id_cms'];
        		$this->_cmscategory_nested[$this->_cmscategory_count]['name'] = $spacer.$result['meta_title'];
        		$this->_cmscategory_count++;
        	}
        }
    }
    public function getXipBlogcategory($id_category_group = 0)
    {
    	if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
    	{
	    	if(class_exists("xipcategoryclass"))
	    	{
	    		if($id_category_group == 0)
	    		{
	    			$level_depth = 5;
	    		}else{
	    			$level_depth = 9;
	    		}
	    		$spacer = str_repeat('&nbsp;',1*$level_depth);
	    		$all_categories = xipcategoryclass::GetCategories('category',$id_category_group);
	    		if(isset($all_categories) && !empty($all_categories)){
	    			$results = array();
	    			foreach ($all_categories as $all_category) {
	    				$this->_blogcategory_nested[$this->_blogcategory_count]['id'] = $all_category['id_xipcategory'];
	    				$this->_blogcategory_nested[$this->_blogcategory_count]['name'] = $spacer.$all_category['name'];
	    				$this->_blogcategory_count++;
	    				$this->getXipBlogcategory($all_category['id_xipcategory']);
	    			}
	    		}
	    		return $this->_blogcategory_nested;
	    	}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
    public function getXipBlogPosts()
    {
    	$results = array();
    	if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
    	{
	    	if(class_exists("xippostsclass"))
	    	{
	    		$all_posts = xippostsclass::GetRecentPosts(50);
	    		if(isset($all_posts) && !empty($all_posts))
	    		{
	    			$i = 0;
	    			foreach ($all_posts as $all_post) {
	    				$results[$i]['id'] = $all_post['id_xipposts'];
	    				$results[$i]['name'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$all_post['post_title'];
	    				$i++;
	    			}
	    		}
	    	}
	    }
	    return $results;
    }
    public function getAllShopPages()
    {
    	$res = array();
    	$shops = Shop::getShops();
    	if(isset($shops)){
    		$i=0;
    		foreach($shops as $shop){
    		    $res[$i]['id'] = $shop['id_shop'];
    		    $res[$i]['name'] = 'Shop : '.ucwords($shop['name']);
    		    $i++;
    		}
    	}
    	return $res;
    }
    public function getAllPages(){

    	$controllers = Dispatcher::getControllers(_PS_FRONT_CONTROLLER_DIR_);
    	$res = array();
	    if(isset($controllers)){
	        ksort($controllers);
	    }
    	if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
    	{
	    	$res[0]['id'] = 'xipblog';
	    	$res[0]['name'] = 'Page : Xpert Blog';
	    	$i = 1;
	    }else{
	    	$i = 0;
	    }
    	if(isset($controllers)){
    		foreach($controllers as $r => $v){
    		    $res[$i]['id'] = $r;
    		    $res[$i]['name'] = 'Page : '.ucwords($r);
    		    $i++;
    		}
    	}
    	return $res;
    }
    public function getAllModulesPages(){
    	$controllers = Dispatcher::getModuleControllers('front');
    	$res = array();
	    if(isset($controllers)){
	        ksort($controllers);
	    }
    	if(isset($controllers)){
    		foreach ($controllers as $module => $modules_controllers) {
    			if($module != 'xipblog'){
					$i = 0;
				    foreach ($modules_controllers as $cont) {
				    	$res[ucwords($module)][$i]['id'] = $module.'-'.$cont;
				    	$res[ucwords($module)][$i]['name'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.ucwords($cont);
				    	$i++;
				    }
    			}
    		}
    	}
    	return $res;
    }
    public function getProducts(){
    	$res = array();
    	$id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT p.`id_product`, pl.`name`FROM `'._DB_PREFIX_.'product` p '.Shop::addSqlAssociation('product', 'p').'
	            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').') WHERE pl.`id_lang` = '.(int)$id_lang;
	    $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	    if(isset($products) && !empty($products)){
	    	$i = 0;
	    	foreach ($products as $product) {
	    		$res[$i]['id'] = $product['id_product'];
	    		$res[$i]['name'] = $product['name'];
	    		$i++;
	    	}
	    }
	    return $res;
    }
    public function getHooks(){
    	$res = array();
    	$id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT `name` FROM `'._DB_PREFIX_.'hook`';
	    $hooks = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	    if(isset($hooks) && !empty($hooks)){
	    	$i = 0;
	    	foreach ($hooks as $hook) {
	    		$res[$i]['id'] = $hook['name'];
	    		$res[$i]['name'] = $hook['name'];
	    		$i++;
	    	}
	    }
	    return $res;
    }
    public static function getCMSCategories($id_parent = 0)
    {
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
		$results = array();
        $query = 'SELECT * FROM `'._DB_PREFIX_.'cms_category` pb INNER JOIN `'._DB_PREFIX_.'cms_category_lang` pbl ON (pb.`id_cms_category` = pbl.`id_cms_category` AND pbl.`id_lang` = '.$id_lang.' AND pbl.`id_shop` = '.$id_shop.') INNER JOIN `'._DB_PREFIX_.'cms_category_shop` pbs ON (pb.`id_cms_category` = pbs.`id_cms_category` AND pbs.`id_shop` = '.$id_shop.') WHERE pb.`id_parent` = '.(int)$id_parent;
        $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        return $results;
    }
	public function getContent()
	{
		$this->Register_Tabs();
		$this->context->controller->addJqueryUi('ui.widget');
		$this->context->controller->addJqueryPlugin('tagify');  
		$this->context->controller->addJqueryPlugin('colorpicker');  
		$this->context->controller->addJqueryUi(array('ui.sortable'));
		$this->_postProcess();
			$this->_html .= $this->StatusMessage();
		if(Tools::getValue("updateitem".$this->name) && Tools::getValue("id_".$this->name."item")){
			$this->_html .= $this->renderAddItemForm();
		}elseif(Tools::getValue("update".$this->name) && Tools::getValue("id_".$this->name)){
			$this->_html .= $this->renderAddForm();
		}elseif(Tools::getValue("views".$this->name)){
			$this->_html .= $this->renderItemList();
			$this->_html .= $this->headerItemHTML();
		}elseif(Tools::getValue("additem".$this->name)){
			$this->_html .= $this->renderAddItemForm();
		}elseif(Tools::getValue("add".$this->name)){
			$this->_html .= $this->renderAddForm();
		}else{
			$this->_html .= $this->renderList();
			$this->_html .= $this->headerHTML();
		}
		$this->_html .= $this->headerJSheaderJS();
		return $this->_html;
	}
	public function StatusMessage()
	{
		$html = '';
		if(Tools::getValue("withsuccess") == 1){
			$html = $this->displayConfirmation($this->l("Successfully Updated Menu. Please Add or Edit Menu Item to This Menu!"));
		}elseif(Tools::getValue("witherror") == 1){
			$html = $this->displayError($this->l(" SomeThing Wrong To Added/Update Menu Item! Please try Again! "));
		}elseif(Tools::getValue("withsuccess") == 2){
			$html = $this->displayConfirmation($this->l("Successfully Changed Status! "));
		}elseif(Tools::getValue("witherror") == 2){
			$html = $this->displayError($this->l(" SomeThing Wrong To Change Status! Please try Again! "));
		}elseif(Tools::getValue("withsuccess") == 3){
			$html = $this->displayConfirmation($this->l("Successfully Deleted Menu Item!"));
		}elseif(Tools::getValue("witherror") == 3){
			$html = $this->displayError($this->l(" SomeThing Wrong To Delete Menu Item! Please try Again! "));
		}elseif(Tools::getValue("withsuccess") == 4){
			$html = $this->displayConfirmation($this->l("Successfully Added/Updated Menu Item!"));
		}elseif(Tools::getValue("witherror") == 4){
			$html = $this->displayError($this->l(" SomeThing Wrong To Added/Updated Menu Item! Please try Again! "));
		}
		return $html;
	}
	private function _postProcess()
	{
		$errors = array();
		$content_group = array();
		$this->context->controller->addJqueryPlugin('select2');
		$shop_context = Shop::getContext();
		if(Tools::isSubmit('submit'.$this->name.'item')){
			// start testing system
				$i = 0;
				foreach($_POST[$this->name] as $xkey => $kevalue){
					if(is_int($xkey)){
						$ij = 0;
						foreach ($kevalue as $kes => $valu) {
							if(isset($valu) && !empty($valu)){
								if(is_int($kes)){
									$content_group['rowcolumnsetting'][$i]['columnvalues'][$ij] = $valu;
									$ij++;
								}else{
									$content_group['rowcolumnsetting'][$i][$kes] = $valu;
								}
							}
						}
						$i++;
					}else{
						$content_group['mainrowsetting'][$xkey] = $kevalue;
					}
				}
			// end testing system
			$id = Tools::getValue('id_'.$this->name.'item');
			$id_parent = Tools::getValue('id_'.$this->name);
			if(isset($id) && !empty($id)){
				$xprtobj = new xprtmegamenuitemclass($id);
			}else{
				$xprtobj = new xprtmegamenuitemclass();
			}
			// start save
			$languages = Language::getLanguages(false);
			$fields_form = $this->initItemFieldsForm();
			if(isset($fields_form['form']['input']) && !empty($fields_form['form']['input'])){

				foreach ($fields_form['form']['input'] as $field) {
					if(isset($field['type']) && $field['type'] == 'file'){
						if(isset($_FILES[$field['name']]) && isset($_FILES[$field['name']]['tmp_name']) && !empty($_FILES[$field['name']]['tmp_name'])){
							$xprtobj->{$field['name']} = $this->processImage($field['name']);
						}else{
							$xprtobj->{$field['name']} = (isset($xprtobj->{$field['name']}) && !empty($xprtobj->{$field['name']})) ? $xprtobj->{$field['name']} : "";
						}
					}else{
						// start
						if(isset($field['group']) && $field['group'] == "content"){
							// start
							if(isset($field['type']) && $field['type'] == 'file'){
								$content_group[$field['name']] = $this->processImage($field['name']);
								if(isset($_FILES[$field['name']]) && isset($_FILES[$field['name']]['tmp_name']) && !empty($_FILES[$field['name']]['tmp_name'])){
									$content_group[$field['name']] = $this->processImage($field['name']);
								}else{
									if(isset($xprtobj->gcontent) && !empty($xprtobj->gcontent)){
										$contentdecode = Tools::jsonDecode($xprtobj->gcontent);
										$content_group[$field['name']] = (isset($contentdecode[$field['name']]) && !empty($contentdecode[$field['name']])) ? $contentdecode[$field['name']] : "";
									}else{
										$content_group[$field['name']] = "";
									}
								}
							}else{
								if(isset($field['lang']) && $field['lang'] == true){
									foreach ($languages as $lang)
									{
										if(isset($field['name']) && !empty($field['name'])){
											$content_group[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
										}
									}
								}else{
									if(isset($field['name']) && !empty($field['name'])){
										$content_group[$field['name']] = Tools::getValue($field['name']);
									}
								}
							}
							// end
						}else{
							if(isset($field['lang']) && $field['lang'] == true){
								foreach ($languages as $lang)
								{
									if(isset($field['name']) && !empty($field['name'])){
										$xprtobj->{$field['name']}[$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
									}
								}
							}else{
								if(isset($field['name']) && !empty($field['name'])){
									$xprtobj->{$field['name']} = Tools::getValue($field['name']);
								}
							}
						}
						// End
					}
				}
			}
			// end save
			if(isset($content_group) && !empty($content_group)){
				$xprtobj->gcontent = json_encode($content_group,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
			}else{
				$xprtobj->gcontent = "";
			}

			$res = $xprtobj->save();
			if($res){
				$link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&id_".$this->name."=".$xprtobj->id_xprtmegamenu."&updateitem".$this->name."=1&id_".$this->name."item=".$id."&withsuccess=4";
				// $link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&id_".$this->name."=".$xprtobj->id_xprtmegamenu."&views".$this->name."=1&withsuccess=4";
			}else{
				// $link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&id_".$this->name."=".$xprtobj->id_xprtmegamenu."&views".$this->name."=1&witherror=4";
				$link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&id_".$this->name."=".$xprtobj->id_xprtmegamenu."&updateitem".$this->name."=1&id_".$this->name."item=".$id."&witherror=4";
			}
			Tools::redirectAdmin($link_uri);
		}elseif(Tools::isSubmit('submit'.$this->name)){
			$id = Tools::getValue('id_'.$this->name);
			if(isset($id) && !empty($id)){
				$xprtobj = new self::$classname($id);
			}else{
				$xprtobj = new self::$classname();
			}
			// start save
			$languages = Language::getLanguages(false);
			$fields_form = $this->initFieldsForm();
			if(isset($fields_form['form']['input']) && !empty($fields_form['form']['input'])){
				$content_group = array();
				foreach ($fields_form['form']['input'] as $field) {
					if(isset($field['type']) && $field['type'] == 'file'){
						if(isset($_FILES[$field['name']]) && isset($_FILES[$field['name']]['tmp_name']) && !empty($_FILES[$field['name']]['tmp_name'])){
							$xprtobj->{$field['name']} = $this->processImage($field['name']);
						}else{
							$xprtobj->{$field['name']} = (isset($xprtobj->{$field['name']}) && !empty($xprtobj->{$field['name']})) ? $xprtobj->{$field['name']} : "";
						}
					}else{
						// start
						if(isset($field['group']) && $field['group'] == "content"){
							// start
							if(isset($field['type']) && $field['type'] == 'file'){
								$content_group[$field['name']] = $this->processImage($field['name']);
								if(isset($_FILES[$field['name']]) && isset($_FILES[$field['name']]['tmp_name']) && !empty($_FILES[$field['name']]['tmp_name'])){
									$content_group[$field['name']] = $this->processImage($field['name']);
								}else{
									if(isset($xprtobj->gcontent) && !empty($xprtobj->gcontent)){
										$contentdecode = Tools::jsonDecode($xprtobj->gcontent);
										$content_group[$field['name']] = (isset($contentdecode[$field['name']]) && !empty($contentdecode[$field['name']])) ? $contentdecode[$field['name']] : "";
									}else{
										$content_group[$field['name']] = "";
									}
								}
							}else{
								if(isset($field['lang']) && $field['lang'] == true){
									foreach ($languages as $lang)
									{
										if(isset($field['name']) && !empty($field['name'])){
											$content_group[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
										}
									}
								}else{
									if(isset($field['name']) && !empty($field['name'])){
										$content_group[$field['name']] = Tools::getValue($field['name']);
									}
								}
							}
							// end
						}else{
							if(isset($field['lang']) && $field['lang'] == true){
								foreach ($languages as $lang)
								{
									if(isset($field['name']) && !empty($field['name'])){
										$xprtobj->{$field['name']}[$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
									}
								}
							}else{
								if(isset($field['name']) && !empty($field['name'])){
									$xprtobj->{$field['name']} = Tools::getValue($field['name']);
								}
							}
						}
						// End
					}
				}
			}
			// end save
			if(isset($content_group) && !empty($content_group)){
				$xprtobj->gcontent = json_encode($content_group,JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
			}else{
				$xprtobj->gcontent = "";
			}
			$hookname = Tools::getValue("hook");
			if (Validate::isHookName($hookname)) {
				if(!$this->isRegisteredInHook($hookname)){
					$this->registerHook($hookname);
				}
			}
			$res = $xprtobj->save();
			if(isset($xprtobj->id) && !empty($xprtobj->id)){
				$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully Updated')) : $this->displayError($this->l('The Values could not be Updated.')));
				$link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&id_".$this->name."=".$xprtobj->id."&views".$this->name."=1&withsuccess=1";
				Tools::redirectAdmin($link_uri);
			}else{
				$link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&witherror=1";
				Tools::redirectAdmin($link_uri);
			}
		}
		elseif (Tools::isSubmit('changeItemStatus') && Tools::getValue('id_'.$this->name.'item'))
		{
			$xprtobj = new xprtmegamenuitemclass((int)Tools::getValue('id_'.$this->name.'item'));
			if ($xprtobj->active == 0)
				$xprtobj->active = 1;
			else
				$xprtobj->active = 0;
			$res = $xprtobj->update();
			if($res){
				$link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&id_".$this->name."=".$xprtobj->id_xprtmegamenu."&views".$this->name."=1&withsuccess=2";
			}else{
				$link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&id_".$this->name."=".$xprtobj->id_xprtmegamenu."&views".$this->name."=1&witherror=2";
			}
			Tools::redirectAdmin($link_uri);
		}
		elseif (Tools::isSubmit('changeStatus') && Tools::isSubmit('id_'.$this->name))
		{
			$xprtobj = new self::$classname((int)Tools::getValue('id_'.$this->name));
			if ($xprtobj->active == 0)
				$xprtobj->active = 1;
			else
				$xprtobj->active = 0;
			$res = $xprtobj->update();
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully updated')) : $this->displayError($this->l('The Values could not be updated.')));
		}
		elseif (Tools::isSubmit('deleteitem_id_'.$this->name))
		{
			$xprtobj = new xprtmegamenuitemclass((int)Tools::getValue('deleteitem_id_'.$this->name));
			$res = $xprtobj->delete();
			if($res){
				$link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&id_".$this->name."=".$xprtobj->id_xprtmegamenu."&views".$this->name."=1&withsuccess=3";
			}else{
				$link_uri = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&id_".$this->name."=".$xprtobj->id_xprtmegamenu."&views".$this->name."=1&witherror=3";
			}
			Tools::redirectAdmin($link_uri);
		}
		elseif (Tools::isSubmit('delete_id_'.$this->name))
		{
			$xprtobj = new self::$classname((int)Tools::getValue('delete_id_'.$this->name));
			$res = $xprtobj->delete();
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully Deleted')) : $this->displayError($this->l('The Values could not be Deleted.')));
		}
		elseif (Tools::isSubmit('updateitemblocklistsPosition'))
		{
			$itemblock_lists = array();
			if (Tools::getValue('itemblock_lists'))
			{
				$itemblock_lists = Tools::getValue('itemblock_lists');
				foreach ($itemblock_lists as $position => $id_i)
				{
					Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'xprtmegamenuitem` SET `position` = '.(int)$position.' WHERE `id_xprtmegamenuitem` = '.(int)$id_i
					);
				}
				die(1);
			}
		}
		elseif (Tools::isSubmit('updateblocklistsPosition'))
		{
			$block_lists = array();
			if (Tools::getValue('block_lists'))
			{
				$block_lists = Tools::getValue('block_lists');
				foreach ($block_lists as $position => $id_i)
				{
					Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.self::$tablename.'` SET `position` = '.(int)$position.' WHERE `id_'.self::$tablename.'` = '.(int)$id_i
					);
				}
				die(1);
			}
		}
	}
	public function headerItemHTML()
	{
		if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name)
			return;
		$links = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&updateitemblocklistsPosition=1";
		$this->context->controller->addJqueryUI('ui.sortable');
		$html = '<script type="text/javascript">
			$(function() {
				var $mySlides = $("#itemblock_lists");
				$mySlides.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize");
						$.post("'.$links.'", order);
						}
					});
				$mySlides.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
			});
		</script>';
		return $html;
	}
	public function headerJSheaderJS(){
		$html = '<script>$(document).ready(function(){
			$(".page-bar.toolbarBox").hide();
		});</script>';
		return $html;
	}
	public function headerHTML()
	{
		if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name)
			return;
		$links = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&updateblocklistsPosition=1";
		$this->context->controller->addJqueryUI('ui.sortable');
		$html = '<script type="text/javascript">
			$(function() {
				var $mySlides = $("#block_lists");
				$mySlides.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize");
						$.post("'.$links.'", order);
						}
					});
				$mySlides.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
			});
		</script>';
		return $html;
	}
	public function getblock_lists($active = null)
	{
		if(!$this->context)
			$this->context = Context::getContext();
		$id_shop = (int)$this->context->shop->id;
		$id_lang = (int)$this->context->language->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.self::$tablename.'` WHERE `id_shop` = '.$id_shop;
        $sql .= ' ORDER BY `position` ASC ';
        $results = Db::getInstance()->executeS($sql);
        if(isset($results) && !empty($results)){
        	foreach ($results as &$result) {
        		$result['id'] = $result['id_'.self::$tablename];
        	}
        }
        return $results;
	}
	public function getblock_Itemlists($id = null)
	{
		if(empty($id))
			return false;
		if(!$this->context)
			$this->context = Context::getContext();
		$id_shop = (int)$this->context->shop->id;
		$id_lang = (int)$this->context->language->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtmegamenuitem` xmm INNER JOIN `'._DB_PREFIX_.'xprtmegamenuitem_lang` xmml ON (xmm.`id_xprtmegamenuitem` = xmml.`id_xprtmegamenuitem` AND xmml.`id_lang` = '.$id_lang.') WHERE xmm.id_xprtmegamenu = '.$id.' ORDER BY `position` ASC ';
        $results = Db::getInstance()->executeS($sql);
        if(isset($results) && !empty($results)){
        	foreach ($results as &$result) {
        		$result['id'] = $result['id_xprtmegamenuitem'];
        	}
        }
        return $results;
	}
	public function displayStatus($id_i, $active)
	{
		$title = ((int)$active == 0 ? $this->l('Disabled') : $this->l('Enabled'));
		$icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
		$class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
		$html = '<a class="btn '.$class.'" href="'.AdminController::$currentIndex.
			'&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&changeStatus&id_'.$this->name.'='.(int)$id_i.'" title="'.$title.'"><i class="'.$icon.'"></i></a>';
		return $html;
	}
	public function displayItemStatus($id_i, $active)
	{
		$title = ((int)$active == 0 ? $this->l('Disabled') : $this->l('Enabled'));
		$icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
		$class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
		$html = '<a class="btn '.$class.'" href="'.AdminController::$currentIndex.
			'&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&changeItemStatus&id_'.$this->name.'item='.(int)$id_i.'" title="'.$title.'"><i class="'.$icon.'"></i></a>';
		return $html;
	}
	public function BlockExists($id_i)
	{
		$req = 'SELECT hs.`id_'.self::$tablename.'` as id_'.self::$tablename.'
				FROM `'._DB_PREFIX_.self::$tablename.'` hs
				WHERE hs.`id_'.self::$tablename.'` = '.(int)$id_i;
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);
		return ($row);
	}
	public function BlockItemExists($id_i)
	{
		$req = 'SELECT hs.`id_'.self::$tablename.'item` as id_'.self::$tablename.'item
				FROM `'._DB_PREFIX_.self::$tablename.'item` hs
				WHERE hs.`id_'.self::$tablename.'item` = '.(int)$id_i;
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);
		return ($row);
	}
	public function renderList()
	{
		$block_lists = $this->getblock_lists();
		foreach ($block_lists as $key => $block)
		{
			$block_lists[$key]['status'] = $this->displayStatus($block['id_'.$this->name], $block['active']);
		}
		$this->context->smarty->assign(
			array(
				'link' => $this->context->link,
				'xprttablename' => self::$tablename,
				'xprtclassname' => self::$classname,
				'xprtmodulename' => $this->name,
				'block_lists' => $block_lists,
				'image_baseurl' => $this->_path.'images/'
			)
		);
		return $this->display(__FILE__, 'list.tpl');
	}
	public function renderItemList()
	{
		$id = Tools::getValue('id_'.$this->name);
		if(empty($id))
			return false;
		$block_lists = $this->getblock_Itemlists($id);
		foreach ($block_lists as $key => $block)
		{
			$block_lists[$key]['status'] = $this->displayItemStatus($block['id_'.$this->name.'item'], $block['active']);
		}
		$this->context->smarty->assign(
			array(
				'identity' => $id,
				'link' => $this->context->link,
				'xprttablename' => self::$tablename,
				'xprtclassname' => self::$classname,
				'xprtmodulename' => $this->name,
				'block_lists' => $block_lists,
				'image_baseurl' => $this->_path.'images/'
			)
		);
		return $this->display(__FILE__, 'itemlist.tpl');
	}
	public static function layout_style_val()
	{
	    $layout_style_val = array();
	    $theme_path =  _PS_THEME_DIR_.'modules/'.self::$module_name.'/views/templates/front/layout/';
	    $mod_path =  _PS_MODULE_DIR_.self::$module_name.'/views/templates/front/layout/';
	    if(file_exists($theme_path.'default.tpl')){
	        $file_lists = array_diff(scandir($theme_path), array('..', '.'));
	    }else{
		    $file_lists = array_diff(scandir($mod_path), array('..', '.'));
	    }
	    if(isset($file_lists) && !empty($file_lists)){
	        $i = 0;
	        foreach ($file_lists as $key => $value) {
	            $layout_style_val[$i]['id'] = str_replace(".tpl","",$value);
	            $layout_style_val[$i]['name'] = ucwords(str_replace(".tpl","",$value))." Style";
	            $i++;
	        }
	    }
		return $layout_style_val;
	}
	public function GetIcons()
	{
		$icon_list = array();
		if(file_exists(dirname(__FILE__).$this->icon_url))
			require_once(dirname(__FILE__).$this->icon_url);
        return $icon_list;
	}
	public static function AllPageExceptions()
	{
	    $id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT p.`id_product`, pl.`name`
	            FROM `'._DB_PREFIX_.'product` p
	            '.Shop::addSqlAssociation('product', 'p').'
	            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
	            WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
	    $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	    $categories =  Category::getCategories($id_lang,true,false);
	    $controllers = Dispatcher::getControllers(_PS_FRONT_CONTROLLER_DIR_);
	    if(isset($controllers)){
	        ksort($controllers);
	    }
	    $Manufacturers =  Manufacturer::getManufacturers();
	    $Suppliers =  Supplier::getSuppliers();
	    $rslt = array();
	    $rslt[0]['id'] = 'all_page';
	    $rslt[0]['name'] = 'All Pages';
	    $i = 1;
	    if(isset($controllers))
	        foreach($controllers as $r => $v){
	            $rslt[$i]['id'] = $r;
	            $rslt[$i]['name'] = 'Page : '.ucwords($r);
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'man_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'sup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier : '.$r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'cat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category : '.$cats['name'];
	            $i++;
	        }
	    if(isset($products))
	        foreach($products as $r){
	            $rslt[$i]['id'] = 'prd_'.$r['id_product'];
	            $rslt[$i]['name'] = 'Product : '. $r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'prdcat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category Product: '.$cats['name'];
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'prdman_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer Product : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'prdsup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier Product : '.$r['name'];
	            $i++;
	        }
	    return $rslt;
	}
	public function SimpleProductS()
	{
	    $id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT p.`id_product`, pl.`name`
	            FROM `'._DB_PREFIX_.'product` p
	            '.Shop::addSqlAssociation('product', 'p').'
	            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
	            WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
	    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}
	public function AllProductS()
	{
	    $rs = array();
	    $rslt = array();
	    $rs =  $this->SimpleProductS();
	    $i = 0;
	    foreach($rs as $r){
	        $rslt[$i]['id'] = 'prd_'.$r['id_product'];
	        $rslt[$i]['name'] = $r['name'];
	        $i++;
	    }
	    return $rslt;
	}
	public static function getPagesLink($page_name = null)
	{
		if($page_name == null){
			return '#';
		}
		if($page_name == 'xipblog'){
			$link = '#';
			if(Module::isInstalled('xipblog') && Module::isEnabled('xipblog'))
			{
				if(class_exists('xipblog')){
					$link = xipblog::XipBlogLink();
				}
			}
		}else{
			$link = self::GetLinkObject()->getPageLink($page_name);
		}
		return $link;
	}
	public static function getPagesLevel($page_name = null)
	{
		if($page_name == null){
			return 'No Level';
		}
		$pages_level = array();
		$file = '/data/level/default.php';
		$iso_code = Context::getContext()->language->iso_code;
		if(file_exists(dirname(__FILE__).'/data/level/'.$iso_code.'.php')){
			$file = '/data/level/'.$iso_code.'.php';
		}
		if(file_exists(dirname(__FILE__).$file)){
			require_once(dirname(__FILE__).$file);
		}
		if(isset($pages_level[$page_name]) && !empty($pages_level[$page_name])){
			return $pages_level[$page_name];
		}else{
			return ucwords($page_name);
		}
	}
	public static function getModulePagesLink($module = null)
	{
		if($module == null){
			return '#';
		}
		if(isset($module) && !empty($module)){
			$moduleData = @explode('-',$module);
			if(isset($moduleData[0]) && !empty($moduleData[0]) && isset($moduleData[1]) && !empty($moduleData[1])){
				$module_name = $moduleData[0];
				$module_controller = $moduleData[1];
				$link = self::GetLinkObject()->getModuleLink($module_name,$module_controller);
				return $link;
			}else{
				return '#';
			}
		}else{
			return '#';
		}
	}
	public static function getModulePagesLevel($module = null)
	{
		if($module == null){
			return '';
		}
		if(isset($module) && !empty($module)){
			$moduleData = @explode('-',$module);
			if(isset($moduleData[0]) && !empty($moduleData[0]) && isset($moduleData[1]) && !empty($moduleData[1])){
				$module_name = $moduleData[0];
				$module_controller = $moduleData[1];
				return ucwords($module_controller); 
			}else{
				return '';
			}
		}else{
			return '';
		}
	}
	public function initItemFieldsForm()
	{
		$id_parent = Tools::getValue('id_'.$this->name);
		$menu_list_href = $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$item_list_href = $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&views'.$this->name.'=1&id_'.$this->name.'='.$id_parent;
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Menu Item Block'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Menu level'),
						'name' => 'name',
						// 'class' => 'fixed-width-lg',
						'lang' => true,
						'desc' => 'Please Enter Menu Level',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Menu Badge'),
						'name' => 'badge',
						// 'class' => 'fixed-width-lg',
						'lang' => true,
						'desc' => 'Please Enter Menu Badge',
					),
			        array(
						'type' => 'xprticon_type',
						'label' => $this->l('Menu Icon'),
						'name' => 'icon',
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Icon Position'),
					    'name' => 'icon_pos',
					    'options' => array(
					        'query' => array(
					            array(
					                'id'=>'left',
					                'name'=>'Left Position',
					            ),
					            array(
					                'id'=>'right',
					                'name'=>'Right Position',
					            ),
					            array(
					                'id'=>'top',
					                'name'=>'Top Position',
					            ),
					        ),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Link Type'),
					    'name' => 'link_type',
					    'options' => array(
					        'query' => $this->GetParentLinkType(),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
						'type' => 'text',
						'label' => $this->l('External Link'),
						'name' => 'external_link',
						'class' => 'external_link',
						'group' => 'content',
						'desc' => 'Please Enter External Url',
					),
					array(
					    'type' => 'systemlink',
					    'label' => $this->l('System Link'),
					    'name' => 'system_link',
					    'class' => 'system_link',
						'group' => 'content',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Is SubMenu'),
						'name' => 'is_submenu',
						'class' => 'is_submenu',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'is_submenu_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'is_submenu_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Sub Menu Level'),
					    'name' => 'submenu_level',
					    'options' => array(
					        'query' => array(
					            array(
					                'id'=>'one',
					                'name'=>'One Level',
					            ),
					            array(
					                'id'=>'multi',
					                'name'=>'Multi Level',
					            ),
					        ),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Select Sub Menu Link Type'),
					    'name' => 'submenu_type',
					    'group' => 'content',
					    'options' => array(
					        'query' => $this->GetSubLinkType(),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
						'type' => 'text',
						'label' => $this->l('External Url Level'),
						'name' => 'sub_external_level',
						'class' => 'sub_external_link',
						'group' => 'content',
						'lang' => true,
						'desc' => 'Please Enter External Url Level',
					),
					array(
						'type' => 'text',
						'label' => $this->l('External Url'),
						'name' => 'sub_external_link',
						'class' => 'sub_external_link',
						'group' => 'content',
						'lang' => true,
						'desc' => 'Please Enter External Url',
					),
					array(
					    'type' => 'systemlink',
					    'label' => $this->l('System Link'),
					    'name' => 'sub_system_link',
					    'class' => 'sub_system_link',
						'group' => 'content',
						'is_multiple' => true,
					),
					array(
						'type' => 'textarea',
						'label' => $this->l('HTML Link'),
						'name' => 'sub_html_link',
						'lang' => true,
						'rows' => '12',
						'group' => 'content',
						'cols' => '20',
                    	'class' => 'rte sub_html_link',
                    	'autoload_rte' => true,
						'desc' => 'Please Enter HTML Link Block',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Is Mega Menu'),
						'name' => 'is_megamenu',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'is_megamenu_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'is_megamenu_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Is Full Width'),
						'name' => 'is_fullwidth',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'is_fullwidth_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'is_fullwidth_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => 'text',
						'label' => $this->l('MegaMenu Width'),
						'name' => 'megawidth',
						'class' => 'megawidth fixed-width-lg',
						'group' => 'content',
						'desc' => 'Please Enter width value. (ex:600px)',
					),
					array(
						'type' => 'rowset',
						'label' => $this->l('MegaMenu Settings'),
						'name' => 'megasetting',
						'class' => 'megasetting fixed-width-lg',
						'group' => 'content',
						'desc' => 'Please Click the setting icon change the popup settings.',
					),
					array(
						'type' => 'rowcolumntype',
						'label' => $this->l('rowcolumntype Type'),
						'name' => 'rowcolumntype',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Custom Class'),
						'name' => 'custom_class',
						'class' => 'fixed-width-lg',
						'desc' => 'Please Enter Custom Class',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enabled'),
						'name' => 'active',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => 'hidden',
						'name' => 'id_xprtmegamenu',
					),
				),
				'buttons' => array(
					array(
						'href' => $menu_list_href,
						'id' => 'Back_Menu_List',
						'class' => 'Back_Menu_List',
						'icon' => 'process-icon-back',
						'title' => 'Back Menu List',
					),
					array(
						'href' => $item_list_href,
						'id' => 'Back_Item_List',
						'class' => 'Back_Item_List',
						'icon' => 'process-icon-back',
						'title' => 'Back Item List',
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				),
			),
		);
		return $fields_form;
	}
	public function initFieldsForm()
	{
		$menu_list_href = $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Menu Block'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Menu Name'),
						'name' => 'menu_name',
						'desc' => 'Please Enter Menu Name',
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Menu Display Style'),
					    'name' => 'is_horizontal',
					    'options' => array(
					        'query' => array(
					            array(
					                'id'=>'yes',
					                'name'=>'Horizontal',
					            ),
					            array(
					                'id'=>'no',
					                'name'=>'Vertical',
					            ),
					        ),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Menu Sticky Style'),
					    'name' => 'is_sticky',
					    'options' => array(
					        'query' => array(
					            array(
					                'id'=>'yes',
					                'name'=>'Sticky Style',
					            ),
					            array(
					                'id'=>'no',
					                'name'=>'Normal Style',
					            ),
					        ),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Select Hook'),
					    'name' => 'hook',
					    'options' => array(
					        'query' => $this->GetHookarray(),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
					    'type' => 'selecttwotype',
					    'label' => $this->l('Which Page You Want to Display'),
					    'placeholder' => $this->l('Please Type Your Controller Name.'),
					    'initvalues' => self::AllPageExceptions(),
					    'name' => 'pages',
					    'desc' => $this->l('Please Type Your Specific Page Name,Category name,Product Name,For All Product specific Category select category product: category name.<br>For showing All page Type: All Page. For Home Page Type:index.')
					),
					array(
						'type' => 'text',
						'label' => $this->l('Menu Custom Class'),
						'name' => 'custom_class',
						'desc' => 'Please Enter Custom Class',
					),
					array(
						'type' => 'textarea',
						'label' => $this->l('Custom CSS'),
						'name' => 'custom_css',
						'rows' => '12',
						'cols' => '20',
						'desc' => 'Please Write Your Own Style',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enabled'),
						'name' => 'active',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
				),
				'buttons' => array(
					array(
						'href' => $menu_list_href,
						'id' => 'Back_Menu_List',
						'class' => 'Back_Menu_List',
						'icon' => 'process-icon-back',
						'title' => 'Back Menu List',
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				),
			),
		);
		return $fields_form;
	}
	public function renderAddItemForm()
	{
		$fields_form = $this->initItemFieldsForm();
		$rowcolumnsettings = array();
		$mainrowsettings = array();
		if (Tools::isSubmit('id_'.$this->name.'item') && $this->BlockItemExists((int)Tools::getValue('id_'.$this->name.'item')))
		{
			$xprtobj = new xprtmegamenuitemclass((int)Tools::getValue('id_'.$this->name.'item'));
			$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_'.$this->name.'item');
			if(isset($xprtobj->gcontent) && !empty($xprtobj->gcontent)){
				$gcon = Tools::jsonDecode($xprtobj->gcontent);
				if(isset($gcon->rowcolumnsetting) && !empty($gcon->rowcolumnsetting)){
					$rowcolumnsettings = $gcon->rowcolumnsetting;
				}
				if(isset($gcon->mainrowsetting) && !empty($gcon->mainrowsetting)){
					$mainrowsettings = $gcon->mainrowsetting;
				}
			}
		}
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		// $helper->back_url = "http://google.com/";
		$helper->show_cancel_button = false;
		$helper->table = "xprtmegamenuitem";
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->module = $this;
		$helper->identifier = "id_xprtmegamenuitem";
		$helper->submit_action = 'submit'.$this->name.'item';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&itemview=1';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$icon_list = $this->GetIcons();
		$xprt_categories = $this->getCategories();
		$xprt_manufacturers =  Manufacturer::getManufacturers();
		$xprt_suppliers =  Supplier::getSuppliers();
		$xprt_cmscategories =  $this->getCMSCategoriesList();
		
		$xprt_products =  $this->getProducts();
		$xprt_modulepages =  $this->getAllModulesPages();
		$getxipblogcategory = $this->getXipBlogcategory();
		$getxipblogposts = $this->getXipBlogPosts();
		$xprt_pages =  $this->getAllPages();
		$xprt_shoppages =  $this->getAllShopPages();
		$xprt_hooks =  $this->getHooks();
		if(Shop::isFeatureActive()){
			$xprtmultishop = 'enable';
		}else{
			$xprtmultishop = 'disable';
		}
		$helper->tpl_vars = array(
			'xprt_imagelink' => _MODULE_DIR_.$this->name.'/images/',
			'base_url' => $this->context->shop->getBaseURL(),
            'icon_list' => $icon_list,
            'rowcolumnsettings' => $rowcolumnsettings,
            'mainrowsettings' => $mainrowsettings,
            'xprt_hooks' => $xprt_hooks,
            'xprt_categories' => $xprt_categories,
            'xprt_manufacturers' => $xprt_manufacturers,
            'xprt_suppliers' => $xprt_suppliers,
            'xprt_cmscategories' => $xprt_cmscategories,
            'xprt_products' => $xprt_products,
            'xprt_pages' => $xprt_pages,
            'xprtmultishop' => $xprtmultishop,
            'xprt_shoppages' => $xprt_shoppages,
            'xprt_modulepages' => $xprt_modulepages,
            'getxipblogcategory' => $getxipblogcategory,
            'getxipblogposts' => $getxipblogposts,
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $this->getAddItemFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
			'image_baseurl' => $this->_path.'images/',
			'controllers_link' => $this->context->link->getAdminLink("Adminxprtmegamenu"),
		);
		$helper->override_folder = '/';
		return $helper->generateForm(array($fields_form));
	}
	public function renderAddForm()
	{
		$fields_form = $this->initFieldsForm();
		if (Tools::isSubmit('id_'.$this->name) && $this->BlockExists((int)Tools::getValue('id_'.$this->name)))
		{
			$xprtobj = new self::$classname((int)Tools::getValue('id_'.$this->name));
			$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_'.$this->name);
		}
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->show_cancel_button = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->module = $this;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$icon_list = $this->GetIcons();
		$helper->tpl_vars = array(
			'base_url' => $this->context->shop->getBaseURL(),
            'icon_list' => $icon_list,
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $this->getAddFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
			'image_baseurl' => $this->_path.'images/',
			'controllers_link' => $this->context->link->getAdminLink("Adminxprtmegamenu"),
		);
		$helper->override_folder = '/';
		return $helper->generateForm(array($fields_form));
	}
	public function getAddItemFieldsValues()
	{
		$fields_form = $this->initItemFieldsForm();
		$languages = Language::getLanguages(false);
		$fields_values = array();
		if(Tools::getValue('id_'.$this->name.'item') && $this->BlockItemExists((int)Tools::getValue('id_'.$this->name.'item')))
		{
			$xprtobj = new xprtmegamenuitemclass((int)Tools::getValue('id_'.$this->name.'item'));
			$fields_values['id_'.$this->name.'item'] = (int)Tools::getValue('id_'.$this->name.'item', $xprtobj->id);
		}else{
			$xprtobj = new xprtmegamenuitemclass();
		}
		if(isset($xprtobj->gcontent) && !empty($xprtobj->gcontent)){
			$content_fields_values = Tools::jsonDecode($xprtobj->gcontent);
			if(isset($content_fields_values) && !empty($content_fields_values)){
				foreach ($content_fields_values as $content_key => $content_value) {
					if(is_object($content_value)){
						foreach ($content_value as $content_value_key => $content_value_value){
							$fields_values_temp[$content_key][$content_value_key] = $content_value_value;
						}
					}else{
						$fields_values_temp[$content_key] = Tools::getValue($content_key, $content_value);
					}
				}
			}
		}
		if(isset($fields_form['form']['input']) && !empty($fields_form['form']['input'])){
			foreach ($fields_form['form']['input'] as $field) {
				if(isset($field['expandstyle']) && $field['expandstyle'] == true){
					$fields_values["put-cls-".$field['name']] = isset($fields_values_temp["put-cls-".$field['name']]) ? $fields_values_temp["put-cls-".$field['name']] : "";
					$fields_values["put-cls-".$field['name']] = isset($fields_values_temp["put-cls-".$field['name']]) ? $fields_values_temp["put-cls-".$field['name']] : "";
					$fields_values["put-mar-".$field['name']] = isset($fields_values_temp["put-mar-".$field['name']]) ? $fields_values_temp["put-mar-".$field['name']] : "";
					$fields_values["put-pad-".$field['name']] = isset($fields_values_temp["put-pad-".$field['name']]) ? $fields_values_temp["put-pad-".$field['name']] : "";
				}
				if(isset($field['group']) && $field['group'] == "content"){
					// start
						if(isset($field['lang']) && $field['lang'] == true){
							foreach ($languages as $lang)
							{
								if(isset($field['name']) && !empty($field['name'])){
									$fields_values[$field['name']][$lang['id_lang']] = isset($fields_values_temp[$field['name']][$lang['id_lang']]) ? $fields_values_temp[$field['name']][$lang['id_lang']] : "";
								}
							}
						}else{
							if(isset($field['name']) && !empty($field['name'])){
								$fields_values[$field['name']] = isset($fields_values_temp[$field['name']]) ? $fields_values_temp[$field['name']] : "";
							}
						}
					// end
					
				}else{
					if(isset($field['lang']) && $field['lang'] == true){
						foreach ($languages as $lang)
						{
							if(isset($field['name']) && !empty($field['name'])){
								if(isset($xprtobj->{$field['name']})){
									$fields_values[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang'], $xprtobj->{$field['name']}[$lang['id_lang']]);
								}else{
									$fields_values[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
								}
							}
						}
					}else{
						if(isset($field['name']) && !empty($field['name'])){
							if(isset($xprtobj->{$field['name']})){
								$fields_values[$field['name']] = Tools::getValue($field['name'], $xprtobj->{$field['name']});
							}else{
								$fields_values[$field['name']] = Tools::getValue($field['name']);
							}
						}
					}
				}
			}
		}
		return $fields_values;
	}
	public function getAddFieldsValues()
	{
		$fields_form = $this->initFieldsForm();
		$languages = Language::getLanguages(false);
		$fields_values = array();
		if(Tools::getValue('id_'.$this->name) && $this->BlockExists((int)Tools::getValue('id_'.$this->name)))
		{
			$xprtobj = new self::$classname((int)Tools::getValue('id_'.$this->name));
			$fields_values['id_'.$this->name] = (int)Tools::getValue('id_'.$this->name, $xprtobj->id);
		}else{
			$xprtobj = new self::$classname();
		}
		if(isset($xprtobj->gcontent) && !empty($xprtobj->gcontent)){
			$content_fields_values = Tools::jsonDecode($xprtobj->gcontent);
			if(isset($content_fields_values) && !empty($content_fields_values)){
				foreach ($content_fields_values as $content_key => $content_value) {
					if(is_object($content_value)){
						foreach ($content_value as $content_value_key => $content_value_value){
							$fields_values_temp[$content_key][$content_value_key] = $content_value_value;
						}
					}else{
						$fields_values_temp[$content_key] = Tools::getValue($content_key, $content_value);
					}
				}
			}
		}
		if(isset($fields_form['form']['input']) && !empty($fields_form['form']['input'])){
			foreach ($fields_form['form']['input'] as $field) {
				if(isset($field['expandstyle']) && $field['expandstyle'] == true){
					$fields_values["put-cls-".$field['name']] = isset($fields_values_temp["put-cls-".$field['name']]) ? $fields_values_temp["put-cls-".$field['name']] : "";
					$fields_values["put-cls-".$field['name']] = isset($fields_values_temp["put-cls-".$field['name']]) ? $fields_values_temp["put-cls-".$field['name']] : "";
					$fields_values["put-mar-".$field['name']] = isset($fields_values_temp["put-mar-".$field['name']]) ? $fields_values_temp["put-mar-".$field['name']] : "";
					$fields_values["put-pad-".$field['name']] = isset($fields_values_temp["put-pad-".$field['name']]) ? $fields_values_temp["put-pad-".$field['name']] : "";
				}
				if(isset($field['group']) && $field['group'] == "content"){
					// start
						if(isset($field['lang']) && $field['lang'] == true){
							foreach ($languages as $lang)
							{
								if(isset($field['name']) && !empty($field['name'])){
									$fields_values[$field['name']][$lang['id_lang']] = isset($fields_values_temp[$field['name']][$lang['id_lang']]) ? $fields_values_temp[$field['name']][$lang['id_lang']] : "";
								}
							}
						}else{
							if(isset($field['name']) && !empty($field['name'])){
								$fields_values[$field['name']] = isset($fields_values_temp[$field['name']]) ? $fields_values_temp[$field['name']] : "";
							}
						}
					// end
					
				}else{
					if(isset($field['lang']) && $field['lang'] == true){
						foreach ($languages as $lang)
						{
							if(isset($field['name']) && !empty($field['name'])){
								if(isset($xprtobj->{$field['name']})){
									$fields_values[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang'], $xprtobj->{$field['name']}[$lang['id_lang']]);
								}else{
									$fields_values[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
								}
							}
						}
					}else{
						if(isset($field['name']) && !empty($field['name'])){
							if(isset($xprtobj->{$field['name']})){
								$fields_values[$field['name']] = Tools::getValue($field['name'], $xprtobj->{$field['name']});
							}else{
								$fields_values[$field['name']] = Tools::getValue($field['name']);
							}
						}
					}
				}
			}
		}
		return $fields_values;
	}
	protected function deleteTables(){
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::$tablename.'`');
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtmegamenuitem`');
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtmegamenuitem_lang`');
		return true;
	}
	protected function CreateTables()
	{
		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::$tablename.'` (
			`id_'.self::$tablename.'` INT UNSIGNED NOT NULL AUTO_INCREMENT,
			`menu_name` VARCHAR(200) NOT NULL,
			`is_horizontal` VARCHAR(50) NOT NULL,
			`is_sticky` VARCHAR(50) NOT NULL,
			`hook` VARCHAR(200) NOT NULL,
			`custom_class` VARCHAR(150) NOT NULL,
			`pages` text NULL,
			`custom_css` text NULL,
			`gcontent` longtext NULL,
			`truck_identify` VARCHAR(100) NOT NULL,
			`id_shop` int(10) NOT NULL,
			`active` int(10) NOT NULL,
			`position` int(10) NOT NULL,
			PRIMARY KEY (`id_'.self::$tablename.'`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;',false);

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtmegamenuitem` (
			`id_xprtmegamenuitem` INT UNSIGNED NOT NULL AUTO_INCREMENT,
			`icon` VARCHAR(200) NOT NULL,
			`icon_pos` VARCHAR(50) NOT NULL,
			`link_type` VARCHAR(50) NOT NULL,
			`link_value` text NULL,
			`is_submenu` VARCHAR(50) NOT NULL,
			`is_megamenu` VARCHAR(50) NOT NULL,
			`is_fullwidth` VARCHAR(50) NOT NULL,
			`row_ids` VARCHAR(300) NOT NULL,
			`custom_class` VARCHAR(200) NOT NULL,
			`submenu_level` VARCHAR(200) NOT NULL,
			`submenu_value` text NULL,
			`gcontent` longtext NULL,
			`active` int(10) NOT NULL,
			`position` int(10) NOT NULL,
			`id_xprtmegamenu` int(10) NOT NULL,
			PRIMARY KEY (`id_xprtmegamenuitem`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;',false);

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtmegamenuitem_lang` (
			`id_xprtmegamenuitem` INT UNSIGNED NOT NULL AUTO_INCREMENT,
			`id_lang` int(10) unsigned NOT NULL,
			`name` VARCHAR(200) NOT NULL,
			`badge` VARCHAR(200) NOT NULL,
			`link_value_lang` text NULL,
			PRIMARY KEY (`id_xprtmegamenuitem`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;',false);
		return true;
	}
	public function hookexecute($hook)
	{
		$html = '';
		$classname = self::$classname;
		$mod_name = self::$module_name;
		$query_values = $classname::GetMenuBlock($hook);
		if(isset($query_values) && !empty($query_values)){
			foreach ($query_values as $query => $vale){
				$this->context->smarty->assign(array($mod_name => $vale));
				$html .= $this->display(__FILE__,'views/templates/front/'.$mod_name.'.tpl');
			}
		}
		return $html;
	}
	public function hookdisplayHeader($params)
	{
		$this->context->controller->addCSS($this->_path.'css/'.$this->name.'.css');
		$this->context->controller->addJS($this->_path.'js/jquery.slicknav.min.js');
		$this->context->controller->addJS($this->_path.'js/xprthoverIntent.js');
		$this->context->controller->addJS($this->_path.'js/xprtsuperfish-modified.js');
		$this->context->controller->addJS($this->_path.'js/'.$this->name.'.js');
	}
	public function hookDisplayTopColumn($params)
	{
		return $this->hookexecute('DisplayTopColumn');
	}
	public function hookDisplayTop($params)
	{
		return $this->hookexecute('DisplayTop');
	}
	public function hookdisplayMainMenu($params)
	{
		return $this->hookexecute('displayMainMenu');
	}
	public function hookdisplayNav($params)
	{
		return $this->hookexecute('displayNav');
	}
	public function hookdisplayTopLeft($params)
	{
		return $this->hookexecute('displayTopLeft');
	}
}