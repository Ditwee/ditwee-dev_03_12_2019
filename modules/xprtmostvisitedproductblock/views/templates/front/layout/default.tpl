{if isset($xprtmostvisitedproductblock) && !empty($xprtmostvisitedproductblock)}
	{if isset($xprtmostvisitedproductblock.device)}
		{assign var=device_data value=$xprtmostvisitedproductblock.device|json_decode:true}
	{/if}
	<div id="xprtmostvisitedproductblock_{$xprtmostvisitedproductblock.id_xprtmostvisitedproductblock}" class="xprtmostvisitedproductblock xprt_default_products_block" style="margin:{$xprtmostvisitedproductblock.section_margin};">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtmostvisitedproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtmostvisitedproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtmostvisitedproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtmostvisitedproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>

		{if isset($xprtmostvisitedproductblock) && $xprtmostvisitedproductblock}
			<div id="xprt_mostviewedproductsblock_{$xprtmostvisitedproductblock.id_xprtmostvisitedproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtmostvisitedproductblock.products id='' class="xprt_mostviewedproductsblock {if $xprtmostvisitedproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No special products at this time.' mod='xprtmostvisitedproductblock'}</p>
			</div>
		{/if}
	</div>
<script type="text/javascript">
	var xprtmostvisitedproductblock = $("#xprtmostvisitedproductblock_{$xprtmostvisitedproductblock.id_xprtmostvisitedproductblock}");
	var sliderSelect = xprtmostvisitedproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtmostvisitedproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtmostvisitedproductblock.nav_arrow_style) && ($xprtmostvisitedproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}
	
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtmostvisitedproductblock.play_again)}{$xprtmostvisitedproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtmostvisitedproductblock.autoplay)}{$xprtmostvisitedproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtmostvisitedproductblock.pause_on_hover)}{$xprtmostvisitedproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtmostvisitedproductblock.navigation_dots)}{$xprtmostvisitedproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtmostvisitedproductblock.navigation_arrow)}{$xprtmostvisitedproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtmostvisitedproductblock.product_rows)}{$xprtmostvisitedproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtmostvisitedproductblock.product_scroll) && ($xprtmostvisitedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtmostvisitedproductblock.product_scroll) && ($xprtmostvisitedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtmostvisitedproductblock.product_scroll) && ($xprtmostvisitedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtmostvisitedproductblock.product_scroll) && ($xprtmostvisitedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtmostvisitedproductblock.product_scroll) && ($xprtmostvisitedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}