{if isset($xprtmostvisitedproductblock) && !empty($xprtmostvisitedproductblock)}
	{if isset($xprtmostvisitedproductblock.device)}
		{assign var=device_data value=$xprtmostvisitedproductblock.device|json_decode:true}
	{/if}
	<div class="xprt_product_home_small col-sm-4">
		<div class="xprtmostvisitedproductblock block carousel">
			<h4 class="title_block">
		    	<em>{$xprtmostvisitedproductblock.title}</em>
		    </h4>
		    <div class="block_content products-block">
		        {if isset($xprtmostvisitedproductblock) && $xprtmostvisitedproductblock}
		        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtmostvisitedproductblock.products}
		        {else}
	        		<p class="alert alert-info">{l s='No products at this time.' mod='xprtmostvisitedproductblock'}</p>
		        {/if}
		    </div>
		</div>
	</div>
{/if}