{if isset($xprtmostvisitedproductblock) && !empty($xprtmostvisitedproductblock)}
	{if isset($xprtmostvisitedproductblock.device)}
		{assign var=device_data value=$xprtmostvisitedproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtmostvisitedproductblock) && $xprtmostvisitedproductblock}
		<div id="xprt_mostviewedproductsblock_tab_{if isset($xprtmostvisitedproductblock.id_xprtmostvisitedproductblock)}{$xprtmostvisitedproductblock.id_xprtmostvisitedproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtmostvisitedproductblock.products class='xprt_mostviewedproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_mostviewedproductsblock_tab_{if isset($xprtmostvisitedproductblock.id_xprtmostvisitedproductblock)}{$xprtmostvisitedproductblock.id_xprtmostvisitedproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No products at this time.' mod='xprtmostvisitedproductblock'}</p>
		</div>
	{/if}
{/if}