{if isset($xprtnewproductblock) !empty($xprtnewproductblock)}
	{if isset($xprtnewproductblock.device)}
		{assign var=device_data value=$xprtnewproductblock.device|json_decode:true}
	{/if}
	<div class="xprt_product_home_small col-sm-4">
		<div class="xprtblocknewproducts block carousel">
			<h4 class="title_block">
		    	<em>{$xprtnewproductblock.title}</em>
		    </h4>
		    <div class="block_content products-block">
		        {if isset($xprtnewproductblock) && $xprtnewproductblock}
		        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtnewproductblock.products class="blocknewproducts" }
		        {else}
		        	<ul id="blocknewproducts_{$xprtnewproductblock.id_xprtnewproductblock}" class="blocknewproducts">
		        		<li class="alert alert-info">{l s='No products at this time.' mod='xprtblocknewproducts'}</li>
		        	</ul>
		        {/if}
		    </div>
		</div>
	</div>
{/if}