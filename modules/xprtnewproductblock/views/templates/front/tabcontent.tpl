{if isset($xprtnewproductblock) && !empty($xprtnewproductblock)}
	{if isset($xprtnewproductblock.device)}
		{assign var=device_data value=$xprtnewproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtnewproductblock) && $xprtnewproductblock}
		<div id="xprt_newproductsblock_tab_{if isset($xprtnewproductblock.id_xprtnewproductblock)}{$xprtnewproductblock.id_xprtnewproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtnewproductblock.products class='xprt_newproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_newproductsblock_tab_{if isset($xprtnewproductblock.id_xprtnewproductblock)}{$xprtnewproductblock.id_xprtnewproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No new products at this time.' mod='xprtnewproductblock'}</p>
		</div>
	{/if}
{/if}
