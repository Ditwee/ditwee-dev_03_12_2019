{if isset($xprtonsaleproductblock) && !empty($xprtonsaleproductblock)}
	{if isset($xprtonsaleproductblock.device)}
		{assign var=device_data value=$xprtonsaleproductblock.device|json_decode:true}
	{/if}
	<div class="xprtonsaleproductblock block carousel">
		<h4 class="title_block">
	    	{$xprtonsaleproductblock.title}
	    </h4>
	    <div class="block_content">
	        {if isset($xprtonsaleproductblock) && $xprtonsaleproductblock}
	        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtonsaleproductblock.products }
	        {else}
	        	<p class="alert alert-info">{l s='No products at this time.' mod='xprtonsaleproductblock'}</p>
	        {/if}
	    </div>
	</div>
{/if}