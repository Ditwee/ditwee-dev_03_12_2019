{if isset($xprtonsaleproductblock) && !empty($xprtonsaleproductblock)}
		{if isset($xprtonsaleproductblock.device)}
			{assign var=device_data value=$xprtonsaleproductblock.device|json_decode:true}
		{/if}
		{if isset($xprtonsaleproductblock) && $xprtonsaleproductblock}
			<div id="xprt_onsaleproductsblock_tab_{if isset($xprtonsaleproductblock.id_xprtonsaleproductblock)}{$xprtonsaleproductblock.id_xprtonsaleproductblock}{/if}" class="tab-pane fade">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtonsaleproductblock.products class='xprt_onsaleproductsblock ' id=''}
			</div>
		{else}
			<div id="xprt_onsaleproductsblock_tab_{if isset($xprtonsaleproductblock.id_xprtonsaleproductblock)}{$xprtonsaleproductblock.id_xprtonsaleproductblock}{/if}" class="tab-pane fade">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtonsaleproductblock'}</p>
			</div>
		{/if}
{/if}