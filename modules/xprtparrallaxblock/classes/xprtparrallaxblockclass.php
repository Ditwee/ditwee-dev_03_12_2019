<?php

class xprtparrallaxblockclass extends ObjectModel
{
	public $id;
	public $id_xprtparrallaxblocktbl;
    public $image_type;
    public $btntarget;
    public $image;
    public $height;
    public $padding;
    public $margin;
    public $hook;
    public $pages;
    public $title;
    public $subtitle;
    public $btntext;
    public $btnurl;
    public $fullwidth;
    public $content;
    public $contentposition;
    public $truck_identify;
	public $position=0;
	public $active=1;
	public static $definition = array(
		'table' => 'xprtparrallaxblocktbl',
		'primary' => 'id_xprtparrallaxblocktbl',
		'multilang' => true,
		'fields' => array(
                'image_type' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'btntarget' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'image' =>		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'height' =>	    array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'padding' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'margin' =>	    array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'hook' =>		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
				'pages' =>		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                'fullwidth' =>  array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                'position' =>   array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                'active' =>     array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
				'title' =>		array('type' => self::TYPE_STRING, 'validate' => 'isString','lang' => true),
				'subtitle' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString','lang' => true),
				'btntext' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString','lang' => true),
				'btnurl' =>	    array('type' => self::TYPE_STRING, 'validate' => 'isString','lang' => true),
                'content' =>	array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml','lang' => true),
                'contentposition' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                'truck_identify' =>	array('type' => self::TYPE_STRING, 'validate' => 'isString'),
		),
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
	{
        Shop::addTableAssociation('xprtparrallaxblocktbl', array('type' => 'shop'));
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if($this->position <= 0){
            $this->position = self::getTopPosition() + 1;
        }
		if(isset($this->image) && !empty($this->image)){
			$this->image = $this->image;
		}else{
			$this->image = $this->processImage($_FILES);
		}
		if(isset($this->truck_identify) && !empty($this->truck_identify)){
			$this->truck_identify = $this->truck_identify;
		}else{
			$this->truck_identify = "";
		}
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public function update($null_values = false)
    {
        if (isset($_FILES['image']) && isset($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name']))
            $this->image = $this->processImage($_FILES);
        if(!parent::update($null_values))
            return false;
        return true;
    }
    public function processImage($FILES) {

        if (isset($FILES['image']) && isset($FILES['image']['tmp_name']) && !empty($FILES['image']['tmp_name'])) {
                $ext = substr($FILES['image']['name'], strrpos($FILES['image']['name'], '.') + 1);
                $id = time();
                $file_name = $id . '.' . $ext;
                $path = _PS_MODULE_DIR_ .'xprtparrallaxblock/img/' . $file_name;
                if (!move_uploaded_file($FILES['image']['tmp_name'], $path))
                    return false;         
                else
                    return $file_name;   
        }
    }
    public static function getTopPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtparrallaxblocktbl`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetParrallaxBlock($hook = NULL,$id_lang = NULL,$id_shop = NULL)
    {
    	$results = array();
        if($hook == NULL)
            return false;
        if($id_lang == NULL)
            $id_lang = (int)Context::getContext()->language->id;
        if($id_shop == NULL)
            $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM '._DB_PREFIX_.'xprtparrallaxblocktbl x INNER JOIN 
                '._DB_PREFIX_.'xprtparrallaxblocktbl_lang xl ON x.id_xprtparrallaxblocktbl=xl.id_xprtparrallaxblocktbl INNER JOIN 
                '._DB_PREFIX_.'xprtparrallaxblocktbl_shop xs ON x.id_xprtparrallaxblocktbl = xs.id_xprtparrallaxblocktbl 
                WHERE xl.id_lang='.(int)$id_lang.' AND xs.id_shop = '.(int)$id_shop.'
                AND x.active= 1 AND x.hook = "'.$hook.'"';
        if(!$parrallaxs = DB::getInstance()->executeS($sql))
            return false;
        if(isset($parrallaxs) && !empty($parrallaxs)){
        	$i = 0;
        	foreach ($parrallaxs as $parrallaxkey => $parrallaxvalue) {
        		if(empty($parrallaxvalue['pages'])){
        			$parrallaxvalue['pages'] = 'all_page';
        		}
        		if(xprtparrallaxblock::PageException($parrallaxvalue['pages'])){
        			$results[$i] = $parrallaxvalue;
        		}
        		$i++;
        	}
        }
            return $results;
    }
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS('
            SELECT `id_xprtparrallaxblocktbl`, `position`
            FROM `'._DB_PREFIX_.'xprtparrallaxblocktbl`
            ORDER BY `position` ASC'
        ))
            return false;
        if(!empty($res))
        foreach($res as $xprtparrallaxblocktbl)
            if((int)$xprtparrallaxblocktbl['id_xprtparrallaxblocktbl'] == (int)$this->id)
        $moved_xprtparrallaxblocktbl = $xprtparrallaxblocktbl;
        if(!isset($moved_xprtparrallaxblocktbl) || !isset($position))
            return false;
        $queryx = ' UPDATE `'._DB_PREFIX_.'xprtparrallaxblocktbl`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
        ? '> '.(int)$moved_xprtparrallaxblocktbl['position'].' AND `position` <= '.(int)$position
        : '< '.(int)$moved_xprtparrallaxblocktbl['position'].' AND `position` >= '.(int)$position.'
        ');
        $queryy = ' UPDATE `'._DB_PREFIX_.'xprtparrallaxblocktbl`
        SET `position` = '.(int)$position.'
        WHERE `id_xprtparrallaxblocktbl` = '.(int)$moved_xprtparrallaxblocktbl['id_xprtparrallaxblocktbl'];
        return (Db::getInstance()->execute($queryx,false)
        && Db::getInstance()->execute($queryy,false));
    }
}