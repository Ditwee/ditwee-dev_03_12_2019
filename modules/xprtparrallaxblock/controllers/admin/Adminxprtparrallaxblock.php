<?php
class AdminxprtparrallaxblockController extends ModuleAdminController {

    public function __construct() {
        $this->table = 'xprtparrallaxblocktbl';
        $this->className = 'xprtparrallaxblockclass';
        $this->lang = true;
        $this->deleted = false;
        $this->module = 'xprtparrallaxblock';
        $this->explicitSelect = true;
        $this->_defaultOrderBy = 'position';
        $this->allow_export = false;
        $this->_defaultOrderWay = 'DESC';
        $this->bootstrap = true;
            if(Shop::isFeatureActive())
            Shop::addTableAssociation($this->table, array('type' => 'shop'));
            parent::__construct();
        $this->fields_list = array(
            'id_xprtparrallaxblocktbl' => array(
                    'title' => $this->l('Id'),
                    'width' => 100,
                    'type' => 'text',
            ),
            'title' => array(
                    'title' => $this->l('Title'),
                    'width' => 60,
                    'type' => 'text',
            ),
            'hook' => array(
                    'title' => $this->l('Hook'),
                    'width' => 220,
                    'type' => 'text',
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'width' => 60,
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false
            )
        );
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        parent::__construct();
    }
    public function init()
    {
        parent::init();
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'xprtparrallaxblocktbl_shop sbp ON a.id_xprtparrallaxblocktbl=sbp.id_xprtparrallaxblocktbl && sbp.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
        $this->_select = 'sbp.id_shop';
        $this->_defaultOrderBy = 'a.position';
        $this->_defaultOrderWay = 'DESC';
        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
        $this->_group = 'GROUP BY a.id_xprtparrallaxblocktbl';
        $this->_select = 'a.position position';
    }
    public function setMedia()
    {          
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('select2');
        $this->addJqueryPlugin('tagify');
    }
    public static function hook_val()
    {
        $allhooks = array();
    	require_once(dirname(__FILE__) . '/../../hooks/hooks.php');
		$hook_val = $hooks;
        if(isset($hook_val)){
            $i = 0;
            foreach ($hook_val as $hok) {
               $allhooks[$i]['id'] = $hok;
               $allhooks[$i]['name'] = ucwords($hok);
               $i++;
            }
        }
    	return $allhooks;
    }
    public static function AllPageExceptions()
	{
	    $id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT p.`id_product`, pl.`name`
	            FROM `'._DB_PREFIX_.'product` p
	            '.Shop::addSqlAssociation('product', 'p').'
	            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
	            WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
	    $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	    $id_lang = Context::getContext()->language->id;
	    $categories =  Category::getCategories($id_lang,true,false);
	    $controllers = Dispatcher::getControllers(_PS_FRONT_CONTROLLER_DIR_);
	    if(isset($controllers))
	        ksort($controllers);
	    $Manufacturers =  Manufacturer::getManufacturers();
	    $Suppliers =  Supplier::getSuppliers();
	    $rslt = array();
	    $rslt[0]['id'] = 'all_page';
	    $rslt[0]['name'] = 'All Pages';
	    $i = 1;
	    if(isset($controllers))
	        foreach($controllers as $r => $v){
	            $rslt[$i]['id'] = $r;
	            $rslt[$i]['name'] = 'Page : '.ucwords($r);
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'man_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'sup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier : '.$r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'cat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category : '.$cats['name'];
	            $i++;
	        }
	    if(isset($products))
	        foreach($products as $r){
	            $rslt[$i]['id'] = 'prd_'.$r['id_product'];
	            $rslt[$i]['name'] = 'Product : '. $r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'prdcat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category Product: '.$cats['name'];
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'prdman_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer Product : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'prdsup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier Product : '.$r['name'];
	            $i++;
	        }
	    return $rslt;
	}
    public function renderForm()
    {
        $image_url = false;
        $image_size = file_exists($image_url) ? filesize($image_url) / 1000 : false;
        $this->fields_form = array(
        	'tinymce' => true,
            'legend' => array(
          		'title' => $this->l('Jakiro Theme Parrallax Block'),
            ),
            'input' => array(
                array(
                    'type' => 'file',
                    'label' => $this->l('Parrallax Image'),
                    'name' => 'image',
                    'display_image' => true,
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                ),
            	array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'name' => 'title',
                    'desc' => $this->l('Enter Your Parrallax Title'),
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Sub Title'),
                    'name' => 'subtitle',
                    'desc' => $this->l('Enter Your Parrallax Subtitle'),
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Content'),
                    'name' => 'content',
                    'desc' => $this->l('Enter Your Parrallax Content'),
                    'lang' => true,
                    'cols' => 40,
                    'rows' => 10,
                    'class' => 'rte',
                    'autoload_rte' => true,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Content position'),
                    'name' => 'contentposition',
                    'options' => array(
                        'query' => array(
                            array(
                                'id'=>'top_left',
                                'name'=>'Top left',
                            ),
                            array(
                                'id'=>'top_middle',
                                'name'=>'Top middle',
                            ),
                            array(
                                'id'=>'top_right',
                                'name'=>'Top right',
                            ),
                            array(
                                'id'=>'center_left',
                                'name'=>'Center left',
                            ),
                            array(
                                'id'=>'center_middle',
                                'name'=>'Center middle',
                            ),
                            array(
                                'id'=>'center_right',
                                'name'=>'Center right',
                            ),
                            array(
                                'id'=>'bottom_left',
                                'name'=>'Bottom left',
                            ),
                            array(
                                'id'=>'bottom_middle',
                                'name'=>'Bottom middle',
                            ),
                            array(
                                'id'=>'bottom_right',
                                'name'=>'Bottom right',
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Button Label'),
                    'name' => 'btntext',
                    'desc' => $this->l('Enter Your Parrallax Button Label Text'),
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Button URL'),
                    'name' => 'btnurl',
                    'desc' => $this->l('Enter Your Parrallax Button URl Link'),
                    'lang' => true,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Content display style'),
                    'name' => 'btntarget',
                    'options' => array(
                        'query' => array(
                            array(
                                'id'=>'style_white',
                                'name'=>'White style',
                            ),
                            array(
                                'id'=>'style_black',
                                'name'=>'Black style',
                            ),
                            array(
                                'id'=>'style_other',
                                'name'=>'Other style',
                            ),
                        ),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Height'),
                    'name' => 'height',
                    'desc' => $this->l('Enter Your Parrallax Height'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Padding'),
                    'name' => 'padding',
                    'desc' => $this->l('Enter Your Parrallax Padding(Format: 0px 0px 0px 0px) Top Right Bottom Left'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Margin'),
                    'name' => 'margin',
                    'desc' => $this->l('Enter Your Parrallax Margin(Format: 0px 0px 0px 0px) Top Right Bottom Left'),
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Where You Want To Display'),
                    'name' => 'hook',
                    'options' => array(
                        'query' => self::hook_val(),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                ),
				array(
				    'type' => 'selecttwotype',
				    'label' => $this->l('Which Page You Want to Display'),
				    'placeholder' => $this->l('Please Type Your Controller Name.'),
				    'initvalues' => self::AllPageExceptions(),
				    'name' => 'pages',
				    'desc' => $this->l('Please Type Your Specific Page Name,Category name,Product Name,For All Product specific Category select category product: category name.<br>For showing All page Type: All Page. For Home Page Type:index.')
				),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Force Full Width'),
                    'name' => 'fullwidth',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
	                        'id' => 'fullwidth',
	                        'value' => 1,
	                        'label' => $this->l('Enabled')
                        ),
                        array(
	                        'id' => 'fullwidth',
	                        'value' => 0,
	                        'label' => $this->l('Disabled')
                        )
                    )
               	),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Status'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        if(Shop::isFeatureActive())
			$this->fields_form['input'][] = array(
				'type' => 'shop',
				'label' => $this->l('Shop association:'),
				'name' => 'checkBoxShopAsso',
			);
        if(!($xprtparrallaxblockclass = $this->loadObject(true)))
            return;
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save   '),
            'class' => 'button'
        );
        return parent::renderForm();
    }
    public function renderList()
    {
        if(isset($this->_filter) && trim($this->_filter) == '')
            $this->_filter = $this->original_filter;
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();
    }
    public function initToolbar(){
          parent::initToolbar();
    }
    public function processPosition()
    {
        if($this->tabAccess['edit'] !== '1')
            $this->errors[] = Tools::displayError('You do not have permission to edit this.');
        else if(!Validate::isLoadedObject($object = new xprtparrallaxblockclass((int)Tools::getValue($this->identifier, Tools::getValue('id_xprtparrallaxblocktbl', 1)))))
        $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.').' <b>'.
        $this->table.'</b> '.Tools::displayError('(cannot load object)');
        if(!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position')))
        $this->errors[] = Tools::displayError('Failed to update the position.');
        else
        {
        $object->regenerateEntireNtree();
        Tools::redirectAdmin(self::$currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_xprtparrallaxblocktbl = (int)Tools::getValue($this->identifier)) ? ('&'.$this->identifier.'='.$id_xprtparrallaxblocktbl) : '').'&token='.Tools::getAdminTokenLite('Adminxprtparrallaxblock'));
        }
    }
    public function ajaxProcessUpdatePositions()
    {
      $id_xprtparrallaxblocktbl = (int)(Tools::getValue('id'));
      $way = (int)(Tools::getValue('way'));
      $positions = Tools::getValue($this->table);
      if (is_array($positions))
        foreach ($positions as $key => $value)
        {
          $pos = explode('_', $value);
          if ((isset($pos[1]) && isset($pos[2])) && ($pos[2] == $id_xprtparrallaxblocktbl))
          {
            $position = $key + 1;
            break;
          }
        }
      $xprtparrallaxblockclass = new xprtparrallaxblockclass($id_xprtparrallaxblocktbl);
      if (Validate::isLoadedObject($xprtparrallaxblockclass))
      {
        if (isset($position) && $xprtparrallaxblockclass->updatePosition($way, $position))
        {
          Hook::exec('action'.$this->className.'Update');
          die(true);
        }
        else
          die('{"hasError" : true, errors : "Can not update xprtparrallaxblockclass position"}');
      }
      else
        die('{"hasError" : true, "errors" : "This xprtparrallaxblockclass can not be loaded"}');
    }
}
