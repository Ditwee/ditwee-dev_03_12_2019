<?php

$hooks = array(
'displayTopColumn',
'displayFooterTop',
'displayFooterBottom',
'displayHome',
'displayHomeFullWidthMiddle',
'displayHomeFullWidthBottom',
'displayMaintenance',
'displayFooterProduct',
);
