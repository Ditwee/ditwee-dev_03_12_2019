<?php

$querys = array();

$querys[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtparrallaxblocktbl` (
				`id_xprtparrallaxblocktbl` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`image_type` VARCHAR(100) NULL,
				`image` VARCHAR(100) NULL,
				`height` VARCHAR(100) NULL,
				`padding` VARCHAR(100) NULL,
				`margin` VARCHAR(100) NULL,
				`hook` VARCHAR(150) NULL,
				`pages` VARCHAR(150) NULL,
				`btntarget` VARCHAR(150) NULL,
				`contentposition` VARCHAR(150) NULL,
				`fullwidth` int(10) NULL,
				`active` int(10) NULL,
				`position`int(10) NULL ,
				`truck_identify` VARCHAR(200) NULL,
				PRIMARY KEY (`id_xprtparrallaxblocktbl`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$querys[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtparrallaxblocktbl_lang` (
				`id_xprtparrallaxblocktbl` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NULL ,
				`title` VARCHAR(300) NULL,
				`subtitle` VARCHAR(300) NULL,
				`btntext` VARCHAR(300) NULL,
				`btnurl` VARCHAR(300) NULL,
				`content` text NULL,
				PRIMARY KEY (`id_xprtparrallaxblocktbl`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$querys[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtparrallaxblocktbl_shop` (
			  `id_xprtparrallaxblocktbl` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprtparrallaxblocktbl`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$querys_u = array();

$querys_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtparrallaxblocktbl`';

$querys_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtparrallaxblocktbl_lang`';

$querys_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtparrallaxblocktbl_shop`';
