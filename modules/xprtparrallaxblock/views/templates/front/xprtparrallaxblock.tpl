{if isset($xprtparrallaxblock) and !empty($xprtparrallaxblock)}
	<div class="xprt_parallax_area">
		{foreach from=$xprtparrallaxblock item=parallax}
			<div class="xprt_parallax xprt_parallax_section" style="background-image: url({$modules_dir}xprtparrallaxblock/img/{$parallax.image}); height: {$parallax.height|intval}px;margin: {$parallax.margin};">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="xprt_parallax_content_area" style="min-height:{$parallax.height|intval}px;">
								<div class="xprt_parallax_content {$parallax.contentposition}">
									<div class="xprt_parallax_content_inner {$parallax.btntarget}" style="padding: {$parallax.padding};">
										{if isset($parallax.title) && !empty($parallax.title)}
											<h2>{$parallax.title}</h2>
										{/if}
										{if isset($parallax.subtitle) && !empty($parallax.subtitle)}
											<h4>{$parallax.subtitle}</h4>
										{/if}
										{if isset($parallax.btntext) && !empty($parallax.btntext)}
											<p><a href="{$parallax.btnurl}" class="btn btn-default btn-black">{$parallax.btntext}</a></p>
										{/if}
										{if isset($parallax.content) && !empty($parallax.content)}
											{$parallax.content}
										{/if}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		{/foreach}
	</div>
{/if}