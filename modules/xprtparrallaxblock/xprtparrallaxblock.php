<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
include_once _PS_MODULE_DIR_.'xprtparrallaxblock/classes/xprtparrallaxblockclass.php';
class xprtparrallaxblock extends Module
{
	public $tabs_files_url = '/tabs/tabs.php';
	public $mysql_files_url = '/querys/querys.php';
	public $hooks_url = '/hooks/hooks.php';
	public $css_array = array("xprtparrallaxblock.css","xprtparrallaxblock.css");
	public $js_array = array("xprtparrallaxblock.js","xprtparrallaxblock.js");
	public function __construct()
	{
		$this->name = 'xprtparrallaxblock';
		$this->tab = 'front_office_features';
		$this->version = '2.0.2';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		parent::__construct();	
		$this->displayName = $this->l('Great Store Theme Parallax Display Block');
		$this->description = $this->l('Prestashop Most Powerfull Great Store Theme Parallax Display Block Module by Xpert-Idea.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
		 || !$this->Register_Hooks()
		 || !$this->Register_Tabs()
		 || !$this->Register_SQL()
		 || !$this->DummyData()
		 || !$this->xpertsampledata()
		)
			return false;
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
		 || !$this->UnRegister_Hooks()
		 || !$this->UnRegister_Tabs()
		 || !$this->UnRegister_SQL()
		)
			return false;
		return true;
	}
	public function Register_Hooks()
	{
        $this->registerHook('displayheader');
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
        		$this->registerHook($hook);
        	endforeach;
        	return true;
	}
	public function UnRegister_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
	        		$hook_id = Module::getModuleIdByName($hook);
	        	if(isset($hook_id) && !empty($hook_id))
	        		$this->unregisterHook((int)$hook_id);
        	endforeach;
        	return true;
	}
	public function Register_SQL()
	{
		$querys = array();
		if(file_exists(dirname(__FILE__).$this->mysql_files_url)){
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($querys) && !empty($querys))
				foreach($querys as $query){
					if(!Db::getInstance()->Execute($query,false))
					    return false;
				}
		}
        return true;
	}
	public function UnRegister_SQL()
	{
		$querys_u = array();
		if(file_exists(dirname(__FILE__).$this->mysql_files_url)){
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($querys_u) && !empty($querys_u))
				foreach($querys_u as $query_u){
					if(!Db::getInstance()->Execute($query_u,false))
					    return false;
				}
		}
        return true;
	}
	public function UnRegister_Tabs()
    {
        $tabs_lists = array();
        require_once(dirname(__FILE__) .$this->tabs_files_url);
        if(isset($tabs_lists) && !empty($tabs_lists)){
        	foreach($tabs_lists as $tab_list){
        	    $tab_list_id = Tab::getIdFromClassName($tab_list['class_name']);
        	    if(isset($tab_list_id) && !empty($tab_list_id)){
        	        $tabobj = new Tab($tab_list_id);
        	        $tabobj->delete();
        	    }
        	}
        } 
        $save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
        if($save_tab_id != 0){
        	$count = Tab::getNbTabs($save_tab_id);
        	if($count == 0){
        		if(isset($save_tab_id) && !empty($save_tab_id)){
        		    $tabobjs = new Tab($save_tab_id);
        		    $tabobjs->delete();
        		}
        	}
        }
        return true;
    }
	public function RegisterParentTabs(){
    	$langs = Language::getLanguages();
    	$save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
    	if($save_tab_id != 0){
    		return $save_tab_id;
    	}else{
    		$tab_listobj = new Tab();
    		$tab_listobj->class_name = 'Adminxprtdashboard';
    		$tab_listobj->id_parent = 0;
    		$tab_listobj->module = $this->name;
    		foreach($langs as $l)
    		{
    		    $tab_listobj->name[$l['id_lang']] = $this->l("Theme Settings");
    		}
    		if($tab_listobj->save())
    			return (int)$tab_listobj->id;
    		else
    			return (int)$save_tab_id;
    	}
    }
    public function Register_Tabs()
    {
        $tabs_lists = array();
        $langs = Language::getLanguages();
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $save_tab_id = $this->RegisterParentTabs();
            require_once(dirname(__FILE__) .$this->tabs_files_url);
            if(isset($tabs_lists) && !empty($tabs_lists))
	            foreach ($tabs_lists as $tab_list)
	            {
	                $tab_listobj = new Tab();
	                $tab_listobj->class_name = $tab_list['class_name'];
	                if($tab_list['id_parent'] == 'parent'){
	                    $tab_listobj->id_parent = $save_tab_id;
	                }else{
	                    $tab_listobj->id_parent = $tab_list['id_parent'];
	                }
	                if(isset($tab_list['module']) && !empty($tab_list['module'])){
	                    $tab_listobj->module = $tab_list['module'];
	                }else{
	                    $tab_listobj->module = $this->name;
	                }
	                foreach($langs as $l)
	                {
	                    $tab_listobj->name[$l['id_lang']] = $this->l($tab_list['name']);
	                }
	                $tab_listobj->save();
	            }
        return true;
    }
    public static function PageException($exceptions = NULL)
	{
		if($exceptions == NULL)
			return false;
		$exceptions = explode(",",$exceptions);
		$page_name = Context::getContext()->controller->php_self;
		$this_arr = array();
		$this_arr[] = 'all_page';
		$this_arr[] = $page_name;
		if($page_name == 'category'){
			$id_category = Tools::getvalue('id_category');
			$this_arr[] = 'cat_'.$id_category;
		}elseif($page_name == 'product'){
			$id_product = Tools::getvalue('id_product');
			$this_arr[] = 'prd_'.$id_product;
			// Start Get Product Category
			$prd_cat_sql = 'SELECT cp.`id_category` AS id
			    FROM `'._DB_PREFIX_.'category_product` cp
			    LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = cp.id_category)
			    '.Shop::addSqlAssociation('category', 'c').'
			    WHERE cp.`id_product` = '.(int)$id_product;
			$prd_catresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_cat_sql);
			if(isset($prd_catresults) && !empty($prd_catresults))
			{
			    foreach($prd_catresults as $prd_catresult)
			    {
			        $this_arr[] = 'prdcat_'.$prd_catresult['id'];
			    }
			}
			// END Get Product Category
			// Start Get Product Manufacturer
			$prd_man_sql = 'SELECT `id_manufacturer` AS id FROM `'._DB_PREFIX_.'product` WHERE `id_product` = '.(int)$id_product;
			$prd_manresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_man_sql);
			if(isset($prd_manresults) && !empty($prd_manresults))
			{
			    foreach($prd_manresults as $prd_manresult)
			    {
			        $this_arr[] = 'prdman_'.$prd_manresult['id'];
			    }
			}
			// END Get Product Manufacturer
			// Start Get Product SupplierS
			$prd_sup_sql = "SELECT `id_supplier` AS id FROM `"._DB_PREFIX_."product_supplier` WHERE `id_product` = ".(int)$id_product." GROUP BY `id_supplier`";
			$prd_supresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_sup_sql);
			if(isset($prd_supresults) && !empty($prd_supresults))
			{
			    foreach($prd_supresults as $prd_supresult)
			    {
			        $this_arr[] = 'prdsup_'.$prd_supresult['id'];
			    }
			}
			// END Get Product SupplierS
		}elseif($page_name == 'cms'){
			$id_cms = Tools::getvalue('id_cms');
			$this_arr[] = 'cms_'.$id_cms;
		}elseif($page_name == 'manufacturer'){
			$id_manufacturer = Tools::getvalue('id_manufacturer');
			$this_arr[] = 'man_'.$id_manufacturer;
		}elseif($page_name == 'supplier'){
			$id_supplier = Tools::getvalue('id_supplier');
			$this_arr[] = 'sup_'.$id_supplier;
		}
		if(isset($this_arr)){
			foreach ($this_arr as $this_arr_val) {
				if(in_array($this_arr_val,$exceptions))
					return true;
			}
		}
		return false;
	}
    public function hookexecute($hook)
	{
		$xprtparrallaxblock = xprtparrallaxblockclass::GetParrallaxBlock($hook);
		$this->context->smarty->assign(array('xprtparrallaxblock' => $xprtparrallaxblock));
		return $this->display(__FILE__,'views/templates/front/xprtparrallaxblock.tpl');
	}
	public function hookdisplayTopColumn($params)
	{
		return $this->hookexecute('displayTopColumn');
	}
	public function hookdisplayFooterTop($params)
	{
		return $this->hookexecute('displayFooterTop');
	}
	public function hookdisplayFooterBottom($params)
	{
		return $this->hookexecute('displayFooterBottom');
	}
	public function hookdisplayHome($params)
	{
		return $this->hookexecute('displayHome');
	}
	public function hookdisplayHomeFullWidthMiddle($params)
	{
		return $this->hookexecute('displayHomeFullWidthMiddle');
	}
	public function hookdisplayHomeFullWidthBottom($params)
	{
		return $this->hookexecute('displayHomeFullWidthBottom');
	}
	public function hookdisplayMaintenance($params)
	{
		return $this->hookexecute('displayMaintenance');
	}
	public function hookdisplayFooterProduct($params)
	{
		return $this->hookexecute('displayFooterProduct');
	}
	public function hookdisplayheader($params)
	{
		if(isset($this->css_array))
			foreach ($this->css_array as $css) {
				$this->context->controller->addCSS($this->_path.'css/'.$css);
			}
		if(isset($this->js_array))
			foreach ($this->js_array as $js) {
				$this->context->controller->addJS($this->_path.'js/'.$js);
			}
	}
	public function DummyData()
	{
	    include_once(dirname(__FILE__).'/data/dummy_data.php');
	    $this->InsertDummyData($parallax_dummy_data,"xprtparrallaxblockclass");
	    return true;
	}
	public function InsertDummyData($parallax_dummy_data=NULL,$class=NULL){
		if($parallax_dummy_data == NULL || $class == NULL)
			return false;
		$languages = Language::getLanguages(false);
	    if(isset($parallax_dummy_data) && !empty($parallax_dummy_data)){
	        $classobj = new $class();
	        foreach($parallax_dummy_data as $valu){
	        	if(isset($valu['lang']) && !empty($valu['lang'])){
	        		foreach ($valu['lang'] as $valukey => $value){
	        			foreach ($languages as $language){
	        				if(isset($valukey)){
	        					$classobj->{$valukey}[$language['id_lang']] = isset($value) ? $value : '';
	        				}
	        			}
	        		}
	        	}
        		if(isset($valu['notlang']) && !empty($valu['notlang'])){
        			foreach ($valu['notlang'] as $valukey => $value){
        				if(isset($valukey)){
        					$classobj->{$valukey} = $value;
        				}
        			}
        		}
	        	$classobj->add();
	        }
	    }
	    return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$this->alldisabled();
		$this->demowiseenabled($demo);
	    return true;
	}
	public function alldisabled(){
		$data = array();
		$data['active'] = 0;
		Db::getInstance()->update("xprtparrallaxblocktbl",$data);
		return true;
	}
	public function demowiseenabled($demo = "demo_1"){
		$data = array();
		$data['active'] = 1;
		Db::getInstance()->update("xprtparrallaxblocktbl",$data," `truck_identify` = '".$demo."' ");
		return true;
	}
}