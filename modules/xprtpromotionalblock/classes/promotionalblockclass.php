<?php

class promotionalblockclass extends ObjectModel
{
	public $id;
	public $promo_Images;
	public $maincolumn;
    public $hook;
	public $mainstyle;
	public $promo_margin;
	public $pages;
	public $active = 1;
	public $position = 0;
	public $text;
	public static $definition = array(
		'table' => 'xprtpromotionaltbl',
		'primary' => 'id_xprtpromotionaltbl',
		'multilang' => true,
		'fields' => array(
			'promo_Images' =>				array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
			'maincolumn' =>				array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
            'hook' =>               array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
			'mainstyle' =>				array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml'),
			'active' =>				array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'position' =>			array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			'promo_margin' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'pages' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			// Lang fields
			'text' =>				array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
	{
        Shop::addTableAssociation('xprtpromotionaltbl', array('type' => 'shop'));
            parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0){
            $this->position = self::getTopPosition() + 1;
        }
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public function update($null_values = false)
    {
        if(!parent::update($null_values))
            return false;
        return true;
    }
    public static function getTopPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtpromotionaltbl`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS('
            SELECT `id_xprtpromotionaltbl`, `position`
            FROM `'._DB_PREFIX_.'xprtpromotionaltbl`
            ORDER BY `position` ASC'
        ))
            return false;
        if(!empty($res))
        foreach($res as $xprtpromotionaltbl)
            if((int)$xprtpromotionaltbl['id_xprtpromotionaltbl'] == (int)$this->id)
        $moved_xprtpromotionaltbl = $xprtpromotionaltbl;
        if(!isset($moved_xprtpromotionaltbl) || !isset($position))
            return false;
        $queryx = ' UPDATE `'._DB_PREFIX_.'xprtpromotionaltbl`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
        ? '> '.(int)$moved_xprtpromotionaltbl['position'].' AND `position` <= '.(int)$position
        : '< '.(int)$moved_xprtpromotionaltbl['position'].' AND `position` >= '.(int)$position.'
        ');
        $queryy = ' UPDATE `'._DB_PREFIX_.'xprtpromotionaltbl`
        SET `position` = '.(int)$position.'
        WHERE `id_xprtpromotionaltbl` = '.(int)$moved_xprtpromotionaltbl['id_xprtpromotionaltbl'];
        return (Db::getInstance()->execute($queryx,false)
        && Db::getInstance()->execute($queryy,false));
    }
}
