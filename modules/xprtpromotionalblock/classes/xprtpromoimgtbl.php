<?php

class xprtpromoimgtbl extends ObjectModel
{
	public $id;
	public $id_xprtpromoimgtbl;
	public $image;
	public $column;
	public $additionalclass;
	public $link;
	public $style;
	public $contentposition;
	public $position = 0;
	public $content;
	public static $definition = array(
		'table' => 'xprtpromoimgtbl',
		'primary' => 'id_xprtpromoimgtbl',
		'multilang' => true,
		'fields' => array(
			'image' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'column' => 		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'additionalclass' =>array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'link' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'style' => 			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'contentposition' =>array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'position' =>		array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
			'content' => 		array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
	{
        Shop::addTableAssociation('xprtpromoimgtbl', array('type' => 'shop'));
            parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0){
            $this->position = self::getTopPosition() + 1;
        }
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getTopPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtpromoimgtbl`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS('
            SELECT `id_xprtpromoimgtbl`, `position`
            FROM `'._DB_PREFIX_.'xprtpromoimgtbl`
            ORDER BY `position` ASC'
        ))
            return false;
        if(!empty($res))
        foreach($res as $xprtpromoimgtbl)
            if((int)$xprtpromoimgtbl['id_xprtpromoimgtbl'] == (int)$this->id)
        $moved_xprtpromoimgtbl = $xprtpromoimgtbl;
        if(!isset($moved_xprtpromoimgtbl) || !isset($position))
            return false;
        $queryx = ' UPDATE `'._DB_PREFIX_.'xprtpromoimgtbl`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
        ? '> '.(int)$moved_xprtpromoimgtbl['position'].' AND `position` <= '.(int)$position
        : '< '.(int)$moved_xprtpromoimgtbl['position'].' AND `position` >= '.(int)$position.'
        ');
        $queryy = ' UPDATE `'._DB_PREFIX_.'xprtpromoimgtbl`
        SET `position` = '.(int)$position.'
        WHERE `id_xprtpromoimgtbl` = '.(int)$moved_xprtpromoimgtbl['id_xprtpromoimgtbl'];
        return (Db::getInstance()->execute($queryx,false)
        && Db::getInstance()->execute($queryy,false));
    }
    public static function getImages($id = NULL)
    {
    	if($id == NULL || $id == 0)
    		return false;
    	$id_lang = (int)Context::getContext()->language->id;
    	$id_shop = (int)Context::getContext()->shop->id;
    	$sql = 'SELECT pb.`promo_Images` FROM `'._DB_PREFIX_.'xprtpromotionaltbl` pb 
                INNER JOIN `'._DB_PREFIX_.'xprtpromotionaltbl_lang` pbl ON (pb.`id_xprtpromotionaltbl` = pbl.`id_xprtpromotionaltbl` AND pbl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.'xprtpromotionaltbl_shop` pbs ON (pb.`id_xprtpromotionaltbl` = pbs.`id_xprtpromotionaltbl` AND pbs.`id_shop` = '.$id_shop.')
                WHERE pb.id_xprtpromotionaltbl = '.$id;
        $sql .= ' ORDER BY pb.`position` ASC';
    	$results = Db::getInstance()->executeS($sql);
    	return isset($results[0]['promo_Images']) ? $results[0]['promo_Images'] : 0;
    }
    public static function getImageslists($id = NULL)
    {
        if($id == NULL)
            return false;
        $results = array();
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtpromoimgtbl` pb 
                INNER JOIN `'._DB_PREFIX_.'xprtpromoimgtbl_lang` pbl ON (pb.`id_xprtpromoimgtbl` = pbl.`id_xprtpromoimgtbl` AND pbl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.'xprtpromoimgtbl_shop` pbs ON (pb.`id_xprtpromoimgtbl` = pbs.`id_xprtpromoimgtbl` AND pbs.`id_shop` = '.$id_shop.')
                WHERE pb.id_xprtpromoimgtbl IN('.$id.') ';
        $sql .= ' ORDER BY pb.`position` ASC';
        $results = Db::getInstance()->executeS($sql);
        if(isset($results)){
        	foreach($results as &$result) {
        		if(isset($result['image'])){
	        		$image_path = _PS_MODULE_DIR_.xprtpromotionalblock::$mod_name.'/images/'.$result['image'];
	        		if(file_exists($image_path)){
	        			$image_uri = _MODULE_DIR_.xprtpromotionalblock::$mod_name.'/images/'.$result['image'];
	        			$image_size = @getimagesize($image_path);
	        			$result['image_width'] = isset($image_size[0])?$image_size[0]:"";
	        			$result['image_height'] = isset($image_size[1])?$image_size[1]:"";
	        			$result['image_uri'] = isset($image_uri)?$image_uri:"";
	        		}	
        		}
        	}
        }
        return $results;
    }
}