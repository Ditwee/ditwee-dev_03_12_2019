<?php
class AdminpromotionalController extends ModuleAdminController{
	protected $position_identifier = 'id_xprtpromotionaltbl';
    public function __construct()
    {
        $this->table = 'xprtpromotionaltbl';
        $this->className = 'promotionalblockclass';
        $this->module = 'xprtpromotionalblock';
        $this->lang = true;
        $this->context = Context::getContext();
        $this->bootstrap = true;
        if(Shop::isFeatureActive())
            Shop::addTableAssociation($this->table, array('type' => 'shop'));
            parent::__construct();
        $this->fields_list = array(
                            'id_xprtpromotionaltbl' => array(
                                    'title' => $this->l('Id'),
                                    'width' => 100,
                                    'type' => 'text',
                            ),
                            'maincolumn' => array(
                                    'title' => $this->l('Column'),
                                    'width' => 60,
                                    'type' => 'text',
                            ),
                            'hook' => array(
                                    'title' => $this->l('Hook'),
                                    'width' => 220,
                                    'type' => 'text',
                            ),
                            'mainstyle' => array(
                                    'title' => $this->l('Main Layout Style'),
                                    'width' => 220,
                                    'type' => 'text',
                            ),
                            'active' => array(
                                'title' => $this->l('Status'),
                                'width' => 60,
                                'align' => 'center',
                                'active' => 'status',
                                'type' => 'bool',
                                'orderby' => false
                            ),
                            'position' => array(
                                'title' => $this->l('Position'),
                                'filter_key' => 'a!position',
                                'position' => 'position',
                                'align' => 'center',
                                'class' => 'fixed-width-md'
                            )
                    );
        parent::__construct();
    }
    public function init()
    {
        parent::init();
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'xprtpromotionaltbl_shop st ON a.id_xprtpromotionaltbl=st.id_xprtpromotionaltbl && st.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
        $this->_select = 'st.id_shop';
        $this->_defaultOrderBy = 'a.position';
        $this->_defaultOrderWay = 'DESC';
        $this->_orderBy = 'position';
        $this->_orderWay = 'DESC';
        if(Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
          $this->_group = 'GROUP BY a.id_xprtpromotionaltbl';
        $this->_select = 'a.position position';
    }
    public function setMedia()
    {          
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
        $this->addJqueryPlugin('select2');
        $this->addJqueryPlugin('tablednd');
        $this->addJS(array(
                _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
                _PS_JS_DIR_.'tinymce.inc.js',
                _PS_JS_DIR_.'admin/tinymce.inc.js',
            ));
    }
    public function InitUploadedImages()
    {
        $obj = $this->object;
        $image_uri = _MODULE_DIR_.xprtpromotionalblock::$mod_name.'/images/';
        $imgintval = '';
        $data = $this->createTemplate('uploadedimages.tpl');

        $shops = false;
        if(Shop::isFeatureActive())
        $shops = Shop::getShops();
        if($shops)
        foreach ($shops as $key => $shop)
        if(!$obj->isAssociatedToShop($shop['id_shop']))
        unset($shops[$key]);
        $data->assign('shops', $shops);
        if(Context::getContext()->shop->getContext() == Shop::CONTEXT_SHOP)
            $current_shop_id = (int)Context::getContext()->shop->id;
        else
            $current_shop_id = 0;
        $languages = Language::getLanguages(true);
        $image_uploader = new HelperImageUploader('image');
        $image_uploader->setMultiple(!(Tools::getUserBrowser() == 'Apple Safari' && Tools::getUserPlatform() == 'Windows'))
            ->setUseAjax(true)->setUrl(
            Context::getContext()->link->getAdminLink('Adminpromotional').'&ajax=1&id_xprtpromotionaltbl='.(int)$obj->id
            .'&action=addProductImage');
        $max_file_size = (int)(Configuration::get('PS_LIMIT_UPLOAD_FILE_VALUE'));
        if(Tools::getvalue('id_xprtpromotionaltbl')){
            $id_xprtpromotionaltbl = Tools::getvalue('id_xprtpromotionaltbl');
            $images = xprtpromoimgtbl::getImages((int)$id_xprtpromotionaltbl);
            $imgintval = $images;
        $data->assign('promotionalobj', $this->loadObject());
            $imagelists = array();
            if(isset($images)){
                $images = explode(",",$images);
                $countImages = count($images);
                $imagelists = xprtpromoimgtbl::getImageslists($imgintval);
            }
        }else{
            $countImages = 0;
            $imagelists = '';
        }
        $iso = Context::getcontext()->language->iso_code;
        $data->assign(array(
            'iso' => file_exists(_PS_CORE_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en',
            'path_css' => _THEME_CSS_DIR_,
            'ad' => __PS_BASE_URI__.basename(_PS_ADMIN_DIR_),
            'countImages' => $countImages,
            'imgintval' => $imgintval,
            'id_xprtpromotionaltbl' => (int)Tools::getValue('id_xprtpromotionaltbl'),
            'id_category_default' => (int)0,
            'images' => $imagelists,
            'image_uri' => $image_uri,
            'iso_lang' => $languages[0]['iso_code'],
            'token' =>  $this->token,
            'table' => $this->table,
            'max_image_size' =>  $max_file_size / 1024 / 1024,
            'up_filename' => (string)Tools::getValue('virtual_product_filename_attribute'),
            'currency' => $this->context->currency,
            'current_shop_id' => $current_shop_id,
            'languages' => $this->_languages,
            'default_language' => (int)Configuration::get('PS_LANG_DEFAULT'),
            'image_uploader' => $image_uploader->render()
        ));
        return $data->fetch();
    }
    public function ajaxProcessEditPromotioal()
    {
    	$id_promotional_image = Tools::getValue("id_promotional_image");
    	$promoimages = array();
    	if($id_promotional_image){
     		$xprtpromoimgtbl = new xprtpromoimgtbl($id_promotional_image);
			$promoimages['id_xprtpromoimgtbl']	=	$xprtpromoimgtbl->id_xprtpromoimgtbl;
			$promoimages['image']	=	$xprtpromoimgtbl->image;
			$promoimages['image_path']	= _MODULE_DIR_.xprtpromotionalblock::$mod_name.'/images/'.$xprtpromoimgtbl->image;
			$promoimages['column']	=	$xprtpromoimgtbl->column;
			$promoimages['link']	=	$xprtpromoimgtbl->link;
			$promoimages['style']	=	$xprtpromoimgtbl->style;
			$promoimages['contentposition']	=	$xprtpromoimgtbl->contentposition;
			$promoimages['additionalclass']	=	$xprtpromoimgtbl->additionalclass;
			$promoimages['position']	=	$xprtpromoimgtbl->position;
			$languages = Language::getLanguages(false);
			foreach($languages as $lang){
				$promoimages['content_'.$lang['id_lang']]	=	$xprtpromoimgtbl->content[$lang['id_lang']];
			}
    	}
    	die(Tools::jsonEncode(array("promoimages" => $promoimages)));
    }
    public function ProcessEdit()
    {
        // var_dump('Id1');
    }
    public function ajaxProcessdeletePromotioalImage()
    {
        
    }
    public function ajaxProcessupdateImagePosition(){
        $res = false;
        if($json = Tools::getValue('json'))
        {
            $res = true;
            $json = stripslashes($json);
            $images = Tools::jsonDecode($json, true);
            foreach ($images as $id => $position)
            {
                $img = new xprtpromoimgtbl((int)$id);
                $img->position = (int)$position;
                $res &= $img->update();
            }
        }
        if ($res)
            $this->jsonConfirmation($this->_conf[25]);
        else
            $this->jsonError(Tools::displayError('An error occurred while attempting to move this picture.'));
    }
    public function ajaxProcesseditProductImage(){
    		$results = array();
	    	if(isset($_FILES['editimage']['name'])){
	    	    $ext = substr($_FILES['editimage']['name'],strrpos($_FILES['editimage']['name'], '.') + 1);
	    	}else{
	    	    $ext = 'jpg';
	    	}
			$img_size = (int)(Configuration::get('PS_LIMIT_UPLOAD_FILE_VALUE'));
		    $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		    $timex = time();
		    $filename = $timex.'.'.$ext;
		    $image_path = _PS_MODULE_DIR_.xprtpromotionalblock::$mod_name.'/images/'.$filename;
		    $image_uri = _MODULE_DIR_.xprtpromotionalblock::$mod_name.'/images/'.$filename;
		    $image_uploader = new HelperImageUploader('editimage');
		    $image_uploader->setAcceptTypes(array('jpeg', 'gif', 'png', 'jpg'))->setMaxSize($img_size);
		    $files = $image_uploader->process();
		    if(isset($files)){
		        foreach($files as &$file)
		        {
		            if(!ImageManager::resize($file['save_path'],$image_path,null, null,$ext,false,$error))
		            {
		                switch ($error)
		                {
		                    case ImageManager::ERROR_FILE_NOT_EXIST :
		                        $file['error'] = Tools::displayError('An error occurred while copying image, the file does not exist anymore.');
		                        break;
		                    case ImageManager::ERROR_FILE_WIDTH :
		                        $file['error'] = Tools::displayError('An error occurred while copying image, the file width is 0px.');
		                        break;
		                    case ImageManager::ERROR_MEMORY_LIMIT :
		                        $file['error'] = Tools::displayError('An error occurred while copying image, check your memory limit.');
		                        break;
		                    default:
		                        $file['error'] = Tools::displayError('An error occurred while copying image.');
		                        break;
		                }
		                $image = 'no-image.jpg';
		                continue;
		            }else{
		                $image = $filename;
		            }
		            unlink($file['save_path']);
		            unset($file['save_path']);
		        }
		    }else{
		        $file['error'] = Tools::displayError('Image Upload Failed. Please Try Again');
		    }
	    	die(Tools::jsonEncode(array("results" => $file)));
    }
    public static function AllPageExceptions()
	{
	    $id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT p.`id_product`, pl.`name`
	            FROM `'._DB_PREFIX_.'product` p
	            '.Shop::addSqlAssociation('product', 'p').'
	            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
	            WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
	    $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	    $id_lang = Context::getContext()->language->id;
	    $categories =  Category::getCategories($id_lang,true,false);
	    $controllers = Dispatcher::getControllers(_PS_FRONT_CONTROLLER_DIR_);
	    if(isset($controllers))
	        ksort($controllers);
	    $Manufacturers =  Manufacturer::getManufacturers();
	    $Suppliers =  Supplier::getSuppliers();
	    $rslt = array();
	    $rslt[0]['id'] = 'all_page';
	    $rslt[0]['name'] = 'All Pages';
	    $i = 1;
	    if(isset($controllers))
	        foreach($controllers as $r => $v){
	            $rslt[$i]['id'] = $r;
	            $rslt[$i]['name'] = 'Page : '.ucwords($r);
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'man_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'sup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier : '.$r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'cat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category : '.$cats['name'];
	            $i++;
	        }
	    if(isset($products))
	        foreach($products as $r){
	            $rslt[$i]['id'] = 'prd_'.$r['id_product'];
	            $rslt[$i]['name'] = 'Product : '. $r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'prdcat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category Product: '.$cats['name'];
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'prdman_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer Product : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'prdsup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier Product : '.$r['name'];
	            $i++;
	        }
	    return $rslt;
	}
    public function ajaxProcessaddProductImage()
    {
    	$img_size = (int)(Configuration::get('PS_LIMIT_UPLOAD_FILE_VALUE'));
    	$max_file_size = (int)($img_size*1024)*1024;
    	$id_xprtpromoimgtbl = Tools::getValue("id_xprtpromoimgtbl");
    	if($id_xprtpromoimgtbl){
    		$data = new xprtpromoimgtbl($id_xprtpromoimgtbl);
    	}else{
    		$data = new xprtpromoimgtbl();
    	}
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $timex = time();
        if(isset($_FILES['editimage']['name'])){
            $ext = substr($_FILES['editimage']['name'],strrpos($_FILES['editimage']['name'], '.') + 1);
        }else{
            $ext = 'jpg';
        }
        if(isset($_FILES['editimage']['name']) && ($max_file_size > $_FILES['editimage']['size'])){
        $filename = $timex.'.'.$ext;
        $image_path = _PS_MODULE_DIR_.xprtpromotionalblock::$mod_name.'/images/'.$filename;
        $image_uri = _MODULE_DIR_.xprtpromotionalblock::$mod_name.'/images/'.$filename;
        $image_uploader = new HelperImageUploader('editimage');
        $image_uploader->setAcceptTypes(array('jpeg', 'gif', 'png', 'jpg'))->setMaxSize($img_size);
        $files = $image_uploader->process();
        if(isset($files)){
            foreach($files as &$file)
            {
                if(!ImageManager::resize($file['save_path'],$image_path,null, null,$ext,false,$error))
                {
                    switch ($error)
                    {
                        case ImageManager::ERROR_FILE_NOT_EXIST :
                            $file['error'] = Tools::displayError('An error occurred while copying image, the file does not exist anymore.');
                            break;
                        case ImageManager::ERROR_FILE_WIDTH :
                            $file['error'] = Tools::displayError('An error occurred while copying image, the file width is 0px.');
                            break;
                        case ImageManager::ERROR_MEMORY_LIMIT :
                            $file['error'] = Tools::displayError('An error occurred while copying image, check your memory limit.');
                            break;
                        default:
                            $file['error'] = Tools::displayError('An error occurred while copying image.');
                            break;
                    }
                    $data->image = 'no-image.jpg';
                    continue;
                }else{
                    $data->image = $filename;
                }
                unlink($file['save_path']);
                unset($file['save_path']);
            }
        	}
    	}
        if(!isset($file)){
        	$file = array();
        }
        if(isset($_FILES['editimage']['name'])){
	        if($max_file_size < $_FILES['editimage']['size']){
	        	$file['error'] = Tools::displayError('Image Size Must be Less Than '.$img_size.'MB');
	        }
    	}
        // START Table data Manupulation
        $languages = Language::getLanguages(false);
        if(isset($languages))
        foreach($languages as $lang){
        	if(Tools::getvalue('content_'.$lang['id_lang'])){
        		$data->content[$lang['id_lang']] = str_replace(array("\n", "\r"), ' ', Tools::getvalue('content_'.$lang['id_lang']));
        	}else{
        		// $data->content[$lang['id_lang']] = (isset($data->content[$lang['id_lang']]) && !empty($data->content[$lang['id_lang']])) ? $data->content[$lang['id_lang']] : "";
        		$data->content[$lang['id_lang']] = "";
        	}
        }
        if(Tools::getvalue('column')){
        	$data->column = Tools::getvalue('column');
        }
        if(Tools::getvalue('link')){
        	$data->link = Tools::getvalue('link');
        }
        if(Tools::getvalue('childposition')){
        	$data->position = Tools::getvalue('childposition');
        }
        if(Tools::getvalue('style')){
        	$data->style = Tools::getvalue('style');
        }
        if(Tools::getvalue('contentposition')){
        	$data->contentposition = Tools::getvalue('contentposition');
        }
        if(Tools::getvalue('additionalclass')){
        	$data->additionalclass = Tools::getvalue('additionalclass');
        }
        if($id_xprtpromoimgtbl){
        	$file['editsettings'] = "editsettings";
        }
        $file['column'] = $data->column;
        $file['link'] = $data->link;
        $file['style'] = $data->style;
        $file['contentposition'] = $data->contentposition;
        $file['additionalclass'] = $data->additionalclass;
		if(!isset($image_uri)){
		    $image_uri = _MODULE_DIR_.xprtpromotionalblock::$mod_name.'/images/'.$data->image;
		}
        $file['imagess'] = $data->image;
        $file['image_uri'] = $image_uri;
        if($id_xprtpromoimgtbl){
    		$file['position'] = $data->position;
    	}else{
    		$file['position'] = xprtpromoimgtbl::getTopPosition()+1;
    	}
        $file['description'] = Tools::getvalue('content_'.$id_lang);
        if($data->save()){
            $file['success'] = Tools::displayError('Save successfully.');
            if($id_xprtpromoimgtbl){
            	$file['image_id'] = $id_xprtpromoimgtbl;
            }else{
            	$file['image_id'] = $data->id;
            }
        }else{
            $file['error'] = Tools::displayError('Error Found! Please Try again.');
        }
        // END Table data Manupulation
        die(Tools::jsonEncode(array("editimage" => $file)));
    }
    public function renderForm()
    {
        $uploadedimageshtml = $this->InitUploadedImages();
        $mainstyle = array(
                array('id' => "general",'name'=>"General Style"),
                array('id' => "slider",'name'=>"Slider Style"),
                array('id' => "carousel",'name'=>"Carousel Style"),
                array('id' => "small_padding",'name'=>"Small padding"),
                array('id' => "no_padding",'name'=>"No padding"),
            );
        require_once(dirname(__FILE__) . '/../../hooks/support_hook.php');
        $this->fields_form = array(
          'legend' => array(
          'title' => $this->l('Promotional Block Setting'),
            ),
            'input' => array(
                array(
                        'type' => 'select',
                        'label' => $this->l('Type'),
                        'name' => 'maincolumn',
                        'options' => array(
                            'query' => array(
                                array(
                                'id' => '12',
                                'name' => 'Single column (col-sm-12)'
                                ),
                                array(
                                'id' => '8',
                                'name' => 'Two-third column (col-sm-8)'
                                ),
                                array(
                                'id' => '6',
                                'name' => 'Two column (col-sm-6)'
                                ),
                                array(
                                'id' => '4',
                                'name' => 'Three column (col-sm-4)'
                                ),
                                array(
                                'id' => '3',
                                'name' => 'Four column (col-sm-3)'
                                ),
                                array(
                                'id' => '2',
                                'name' => 'Six column (col-sm-2)'
                                ),
                                array(
                                'id' => '1',
                                'name' => 'Twelve column (col-sm-1)'
                                )
                                
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                ),
                array(
                  'type' => 'select',
                  'label' => $this->l('Where you Display'),
                  'name' => 'hook',
                  'options' => array(
                          'query' => $support_hook,
                          'id' => 'id',
                          'name' => 'name'
                        ),
                  'desc' => $this->l('Select Your Hook Position where you want to display!')
                ),
				array(
				    'type' => 'selecttwotype',
				    'label' => $this->l('Which Page You Want to Display'),
				    'placeholder' => $this->l('Please Type Your Controller Name.'),
				    'initvalues' => self::AllPageExceptions(),
				    'name' => 'pages',
				    'desc' => $this->l('Please Type Your Specific Page Name,Category name,Product Name,For All Product specific Category select category product: category name.<br>For showing All page Type: All Page. For Home Page Type:index.')
				),
                array(
                  'type' => 'select',
                  'label' => $this->l('Main layout Style'),
                  'name' => 'mainstyle',
                  'options' => array(
                          'query' => $mainstyle,
                          'id' => 'id',
                          'name' => 'name'
                    ),
                  'desc' => $this->l('Select Your Main Layout Style!')
                ),
                array(
                    'type' => 'text',
                    'class' => 'fixed-width-xl',
                    'label' => $this->l('Margin'),
                    'name' => 'promo_margin',
                    'desc' => 'Please enter you margin. such as : 0px 0px 0px 0px (top right bottom left)',
                ), 
                array(
                    'type' => 'upmultipleimages',
                    'label' => $this->l('Images'),
                    'name' => 'upmultipleimagesname',
                    'uploadedimageshtml' => $uploadedimageshtml,
                ),                
                array(
                    'type' => 'switch',
                    'label' => $this->l('Status'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                        'id' => 'active',
                        'value' => 1,
                        'label' => $this->l('Enabled')
                        ),
                        array(
                        'id' => 'active',
                        'value' => 0,
                        'label' => $this->l('Disabled')
                        )
                    )
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        if(Shop::isFeatureActive())
              $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association:'),
                'name' => 'checkBoxShopAsso',
              );
        if(!($promotionalblockclass = $this->loadObject(true)))
            return;
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save   '),
            'class' => 'button'
        );
        return parent::renderForm();
    }
    public function ProcessPositions()
    {
    	return true;
    }
    public function ajaxProcessUpdatePositions()
    {
        $way = (int)(Tools::getValue('way'));
        $id_xprtpromotionaltbl = (int)(Tools::getValue('id'));
        $positions = Tools::getValue('xprtpromotionaltbl');

        // when changing positions in a xprtpromotionaltbl sub-list, the first array value is empty and needs to be removed
        if (!$positions[0]) {
            unset($positions[0]);
            // reset indexation from 0
            $positions = array_merge($positions);
        }

        foreach ($positions as $position => $value) {
            $pos = explode('_', $value);

            if (isset($pos[2]) && (int)$pos[2] === $id_xprtpromotionaltbl) {
                if ($promotionalblockclass = new promotionalblockclass((int)$pos[2])) {
                    if (isset($position) && $promotionalblockclass->updatePosition($way, $position)) {
                        echo 'ok position '.(int)$position.' for promotionalblockclass '.(int)$pos[1].'\r\n';
                    } else {
                        echo '{"hasError" : true, "errors" : "Can not update promotionalblockclass '.(int)$id_xprtpromotionaltbl.' to position '.(int)$position.' "}';
                    }
                } else {
                    echo '{"hasError" : true, "errors" : "This promotionalblockclass ('.(int)$id_xprtpromotionaltbl.') can t be loaded"}';
                }

                break;
            }
        }
    }
    public function renderList() {
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->_orderBy = 'position';
        return parent::renderList();
    }
}