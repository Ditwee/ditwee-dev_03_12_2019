<?php
$xippromo_images = array(
    array(
    	'lang' => array( //1
		    'content' => '<h3>fashion women</h3>  <h4>30% off</h4>  <p>Sale Off</p>',
		),
	    'notlang' => array(
		    'image' => '1.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 0,
		),
    ),
    array(//2
    	'lang' => array(
		    'content' => '<p>sale</p>',
		),
	    'notlang' => array(
		    'image' => '2.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 1,
		),
    ),
    array( //3
    	'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '3.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => '',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 2,
		),
    ),
    array( //4
    	'lang' => array(
		    'content' => '<h3>fashion men</h3>  <h4>35% off</h4>  <p>Specail</p>',
		),
	    'notlang' => array(
		    'image' => '4.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 3,
		),
    ),
    array( //5
    	'lang' => array(
		    'content' => '<h2>Sale Off</h2>',
		),
	    'notlang' => array(
		    'image' => '5.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale_off',
		    'style' => 'regular',
		    'contentposition' => 'center_middle',
		    'position' => 4,
		),
    ),
    array(//6
    	'lang' => array(
		    'content' => '<h2>Sale off</h2>',
		),
	    'notlang' => array(
		    'image' => '6.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale_off',
		    'style' => 'regular',
		    'contentposition' => 'center_middle',
		    'position' => 5,
		),
    ),
    array(//7
    	'lang' => array(
		    'content' => '<h2>Jelewy\'s Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '7.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 6,
		),
    ),
    array(//8
    	'lang' => array(
		    'content' => '<h2>Men\'s Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '8.jpg',
		    'column' => '3',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 7,
		),
    ),
    array(//9
    	'lang' => array(
		    'content' => '<h2>Women\'s Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '9.jpg',
		    'column' => '3',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 8,
		),
    ),
    array(//10
    	'lang' => array(
		    'content' => '<h2>Bag\'s Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '10.jpg',
		    'column' => '3',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 9,
		),
    ),
    array(//11
    	'lang' => array(
		    'content' => '<h2>Dress\'s Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '11.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 10,
		),
    ),
    array(//12
    	'lang' => array(
		    'content' => '<h2>Shoes Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '12.jpg',
		    'column' => '3',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 11,
		),
    ),
    array( //13
    	'lang' => array(
		    'content' => '<h2>new design<br />send her your love</h2>  <p><a class="btn btn-default">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '13.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_three',
		    'style' => 'alpha',
		    'contentposition' => 'top_middle',
		    'position' => 12,
		),
    ),
    array( //14
    	'lang' => array(
		    'content' => '<h4>fashion’s men</h4>  <h2>Celebrate Her With<br />Accessories</h2>  <p><a class="btn btn-default">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '14.jpg',
		    'column' => '8',
		    'link' => '#',
		    'additionalclass' => 'layout_style_three',
		    'style' => 'alpha',
		    'contentposition' => 'center_right',
		    'position' => 13,
		),
    ),
    array( //15
    	'lang' => array(
		    'content' => '<h2>new arrivals<br />send her your love</h2>  <p><a class="btn btn-default">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '15.jpg',
		    'column' => '8',
		    'link' => '#',
		    'additionalclass' => 'layout_style_three',
		    'style' => 'alpha',
		    'contentposition' => 'center_left',
		    'position' => 14,
		),
    ),
    array( //16
    	'lang' => array(
		    'content' => '<h2>T-shirt\'s Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '16.jpg',
		    'column' => '3',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 15,
		),
    ),
    array( //17
    	'lang' => array(
		    'content' => '<h2>Men\'s Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '17.jpg',
		    'column' => '3',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 16,
		),
    ),
    array( //18
    	'lang' => array(
		    'content' => '<h2>Women\'s Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '18.jpg',
		    'column' => '3',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 17,
		),
    ),
    array( //19
    	'lang' => array(
		    'content' => '<h2>Shoe\'s Collections</h2>  <p><a class="btn btn-default btn-white">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '19.jpg',
		    'column' => '3',
		    'link' => '#',
		    'additionalclass' => 'layout_style_two',
		    'style' => 'creative',
		    'contentposition' => 'center_middle',
		    'position' => 18,
		),
    ),
    array( //20
    	'lang' => array(
		    'content' => '<h4>fashion’s men</h4>  <h2>Celebrate Her With<br />Accessories</h2>  <p><a class="btn btn-default">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '20.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_three',
		    'style' => 'alpha',
		    'contentposition' => 'center_right',
		    'position' => 19,
		),
    ),
    array( //21
    	'lang' => array(
		    'content' => '<h4>fashion’s women</h4>  <h2>Celebrate Her With<br />Accessories</h2>  <p><a class="btn btn-default">Shop Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '21.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_three',
		    'style' => 'alpha',
		    'contentposition' => 'center_left',
		    'position' => 20,
		),
    ),
    array( //22
    	'lang' => array(
		    'content' => '<h3>men’s fashion </h3>  <h2>mid season sale</h2>  <p><a href="#">view collection </a></p>',
		),
	    'notlang' => array(
		    'image' => '22.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_four',
		    'style' => 'alpha',
		    'contentposition' => 'bottom_middle',
		    'position' => 21,
		),
    ),
    array( //23
    	'lang' => array(
		    'content' => '<h2>the biggest<br />winter collection</h2>  <p><a href="#">go to shop </a></p>',
		),
	    'notlang' => array(
		    'image' => '23.jpg',
		    'column' => '8',
		    'link' => '#',
		    'additionalclass' => 'layout_style_four',
		    'style' => 'alpha',
		    'contentposition' => 'center_left',
		    'position' => 22,
		),
    ),
    array( //24
    	'lang' => array(
		    'content' => '<h2>new design <br />send her your love</h2>  <p><a href="#">go to shop </a></p>',
		),
	    'notlang' => array(
		    'image' => '24.jpg',
		    'column' => '8',
		    'link' => '#',
		    'additionalclass' => 'layout_style_four',
		    'style' => 'alpha',
		    'contentposition' => 'center_right',
		    'position' => 23,
		),
    ),
    array( //25
    	'lang' => array(
		    'content' => '<h4>SPRING COLLECTION</h4>  <h2>big sale for season</h2>  <p><a class="btn btn-default btn-black" href="">get It Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '25.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_four',
		    'style' => 'beta',
		    'contentposition' => 'center_left',
		    'position' => 24,
		),
    ),
    array( //26
    	'lang' => array(
		    'content' => '<div class="p_bottom_100"><h4>Mother’s Day</h4>  <h2>Celebrate Her With <br />Accessories</h2>  <p><a href="#">great Celebration </a></p></div>',
		),
	    'notlang' => array(
		    'image' => '26.jpg',
		    'column' => '8',
		    'link' => '#',
		    'additionalclass' => 'layout_style_five',
		    'style' => 'regular',
		    'contentposition' => 'bottom_middle',
		    'position' => 25,
		),
    ),
    array( //27
    	'lang' => array(
		    'content' => '<h4>fashion’s women</h4>  	<h2>send her your love</h2>  	<p><a href="#">great Celebration </a></p>',
		),
	    'notlang' => array(
		    'image' => '27.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_six',
		    'style' => 'regular',
		    'contentposition' => 'bottom_middle',
		    'position' => 26,
		),
    ),
    array(//28
    	'lang' => array(
		    'content' => '<h2>new design <br />send her your love</h2>  <p><a class="btn btn-default" href="#">Show Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '28.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_three',
		    'style' => 'alpha',
		    'contentposition' => 'top_middle',
		    'position' => 27,
		),
    ),
    array(//29
    	'lang' => array(
		    'content' => '<h4>fashion’s men</h4>  <h2>new design <br />send her your love</h2>  <p><a class="btn btn-default" href="#">Show Now</a></p>',
		),
	    'notlang' => array(
		    'image' => '29.jpg',
		    'column' => '8',
		    'link' => '#',
		    'additionalclass' => 'layout_style_three',
		    'style' => 'alpha',
		    'contentposition' => 'center_right',
		    'position' => 28,
		),
    ),
    array(//30
    	'lang' => array(
		    'content' => '<h2>new arrivals <br />send her your love</h2>  <p><a href="#">great Celebration </a></p>',
		),
	    'notlang' => array(
		    'image' => '30.jpg',
		    'column' => '8',
		    'link' => '#',
		    'additionalclass' => 'layout_style_five',
		    'style' => 'alpha',
		    'contentposition' => 'center_left',
		    'position' => 29,
		),
    ),
    array(//31
    	'lang' => array(
		    'content' => '<h3>fashion men</h3>  <h4>35% off</h4>  <p>Specail</p>',
		),
	    'notlang' => array(
		    'image' => '31.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 30,
		),
    ),
    array(//32
    	'lang' => array(
		    'content' => '<p>sale</p>',
		),
	    'notlang' => array(
		    'image' => '32.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 31,
		),
    ),
    array(//33
    	'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '33.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'regular',
		    'contentposition' => 'normal',
		    'position' => 32,
		),
    ),
    array(//34
    	'lang' => array(
		    'content' => '<h3>fashion women</h3>  <h4>30% off</h4>  <p>Sale Off</p>',
		),
	    'notlang' => array(
		    'image' => '34.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 33,
		),
    ),
    array(//35
    	'lang' => array(
		    'content' => '<h3>fashion women</h3>  <h4>30% off</h4>',
		),
	    'notlang' => array(
		    'image' => '35.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 34,
		),
    ),
    array(//36
    	'lang' => array(
		    'content' => '<p>sale</p>',
		),
	    'notlang' => array(
		    'image' => '36.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 35,
		),
    ),
    array(//37
    	'lang' => array(
		    'content' => '<h3>fashion men</h3>  <h4>35% off</h4>',
		),
	    'notlang' => array(
		    'image' => '37.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 36,
		),
    ),
    array(//38
    	'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '38.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'center_right',
		    'position' => 37,
		),
    ),
    array(//39
    	'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '39.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'top_middle',
		    'position' => 38,
		),
    ),
    array(//40
    	'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '40.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale_off',
		    'style' => 'alpha',
		    'contentposition' => 'center_middle',
		    'position' => 39,
		),
    ),
    array(//41
    	'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '41.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'center_left',
		    'position' => 40,
		),
    ),
    array(//42
    	'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '42.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 41,
		),
    ),
    array(//43
    	'lang' => array(
		    'content' => '<p>Sale</p>',
		),
	    'notlang' => array(
		    'image' => '43.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale',
		    'style' => 'general',
		    'contentposition' => 'center_middle',
		    'position' => 42,
		),
    ),
    array(//44
    	'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '44.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'center_middle',
		    'position' => 43,
		),
    ),
    array(//45
    	'lang' => array(
		    'content' => '<h2>Sale Off</h2>',
		),
	    'notlang' => array(
		    'image' => '45.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale_off',
		    'style' => 'general',
		    'contentposition' => 'center_middle',
		    'position' => 44,
		),
    ),
    array(//46
    	'lang' => array(
		    'content' => '<h2>Sale Off</h2>',
		),
	    'notlang' => array(
		    'image' => '46.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale_off',
		    'style' => 'regular',
		    'contentposition' => 'center_middle',
		    'position' => 45,
		),
    ),
    array(//47
    	'lang' => array(
		    'content' => '<h2>Sale Off</h2>',
		),
	    'notlang' => array(
		    'image' => '47.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale_off',
		    'style' => 'regular',
		    'contentposition' => 'center_middle',
		    'position' => 46,
		),
    ),
    array(//48
    	'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '48.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'none',
		    'style' => 'regular',
		    'contentposition' => 'center_middle',
		    'position' => 47,
		),
    ),
    array(//49
    	'lang' => array(
		    'content' => '<h3>fashion women</h3>  <h4>30% off</h4>',
		),
	    'notlang' => array(
		    'image' => '49.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 3,
		),
    ),
    array(//50
    	'lang' => array(
		    'content' => '<p>sale</p>',
		),
	    'notlang' => array(
		    'image' => '50.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale',
		    'style' => 'alpha',
		    'contentposition' => 'normal',
		    'position' => 2,
		),
    ),
    array(//51
    	'lang' => array(
		    'content' => '<h3>fashion men</h3>  <h4>35% off</h4>',
		),
	    'notlang' => array(
		    'image' => '51.jpg',
		    'column' => '4',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 1,
		),
    ),
	array(//52 
		'lang' => array(
		    'content' => '<h2>fashion men</h2>  <h4>35% off</h4>',
		),
	    'notlang' => array(
		    'image' => '52.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_seven',
		    'style' => 'general',
		    'contentposition' => 'center_right',
		    'position' => 1,
		),
	),
	array(//53
		'lang' => array(
		    'content' => '<h3>specail to day</h3>  <p><a href="#" class="btn btn-default">Shop now</a></p>',
		),
	    'notlang' => array(
		    'image' => '53.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_seven',
		    'style' => 'general',
		    'contentposition' => 'top_middle',
		    'position' => 2,
		),
	),
	array(//54
		'lang' => array(
		    'content' => '<h2>Sale Off</h2>',
		),
	    'notlang' => array(
		    'image' => '54.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale_off',
		    'style' => 'regular',
		    'contentposition' => 'center_middle',
		    'position' => 3,
		),
	),
	array(//55
		'lang' => array(
		    'content' => '<h2>fashion women</h2>  <h4>40% off</h4>',
		),
	    'notlang' => array(
		    'image' => '55.jpg',
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_seven',
		    'style' => 'regular',
		    'contentposition' => 'center_left',
		    'position' => 4,
		),
	),
	array(//56
		'lang' => array(
		    'content' => '<p class="special_off">35% off</p>',
		),
	    'notlang' => array(
		    'image' => '56.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 5,
		),
	),
	array(//57
		'lang' => array(
		    'content' => '<p>Sale</p>',
		),
	    'notlang' => array(
		    'image' => '57.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 6,
		),
	),
	array(//58
		'lang' => array(
		    'content' => '',
		),
	    'notlang' => array(
		    'image' => '58.jpg',
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'center_middle',
		    'position' => 7,
		),
	),
	array(//59 // 68
		'lang' => array(
		    'content' => '<h3>fashion men</h3><h4>35% off</h4>',
		),
	    'notlang' => array(
		    'image' => '370x230.jpg', // 1478331506.jpg
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 7,
		),
	),
	array(//60 //69
		'lang' => array(
		    'content' => '<h3>fashion women</h3><h4>30% off</h4>',
		),
	    'notlang' => array(
		    'image' => '370x230.jpg', // 1478331561.jpg
		    'column' => '6',
		    'link' => '#',
		    'additionalclass' => 'layout_style_one',
		    'style' => 'general',
		    'contentposition' => 'normal',
		    'position' => 8,
		),
	),
	array(//61 //70
		'lang' => array(
		    'content' => '<h2>Sale Off</h2>',
		),
	    'notlang' => array(
		    'image' => '157x241.jpg', // 1478331833.jpg
		    'column' => '12',
		    'link' => '#',
		    'additionalclass' => 'layout_style_sale_off',
		    'style' => 'regular',
		    'contentposition' => 'center_middle',
		    'position' => 9,
		),
	),
);

$xippromo_tbl = array(
    array( //1
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '1',
	    	'maincolumn' => '4',
	    	'hook' => 'displayHomeTop',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 8,
	    	'promo_margin' => '',
		),
    ),
    array( //2
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '2,3',
	    	'maincolumn' => '4',
	    	'hook' => 'displayHomeTop',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 9,
	    	'promo_margin' => '',
		),
    ),
    array( //3
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '4',
	    	'maincolumn' => '4',
	    	'hook' => 'displayHomeTop',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 10,
	    	'promo_margin' => '',
		),
    ),
    array( //4
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '5,6',
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeBottomRight',
	    	'mainstyle' => 'slider',
	    	'active' => 0,
	    	'position' => 11,
	    	'promo_margin' => '',
		),
    ),
    array( //5
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '7,8,9,10,11,12',
	    	'maincolumn' => '12',
	    	'hook' => 'displayTopColumn',
	    	'mainstyle' => 'small_padding',
	    	'active' => 1,
	    	'position' => 12,
	    	'promo_margin' => '0px 0px 60px 0px',
		),
    ),
    array( //6
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '13,14,15',
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeFullWidthBottom',
	    	'mainstyle' => 'small_padding',
	    	'active' => 1,
	    	'position' => 13,
	    	'promo_margin' => '0px 0px 60px 0px ',
		),
    ),
    array( //7
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '16,17,18,19',
	    	'maincolumn' => '12',
	    	'hook' => 'displayTopColumn',
	    	'mainstyle' => 'no_padding',
	    	'active' => 0,
	    	'position' => 14,
	    	'promo_margin' => '0px 0px 60px 0px',
		),
    ),
    array( //8
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '20,21',
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeFullWidthMiddle',
	    	'mainstyle' => 'no_padding',
	    	'active' => 0,
	    	'position' => 15,
	    	'promo_margin' => '0px 0px 60px 0px',
		),
    ),
    array( //9
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '22,23,24',
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeTop',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 16,
	    	'promo_margin' => '0px 0px 30px 0px ',
		),
    ),
    array( //10
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '25',
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeFullWidthBottom',
	    	'mainstyle' => 'no_padding',
	    	'active' => 0,
	    	'position' => 17,
	    	'promo_margin' => '0px 0px 60px 0px',
		),
    ),
    array( //11
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '26,27,28,29,30',
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeFullWidthMiddle',
	    	'mainstyle' => 'small_padding',
	    	'active' => 0,
	    	'position' => 18,
	    	'promo_margin' => '0px 0px 0px 0px ',
		),
    ),
    array( //12
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '31',
	    	'maincolumn' => '4',
	    	'hook' => 'displayHomeTop',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 19,
	    	'promo_margin' => '0px 0px 30px 0px',
		),
    ),
    array( //13
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '32,33',
	    	'maincolumn' => '4',
	    	'hook' => 'displayHomeTop',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 20,
	    	'promo_margin' => '0px 0px 30px 0px',
		),
    ),
    array( //14
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '34',
	    	'maincolumn' => '4',
	    	'hook' => 'displayHomeTop',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 21,
	    	'promo_margin' => '0px 0px 30px 0px',
		),
    ),
    array( //15
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '35,36,37',
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeTop',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 22,
	    	'promo_margin' => '0px 0px 30px 0px ',
		),
    ),
    array( //16
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '38,39,40,41,42,43,44',
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeTopLeft',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 23,
	    	'promo_margin' => '0px 0px 30px 0px ',
		),
    ),
    array( //17
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '45',
	    	'maincolumn' => '12',
	    	'hook' => 'displayProductRightSidebar',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 24,
	    	'promo_margin' => '0px 0px 30px 0px',
		),
    ),
    array( //18
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '46',
	    	'maincolumn' => '12',
	    	'hook' => 'displayLeftColumn',
	    	'mainstyle' => 'general',
	    	'active' => 1,
	    	'position' => 25,
	    	'promo_margin' => '0px 0px 30px 0px ',
		),
    ),
    array( //19
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '47',
	    	'maincolumn' => '12',
	    	'hook' => 'displayLeftColumn',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 26,
	    	'promo_margin' => '0px 0px 30px 0px ',
		),
    ),
    array( //20
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '48',
	    	'maincolumn' => '12',
	    	'hook' => 'displayLeftColumn',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 27,
	    	'promo_margin' => '0px 0px 30px 0px ',
		),
    ),
    array( //21
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '49,50,51', //53,54,55
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeTop',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 22,
	    	'promo_margin' => '0px 0px 30px 0px ',
		),
    ),
    array( //22
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '52,53,54,55,56,57,58', //56,57,58,59,60,61,62
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeTopLeft',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 22,
	    	'promo_margin' => '0px 0px 30px 0px ',
		),
    ),
    array( //23
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '59,60', //68,69
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeTopRight',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 22,
	    	'promo_margin' => '0px 0px 0px 0px ',
		),
    ),
    array( //24
    	'lang' => array(
    		'text' => '',
		),
	    'notlang' => array(
	    	'promo_Images' => '61', //70
	    	'maincolumn' => '12',
	    	'hook' => 'displayHomeTopLeft',
	    	'mainstyle' => 'general',
	    	'active' => 0,
	    	'position' => 22,
	    	'promo_margin' => '0px 0px 30px 0px ',
		),
    ),
);