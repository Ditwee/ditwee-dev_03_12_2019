<?php
$support_hook = array(
    array(
        'id' => 'displayBanner',
        'name' => 'displayBanner',
    ),
     array(
        'id' => 'displayFooter',
        'name' => 'displayFooter',
    ),
     array(
        'id' => 'displayFooterProduct',
        'name' => 'displayFooterProduct',
    ),
    array(
        'id' => 'displayHome',
        'name' => 'displayHome',
    ),
    array(
        'id' => 'displayLeftColumn',
        'name' => 'displayLeftColumn',
    ),
     array(
        'id' => 'displayLeftColumnProduct',
        'name' => 'displayLeftColumnProduct',
    ),   
     array(
        'id' => 'displayMaintenance',
        'name' => 'displayMaintenance',
    ),   
     array(
        'id' => 'displayMyAccountBlock',
        'name' => 'displayMyAccountBlock',
    ),    
     array(
        'id' => 'displayMyAccountBlockfooter',
        'name' => 'displayMyAccountBlockfooter',
    ),     
     array(
        'id' => 'displayRightColumn',
        'name' => 'displayRightColumn',
    ),   
     array(
        'id' => 'displayRightColumnProduct',
        'name' => 'displayRightColumnProduct',
    ),   
     array(
        'id' => 'displayTop',
        'name' => 'displayTop',
    ),   
     array(
        'id' => 'displayTopColumn',
        'name' => 'displayTopColumn',
    ),   
     array(
        'id' => 'displayHomeTop',
        'name' => 'displayHomeTop',
    ),   
     array(
        'id' => 'displayHomeTopLeft',
        'name' => 'displayHomeTopLeft',
    ),   
     array(
        'id' => 'displayHomeTopRight',
        'name' => 'displayHomeTopRight',
    ),   
     array(
        'id' => 'displayHomeTopLeftOne',
        'name' => 'displayHomeTopLeftOne',
    ),   
     array(
        'id' => 'displayHomeTopLeftTwo',
        'name' => 'displayHomeTopLefTtwo',
    ),   
     array(
        'id' => 'displayHomeTopRightOne',
        'name' => 'displayHomeTopRightOne',
    ),   
     array(
        'id' => 'displayHomeTopRightTwo',
        'name' => 'displayHomeTopRightTwo',
    ),

      array(
        'id' => 'displayHomeBottomLeft',
        'name' => 'displayHomeBottomLeft',
    ),   
     array(
        'id' => 'displayHomeBottomRight',
        'name' => 'displayHomeBottomRight',
    ),   
     array(
        'id' => 'displayHomeBottomLeftOne',
        'name' => 'displayHomeBottomLeftOne',
    ),   
     array(
        'id' => 'displayHomeBottomLeftTwo',
        'name' => 'displayHomeBottomLefTtwo',
    ),   
     array(
        'id' => 'displayHomeBottomRightOne',
        'name' => 'displayHomeBottomRightOne',
    ),   
     array(
        'id' => 'displayHomeBottomRightTwo',
        'name' => 'displayHomeBottomRightTwo',
    ),  
     array(
        'id' => 'displayHomeFullWidthBottom',
        'name' => 'displayHomeFullWidthBottom',
    ),  
     array(
        'id' => 'displayHomeFullWidthMiddle',
        'name' => 'displayHomeFullWidthMiddle',
    ),  
     array(
        'id' => 'displayProductRightSidebar',
        'name' => 'displayProductRightSidebar',
    ),  
     array(
        'id' => 'displayCatTop',
        'name' => 'displayCatTop',
    ),
);
