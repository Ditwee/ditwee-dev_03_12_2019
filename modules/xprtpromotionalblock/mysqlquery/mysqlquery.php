<?php

$mysqlquery = array();

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtpromotionaltbl` (
				`id_xprtpromotionaltbl` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`promo_Images` text NOT NULL,
				`maincolumn` VARCHAR(55) NOT NULL,
				`hook` VARCHAR(300) NOT NULL,
				`mainstyle` VARCHAR(300) NOT NULL,
				`promo_margin` VARCHAR(150) NOT NULL,
				`pages` VARCHAR(150) NOT NULL,
				`active` int(10) NOT NULL,
				`position`int(10) NOT NULL ,
				PRIMARY KEY (`id_xprtpromotionaltbl`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtpromotionaltbl_lang` (
				`id_xprtpromotionaltbl` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL ,
				`text` text NOT NULL,
				PRIMARY KEY (`id_xprtpromotionaltbl`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtpromotionaltbl_shop` (
			  `id_xprtpromotionaltbl` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprtpromotionaltbl`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtpromoimgtbl` (
				`id_xprtpromoimgtbl` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`image` VARCHAR(300) NOT NULL,
				`column` VARCHAR(55) NOT NULL,
				`link` VARCHAR(400) NOT NULL,
				`additionalclass` VARCHAR(400) NOT NULL,
				`style` VARCHAR(100) NOT NULL,
				`contentposition` VARCHAR(100) NOT NULL,
				`position`int(10) NOT NULL ,
				PRIMARY KEY (`id_xprtpromoimgtbl`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtpromoimgtbl_lang` (
				`id_xprtpromoimgtbl` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL ,
				`content` text NOT NULL,
				PRIMARY KEY (`id_xprtpromoimgtbl`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtpromoimgtbl_shop` (
			  `id_xprtpromoimgtbl` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprtpromoimgtbl`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;


$mysqlquery_u = array();

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtpromotionaltbl`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtpromotionaltbl_lang`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtpromotionaltbl_shop`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtpromoimgtbl`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtpromoimgtbl_lang`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtpromoimgtbl_shop`';

