{extends file="helpers/form/form.tpl"}
{block name="field"}
	{if ($input.type == 'upmultipleimages')}
		{$input.uploadedimageshtml}
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
{block name="input"}
	{if ($input.type == 'selecttwotype')}
			<div class="{if isset($input.hideclass)}{$input.hideclass}{/if} {$input.name} {$input.name}_class" id="{$input.name}_id">
			<select name="selecttwotype_{$input.name}" class="selecttwotype_{$input.name}_cls" id="selecttwotype_{$input.name}_id" multiple="true">
			    {foreach from=$input.initvalues item=initval}
			        {if isset($fields_value[$input.name])}
			            {assign var=settings_def_value value=","|explode:$fields_value[$input.name]}
			            {if $initval['id']|in_array:$settings_def_value}
			                {$selected = 'selected'}
			            {else}
			                {$selected = ''}
			            {/if}
			        {else}
			            {$selected = ''}
			        {/if}
			        <option {$selected} value="{$initval['id']}">{$initval['name']}</option>
			    {/foreach}
			</select>
			<input type="hidden" name="{$input.name}" id="{$input.name}" value="{if isset($input.defvalues)}{$input.defvalues}{else}{$fields_value[$input.name]}{/if}" class=" {$input.name} {$input.type}_field">
			</div>
			<script type="text/javascript">
			    // START SELECT TWO CALLING
			    $(function(){
			        var defVal = $("input#{$input.name}").val();
			        if(defVal.length){
			            var ValArr = defVal.split(',');
			            for(var n in ValArr){
			                $( "select#selecttwotype_{$input.name}_id" ).children('option[value="'+ValArr[n]+'"]').attr('selected','selected');
			            }
			        }
			        $( "select#selecttwotype_{$input.name}_id" ).select2( { placeholder: "{$input.placeholder}", width: 200, tokenSeparators: [',', ' '] } ).on('change',function(){
			            var data = $(this).select2('data');
			            var select = $(this);
			            var field = select.next("input#{$input.name}");
			            var saved = '';
			            select.children('option').attr('selected',null);
			            if(data.length)
			                $.each(data, function(k,v){
			                    var selected = v.id;   
			                    select.children('option[value="'+selected+'"]').attr('selected','selected');
			                    if(k > 0)
			                        saved += ',';
			                    saved += selected;                                
			                });
			             field.val(saved);   
			        });
			    });
 			// END SELECT TWO CALLING
			</script>
			<style type="text/css">
				.select2-container.select2-container-multi
				{ 
					width: 100% !important;
				}
			</style>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}



<div class="well" style="">
	<div id="image-files-list"><div class="form-group"><span><i class="icon-picture-o"></i> <strong>myaccount.png</strong> (341.23 KB)</span><button class="btn btn-default pull-right" type="button"><i class="icon-trash"></i> Remove file</button></div></div>
	<button class="ladda-button btn btn-primary" data-style="expand-right" type="button" id="image-upload-button" style="">
		<span class="ladda-label"><i class="icon-check"></i> Upload files</span>
	<span class="ladda-spinner"></span></button>
</div>