<div id="promotional-images" class="panel promotional-tab">
	<input type="hidden" name="submitted_tabs[]" value="Images" />
	<input type="hidden" name="promo_Images" value="{$imgintval}" id="promo_Images_id" class="promo_Images_class" />
	<div class="panel-heading tab" >
		{l s='Images'}
		<span class="badge" id="countImage">{$countImages}</span>
	</div>
	<div class="row">

		<!-- Start field -->
		<div class="form-group">
			<label class="control-label col-lg-3">
				{l s='Additional Style class'}
			</label>
			<div class="col-lg-9">
			
				<select name="additionalclass" id="xprtida_additionalclass" class="column">
					<option value="none">{l s='None'}</option>
					<option value="layout_style_one">{l s='layout style one'}</option>
					<option value="layout_style_two">{l s='layout style two'}</option>
					<option value="layout_style_three">{l s='layout style three'}</option>
					<option value="layout_style_four">{l s='layout style four'}</option>
					<option value="layout_style_five">{l s='layout style five'}</option>
					<option value="layout_style_six">{l s='layout style six'}</option>
					<option value="layout_style_seven">{l s='layout style seven'}</option>
					<option value="layout_style_eight">{l s='layout style eight'}</option>
					<option value="layout_style_nine">{l s='layout style nine'}</option>
					<option value="layout_style_sale">{l s='layout style sale'}</option>
					<option value="layout_style_sale_off">{l s='layout style sale off'}</option>
				</select>

			</div>
		</div>
		<!-- End field -->

		<!-- Start field -->
		<div class="form-group">
			<label class="control-label col-lg-3">
				<span class="label-tooltip" data-toggle="tooltip"
					title="{l s='Invalid characters:'} <>;=#{}">
					{l s='Description'}
				</span>
			</label>
			<div class="col-lg-9">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
				<div class="translatable-field row lang-{$language.id_lang}">
					<div class="col-lg-6">
				{/if}
						<textarea name="content_{$language.id_lang}" id="xprtida_content_{$language.id_lang}" class=" {if isset($input_class)}{$input_class}{/if}" rows="10" cols="150">{if isset($promotionalobj->text)}{$promotionalobj->text[$language.id_lang]|escape:'html':'UTF-8'}{/if}</textarea>
				{if $languages|count > 1}
					</div>
					<div class="col-lg-2">
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1">
							{$language.iso_code}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							{foreach from=$languages item=language}
							<li>
								<a href="javascript:hideOtherLanguage({$language.id_lang});">{$language.name}</a>
							</li>
							{/foreach}
						</ul>
					</div>
				</div>
				{/if}
			{/foreach}
			</div>
		</div>
		<!-- End field -->

		<!-- Start field -->
		<div class="form-group">
			<label class="control-label col-lg-3">
				{l s='Column'}
			</label>
			<div class="col-lg-9">
				<select name="column" id="xprtida_column" class="column">
					<option value="12">{l s='Single column (col-sm-12)'}</option>
					<option value="8">{l s='Two-third column (col-sm-8)'}</option>
					<option value="6">{l s='Two column (col-sm-6)'}</option>
					<option value="4">{l s='Three column (col-sm-4)'}</option>
					<option value="3">{l s='Four column (col-sm-3)'}</option>
					<option value="2">{l s='Six column (col-sm-2)'}</option>
					<!-- <option value="9">Nine Column</option> -->
					<option value="1">{l s='Twelve column (col-sm-1)'}</option>
				</select>
			</div>
		</div>
		<!-- End field -->
		<!-- Start field -->
		<div class="form-group">
			<label class="control-label col-lg-3">
				{l s='Link'}
			</label>
			<div class="col-lg-9">
				<input type="text" id="xprtida_link" name="link" value="#"/>
				<input type="hidden" id="xprtida_active" name="childactive" value="1"/>
				<input type="hidden" id="xprtida_position" name="childposition" value="0"/>
			</div>
		</div>
		<!-- End field -->
		<!-- Start field -->
		<div class="form-group">
			<label class="control-label col-lg-3">
				{l s='Effect Style'}
			</label>
			<div class="col-lg-9">
				<select name="style" id="xprtida_style" class="style">
					<option value="regular">{l s='Regular'}</option>
					<option value="general">{l s='General'}</option>
					<option value="classic">{l s='Classic'}</option>
					<option value="expand">{l s='Expand'}</option>
					<option value="creative">{l s='Creative'}</option>
					<option value="alpha">{l s='Alpha'}</option>
					<option value="beta">{l s='Beta'}</option>
				</select>
			</div>
		</div>
		<!-- End field -->
		<!-- Start field -->
		<div class="form-group">
			<label class="control-label col-lg-3">
				{l s='Content position'}
			</label>
			<div class="col-lg-9">
				<select name="contentposition" id="xprtida_contentposition" class="contentposition">
					<option value="normal">{l s='Normal'}</option>
					<option value="top_left">{l s='Top left'}</option>
					<option value="top_middle">{l s='Top middle'}</option>
					<option value="top_right">{l s='Top right'}</option>
					<option value="center_left">{l s='Center left'}</option>
					<option value="center_middle">{l s='Center middle'}</option>
					<option value="center_right">{l s='Center right'}</option>
					<option value="bottom_left">{l s='Bottom left'}</option>
					<option value="bottom_middle">{l s='Bottom middle'}</option>
					<option value="bottom_right">{l s='Bottom right'}</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<!-- start -->
			<div class="form-group"> <!-- start image field -->
				<label class="control-label col-lg-3">

				</label>
				<div class="col-lg-9">
					<div class="form-group">
						<div class="col-lg-12 displayimageblock">

						</div>
					</div>
				</div>
			</div>
			<!-- end -->
		</div>
		<!-- End field -->
		<div class="form-group edit_up_img_blck">
			<!-- start -->
			<div class="form-group"> <!-- start image field -->
				<label class="control-label col-lg-3 file_upload_label">
					Edit this image to this  Promotional Block
				</label>
				<div class="col-lg-9">
					<div class="form-group">
						<div class="col-lg-12">
							<input id="editimage" type="file" name="editimage" data-url="index.php?controller=Adminpromotional&token={$token|escape:'html':'UTF-8'}&ajax=1&id_xprtpromotionaltbl={$id_xprtpromotionaltbl}&action=addProductImage">
						</div>
					</div>
				</div>
			</div>
			<!-- end -->
		</div>
		 <!-- start image field -->
		 <!-- End Image Field -->
		<div class="form-group">
			<div class="pull-right"><button id="add_new_block_img" class="btn btn-default"><i class="icon-plus-sign"></i>Add New</button></div>
		</div>
	</div> <!-- row -->

	<table class="table tableDnD" id="imageTable">
		<thead>
			<tr class="nodrag nodrop">
				<th class="center fixed-width-xs"><span class="title_box">{l s='ID'}</span></th>
				<th class="center fixed-width-xs"><span class="title_box">{l s='Image'}</span></th>
				<th class="center fixed-width-xs"><span class="title_box">{l s='Column'}</span></th>
				<th class="center fixed-width-xs"><span class="title_box">{l s='Style'}</span></th>
				<th class="center fixed-width-xs"><span class="title_box">{l s='Content Position'}</span></th>
				{* <th class="center fixed-width-xs"><span class="title_box">{l s='Additional Style'}</span></th> *}
				<th class="center fixed-width-xs"><span class="title_box">{l s='Position'}</span></th>
				<th class="center fixed-width-xs"><span class="title_box">{l s='Edit'}</span></th>
				<th class="center fixed-width-xs"><span class="title_box">{l s='Delete'}</span></th>
			</tr>
		</thead>
		<tbody id="imageList">
		</tbody>
	</table>
	<table id="lineType" style="display:none;">
		<tr id="image_id">
			<td>
				<a href="{$smarty.const._THEME_PROD_DIR_}image_path.jpg" class="fancybox">
					<img
						src="{* {$smarty.const._THEME_PROD_DIR_}{$iso_lang}-default-{$imageType}.jpg *}"
						alt="legend"
						title="legend"
						class="img-thumbnail" />
				</a>
			</td>
			<td>{l s='Column'}</td>
			<td id="td_image_id" class="pointer dragHandle center positionImage">
				<div class="dragGroup">
					<div class="positions">
						{l s='image_position'}
                    </div>
                </div>
			</td>
			<td>
				<a href="#" class="edit_promotioal_image pull-right btn btn-default" >
					<i class="icon-trash"></i> {l s='Edit'}
				</a>
			</td>
			<td>
				<a href="#" class="delete_promotioal_image pull-right btn btn-default" >
					<i class="icon-modify"></i> {l s='Delete this image'}
				</a>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
		var upbutton = '{l s='Upload an image'}';
		var come_from = '{$table}';
		var success_add =  '{l s='The image has been successfully added.'}';
		var id_tmp = 0;
		var current_shop_id = {$current_shop_id|intval};
		{literal}
		//Ready Function
		function addpromoimages(id){
			if(typeof id !== 'undefined'){
				if($("#promo_Images_id").val() == ''){
					$("#promo_Images_id").val(id);
				}else{
					$("#promo_Images_id").val($("#promo_Images_id").val()+','+id);
				}
			}
		}
		function resetallfielddata(){
			$('[id^="xprtida_"]').each(function()
			{
				$(this).val("");
			});
			$("#editimage").val("");
		}
		function uppercasefunc(str) {
			str = str.replace("_", " ");
		  return (str + '')
		    .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
		      return $1.toUpperCase();
		    });
		}
		function imageLine(imagedata)
		{
			var line;

			line = '<tr id="'+imagedata.id+'">';

			line += '<td  class="center fixed-width-xs">'+imagedata.id+'</td>';

			line += '<td class="center fixed-width-xs"><a href="'+imagedata.img_src+'" class="fancybox imglinehrf_'+imagedata.id+'"><img src="'+imagedata.img_src+'" alt="" title="" class="img-thumbnail imglinesrc_'+imagedata.id+'"></a></td>';

			line += '<td class="center fixed-width-xs imglinecolumn_'+imagedata.id+'">'+imagedata.column+'</td>';

			line += '<td class="center fixed-width-xs imglinestyle_'+imagedata.id+'">'+uppercasefunc(imagedata.style)+'</td>';

			line += '<td class="center fixed-width-xs imglineconpos_'+imagedata.id+'">'+uppercasefunc(imagedata.contentposition)+'</td>';

			// line += '<td class="center fixed-width-xs imglineaddclass_'+imagedata.id+'">'+uppercasefunc(imagedata.additionalclass)+'</td>';

			line += '<td id="td_'+imagedata.id+'" class="center fixed-width-xs pointer dragHandle center positionImage"><div class="dragGroup"><div class="positions">'+imagedata.position+'</div></div></td>';

			line += '<td class="center fixed-width-xs"><a href="#" class="edit_promotioal_image pull-right btn btn-default" data-id="'+imagedata.id+'"><i class="icon-pencil"></i> Edit</a></td>';

			line += '<td class="center fixed-width-xs"><a href="#" class="delete_promotioal_image pull-right btn btn-default" data-id="'+imagedata.id+'"><i class="icon-trash"></i> Delete</a></td>';

			line += '</tr>';

			$("#imageList").append(line);
		}
		$(document).ready(function(){
			// start xpert
					$("#add_new_block_img").on('click',function(e){
						e.preventDefault();
						var actionurl = $("#editimage").data("url");
						var file_data = $("#editimage").prop("files")[0]; 
						var form_data = new FormData();  
						$('[id^="xprtida_"]').each(function()
						{
							id = $(this).prop("id").replace("xprtida_", "");
							form_data.append(id, $(this).val());
						});
						form_data.append("editimage",file_data); 
						$.ajax({
			                url: actionurl,
			                dataType: 'json',
			                cache: false,
			                contentType: false,
			                processData: false,
			                data: form_data,
			                type: 'post',
			                error: function(data){
			                	if(typeof data.responseText == 'undefined'){
			                		$.growl.error({ title: "", message: "Something Wrong ! Please Try Again With Different Value."});
			                	}else{
			                		$.growl.error({ title: "", message: data.responseText});
			                	}
			                },
			                success: function(data){
    			                	// start
    			                	if(typeof data.editimage.editsettings == 'undefined'){
    			                		addpromoimages(data.editimage.image_id);
    			                		$("#imageTable").tableDnDUpdate();
    			                			imageLine({
    			                				id:data.editimage.image_id,
    			                				img_src:''+data.editimage.image_uri+'',
    			                				column:''+data.editimage.column+'',
    			                				style:''+data.editimage.style+'',
    			                				contentposition:''+data.editimage.contentposition+'',
    			                				additionalclass:''+data.editimage.additionalclass+'',
    			                				position:''+data.editimage.position+'',
    			                			});
    			                		resetallfielddata();
    			                		$.growl({title: "", message: "successfully Added"});
    			                	}else{
    			                		$("#xprtida_id_xprtpromoimgtbl").remove();
    			                		$(".displaypromoimg").remove();
    			                		$(".cancle_new_block_imgprnt").remove();
    			                		$("#add_new_block_img").text("Add New Block");
    									$(".imglinehrf_"+data.editimage.image_id).attr("href",data.editimage.image_uri);
    									$(".imglinesrc_"+data.editimage.image_id).attr("src",data.editimage.image_uri);
    									$(".imglinecolumn_"+data.editimage.image_id).text(data.editimage.column);
    									$(".imglinestyle_"+data.editimage.image_id).text(uppercasefunc(data.editimage.style));
    									$(".imglineconpos_"+data.editimage.image_id).text(uppercasefunc(data.editimage.contentposition));
    									$(".imglineaddclass_"+data.editimage.image_id).text(uppercasefunc(data.editimage.additionalclass));
    									resetallfielddata();
    									$.growl({title: "", message: "successfully Updated"});
    			                	}
    			                	// end
			                  }
					    });
					});
			// end xpert
			$("#promotional-images").on('click','#cancle_new_block_img', function(e){
				e.preventDefault();
				$("#xprtida_id_xprtpromoimgtbl").remove();
				$(".displaypromoimg").remove();
				$("#add_new_block_img").text("Add New Block");
				$(this).remove();
				resetallfielddata();
			});
		});
		$(document).ready(function(){
			{/literal}
				{if isset($images) && !empty($images)}
					{foreach from=$images item=image}
						{if isset($image.position) && isset($image.image)}
							imageLine({literal}{{/literal}
								id:{$image.id_xprtpromoimgtbl},
								img_src:'{$image_uri}{$image.image}',
								column:'{$image.column}',
								style:'{$image.style}',
								contentposition:'{$image.contentposition}',
								additionalclass:'{$image.additionalclass}',
								position:'{$image.position}',
							{literal}}{/literal});
						{/if}
					{/foreach}
				{/if}
			{literal}
			
			var originalOrder = false;
			$("#imageTable").tableDnD(
			{	dragHandle: 'dragHandle',
                                onDragClass: 'myDragClass',
                                onDragStart: function(table, row) {
                                        originalOrder = $.tableDnD.serialize();
                                        reOrder = ':even';
                                        if (table.tBodies[0].rows[1] && $('#' + table.tBodies[0].rows[1].id).hasClass('alt_row'))
                                                reOrder = ':odd';
                                        $(table).find('#' + row.id).parent('tr').addClass('myDragClass');
                                },
				onDrop: function(table, row) {
					if (originalOrder != $.tableDnD.serialize()) {
						current = $(row).attr("id");
						stop = false;
						image_up = "{";
						$("#imageList").find("tr").each(function(i) {
							$("#td_" +  $(this).attr("id")).html('<div class="dragGroup"><div class="positions">'+(i + 1)+'</div></div>');
							if (!stop || (i + 1) == 2)
								image_up += '"' + $(this).attr("id") + '" : ' + (i + 1) + ',';
						});
						image_up = image_up.slice(0, -1);
						image_up += "}";
						updateImagePosition(image_up);
					}
				}
			});
			/**
			 * on success function
			 */
			function afterDeletePromotioalImage(data)
			{
				data = $.parseJSON(data);
				if (data)
				{
					cover = 0;
					id = data.content.id;
					if (data.status == 'ok')
					{
						if ($("#" + id + ' .covered').hasClass('icon-check-sign'))
							cover = 1;
						$("#" + id).remove();
					}
					if (cover)
						$("#imageTable tr").eq(1).find(".covered").addClass('icon-check-sign');
					$("#countImage").html(parseInt($("#countImage").html()) - 1);
					refreshImagePositions($("#imageTable"));
					showSuccessMessage(data.confirmations);
				}
			}
			function afterpromotionimageedit(data){
				data = jQuery.parseJSON(data);
				datax = data.promoimages;
					{/literal}
						{foreach from=$languages item=language}{literal}
						var contentx = datax.content_{/literal}{$language.id_lang}{literal};
							$("#xprtida_content_{/literal}{$language.id_lang}{literal}").val(contentx);{/literal}
						{/foreach}
					{literal}
					$('#xprtida_column option[value='+datax.column+']').attr('selected','selected');
					$('#xprtida_link').val(datax.link);
					$('#xprtida_additionalclass').val(datax.additionalclass);
					$('#xprtida_position').val(datax.position);
					$('#xprtida_style option[value='+datax.style+']').attr('selected','selected');
					$('#xprtida_contentposition option[value='+datax.contentposition+']').attr('selected','selected');
					$("#xprtida_id_xprtpromoimgtbl").remove();
					$(".edit_up_img_blck").append("<input type='hidden' id='xprtida_id_xprtpromoimgtbl' value='"+datax.id_xprtpromoimgtbl+"' name='xprtida_id_xprtpromoimgtbl'>");
					$(".displaypromoimg").remove();
					$(".displayimageblock").append("<img class='displaypromoimg img-responsive' src='"+datax.image_path+"'>");
					$("#add_new_block_img").text("Update Block");
					$(".cancle_new_block_imgprnt").remove();
					$("#add_new_block_img").after('<div class="cancle_new_block_imgprnt pull-right"><button id="cancle_new_block_img" class="btn btn-default">Cancle Update</button></div>');
			}
			function PromoRemove(arr, item)
			{
			   var newval = [];
		       for(var i=0;i<arr.length;i++){
		       		if(arr[i] != item){
		       			newval.push(arr[i]);
		       		}
		       }
		       return newval;
			}
			$('.delete_promotioal_image').on('click', function(e)
			{
				e.preventDefault();
				var promoimg = $("#promo_Images_id").val();
				var promoimgsp = promoimg.split(',');
				var deleteid = $(this).data("id");
				var index = promoimgsp.indexOf(deleteid);
				var currentval = PromoRemove(promoimgsp,deleteid);
				var currentvalstr = currentval.toString();
				$("#promo_Images_id").val(currentvalstr);
				$("#imageList #"+deleteid).remove();
			});
			
			$('#imageList').on('click',".edit_promotioal_image",function(e)
			{
				e.preventDefault();
				var editedid = $(this).data("id");
				doAdminAjax({
						"action":"EditPromotioal",
						"id_promotional_image":editedid,
						"token" : "{/literal}{$token|escape:'html':'UTF-8'}{literal}",
						"tab" : "Adminpromotional",
						"ajax" : 1 },afterpromotionimageedit);
			});
			$('.covered').die().live('click', function(e)
			{
				e.preventDefault();
				id = $(this).parent().parent().parent().attr('id');
				$("#imageList .cover i").each( function(i){
					$(this).removeClass('icon-check-sign').addClass('icon-check-empty');
				});
				$(this).removeClass('icon-check-empty').addClass('icon-check-sign');
				if (current_shop_id != 0)
					$('#' + current_shop_id + id).attr('check', true);
				else
					$(this).parent().parent().parent().children('td input').attr('check', true);
				doAdminAjax({
					"action":"UpdateCover",
					"id_image":id,
					"id_xprtpromotionaltbl" : {/literal}{$id_xprtpromotionaltbl}{literal},
					"token" : "{/literal}{$token|escape:'html':'UTF-8'}{literal}",
					"controller" : "Adminpromotional",
					"ajax" : 1 }
				);
			});
			$('.image_shop').die().live('click', function()
			{
				active = false;
				if ($(this).attr("checked"))
					active = true;
				id = $(this).parent().parent().attr('id');
				id_shop = $(this).attr("id").replace(id, "");
				doAdminAjax(
				{
					"action":"UpdatePromotioalImageShopAsso",
					"id_image":id,
					"id_xprtpromotionaltbl":id_xprtpromotionaltbl,
					"id_shop": id_shop,
					"active":active,
					"token" : "{/literal}{$token|escape:'html':'UTF-8'}{literal}",
					"tab" : "Adminpromotional",
					"ajax" : 1
				});
			});
			function updateImagePosition(json)
			{
				doAdminAjax(
				{
					"action":"updateImagePosition",
					"json":json,
					"token" : "{/literal}{$token|escape:'html':'UTF-8'}{literal}",
					"tab" : "Adminpromotional",
					"ajax" : 1
				});
			}
			function delQueue(id)
			{
				$("#img" + id).fadeOut("slow");
				$("#img" + id).remove();
			}
			$('.fancybox').fancybox();
		});
		hideOtherLanguage(default_language);
		{/literal}
	</script>
</div>
