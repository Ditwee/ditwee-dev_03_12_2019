{if isset($promotional.maincolumn)}
	<div class="{if isset($promotional.mainstyle)}promotional_{$promotional.mainstyle}{/if} col-xs-12 col-sm-{$promotional.maincolumn}">
		<div class="row">
			{if isset($promotional.promo_imgs)}
				{foreach from=$promotional.promo_imgs item=promo_Img}
					<div class="col-xs-12 col-sm-{if isset($promo_Img.column)}{$promo_Img.column}{/if} m_bottom_30 o_hidden">
						{if isset($promo_Img.image)}
						<div class="image_thumnail {if isset($promo_Img.style)}{$promo_Img.style}{/if}">
							<a class="promo_image" href="{if isset($promo_Img.link)}{$promo_Img.link}{/if}" style="background: url({$modules_dir}xprtpromotionalblock/images/{$promo_Img.image}) no-repeat scroll center center /cover; min-height: {$promo_Img.image_height}px">
							{if isset($promo_Img.content) && !empty($promo_Img.content)}
							<div class="thumnail_content {if isset($promo_Img.contentposition)}{$promo_Img.contentposition}{/if} {if isset($promo_Img.additionalclass)}{$promo_Img.additionalclass}{/if}">
								<div class="thumnail_content_inner">
									{$promo_Img.content}
								</div>
							</div>
							{/if}
							</a>
						</div>
						{/if}
					</div>
				{/foreach}
			{/if}
		</div>
	</div>
{/if}