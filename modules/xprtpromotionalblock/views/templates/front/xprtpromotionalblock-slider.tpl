{if isset($promotional.maincolumn)}
	<div class="{if isset($promotional.mainstyle)}promotional_{$promotional.mainstyle}{/if} col-xs-12 col-sm-{$promotional.maincolumn}">
			{if isset($promotional.promo_imgs)}
				<div class="image_thumnail_carousel m_bottom_30 o_hidden">
					{foreach from=$promotional.promo_imgs item=promo_Img}
							{if isset($promo_Img.image)}
								<div class="image_thumnail {if isset($promo_Img.style)}{$promo_Img.style}{/if}">
									<a href="{if isset($promo_Img.link)}{$promo_Img.link}{/if}">
										<img class="img-responsive" src="{$modules_dir}xprtpromotionalblock/images/{$promo_Img.image}" alt="{$promo_Img.content}">
									</a>
									{if isset($promo_Img.content) && !empty($promo_Img.content)}
									<div class="thumnail_content {if isset($promo_Img.contentposition)}{$promo_Img.contentposition}{/if} {if isset($promo_Img.additionalclass)}{$promo_Img.additionalclass}{/if}">
										{$promo_Img.content}
									</div>
									{/if}
								</div>
							{/if}
					{/foreach}
				</div>
			{/if}
		</div>
{/if}