{assign var=mainstyle value=[]}
{if isset($promotionalblock) && !empty($promotionalblock)}
	{foreach from=$promotionalblock item=promotional}
		{$mainstyle[]=$promotional.mainstyle}
		{$margin=$promotional.promo_margin}
	{/foreach}
	<div class="kr_promotional_block" style="{if !empty($margin)}margin:{$margin};{/if}">
		<div class="{if 'small_padding'|in_array:$mainstyle || 'no_padding'|in_array:$mainstyle}row-fluid{else}row{/if}">
		{foreach from=$promotionalblock item=promotional}
			{if $promotional.mainstyle == 'general'}
				{include file="./xprtpromotionalblock-column.tpl"}
			{/if}
			{if $promotional.mainstyle == 'slider'}
				{include file="./xprtpromotionalblock-slider.tpl"}
			{/if}
			{if $promotional.mainstyle == 'small_padding'}
				{include file="./xprtpromotionalblock-column-smpad.tpl"}
			{/if}
			{if $promotional.mainstyle == 'no_padding'}
				{include file="./xprtpromotionalblock-column-nopad.tpl"}
			{/if}
			{if $promotional.mainstyle == 'carousel'}
				{include file="./xprtpromotionalblock-carousel.tpl"}
			{/if}
		{/foreach}
		</div>
	</div>
{/if}