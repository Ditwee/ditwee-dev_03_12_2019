<?php

if (!defined('_CAN_LOAD_FILES_'))
	exit;
include_once _PS_MODULE_DIR_.'xprtpromotionalblock/classes/promotionalblockclass.php';
include_once _PS_MODULE_DIR_.'xprtpromotionalblock/classes/xprtpromoimgtbl.php';
class xprtpromotionalblock extends Module
{
	public $hooks_url = '/hooks/hooks.php';
	public $css_files_url = '/css/css.php';
	public $js_files_url = '/js/js.php';
	public $tabs_files_url = '/tabs/tabs.php';
	public $mysql_files_url = '/mysqlquery/mysqlquery.php';
	public $data_url = '/data/data.php';
	public $icon_url = '/icon/icon.php';
	public static $mod_name = 'xprtpromotionalblock';
	public function __construct()
	{
		$this->name = 'xprtpromotionalblock';
		$this->tab = 'front_office_features';
		$this->version = '2.0.3';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 1;
		$this->bootstrap = true;
		parent::__construct();	
		$this->displayName = $this->l('Great Store Theme Promotional Block');
		$this->description = $this->l('Adds an Promotional Block in any where.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
		 || !$this->Register_Hooks()
		 || !$this->Register_Tabs()
		 || !$this->Register_SQL()
		 || !$this->Register_Config()
		 || !$this->DummyData()
		 || !$this->xpertsampledata()
		)
			return false;
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
		 || !$this->UnRegister_Hooks()
		 || !$this->UnRegister_Tabs()
		 || !$this->UnRegister_SQL()
		 || !$this->UnRegister_Config()
		)
			return false;
		return true;
	}
	public function Register_Config($data = '')
	{
		if(empty($data))
		{
			$data = array();
	        require_once(dirname(__FILE__).$this->data_url);
		}
        if(isset($data) && !empty($data))
        	foreach($data as $data_key => $data_val):
        		Configuration::updateValue($data_key,$data_val);
        	endforeach;
        	return true;
        return false;
	}
	public function UnRegister_Config($data = '')
	{
		if(empty($data))
		{
			$data = array();
	        require_once(dirname(__FILE__).$this->data_url);
		}
        if(isset($data) && !empty($data))
        	foreach($data as $data_key => $data_val):
        		Configuration::deleteByName($data_key);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
        		$this->registerHook($hook);
        	endforeach;
        	return true;
        return false;
	}
	public function UnRegister_Hooks()
	{
		$hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
        	foreach($hooks as $hook):
	        		$hook_id = Module::getModuleIdByName($hook);
	        	if(isset($hook_id) && !empty($hook_id))
	        		$this->unregisterHook((int)$hook_id);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Css()
	{
		$css_files = array();
        require_once(dirname(__FILE__).$this->css_files_url);
        if(isset($css_files) && !empty($css_files))
        	foreach($css_files as $css_file):
        		$this->context->controller->addCSS($css_file);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_Js()
	{
		$js_files = array();
        require_once(dirname(__FILE__).$this->js_files_url);
        if(isset($js_files) && !empty($js_files))
        	foreach($js_files as $js_file):
        		$this->context->controller->addJS($js_file);
        	endforeach;
        	return true;
        return false;
	}
	public function Register_SQL()
	{
		$mysqlquery = array();
		if(file_exists(dirname(__FILE__).$this->mysql_files_url)){
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($mysqlquery) && !empty($mysqlquery))
				foreach($mysqlquery as $query){
					if(!Db::getInstance()->Execute($query,false))
					    return false;
				}
		}
        return true;
	}
	public function UnRegister_SQL()
	{
		$mysqlquery_u = array();
		if(file_exists(dirname(__FILE__).$this->mysql_files_url)){
			require_once(dirname(__FILE__).$this->mysql_files_url);
			if(isset($mysqlquery_u) && !empty($mysqlquery_u))
				foreach($mysqlquery_u as $query_u){
					if(!Db::getInstance()->Execute($query_u,false))
					    return false;
				}
		}
        return true;
	}
	public function UnRegister_Tabs()
    {
        $tabs_lists = array();
        require_once(dirname(__FILE__) .$this->tabs_files_url);
        if(isset($tabs_lists) && !empty($tabs_lists)){
        	foreach($tabs_lists as $tab_list){
        	    $tab_list_id = Tab::getIdFromClassName($tab_list['class_name']);
        	    if(isset($tab_list_id) && !empty($tab_list_id)){
        	        $tabobj = new Tab($tab_list_id);
        	        $tabobj->delete();
        	    }
        	}
        } 
        $save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
        if($save_tab_id != 0){
        	$count = Tab::getNbTabs($save_tab_id);
        	if($count == 0){
        		if(isset($save_tab_id) && !empty($save_tab_id)){
        		    $tabobjs = new Tab($save_tab_id);
        		    $tabobjs->delete();
        		}
        	}
        }
        return true;
    }
	public function RegisterParentTabs(){
	    	$langs = Language::getLanguages();
	    	$save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
	    	if($save_tab_id != 0){
	    		return $save_tab_id;
	    	}else{
	    		$tab_listobj = new Tab();
	    		$tab_listobj->class_name = 'Adminxprtdashboard';
	    		$tab_listobj->id_parent = 0;
	    		$tab_listobj->module = $this->name;
	    		foreach($langs as $l)
	    		{
	    		    $tab_listobj->name[$l['id_lang']] = $this->l("Theme Settings");
	    		}
	    		if($tab_listobj->save())
	    			return (int)$tab_listobj->id;
	    		else
	    			return (int)$save_tab_id;
	    	}
    }
    public function Register_Tabs()
    {
        $tabs_lists = array();
        $langs = Language::getLanguages();
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $save_tab_id = $this->RegisterParentTabs();
            require_once(dirname(__FILE__) .$this->tabs_files_url);
            if(isset($tabs_lists) && !empty($tabs_lists))
	            foreach ($tabs_lists as $tab_list)
	            {
	                $tab_listobj = new Tab();
	                $tab_listobj->class_name = $tab_list['class_name'];
	                if($tab_list['id_parent'] == 'parent'){
	                    $tab_listobj->id_parent = $save_tab_id;
	                }else{
	                    $tab_listobj->id_parent = $tab_list['id_parent'];
	                }
	                if(isset($tab_list['module']) && !empty($tab_list['module'])){
	                    $tab_listobj->module = $tab_list['module'];
	                }else{
	                    $tab_listobj->module = $this->name;
	                }
	                foreach($langs as $l)
	                {
	                    $tab_listobj->name[$l['id_lang']] = $this->l($tab_list['name']);
	                }
	                $tab_listobj->save();
	            }
        return true;
    }
    public function GetIcons()
	{
		$icon_list = array();
		if(file_exists(dirname(__FILE__).$this->icon_url))
			require_once(dirname(__FILE__).$this->icon_url);
        return $icon_list;
	}
	public function hookexecute($hook)
	{
		$promotionalblock = $this->getpromotion($hook);
		$this->context->smarty->assign(array('promotionalblock' => $promotionalblock));
		return $this->display(__FILE__, 'views/templates/front/xprtpromotionalblock.tpl');
	}
	public static function AllPageExceptions()
	{
	    $id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT p.`id_product`, pl.`name`
	            FROM `'._DB_PREFIX_.'product` p
	            '.Shop::addSqlAssociation('product', 'p').'
	            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
	            WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
	    $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	    $id_lang = Context::getContext()->language->id;
	    $categories =  Category::getCategories($id_lang,true,false);
	    $controllers = Dispatcher::getControllers(_PS_FRONT_CONTROLLER_DIR_);
	    if(isset($controllers))
	        ksort($controllers);
	    $Manufacturers =  Manufacturer::getManufacturers();
	    $Suppliers =  Supplier::getSuppliers();
	    $rslt = array();
	    $rslt[0]['id'] = 'all_page';
	    $rslt[0]['name'] = 'All Pages';
	    $i = 1;
	    if(isset($controllers))
	        foreach($controllers as $r => $v){
	            $rslt[$i]['id'] = $r;
	            $rslt[$i]['name'] = 'Page : '.ucwords($r);
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'man_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'sup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier : '.$r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'cat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category : '.$cats['name'];
	            $i++;
	        }
	    if(isset($products))
	        foreach($products as $r){
	            $rslt[$i]['id'] = 'prd_'.$r['id_product'];
	            $rslt[$i]['name'] = 'Product : '. $r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'prdcat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category Product: '.$cats['name'];
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'prdman_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer Product : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'prdsup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier Product : '.$r['name'];
	            $i++;
	        }
	    return $rslt;
	}
	public static function PageException($exceptions = NULL)
	{
		if($exceptions == NULL)
			return false;
		$exceptions = explode(",",$exceptions);
		$page_name = Context::getContext()->controller->php_self;
		$this_arr = array();
		$this_arr[] = 'all_page';
		$this_arr[] = $page_name;
		if($page_name == 'category'){
			$id_category = Tools::getvalue('id_category');
			$this_arr[] = 'cat_'.$id_category;
		}elseif($page_name == 'product'){
			$id_product = Tools::getvalue('id_product');
			$this_arr[] = 'prd_'.$id_product;
			// Start Get Product Category
			$prd_cat_sql = 'SELECT cp.`id_category` AS id
			    FROM `'._DB_PREFIX_.'category_product` cp
			    LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = cp.id_category)
			    '.Shop::addSqlAssociation('category', 'c').'
			    WHERE cp.`id_product` = '.(int)$id_product;
			$prd_catresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_cat_sql);
			if(isset($prd_catresults) && !empty($prd_catresults))
			{
			    foreach($prd_catresults as $prd_catresult)
			    {
			        $this_arr[] = 'prdcat_'.$prd_catresult['id'];
			    }
			}
			// END Get Product Category
			// Start Get Product Manufacturer
			$prd_man_sql = 'SELECT `id_manufacturer` AS id FROM `'._DB_PREFIX_.'product` WHERE `id_product` = '.(int)$id_product;
			$prd_manresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_man_sql);
			if(isset($prd_manresults) && !empty($prd_manresults))
			{
			    foreach($prd_manresults as $prd_manresult)
			    {
			        $this_arr[] = 'prdman_'.$prd_manresult['id'];
			    }
			}
			// END Get Product Manufacturer
			// Start Get Product SupplierS
			$prd_sup_sql = "SELECT `id_supplier` AS id FROM `"._DB_PREFIX_."product_supplier` WHERE `id_product` = ".(int)$id_product." GROUP BY `id_supplier`";
			$prd_supresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_sup_sql);
			if(isset($prd_supresults) && !empty($prd_supresults))
			{
			    foreach($prd_supresults as $prd_supresult)
			    {
			        $this_arr[] = 'prdsup_'.$prd_supresult['id'];
			    }
			}
			// END Get Product SupplierS
		}elseif($page_name == 'cms'){
			$id_cms = Tools::getvalue('id_cms');
			$this_arr[] = 'cms_'.$id_cms;
		}elseif($page_name == 'manufacturer'){
			$id_manufacturer = Tools::getvalue('id_manufacturer');
			$this_arr[] = 'man_'.$id_manufacturer;
		}elseif($page_name == 'supplier'){
			$id_supplier = Tools::getvalue('id_supplier');
			$this_arr[] = 'sup_'.$id_supplier;
		}
		if(isset($this_arr)){
			foreach ($this_arr as $this_arr_val) {
				if(in_array($this_arr_val,$exceptions))
					return true;
			}
		}
		return false;
	}
	public function getpromotion($hook)
	{
		$data = array();
	    $sql = 'SELECT * FROM '._DB_PREFIX_.'xprtpromotionaltbl p INNER JOIN 
	        '._DB_PREFIX_.'xprtpromotionaltbl_lang pl ON p.id_xprtpromotionaltbl=pl.id_xprtpromotionaltbl INNER JOIN 
	        '._DB_PREFIX_.'xprtpromotionaltbl_shop ps ON pl.id_xprtpromotionaltbl = ps.id_xprtpromotionaltbl AND ps.id_shop = '.(int) Context::getContext()->shop->id.'
	        WHERE pl.id_lang='.(int) Context::getContext()->language->id.'  AND p.active = 1 AND p.hook = "'.$hook.'" ORDER BY p.position DESC ';
		if(!$results = Db::getInstance()->executeS($sql))
			return false;
		if(isset($results) && !empty($results)){
			$i = 0;
			foreach ($results as $result) {
				if(empty($result['pages'])){
					$result['pages'] = 'all_page';
				}
				if(self::PageException($result['pages'])){
					$result['promo_imgs'] = xprtpromoimgtbl::getImageslists($result['promo_Images']);
					$data[$i] = $result;
					$i++;
				}
			}
		}
      return $data;
	}
	public function hookdisplayBanner($params)
	{
		return $this->hookexecute('displayBanner');
	}
	public function hookdisplayFooter($params)
	{
		return $this->hookexecute('displayFooter');
	}
	public function hookdisplayFooterProduct($params)
	{
		return $this->hookexecute('displayFooterProduct');
	}
	public function hookdisplayHome($params)
	{
		return $this->hookexecute('displayHome');
	}
	public function hookdisplayLeftColumn($params)
	{
		return $this->hookexecute('displayLeftColumn');
	}
	public function hookdisplayLeftColumnProduct($params)
	{
		return $this->hookexecute('displayLeftColumnProduct');
	}
	public function hookdisplayMaintenance($params)
	{
		return $this->hookexecute('displayMaintenance');
	}
	public function hookdisplayMyAccountBlock($params)
	{
		return $this->hookexecute('displayMyAccountBlock');
	}
	public function hookdisplayMyAccountBlockfooter($params)
	{
		return $this->hookexecute('displayMyAccountBlockfooter');
	}
	public function hookdisplayRightColumn($params)
	{
		return $this->hookexecute('displayRightColumn');
	}
	public function hookdisplayRightColumnProduct($params)
	{
		return $this->hookexecute('displayRightColumnProduct');
	}
	public function hookdisplayTop($params)
	{
		return $this->hookexecute('displayTop');
	}
	public function hookdisplayTopColumn($params)
	{
		return $this->hookexecute('displayTopColumn');
	}
	public function hookdisplayHomeTop($params)
	{
		return $this->hookexecute('displayHomeTop');
	}
	public function hookdisplayHomeTopLeft($params)
	{
		return $this->hookexecute('displayHomeTopLeft');
	}
	public function hookdisplayHomeTopRight($params)
	{
		return $this->hookexecute('displayHomeTopRight');
	}
	public function hookdisplayHomeTopLeftOne($params)
	{
		return $this->hookexecute('displayHomeTopLeftOne');
	}
	public function hookdisplayHomeTopLeftTwo($params)
	{
		return $this->hookexecute('displayHomeTopLeftTwo');
	}
	public function hookdisplayHomeTopRightOne($params)
	{
		return $this->hookexecute('displayHomeTopRightOne');
	}
	public function hookdisplayHomeTopRightTwo($params)
	{
		return $this->hookexecute('displayHomeTopRightTwo');
	}

	public function hookdisplayHomeBottomLeft($params)
	{
		return $this->hookexecute('displayHomeBottomLeft');
	}
	public function hookdisplayHomeBottomRight($params)
	{
		return $this->hookexecute('displayHomeBottomRight');
	}
	public function hookdisplayHomeBottomLeftOne($params)
	{
		return $this->hookexecute('displayHomeBottomLeftOne');
	}
	public function hookdisplayHomeBottomLeftTwo($params)
	{
		return $this->hookexecute('displayHomeBottomLeftTwo');
	}
	public function hookdisplayHomeBottomRightOne($params)
	{
		return $this->hookexecute('displayHomeBottomRightOne');
	}
	public function hookdisplayHomeBottomRightTwo($params)
	{
		return $this->hookexecute('displayHomeBottomRightTwo');
	}
	public function hookdisplayHomeFullWidthMiddle($params)
	{
		return $this->hookexecute('displayHomeFullWidthMiddle');
	}
	public function hookdisplayHomeFullWidthBottom($params)
	{
		return $this->hookexecute('displayHomeFullWidthBottom');
	}
	public function hookdisplayProductRightSidebar($params)
	{
		return $this->hookexecute('displayProductRightSidebar');
	}
	public function hookdisplayCatTop($params)
	{
		return $this->hookexecute('displayCatTop');
	}
	public function hookdisplayCatBottom($params)
	{
		return $this->hookexecute('displayCatBottom');
	}
	public function InsertDummyData($categories=NULL,$class=NULL)
	{
		if($categories == NULL || $class == NULL)
			return false;
		$languages = Language::getLanguages(false);
	    if(isset($categories) && !empty($categories)){
	        $classobj = new $class();
	        foreach($categories as $valu){
	        	if(isset($valu['lang']) && !empty($valu['lang'])){
	        		foreach ($valu['lang'] as $valukey => $value){
	        			foreach ($languages as $language){
	        				if(isset($valukey)){
	        					$classobj->{$valukey}[$language['id_lang']] = isset($value) ? $value : '';
	        				}
	        			}
	        		}
	        	}
        		if(isset($valu['notlang']) && !empty($valu['notlang'])){
        			foreach ($valu['notlang'] as $valukey => $value){
        				if(isset($valukey)){
        					$classobj->{$valukey} = $value;
        				}
        			}
        		}
	        	$classobj->add();
	        }
	    }
	    return true;
	}
	public function alldisabled(){
		$data = array();
		$data['active'] = 0;
		Db::getInstance()->update("xprtpromotionaltbl",$data);
		return true;
	}
	public function DummyData()
	{
	    include_once(dirname(__FILE__).'/data/dummy_data.php');
	    $this->InsertDummyData($xippromo_images,'xprtpromoimgtbl');
	    $this->InsertDummyData($xippromo_tbl,'promotionalblockclass');
	    return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
			$this->alldisabled();
        	$this->{$func}();
        }else{
        	$this->alldisabled();
        }
        return true;
	}
	public function xpertsample_demo_2()
	{
		$results = array(1,2,3,4);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new promotionalblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_1()
	{
		$results = array(5,6,18);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new promotionalblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_3()
	{
		$results = array(7,8);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new promotionalblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_4()
	{
		$results = array(9,10,19);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new promotionalblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_5()
	{
		$results = array(11);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new promotionalblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_6()
	{
		$results = array(12,13,14,20);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new promotionalblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_7()
	{
		$results = array(21);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new promotionalblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_8()
	{
		$results = array(22);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new promotionalblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_12()
	{
		$results = array(23,24);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new promotionalblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
}