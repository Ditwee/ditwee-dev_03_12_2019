{if isset($xprtrelatedproductblock) && !empty($xprtrelatedproductblock) && !empty($xprtrelatedproductblock.products)}
	{if isset($xprtrelatedproductblock.device)}
		{assign var=device_data value=$xprtrelatedproductblock.device|json_decode:true}
	{/if}
	<div id="xprtrelatedproductblock_{$xprtrelatedproductblock.id_xprtrelatedproductblock}" class="xprtrelatedproductblock xprt_default_products_block" style="margin:{$xprtrelatedproductblock.section_margin};">
		<div class="page_title_area {*$xprt.home_title_style*}">
			{if isset($xprtrelatedproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtrelatedproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtrelatedproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtrelatedproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		{if isset($xprtrelatedproductblock) && $xprtrelatedproductblock}
			<div id="xprt_relatedproductsblock_{$xprtrelatedproductblock.id_xprtrelatedproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtrelatedproductblock.products id='' class="xprt_relatedproductsblock {if $xprtrelatedproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtrelatedproductblock'}</p>
			</div>
		{/if}
	</div>
<script type="text/javascript">
	var xprtrelatedproductblock = $("#xprtrelatedproductblock_{$xprtrelatedproductblock.id_xprtrelatedproductblock}");
	var sliderSelect = xprtrelatedproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtrelatedproductblock.find('.heading_carousel_arrow'); 
	
	{if isset($xprtrelatedproductblock.nav_arrow_style) && ($xprtrelatedproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}
	
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtrelatedproductblock.play_again)}{$xprtrelatedproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtrelatedproductblock.autoplay)}{$xprtrelatedproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtrelatedproductblock.pause_on_hover)}{$xprtrelatedproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtrelatedproductblock.navigation_dots)}{$xprtrelatedproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtrelatedproductblock.navigation_arrow)}{$xprtrelatedproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtrelatedproductblock.product_rows)}{$xprtrelatedproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtrelatedproductblock.product_scroll) && ($xprtrelatedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtrelatedproductblock.product_scroll) && ($xprtrelatedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtrelatedproductblock.product_scroll) && ($xprtrelatedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtrelatedproductblock.product_scroll) && ($xprtrelatedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtrelatedproductblock.product_scroll) && ($xprtrelatedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtrelatedproductblock.product_scroll) && ($xprtrelatedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>

{/if}