{if isset($xprtrelatedproductblock) && !empty($xprtrelatedproductblock)}
	{if isset($xprtrelatedproductblock.device)}
		{assign var=device_data value=$xprtrelatedproductblock.device|json_decode:true}
	{/if}
	<div class="xprt_home_simple_medium col-sm-4">
		<div class="xprtrelatedproductblock block carousel">
			<h4 class="title_block">
		    	{$xprtrelatedproductblock.title}
		    </h4>
		    <div class="block_content products-block">
		        {if isset($xprtrelatedproductblock) && $xprtrelatedproductblock}
		        	{include file="$tpl_dir./product-list/product-list-simple-medium.tpl" xprtprdcolumnclass=$device_data products=$xprtrelatedproductblock.products}
		        {else}
	        		<p class="alert alert-info">{l s='No products at this time.' mod='xprtrelatedproductblock'}</p>
		        {/if}
		    </div>
		</div>
	</div>
{/if}