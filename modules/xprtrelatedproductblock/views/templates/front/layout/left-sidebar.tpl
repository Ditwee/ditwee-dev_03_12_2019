
{if isset($xprtrelatedproductblock) && !empty($xprtrelatedproductblock) && !empty($xprtrelatedproductblock.products)}
	{if isset($xprtrelatedproductblock.device)}
		{assign var=device_data value=$xprtrelatedproductblock.device|json_decode:true}
	{/if}
	<div class="xprtrelatedproductblock block carousel">
		<h4 class="title_block">
	    	<em>{$xprtrelatedproductblock.title}</em>
	    </h4>
	    <div class="block_content">
	        {if isset($xprtrelatedproductblock) && $xprtrelatedproductblock}
	        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtrelatedproductblock.products }
	        {else}
	        	<p class="alert alert-info">{l s='No products at this time.' mod='xprtrelatedproductblock'}</p>
	        {/if}
	    </div>
	</div>
{/if}