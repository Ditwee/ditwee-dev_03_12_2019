{if isset($xprtrelatedproductblock) && !empty($xprtrelatedproductblock)}
	{if isset($xprtrelatedproductblock.device)}
		{assign var=device_data value=$xprtrelatedproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtrelatedproductblock) && $xprtrelatedproductblock}
		<div id="xprt_relatedproductsblock_tab_{if isset($xprtrelatedproductblock.id_xprtrelatedproductblock)}{$xprtrelatedproductblock.id_xprtrelatedproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtrelatedproductblock.products class='xprt_relatedproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_relatedproductsblock_tab_{if isset($xprtrelatedproductblock.id_xprtrelatedproductblock)}{$xprtrelatedproductblock.id_xprtrelatedproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No products at this time.' mod='xprtrelatedproductblock'}</p>
		</div>
	{/if}
{/if}