{if isset($xprtselectedproductblock) && !empty($xprtselectedproductblock)}
	{if isset($xprtselectedproductblock.device)}
		{assign var=device_data value=$xprtselectedproductblock.device|json_decode:true}
	{/if}
	<div id="xprtselectedproductblock_{$xprtselectedproductblock.id_xprtselectedproductblock}" class="xprtselectedproductblock xprt_default_products_block" style="margin:{$xprtselectedproductblock.section_margin};">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtselectedproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtselectedproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtselectedproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtselectedproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		{if isset($xprtselectedproductblock) && $xprtselectedproductblock}
			<div id="xprt_selectedproductsblock_{$xprtselectedproductblock.id_xprtselectedproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtselectedproductblock.products id='' class="xprt_selectedproductsblock {if $xprtselectedproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtselectedproductblock'}</p>
			</div>
		{/if}
	</div>
<script type="text/javascript">
	var xprtselectedproductblock = $("#xprtselectedproductblock_{$xprtselectedproductblock.id_xprtselectedproductblock}");
	var sliderSelect = xprtselectedproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtselectedproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtselectedproductblock.nav_arrow_style) && ($xprtselectedproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}

	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtselectedproductblock.play_again)}{$xprtselectedproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtselectedproductblock.autoplay)}{$xprtselectedproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtselectedproductblock.pause_on_hover)}{$xprtselectedproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtselectedproductblock.navigation_dots)}{$xprtselectedproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtselectedproductblock.navigation_arrow)}{$xprtselectedproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: appendArrows,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtselectedproductblock.product_rows)}{$xprtselectedproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtselectedproductblock.product_scroll) && ($xprtselectedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtselectedproductblock.product_scroll) && ($xprtselectedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtselectedproductblock.product_scroll) && ($xprtselectedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtselectedproductblock.product_scroll) && ($xprtselectedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtselectedproductblock.product_scroll) && ($xprtselectedproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}