{if isset($xprtselectedproductblock) && !empty($xprtselectedproductblock)}
{if isset($xprtselectedproductblock.device)}
	{assign var=device_data value=$xprtselectedproductblock.device|json_decode:true}
{/if}
    {if $xprtselectedproductblock.products && $xprtselectedproductblock.products|@count > 0}
		{include file="$tpl_dir./product-list/product-list-simple-paralax.tpl" xprtprdcolumnclass=$device_data products=$xprtselectedproductblock.products parallaxbg=$xprtselectedproductblock.image }
	{else}
		<p>{l s='No products at this time' mod='xprtselectedproductblock'}</p>
	{/if}
{/if}