{if isset($xprtselectedproductblock) && !empty($xprtselectedproductblock)}
	{if isset($xprtselectedproductblock.device)}
		{assign var=device_data value=$xprtselectedproductblock.device|json_decode:true}
	{/if}
	<div class="xprtselectedproductblock block carousel">
		<h4 class="title_block">
	    	<em>{$xprtselectedproductblock.title}</em>
	    </h4>
	    <div class="block_content">
	        {if isset($xprtselectedproductblock) && $xprtselectedproductblock}
	        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtselectedproductblock.products }
	        {else}
	        	<p class="alert alert-info">{l s='No products at this time.' mod='xprtselectedproductblock'}</p>
	        {/if}
	    </div>
	</div>
{/if}