{if isset($xprtselectedproductblock) && !empty($xprtselectedproductblock)}
{if isset($xprtselectedproductblock.device)}
	{assign var=device_data value=$xprtselectedproductblock.device|json_decode:true}
{/if}
		{if isset($xprtselectedproductblock) && $xprtselectedproductblock}
			<div id="xprt_specialproductsblock_tab_{if isset($xprtselectedproductblock.id_xprtselectedproductblock)}{$xprtselectedproductblock.id_xprtselectedproductblock}{/if}" class="tab-pane fade">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtselectedproductblock.products class='xprt_specialproductsblock ' id=''}
			</div>
		{else}
			<div id="xprt_specialproductsblock_tab_{if isset($xprtselectedproductblock.id_xprtselectedproductblock)}{$xprtselectedproductblock.id_xprtselectedproductblock}{/if}" class="tab-pane fade">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtselectedproductblock'}</p>
			</div>
		{/if}
{/if}