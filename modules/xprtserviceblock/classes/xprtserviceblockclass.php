<?php

class xprtserviceblockclass extends ObjectModel
{
	public $id;
	public $id_xprtserviceblock;
	public $height;
	public $margin;
	public $is_parallax;
	public $design_style;
	public $bgimage;
	public $bgimage_enable;
	public $hook;
	public $pages;
	public $content;
	public $active;
	public $position;
	public $title;
	public $subtitle;
	public $truck_identify;
	private static $module_name = 'xprtserviceblock';
	private static $tablename = 'xprtserviceblock';
	private static $classname = 'xprtserviceblockclass';
	public static $definition = array(
		'table' => 'xprtserviceblock',
		'primary' => 'id_xprtserviceblock',
		'multilang' => true,
		'fields' => array(
			'height' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'margin' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'is_parallax' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'bgimage' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'bgimage_enable' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'design_style' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'truck_identify' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			
			'hook' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'content' =>		array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'pages' =>			array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'position' =>		array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'active' =>			array('type' => self::TYPE_BOOL,'validate' => 'isBool'),

			'title' =>			array('type' => self::TYPE_STRING,'validate' => 'isString','lang' => true),
			'subtitle' =>		array('type' => self::TYPE_STRING,'validate' => 'isString','lang' => true),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation(self::$tablename, array('type' => 'shop'));
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = self::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.self::$tablename.'`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetServiceBlock($hook = NULL)
    {
        if($hook == NULL)
            return false;
        $results = array();
      	$all_sliders = self::GetServiceBlockS($hook);
      	if(isset($all_sliders) && !empty($all_sliders)){
      		foreach ($all_sliders as $all_sliderk => $all_slider){
      			if(xprtserviceblock::PageException($all_slider['pages'])){
      			 	foreach ($all_slider as $sldr_key => $sldr_val){
      			 		$results[$all_sliderk][$sldr_key] = $sldr_val;
      			 		if($sldr_key == 'id_'.self::$tablename){
      			 			$results[$all_sliderk]["all_service"] = self::GetServiceByBlockId($sldr_val);
      			 		}
      			 	}
      			}
      		}
      	}
      	return $results;
    }
    public static function GetServiceByBlockId($id_xprtserviceblock = NULL)
    {
		$results = array();
	    $id_lang = (int)Context::getContext()->language->id;
	    $id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtservice` pb 
	            INNER JOIN `'._DB_PREFIX_.'xprtservice_lang` pbl ON (pb.`id_xprtservice` = pbl.`id_xprtservice` AND pbl.`id_lang` = '.$id_lang.') ';
	    $sql .= ' WHERE pb.`active` = 1 ';
	    if($id_xprtserviceblock != NULL)
	        $sql .= ' AND pb.`id_xprtserviceblock` = "'.(int)$id_xprtserviceblock.'" ';
	    $sql .= ' ORDER BY pb.`position` DESC';
	    $results = Db::getInstance()->executeS($sql);
	    if(isset($results) && !empty($results) && is_array($results)){
	    	foreach ($results as &$result) {
	    		if(isset($result['content']) && !empty($result['content'])){
	    			$content = Tools::jsonDecode($result['content']);
	    			if(isset($content) && !empty($content)){
	    				foreach ($content as $content_key => $content_value) {
	    					if(isset($content_value) && !is_string($content_value) && is_object($content_value)){
	    						foreach ($content_value as $content_value_key => $content_value_value) {
	    							$result[$content_key][$content_value_key] = $content_value_value;
	    						}
	    					}else{
	    						$result[$content_key] = $content_value;
	    					}
	    				}
	    			}
	    		}
	    	}
	    }
	    return $results;
    }
    public static function GetServiceBlockS($hook = NULL)
    {
    	$results = array();
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.self::$tablename.'` pb 
                INNER JOIN `'._DB_PREFIX_.self::$tablename.'_lang` pbl ON (pb.`id_'.self::$tablename.'` = pbl.`id_'.self::$tablename.'` AND pbl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.self::$tablename.'_shop` pbs ON (pb.`id_'.self::$tablename.'` = pbs.`id_'.self::$tablename.'` AND pbs.`id_shop` = '.$id_shop.')
                ';
        $sql .= ' WHERE pb.`active` = 1 ';
        if($hook != NULL)
            $sql .= ' AND pb.`hook` = "'.$hook.'" ';
        $sql .= ' ORDER BY pb.`position` ASC';
        $results = Db::getInstance()->executeS($sql);
        if(isset($results) && !empty($results) && is_array($results)){
        	foreach ($results as &$result) {
        		if(isset($result['content']) && !empty($result['content'])){
        			$content = Tools::jsonDecode($result['content']);
        			if(isset($content) && !empty($content)){
        				foreach ($content as $content_key => $content_value) {
        					if(isset($content_value) && !is_string($content_value) && is_object($content_value)){
        						foreach ($content_value as $content_value_key => $content_value_value) {
        							$result[$content_key][$content_value_key] = $content_value_value;
        						}
        					}else{
        						$result[$content_key] = $content_value;
        					}
        				}
        			}
        		}
        	}
        }
        return $results;
    }
}