<?php
class xprtserviceclass extends ObjectModel
{
	public $id;
	public $id_xprtservice;
	public $id_xprtserviceblock;
	public $title;
	public $subtitle;
	public $description;
	public $icon;
	public $serviceimage;
	// public $button_label;
	// public $link_type;
	public $serviceimage_enable;
	public $content;
	public $position;
	public $active;
	// public $link;
	public static $definition = array(
		'table' => 'xprtservice',
		'primary' => 'id_xprtservice',
		'multilang' => true,
		'fields' => array(
			'id_xprtserviceblock' =>	array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'icon' =>					array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'serviceimage' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			// 'link_type' =>				array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'serviceimage_enable' =>				array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'content' =>				array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'position' =>				array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'active' =>					array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
			'title' =>					array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			'subtitle' =>				array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			// 'link' =>					array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			// 'button_label' =>			array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			'description' =>			array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml','lang'=>true),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = self::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtservice`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
	public function reOrderPositions()
	{
		$id_slide = $this->id;
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$max = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT MAX(`position`) as position FROM `'._DB_PREFIX_.'xprtservice` ');
		if ((int)$max == (int)$id_slide)
			return true;
		$rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT xs.`position` as position, xs.`id_xprtservice` as id_xprtservice FROM `'._DB_PREFIX_.'xprtservice` xs WHERE xs.`position` > '.(int)$this->position
		);
		foreach ($rows as $row)
		{
			$current_slide = new xprtserviceclass($row['id_xprtservice']);
			--$current_slide->position;
			$current_slide->update();
			unset($current_slide);
		}
		return true;
	}
}