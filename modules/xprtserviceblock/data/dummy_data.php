<?php
$serviceblock_dummy_data = array(
    array( // 1
    	'lang' => array(
		    'title' => 'Theme Featured',
		    'subtitle' => 'Latest theme featured for you',
		),
	    'notlang' => array(
			'height'	=>	'350px',
			'margin'	=>	'0px 0px 60px 0px',
			'is_parallax'	=>	'1',
			'design_style'	=>	'general',
			'truck_identify'	=>	'demo_6',
			'bgimage'	=>	'',
			'bgimage_enable'	=>	'1',
			'hook'	=>	'displayHome',
			'pages'	=>	'all_page',
			'content'	=>	'',
			'active'	=>	0,
			'position'	=>	0,
		),
    ),
    array( //2
    	'lang' => array(
		    'title' => 'Theme Featured',
		    'subtitle' => 'Latest theme featured for you',
		),
	    'notlang' => array(
			'height'	=>	'',
			'margin'	=>	'0px 0px 60px 0px',
			'is_parallax'	=>	'0',
			'design_style'	=>	'classic',
			'truck_identify'	=>	'demo_9',
			'bgimage'	=>	'',
			'bgimage_enable'	=>	'0',
			'hook'	=>	'displayHomeTop',
			'pages'	=>	'index',
			'content'	=>	'',
			'active'	=>	0,
			'position'	=>	0,
		),
    ),
    array( //3
    	'lang' => array(
		    'title' => 'displayProductRightSidebar',
		    'subtitle' => '',
		),
	    'notlang' => array(
			'height'	=>	'',
			'margin'	=>	'',
			'is_parallax'	=>	'0',
			'design_style'	=>	'sidebar',
			'truck_identify'	=>	'demo_2',
			'bgimage'	=>	'',
			'bgimage_enable'	=>	'0',
			'hook'	=>	'displayProductRightSidebar',
			'pages'	=>	'all_page',
			'content'	=>	'',
			'active'	=>	0,
			'position'	=>	0,
		),
    ),
    array( //4
    	'lang' => array(
		    'title' => 'Product sidebar',
		    'subtitle' => '',
		),
	    'notlang' => array(
			'height'	=>	'',
			'margin'	=>	'',
			'is_parallax'	=>	'0',
			'design_style'	=>	'sidebar',
			'truck_identify'	=>	'demo_5',
			'bgimage'	=>	'',
			'bgimage_enable'	=>	'0',
			'hook'	=>	'displayProductRightSidebar',
			'pages'	=>	'product',
			'content'	=>	'',
			'active'	=>	0,
			'position'	=>	0,
		),
    ),
    array( //5
    	'lang' => array(
		    'title' => 'products center column',
		    'subtitle' => '',
		),
	    'notlang' => array(
			'height'	=>	'',
			'margin'	=>	'0px 0px 30px 0px',
			'is_parallax'	=>	'0',
			'design_style'	=>	'sidebar',
			'truck_identify'	=>	'demo_6',
			'bgimage'	=>	'',
			'bgimage_enable'	=>	'0',
			'hook'	=>	'displayCenterColumnProduct',
			'pages'	=>	'all_page',
			'content'	=>	'',
			'active'	=>	0,
			'position'	=>	0,
		),
    ),
    array( //6
    	'lang' => array(
		    'title' => 'Top service block',
		    'subtitle' => '',
		),
	    'notlang' => array(
			'height'	=>	'',
			'margin'	=>	'20px 0px 30px 0px',
			'is_parallax'	=>	'0',
			'design_style'	=>	'general',
			'truck_identify'	=>	'demo_8',
			'bgimage'	=>	'',
			'bgimage_enable'	=>	'0',
			'hook'	=>	'displayHomeTop',
			'pages'	=>	'all_page',
			'content'	=>	'',
			'active'	=>	0,
			'position'	=>	0,
		),
    ),
    array( //7
    	'lang' => array(
		    'title' => '',
		    'subtitle' => '',
		),
	    'notlang' => array(
			'height'	=>	'',
			'margin'	=>	'',
			'is_parallax'	=>	'0',
			'design_style'	=>	'sidebar',
			'truck_identify'	=>	'demo_12',
			'bgimage'	=>	'',
			'bgimage_enable'	=>	'0',
			'hook'	=>	'displayHomeTopLeft',
			'pages'	=>	'all_page',
			'content'	=>	'',
			'active'	=>	0,
			'position'	=>	0,
		),
    ),

);

$service_dummy_data = array(
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 1,
	            'id_xprtserviceblock' => 1,
	            'icon' => 'icon-truck',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 2,
	            'active' => 1,
	            // 'id_lang' => 1,
	        ),
	    'lang' => array
	        (
	            'title' => 'Free Shipping',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum 
	            dolor sit amet, consectetur adipiscing elit. ',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 2
	            'id_xprtserviceblock' => 1,
	            'icon' => 'icon-mail-forward',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 1,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => '100% Money Back',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum 
	            dolor sit amet, consectetur adipiscing elit. ',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 3
	            'id_xprtserviceblock' => 1,
	            'icon' => 'icon-comments-alt',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 0,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Online Support 24/7',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum 
	            dolor sit amet, consectetur adipiscing elit.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 4
	            'id_xprtserviceblock' => 2,
	            'icon' => 'icon-film',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 3,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Sliders',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 5
	            'id_xprtserviceblock' => 2,
	            'icon' => 'icon-desktop',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 4,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Lightbox',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 6
	            'id_xprtserviceblock' => 2,
	            'icon' => 'icon-comments',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 5,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Customer Support',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 7
	            'id_xprtserviceblock' => 2,
	            'icon' => 'icon-sun',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 6,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Clean Design',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 8
	            'id_xprtserviceblock' => 2,
	            'icon' => 'icon-magic',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 7,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Style for every budget',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 9
	            'id_xprtserviceblock' => 2,
	            'icon' => 'icon-upload',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 8,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Loaded with Power',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 10
	            'id_xprtserviceblock' => 3,
	            'icon' => 'icon-credit-card',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 2,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Payment',
	            'subtitle' => '',
	            'description' => 'We accept Visa, MasterCard and American Express.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 11
	            'id_xprtserviceblock' => 3,
	            'icon' => 'icon-truck',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 1,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Free shipping',
	            'subtitle' => '',
	            'description' => 'All orders over $100 free super fast delivery.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 12
	            'id_xprtserviceblock' => 3,
	            'icon' => 'icon-gift',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 0,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Best priec guarantee',
	            'subtitle' => '',
	            'description' => 'The best choice for high quality at good prices.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 13
	            'id_xprtserviceblock' => 4,
	            'icon' => 'icon-credit-card',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 2,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Payment',
	            'subtitle' => '',
	            'description' => 'We accept Visa, MasterCard and American Express.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 14
	            'id_xprtserviceblock' => 4,
	            'icon' => 'icon-truck',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 1,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Free shipping',
	            'subtitle' => '',
	            'description' => 'All orders over $100 free super fast delivery.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 15
	            'id_xprtserviceblock' => 4,
	            'icon' => 'icon-gift',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 0,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Best priec guarantee',
	            'subtitle' => '',
	            'description' => 'The best choice for high quality at good prices.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 16
	            'id_xprtserviceblock' => 5,
	            'icon' => 'icon-credit-card',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 2,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Payment',
	            'subtitle' => '',
	            'description' => 'We accept Visa, MasterCard and American Express.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtservice' => 17
	            'id_xprtserviceblock' => 5,
	            'icon' => 'icon-truck',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 1,
	            'active' => 1,
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Free shipping',
	            'subtitle' => '',
	            'description' => 'All orders over $100 free super fast delivery.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            'id_xprtserviceblock' => 5,
	            'icon' => 'icon-gift',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 0,
	            'active' => 1,
	        ),
	    'lang' => array
	        (
	            'title' => 'Best priec guarantee',
	            'subtitle' => '',
	            'description' => 'The best choice for high quality at good prices.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            'id_xprtserviceblock' => 6,
	            'icon' => 'icon-truck',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 0,
	            'active' => 1,
	        ),
	    'lang' => array
	        (
	            'title' => 'Free Shipping',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum 
dolor sit amet, consectetur adipiscing elit.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            'id_xprtserviceblock' => 6,
	            'icon' => 'icon-share',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 0,
	            'active' => 1,
	        ),
	    'lang' => array
	        (
	            'title' => '100% Money Back',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum 
dolor sit amet, consectetur adipiscing elit.',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            'id_xprtserviceblock' => 6,
	            'icon' => 'icon-comments',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 0,
	            'active' => 1,
	        ),
	    'lang' => array
	        (
	            'title' => 'Online Support 24/7',
	            'subtitle' => '',
	            'description' => 'Donec at mattis purus, ut accumsan nisl. Lorem ipsum 
dolor sit amet, consectetur adipiscing elit.',
	        ),
	),// new item
	array 
	(
	    'notlang' => array
	        (
	            'id_xprtserviceblock' => 7,
	            'icon' => 'icon-credit-card',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 2,
	            'active' => 1,
	        ),
	    'lang' => array
	        (
	            'title' => 'Payment',
	            'subtitle' => '',
	            'description' => 'We accept Visa, MasterCard and American Express..',
	        ),
	),
	array 
	(
	    'notlang' => array
	        (
	            'id_xprtserviceblock' => 7,
	            'icon' => 'icon-truck',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 1,
	            'active' => 1,
	        ),
	    'lang' => array
	        (
	            'title' => 'Free shipping',
	            'subtitle' => '',
	            'description' => 'All orders over $100 free super fast delivery..',
	        ),
	),
	array 
	(
	    'notlang' => array
	        (
	            'id_xprtserviceblock' => 7,
	            'icon' => 'icon-gift',
	            'serviceimage' => '',
	            'content' => '',
	            'serviceimage_enable' => '0',
	            'position' => 0,
	            'active' => 1,
	        ),
	    'lang' => array
	        (
	            'title' => 'Best priec guarantee',
	            'subtitle' => '',
	            'description' => 'The best choice for high quality at good prices..',
	        ),
	),
);

