{extends file="helpers/form/form.tpl"}
{block name="description"}
		<div class="help-block-container">
			{if isset($input.type) && $input.type == "file"}
				{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
					<img src="{$image_baseurl}{$fields_value[$input.name]}" height="200px" width="auto"/>
				{/if}
			{/if}
			{$smarty.block.parent}
		</div>
{/block}
{block name="label"}
    {if $input.type == 'xprticon_type'}
<div class="form-group">
<label class="control-label col-lg-3">
{$input.label}
</label>
<div class="col-lg-9 ">
<input name="{$input.name}" type="text" value="{if isset($fields_value[$input.name])}{$fields_value[$input.name]|escape:'html':'UTF-8'}{/if}" id="icon_type_{$input.name}" class="xprticon_type_cls fixed-width-lg">
<a href="#" class="toolbar_btn" data-toggle="modal" data-target="#icon_{$input.name}_id" title="Addons">Select icon</a>
</div>
</div>
<div class="bootstrap">
    <div class="modal fade in" id="icon_{$input.name}_id" tabindex="-1" aria-hidden="false" style="display: block;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="icon_popup_cls close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Select Icon</h4>
                </div>
                <div class="modal-body">
                    <ul class="icon_type_list">
                        {foreach $icon_list as $icon_class}
                            <li data-icon_class="{$icon_class}"><i class="{$icon_class}"></i></li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
	ul.icon_type_list{
		list-style:none;
		padding:0;
		overflow: hidden;
		display: block;
		text-align: center;
	}
	ul.icon_type_list li{
		display: inline-block;
		border: 1px solid #eee;
		margin: 0px 10px 10px 0px;
		width: 40px;
		height: 40px;
		line-height: 44px;
		text-align: center;
		cursor: pointer;
	}
	ul.icon_type_list li i{
		font-size: 20px;
	}
</style>

<script type="text/javascript">
$("#icon_{$input.name}_id").hide();
$("ul.icon_type_list li").on('click',function(){
var icc =  $(this).attr("data-icon_class");
$("#icon_type_{$input.name}").val(icc);
$(".icon_popup_cls").trigger( "click" );
$(".modal-backdrop").remove();
});
</script>
	{else}
        {$smarty.block.parent}
    {/if}
{/block}
{block name="input"}
	{if ($input.type == 'selecttwotype')}
			<div class="{if isset($input.hideclass)}{$input.hideclass}{/if} {$input.name} {$input.name}_class" id="{$input.name}_id">
			<select name="selecttwotype_{$input.name}" class="selecttwotype_{$input.name}_cls" id="selecttwotype_{$input.name}_id" multiple="true">
			    {foreach from=$input.initvalues item=initval}
			        {if isset($fields_value[$input.name])}
			            {assign var=settings_def_value value=","|explode:$fields_value[$input.name]}
			            {if $initval['id']|in_array:$settings_def_value}
			                {$selected = 'selected'}
			            {else}
			                {$selected = ''}
			            {/if}
			        {else}
			            {$selected = ''}
			        {/if}
			        <option {$selected} value="{$initval['id']}">{$initval['name']}</option>
			    {/foreach}
			</select>
			<input type="hidden" name="{$input.name}" id="{$input.name}" value="{if isset($input.defvalues)}{$input.defvalues}{else}{$fields_value[$input.name]}{/if}" class=" {$input.name} {$input.type}_field">
			</div>
			<script type="text/javascript">
			    // START SELECT TWO CALLING
			    $(function(){
			        var defVal = $("input#{$input.name}").val();
			        if(defVal.length){
			            var ValArr = defVal.split(',');
			            for(var n in ValArr){
			                $( "select#selecttwotype_{$input.name}_id" ).children('option[value="'+ValArr[n]+'"]').attr('selected','selected');
			            }
			        }
			        $( "select#selecttwotype_{$input.name}_id" ).select2( { placeholder: "{$input.placeholder}", width: 200, tokenSeparators: [',', ' '] } ).on('change',function(){
			            var data = $(this).select2('data');
			            var select = $(this);
			            var field = select.next("input#{$input.name}");
			            var saved = '';
			            select.children('option').attr('selected',null);
			            if(data.length)
			                $.each(data, function(k,v){
			                    var selected = v.id;   
			                    select.children('option[value="'+selected+'"]').attr('selected','selected');
			                    if(k > 0)
			                        saved += ',';
			                    saved += selected;                                
			                });
			             field.val(saved);   
			        });
			    });
 			// END SELECT TWO CALLING
			</script>
			<style type="text/css">
				.select2-container.select2-container-multi
				{ 
					width: 100% !important;
				}
			</style>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
{block name="input_row"}
	{if $input.type == 'servicelists'}
		{* start slides lists *}
		{if isset($id_xprtserviceblock) && !empty($id_xprtserviceblock)}
		<div class="panel"><h3><i class="icon-list-ul"></i> {l s='Service Block Items Lists' mod='xprtserviceblock'}
			<span class="panel-heading-action">
				<a id="desc-product-new" class="list-toolbar-btn" href="{$adminmoduleslink}&configure=xprtserviceblock&AddService=1&id_xprtserviceblock={$id_xprtserviceblock}">
					<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Add new" data-html="true">
						<i class="process-icon-new "></i>
					</span>
				</a>
			</span>
			</h3>
			<div id="slidesContent">
				<div id="slides">
				{if isset($xprtservicelists) && !empty($xprtservicelists)}
					{foreach from=$xprtservicelists item=xprtservice}
						<div id="slides_{$xprtservice.id_xprtservice}" class="panel">
							<div class="row">
								<div class="col-lg-1">
									<span><i class="icon-arrows "></i></span>
								</div>
								{* <div class="col-md-3">
								{if isset($xprtservice.slideimage) && !empty($xprtservice.slideimage)}
									<img src="{$image_baseurl}{$xprtservice.slideimage}" alt="{$xprtservice.title}" class="img-thumbnail" height="auto" width="150" />
								{else}
									<img src="{$image_baseurl}noimage.jpg" alt="{$xprtservice.title}" class="img-thumbnail" height="auto" width="150" />
								{/if}
								</div> *}
								<div class="col-md-8">
									<h4 class="pull-left">
										#{$xprtservice.id_xprtservice} - {$xprtservice.title}
									</h4>
									<div class="btn-group-action pull-right">
										{$xprtservice.status}
										<a class="btn btn-default"
											href="{$adminmoduleslink}&configure=xprtserviceblock&id_xprtservice={$xprtservice.id_xprtservice}&id_xprtserviceblock={$id_xprtserviceblock}&UpdateService=1">
											<i class="icon-edit"></i>
											{l s='Edit' mod='xprtserviceblock'}
										</a>
										<a class="btn btn-default"
											href="{$adminmoduleslink}&configure=xprtserviceblock&delete_id_xprtservice={$xprtservice.id_xprtservice}&id_xprtserviceblock={$id_xprtserviceblock}&editxprtservice=1">
											<i class="icon-trash"></i>
											{l s='Delete' mod='xprtserviceblock'}
										</a>
									</div>
								</div>
							</div>
						</div>
					{/foreach}
				{/if}
				</div>
			</div>
		</div>
		{else}
			<div class="alert alert-warning">
					There is 1 warning.
				<ul style="display:block;" id="seeMore">
					<li>You must save this Block before adding Service block items.</li>
				</ul>
			</div>
		{/if}
		{* end slides lists *}
	{else}
		{$smarty.block.parent}
	{/if}
{/block}