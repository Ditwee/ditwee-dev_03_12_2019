{if isset($xprtserviceblock) && !empty($xprtserviceblock)}
<div class="kr_service_block_area">
	<div class="kr_service_block {if isset($xprtserviceblock.design_style) && !empty($xprtserviceblock.design_style)} service_style_{$xprtserviceblock.design_style}{/if}">
		<div class="kr_service_block_inner">
			<ul class="row">
				{foreach from=$xprtserviceblock.all_service item=service}
					<li class="kr_service_block_single col-sm-4">
						<div class="kr_service_block_top clearfix">
							{if $service.serviceimage_enable != 1}
								<div class="kr_service_block_icon">
									<i class="{$service.icon}"></i>
								</div>
							{else}
								<div class="kr_service_block_img">
									{if isset($service.serviceimage) && !empty($service.serviceimage)}
										<img class="img-responsive" src="{$service.serviceimage}" alt="{$service.title}" />
									{/if}
								</div>
							{/if}
						</div>
						<div class="kr_service_block_bottom">
							<h4>{$service.title}</h4>
							<p>{$service.description}</p>
						</div>
					</li>
				{/foreach}
			</ul>
		</div>
	</div>
</div>
{/if}