{if isset($xprtserviceblock) && !empty($xprtserviceblock)}
<div class="kr_service_block_area">
	<div class="kr_service_block kr_service_proddesc_left {if isset($xprtserviceblock.design_style) && !empty($xprtserviceblock.design_style)} service_style_{$xprtserviceblock.design_style}{/if}">
		<div class="kr_service_block_inner clearfix">
			<div class="kr_service_block_inner_left" style="background:url({$image_baseurl}{$xprtserviceblock.bgimage}) no-repeat scroll center center/ cover;height:{$xprtserviceblock.height|intval}px;">
			</div>
			<div class="kr_service_block_inner_right">
				{if isset($xprtserviceblock.title) && !empty($xprtserviceblock.title)}
					<h3 class="service_title">{$xprtserviceblock.title}</h3>
				{/if}
				{if isset($xprtserviceblock.subtitle) && !empty($xprtserviceblock.subtitle)}
					<p class="service_subtitle">{$xprtserviceblock.subtitle}</p>
				{/if}
				<ul class="clearfix service_list">
					{foreach from=$xprtserviceblock.all_service item=service}
						<li class="kr_service_block_single">
							<div class="kr_service_block_top clearfix">
								{if $service.serviceimage_enable != 1}
									<div class="kr_service_block_icon">
										<i class="{$service.icon}"></i>
									</div>
								{else}
									<div class="kr_service_block_img">
										{if isset($service.serviceimage) && !empty($service.serviceimage)}
											<img class="img-responsive" src="{$service.serviceimage}" alt="{$service.title}" />
										{/if}
									</div>
								{/if}
							</div>
							<div class="kr_service_block_bottom">
								{if isset($service.title) && !empty($service.title)}
									<h4>{$service.title}</h4>
								{/if}
								{if isset($service.subtitle) && !empty($service.subtitle)}
									<p>{$service.subtitle}</p>
								{/if}
								{if isset($service.description) && !empty($service.description)}
									<p>{$service.description}</p>
								{/if}
							</div>
						</li>
					{/foreach}
				</ul>
			</div>
		</div>
	</div>
</div>
{/if}