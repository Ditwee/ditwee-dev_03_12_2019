<?php
class xprtsizechartmainclass extends ObjectModel
{
    public $id_xprtsizechartmain;   
    public $active = 1;
    public $position;
    public $titlemain;
    public $contentmain;
    public $productpage;
    public $hook;
    public $sizeimage;
    public $sizeimagepos;
    public $sizeguide;
    public static $definition = array(
        'table' => 'xprtsizechartmain',
        'primary' => 'id_xprtsizechartmain',
        'multilang'=>true,
        'fields' => array(
            'position' =>    array('type' => self::TYPE_INT),
            'active' =>      array('type' => self::TYPE_BOOL, 'validate' => 'isBool','required' => true),
            'titlemain' =>   array('type' => self::TYPE_STRING, 'validate' => 'isString','required' => true, 'lang' => true),
            'contentmain' => array('type' => self::TYPE_STRING, 'validate' => 'isString','required' => true),
            'sizeguide' =>   array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml','lang' => true),
            'productpage' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'hook' =>        array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'sizeimage' =>   array('type' => self::TYPE_STRING, 'validate' => 'isString'),
            'sizeimagepos' =>array('type' => self::TYPE_STRING, 'validate' => 'isString'),
        ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation('xprtsizechartmain', array('type' => 'shop'));
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = xprtsizechartmainclass::getHigherPosition() + 1;
            $xvalues = array();
            if(isset($_POST))
	            foreach ($_POST as $xkey => $xvalue) {
	            	if(substr($xkey,0,7) == "sizeval"){
	            			$xvalues[$xkey] = $xvalue;
	            	}
	            }
        $this->contentmain = Tools::jsonEncode($xvalues);
        $this->sizeimage = $this->processImage($_FILES);
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public function update($null_values = false)
    {
        $xvalues = array();
        if(isset($_POST))
            foreach ($_POST as $xkey => $xvalue) {
            	if(substr($xkey,0,7) == "sizeval"){
            			$xvalues[$xkey] = $xvalue;
            	}
            }
        $this->contentmain = Tools::jsonEncode($xvalues);
    	if(isset($_FILES['sizeimage']) && isset($_FILES['sizeimage']['tmp_name']) && !empty($_FILES['sizeimage']['tmp_name']))
            $this->sizeimage = $this->processImage($_FILES);
        if(!parent::update($null_values))
            return false;
        return true;
    }
    public function processImage($FILES) {
        if (isset($FILES['sizeimage']) && isset($FILES['sizeimage']['tmp_name']) && !empty($FILES['sizeimage']['tmp_name'])) {
                $ext = substr($FILES['sizeimage']['name'], strrpos($FILES['sizeimage']['name'], '.') + 1);
                $id = time();
                $file_name = $id . '.' . $ext;
                $path = _PS_MODULE_DIR_ .'xprtsizechart/img/' . $file_name;
                if (!move_uploaded_file($FILES['sizeimage']['tmp_name'], $path))
                    return false;         
                else
                    return $file_name;   
        }
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtsizechartmain`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetInstance()
    {
        $ins = new xprtsizechartmainclass();
        return $ins;
    }
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS('
            SELECT `id_xprtsizechartmain`, `position`
            FROM `'._DB_PREFIX_.'xprtsizechartmain`
            ORDER BY `position` ASC'
        ))
            return false;
        foreach ($res as $xprtsizechartmain)
            if ((int)$xprtsizechartmain['id_xprtsizechartmain'] == (int)$this->id)
                $moved_xprtsizechartmain = $xprtsizechartmain;
        if (!isset($moved_xprtsizechartmain) || !isset($position))
            return false;
        $query_1 = ' UPDATE `'._DB_PREFIX_.'xprtsizechartmain`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
        ? '> '.(int)$moved_xprtsizechartmain['position'].' AND `position` <= '.(int)$position
        : '< '.(int)$moved_xprtsizechartmain['position'].' AND `position` >= '.(int)$position.'
        ');
        $query_2 = ' UPDATE `'._DB_PREFIX_.'xprtsizechartmain`
        SET `position` = '.(int)$position.'
        WHERE `id_xprtsizechartmain` = '.(int)$moved_xprtsizechartmain['id_xprtsizechartmain'];
        return (Db::getInstance()->execute($query_1)
        && Db::getInstance()->execute($query_2));
    }
    public static function GetAllSizeChart($hook = NULL)
    {
    	if($hook != NULL){
    		$sqlmidfix = ' AND v.hook = "'.$hook.'" ';
    	}else{
    		$sqlmidfix = '';
    	}
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtsizechartmain` v 
                INNER JOIN `'._DB_PREFIX_.'xprtsizechartmain_lang` vl ON (v.`id_xprtsizechartmain` = vl.`id_xprtsizechartmain` AND vl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.'xprtsizechartmain_shop` vs ON (v.`id_xprtsizechartmain` = vs.`id_xprtsizechartmain` AND vs.`id_shop` = '.$id_shop.')
                WHERE ';
        $sql .= ' v.`active` = 1 '.$sqlmidfix.' ORDER BY v.`position` DESC';
            $results = Db::getInstance()->executeS($sql);
        return $results;
    }
}