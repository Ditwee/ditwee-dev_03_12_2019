<?php
class xprtsizechartoneclass extends ObjectModel
{
        public $id_xprtsizechartone;   
        public $active = 1;
        public $position;
        public $titleone;
        public static $definition = array(
            'table' => 'xprtsizechartone',
            'primary' => 'id_xprtsizechartone',
            'multilang'=>true,
            'fields' => array(
                'position' =>       array('type' => self::TYPE_INT),
                'active' =>         array('type' => self::TYPE_BOOL, 'validate' => 'isBool','required' => true),
                'titleone' =>       array('type' => self::TYPE_STRING, 'validate' => 'isString','required' => true, 'lang' => true),
            ),
        );
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation('xprtsizechartone', array('type' => 'shop'));
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = xprtsizechartoneclass::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public function update($null_values = false)
    {
        if(!parent::update($null_values))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtsizechartone`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetInstance()
    {
        $ins = new xprtsizechartoneclass();
        return $ins;
    }
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS('
            SELECT `id_xprtsizechartone`, `position`
            FROM `'._DB_PREFIX_.'xprtsizechartone`
            ORDER BY `position` ASC'
        ))
            return false;
        foreach ($res as $xprtsizechartone)
            if ((int)$xprtsizechartone['id_xprtsizechartone'] == (int)$this->id)
                $moved_xprtsizechartone = $xprtsizechartone;
        if (!isset($moved_xprtsizechartone) || !isset($position))
            return false;
        $query_1 = ' UPDATE `'._DB_PREFIX_.'xprtsizechartone`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
        ? '> '.(int)$moved_xprtsizechartone['position'].' AND `position` <= '.(int)$position
        : '< '.(int)$moved_xprtsizechartone['position'].' AND `position` >= '.(int)$position.'
        ');
        $query_2 = ' UPDATE `'._DB_PREFIX_.'xprtsizechartone`
        SET `position` = '.(int)$position.'
        WHERE `id_xprtsizechartone` = '.(int)$moved_xprtsizechartone['id_xprtsizechartone'];
        return (Db::getInstance()->execute($query_1)
        && Db::getInstance()->execute($query_2));
    }
    public static function GetAllSizeChart()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtsizechartone` v 
                INNER JOIN `'._DB_PREFIX_.'xprtsizechartone_lang` vl ON (v.`id_xprtsizechartone` = vl.`id_xprtsizechartone` AND vl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.'xprtsizechartone_shop` vs ON (v.`id_xprtsizechartone` = vs.`id_xprtsizechartone` AND vs.`id_shop` = '.$id_shop.')
                WHERE ';
        $sql .= ' v.`active` = 1 ORDER BY v.`id_xprtsizechartone` ASC';
            $results = Db::getInstance()->executeS($sql);
        return $results;
    }
}