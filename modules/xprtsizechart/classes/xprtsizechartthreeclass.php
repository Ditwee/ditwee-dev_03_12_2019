<?php
class xprtsizechartthreeclass extends ObjectModel
{
        public $id_xprtsizechartthree;   
        public $active = 1;
        public $position;
        public $titlethree;
        public static $definition = array(
            'table' => 'xprtsizechartthree',
            'primary' => 'id_xprtsizechartthree',
            'multilang'=>true,
            'fields' => array(
                'position' =>       array('type' => self::TYPE_INT),
                'active' =>         array('type' => self::TYPE_BOOL, 'validate' => 'isBool','required' => true),
                'titlethree' =>       array('type' => self::TYPE_STRING, 'validate' => 'isString','required' => true, 'lang' => true),
            ),
        );
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation('xprtsizechartthree', array('type' => 'shop'));
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = xprtsizechartthreeclass::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public function update($null_values = false)
    {
        if(!parent::update($null_values))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtsizechartthree`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetInstance()
    {
        $ins = new xprtsizechartthreeclass();
        return $ins;
    }
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS('
            SELECT `id_xprtsizechartthree`, `position`
            FROM `'._DB_PREFIX_.'xprtsizechartthree`
            ORDER BY `position` ASC'
        ))
            return false;
        foreach ($res as $xprtsizechartthree)
            if ((int)$xprtsizechartthree['id_xprtsizechartthree'] == (int)$this->id)
                $moved_xprtsizechartthree = $xprtsizechartthree;
        if (!isset($moved_xprtsizechartthree) || !isset($position))
            return false;
        $query_1 = ' UPDATE `'._DB_PREFIX_.'xprtsizechartthree`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
        ? '> '.(int)$moved_xprtsizechartthree['position'].' AND `position` <= '.(int)$position
        : '< '.(int)$moved_xprtsizechartthree['position'].' AND `position` >= '.(int)$position.'
        ');
        $query_2 = ' UPDATE `'._DB_PREFIX_.'xprtsizechartthree`
        SET `position` = '.(int)$position.'
        WHERE `id_xprtsizechartthree` = '.(int)$moved_xprtsizechartthree['id_xprtsizechartthree'];
        return (Db::getInstance()->execute($query_1)
        && Db::getInstance()->execute($query_2));
    }
    public static function GetAllSizeChart()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtsizechartthree` v 
                INNER JOIN `'._DB_PREFIX_.'xprtsizechartthree_lang` vl ON (v.`id_xprtsizechartthree` = vl.`id_xprtsizechartthree` AND vl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.'xprtsizechartthree_shop` vs ON (v.`id_xprtsizechartthree` = vs.`id_xprtsizechartthree` AND vs.`id_shop` = '.$id_shop.')
                WHERE ';
        $sql .= ' v.`active` = 1 ORDER BY v.`id_xprtsizechartthree` ASC';
            $results = Db::getInstance()->executeS($sql);
        return $results;
    }
}