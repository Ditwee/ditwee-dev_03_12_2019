<?php
class xprtsizecharttwoclass extends ObjectModel
{
    public $id_xprtsizecharttwo;   
    public $active = 1;
    public $position;
    public $titletwo;
    public static $definition = array(
        'table' => 'xprtsizecharttwo',
        'primary' => 'id_xprtsizecharttwo',
        'multilang'=>true,
        'fields' => array(
            'position' =>       array('type' => self::TYPE_INT),
            'active' =>         array('type' => self::TYPE_BOOL, 'validate' => 'isBool','required' => true),
            'titletwo' =>       array('type' => self::TYPE_STRING, 'validate' => 'isString','required' => true, 'lang' => true),
        ),
    );
    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation('xprtsizecharttwo', array('type' => 'shop'));
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = xprtsizecharttwoclass::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public function update($null_values = false)
    {
        if(!parent::update($null_values))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtsizecharttwo`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetInstance()
    {
        $ins = new xprtsizecharttwoclass();
        return $ins;
    }
    public function updatePosition($way, $position)
    {
        if (!$res = Db::getInstance()->executeS('
            SELECT `id_xprtsizecharttwo`, `position`
            FROM `'._DB_PREFIX_.'xprtsizecharttwo`
            ORDER BY `position` ASC'
        ))
            return false;
        foreach ($res as $xprtsizecharttwo)
            if ((int)$xprtsizecharttwo['id_xprtsizecharttwo'] == (int)$this->id)
                $moved_xprtsizecharttwo = $xprtsizecharttwo;
        if (!isset($moved_xprtsizecharttwo) || !isset($position))
            return false;
        $query_1 = ' UPDATE `'._DB_PREFIX_.'xprtsizecharttwo`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
        ? '> '.(int)$moved_xprtsizecharttwo['position'].' AND `position` <= '.(int)$position
        : '< '.(int)$moved_xprtsizecharttwo['position'].' AND `position` >= '.(int)$position.'
        ');
        $query_2 = ' UPDATE `'._DB_PREFIX_.'xprtsizecharttwo`
        SET `position` = '.(int)$position.'
        WHERE `id_xprtsizecharttwo` = '.(int)$moved_xprtsizecharttwo['id_xprtsizecharttwo'];
        return (Db::getInstance()->execute($query_1)
        && Db::getInstance()->execute($query_2));
    }
    public static function GetAllSizeChart()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtsizecharttwo` v 
                INNER JOIN `'._DB_PREFIX_.'xprtsizecharttwo_lang` vl ON (v.`id_xprtsizecharttwo` = vl.`id_xprtsizecharttwo` AND vl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.'xprtsizecharttwo_shop` vs ON (v.`id_xprtsizecharttwo` = vs.`id_xprtsizecharttwo` AND vs.`id_shop` = '.$id_shop.')
                WHERE ';
        $sql .= ' v.`active` = 1 ORDER BY v.`id_xprtsizecharttwo` ASC';
            $results = Db::getInstance()->executeS($sql);
        return $results;
    }
}