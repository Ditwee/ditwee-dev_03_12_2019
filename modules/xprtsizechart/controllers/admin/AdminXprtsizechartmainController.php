<?php

class AdminXprtsizechartmainController extends ModuleAdminController{
  public $asso_type = 'shop';
  private $original_filter = '';
  protected $position_identifier = 'id_xprtsizechartmain';
	public function __construct()
	{
    $this->table = 'xprtsizechartmain';
    $this->className = 'xprtsizechartmainclass';
    $this->lang = true;
    $this->deleted = false;
    $this->module = 'xprtsizechart';
    $this->explicitSelect = true;
    $this->_defaultOrderBy = 'position';
    $this->allow_export = false;
    $this->_defaultOrderWay = 'DESC';
    $this->bootstrap = true;
    $this->context = Context::getContext();
        if (Shop::isFeatureActive())
        Shop::addTableAssociation($this->table, array('type' => 'shop'));
        parent::__construct();
		$this->fields_list = array(
                            'id_xprtsizechartmain' => array(
                                    'title' => $this->l('Id'),
                                    'width' => 100,
                                    'type' => 'text',
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                            ),
                            'titlemain' => array(
                                    'title' => $this->l('Name'),
                                    'width' => 440,
                                    'type' => 'text',
                                    'lang'=>true,
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                            ),
                            'position' => array(
                                    'title' => $this->l('Position'),
                                    'filter_key' => 'a!position',
                                    'position' => 'position',
                                    'align' => 'center'
                          ),
                          'active' => array(
                                    'title' => $this->l('Status'),
                                    'width' => '70',
                                    'align' => 'center',
                                    'active' => 'status',
                                    'type' => 'bool',
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                          )
		          );
              $this->bulk_actions = array(
                  'delete' => array(
                      'text' => $this->l('Delete selected'),
                      'icon' => 'icon-trash',
                      'confirm' => $this->l('Delete selected items?')
                  )
              );
		parent::__construct();
	}
	public function init()
	{
		parent::init();
		$this->_join = 'LEFT JOIN '._DB_PREFIX_.'xprtsizechartmain_shop sbs ON a.id_xprtsizechartmain=sbs.id_xprtsizechartmain && sbs.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
		$this->_select = 'sbs.id_shop';
		$this->_defaultOrderBy = 'a.position';
		$this->_defaultOrderWay = 'DESC';
		if(Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
		$this->_group = 'GROUP BY a.id_xprtsizechartmain';
		$this->_select = 'a.position position';
	}
	public function renderList()
	{
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		return parent::renderList();
	}
	public function renderForm()
  	{
  		$ones = xprtsizechartoneclass::GetAllSizeChart();
  		$twos = xprtsizecharttwoclass::GetAllSizeChart();
  		$threes = xprtsizechartthreeclass::GetAllSizeChart();
  		$id_xprtsizechartmain = Tools::getValue("id_xprtsizechartmain");
  		if(isset($id_xprtsizechartmain) && !empty($id_xprtsizechartmain)){
  			$xobjs = new xprtsizechartmainclass($id_xprtsizechartmain);
  			$xvalues = Tools::jsonDecode($xobjs->contentmain);
  			$productpage = $xobjs->productpage;
  		}else{
  			$xvalues = "";
  			$productpage = "";
  		}
        $this->fields_form = array(
          'legend' => array(
          'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'titlemain',
                    'lang'=>true,
                    'required' => true,
                    'desc' => $this->l('Enter Name')
                ),
                array(
                    'type' => 'xprtsizechart',
                    'label' => $this->l('Size Chart Settings'),
                    'name' => 'contentall',
                    'ones' => $ones,
                    'twos' => $twos,
                    'threes' => $threes,
                    'xvalues' => $xvalues,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Size Guide'),
                    'name' => 'sizeguide',
                    'lang'=> true,
                    'rows' => 10,
                    'cols' => 62,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'desc' => $this->l('Enter your Size Guide.')
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('SizeChart Image'),
                    'name' => 'sizeimage',
                    'desc' => $this->l('Upload A Image For Display Sizechart.')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('How to show Size chart'),
                    'name' => 'sizeimagepos',
                    'options' => array(
                        'query' => array(
                        		array(
                        			'id'=>'popup',
                        			'name'=>'Popup',
                        		),
                        		array(
                        			'id'=>'reg_tab',
                        			'name'=>'Regular Tab',
                        		),
                        		array(
                        			'id'=>'prod_tab',
                        			'name'=>'Show in product tab',
                        		)
                        	),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Please Select Sizechart Image Position.'),
                ),
                array(
                    'type' => 'selecttwotype',
                    'label' => $this->l('Which Product Page You Want to Display'),
                    'placeholder' => $this->l('Please Select Products.'),
                    'initvalues' => self::AllPageExceptions(),
                    'defvalues' => $productpage,
                    'name' => 'productpage',
                    'desc' => $this->l('Please Type Your Specific Product Name. You Can Select Specific Category,Brands,Supplier Wise all Product Page.')
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Select Hook: '),
                    'name' => 'hook',
                    'options' => array(
                        'query' => array(
                        		array(
                        			'id'=>'displayLeftColumnProduct',
                        			'name'=>'displayLeftColumnProduct',
                        		),
                        		array(
                        			'id'=>'displayRightColumnProduct',
                        			'name'=>'displayRightColumnProduct',
                        		),
                        		array(
                        			'id'=>'displayProductButtons',
                        			'name'=>'displayProductButtons',
                        		),
                        		array(
                        			'id'=>'displayProductTabContent',
                        			'name'=>'displayProductTabContent',
                        		),
                        		array(
                        			'id'=>'displayProductContent',
                        			'name'=>'displayProductContent',
                        		)
                        	),
                        'id' => 'id',
                        'name' => 'name'
                    ),
                    'desc' => $this->l('Please Select Your Hook Where you want to display.'),
                ),
                array(
                       'type' => 'switch',
                       'label' => $this->l('Status'),
                       'name' => 'active',
                       'required' => true,
                       'class' => 't',
                       'is_bool' => true,
                       'values' => array(
                           array(
                             'id' => 'active',
                             'value' => 1,
                             'label' => $this->l('Enabled')
                           ),
                           array(
                             'id' => 'active',
                             'value' => 0,
                             'label' => $this->l('Disabled')
                           )
                       )
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        if (Shop::isFeatureActive())
        {
          $this->fields_form['input'][] = array(
            'type' => 'shop',
            'label' => $this->l('Shop association:'),
            'name' => 'checkBoxShopAsso',
          );
        }
        if (!($xprtsizechartmainclass = $this->loadObject(true)))
            return;
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save   '),
            'class' => 'button'
        );
        return parent::renderForm();
    }
    public static function AllPageExceptions()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $sql = 'SELECT p.`id_product`, pl.`name`
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
        $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        $id_lang = Context::getContext()->language->id;
        $categories =  Category::getCategories($id_lang,true,false);
        $Manufacturers =  Manufacturer::getManufacturers();
        $Suppliers =  Supplier::getSuppliers();
        $rslt = array();
        $rslt[0]['id'] = 'all_page';
        $rslt[0]['name'] = 'All Products';
        $i = 1;
        if(isset($products))
            foreach($products as $r){
                $rslt[$i]['id'] = 'prd_'.$r['id_product'];
                $rslt[$i]['name'] = 'Product : '. $r['name'];
                $i++;
            }
        if(isset($categories))
            foreach($categories as $cats){
                $rslt[$i]['id'] = 'prdcat_'.$cats['id_category'];
                $rslt[$i]['name'] = 'Category Product: '.$cats['name'];
                $i++;
            }
        if(isset($Manufacturers))
            foreach($Manufacturers as $r){
                $rslt[$i]['id'] = 'prdman_'.$r['id_manufacturer'];
                $rslt[$i]['name'] = 'Manufacturer Product : '.$r['name'];
                $i++;
            }
        if(isset($Suppliers))
            foreach($Suppliers as $r){
                $rslt[$i]['id'] = 'prdsup_'.$r['id_supplier'];
                $rslt[$i]['name'] = 'Supplier Product : '.$r['name'];
                $i++;
            }
        return $rslt;
    }
    public function setMedia()
    {          
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
        $this->addJqueryPlugin('select2');
    }
    public function initToolbar()
    {
        parent::initToolbar();
    }
    public function processPosition()
    {
      if ($this->tabAccess['edit'] !== '1')
        $this->errors[] = Tools::displayError('You do not have permission to edit this.');
      else if (!Validate::isLoadedObject($object = new xprtsizechartmainclass((int)Tools::getValue($this->identifier, Tools::getValue('id_xprtsizechartmain', 1)))))
        $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.').' <b>'.
          $this->table.'</b> '.Tools::displayError('(cannot load object)');
      if (!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position')))
        $this->errors[] = Tools::displayError('Failed to update the position.');
      else
      {
        $object->regenerateEntireNtree();
        Tools::redirectAdmin(self::$currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_xprtsizechartmain = (int)Tools::getValue($this->identifier)) ? ('&'.$this->identifier.'='.$id_xprtsizechartmain) : '').'&token='.Tools::getAdminTokenLite('AdminXprtsizechartmainController'));
      }
    }
    public function ajaxProcessUpdatePositions()
    {
      $id_xprtsizechartmain = (int)(Tools::getValue('id'));
      $way = (int)(Tools::getValue('way'));
      $positions = Tools::getValue($this->table);
      if (is_array($positions))
        foreach ($positions as $key => $value)
        {
          $pos = explode('_', $value);
          if ((isset($pos[1]) && isset($pos[2])) && ($pos[2] == $id_xprtsizechartmain))
          {
            $position = $key + 1;
            break;
          }
        }
      $xprtsizechartmainclass = new xprtsizechartmainclass($id_xprtsizechartmain);
      if (Validate::isLoadedObject($xprtsizechartmainclass))
      {
        if (isset($position) && $xprtsizechartmainclass->updatePosition($way, $position))
        {
          Hook::exec('actionxprtsizechartmainclassUpdate');
          die(true);
        }
        else
          die('{"hasError" : true, errors : "Can not update position"}');
      }
      else
        die('{"hasError" : true, "errors" : "This Object can not be loaded"}');
    }
}

