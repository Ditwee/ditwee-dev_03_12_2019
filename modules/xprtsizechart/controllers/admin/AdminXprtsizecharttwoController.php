<?php

class AdminXprtsizecharttwoController extends ModuleAdminController{
  public $asso_type = 'shop';
  private $original_filter = '';
  protected $position_identifier = 'id_xprtsizecharttwo';
	public function __construct()
	{
    $this->table = 'xprtsizecharttwo';
    $this->className = 'xprtsizecharttwoclass';
    $this->lang = true;
    $this->deleted = false;
    $this->module = 'xprtsizechart';
    $this->explicitSelect = true;
    $this->_defaultOrderBy = 'position';
    $this->allow_export = false;
    $this->_defaultOrderWay = 'DESC';
    $this->bootstrap = true;
    $this->context = Context::getContext();
        if (Shop::isFeatureActive())
        Shop::addTableAssociation($this->table, array('type' => 'shop'));
        parent::__construct();
		$this->fields_list = array(
                            'id_xprtsizecharttwo' => array(
                                    'title' => $this->l('Id'),
                                    'width' => 100,
                                    'type' => 'text',
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                            ),
                            'titletwo' => array(
                                    'title' => $this->l('Name'),
                                    'width' => 440,
                                    'type' => 'text',
                                    'lang'=>true,
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                            ),
                            'position' => array(
                                    'title' => $this->l('Position'),
                                    'filter_key' => 'a!position',
                                    'position' => 'position',
                                    'align' => 'center'
                          ),
                          'active' => array(
                                    'title' => $this->l('Status'),
                                    'width' => '70',
                                    'align' => 'center',
                                    'active' => 'status',
                                    'type' => 'bool',
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                          )
		          );
              $this->bulk_actions = array(
                  'delete' => array(
                      'text' => $this->l('Delete selected'),
                      'icon' => 'icon-trash',
                      'confirm' => $this->l('Delete selected items?')
                  )
              );
		parent::__construct();
	}
  public function init()
  {
      parent::init();
      $this->_join = 'LEFT JOIN '._DB_PREFIX_.'xprtsizecharttwo_shop sbs ON a.id_xprtsizecharttwo=sbs.id_xprtsizecharttwo && sbs.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
      $this->_select = 'sbs.id_shop';
      $this->_defaultOrderBy = 'a.position';
      $this->_defaultOrderWay = 'DESC';
      if(Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
        $this->_group = 'GROUP BY a.id_xprtsizecharttwo';
      $this->_select = 'a.position position';
  }
  public function renderList()
  {
    $this->addRowAction('edit');
    $this->addRowAction('delete');
    return parent::renderList();
  }
	public function renderForm()
  {
        $this->fields_form = array(
          'legend' => array(
          'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'titletwo',
                    'lang'=>true,
                    'required' => true,
                    'desc' => $this->l('Enter Name')
                ),
                array(
                       'type' => 'switch',
                       'label' => $this->l('Status'),
                       'name' => 'active',
                       'required' => true,
                       'class' => 't',
                       'is_bool' => true,
                       'values' => array(
                           array(
                             'id' => 'active',
                             'value' => 1,
                             'label' => $this->l('Enabled')
                           ),
                           array(
                             'id' => 'active',
                             'value' => 0,
                             'label' => $this->l('Disabled')
                           )
                       )
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        if (Shop::isFeatureActive())
        {
          $this->fields_form['input'][] = array(
            'type' => 'shop',
            'label' => $this->l('Shop association:'),
            'name' => 'checkBoxShopAsso',
          );
        }
        if (!($xprtsizecharttwoclass = $this->loadObject(true)))
            return;
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save   '),
            'class' => 'button'
        );
        return parent::renderForm();
    }
    public function setMedia()
    {          
          parent::setMedia();
          $this->addJqueryUi('ui.widget');
          $this->addJqueryPlugin('tagify');
    }
    public function initToolbar()
    {
        parent::initToolbar();
    }
    public function processPosition()
    {
      if ($this->tabAccess['edit'] !== '1')
        $this->errors[] = Tools::displayError('You do not have permission to edit this.');
      else if (!Validate::isLoadedObject($object = new xprtsizecharttwoclass((int)Tools::getValue($this->identifier, Tools::getValue('id_xprtsizecharttwo', 1)))))
        $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.').' <b>'.
          $this->table.'</b> '.Tools::displayError('(cannot load object)');
      if (!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position')))
        $this->errors[] = Tools::displayError('Failed to update the position.');
      else
      {
        $object->regenerateEntireNtree();
        Tools::redirectAdmin(self::$currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_xprtsizecharttwo = (int)Tools::getValue($this->identifier)) ? ('&'.$this->identifier.'='.$id_xprtsizecharttwo) : '').'&token='.Tools::getAdminTokenLite('AdminXprtsizecharttwoController'));
      }
    }
    public function ajaxProcessUpdatePositions()
    {
      $id_xprtsizecharttwo = (int)(Tools::getValue('id'));
      $way = (int)(Tools::getValue('way'));
      $positions = Tools::getValue($this->table);
      if (is_array($positions))
        foreach ($positions as $key => $value)
        {
          $pos = explode('_', $value);
          if ((isset($pos[1]) && isset($pos[2])) && ($pos[2] == $id_xprtsizecharttwo))
          {
            $position = $key + 1;
            break;
          }
        }
      $xprtsizecharttwoclass = new xprtsizecharttwoclass($id_xprtsizecharttwo);
      if (Validate::isLoadedObject($xprtsizecharttwoclass))
      {
        if (isset($position) && $xprtsizecharttwoclass->updatePosition($way, $position))
        {
          Hook::exec('actionxprtsizecharttwoclassUpdate');
          die(true);
        }
        else
          die('{"hasError" : true, errors : "Can not update position"}');
      }
      else
        die('{"hasError" : true, "errors" : "This Object can not be loaded"}');
    }
}

