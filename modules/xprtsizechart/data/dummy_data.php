<?php

$xprtsizechartone = array(
    array(
    	'lang' => array(
		    'titleone' => 'Chest size',
		),
	    'notlang' => array(
		    'active' => 1,
		),
    ),
    array(
    	'lang' => array(
		    'titleone' => 'Neck Size',
		),
	    'notlang' => array(
		    'active' => 1,
		),
    ),
);

$xprtsizecharttwo = array(
    array(
    	'lang' => array(
		    'titletwo' => 'Inches',
		),
	    'notlang' => array(
		    'active' => 1,
		),
    ),
    array(
    	'lang' => array(
		    'titletwo' => 'CM',
		),
	    'notlang' => array(
		    'active' => 1,
		),
    ),
);

$xprtsizechartthree = array(
    array(
    	'lang' => array(
		    'titlethree' => 'XS',
		),
	    'notlang' => array(
		    'active' => 1,
		),
    ),
    array(
    	'lang' => array(
		    'titlethree' => 'S',
		),
	    'notlang' => array(
		    'active' => 1,
		),
    ),
    array(
    	'lang' => array(
		    'titlethree' => 'M',
		),
	    'notlang' => array(
		    'active' => 1,
		),
    ),
    array(
    	'lang' => array(
		    'titlethree' => 'L',
		),
	    'notlang' => array(
		    'active' => 1,
		),
    ),
    array(
    	'lang' => array(
		    'titlethree' => 'XL',
		),
	    'notlang' => array(
		    'active' => 1,
		),
    ),
);

$xprtsizechartmain = array(
    array(
    	'lang' => array(
		    'titlemain' => 'Mens Apparel sizing',
		    'sizeguide' => '<table width="390" style="height:221px;"><tbody><tr><td><p>HOW TO MEASURE:</p><p>To choose the correct size for you, measure your body as follows:</p><p>1. Chestmeasure around the fullest part, place the tape close under the arms and make sure the tape is flat across the back</p></td><td><img src="http://xpert-idea.com/prestashop/jakiro/demo/six/img/cms/mens-blazers.jpg" alt="" width="150" height="207" /></td></tr></tbody></table>',
		),
	    'notlang' => array(
			'active' => 1,
			'contentmain' => '{"sizeval_2_2_5":"45.5","sizeval_2_1_5":"17.5","sizeval_2_2_4":"43.5","sizeval_2_1_4":"17","sizeval_2_2_3":"41.5","sizeval_2_1_3":"16","sizeval_2_2_2":"39.5","sizeval_2_1_2":"15.5","sizeval_2_2_1":"38.5","sizeval_2_1_1":"15","sizeval_1_2_5":"106-111","sizeval_1_1_5":"42-44","sizeval_1_2_4":"101-106","sizeval_1_1_4":"40-42","sizeval_1_2_3":"96-101","sizeval_1_1_3":"38-40","sizeval_1_2_2":"91-96","sizeval_1_1_2":"36-38","sizeval_1_2_1":"86-91","sizeval_1_1_1":"34-36"}',
			'productpage' => 'all_page',
			'hook' => 'displayProductTabContent',
			'sizeimage' => 'image_left.jpg',
			'sizeimagepos' => 'left',
		),
    ),
);