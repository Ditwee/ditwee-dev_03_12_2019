<?php

$mysqlquery = array();

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtsizechartone` (
				`id_xprtsizechartone` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`active` int(10) NOT NULL,
				`position`int(10) NOT NULL,
				PRIMARY KEY (`id_xprtsizechartone`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtsizechartone_lang` (
				`id_xprtsizechartone` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL,
				`titleone` VARCHAR(100) NOT NULL,
				PRIMARY KEY (`id_xprtsizechartone`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtsizechartone_shop` (
			  `id_xprtsizechartone` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprtsizechartone`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

// start two 

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtsizecharttwo` (
				`id_xprtsizecharttwo` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`active` int(10) NOT NULL,
				`position`int(10) NOT NULL,
				PRIMARY KEY (`id_xprtsizecharttwo`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtsizecharttwo_lang` (
				`id_xprtsizecharttwo` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL,
				`titletwo` VARCHAR(100) NOT NULL,
				PRIMARY KEY (`id_xprtsizecharttwo`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtsizecharttwo_shop` (
			  `id_xprtsizecharttwo` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprtsizecharttwo`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

// start three 

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtsizechartthree` (
				`id_xprtsizechartthree` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`active` int(10) NOT NULL,
				`position`int(10) NOT NULL,
				PRIMARY KEY (`id_xprtsizechartthree`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtsizechartthree_lang` (
				`id_xprtsizechartthree` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL,
				`titlethree` VARCHAR(100) NOT NULL,
				PRIMARY KEY (`id_xprtsizechartthree`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtsizechartthree_shop` (
			  `id_xprtsizechartthree` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprtsizechartthree`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

// start main 

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtsizechartmain` (
				`id_xprtsizechartmain` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`active` int(10) NOT NULL,
				`position`int(10) NOT NULL,
				`contentmain` LONGTEXT,
				`productpage` VARCHAR(100) NOT NULL,
				`hook` VARCHAR(100) NOT NULL,
				`sizeimage` VARCHAR(100) NOT NULL,
				`sizeimagepos` VARCHAR(100) NOT NULL,
				PRIMARY KEY (`id_xprtsizechartmain`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtsizechartmain_lang` (
				`id_xprtsizechartmain` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL,
				`titlemain` VARCHAR(100) NOT NULL,
				`sizeguide` LONGTEXT,
				PRIMARY KEY (`id_xprtsizechartmain`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtsizechartmain_shop` (
			  `id_xprtsizechartmain` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprtsizechartmain`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$mysqlquery_u = array();

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizechartone`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizechartone_lang`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizechartone_shop`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizecharttwo`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizecharttwo_lang`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizecharttwo_shop`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizechartthree`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizechartthree_lang`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizechartthree_shop`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizechartmain`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizechartmain_lang`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtsizechartmain_shop`';