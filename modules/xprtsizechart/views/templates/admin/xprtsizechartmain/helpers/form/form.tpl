{extends file="helpers/form/form.tpl"}
{block name="input"}
	{if ($input.type == 'xprtsizechart')}
		<table class="main_table">
			<tr>
				{if isset($input.threes) && !empty($input.threes)}
				<td>
					<table>
						<tr>
							<td>Chart</td>
						</tr>
						<tr>
							<td>Country</td>
						</tr>
						{foreach from=$input.threes item=sizethree}
							<tr>
								<td>{$sizethree.titlethree}</td>
							</tr>
						{/foreach}
					</table>
				</td>
				{/if}
				{if isset($input.ones) && !empty($input.ones)}
					{foreach from=$input.ones item=sizeone}
						<td>
							<table>
								<tr>
									<td colspan="4">{$sizeone.titleone}</td>
								</tr>
								{if isset($input.twos) && !empty($input.twos)}
									{if isset($input.threes) && !empty($input.threes)}
									<tr>
										{foreach from=$input.twos item=sizetwo}
											<td>{$sizetwo.titletwo}</td>
										{/foreach}
									</tr>
										{foreach from=$input.threes item=sizethree}
											<tr class="table_elements">
												{if isset($input.twos) && !empty($input.twos)}
													{foreach from=$input.twos item=sizetwo}
														{assign var="xsizename" value="sizeval_{$sizeone.id_xprtsizechartone}_{$sizetwo.id_xprtsizecharttwo}_{$sizethree.id_xprtsizechartthree}"}

														<td><input type="text" name="sizeval_{$sizeone.id_xprtsizechartone}_{$sizetwo.id_xprtsizecharttwo}_{$sizethree.id_xprtsizechartthree}" value="{if isset($input.xvalues) && !empty($input.xvalues)}{$input.xvalues->$xsizename}{else}0{/if}" class="{$xsizename}"></td>

													{/foreach}
												{/if}
											</tr>
										{/foreach}
									{/if}
								{/if}
							</table>
						</td>
					{/foreach}
				{/if}
			</tr>
		</table>
		{* End markup *}
<style>
.main_table, .main_table table, .main_table th, .main_table td {
   text-align: center;
   vertical-align: top;
   border-collapse: collapse;
}
.main_table{
	border-spacing: 15px;
	padding: 15px;
}
.main_table > tbody > tr > td{
	/*border-right: 1px solid #ccc;*/
}
.main_table > tbody > tr > td:first-child{
	/*border-left: 1px solid #ccc;*/
}
.main_table table tr:first-child td:first-child{
	/*border-top: 1px solid #ccc;
	border-bottom: 1px solid #ccc;*/
	padding: 10px 15px;
}
.main_table table tr td{
	/*border-bottom: 1px solid #ccc;*/
	border: 1px solid #ccc;
	padding: 10px 15px;
}
.main_table table tr td + td{
	/*border-left: 1px solid #ccc;*/
}
.main_table table tr.table_elements td {
    padding: 3px 0px;
}
</style>
	{elseif ($input.type == 'selecttwotype')}
		<div class="{if isset($input.hideclass)}{$input.hideclass}{/if} {$input.name} {$input.name}_class" id="{$input.name}_id">
		<select name="selecttwotype_{$input.name}" class="selecttwotype_{$input.name}_cls" id="selecttwotype_{$input.name}_id" multiple="true">
		    {foreach from=$input.initvalues item=initval}
		        {if isset($fields_value[$input.name])}
		            {assign var=settings_def_value value=","|explode:$fields_value[$input.name]}
		            {if $initval['id']|in_array:$settings_def_value}
		                {$selected = 'selected'}
		            {else}
		                {$selected = ''}
		            {/if}
		        {else}
		            {$selected = ''}
		        {/if}
		        <option {$selected} value="{$initval['id']}">{$initval['name']}</option>
		    {/foreach}
		</select>
		<input type="hidden" name="{$input.name}" id="{$input.name}" value="{if isset($input.defvalues)}{$input.defvalues}{else}{$fields_value[$input.name]}{/if}" class=" {$input.name} {$input.type}_field">
		</div>
		<script type="text/javascript">

		    // START SELECT TWO CALLING
		    $(function(){
		        var defVal = $("input#{$input.name}").val();
		        if(defVal.length){
		            var ValArr = defVal.split(',');
		            for(var n in ValArr){
		                $( "select#selecttwotype_{$input.name}_id" ).children('option[value="'+ValArr[n]+'"]').attr('selected','selected');
		            }
		        }
		        $( "select#selecttwotype_{$input.name}_id" ).select2( { placeholder: "{$input.placeholder}", width: 200, tokenSeparators: [',', ' '] } ).on('change',function(){
		            var data = $(this).select2('data');
		            var select = $(this);
		            var field = select.next("input#{$input.name}");
		            var saved = '';
		            select.children('option').attr('selected',null);
		            if(data.length)
		                $.each(data, function(k,v){
		                    var selected = v.id;   
		                    select.children('option[value="'+selected+'"]').attr('selected','selected');
		                    if(k > 0)
		                        saved += ',';
		                    saved += selected;                                
		                });
		             field.val(saved);   
		        });
		    });
			// END SELECT TWO CALLING
		</script>
		<style type="text/css">
			.select2-container.select2-container-multi
			{ 
				width: 100% !important;
			}
		</style>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}