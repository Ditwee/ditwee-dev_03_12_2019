<table class="main_table">
	<tr>
		{if isset($xprtthree) && !empty($xprtthree)}
		<td>
			<table>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td>{l s='Size' mod='xprtsizechart'}</td>
				</tr>
				{foreach from=$xprtthree item=sizethree}
					<tr>
						<td>{$sizethree.titlethree}</td>
					</tr>
				{/foreach}
			</table>
		</td>
		{/if}
		{if isset($xprtone) && !empty($xprtone)}
			{foreach from=$xprtone item=sizeone}
				<td>
					<table>
						<tr>
							<td colspan="{$sizeone|@count}">{$sizeone.titleone}</td>
						</tr>
						{if isset($xprttwo) && !empty($xprttwo)}
							{if isset($xprtthree) && !empty($xprtthree)}
							<tr>
								{foreach from=$xprttwo item=sizetwo}
									<td>{$sizetwo.titletwo}</td>
								{/foreach}
							</tr>
								{foreach from=$xprtthree item=sizethree}
									<tr class="table_elements">
										{if isset($xprttwo) && !empty($xprttwo)}
											{foreach from=$xprttwo item=sizetwo}
												{assign var="xsizename" value="sizeval_{$sizeone.id_xprtsizechartone}_{$sizetwo.id_xprtsizecharttwo}_{$sizethree.id_xprtsizechartthree}"}
												<td>
												{if isset($xprtmainchart.contentmain) && !empty($xprtmainchart.contentmain)}
												{$xprtmainchart.contentmain->$xsizename}
												{else}
												0
												{/if}</td>
											{/foreach}
										{/if}
									</tr>
								{/foreach}
							{/if}
						{/if}
					</table>
				</td>
			{/foreach}
		{/if}
	</tr>
</table>