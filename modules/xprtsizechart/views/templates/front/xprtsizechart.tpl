{if isset($xprtmain) && !empty($xprtmain)}
	<p class="product_size_chart"><a id="size_chart" href="#product_size_chart">{l s='Size Chart' mod='xprtsizechart'}</a></p>
		<div id="product_size_chart" class="product_size_chart_table" style="display:none;">
			<ul class="nav nav-tabs" role="tablist">
				{foreach from=$xprtmain item=xprtmainchart}
						<li class="active"><a href="#xprtsizechart_{$xprtmainchart.id_xprtsizechartmain}_{$hook}" role="tab" data-toggle="tab">{l s='Size chart' mod='xprtsizechart'}</a></li>
						{if isset($xprtmainchart.sizeguide) && !empty($xprtmainchart.sizeguide)}
							<li><a href="#xprtsizechartguide_{$xprtmainchart.id_xprtsizechartmain}_{$hook}" role="tab" data-toggle="tab">{l s='Size Guide' mod='xprtsizechart'}</a></li>
						{/if}
				{/foreach}
			</ul>
			<div class="tab-content">
				{foreach from=$xprtmain item=xprtmainchart}
					<div class="tab-pane active" id="xprtsizechart_{$xprtmainchart.id_xprtsizechartmain}_{$hook}">
						{include file="./sizechart.tpl"}
					</div>
					{if isset($xprtmainchart.sizeguide) && !empty($xprtmainchart.sizeguide)}
						<div class="tab-pane" id="xprtsizechartguide_{$xprtmainchart.id_xprtsizechartmain}_{$hook}">
							{$xprtmainchart.sizeguide}
						</div>
					{/if}
				{/foreach}
			</div>
		</div>
{/if}