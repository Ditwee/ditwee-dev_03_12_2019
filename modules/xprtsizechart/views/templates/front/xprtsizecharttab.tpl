{if isset($xprtmain) && !empty($xprtmain)}
	{foreach from=$xprtmain item=xprtmainchart}
				<li><a href="#xprtsizechart_{$xprtmainchart.id_xprtsizechartmain}_{$hook}" role="tab" data-toggle="tab">{l s='Size chart' mod='xprtsizechart'}</a></li>
			{if isset($xprtmainchart.sizeguide) && !empty($xprtmainchart.sizeguide)}
				<li><a href="#xprtsizechartguide_{$xprtmainchart.id_xprtsizechartmain}_{$hook}" role="tab" data-toggle="tab">{l s='Size Guide' mod='xprtsizechart'}</a></li>
			{/if}
	{/foreach}
{/if}