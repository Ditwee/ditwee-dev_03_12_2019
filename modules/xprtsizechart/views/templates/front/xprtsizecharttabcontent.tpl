{if isset($xprtmain) && !empty($xprtmain)}
	{foreach from=$xprtmain item=xprtmainchart}
			<div class="tab-pane product_size_chart_table" id="xprtsizechart_{$xprtmainchart.id_xprtsizechartmain}_{$hook}">
				{include file="./sizechart.tpl"}
			</div>
		{if isset($xprtmainchart.sizeguide) && !empty($xprtmainchart.sizeguide)}
			<div class="tab-pane" id="xprtsizechartguide_{$xprtmainchart.id_xprtsizechartmain}_{$hook}">
				{$xprtmainchart.sizeguide}
			</div>
		{/if}
	{/foreach}
{/if}