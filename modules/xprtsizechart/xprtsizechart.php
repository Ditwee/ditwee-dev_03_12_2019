<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
if (!defined('_PS_VERSION_'))
	exit;
require_once (dirname(__FILE__) . '/classes/xprtsizechartoneclass.php');
require_once (dirname(__FILE__) . '/classes/xprtsizecharttwoclass.php');
require_once (dirname(__FILE__) . '/classes/xprtsizechartthreeclass.php');
require_once (dirname(__FILE__) . '/classes/xprtsizechartmainclass.php');
class xprtsizechart extends Module
{
    public $tabs_files_url = '/tabs/tabs.php';
    public $mysql_files_url = '/mysqlquery/mysqlquery.php';
	public function __construct()
	{
		$this->name = 'xprtsizechart';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Size Chart Modules');
		$this->description = $this->l('Great Store Size Chart Modules.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			// || !$this->registerHook('displayLeftColumnProduct')
			// || !$this->registerHook('displayRightColumnProduct')
			// || !$this->registerHook('displayProductButtons')
			|| !$this->registerHook('displayProductTab')
			|| !$this->registerHook('displayProductTabContent')
			// || !$this->registerHook('displayProductContent')
			// || !$this->registerHook('displayheader')
			|| !$this->Register_SQL()
			|| !$this->Register_Tabs()
			|| !$this->xpertsample_demo_all()
			|| !$this->xpertsampledata()
			)
			return false;
		else
			return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
			|| !$this->UnRegister_SQL()
			|| !$this->UnRegister_Tabs()
			)
			return false;
		else
			return true;
	}
	public function UnRegister_Tabs()
    {
        $tabs_lists = array();
        require_once(dirname(__FILE__) .$this->tabs_files_url);
        if(isset($tabs_lists) && !empty($tabs_lists)){
        	foreach($tabs_lists as $tab_list){
        	    $tab_list_id = Tab::getIdFromClassName($tab_list['class_name']);
        	    if(isset($tab_list_id) && !empty($tab_list_id)){
        	        $tabobj = new Tab($tab_list_id);
        	        $tabobj->delete();
        	    }
        	}
        } 
        $save_tab_id = (int)Tab::getIdFromClassName("Adminxprtchartdashboard");
        if($save_tab_id != 0){
        	$count = Tab::getNbTabs($save_tab_id);
        	if($count == 0){
        		if(isset($save_tab_id) && !empty($save_tab_id)){
        		    $tabobjs = new Tab($save_tab_id);
        		    $tabobjs->delete();
        		}
        	}
        }
        return true;
    }
    public function RegisterParentTabs(){

	    	$langs = Language::getLanguages();
	    	$save_tab_id = (int)Tab::getIdFromClassName("Adminxprtchartdashboard");
	    	if($save_tab_id != 0){
	    		return $save_tab_id;
	    	}else{
	    		$tab_listobj = new Tab();
	    		$tab_listobj->class_name = 'Adminxprtchartdashboard';
	    		$tab_listobj->id_parent = 0;
	    		$tab_listobj->module = $this->name;
	    		foreach($langs as $l)
	    		{
	    		    $tab_listobj->name[$l['id_lang']] = $this->l("Xpert SizeChart");
	    		}
	    		if($tab_listobj->save())
	    			return (int)$tab_listobj->id;
	    		else
	    			return (int)$save_tab_id;
	    	}
    }
    public function Register_Tabs()
    {
        $tabs_lists = array();
        $langs = Language::getLanguages();
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $save_tab_id = $this->RegisterParentTabs();
            require_once(dirname(__FILE__) .$this->tabs_files_url);
            if(isset($tabs_lists) && !empty($tabs_lists))
	            foreach ($tabs_lists as $tab_list)
	            {
	                $tab_listobj = new Tab();
	                $tab_listobj->class_name = $tab_list['class_name'];
	                if($tab_list['id_parent'] == 'parent'){
	                    $tab_listobj->id_parent = $save_tab_id;
	                }else{
	                    $tab_listobj->id_parent = $tab_list['id_parent'];
	                }
	                if(isset($tab_list['module']) && !empty($tab_list['module'])){
	                    $tab_listobj->module = $tab_list['module'];
	                }else{
	                    $tab_listobj->module = $this->name;
	                }
	                foreach($langs as $l)
	                {
	                    $tab_listobj->name[$l['id_lang']] = $this->l($tab_list['name']);
	                }
	                $tab_listobj->save();
	            }
        return true;
    }
    public function Register_SQL()
    {
        $mysqlquery = array();
            require_once(dirname(__FILE__).$this->mysql_files_url);
            if(isset($mysqlquery) && !empty($mysqlquery))
                foreach($mysqlquery as $query){
                    if(!Db::getInstance()->Execute($query,false))
                        return false;
                }
        return true;
    }
    public function UnRegister_SQL()
    {
        $mysqlquery_u = array();
            require_once(dirname(__FILE__).$this->mysql_files_url);
            if(isset($mysqlquery_u) && !empty($mysqlquery_u))
                foreach($mysqlquery_u as $query_u){
                    if(!Db::getInstance()->Execute($query_u,false))
                        return false;
                }
        return true;
    }
    public static function PageException($exceptions = NULL)
    {
    	if($exceptions == NULL)
    		return false;
    	$exceptions = explode(",",$exceptions);
    	$page_name = Context::getContext()->controller->php_self;
    	$this_arr = array();
    	if($page_name == 'product'){
    		$this_arr[] = 'all_page';
    		$id_product = Tools::getvalue('id_product');
    		$this_arr[] = 'prd_'.$id_product;
    		// Start Get Product Category
    		$prd_cat_sql = 'SELECT cp.`id_category` AS id
    		    FROM `'._DB_PREFIX_.'category_product` cp
    		    LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = cp.id_category)
    		    '.Shop::addSqlAssociation('category', 'c').'
    		    WHERE cp.`id_product` = '.(int)$id_product;
    		$prd_catresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_cat_sql);
    		if(isset($prd_catresults) && !empty($prd_catresults))
    		{
    		    foreach($prd_catresults as $prd_catresult)
    		    {
    		        $this_arr[] = 'prdcat_'.$prd_catresult['id'];
    		    }
    		}
    		// END Get Product Category
    		// Start Get Product Manufacturer
    		$prd_man_sql = 'SELECT `id_manufacturer` AS id FROM `'._DB_PREFIX_.'product` WHERE `id_product` = '.(int)$id_product;
    		$prd_manresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_man_sql);
    		if(isset($prd_manresults) && !empty($prd_manresults))
    		{
    		    foreach($prd_manresults as $prd_manresult)
    		    {
    		        $this_arr[] = 'prdman_'.$prd_manresult['id'];
    		    }
    		}
    		// END Get Product Manufacturer
    		// Start Get Product SupplierS
    		$prd_sup_sql = "SELECT `id_supplier` AS id FROM `"._DB_PREFIX_."product_supplier` WHERE `id_product` = ".(int)$id_product." GROUP BY `id_supplier`";
    		$prd_supresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_sup_sql);
    		if(isset($prd_supresults) && !empty($prd_supresults))
    		{
    		    foreach($prd_supresults as $prd_supresult)
    		    {
    		        $this_arr[] = 'prdsup_'.$prd_supresult['id'];
    		    }
    		}
    		// END Get Product SupplierS
    	}
    	if(isset($this_arr)){
    		foreach($this_arr as $this_arr_val){
    			if(in_array($this_arr_val,$exceptions))
    				return true;
    		}
    	}
    	return false;
    }
	public function SizeChartValues($hook = NULL){
		if($hook == NULL)
			return false;
		$ones = xprtsizechartoneclass::GetAllSizeChart();
  		$twos = xprtsizecharttwoclass::GetAllSizeChart();
  		$threes = xprtsizechartthreeclass::GetAllSizeChart();
  		$mains = xprtsizechartmainclass::GetAllSizeChart($hook);
  		$xprtimg_dir = _MODULE_DIR_.$this->name."/img/";
  		$finalmain = array();
  		if(isset($mains) && !empty($mains)){
  			foreach ($mains as $xval){
  				if(self::PageException($xval['productpage'])){
  					$xval['contentmain'] = Tools::jsonDecode($xval['contentmain']);
  					$finalmain[] = $xval;
  				}
  			}
			$this->smarty->assign(array(
	                'xprtone' => $ones,
	                'xprttwo' => $twos,
	                'xprtthree' => $threes,
	                'xprtmain' => $finalmain,
	                'hook' => $hook,
	                'xprtimg_dir' => $xprtimg_dir,
			));
  		}
	}
	public function hookdisplayLeftColumnProduct($params)
	{
		$this->SizeChartValues("displayLeftColumnProduct");
		return $this->display(__FILE__, 'views/templates/front/xprtsizechart.tpl');
	}
	public function hookdisplayRightColumnProduct($params)
	{
		$this->SizeChartValues("displayRightColumnProduct");
		return $this->display(__FILE__, 'views/templates/front/xprtsizechart.tpl');
	}
	public function hookdisplayProductButtons($params)
	{
		$this->SizeChartValues("displayProductButtons");
		return $this->display(__FILE__, 'views/templates/front/xprtsizechart.tpl');
	}
	public function hookdisplayProductTab($params)
	{
		$this->SizeChartValues("displayProductTabContent");
		return $this->display(__FILE__, 'views/templates/front/xprtsizecharttab.tpl');
	}
	public function hookdisplayProductTabContent($params)
	{
		$this->SizeChartValues("displayProductTabContent");
		return $this->display(__FILE__, 'views/templates/front/xprtsizecharttabcontent.tpl');
	}
	public function hookdisplayProductContent($params)
	{
		$this->SizeChartValues("displayProductContent");
		return $this->display(__FILE__, 'views/templates/front/xprtsizechart.tpl');
	}
	public function hookdisplayheader($params)
	{
		$this->context->controller->addCSS($this->_path.'css/xprtsizechart.css');
		$this->context->controller->addJS($this->_path.'js/xprtsizechart.js');
	}
	public function InsertDummyData($categories,$class){
		$languages = Language::getLanguages(false);
	    if(isset($categories) && !empty($categories)){
	        $classobj = new $class();
	        foreach($categories as $valu){
	        	if(isset($valu['lang']) && !empty($valu['lang'])){
	        		foreach ($valu['lang'] as $valukey => $value){
	        			foreach ($languages as $language){
	        				if(isset($valukey)){
	        					$classobj->{$valukey}[$language['id_lang']] = isset($value) ? $value : '';
	        				}
	        			}
	        		}
	        	}
        		if(isset($valu['notlang']) && !empty($valu['notlang'])){
        			foreach ($valu['notlang'] as $valukey => $value){
        				if(isset($valukey)){
        					$classobj->{$valukey} = $value;
        				}
        			}
        		}
	        	$classobj->add();
	        }
	    }
	       return true;
	}
	public function xpertsampledata($demo=NULL)
	{
        return true;
	}
	private function xpertsample_demo_all()
	{
		$xprtsizechartone = "";
		$xprtsizecharttwo = "";
		$xprtsizechartthree = "";
		$xprtsizechartmain = "";
		include_once(dirname(__FILE__).'/data/dummy_data.php');
		$this->InsertDummyData($xprtsizechartone,'xprtsizechartoneclass');
		$this->InsertDummyData($xprtsizecharttwo,'xprtsizecharttwoclass');
		$this->InsertDummyData($xprtsizechartthree,'xprtsizechartthreeclass');
		$this->InsertDummyData($xprtsizechartmain,'xprtsizechartmainclass');
		return true;
	}
}
