<?php
class xprthomesliderlayer extends ObjectModel
{			
	public $text;
	public $id_slide;
	public $in;
	public $out;
	public $delay;
	public $class;
	public $width;
	public $height;
	public $top;
	public $left;
	public $layer_type;
	public $layertext;
	public $margin;
	public $padding;
	public $line_height;
	public $font_family;
	public $font_weight;
	public $font_size;
	public $bg_color;
	public $bg_color_hover;
	public $text_color;
	public $text_color_hover;
	public $border_size;
	public $border_color;
	public $border_color_hover;
	public static $definition = array(
		'table' => 'xprthomesliderlayer',
		'primary' => 'id_xprthomesliderlayer',
		'multilang' => true,
		'fields' => array(
			'id_slide' =>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			'in' =>			array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'out' =>		array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'delay' =>		array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			'width' =>		array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			'height' =>		array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			'top' =>		array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			'left' =>		array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			'text' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'layer_type' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'class' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'margin' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'padding' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'line_height' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'font_family' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'font_weight' =>		array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'font_size' =>		array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'bg_color' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'bg_color_hover' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'text_color' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'text_color_hover' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'border_color' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'border_size' =>		array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'border_color_hover' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'layertext' =>	array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml','lang' => true),
		)
	);
}