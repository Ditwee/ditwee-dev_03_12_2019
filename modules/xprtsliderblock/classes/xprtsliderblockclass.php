<?php
class xprtsliderblockclass extends ObjectModel
{
	public $id;
	public $content;
	public $title;
	public $id_xprtsliderblock;
	public $position;
	public $hook;
	public $pages;
	public $active;
	public $sliderwidth;
	public $sliderheight;
	public $sliderpostion;
	public $sliderspeed;
	public $betweenslidedelay;
	public $sliderarrow;
	public $sliderbullet;
	public $sliderthumbnail;
	public $autoplay;
	public $lazyload;
	public $forcefullwidth;
	private static $module_name = 'xprtsliderblock';
	private static $tablename = 'xprtsliderblock';
	private static $classname = 'xprtsliderblockclass';
	public static $definition = array(
		'table' => 'xprtsliderblock',
		'primary' => 'id_xprtsliderblock',
		'multilang' => true,
		'fields' => array(
			'sliderwidth' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'sliderheight' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'sliderpostion' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'sliderspeed' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'betweenslidedelay' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'sliderarrow' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'sliderbullet' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'sliderthumbnail' =>	array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'hook' =>				array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'title' =>				array('type' => self::TYPE_STRING,'validate' => 'isString','lang' => true),
			'autoplay' =>			array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
			'lazyload' =>			array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
			'forcefullwidth' =>		array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
			'content' =>			array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'pages' =>				array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'position' =>			array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'active' =>				array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        Shop::addTableAssociation(self::$tablename, array('type' => 'shop'));
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = self::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.self::$tablename.'`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetSliderSBlock($hook = NULL)
    {
        $results = array();
      	$all_sliders = self::GetAllSliderSBlockS($hook);
      	if(isset($all_sliders) && !empty($all_sliders)){
      		foreach ($all_sliders as $all_sliderk => $all_slider){
      			if(xprtsliderblock::PageException($all_slider['pages'])){
      			 	foreach ($all_slider as $sldr_key => $sldr_val){
      			 		$results[$all_sliderk][$sldr_key] = $sldr_val;
						if($sldr_key == 'slideeffecteffect'){
							$effect_lists = explode(",",$sldr_val);
							if(isset($effect_lists) && !empty($effect_lists)){
								foreach($effect_lists as $effect) {
									$results[$all_sliderk]["slideeffectlists"][] = xprtslideeffectclass::GetEffectById($effect);
								}
							}
						}
      			 		if($sldr_key == 'id_'.self::$tablename){
      			 			$results[$all_sliderk]["all_slides"] = self::GetSlidesBySliderId($sldr_val);
      			 		}
      			 	}
      			}
      		}
      	}
      	return $results;
    }
    public static function GetEffectName($id_xprtslideseffect = null)
    {
    	if($id_xprtslideseffect == null)
    		return false;
        $sql = 'SELECT `title` FROM `'._DB_PREFIX_.'xprtslideseffect` WHERE id_xprtslideseffect = '.(int)$id_xprtslideseffect;
        $result = DB::getInstance()->getValue($sql);
        return $result;
    }
    public static function GetEffectEffect($id_xprtslideseffect = null)
    {
    	if($id_xprtslideseffect == null)
    		return false;
        $sql = 'SELECT `effect` FROM `'._DB_PREFIX_.'xprtslideseffect` WHERE id_xprtslideseffect = '.(int)$id_xprtslideseffect;
        $result = DB::getInstance()->getValue($sql);
        return $result;
    }
    public static function GetSlidesBySliderId($id_slider = NULL)
    {
		$results = array();
	    $id_lang = (int)Context::getContext()->language->id;
	    $id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtslides` pb 
	            INNER JOIN `'._DB_PREFIX_.'xprtslides_lang` pbl ON (pb.`id_xprtslides` = pbl.`id_xprtslides` AND pbl.`id_lang` = '.$id_lang.') ';
	    $sql .= ' WHERE pb.`active` = 1 ';
	    if($id_slider != NULL)
	        $sql .= ' AND pb.`id_slider` = "'.(int)$id_slider.'" ';
	    $sql .= ' ORDER BY pb.`position` DESC';
	    $results = Db::getInstance()->executeS($sql);
	    if(isset($results) && !empty($results) && is_array($results)){
	    	foreach ($results as &$result) {
	    		if(isset($result['content']) && !empty($result['content'])){
	    			$content = Tools::jsonDecode($result['content']);
	    			if(isset($content) && !empty($content)){
	    				foreach ($content as $content_key => $content_value) {
	    					if(isset($content_value) && !is_string($content_value) && is_object($content_value)){
	    						foreach ($content_value as $content_value_key => $content_value_value) {
	    							$result[$content_key][$content_value_key] = $content_value_value;
	    						}
	    					}else{
	    						$result[$content_key] = $content_value;
	    					}
	    				}
	    			}
	    		}
	    		if(isset($result['slideimage']) && !empty($result['slideimage'])) {
	    			$result['sizes'] = @getimagesize((_PS_MODULE_DIR_.self::$module_name.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$result['slideimage']));
	    			if (isset($result['sizes'][3]) && $result['sizes'][3])
						$result['size'] = $result['sizes'][3];
	    		}
	    		if(isset($result['id_xprtslides']) && !empty($result['id_xprtslides'])) {
	    			$result['all_layers'] = self::GetAllLayersBySlideId($result['id_xprtslides']);
	    		}
	    	}
	    }
	    return $results;
    }
    public static function GetAllLayersBySlideId($id_slide = NULL)
    {
    	$results = array();
	    $id_lang = (int)Context::getContext()->language->id;
	    $id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprthomesliderlayer` pb 
	            INNER JOIN `'._DB_PREFIX_.'xprthomesliderlayer_lang` pbl ON (pb.`id_xprthomesliderlayer` = pbl.`id_xprthomesliderlayer` AND pbl.`id_lang` = '.$id_lang.') ';
	    if($id_slide != NULL)
	        $sql .= ' AND pb.`id_slide` = "'.(int)$id_slide.'" ';
	    $results = Db::getInstance()->executeS($sql);
	    if(isset($results) && !empty($results) && is_array($results)){
	    	foreach ($results as &$result){
	    		if(isset($result['in']) && !empty($result['in'])){
	    			$result['in_lavel'] = self::GetEffectName($result['in']);
	    			$result['in_value'] = self::GetEffectEffect($result['in']);
	    		}
	    		if(isset($result['out']) && !empty($result['out'])){
	    			$result['out_lavel'] = self::GetEffectName($result['out']);
	    			$result['out_value'] = self::GetEffectEffect($result['out']);
	    		}
	    	}
	    }
	    return $results;
    }
    public static function GetAllSliderSBlockS($hook = NULL)
    {
    	$results = array();
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.self::$tablename.'` pb 
                INNER JOIN `'._DB_PREFIX_.self::$tablename.'_lang` pbl ON (pb.`id_'.self::$tablename.'` = pbl.`id_'.self::$tablename.'` AND pbl.`id_lang` = '.$id_lang.')
                INNER JOIN `'._DB_PREFIX_.self::$tablename.'_shop` pbs ON (pb.`id_'.self::$tablename.'` = pbs.`id_'.self::$tablename.'` AND pbs.`id_shop` = '.$id_shop.')
                ';
        $sql .= ' WHERE pb.`active` = 1 ';
        if($hook != NULL)
            $sql .= ' AND pb.`hook` = "'.$hook.'" ';
        $sql .= ' ORDER BY pb.`position` DESC';
        $results = Db::getInstance()->executeS($sql);
        if(isset($results) && !empty($results) && is_array($results)){
        	foreach ($results as &$result) {
        		if(isset($result['content']) && !empty($result['content'])){
        			$content = Tools::jsonDecode($result['content']);
        			if(isset($content) && !empty($content)){
        				foreach ($content as $content_key => $content_value) {
        					if(isset($content_value) && !is_string($content_value) && is_object($content_value)){
        						foreach ($content_value as $content_value_key => $content_value_value) {
        							$result[$content_key][$content_value_key] = $content_value_value;
        						}
        					}else{
        						$result[$content_key] = $content_value;
        					}
        				}
        			}
        		}
        	}
        }
        return $results;
    }
    // public static function AddMediaFile($layout,$css=true)
    // {
    //     if($css){
    //         $theme_media_path = _PS_THEME_DIR_."css/modules/".self::$module_name."/css/";
    //         $mod_media_path = _PS_MODULE_DIR_.self::$module_name."/css/";
    //         $postfix = ".css";
    //     }else{
    //         $theme_media_path = _PS_THEME_DIR_."js/modules/".self::$module_name."/js/";
    //         $mod_media_path = _PS_MODULE_DIR_.self::$module_name."/js/";
    //         $postfix = ".js";
    //     }
    //     if(file_exists($theme_media_path.$layout.$postfix)){
    //         if($css){
    //             Context::getContext()->controller->addCSS(_THEME_CSS_DIR_."modules/".self::$module_name."/css/".$layout.$postfix);
    //         }else{
    //             Context::getContext()->controller->addJS(_THEME_JS_DIR_."modules/".self::$module_name."/js/".$layout.$postfix);
    //         }
    //     }else{
    //         if(file_exists($mod_media_path.$layout.$postfix)){
    //             if($css){
    //                 Context::getContext()->controller->addCSS(_MODULE_DIR_.self::$module_name."/css/".$layout.$postfix);
    //             }else{
    //                 Context::getContext()->controller->addJS(_MODULE_DIR_.self::$module_name."/js/".$layout.$postfix);
    //             }
    //         }
    //     }
    // }
    public static function GetAllPrdExclude($orderby = 'id_product',$order = 'DESC')
    {
        $excludeprd = '';
        $excludeprds = '';
        if($orderby == 'id_product'){
            $order_init = ' p.`id_product` '.$order;
        }elseif($orderby == 'price'){
            $order_init = ' p.`price` '.$order;
        }elseif($orderby == 'date_add'){
            $order_init = ' p.`date_add` '.$order;
        }elseif($orderby == 'date_upd'){
            $order_init = ' p.`date_upd` '.$order;
        }else{
            $order_init = ' p.`id_product` DESC ';
        }
        $sql = 'SELECT p.`id_product` as id
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                WHERE pl.`id_lang` = '.(int)Context::getContext()->language->id.' ORDER BY '.$order_init;
        $allproducts =  Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if(isset($allproducts) && !empty($allproducts)){
            foreach($allproducts as $allprd){
                $excludeprd .= $allprd['id'].',';
            }
            if(isset($excludeprd) && !empty($excludeprd)){
                $excludeprds = substr($excludeprd,0,-1);
            }
        }
        return $excludeprds;
    }
    public static function GetProductsByID($ids = NULL)
    {
        if($ids == NULL)
            return false;
            $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity,
             pl.`description`, pl.`description_short`, product_attribute_shop.id_product_attribute, pl.`link_rewrite`,
              pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, image_shop.`id_image`, il.`legend`,
               m.`name` AS manufacturer_name FROM `' . _DB_PREFIX_ . 'product` p
            ' . Shop::addSqlAssociation('product', 'p') . ' LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute pa ON (pa.id_product = p.id_product) 
            ' . Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.default_on=1') . '
            ' . Product::sqlStock('p', 0, false) . ' LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON ( p.`id_product` = pl.`id_product` AND pl.`id_lang` = 
                ' . (int) Context::getContext()->language->id . Shop::addSqlRestrictionOnLang('pl') . '
            ) LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_product` = p.`id_product`)' . Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1') . 
            ' LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int) Context::getContext()->language->id . ')
            LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`) WHERE  p.`id_product` IN(' . $ids . ') AND product_shop.`active` = 1  
            AND product_shop.`show_price` = 1 AND ((image_shop.id_image IS NOT NULL OR i.id_image IS NULL) OR (image_shop.id_image IS NULL AND i.cover=1)) AND (pa.id_product_attribute IS NULL OR 
                product_attribute_shop.default_on = 1)';
            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
            return Product::getProductsProperties((int) Context::getContext()->language->id,$result);
    }
    public static function GetProductsByCatID($category_id,$limit=4, $id_lang = null, $id_shop = null, $child_count = false, $order_by = 'id_product', $order_way = "DESC")
    {
        $context = Context::getContext(); 
        $id_lang = is_null($id_lang) ? $context->language->id : $id_lang ;
        $id_shop = is_null($id_shop) ? $context->shop->id : $id_shop ;
        $id_supplier = '';
        $active = true;
        $front = true;
        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, MAX(product_attribute_shop.id_product_attribute) id_product_attribute, 
        product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`,
         pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, MAX(image_shop.`id_image`) id_image, il.`legend`, m.`name` AS manufacturer_name,
          cl.`name` AS category_default, DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
                    DAY)) > 0 AS new, product_shop.price AS orderprice FROM `'._DB_PREFIX_.'category_product` cp LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product` 
        '.Shop::addSqlAssociation('product', 'p').' LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product`)
            '.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
            '.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).' LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (product_shop.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').') LEFT JOIN `'._DB_PREFIX_.'image` i
                ON (i.`id_product` = p.`id_product`)'. Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').' LEFT JOIN `'._DB_PREFIX_.'image_lang` il
                ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.') LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
                ON m.`id_manufacturer` = p.`id_manufacturer` WHERE product_shop.`id_shop` = '.(int)$context->shop->id.' AND cp.`id_category` = '.(int)$category_id
                .($active ? ' AND product_shop.`active` = 1' : '') .($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '') .($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
                .' GROUP BY product_shop.id_product';
        if (empty($order_by) || $order_by == 'position') $order_by = 'price';
        if (empty($order_way)) $order_way = 'DESC';
        if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add'  || $order_by == 'date_upd')
                $order_by_prefix = 'p';
        else if ($order_by == 'name')
                $order_by_prefix = 'pl';
        $sql .= " ORDER BY {$order_by_prefix}.{$order_by} {$order_way}";
        $sql .= ' LIMIT '.$limit.' '; 
       $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);  
        return Product::getProductsProperties($id_lang,$result);
    }
    public static function GetBestSellProducts($limit = 10)
    {
        if(Configuration::get('PS_CATALOG_MODE'))
            return false;
        $context = Context::getcontext();
        if(!($result = ProductSale::getBestSalesLight((int)$context->language->id,0,$limit)))
            return false;
        $currency = new Currency((int)$context->cookie->id_currency);
        $usetax = Product::getTaxCalculationMethod();
        if(isset($result) && !empty($result)){
                foreach ($result as &$row){
                    $row['price'] = Tools::displayPrice(Product::getPriceStatic((int)$row['id_product'], $usetax), $currency);
                }
            return $result;
        }else
            return false;
    }
    public static function GetIndividualItem($items=NULL,$pref=NULL,$string=false)
    {
        if($pref == NULL)
            return false; 
        if($items == NULL)
            return false;  
        $results = array();
        $results_str = '';
        $items_arr = explode(",",$items);
        if(isset($items_arr) && !empty($items_arr)){
            foreach($items_arr as $item_ar){
                if(strpos($item_ar,$pref) !== false){
                    $results[] = str_replace($pref.'_',"",$item_ar);
                }
            }
            $results_str = implode(",",$results);
        }
        if($string == false)
            return $results;
        else
            return $results_str;
    }
    public static function GetProductProperties($product_type='new_product',$product_item=NULL,$limit=10,$order_by='id_product',$order_way='DESC')
    {
        $productproperties = array();
        $context = Context::getcontext();
            if($product_type == 'all_product'){
                $prd_ids = self::GetAllPrdExclude($order_by,$order_way);
                $productproperties = self::GetProductsByID($prd_ids);
            }elseif($product_type == 'featured_product'){
                $productproperties = self::GetProductsByCatID((int)Configuration::get('HOME_FEATURED_CAT'),(int)$limit,(int)$context->language->id, null, false, $order_by ,$order_way);
            }elseif($product_type == 'new_product'){
                $productproperties = Product::getNewProducts((int)$context->language->id, 0, (int)$limit,false,$order_by,$order_way);
            }elseif($product_type == 'best_product'){
                $productproperties = self::GetBestSellProducts((int)$limit);
            }elseif($product_type == 'specials_product'){
                $productproperties = Product::getPricesDrop((int)$context->language->id,0,(int)$limit,false,$order_by,$order_way);
            }elseif($product_type == 'category_product'){
                if(isset($product_item) && !empty($product_item)){
                    $category_arr = self::GetIndividualItem($product_item,"cat",false);
                    foreach($category_arr as $category_ar){
                        $cat_products = self::GetProductsByCatID($category_ar,(int)$limit,(int)$context->language->id, null, false, $order_by ,$order_way);
                        if(is_array($cat_products) && !empty($cat_products))
                            $productproperties = array_merge($productproperties,$cat_products);
                    }
                }
            }elseif($product_type == 'selected_product'){
                if(isset($product_item) && !empty($product_item)){
                    $prd_ids = self::GetIndividualItem($product_item,"prd",true);
                    $productproperties = self::GetProductsByID($prd_ids);
                }
            }elseif($product_type == 'manufacturer_product'){
                if(isset($product_item) && !empty($product_item)){
                    $brand_ids_arr = self::GetIndividualItem($product_item,"man",false);
                    foreach($brand_ids_arr as $brand_ids_ar){
                        $manufacturer = new Manufacturer((int)$brand_ids_ar, $context->language->id);
                        $man_products = $manufacturer->getProducts($brand_ids_ar, $context->language->id,1,(int)$limit, $order_by, $order_way);
                        if(is_array($man_products) && !empty($man_products))
                            $productproperties = array_merge($productproperties,$man_products);
                    }
                }
            }elseif($product_type == 'supplier_product'){
                if(isset($product_item) && !empty($product_item)){
                    $supplier_ids_arr = self::GetIndividualItem($product_item,"sup",false);
                    foreach($supplier_ids_arr as $supplier_ids_ar){
                        $sup_products = Supplier::getProducts($supplier_ids_ar, $context->language->id,1, $limit, $order_by, $order_way, false);
                        if(is_array($sup_products) && !empty($sup_products))
                            $productproperties = array_merge($productproperties,$sup_products);
                    }
                }
            }
        return $productproperties;
    }
}