<?php
class xprtslidesclass extends ObjectModel
{
	public $id;
	public $id_xprtslides;
	public $content;
	public $layered;
	public $title;
	public $slideurl;
	public $slidebgtype;
	public $slideimage;
	public $slidethumbnail;
	public $slidebgcolor;
	public $design_style;
	public $ineffect;
	public $outeffect;
	public $customclass;
	public $id_slider;
	public $position;
	public $active;
	public static $definition = array(
		'table' => 'xprtslides',
		'primary' => 'id_xprtslides',
		'multilang' => true,
		'fields' => array(
			'title' =>				array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			'content' =>			array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'layered' =>			array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml','lang'=>true),
			'slideurl' =>			array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			'slidebgtype' =>		array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			'slideimage' =>			array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			'slidethumbnail' =>		array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			'slidebgcolor' =>		array('type' => self::TYPE_STRING,'validate' => 'isString','lang'=>true),
			'design_style' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'customclass' =>		array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'ineffect' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'outeffect' =>			array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'id_slider' =>			array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'position' =>			array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'active' =>				array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
                parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = self::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtslides`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
	public function reOrderPositions()
	{
		$id_slide = $this->id;
		$context = Context::getContext();
		$id_shop = $context->shop->id;
		$max = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT MAX(`position`) as position FROM `'._DB_PREFIX_.'xprtslides` ');
		if ((int)$max == (int)$id_slide)
			return true;
		$rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('SELECT xs.`position` as position, xs.`id_xprtslides` as id_xprtslides FROM `'._DB_PREFIX_.'xprtslides` xs WHERE xs.`position` > '.(int)$this->position
		);
		foreach ($rows as $row)
		{
			$current_slide = new xprtslidesclass($row['id_xprtslides']);
			--$current_slide->position;
			$current_slide->update();
			unset($current_slide);
		}
		return true;
	}
}