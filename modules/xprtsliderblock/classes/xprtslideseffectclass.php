<?php
class xprtslideseffectclass extends ObjectModel
{
	public $id;
	public $id_xprtslideseffect;
	public $title;
	public $effect;
	public $position;
	public $active;
	public static $definition = array(
		'table' => 'xprtslideseffect',
		'primary' => 'id_xprtslideseffect',
		'multilang' => false,
		'fields' => array(
			'title' =>				array('type' => self::TYPE_STRING,'validate' => 'isString'),
			'effect' =>				array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
			'position' =>			array('type' => self::TYPE_INT,'validate' => 'isunsignedInt'),
			'active' =>				array('type' => self::TYPE_BOOL,'validate' => 'isBool'),
		)
	);
    public function add($autodate = true, $null_values = false)
    {
        if ($this->position <= 0)
            $this->position = self::getHigherPosition() + 1;
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getHigherPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtslideseffect`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public static function GetEffects($formated = true)
    {
    	$values = array();
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtslideseffect` WHERE active = 1 ';
        $results = DB::getInstance()->executeS($sql);
        if($formated == true){
        	if(isset($results) && !empty($results)){
        		$i = 0;
        		foreach ($results as $result) {
        			$values[$i]['id'] = $result['id_xprtslideseffect'];
        			$values[$i]['name'] = $result['title'];
        			$i++;
        		}
        	}
        }else{
        	$values = $results;
        }
        return $values;
    }
}