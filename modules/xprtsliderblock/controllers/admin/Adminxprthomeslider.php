<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
/**
 * @since 1.6.0
 */
class AdminxprthomesliderController extends ModuleAdminController
{
	private static $module_name = 'xprtsliderblock';
	public function ajaxProcessuploadlayerimage()
	{
		$results = array();
		if(isset($_FILES['imagedata']) && !empty($_FILES['imagedata'])){
			$results['imagedata'] = $this->processImage('imagedata');
			$results['imagelink'] = _MODULE_DIR_.self::$module_name.'/images/';
		}else{
			$results['imagedata'] = '';
			$results['imagelink'] = _MODULE_DIR_.self::$module_name.'/images/';
		}
		die(Tools::jsonEncode($results));
	}
	public function ajaxProcessEdittextLayer()
	{
		$results = array();
		$layerid = (int)Tools::getvalue("layerid");
		if($layerid != 0){
			$xprthomesliderlayer = new xprthomesliderlayer($layerid);
			// print '<pre>';
			// print_r($xprthomesliderlayer);
			// print '</pre>';
			if(isset($xprthomesliderlayer) && !empty($xprthomesliderlayer)){
				foreach ($xprthomesliderlayer as $layerkey => $layervalue){
					$results[$layerkey] = $layervalue;
				}
			}
			die(Tools::jsonEncode($results));
		}else{
			$results['errors'] = "Something Wrong"; 
			die(Tools::jsonEncode($results));
		}
	}
	public function ajaxProcessDeletetextLayer()
	{
		$layerid = (int)Tools::getvalue("layerid");
		if($layerid != 0){
			$xprthomesliderlayer = new xprthomesliderlayer($layerid);
			if(!$xprthomesliderlayer->delete()){
				$results['errors'] = "Something Wrong"; 
				die(Tools::jsonEncode($results));
			}
		}else{
			$results['errors'] = "Something Wrong"; 
			die(Tools::jsonEncode($results));
		}
		die(1);
	}
	public function ajaxProcessAddtextLayer()
	{
		$results = array(); 
		$id_xprthomesliderlayer = (int)Tools::getvalue("id_xprthomesliderlayer");
		if($id_xprthomesliderlayer != 0){
			$xprthomesliderlayer = new xprthomesliderlayer($id_xprthomesliderlayer);
		}else{
			$xprthomesliderlayer = new xprthomesliderlayer();
		}
		$xprthomesliderlayer->in = Tools::getvalue("in");
		$xprthomesliderlayer->out = Tools::getvalue("out");
		$xprthomesliderlayer->delay = Tools::getvalue("delay");
		$xprthomesliderlayer->class = Tools::getvalue("class");
		$xprthomesliderlayer->width = (int)Tools::getvalue("width");
		$xprthomesliderlayer->height = (int)Tools::getvalue("height");
		$xprthomesliderlayer->top = (int)Tools::getvalue("top");
		$xprthomesliderlayer->left = (int)Tools::getvalue("left");
		$xprthomesliderlayer->margin =  Tools::getvalue("margin");
		$xprthomesliderlayer->padding =  Tools::getvalue("padding");
		$xprthomesliderlayer->line_height =  Tools::getvalue("line_height");
		$xprthomesliderlayer->font_family =  Tools::getvalue("font_family");
		$xprthomesliderlayer->font_weight =  Tools::getvalue("font_weight");
		$xprthomesliderlayer->font_size =  Tools::getvalue("font_size");
		$xprthomesliderlayer->bg_color =  Tools::getvalue("bg_color");
		$xprthomesliderlayer->bg_color_hover =  Tools::getvalue("bg_color_hover");
		$xprthomesliderlayer->text_color =  Tools::getvalue("text_color");
		$xprthomesliderlayer->text_color_hover =  Tools::getvalue("text_color_hover");
		$xprthomesliderlayer->border_color =  Tools::getvalue("border_color");
		$xprthomesliderlayer->border_size =  Tools::getvalue("border_size");
		$xprthomesliderlayer->border_color_hover =  Tools::getvalue("border_color_hover");
		$xprthomesliderlayer->id_slide = (int)Tools::getvalue("id_slide");
		$xprthomesliderlayer->text = Tools::getvalue("text");
		$xprthomesliderlayer->layer_type = Tools::getvalue("layer_type");
		$languages = Language::getLanguages(false);
		foreach ($languages as $language)
		{
			$xprthomesliderlayer->layertext[$language['id_lang']] = Tools::getvalue("layertext_".$language['id_lang']);
		}
		if($xprthomesliderlayer->save()){
			$results['id_xprthomesliderlayer'] = $xprthomesliderlayer->id; 
		}else{
			$results['errors'] = "Something Wrong"; 
		}
		die(Tools::jsonEncode($results));
	}
	public function ProcessLayer()
	{

	}
	public function processImage($file_name = null){
		if($file_name == null)
			return false;
	    if(isset($_FILES[$file_name]) && isset($_FILES[$file_name]['tmp_name']) && !empty($_FILES[$file_name]['tmp_name'])){
            $ext = substr($_FILES[$file_name]['name'], strrpos($_FILES[$file_name]['name'], '.') + 1);
            $basename_file_name = basename($_FILES[$file_name]["name"]);
            $strlen = strlen($basename_file_name);
            $strlen_ext = strlen($ext);
            $basename_file_name = substr($basename_file_name,0,($strlen-$strlen_ext));
            $link_rewrite_file_name = Tools::link_rewrite($basename_file_name);
            $file_orgname = $link_rewrite_file_name.'.'.$ext;
            $path = _PS_MODULE_DIR_.self::$module_name.'/images/' . $file_orgname;
            if(!move_uploaded_file($_FILES[$file_name]['tmp_name'],$path))
                return false;         
            else
                return $file_orgname;   
	    }else{
	    	return false;
	    }
	}
}
