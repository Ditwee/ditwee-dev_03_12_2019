<?php
$xprtslideeffect = array(
    array(
	    'notlang' => array(
		    'title' => 'Fade Twins',
		    'effect' => '{$Duration:700,$Opacity:2,$Brother:{$Duration:1000,$Opacity:2}}',
		    'position' => 0,
		    'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
		    'title' => 'Shift LR',
		    'effect' => '{$Duration:1200,x:1,$Easing:{$Left:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2,$Brother:{$Duration:1200,x:-1,$Easing:{$Left:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2}}',
		    'position' => 1,
		    'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
		    'title' => 'Return LR',
		    'effect' => '{$Duration:1200,x:1,$Delay:40,$Cols:6,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:{$Left:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2,$ZIndex:-10,$Brother:{$Duration:1200,x:1,$Delay:40,$Cols:6,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:{$Top:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2,$ZIndex:-10,$Shift:-100}}',
		    'position' => 2,
		    'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
		    'title' => 'Fade Clip in H',
		    'effect' => '{$Duration:1200,$Delay:20,$Clip:3,$Easing:{$Clip:$JssorEasing$.$EaseInCubic,$Opacity:$JssorEasing$.$EaseLinear},$Assembly:260,$Opacity:2}',
		    'position' => 3,
		    'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
		    'title' => 'Fade Stairs',
		    'effect' => '{$Duration:800,$Delay:30,$Cols:8,$Rows:4,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2050,$Opacity:2}',
		    'position' => 4,
		    'active' => 1,
		),
    ),
);


$xprtslideseffect = array(
    array(
	    'notlang' => array(
	    	'title' => 'Left',
	    	'effect' => '{$Duration:900,x:0.6,$Easing:{$Left:$JssorEasing$.$EaseInOutSine},$Opacity:2}',
	    	'position' => 0,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Right',
	    	'effect' => '{$Duration:900,x:-0.6,$Easing:{$Left:$JssorEasing$.$EaseInOutSine},$Opacity:2}',
	    	'position' => 1,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Top',
	    	'effect' => '{$Duration:900,y:0.6,$Easing:{$Top:$JssorEasing$.$EaseInOutSine},$Opacity:2}',
	    	'position' => 2,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Bounce Left',
	    	'effect' => '{$Duration:1200,x:0.6,$Easing:{$Left:$JssorEasing$.$EaseInOutBack},$Opacity:2}',
	    	'position' => 3,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Bounce right',
	    	'effect' => '{$Duration:1200,x:-0.6,$Easing:{$Left:$JssorEasing$.$EaseInOutBack},$Opacity:2}',
	    	'position' => 4,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Bounce Top',
	    	'effect' => '{$Duration:1200,y:0.6,$Easing:{$Top:$JssorEasing$.$EaseInOutBack},$Opacity:2}',
	    	'position' => 5,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Fade',
	    	'effect' => '{$Duration:900,$Opacity:2}',
	    	'position' => 6,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Move and clip left',
	    	'effect' => '{$Duration:1200,$Clip:1,$Move:true,$Opacity:1.7,$During:{$Clip:[0.5,0.5],$Opacity:[0,0.5]}}',
	    	'position' => 6,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Zoom',
	    	'effect' => '{$Duration:900,$Zoom:4,$Easing:{$Zoom:$JssorEasing$.$EaseInExpo,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2}',
	    	'position' => 7,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Rotate from top',
	    	'effect' => '{$Duration:900,y:0.6,$Zoom:1,$Rotate:1,$Easing:{$Top:$JssorEasing$.$EaseInQuad,$Zoom:$JssorEasing$.$EaseInQuad,$Rotate:$JssorEasing$.$EaseInQuad,$Opacity:$JssorEasing$.$EaseOutQuad},$Opacity:2,$Round:{$Rotate:1.2}}',
	    	'position' => 8,
	    	'active' => 1,
		),
    ),
    array(
	    'notlang' => array(
	    	'title' => 'Rotate',
	    	'effect' => '{$Duration:900,$Rotate:1,$Easing:{$Opacity:$JssorEasing$.$EaseLinear,$Rotate:$JssorEasing$.$EaseInQuad},$Opacity:2,$Round:{$Rotate:0.5}}',
	    	'position' => 9,
	    	'active' => 1,
		),
    ),
);

$xprthomesliderlayer = array(
		array(
		    'notlang' => array
		    (
		            // 'id_xprthomesliderlayer' => 2
		            'text' => '', 
		            'id_slide' => 1,
		            'in' => '6',
		            'out' => '6',
		            'delay' => '200',
		            'class' => '', 
		            'width' => '525',
		            'height' => '525',
		            'top' => '167',
		            'left' => '1095',
		            'margin' => '', 
		            'padding' => '0px 0px 0px 0px',
		            'line_height' => '1',
		            'font_family' => 'Roboto',
		            'font_weight' => '400',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => '1'
		    ),
		    'lang' => array
		    (
		        'layertext' => '<p class="caption1">fashionfor men</p>
		         <h3 class="caption2"><strong>30%</strong>off</h3>
		        <p class="caption3">winter collection</p>',
		    )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 3
		            'text' => '', 
		            'id_slide' => '2',
		            'in' => '6',
		            'out' => '6',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '525',
		            'height' => '525',
		            'top' => '167',
		            'left' => '400',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'Roboto',
		            'font_weight' => '400',
		            'font_size' => '75',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1,
		        ),
		    'lang' => array
		        (
		            'layertext' =>  '<p class="caption1">style for men</p>
		             <h3 class="caption2"><strong>big</strong>sale</h3>
		            <p class="caption3">winter collection</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 4
		            'text' => '', 
		            'id_slide' => '3',
		            'in' => '6',
		            'out' => '6',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '525',
		            'height' => '525',
		            'top' => '268',
		            'left' => '980',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => 'Roboto',
		            'font_weight' => '400',
		            'font_size' => '45',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' =>  '<p class="caption1">fashion for men</p>
		             <h3 class="caption2"><strong>30%</strong>off</h3>
		            <p class="caption3">winter collection</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 5
		            'text' => '', 
		            'id_slide' => '4',
		            'in' => '6',
		            'out' => '6',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '200',
		            'height' => '50',
		            'top' => '377',
		            'left' => '893',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#ffffff',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption1 tt_uppercase">Specail</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 6
		            'text' => '', 
		            'id_slide' => '4',
		            'in' => '6',
		            'out' => '6',
		            'delay' => '200',
		            'class' => '', 
		            'width' => '850',
		            'height' => '100',
		            'top' => '410',
		            'left' => '550',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '100',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#ffffff',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' =>  '<h3 class="caption2 tt_uppercase">big sale for season</h3>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 7
		            'text' => '', 
		            'id_slide' => '4',
		            'in' => '6',
		            'out' => '6',
		            'delay' => '300',
		            'class' => '', 
		            'width' => '510',
		            'height' => '50',
		            'top' => '512',
		            'left' => '715',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption3 tt_uppercase">new collection for women</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 8
		            'text' => '', 
		            'id_slide' => '4',
		            'in' => '6',
		            'out' => '6',
		            'delay' => '400',
		            'class' => '', 
		            'width' => '130',
		            'height' => '66',
		            'top' => '560',
		            'left' => '900',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => '',
		            'font_weight' => '',
		            'font_size' => '',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p><a href="#" class="btn btn-default">Shop Now</a></p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 9
		            'text' => '', 
		            'id_slide' => '5',
		            'in' => '8',
		            'out' => '8',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '200',
		            'height' => '50',
		            'top' => '377',
		            'left' => '893',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#ffffff',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption1 tt_uppercase">Specail</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 10
		            'text' => '', 
		            'id_slide' => '5',
		            'in' => '8',
		            'out' => '8',
		            'delay' => '200',
		            'class' => '', 
		            'width' => '850',
		            'height' => '100',
		            'top' => '410',
		            'left' => '550',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '100',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<h3 class="caption2 tt_uppercase">big sale for season</h3>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 11
		            'text' => '', 
		            'id_slide' => 5,
		            'in' => '8',
		            'out' => '8',
		            'delay' => '300',
		            'class' => '', 
		            'width' => '510',
		            'height' => '50',
		            'top' => '512',
		            'left' => '715',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption3 tt_uppercase">new collection for women</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 12
		            'text' => '', 
		            'id_slide' => 5,
		            'in' => '8',
		            'out' => '8',
		            'delay' => '400',
		            'class' => '', 
		            'width' => '130',
		            'height' => '66',
		            'top' => '560',
		            'left' => '900',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p><a href="#" class="btn btn-default">Shop Now</a></p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 13
		            'text' => '', 
		            'id_slide' => 6,
		            'in' => '9',
		            'out' => '9',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '525',
		            'height' => '525',
		            'top' => '268',
		            'left' => '980',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => 'Roboto',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' =>  '<p class="caption1">fashionfor men</p>
		             <h3 class="caption2"><strong>30%</strong>off</h3>
		            <p class="caption3">winter collection</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 14
		            'text' => '', 
		            'id_slide' => 7,
		            'in' => '10',
		            'out' => '10',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '200',
		            'height' => '50',
		            'top' => '377',
		            'left' => '893',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#ffffff',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption1 tt_uppercase">Specail</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 15
		            'text' => '', 
		            'id_slide' => 7,
		            'in' => '10',
		            'out' => '10',
		            'delay' => '200',
		            'class' => '', 
		            'width' => '850',
		            'height' => '100',
		            'top' => '410',
		            'left' => '550',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '100',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<h3 class="caption2 tt_uppercase">big sale for season</h3>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 16
		            'text' => '', 
		            'id_slide' => 7,
		            'in' => '10',
		            'out' => '10',
		            'delay' => '300',
		            'class' => '', 
		            'width' => '510',
		            'height' => '50',
		            'top' => '512',
		            'left' => '715',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption3 tt_uppercase">new collection for women</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 17
		            'text' => '', 
		            'id_slide' => 7,
		            'in' => '10',
		            'out' => '10',
		            'delay' => '400',
		            'class' => '', 
		            'width' => '130',
		            'height' => '66',
		            'top' => '560',
		            'left' => '900',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p><a href="#" class="btn btn-default">Shop Now</a></p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 18
		            'text' => '', 
		            'id_slide' => 8,
		            'in' => '6',
		            'out' => '10',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '200',
		            'height' => '50',
		            'top' => '377',
		            'left' => '893',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#ffffff',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption1 tt_uppercase">Specail</p>',
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 19
		            'text' => '', 
		            'id_slide' => '8',
		            'in' => '6',
		            'out' => '6',
		            'delay' => '200',
		            'class' => '', 
		            'width' => '850',
		            'height' => '100',
		            'top' => '410',
		            'left' => '550',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '100',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<h3 class="caption2 tt_uppercase">big sale for season</h3>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 20
		            'text' => '', 
		            'id_slide' => '8',
		            'in' => '6',
		            'out' => '6',
		            'delay' => '300',
		            'class' => '', 
		            'width' => '510',
		            'height' => '50',
		            'top' => '512',
		            'left' => '715',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption3 tt_uppercase">new collection for women</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 21
		            'text' => '', 
		            'id_slide' => 8,
		            'in' =>'6',
		            'out' => '6',
		            'delay' => '400',
		            'class' => '', 
		            'width' => '130',
		            'height' => '66',
		            'top' => '560',
		            'left' => '900',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),

		    'lang' => array
		        (
		            'layertext' => '<p><a href="#" class="btn btn-default">Shop Now</a></p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 22
		            'text' => '', 
		            'id_slide' => 9,
		            'in' => '9',
		            'out' => '9',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '525',
		            'height' => '525',
		            'top' => '168',
		            'left' => '300',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '24',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption1">fashionfor men</p>
		             <h3 class="caption2"><strong>30%</strong>off</h3>
		            <p class="caption3">winter collection</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 23
		            'text' => '', 
		            'id_slide' => 11,
		            'in' => '6',
		            'out' => '6',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '600',
		            'height' => '150',
		            'top' => '204',
		            'left' => '410',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1.1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '60',
		            'bg_color' => '',
		            'bg_color_hover' => '', 
		            'text_color' => '#3d3d3d',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<div class="caption1 tt_uppercase t_align_c">introducing<br />shoes fashion ct60</div>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 24
		            'text' => '', 
		            'id_slide' => 11,
		            'in' => '6',
		            'out' => '6',
		            'delay' => '200',
		            'class' => '', 
		            'width' => '428',
		            'height' => '50',
		            'top' => '377',
		            'left' => '473',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1.5',
		            'font_family' => 'Roboto',
		            'font_weight' => '400',
		            'font_size' => '16',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#6b6b6b',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption2">Inspired by New York. Crafted in Switzerland.<br />From the inventors of the New York Minute.</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 25
		            'text' => '', 
		            'id_slide' => '11',
		            'in' => '6',
		            'out' => '6',
		            'delay' => '300',
		            'class' => '', 
		            'width' => '0',
		            'height' => '0',
		            'top' => '460',
		            'left' => '580',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '400',
		            'font_size' => '18',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption3"><a href="#">Discover The Collection</a></p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 26
		            'text' => '', 
		            'id_slide' => 10,
		            'in' => '5',
		            'out' => '5',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '600',
		            'height' => '150',
		            'top' => '204',
		            'left' => '910',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1.1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '60',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#3d3d3d',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '',
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<div class="caption1 tt_uppercase t_align_c">introducing<br />new fashion ct60</div>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 27
		            'text' => '', 
		            'id_slide' => 10,
		            'in' => '5',
		            'out' => '5',
		            'delay' => '300',
		            'class' => '', 
		            'width' => '428',
		            'height' => '50',
		            'top' => '377',
		            'left' => '973',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1.5',
		            'font_family' => 'Roboto',
		            'font_weight' => '400',
		            'font_size' => '16',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption2">Inspired by New York. Crafted in Switzerland.<br />From the inventors of the New York Minute.</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 28
		            'text' => '', 
		            'id_slide' => 10,
		            'in' => '5',
		            'out' => '5',
		            'delay' => '500',
		            'class' => '', 
		            'width' => '200',
		            'height' => '40',
		            'top' => '454',
		            'left' => '1089',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1',
		            'font_family' => 'BenchNine',
		            'font_weight' => '400',
		            'font_size' => '18',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption3"><a href="#">Discover The Collection</a></p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 29 // new 32
		            'text' => '', 
		            'id_slide' => 19,
		            'in' => '6',
		            'out' => '6',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '378',
		            'height' => '96',
		            'top' => '98',
		            'left' => '70',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1.2',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '35',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#3d3d3d',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<div class="caption1 tt_uppercase t_align_c">introducing<br />shoes fashion ct60</div>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 30 // new 33
		            'text' => '', 
		            'id_slide' => 19,
		            'in' => '6',
		            'out' => '6',
		            'delay' => '200',
		            'class' => '', 
		            'width' => '378',
		            'height' => '50',
		            'top' => '200',
		            'left' => '71',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => 'Roboto',
		            'font_weight' => '400',
		            'font_size' => '14',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#383838',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption2">Inspired by New York. Crafted in Switzerland.<br />From the inventors of the New York Minute.</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 31 // new 34
		            'text' => '', 
		            'id_slide' => 18,
		            'in' => '5',
		            'out' => '5',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '378',
		            'height' => '96',
		            'top' => '98',
		            'left' => '450',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1.2',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '35',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#3d3d3d',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<div class="caption1 tt_uppercase t_align_c">introducing<br />shoes fashion ct60</div>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 32 // new 36
		            'text' => '', 
		            'id_slide' => 18,
		            'in' => '5',
		            'out' => '5',
		            'delay' => '200',
		            'class' => '', 
		            'width' => '378',
		            'height' => '50',
		            'top' => '200',
		            'left' => '456',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => 'Roboto',
		            'font_weight' => '400',
		            'font_size' => '14',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#3d3d3d',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption2">Inspired by New York. Crafted in Switzerland.<br />From the inventors of the New York Minute.</p>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 33 // new 30
		            'text' => '', 
		            'id_slide' => 17,
		            'in' => '6',
		            'out' => '6',
		            'delay' => '100',
		            'class' => '', 
		            'width' => '378',
		            'height' => '96',
		            'top' => '98',
		            'left' => '70',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '1.2',
		            'font_family' => 'BenchNine',
		            'font_weight' => '700',
		            'font_size' => '35',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#3d3d3d',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<div class="caption1 tt_uppercase t_align_c">introducing<br />shoes fashion ct60</div>',
		        )
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprthomesliderlayer' => 34 // new 31
		            'text' => '', 
		            'id_slide' => 17,
		            'in' => '6',
		            'out' => '6',
		            'delay' => '200',
		            'class' => '', 
		            'width' => '378',
		            'height' => '50',
		            'top' => '200',
		            'left' => '71',
		            'margin' => '', 
		            'padding' => '', 
		            'line_height' => '',
		            'font_family' => 'Roboto',
		            'font_weight' => '400',
		            'font_size' => '14',
		            'bg_color' => '', 
		            'bg_color_hover' => '', 
		            'text_color' => '#383838',
		            'text_color_hover' => '', 
		            'border_size' => '0',
		            'border_color' => '', 
		            'border_color_hover' => '', 
		            'layer_type' => 'text',
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'layertext' => '<p class="caption2">Inspired by New York. Crafted in Switzerland.<br />From the inventors of the New York Minute.</p>',
		        )
		),
	);

$xprtslidesclass = array(
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 1
		            'design_style' => 'layer_box_style',
		            'id_slider' => 1,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slide-1',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x682.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),

		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 2
		            'design_style' => 'layer_box_style',
		            'id_slider' => 1,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 1,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),

		    'lang' => array
		        (
		            'title' => '',
		            'layered' => '', 
		            'slideurl' => '#', 
		            'slidebgtype' => '', 
		            'slideimage' => '1920x682.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),

		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 3
		            'design_style' => 'layer_box_style_transparent',
		            'id_slider' => 2,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),

		    'lang' => array
		        (
		            'title' => 'Slide title 3',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        )

		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 4
		            'design_style' => 'layer_style_normal',
		            'id_slider' => 2,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 2,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),

		    'lang' => array
		        (
		            'title' => 'Slide 2',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        )

		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 5
		            'design_style' => 'layer_style_normal',
		            'id_slider' => 2,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 1,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'slide-4',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 6
		            'design_style' => 'layer_box_style_transparent',
		            'id_slider' => 3,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 3,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),

		    'lang' => array
		        (
		            'title' => 'slide-1',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),

		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 7
		            'design_style' => 'layer_style_normal',
		            'id_slider' => 3,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 4,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),

		    'lang' => array
		        (
		            'title' => 'slide 2',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),

		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 8
		            'design_style' => 'layer_style_normal',
		            'id_slider' => 3,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 5,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),

		    'lang' => array
		        (
		            'title' => 'slide 3',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),

		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 9
		            'design_style' => 'layer_box_style_transparent',
		            'id_slider' => 4,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 2,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),

		    'lang' => array
		        (
		            'title' => 'Slider 1',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x682.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        )

		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 10
		            'design_style' => 'layer_style_two',
		            'id_slider' => 4,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 1,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),

		    'lang' => array
		        (
		            'title' => 'Slider 2',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x682.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        )

		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 11
		            'design_style' => 'layer_style_two',
		            'id_slider' => 4,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slider 3',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x682.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 12
		            'design_style' => 'layer_box_style_transparent',
		            'id_slider' => 5,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slide-1',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 13
		            'design_style' => 'layer_box_style',
		            'id_slider' => 5,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slide-2',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 14
		            'design_style' => 'layer_box_style',
		            'id_slider' => 5,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slide-3',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 15
		            'design_style' => 'layer_style_normal',
		            'id_slider' => 6,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slide-4',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 16
		            'design_style' => 'layer_style_normal',
		            'id_slider' => 6,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slide-5',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '1920x980.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 17
		            'design_style' => 'layer_style_two',
		            'id_slider' => 7,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slide-1',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '870x380.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 18
		            'design_style' => 'layer_style_two',
		            'id_slider' => 7,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slide-2',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '870x380.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
		array
		(
		    'notlang' => array
		        (
		            // 'id_xprtslides' => 19
		            'design_style' => 'layer_style_two',
		            'id_slider' => 7,
		            'customclass' => '', 
		            'ineffect' => '', 
		            'outeffect' => '', 
		            'content' => '', 
		            'position' => 0,
		            'active' => 1,
		            // 'id_lang' => 1
		        ),
		    'lang' => array
		        (
		            'title' => 'Slide-3',
		            'layered' => '', 
		            'slideurl' => '#',
		            'slidebgtype' => '', 
		            'slideimage' => '870x380.jpg',
		            'slidethumbnail' => '', 
		            'slidebgcolor' => '', 
		        ),
		),
	);

$xprtsliderblockclass = array(
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtsliderblock' => 1,
	            'sliderwidth' => '1920',
	            'sliderheight' => '683',
	            'sliderpostion' => '0px 0px 60px 0px',
	            'sliderspeed' => '1000',
	            'sliderarrow' => 'default',
	            'sliderbullet' => 'default',
	            'sliderthumbnail' => '',
	            'betweenslidedelay' => '5000',
	            'autoplay' => 1,
	            'lazyload' => 0,
	            'forcefullwidth' => 0,
	            'hook' => 'displayTopColumn',
	            'pages' => 'index',
	            'content' => '{"slideeffecteffect":"1,3,5,4,2"}',
	            'active' => 0,
	            'position' => 0,
	            'truck_identify' => '',
	            // 'id_lang' => 1,
	        ),
	    'lang' => array
	        (
	            'title' => 'Slider 1',
	        )
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtsliderblock' => 2
	            'sliderwidth' => '1920',
	            'sliderheight' => '979',
	            'sliderpostion' => '0px 0px 60px 0px',
	            'sliderspeed' => '1000',
	            'sliderarrow' => 'default',
	            'sliderbullet' => 'default',
	            'sliderthumbnail' => '',
	            'betweenslidedelay' => '5000',
	            'autoplay' => 0,
	            'lazyload' => 0,
	            'forcefullwidth' => 0,
	            'hook' => 'displayTopColumn',
	            'pages' => 'index',
	            'content' => '{"slideeffecteffect":"1,3,4,5"}',
	            'active' => 1,
	            'position' => 1,
	            'truck_identify' => '',
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Slider for demo-2',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtsliderblock' => 3
	            'sliderwidth' => '1920',
	            'sliderheight' => '979',
	            'sliderpostion' => '0px 0px 0px 0px',
	            'sliderspeed' => '1000',
	            'sliderarrow' => 'default',
	            'sliderbullet' => 'default',
	            'sliderthumbnail' => '',
	            'betweenslidedelay' => '5000',
	            'autoplay' => 1,
	            'lazyload' => 1,
	            'forcefullwidth' => 0,
	            'hook' => 'displayTopColumn',
	            'pages' => 'all_page',
	            'content' => '{"slideeffecteffect":"1,3,2,5,4"}',
	            'active' => 0,
	            'position' => 2,
	            'truck_identify' => '',
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Slider for demo-3',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtsliderblock' => 4
	            'sliderwidth' => '1920',
	            'sliderheight' => '682',
	            'sliderpostion' => '0px 0px 40px 0px',
	            'sliderspeed' => '1000',
	            'sliderarrow' => 'default',
	            'sliderbullet' => 'default',
	            'sliderthumbnail' => '',
	            'betweenslidedelay' => '5000',
	            'autoplay' => 0,
	            'lazyload' => 0,
	            'forcefullwidth' => 0,
	            'hook' => 'displayTopColumn',
	            'pages' => 'all_page',
	            'content' => '{"slideeffecteffect":"1,3,4,5,2"}',
	            'active' => 0,
	            'position' => 3,
	            'truck_identify' => '',
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Slider for demo-4',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtsliderblock' => 5
	            'sliderwidth' => '1920',
	            'sliderheight' => '892',
	            'sliderpostion' => '0px 0px 60px 0px',
	            'sliderspeed' => '1000',
	            'sliderarrow' => 'default',
	            'sliderbullet' => 'default',
	            'sliderthumbnail' => '',
	            'betweenslidedelay' => '5000',
	            'autoplay' => 1,
	            'lazyload' => 1,
	            'forcefullwidth' => 0,
	            'hook' => 'displayTopColumn',
	            'pages' => 'index',
	            'content' => '{"slideeffecteffect":"1,3,4,2,5"}',
	            'active' => 0,
	            'position' => 4,
	            'truck_identify' => '',
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Home -7',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtsliderblock' => 6
	            'sliderwidth' => '1920',
	            'sliderheight' => '737',
	            'sliderpostion' => '0px 0px 60px 0px',
	            'sliderspeed' => '1000',
	            'sliderarrow' => 'default',
	            'sliderbullet' => 'default',
	            'sliderthumbnail' => '',
	            'betweenslidedelay' => '5000',
	            'autoplay' => 0,
	            'lazyload' => 0,
	            'forcefullwidth' => 0,
	            'hook' => 'displayTopColumn',
	            'pages' => 'all_page',
	            'content' => '{"slideeffecteffect":"1,2,4,5,3"}',
	            'active' => 0,
	            'position' => 5,
	            'truck_identify' => '',
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'demo-9',
	        ),
	),
	array
	(
	    'notlang' => array
	        (
	            // 'id_xprtsliderblock' => 7
	            'sliderwidth' => '870',
	            'sliderheight' => '380',
	            'sliderpostion' => '0px 0px 30px 0px',
	            'sliderspeed' => '1000',
	            'sliderarrow' => 'default',
	            'sliderbullet' => 'default',
	            'sliderthumbnail' => '',
	            'betweenslidedelay' => '5000',
	            'autoplay' => 1,
	            'lazyload' => 0,
	            'forcefullwidth' => 0,
	            'hook' => 'displayHomeTopRight',
	            'pages' => 'index',
	            'content' => '{"slideeffecteffect":"1,3,5,4,2"}',
	            'active' => 0,
	            'position' => 5,
	            'truck_identify' => '',
	            // 'id_lang' => 1
	        ),
	    'lang' => array
	        (
	            'title' => 'Slider 1',
	        ),
	),
);

