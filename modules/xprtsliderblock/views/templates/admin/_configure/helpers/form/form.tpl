{extends file="helpers/form/form.tpl"}
{block name="field"}
	{if $input.type == 'device'}
		<div class="col-lg-{if isset($input.col)}{$input.col|intval}{else}9{/if}{if !isset($input.label)} col-lg-offset-3{/if}">
			{if isset($input.device_desc) && !empty($input.device_desc)}
				{foreach $input.device_desc AS $dev}
					<div class="col-xs-4 col-sm-3">
						<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="{if isset($dev.tooltip)}{$dev.tooltip}{/if}">{if isset($dev.title)}{$dev.title}{/if}</label>
						{$devicess = "{$input.name}_{$dev.name}"}
						<select name="{$input.name}_{$dev.name}" id="{$input.name}_{$dev.name}" class="fixed-width-md">
							{if isset($dev.column) && !empty($dev.column)}
			        			{foreach $dev.column AS $column}
			                        <option value="{$column}" {if isset($fields_value[$input['name']]->{$devicess}) && ($fields_value[$input['name']]->{$devicess} == $column)} selected="selected"  {/if} >{$column}</option>
			                    {/foreach}
		                    {/if}
	        			</select>
	        			{$devicess = ""}
					</div>
				{/foreach}
			{/if}
		</div>
	{elseif $input.type == 'file_lang'}
		<div class="row">
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="translatable-field lang-{$language.id_lang}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
				{/if}
					<div class="col-lg-6">
						{if isset($fields[0]['form'][{$input.name}])}
						<img src="{$image_baseurl}{$fields[0]['form'][{$input.name}][$language.id_lang]}" class="img-thumbnail" />
						{/if}
						<div class="dummyfile input-group">
							<input id="{$input.name}_{$language.id_lang}" type="file" name="{$input.name}_{$language.id_lang}" class="hide-file-upload" />
							<span class="input-group-addon"><i class="icon-file"></i></span>
							<input id="{$input.name}_{$language.id_lang}-name" type="text" class="disabled" name="filename" readonly />
							<span class="input-group-btn">
								<button id="{$input.name}_{$language.id_lang}-selectbutton" type="button" name="submitAddAttachments" class="btn btn-default">
									<i class="icon-folder-open"></i> {l s='Choose a file' mod='atelier_homeslider'}
								</button>
							</span>
						</div>
					</div>
				{if $languages|count > 1}
					<div class="col-lg-2">
						<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
							{$language.iso_code}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							{foreach from=$languages item=lang}
							<li><a href="javascript:hideOtherLanguage({$lang.id_lang});" tabindex="-1">{$lang.name}</a></li>
							{/foreach}
						</ul>
					</div>
				{/if}
				{if $languages|count > 1}
					</div>
				{/if}
				<script>
				$(document).ready(function(){
					$('#{$input.name}_{$language.id_lang}-selectbutton').click(function(e){
						$('#{$input.name}_{$language.id_lang}').trigger('click');
					});
					$('#{$input.name}_{$language.id_lang}').change(function(e){
						var val = $(this).val();
						var file = val.split(/[\\/]/);
						$('#{$input.name}_{$language.id_lang}-name').val(file[file.length-1]);
					});
				});
			</script>
			{/foreach}
		</div>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
{block name="description"}
{if !isset($input.expandstyle_class)}{$input.expandstyle_class = true}{/if}
{if !isset($input.expandstyle_margin)}{$input.expandstyle_margin = true}{/if}
{if !isset($input.expandstyle_padding)}{$input.expandstyle_padding = true}{/if}
{if (isset($input.expandstyle) && $input.expandstyle == true) || isset($input.desc) && !empty($input.desc)}
		<div class="help-block-container">
			{if isset($input.expandstyle) && $input.expandstyle == true}
					<a class="click-to-put-cls clk-put-cls-{$input.name}" href="#">Click Here To Use Expand Style {if isset($input.label) && !empty($input.label)} In {$input.label} {/if}</a>
					<div class="col-xs-12 col-sm-12 classnamecontainerclass">
						{if $input.expandstyle_class}
							<div class="col-xs-4 col-sm-3">
								<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="Please Enter a custom class for custom design">Custom CSS Class</label>
								<input type="text" class="fixed-width-md classnameclass put-cls-{$input.name}" id="put-cls-{$input.name}" name="put-cls-{$input.name}" value="{if isset($fields_value["put-cls-{$input.name}"])}{$fields_value["put-cls-{$input.name}"]}{/if}">
							</div>
						{/if}
						{if $input.expandstyle_margin}
						<div class="col-xs-4 col-sm-3">
							<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="top right bottom left">Custom Margin</label>
							<input type="text" class="fixed-width-md classnamemar put-mar-{$input.name}" id="put-mar-{$input.name}" name="put-mar-{$input.name}" value="{if isset($fields_value["put-mar-{$input.name}"])}{$fields_value["put-mar-{$input.name}"]}{/if}">
						</div>
						{/if}
						{if $input.expandstyle_padding}
						<div class="col-xs-4 col-sm-3">
							<label data-html="true" data-toggle="tooltip" class="label-tooltip" data-original-title="top right bottom left">Custom Padding</label>
							<input type="text" class="fixed-width-md classnamepad put-pad-{$input.name}" id="put-pad-{$input.name}" name="put-pad-{$input.name}" value="{if isset($fields_value["put-pad-{$input.name}"])}{$fields_value["put-pad-{$input.name}"]}{/if}">
						</div>
						{/if}
					</div>
			{/if}
			{if isset($input.type) && $input.type == "file"}
				{if isset($fields_value[$input.name]) && !empty($fields_value[$input.name])}
					<img src="{$image_baseurl}{$fields_value[$input.name]}" height="200px" width="auto"/>
				{/if}
			{/if}
			{$smarty.block.parent}
		</div>
	{/if}
{/block}
{block name="script"}
		$(document).ready(function(){
			$(".classnamecontainerclass").hide();
			$(".click-to-put-cls").on("click",function(){
				$(this).parent().find(".classnamecontainerclass").toggle(500);
			});
		});
{/block}
{block name="input"}
	{if ($input.type == 'selecttwotype')}
			<div class="{if isset($input.hideclass)}{$input.hideclass}{/if} {$input.name} {$input.name}_class" id="{$input.name}_id">
			<select name="selecttwotype_{$input.name}" class="selecttwotype_{$input.name}_cls" id="selecttwotype_{$input.name}_id" multiple="true">
			    {foreach from=$input.initvalues item=initval}
			        {if isset($fields_value[$input.name])}
			            {assign var=settings_def_value value=","|explode:$fields_value[$input.name]}
			            {if $initval['id']|in_array:$settings_def_value}
			                {$selected = 'selected'}
			            {else}
			                {$selected = ''}
			            {/if}
			        {else}
			            {$selected = ''}
			        {/if}
			        <option {$selected} value="{$initval['id']}">{$initval['name']}</option>
			    {/foreach}
			</select>
			<input type="hidden" name="{$input.name}" id="{$input.name}" value="{if isset($input.defvalues)}{$input.defvalues}{else}{$fields_value[$input.name]}{/if}" class=" {$input.name} {$input.type}_field">
			</div>
			<script type="text/javascript">
			    // START SELECT TWO CALLING
			    $(function(){
			        var defVal = $("input#{$input.name}").val();
			        if(defVal.length){
			            var ValArr = defVal.split(',');
			            for(var n in ValArr){
			                $( "select#selecttwotype_{$input.name}_id" ).children('option[value="'+ValArr[n]+'"]').attr('selected','selected');
			            }
			        }
			        $( "select#selecttwotype_{$input.name}_id" ).select2( { placeholder: "{$input.placeholder}", width: 200, tokenSeparators: [',', ' '] } ).on('change',function(){
			            var data = $(this).select2('data');
			            var select = $(this);
			            var field = select.next("input#{$input.name}");
			            var saved = '';
			            select.children('option').attr('selected',null);
			            if(data.length)
			                $.each(data, function(k,v){
			                    var selected = v.id;   
			                    select.children('option[value="'+selected+'"]').attr('selected','selected');
			                    if(k > 0)
			                        saved += ',';
			                    saved += selected;                                
			                });
			             field.val(saved);   
			        });
			    });
 			// END SELECT TWO CALLING
			</script>
			<style type="text/css">
				.select2-container.select2-container-multi
				{ 
					width: 100% !important;
				}
			</style>
	{elseif $input.type == 'textlayer'}
		{* start text multiple *}
		{if $slidepagetype == 'edit'}
		<div class="text_{$input.name}_multiple_parent">
			{* start  layer *}
			<div class="row {$input.name}_class_parent">
				<div class="col-sm-9">
					<div class="col-sm-12">
					{* start layer text *}
						{foreach $languages as $language}
								{if $languages|count > 1}
									<div class="form-group translatable-field lang-{$language.id_lang}"{if $language.id_lang != $defaultFormLanguage} style="display:none;"{/if}>
										<div class="col-sm-10">
								{/if}
									<textarea rows="10" cols="10" name="textin_{$input.name}_layertext_{$language.id_lang}" id="textin_{$input.name}_layertext_{$language.id_lang}" class="textin_{$input.name}_layertext_{$language.id_lang}" placeholder="Enter Layer text"></textarea>
								{if $languages|count > 1}
										</div>
										<div class="col-sm-2">
											<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
												{$language.iso_code}
												<span class="caret"></span>
											</button>
											<ul class="dropdown-menu">
												{foreach from=$languages item=language}
												<li>
													<a href="javascript:hideOtherLanguage({$language.id_lang});" tabindex="-1">{$language.name}</a>
												</li>
												{/foreach}
											</ul>
										</div>
									</div>
								{/if}
						{/foreach}
					{* end layer text *}
					</div>
					<div class="col-sm-3">
						In Iffect
						<select class="textin_{$input.name}_in" id="textin_{$input.name}_in" name="textin_{$input.name}_in">
						<option value="NO">No</option>
							{if isset($xprtalleffects) && !empty($xprtalleffects)}
								{foreach from=$xprtalleffects item=xprtalleffect}
									<option value="{$xprtalleffect.id}">{$xprtalleffect.name}</option>
								{/foreach}
							{/if}
						</select>
					</div>
					<div class="col-sm-3">
						Out Iffect
						<select class="textin_{$input.name}_out" id="textin_{$input.name}_out" name="textin_{$input.name}_out">
									<option value="NO">No</option>
							{if isset($xprtalleffects) && !empty($xprtalleffects)}
								{foreach from=$xprtalleffects item=xprtalleffect}
									<option value="{$xprtalleffect.id}">{$xprtalleffect.name}</option>
								{/foreach}
							{/if}
						</select>
					</div>
					<div class="col-sm-3">
						Delay
						<input type="number" class="textin_{$input.name}_delay" name="textin_{$input.name}_delay" id="textin_{$input.name}_delay" value="">
					</div>
					<div class="col-sm-3">
						Class
						<input type="text" class="textin_{$input.name}_class" name="textin_{$input.name}_class" id="textin_{$input.name}_class" value="">
					</div>
					<div class="col-sm-3">
						Width
						<input type="number" class="textin_{$input.name}_width" name="textin_{$input.name}_width" id="textin_{$input.name}_width" value="">
					</div>
					<div class="col-sm-3">
						Height
						<input type="number" class="textin_{$input.name}_height" name="textin_{$input.name}_height" id="textin_{$input.name}_height" value="">
					</div>
					<div class="col-sm-3">
					Top Pos
					<input type="number" class="textin_{$input.name}_top" name="textin_{$input.name}_top" id="textin_{$input.name}_top" value="">
					</div>
					<div class="col-sm-3">
					Left Pos
					<input type="number" class="textin_{$input.name}_left" name="textin_{$input.name}_left" id="textin_{$input.name}_left" value="">
					</div>
					{* <div class="col-sm-3">
					Margin
					<input type="text" class="textin_{$input.name}_margin" name="textin_{$input.name}_margin" id="textin_{$input.name}_margin" value="">
					</div> *}
					<div class="col-sm-6">
					Padding (top right bottom left)
					<input type="text" class="textin_{$input.name}_padding" name="textin_{$input.name}_padding" id="textin_{$input.name}_padding" value="">
					<input type="hidden" class="textin_{$input.name}_layer_type" name="textin_{$input.name}_layer_type" id="textin_{$input.name}_layer_type" value="text">
					<input type="hidden" class="textin_{$input.name}_text" name="textin_{$input.name}_text" id="textin_{$input.name}_text" value="">
					</div>
					<div class="col-sm-3">
					Border Size
					<input type="number" class="textin_{$input.name}_border_size" name="textin_{$input.name}_border_size" id="textin_{$input.name}_border_size" value="">
					</div>
					<div class="col-sm-3">
					Line Height
					<input type="number" class="textin_{$input.name}_line_height" name="textin_{$input.name}_line_height" id="textin_{$input.name}_line_height" value="">
					</div>
					<div class="col-sm-4">
					Font Family
						<select name="textin_{$input.name}_font_family" id="textin_{$input.name}_font_family" class="textin_{$input.name}_font_family">
							{if isset($gets_fonts_family) && !empty($gets_fonts_family)}
								{foreach $gets_fonts_family as $gets_font_family}
									<option value="{$gets_font_family.name}">{$gets_font_family.name}</option>
								{/foreach}
							{/if}
						</select>
					</div>
					<div class="col-sm-4">
					Font Weight
						<input type="text" class="textin_{$input.name}_font_weight" name="textin_{$input.name}_font_weight" id="textin_{$input.name}_font_weight" value="">
					</div>
					<div class="col-sm-4">
					Font Size
						<input type="number" class="textin_{$input.name}_font_size" name="textin_{$input.name}_font_size" id="textin_{$input.name}_font_size" value="">
					</div>
					<div class="col-sm-2">
					BG Color
						<input type="color" data-hex="true" class="color mColorPickerInput textin_{$input.name}_bg_color {if isset($input.class)} {$input.class} {/if}" name="textin_{$input.name}_bg_color" value="transparent" id="textin_{$input.name}_bg_color"/>
					</div>
					<div class="col-sm-2">
					BG  Hover Color
						<input type="color" data-hex="true" class="color mColorPickerInput textin_{$input.name}_bg_color_hover {if isset($input.class)} {$input.class} {/if}" name="textin_{$input.name}_bg_color_hover" value="transparent"  id="textin_{$input.name}_bg_color_hover"/>
					</div>
					<div class="col-sm-2">
					Text Color
						<input type="color" data-hex="true" class="color mColorPickerInput textin_{$input.name}_text_color {if isset($input.class)} {$input.class} {/if}" name="textin_{$input.name}_text_color" value="#000000"  id="textin_{$input.name}_text_color"/>
					</div>
					<div class="col-sm-2">
					Text Hover Color
						<input type="color" data-hex="true" class="color mColorPickerInput textin_{$input.name}_text_color_hover {if isset($input.class)} {$input.class} {/if}" name="textin_{$input.name}_text_color_hover" value="#000000"  id="textin_{$input.name}_text_color_hover"/>
					</div>
					<div class="col-sm-2">
					Border Color
						<input type="color" data-hex="true" class="color mColorPickerInput textin_{$input.name}_border_color {if isset($input.class)} {$input.class} {/if}" name="textin_{$input.name}_border_color" value="transparent"  id="textin_{$input.name}_border_color"/>
					</div>
					<div class="col-sm-2">
					Border Hover
						<input type="color" data-hex="true" class="color mColorPickerInput textin_{$input.name}_border_color_hover {if isset($input.class)} {$input.class} {/if}" name="textin_{$input.name}_border_color_hover" value="transparent"  id="textin_{$input.name}_border_color_hover"/>
					</div>
				</div>
				<div class="col-sm-3">
					
				</div>
				<div class="row">
					<div class="col-sm-3">
						
					</div>
					<div class="existstextlayers col-sm-9 text_{$input.name}_alllayers m_top_10">
						<p class="clearfix"><button type="button" class="btn btn-default pull-right text_{$input.name}_addclass"> Add New</button></p>
						<div class="m_top_30"></div>
						{if isset($text_layers) && !empty($text_layers)}
							{foreach $text_layers AS $text_layer}
								<div class="form-control-static text_{$input.name}_viewclass text_{$input.name}_view_{$text_layer.id_xprthomesliderlayer} m_bottom_5" style="background-color:#F5F8F9;margin-bottom:20px;border: 1px solid #C7D6DB;padding: 20px;">
								* <strong>Text layer : </strong> {$text_layer.layertext} <button type="button" data-layerid="{$text_layer.id_xprthomesliderlayer}" class="text_{$input.name}_editclass btn btn-default"><i class="icon-pencil text-danger"></i></button>  <button type="button" data-layerid="{$text_layer.id_xprthomesliderlayer}" class="text_{$input.name}_deleteclass btn btn-default"><i class="icon-remove text-danger"></i></button> 
								</div>
							{/foreach}
						{/if}
					</div>
					<div class="col-sm-3">
						
					</div>
				</div>
			</div>
			{* End Layer *}
				<script>
					function reset_text_layer(){
						$('[id^="textin_{$input.name}_"]').each(function()
						{
							$(this).val("");
						});
						$("#textin_{$input.name}_layer_type").val("text");
					}
					$(".text_{$input.name}_alllayers").on("click",".text_{$input.name}_editclass",function() { 
							$("#textin_{$input.name}_id_xprthomesliderlayer").remove();
							var layer_text = new Object();
							var layerid = $(this).data("layerid");
							layer_text['layerid'] = layerid;
							$.ajax({
								url: "{$controllers_link}&action=edittextlayer&ajax=1",
								data: layer_text,
								type:'post',
								dataType: 'json',
								success: function(data){
									$.each(data,function( li, lval ) {
									  if($.inArray(li,["id_slide","layertext","id","id_shop_list","force_id"]) != -1){
											if(li == 'layertext'){
												$.each(lval,function( lkey,lkeyval ) {
														$("#textin_{$input.name}_"+li+"_"+lkey).val(lkeyval);
												});
											}
											if(li == 'id'){
													$(".text_{$input.name}_alllayers").after('<input type="hidden" class="textin_{$input.name}_id_xprthomesliderlayer" name="textin_{$input.name}_id_xprthomesliderlayer" id="textin_{$input.name}_id_xprthomesliderlayer" value="'+lval+'">');
											}
									  }else{
									  	$("#textin_{$input.name}_"+li).val(lval);
									  }
								});
									$(".text_{$input.name}_cancleclass").remove();
									$(".text_{$input.name}_addclass").after('<button type="button" class="btn btn-default pull-right text_{$input.name}_cancleclass"> Cancle</button>');
									$(".text_{$input.name}_addclass").text('Update');
								},
								error: function(data){
									$.growl.error({ title: "Error", message: "Something Wrong!"});
								},
							});
					 } );
					$(".text_{$input.name}_alllayers").on("click",".text_{$input.name}_cancleclass",function() { 
						$("#textin_{$input.name}_id_xprthomesliderlayer").remove();
						$(this).remove();
						reset_text_layer();	
						$(".text_{$input.name}_addclass").text('Add New');
					});
					$(".text_{$input.name}_alllayers").on("click",".text_{$input.name}_deleteclass",function() { 
							var layer_text = new Object();
							var layerid = $(this).data("layerid");
							layer_text['layerid'] = layerid;
							$.ajax({
								url: "{$controllers_link}&action=deletetextlayer&ajax=1",
								data: layer_text,
								type:'post',
								dataType: 'json',
								success: function(data){
									$(".text_{$input.name}_view_"+layerid).remove();
								},
								error: function(data){
									$.growl.error({ title: "Error", message: "Something Wrong!"});
								},
							});
							$(".text_{$input.name}_cancleclass").remove();
							$("#textin_{$input.name}_id_xprthomesliderlayer").remove();
							$(".text_{$input.name}_addclass").text('Add New');
							reset_text_layer();
					 } ); 
					$(".text_{$input.name}_addclass").on("click",function() { 
						var layer_text = new Object();
						$('[id^="textin_{$input.name}_"]').each(function()
						{
							id = $(this).prop("id").replace("textin_{$input.name}_", "");
							layer_text[id] = $(this).val();
						});

							layer_text['id_slide'] = "{$id_xprtslides}";
							var samplelayertext = $("#textin_{$input.name}_layertext_{$defaultFormLanguage}").val();

						$.ajax({
								url: "{$controllers_link}&action=addtextlayer&ajax=1",
								data: layer_text,
								type:'post',
								dataType: 'json',
								success: function(data){
									if (typeof data.id_xprthomesliderlayer !== 'undefined'){
									if (typeof layer_text['id_xprthomesliderlayer'] == 'undefined'){
										$(".text_{$input.name}_addclass").after('<div class="form-control-static text_{$input.name}_viewclass text_{$input.name}_view_'+data.id_xprthomesliderlayer+'">* <strong>Text layer : </strong> '+samplelayertext+' <button type="button" data-layerid="'+data.id_xprthomesliderlayer+'" class="text_{$input.name}_editclass btn btn-default"><i class="icon-pencil text-danger"></i></button>  <button type="button" data-layerid="'+data.id_xprthomesliderlayer+'" class="text_{$input.name}_deleteclass btn btn-default"><i class="icon-remove text-danger"></i></button> </div>');
									}										reset_text_layer();	
										$("#textin_{$input.name}_id_xprthomesliderlayer").remove();
										$(".text_{$input.name}_cancleclass").remove();
										$(".text_{$input.name}_addclass").text('Add New');
									}
									if (typeof data.errors !== 'undefined'){
										$.growl.error({ title: "Error", message: data.errors});
									}
								},
								error: function(data){
									$.growl.error({ title: "Error", message: "Something Wrong!"});
								},
							});	
						
					 } );
				</script>
		</div>
		{* end text multiple *}
		<style type="text/css">
			.form-group.imglayercontainerclass {
			    background-color: #eee;
			    border: 1px solid #bbb;
			}
			input[type="number"]{
				display: block;
				width: 100%;
				height: 31px;
				padding: 6px 8px;
				font-size: 12px;
				line-height: 1.42857;
				color: #555;
				background-color: #F5F8F9;
				background-image: none;
				border: 1px solid #C7D6DB;
				border-radius: 3px;
				-webkit-transition: border-color ease-in-out 0.15s,box-shadow ease-in-out 0.15s;
				-o-transition: border-color ease-in-out 0.15s,box-shadow ease-in-out 0.15s;
				transition: border-color ease-in-out 0.15s,box-shadow ease-in-out 0.15s;
			}
			.form-group.imglayercontainerclass {
			    padding: 15px 0px;
			}
		</style>
		{else}
			<div class="alert alert-warning">
					There is 1 warning.
				<ul style="display:block;" id="seeMore">
					<li>You must save this product before adding layers.</li>
				</ul>
			</div>
		{/if}
	{* start image layer *}
		{elseif $input.type == 'imagelayer'}
			{* start text multiple *}
					{if $slidepagetype == 'edit'}
					<div class="img_{$input.name}_multiple_parent">
						{* start  layer *}
						<div class="row {$input.name}_class_parent">
							<div class="col-sm-9">
								<div class="col-sm-12 m_bottom_5">
									<p>Upload a image</p>
									<p class="input-group-btn" style="padding-bottom:5px;">
										<input type="file" name="imgin_{$input.name}_text_temp" id="imgin_{$input.name}_text_temp" class="imgin_{$input.name}_text_temp btn btn-default">
									</p>
									<input type="hidden" name="imgin_{$input.name}_text" id="imgin_{$input.name}_text" class="imgin_{$input.name}_text">
									<div class="imgin_{$input.name}_text_show" id="imgin_{$input.name}_text_show"></div>
									<div name="imgin_{$input.name}_upload" id="imgin_{$input.name}_upload" class="imgin_{$input.name}_upload btn btn-default m_top_5"><i class="icon-folder-open"></i> Upload</div>
								</div>
								<div class="col-sm-3">
									In Iffect
									<select class="imgin_{$input.name}_in" id="imgin_{$input.name}_in" name="imgin_{$input.name}_in">
										<option value="NO">No</option>
										{if isset($xprtalleffects) && !empty($xprtalleffects)}
											{foreach from=$xprtalleffects item=xprtalleffect}
												<option value="{$xprtalleffect.id}">{$xprtalleffect.name}</option>
											{/foreach}
										{/if}
									</select>
								</div>
								<div class="col-sm-3">
									Out Iffect
									<select class="imgin_{$input.name}_out" id="imgin_{$input.name}_out" name="imgin_{$input.name}_out">
										<option value="NO">No</option>
										{if isset($xprtalleffects) && !empty($xprtalleffects)}
											{foreach from=$xprtalleffects item=xprtalleffect}
												<option value="{$xprtalleffect.id}">{$xprtalleffect.name}</option>
											{/foreach}
										{/if}
									</select>
								</div>
								<div class="col-sm-3">
									Delay
									<input type="number" class="imgin_{$input.name}_delay" name="imgin_{$input.name}_delay" id="imgin_{$input.name}_delay" value="">
								</div>
								<div class="col-sm-3">
									Class
									<input type="text" class="imgin_{$input.name}_class" name="imgin_{$input.name}_class" id="imgin_{$input.name}_class" value="">
								</div>
								<div class="col-sm-3">
									Width
									<input type="number" class="imgin_{$input.name}_width" name="imgin_{$input.name}_width" id="imgin_{$input.name}_width" value="">
								</div>
								<div class="col-sm-3">
									Height
									<input type="number" class="imgin_{$input.name}_height" name="imgin_{$input.name}_height" id="imgin_{$input.name}_height" value="">
								</div>
								<div class="col-sm-3">
								Top Pos
								<input type="number" class="imgin_{$input.name}_top" name="imgin_{$input.name}_top" id="imgin_{$input.name}_top" value="">
								</div>
								<div class="col-sm-3">
								Left Pos
								<input type="number" class="imgin_{$input.name}_left" name="imgin_{$input.name}_left" id="imgin_{$input.name}_left" value="">
								<input type="hidden" class="imgin_{$input.name}_layer_type" name="imgin_{$input.name}_layer_type" id="imgin_{$input.name}_layer_type" value="image">
								</div>
							</div>
							<div class="col-sm-3">
								
							</div>
							<div class="row">
								<div class="col-sm-3">
									
								</div>
								<div class="existstextlayers col-sm-9 img_{$input.name}_alllayers m_top_5">
									<button type="button" class="btn btn-default pull-right img_{$input.name}_addclass"> Add New</button>
									{if isset($image_layers) && !empty($image_layers)}
										{foreach $image_layers AS $img_layer}
											<div class="form-control-static img_{$input.name}_viewclass img_{$input.name}_view_{$img_layer.id_xprthomesliderlayer}">
											* <strong>Image layer : </strong> <img src="{$xprtimagelink}{$img_layer.text}" width="35" height="auto"> <button type="button" data-layerid="{$img_layer.id_xprthomesliderlayer}" class="img_{$input.name}_editclass btn btn-default"><i class="icon-pencil text-danger"></i></button>  <button type="button" data-layerid="{$img_layer.id_xprthomesliderlayer}" class="img_{$input.name}_deleteclass btn btn-default"><i class="icon-remove text-danger"></i></button> 
											</div>
										{/foreach}
									{/if}
								</div>
								<div class="col-sm-3">
									
								</div>
							</div>
						</div>
						{* End Layer *}
							<script>
							// imgin_{$input.name}_text_temp
								function reset_image_layer(){
									$('[id^="imgin_{$input.name}_"]').each(function()
									{
										$(this).val("");
									});
									$(".imgin_{$input.name}_img_show").remove();
									$("#imgin_{$input.name}_layer_type").val("image");
								}
								$(".imglayercontainerclass").on("click",".imgin_{$input.name}_upload",function() { 
									var file_data = $("#imgin_{$input.name}_text_temp").prop("files")[0]; 
									  var form_data = new FormData(); 
									  form_data.append("imagedata", file_data);
									  $.ajax({
									    url: "{$controllers_link}&action=uploadlayerimage&ajax=1",
									    dataType: 'json',
									    cache: false,
									    contentType: false,
									    processData: false,
									    data: form_data, 
									    type: 'post',
									    success: function(data) {
											if(data.imagedata != ''){
												$("#imgin_{$input.name}_text").val(data.imagedata);
												$(".imgin_{$input.name}_img_show").remove();
												$(".imgin_{$input.name}_text_show").append("<img class='imgin_{$input.name}_img_show' src='"+data.imagelink+data.imagedata+"' width='auto' height='70'>");
											}
									    },
									    error: function(data) {
									    	 $.growl.error({ title: "Error", message: "Something Wrong to upload image"});
									    },
									  });
								 } );
								$(".img_{$input.name}_alllayers").on("click",".img_{$input.name}_editclass",function() { 
										$("#imgin_{$input.name}_id_xprthomesliderlayer").remove();
										var layer_text = new Object();
										var layerid = $(this).data("layerid");
										layer_text['layerid'] = layerid;
										$.ajax({
											url: "{$controllers_link}&action=edittextlayer&ajax=1",
											data: layer_text,
											type:'post',
											dataType: 'json',
											success: function(data){
												$.each(data,function( li, lval ) {
												  if($.inArray(li,["id_slide","layertext","id","id_shop_list","force_id"]) != -1){
														if(li == 'layertext'){
															$.each(lval,function( lkey,lkeyval ) {
																	$("#imgin_{$input.name}_"+li+"_"+lkey).val(lkeyval);
															});
														}
														if(li == 'id'){
																$(".img_{$input.name}_alllayers").after('<input type="hidden" class="imgin_{$input.name}_id_xprthomesliderlayer" name="imgin_{$input.name}_id_xprthomesliderlayer" id="imgin_{$input.name}_id_xprthomesliderlayer" value="'+lval+'">');
														}
												  }else{
												  	$("#imgin_{$input.name}_"+li).val(lval);
												  }
											});
												$(".img_{$input.name}_cancleclass").remove();
												$(".img_{$input.name}_addclass").after('<button type="button" class="btn btn-default pull-right img_{$input.name}_cancleclass"> Cancle</button>');
												$(".img_{$input.name}_addclass").text('Update');
											},
											error: function(data){
												$.growl.error({ title: "Error", message: "Something Wrong!"});
											},
										});
								 } );
								$(".img_{$input.name}_alllayers").on("click",".img_{$input.name}_cancleclass",function() { 
									$("#imgin_{$input.name}_id_xprthomesliderlayer").remove();
									$(this).remove();
									reset_image_layer();	
									$(".img_{$input.name}_addclass").text('Add New');
								});
								$(".img_{$input.name}_alllayers").on("click",".img_{$input.name}_deleteclass",function() { 
										var layer_text = new Object();
										var layerid = $(this).data("layerid");
										layer_text['layerid'] = layerid;
										$.ajax({
											url: "{$controllers_link}&action=deletetextlayer&ajax=1",
											data: layer_text,
											type:'post',
											dataType: 'json',
											success: function(data){
												$(".img_{$input.name}_view_"+layerid).remove();
											},
											error: function(data){
												$.growl.error({ title: "Error", message: "Something Wrong!"});
											},
										});
										$(".img_{$input.name}_cancleclass").remove();
										$("#imgin_{$input.name}_id_xprthomesliderlayer").remove();
										$(".img_{$input.name}_addclass").text('Add New');
										reset_image_layer();
								 } ); 
								$(".img_{$input.name}_addclass").on("click",function() { 
									var layer_text = new Object();
									$('[id^="imgin_{$input.name}_"]').each(function()
									{
										id = $(this).prop("id").replace("imgin_{$input.name}_", "");
										layer_text[id] = $(this).val();
									});
										layer_text['id_slide'] = "{$id_xprtslides}";
										var samplelayerimg = $("#imgin_{$input.name}_text").val();
									$.ajax({
											url: "{$controllers_link}&action=addtextlayer&ajax=1",
											data: layer_text,
											type:'post',
											dataType: 'json',
											success: function(data){
												if (typeof data.id_xprthomesliderlayer !== 'undefined'){
												if (typeof layer_text['id_xprthomesliderlayer'] == 'undefined'){
													$(".img_{$input.name}_addclass").after('<div class="form-control-static img_{$input.name}_viewclass img_{$input.name}_view_'+data.id_xprthomesliderlayer+'">* <strong>Image layer : </strong> <img src="{$xprtimagelink}'+samplelayerimg+'" width="35" height="auto"> <button type="button" data-layerid="'+data.id_xprthomesliderlayer+'" class="img_{$input.name}_editclass btn btn-default"><i class="icon-pencil text-danger"></i></button>  <button type="button" data-layerid="'+data.id_xprthomesliderlayer+'" class="img_{$input.name}_deleteclass btn btn-default"><i class="icon-remove text-danger"></i></button> </div>');
												}	reset_image_layer();	
													$("#imgin_{$input.name}_id_xprthomesliderlayer").remove();
													$(".img_{$input.name}_cancleclass").remove();
													$(".img_{$input.name}_addclass").text('Add New');
												}
												if (typeof data.errors !== 'undefined'){
													$.growl.error({ title: "Error", message: data.errors});
												}
											},
											error: function(data){
												$.growl.error({ title: "Error", message: "Something Wrong!"});
											},
										});
								 } );
							</script>
					</div>
					{* end text multiple *}
					<style type="text/css">
						.form-group.textlayercontainerclass {
						    background-color: #eee;
						    border: 1px solid #bbb;
						}
						input[type="number"]{
							display: block;
							width: 100%;
							height: 31px;
							padding: 6px 8px;
							font-size: 12px;
							line-height: 1.42857;
							color: #555;
							background-color: #F5F8F9;
							background-image: none;
							border: 1px solid #C7D6DB;
							border-radius: 3px;
							-webkit-transition: border-color ease-in-out 0.15s,box-shadow ease-in-out 0.15s;
							-o-transition: border-color ease-in-out 0.15s,box-shadow ease-in-out 0.15s;
							transition: border-color ease-in-out 0.15s,box-shadow ease-in-out 0.15s;
						}
						.form-group.textlayercontainerclass {
						    padding: 15px 0px;
						}
					</style>
					{else}
						<div class="alert alert-warning">
								There is 1 warning.
							<ul style="display:block;" id="seeMore">
								<li>You must save this product before adding layers.</li>
							</ul>
						</div>
					{/if}
				{* end image layer *}
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
{block name="input_row"}
	{if $input.type == 'slidelists'}
		{* start slides lists *}
		{if isset($id_xpertslider) && !empty($id_xpertslider)}
		<div class="panel"><h3><i class="icon-list-ul"></i> {l s='Slides list' mod='xprtsliderblock'}
			<span class="panel-heading-action">
				<a id="desc-product-new" class="list-toolbar-btn" href="{$adminmoduleslink}&configure=xprtsliderblock&addSlide=1&id_xpertslider={$id_xpertslider}">
					<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Add new" data-html="true">
						<i class="process-icon-new "></i>
					</span>
				</a>
			</span>
			</h3>
			<div id="slidesContent">
				<div id="slides">
				{if isset($xprtslidelists) && !empty($xprtslidelists)}
					{foreach from=$xprtslidelists item=xprtslide}
						<div id="slides_{$xprtslide.id_xprtslides}" class="panel">
							<div class="row">
								<div class="col-lg-1">
									<span><i class="icon-arrows "></i></span>
								</div>
								<div class="col-md-3">
								{if isset($xprtslide.slideimage) && !empty($xprtslide.slideimage)}
									<img src="{$image_baseurl}{$xprtslide.slideimage}" alt="{$xprtslide.title}" class="img-thumbnail" height="auto" width="150" />
								{else}
									<img src="{$image_baseurl}noimage.jpg" alt="{$xprtslide.title}" class="img-thumbnail" height="auto" width="150" />
								{/if}
								</div>
								<div class="col-md-8">
									<h4 class="pull-left">
										#{$xprtslide.id_xprtslides} - {$xprtslide.title}
									</h4>
									<div class="btn-group-action pull-right">
										{$xprtslide.status}
										<a class="btn btn-default"
											href="{$adminmoduleslink}&configure=xprtsliderblock&id_xprtslides={$xprtslide.id_xprtslides}&id_xpertslider={$id_xpertslider}&addSlide=1">
											<i class="icon-edit"></i>
											{l s='Edit' mod='xprtsliderblock'}
										</a>
										<a class="btn btn-default"
											href="{$adminmoduleslink}&configure=xprtsliderblock&delete_id_xprtslides={$xprtslide.id_xprtslides}&id_xpertslider={$id_xpertslider}&editxprtslides=1">
											<i class="icon-trash"></i>
											{l s='Delete' mod='xprtsliderblock'}
										</a>
									</div>
								</div>
							</div>
						</div>
					{/foreach}
				{/if}
				</div>
			</div>
		</div>
		{else}
			<div class="alert alert-warning">
					There is 1 warning.
				<ul style="display:block;" id="seeMore">
					<li>You must save this product before adding slides.</li>
				</ul>
			</div>
		{/if}
		{* end slides lists *}
	{elseif $input.type == 'slideeffect'}
	{* Start Slide Effect *}
	<table border="0" cellpadding="0" cellspacing="0" width="600" height="300" align="center">
	    <tr>
	        <td>
	            <div style="position: relative; width: 600px; height: 300px;" id="slider1_container">
	                <div u="loading" style="position: absolute; top: 0px; left: 0px;">
	                    <div style="filter: alpha(opacity=70); opacity:.7; position: absolute; display: block;
	                        background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
	                    </div>
	                    <div style="position: absolute; display: block; background: url({$image_baseurl}img/loading.gif) no-repeat center center;
	                        top: 0px; left: 0px;width: 100%;height:100%;">
	                    </div>
	                </div>

	                <div u="slides" style="cursor: move; position: absolute; width: 600px; height: 300px;top:0px;left:0px;overflow:hidden;">
	                    <div>
	                        <img u="image" src="{$image_baseurl}img/landscape/01.jpg">
	                    </div>
	                    <div>
	                        <img u="image" src="{$image_baseurl}img/landscape/02.jpg">
	                    </div>
	                    <div>
	                        <img u="image" src="{$image_baseurl}img/landscape/04.jpg">
	                    </div>
	                    <div>
	                        <img u="image" src="{$image_baseurl}img/landscape/05.jpg">
	                    </div>
	                </div>
	            </div>
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" align="center">
	    <tr>
	        <td width="850" height="25"></td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#EEEEEE">
	    <tr>
	        <td width="850" height="15">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30">
	        </td>
	        <td width="110" bgcolor="Silver" style="color: white">
	            <label for="ssTransition">
	                <b>&nbsp; Slide Transition</b></label>
	        </td>
	        <td width="570" height="30" bgcolor="Silver" style="color: white">
	            <select name="ssTransition" id="ssTransition" style="width: 300px">
	                <option value="">
	            </select>
	        </td>
	        <td width="110" bgcolor="Silver">
	            <input type="button" value="Play" id="sButtonPlay" style="width: 110px" name="sButtonPlay" disabled="disabled">
	        </td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="ssFormation">
	                Formation:</label>
	        </td>
	        <td width="110">
	            <select name="ssFormation" id="ssFormation">
	                <option value="$JssorSlideshowFormations$.$FormationStraight">Straight
	                <option value="$JssorSlideshowFormations$.$FormationStraightStairs">Straight Stairs
	                <option value="$JssorSlideshowFormations$.$FormationSwirl">Swirl
	                <option value="$JssorSlideshowFormations$.$FormationZigZag">ZigZag
	                <option value="$JssorSlideshowFormations$.$FormationSquare">Square
	                <option value="$JssorSlideshowFormations$.$FormationRectangle">Rectangle
	                <option value="$JssorSlideshowFormations$.$FormationCircle">Circle
	                <option value="$JssorSlideshowFormations$.$FormationCross">Cross
	                <option value="$JssorSlideshowFormations$.$FormationRectangleCross">Rectangle Cross
	                <option value="$JssorSlideshowFormations$.$FormationRandom">Random
	            </select>
	        </td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="ssCols">
	                Cols:</label>
	        </td>
	        <td width="110">
	            <select name="ssCols" id="ssCols">
	                <option value="1" selected="">1
	                <option value="2">2
	                <option value="3">3
	                <option value="4">4
	                <option value="5">5
	                <option value="6">6
	                <option value="7">7
	                <option value="8">8
	                <option value="9">9
	                <option value="10">10
	                <option value="11">11
	                <option value="12">12
	                <option value="13">13
	                <option value="14">14
	                <option value="15">15
	            </select>
	        </td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <input type="checkbox" id="scReverse" name="scReverse"><label for="scReverse" title="Reverse the Assembly">Reverse</label></td>
	        <td width="110">
	            <input type="checkbox" id="scSlideOut" name="scSlideOut"><label for="scSlideOut">Slide
	                Out</label>
	        </td>
	        <td width="30">
	        </td>
	    </tr>
	    <tr>
	        <td height="25">
	        </td>
	        <td>
	            <label for="ssAssembly">
	                Assembly:</label>
	        </td>
	        <td>
	            <select name="ssAssembly" id="ssAssembly">
	                <option value="2049">Bottom to Left
	                <option value="2050">Bottom to Right
	                <option value="513">Top to Left
	                <option value="514">Top to Right
	                <option value="260">Left to Top
	                <option value="264">Left to Bottom
	                <option value="1028">Right to Top
	                <option value="1032">Right to Bottom
	            </select>
	        </td>
	        <td>
	        </td>
	        <td>
	            <label for="ssRows">
	                Rows:</label>
	        </td>
	        <td>
	            <select name="ssRows" id="ssRows">
	                <option value="1" selected="">1
	                <option value="2">2
	                <option value="3">3
	                <option value="4">4
	                <option value="5">5
	                <option value="6">6
	                <option value="7">7
	                <option value="8">8
	                <option value="9">9
	                <option value="10">10
	                <option value="11">11
	                <option value="12">12
	                <option value="13">13
	                <option value="14">14
	                <option value="15">15
	            </select>
	        </td>
	        <td>
	        </td>
	        <td>
	            <input type="checkbox" id="scOutside" name="scOutside"><label for="scOutside" id="slOutside">Play Outside</label></td>
	        <td>
	            &nbsp;</td>
	        <td>
	        </td>
	    </tr>
	    <tr>
	        <td height="25">
	        </td>
	        <td>
	            <label for="ssDuration">
	                Duration:</label>
	        </td>
	        <td>
	            <select name="ssDuration" id="ssDuration">
	                <option value="200">200</option>
	                <option value="300">300</option>
	                <option value="400">400</option>
	                <option value="500">500</option>
	                <option value="600">600</option>
	                <option value="700">700</option>
	                <option value="800">800</option>
	                <option value="900">900</option>
	                <option value="1000">1000</option>
	                <option value="1100">1100</option>
	                <option value="1200">1200</option>
	                <option value="1500">1500</option>
	                <option value="1800">1800</option>
	                <option value="2000">2000</option>
	                <option value="2500">2500</option>
	                <option value="3000">3000</option>
	                <option value="4000">4000</option>
	                <option value="5000">5000</option>
	            </select>
	        </td>
	        <td>
	        </td>
	        <td>
	            <label for="ssDelay">
	                Delay:</label>
	        </td>
	        <td>
	            <select name="ssDelay" id="ssDelay">
	                <option value="0">0
	                <option value="10">10
	                <option value="20">20
	                <option value="30">30
	                <option value="40">40
	                <option value="50">50
	                <option value="60">60
	                <option value="80">80
	                <option value="100">100
	                <option value="120">120
	                <option value="150">150
	                <option value="160">160
	                <option value="200">200
	                <option value="300">300
	                <option value="400">400
	                <option value="500">500
	                <option value="600">600
	                <option value="800">800
	            </select>
	        </td>
	        <td>
	        </td>
	        <td>
	            &nbsp;</td>
	        <td>
	            <input type="checkbox" id="scMove" name="scMove"><label for="scMove" id="slMove">Move</label> </td>
	        <td>
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="850" height="5">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	    <td width="30"></td>
	    <td width="790" height="1" bgcolor="Silver"></td>
	    <td width="30"></td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="850" height="5">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	                Fly 
	            (Hor):&nbsp;
	        </td>
	        <td width="110">
	        <select name="ssFlyHorizontal" id="ssFlyHorizontal">
	                <option value="">
	                <option value="2">To Right
	                <option value="1">To Left
	            </select></td>
	        <td width="65">
	        </td>
	        <td width="110">
	                Fly (Ver):
	        </td>
	        <td width="110">
	            <select name="ssFlyVertical" id="ssFlyVertical">
	                <option value="">
	                <option value="8">To Bottom
	                <option value="4">To Top
	            </select></td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="ssClip" id="slClip">Clip:</label></td>
	        <td width="110">
	            <select name="ssClip" id="ssClip">
	                <option value="">
	                <option value="15">Around
	                <option value="3">Left & Right
	                <option value="12">Top & Bottom
	                <option value="5">Top & Left
	                <option value="6">Top & Right
	                <option value="9">Bottom & Left
	                <option value="10">Bottom & Right
	                <option value="4">Top
	                <option value="8">Bottom
	                <option value="1">Left
	                <option value="2">Right
	                <option value="11">Exclude Top
	                <option value="7">Exclude Bottom
	                <option value="14">Exclude Left
	                <option value="13">Exclude Right
	            </select></td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="stDuringBeginHor" id="slDuringBeginHor">During (Hor):</label></td>
	        <td width="110">
	            <input name="stDuringBeginHor" id="stDuringBeginHor" style="width:30px;" class="inputText"> to <input name="stDuringLengthHor" id="stDuringLengthHor" style="width:30px; display:inline;" class="inputText"></td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="stDuringBeginVer" id="slDuringBeginVer">During (Ver):</label></td>
	        <td width="110">
	            <input name="stDuringBeginVer" id="stDuringBeginVer" style="width:30px;" class="inputText"> to <input name="stDuringLengthVer" id="stDuringLengthVer" style="width:30px; display:inline;" class="inputText"></td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="stDuringBeginClip" id="slDuringBeginClip">During (Clip):</label></td>
	        <td width="110">
	            <input name="stDuringBeginClip" id="stDuringBeginClip" style="width:30px;" class="inputText"> to <input name="stDuringLengthClip" id="stDuringLengthClip" style="width:30px; display:inline;" class="inputText"></td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="85">
	            <label for="ssEasingHorizontal" id="slEasingHorizontal">
	                Easing (Hor):</label></td>
	        <td width="135">
	            <select style="width: 135px;" name="ssEasingHorizontal" id="ssEasingHorizontal">
	                <option value="">
	            </select>
	        </td>
	        <td width="65">
	        </td>
	        <td width="85">
	            <label for="ssEasingVertical" id="slEasingVertical">
	                Easing (Ver):</label>
	        </td>
	        <td width="135">
	            <select style="width: 135px;" name="ssEasingVertical" id="ssEasingVertical">
	                <option value="">
	            </select>
	        </td>
	        <td width="65">
	        </td>
	        <td width="85">
	            <label for="ssEasingClip" id="slEasingClip">
	                Easing (Clip):</label>
	        </td>
	        <td width="135">
	            <select style="width: 135px;" name="ssEasingClip" id="ssEasingClip">
	                <option value="">
	            </select></td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="stScaleHorizontal" id="slScaleHorizontal">
	                Scale (Hor):</label></td>
	        <td width="110"><input name="stScaleHorizontal" id="stScaleHorizontal" style="width:108px;" class="inputText"></td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="stScaleVertical" id="slScaleVertical">
	                Scale (Ver):</label>
	        </td>
	        <td width="110"><input name="stScaleVertical" id="stScaleVertical" style="width:108px;" class="inputText"></td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="stScaleClip" id="slScaleClip">
	                Scale (Clip):</label>
	        </td>
	        <td width="110"><input name="stScaleClip" id="stScaleClip" style="width:108px;" class="inputText"></td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="ssRoundHorizontal" id="slRoundHorizontal">
	                Round (Hor):</label>
	        </td>
	        <td width="110">
	            <select name="ssRoundHorizontal" id="ssRoundHorizontal">
	                <option value="">
	            </select>
	        </td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="ssRoundVertical" id="slRoundVertical">
	                Round (Ver):</label>
	        </td>
	        <td width="110">
	            <select name="ssRoundVertical" id="ssRoundVertical">
	                <option value="">
	            </select>
	        </td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="ssRoundClip" id="slRoundClip">
	                Round (Clip):</label>
	        </td>
	        <td width="110">
	            <select name="ssRoundClip" id="ssRoundClip">
	                <option value="">
	            </select></td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="850" height="5">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	    <td width="30"></td>
	    <td width="790" height="1" bgcolor="Silver"></td>
	    <td width="30"></td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="850" height="5">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <input type="checkbox" id="scZoom" name="scZoom"><label for="scZoom" id="slZoom">Zoom</label>
	        </td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="85">
	            <input type="checkbox" id="scRotate" name="scRotate" disabled="disabled"><label for="scRotate" id="slRotate">Rotate</label></td>
	        <td width="135">
	            &nbsp;</td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <input type="checkbox" id="scFade" name="scFade"><label for="scFade">Fade</label></td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="stDuringBeginZoom" id="slDuringBeginZoom">During (Zoom):</label></td>
	        <td width="110">
	            <input name="stDuringBeginZoom" id="stDuringBeginZoom" style="width:30px;" class="inputText"> to <input name="stDuringLengthZoom" id="stDuringLengthZoom" style="width:30px; display:inline;" class="inputText"></td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="stDuringBeginRotate" id="slDuringBeginRotate">During (Rotate):</label></td>
	        <td width="110">
	            <input name="stDuringBeginRotate" id="stDuringBeginRotate" style="width:30px;" class="inputText"> to <input name="stDuringLengthRotate" id="stDuringLengthRotate" style="width:30px; display:inline;" class="inputText"></td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="stDuringBeginFade" id="slDuringBeginFade">During (Fade):</label></td>
	        <td width="110">
	            <input name="stDuringBeginFade" id="stDuringBeginFade" style="width:30px;" class="inputText"> to <input name="stDuringLengthFade" id="stDuringLengthFade" style="width:30px; display:inline;" class="inputText"></td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="85">
	            <label for="ssEasingZoom" id="slEasingZoom">
	                Easing (Zoom)</label></td>
	        <td width="135">
	            <select style="width: 135px;" name="ssEasingZoom" id="ssEasingZoom">
	                <option value="">
	            </select></td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="85">
	            <label for="ssEasingRotate" id="slEasingRotate">
	            Easing(Rotate)</label></td>
	        <td width="135">
	            <select style="width: 135px;" name="ssEasingRotate" id="ssEasingRotate">
	                <option value="">
	            </select></td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="85">
	            <label for="ssEasingFade" id="slEasingFade">
	            Easing(Fade)</label></td>
	        <td width="135">
	            <select name="ssEasingFade" id="ssEasingFade" style="width: 135px;">
	                <option value="">
	            </select></td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="stScaleZoom" id="slScaleZoom">
	                Scale (Zoom):</label> </td>
	        <td width="110">
	            <input name="stScaleZoom" id="stScaleZoom" style="width:108px;" class="inputText"></td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="110">
	            <label for="slScaleRotate" id="slScaleRotate">
	                Scale (Rotate):</label> </td>
	        <td width="110">
	            <input name="stScaleRotate" id="stScaleRotate" style="width:108px;" class="inputText"></td>
	        <td width="65">
	        </td>
	        <td width="110">
	            <label for="stScaleFade" id="slScaleFade">
	                Scale (Fade):</label> </td>
	        <td width="110">
	            <input name="stScaleFade" id="stScaleFade" style="width:108px;" class="inputText"></td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="ssRoundZoom" id="slRoundZoom">
	                Round (Zoom):</label> </td>
	        <td width="110">
	            <select name="ssRoundZoom" id="ssRoundZoom">
	                <option value="">
	            </select></td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="110">
	            <label for="ssRoundRotate" id="slRoundRotate">
	                Round (Rotate):</label></td>
	        <td width="110">
	            <select name="ssRoundRotate" id="ssRoundRotate">
	                <option value="">
	            </select>
	            </td>
	        <td width="65">
	        </td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="850" height="5">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <input type="checkbox" id="scZIndex" name="scZIndex"><label for="scZoom" id="Label1">Z-Index</label>
	        </td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="85">
	            &nbsp;</td>
	        <td width="135">
	            &nbsp;</td>
	        <td width="65">
	        </td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="stDuringBeginZIndex" id="slDuringBeginZIndex">During (ZIndex):</label></td>
	        <td width="110">
	            <input name="stDuringBeginZIndex" id="stDuringBeginZIndex" style="width:30px;" class="inputText"> to <input name="stDuringLengthZIndex" id="stDuringLengthZIndex" style="width:30px; display:inline;" class="inputText"></td>
	        <td width="65">
	        </td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="65">
	        </td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="85">
	            <label for="ssEasingZIndex" id="slEasingZIndex">
	                Easing (ZInd..)</label></td>
	        <td width="135">
	            <select style="width: 135px;" name="ssEasingZIndex" id="ssEasingZIndex">
	                <option value="">
	            </select></td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="85">
	            &nbsp;</td>
	        <td width="135">
	            &nbsp;</td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="85">
	            &nbsp;</td>
	        <td width="135">
	            &nbsp;</td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="stScaleZIndex" id="slScaleZIndex">
	                Scale (ZIndex):</label> </td>
	        <td width="110">
	            <input name="stScaleZIndex" id="stScaleZIndex" style="width:108px;" class="inputText"></td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="65">
	        </td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            <label for="ssRoundZIndex" id="slRoundZIndex">
	                Round (ZIndex):</label> </td>
	        <td width="110">
	            <select name="ssRoundZIndex" id="ssRoundZIndex">
	                <option value="">
	            </select></td>
	        <td width="65">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="65">
	        </td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="30">
	        </td>
	    </tr>
	</table>

	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="110">
	            &nbsp;</td>
	        <td width="65">
	            Chess C:
	        </td>
	        <td width="110">
	            <input type="checkbox" id="scChessColHorizontal" name="scChessColHorizontal"><label for="scChessColHorizontal" id="slChessColHorizontal">Horizontal</label> </td>
	        <td width="110">
	            <input type="checkbox" id="scChessColVertical" name="scChessColVertical"><label for="scChessColVertical" id="slChessColVertical">Vertical</label>
	        </td>
	        <td width="65">Chess R:
	            </td>
	        <td width="110"><input type="checkbox" id="scChessRowHorizontal" name="scChessRowHorizontal"><label for="scChessRowHorizontal" id="slChessRowHorizontal">Horizontal</label>
	        </td>
	        <td width="110"><input type="checkbox" id="scChessRowVertical" name="scChessRowVertical"><label for="scChessRowVertical" id="slChessRowVertical">Vertical</label>
	        </td>
	        <td width="30">
	        </td>
	    </tr>
	    </table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="850" height="5">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
	    <tr>
	        <td width="30" height="25">
	        </td>
	        <td width="790" align="center">
	            <input id="stTransition" style="width: 780px; height: 25px;" type="text" name="stTransition">
	        </td>
	        <td width="30">
	        </td>
	    </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#EEEEEE">
	    <tr>
	        <td width="850" height="15">
	            &nbsp;&nbsp;
	            </td>
	    </tr>
	</table>
	<script>
	    slideshow_transition_controller_starter("slider1_container");
	</script>
	<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 800px; height: 50px;">
	    <tr>
	        <td>
	        </td>
	    </tr>
	</table>
	{* End Slide Effect *}
	{elseif $input.type == 'layereffect'}
		<table border="0" cellpadding="0" cellspacing="0" width="600" height="300" align="center" bgcolor="#EEEEEE">
		    <tr>
		        <td>
		            <div style="position: relative; width: 960px; height: 380px; overflow: hidden;" id="slider1_container">
		                <!-- Loading Screen -->
		                <div u="loading" style="position: absolute; top: 0px; left: 0px;">
		                    <div style="filter: inherit; position: absolute; display: block;
		                        background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
		                    </div>
		                    <div style="position: absolute; display: block; background: url(img/loading.gif) no-repeat center center;
		                        top: 0px; left: 0px;width: 100%;height:100%;">
		                    </div>
		                </div>
		                <div u="slides" style="cursor: move; position: absolute; width: 960px; height: 380px;left:0px;top:0px; overflow: hidden;">
		                    <div>
		                        <img u="image" src="{$image_baseurl}img/home/01.jpg">
		                        <img u="caption" t="0" style="position:absolute;left:200px;top:60px;width:80px;height:80px;" src="{$image_baseurl}img/icon-slider-12-jquery.png">
		                        <div u=caption t="0" d=-200 class="captionOrange"  style="position:absolute; left:350px; top: 85px; width:500px; height:30px;"> 
		                            Jssor responsive touch swipe javascript image slider
		                        </div> 
		                        <div u=caption t="0" d=-200 class="captionOrange"  style="position:absolute; left:110px; top: 265px; width:500px; height:30px;"> 
		                            Best Performance Slider, Most Scalable Slider
		                        </div> 
		                        <img u="caption" t="0" d=-200 style="position:absolute;left:680px;top:240px;width:80px;height:80px;" src="{$image_baseurl}img/icon-slider-12-jquery.png">
		                    </div>
		                    <div>
		                        <img u="image" src="{$image_baseurl}img/home/02.jpg">
		                        <img u="caption" t="0" style="position:absolute;left:200px;top:60px;width:80px;height:80px;" src="{$image_baseurl}img/icon-slider-12-jquery.png">
		                        <div u=caption t="0" d=-200 class="captionOrange"  style="position:absolute; left:350px; top: 85px; width:500px; height:30px;"> 
		                            Jssor responsive touch swipe javascript image slider
		                        </div> 
		                        <div u=caption t="0" d=-200 class="captionOrange"  style="position:absolute; left:110px; top: 265px; width:500px; height:30px;"> 
		                            Best Performance Slider, Most Scalable Slider
		                        </div> 
		                        <img u="caption" t="0" d=-200 style="position:absolute;left:680px;top:240px;width:80px;height:80px;" src="{$image_baseurl}img/icon-slider-12-jquery.png">
		                    </div>
		                </div>
		            </div>
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" align="center">
		    <tr>
		        <td width="850" height="25">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30">
		        </td>
		        <td width="110" bgcolor="Silver" style="color: white">
		            <b>&nbsp; Caption Transition</b>
		        </td>
		        <td width="570" height="30" bgcolor="Silver" style="color: white">
		            &nbsp;
		            <select name="ssTransition" id="ssTransition" style="width: 200px">
		                <option value="">
		            </select></td>
		        <td width="110" bgcolor="Silver">
		            <input type="button" value="Play" id="sButtonPlay" style="width: 110px" name="sButtonPlay" disabled="disabled">
		        </td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="30">
		        </td>
		        <td width="110">
		                Duration:
		        </td>
		        <td width="110"><select name="ssDuration" id="ssDuration">
		                <option value="200">200</option>
		                <option value="300">300</option>
		                <option value="400">400</option>
		                <option value="500">500</option>
		                <option value="600">600</option>
		                <option value="700">700</option>
		                <option value="800">800</option>
		                <option value="900">900</option>
		                <option value="1000">1000</option>
		                <option value="1100">1100</option>
		                <option value="1200">1200</option>
		                <option value="1500">1500</option>
		                <option value="1800">1800</option>
		                <option value="2000">2000</option>
		                <option value="2500">2500</option>
		                <option value="3000">3000</option>
		                <option value="4000">4000</option>
		                <option value="5000">5000</option>
		            </select></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="65">
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110"><input type="checkbox" id="scMove" name="scMove"><label for="scMove" id="slMove">Move</label></td>
		        <td width="30">
		        </td>
		    </tr>
		    </table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="850" height="5">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		    <td width="30"></td>
		    <td width="790" height="1" bgcolor="Silver"></td>
		    <td width="30"></td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="850" height="5">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		                Fly (Hor):
		        </td>
		        <td width="110">
		        <select name="ssFlyHorizontal" id="ssFlyHorizontal">
		                <option value="">
		                <option value="2">To Right
		                <option value="1">To Left
		            </select></td>
		        <td width="65">
		        </td>
		        <td width="110">
		                Fly (Ver):
		        </td>
		        <td width="110">
		            <select name="ssFlyVertical" id="ssFlyVertical">
		                <option value="">
		                <option value="8">To Bottom
		                <option value="4">To Top
		            </select></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="ssClip" id="slClip">Clip:</label></td>
		        <td width="110">
		            <select name="ssClip" id="ssClip">
		                <option value="">
		                <option value="15">Around
		                <option value="3">Left & Right
		                <option value="12">Top & Bottom
		                <option value="5">Top & Left
		                <option value="6">Top & Right
		                <option value="9">Bottom & Left
		                <option value="10">Bottom & Right
		                <option value="4">Top
		                <option value="8">Bottom
		                <option value="1">Left
		                <option value="2">Right
		                <option value="11">Exclude Top
		                <option value="7">Exclude Bottom
		                <option value="14">Exclude Left
		                <option value="13">Exclude Right
		            </select></td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <label for="stDuringBeginHor" id="slDuringBeginHor">During (Hor):</label></td>
		        <td width="110">
		            <input name="stDuringBeginHor" id="stDuringBeginHor" style="width:30px;" class="inputText"> to <input name="stDuringLengthHor" id="stDuringLengthHor" style="width:30px; display:inline;" class="inputText"></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="stDuringBeginVer" id="slDuringBeginVer">During (Ver):</label></td>
		        <td width="110">
		            <input name="stDuringBeginVer" id="stDuringBeginVer" style="width:30px;" class="inputText"> to <input name="stDuringLengthVer" id="stDuringLengthVer" style="width:30px; display:inline;" class="inputText"></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="stDuringBeginClip" id="slDuringBeginClip">During (Clip):</label></td>
		        <td width="110">
		            <input name="stDuringBeginClip" id="stDuringBeginClip" style="width:30px;" class="inputText"> to <input name="stDuringLengthClip" id="stDuringLengthClip" style="width:30px; display:inline;" class="inputText"></td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="85">
		            <label for="ssEasingHorizontal" id="slEasingHorizontal">
		                Easing (Hor):</label></td>
		        <td width="135">
		            <select style="width: 135px;" name="ssEasingHorizontal" id="ssEasingHorizontal">
		                <option value="">
		            </select>
		        </td>
		        <td width="65">
		        </td>
		        <td width="85">
		            <label for="ssEasingVertical" id="slEasingVertical">
		                Easing (Ver):</label>
		        </td>
		        <td width="135">
		            <select style="width: 135px;" name="ssEasingVertical" id="ssEasingVertical">
		                <option value="">
		            </select>
		        </td>
		        <td width="65">
		        </td>
		        <td width="85">
		            <label for="ssEasingClip" id="slEasingClip">
		                Easing (Clip):</label>
		        </td>
		        <td width="135">
		            <select style="width: 135px;" name="ssEasingClip" id="ssEasingClip">
		                <option value="">
		            </select></td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <label for="stScaleHorizontal" id="slScaleHorizontal">
		                Scale (Hor):</label></td>
		        <td width="110"><input name="stScaleHorizontal" id="stScaleHorizontal" style="width:108px;" class="inputText"></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="stScaleVertical" id="slScaleVertical">
		                Scale (Ver):</label>
		        </td>
		        <td width="110"><input name="stScaleVertical" id="stScaleVertical" style="width:108px;" class="inputText"></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="stScaleClip" id="slScaleClip">
		                Scale (Clip):</label>
		        </td>
		        <td width="110"><input name="stScaleClip" id="stScaleClip" style="width:108px;" class="inputText"></td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <label for="ssRoundHorizontal" id="slRoundHorizontal">
		                Round (Hor):</label>
		        </td>
		        <td width="110">
		            <select name="ssRoundHorizontal" id="ssRoundHorizontal">
		                <option value="">
		            </select>
		        </td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="ssRoundVertical" id="slRoundVertical">
		                Round (Ver):</label>
		        </td>
		        <td width="110">
		            <select name="ssRoundVertical" id="ssRoundVertical">
		                <option value="">
		            </select>
		        </td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="ssRoundClip" id="slRoundClip">
		                Round (Clip):</label>
		        </td>
		        <td width="110">
		            <select name="ssRoundClip" id="ssRoundClip">
		                <option value="">
		            </select></td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="850" height="5">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		    <td width="30"></td>
		    <td width="790" height="1" bgcolor="Silver"></td>
		    <td width="30"></td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="850" height="5">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <input type="checkbox" id="scZoom" name="scZoom"><label for="scZoom" id="slZoom">Zoom</label>
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="85">
		            <input type="checkbox" id="scRotate" name="scRotate" disabled="disabled"><label for="scRotate" id="slRotate">Rotate</label></td>
		        <td width="135">
		            &nbsp;</td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <input type="checkbox" id="scFade" name="scFade"><label for="scFade">Fade</label></td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <label for="stDuringBeginZoom" id="slDuringBeginZoom">During (Zoom):</label></td>
		        <td width="110">
		            <input name="stDuringBeginZoom" id="stDuringBeginZoom" style="width:30px;" class="inputText"> to <input name="stDuringLengthZoom" id="stDuringLengthZoom" style="width:30px; display:inline;" class="inputText"></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="stDuringBeginRotate" id="slDuringBeginRotate">During (Rotate):</label></td>
		        <td width="110">
		            <input name="stDuringBeginRotate" id="stDuringBeginRotate" style="width:30px;" class="inputText"> to <input name="stDuringLengthRotate" id="stDuringLengthRotate" style="width:30px; display:inline;" class="inputText"></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="stDuringBeginFade" id="slDuringBeginFade">During (Fade):</label></td>
		        <td width="110">
		            <input name="stDuringBeginFade" id="stDuringBeginFade" style="width:30px;" class="inputText"> to <input name="stDuringLengthFade" id="stDuringLengthFade" style="width:30px; display:inline;" class="inputText"></td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="85">
		            <label for="ssEasingZoom" id="slEasingZoom">
		                Easing (Zoom)</label></td>
		        <td width="135">
		            <select style="width: 135px;" name="ssEasingZoom" id="ssEasingZoom">
		                <option value="">
		            </select></td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="85">
		            <label for="ssEasingRotate" id="slEasingRotate">
		            Easing(Rotate)</label></td>
		        <td width="135">
		            <select style="width: 135px;" name="ssEasingRotate" id="ssEasingRotate">
		                <option value="">
		            </select></td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="85">
		            <label for="ssEasingFade" id="slEasingFade">
		            Easing(Fade)</label></td>
		        <td width="135">
		            <select name="ssEasingFade" id="ssEasingFade" style="width: 135px;">
		                <option value="">
		            </select></td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <label for="stScaleZoom" id="slScaleZoom">
		                Scale (Zoom):</label> </td>
		        <td width="110">
		            <input name="stScaleZoom" id="stScaleZoom" style="width:108px;" class="inputText"></td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="110">
		            <label for="slScaleRotate" id="slScaleRotate">
		                Scale (Rotate):</label> </td>
		        <td width="110">
		            <input name="stScaleRotate" id="stScaleRotate" style="width:108px;" class="inputText"></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            <label for="stScaleFade" id="slScaleFade">
		                Scale (Fade):</label> </td>
		        <td width="110">
		            <input name="stScaleFade" id="stScaleFade" style="width:108px;" class="inputText"></td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <label for="ssRoundZoom" id="slRoundZoom">
		                Round (Zoom):</label> </td>
		        <td width="110">
		            <select name="ssRoundZoom" id="ssRoundZoom">
		                <option value="">
		            </select></td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="110">
		            <label for="ssRoundRotate" id="slRoundRotate">
		                Round (Rotate):</label></td>
		        <td width="110">
		            <select name="ssRoundRotate" id="ssRoundRotate">
		                <option value="">
		            </select></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="850" height="5">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <input type="checkbox" id="scZIndex" name="scZIndex"><label for="scZoom" id="Label1">Z-Index</label>
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="85">
		            &nbsp;</td>
		        <td width="135">
		            &nbsp;</td>
		        <td width="65">
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <label for="stDuringBeginZIndex" id="slDuringBeginZIndex">During (ZIndex):</label></td>
		        <td width="110">
		            <input name="stDuringBeginZIndex" id="stDuringBeginZIndex" style="width:30px;" class="inputText"> to <input name="stDuringLengthZIndex" id="stDuringLengthZIndex" style="width:30px; display:inline;" class="inputText"></td>
		        <td width="65">
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="65">
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="85">
		            <label for="ssEasingZIndex" id="slEasingZIndex">
		                Easing (ZInd..)</label></td>
		        <td width="135">
		            <select style="width: 135px;" name="ssEasingZIndex" id="ssEasingZIndex">
		                <option value="">
		            </select></td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="85">
		            &nbsp;</td>
		        <td width="135">
		            &nbsp;</td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="85">
		            &nbsp;</td>
		        <td width="135">
		            &nbsp;</td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <label for="stScaleZIndex" id="slScaleZIndex">
		                Scale (ZIndex):</label> </td>
		        <td width="110">
		            <input name="stScaleZIndex" id="stScaleZIndex" style="width:108px;" class="inputText"></td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="65">
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="25">
		        </td>
		        <td width="110">
		            <label for="ssRoundZIndex" id="slRoundZIndex">
		                Round (ZIndex):</label> </td>
		        <td width="110">
		            <select name="ssRoundZIndex" id="ssRoundZIndex">
		                <option value="">
		            </select></td>
		        <td width="65">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="65">
		        </td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="110">
		            &nbsp;</td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="850" height="5">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" bgcolor="#EEEEEE" align="center" style="color:#000;">
		    <tr>
		        <td width="30" height="30">
		        </td>
		        <td width="790" align="center">
		            <input id="stTransition" style="width: 780px; height: 25px;" type="text" name="stTransition">
		        </td>
		        <td width="30">
		        </td>
		    </tr>
		</table>
		<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#EEEEEE">
		    <tr>
		        <td width="850" height="5">
		        </td>
		    </tr>
		</table>
		<script>
		    caption_transition_controller_starter("slider1_container");
		    {if isset($stTransition) && !empty($stTransition)}
		    	$("#stTransition").val("{$stTransition}");
		    {/if}
		</script>
		<table align="center" border="0" cellspacing="0" cellpadding="0" style="width: 800px; height: 50px;">
		    <tr>
		        <td>
		        </td>
		    </tr>
		</table>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}