<link href="{$this__path}css/xprtsliderblock_back.css" rel="stylesheet" type="text/css"/>

{if $xprtsliderbopage == 'layereffect'}

<link href="{$this__path}css/xprtsliderblock_backlayer.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="{$this__path}js/caption-transition-builder-controller.min.js"></script>

{elseif $xprtsliderbopage == 'slideeffect'}

<link href="{$this__path}css/xprtsliderblock_backslide.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="{$this__path}js/slideshow-transition-builder-controller.min.js"></script>

{/if}
<script type="text/javascript" src="{$colorpicker}"></script>
