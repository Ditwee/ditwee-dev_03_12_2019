<div class="panel"><h3><i class="icon-list-ul"></i> {l s='Slide Effects list' mod='{$xprtmodulename}'}
	<span class="panel-heading-action">
		<a id="desc-product-new" class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')}&configure={$xprtmodulename}&addxprtsldeffect=1">
			<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Add new" data-html="true">
				<i class="process-icon-new "></i>
			</span>
		</a>
	</span>
	</h3>
	<div id="xprtslideeffectContent">
			{if isset($block_lists) && !empty($block_lists)}
		<div id="xprtslideeffect_lists">
			{foreach from=$block_lists item=block_list}
				<div id="xprtslideeffect_lists_{$block_list.id}" class="panel">
					<div class="row">
						<div class="col-md-1">
							<div><i class="icon-arrows"></i></div>
						</div>
						<div class="col-md-3">
							{$block_list.id}
						</div>
						<div class="col-md-8">
							<h4 class="pull-left">
								{$block_list.title}
							</h4>
							<div class="btn-group-action pull-right">
								{$block_list.status}
								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')}&configure={$xprtmodulename}&id_xprtslideeffect={$block_list.id}&updatexprtslideeffect=1">
									<i class="icon-edit"></i>
								</a>
								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')}&configure={$xprtmodulename}&delete_id_xprtslideeffect={$block_list.id}&deletexprtslideeffect=1">
									<i class="icon-trash"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			{/foreach}
		</div>
			{else}
				<center><p style="font-size:15px;"><a id="desc-product-new2" class="list-toolbar-btn" href="{$link->getAdminLink('AdminModules')}&configure={$xprtmodulename}&addxprtsldeffect=1">You don't have any Layer Effect lists. Please  click here  to add new.</a></p></center>
			{/if}
	</div>
</div>
<style type="text/css">
	#block_lists .panel{
	    padding: 0px !important;
	    padding-left: 15px !important;
	    padding-right: 15px !important;
	    margin-bottom: 10px !important;
	}
</style>