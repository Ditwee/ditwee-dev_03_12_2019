{if isset($xprtsliderblock) || isset($xprtsliderblock)}
{foreach from=$xprtsliderblock item=xprtslideblock}
.xprt_slider_container_{$xprtslideblock.id_xprtsliderblock} { 
	position: relative;
	margin: 0 auto;
	top: 0px;
	left: 0px;
	width: {$xprtslideblock.sliderwidth}px;
	height: {$xprtslideblock.sliderheight}px;
	overflow: hidden;
 } 
 .xprt_slider_wrapper_{$xprtslideblock.id_xprtsliderblock} { 
 	cursor: move;
 	position: absolute;
 	left: 0px;
 	top: 0px;
 	width:{$xprtslideblock.sliderwidth}px;
 	height: {$xprtslideblock.sliderheight}px;
 	overflow: hidden;
 } 
{if isset($xprtslideblock.all_slides) && !empty($xprtslideblock.all_slides)}
{foreach from=$xprtslideblock.all_slides item=slide}
{if isset($slide.all_layers) && !empty($slide.all_layers)}
{foreach from=$slide.all_layers item=layer}
{if isset($layer.layer_type) && !empty($layer.layer_type) && $layer.layer_type == 'text'}
.xprt_layer_item_{$layer.id_xprthomesliderlayer} { 
	position: absolute;
{if isset($layer.width) && !empty($layer.width)}
	width:{$layer.width}px;
{/if}
{if isset($layer.height) && !empty($layer.height)}
	height:{$layer.height}px;			
{/if}
{if isset($layer.top) && !empty($layer.top)}
	top:{$layer.top}px;
{/if}
{if isset($layer.left) && !empty($layer.left)}
	left:{$layer.left}px;
{/if}
{if isset($layer.margin) && !empty($layer.margin)}
	margin:{$layer.margin};
{/if}
{if isset($layer.padding) && !empty($layer.padding)}
	padding:{$layer.padding};
{/if}
{if isset($layer.line_height) && !empty($layer.line_height)}
	line-height:{$layer.line_height};
{/if}
{if isset($layer.font_family) && !empty($layer.font_family)}
	font-family:{$layer.font_family};
{/if}
{if isset($layer.font_weight) && !empty($layer.font_weight)}
	font-weight:{$layer.font_weight};
{/if}
{if isset($layer.font_size) && !empty($layer.font_size)}
	font-size:{$layer.font_size}px;
{/if}
{if isset($layer.bg_color) && !empty($layer.bg_color)}
	background-color:{$layer.bg_color};
{/if}
{if isset($layer.text_color) && !empty($layer.text_color)}
	color:{$layer.text_color};
{/if}
{if isset($layer.border_size) && !empty($layer.border_size)}
	border:{$layer.border_size}px solid {if isset($layer.border_color) && !empty($layer.border_color)}{$layer.border_color};{/if}
{/if}
} 
.xprt_layer_item_{$layer.id_xprthomesliderlayer}:hover { 
{if isset($layer.bg_color_hover) && !empty($layer.bg_color_hover)}
	background-color:{$layer.bg_color_hover};
{/if}
{if isset($layer.text_color_hover) && !empty($layer.text_color_hover)}
	color:{$layer.text_color_hover};
{/if}
{if isset($layer.border_color_hover) && !empty($layer.border_color_hover)}
	border:{$layer.border_color_hover}px solid {if isset($layer.border_color_hover) && !empty($layer.border_color_hover)}{$layer.border_color_hover};{/if}
{/if}
} 
{/if}
{if isset($layer.layer_type) && !empty($layer.layer_type) && $layer.layer_type == 'image'}
.xprt_layer_item_{$layer.id_xprthomesliderlayer} { 
	position: absolute;
{if isset($layer.width) && !empty($layer.width)}
	width:{$layer.width}px;
{/if}
{if isset($layer.height) && !empty($layer.height)}
	height:{$layer.height}px;			
{/if}
{if isset($layer.top) && !empty($layer.top)}
	top:{$layer.top}px;
{/if}
{if isset($layer.left) && !empty($layer.left)}
	left:{$layer.left}px;
{/if}
} 
{/if}
{/foreach}
{/if}
{/foreach}
{/if}
{/foreach}
{/if}