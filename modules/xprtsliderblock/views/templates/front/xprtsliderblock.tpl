{*
<pre>
{$xprtsliderblock|print_r}
</pre>
*}
{if isset($xprtsliderblock) || isset($xprtsliderblock)}
{foreach from=$xprtsliderblock item=xprtslideblock}
{literal}
	<script>
		var _CaptionTransitions_{/literal}{$xprtslideblock.id_xprtsliderblock|intval}{literal} = [];
		var _SlideshowTransitions_{/literal}{$xprtslideblock.id_xprtsliderblock|intval}{literal} = [
{/literal}
	{foreach from=$xprtslideblock.slideeffectlists item=slideeffectlist}
		{$slideeffectlist.effect},
	{/foreach}
{literal}
		];
	</script>
{/literal}
<div class="xprt_slider_area xprt_slider_area_{$xprtslideblock.id_xprtsliderblock}">
	<div class="xprt_slider xprt_slider_{$xprtslideblock.id_xprtsliderblock}" style="margin: {$xprtslideblock.sliderpostion};{if $xprtslideblock.forcefullwidth == '0'} overflow: hidden;{/if}">
		<div id="xprt_slider_container" class="xprt_slider_container xprt_slider_container_{$xprtslideblock.id_xprtsliderblock} loading" {* style="
			position: relative;
			margin: 0 auto;
			top: 0px;
			left: 0px;
			width: {$xprtslideblock.sliderwidth}px;
			height: {$xprtslideblock.sliderheight}px;
			overflow: hidden;" *}>
				<!-- Loading Screen -->
			    <div class="preloader" data-u="loading" style="position: absolute; top: 0px; left: 0px;">
	                <div style="background-color:#fff;filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
	                <div style="position:absolute;display:block;background:url({$img_dir}bx_loader.gif) no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
	            </div>
		        <div class="xprt_slider_wrapper xprt_slider_wrapper_{$xprtslideblock.id_xprtsliderblock}" data-u="slides" {* style="
			        cursor: pointer;
			        position: absolute;
			        left: 0px;
			        top: 0px;
			        width:{$xprtslideblock.sliderwidth}px;
			        height: {$xprtslideblock.sliderheight}px;
			        overflow: hidden;" *}>
			        {if isset($xprtslideblock.all_slides) && !empty($xprtslideblock.all_slides)}
						{foreach from=$xprtslideblock.all_slides item=slide}
					        	<div class="xprt_slider_item">
				<a href="{if $slide.slideurl<>'' && $slide.slideurl<>'#'}{$slide.slideurl}{else}#slide{/if}">
					        	    <img data-u="image" {if isset($slide.lazyload) && !empty($slide.lazyload) && $slide.lazyload == 1} src2="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`xprtsliderblock/images/`$slide.slideimage|escape:'htmlall':'UTF-8'`")}" {else} src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`xprtsliderblock/images/`$slide.slideimage|escape:'htmlall':'UTF-8'`")}" {/if} {if isset($slide.size) && $slide.size} {$slide.size}{else} width="100%" height="100%"{/if} alt="{$slide.title|escape:'htmlall':'UTF-8'}" class="xprt_slider_img img-responsive" />
				</a>
					        	    {if isset($slide.all_layers) && !empty($slide.all_layers)}
										{foreach from=$slide.all_layers item=layer}
										{if isset($layer.in_lavel) && !empty($layer.in_lavel) && isset($layer.in_value) && !empty($layer.in_value)} {literal}
										<script>
										_CaptionTransitions_{/literal}{$xprtslideblock.id_xprtsliderblock|intval}["{$layer.in_lavel}"] = {$layer.in_value};{literal}
										</script>{/literal}
										{/if}
										{if isset($layer.out_lavel) && !empty($layer.out_lavel) && isset($layer.out_value) && !empty($layer.out_value)} {literal}
										<script>
										_CaptionTransitions_{/literal}{$xprtslideblock.id_xprtsliderblock|intval}["{$layer.out_lavel}"] = {$layer.out_value};{literal}
										</script>{/literal}
										{/if}
											{if isset($layer.layer_type) && !empty($layer.layer_type) && $layer.layer_type == 'text'}
											
									<a href="{if $slide.slideurl<>'' && $slide.slideurl<>'#'}{$slide.slideurl}{else}#slide{/if}">
								        	    <div class="xprt_layer xprt_layer_item_{$layer.id_xprthomesliderlayer} {if isset($layer.class) && !empty($layer.class)}{$layer.class}{/if}" data-u="caption" data-t="{if isset($layer.in_lavel) && !empty($layer.in_lavel)}{$layer.in_lavel}{else}T{/if}" data-t2="{if isset($layer.out_lavel) && !empty($layer.out_lavel)}{$layer.out_lavel}{else}L{/if}" data-d="{if isset($layer.delay) && !empty($layer.delay)}{$layer.delay}{/if}" style="">
									        	    <div class="xprt_single_layer {$slide.design_style}">
										        	    <div class="xprt_single_layer_inner">
											        	{if isset($layer.layertext) && !empty($layer.layertext)}{$layer.layertext}{/if}
										        	    </div>
									        	    </div>
								        	    </div>
									</a>
							        	    {elseif isset($layer.layer_type) && !empty($layer.layer_type) && $layer.layer_type == 'image'}
								        	    <div>
										        	{if isset($layer.text) && !empty($layer.text)}
												
											<a href="{if $slide.slideurl<>'' && $slide.slideurl<>'#'}{$slide.slideurl}{else}#slide{/if}">
										        		<img data-u="caption" class="xprt_layer xprt_layer_item_{$layer.id_xprthomesliderlayer} {$layer.design_style} {if isset($layer.class) && !empty($layer.class)}{$layer.class}{/if}" data-t="{if isset($layer.in_lavel) && !empty($layer.in_lavel)}{$layer.in_lavel}{else}T{/if}" data-t2="{if isset($layer.out_lavel) && !empty($layer.out_lavel)}{$layer.out_lavel}{else}L{/if}" src="{$link->getMediaLink("`$smarty.const._MODULE_DIR_`xprtsliderblock/images/`$layer.text`")}" data-d="{if isset($layer.delay) && !empty($layer.delay)}{$layer.delay}{/if}" style="" alt=""/>
											</a>
										        	{/if}
								        	    </div>
							        	    {/if}
						        	    {/foreach}
						        	{/if}
					        	</div>
						{/foreach}
					{/if}
		        </div>
		        {if isset($xprtslideblock.sliderbullet) && $xprtslideblock.sliderbullet != 'none'}
			        <div data-u="navigator" class="xprt_navigator {$xprtslideblock.sliderbullet}" {* style="bottom: 16px; right: 6px;" *}>
			            <div data-u="prototype"></div>
			        </div>
		        {/if}
		        {if isset($xprtslideblock.sliderarrow) && $xprtslideblock.sliderarrow != 'none'}
			        <div class="xprt_arrow_nav {$xprtslideblock.sliderarrow}">
			        	<span data-u="arrowleft" class="{$xprtslideblock.sliderarrow}l xprt_arrow_left" {* style="position: absolute;top: 0px;	left: 8px;	width: 45px; height: 45px;" *}></span>
			        	<span data-u="arrowright" class="{$xprtslideblock.sliderarrow}r xprt_arrow_right" {* style="position: absolute;top: 0px;	right: 8px; width: 45px; height: 45px;" *}></span>
			        </div>
		        {/if}
		    </div>
	</div>
</div>
{literal}
<script>
	$(document).ready(function(){
	var sliderHeight = {/literal}{$xprtslideblock.sliderheight}{literal};
	var forcefullwidth = false;
	{/literal}
	{if isset($xprtslideblock.forcefullwidth)}
		{literal}forcefullwidth = {/literal}{$xprtslideblock.forcefullwidth|intval};
	{/if}
	{literal}
            var options = {
            	$AutoPlaySteps: 1,
            	$LazyLoading: 3,
                $FillMode: 2,                                       //[Optional] The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, 5 contain for large image, actual size for small image, default value is 0
                $AutoPlay:  {/literal}{$xprtslideblock.autoplay|intval}, {literal}                            //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlayInterval:  {/literal}{$xprtslideblock.betweenslidedelay|intval}, {literal}                           //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 0,                                   //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideEasing: $JssorEasing$.$EaseOutQuint,          //[Optional] Specifies easing for right to left animation, default value is $JssorEasing$.$EaseOutQuad
                $SlideDuration: {/literal}{$xprtslideblock.sliderspeed|intval}, {literal}                              //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                // $SlideWidth: 1920,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                // $SlideHeight: 678,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
                $CaptionSliderOptions: {                            //[Optional] Options which specifies how to animate caption
                    $Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
                    $CaptionTransitions: _CaptionTransitions_{/literal}{$xprtslideblock.id_xprtsliderblock|intval}{literal},     
                      //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
                    $PlayInMode: 3,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
                    $PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
                },
                $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                    $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                    $Transitions: _SlideshowTransitions_{/literal}{$xprtslideblock.id_xprtsliderblock|intval}{literal},             //[Required] An array of slideshow transitions to play slideshow
                    $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                    $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                },
                $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 12,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 12,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                },
                $ArrowNavigatorOptions: {                       //[Optional] Options to specify and enable arrow navigator or not
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };


        	var main_slider1 = new $JssorSlider$("xprt_slider_container", options);
            
            //e.g. jssor_slider1.$On($JssorSlider$.$EVT_PARK,function(slideIndex,fromIndex){});
            //$EVT_LOAD_START       function(slideIndex)

            // main_slider1.$On($JssorSlider$.$EVT_LOAD_START,function(slideIndex){
            // 	$('#xprt_slider_container').addClass('loading');
            // 	// console.log('slideIndex');
            // })

            main_slider1.$On($JssorSlider$.$EVT_LOAD_END,function(slideIndex){
            	$('#xprt_slider_container').removeClass('loading');
            	// console.log('slideIndex');
            });

            
            function ScaleSliderFull() {
                var bodyWidth = $(window).width();
                var parentPosLeft = $('#xprt_slider_container').parent().offset().left;
                if (typeof bodyWidth != 'undefined' && typeof parentPosLeft != 'undefined'){
                    main_slider1.$SetScaleWidth(bodyWidth);
					
                    $('#xprt_slider_container').css({'left': -(parentPosLeft) + 'px'});
                }
                else{
                    window.setTimeout(ScaleSlider, 30);
                }
            };
            function ScaleSlider() {
                var parentWidth = $('#xprt_slider_container').parent().width();
				console.log(parentWidth);
                if (parentWidth) {
                    //main_slider1.$ScaleWidth(parentWidth);
					main_slider1.$ScaleWidth(parentWidth);
					if (navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
						main_slider1.$ScaleHeight(300);
					}
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            };
            if( forcefullwidth != false){
        		ScaleSliderFull();	
            	//$(window).bind("load", ScaleSliderFull);
            	$(window).bind("resize", ScaleSliderFull);
            	$(window).bind("orientationchange", ScaleSliderFull);
            	if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
            	    $(window).bind('resize', ScaleSliderFull);
            	};
            	if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
            	   $(window).bind("orientationchange", ScaleSliderFull);
            	};
            }
            else{
            	ScaleSlider();
	            $(window).bind("load", ScaleSlider);
	            $(window).bind("resize", ScaleSlider);
	            $(window).bind("orientationchange", ScaleSlider);
	            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
	                $(window).bind('resize', ScaleSlider);
	            };
	            if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
	               $(window).bind("orientationchange", ScaleSlider);
	            };
            };	
}); // doc ready
</script>
{/literal}
{/foreach}
{/if}