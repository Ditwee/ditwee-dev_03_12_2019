<?php

if (!defined('_PS_VERSION_'))
	exit;
include_once(_PS_MODULE_DIR_.'xprtsliderblock/classes/xprtsliderblockclass.php');
include_once(_PS_MODULE_DIR_.'xprtsliderblock/classes/xprthomesliderlayer.php');
include_once(_PS_MODULE_DIR_.'xprtsliderblock/classes/xprtslidesclass.php');
include_once(_PS_MODULE_DIR_.'xprtsliderblock/classes/xprtslideseffectclass.php');
include_once(_PS_MODULE_DIR_.'xprtsliderblock/classes/xprtslideeffectclass.php');
class xprtsliderblock extends Module
{
	private $_html = '';
	private $_hooks = '';
	public static $module_name = 'xprtsliderblock';
	public static $tablename = 'xprtsliderblock';
	public static $classname = 'xprtsliderblockclass';
	private static $device_desc = array(
			array(
				'name' => 'xs',
				'title' => 'Extra Small Device',
				'tooltip' => '',
				'column' => array(1,2,3),
			),
			array(
				'name' => 'sm',
				'title' => 'Small Device',
				'tooltip' => '',
				'column' => array(2,3,4),
			),
			array(
				'name' => 'md',
				'title' => 'Medium Device',
				'tooltip' => '',
				'column' => array(3,4,5,6),
			),
			array(
				'name' => 'lg',
				'title' => 'Large Device',
				'tooltip' => '',
				'column' => array(3,4,5,6),
			),
	);
	public $all_tabs = array(
		array(
	        'class_name' => 'Adminxprthomeslider',
	        'id_parent' => -1,
	        'name' => 'home slider',
		),
	);
	public function __construct()
	{
		$this->name = 'xprtsliderblock';
		$this->tab = 'front_office_features';
		$this->version = '1.0.1';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		$this->secure_key = Tools::encrypt($this->name);
		$this->bootstrap = true;
		$this->fonts_files = dirname(__FILE__).'/fonts/xprt_google_fonts.txt';
		$this->css_files = 'generated_css';
		parent::__construct();
		$this->initializeHooks();
		$this->displayName = $this->l('Xpert Idea Prestashop Slider Modules');
		$this->description = $this->l('Xpert Idea Prestashop Slider Module.');
		$this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->HookRegister()
			|| !$this->createTables()
			|| !$this->Register_Tabs()
		 	|| !$this->DummyData()
			// || !$this->xpertsampledata()
		)
			return false;	
		else	
			return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
			|| !$this->deleteTables()
			|| !$this->UnRegister_Tabs()
		)
			return false;	
		else	
			return true;
	}
	private function HookRegister(){
		if(isset($this->_hooks) && !empty($this->_hooks)){
			foreach ($this->_hooks as $_hooks_values) {
				if(isset($_hooks_values) && !empty($_hooks_values)){
					foreach ($_hooks_values as $_hook) {
						$this->registerHook($_hook);
					}
				}
			}
		}
		return true;
	}
	private function GetHookarray(){
		$results = array();
		if(isset($this->_hooks['display']) && !empty($this->_hooks['display'])){
			$i = 0;
			foreach ($this->_hooks['display'] as $_hook) {
				$results[$i]['id'] = $_hook;
				$results[$i]['name'] = $_hook;
				$i++;
			}
		}
		return $results;
	}
	private function SliderBulletStyle(){
		$results = array();
		$results[0]['id'] = "none";
		$results[0]['name'] = "Disabled Bullet";

		$results[1]['id'] = "default";
		$results[1]['name'] = "Default Bullet Style";

		$results[2]['id'] = "jssorb01";
		$results[2]['name'] = "Bullet Style One";

		$results[3]['id'] = "jssorb02";
		$results[3]['name'] = "Bullet Style two";

		$results[4]['id'] = "jssorb03";
		$results[4]['name'] = "Bullet Style Three";

		$results[5]['id'] = "jssorb05";
		$results[5]['name'] = "Bullet Style Five";

		$results[6]['id'] = "jssorb06";
		$results[6]['name'] = "Bullet Style Six";

		$results[7]['id'] = "jssorb07";
		$results[7]['name'] = "Bullet Style Seven";

		$results[8]['id'] = "jssorb09";
		$results[8]['name'] = "Bullet Style Nine";

		$results[9]['id'] = "jssorb10";
		$results[9]['name'] = "Bullet Style Ten";

		$results[10]['id'] = "jssorb11";
		$results[10]['name'] = "Bullet Style Eleven";

		$results[11]['id'] = "jssorb12";
		$results[11]['name'] = "Bullet Style Twelve";

		$results[12]['id'] = "jssorb13";
		$results[12]['name'] = "Bullet Style Thirteen";

		$results[13]['id'] = "jssorb14";
		$results[13]['name'] = "Bullet Style Fourteen";

		$results[14]['id'] = "jssorb16";
		$results[14]['name'] = "Bullet Style Sixteen";

		$results[15]['id'] = "jssorb17";
		$results[15]['name'] = "Bullet Style Seventeen";

		$results[16]['id'] = "jssorb18";
		$results[16]['name'] = "Bullet Style Eightteen";

		$results[17]['id'] = "jssorb20";
		$results[17]['name'] = "Bullet Style Twenty";

		$results[18]['id'] = "jssorb21";
		$results[18]['name'] = "Bullet Style Twenty One";

		$results[19]['id'] = "jssorb22";
		$results[19]['name'] = "Bullet Style Twenty Two";

		return $results;
	}
	private function SliderThumbStyle(){
		$results = array();
		$results[0]['id'] = "default";
		$results[0]['name'] = "Thumbnail Style default";

		$results[1]['id'] = "one";
		$results[1]['name'] = "Thumbnail Style One";

		$results[2]['id'] = "two";
		$results[2]['name'] = "Thumbnail Style two";

		$results[3]['id'] = "three";
		$results[3]['name'] = "Thumbnail Style Three";

		$results[4]['id'] = "four";
		$results[4]['name'] = "Thumbnail Style Four";

		$results[5]['id'] = "five";
		$results[5]['name'] = "Thumbnail Style Five";

		$results[6]['id'] = "six";
		$results[6]['name'] = "Thumbnail Style Six";

		$results[7]['id'] = "seven";
		$results[7]['name'] = "Thumbnail Style Seven";

		return $results;
	}
	private function SliderArrowStyle(){
		$results = array();
		$results[0]['id'] = "none";
		$results[0]['name'] = "Disabled Arrow";

		$results[1]['id'] = "default";
		$results[1]['name'] = "Arrow Style default";

		$results[2]['id'] = "jssora01";
		$results[2]['name'] = "Arrow Style One";

		$results[3]['id'] = "jssora02";
		$results[3]['name'] = "Arrow Style two";

		$results[4]['id'] = "jssora03";
		$results[4]['name'] = "Arrow Style Three";

		$results[5]['id'] = "jssora04";
		$results[5]['name'] = "Arrow Style Four";

		$results[6]['id'] = "jssora05";
		$results[6]['name'] = "Arrow Style Five";

		$results[7]['id'] = "jssora06";
		$results[7]['name'] = "Arrow Style Six";

		$results[8]['id'] = "jssora07";
		$results[8]['name'] = "Arrow Style Seven";

		$results[9]['id'] = "jssora08";
		$results[9]['name'] = "Arrow Style Eight";

		$results[10]['id'] = "jssora09";
		$results[10]['name'] = "Arrow Style Nine";

		$results[11]['id'] = "jssora10";
		$results[11]['name'] = "Arrow Style Ten";

		$results[12]['id'] = "jssora11";
		$results[12]['name'] = "Arrow Style Eleven";

		$results[13]['id'] = "jssora12";
		$results[13]['name'] = "Arrow Style twelve";

		$results[14]['id'] = "jssora13";
		$results[14]['name'] = "Arrow Style Thirteen";

		$results[15]['id'] = "jssora14";
		$results[15]['name'] = "Arrow Style Fourteen";

		$results[16]['id'] = "jssora15";
		$results[16]['name'] = "Arrow Style Fifteen";

		$results[17]['id'] = "jssora16";
		$results[17]['name'] = "Arrow Style Sixteen";

		$results[18]['id'] = "jssora17";
		$results[18]['name'] = "Arrow Style Seventeen";

		$results[19]['id'] = "jssora18";
		$results[19]['name'] = "Arrow Style Eighteen";

		$results[20]['id'] = "jssora19";
		$results[20]['name'] = "Arrow Style Nineteen";

		$results[21]['id'] = "jssora20";
		$results[21]['name'] = "Arrow Style Twenty";

		$results[22]['id'] = "jssora21";
		$results[22]['name'] = "Arrow Style Twenty One";

		$results[23]['id'] = "jssora22";
		$results[23]['name'] = "Arrow Style Twenty Two";

		return $results;
	}
	private function initializeHooks()
    {
        $this->_hooks = array(
        	'display' => array(
        		'displayLeftColumn',
        		'displayTopColumn',
        		'displayHomeTop',
        		'displayHome',
        		'displayHomeTabContent',
        		'displayHomeMiddle',
        		'displayHomeBottomLeft',
        		'displayHomeBottom',
        		'displayHomeFullWidthBottom',
        	),
        	'action' => array(
        		'displayHeader',
        		'displayHomeTab',
        		'displaybackofficeheader',
        	),
        );
    }
	public function Register_Tabs()
	{
		$tabs_lists = array();
        $langs = Language::getLanguages();
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
    	if(isset($this->all_tabs) && !empty($this->all_tabs)){
    		foreach ($this->all_tabs as $tab_list)
    		{
    		    $tab_listobj = new Tab();
    		    $tab_listobj->class_name = $tab_list['class_name'];
    		    $tab_listobj->id_parent = -1;
    		    if(isset($tab_list['module']) && !empty($tab_list['module'])){
    		    	$tab_listobj->module = $tab_list['module'];
    		    }else{
    		    	$tab_listobj->module = $this->name;
    		    }
    		    foreach($langs as $l)
    		    {
    		    	$tab_listobj->name[$l['id_lang']] = $this->l($tab_list['name']);
    		    }
    		    $tab_listobj->save();
    		}
    	}
        return true;
    }
	public function UnRegister_Tabs()
	{
		$tabs_lists = array();
			if(isset($this->all_tabs) && !empty($this->all_tabs))
	        foreach($this->all_tabs as $tab_list){
	        	$tab_list_id = Tab::getIdFromClassName($tab_list['class_name']);
	            if(isset($tab_list_id) && !empty($tab_list_id)){
	                $tabobj = new Tab($tab_list_id);
	                $tabobj->delete();
	            }
	        }
        return true;
	}
	public function CurrentUrl()
	{
		$token = Tools::getAdminTokenLite('AdminModules');
		$current_index = 'index.php?controller=AdminModules&configure='.$this->name.'&tab_module=front_office_features&module_name='.$this->name.'&token='.$token.'';
		return $current_index;
	}
	public function SliderURL($id_slider = null)
	{
		if($id_slider == null)
			return $this->CurrentUrl();
		$token = Tools::getAdminTokenLite('AdminModules');
		$current_index = 'index.php?controller=AdminModules&configure='.$this->name.'&tab_module=front_office_features&module_name='.$this->name.'&token='.$token.'';
		$current_index .= '&id_xprtsliderblock='.(int)$id_slider.'&updatexprtsliderblock=1';
		return $current_index;
	}
	public function SlideSURL($id_slider = null, $id_slide = null)
	{
		if($id_slider == null || $id_slide)
			return $this->CurrentUrl();
		$token = Tools::getAdminTokenLite('AdminModules');
		$current_index = 'index.php?controller=AdminModules&configure='.$this->name.'&tab_module=front_office_features&module_name='.$this->name.'&token='.$token.'';
		$current_index .= '&id_xprtslides='.(int)$id_slide.'&id_xpertslider='.(int)$id_slider.'&addSlide=1';
		return $current_index;
	}
	public function getContent()
	{
		$this->_postProcess();
		if(Tools::getValue("update".$this->name) && Tools::getValue("id_".$this->name)){
			$this->_html .= $this->renderAddForm();
			$this->_html .= $this->headerSlidesHTML();
		}elseif(Tools::getValue("add".$this->name)){
			$this->_html .= $this->renderAddForm();
			$this->_html .= $this->headerSlidesHTML();
		}elseif(Tools::getValue("addxprtsldeffect") || Tools::getValue('updatexprtslideeffect')){
			$this->_html .= $this->renderSlideEffectForm();
		}elseif(Tools::getValue("addxprtlayereffect") || Tools::getValue('updatexprtlayereffect')){
			$this->_html .= $this->renderForm();
		}elseif(Tools::getValue("addSlide")){
			$this->_html .= $this->renderSlideAddForm();
		}else{
			$this->_html .= $this->renderList();
			$this->_html .= $this->rendereffectList();
			$this->_html .= $this->renderSlideList();
			$this->_html .= $this->headerHTML();
			$this->_html .= $this->headerEffectHTML();
			$this->_html .= $this->headerSlideHTML();
		}
		return $this->_html;
	}
	public function processImage($file_name = null){
		if($file_name == null)
			return false;
	    if(isset($_FILES[$file_name]) && isset($_FILES[$file_name]['tmp_name']) && !empty($_FILES[$file_name]['tmp_name'])){
            $ext = substr($_FILES[$file_name]['name'], strrpos($_FILES[$file_name]['name'], '.') + 1);
            $basename_file_name = basename($_FILES[$file_name]["name"]);
            $strlen = strlen($basename_file_name);
            $strlen_ext = strlen($ext);
            $basename_file_name = substr($basename_file_name,0,($strlen-$strlen_ext));
            $link_rewrite_file_name = Tools::link_rewrite($basename_file_name);
            $file_orgname = $link_rewrite_file_name.'.'.$ext;
            $path = _PS_MODULE_DIR_.self::$module_name.'/images/' . $file_orgname;
            if(!move_uploaded_file($_FILES[$file_name]['tmp_name'],$path))
                return false;         
            else
                return $file_orgname;   
	    }else{
	    	return false;
	    }
	}
	public static function PageException($exceptions = NULL)
	{
		if($exceptions == NULL)
			return false;
		$exceptions = explode(",",$exceptions);
		$page_name = Context::getContext()->controller->php_self;
		$this_arr = array();
		$this_arr[] = 'all_page';
		$this_arr[] = $page_name;
		if($page_name == 'category'){
			$id_category = Tools::getvalue('id_category');
			$this_arr[] = 'cat_'.$id_category;
		}elseif($page_name == 'product'){
			$id_product = Tools::getvalue('id_product');
			$this_arr[] = 'prd_'.$id_product;
			// Start Get Product Category
			$prd_cat_sql = 'SELECT cp.`id_category` AS id
			    FROM `'._DB_PREFIX_.'category_product` cp
			    LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = cp.id_category)
			    '.Shop::addSqlAssociation('category', 'c').'
			    WHERE cp.`id_product` = '.(int)$id_product;
			$prd_catresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_cat_sql);
			if(isset($prd_catresults) && !empty($prd_catresults))
			{
			    foreach($prd_catresults as $prd_catresult)
			    {
			        $this_arr[] = 'prdcat_'.$prd_catresult['id'];
			    }
			}
			// END Get Product Category
			// Start Get Product Manufacturer
			$prd_man_sql = 'SELECT `id_manufacturer` AS id FROM `'._DB_PREFIX_.'product` WHERE `id_product` = '.(int)$id_product;
			$prd_manresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_man_sql);
			if(isset($prd_manresults) && !empty($prd_manresults))
			{
			    foreach($prd_manresults as $prd_manresult)
			    {
			        $this_arr[] = 'prdman_'.$prd_manresult['id'];
			    }
			}
			// END Get Product Manufacturer
			// Start Get Product SupplierS
			$prd_sup_sql = "SELECT `id_supplier` AS id FROM `"._DB_PREFIX_."product_supplier` WHERE `id_product` = ".(int)$id_product." GROUP BY `id_supplier`";
			$prd_supresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_sup_sql);
			if(isset($prd_supresults) && !empty($prd_supresults))
			{
			    foreach($prd_supresults as $prd_supresult)
			    {
			        $this_arr[] = 'prdsup_'.$prd_supresult['id'];
			    }
			}
			// END Get Product SupplierS
		}elseif($page_name == 'cms'){
			$id_cms = Tools::getvalue('id_cms');
			$this_arr[] = 'cms_'.$id_cms;
		}elseif($page_name == 'manufacturer'){
			$id_manufacturer = Tools::getvalue('id_manufacturer');
			$this_arr[] = 'man_'.$id_manufacturer;
		}elseif($page_name == 'supplier'){
			$id_supplier = Tools::getvalue('id_supplier');
			$this_arr[] = 'sup_'.$id_supplier;
		}
		if(isset($this_arr)){
			foreach ($this_arr as $this_arr_val) {
				if(in_array($this_arr_val,$exceptions))
					return true;
			}
		}
		return false;
	}
	private function _postProcess()
	{
		$errors = array();
		$this->context->controller->addJqueryPlugin('select2');
		$shop_context = Shop::getContext();
		if(Tools::isSubmit('submit'.$this->name))
		{
			$id = Tools::getValue('id_'.$this->name);
			if(isset($id) && !empty($id)){
				$xprtobj = new self::$classname($id);
			}else{
				$xprtobj = new self::$classname();
			}
			// start save
			$languages = Language::getLanguages(false);
			$fields_form = $this->initFieldsForm();
			if(isset($fields_form['form']['input']) && !empty($fields_form['form']['input'])){
				$content_group = array();
				foreach ($fields_form['form']['input'] as $field) {
					if(isset($field['expandstyle']) && $field['expandstyle'] == true){
						$content_group["put-cls-".$field['name']] = Tools::getValue("put-cls-".$field['name']);
						$content_group["put-mar-".$field['name']] = Tools::getValue("put-mar-".$field['name']);
						$content_group["put-pad-".$field['name']] = Tools::getValue("put-pad-".$field['name']);
					}
					if(isset($field['type']) && $field['type'] == 'device'){
					$device_data = array();
						if(isset(self::$device_desc) && !empty(self::$device_desc)){
							foreach (self::$device_desc as $dvce_des) {
								$device_data[$field['name'].'_'.$dvce_des['name']] = Tools::getValue($field['name'].'_'.$dvce_des['name']);
							}
							$xprtobj->{$field['name']} = Tools::jsonEncode($device_data);
						}
					}elseif(isset($field['type']) && $field['type'] == 'file_lang'){
						foreach ($languages as $lang)
						{
							if(isset($_FILES[$field['name']."_".$lang['id_lang']]) && isset($_FILES[$field['name']."_".$lang['id_lang']]['tmp_name']) && !empty($_FILES[$field['name']."_".$lang['id_lang']]['tmp_name'])){
								$xprtobj->{$field['name']}[$lang['id_lang']] = $this->processImage($field['name']."_".$lang['id_lang']);
							}else{
								$xprtobj->{$field['name']}[$lang['id_lang']] = (isset($xprtobj->{$field['name']}[$lang['id_lang']]) && !empty($xprtobj->{$field['name']}[$lang['id_lang']])) ? $xprtobj->{$field['name']}[$lang['id_lang']] : "noimage.jpg";
							}
						}
					}elseif(isset($field['type']) && $field['type'] == 'file'){
						if(isset($_FILES[$field['name']]) && isset($_FILES[$field['name']]['tmp_name']) && !empty($_FILES[$field['name']]['tmp_name'])){
							$xprtobj->{$field['name']} = $this->processImage($field['name']);
						}else{
							$xprtobj->{$field['name']} = (isset($xprtobj->{$field['name']}) && !empty($xprtobj->{$field['name']})) ? $xprtobj->{$field['name']} : "";
						}
					}else{
						// start
						if(isset($field['group']) && $field['group'] == "content"){
							// start
							if(isset($field['type']) && $field['type'] == 'file_lang'){
								foreach ($languages as $lang)
								{
									if(isset($_FILES[$field['name']."_".$lang['id_lang']]) && isset($_FILES[$field['name']."_".$lang['id_lang']]['tmp_name']) && !empty($_FILES[$field['name']."_".$lang['id_lang']]['tmp_name'])){
										$content_group[$field['name']][$lang['id_lang']] = $this->processImage($field['name']."_".$lang['id_lang']);
									}else{
										if(isset($xprtobj->content) && !empty($xprtobj->content)){
											$contentdecode = Tools::jsonDecode($xprtobj->content);
											$content_group[$field['name']][$lang['id_lang']] = (isset($contentdecode[$field['name']][$lang['id_lang']]) && !empty($contentdecode[$field['name']][$lang['id_lang']])) ? $contentdecode[$field['name']][$lang['id_lang']] : "";
										}else{
											$content_group[$field['name']][$lang['id_lang']] = "noimage.jpg";
										}
									}
								}
							}elseif(isset($field['type']) && $field['type'] == 'file'){
								if(isset($_FILES[$field['name']]) && isset($_FILES[$field['name']]['tmp_name']) && !empty($_FILES[$field['name']]['tmp_name'])){
									$content_group[$field['name']] = $this->processImage($field['name']);
								}else{
									if(isset($xprtobj->content) && !empty($xprtobj->content)){
										$contentdecode = Tools::jsonDecode($xprtobj->content);
										$content_group[$field['name']] = (isset($contentdecode[$field['name']]) && !empty($contentdecode[$field['name']])) ? $contentdecode[$field['name']] : "";
									}else{
										$content_group[$field['name']] = "";
									}
								}
							}else{
								if(isset($field['lang']) && $field['lang'] == true){
									foreach ($languages as $lang)
									{
										if(isset($field['name']) && !empty($field['name'])){
											$content_group[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
										}
									}
								}else{
									if(isset($field['name']) && !empty($field['name'])){
										$content_group[$field['name']] = Tools::getValue($field['name']);
									}
								}
							}
							// end
						}else{
							if(isset($field['lang']) && $field['lang'] == true){
								foreach ($languages as $lang)
								{
									if(isset($field['name']) && !empty($field['name'])){
										$xprtobj->{$field['name']}[$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
									}
								}
							}else{
								if(isset($field['name']) && !empty($field['name'])){
									$xprtobj->{$field['name']} = Tools::getValue($field['name']);
								}
							}
						}
						// End
					}
				}
			}
			// end save
			if(isset($content_group) && !empty($content_group)){
				$xprtobj->content = Tools::jsonEncode($content_group);
			}else{
				$xprtobj->content = "";
			}
			$res = $xprtobj->save();
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully Updated')) : $this->displayError($this->l('The Values could not be Updated.')));
			Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif (Tools::isSubmit('changeStatus') && Tools::isSubmit('id_'.$this->name))
		{
			$xprtobj = new self::$classname((int)Tools::getValue('id_'.$this->name));
			if ($xprtobj->active == 0)
				$xprtobj->active = 1;
			else
				$xprtobj->active = 0;
			$res = $xprtobj->update();
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully updated')) : $this->displayError($this->l('The Values could not be updated.')));
			Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif(Tools::isSubmit('deleteslider') && Tools::isSubmit('delete_id_'.$this->name))
		{
			$xprtobj = new self::$classname((int)Tools::getValue('delete_id_'.$this->name));
			$res = $xprtobj->delete();
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully Deleted')) : $this->displayError($this->l('The Values could not be Deleted.')));
			Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif (Tools::isSubmit('updateblocklistsPosition'))
		{
			$block_lists = array();
			if (Tools::getValue('block_lists'))
			{
				$block_lists = Tools::getValue('block_lists');
				foreach ($block_lists as $position => $id_i)
				{
					Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.self::$tablename.'` SET `position` = '.(int)$position.' WHERE `id_'.self::$tablename.'` = '.(int)$id_i );
				}
			}
			// Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif (Tools::isSubmit('updateeffectslistsPosition'))
		{
			$block_lists = array();
			if (Tools::getValue('xprtlayereffect_lists'))
			{
				$block_lists = Tools::getValue('xprtlayereffect_lists');
				foreach ($block_lists as $position => $id_i)
				{
					Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'xprtslideseffect` SET `position` = '.(int)$position.' WHERE `id_xprtslideseffect'.'` = '.(int)$id_i
					);
				}
			}
			die(1);
			// Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif (Tools::isSubmit('updateSlidelistsPosition'))
		{
			$block_lists = array();
			if (Tools::getValue('slides'))
			{
				$block_lists = Tools::getValue('slides');
				foreach ($block_lists as $position => $id_i)
				{
					Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'xprtslides` SET `position` = '.(int)$position.' WHERE `id_xprtslides` = '.(int)$id_i);
				}
			}
			// Tools::redirectAdmin($this->SliderURL());
		}
		elseif (Tools::isSubmit('updateSlidelistPosition'))
		{
			$block_lists = array();
			if (Tools::getValue('xprtslideeffect_lists'))
			{
				$block_lists = Tools::getValue('xprtslideeffect_lists');
				foreach ($block_lists as $position => $id_i)
				{
					Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'xprtslideeffect` SET `position` = '.(int)$position.' WHERE `id_xprtslideeffect` = '.(int)$id_i);
				}
			}
			// Tools::redirectAdmin($this->SliderURL());
		}
		elseif (Tools::isSubmit('submitxprtslides'))
		{
			// Start slide add
			$id_xprtslides = Tools::getValue('id_xprtslides');
			if(isset($id_xprtslides) && !empty($id_xprtslides)){
				$xprtobjslides = new xprtslidesclass($id_xprtslides);
			}else{
				$xprtobjslides = new xprtslidesclass();
			}
			// start save
			$languages = Language::getLanguages(false);
			$fields_form = $this->initSlidesFieldsForm();
			if(isset($fields_form['form']['input']) && !empty($fields_form['form']['input'])){
				$content_group = array();
				foreach ($fields_form['form']['input'] as $field) {
					if(isset($field['expandstyle']) && $field['expandstyle'] == true){
						$content_group["put-cls-".$field['name']] = Tools::getValue("put-cls-".$field['name']);
						$content_group["put-mar-".$field['name']] = Tools::getValue("put-mar-".$field['name']);
						$content_group["put-pad-".$field['name']] = Tools::getValue("put-pad-".$field['name']);
					}
					if(isset($field['type']) && $field['type'] == 'device'){
					$device_data = array();
						if(isset(self::$device_desc) && !empty(self::$device_desc)){
							foreach (self::$device_desc as $dvce_des) {
								$device_data[$field['name'].'_'.$dvce_des['name']] = Tools::getValue($field['name'].'_'.$dvce_des['name']);
							}
							$xprtobjslides->{$field['name']} = Tools::jsonEncode($device_data);
						}
					}elseif(isset($field['type']) && $field['type'] == 'file_lang'){
						foreach ($languages as $lang)
						{
							if(isset($_FILES[$field['name']."_".$lang['id_lang']]) && isset($_FILES[$field['name']."_".$lang['id_lang']]['tmp_name']) && !empty($_FILES[$field['name']."_".$lang['id_lang']]['tmp_name'])){
								$xprtobjslides->{$field['name']}[$lang['id_lang']] = $this->processImage($field['name']."_".$lang['id_lang']);
							}else{
								$xprtobjslides->{$field['name']}[$lang['id_lang']] = (isset($xprtobjslides->{$field['name']}[$lang['id_lang']]) && !empty($xprtobjslides->{$field['name']}[$lang['id_lang']])) ? $xprtobjslides->{$field['name']}[$lang['id_lang']] : "noimage.jpg";
							}
						}
					}elseif(isset($field['type']) && $field['type'] == 'file'){
						if(isset($_FILES[$field['name']]) && isset($_FILES[$field['name']]['tmp_name']) && !empty($_FILES[$field['name']]['tmp_name'])){
							$xprtobjslides->{$field['name']} = $this->processImage($field['name']);
						}else{
							$xprtobjslides->{$field['name']} = (isset($xprtobjslides->{$field['name']}) && !empty($xprtobjslides->{$field['name']})) ? $xprtobjslides->{$field['name']} : "";
						}
					}else{
						// start
						if(isset($field['group']) && $field['group'] == "content"){
							// start
							if(isset($field['type']) && $field['type'] == 'file_lang'){
								foreach ($languages as $lang)
								{
									if(isset($_FILES[$field['name']."_".$lang['id_lang']]) && isset($_FILES[$field['name']."_".$lang['id_lang']]['tmp_name']) && !empty($_FILES[$field['name']."_".$lang['id_lang']]['tmp_name'])){
										$content_group[$field['name']][$lang['id_lang']] = $this->processImage($field['name']."_".$lang['id_lang']);
									}else{
										if(isset($xprtobj->content) && !empty($xprtobj->content)){
											$contentdecode = Tools::jsonDecode($xprtobj->content);
											$content_group[$field['name']][$lang['id_lang']] = (isset($contentdecode[$field['name']][$lang['id_lang']]) && !empty($contentdecode[$field['name']][$lang['id_lang']])) ? $contentdecode[$field['name']][$lang['id_lang']] : "";
										}else{
											$content_group[$field['name']][$lang['id_lang']] = "noimage.jpg";
										}
									}
								}
							}elseif(isset($field['type']) && $field['type'] == 'file'){
								$content_group[$field['name']] = $this->processImage($field['name']);
								if(isset($_FILES[$field['name']]) && isset($_FILES[$field['name']]['tmp_name']) && !empty($_FILES[$field['name']]['tmp_name'])){
									$content_group[$field['name']] = $this->processImage($field['name']);
								}else{
									if(isset($xprtobjslides->content) && !empty($xprtobjslides->content)){
										$contentdecode = Tools::jsonDecode($xprtobjslides->content);
										$content_group[$field['name']] = (isset($contentdecode[$field['name']]) && !empty($contentdecode[$field['name']])) ? $contentdecode[$field['name']] : "";
									}else{
										$content_group[$field['name']] = "";
									}
								}
							}else{
								if(isset($field['lang']) && $field['lang'] == true){
									foreach ($languages as $lang)
									{
										if(isset($field['name']) && !empty($field['name'])){
											$content_group[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
										}
									}
								}else{
									if(isset($field['name']) && !empty($field['name'])){
										$content_group[$field['name']] = Tools::getValue($field['name']);
									}
								}
							}
							// end
						}else{
							if(isset($field['lang']) && $field['lang'] == true){
								foreach ($languages as $lang)
								{
									if(isset($field['name']) && !empty($field['name'])){
										$xprtobjslides->{$field['name']}[$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang']);
									}
								}
							}else{
								if(isset($field['name']) && !empty($field['name'])){
									$xprtobjslides->{$field['name']} = Tools::getValue($field['name']);
								}
							}
						}
						// End
					}
				}
			}
			// end save
			if(isset($content_group) && !empty($content_group)){
				$xprtobjslides->content = Tools::jsonEncode($content_group);
			}else{
				$xprtobjslides->content = "";
			}
			$res = $xprtobjslides->save();
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully Updated')) : $this->displayError($this->l('The Values could not be Updated.')));
			Tools::redirectAdmin($this->SliderURL(Tools::getValue('id_slider')));
			// End slide add
		}
		elseif (Tools::isSubmit('editxprtslides'))
		{
			$id_xprtslides = Tools::getValue('delete_id_xprtslides');
			if(isset($id_xprtslides) && !empty($id_xprtslides)){
				$xprtobjslides = new xprtslidesclass($id_xprtslides);
				$res = $xprtobjslides->delete();
				$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully Deleted')) : $this->displayError($this->l('The Values could not be Deleted.')));
			}
			Tools::redirectAdmin($this->SliderURL(Tools::getValue('id_slider')));
		}
		elseif (Tools::isSubmit('deletexprtlayereffect') && Tools::getValue('delete_id_xprtslideseffect'))
		{
			$id_xprtslideseffect = Tools::getValue('delete_id_xprtslideseffect');
			if(isset($id_xprtslideseffect) && !empty($id_xprtslideseffect)){
				$xprtslideseffect = new xprtslideseffectclass($id_xprtslideseffect);
				$res = $xprtslideseffect->delete();
				$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully Deleted')) : $this->displayError($this->l('The Values could not be Deleted.')));
			}
			Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif (Tools::isSubmit('deletexprtslideeffect') && Tools::getValue('delete_id_xprtslideeffect'))
		{
			$id_xprtslideeffect = Tools::getValue('delete_id_xprtslideeffect');
			if(isset($id_xprtslideeffect) && !empty($id_xprtslideeffect)){
				$xprtslideeffect = new xprtslideeffectclass($id_xprtslideeffect);
				$res = $xprtslideeffect->delete();
				$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully Deleted')) : $this->displayError($this->l('The Values could not be Deleted.')));
			}
			Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif(Tools::isSubmit('changeSlideStatus') && Tools::isSubmit('id_xprtslides'))
		{
			$id_xprtslides = Tools::getValue('id_xprtslides');
			if(isset($id_xprtslides) && !empty($id_xprtslides)){
				$xprtobjslides = new xprtslidesclass($id_xprtslides);
				if ($xprtobjslides->active == 0)
					$xprtobjslides->active = 1;
				else
					$xprtobjslides->active = 0;
				$res = $xprtobjslides->update();
				$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully updated')) : $this->displayError($this->l('The Values could not be updated.')));
			}
			Tools::redirectAdmin($this->SliderURL($xprtobjslides->id_slider));
		}
		elseif(Tools::isSubmit('changexprtslideseffect') && Tools::isSubmit('id_xprtslideseffect'))
		{
			$id_xprtslideseffect = Tools::getValue('id_xprtslideseffect');
			if(isset($id_xprtslideseffect) && !empty($id_xprtslideseffect)){
				$xprtslideseffect = new xprtslideseffectclass($id_xprtslideseffect);
				if ($xprtslideseffect->active == 0)
					$xprtslideseffect->active = 1;
				else
					$xprtslideseffect->active = 0;
				$res = $xprtslideseffect->update();
				$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully updated')) : $this->displayError($this->l('The Values could not be updated.')));
			}
			Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif(Tools::isSubmit('changexprtslideeffect') && Tools::isSubmit('id_xprtslideeffect'))
		{
			$id_xprtslideeffect = Tools::getValue('id_xprtslideeffect');
			if(isset($id_xprtslideeffect) && !empty($id_xprtslideeffect)){
				$xprtslideeffect = new xprtslideeffectclass($id_xprtslideeffect);
				if ($xprtslideeffect->active == 0)
					$xprtslideeffect->active = 1;
				else
					$xprtslideeffect->active = 0;
				$res = $xprtslideeffect->update();
				$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully updated')) : $this->displayError($this->l('The Values could not be updated.')));
			}
			Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif(Tools::isSubmit('submitxprtslideseffect'))
		{
			$id_xprtslideseffect = Tools::getValue('id_xprtslideseffect');
			if(isset($id_xprtslideseffect) && !empty($id_xprtslideseffect)){
				$xprtobjslides = new xprtslideseffectclass($id_xprtslideseffect);
			}else{
				$xprtobjslides = new xprtslideseffectclass();
			}
			$xprtobjslides->title = Tools::getValue('title');
			$xprtobjslides->effect = Tools::getValue('stTransition');
			$xprtobjslides->active = Tools::getValue('active');
			$res = $xprtobjslides->save();
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully updated')) : $this->displayError($this->l('The Values could not be updated.')));
			Tools::redirectAdmin($this->CurrentUrl());
		}
		elseif(Tools::isSubmit('submitxprtslideeffect'))
		{
			$id_xprtslideeffect = Tools::getValue('id_xprtslideeffect');
			if(isset($id_xprtslideeffect) && !empty($id_xprtslideeffect)){
				$xprtobjslide = new xprtslideeffectclass($id_xprtslideeffect);
			}else{
				$xprtobjslide = new xprtslideeffectclass();
			}
			$xprtobjslide->title = Tools::getValue('title');
			$xprtobjslide->effect = Tools::getValue('stTransition');
			$xprtobjslide->active = Tools::getValue('active');
			$res = $xprtobjslide->save();
			$this->_html .= ($res ? $this->displayConfirmation($this->l('Successfully updated')) : $this->displayError($this->l('The Values could not be updated.')));
			Tools::redirectAdmin($this->CurrentUrl());
		}
		$this->GenerateCss();
	}
	public function headerHTML()
	{
		if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name)
			return;
		$links = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&updateblocklistsPosition=1";
		$this->context->controller->addJqueryUI('ui.sortable');
		$html = '<script type="text/javascript">
			$(function() {
				var $mySlides = $("#block_lists");
				$mySlides.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize");
						$.post("'.$links.'", order);
						}
					});
				$mySlides.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
			});
		</script>';
		return $html;
	}
	public function headerEffectHTML()
	{
		if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name)
			return;
		$links = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&updateeffectslistsPosition=1";
		$this->context->controller->addJqueryUI('ui.sortable');
		$html = '<script type="text/javascript">
			$(function() {
				var $mySlides = $("#xprtlayereffect_lists");
				$mySlides.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize");
						$.post("'.$links.'", order);
						}
					});
				$mySlides.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
			});
		</script>';
		return $html;
	}
	public function headerSlideHTML()
	{
		if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name)
			return;
		$links = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&updateslidelistPosition=1";
		$this->context->controller->addJqueryUI('ui.sortable');
		$html = '<script type="text/javascript">
			$(function() {
				var $mySlides = $("#xprtslideeffect_lists");
				$mySlides.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize");
						$.post("'.$links.'", order);
						}
					});
				$mySlides.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
			});
		</script>';
		return $html;
	}
	public function headerSlidesHTML()
	{
		if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name)
			return;
		$links = $this->context->link->getAdminLink('AdminModules')."&configure=".$this->name."&updateSlidelistsPosition=1";
		$this->context->controller->addJqueryUI('ui.sortable');
		$html = '<script type="text/javascript">
			$(function() {
				var $mySlides = $("#slides");
				$mySlides.sortable({
					opacity: 0.6,
					cursor: "move",
					update: function() {
						var order = $(this).sortable("serialize");
						$.post("'.$links.'", order);
						}
					});
				$mySlides.hover(function() {
					$(this).css("cursor","move");
					},
					function() {
					$(this).css("cursor","auto");
				});
			});
		</script>';
		return $html;
	}
	public function getblock_lists($active = null)
	{
		if(!$this->context)
			$this->context = Context::getContext();
		$id_shop = (int)$this->context->shop->id;
		$id_lang = (int)$this->context->language->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.self::$tablename.'` v 
                INNER JOIN `'._DB_PREFIX_.self::$tablename.'_lang` vl ON (v.`id_'.self::$tablename.'` = vl.`id_'.self::$tablename.'` AND vl.`id_lang` = '.$id_lang.') INNER JOIN `'._DB_PREFIX_.self::$tablename.'_shop` vs ON (v.`id_'.self::$tablename.'` = vs.`id_'.self::$tablename.'` AND vs.`id_shop` = '.$id_shop.') ';
        $sql .= 'ORDER BY v.`position` DESC';
        $results = Db::getInstance()->executeS($sql);
        if(isset($results) && !empty($results)){
        	foreach ($results as &$result) {
        		$result['id'] = $result['id_'.self::$tablename];
        	}
        }
        return $results;
	}
	public function geteffectlists()
	{
		if(!$this->context)
			$this->context = Context::getContext();
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtslideseffect` ';
        $sql .= ' ORDER BY `position` DESC';
        $results = Db::getInstance()->executeS($sql);
        if(isset($results) && !empty($results)){
        	foreach ($results as &$result) {
        		$result['id'] = $result['id_xprtslideseffect'];
        	}
        }
        return $results;
	}
	public function getslidelists()
	{
		if(!$this->context)
			$this->context = Context::getContext();
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtslideeffect` ';
        $sql .= ' ORDER BY `position` DESC';
        $results = Db::getInstance()->executeS($sql);
        if(isset($results) && !empty($results)){
        	foreach ($results as &$result) {
        		$result['id'] = $result['id_xprtslideeffect'];
        	}
        }
        return $results;
	}
	public function displayStatus($id_i, $active)
	{
		$title = ((int)$active == 0 ? $this->l('Disabled') : $this->l('Enabled'));
		$icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
		$class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
		$html = '<a class="btn '.$class.'" href="'.AdminController::$currentIndex.
			'&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&changeStatus&id_'.$this->name.'='.(int)$id_i.'" title="'.$title.'"><i class="'.$icon.'"></i></a>';
		return $html;
	}
	public function displaySlidesStatus($id_i, $active)
	{
		$title = ((int)$active == 0 ? $this->l('Disabled') : $this->l('Enabled'));
		$icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
		$class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
		$html = '<a class="btn '.$class.'" href="'.AdminController::$currentIndex.
			'&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&changeSlideStatus&id_xprtslides='.(int)$id_i.'" title="'.$title.'"><i class="'.$icon.'"></i></a>';
		return $html;
	}
	public function displayEffectStatus($id_i, $active)
	{
		$title = ((int)$active == 0 ? $this->l('Disabled') : $this->l('Enabled'));
		$icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
		$class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
		$html = '<a class="btn '.$class.'" href="'.AdminController::$currentIndex.
			'&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&changexprtslideseffect&id_xprtslideseffect='.(int)$id_i.'" title="'.$title.'"><i class="'.$icon.'"></i></a>';
		return $html;
	}
	public function displaySlideStatus($id_i, $active)
	{
		$title = ((int)$active == 0 ? $this->l('Disabled') : $this->l('Enabled'));
		$icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
		$class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
		$html = '<a class="btn '.$class.'" href="'.AdminController::$currentIndex.
			'&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&changexprtslideeffect&id_xprtslideeffect='.(int)$id_i.'" title="'.$title.'"><i class="'.$icon.'"></i></a>';
		return $html;
	}
	public function BlockExists($id_i)
	{
		$req = 'SELECT hs.`id_'.self::$tablename.'` as id_'.self::$tablename.'
				FROM `'._DB_PREFIX_.self::$tablename.'` hs
				WHERE hs.`id_'.self::$tablename.'` = '.(int)$id_i;
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($req);
		return ($row);
	}
	public function renderList()
	{
		$block_lists = $this->getblock_lists();
		foreach ($block_lists as $key => $block)
		{
			$block_lists[$key]['status'] = $this->displayStatus($block['id_'.$this->name], $block['active']);
		}
		$this->context->smarty->assign(
			array(
				'link' => $this->context->link,
				'xprttablename' => self::$tablename,
				'xprtclassname' => self::$classname,
				'xprtmodulename' => $this->name,
				'block_lists' => $block_lists,
				'image_baseurl' => $this->_path.'images/'
			)
		);
		return $this->display(__FILE__, 'views/templates/admin/list.tpl');
	}
	public function rendereffectList()
	{
		$block_lists = $this->geteffectlists();
		foreach ($block_lists as $key => $block)
		{
			$block_lists[$key]['status'] = $this->displayEffectStatus($block['id_xprtslideseffect'], $block['active']);
		}
		$this->context->smarty->assign(
			array(
				'link' => $this->context->link,
				'xprttablename' => self::$tablename,
				'xprtclassname' => self::$classname,
				'xprtmodulename' => $this->name,
				'block_lists' => $block_lists,
				'image_baseurl' => $this->_path.'images/'
			)
		);
		return $this->display(__FILE__, 'views/templates/admin/effectlist.tpl');
	}
	public function renderSlideList()
	{
		$block_lists = $this->getslidelists();
		foreach ($block_lists as $key => $block)
		{
			$block_lists[$key]['status'] = $this->displaySlideStatus($block['id_xprtslideeffect'], $block['active']);
		}
		$this->context->smarty->assign(
			array(
				'link' => $this->context->link,
				'xprttablename' => self::$tablename,
				'xprtclassname' => self::$classname,
				'xprtmodulename' => $this->name,
				'block_lists' => $block_lists,
				'image_baseurl' => $this->_path.'images/'
			)
		);
		return $this->display(__FILE__, 'views/templates/admin/slideeffectlist.tpl');
	}
	public static function AllPageExceptions()
	{
	    $id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT p.`id_product`, pl.`name`
	            FROM `'._DB_PREFIX_.'product` p
	            '.Shop::addSqlAssociation('product', 'p').'
	            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
	            WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
	    $products = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	    $id_lang = Context::getContext()->language->id;
	    $categories =  Category::getCategories($id_lang,true,false);
	    $controllers = Dispatcher::getControllers(_PS_FRONT_CONTROLLER_DIR_);
	    if(isset($controllers))
	        ksort($controllers);
	    $Manufacturers =  Manufacturer::getManufacturers();
	    $Suppliers =  Supplier::getSuppliers();
	    $rslt = array();
	    $rslt[0]['id'] = 'all_page';
	    $rslt[0]['name'] = 'All Pages';
	    $i = 1;
	    if(isset($controllers))
	        foreach($controllers as $r => $v){
	            $rslt[$i]['id'] = $r;
	            $rslt[$i]['name'] = 'Page : '.ucwords($r);
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'man_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'sup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier : '.$r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'cat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category : '.$cats['name'];
	            $i++;
	        }
	    if(isset($products))
	        foreach($products as $r){
	            $rslt[$i]['id'] = 'prd_'.$r['id_product'];
	            $rslt[$i]['name'] = 'Product : '. $r['name'];
	            $i++;
	        }
	    if(isset($categories))
	        foreach($categories as $cats){
	            $rslt[$i]['id'] = 'prdcat_'.$cats['id_category'];
	            $rslt[$i]['name'] = 'Category Product: '.$cats['name'];
	            $i++;
	        }
	    if(isset($Manufacturers))
	        foreach($Manufacturers as $r){
	            $rslt[$i]['id'] = 'prdman_'.$r['id_manufacturer'];
	            $rslt[$i]['name'] = 'Manufacturer Product : '.$r['name'];
	            $i++;
	        }
	    if(isset($Suppliers))
	        foreach($Suppliers as $r){
	            $rslt[$i]['id'] = 'prdsup_'.$r['id_supplier'];
	            $rslt[$i]['name'] = 'Supplier Product : '.$r['name'];
	            $i++;
	        }
	    return $rslt;
	}
	public function SimpleProductS()
	{
	    $id_lang = (int)Context::getContext()->language->id;
	    $sql = 'SELECT p.`id_product`, pl.`name`
	            FROM `'._DB_PREFIX_.'product` p
	            '.Shop::addSqlAssociation('product', 'p').'
	            LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
	            WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
	    return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}
	public function AllProductS()
	{
	    $rs = array();
	    $rslt = array();
	    $rs =  $this->SimpleProductS();
	    $i = 0;
	    foreach($rs as $r){
	        $rslt[$i]['id'] = 'prd_'.$r['id_product'];
	        $rslt[$i]['name'] = $r['name'];
	        $i++;
	    }
	    return $rslt;
	}
	public function initFieldsForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Slider Block'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Slider Name'),
						'name' => 'title',
						'desc' => 'Please Enter Slider Name',
						'lang' => true,
					),
					array(
						'type' => 'text',
						'label' => $this->l('Slider Width'),
						'name' => 'sliderwidth',
						'class' => 'fixed-width-lg',
						'suffix' => 'pixels',
						'desc' => 'Please Enter Slider Width in pixels value',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Slider Height'),
						'name' => 'sliderheight',
						'class' => 'fixed-width-lg',
						'suffix' => 'pixels',
						'desc' => 'Please Enter Slider Height in pixels value',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Enter Slider Margin'),
						'name' => 'sliderpostion',
						'class' => 'fixed-width-lg',
						'desc' => 'Please Enter margin for this section. Ex: 0px 0px 0px 0px (top right bottom left)',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Slider Speed'),
						'name' => 'sliderspeed',
						'class' => 'fixed-width-lg',
						'suffix' => 'milliseconds',
						'desc' => 'The duration of the transition between two slides.',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Slider Delay'),
						'name' => 'betweenslidedelay',
						'class' => 'fixed-width-lg',
						'suffix' => 'milliseconds',
						'desc' => 'The delay between two slides..',
					),
					array(
					    'type' => 'selecttwotype',
					    'label' => $this->l('Select Slides Effect'),
					    'placeholder' => $this->l('Please Select Slides Effect'),
					    'initvalues' => xprtslideeffectclass::GetEffects(),
					    'name' => 'slideeffecteffect',
					    'group' => 'content',
					    'desc' => $this->l('Please Select Slides Effect')
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Slider Arrow Style'),
					    'name' => 'sliderarrow',
					    'options' => array(
					        'query' => $this->SliderArrowStyle(),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Slider Bullet Style'),
					    'name' => 'sliderbullet',
					    'options' => array(
					        'query' => $this->SliderBulletStyle(),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					// array(
					//     'type' => 'select',
					//     'label' => $this->l('Slider Thumbnail Style'),
					//     'name' => 'sliderthumbnail',
					//     'options' => array(
					//         'query' => $this->SliderThumbStyle(),
					//         'id' => 'id',
					//         'name' => 'name'
					//     ),
					// ),
					array(
						'type' => 'switch',
						'label' => $this->l('Autoplay'),
						'name' => 'autoplay',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'autoplay_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'autoplay_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Slider lazyload'),
						'name' => 'lazyload',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'autoplay_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'autoplay_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Force Full Width'),
						'name' => 'forcefullwidth',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'autoplay_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'autoplay_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
					array(
					    'type' => 'slidelists',
					),
					array(
					    'type' => 'select',
					    'label' => $this->l('Select Hook'),
					    'name' => 'hook',
					    'desc' => $this->l('Where you want to display.'),
					    'options' => array(
					        'query' => $this->GetHookarray(),
					        'id' => 'id',
					        'name' => 'name'
					    ),
					),
					array(
					    'type' => 'selecttwotype',
					    'label' => $this->l('Which Page You Want to Display'),
					    'placeholder' => $this->l('Please Type Your Controller Name.'),
					    'initvalues' => self::AllPageExceptions(),
					    'name' => 'pages',
					    'desc' => $this->l('Please Type Your Specific Page Name,Category name,Product Name,For All Product specific Category select category product: category name.<br>For showing All page Type: All Page. For Home Page Type:index.')
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enabled'),
						'name' => 'active',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				),
			),
		);
		return $fields_form;
	}
	public function renderAddForm()
	{
		$fields_form = $this->initFieldsForm();
		$id_xpertslider = null;
		$xprtslidelists = array();
		if (Tools::isSubmit('id_'.$this->name) && $this->BlockExists((int)Tools::getValue('id_'.$this->name)))
		{
			$id_xpertslider = (int)Tools::getValue('id_'.$this->name);
			$xprtobj = new self::$classname($id_xpertslider);
			$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_'.$this->name);
			$xprtslidelist = $this->getSlides($id_xpertslider);
			$xprtslidelists = $this->modifyslideslist($xprtslidelist);
		}
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->show_cancel_button = true;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->module = $this;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$adminmoduleslink = $this->context->link->getAdminLink('AdminModules');
		$helper->tpl_vars = array(
			'base_url' => $this->context->shop->getBaseURL(),
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $this->getAddFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
			'id_xpertslider' => $id_xpertslider,
			'adminmoduleslink' => $adminmoduleslink,
			'xprtslidelists' => $xprtslidelists,
			'image_baseurl' => $this->_path.'images/'
		);
		$helper->override_folder = '/';
		return $helper->generateForm(array($fields_form));
	}
	public function modifyslideslist($slides = null)
	{
		if($slides == null)
			return false;
		if(isset($slides) && !empty($slides)){
			foreach ($slides as $key => $slide)
			{
				$slides[$key]['status'] = $this->displaySlidesStatus($slide['id_xprtslides'], $slide['active']);
			}
			return $slides;
		}else{
			return false;
		}
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Effect Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Title'),
						'name' => 'title',
					),
					array(
						'type' => 'layereffect',
						'name' => 'layereffect',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enabled'),
						'name' => 'active',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		if(Tools::getValue('id_xprtslideseffect')){
			$fields_form['form']['input'][] = array('type' => 'hidden','name' => 'id_xprtslideseffect');
		}
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$helper->module = $this;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->identifier = "id_xprtslideseffect";
		$helper->submit_action = 'submitxprtslideseffect';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		if(Tools::getValue('id_xprtslideseffect')){
			$xprtslideseffect = new xprtslideseffectclass(Tools::getValue('id_xprtslideseffect'));
			$stTransition = $xprtslideseffect->effect;
		}else{
			$stTransition = "";
		}
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'stTransition' => $stTransition,
			'image_baseurl' => $this->_path.'images/',
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function renderSlideEffectForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Slide Effect Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Title'),
						'name' => 'title',
					),
					array(
						'type' => 'slideeffect',
						'name' => 'slideeffect',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enabled'),
						'name' => 'active',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		if(Tools::getValue('id_xprtslideeffect')){
			$fields_form['form']['input'][] = array('type' => 'hidden','name' => 'id_xprtslideeffect');
		}
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$helper->module = $this;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->identifier = "id_xprtslideeffect";
		$helper->submit_action = 'submitxprtslideeffect';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		if(Tools::getValue('id_xprtslideeffect')){
			$xprtslideeffect = new xprtslideeffectclass(Tools::getValue('id_xprtslideeffect'));
			$stTransition = $xprtslideeffect->effect;
		}else{
			$stTransition = "";
		}
		$helper->tpl_vars = array(
			'fields_value' => $this->getSlideConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'stTransition' => $stTransition,
			'image_baseurl' => $this->_path.'images/',
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getSlideConfigFieldsValues()
	{
		if(Tools::getValue('id_xprtslideeffect')){
			$xprtslideeffect = new xprtslideeffectclass(Tools::getValue('id_xprtslideeffect'));
			return array(
				'id_xprtslideeffect' => Tools::getValue('id_xprtslideeffect'),
				'title' => Tools::getValue('title',$xprtslideeffect->title),
				'effect' => Tools::getValue('stTransition',$xprtslideeffect->effect),
				'active' => Tools::getValue('active',$xprtslideeffect->active),
			);
			$helper->tpl_vars = array(
				'stTransition' => Tools::getValue('stTransition',$xprtslideseffect->effect),
			);
		}else{
			return array(
				'title' => Tools::getValue('title'),
				'stTransition' => Tools::getValue('stTransition'),
				'active' => Tools::getValue('active'),
			);
		}
	}
	public function getConfigFieldsValues()
	{
		if(Tools::getValue('id_xprtslideseffect')){
			$xprtslideseffect = new xprtslideseffectclass(Tools::getValue('id_xprtslideseffect'));
			return array(
				'id_xprtslideseffect' => Tools::getValue('id_xprtslideseffect'),
				'title' => Tools::getValue('title',$xprtslideseffect->title),
				'effect' => Tools::getValue('stTransition',$xprtslideseffect->effect),
				'active' => Tools::getValue('active',$xprtslideseffect->active),
			);
			$helper->tpl_vars = array(
				'stTransition' => Tools::getValue('stTransition',$xprtslideseffect->effect),
			);
		}else{
			return array(
				'title' => Tools::getValue('title'),
				'stTransition' => Tools::getValue('stTransition'),
				'active' => Tools::getValue('active'),
			);
		}
		
	}
	public function getAddFieldsValues()
	{
		$fields_form = $this->initFieldsForm();
		$languages = Language::getLanguages(false);
		$fields_values = array();
		if(Tools::getValue('id_'.$this->name) && $this->BlockExists((int)Tools::getValue('id_'.$this->name)))
		{
			$xprtobj = new self::$classname((int)Tools::getValue('id_'.$this->name));
			$fields_values['id_'.$this->name] = (int)Tools::getValue('id_'.$this->name, $xprtobj->id);
		}else{
			$xprtobj = new self::$classname();
		}
		if(isset($xprtobj->content) && !empty($xprtobj->content)){
			$content_fields_values = Tools::jsonDecode($xprtobj->content);
			if(isset($content_fields_values) && !empty($content_fields_values)){
				foreach ($content_fields_values as $content_key => $content_value) {
					if(is_object($content_value)){
						foreach ($content_value as $content_value_key => $content_value_value){
							$fields_values_temp[$content_key][$content_value_key] = $content_value_value;
						}
					}else{
						$fields_values_temp[$content_key] = Tools::getValue($content_key, $content_value);
					}
				}
			}
		}
		if(isset($fields_form['form']['input']) && !empty($fields_form['form']['input'])){
			foreach ($fields_form['form']['input'] as $field) {
				if(isset($field['expandstyle']) && $field['expandstyle'] == true){
					$fields_values["put-cls-".$field['name']] = isset($fields_values_temp["put-cls-".$field['name']]) ? $fields_values_temp["put-cls-".$field['name']] : "";
					$fields_values["put-cls-".$field['name']] = isset($fields_values_temp["put-cls-".$field['name']]) ? $fields_values_temp["put-cls-".$field['name']] : "";
					$fields_values["put-mar-".$field['name']] = isset($fields_values_temp["put-mar-".$field['name']]) ? $fields_values_temp["put-mar-".$field['name']] : "";
					$fields_values["put-pad-".$field['name']] = isset($fields_values_temp["put-pad-".$field['name']]) ? $fields_values_temp["put-pad-".$field['name']] : "";
				}
				if(isset($field['type']) && $field['type'] == 'device'){
					$fields_values[$field['name']] = Tools::jsonDecode($xprtobj->{$field['name']});
				}elseif(isset($field['group']) && $field['group'] == "content"){
					// start
						if(isset($field['lang']) && $field['lang'] == true){
							foreach ($languages as $lang)
							{
								if(isset($field['name']) && !empty($field['name'])){
									$fields_values[$field['name']][$lang['id_lang']] = isset($fields_values_temp[$field['name']][$lang['id_lang']]) ? $fields_values_temp[$field['name']][$lang['id_lang']] : "";
								}
							}
						}else{
							if(isset($field['name']) && !empty($field['name'])){
								$fields_values[$field['name']] = isset($fields_values_temp[$field['name']]) ? $fields_values_temp[$field['name']] : "";
							}
						}
					// end
				}else{
					if(isset($field['lang']) && $field['lang'] == true){
						foreach ($languages as $lang)
						{
							if(isset($field['name']) && !empty($field['name'])){
								$fields_values[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang'], $xprtobj->{$field['name']}[$lang['id_lang']]);
							}
						}
					}else{
						if(isset($field['name']) && !empty($field['name'])){
							$fields_values[$field['name']] = Tools::getValue($field['name'], $xprtobj->{$field['name']});
						}
					}
				}
			}
		}
		return $fields_values;
	}
	public function getSlides($id_slider = null)
	{
		if($id_slider == null)
			return false;
		$this->context = Context::getContext();
		$id_shop = (int)$this->context->shop->id;
		$id_lang = (int)$this->context->language->id;
		return Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'xprtslides xs LEFT JOIN '._DB_PREFIX_.'xprtslides_lang xsl ON (xs.id_xprtslides = xsl.id_xprtslides and xsl.id_lang = '.$id_lang.' ) WHERE xs.id_slider = '.(int)$id_slider.' ORDER BY xs.position');
	}
	public function getSlidesAddFieldsValues()
	{
		$fields_form = $this->initSlidesFieldsForm();
		$languages = Language::getLanguages(false);
		$fields_values = array();
		if(Tools::getValue('id_xprtslides') && $this->slideExists((int)Tools::getValue('id_xprtslides')))
		{
			$xprtslidesobj = new xprtslidesclass((int)Tools::getValue('id_xprtslides'));
			$fields_values['id_xprtslides'] = (int)Tools::getValue('id_xprtslides', $xprtslidesobj->id);
		}else{
			$xprtslidesobj = new xprtslidesclass();
		}
		if(isset($xprtslidesobj->content) && !empty($xprtslidesobj->content)){
			$content_fields_values = Tools::jsonDecode($xprtslidesobj->content);
			if(isset($content_fields_values) && !empty($content_fields_values)){
				foreach ($content_fields_values as $content_key => $content_value) {
					if(is_object($content_value)){
						foreach ($content_value as $content_value_key => $content_value_value){
							$fields_values_temp[$content_key][$content_value_key] = $content_value_value;
						}
					}else{
						$fields_values_temp[$content_key] = Tools::getValue($content_key, $content_value);
					}
				}
			}
		}
		if(isset($fields_form['form']['input']) && !empty($fields_form['form']['input'])){
			foreach ($fields_form['form']['input'] as $field) {
				if(isset($field['expandstyle']) && $field['expandstyle'] == true){
					$fields_values["put-cls-".$field['name']] = isset($fields_values_temp["put-cls-".$field['name']]) ? $fields_values_temp["put-cls-".$field['name']] : "";
					$fields_values["put-cls-".$field['name']] = isset($fields_values_temp["put-cls-".$field['name']]) ? $fields_values_temp["put-cls-".$field['name']] : "";
					$fields_values["put-mar-".$field['name']] = isset($fields_values_temp["put-mar-".$field['name']]) ? $fields_values_temp["put-mar-".$field['name']] : "";
					$fields_values["put-pad-".$field['name']] = isset($fields_values_temp["put-pad-".$field['name']]) ? $fields_values_temp["put-pad-".$field['name']] : "";
				}
				if(isset($field['type']) && $field['type'] == 'device'){
					$fields_values[$field['name']] = Tools::jsonDecode($xprtslidesobj->{$field['name']});
				}elseif(isset($field['group']) && $field['group'] == "content"){
					// start
						if(isset($field['lang']) && $field['lang'] == true){
							foreach ($languages as $lang)
							{
								if(isset($field['name']) && !empty($field['name'])){
									$fields_values[$field['name']][$lang['id_lang']] = isset($fields_values_temp[$field['name']][$lang['id_lang']]) ? $fields_values_temp[$field['name']][$lang['id_lang']] : "";
								}
							}
						}else{
							if(isset($field['name']) && !empty($field['name'])){
								$fields_values[$field['name']] = isset($fields_values_temp[$field['name']]) ? $fields_values_temp[$field['name']] : "";
							}
						}
					// end
				}else{
					if(isset($field['lang']) && $field['lang'] == true){
						foreach ($languages as $lang)
						{
							if(isset($field['name']) && !empty($field['name'])){
								$fields_values[$field['name']][$lang['id_lang']] = Tools::getValue($field['name'].'_'.(int)$lang['id_lang'], $xprtslidesobj->{$field['name']}[$lang['id_lang']]);
							}
						}
					}else{
						if(isset($field['name']) && !empty($field['name'])){
							if(isset($xprtslidesobj->{$field['name']}) && !empty($xprtslidesobj->{$field['name']})){
								$fields_values[$field['name']] = Tools::getValue($field['name'], $xprtslidesobj->{$field['name']});
							}else{
								$fields_values[$field['name']] = Tools::getValue($field['name']);
							}
						}
					}
				}
			}
		}
		$fields_values['id_slider'] = Tools::getValue("id_xpertslider");
		$fields_values['id_xprtslides'] = Tools::getValue("id_xprtslides");
		return $fields_values;
	}
	
	protected function deleteTables(){
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::$tablename.'`');
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::$tablename.'_lang`');
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.self::$tablename.'_shop`');
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtslides`');
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtslides_lang`');
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprthomesliderlayer`');
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprthomesliderlayer_lang`');
		Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtslideseffect`');
		return true;
	}
	protected function createTables()
	{

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::$tablename.'` (
				`id_'.self::$tablename.'` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`sliderwidth` VARCHAR(150) NOT NULL,
				`sliderheight` VARCHAR(150) NOT NULL,
				`sliderpostion` VARCHAR(150) NOT NULL,
				`sliderspeed` VARCHAR(150) NOT NULL,
				`sliderarrow` VARCHAR(150) NOT NULL,
				`sliderbullet` VARCHAR(150) NOT NULL,
				`sliderthumbnail` VARCHAR(150) NOT NULL,
				`betweenslidedelay` VARCHAR(150) NOT NULL,
				`autoplay` int(10) NOT NULL,
				`lazyload` int(10) NOT NULL,
				`forcefullwidth` int(10) NOT NULL,
				`hook` VARCHAR(150) NOT NULL,
				`pages` text NULL,
				`content` longtext NULL,
				`active` int(10) NOT NULL,
				`position` int(10) NOT NULL,
				PRIMARY KEY (`id_'.self::$tablename.'`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;',false);

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::$tablename.'_lang` (
				`id_'.self::$tablename.'` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL,
				`title` VARCHAR(200) NOT NULL,
				PRIMARY KEY (`id_'.self::$tablename.'`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;',false);

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.self::$tablename.'_shop` (
			  `id_'.self::$tablename.'` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_'.self::$tablename.'`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;',false);

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtslides` (
			  `id_xprtslides` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `design_style` varchar(255) NOT NULL,
			  `id_slider` int(10) NOT NULL,
			  `customclass` varchar(255) NOT NULL,
			  `ineffect` varchar(255) NOT NULL,
			  `outeffect` varchar(255) NOT NULL,
			  `content` longtext NULL,
			  `position` int(10) unsigned NOT NULL DEFAULT \'0\',
			  `active` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
			  PRIMARY KEY (`id_xprtslides`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;');

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtslides_lang` (
			  `id_xprtslides` int(10) unsigned NOT NULL,
			  `id_lang` int(10) unsigned NOT NULL,
			  `title` varchar(255) NOT NULL,
			  `layered` longtext,
			  `slideurl` varchar(255) NOT NULL,
			  `slidebgtype` varchar(255) NOT NULL,
			  `slideimage` varchar(255) NOT NULL,
			  `slidethumbnail` varchar(255) NOT NULL,
			  `slidebgcolor` varchar(255) NOT NULL,
			  PRIMARY KEY (`id_xprtslides`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;');

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprthomesliderlayer` (
			  `id_xprthomesliderlayer` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `text` text NOT NULL,
			  `id_slide` int(10) unsigned NOT NULL,
			  `in` varchar(150) NOT NULL,
			  `out` varchar(150) NOT NULL,
			  `delay` varchar(150) NOT NULL,
			  `class` varchar(150) NOT NULL,
			  `width` varchar(150) NOT NULL,
			  `height` varchar(150) NOT NULL,
			  `top` varchar(150) NOT NULL,
			  `left` varchar(150) NOT NULL,
			  `margin`  varchar(150) NOT NULL,
			  `padding`  varchar(150) NOT NULL,
			  `line_height`  varchar(150) NOT NULL,
			  `font_family`  varchar(150) NOT NULL,
			  `font_weight`  varchar(150) NOT NULL,
			  `font_size`  varchar(150) NOT NULL,
			  `bg_color`  varchar(150) NOT NULL,
			  `bg_color_hover`  varchar(150) NOT NULL,
			  `text_color`  varchar(150) NOT NULL,
			  `text_color_hover`  varchar(150) NOT NULL,
			  `border_size`  varchar(150) NOT NULL,
			  `border_color`  varchar(150) NOT NULL,
			  `border_color_hover`  varchar(150) NOT NULL,
			  `layer_type` varchar(150) NOT NULL,
			  PRIMARY KEY (`id_xprthomesliderlayer`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;');

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprthomesliderlayer_lang` (
			  `id_xprthomesliderlayer` int(10) unsigned NOT NULL,
			  `id_lang` int(10) unsigned NOT NULL,
			  `layertext` text NOT NULL,
			  PRIMARY KEY (`id_xprthomesliderlayer`,`id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;');

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtslideseffect` (
			  `id_xprtslideseffect` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `title` varchar(255) NOT NULL,
			  `effect` text NULL,
			  `position` int(10) unsigned NOT NULL DEFAULT \'0\',
			  `active` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
			  PRIMARY KEY (`id_xprtslideseffect`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;');

		Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtslideeffect` (
			  `id_xprtslideeffect` int(10) unsigned NOT NULL AUTO_INCREMENT,
			  `title` varchar(255) NOT NULL,
			  `effect` text NULL,
			  `position` int(10) unsigned NOT NULL DEFAULT \'0\',
			  `active` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
			  PRIMARY KEY (`id_xprtslideeffect`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;');

		return true;
	}
	public function initSlidesFieldsForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Slides Block'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'hidden',
						'name' => 'id_slider',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Slide title'),
						'name' => 'title',
						'lang' => true,
					),
					array(
						'type' => 'file_lang',
						'label' => $this->l('Select a file'),
						'name' => 'slideimage',
						'lang' => true,
						'desc' => $this->l(sprintf('Maximum image size: %s.', ini_get('upload_max_filesize')))
					),
					array(
						'type' => 'text',
						'label' => $this->l('Target URL'),
						'name' => 'slideurl',
						'lang' => true,
					),
					array(
						'type' => 'textlayer',
						'label' => $this->l('Text Layer Settings'),
						'name' => 'layered',
						'form_group_class' => 'textlayercontainerclass',
					),
					array(
						'type' => 'imagelayer',
						'label' => $this->l('Image Layer Settings'),
						'name' => 'imglayered',
						'form_group_class' => 'imglayercontainerclass',
					),
					array(
						'type' => 'select',
						'label' => $this->l('Layer style'),
						'name' => 'design_style',
						'options' => array(
							'id' => 'id',
							'name' => 'name',
							'query' => array(
								array(
									'id' => 'layer_box_style',
									'name' => 'Single Layer box style'
									),
								array(
									'id' => 'layer_box_style_transparent',
									'name' => 'Single Layer box tranparent style'
									),
								array(
									'id' => 'layer_style_normal',
									'name' => 'Layer style normal'
									),
								array(
									'id' => 'layer_style_two',
									'name' => 'Layer style two'
									),
								array(
									'id' => 'layer_style_other',
									'name' => 'Layer style other'
									),
								)
							)
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Enabled'),
						'name' => 'active',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				),
			),
		);
		return $fields_form;
	}
	public function renderSlideAddForm()
	{
		$id_slider = Tools::getValue("id_xpertslider");
		if(empty($id_slider))
			return $this->getemptysliderInfoMsg();
		$fields_form = $this->initSlidesFieldsForm();
		$id_lang = (int)$this->context->language->id;
		if (Tools::isSubmit('id_xprtslides') && $this->slideExists((int)Tools::getValue('id_xprtslides')))
		{
			$languages = Language::getLanguages(false);
			$slide = new xprtslidesclass((int)Tools::getValue('id_xprtslides'));
			$fields_form['form']['input'][] = array('type' => 'hidden', 'name' => 'id_xprtslides');
			foreach ($languages as $lang){
				if(isset($slide->slideimage[$lang['id_lang']]) && !empty($slide->slideimage[$lang['id_lang']])){
					$fields_form['form']['slideimage'][$lang['id_lang']] = $slide->slideimage[$lang['id_lang']];
				}else{
					$fields_form['form']['slideimage'][$lang['id_lang']] = "noimage.jpg";
				}
			}
		}
		$id_xprtslides = Tools::getValue("id_xprtslides");
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->module = $this;
		$helper->identifier = "id_xprtslides";
		$helper->submit_action = 'submitxprtslides';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$xprtalleffects = xprtslideseffectclass::GetEffects();
		$helper->tpl_vars = array(
			'base_url' => $this->context->shop->getBaseURL(),
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $this->getSlidesAddFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
			'id_xprtslides' => $id_xprtslides,
			'xprtalleffects' => $xprtalleffects,
			'xprtimagelink' => _MODULE_DIR_.self::$module_name.'/images/',
			'submit_action' => "submitxprtslides",
            'gets_fonts_family' => $this->gets_fonts_family(),
			'slidepagetype' => ($id_xprtslides) ? 'edit' : 'add',
			'text_layers' => $this->getlayerbyslideid($id_xprtslides,'text'),
			'image_layers' => $this->getlayerbyslideid($id_xprtslides,'image'),
			'controllers_link' => $this->context->link->getAdminLink("Adminxprthomeslider"),
			'image_baseurl' => $this->_path.'images/',
		);
		$helper->override_folder = '/';
		$languages = Language::getLanguages(false);
		if (count($languages) > 1)
			return $this->getMultiLanguageInfoMsg().$helper->generateForm(array($fields_form));
		else
			return $helper->generateForm(array($fields_form));
	}
	public function gets_google_fonts()
	{
	    $all_fonts = array();
	    $new_fonts = array();
	    $fonts = Tools::file_get_contents($filename = 'https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyD_6TR2RyX2VRf8bABDRXCcVqdMXB5FQvs');
	    $fonts = Tools::jsonDecode($fonts);
	    $google_fonts = (array)$fonts->items;
	    if(isset($google_fonts)){
	        foreach($google_fonts as $font){
	            $new_fonts['kind'] = $font->kind;
	            $new_fonts['family'] = $font->family;
	            $new_fonts['category'] = $font->category;
	            $new_fonts['variants'] = $font->variants;
	            $new_fonts['subsets'] = $font->subsets;
	            $new_fonts['version'] = $font->version;
	            $new_fonts['lastModified'] = $font->lastModified;
	            $new_fonts['files'] = $font->files;
	            array_push($all_fonts,$new_fonts);
	        }
	    }
	    $all_fonts = Tools::jsonEncode($all_fonts);
	    @file_put_contents($this->fonts_files,$all_fonts);
	}
	public function gets_fonts_family()
	{
	    $fonts_family = array();
	    $all_fonts = Tools::file_get_contents($this->fonts_files);
	    $all_fonts = Tools::jsonDecode($all_fonts);
	    if(isset($all_fonts) && !empty($all_fonts)){
	        $i= 0;
	        foreach($all_fonts as $all_font){
	            $fonts_family[$i]['id'] = $all_font->family;
	            $fonts_family[$i]['name'] = $all_font->family;
	            $i++;
	        }
	    }
	    return $fonts_family;
	}
	public function slideExists($id_xprtslides = null)
	{
		if($id_xprtslides == null)
			return false;
		$req = 'SELECT hs.`id_xprtslides` as id_xprtslides FROM `'._DB_PREFIX_.'xprtslides` hs WHERE hs.`id_xprtslides` = '.(int)$id_xprtslides;
		$row = Db::getInstance()->getRow($req);
		return ($row);
	}
	public function getlayerbyslideid($id_xprtslides = null,$layer_type = 'text'){
		if($id_xprtslides == null)
			return false;
		$id_lang = $this->context->language->id;
		$rows = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'xprthomesliderlayer` tl INNER JOIN `'._DB_PREFIX_.'xprthomesliderlayer_lang` tll ON (tl.`id_xprthomesliderlayer` = tll.`id_xprthomesliderlayer` AND tll.`id_lang` = '.(int)$id_lang.' )	WHERE tl.`id_slide` = '.(int)$id_xprtslides.' AND tl.`layer_type` = "'.$layer_type.'" ');
		return $rows;
	}
	private function getMultiLanguageInfoMsg()
	{
		return '<p class="alert alert-warning">'.
					$this->l('Since multiple languages are activated on your shop, please mind to upload your image for each one of them').
				'</p>';
	}
	private function getemptysliderInfoMsg()
	{
		return '<div class="alert alert-warning">
					There is 1 warning.
				<ul style="display:block;" id="seeMore">
					<li>'.$this->l('You must save this product before adding slides.').'</li>
				</ul>
			</div>';
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
			$this->alldisabled();
        	$this->{$func}();
        }else{
        	$this->alldisabled();
        }
        return true;
	}
	public function xpertsample_demo_2()
	{
		$results = array(1);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new xprtsliderblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_1()
	{
		$results = array(2);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new xprtsliderblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_3()
	{
		$results = array(3);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new xprtsliderblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_4()
	{
		$results = array(4);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new xprtsliderblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_6()
	{
		$results = array(4);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new xprtsliderblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_7()
	{
		$results = array(5);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new xprtsliderblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_8()
	{
		$results = array(4);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new xprtsliderblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_9()
	{
		$results = array(6);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new xprtsliderblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function xpertsample_demo_12()
	{
		$results = array(7);
		if(isset($results) && !empty($results)){
			foreach ($results as $rslt) {
				$classobj = new xprtsliderblockclass((int)$rslt);
				$classobj->active = 1;
				$classobj->update();
			}
		}
		return true;
	}
	public function InsertDummyData($categories=NULL,$class=NULL)
	{
		if($categories == NULL || $class == NULL)
			return false;
		$languages = Language::getLanguages(false);
	    if(isset($categories) && !empty($categories)){
	        $classobj = new $class();
	        foreach($categories as $valu){
	        	if(isset($valu['lang']) && !empty($valu['lang'])){
	        		foreach ($valu['lang'] as $valukey => $value){
	        			foreach ($languages as $language){
	        				if(isset($valukey)){
	        					$classobj->{$valukey}[$language['id_lang']] = isset($value) ? $value : '';
	        				}
	        			}
	        		}
	        	}
        		if(isset($valu['notlang']) && !empty($valu['notlang'])){
        			foreach ($valu['notlang'] as $valukey => $value){
        				if(isset($valukey)){
        					$classobj->{$valukey} = $value;
        				}
        			}
        		}
	        	$classobj->add();
	        }
	    }
	    return true;
	}
	public function alldisabled(){
		$data = array();
		$data['active'] = 0;
		Db::getInstance()->update("xprtsliderblock",$data);
		return true;
	}
	public function DummyData()
	{
	    include_once(dirname(__FILE__).'/data/dummy_data.php');
	    $this->InsertDummyData($xprtslideeffect,'xprtslideeffectclass');
	    $this->InsertDummyData($xprtslideseffect,'xprtslideseffectclass');
	    $this->InsertDummyData($xprthomesliderlayer,'xprthomesliderlayer');
	    $this->InsertDummyData($xprtslidesclass,'xprtslidesclass');
	    $this->InsertDummyData($xprtsliderblockclass,'xprtsliderblockclass');
	    return true;
	}
	public function GenerateCss(){
		$css = '
		.xprt_navigator{
			bottom: 30px;
			right: 6px;
		}
		.xprt_arrow_left{
			top: 50%;	left: 20px;
			transform: translateY(-50%);
		}
		.xprt_arrow_right{
			top: 50%;	right: 20px; 
			transform: translateY(-50%);
		}
				';
		$classname = self::$classname;
		$mod_name = self::$module_name;
		$id_shop = (int)$this->context->shop->id;
		$query_values = $classname::GetSliderSBlock();
		$this->context->smarty->assign(array($mod_name => $query_values));
		$css .= $this->display(__FILE__,'views/templates/front/generatecss.tpl');
		$css_files = dirname(__FILE__).'/css/'.$this->css_files.'_'.$id_shop.'.css';
		@file_put_contents($css_files,$css);
	}
	public function hookexecute($hook,$istab = true)
	{
		$this->GenerateCss();
		$classname = self::$classname;
		$mod_name = self::$module_name;
		$query_values = $classname::GetSliderSBlock($hook);
		if(empty($query_values))
			return false;
		$this->context->smarty->assign(array($mod_name => $query_values));
		if($istab == true){
			return $this->display(__FILE__,'views/templates/front/'.$mod_name.'.tpl');
		}
	}
	public function hookdisplaybackofficeheader($params)
	{
		$xprtsliderbopage = 'none';
		if(Tools::getValue("addxprtlayereffect") || Tools::getValue("updatexprtlayereffect")){
			$xprtsliderbopage = 'layereffect';
		}
		if(Tools::getValue("updatexprtslideeffect") || Tools::getValue("addxprtsldeffect")){
			$xprtsliderbopage = 'slideeffect';
		}
		$this->context->smarty->assign(array('this__path' => $this->_path,'xprtsliderbopage'=>$xprtsliderbopage));
		//$this->context->controller->addJqueryPlugin('colorpicker');
		
		$this->context->smarty->assign(array('colorpicker' => __PS_BASE_URI__.'js/jquery/plugins/jquery.colorpicker.js'));
		
		return $this->display(__FILE__,'views/templates/admin/displaybackofficeheader.tpl');
	}
	public function hookdisplayHeader($params)
	{
// 	    $id_lang = (int)Context::getContext()->language->id;
// 		$sql = 'SELECT * FROM `'._DB_PREFIX_.'xprthtmlblock` pb 
// 	            INNER JOIN `'._DB_PREFIX_.'xprthtmlblock_lang` pbl ON (pb.`id_xprthtmlblock` = pbl.`id_xprthtmlblock` AND pbl.`id_lang` = '.$id_lang.')';
// 	            print $sql;
// 	    $results = Db::getInstance()->executeS($sql);

// print '<pre>';
// $datas = [];
// foreach ($results as $keys=>$result) {
// 	foreach ($result as $kresult=>$vresult) {
// 		if($kresult == "title" || $kresult == "id_lang" || $kresult == "sub_title"){
// 			$datas['lang'][$kresult] =  $vresult;
// 		}else{
// 			$datas['notlang'][$kresult] =  $vresult;
// 		}
// 	}
// 	print_r($datas);
// }
// print '</pre>';
// die();
		$link = 'https://fonts.googleapis.com/css?family=';
		$classname = self::$classname;
		$query_values = $classname::GetSliderSBlock();
		if(isset($query_values) && !empty($query_values)){
			foreach ($query_values as $query_value) {
				if(isset($query_value['all_slides']) && !empty($query_value['all_slides'])){
					foreach ($query_value['all_slides'] as $queryvalue) {
						if(isset($queryvalue['all_layers']) && !empty($queryvalue['all_layers'])){
							foreach ($queryvalue['all_layers'] as $all_layer) {
								if(isset($all_layer['font_family']) && !empty($all_layer['font_family'])){
									$this->context->controller->addCSS($link.$all_layer['font_family']);
								}
							}
						}
					}
				}
			}
		}
		$this->context->controller->addJS($this->_path.'js/main.js');
		$this->context->controller->addCSS($this->_path.'css/'.$this->name.'.css');
		$this->context->controller->addJS($this->_path.'js/'.$this->name.'.js');
		$id_shop = (int)$this->context->shop->id;
		$this->context->controller->addCSS($this->_path.'css/'.$this->css_files.'_'.$id_shop.'.css');

	}
	public function hookdisplayHome($params)
	{
		return $this->hookexecute('displayHome');
	}
	public function hookdisplayLeftColumn($params)
	{
		return $this->hookexecute('displayLeftColumn');
	}
	public function hookdisplayLeftColumnProduct($params)
	{
		return $this->hookexecute('displayLeftColumnProduct');
	}
	public function hookdisplayMaintenance($params)
	{
		return $this->hookexecute('displayMaintenance');
	}
	public function hookdisplayRightColumn($params)
	{
		return $this->hookexecute('displayRightColumn');
	}
	public function hookdisplayRightColumnProduct($params)
	{
		return $this->hookexecute('displayRightColumnProduct');
	}
	public function hookdisplayHomeTop($params)
	{
		return $this->hookexecute('displayHomeTop');
	}
	public function hookdisplayTopColumn($params)
	{
		return $this->hookexecute('displayTopColumn');
	}
	public function hookdisplayHomeTopLeft($params)
	{
		return $this->hookexecute('displayHomeTopLeft');
	}
	public function hookdisplayHomeTopRight($params)
	{
		return $this->hookexecute('displayHomeTopRight');
	}
	public function hookdisplayHomeTopLeftOne($params)
	{
		return $this->hookexecute('displayHomeTopLeftOne');
	}
	public function hookdisplayHomeTopLeftTwo($params)
	{
		return $this->hookexecute('displayHomeTopLeftTwo');
	}
	public function hookdisplayHomeTopRightOne($params)
	{
		return $this->hookexecute('displayHomeTopRightOne');
	}
	public function hookdisplayHomeTopRightTwo($params)
	{
		return $this->hookexecute('displayHomeTopRightTwo');
	}
	public function hookdisplayHomeBottomLeft($params)
	{
		return $this->hookexecute('displayHomeBottomLeft');
	}
	public function hookdisplayHomeBottomRight($params)
	{
		return $this->hookexecute('displayHomeBottomRight');
	}
	public function hookdisplayHomeBottomLeftOne($params)
	{
		return $this->hookexecute('displayHomeBottomLeftOne');
	}
	public function hookdisplayHomeBottomLeftTwo($params)
	{
		return $this->hookexecute('displayHomeBottomLeftTwo');
	}
	public function hookdisplayHomeBottomRightOne($params)
	{
		return $this->hookexecute('displayHomeBottomRightOne');
	}
	public function hookdisplayHomeBottomRightTwo($params)
	{
		return $this->hookexecute('displayHomeBottomRightTwo');
	}
	public function hookdisplayHomeMiddle($params)
	{
		return $this->hookexecute('displayHomeMiddle');
	}
	public function hookdisplayHomeFullWidthMiddle($params)
	{
		return $this->hookexecute('displayHomeFullWidthMiddle');
	}
	public function hookdisplayHomeFullWidthBottom($params)
	{
		return $this->hookexecute('displayHomeFullWidthBottom');
	}

}