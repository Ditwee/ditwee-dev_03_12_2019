{if isset($xprtspecialsproductblock) && !empty($xprtspecialsproductblock)}
	{if isset($xprtspecialsproductblock.device)}
		{assign var=device_data value=$xprtspecialsproductblock.device|json_decode:true}
	{/if}
	<div id="xprtspecialsproductblock_{$xprtspecialsproductblock.id_xprtspecialsproductblock}" class="xprtspecialsproductblock xprt_default_products_block" style="margin:{$xprtspecialsproductblock.section_margin};">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtspecialsproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtspecialsproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtspecialsproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtspecialsproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		<div class="heading-line"><span></span></div>
		{if isset($xprtspecialsproductblock) && $xprtspecialsproductblock}
			<div id="xprt_specialproductsblock_{$xprtspecialsproductblock.id_xprtspecialsproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtspecialsproductblock.products id='' class="xprt_specialproductsblock {if $xprtspecialsproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No special products at this time.' mod='xprtspecialsproductblock'}</p>
			</div>
		{/if}
	</div>
<script type="text/javascript">
	var xprtspecialsproductblock = $("#xprtspecialsproductblock_{$xprtspecialsproductblock.id_xprtspecialsproductblock}");
	var sliderSelect = xprtspecialsproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtspecialsproductblock.find('.heading_carousel_arrow'); 

	{if isset($xprtspecialsproductblock.nav_arrow_style) && ($xprtspecialsproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}
	
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtspecialsproductblock.play_again)}{$xprtspecialsproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtspecialsproductblock.autoplay)}{$xprtspecialsproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtspecialsproductblock.pause_on_hover)}{$xprtspecialsproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtspecialsproductblock.navigation_dots)}{$xprtspecialsproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtspecialsproductblock.navigation_arrow)}{$xprtspecialsproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: arrowSelect,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtspecialsproductblock.product_rows)}{$xprtspecialsproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtspecialsproductblock.product_scroll) && ($xprtspecialsproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtspecialsproductblock.product_scroll) && ($xprtspecialsproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtspecialsproductblock.product_scroll) && ($xprtspecialsproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtspecialsproductblock.product_scroll) && ($xprtspecialsproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtspecialsproductblock.product_scroll) && ($xprtspecialsproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}