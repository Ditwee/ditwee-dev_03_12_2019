{if isset($xprtspecialsproductblock) && !empty($xprtspecialsproductblock)}
	{if isset($xprtspecialsproductblock.device)}
		{assign var=device_data value=$xprtspecialsproductblock.device|json_decode:true}
	{/if}
    {if isset($xprtspecialsproductblock.products) && $xprtspecialsproductblock.products|@count > 0}
		{include file="$tpl_dir./product-list/product-list-simple-paralax.tpl" xprtprdcolumnclass=$device_data products=$xprtspecialsproductblock.products parallaxbg=$xprtspecialsproductblock.image margin=$xprtspecialsproductblock.section_margin}
	{else}
		<p>{l s='No products at this time' mod='xprtspecialsproductblock'}</p>
	{/if}
{/if}