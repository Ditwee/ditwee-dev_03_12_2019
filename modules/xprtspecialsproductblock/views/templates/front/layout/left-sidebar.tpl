{if isset($xprtspecialsproductblock) && !empty($xprtspecialsproductblock)}
	{if isset($xprtspecialsproductblock.device)}
		{assign var=device_data value=$xprtspecialsproductblock.device|json_decode:true}
	{/if}
	<div class="xprtspecialsproductblock block carousel">
		<h4 class="title_block">
	    	{$xprtspecialsproductblock.title}
	    </h4>
	    <div class="block_content">
	        {if isset($xprtspecialsproductblock) && $xprtspecialsproductblock}
	        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtspecialsproductblock.products }
	        {else}
	        	<p class="alert alert-info">{l s='No products at this time.' mod='xprtspecialsproductblock'}</p>
	        {/if}
	    </div>
	</div>
{/if}