{if isset($xprtspecialsproductblock) && !empty($xprtspecialsproductblock)}
	{if isset($xprtspecialsproductblock.device)}
		{assign var=device_data value=$xprtspecialsproductblock.device|json_decode:true}
	{/if}
		{if isset($xprtspecialsproductblock) && $xprtspecialsproductblock}
			<div id="xprt_specialproductsblock_tab_{if isset($xprtspecialsproductblock.id_xprtspecialsproductblock)}{$xprtspecialsproductblock.id_xprtspecialsproductblock}{/if}" class="tab-pane fade">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtspecialsproductblock.products class='xprt_specialproductsblock ' id=''}
			</div>
		{else}
			<div id="xprt_specialproductsblock_tab_{if isset($xprtspecialsproductblock.id_xprtspecialsproductblock)}{$xprtspecialsproductblock.id_xprtspecialsproductblock}{/if}" class="tab-pane fade">
				<p class="alert alert-info">{l s='No products at this time.' mod='xprtspecialsproductblock'}</p>
			</div>
		{/if}
{/if}