{if isset($xprtsubcategories)}
	<div class="xprtsubcategoryblock sub_categories_area m_bottom_30" style="{if isset($xprtsubsection_margin)}{$xprtsubsection_margin}{/if}">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtsubcat_title)}
				<h3 class="page-heading">
					<em>{$xprtsubcat_title}</em>
					<div class="heading_carousel_arrow"></div>
				</h3>
			{/if}
			{if isset($xprtsubcat_subtitle)}
				<p class="page_subtitle d_none">{$xprtsubcat_subtitle}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>

		<ul class="product_list grid carousel sub_categories row">
			{foreach from=$xprtsubcategories item=category}
				<li class="sub_categories_list col-sm-{if isset($xprtsubnumber_col)}{$xprtsubnumber_col}{else}4{/if} m_bottom_30">
					<div class="sub_categories_list_content">
						<div class="sub_categories_list_title">
							<h4><a href="{$link->getCategoryLink($category.id_category, $category.link_rewrite)|escape:'html':'UTF-8'}">{$category.name}</a></h4>
							<p class="cat_view_link"><a href="{$link->getCategoryLink($category.id_category, $category.link_rewrite)|escape:'html':'UTF-8'}">{l s='View category' mod='xprtsubcategoryblock'}</a></p>
						</div>
						<div class="sub_categories_list_thumbnail">
							<a href="{$link->getCategoryLink($category.id_category, $category.link_rewrite)|escape:'html':'UTF-8'}">
								{assign var="catextraextrathumb" value="xprtcatextra{$category.id_category}extrathumb"}
								{assign var="catextraextrathumbimg" value="{Configuration::get({$catextraextrathumb})}"}
								{assign var="extraimage" value="{$modules_dir}xprtcategoryextraimg/images/{$catextraextrathumbimg}"}
								{if (isset($catextraextrathumbimg) && !empty($catextraextrathumbimg))}
									<img class="img-responsive" src="{$extraimage}" alt="{$category.name|escape:'html':'UTF-8'}">
								{else}
									<img class="replace-2x" src="{$img_dir}no-image.jpg" alt=""/>
								{/if}
							</a>
						</div>
					</div>
				</li>
			{/foreach}
		</ul>
	</div>
{/if}
{assign var="xprtsubnumber_cols" value={12/($xprtsubnumber_col)}}
{addJsDef subblockcategoriescol=$xprtsubnumber_cols|intval}
<script type="text/javascript">
	$(document).ready(function(){
		var subCatBlock = $('.sub_categories_area');
		var subsliderSelect = subCatBlock.find('.sub_categories.carousel'); 
		var arrowSelect = subCatBlock.find('.heading_carousel_arrow'); 
		if (typeof(subblockcategoriescol) == 'undefined')
			subblockcategoriescol = 4;
		if (!!$.prototype.slick)
		subsliderSelect.slick({
			infinite: true,
			dots: false,
			autoplay: false,
			infinite: false,
			slide: '.sub_categories_list',
			slidesToShow : subblockcategoriescol,
			slidesToScroll : 1,
			appendArrows: subsliderSelect,
			arrows: true,
			prevArrow : '<i class="slick-prev arrow-double-left"></i>',
			nextArrow : '<i class="slick-next arrow-double-right"></i>',
			responsive:[
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
						// slidesToShow : prd_per_column,
					}
				},
				{
					breakpoint: 993,
					settings: {
						slidesToShow: 3,
						// slidesToShow : prd_per_column_tab,
					}
				},
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 2,
						// slidesToShow : prd_per_column_tab,
					}
				},
				{
					breakpoint: 641,
					settings: {
						slidesToShow: 2,
						// slidesToShow : prd_per_column_tab,
					}
				},
				{
					breakpoint: 481,
					settings: {
						slidesToShow: 1,
						// slidesToShow : prd_per_column_mob,
					}
				}
			]
		});
	});
</script>