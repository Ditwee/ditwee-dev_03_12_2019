<?php

if (!defined('_PS_VERSION_'))
	exit;
class xprtsubcategoryblock extends Module
{
	public function __construct()
	{
		$this->name = 'xprtsubcategoryblock';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Sub Category Block');
		$this->description = $this->l('Great Store Theme Sub Category Block');
	}
	public function install()
	{
		if (!parent::install()
			|| !$this->registerHook('displayCatTop')
		)
			return false;
		Configuration::updateValue('xprtCATEGORYSUB_NBR',20);
		Configuration::updateValue('xprtSUBCATEGORYSUB_NBR',20);
		Configuration::updateValue('xprtsubsection_margin', '0px 0px 30px 0px');
		Configuration::updateValue('xprtsubnumber_col', 3);
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			Configuration::updateValue('xprtsubcat_title_'.$l['id_lang'],'Popular categories');
			Configuration::updateValue('xprtsubcat_subtitle_'.$l['id_lang'],'Shop by most popular categories');
		}
		return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall())
			return false;
		return true;
	}
	public function getContent()
	{
		$output = '';
		$errors = array();
		if(Tools::isSubmit('submitxprtsubcategoryblock'))
		{
				Configuration::updateValue('xprtCATEGORYSUB_NBR', (int)Tools::getvalue('xprtCATEGORYSUB_NBR'));
				Configuration::updateValue('xprtSUBCATEGORYSUB_NBR', (int)Tools::getvalue('xprtSUBCATEGORYSUB_NBR'));
				Configuration::updateValue('xprtsubnumber_col', (int)Tools::getvalue('xprtsubnumber_col'));
				Configuration::updateValue('xprtsubsection_margin', Tools::getvalue('xprtsubsection_margin'));

				$langs = Language::getLanguages();
				foreach($langs as $l)
				{
					Configuration::updateValue('xprtsubcat_title_'.$l['id_lang'],Tools::getvalue('xprtsubcat_title_'.$l['id_lang']));
					Configuration::updateValue('xprtsubcat_subtitle_'.$l['id_lang'],Tools::getvalue('xprtsubcat_title_'.$l['id_lang']));
				}
				$output = $this->displayConfirmation($this->l('Your settings have been updated.'));
		}
		return $output.$this->renderForm();
	}
	public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Title'),
						'name' => 'xprtsubcat_title',
						'lang' => true,
						'desc' => $this->l('Enter the title.'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Subtitle'),
						'name' => 'xprtsubcat_subtitle',
						'lang' => true,
						'desc' => $this->l('Enter Subtitle'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Section margin'),
						'name' => 'xprtsubsection_margin',
						'class' => 'fixed-width-lg',
						'desc' => $this->l('Enter section margin: Exaple: (0px 0px 30px 0px) top right bottom left'),
					),
					array(
						'type' => 'select',
						'label' => $this->l('Number of column to show'),
						'name' => 'xprtsubnumber_col',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Enter number of column to show'),
							'options' => array(
								'id' => 'id',
							    'name' => 'name',
							    'query' => array(
							        array(
							            'id'=>'12',
							            'name'=>'one',
							        ),
							        array(
							            'id'=>'6',
							            'name'=>'two',
							        ),
							        array(
							            'id'=>'4',
							            'name'=>'three',
							        ),
							        array(
							            'id'=>'3',
							            'name'=>'four',
							        ),
							    ),
							),
					),
					// array(
					// 	'type' => 'text',
					// 	'label' => $this->l('Number of Parent Category to be displayed'),
					// 	'name' => 'xprtCATEGORYSUB_NBR',
					// 	'class' => 'fixed-width-xs',
					// 	'desc' => $this->l('Number of Parent Category to be displayed.'),
					// ),
					// array(
					// 	'type' => 'text',
					// 	'label' => $this->l('Number of Sub Category to be displayed'),
					// 	'name' => 'xprtSUBCATEGORYSUB_NBR',
					// 	'class' => 'fixed-width-xs',
					// 	'desc' => $this->l('Number of Sub Category to be displayed.'),
					// ),
					
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submit'.$this->name;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}
	public function getConfigFieldsValues()
	{
		$langs = Language::getLanguages();
		$xprtsubcat_title = array();
		$xprtsubcat_subtitle = array();
		foreach($langs as $l)
		{
			$xprtsubcat_title[$l['id_lang']] = Tools::getValue('xprtsubcat_title_'.$l['id_lang'], Configuration::get('xprtsubcat_title_'.$l['id_lang']));
			$xprtsubcat_subtitle[$l['id_lang']] = Tools::getValue('xprtsubcat_subtitle_'.$l['id_lang'], Configuration::get('xprtsubcat_subtitle_'.$l['id_lang']));
		}
		return array(
			'xprtCATEGORYSUB_NBR' => Tools::getValue('xprtCATEGORYSUB_NBR', (int)Configuration::get('xprtCATEGORYSUB_NBR')),
			'xprtSUBCATEGORYSUB_NBR' => Tools::getValue('xprtSUBCATEGORYSUB_NBR', (int)Configuration::get('xprtSUBCATEGORYSUB_NBR')),
			'xprtsubnumber_col' => Tools::getValue('xprtsubnumber_col', (int)Configuration::get('xprtsubnumber_col')),
			'xprtsubsection_margin' => Tools::getValue('xprtsubsection_margin', Configuration::get('xprtsubsection_margin')),
			'xprtsubcat_title' => $xprtsubcat_title,
			'xprtsubcat_subtitle' => $xprtsubcat_subtitle,
		);
	}
	public static function NumberOfProduct($id = NULL)
	{
		if($id==NULL)
			return false;
		$sql = 'SELECT COUNT(id_category) FROM `'._DB_PREFIX_.'category_product` WHERE `id_category` = '.$id;
		$results = Db::getInstance()->getValue($sql);
		return $results;
	}
	public static function GetCategory($id_parent = null)
	{
		$results = array();
		$context = Context::getContext();
		$id_lang = (int)$context->language->id;
		$extraimage_uri = _THEME_IMG_DIR_."no-image.jpg";
		if($id_parent != null){
			$category = new Category($id_parent);
			$categories = $category->getSubCategories($id_lang);
		}else{
			$categories = array();
		}
		if(isset($categories) && !empty($categories)){
			foreach($categories as &$catego){
				$catego['productcount'] =  self::NumberOfProduct($catego['id_category']);
				$catextraextrathumbimg = Configuration::get("xprtcatextra".$catego['id_category']."extrathumb");
				if(isset($catextraextrathumbimg) && !empty($catextraextrathumbimg) && Module::isInstalled('xprtcategoryextraimg'))
				{
					$extraimage_uri = _MODULE_DIR_."xprtcategoryextraimg/images/{$catextraextrathumbimg}";
				}
				$catego['catgegor_image'] =  $extraimage_uri;
				$catego['id_image'] =  ($catego['id_category'] && file_exists(_PS_CAT_IMG_DIR_.(int)$catego['id_category'].'.jpg')) ? (int)$catego['id_category'] : $context->language->iso_code.'-default';
			}
		}
		return $categories;
	}
	public function HookExecute($id_parent = null)
	{
		if($id_parent == null)
			return false;
		$id_lang = (int)$this->context->language->id;
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		$xprtsubcat_title = Configuration::get('xprtsubcat_title_'.$id_lang);
		$xprtsubcat_subtitle = Configuration::get('xprtsubcat_subtitle_'.$id_lang);
		if(empty($xprtsubcat_title)){
			$xprtsubcat_title = Configuration::get('xprtsubcat_title_'.$default_lang);
		}
		if(empty($xprtsubcat_title)){
			$xprtsubcat_title = Configuration::get('xprtsubcat_title');
		}
		if(empty($xprtsubcat_subtitle)){
			$xprtsubcat_subtitle = Configuration::get('xprtsubcat_subtitle_'.$default_lang);
		}
		if(empty($xprtsubcat_subtitle)){
			$xprtsubcat_subtitle = Configuration::get('xprtsubcat_subtitle');
		}
		$this->smarty->assign(array(
				'xprtsubcategories' => self::GetCategory($id_parent),
				'xprtSUBCATEGORYSUB_NBR' => (int)Configuration::get('xprtSUBCATEGORYSUB_NBR'),
				'xprtsubnumber_col' => (int)Configuration::get('xprtsubnumber_col'),
				'xprtsubsection_margin' => Configuration::get('xprtsubsection_margin'),
				'xprtsubcat_title' => $xprtsubcat_title,
				'xprtsubcat_subtitle' => $xprtsubcat_subtitle,
			));
		return $this->display(__FILE__,'views/templates/front/xprtsubcategoryblock.tpl');	
	}
	public function hookdisplayCatTop($params)
	{
		$id_category = Tools::getValue('id_category');
		if($id_category){
			return $this->HookExecute($id_category);
		}else{
			return false;
		}
	}
}
