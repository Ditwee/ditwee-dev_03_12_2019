{if isset($xprtsupplierwiseproductblock) && !empty($xprtsupplierwiseproductblock)}
	{if isset($xprtsupplierwiseproductblock.device)}
		{assign var=device_data value=$xprtcrossviewproductblock.device|json_decode:true}
	{/if}
	<div id="xprtsupplierwiseproductblock_{$xprtsupplierwiseproductblock.id_xprtsupplierwiseproductblock}" class="xprtsupplierwiseproductblock xprt_default_products_block" style="margin:{$xprtsupplierwiseproductblock.section_margin};">
		<div class="page_title_area {$xprt.home_title_style}">
			{if isset($xprtsupplierwiseproductblock.title)}
				<h3 class="page-heading">
					<em>{$xprtsupplierwiseproductblock.title}</em>
					<span class="heading_carousel_arrow"></span>
				</h3>
			{/if}
			{if isset($xprtsupplierwiseproductblock.sub_title)}
				<p class="page_subtitle d_none">{$xprtsupplierwiseproductblock.sub_title}</p>
			{/if}
			<div class="heading-line d_none"><span></span></div>
		</div>
		{if isset($xprtsupplierwiseproductblock) && $xprtsupplierwiseproductblock}
			<div id="xprt_specialproductsblock_{$xprtsupplierwiseproductblock.id_xprtsupplierwiseproductblock}" class="xprt_default_products_block_content">
				{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtsupplierwiseproductblock.products id='' class="xprt_specialproductsblock {if $xprtsupplierwiseproductblock.enable_carousel == 1}carousel{/if}"}
			</div>
		{else}
			<div class="xprt_default_products_block_content">
				<p class="alert alert-info">{l s='No special products at this time.' mod='xprtsupplierwiseproductblock'}</p>
			</div>
		{/if}
	</div>
<script type="text/javascript">
	var xprtsupplierwiseproductblock = $("#xprtsupplierwiseproductblock_{$xprtsupplierwiseproductblock.id_xprtsupplierwiseproductblock}");
	var sliderSelect = xprtsupplierwiseproductblock.find('ul.product_list.carousel'); 
	var arrowSelect = xprtsupplierwiseproductblock.find('.heading_carousel_arrow'); 


	{if isset($xprtsupplierwiseproductblock.nav_arrow_style) && ($xprtsupplierwiseproductblock.nav_arrow_style == 'arrow_top')}
		var appendArrows = arrowSelect;
	{else}
		var appendArrows = sliderSelect;
	{/if}
	if (!!$.prototype.slick)
	sliderSelect.slick( { 
		infinite: {if isset($xprtsupplierwiseproductblock.play_again)}{$xprtsupplierwiseproductblock.play_again|boolval|var_export:true}{else}false{/if},
		autoplay: {if isset($xprtsupplierwiseproductblock.autoplay)}{$xprtsupplierwiseproductblock.autoplay|boolval|var_export:true}{else}false{/if},
		pauseOnHover: {if isset($xprtsupplierwiseproductblock.pause_on_hover)}{$xprtsupplierwiseproductblock.pause_on_hover|boolval|var_export:true}{else}true{/if},
		dots: {if isset($xprtsupplierwiseproductblock.navigation_dots)}{$xprtsupplierwiseproductblock.navigation_dots|boolval|var_export:true}{else}false{/if},
		arrows: {if isset($xprtsupplierwiseproductblock.navigation_arrow)}{$xprtsupplierwiseproductblock.navigation_arrow|boolval|var_export:true}{else}false{/if},
		appendArrows: arrowSelect,
		nextArrow : '<i class="slick-next arrow-double-right"></i>',
		prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		rows: {if isset($xprtsupplierwiseproductblock.product_rows)}{$xprtsupplierwiseproductblock.product_rows|intval}{else}1{/if},
		// slidesPerRow: 3,
		slidesToShow : {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
		slidesToScroll : {if isset($xprtsupplierwiseproductblock.product_scroll) && ($xprtsupplierwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
		responsive:[
			 { 
				breakpoint: 1200,
				settings:  { 
					slidesToShow: {if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtsupplierwiseproductblock.product_scroll) && ($xprtsupplierwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_lg)}{$device_data.device_lg|intval}{else}4{/if}{/if},
					// slidesToShow : prd_per_column,
				 } 
			 } ,
			 { 
				breakpoint: 993,
				settings:  { 
					slidesToShow: {if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if},
					slidesToScroll : {if isset($xprtsupplierwiseproductblock.product_scroll) && ($xprtsupplierwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_md)}{$device_data.device_md|intval}{else}4{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 769,
				settings:  { 
					slidesToShow: {if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if},
					slidesToScroll : {if isset($xprtsupplierwiseproductblock.product_scroll) && ($xprtsupplierwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_sm)}{$device_data.device_sm|intval}{else}3{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 641,
				settings:  { 
					slidesToShow: {if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if},
					slidesToScroll : {if isset($xprtsupplierwiseproductblock.product_scroll) && ($xprtsupplierwiseproductblock.product_scroll=='per_item')}1{else}{if isset($device_data.device_xs)}{$device_data.device_xs|intval}{else}2{/if}{/if},
					// slidesToShow : 2,
				 } 
			 } ,
			 { 
				breakpoint: 481,
				settings:  { 
					slidesToShow: 1,
					slidesToScroll : 1,
					// slidesToShow : 1,
				 } 
			 } 
		]
	 } );
</script>
{/if}