{if isset($xprtsupplierwiseproductblock) && !empty($xprtsupplierwiseproductblock)}
	{if isset($xprtsupplierwiseproductblock.device)}
		{assign var=device_data value=$xprtcrossviewproductblock.device|json_decode:true}
	{/if}
	<div class="xprt_product_home_small col-sm-4">
		<div class="xprtsupplierwiseproductblock block carousel">
			<h4 class="title_block">
		    	{$xprtsupplierwiseproductblock.title}
		    </h4>
		    <div class="block_content products-block">
		        {if isset($xprtsupplierwiseproductblock) && $xprtsupplierwiseproductblock}
		        	{include file="$tpl_dir./product-list/product-list-simple.tpl" xprtprdcolumnclass=$device_data products=$xprtsupplierwiseproductblock.products}
		        {else}
	        		<p class="alert alert-info">{l s='No products at this time.' mod='xprtsupplierwiseproductblock'}</p>
		        {/if}
		    </div>
		</div>
	</div>
{/if}