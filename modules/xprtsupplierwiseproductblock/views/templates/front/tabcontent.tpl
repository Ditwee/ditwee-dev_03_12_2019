{if isset($xprtsupplierwiseproductblock) && !empty($xprtsupplierwiseproductblock)}
	{if isset($xprtsupplierwiseproductblock.device)}
		{assign var=device_data value=$xprtcrossviewproductblock.device|json_decode:true}
	{/if}
	{if isset($xprtsupplierwiseproductblock) && $xprtsupplierwiseproductblock}
		<div id="xprt_specialproductsblock_tab_{if isset($xprtsupplierwiseproductblock.id_xprtsupplierwiseproductblock)}{$xprtsupplierwiseproductblock.id_xprtsupplierwiseproductblock}{/if}" class="tab-pane fade">
			{include file="$tpl_dir./product-list.tpl" xprtprdcolumnclass=$device_data products=$xprtsupplierwiseproductblock.products class='xprt_specialproductsblock ' id=''}
		</div>
	{else}
		<div id="xprt_specialproductsblock_tab_{if isset($xprtsupplierwiseproductblock.id_xprtsupplierwiseproductblock)}{$xprtsupplierwiseproductblock.id_xprtsupplierwiseproductblock}{/if}" class="tab-pane fade">
			<p class="alert alert-info">{l s='No products at this time.' mod='xprtsupplierwiseproductblock'}</p>
		</div>
	{/if}
{/if}