<?php

require_once (dirname(__FILE__) . '/../../classes/xprttestimonialclass.php');
class AdminTestimonialController extends ModuleAdminController{
  public $asso_type = 'shop';
  private $original_filter = '';
  protected $position_identifier = 'id_xprttestimonial';
	public function __construct()
	{
    $this->table = 'xprttestimonial';
    $this->className = 'xprttestimonialclass';
    $this->lang = true;
    $this->deleted = false;
    $this->module = 'xprttestimonial';
    $this->explicitSelect = true;
    $this->_defaultOrderBy = 'position';
    $this->allow_export = false;
    $this->_defaultOrderWay = 'DESC';
    $this->bootstrap = true;
    $this->context = Context::getContext();
        if (Shop::isFeatureActive())
        Shop::addTableAssociation($this->table, array('type' => 'shop'));
        parent::__construct();
		$this->fields_list = array(
                            'id_xprttestimonial' => array(
                                    'title' => $this->l('Id'),
                                    'width' => 100,
                                    'type' => 'text',
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                            ),
                            'name' => array(
                                    'title' => $this->l('Name'),
                                    'width' => 440,
                                    'type' => 'text',
                                    'lang'=>true,
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                            ),
                            'company' => array(
                                    'title' => $this->l('Company'),
                                    'width' => 440,
                                    'type' => 'text',
                                    'lang'=>true,
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                            ),
                            'desination' => array(
                                    'title' => $this->l('Desination'),
                                    'width' => 440,
                                    'type' => 'text',
                                    'lang'=>true,
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                            ),
                            'position' => array(
                                    'title' => $this->l('Position'),
                                    'filter_key' => 'a!position',
                                    'position' => 'position',
                                    'align' => 'center'
                          ),
                          'active' => array(
                                    'title' => $this->l('Status'),
                                    'width' => '70',
                                    'align' => 'center',
                                    'active' => 'status',
                                    'type' => 'bool',
                                    'orderby' => false,
                                    'filter' => false,
                                    'search' => false
                          )
		          );
              $this->bulk_actions = array(
                  'delete' => array(
                      'text' => $this->l('Delete selected'),
                      'icon' => 'icon-trash',
                      'confirm' => $this->l('Delete selected items?')
                  )
              );
		parent::__construct();
	}
  public function init()
  {
      parent::init();
      $this->_join = 'LEFT JOIN '._DB_PREFIX_.'xprttestimonial_shop sbs ON a.id_xprttestimonial=sbs.id_xprttestimonial && sbs.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
      $this->_select = 'sbs.id_shop';
      $this->_defaultOrderBy = 'a.position';
      $this->_defaultOrderWay = 'DESC';
      if(Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
        $this->_group = 'GROUP BY a.id_xprttestimonial';
      $this->_select = 'a.position position';
  }
	public function renderList()
  {
    $this->addRowAction('edit');
    $this->addRowAction('delete');
    return parent::renderList();
  }
	public function renderForm()
  	{
  		$id_xprttestimonial = Tools::getValue("id_xprttestimonial");
  		if(!empty($id_xprttestimonial)){
  			$xprttestimonialclass = new xprttestimonialclass($id_xprttestimonial);
  			$xprttest_image = $xprttestimonialclass->authorimage;
  			$img_src = Context::getContext()->shop->getBaseURL().'modules/xprttestimonial/img/'.$xprttest_image;
  			$img_src_txt = '<img src="'.$img_src.'" height="100" width="auto">';
  		}else{
  			$img_src_txt = '';
  		}
        // $image_url = false;
        // $image_size = file_exists($image_url) ? filesize($image_url) / 1000 : false;
        $this->fields_form = array(
          'legend' => array(
          'title' => $this->l('Testimonial Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'lang'=>true,
                    'required' => true,
                    'desc' => $this->l('Enter Your Author Name')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('company'),
                    'name' => 'company',
                    'lang'=>true,
                    'required' => false,
                    'desc' => $this->l('Enter Your Author Company Name')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Desination'),
                    'name' => 'desination',
                    'lang'=>true,
                    'required' => false,
                    'desc' => $this->l('Enter Your Author Company Desination')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Company URL'),
                    'name' => 'link',
                    'lang'=>true,
                    'required' => false,
                    'desc' => $this->l('Enter Your Author Company URL')
                ),array(
                    'type' => 'textarea',
                    'label' => $this->l('Message'),
                    'name' => 'msg',
                    'rows' => 10,
                    'cols' => 62,
                    'class' => '',
                    'lang'=>true,
                    'autoload_rte' => false,
                    'required' => true,
                    'desc' => $this->l('Enter Your Author Message')
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Author Image'),
                    'name' => 'authorimage',
                    'desc' => $img_src_txt,
                    'display_image' => false,
                    // 'image' => $image_url ? $image_url : false,
                    // 'size' => $image_size,
                ),
                array(
                       'type' => 'switch',
                       'label' => $this->l('Status'),
                       'name' => 'active',
                       'required' => true,
                       'class' => 't',
                       'is_bool' => true,
                       'values' => array(
                           array(
                             'id' => 'active',
                             'value' => 1,
                             'label' => $this->l('Enabled')
                           ),
                           array(
                             'id' => 'active',
                             'value' => 0,
                             'label' => $this->l('Disabled')
                           )
                       )
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            ),
            'buttons' => array(
                'save-and-stay' => array(
                  'name' => 'submitAdd'.$this->table.'AndStay',
                  'type' => 'submit',
                  'title' => $this->l('Save And Stay'),
                  'class' => 'btn btn-default pull-right',
                  'icon' => 'process-icon-save'
                )
            )
        );
        if (Shop::isFeatureActive())
        {
          $this->fields_form['input'][] = array(
            'type' => 'shop',
            'label' => $this->l('Shop association:'),
            'name' => 'checkBoxShopAsso',
          );
        }
        if (!($xprttestimonialclass = $this->loadObject(true)))
            return;
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save   '),
            'class' => 'button'
        );
        return parent::renderForm();
    }
    public function setMedia()
    {          
          parent::setMedia();
          $this->addJqueryUi('ui.widget');
          $this->addJqueryPlugin('tagify');
    }
    public function initToolbar()
    {
        parent::initToolbar();
    }
    public function processPosition()
    {
      if ($this->tabAccess['edit'] !== '1')
        $this->errors[] = Tools::displayError('You do not have permission to edit this.');
      else if (!Validate::isLoadedObject($object = new xprttestimonialclass((int)Tools::getValue($this->identifier, Tools::getValue('id_xprttestimonial', 1)))))
        $this->errors[] = Tools::displayError('An error occurred while updating the status for an object.').' <b>'.
          $this->table.'</b> '.Tools::displayError('(cannot load object)');
      if (!$object->updatePosition((int)Tools::getValue('way'), (int)Tools::getValue('position')))
        $this->errors[] = Tools::displayError('Failed to update the position.');
      else
      {
        $object->regenerateEntireNtree();
        Tools::redirectAdmin(self::$currentIndex.'&'.$this->table.'Orderby=position&'.$this->table.'Orderway=asc&conf=5'.(($id_xprttestimonial = (int)Tools::getValue($this->identifier)) ? ('&'.$this->identifier.'='.$id_xprttestimonial) : '').'&token='.Tools::getAdminTokenLite('AdminTestimonialController'));
      }
    }
    public function ajaxProcessUpdatePositions()
    {
      $id_xprttestimonial = (int)(Tools::getValue('id'));
      $way = (int)(Tools::getValue('way'));
      $positions = Tools::getValue($this->table);
      if (is_array($positions))
        foreach ($positions as $key => $value)
        {
          $pos = explode('_', $value);
          if ((isset($pos[1]) && isset($pos[2])) && ($pos[2] == $id_xprttestimonial))
          {
            $position = $key + 1;
            break;
          }
        }
      $xprttestimonialclass = new xprttestimonialclass($id_xprttestimonial);
      if (Validate::isLoadedObject($xprttestimonialclass))
      {
        if (isset($position) && $xprttestimonialclass->updatePosition($way, $position))
        {
          Hook::exec('actionxprttestimonialclassUpdate');
          die(true);
        }
        else
          die('{"hasError" : true, errors : "Can not update xprttestimonial position"}');
      }
      else
        die('{"hasError" : true, "errors" : "This xprttestimonial can not be loaded"}');
    }
}

