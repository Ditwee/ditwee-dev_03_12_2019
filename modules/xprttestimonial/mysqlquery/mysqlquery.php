<?php

$mysqlquery = array();

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprttestimonial` (
				`id_xprttestimonial` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`active` int(10) NOT NULL,
				`position`int(10) NOT NULL,
				`authorimage` VARCHAR(100) NOT NULL,
				PRIMARY KEY (`id_xprttestimonial`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprttestimonial_lang` (
				`id_xprttestimonial` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL,
				`name` VARCHAR(100) NOT NULL,
				`company` VARCHAR(150) NOT NULL,
				`desination` VARCHAR(100) NOT NULL,
				`msg` text NOT NULL,
				`link` VARCHAR(450) NOT NULL,
				PRIMARY KEY (`id_xprttestimonial`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprttestimonial_shop` (
			  `id_xprttestimonial` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprttestimonial`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$mysqlquery_u = array();

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprttestimonial`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprttestimonial_lang`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprttestimonial_shop`';
