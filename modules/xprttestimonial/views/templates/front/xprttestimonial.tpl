{if isset($alltestimonial)}
	<div id="testimonial_block_area" class="xprttestimonial kr_testimonial_area xprt_parallax_section" style="background-image:url({$xprttest_image}); min-height:{$xprttest_height}px; padding: {$xprttest_padding}; margin:{$xprttest_margin};">
		<div class="kr_testimonial_content">
			<div class="kr_testimonial_top_content">
				<h3 class="msg_title">{l s='What’s Client Say?' mod='xprttestimonial'}</h3>
				<div class="testimonial_dots"></div>
			</div>
			<ul class="clearfix carousel">
				{foreach from=$alltestimonial item="testimonial"}
					<li>
						<div class="single_testimonial_content">
							<div class="single_testimonial_content_top">
								<p class="author_image"><img src="{$modules_dir}xprttestimonial/img/{$testimonial.authorimage}" alt=""></p>
							</div>
							<div class="single_testimonial_content_bottom">
								{if $testimonial.msg != ''}
									<p class="msg_text">{$testimonial.msg}</p>
								{/if}
								{if $testimonial.name != ''}
									<p class="author">{$testimonial.name}</p>
								{/if}
								{if $testimonial.company != ''}
									<p class="company"><a href="{$testimonial.link}">{$testimonial.company}</a></p>
								{/if}
								{if $testimonial.desination != ''}
									<p class="designation">{$testimonial.desination}</p>
								{/if}
							</div>
						</div>
					</li>
				{/foreach}
			</ul>
		</div>
	</div>
{/if}