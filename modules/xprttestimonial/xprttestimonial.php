<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;
require_once (dirname(__FILE__) . '/classes/xprttestimonialclass.php');
class xprttestimonial extends Module
{

    public $tabs_files_url = '/tabs/tabs.php';
    public $mysql_files_url = '/mysqlquery/mysqlquery.php';
	public function __construct()
	{
		$this->name = 'xprttestimonial';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Testimonial block');
		$this->description = $this->l('Displays Testimonial block of the shop.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install()
	{
		if(!parent::install()
			|| !$this->registerHook('displayhomefullwidthmiddle')
			|| !$this->Register_SQL()
			|| !$this->Register_Tabs()
			|| !$this->xpertsampledata()
			)
			return false;
			return true;
	}
	public function uninstall()
	{
		if(!parent::uninstall()
			|| !$this->UnRegister_SQL()
			|| !$this->UnRegister_Tabs()
			)
			return false;
		else
			return true;
	}
	public function hookDisplayHome($params)
	{
        $xprttest_image =   Configuration::get('xprttest_image');
        $xprttest_height = Configuration::get('xprttest_height');
        $xprttest_padding = Configuration::get('xprttest_padding');
        $xprttest_margin = Configuration::get('xprttest_margin');
        $xprttest_fullwidth = Configuration::get('xprttest_fullwidth');
		$this->smarty->assign(array(
                'xprttest_image' => $this->context->link->protocol_content.Tools::getMediaServer($xprttest_image).$this->_path.'img/'.$xprttest_image,
                'xprttest_height' => $xprttest_height,
                'xprttest_padding' => $xprttest_padding,
                'xprttest_margin' => $xprttest_margin,
                'xprttest_fullwidth' => $xprttest_fullwidth,
				'alltestimonial' => xprttestimonialclass::GetAllTestimonial(),
		));
		return $this->display(__FILE__, 'views/templates/front/xprttestimonial.tpl');
	}
	public function hookdisplayhomefullwidthmiddle($params)
	{
		return $this->hookDisplayHome($params);
	}
	public function hookDisplayFooter($params)
	{
		return $this->hookDisplayHome($params);
	}
	public function UnRegister_Tabs()
    {
        $tabs_lists = array();
        require_once(dirname(__FILE__) .$this->tabs_files_url);
        if(isset($tabs_lists) && !empty($tabs_lists)){
        	foreach($tabs_lists as $tab_list){
        	    $tab_list_id = Tab::getIdFromClassName($tab_list['class_name']);
        	    if(isset($tab_list_id) && !empty($tab_list_id)){
        	        $tabobj = new Tab($tab_list_id);
        	        $tabobj->delete();
        	    }
        	}
        } 
        $save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
        if($save_tab_id != 0){
        	$count = Tab::getNbTabs($save_tab_id);
        	if($count == 0){
        		if(isset($save_tab_id) && !empty($save_tab_id)){
        		    $tabobjs = new Tab($save_tab_id);
        		    $tabobjs->delete();
        		}
        	}
        }
        return true;
    }
    public function RegisterParentTabs(){
	    	$langs = Language::getLanguages();
	    	$save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
	    	if($save_tab_id != 0){
	    		return $save_tab_id;
	    	}else{
	    		$tab_listobj = new Tab();
	    		$tab_listobj->class_name = 'Adminxprtdashboard';
	    		$tab_listobj->id_parent = 0;
	    		$tab_listobj->module = $this->name;
	    		foreach($langs as $l)
	    		{
	    		    $tab_listobj->name[$l['id_lang']] = $this->l("Theme Settings");
	    		}
	    		if($tab_listobj->save())
	    			return (int)$tab_listobj->id;
	    		else
	    			return (int)$save_tab_id;
	    	}
    }
    public function Register_Tabs()
    {
        $tabs_lists = array();
        $langs = Language::getLanguages();
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $save_tab_id = $this->RegisterParentTabs();
            require_once(dirname(__FILE__) .$this->tabs_files_url);
            if(isset($tabs_lists) && !empty($tabs_lists))
	            foreach ($tabs_lists as $tab_list)
	            {
	                $tab_listobj = new Tab();
	                $tab_listobj->class_name = $tab_list['class_name'];
	                if($tab_list['id_parent'] == 'parent'){
	                    $tab_listobj->id_parent = $save_tab_id;
	                }else{
	                    $tab_listobj->id_parent = $tab_list['id_parent'];
	                }
	                if(isset($tab_list['module']) && !empty($tab_list['module'])){
	                    $tab_listobj->module = $tab_list['module'];
	                }else{
	                    $tab_listobj->module = $this->name;
	                }
	                foreach($langs as $l)
	                {
	                    $tab_listobj->name[$l['id_lang']] = $this->l($tab_list['name']);
	                }
	                $tab_listobj->save();
	            }
        return true;
    }
    public function Register_SQL()
    {
        $mysqlquery = array();
            require_once(dirname(__FILE__).$this->mysql_files_url);
            if(isset($mysqlquery) && !empty($mysqlquery))
                foreach($mysqlquery as $query){
                    if(!Db::getInstance()->Execute($query,false))
                        return false;
                }
        return true;
    }
    public function UnRegister_SQL()
    {
        $mysqlquery_u = array();
            require_once(dirname(__FILE__).$this->mysql_files_url);
            if(isset($mysqlquery_u) && !empty($mysqlquery_u))
                foreach($mysqlquery_u as $query_u){
                    if(!Db::getInstance()->Execute($query_u,false))
                        return false;
                }
        return true;
    }
    public function processImage($FILES) {
        if (isset($FILES['xprttest_image']) && isset($FILES['xprttest_image']['tmp_name']) && !empty($FILES['xprttest_image']['tmp_name'])) {
                $ext = substr($FILES['xprttest_image']['name'], strrpos($FILES['xprttest_image']['name'], '.') + 1);
                $id = time();
                $file_name = $id . '.' . $ext;
                $path = _PS_MODULE_DIR_ .'xprttestimonial/img/'.$file_name;
                if (!move_uploaded_file($FILES['xprttest_image']['tmp_name'], $path))
                    return false;         
                else
                    return $file_name;   
        }else{
        	return false;
        }
    }
    public function postProcess()
    {
        if(Tools::isSubmit('submit'.$this->name))
        {
            $xprttest_image = $this->processImage($_FILES);
            if($xprttest_image){
            	Configuration::updateValue('xprttest_image',$xprttest_image);
            }
            Configuration::updateValue('xprttest_height',Tools::getValue('xprttest_height'));
            Configuration::updateValue('xprttest_padding',Tools::getValue('xprttest_padding'));
            Configuration::updateValue('xprttest_margin',Tools::getValue('xprttest_margin'));
            Configuration::updateValue('xprttest_fullwidth',Tools::getValue('xprttest_fullwidth'));
            return $this->displayConfirmation($this->l('The settings have been updated.'));
        }
        return '';
    }
    public function getContent()
    {
        return $this->postProcess().$this->renderForm();
    }
    public function renderForm()
    {
    	$xprttest_image = Configuration::get('xprttest_image');
    	if(!empty($xprttest_image)){
    		$img_src = $this->context->link->protocol_content.Tools::getMediaServer($xprttest_image).$this->_path.'img/'.$xprttest_image;
    		$img_src_txt = '<img src="'.$img_src.'" height="250" width="auto">';
    	}else{
    		$img_src_txt = '';
    	}
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Testimonial Settings'),
                ),
                'input' => array(
                    array(
                        'type' => 'file',
                        'label' => $this->l('Testimonial Background Image'),
                        'name' => 'xprttest_image',
                        'desc' => $this->l('Upload An Image for Your Testimonial Parrallax Background .').$img_src_txt,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Testimonial Block Height'),
                        'name' => 'xprttest_height',
                        'desc' => $this->l('Please Enter Testimonial Block Height in PX Value.')
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Testimonial Block Padding'),
                        'name' => 'xprttest_padding',
                        'desc' => $this->l('Please Enter Testimonial Block Padding in PX Value.(Example : 0px 0px 0px 0px)')
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Testimonial Block Margin'),
                        'name' => 'xprttest_margin',
                        'desc' => $this->l('Please Enter Testimonial Block Margin in PX Value.(Example : 0px 0px 0px 0px)')
                    ),
                    array(
                       'type' => 'switch',
                       'label' => $this->l('Force Full Width'),
                       'name' => 'xprttest_fullwidth',
                       'required' => true,
                       'class' => 't',
                       'is_bool' => true,
                       'values' => array(
                           array(
                             'id' => 'active',
                             'value' => 1,
                             'label' => $this->l('Enabled')
                           ),
                           array(
                             'id' => 'active',
                             'value' => 0,
                             'label' => $this->l('Disabled')
                           )
                       )
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save')
                )
            ),
        );
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit'.$this->name;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form));
    }
    public function getConfigFieldsValues()
    {
        $languages = Language::getLanguages(false);
        $fields = array();
        $fields['xprttest_image'] = Tools::getValue('xprttest_image', Configuration::get('xprttest_image'));
        $fields['xprttest_height'] = Tools::getValue('xprttest_height', Configuration::get('xprttest_height'));
        $fields['xprttest_padding'] = Tools::getValue('xprttest_padding', Configuration::get('xprttest_padding'));
        $fields['xprttest_margin'] = Tools::getValue('xprttest_margin', Configuration::get('xprttest_margin'));
        $fields['xprttest_fullwidth'] = Tools::getValue('xprttest_fullwidth', Configuration::get('xprttest_fullwidth'));
        return $fields;
    }
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_2";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
			$this->alldisabled();
    		$this->{$func}();
	    }else{
			$this->alldisabled();
	    }
	    return true;
	}
	public function alldisabled(){
		Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'xprttestimonial`;',false);
		Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'xprttestimonial_lang`;',false);
		Db::getInstance()->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'xprttestimonial_shop`;',false);
		return true;
	}
	private function xpertsample_demo_2()
	{
		Configuration::updateValue('xprttest_image',"bg.jpg");
		Configuration::updateValue('xprttest_height',398);
		Configuration::updateValue('xprttest_padding',"42px 0px 40px 0px");
		Configuration::updateValue('xprttest_margin',"0px 0px 60px 0px");
		Configuration::updateValue('xprttest_fullwidth',1);
		$dummy_datas = array(
                array(
                    'name' => 'Tyler Banks',
                    'company' => 'Menswear',
                    'desination' => '',
                    'msg' => "The whole buying experience was great. I had no need for customer service, as it all went smoothly, but as assuming it would be very good, as they did a real professional job. I love the website, too.",
                    'link' => '#',
                    'authorimage' => '1.jpg',
                    'active' => 1,
                    'position' => 0,
                ),
                array(
                    'name' => 'Arthur Porter',
                    'company' => 'Shopify',
                    'desination' => '',
                    'msg' => "I was amazed how fast I received my parka through international mail. Only 8 Business days. The price is lowered than any other companies I rearched on the website. It was really amazing!.",
                    'link' => '#',
                    'authorimage' => '2.jpg',
                    'active' => 1,
                    'position' => 1,
                ),
                array(
                    'name' => 'Dylan Mills',
                    'company' => 'CR Clothing',
                    'desination' => '',
                    'msg' => "This is the third time I have shopped with them, and its always been an easy straightforward experience; the merchandise is exactly as ordered with excellent quality and timely delivery.",
                    'link' => '#',
                    'authorimage' => '3.jpg',
                    'active' => 1,
                    'position' => 2,
                ),
            );
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        if(isset($dummy_datas) && !empty($dummy_datas)){
            $languages = Language::getLanguages(false);
            $i = 1;
            foreach($dummy_datas as $valu){
                $sqldumi2 = "INSERT INTO "._DB_PREFIX_."xprttestimonial(id_xprttestimonial,active,position,authorimage)VALUES(".(int)$i.",".(int)$valu['active'].",".(int)$i.",'".$valu['authorimage']."');";
                    Db::getInstance()->execute($sqldumi2,false);
                    // Start Lang
                foreach($languages as $language)
                {
                    $sqldumi = "INSERT INTO "._DB_PREFIX_."xprttestimonial_lang(id_xprttestimonial,id_lang,name,company,desination,msg,link)VALUES(".(int)$i.",".(int)$language['id_lang'].",'".$valu['name']."','".$valu['company']."','".$valu['desination']."','".$valu['msg']."','".$valu['link']."');";
                    Db::getInstance()->execute($sqldumi,false);
                }
                    // End Lang
                    // Start shop
                $damisqs1 = "INSERT INTO "._DB_PREFIX_."xprttestimonial_shop(id_xprttestimonial,id_shop)VALUES(".$i.",".$id_shop.");";
                Db::getInstance()->execute($damisqs1,false); 
                    // End shop
            $i = $i + 1;    
            }
        }
        return true;
	}
	private function xpertsample_demo_9()
	{
		$dummy_datas = array(
                array(
                    'name' => 'Tyler Banks',
                    'company' => 'Menswear',
                    'desination' => '',
                    'msg' => "The whole buying experience was great. I had no need for customer service, as it all went smoothly, but as assuming it would be very good, as they did a real professional job. I love the website, too.",
                    'link' => '#',
                    'authorimage' => '1.jpg',
                    'active' => 1,
                    'position' => 0,
                ),
                array(
                    'name' => 'Arthur Porter',
                    'company' => 'Shopify',
                    'desination' => '',
                    'msg' => "I was amazed how fast I received my parka through international mail. Only 8 Business days. The price is lowered than any other companies I rearched on the website. It was really amazing!.",
                    'link' => '#',
                    'authorimage' => '2.jpg',
                    'active' => 1,
                    'position' => 1,
                ),
                array(
                    'name' => 'Dylan Mills',
                    'company' => 'CR Clothing',
                    'desination' => '',
                    'msg' => "This is the third time I have shopped with them, and its always been an easy straightforward experience; the merchandise is exactly as ordered with excellent quality and timely delivery.",
                    'link' => '#',
                    'authorimage' => '3.jpg',
                    'active' => 1,
                    'position' => 2,
                ),
            );
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        if(isset($dummy_datas) && !empty($dummy_datas)){
            $languages = Language::getLanguages(false);
            $i = 1;
            foreach($dummy_datas as $valu){
                $sqldumi2 = "INSERT INTO "._DB_PREFIX_."xprttestimonial(id_xprttestimonial,active,position,authorimage)VALUES(".(int)$i.",".(int)$valu['active'].",".(int)$i.",'".$valu['authorimage']."');";
                    Db::getInstance()->execute($sqldumi2,false);
                    // Start Lang
                foreach($languages as $language)
                {
                    $sqldumi = "INSERT INTO "._DB_PREFIX_."xprttestimonial_lang(id_xprttestimonial,id_lang,name,company,desination,msg,link)VALUES(".(int)$i.",".(int)$language['id_lang'].",'".$valu['name']."','".$valu['company']."','".$valu['desination']."','".$valu['msg']."','".$valu['link']."');";
                    Db::getInstance()->execute($sqldumi,false);
                }
                    // End Lang
                    // Start shop
                $damisqs1 = "INSERT INTO "._DB_PREFIX_."xprttestimonial_shop(id_xprttestimonial,id_shop)VALUES(".$i.",".$id_shop.");";
                Db::getInstance()->execute($damisqs1,false); 
                    // End shop
            $i = $i + 1;    
            }
        }
        return true;
	}
}
