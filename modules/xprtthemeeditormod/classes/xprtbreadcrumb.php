<?php

class xprtbreadcrumb extends ObjectModel
{
	public static function GetCategoryTitle($id_category)
	{
		$id_lang = (int)Context::getContext()->language->id;
		$category = new Category($id_category,$id_lang);
		return $category->name;
	}
	public static function GetProductTitle($id_product)
	{
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
		$product = new Product($id_product, false, $id_lang, $id_shop);
		return $product->name;
	}
	public static function GetCMSTitle($id_cms)
	{
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
		$cms = new CMS($id_cms, $id_lang, $id_shop);
		return $cms->meta_title;
	}
	public static function GetManufacturerTitle($id_manufacturer)
	{
		$id_lang = (int)Context::getContext()->language->id;
		$manufacturer = new Manufacturer((int)$id_manufacturer,$id_lang);
		return $manufacturer->name;
	}
	public static function GetSupplierTitle($id_supplier)
	{
		$id_lang = (int)Context::getContext()->language->id;
		$supplier = new Supplier($id_supplier,$id_lang);
		return $supplier->name();
	}
	public static function GetBlogArchiveTitle($months='',$year='')
	{
		$monthName = date("F", mktime(0, 0, 0, $months, 10));
		return $monthName.' '.$year;
	}
	public static function GetBlogCategoryTitle($id_category)
	{
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT pl.meta_title FROM '._DB_PREFIX_.'smart_blog_category_lang pl, '._DB_PREFIX_.'smart_blog_category p , '._DB_PREFIX_.'smart_blog_category_shop ps
	           WHERE pl.id_smart_blog_category=p.id_smart_blog_category AND p.id_smart_blog_category='.$id_category.' AND pl.id_lang = '.$id_lang.' AND ps.id_smart_blog_category=p.id_smart_blog_category AND ps.id_shop = '.$id_shop;
	    if (!$result = Db::getInstance()->executeS($sql))
	        return false;
	    return $result[0]['meta_title'];
	}
	public static function GetBlogSearchTitle()
	{	
		$smartsearch = Tools::getValue('smartsearch');
		return $smartsearch;
	}
	public static function GetBlogDetailsTitle($id_post)
	{
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT pl.meta_title FROM '._DB_PREFIX_.'smart_blog_post_lang pl, '._DB_PREFIX_.'smart_blog_post p , '._DB_PREFIX_.'smart_blog_post_shop ps
	           WHERE pl.id_smart_blog_post=p.id_smart_blog_post AND p.id_smart_blog_post='.$id_post.' AND pl.id_lang = '.$id_lang.' AND ps.id_smart_blog_post=p.id_smart_blog_post AND ps.id_shop = '.$id_shop;
	    if (!$result = Db::getInstance()->executeS($sql))
	        return false;
	    return $result[0]['meta_title'];
	}
	public static function GetBlogTagpostTitle()
	{
		$keyword = Tools::getValue('tag');
		return $keyword;
	}
	public static function GetAnotherPageTitle($page)
	{
		$id_lang = (int)Context::getContext()->language->id;
		$id_shop = (int)Context::getContext()->shop->id;
	    $sql = 'SELECT pl.title FROM '._DB_PREFIX_.'meta_lang pl,'._DB_PREFIX_.'meta p WHERE pl.id_meta=p.id_meta AND pl.id_lang = '.$id_lang.' AND pl.id_shop = '.$id_shop.' AND p.page= "'.$page.'" ';
	    if(!$result = Db::getInstance()->executeS($sql))
	        return false;
	    return $result[0]['title'];
	}
}