<?php

class xprtproducttab extends ObjectModel
{
    public $id;
	public $id_xprtproducttab;
	public $selection;
	public $active = 1;
	public $position = 0;
    public $content;
	public $title;
	public static $definition = array(
		'table' => 'xprtproducttab',
		'primary' => 'id_xprtproducttab',
		'multilang' => true,
		'fields' => array(
			'selection' =>		    array('type' => self::TYPE_STRING, 'validate' => 'isString'),
			'active' =>				array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'position' =>			array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			// Lang fields
            'title' =>              array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString','required' => true),
			'content' =>			array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString','required' => true),
		)
	);
	public function __construct($id = null, $id_lang = null, $id_shop = null)
	{
        Shop::addTableAssociation('xprtproducttab', array('type' => 'shop'));
            parent::__construct($id, $id_lang, $id_shop);
    }
    public function add($autodate = true, $null_values = false)
    {
        if($this->position <= 0){
            $this->position = self::getTopPosition() + 1;
        }
        if($this->selection == ''){
            $this->selection = 'all';
        }
        if(!parent::add($autodate, $null_values) || !Validate::isLoadedObject($this))
            return false;
        return true;
    }
    public static function getTopPosition()
    {
        $sql = 'SELECT MAX(`position`)
                FROM `'._DB_PREFIX_.'xprtproducttab`';
        $position = DB::getInstance()->getValue($sql);
        return (is_numeric($position)) ? $position : -1;
    }
    public function updatePosition($way, $position)
    {
        if(!$res = Db::getInstance()->executeS('
            SELECT `id_xprtproducttab`, `position`
            FROM `'._DB_PREFIX_.'xprtproducttab`
            ORDER BY `position` ASC'
        ))
            return false;
        if(!empty($res))
        foreach($res as $xprtproducttab)
            if((int)$xprtproducttab['id_xprtproducttab'] == (int)$this->id)
        $moved_xprtproducttab = $xprtproducttab;
        if(!isset($moved_xprtproducttab) || !isset($position))
            return false;
        $queryx = ' UPDATE `'._DB_PREFIX_.'xprtproducttab`
        SET `position`= `position` '.($way ? '- 1' : '+ 1').'
        WHERE `position`
        '.($way
        ? '> '.(int)$moved_xprtproducttab['position'].' AND `position` <= '.(int)$position
        : '< '.(int)$moved_xprtproducttab['position'].' AND `position` >= '.(int)$position.'
        ');
        $queryy = ' UPDATE `'._DB_PREFIX_.'xprtproducttab`
        SET `position` = '.(int)$position.'
        WHERE `id_xprtproducttab` = '.(int)$moved_xprtproducttab['id_xprtproducttab'];
        return (Db::getInstance()->execute($queryx,false)
        && Db::getInstance()->execute($queryy,false));
    }
    public function GetTabData(){
        $context = Context::getContext();
        $id_lang = (int)$context->language->id;
        $id_shop = (int)$context->shop->id;
        $sql = 'SELECT * FROM `'._DB_PREFIX_.'xprtproducttab` ap INNER JOIN `'._DB_PREFIX_.'xprtproducttab_lang` apl ON ap.id_xprtproducttab = apl.id_xprtproducttab INNER JOIN `'._DB_PREFIX_.'xprtproducttab_shop` aps ON ap.id_xprtproducttab = aps.id_xprtproducttab WHERE apl.id_lang = '.$id_lang.' AND aps.id_shop = '.$id_shop.' AND ap.active = 1 ORDER BY ap.position DESC';
        $results = Db::getInstance()->executeS($sql);
        return $results;
    }
}
