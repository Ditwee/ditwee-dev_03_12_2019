<?php
class AdminxprtproducttabController extends ModuleAdminController {
    public function __construct()
    {
        $this->table = 'xprtproducttab';
        $this->className = 'xprtproducttab';
        $this->module = 'xprtthemeeditormod';
        $this->lang = true;
        $this->context = Context::getContext();
        $this->bootstrap = true;
        if(Shop::isFeatureActive())
        Shop::addTableAssociation($this->table, array('type' => 'shop'));
        parent::__construct();
        $this->fields_list = array(
                            'id_xprtproducttab' => array(
                                    'title' => $this->l('Id'),
                                    'width' => 100,
                                    'type' => 'text',
                            ),
                            'title' => array(
                                    'title' => $this->l('Title'),
                                    'width' => 220,
                                    'type' => 'text',
                            ),
                            'active' => array(
                                'title' => $this->l('Status'),
                                'width' => 60,
                                'align' => 'center',
                                'active' => 'status',
                                'type' => 'bool',
                                'orderby' => false
                            ),
                            'position' => array(
                                'title' => $this->l('Position'),
                                'width' => 60,
                            )
                    );
        parent::__construct();
    }
    public function init()
    {
        parent::init();
        $this->_join = 'LEFT JOIN '._DB_PREFIX_.'xprtproducttab_shop st ON a.id_xprtproducttab=st.id_xprtproducttab && st.id_shop IN('.implode(',',Shop::getContextListShopID()).')';
        $this->_select = 'st.id_shop';
        $this->_defaultOrderBy = 'a.position';
        $this->_defaultOrderWay = 'DESC';
        if(Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
        {
          $this->_group = 'GROUP BY a.id_xprtproducttab';
        }
        $this->_select = 'a.position position';
    }
    public function setMedia()
    {          
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
        $this->addJqueryPlugin('select2');
    }
    public function renderForm()
    {
        $this->fields_form = array(
          'legend' => array(
          'title' => $this->l('jakiro Theme Product Tab Creator'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Tab Title'),
                    'name' => 'title',
                    'size' => 60,
                    'required' => true,
                    'desc' => $this->l('Enter Your Tab Title Here'),
                    'lang' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Tab Content'),
                    'name' => 'content',
                    'lang' => true,
                    'rows' => 10,
                    'cols' => 62,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'required' => true,
                    'desc' => $this->l('Enter Your Tab Content Here')
                ),
                array(
                    'type' => 'selection',
                    'label' => $this->l('Which Product Want To Display'),
                    'name' => 'selection',
                    'fieldsvalues' => self::GetFieldValues(),
                    'desc' => $this->l('Which product you want to display.For all product: all product,category wise: Type product Category,Product wise: Type Product'),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Status'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                        'id' => 'active',
                        'value' => 1,
                        'label' => $this->l('Enabled')
                        ),
                        array(
                        'id' => 'active',
                        'value' => 0,
                        'label' => $this->l('Disabled')
                        )
                    )
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        if(Shop::isFeatureActive())
        {
          $this->fields_form['input'][] = array(
            'type' => 'shop',
            'label' => $this->l('Shop association:'),
            'name' => 'checkBoxShopAsso',
          );
        }
        if(!($xprtproducttab = $this->loadObject(true)))
            return;
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save   '),
            'class' => 'button'
        );
        return parent::renderForm();
    }
    public function renderList() {
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        return parent::renderList();
    }
    public static function GetProductS()
    {
        $context = Context::getContext();
        $id_lang = (int)$context->language->id;
        $sql = 'SELECT p.`id_product`, pl.`name`
                FROM `'._DB_PREFIX_.'product` p
                '.Shop::addSqlAssociation('product', 'p').'
                LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
                WHERE pl.`id_lang` = '.(int)$id_lang.' ORDER BY pl.`name`';
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
    }
    public static function GetFieldValues()
    {
        $results = array();
        $id_lang = (int)Context::getContext()->language->id;
        $results[0]['id'] = 'all';
        $results[0]['name'] = 'All Products';
        $i = 1;
        $products = self::GetProductS();
        if(isset($products) && !empty($products))
            foreach($products as $product){
                $results[$i]['id'] = 'product_'.$product['id_product'];
                $results[$i]['name'] = 'Product : '.$product['name'];
                $i++;
            }
        $categories = Category::getCategories($id_lang,true,false);;
        if(isset($categories) && !empty($categories))
            foreach($categories as $category){
                $results[$i]['id'] = 'category_'.$category['id_category'];
                $results[$i]['name'] = 'Category : '.$category['name'];
                $i++;
            }
        return $results;
    }
    public function ajaxProcessUploadImages()
    {
        $type_name = Tools::getValue('type_name');
        if(isset($_FILES[$type_name]))
        {
            $rand_time = Tools::getValue('rand_time');
            $type_mod_name = Tools::getValue('type_mod_name');
            $image_path = _PS_MODULE_DIR_.$type_mod_name.'/images/'.$rand_time.'_img.jpg';
            ImageManager::resize($_FILES[$type_name]['tmp_name'],$image_path);
        }
        die(1);
    }
    public function ProcessImages()
    {

    }
}