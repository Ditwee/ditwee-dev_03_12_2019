<?php
$xprt_prdtabs = array(
    array(
    	'lang' => array(
		    'title' => 'Video',
		    'content' => '<div style="max-width: 560px;">
<p class="embed-responsive embed-responsive-16by9"><iframe width="560" height="315" class="embed-responsive-item" src="https://www.youtube.com/embed/cPLzuVFOEUs" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
</div>',
		),
	    'notlang' => array(
		    'selection' => 'all',
		    'active' => 1,
		    'position' => 1,
		),
    ),
    array(
    	'lang' => array(
		    'title' => 'Shipping and Payment',
		    'content' => '<div class="row col-row">
<div class="col-sm-6 col-single">
<div class="col-image"></div>
<h3 class=" text-left">PAYMENT METHODS</h3>
<div class="col-text text-left">
<ul>
<li><strong>PayPal</strong>: secure and fast payment via PayPal</li>
<li><strong>Credit Cards</strong>: We accept VISA, MasterCard, AMERICAN EXPRESS*, JCB and Diner\'s Club International<br />(<a title="" verified="" by="" visa="" class=" internal-link" href="/en/shop-information/shipping-fees-payment/verified-by-visa/" target="_self">"Verified by VISA</a>" and <a title="" mastercard="" securecode="" class=" internal-link" href="/en/shop-information/shipping-fees-payment/mastercard-securecode/" target="_self">"MasterCard SecureCode"</a> are supported by NI)</li>
<li><strong>Debit Cards</strong>: VISA debit cards, Carte Bleue (F), Carte Bancaire (F), Mister Cash (B), postepay (IT)</li>
<li>Payment in advance by bank transfer (Austria, Belgium, Cyprus, Estonie, France, Finland, Germany, Greece, Ireland, Italy, Latvia, Luxembourg, Malta, Netherlands, Portugal, Slovak Republic, Slovenia, Spain, <strong>not applicable to downloads</strong>)</li>
<li><strong>Direct Bank Transfer Services</strong>: SOFORT Banking (Germany, Austria, France, Belgium), iDEAL (NL), POLi (Australia)</li>
</ul>
<div class="image-container"><img src="https://www.native-instruments.com/typo3temp/pics/img-ce-150609_shop_info_payment_en-dbb9e8427380f1fa7480b4ee558e8357-d.jpg" class="img-responsive" width="293" height="169" /></div>
<br /> <br /> *<em>Payment by American Express currently not available in Japan and Australia.</em></div>
</div>
<div class="col-sm-6 col-single">
<div class="col-image"></div>
<h3 class=" text-left">SHIPPING</h3>
<div class="col-text text-left"><strong>Free shipping via standard delivery in the USA, Canada, Europe, Japan, and in many other countries.</strong> See below for more info.<br /> <br /> Shipping costs apply in <a title="these countries." class=" internal-link" href="/en/shop-information/shipping-fees-payment/countries-with-no-free-shipping/" target="_self">these countries.</a> <br /> Express delivery available in many countries/regions – additional costs apply.</div>
<div class="image-container"><img src="https://www.native-instruments.com/typo3temp/pics/img-ce-shop-information_payment-shipping_free-shipping_en_2x-7e63162696727f8cb5ce6d8e56295a6a-d.jpg" class="img-responsive m_top_20" width="201" height="116" /></div>
</div>
</div>',
		),
	    'notlang' => array(
		    'selection' => 'all',
		    'active' => 1,
		    'position' => 1,
		),
    ),
);