<?php

// start tab general
$this->fields_form[0]['form'] = array(
	'tinymce' => true,
    'legend' => array(
        'title' => $this->l('General Setting'),
    ),
    'input' => array(
        array(
            'type' => 'select',
            'label' => $this->l('Home page title style'),
            'name' => 'home_title_style',
            'default_val' => 'classic_left',
            'data_by_demo' => array(
            	'demo_2' => 'classic_left',
            	'demo_12' => 'classic_left',
            	'demo_1' => 'classic_left',
            	'demo_3' => 'classic_left',
            	'demo_4' => 'featured_center',
            	'demo_5' => 'featured_center',
            	'demo_6' => 'classic_left',
            	'demo_7' => 'classic_left',
            	'demo_8' => 'featured_center',
            	'demo_9' => 'featured_center',
            	'demo_10' => 'classic_left',
            ),
            'desc' => $this->l('Choose Page Title style for home'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'classic_left',
            			'name' => 'Classic style left'
            			),
            		array(
            			'id' => 'featured_center',
            			'name' => 'Featured style Center'
            			),
            		)
            	)
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Sticky Menu'),
            'desc' => $this->l('You Can Enable or Disable Sticky Menu'),
            'name' => 'sticky_menu',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 1,
            	'demo_6' => 1,
            	'demo_7' => 1,
            	'demo_8' => 1,
            	'demo_9' => 1,
            	'demo_10' => 1,
            ),
            'values' => array(
              array(
                'id' => 'sticky_menu',
                'value' => 1,
                'label' => $this->l('Enabled')
                ),
              array(
                'id' => 'sticky_menu',
                'value' => 0,
                'label' => $this->l('Disabled')
                )
            )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Page heading style'),
            'name' => 'page_heading_style',
            'default_val' => 'page_heaing_bg',
            'data_by_demo' => array(
            	'demo_2' => 'page_heaing_bg',
            	'demo_12' => 'page_heaing_bg',
            	'demo_1' => 'page_heaing_bg',
            	'demo_3' => 'page_heaing_bg',
            	'demo_4' => 'page_heaing_simple',
            	'demo_5' => 'page_heaing_bg',
            	'demo_6' => 'page_heaing_bg',
            	'demo_7' => 'page_heaing_simple',
            	'demo_8' => 'page_heaing_bg',
            	'demo_9' => 'page_heaing_simple',
            	'demo_10' => 'page_heaing_bg',
            ),
            'options' => array(
            	'id' => 'id',
                'name' => 'name',
                'query' => array(
                    array(
                        'id'=>'page_heaing_bg',
                        'name'=>'Page heading area bg',
                    ),
                    array(
                        'id'=>'page_heaing_simple',
                        'name'=>'Page heading area simple',
                    ),
                ),
            ),
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Popup layer cart style'),
            'name' => 'popup_layer_cart_style',
            'default_val' => 'layer_cart_classic',
            'data_by_demo' => array(
            	'demo_2' => 'layer_cart_classic',
            	'demo_12' => 'layer_cart_classic',
            	'demo_1' => 'layer_cart_classic',
            	'demo_3' => 'layer_cart_classic',
            	'demo_4' => 'layer_cart_classic',
            	'demo_5' => 'layer_cart_normal',
            	'demo_6' => 'layer_cart_classic',
            	'demo_7' => 'layer_cart_normal',
            	'demo_8' => 'layer_cart_normal',
            	'demo_9' => 'layer_cart_normal',
            	'demo_10' => 'layer_cart_classic',
            ),
            'options' => array(
            	'id' => 'id',
                'name' => 'name',
                'query' => array(
                    array(
                        'id'=>'layer_cart_normal',
                        'name'=>'Layer cart style normal',
                    ),
                    array(
                        'id'=>'layer_cart_classic',
                        'name'=>'Layer cart style classic',
                    ),
                ),
            ),
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Enable Lazy load'),
            'desc' => $this->l('You Can Enable or Disable Lazy load'),
            'name' => 'lazy_load',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 0,
            	'demo_3' => 0,
            	'demo_4' => 0,
            	'demo_5' => 0,
            	'demo_6' => 0,
            	'demo_7' => 0,
            	'demo_8' => 0,
            	'demo_9' => 0,
            	'demo_10' => 0,
            ),
            'values' => array(
              array(
                'id' => 'lazy_load_on',
                'value' => 1,
                'label' => $this->l('Enabled')
                ),
              array(
                'id' => 'lazy_load_off',
                'value' => 0,
                'label' => $this->l('Disabled')
                )
            )
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Home page product tab'),
            'name' => 'infoheading1',
            'sublabel' => 'Select Home page product tab several options in carosel mode',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'text',
            'label' => $this->l('Home page product tab Sub Heading'),
            'desc' => $this->l('Enter sub heading for home page product tab.'),
            'name' => 'home_product_tab_sub_heading',
            'default_val' => '',
            'data_by_demo' => array(
            	'demo_2' => '',
            	'demo_12' => '',
            	'demo_1' => 'Updated daily with latest price',
            	'demo_3' => '',
            	'demo_4' => 'Updated daily with latest price',
            	'demo_5' => '',
            	'demo_6' => '',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '',
            	'demo_10' => '',
            ),
        ),
        array(
            'type' => 'text',
            'label' => $this->l('Home page product tab section margin'),
            'desc' => $this->l('Enter margin for home page product tab section. (default 0px 0px 30px 0px)'),
            'name' => 'home_product_tab_margin',
            'default_val' => '0px 0px 60px 0px',
            'data_by_demo' => array(
            	'demo_2' => '0px 0px 60px 0px',
            	'demo_12' => '0px 0px 60px 0px',
            	'demo_1' => '0px 0px 30px 0px',
            	'demo_3' => '0px 0px 30px 0px',
            	'demo_4' => '0px 0px 30px 0px',
            	'demo_5' => '0px 0px 60px 0px',
            	'demo_6' => '0px 0px 60px 0px',
            	'demo_7' => '0px 0px 60px 0px',
            	'demo_8' => '0px 0px 60px 0px',
            	'demo_9' => '0px 0px 60px 0px',
            	'demo_10' => '0px 0px 60px 0px',
            ),
            'size' => 10,
        ),
        array(
            'type' => 'select',
            'label' => $this->l('How Many Product Display Per Line in home page product tab '),
            'name' => 'per_line_hometab',
            'default_val' => '4',
            'data_by_demo' => array(
            	'demo_2' => '4',
            	'demo_12' => '4',
            	'demo_1' => '4',
            	'demo_3' => '4',
            	'demo_4' => '4',
            	'demo_5' => '4',
            	'demo_6' => '4',
            	'demo_7' => '4',
            	'demo_8' => '4',
            	'demo_9' => '4',
            	'demo_10' => '4',
            ),
            'desc' => $this->l('Choose How Many Product Display Per Line Grid View home page product tab (N.B:5,7,8 Work Only Carosel View) '),
            'options' => array(
                'id' => 'id',
                'name' => 'name',
                'query' => array(
                    array(
                        'id' => '2',
                        'name' => '2 Products Per Line'
                        ),
                    array(
                        'id' => '3',
                        'name' => '3 Products Per Line'
                        ),
                    array(
                        'id' => '4',
                        'name' => '4 Products Per Line'
                        ),
                    array(
                        'id' => '5',
                        'name' => '5 Products Per Line'
                        ),
                    array(
                        'id' => '6',
                        'name' => '6 Products Per Line'
                        ),
                    )
                )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Home page product tab rows'),
            'name' => 'per_rows_hometab',
            'default_val' => '1',
            'data_by_demo' => array(
            	'demo_2' => '1',
            	'demo_12' => '1',
            	'demo_1' => '2',
            	'demo_3' => '1',
            	'demo_4' => '1',
            	'demo_5' => '1',
            	'demo_6' => '1',
            	'demo_7' => '1',
            	'demo_8' => '1',
            	'demo_9' => '1',
            	'demo_10' => '2',
            ),
            'desc' => $this->l('Choose How Many rows for product tab'),
            'options' => array(
                'id' => 'id',
                'name' => 'name',
                'query' => array(
                    array(
                        'id' => '1',
                        'name' => 'One rows'
                        ),
                    array(
                        'id' => '2',
                        'name' => 'Two rows'
                        ),
                    array(
                        'id' => '3',
                        'name' => 'Three rows'
                        ),
                    )
                )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Home tab Products Scroll '),
            'name' => 'hometab_product_scroll',
            'default_val' => 'per_item',
            'data_by_demo' => array(
            	'demo_2' => 'per_item',
            	'demo_12' => 'per_item',
            	'demo_1' => 'per_item',
            	'demo_3' => 'per_item',
            	'demo_4' => 'per_item',
            	'demo_5' => 'per_item',
            	'demo_6' => 'per_item',
            	'demo_7' => 'per_item',
            	'demo_8' => 'per_item',
            	'demo_9' => 'per_item',
            	'demo_10' => 'per_item',
            ),
            'options' => array(
                'query' => array(
                	array(
                	    'id'=>'per_item',
                	    'name'=>'Per Item',
                	),
                    array(
                        'id'=>'per_page',
                        'name'=>'Per Page',
                    ),
                ),
                'id' => 'id',
                'name' => 'name'
            ),
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Home tab carousel autoplay'),
            'desc' => $this->l('You Can Enable or Disable Home tab carousel autoplay'),
            'name' => 'hometab_autoplay',
            'is_bool' => true,
            'default_val' => 0,
            'data_by_demo' => array(
            	'demo_2' => 0,
            	'demo_12' => 0,
            	'demo_1' => 0,
            	'demo_3' => 0,
            	'demo_4' => 0,
            	'demo_5' => 0,
            	'demo_6' => 0,
            	'demo_7' => 0,
            	'demo_8' => 0,
            	'demo_9' => 0,
            	'demo_10' => 0,
            ),
            'values' => array(
              array(
                'id' => 'hometab_autoplay_on',
                'value' => 1,
                'label' => $this->l('Yes')
                ),
              array(
                'id' => 'hometab_autoplay_off',
                'value' => 0,
                'label' => $this->l('No')
                )
            )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Home tab carousel navigation arrows'),
            'desc' => $this->l('You Can Enable or Disable Home tab carousel navigation arrows'),
            'name' => 'hometab_nav_arrows',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 1,
            	'demo_6' => 1,
            	'demo_7' => 1,
            	'demo_8' => 1,
            	'demo_9' => 1,
            	'demo_10' => 1,
            ),
            'values' => array(
              array(
                'id' => 'hometab_nav_arrows_on',
                'value' => 1,
                'label' => $this->l('Yes')
                ),
              array(
                'id' => 'hometab_nav_arrows_off',
                'value' => 0,
                'label' => $this->l('No')
                )
            )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Home tab carousel navigation dots'),
            'desc' => $this->l('You Can Enable or Disable Home tab carousel navigation dots'),
            'name' => 'hometab_nav_dots',
            'is_bool' => true,
            'default_val' => 0,
            'data_by_demo' => array(
            	'demo_2' => 0,
            	'demo_12' => 0,
            	'demo_1' => 0,
            	'demo_3' => 0,
            	'demo_4' => 0,
            	'demo_5' => 0,
            	'demo_6' => 0,
            	'demo_7' => 0,
            	'demo_8' => 0,
            	'demo_9' => 0,
            	'demo_10' => 0,
            ),
            'values' => array(
              array(
                'id' => 'hometab_nav_dots_on',
                'value' => 1,
                'label' => $this->l('Yes')
                ),
              array(
                'id' => 'hometab_nav_dots_off',
                'value' => 0,
                'label' => $this->l('No')
                )
            )
        ),
        // Home tab end
        array(
            'type' => 'infoheading',
            'label' => $this->l('Display Home top left and right column size'),
            'name' => 'infoheading',
            'colorclass' => 'info_custom success',
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Display home top left/right column size'),
            'name' => 'hometopleftright_col',
            'default_val' => 6,
            'data_by_demo' => array(
            	'demo_2' => '6',
            	'demo_12' => '3',
            	'demo_1' => '6',
            	'demo_3' => '6',
            	'demo_4' => '6',
            	'demo_5' => '6',
            	'demo_6' => '6',
            	'demo_7' => '6',
            	'demo_8' => '6',
            	'demo_9' => '6',
            	'demo_10' => '6',
            ),
            'desc' => $this->l('Select display home top left/right column size'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => '6',
            			'name' => 'column-6 and column-6'
            			),
            		array(
            			'id' => '4',
            			'name' => 'column-4 and column-8'
            			),
            		array(
            			'id' => '8',
            			'name' => 'column-8 and column-4'
            			),
            		array(
            			'id' => '3',
            			'name' => 'column-3 and column-9'
            			),
            		array(
            			'id' => '9',
            			'name' => 'column-9 and column-3'
            			),
            		array(
            			'id' => '5',
            			'name' => 'column-5 and column-7'
            			),
            		array(
            			'id' => '7',
            			'name' => 'column-7 and column-5'
            			),
            		)
            	)
        ),
		array(
		    'type' => 'infoheading',
		    'label' => $this->l('Display Home bottom left and right column size'),
		    'name' => 'infoheading',
		    'colorclass' => 'info_custom success',
		),
		array(
            'type' => 'select',
            'label' => $this->l('Display home bottom left and right column size'),
            'name' => 'homebottomleftright_col',
            'default_val' => '9',
            'data_by_demo' => array(
            	'demo_2' => '9',
            	'demo_12' => '9',
            	'demo_1' => '9',
            	'demo_3' => '9',
            	'demo_4' => '9',
            	'demo_5' => '9',
            	'demo_6' => '9',
            	'demo_7' => '6',
            	'demo_8' => '9',
            	'demo_9' => '9',
            	'demo_10' => '9',
            ),
            'desc' => $this->l('Select display home bottom left and right column size'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => '6',
            			'name' => 'column-6 and column-6'
            			),
            		array(
            			'id' => '4',
            			'name' => 'column-4 and column-8'
            			),
            		array(
            			'id' => '8',
            			'name' => 'column-8 and column-4'
            			),
            		array(
            			'id' => '3',
            			'name' => 'column-3 and column-9'
            			),
            		array(
            			'id' => '9',
            			'name' => 'column-9 and column-3'
            			),
            		array(
            			'id' => '5',
            			'name' => 'column-5 and column-7'
            			),
            		array(
            			'id' => '7',
            			'name' => 'column-7 and column-5'
            			),
            		)
            	)
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);
// end tab general

// Header area
$this->fields_form[13]['form'] = array(
	'tinymce' => true,
  	'legend' => array(
  	'title' => $this->l('Header area'),
    ),
    'input' => array(
        array(
            'type' => 'infoheading',
            'label' => $this->l('Display Header top left and right column size'),
            'name' => 'infoheading',
            'colorclass' => 'info_custom success',
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Header style'),
            'name' => 'page_header_style',
            'default_val' => 'header_general',
            'data_by_demo' => array(
            	'demo_2' => 'header_general',
            	'demo_12' => 'header_general',
            	'demo_1' => 'header_classic',
            	'demo_3' => 'header_classic',
            	'demo_4' => 'header_general',
            	'demo_5' => 'header_classic',
            	'demo_6' => 'header_nav_classic',
            	'demo_7' => 'header_transparent',
            	'demo_8' => 'header_general',
            	'demo_9' => 'header_general',
            	'demo_10' => 'header_classic',
            ),
            'desc' => $this->l('Choose Home page several header style'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'header_general',
            			'name' => 'Header general style'
            			),
            		array(
            			'id' => 'header_classic',
            			'name' => 'Header classic style'
            			),
            		array(
            			'id' => 'header_nav_classic',
            			'name' => 'Header nav classic style'
            			),
            		array(
            			'id' => 'header_transparent',
            			'name' => 'Header transparent style'
            			),
            		array(
            			'id' => 'header_creative',
            			'name' => 'Header creative style'
            			),
            		array(
            			'id' => 'header_featured',
            			'name' => 'Header featured style'
            			),
            		)
            	)
        ),
    	array(
            'type' => 'upload_img',
            'label' => $this->l('Header background for inner page'),
            'desc' => $this->l('Select background image for inner page transparent style'),
            'name' => 'header_top_bg_inner',
            'default_val' => '',
            'data_by_demo' => array(
            	'demo_2' => '',
            	'demo_12' => '',
            	'demo_1' => '',
            	'demo_3' => '',
            	'demo_4' => '',
            	'demo_5' => '',
            	'demo_6' => '',
            	'demo_7' => 'headerbg.jpg',
            	'demo_8' => '',
            	'demo_9' => '',
            	'demo_10' => '',
            ),
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Display Header top left/right column size'),
            'name' => 'header_top_left_col',
            'default_val' => '4',
            'data_by_demo' => array(
            	'demo_2' => '4',
            	'demo_12' => '4',
            	'demo_1' => '9',
            	'demo_3' => '9',
            	'demo_4' => '4',
            	'demo_5' => '9',
            	'demo_6' => '4',
            	'demo_7' => '4',
            	'demo_8' => '4',
            	'demo_9' => '4',
            	'demo_10' => '9',
            ),
            'desc' => $this->l('Select display Header top left/right column size'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => '4',
            			'name' => 'column-4 and column-8'
            			),
            		array(
            			'id' => '3',
            			'name' => 'column-3 and column-9'
            			),
            		array(
            			'id' => '6',
            			'name' => 'column-6 and column-6'
            			),
            		array(
            			'id' => '8',
            			'name' => 'column-8 and column-4'
            			),
            		array(
            			'id' => '9',
            			'name' => 'column-9 and column-3'
            			),
            		)
            	)
        ),
		array(
            'type' => 'select',
            'label' => $this->l('Display header Top Right Inner column size'),
            'name' => 'header_top_right_inner_col',
            'default_val' => '6',
            'data_by_demo' => array(
            	'demo_2' => '6',
            	'demo_12' => '6',
            	'demo_1' => '6',
            	'demo_3' => '6',
            	'demo_4' => '6',
            	'demo_5' => '6',
            	'demo_6' => '6',
            	'demo_7' => '6',
            	'demo_8' => '6',
            	'demo_9' => '6',
            	'demo_10' => '6',
            ),
            'desc' => $this->l('Select display header Top Right Inner one and two column size'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => '6',
            			'name' => 'column-6 and column-6'
            			),
            		array(
            			'id' => '8',
            			'name' => 'column-8 and column-4'
            			),
            		array(
            			'id' => '4',
            			'name' => 'column-4 and column-8'
            			),
            		array(
            			'id' => '9',
            			'name' => 'column-9 and column-3'
            			),
            		array(
            			'id' => '5',
            			'name' => 'column-5 and column-7'
            			),
            		array(
            			'id' => '7',
            			'name' => 'column-7 and column-5'
            			),
            		)
            	)
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Header sidebar panel style enable/disable'),
            'name' => 'infoheading',
            'colorclass' => 'info_custom success',
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Sidebar panel display style'),
            'name' => 'sidebar_display_style',
            'default_val' => 'sidebar_push',
            'data_by_demo' => array(
            	'demo_2' => 'sidebar_push',
            	'demo_12' => 'sidebar_push',
            	'demo_1' => 'sidebar_push',
            	'demo_3' => 'sidebar_push',
            	'demo_4' => 'sidebar_push',
            	'demo_5' => 'sidebar_push',
            	'demo_6' => 'sidebar_push',
            	'demo_7' => 'sidebar_push',
            	'demo_8' => 'sidebar_overplay',
            	'demo_9' => 'sidebar_push',
            	'demo_10' => 'sidebar_push',
            ),
            'desc' => $this->l('Select sidebar panel display style'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'sidebar_push',
            			'name' => 'Sidebar panel display style push'
            			),
            		array(
            			'id' => 'sidebar_overplay',
            			'name' => 'Sidebar panel display style overplay'
            			),
            		)
            	)
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Header top nav icon content style'),
            'name' => 'header_top_nav_content',
            'default_val' => 'header_nav_sidebarpanel',
            'data_by_demo' => array(
            	'demo_2' => 'header_nav_sidebarpanel',
            	'demo_12' => 'header_nav_sidebarpanel',
            	'demo_1' => 'header_nav_regular',
            	'demo_3' => 'header_nav_regular',
            	'demo_4' => 'header_nav_regular',
            	'demo_5' => 'header_nav_regular',
            	'demo_6' => 'header_nav_regular',
            	'demo_7' => 'header_nav_regular',
            	'demo_8' => 'header_nav_sidebarpanel',
            	'demo_9' => 'header_nav_regular',
            	'demo_10' => 'header_nav_regular',
            ),
            'desc' => $this->l('Select header nav icon content expand style'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'header_nav_regular',
            			'name' => 'Header nav icon expand Regular style'
            			),
            		array(
            			'id' => 'header_nav_sidebarpanel',
            			'name' => 'Header nav icon expand sidebar panel'
            			),
            		)
            	)
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Header cart icon content style'),
            'name' => 'header_top_cart_content',
            'default_val' => 'header_cart_sidebarpanel',
            'data_by_demo' => array(
            	'demo_2' => 'header_cart_sidebarpanel',
            	'demo_12' => 'header_cart_sidebarpanel',
            	'demo_1' => 'header_cart_click',
            	'demo_3' => 'header_cart_hover',
            	'demo_4' => 'header_cart_hover',
            	'demo_5' => 'header_cart_hover',
            	'demo_6' => 'header_cart_hover',
            	'demo_7' => 'header_cart_hover',
            	'demo_8' => 'header_cart_sidebarpanel',
            	'demo_9' => 'header_cart_hover',
            	'demo_10' => 'header_cart_click',
            ),
            'desc' => $this->l('Select header cart icon content expand stle'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'header_cart_hover',
            			'name' => 'Header cart icon expand hover regular style'
            			),
            		array(
            			'id' => 'header_cart_click',
            			'name' => 'Header cart icon expand click style'
            			),
            		array(
            			'id' => 'header_cart_sidebarpanel',
            			'name' => 'Header cart icon expand sidebar panel'
            			),
            		)
            	)
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Header icon change'),
            'name' => 'infoheading',
            'colorclass' => 'info_custom success',
        ),
        array(
			'type' => 'xprticon_type',
			'label' => $this->l('Header Nav Icon'),
			'name' => 'header_nav_icon',
			'default_val' => 'icon-reorder',
            'data_by_demo' => array(
            	'demo_2' => 'icon-reorder',
            	'demo_12' => 'icon-reorder',
            	'demo_1' => 'icon-reorder',
            	'demo_3' => 'icon-bars',
            	'demo_4' => 'icon-bars',
            	'demo_5' => 'icon-bars',
            	'demo_6' => 'icon-bars',
            	'demo_7' => 'icon-bars',
            	'demo_8' => 'icon-bars',
            	'demo_9' => 'icon-bars',
            	'demo_10' => 'icon-reorder',
            ),
			'desc' => $this->l('Default icon: (icon-bars)'),
		),
        array(
			'type' => 'xprticon_type',
			'label' => $this->l('Header Search Icon'),
			'name' => 'header_search_icon',
			'default_val' => 'icon_search',
            'data_by_demo' => array(
            	'demo_2' => 'icon_search',
            	'demo_12' => 'icon_search',
            	'demo_1' => 'icon_search',
            	'demo_3' => 'icon_search',
            	'demo_4' => 'icon_search',
            	'demo_5' => 'icon_search',
            	'demo_6' => 'icon_search',
            	'demo_7' => 'icon_search',
            	'demo_8' => 'icon_search',
            	'demo_9' => 'icon_search',
            	'demo_10' => 'icon_search',
            ),
			'desc' => $this->l('Default icon: (icon_search)'),
		),
        array(
			'type' => 'xprticon_type',
			'label' => $this->l('Header cart Icon'),
			'name' => 'header_cart_icon',
			'default_val' => 'icon-ecommerce-bag',
            'data_by_demo' => array(
            	'demo_2' => 'icon-ecommerce-bag',
            	'demo_12' => 'icon-ecommerce-bag',
            	'demo_1' => 'icon-ecommerce-bag',
            	'demo_3' => 'icon-ecommerce-bag',
            	'demo_4' => 'icon-ecommerce-bag',
            	'demo_5' => 'icon-ecommerce-bag',
            	'demo_6' => 'icon-ecommerce-bag',
            	'demo_7' => 'icon-ecommerce-bag',
            	'demo_8' => 'icon-ecommerce-bag',
            	'demo_9' => 'icon-ecommerce-bag',
            	'demo_10' => 'icon-ecommerce-bag',
            ),
			'desc' => $this->l('Default icon: (icon-ecommerce-bag)'),
		),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);



// start tab background
$this->fields_form[1]['form'] = array(
	'tinymce' => true,
    'legend' => array(
        'title' => $this->l('Page body style'),
    ),
    'input' => array(
    	array(
    	    'type' => 'select',
    	    'label' => $this->l('Select page style'),
    	    'name' => 'body_page_style',
            'default_val' => 'full_width',
            'data_by_demo' => array(
            	'demo_2' => 'full_width',
            	'demo_12' => 'full_width',
            	'demo_1' => 'full_width',
            	'demo_3' => 'full_width',
            	'demo_4' => 'full_width',
            	'demo_5' => 'full_width',
            	'demo_6' => 'full_width',
            	'demo_7' => 'full_width',
            	'demo_8' => 'boxed_classic',
            	'demo_9' => 'full_width',
            	'demo_10' => 'full_width',
            ),
    	    'desc' => $this->l('Select page style box shadow/full width. (Works with Header Simple style Black)'),
    	    'options' => array(
    	    	'id' => 'id',
    	    	'name' => 'name',
    	    	'query' => array(
    	    		array(
    	    			'id' => 'full_width',
    	    			'name' => 'Full width'
    	    			),
    	    		array(
    	    			'id' => 'boxed',
    	    			'name' => 'Boxed'
    	    			),
    	    		array(
    	    			'id' => 'boxed_classic',
    	    			'name' => 'Boxed classic'
    	    			),
    	    		)
    	    	)
    	),
    	array(
    	    'type' => 'select',
    	    'label' => $this->l('Page container width'),
    	    'name' => 'body_boxed_width',
            'default_val' => '1170',
            'data_by_demo' => array(
            	'demo_2' => '1170',
            	'demo_12' => '1170',
            	'demo_1' => '1170',
            	'demo_3' => '1170',
            	'demo_4' => '1170',
            	'demo_5' => '1170',
            	'demo_6' => '1170',
            	'demo_7' => '1170',
            	'demo_8' => '1170',
            	'demo_9' => '1170',
            	'demo_10' => '1170',
            ),
    	    'desc' => $this->l('Select page container width. (Works with Header Simple style Black)'),
    	    'options' => array(
    	    	'id' => 'id',
    	    	'name' => 'name',
    	    	'query' => array(
    	    		array(
    	    			'id' => '1170',
    	    			'name' => '1170px'
    	    			),
    	    		array(
    	    			'id' => '1440',
    	    			'name' => '1440px'
    	    			),
    	    		array(
    	    			'id' => '970',
    	    			'name' => '970px'
    	    			),
    	    		)
    	    	)
    	),
    	array(
    	    'type' => 'text',
    	    'label' => $this->l('Body font size'),
    	    'desc' => $this->l('Select body font size. (default 13)'),
    	    'name' => 'body_f_size',
    	    'default_val' => '13',
            'data_by_demo' => array(
            	'demo_2' => '13',
            	'demo_12' => '13',
            	'demo_1' => '13',
            	'demo_3' => '13',
            	'demo_4' => '13',
            	'demo_5' => '13',
            	'demo_6' => '13',
            	'demo_7' => '13',
            	'demo_8' => '13',
            	'demo_9' => '13',
            	'demo_10' => '13',
            ),
    	    'size' => 10,
    	    'required' => true
    	),
    	array(
    	    'type' => 'color',
    	    'label' => $this->l('Body background color'),
    	    'desc' => $this->l('Select body background color. (#fff)'),
    	    'name' => 'body_bg_color',
    	    'default_val' => '#FFFFFF',
            'data_by_demo' => array(
            	'demo_2' => '#FFFFFF',
            	'demo_12' => '#FFFFFF',
            	'demo_1' => '#FFFFFF',
            	'demo_3' => '#FFFFFF',
            	'demo_4' => '#FFFFFF',
            	'demo_5' => '#FFFFFF',
            	'demo_6' => '#FFFFFF',
            	'demo_7' => '#FFFFFF',
            	'demo_8' => '#F5F5F5',
            	'demo_9' => '#FFFFFF',
            	'demo_10' => '#FFFFFF',
            ),
    	    'predefine' => array(
            	),
    	),
    	array(
    	    'type' => 'color',
    	    'label' => $this->l('Page boxed background color'),
    	    'desc' => $this->l('Default value #fff'),
    	    'name' => 'page_boxed_bg',
            'default_val' => '#FFFFFF',
            'data_by_demo' => array(
            	'demo_2' => '#FFFFFF',
            	'demo_12' => '#FFFFFF',
            	'demo_1' => '#FFFFFF',
            	'demo_3' => '#FFFFFF',
            	'demo_4' => '#FFFFFF',
            	'demo_5' => '#FFFFFF',
            	'demo_6' => '#FFFFFF',
            	'demo_7' => '#FFFFFF',
            	'demo_8' => '#fff',
            	'demo_9' => '#FFFFFF',
            	'demo_10' => '#FFFFFF',
            ),
            'predefine' => array(
                //'red' => '#FFFFFF',
                //'dark' => '#212121',
                //'green' => '#84BB45',
                //'black' => '#FFFFFF',
            	),
    	),
    	array(
    	    'type' => 'color',
    	    'label' => $this->l('Body font color'),
    	    'desc' => $this->l('Select body font color. (#8f8f8f)'),
    	    'name' => 'body_f_color',
    	    'default_val' => '#8f8f8f',
            'data_by_demo' => array(
            	'demo_2' => '#8f8f8f',
            	'demo_12' => '#8f8f8f',
            	'demo_1' => '#666666',
            	'demo_3' => '#8f8f8f',
            	'demo_4' => '#666666',
            	'demo_5' => '#8f8f8f',
            	'demo_6' => '#8f8f8f',
            	'demo_7' => '#666666',
            	'demo_8' => '#8f8f8f',
            	'demo_9' => '#8f8f8f',
            	'demo_10' => '#666666',
            ),
    	    'predefine' => array(
                //'red' => '#666666',
                //'dark' => '#9E9696',
                //'green' => '#4A4848',
                ////'black' => '#666666',
            	),
    	),
        array(
            'type' => 'select',
            'label' => $this->l('Enable Background image'),
            'name' => 'enable_body_bg',
            'default_val' => 'none',
            'data_by_demo' => array(
            	'demo_2' => 'none',
            	'demo_12' => 'none',
            	'demo_1' => 'none',
            	'demo_3' => 'none',
            	'demo_4' => 'none',
            	'demo_5' => 'none',
            	'demo_6' => 'none',
            	'demo_7' => 'none',
            	'demo_8' => 'none',
            	'demo_9' => 'none',
            	'demo_10' => 'none',
            ),
            'desc' => $this->l('Enable background image or use background pattern'),
            'options' => array(
                'id' => 'id',
                'name' => 'name',
                'query' => array(
                    array(
                        'id' => '1',
                        'name' => 'BackGround'
                        ),
                    array(
                        'id' => '0',
                        'name' => 'Pattern'
                        ),
                    array(
                        'id' => 'none',
                        'name' => 'None'
                        ),
                    )
                )
        ),
    	array(
    	    'type' => 'upload_img',
    	    'label' => $this->l('Select Background Image'),
    	    'desc' => $this->l('Select body background Image'),
    	    'name' => 'body_bg_image',
    	    'default_val' => '',
            'data_by_demo' => array(
            	'demo_2' => '',
            	'demo_12' => '',
            	'demo_1' => '',
            	'demo_3' => '',
            	'demo_4' => '',
            	'demo_5' => '',
            	'demo_6' => '',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '',
            	'demo_10' => '',
            ),
    	),
    	array(
    	    'type' => 'bg_pattern',
    	    'label' => $this->l('Select Background Pattern'),
    	    'desc' => $this->l('Select body background pattern'),
    	    'name' => 'body_bg_pattern',
    	    'default_val' => '',
            'data_by_demo' => array(
            	'demo_2' => '',
            	'demo_12' => '',
            	'demo_1' => '',
            	'demo_3' => '',
            	'demo_4' => '',
            	'demo_5' => '',
            	'demo_6' => '',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '',
            	'demo_10' => '',
            ),
    	),
    	array(
    	    'type' => 'select',
    	    'label' => $this->l('Body background repeat'),
    	    'name' => 'body_bg_repeat',
            'default_val' => 'repeat',
            'data_by_demo' => array(
            	'demo_2' => 'repeat',
            	'demo_12' => 'repeat',
            	'demo_1' => 'repeat',
            	'demo_3' => 'repeat',
            	'demo_4' => 'repeat',
            	'demo_5' => 'repeat',
            	'demo_6' => 'repeat',
            	'demo_7' => 'repeat',
            	'demo_8' => 'repeat',
            	'demo_9' => 'repeat',
            	'demo_10' => 'repeat',
            ),
    	    'desc' => $this->l('Select body background repeat/no-repeat'),
    	    'options' => array(
    	    	'id' => 'id',
    	    	'name' => 'name',
    	    	'query' => array(
    	    		array(
    	    			'id' => 'repeat',
    	    			'name' => 'Repeat'
    	    			),
    	    		array(
    	    			'id' => 'no-repeat',
    	    			'name' => 'No repeat'
    	    			),
    	    		array(
    	    			'id' => 'repeat-x',
    	    			'name' => 'Repeat-X'
    	    			),
    	    		array(
    	    			'id' => 'repeat-y',
    	    			'name' => 'Repeat-Y'
    	    			),
    	    		)
    	    	)
    	),
        array(
    	    'type' => 'select',
    	    'label' => $this->l('Body background position'),
    	    'name' => 'body_bg_position',
            'default_val' => 'left top',
            'data_by_demo' => array(
            	'demo_2' => 'left top',
            	'demo_12' => 'left top',
            	'demo_1' => 'left top',
            	'demo_3' => 'left top',
            	'demo_4' => 'left top',
            	'demo_5' => 'left top',
            	'demo_6' => 'left top',
            	'demo_7' => 'left top',
            	'demo_8' => 'left top',
            	'demo_9' => 'left top',
            	'demo_10' => 'left top',
            ),
    	    'desc' => $this->l('Select body background position'),
    	    'options' => array(
    	    	'id' => 'id',
    	    	'name' => 'name',
    	    	'query' => array(
    	    		array(
    	    			'id' => 'left top',
    	    			'name' => 'left top'
    	    			),
    	    		array(
    	    			'id' => 'left center',
    	    			'name' => 'left center'
    	    			),
    	    		array(
    	    			'id' => 'left bottom',
    	    			'name' => 'left bottom'
    	    			),
    	    		array(
    	    			'id' => 'right top',
    	    			'name' => 'right top'
    	    			),
    	    		array(
    	    			'id' => 'right center',
    	    			'name' => 'right center'
    	    			),
    	    		array(
    	    			'id' => 'right bottom',
    	    			'name' => 'right bottom'
    	    			),
    	    		array(
    	    			'id' => 'center top',
    	    			'name' => 'center top'
    	    			),
    	    		array(
    	    			'id' => 'center center',
    	    			'name' => 'center center'
    	    			),
    	    		array(
    	    			'id' => 'center bottom',
    	    			'name' => 'center bottom'
    	    			),
    	    		)
    	    	)
    	),
	    array(
		    'type' => 'select',
		    'label' => $this->l('Body background size'),
		    'name' => 'body_bg_size',
            'default_val' => 'initial',
            'data_by_demo' => array(
            	'demo_2' => 'initial',
            	'demo_12' => 'initial',
            	'demo_1' => 'initial',
            	'demo_3' => 'initial',
            	'demo_4' => 'initial',
            	'demo_5' => 'initial',
            	'demo_6' => 'initial',
            	'demo_7' => 'initial',
            	'demo_8' => 'initial',
            	'demo_9' => 'initial',
            	'demo_10' => 'initial',
            ),
		    'desc' => $this->l('Select body background size'),
		    'options' => array(
		    	'id' => 'id',
		    	'name' => 'name',
		    	'query' => array(
		    		array(
		    			'id' => 'cover',
		    			'name' => 'Cover'
		    			),
		    		array(
		    			'id' => 'contain',
		    			'name' => 'Contain'
		    			),
		    		array(
		    			'id' => 'initial',
		    			'name' => 'initial'
		    			),
		    		array(
		    			'id' => '100% 100%',
		    			'name' => '100% 100%'
		    			),
		    		)
		    	)
		),
	    array(
		    'type' => 'select',
		    'label' => $this->l('Body background attachment'),
		    'name' => 'body_bg_attachment',
            'default_val' => 'scroll',
            'data_by_demo' => array(
            	'demo_2' => 'scroll',
            	'demo_12' => 'scroll',
            	'demo_1' => 'scroll',
            	'demo_3' => 'scroll',
            	'demo_4' => 'scroll',
            	'demo_5' => 'scroll',
            	'demo_6' => 'scroll',
            	'demo_7' => 'scroll',
            	'demo_8' => 'scroll',
            	'demo_9' => 'scroll',
            	'demo_10' => 'scroll',
            ),
		    'desc' => $this->l('Select body background attachment'),
		    'options' => array(
		    	'id' => 'id',
		    	'name' => 'name',
		    	'query' => array(
		    		array(
		    			'id' => 'scroll',
		    			'name' => 'scroll'
		    			),
		    		array(
		    			'id' => 'fixed',
		    			'name' => 'fixed'
		    			),
		    		array(
		    			'id' => 'local',
		    			'name' => 'local'
		    			),
		    		)
		    	)
		),
        
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);
// end tab background 
// start tab fonts
$this->fields_form[2]['form'] = array(
	'tinymce' => true,
    'legend' => array(
        'title' => $this->l('Font settings'),
    ),
    'input' => array(
        array(
            'type' => 'infoheading',
            'label' => $this->l('Select body Google font'),
            'name' => 'infoheading1',
            'sublabel' => 'Select Google font for body',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'googlefont',
            'label' => $this->l('Select Body font'),
            'name' => 'bodyfont',
            'sublabel' => 'Select google for body',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Select Heading Google font'),
            'name' => 'infoheading1',
            'sublabel' => 'Select Google font for page heading',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'googlefont',
            'label' => $this->l('Select page heading font'),
            'name' => 'headingfont',
            'sublabel' => 'Select google for page heading',
            'colorclass' => 'success',
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);
// end tab fonts

// start tab fonts
$this->fields_form[21]['form'] = array(
	'tinymce' => true,
    'legend' => array(
        'title' => $this->l('Main Menu settings'),
    ),
    'input' => array(
        array(
            'type' => 'infoheading',
            'label' => $this->l('Main Menu area'),
            'name' => 'infoheading1',
            'sublabel' => 'Main Menu area setting',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Main menu style'),
            'name' => 'main_menu_style',
            'default_val' => 'menu_default_style',
            'data_by_demo' => array(
            	'demo_2' => 'menu_default_style',
            	'demo_12' => 'menu_default_style',
            	'demo_1' => 'menu_default_style',
            	'demo_3' => 'menu_default_style',
            	'demo_4' => 'menu_default_style',
            	'demo_5' => 'menu_default_style',
            	'demo_6' => 'menu_default_style',
            	'demo_7' => 'menu_default_style',
            	'demo_8' => 'menu_default_style',
            	'demo_9' => 'menu_default_style',
            	'demo_10' => 'menu_default_style',
            ),
            'desc' => $this->l('Choose Menu style'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'menu_default_style',
            			'name' => 'Menu default style'
            			),
            		array(
            			'id' => 'menu_noborder_style',
            			'name' => 'Menu without border style'
            			),
            		)
            	)
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);
// end tab fonts


// Product list/grid style
$this->fields_form[9]['form'] = array(
	'tinymce' => true,
  	'legend' => array(
  	'title' => $this->l('Product Grid/list style'),
    ),
    'input' => array(
        array(
            'type' => 'select',
            'label' => $this->l('Product grid hover image show'),
            'name' => 'prod_grid_img_style',
            'default_val' => 'prod_grid_img_style_hover',
            'data_by_demo' => array(
            	'demo_2' => 'prod_grid_img_style_hover',
            	'demo_12' => 'prod_grid_img_style_hover',
            	'demo_1' => 'prod_grid_img_style_multi',
            	'demo_3' => 'prod_grid_img_style_multi',
            	'demo_4' => 'prod_grid_img_style_multi',
            	'demo_5' => 'prod_grid_img_style_multi',
            	'demo_6' => 'prod_grid_img_style_hover',
            	'demo_7' => 'prod_grid_img_style_hover',
            	'demo_8' => 'prod_grid_img_style_hover',
            	'demo_9' => 'prod_grid_img_style_hover',
            	'demo_10' => 'prod_grid_img_style_multi',
            ),
            'desc' => $this->l('Choose Product grid/list hover image show'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'prod_grid_img_style_hover',
            			'name' => 'Product grid hover image show'
            			),
            		array(
            			'id' => 'prod_grid_img_style_multi',
            			'name' => 'Product grid hover multiple image show'
            			),
            		)
            	)
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Product grid layout style'),
            'name' => 'prod_grid_layout_style',
            'default_val' => 'prod_grid_default',
            'data_by_demo' => array(
            	'demo_2' => 'prod_grid_default',
            	'demo_12' => 'prod_grid_default',
            	'demo_1' => 'prod_grid_regular',
            	'demo_3' => 'prod_grid_creative',
            	'demo_4' => 'prod_grid_extream',
            	'demo_5' => 'prod_grid_classic',
            	'demo_6' => 'prod_grid_default',
            	'demo_7' => 'prod_grid_classic',
            	'demo_8' => 'prod_grid_default',
            	'demo_9' => 'prod_grid_default',
            	'demo_10' => 'prod_grid_regular',
            ),
            'desc' => $this->l('Choose Product grid quickview wishlist style'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'prod_grid_default',
            			'name' => 'Product grid default layout'
            			),
            		array(
            			'id' => 'prod_grid_regular',
            			'name' => 'Product grid regular layout'
            			),
            		array(
            			'id' => 'prod_grid_classic',
            			'name' => 'Product grid classic layout'
            			),
            		array(
            			'id' => 'prod_grid_creative',
            			'name' => 'Product grid creative layout'
            			),
            		array(
            			'id' => 'prod_grid_expand',
            			'name' => 'Product grid expand layout'
            			),
            		array(
            			'id' => 'prod_grid_extream',
            			'name' => 'Product grid extream layout'
            			),
            		array(
            			'id' => 'prod_grid_uno',
            			'name' => 'Product grid expand layout'
            			),
            		)
            	)
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('New Badge'),
            'desc' => $this->l('You Can Enable or Disable New Badge'),
            'name' => 'prd_list_new_badge',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 1,
            	'demo_6' => 1,
            	'demo_7' => 0,
            	'demo_8' => 1,
            	'demo_9' => 1,
            	'demo_10' => 1,
            ),
            'values' => array(
              array(
                'id' => 'prd_list_new_badge',
                'value' => 1,
                'label' => $this->l('Enabled')
                ),
              array(
                'id' => 'prd_list_new_badge',
                'value' => 0,
                'label' => $this->l('Disabled')
                )
            )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Sale Badge'),
            'desc' => $this->l('You Can Enable or Disable Sale Badge'),
            'name' => 'prd_list_sale_badge',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 1,
            	'demo_6' => 1,
            	'demo_7' => 0,
            	'demo_8' => 1,
            	'demo_9' => 1,
            	'demo_10' => 1,
            ),
            'values' => array(
              array(
                'id' => 'prd_list_sale_badge',
                'value' => 1,
                'label' => $this->l('Enabled')
                ),
              array(
                'id' => 'prd_list_sale_badge',
                'value' => 0,
                'label' => $this->l('Disabled')
                )
            )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('New and sale badge style'),
            'name' => 'new_sale_badge_style',
            'default_val' => 'default_style',
            'data_by_demo' => array(
            	'demo_2' => 'default_style',
            	'demo_12' => 'default_style',
            	'demo_1' => 'default_style',
            	'demo_3' => 'classic_style',
            	'demo_4' => 'default_style',
            	'demo_5' => 'default_style',
            	'demo_6' => 'default_style',
            	'demo_7' => 'default_style',
            	'demo_8' => 'default_style',
            	'demo_9' => 'square_style',
            	'demo_10' => 'default_style',
            ),
            'desc' => $this->l('Choose Product grid quickview wishlist style'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'default_style',
            			'name' => 'New and Sale badge default style'
            			),
            		array(
            			'id' => 'classic_style',
            			'name' => 'New and Sale badge classic style'
            			),
            		array(
            			'id' => 'square_style',
            			'name' => 'New and Sale badge square style'
            			),
            		)
            	)
        ), // add in this positions.
        array(
            'type' => 'switch',
            'label' => $this->l('Display Quick View Button'),
            'desc' => $this->l('You Can Enable or Disable Quick View Button'),
            'name' => 'prd_list_quickview',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 1,
            	'demo_6' => 1,
            	'demo_7' => 1,
            	'demo_8' => 1,
            	'demo_9' => 1,
            	'demo_10' => 1,
            ),
            'values' => array(
              array(
                'id' => 'prd_list_quickview',
                'value' => 1,
                'label' => $this->l('Enabled')
                ),
              array(
                'id' => 'prd_list_quickview',
                'value' => 0,
                'label' => $this->l('Disabled')
                )
            )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Display Add To Cart In Product List'),
            'desc' => $this->l('You Can Enable or Disable Add To Cart Button In Product List'),
            'name' => 'prd_list_addtocart',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 1,
            	'demo_6' => 1,
            	'demo_7' => 1,
            	'demo_8' => 1,
            	'demo_9' => 1,
            	'demo_10' => 1,
            ),
            'values' => array(
              array(
                'id' => 'prd_list_addtocart',
                'value' => 1,
                'label' => $this->l('Enabled')
                ),
              array(
                'id' => 'prd_list_addtocart',
                'value' => 0,
                'label' => $this->l('Disabled')
                )
            )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Display WishList In Product List'),
            'desc' => $this->l('You Can Enable or Disable WishList Button In Product List'),
            'name' => 'prd_list_wishlist',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 1,
            	'demo_6' => 1,
            	'demo_7' => 1,
            	'demo_8' => 1,
            	'demo_9' => 1,
            	'demo_10' => 1,
            ),
            'values' => array(
              array(
                'id' => 'prd_list_wishlist',
                'value' => 1,
                'label' => $this->l('Enabled')
                ),
              array(
                'id' => 'prd_list_wishlist',
                'value' => 0,
                'label' => $this->l('Disabled')
                )
            )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Display Rating In Product List'),
            'desc' => $this->l('You Can Enable or Disable Rating In Product List'),
            'name' => 'prd_list_rating',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 1,
            	'demo_6' => 1,
            	'demo_7' => 1,
            	'demo_8' => 1,
            	'demo_9' => 1,
            	'demo_10' => 1,
            ),
            'values' => array(
              array(
                'id' => 'prd_list_rating',
                'value' => 1,
                'label' => $this->l('Enabled')
                ),
              array(
                'id' => 'prd_list_rating',
                'value' => 0,
                'label' => $this->l('Disabled')
                )
            )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Enable Image gray background'),
            'desc' => $this->l('Enable gray background on product image'),
            'name' => 'gray_image_bg',
            'is_bool' => true,
            'default_val' => 1,
            'data_by_demo' => array(
            	'demo_2' => 1,
            	'demo_12' => 1,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 1,
            	'demo_6' => 1,
            	'demo_7' => 1,
            	'demo_8' => 1,
            	'demo_9' => 1,
            	'demo_10' => 1,
            ),
            'values' => array(
              array(
	            'id' => 'gray_image_bg',
	            'value' => 1,
	            'label' => $this->l('Enabled')
             	),
              array(
	            'id' => 'gray_image_bg',
	            'value' => 0,
	            'label' => $this->l('Disabled')
            	)
          	)
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);
$this->fields_form[10]['form'] = array(
	'tinymce' => true,
  	'legend' => array(
  	'title' => $this->l('Category Page'),
    ),
    'input' => array(
    	array(
            'type' => 'select',
            'label' => $this->l('Where You want to display Filters Block.'),
            'name' => 'display_filters',
            'default_val' => 'top',
            'data_by_demo' => array(
            	'demo_2' => 'top',
            	'demo_12' => 'top',
            	'demo_1' => 'left',
            	'demo_3' => 'top',
            	'demo_4' => 'left',
            	'demo_5' => 'top',
            	'demo_6' => 'left',
            	'demo_7' => 'top',
            	'demo_8' => 'left',
            	'demo_9' => 'left',
            	'demo_10' => 'left',
            ),
            'options' => array(
    			'id' => 'id_option',
    			'name' => 'name',
    			'query' => array(
    				array(
    					'id_option' => 'left',
    					'name' => 'Left Column'
    					),
    				array(
    					'id_option' => 'right',
    					'name' => 'Right Column'
    					),
    				array(
    					'id_option' => 'top',
    					'name' => 'Page Top Position'
    					),
    				),
    		  )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Filters on top: Several style'),
            'name' => 'filters_colps_style',
            'default_val' => 'filters_colps_fluid',
            'data_by_demo' => array(
            	'demo_2' => 'filters_colps_fluid',
            	'demo_12' => 'filters_colps_fluid',
            	'demo_1' => 'filters_colps_fluid',
            	'demo_3' => 'filters_colps_open',
            	'demo_4' => 'filters_colps_fluid',
            	'demo_5' => 'filters_all_open',
            	'demo_6' => 'filters_colps_fluid',
            	'demo_7' => 'filters_colps_fluid',
            	'demo_8' => 'filters_colps_fluid',
            	'demo_9' => 'filters_colps_fluid',
            	'demo_10' => 'filters_colps_fluid',
            ),
            'options' => array(
    			'id' => 'id_option',
    			'name' => 'name',
    			'query' => array(
    				array(
    					'id_option' => 'filters_all_open',
    					'name' => 'Filters on top always open'
    					),
    				array(
    					'id_option' => 'filters_colps_open',
    					'name' => 'Filters on top collapse open'
    					),
    				array(
    					'id_option' => 'filters_colps_close',
    					'name' => 'Filters on top collapse close'
    					),
    				array(
    					'id_option' => 'filters_colps_fluid',
    					'name' => 'Filters on top fluid style'
    					)
    				),
    		  )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Enable collapse for layered accordion'),
            'name' => 'cat_layred_accordion',
            'desc' => 'You Can Enable or Disable collapse for layered accordion in category Page',
            'class' => 't',
            'is_bool' => true,
            'default_val' => '1',
            'data_by_demo' => array(
            	'demo_2' => '1',
            	'demo_12' => '1',
            	'demo_1' => '1',
            	'demo_3' => '1',
            	'demo_4' => '1',
            	'demo_5' => '1',
            	'demo_6' => '1',
            	'demo_7' => '1',
            	'demo_8' => '1',
            	'demo_9' => '1',
            	'demo_10' => '1',
            ),
            'values' => array(
              array(
            'id' => 'cat_layred_accordion',
            'value' => 1,
            'label' => $this->l('Enabled')
             ),
              array(
            'id' => 'cat_layred_accordion',
            'value' => 0,
            'label' => $this->l('Disabled')
            )
          )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Enable category page content scene block'),
            'name' => 'cat_scene_banner_block',
            'desc' => 'You Can Enable or Disable content scene block in category Page',
            'class' => 't',
            'is_bool' => true,
            'default_val' => '0',
            'data_by_demo' => array(
            	'demo_2' => '0',
            	'demo_12' => '0',
            	'demo_1' => '0',
            	'demo_3' => '0',
            	'demo_4' => '1',
            	'demo_5' => '0',
            	'demo_6' => '0',
            	'demo_7' => '0',
            	'demo_8' => '0',
            	'demo_9' => '1',
            	'demo_10' => '0',
            ),
            'values' => array(
	              array(
		            'id' => 'cat_scene_banner_block_on',
		            'value' => 1,
		            'label' => $this->l('Enabled')
	             ),
	              array(
		            'id' => 'cat_scene_banner_block_off',
		            'value' => 0,
		            'label' => $this->l('Disabled')
	            )
          )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Enable category page content scene banner image'),
            'name' => 'cat_scene_banner_bg',
            'desc' => 'You Can Enable or Disable content scene banner image in category Page',
            'class' => 't',
            'is_bool' => true,
            'default_val' => '0',
            'data_by_demo' => array(
            	'demo_2' => '0',
            	'demo_12' => '0',
            	'demo_1' => '0',
            	'demo_3' => '0',
            	'demo_4' => '1',
            	'demo_5' => '0',
            	'demo_6' => '0',
            	'demo_7' => '0',
            	'demo_8' => '0',
            	'demo_9' => '1',
            	'demo_10' => '0',
            ),
            'values' => array(
	              array(
		            'id' => 'cat_scene_banner_bg',
		            'value' => 1,
		            'label' => $this->l('Enabled')
	             ),
	              array(
		            'id' => 'cat_scene_banner_bg',
		            'value' => 0,
		            'label' => $this->l('Disabled')
	            )
          )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Enable category page sub-category thumbnail image'),
            'name' => 'cat_subcat_thumb',
            'desc' => 'You Can Enable or Disable sub-category thumbnail image in category Page',
            'class' => 't',
            'is_bool' => true,
            'default_val' => '0',
            'data_by_demo' => array(
            	'demo_2' => '0',
            	'demo_12' => '0',
            	'demo_1' => '0',
            	'demo_3' => '0',
            	'demo_4' => '4',
            	'demo_5' => '0',
            	'demo_6' => '0',
            	'demo_7' => '0',
            	'demo_8' => '0',
            	'demo_9' => '1',
            	'demo_10' => '0',
            ),
            'values' => array(
	              array(
		            'id' => 'cat_subcat_thumb',
		            'value' => 1,
		            'label' => $this->l('Enabled')
	             ),
	              array(
		            'id' => 'cat_subcat_thumb',
		            'value' => 0,
		            'label' => $this->l('Disabled')
	            )
          )
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);
// START TAB 3
$this->fields_form[4]['form'] = array(
	'tinymce' => true,
  	'legend' => array(
  	'title' => $this->l('Product Page'),
    ),
    'input' => array(
        array(
            'type' => 'select',
            'label' => $this->l('Product page column style'),
            'name' => 'prod_page_style',
            'default_val' => 'prod_thumb_bottom_sidebar',
            'data_by_demo' => array(
            	'demo_2' => 'prod_thumb_bottom_sidebar',
            	'demo_12' => 'prod_thumb_bottom_sidebar',
            	'demo_1' => 'prod_thumb_right',
            	'demo_3' => 'prod_thumb_bottom_sidebar',
            	'demo_4' => 'prod_thumb_left',
            	'demo_5' => 'prod_thumb_bottom_sidebar',
            	'demo_6' => 'prod_thumb_bottom_sidebar',
            	'demo_7' => 'prod_thumb_bottom_sidebar',
            	'demo_8' => 'prod_thumb_bottom_sidebar',
            	'demo_9' => 'prod_big_left_column',
            	'demo_10' => 'prod_thumb_right',
            ),
            'options' => array(
    			'id' => 'id_option',
    			'name' => 'name',
    			'query' => array(
    				array(
    					'id_option' => 'prod_thumb_left',
    					'name' => 'Product page thumb left'
    					),
    				array(
    					'id_option' => 'prod_thumb_right',
    					'name' => 'Product page thumb right'
    					),
    				array(
    					'id_option' => 'prod_thumb_bottom',
    					'name' => 'Product page thumb bottom'
    					),
    				array(
    					'id_option' => 'prod_thumb_bottom_sidebar',
    					'name' => 'Product page thumb bottom with sidebar'
    					),
    				array(
    					'id_option' => 'prod_big_left_column',
    					'name' => 'Product page big left column for layout Multiple image'
    					),
    				array(
    					'id_option' => 'prod_full_left_column',
    					'name' => 'Product page full width column for layout full width'
    					)
    				),
    		  )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Product page Big image layout'),
            'name' => 'prod_bigimage_layout',
            'default_val' => 'prod_layout_ezoom_popup',
            'data_by_demo' => array(
            	'demo_2' => 'prod_layout_ezoom_popup',
            	'demo_12' => 'prod_layout_ezoom_popup',
            	'demo_1' => 'prod_layout_ezoom_popup',
            	'demo_3' => 'prod_layout_ezoom_popup',
            	'demo_4' => 'prod_layout_ezoom_popup',
            	'demo_5' => 'prod_layout_ezoom_popup',
            	'demo_6' => 'prod_layout_ezoom_popup',
            	'demo_7' => 'prod_layout_ezoom_popup',
            	'demo_8' => 'prod_layout_ezoom_popup',
            	'demo_9' => 'prod_layout_multi_image',
            	'demo_10' => 'prod_layout_ezoom_popup',
            ),
            'options' => array(
    			'id' => 'id_option',
    			'name' => 'name',
    			'query' => array(
    				array(
    					'id_option' => 'prod_layout_ezoom_popup',
    					'name' => 'Product page layout Ezoom and popup'
    					),
    				array(
    					'id_option' => 'prod_layout_multi_image',
    					'name' => 'Product page layout Multiple image'
    					),
    				array(
    					'id_option' => 'prod_layout_full_style',
    					'name' => 'Product page layout full width'
    					),
    				),
    		  )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Product page Big image style'),
            'name' => 'prod_bigimage_style',
            'default_val' => 'ezoom',
            'data_by_demo' => array(
            	'demo_2' => 'ezoom',
            	'demo_12' => 'ezoom',
            	'demo_1' => 'ezoom',
            	'demo_3' => 'ezoom',
            	'demo_4' => 'ezoom',
            	'demo_5' => 'ezoom',
            	'demo_6' => 'default',
            	'demo_7' => 'ezoom',
            	'demo_8' => 'ezoom',
            	'demo_9' => 'ezoom',
            	'demo_10' => 'ezoom',
            ),
            'desc' => 'This option only works with "Ezoom and popup layout" style',
            'options' => array(
    			'id' => 'id_option',
    			'name' => 'name',
    			'query' => array(
    				array(
    					'id_option' => 'default',
    					'name' => 'Slider with fancybox'
    					),
    				array(
    					'id_option' => 'ezoom',
    					'name' => 'Slider with Ezoom style'
    					),
    				),
    		  )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Product Big image Zoom style'),
            'name' => 'prod_bigimage_zoom_style',
            'default_val' => 'innerzooom',
            'data_by_demo' => array(
            	'demo_2' => 'innerzooom',
            	'demo_12' => 'innerzooom',
            	'demo_1' => 'lenszoominner',
            	'demo_3' => 'innerzooom',
            	'demo_4' => 'lenszoomouter',
            	'demo_5' => 'innerzooom',
            	'demo_6' => 'mousewheelzoom',
            	'demo_7' => 'tints',
            	'demo_8' => 'mousewheelzoom',
            	'demo_9' => 'mousewheelzoom',
            	'demo_10' => 'lenszoominner',
            ),
            'options' => array(
    			'id' => 'id_option',
    			'name' => 'name',
    			'query' => array(
    				array(
    					'id_option' => 'innerzooom',
    					'name' => 'Innerzooom'
    					),
    				array(
    					'id_option' => 'lenszoominner',
    					'name' => 'Lens zoom inner'
    					),
    				array(
    					'id_option' => 'windowout',
    					'name' => 'Square window zoom'
    					),
    				array(
    					'id_option' => 'tints',
    					'name' => 'Reverse color window zoom'
    					),
    				array(
    					'id_option' => 'lenszoomouter',
    					'name' => 'Lens zoom outer'
    					),
    				array(
    					'id_option' => 'mousewheelzoom',
    					'name' => 'Mouse wheel zoom'
    					),
    				),
    		  )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Product tab position'),
            'name' => 'prod_tab_pos',
            'default_val' => 'default',
            'data_by_demo' => array(
            	'demo_2' => 'default',
            	'demo_12' => 'default',
            	'demo_1' => 'default',
            	'demo_3' => 'center',
            	'demo_4' => 'default',
            	'demo_5' => 'default',
            	'demo_6' => 'default',
            	'demo_7' => 'right',
            	'demo_8' => 'default',
            	'demo_9' => 'center',
            	'demo_10' => 'default',
            ),
            'options' => array(
    			'id' => 'id_option',
    			'name' => 'name',
    			'query' => array(
    				array(
    					'id_option' => 'default',
    					'name' => 'Default/Bottom'
    					),
    				array(
    					'id_option' => 'center',
    					'name' => 'Center column'
    					),
    				array(
    					'id_option' => 'right',
    					'name' => 'right column'
    					),
    				),
    		  )
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Product tab style'),
            'name' => 'prod_tab_style',
            'default_val' => 'classic',
            'data_by_demo' => array(
            	'demo_2' => 'classic',
            	'demo_12' => 'classic',
            	'demo_1' => 'classic',
            	'demo_3' => 'classic',
            	'demo_4' => 'general',
            	'demo_5' => 'general',
            	'demo_6' => 'classic',
            	'demo_7' => 'classic',
            	'demo_8' => 'classic',
            	'demo_9' => 'classic',
            	'demo_10' => 'classic',
            ),
            'options' => array(
    			'id' => 'id_option',
    			'name' => 'name',
    			'query' => array(
    				array(
    					'id_option' => 'general',
    					'name' => 'General'
    					),
    				array(
    					'id_option' => 'classic',
    					'name' => 'Classic'
    					),
    				),
    		  )
        ),
        array(
            'type' => 'switch',
            'label' => $this->l('Enable Next/Previous Product Show in Product Page'),
            'name' => 'prd_next_prev',
            'desc' => 'You Can Enable or Disable Next/Previous Product Show in Product Page',
            'class' => 't',
            'is_bool' => true,
            'default_val' => '1',
            'data_by_demo' => array(
            	'demo_2' => '1',
            	'demo_12' => '1',
            	'demo_1' => '1',
            	'demo_3' => '1',
            	'demo_4' => '1',
            	'demo_5' => '1',
            	'demo_6' => '1',
            	'demo_7' => '1',
            	'demo_8' => '1',
            	'demo_9' => '1',
            	'demo_10' => '1',
            ),
            'values' => array(
              array(
            'id' => 'prd_next_prev',
            'value' => 1,
            'label' => $this->l('Enabled')
             ),
              array(
            'id' => 'prd_next_prev',
            'value' => 0,
            'label' => $this->l('Disabled')
            )
          )
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);
// END TAB 3
// START TAB 4
// All color style
$this->fields_form[5]['form'] = array(
  'legend' => array(
  'title' => $this->l('Color'),
    ),
    'input' => array(
        array(
            'type' => 'colorgroup',
            'label' => $this->l('Select Color Schema'),
            'name' => 'color_group_name_temp',
        ),
    	array(
    	    'type' => 'infoheading',
    	    'label' => $this->l('General color'),
    	    'name' => 'infoheading',
    	    'sublabel' => 'Choose general color style',
    	    'colorclass' => 'success',
    	),
        array(
            'type' => 'color',
            'label' => $this->l('Link color'),
            'name' => 'link_color',
            'default_val' => '#8f8f8f',
            'data_by_demo' => array(
            	'demo_2' => '#8f8f8f',
            	'demo_12' => '#8f8f8f',
            	'demo_1' => '#666',
            	'demo_3' => '#8f8f8f',
            	'demo_4' => '#666',
            	'demo_5' => '#8f8f8f',
            	'demo_6' => '#8f8f8f',
            	'demo_7' => '#666',
            	'demo_8' => '#666',
            	'demo_9' => '#8f8f8f',
            	'demo_10' => '#666',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Link hover color'),
            'name' => 'link_hvr_color',
            'default_val' => '#6b869d',
            'data_by_demo' => array(
            	'demo_2' => '#6b869d',
            	'demo_12' => '#6b869d',
            	'demo_1' => '#000',
            	'demo_3' => '#6b869d',
            	'demo_4' => '#000',
            	'demo_5' => '#6b869d',
            	'demo_6' => '#6b869d',
            	'demo_7' => '#000',
            	'demo_8' => '#000',
            	'demo_9' => '#6b869d',
            	'demo_10' => '#000',
            ),
            'predefine' => array(

            ),
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Button color'),
            'name' => 'infoheading',
            'sublabel' => 'Choose button color style',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Button text color'),
            'name' => 'button_txt_color',
            'default_val' => '#000',
            'data_by_demo' => array(
            	'demo_2' => '#000',
            	'demo_12' => '#000',
            	'demo_1' => '#000',
            	'demo_3' => '#000',
            	'demo_4' => '#000',
            	'demo_5' => '#000',
            	'demo_6' => '#000',
            	'demo_7' => '#000',
            	'demo_8' => '#000',
            	'demo_9' => '#000',
            	'demo_10' => '#000',
            ),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Button background color'),
            'name' => 'button_bg_color',
            'default_val' => 'transparent',
            'data_by_demo' => array(
            	'demo_2' => 'transparent',
            	'demo_12' => 'transparent',
            	'demo_1' => 'transparent',
            	'demo_3' => 'transparent',
            	'demo_4' => 'transparent',
            	'demo_5' => 'transparent',
            	'demo_6' => 'transparent',
            	'demo_7' => 'transparent',
            	'demo_8' => 'transparent',
            	'demo_9' => 'transparent',
            	'demo_10' => 'transparent',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Button hover text color'),
            'name' => 'button_hvr_txt_color',
            'default_val' => '#FFF',
            'data_by_demo' => array(
            	'demo_2' => '#FFF',
            	'demo_12' => '#FFF',
            	'demo_1' => '#FFF',
            	'demo_3' => '#FFF',
            	'demo_4' => '#FFF',
            	'demo_5' => '#FFF',
            	'demo_6' => '#FFF',
            	'demo_7' => '#FFF',
            	'demo_8' => '#FFF',
            	'demo_9' => '#FFF',
            	'demo_10' => '#FFF',
            ),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Button hover bg color'),
            'name' => 'button_hvr_bg_color',
            'default_val' => '#000000',
            'data_by_demo' => array(
            	'demo_2' => '#000000',
            	'demo_12' => '#000000',
            	'demo_1' => '#000000',
            	'demo_3' => '#000000',
            	'demo_4' => '#000000',
            	'demo_5' => '#000000',
            	'demo_6' => '#000000',
            	'demo_7' => '#000000',
            	'demo_8' => '#000000',
            	'demo_9' => '#000000',
            	'demo_10' => '#000000',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('header area color'),
            'name' => 'infoheading_header',
            'sublabel' => 'Choose header area several section color',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Header Banner background color'),
            'name' => 'header_banner_bg',
            'default_val' => '#6B869D',
            'data_by_demo' => array(
            	'demo_2' => '#6B869D',
            	'demo_12' => '#6B869D',
            	'demo_1' => '#6B869D',
            	'demo_3' => '#6B869D',
            	'demo_4' => '#6B869D',
            	'demo_5' => '#6B869D',
            	'demo_6' => '#6B869D',
            	'demo_7' => '#6B869D',
            	'demo_8' => '#6B869D',
            	'demo_9' => '#6B869D',
            	'demo_10' => '#6B869D',
            ),
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Page heading color area'),
            'name' => 'infoheading_nav',
            'sublabel' => 'Choose page heading color style',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Page heading color'),
            'name' => 'page_headidng_color',
            'default_val' => '#E1E1E1',
            'data_by_demo' => array(
            	'demo_2' => '#E1E1E1',
            	'demo_12' => '#E1E1E1',
            	'demo_1' => '#E1E1E1',
            	'demo_3' => '#E1E1E1',
            	'demo_4' => '#000000',
            	'demo_5' => '#E1E1E1',
            	'demo_6' => '#E1E1E1',
            	'demo_7' => '#000000',
            	'demo_8' => '#000000',
            	'demo_9' => '#E1E1E1',
            	'demo_10' => '#E1E1E1',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Page heading simple color'),
            'name' => 'page_headidng_simple_color',
            'default_val' => '#FFFFFF',
            'data_by_demo' => array(
            	'demo_2' => '#FFFFFF',
            	'demo_12' => '#FFFFFF',
            	'demo_1' => '',
            	'demo_3' => '#FFFFFF',
            	'demo_4' => '',
            	'demo_5' => '#FFFFFF',
            	'demo_6' => '#FFFFFF',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '#FFFFFF',
            	'demo_10' => '',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Main Menu area color'),
            'name' => 'infoheading_menu',
            'sublabel' => 'Choose page heading color style',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Menu Background color'),
            'name' => 'menu_bg_color',
            'default_val' => '#F7F7F7',
            'data_by_demo' => array(
            	'demo_2' => '#F7F7F7',
            	'demo_12' => '#F7F7F7',
            	'demo_1' => '',
            	'demo_3' => '',
            	'demo_4' => '',
            	'demo_5' => '',
            	'demo_6' => '#F7F7F7',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '#F7F7F7',
            	'demo_10' => '',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Menu text color'),
            'name' => 'menu_txt_color',
            'default_val' => '#4b4b4b',
            'data_by_demo' => array(
            	'demo_2' => '#4b4b4b',
            	'demo_12' => '#4b4b4b',
            	'demo_1' => '',
            	'demo_3' => '',
            	'demo_4' => '',
            	'demo_5' => '',
            	'demo_6' => '#4b4b4b',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '#4b4b4b',
            	'demo_10' => '',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Menu text hover color'),
            'name' => 'menu_txt_hvr_color',
            'default_val' => '#6b869d',
            'data_by_demo' => array(
            	'demo_2' => '#6b869d',
            	'demo_12' => '#6b869d',
            	'demo_1' => '',
            	'demo_3' => '',
            	'demo_4' => '',
            	'demo_5' => '',
            	'demo_6' => '#6b869d',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '#6b869d',
            	'demo_10' => '',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Product color'),
            'name' => 'infoheading_nav',
            'sublabel' => 'Choose main menu color style',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('product name color'),
            'name' => 'prod_name_color',
            'default_val' => '#666666',
            'data_by_demo' => array(
            	'demo_2' => '#666666',
            	'demo_12' => '#666666',
            	'demo_1' => '#666666',
            	'demo_3' => '#666666',
            	'demo_4' => '#666666',
            	'demo_5' => '#666666',
            	'demo_6' => '#666666',
            	'demo_7' => '#666666',
            	'demo_8' => '#666666',
            	'demo_9' => '#666666',
            	'demo_10' => '#666666',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('product price color'),
            'name' => 'prod_price_color',
            'default_val' => '#000000',
            'data_by_demo' => array(
            	'demo_2' => '#000000',
            	'demo_12' => '#000000',
            	'demo_1' => '#000000',
            	'demo_3' => '#000000',
            	'demo_4' => '#000000',
            	'demo_5' => '#000000',
            	'demo_6' => '#000000',
            	'demo_7' => '#000000',
            	'demo_8' => '#000000',
            	'demo_9' => '#000000',
            	'demo_10' => '#000000',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('product rating color'),
            'name' => 'prod_rating_color',
            'default_val' => '#FFCC00',
            'data_by_demo' => array(
            	'demo_2' => '#FFCC00',
            	'demo_12' => '#FFCC00',
            	'demo_1' => '#FFCC00',
            	'demo_3' => '#FFCC00',
            	'demo_4' => '#FFCC00',
            	'demo_5' => '#FFCC00',
            	'demo_6' => '#FFCC00',
            	'demo_7' => '#FFCC00',
            	'demo_8' => '#FFCC00',
            	'demo_9' => '#FFCC00',
            	'demo_10' => '#FFCC00',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('product sale badge'),
            'name' => 'prod_sale_badge',
            'default_val' => '#cc0000',
            'data_by_demo' => array(
            	'demo_2' => '#cc0000',
            	'demo_12' => '#cc0000',
            	'demo_1' => '#cc0000',
            	'demo_3' => '#cc0000',
            	'demo_4' => '#cc0000',
            	'demo_5' => '#cc0000',
            	'demo_6' => '#cc0000',
            	'demo_7' => '#cc0000',
            	'demo_8' => '#cc0000',
            	'demo_9' => '#cc0000',
            	'demo_10' => '#cc0000',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('product new badge'),
            'name' => 'prod_new_badge',
            'default_val' => '#000000',
            'data_by_demo' => array(
            	'demo_2' => '#000000',
            	'demo_12' => '#000000',
            	'demo_1' => '#000000',
            	'demo_3' => '#000000',
            	'demo_4' => '#000000',
            	'demo_5' => '#000000',
            	'demo_6' => '#000000',
            	'demo_7' => '#000000',
            	'demo_8' => '#000000',
            	'demo_9' => '#000000',
            	'demo_10' => '#000000',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Footer top fullwidth area color'),
            'name' => 'infoheading_nav',
            'sublabel' => 'Choose footer top fullwidth area color style',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer top fullwidth backgrond color'),
            'name' => 'footer_top_fullwidth_bg_color',
            'default_val' => '#6B869D',
            'data_by_demo' => array(
            	'demo_2' => '#6B869D',
            	'demo_12' => '#6B869D',
            	'demo_1' => '',
            	'demo_3' => '#6B869D',
            	'demo_4' => '',
            	'demo_5' => '#6B869D',
            	'demo_6' => '#6B869D',
            	'demo_7' => '#6B869D',
            	'demo_8' => '',
            	'demo_9' => '#ffffff',
            	'demo_10' => '',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Footer Top area color'),
            'name' => 'infoheading_nav',
            'sublabel' => 'Choose Footer top area color style',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer top backgrond color'),
            'name' => 'footer_top_bg_color',
            'default_val' => '#6B869D',
            'data_by_demo' => array(
            	'demo_2' => '#6B869D',
            	'demo_12' => '#6B869D',
            	'demo_1' => '',
            	'demo_3' => '#6B869D',
            	'demo_4' => '#fff',
            	'demo_5' => '#6B869D',
            	'demo_6' => '#6B869D',
            	'demo_7' => '#FFFFFF',
            	'demo_8' => '',
            	'demo_9' => '#6B869D',
            	'demo_10' => '',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Footer Middle area color'),
            'name' => 'infoheading_nav',
            'sublabel' => 'Choose Footer middle area color style',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer middle backgrond color'),
            'name' => 'footer_mid_bg_color',
            'default_val' => '#1E1E1E',
            'data_by_demo' => array(
            	'demo_2' => '#1E1E1E',
            	'demo_12' => '#1E1E1E',
            	'demo_1' => '#FFF',
            	'demo_3' => '#fff',
            	'demo_4' => '#1E1E1E',
            	'demo_5' => '#fff',
            	'demo_6' => '#1E1E1E',
            	'demo_7' => '#1E1E1E',
            	'demo_8' => '',
            	'demo_9' => '#fff',
            	'demo_10' => '#1E1E1E',
            ),
            'predefine' => array(
            	'footer_white' => '#fff',
            	'footer_mid_white' => '#fff',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer middle heading color'),
            'name' => 'footer_mid_heading_color',
            'default_val' => '#fffbfb',
            'data_by_demo' => array(
            	'demo_2' => '#fffbfb',
            	'demo_12' => '#fffbfb',
            	'demo_1' => '#383838',
            	'demo_3' => '#383838',
            	'demo_4' => '#fffbfb',
            	'demo_5' => '#383838',
            	'demo_6' => '#fffbfb',
            	'demo_7' => '#fffbfb',
            	'demo_8' => '',
            	'demo_9' => '#383838',
            	'demo_10' => '#383838',
            ),
            'predefine' => array(
            	'footer_white' => '#383838',
            	'footer_mid_white' => '#383838',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer middle heading hover color'),
            'name' => 'footer_mid_heading_hvr_color',
            'default_val' => '#858585',
            'data_by_demo' => array(
            	'demo_2' => '#858585',
            	'demo_12' => '#858585',
            	'demo_1' => '#6E839A',
            	'demo_3' => '#6E839A',
            	'demo_4' => '#858585',
            	'demo_5' => '#6E839A',
            	'demo_6' => '#858585',
            	'demo_7' => '#858585',
            	'demo_8' => '',
            	'demo_9' => '#6E839A',
            	'demo_10' => '#6E839A',
            ),
            'predefine' => array(
            	'footer_white' => '#6E839A',
            	'footer_mid_white' => '#6E839A',
            ),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer middle text color'),
            'name' => 'footer_mid_txt_color',
            'default_val' => '#8f8f8f',
            'data_by_demo' => array(
            	'demo_2' => '#8f8f8f',
            	'demo_12' => '#8f8f8f',
            	'demo_1' => '#747474',
            	'demo_3' => '#747474',
            	'demo_4' => '#8f8f8f',
            	'demo_5' => '#747474',
            	'demo_6' => '#8f8f8f',
            	'demo_7' => '#8f8f8f',
            	'demo_8' => '',
            	'demo_9' => '#747474',
            	'demo_10' => '#747474',
            ),
            'predefine' => array(
            	'footer_white' => '#747474',
            	'footer_mid_white' => '#747474',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer middle link color'),
            'name' => 'footer_mid_link_color',
            'default_val' => '#8f8f8f',
            'data_by_demo' => array(
            	'demo_2' => '#8f8f8f',
            	'demo_12' => '#8f8f8f',
            	'demo_1' => '#747474',
            	'demo_3' => '#747474',
            	'demo_4' => '#8f8f8f',
            	'demo_5' => '#747474',
            	'demo_6' => '#8f8f8f',
            	'demo_7' => '#8f8f8f',
            	'demo_8' => '',
            	'demo_9' => '#747474',
            	'demo_10' => '#747474',
            ),
            'predefine' => array(
            	'footer_white' => '#747474',
            	'footer_mid_white' => '#747474',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer middle link hover color'),
            'name' => 'footer_mid_link_hvr_color',
            'default_val' => '#e4e4e4',
            'data_by_demo' => array(
            	'demo_2' => '#e4e4e4',
            	'demo_12' => '#e4e4e4',
            	'demo_1' => '#6E839A',
            	'demo_3' => '#6E839A',
            	'demo_4' => '#e4e4e4',
            	'demo_5' => '#6E839A',
            	'demo_6' => '#e4e4e4',
            	'demo_7' => '#e4e4e4',
            	'demo_8' => '',
            	'demo_9' => '#6E839A',
            	'demo_10' => '#6E839A',
            ),
            'predefine' => array(
            	'footer_white' => '#6E839A',
            	'footer_mid_white' => '#6E839A',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer middle border color'),
            'name' => 'footer_mid_bdr_color',
            'default_val' => '#353535',
            'data_by_demo' => array(
            	'demo_2' => '#353535',
            	'demo_12' => '#353535',
            	'demo_1' => '#E8E8E8',
            	'demo_3' => '#E8E8E8',
            	'demo_4' => '#353535',
            	'demo_5' => '#E8E8E8',
            	'demo_6' => '#353535',
            	'demo_7' => '#353535',
            	'demo_8' => '',
            	'demo_9' => '#E8E8E8',
            	'demo_10' => '#2B2B2B',
            ),
            'predefine' => array(
            	'footer_white' => '#E8E8E8',
            	'footer_mid_white' => '#E8E8E8',
            	),
        ),
        // footer bottom color
        array(
            'type' => 'infoheading',
            'label' => $this->l('Footer Bottom area color'),
            'name' => 'infoheading_nav',
            'sublabel' => 'Choose footer bottom area color style',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer bottom backgrond color'),
            'name' => 'footer_btm_bg_color',
            'default_val' => '#1E1E1E',
            'data_by_demo' => array(
            	'demo_2' => '#1E1E1E',
            	'demo_12' => '#1E1E1E',
            	'demo_1' => '#1E1E1E',
            	'demo_3' => '#1E1E1E',
            	'demo_4' => '#1E1E1E',
            	'demo_5' => '#fff',
            	'demo_6' => '#1E1E1E',
            	'demo_7' => '#1E1E1E',
            	'demo_8' => '',
            	'demo_9' => '#1E1E1E',
            	'demo_10' => '#1E1E1E',
            ),
            'predefine' => array(
            	'footer_white' => '#fff',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer bottom text color'),
            'name' => 'footer_btm_txt_color',
            'default_val' => '#858585',
            'data_by_demo' => array(
            	'demo_2' => '#858585',
            	'demo_12' => '#858585',
            	'demo_1' => '#858585',
            	'demo_3' => '#858585',
            	'demo_4' => '#858585',
            	'demo_5' => '#8f8f8f',
            	'demo_6' => '#858585',
            	'demo_7' => '#858585',
            	'demo_8' => '',
            	'demo_9' => '#858585',
            	'demo_10' => '#858585',
            ),
            'predefine' => array(
            	'footer_white' => '#8f8f8f',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer bottom link color'),
            'name' => 'footer_btm_link_color',
            'default_val' => '#e4e4e4',
            'data_by_demo' => array(
            	'demo_2' => '#e4e4e4',
            	'demo_12' => '#e4e4e4',
            	'demo_1' => '#e4e4e4',
            	'demo_3' => '#e4e4e4',
            	'demo_4' => '#e4e4e4',
            	'demo_5' => '#6b869d',
            	'demo_6' => '#e4e4e4',
            	'demo_7' => '#e4e4e4',
            	'demo_8' => '',
            	'demo_9' => '#e4e4e4',
            	'demo_10' => '#e4e4e4',
            ),
            'predefine' => array(
            	'footer_white' => '#6b869d',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer bottom link hover color'),
            'name' => 'footer_btm_link_hvr_color',
            'default_val' => '#858585',
            'data_by_demo' => array(
            	'demo_2' => '#858585',
            	'demo_12' => '#858585',
            	'demo_1' => '#858585',
            	'demo_3' => '#858585',
            	'demo_4' => '#858585',
            	'demo_5' => '#858585',
            	'demo_6' => '#858585',
            	'demo_7' => '#858585',
            	'demo_8' => '',
            	'demo_9' => '#858585',
            	'demo_10' => '#858585',
            ),
            'predefine' => array(
            	'footer_white' => '#858585',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer bottom border color'),
            'name' => 'footer_btm_bdr_color',
            'default_val' => '#353535',
            'data_by_demo' => array(
            	'demo_2' => '#353535',
            	'demo_12' => '#353535',
            	'demo_1' => '#1E1E1E',
            	'demo_3' => '#353535',
            	'demo_4' => '#353535',
            	'demo_5' => '#E8E8E8',
            	'demo_6' => '#353535',
            	'demo_7' => '#353535',
            	'demo_8' => '',
            	'demo_9' => '#353535',
            	'demo_10' => '#2B2B2B',
            ),
            'predefine' => array(
            	'footer_white' => '#E8E8E8',
            	),
        ),
        array(
            'type' => 'infoheading',
            'label' => $this->l('Newsletter area color'),
            'name' => 'infoheading_nav',
            'sublabel' => 'Choose Newsletter area color style',
            'colorclass' => 'success',
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Newsletter area color'),
            'name' => 'newsletter_block_bg_color',
            'default_val' => '#6B869D',
            'data_by_demo' => array(
            	'demo_2' => '#6B869D',
            	'demo_12' => '#6B869D',
            	'demo_1' => '',
            	'demo_3' => '#7995B2',
            	'demo_4' => '#7995B2',
            	'demo_5' => '#6B869D',
            	'demo_6' => '#6B869D',
            	'demo_7' => '#7995B2',
            	'demo_8' => '',
            	'demo_9' => '#6B869D',
            	'demo_10' => '',
            ),
            'predefine' => array(
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer middle Newsletter color'),
            'name' => 'ft_mid_newsletter_bg_color',
            'default_val' => '#2C2C2C',
            'data_by_demo' => array(
            	'demo_2' => '#2C2C2C',
            	'demo_12' => '#2C2C2C',
            	'demo_1' => '',
            	'demo_3' => '#2C2C2C',
            	'demo_4' => '',
            	'demo_5' => '#E8E8E8',
            	'demo_6' => '#2C2C2C',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '#E8E8E8',
            	'demo_10' => '',
            ),
            'predefine' => array(
            	'footer_white' => '#E8E8E8',
            	'footer_mid_white' => '#E8E8E8',
            	),
        ),
        array(
            'type' => 'color',
            'label' => $this->l('Footer middle Newsletter button color'),
            'name' => 'ft_mid_newsletter_btn_color',
            'default_val' => '#0A0A0A',
            'data_by_demo' => array(
            	'demo_2' => '#0A0A0A',
            	'demo_12' => '#0A0A0A',
            	'demo_1' => '',
            	'demo_3' => '#FFFFFF',
            	'demo_4' => '',
            	'demo_5' => '#FFFFFF',
            	'demo_6' => '#0A0A0A',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '#FFFFFF',
            	'demo_10' => '',
            ),
            'predefine' => array(
            	'footer_white' => '#FFFFFF',
            	'footer_mid_white' => '#FFFFFF',
            	),
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);
// END TAB 4


// Footer area style
$this->fields_form[12]['form'] = array(
	'tinymce' => true,
  	'legend' => array(
  	'title' => $this->l('Footer area'),
    ),
    'input' => array(
    	array(
    	    'type' => 'select',
    	    'label' => $this->l('Footer style'),
    	    'name' => 'footer_style',
    	    'default_val' => 'footer_style_default',
            'data_by_demo' => array(
            	'demo_2' => 'footer_style_default',
            	'demo_12' => 'footer_style_default',
            	'demo_1' => 'footer_style_creative',
            	'demo_3' => 'footer_style_creative',
            	'demo_4' => 'footer_style_creative',
            	'demo_5' => 'footer_style_creative',
            	'demo_6' => 'footer_style_default',
            	'demo_7' => 'footer_style_creative',
            	'demo_8' => 'footer_style_creative',
            	'demo_9' => 'footer_style_classic',
            	'demo_10' => 'footer_style_classic',
            ),
    	    'desc' => $this->l('Choose different footer style'),
    	    'options' => array(
    	    	'id' => 'id',
    	    	'name' => 'name',
    	    	'query' => array(
    	    		array(
    	    			'id' => 'footer_style_default',
    	    			'name' => 'Footer style default'
    	    			),
    	    		array(
    	    			'id' => 'footer_style_creative',
    	    			'name' => 'Footer style creative'
    	    			),
    	    		array(
    	    			'id' => 'footer_style_classic',
    	    			'name' => 'Footer style classic'
    	    			),
    	    		
    	    		)
    	    	)
    	),
    	array(
    	    'type' => 'text',
    	    'label' => $this->l('Footer top full width area height'),
    	    'desc' => $this->l('footer_top_height. (default 101px)'),
    	    'name' => 'footer_top_fullwidth_height',
    	    'default_val' => '',
            'data_by_demo' => array(
            	'demo_2' => '',
            	'demo_12' => '',
            	'demo_1' => '',
            	'demo_3' => '',
            	'demo_4' => '',
            	'demo_5' => '',
            	'demo_6' => '',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '',
            	'demo_10' => '',
            ),
    	    'size' => 13,
    	),
        array(
    	    'type' => 'infoheading',
    	    'label' => $this->l('Footer top fullwidth area background image'),
    	    'name' => 'infoheading',
    	    'colorclass' => 'info_custom success',
    	),
    	array(
    	    'type' => 'select',
    	    'label' => $this->l('Footer Top fullwidth Enable Background image'),
    	    'name' => 'footer_top_fullwidth_bg_on',
    	    'default_val' => 'none',
            'data_by_demo' => array(
            	'demo_2' => 'none',
            	'demo_12' => 'none',
            	'demo_1' => 'none',
            	'demo_3' => 'none',
            	'demo_4' => 'none',
            	'demo_5' => 'none',
            	'demo_6' => 'none',
            	'demo_7' => 'none',
            	'demo_8' => 'none',
            	'demo_9' => 'none',
            	'demo_10' => '0',
            ),
    	    'desc' => $this->l('Enable background image or use background pattern'),
    	    'options' => array(
    	        'id' => 'id',
    	        'name' => 'name',
    	        'query' => array(
    	            array(
    	                'id' => '1',
    	                'name' => 'BackGround'
    	                ),
    	            array(
    	                'id' => '0',
    	                'name' => 'Pattern'
    	                ),
    	            array(
    	                'id' => 'none',
    	                'name' => 'None'
    	                ),
    	            )
    	        )
    	),
    	array(
            'type' => 'upload_img',
            'label' => $this->l('Select Background Image'),
            'desc' => $this->l('Select footer top fullwidth background Image'),
            'name' => 'footer_top_fullwidth_bg',
        ),
        array(
            'type' => 'bg_pattern',
            'label' => $this->l('Select Background Pattern'),
            'desc' => $this->l('Select footer top background pattern'),
            'name' => 'footer_top_fullwidth_pattern',
            'default_val' => '',
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);



// Blog style
$this->fields_form[6]['form'] = array(
	'tinymce' => true,
  	'legend' => array(
  	'title' => $this->l('Blog style'),
    ),
    'input' => array(
        array(
            'type' => 'select',
            'label' => $this->l('Blog style'),
            'name' => 'blog_style',
            'default_val' => 'default',
            'data_by_demo' => array(
            	'demo_2' => 'default',
            	'demo_12' => 'default',
            	'demo_1' => 'default',
            	'demo_3' => 'column',
            	'demo_4' => 'default',
            	'demo_5' => 'default',
            	'demo_6' => 'default',
            	'demo_7' => 'default',
            	'demo_8' => 'default',
            	'demo_9' => 'default',
            	'demo_10' => 'default',
            ),
            'desc' => $this->l('Choose blog style Default/Grid'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => 'default',
            			'name' => 'Default'
            			),
            		array(
            			'id' => 'column',
            			'name' => 'Column'
            			),
            		)
            	)
        ),
        array(
            'type' => 'select',
            'label' => $this->l('Blog no of column'),
            'name' => 'blog_no_of_col',
            'default_val' => '4',
            'data_by_demo' => array(
            	'demo_2' => '4',
            	'demo_12' => '4',
            	'demo_1' => '4',
            	'demo_3' => '6',
            	'demo_4' => '4',
            	'demo_5' => '4',
            	'demo_6' => '4',
            	'demo_7' => '4',
            	'demo_8' => '4',
            	'demo_9' => '4',
            	'demo_10' => '4',
            ),
            'desc' => $this->l('Choose blog gird style no of column'),
            'options' => array(
            	'id' => 'id',
            	'name' => 'name',
            	'query' => array(
            		array(
            			'id' => '6',
            			'name' => 'Two column'
            			),
            		array(
            			'id' => '4',
            			'name' => 'Three column'
            			),
            		array(
            			'id' => '3',
            			'name' => 'Four column'
            			),
            		)
            	)
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);


// Maintanence
$this->fields_form[8]['form'] = array(
	'tinymce' => true,
  	'legend' => array(
  	'title' => $this->l('Maintenance'),
    ),
    'input' => array(
        array(
        	'type' => 'text',
        	'label' => $this->l('Maintanance Date Time Set'),
        	'name' => 'maintanance_date',
        	'desc' => $this->l('Please Enter Date Time. Must be Use This format.(such as June 7, 2087 15:03:26)'),
        	'default_val' => 'June 7, 2087 15:03:26',
            'data_by_demo' => array(
            	'demo_2' => 'June 7, 2087 15:03:26',
            	'demo_12' => 'June 7, 2087 15:03:26',
            	'demo_1' => 'June 7, 2087 15:03:26',
            	'demo_3' => 'June 7, 2087 15:03:26',
            	'demo_4' => 'June 7, 2087 15:03:26',
            	'demo_5' => 'June 7, 2087 15:03:26',
            	'demo_6' => 'June 7, 2087 15:03:26',
            	'demo_7' => 'June 7, 2087 15:03:26',
            	'demo_8' => 'June 7, 2087 15:03:26',
            	'demo_9' => 'June 7, 2087 15:03:26',
            	'demo_10' => 'June 7, 2087 15:03:26',
            	),
        	),
        array(
        	'type' => 'text',
        	'label' => $this->l('Maintanance Title'),
        	'name' => 'maintanance_title',
            'desc' => $this->l('Maintanance Title'),
        	'default_val' => 'We\'ll be back soon',
            'data_by_demo' => array(
            	'demo_2' => 'We\'ll be back soon',
            	'demo_12' => 'We\'ll be back soon',
            	'demo_1' => 'We\'ll be back soon',
            	'demo_3' => 'We\'ll be back soon',
            	'demo_4' => 'We\'ll be back soon',
            	'demo_5' => 'We\'ll be back soon',
            	'demo_6' => 'We\'ll be back soon',
            	'demo_7' => 'We\'ll be back soon',
            	'demo_8' => 'We\'ll be back soon',
            	'demo_9' => 'We\'ll be back soon',
            	'demo_10' => 'We\'ll be back soon',
            	),
        	),
        array(
        	'type' => 'textarea',
        	'label' => $this->l('Maintanance Description'),
        	'name' => 'maintanance_desc',
            'desc' => $this->l('Maintanance Description'),
        	'default_val' => 'We are currently updating our shop and will be back really soon. Thanks you for your patience.',
            'data_by_demo' => array(
            	'demo_2' => 'We are currently updating our shop and will be back really soon. Thanks you for your patience.',
            	'demo_12' => 'We are currently updating our shop and will be back really soon. Thanks you for your patience.',
            	'demo_1' => 'We are currently updating our shop and will be back really soon. Thanks you for your patience.',
            	'demo_3' => 'We are currently updating our shop and will be back really soon. Thanks you for your patience.',
            	'demo_4' => 'We are currently updating our shop and will be back really soon. Thanks you for your patience.',
            	'demo_5' => 'We are currently updating our shop and will be back really soon. Thanks you for your patience.',
            	'demo_6' => 'We are currently updating our shop and will be back really soon. Thanks you for your patience.',
            	),
        	),
        array(
            'type' => 'upload_img',
            'label' => $this->l('Select Background Image'),
            'desc' => $this->l('Select body background Image'),
            'name' => 'maintanance_bg',
        )
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);


// Extra options for the theme
$this->fields_form[11]['form'] = array(
	'tinymce' => true,
  	'legend' => array(
  	'title' => $this->l('Extra Options'),
    ),
    'input' => array(
    	array(
    	    'type' => 'infoheading',
    	    'label' => $this->l('Newsletter block module'),
    	    'name' => 'infoheading',
    	    'sublabel' => 'Newsletter block module title and text',
    	    'colorclass' => 'info_custom success',
    	),
        array(
        	'type' => 'text',
        	'label' => $this->l('Newsletter title'),
        	'name' => 'newsletter_title',
        	'desc' => $this->l('Please Enter title for Newsletter module'),
        	'default_val' => 'Sing Up For Newsletter',
            'data_by_demo' => array(
            	'demo_2' => 'Sing Up For Newsletter',
            	'demo_12' => 'Sing Up For Newsletter',
            	'demo_1' => 'Sing Up For Newsletter',
            	'demo_3' => 'Sing Up For Newsletter',
            	'demo_4' => 'Sing Up For Newsletter',
            	'demo_5' => 'Sing Up For Newsletter',
            	'demo_6' => 'Sing Up For Newsletter',
            	'demo_7' => 'Subscribe',
            	'demo_8' => 'Subscribe',
            	'demo_9' => 'Sign Up For Newsletter',
            	'demo_10' => ' ',
            	),
        	),
        array(
        	'type' => 'text',
        	'label' => $this->l('Newsletter text'),
        	'name' => 'newsletter_text',
            'desc' => $this->l('Please Enter title bottom text for Newsletter module'),
        	'default_val' => 'Lorem ipsum dolor sit amet, news consectetur fashion adipiscing elit. Suspendisse consequat orci new sed nibh ultricies, eget 
sollicitudin quam consectetur. Donec at mattis purus, ut accumsan nisl. ',
            'data_by_demo' => array(
            	'demo_2' => 'Lorem ipsum dolor sit amet, news consectetur fashion adipiscing elit. Suspendisse consequat orci new sed nibh ultricies, eget 
sollicitudin quam consectetur. Donec at mattis purus, ut accumsan nisl.',
            	'demo_12' => 'Lorem ipsum dolor sit amet, news consectetur fashion adipiscing elit. Suspendisse consequat orci new sed nibh ultricies, eget 
sollicitudin quam consectetur. Donec at mattis purus, ut accumsan nisl.',
            	'demo_1' => 'Lorem ipsum dolor sit amet, news consectetur fashion adipiscing elit. Suspendisse consequat orci new sed nibh ultricies, eget 
sollicitudin quam consectetur. Donec at mattis purus, ut accumsan nisl.',
            	'demo_3' => 'Lorem ipsum dolor sit amet, news consectetur fashion adipiscing elit. Suspendisse consequat orci new sed nibh ultricies, eget 
sollicitudin quam consectetur. Donec at mattis purus, ut accumsan nisl.',
            	'demo_4' => 'Lorem ipsum dolor sit amet, news consectetur fashion adipiscing elit. Suspendisse consequat orci new sed nibh ultricies, eget 
sollicitudin quam consectetur. Donec at mattis purus, ut accumsan nisl.',
            	'demo_5' => 'Lorem ipsum dolor sit amet, news consectetur fashion adipiscing elit. Suspendisse consequat orci new sed nibh ultricies, eget 
sollicitudin quam consectetur. Donec at mattis purus, ut accumsan nisl.',
            	'demo_6' => 'Lorem ipsum dolor sit amet, news consectetur fashion adipiscing elit. Suspendisse consequat orci new sed nibh ultricies, eget 
sollicitudin quam consectetur. Donec at mattis purus, ut accumsan nisl.',
            	'demo_7' => 'Donec at mattis purus, new ut accumsan nisl. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at mattis purus, ut accumsan nisl. Lorem ipsum dolor sit amet.Donec at mattis purus, new ut accumsan nisl. Lorem ipsum dolor sit amet, consectetur',
            	'demo_8' => 'Donec at mattis purus, new ut accumsan nisl. Lorem ipsum dolor sit amet, consectetur  adipiscing elit. Donec at mattis purus, ut accumsan nisl. Lorem ipsum dolor sit amet.',
            	'demo_9' => 'Lorem ipsum dolor sit amet, news consectetur fashion adipiscing elit. Suspendisse consequat orci new sed nibh ultricies, eget sollicitudin quam consectetur. Donec at mattis purus, ut accumsan nisl.',
            	'demo_10' => ' ',
            	),
        	),
        array(
    	    'type' => 'text',
    	    'label' => $this->l('Newsletter block height'),
    	    'desc' => $this->l('Newsletter block height. (default 387px)'),
    	    'name' => 'newsletter_block_height',
    	    'default_val' => '387px',
            'data_by_demo' => array(
            	'demo_2' => '387px',
            	'demo_12' => '387px',
            	'demo_1' => '387px',
            	'demo_3' => '387px',
            	'demo_4' => '387px',
            	'demo_5' => '387px',
            	'demo_6' => '387px',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '387px',
            	'demo_10' => '387px',
            ),
    	    'size' => 13,
    	),
    	array(
    	    'type' => 'switch',
    	    'label' => $this->l('Enable Newsletter Background image'),
    	    'desc' => $this->l('You Can Enable or Disable Newsletter Background image'),
    	    'name' => 'enable_newsletter_bg',
    	    'is_bool' => true,
    	    'default_val' => 0,
            'data_by_demo' => array(
            	'demo_2' => 0,
            	'demo_12' => 0,
            	'demo_1' => 1,
            	'demo_3' => 1,
            	'demo_4' => 1,
            	'demo_5' => 0,
            	'demo_6' => 0,
            	'demo_7' => 0,
            	'demo_8' => 0,
            	'demo_9' => 0,
            	'demo_10' => 1,
            ),
    	    'values' => array(
    	      array(
    	        'id' => 'enable_newsletter_bg_on',
    	        'value' => 1,
    	        'label' => $this->l('Enabled')
    	        ),
    	      array(
    	        'id' => 'enable_newsletter_bg_off',
    	        'value' => 0,
    	        'label' => $this->l('Disabled')
    	        )
    	    )
    	),
    	array(
            'type' => 'upload_img',
            'label' => $this->l('Newsletter block Background Image'),
            'desc' => $this->l('Select Newsletter block background Image'),
            'name' => 'newsletter_block_bg',
            'default_val' => 'newsletter_bg.jpg',
            'data_by_demo' => array(
            	'demo_2' => '',
            	'demo_12' => '',
            	'demo_1' => 'newsletter_bg.jpg',
            	'demo_3' => 'newsletter_bg.jpg',
            	'demo_4' => 'newsletter_bg-4.jpg',
            	'demo_5' => '',
            	'demo_6' => '',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '',
            	'demo_10' => 'newsletter_bg.jpg',
            ),
        ),
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);

// Custom css/js block
$this->fields_form[7]['form'] = array(
	'tinymce' => true,
  	'legend' => array(
  	'title' => $this->l('Custom CSS/JS'),
    ),
    'input' => array(
    	array(
    	    'type' => 'select',
    	    'label' => $this->l('Extra style demo'),
    	    'name' => 'gs_style_demo',
    	    'default_val' => 'gs_style_1',
            'data_by_demo' => array(
            	'demo_2' => 'gs_style_1',
            	'demo_12' => 'gs_style_1',
            	'demo_1' => 'gs_style_1',
            	'demo_3' => 'gs_style_1',
            	'demo_4' => 'gs_style_1',
            	'demo_5' => 'gs_style_1',
            	'demo_6' => 'gs_style_1',
            	'demo_7' => 'gs_style_1',
            	'demo_8' => 'gs_style_1',
            	'demo_9' => 'gs_style_1',
            	'demo_10' => 'gs_style_1',
            ),
    	    'desc' => $this->l('Choose demo wise extra style for margin/padding'),
    	    'options' => array(
    	    	'id' => 'id',
    	    	'name' => 'name',
    	    	'query' => array(
    	    		array(
    	    			'id' => 'gs_style_1',
    	    			'name' => 'GS style one'
    	    			),
    	    		array(
    	    			'id' => 'gs_style_2',
    	    			'name' => 'GS style two'
    	    			),
    	    		array(
    	    			'id' => 'gs_style_3',
    	    			'name' => 'GS style three'
    	    			),
    	    		array(
    	    			'id' => 'gs_style_4',
    	    			'name' => 'GS style four'
    	    			),
    	    		array(
    	    			'id' => 'gs_style_5',
    	    			'name' => 'GS style five'
    	    			),
    	    		array(
    	    			'id' => 'gs_style_6',
    	    			'name' => 'GS style six'
    	    			),
    	    		array(
    	    			'id' => 'gs_style_7',
    	    			'name' => 'GS style seven'
    	    			),
    	    		array(
    	    			'id' => 'gs_style_8',
    	    			'name' => 'GS style eight'
    	    			),
    	    		array(
    	    			'id' => 'gs_style_9',
    	    			'name' => 'GS style nine'
    	    			),
    	    		array(
    	    			'id' => 'gs_style_10',
    	    			'name' => 'GS style ten'
    	    			),
    	    		)
    	    	)
    	),
        array(
        	'type' => 'customtextarea',
        	'label' => $this->l('Custom CSS'),
        	'name' => 'custom_css',
        	'desc' => $this->l('Please Enter Your Custom CSS'),
            'rows' => 30,
            'cols' => 25,
            'class' => "custom_css_class",
        	'default_val' => '',
            'data_by_demo' => array(
            	'demo_2' => '',
            	'demo_12' => '',
            	'demo_1' => '',
            	'demo_3' => '',
            	'demo_4' => '',
            	'demo_5' => '',
            	'demo_6' => '',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '',
            	'demo_10' => '',
            ),
        ),
        array(
        	'type' => 'customtextarea',
        	'label' => $this->l('Custom JS'),
        	'name' => 'custom_js',
            'class' => "custom_js_class",
            'desc' => $this->l('Please Enter Your Custom JS'),
            'rows' => 30,
            'cols' => 25,
        	'default_val' => '',
            'data_by_demo' => array(
            	'demo_2' => '',
            	'demo_12' => '',
            	'demo_1' => '',
            	'demo_3' => '',
            	'demo_4' => '',
            	'demo_5' => '',
            	'demo_6' => '',
            	'demo_7' => '',
            	'demo_8' => '',
            	'demo_9' => '',
            	'demo_10' => '',
           	),
        )
    ),
    'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'btn btn-default pull-right'
    )
);