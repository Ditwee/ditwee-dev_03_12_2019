<?php
$back_js_files = array();
$back_js_files[] = __PS_BASE_URI__.'js/vendor/spin.js';
$back_js_files[] = __PS_BASE_URI__.'js/vendor/ladda.js';
$back_js_files[] = __PS_BASE_URI__."{$this->context->controller->admin_webpath}/themes/{$this->context->employee->bo_theme}/js/jquery.fileupload.js";
$back_js_files[] = __PS_BASE_URI__."{$this->context->controller->admin_webpath}/themes/{$this->context->employee->bo_theme}/js/jquery.fileupload-process.js";
$back_js_files[] = __PS_BASE_URI__."{$this->context->controller->admin_webpath}/themes/{$this->context->employee->bo_theme}/js/jquery.fileupload-validate.js";
$back_js_files[] = __PS_BASE_URI__.'js/jquery/plugins/ajaxfileupload/jquery.ajaxfileupload.js';
$back_js_files[] = __PS_BASE_URI__.'js/jquery/plugins/jquery.colorpicker.js';
