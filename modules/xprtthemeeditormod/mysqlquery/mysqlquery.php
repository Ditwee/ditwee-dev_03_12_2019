<?php

$mysqlquery = array();

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtproducttab` (
				`id_xprtproducttab` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`selection` VARCHAR(300) NOT NULL,
				`active` int(10) NOT NULL,
				`position`int(10) NOT NULL ,
				PRIMARY KEY (`id_xprtproducttab`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'xprtproducttab_lang` (
				`id_xprtproducttab` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL ,
				`title` VARCHAR(300) NOT NULL,
				`content` text NOT NULL,
				PRIMARY KEY (`id_xprtproducttab`, `id_lang`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

$mysqlquery[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'xprtproducttab_shop` (
			  `id_xprtproducttab` int(11) NOT NULL,
			  `id_shop` int(11) DEFAULT NULL,
			  PRIMARY KEY (`id_xprtproducttab`,`id_shop`)
			)ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;

$mysqlquery_u = array();

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtproducttab`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtproducttab_lang`';

$mysqlquery_u[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'xprtproducttab_shop`';
