<div class="temp_{$input.name}_class">
	<p>
		<input type="hidden" name="{$input.name}" id="{$input.name}_mid" value="{$fields_value[$input.name]|escape:'html':'UTF-8'}" class="{$input.name}_mclass"/>

		<button class="btn btn-default" data-style="expand-right" data-size="s" type="button" id="temp_{$input.name}_id">
		<span class="ladda-label">
			<i class="icon-folder-open"></i>
			{l s='Add Image'}
		</span>
		<span class="ladda-spinner">
		</span>
		</button>
		<input type="file" name="temp_{$input.name}" data-url="{$type_upload_url}&type_name=temp_{$input.name}" id="temp_{$input.name}" style="display:none;" />
	</p>
	{foreach $type_bgimages as $image_val}
		<div class="{$input.name}_type_class {if $fields_value[$input.name] == $image_val} selected{/if}" data-image-url="{$image_val}" style="background: url({$image_val});"></div>
	{/foreach}
	<style>
		.{$input.name}_type_class{
			height:50px;
			width:50px;
			display: inline-block;
			border: 1px solid #ccc;
		}
		.{$input.name}_type_class:hover{
			border: 2px solid #f00;
			cursor: pointer;
		}
		.{$input.name}_type_class.selected {
			border: 3px solid #f00;
		}
	</style>
	</div>

	<script type="text/javascript">

	$(document).ready(function(){
	    var import_attach_skin_add_button = Ladda.create( document.querySelector("#temp_{$input.name}_id" ));
	    var import_attach_skin_total_files = 0;
	    var success_message = 'Upload successfully';
	    $("#temp_{$input.name}").fileupload({
	        dataType: 'json',
	        autoUpload: true,
	        singleFileUploads: true,
	        maxFileSize: 1000000000,
	        success: function (e) {
	            showSuccessMessage(success_message);
	            // window.setTimeout(function() {
	            //         location.reload();
	            //     }, 1000);
	        },
	        start: function (e){               
	            import_attach_skin_add_button.start();
	        },
	        fail: function (e,data){
	            showErrorMessage(data.errorThrown.message);
	        },
	        done: function (e, data){
	            if(data.result){
	               
	            }
	        },
	    }).on('fileuploadalways', function (e,data){
	        import_attach_skin_add_button.stop();
	    }).on('fileuploadprocessalways', function (e, data){
	        var index = data.index, file = data.files[index];
	    });
	    $("#temp_{$input.name}_id").on('click', function(e) {
	        e.preventDefault();
	        import_attach_skin_total_files = 0;            
	        $("#temp_{$input.name}").trigger('click');
	    });

	    $(".{$input.name}_type_class").click(function(){
	    	var bg_img_item_id = $("#{$input.name}_mid");
	    	$(this).addClass("selected");
	    	$(this).siblings().removeClass("selected");
	    	bg_img_item_id.val($(this).attr("data-image-url"));
	    });

	});

	</script>