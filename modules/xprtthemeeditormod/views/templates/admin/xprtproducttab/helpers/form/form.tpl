{extends file="helpers/form/form.tpl"}
{block name="input"}
	{if ($input.type == 'selection')}
			<div class="selecttypeclass {$input.name}" id="id_{$input.name}_id">
			<select name="xprtfileds_{$input.name}" class=" fixed-width-xl" id="xprtfileds_{$input.name}" multiple="true">
			    {foreach from=$input.fieldsvalues item=allfield}
			        {if isset($fields_value[$input.name])}
			            {assign var=default_value value=","|explode:$fields_value[$input.name]}
			            {if $allfield['id']|in_array:$default_value}
			                {$selected = 'selected'}
			            {else}
			                {$selected = ''}
			            {/if}
			        {else}
			            {$selected = ''}
			        {/if}
			        <option {$selected} value="{$allfield['id']}">{$allfield['name']}</option>
			    {/foreach}
			</select>
			<input type="hidden" name="{$input.name}" id="{$input.name}" value="{$fields_value[$input.name]}" class="{$input.name} {$input.type}_field">
			<script type="text/javascript">
			    $(function(){
			        var defVal = $("input#{$input.name}").val();
			        if(defVal.length){
			            var ValArr = defVal.split(',');
			            for(var n in ValArr){
			                $( "select#xprtfileds_{$input.name}" ).children('option[value="'+ValArr[n]+'"]').attr('selected','selected');
			            }
			        }
			        $("select#xprtfileds_{$input.name}").select2({ placeholder: "Type Product Name / Product Category / All Products", width: 200, tokenSeparators: [',', ' '] } ).on('change',function(){
			            var data = $(this).select2('data');
			            var select = $(this);
			            var field = select.next("input#{$input.name}");
			            var saved = '';
			            select.children('option').attr('selected',null);
			            if(data.length)
			                $.each(data, function(k,v){
			                    var selected = v.id;   
			                    select.children('option[value="'+selected+'"]').attr('selected','selected');
			                    if(k > 0)
			                        saved += ',';
			                    saved += selected;                                
			                });
			             field.val(saved);   
			        });
			    });
			</script>
			<style type="text/css">
				.select2-container.select2-container-multi
				{ 
					width: 100% !important;
				}
			</style>
			</div>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}