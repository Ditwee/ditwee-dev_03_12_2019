<li class="footer_toolbar_item wishlist item_invisible">
	<a class="footer_toolbar_button f_tooltip" href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|escape:'html':'UTF-8'}" title="{l s='Wishlist' mod='xprtthemeeditormod'}">
		<i class="entypo-heart-empty"></i>
		<div class="footer_toolbar_content_sm">
			<span class="btmwishlist_products_count">{$count}</span>
		</div>
	</a>
</li>