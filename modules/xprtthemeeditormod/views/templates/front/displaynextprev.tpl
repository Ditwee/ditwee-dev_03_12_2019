{*Start Previous product markup*}
{if isset($prev_product)}
<a class="prev_product" href="{$prev_product.link|escape:'html':'UTF-8'}">
	<i class="arrow-double-left"></i>
	<div class="prev_product_content">
		<img class="img-responsive" src="{$link->getImageLink($prev_product.link_rewrite,$prev_product.id_image,'small_default')|escape:'html':'UTF-8'}" alt="" />
		<h4>{$prev_product.name|truncate:15|escape:'html':'UTF-8'}</h4>
	</div>
</a>
{/if}
{*END Previous product markup*}
{*Start Next product markup*}
{if isset($next_product)}
<a class="next_product" href="{$next_product.link|escape:'html':'UTF-8'}">
	<i class="arrow-double-right"></i>
	<div class="next_product_content">
		<img class="img-responsive" src="{$link->getImageLink($next_product.link_rewrite,$next_product.id_image,'small_default')|escape:'html':'UTF-8'}" alt="" />
		<h4>{$next_product.name|truncate:15|escape:'html':'UTF-8'}</h4>
	</div>
</a>
{/if}
{*END NEXT product markup*}