{if $kr_maintanance_title != ''}
	<h1 class="maintanance_heading">
		{$kr_maintanance_title}
	</h1>
{/if}
{if $kr_maintanance_desc != ''}
	<div class="maintanance_text">
		{$kr_maintanance_desc}
	</div>
{/if}
{if $kr_maintanance_date != ''}
	<div id="maintenance_countdown" class="prod_countdown styled" data-date="{$kr_maintanance_date}"></div>
{/if}
{if $kr_maintanance_date != ''}
{$countdownjs}
{literal}
<style>
	body { 
		background-image: url({/literal}{$kr_maintanance_bg_image_url}{literal});
	 } 
</style>
<script type="text/javascript">
	var winHeight = 0;
	$(document).ready(function() {
		docheight()
	 } );
	$(window).resize(function() {
		docheight()
	 } );
	function docheight() { 
		winHeight = $(window).height();
		$('body').css( { 'min-height' : winHeight } )
	 } ;
	 $(function() {
	 	$('#maintenance_countdown').countdown({
	 		render: function(data) {
	 			$(this.el).html("<div class='countdown_list days'><span class='countdown_digit'>" + this.leadingZeros(data.days, 3) + "</span><span class='countdown_label'>{/literal}{l s='Days' mod='xprtthemeeditormod'}{literal}</span></div><div class='countdown_list hrs'><span class='countdown_digit'>" + this.leadingZeros(data.hours, 2) + "</span><span class='countdown_label d_none'>{/literal}{l s='hrs' mod='xprtthemeeditormod'}{literal}</span></div><div class='countdown_list min'><span class='countdown_digit'>" + this.leadingZeros(data.min, 2) + "</span><span class='countdown_label d_none'>{/literal}{l s='min' mod='xprtthemeeditormod'}{literal}</span></div><div class='countdown_list sec'><span class='countdown_digit'>" + this.leadingZeros(data.sec, 2) + "</span><span class='countdown_label d_none'>{/literal}{l s='sec' mod='xprtthemeeditormod'}{literal}</span></div>");
	 		}
	 	});
	 });
</script>
{/literal}
{/if}