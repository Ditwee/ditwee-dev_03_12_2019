

{if isset($results) && !empty($results)}
	{foreach from=$results item=result}

		{if $xprt.prod_tab_style == 'general'}
			<!-- box section -->
			<section class="page-product-box">
				<h3 id="#producttab_{$result.id_xprtproducttab}" class="idTabHrefShort page-product-heading">
					<em>{$result.title}</em>
				</h3>
		{/if}

		<div class="{if $xprt.prod_tab_style == 'classic'}tab-pane {/if}" id="producttab_{$result.id_xprtproducttab}">
			{$result.content}
		</div>
		
		{if $xprt.prod_tab_style == 'general'}
			</section>
			<!-- box section end -->
		{/if}

	{/foreach}
{/if}