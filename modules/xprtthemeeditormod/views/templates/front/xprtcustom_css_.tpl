/***********************************************************************************
	        Do not write css to this file. It is auto generated file
*************************************************************************************/



{* General style *}

{if isset($xprt.body_f_size) && !empty($xprt.body_f_size)}
	{$bdyfontsize = $xprt.body_f_size|replace:'px':''}
{else}
	{$bdyfontsize = 13}
{/if}

html {
	font-size: {(62.5 * $bdyfontsize) / 13}%
 } 
body { 
	font-family: "{xprtthemeeditormod::GetGoogleFontsFamily('bodyfont')}", Arial, Helvetica, sans-serif;
	font-size: {$bdyfontsize|replace:'px':''}px;
	color: {$xprt.body_f_color};
	background-color: {$xprt.body_bg_color};
	{if $xprt.enable_body_bg == '1'}
		background-image: url("{$xprt.xprtimageurl}{$xprt.body_bg_image}");
	{elseif $xprt.enable_body_bg == '0'}
		background-image: url("{$xprt.xprtimageurl}{$xprt.body_bg_pattern}");
	{else}
		background-image: none;
	{/if}
	background-repeat: {$xprt.body_bg_repeat};
	background-position: {$xprt.body_bg_position};
	background-size: {$xprt.body_bg_size};
	background-attachment: {$xprt.body_bg_attachment};
 }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     

 h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { 
 	font-family: "{xprtthemeeditormod::GetGoogleFontsFamily('headingfont')}", Arial, Helvetica, sans-serif; 
  } 

 
.block .title_block, .block h4, .footer-block .title_block, .footer-block h4 {
	font-family: "{xprtthemeeditormod::GetGoogleFontsFamily('bodyfont')}", Arial, Helvetica, sans-serif; 
 } 


{* link color *}
a { 
    color: {$xprt.link_color};
 } 


a:hover, a:focus { 
    color: {$xprt.link_hvr_color};
 } 


{* Breadcrumb area *}
{* .page_heading_area, .page_heading_area > .container {
	min-height: {$xprt.breadcrumb_height|replace:'px':''}px;
 }  *}


{* box shadow or full width *}
body.boxed #page { 
	max-width: {$xprt.body_boxed_width + 60}px;
	margin: 0px auto;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
	background-color: {$xprt.page_boxed_bg};
 } 


@media (min-width: 1200px) {
	body.boxed #page .container { 
		max-width: {$xprt.body_boxed_width}px;
	 }
 } 
 

body.full_width #page { 
	max-width: 100%;
	margin: 0px auto;
	box-shadow: none;
 } 

body.full_width #page .columns-container .container { 
	max-width: {$xprt.body_boxed_width}px;
 } 


body.boxed_classic .displayhometopleftright_area > .container, body.boxed_classic:not(.index) .columns-container .columns > .container { 
	max-width: {$xprt.body_boxed_width}px;
	background-color: {$xprt.page_boxed_bg};
 } 



{* sidebar panel display style *}
{if $xprt.sidebar_display_style == 'sidebar_push'}
#page {  
	position: relative;
 } 
{elseif $xprt.sidebar_display_style == 'sidebar_overplay'}
#page {  
	position: static;
 } 
{else}
#page {  
	position: relative;
 } 
{/if}



{* page heading area *}
.page_heading_area {
	background-color: {$xprt.page_headidng_color};
 } 

.page_heading_simple_area {
	background-color: {$xprt.page_headidng_simple_color};
 } 


{* New and sale badge style *}
	
{if $xprt.new_sale_badge_style == 'classic_style'}
	.new-box, .sale-box {
		top:3px;
	 } 
	.new-label {
		color: {$xprt.link_hvr_color};
		background-color: transparent;
		font-family: BenchNine;
	    letter-spacing: 2.5px;
	    font-size: 16px;
	    border-radius:0px;
	    text-transform: uppercase;
	 } 

	.sale-label { 
	    color: #B485AF;
	    background-color: transparent;
		font-family: BenchNine;
	    letter-spacing: 2.5px;
	    font-size: 16px;
	    border-radius:0px;
	    text-transform: uppercase;
	 } 

{elseif $xprt.new_sale_badge_style == 'square_style'}
	.new-box, .sale-box {
		top:0px;
	 } 
	 .new-box {
 		right:0px;
	  }  
	 .sale-box {
 		left:0px;
	  } 
	.sale-label, .new-label {
		border-radius: 0px;
		width: auto;
		height: auto;
		line-height: 1.3;
		padding: 3px 10px;
	 } 

{/if}


{* header banner color *}
header .banner {
	background-color: {$xprt.header_banner_bg};
 } 


{* Main Menu *}
{if $xprt.main_menu_style == 'menu_noborder_style'}
	header .main_menu_area:before {
		display: none;
	 } 
{/if}
 

 header .main_menu_area {
 	background-color: {$xprt.menu_bg_color};
  } 

.block_top_menu ul.sf-menu > li > a {
    color: {$xprt.menu_txt_color};
 } 

.block_top_menu ul.sf-menu > li.sfHoverForce > a, .block_top_menu ul.sf-menu > li > a:hover  {
	color: {$xprt.menu_txt_hvr_color};
 } 


.header-container.header_transparent {
	background-image: url("{$xprt.xprtimageurl}{$xprt.header_top_bg_inner}");
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center top;
	background-attachment: scroll;
 } 









{* Newsletter block *}

.newsletter_block_area { 
	background-color: {$xprt.newsletter_block_bg_color};
	min-height: {$xprt.newsletter_block_height};
	{if ($xprt.enable_newsletter_bg == '1') && !empty($xprt.newsletter_block_bg)}
		background-image: url("{$xprt.xprtimageurl}{$xprt.newsletter_block_bg}");
		background-repeat: no-repeat;
		background-size: cover;
		background-position: center center;
		background-attachment: scroll;
	{/if}
  }  


.footer .block.newsletter_block_footer .block_content .form-group input {
	background-color: {$xprt.ft_mid_newsletter_bg_color};
	border-color: {$xprt.ft_mid_newsletter_bg_color};
 } 
 
 .footer .block.newsletter_block_footer .block_content .form-group button.btn-black {
 	background-color: {$xprt.ft_mid_newsletter_btn_color};
 	    border-color: {$xprt.ft_mid_newsletter_bg_color};
  } 





{* Footer color area *}

{* Footer top full width area background image *}
{* style="background-image:url({$modules_dir}xprtthemeeditormod/images/{$xprt.footer_top_bg});" *}
.footer .footer_top_fullwidth_area { 
	background-color: {$xprt.footer_top_fullwidth_bg_color};
	min-height: {$xprt.footer_top_fullwidth_height};
	{if $xprt.footer_top_fullwidth_bg_on == '1'}
		background-image: url("{$xprt.xprtimageurl}{$xprt.footer_top_fullwidth_bg}");
		background-repeat: no-repeat;
		background-size: cover;
		background-position: center center;
		background-attachment: scroll;
	{elseif $xprt.footer_top_fullwidth_bg_on == '0'}
		background-image: url("{$xprt.xprtpatternsurl}{$xprt.footer_top_fullwidth_pattern}");
		background-repeat: repeat;
	    background-size: contain;
		{* background-position: center center; *}
		background-attachment: scroll;
	{else}
		background-image: none;
	{/if}
 }


{* Footer top area background image *}
.footer .footer_top_area {
	background-color: {$xprt.footer_top_bg_color};
 } 




{* Footer middle area color *}
.footer .footer_middle_area .footer_middle .block h4, .footer .footer_middle_area .footer_middle .block .title_block {
	color: {$xprt.footer_mid_heading_color}
 } 


.footer .footer_middle_area .footer_middle .block h4 a:hover, .footer .footer_middle_area .footer_middle .block .title_block a:hover {
	color: {$xprt.footer_mid_heading_hvr_color}
 } 


.footer .footer_middle_area {
	color: {$xprt.footer_mid_txt_color};
	background-color: {$xprt.footer_mid_bg_color}
 } 

.footer_payment_block .footer_payment_logo ul {
	background-color: {$xprt.footer_mid_bg_color}
  } 

.footer .footer_middle_area a, .footer .footer_middle_area .footer_middle .block .block_content.list-block ul li a {
	color: {$xprt.footer_mid_link_color}
 } 

.footer .footer_middle_area a:hover, .footer .footer_middle_area .footer_middle .block .block_content.list-block ul li a:hover {
	color: {$xprt.footer_mid_link_hvr_color}
 } 

.footer .footer_middle_area .footer_middle .block h4:after, .footer .footer_middle_area .footer_middle .block .title_block:after {
	background-color: {$xprt.footer_mid_bdr_color}
 } 

 .footer .footer_middle_area .tags_block.block .block_content ul li a {
 	border: 1px solid {$xprt.footer_mid_bdr_color};
  } 




{* Footer bottom area color *}
.footer .footer_bottom_area  {
	color: {$xprt.footer_btm_txt_color};
	background-color: {$xprt.footer_btm_bg_color};
 } 

.footer .footer_bottom_area .footer_bottom a  {
	color: {$xprt.footer_btm_link_color};
 } 

.footer .footer_bottom_area .footer_bottom a:hover {
	color: {$xprt.footer_btm_link_hvr_color};
 } 

.footer .footer_bottom_area {
	border-color: {$xprt.footer_btm_bdr_color};
 } 

{$xprt.custom_css}

/***********************************************************************************
	        Do not write css to this file. It is auto generated file
*************************************************************************************/