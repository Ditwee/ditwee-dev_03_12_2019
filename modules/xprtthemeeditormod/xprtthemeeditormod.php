<?php
if (!defined('_PS_VERSION_'))
    exit;
require_once(dirname(__FILE__).'/classes/xprtbreadcrumb.php');
require_once(dirname(__FILE__).'/classes/xprtproducttab.php');
class xprtthemeeditormod extends Module {
    public $themename = 'greatstore';
    public $all_demo;
    public $fields_form;
    public static $shortname = 'xprt';
    public static $quick_key = 'greatstorequickaceslink';
    public static $customcssfilename = 'xprtcustom_css_';
    public $hooks_url = '/hooks/hooks.php';
    public $css_files_url = '/css/css.php';
    public $js_files_url = '/js/js.php';
    public $back_css_files_url = '/css/css_back.php';
    public $back_js_files_url = '/js/js_back.php';
    public $tabs_files_url = '/tabs/tabs.php';
    public $mysql_files_url = '/mysqlquery/mysqlquery.php';
    public $data_url = '/data/data.php';
    public $fields_arr_path = '/data/fields_array.php';
    public $icon_url = '/icon/icon.php';
    public $fonts_files;
    public static $color_group = array(
            'default' => 'Default Schema',
            'black' => 'Black color Schema',
            'footer_white' => 'Footer white',
            'footer_mid_white' => 'Footer middle white',
        );
    public function __construct()
    {
        $this->name = 'xprtthemeeditormod';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Xpert-Idea';
        $this->need_upgrade = true;
        $this->fonts_files = dirname(__FILE__).'/fonts/xprtgoogle_fonts.txt';
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        $this->all_demo = array(
    		'demo_2' => $this->l('Demo One'),
    		'demo_1' => $this->l('Demo Two'),
    		'demo_3' => $this->l('Demo Three'),
    		'demo_4' => $this->l('Demo Four'),
    		'demo_5' => $this->l('Demo Five'),
    		'demo_6' => $this->l('Demo Six'),
    		'demo_7' => $this->l('Demo Seven'),
    		'demo_8' => $this->l('Demo Eight'),
    		'demo_9' => $this->l('Demo Nine'),
    		'demo_10' => $this->l('Demo Ten'),
    		'demo_12' => $this->l('Demo Twelve'),
    	);
            parent::__construct();
        $this->displayName = $this->l('Great Store Theme Admin Panel');
        $this->description = $this->l('The Most Powerfull Presta shop Theme Admin Panel By Xpert-Idea');
    }
    public function install()
    {
        if(!parent::install()
	     ||	!$this->registerHook("actionProductListModifier")
	     ||	!$this->registerHook("actionProductListModifierTop")
         || !$this->Register_SQL()
         || !$this->Register_Tabs()
         || !$this->Register_Config()
         || !$this->Register_Hooks()
         || !$this->AddQuickAccessLink()
         || !$this->xpertsampledata()
         || !$this->DummyData()
        )
            return false;
        self::AddCustomHooks();
        Configuration::updateValue('bodyfont_family','Roboto');
        Configuration::updateValue('bodyfont_subsets','latin');
        Configuration::updateValue('bodyfont_variants','regular,300,400italic,500,700');
        Configuration::updateValue('headingfont_family','BenchNine');
        Configuration::updateValue('headingfont_subsets','latin');
        Configuration::updateValue('headingfont_variants','regular,300,700');
        // Configuration::updateValue('additionalfont_family','Lato');
        // Configuration::updateValue('additionalfont_subsets','latin');
        // Configuration::updateValue('additionalfont_variants','regular,700');
        return true;
    }
    public function uninstall()
    {
        if(!parent::uninstall()
         || !$this->UnRegister_Hooks()
         || !$this->UnInstallSampleData()
         || !$this->UnRegister_Config()
         || !$this->UnRegister_SQL()
         || !$this->UnRegister_Tabs()
        )
            return false;
        Configuration::deleteByName('bodyfont_family');
        Configuration::deleteByName('bodyfont_subsets');
        Configuration::deleteByName('bodyfont_variants');
        Configuration::deleteByName('headingfont_family');
        Configuration::deleteByName('headingfont_subsets');
        Configuration::deleteByName('headingfont_variants');
        // Configuration::deleteByName('additionalfont_family');
        // Configuration::deleteByName('additionalfont_subsets');
        // Configuration::deleteByName('additionalfont_variants');
        return true;
    }
    public function AddQuickAccessLink(){
        $link = new Link();
        $QuickAccess = new QuickAccess();
        $QuickAccess->link = $link->getAdminLink('AdminModules').'&configure='.$this->name;
        $languages = Language::getLanguages(false);
        if(isset($languages) && !empty($languages))
            foreach($languages as $language)
                $QuickAccess->name[$language['id_lang']] = $this->l("Great-Store Theme Admin Settings");
        $QuickAccess->new_window = '0';
        if($QuickAccess->save())
            Configuration::updateValue(self::$quick_key,$QuickAccess->id);
        return true;
    }
    public function hookactionProductListModifierTop($params){
    	$xprtval = $this->AsignGlobalSettingValueTop();
    	$params['xprtval'] = $xprtval;
    }
    public function hookactionProductListModifier($params){
    	$this->AsignGlobalSettingValue();
    }
	public static function AddCustomHooks(){
		$All_Custom_hooks = self::All_Custom_hooks();
		if(isset($All_Custom_hooks) && !empty($All_Custom_hooks)) {
			foreach ($All_Custom_hooks as $hook) {
				$id = Hook::getIdByName($hook);
				if(!$id)
				{
					$hookobj = new Hook();
					$hookobj->name = $hook;
					$hookobj->title = $hook;
					$hookobj->description = $hook;
					$hookobj->position = 1;
					$hookobj->live_edit  = 0;
					$hookobj->add();
				}
			}	
		}
	  return true;
	}
    public static function All_Custom_hooks(){
    	return array(
            'displayCatTop',
            'displaySortPagiBar',
            'displayCatBottom',
            'displayFooterTopFullwidth',
            'displayFooterTop',
            'displayFooterBottom',
            'displaySidebarPanel',
            'displaySidebarPanelCartBlock',
            'displayTopLeft',
            'displayTopRightOne',
            'displayTopRightTwo',
            'displayMainMenu',
            'displayHomeTop',
            'displayHomeFullWidthMiddle',
            'displayHomeMiddle',
            'displayHomeFullWidthBottom',
            'displayHomeBottom',
            'displayHomeTopLeft',
            'displayHomeTopLeftOne',
            'displayHomeTopLeftTwo',
            'displayHomeTopRight',
            'displayHomeTopRightOne',
            'displayHomeTopRightTwo',
            'displayHomeTopLeftRightBottom',
            'displayHomeBottomLeft',
            'displayHomeBottomLeftOne',
            'displayHomeBottomLeftTwo',
            'displayHomeBottomRight',
            'displayHomeBottomRightOne',
            'displayHomeBottomRightTwo',
            'displayProductPriceBlock',
            'displayBlockNewsletterBottom',
            'displayNewsletterSocialBlock',
            'displayHeaderNavIcon',
            'displayLayerCartBottom',
            'displayFooterLogoContact',
            'displayLogoContact',
            'displayxprtblocknewsletterBottom',
            'productgridmultiimgs',
            'displayProductListFunctionalButtons',
            'displayProductListReviews',
            'displayProductRightSidebar',
            'displaynextprev',
            'displaycentercolumnproduct',
        );
    }
    public function Register_Config()
    {
        Configuration::updateValue('xprtshortname',self::$shortname);
        //     $data = array();
        //     require_once(dirname(__FILE__).$this->data_url);
        // if(isset($data) && !empty($data))
        //     foreach($data as $data_key => $data_val):
        //         Configuration::updateValue($data_key,$data_val);
        //     endforeach;
            return true;
        // return false;
    }
    public function UnRegister_Config()
    {
        Configuration::deleteByName(self::$quick_key);
        //     $data = array();
        //     require_once(dirname(__FILE__).$this->data_url);
        // if(isset($data) && !empty($data))
        //     foreach($data as $data_key => $data_val):
        //         Configuration::deleteByName($data_key);
        //     endforeach;
            return true;
        // return false;
    }
    public function Register_Hooks()
    {
        $hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
            foreach($hooks as $hook):
                $this->registerHook($hook);
            endforeach;
            return true;
        return false;
    }
    public function UnRegister_Hooks()
    {
        $hooks = array();
        require_once(dirname(__FILE__).$this->hooks_url);
        if(isset($hooks) && !empty($hooks))
            foreach($hooks as $hook):
                    $hook_id = Module::getModuleIdByName($hook);
                if(isset($hook_id) && !empty($hook_id))
                    $this->unregisterHook((int)$hook_id);
            endforeach;
            return true;
        return false;
    }
    public function Register_Css()
    {
        $css_files = array();
        require_once(dirname(__FILE__).$this->css_files_url);
        if(isset($css_files) && !empty($css_files))
            foreach($css_files as $css_file):
                $this->context->controller->addCSS($css_file);
            endforeach;
        $generatecssfilepath = _PS_MODULE_DIR_.$this->name.'/css/';
        $id_shop = $this->context->shop->id;
        $generatecssfilename = $generatecssfilepath.self::$customcssfilename.$id_shop.'.css';
        $this->context->controller->addCSS($generatecssfilename);
            return true;
        return false;
    }
    public function Register_Js()
    {
        $js_files = array();
        require_once(dirname(__FILE__).$this->js_files_url);
        if(isset($js_files) && !empty($js_files))
            foreach($js_files as $js_file):
                $this->context->controller->addJS($js_file);
            endforeach;
            return true;
        return false;
    }
    public function Register_SQL()
    {
        $mysqlquery = array();
            require_once(dirname(__FILE__).$this->mysql_files_url);
            if(isset($mysqlquery) && !empty($mysqlquery))
                foreach($mysqlquery as $query){
                    if(!Db::getInstance()->Execute($query,false))
                        return false;
                }
        return true;
    }
    public function UnRegister_SQL()
    {
        $mysqlquery_u = array();
            require_once(dirname(__FILE__).$this->mysql_files_url);
            if(isset($mysqlquery_u) && !empty($mysqlquery_u))
                foreach($mysqlquery_u as $query_u){
                    if(!Db::getInstance()->Execute($query_u,false))
                        return false;
                }
        return true;
    }
    public function UnRegister_Tabs()
    {
        $tabs_lists = array();
        require_once(dirname(__FILE__) .$this->tabs_files_url);
        if(isset($tabs_lists) && !empty($tabs_lists)){
        	foreach($tabs_lists as $tab_list){
        	    $tab_list_id = Tab::getIdFromClassName($tab_list['class_name']);
        	    if(isset($tab_list_id) && !empty($tab_list_id)){
        	        $tabobj = new Tab($tab_list_id);
        	        $tabobj->delete();
        	    }
        	}
        } 
        $save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
        if($save_tab_id != 0){
        	$count = Tab::getNbTabs($save_tab_id);
        	if($count == 0){
        		if(isset($save_tab_id) && !empty($save_tab_id)){
        		    $tabobjs = new Tab($save_tab_id);
        		    $tabobjs->delete();
        		}
        	}
        }
        return true;
    }
    public function RegisterParentTabs(){
    	$langs = Language::getLanguages();
    	$save_tab_id = (int)Tab::getIdFromClassName("Adminxprtdashboard");
    	if($save_tab_id != 0){
    		return $save_tab_id;
    	}else{
    		$tab_listobj = new Tab();
    		$tab_listobj->class_name = 'Adminxprtdashboard';
    		$tab_listobj->id_parent = 0;
    		$tab_listobj->module = $this->name;
    		foreach($langs as $l)
    		{
    		    $tab_listobj->name[$l['id_lang']] = $this->l("Theme Settings");
    		}
    		if($tab_listobj->save())
    			return (int)$tab_listobj->id;
    		else
    			return (int)$save_tab_id;
    	}
    }
    public function Register_Tabs()
    {
        $tabs_lists = array();
        $langs = Language::getLanguages();
        $id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $save_tab_id = $this->RegisterParentTabs();
            require_once(dirname(__FILE__) .$this->tabs_files_url);
            if(isset($tabs_lists) && !empty($tabs_lists))
	            foreach ($tabs_lists as $tab_list)
	            {
	                $tab_listobj = new Tab();
	                $tab_listobj->class_name = $tab_list['class_name'];
	                if($tab_list['id_parent'] == 'parent'){
	                    $tab_listobj->id_parent = $save_tab_id;
	                }else{
	                    $tab_listobj->id_parent = $tab_list['id_parent'];
	                }
	                if(isset($tab_list['module']) && !empty($tab_list['module'])){
	                    $tab_listobj->module = $tab_list['module'];
	                }else{
	                    $tab_listobj->module = $this->name;
	                }
	                foreach($langs as $l)
	                {
	                    $tab_listobj->name[$l['id_lang']] = $this->l($tab_list['name']);
	                }
	                $tab_listobj->save();
	            }
        return true;
    }
    public function GenerateCustomCss()
    {
        $this->AsignGlobalSettingValue();
        $context = Context::getContext();
        $generatecssfilepath = _PS_MODULE_DIR_.$this->name.'/css/';
        $id_shop = $context->shop->id;
        $generatecssfilename = $generatecssfilepath.self::$customcssfilename.$id_shop.'.css';
        $custom_css = $context->smarty->fetch(_PS_MODULE_DIR_.$this->name."/views/templates/front/xprtcustom_css_.tpl");
        file_put_contents($generatecssfilename,$custom_css);
        @chmod($generatecssfilename,0777);
    }
    public function getContent()
    {
    	// $this->registerHook("actionProductListModifierTop");
        Configuration::updateValue('xprtshortname',self::$shortname);
        $html = '';
        $multiple_arr = array();
        // START RENDER FIELDS
        if(!$this->fields_form){
        	$this->AllFields();
        }
        // END RENDER FIELDS
        // START DEMO SETUP
        if($selecteddemo = Tools::getValue('select_demo_ready')){
        	// $selecteddemo = Tools::getValue('select_demo_ready');
        	Configuration::updateValue('xprt_demo_number',$selecteddemo);
        	$this->selecteddemosetup($selecteddemo);
        	$this->GenerateCustomCss();
        	$html .= $this->displayConfirmation($this->l('Successfully Setup Your Demo: '.$this->all_demo[$selecteddemo].'. Please ReGenerate your Image for better looking your shop. '));
        }
        // END DEMO SETUP
        if(Tools::isSubmit('save'.$this->name)){
            foreach($this->fields_form as $key => $value){
                $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
            }
            // START LANG
            if(isset($multiple_arr) && !empty($multiple_arr)){
                foreach($multiple_arr as $mvalue){
                    if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
                       $languages = Language::getLanguages(false);
                       foreach($languages as $lang){
                        ${$mvalue['name'].'_lang'}[$lang['id_lang']] = Tools::getvalue($mvalue['name'].'_'.$lang['id_lang']);
                       }
                    }
                }
            }
            // END LANG
            if(isset($multiple_arr) && !empty($multiple_arr)){
                foreach($multiple_arr as $mvalue){
                    if(isset($mvalue['type']) && ($mvalue['type'] == "googlefont")){
                        $this->SaveGoogleFonts($mvalue['name']);
                    }else{
                        if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
                                Configuration::updateValue(self::$shortname.$mvalue['name'],${$mvalue['name'].'_lang'});
                        }else{
                            if(isset($mvalue['name'])){
                                Configuration::updateValue(self::$shortname.$mvalue['name'],Tools::getvalue($mvalue['name']));
                            }
                        }
                    }
                }
            }
            $helper = $this->SettingForm();
            $html_form = $helper->generateForm($this->fields_form);
            $html .= $this->displayConfirmation($this->l('Successfully Saved All Fields Values.'));
            $this->GenerateCustomCss();
            $html .= $html_form;
            Configuration::updateValue(self::$shortname."color_group_name_sel",Tools::getValue('color_group_name_sel'));
        }else{
            $helper = $this->SettingForm();
            $html_form = $helper->generateForm($this->fields_form);
            $html .= $html_form;
        }
        return $html;
    }
    public function SettingForm() {
        $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
        // START RENDER FIELDS
        if(!$this->fields_form){
        	$this->AllFields();
        }
        // END RENDER FIELDS
        $helper = new HelperForm();

        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        foreach (Language::getLanguages(false) as $lang)
                $helper->languages[] = array(
                        'id_lang' => $lang['id_lang'],
                        'iso_code' => $lang['iso_code'],
                        'name' => $lang['name'],
                        'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
                );
        $helper->toolbar_btn = array(
            'save' =>
            array(
                'desc' => $this->l('Save'),
                'class' => "btn btn-default pull-right",
                'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save'.$this->name.'token=' . Tools::getAdminTokenLite('AdminModules'),
            )
        );
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'save'.$this->name;
        $multiple_arr = array();
        foreach($this->fields_form as $key => $value) {
            $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
        }
        foreach($multiple_arr as $mvalue){
            if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
               $languages = Language::getLanguages(false);
               foreach($languages as $lang){
                    $helper->fields_value[$mvalue['name']][$lang['id_lang']] = Configuration::get(self::$shortname.$mvalue['name'],$lang['id_lang']);
               }
            }else{
                if(isset($mvalue['name'])){
                    $helper->fields_value[$mvalue['name']] = Configuration::get(self::$shortname.$mvalue['name']);
                }
            }
        }
        //START CUSTOMR
        $rand_time = time();
        $admin_token = Tools::getAdminTokenLite('Adminxprtproducttab');
        $image_path = _PS_MODULE_DIR_.$this->name.'/images/'.$rand_time.'_img.jpg';
        $image_url = _MODULE_DIR_.$this->name.'/images/'.$rand_time.'_img.jpg';
        $upload_url = $this->context->link->getAdminLink('Adminxprtproducttab').'&ajax=1&action=UploadImages&rand_time='.$rand_time.'&type_mod_name='.$this->name;
        $color_group_name = Configuration::get(self::$shortname."color_group_name");
        $color_group_name_sel = Configuration::get(self::$shortname."color_group_name_sel");
        $theme_dir = $this->context->shop->theme_directory;
        $mod_name = $this->name;
        $icon_list = $this->GetIcons();
		$xprt_demo_number = Configuration::get("xprt_demo_number");
            // start google fonts
        $helper->tpl_vars = array(
            'type_image_path' => $image_path,
            'theme_all_demo' => $this->all_demo,
            'xprt_demo_number' => $xprt_demo_number,
            'type_image_url' => $image_url,
            'type_rand_time' => $rand_time,
            'type_admin_token' => $admin_token,
            'type_mod_name' => $this->name,
            'icon_list' => $icon_list,
            'type_patterns' => $this->GetAllPattern(),
            'type_bgimages' => $this->GetAllBGImage(),
            'xprtbgimagesurl' => Context::getContext()->shop->getBaseURL().'modules/'.$mod_name.'/images/',
            'xprtbgpatternurl' => Context::getContext()->shop->getBaseURL().'themes/'.$theme_dir.'/img/patterns/',
            'type_upload_url' => $upload_url,
            'module_name' => $this->name,
            'color_group' => self::$color_group,
            'color_group_name' => $color_group_name,
            'color_group_name_sel' => $color_group_name_sel,
            'gets_fonts_family' => $this->gets_fonts_family(),
            'gets_fonts_variants' => $this->gets_fonts_variants('ABeeZee'),
            'gets_fonts_subsets' => $this->gets_fonts_subsets('ABeeZee')
        );
        if($this->assigngFontsValues()){
            foreach($this->assigngFontsValues() as $key => $value){
                $helper->tpl_vars[$key] = $value;
            }
        }
            // end google fonts
        //END CUSTOMR
        return $helper;
    }
    public function GetIcons()
	{
		$icon_list = array();
		if(file_exists(dirname(__FILE__).$this->icon_url))
			require_once(dirname(__FILE__).$this->icon_url);
        return $icon_list;
	}
    // public function InstallSampleData()
    // {
    //     $multiple_arr = array();
    //     $this->AllFields();
    //     foreach($this->fields_form as $key => $value){
    //         $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
    //     }
    //     // START LANG
    //     if(isset($multiple_arr) && !empty($multiple_arr)){
    //         foreach($multiple_arr as $mvalue){
    //             if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
    //                $languages = Language::getLanguages(false);
    //                foreach($languages as $lang){
    //                 ${$mvalue['name'].'_lang'}[$lang['id_lang']] = $mvalue['default_val'];
    //                }
    //             }
    //         }
    //     }
    //     // END LANG
    //     if(isset($multiple_arr) && !empty($multiple_arr)){
    //         foreach($multiple_arr as $mvalue){
    //             if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
    //                 Configuration::updateValue(self::$shortname.$mvalue['name'],${$mvalue['name'].'_lang'});
    //             }else{
    //                 if(isset($mvalue['name'])){
    //                     Configuration::updateValue(self::$shortname.$mvalue['name'],$mvalue['default_val']);
    //                 }
    //             }
    //         }
    //     }
    //     return true;
    // }
    // public function UnInstallSampleData()
    // {
    //     $multiple_arr = array();
    //     $this->AllFields();
    //     foreach($this->fields_form as $key => $value){
    //         $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
    //     }
    //     if(isset($multiple_arr) && !empty($multiple_arr)){
    //         foreach($multiple_arr as $mvalue){
    //             if(isset($mvalue['name'])){
    //                 Configuration::deleteByName(self::$shortname.$mvalue['name']);
    //             }
    //         }
    //     }
    //     return true;
    // }
    public function GetAllPattern()
    {
        $theme_dir = $this->context->shop->theme_directory;
        $pattern_path = _PS_THEME_DIR_.'img/patterns/';
        // $pattern_images_url = Context::getContext()->shop->getBaseURL() . 'themes/'.$theme_dir.'/img/patterns/';
        $pattern_images_url = Context::getContext()->shop->getBaseURL() . 'themes/'.$theme_dir.'/img/patterns/';
        $pattern_images = array();
        if(is_dir($pattern_path)){
            if($pattern_images_dir = opendir($pattern_path)) {
                while(($pattern_images_file = readdir($pattern_images_dir)) !== false){
                    if(stristr($pattern_images_file, ".png") !== false || stristr($pattern_images_file,".jpg") !== false){
                        $pattern_images[] = $pattern_images_file;
                        // $pattern_images[] = $pattern_images_url.$pattern_images_file;
                    }
                }
            }
        }
        return $pattern_images;
    }
    public function GetAllBGImage()
    {
        $mod_name = $this->name;
        $theme_dir = $this->context->shop->theme_directory;
        $pattern_path = _PS_MODULE_DIR_ . $mod_name.'/images/';
        $pattern_images_url = Context::getContext()->shop->getBaseURL() . 'modules/'.$mod_name.'/images/';
        // $pattern_images_url =  'modules/'.$mod_name.'/images/';
        $pattern_images = array();
        if (is_dir($pattern_path)){
            if ($pattern_images_dir = opendir($pattern_path)){
                while (($pattern_images_file = readdir($pattern_images_dir)) !== false){
                    if (stristr($pattern_images_file, ".png") !== false || stristr($pattern_images_file, ".jpg") !== false) {
                        // $pattern_images[] = $pattern_images_url.$pattern_images_file;
                        $pattern_images[] = $pattern_images_file;
                    }
                }
            }
        }
        return $pattern_images;
    }
    public function AllFields()
    {
        include_once(dirname(__FILE__).$this->fields_arr_path);
        return $this->fields_form;
    }
    public function AsignGlobalSettingValue()
    {
        $id_lang = Context::getcontext()->language->id;
        $multiple_arr = array();
        $xprt = array();
        $theme_dir = $this->context->shop->theme_directory;
        $mod_name = $this->name;
        $xprt['xprtpatternsurl'] = Context::getContext()->shop->getBaseURL() . 'themes/'.$theme_dir.'/img/patterns/';
        $xprt['xprtimageurl'] = Context::getContext()->shop->getBaseURL() . 'modules/'.$mod_name.'/images/';
        if(!$this->fields_form){
        	$this->AllFields();
        }
        foreach($this->fields_form as $key => $value){
            $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
        }
        if(isset($multiple_arr) && !empty($multiple_arr)){
            foreach($multiple_arr as $mvalue){
                if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
                    $xprt[$mvalue['name']] = Configuration::get(self::$shortname.$mvalue['name'],$id_lang);
                }else{
                    if(isset($mvalue['name'])){
                        $xprt[$mvalue['name']] = Configuration::get(self::$shortname.$mvalue['name']);
                    }
                }
            }
        }
        $xprtlink = new Link();
        // $xprtlink->url = '';
        $this->smarty->assignGlobal('xprtlink',$xprtlink);
        $this->smarty->assignGlobal('xprt',$xprt);
        return false;
    }
    public function AsignGlobalSettingValueTop()
    {
        $id_lang = Context::getcontext()->language->id;
        $multiple_arr = array();
        $xprt = array();
        $theme_dir = $this->context->shop->theme_directory;
        $mod_name = $this->name;
        $xprt['xprtpatternsurl'] = Context::getContext()->shop->getBaseURL() . 'themes/'.$theme_dir.'/img/patterns/';
        $xprt['xprtimageurl'] = Context::getContext()->shop->getBaseURL() . 'modules/'.$mod_name.'/images/';
        if(!$this->fields_form){
        	$this->AllFields();
        }
        foreach($this->fields_form as $key => $value){
            $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
        }
        if(isset($multiple_arr) && !empty($multiple_arr)){
            foreach($multiple_arr as $mvalue){
                if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
                    $xprt[$mvalue['name']] = Configuration::get(self::$shortname.$mvalue['name'],$id_lang);
                }else{
                    if(isset($mvalue['name'])){
                        $xprt[$mvalue['name']] = Configuration::get(self::$shortname.$mvalue['name']);
                    }
                }
            }
        }
        return $xprt;
    }
    public function hookDisplayHeader($params)
    {
        $this->Register_Css();
        $this->Register_Js();
        $this->AsignGlobalSettingValue();
    }
    public function hookDisplayleftColumn($params)
    {
        $this->AsignGlobalSettingValue();
    }
    public function hookdisplaythemecolorlist($params)
    {
        if(isset($params['product_id']) && !empty($params['product_id'])){
            $products = array();
            $id_product = $params['product_id'];
            $products[] = $id_product;
            $colors = Product::getAttributesColorList($products);
            $colors_list = $colors[$id_product];
            $this->context->smarty->assign(array(
                       'colors_list' => $colors_list,
                       'id_product' => $id_product,
                       'link' => Context::getContext()->link,
                       'img_col_dir' => _THEME_COL_DIR_,
                        'col_img_dir' => _PS_COL_IMG_DIR_
                        ));
        }
        return $this->display(__FILE__, 'views/templates/front/product-list-colors.tpl');
    }
    public function hookDisplayrightColumn($params)
    {
        $this->AsignGlobalSettingValue();
    }
    public function hookDisplayTop($params)
    {
        $this->AsignGlobalSettingValue();
    }
    public function hookDisplayTopColumn($params)
    {
        $this->AsignGlobalSettingValue();
    }
    public function hookDisplaynav($params)
    {
        $this->AsignGlobalSettingValue();
    }
    public function hookDisplayhome($params)
    {
        $this->AsignGlobalSettingValue();
    }
    public function hookDisplayfooter($params)
    {
        $this->AsignGlobalSettingValue();
    }
    public function hookdisplayFooterLogoContact($params)
    {
        $this->AsignGlobalSettingValue();
    }
    public function hookdisplayBackOfficeHeader()
    {
        if((Tools::getValue('controller') == 'AdminModules')
            && (Tools::getValue('configure') == $this->name)){
             $back_css = $this->Register_Back_Css();
             $back_js = $this->Register_Back_Js();
             $this->context->smarty->assign(array(
                'back_css' => $back_css,
                'back_js' => $back_js
             ));
            return $this->display(__FILE__, 'views/templates/admin/displaybackofficeheader.tpl');
        }
    }
    public function Register_Back_Css()
    {
        $back_css_files = array();
        require_once(dirname(__FILE__).$this->back_css_files_url);
        if(isset($back_css_files) && !empty($back_css_files))
            return $back_css_files;
    }
    public function Register_Back_Js()
    {
        $back_js_files = array();
        require_once(dirname(__FILE__).$this->back_js_files_url);
        if(isset($back_js_files) && !empty($back_js_files))
                return $back_js_files;
    }
    public function hookproductgridmultiimgs($params)
    {
        $results = array();
        $link = Context::getcontext()->link;
        $product = $params['product'];
        $id_product = $product['id_product'];
        $sql = 'SELECT i.id_image FROM '._DB_PREFIX_.'image i INNER JOIN '._DB_PREFIX_.'image_shop
         iss ON i.id_image = iss.id_image AND iss.id_shop = '.(int)Context::getcontext()->shop->id.'
                WHERE i.id_product='.$id_product.' ORDER BY i.position ASC ';
        if($params['count'] == 'true'){
            $sql .= ' LIMIT 2';
        }
        if(!$posts = Db::getInstance()->executeS($sql))
            return false;
       foreach($posts as $post){
           $results[] = $link->getImageLink($product['link_rewrite'], $post['id_image'],$params['img_type']);
       }
       return implode(",",$results);
    }
    public function hookdisplaybreadcrumb($params)
    {
        $page = isset(Context::getcontext()->controller->php_self) ? Context::getcontext()->controller->php_self : 'index';
        $page_title = '';
        if($page == 'product')
        {
            // START Product
            $id_product = Tools::getvalue('id_product');
            $page_title = xprtbreadcrumb::GetProductTitle($id_product);
            // END Product
        }elseif($page == 'category')
        {
            // START category
            $id_category = Tools::getvalue('id_category');
            $page_title = xprtbreadcrumb::GetCategoryTitle($id_category);
            // END category
        }elseif($page == 'manufacturer')
        {
            // START manufacturer
            $id_manufacturer = Tools::getvalue('id_manufacturer');
            $page_title = xprtbreadcrumb::GetManufacturerTitle($id_manufacturer);
            // END manufacturer
        }elseif($page == 'cms')
        {
            // START cms
            $id_cms = Tools::getvalue('id_cms');
            $page_title = xprtbreadcrumb::GetCMSTitle($id_cms);
            // END cms
        }elseif($page == 'supplier')
        {
            // START supplier
            $id_supplier = Tools::getvalue('id_supplier');
            $page_title = xprtbreadcrumb::GetSupplierTitle($id_supplier);
            // END supplier
        }elseif($page == 'module-smartblog-archive')
        {
            // START Product
            $year = Tools::getvalue('year');
            $month = Tools::getvalue('month');
            $page_title = xprtbreadcrumb::GetBlogArchiveTitle($month,$year);
            // END Product
        }elseif($page == 'module-smartblog-category')
        {
            // START Product
            $id_category = Tools::getvalue('id_category');
            $page_title = xprtbreadcrumb::GetBlogCategoryTitle($id_category);
            // END Product
        }elseif($page == 'module-smartblog-search')
        {
            // START Product
            $page_title = xprtbreadcrumb::GetBlogSearchTitle();
            // END Product
        }elseif($page == 'module-smartblog-details')
        {
            // START Product
            $id_post = Tools::getValue('id_post');
            $page_title = xprtbreadcrumb::GetBlogDetailsTitle($id_post);
            // END Product
        }elseif($page == 'module-smartblog-tagpost')
        {
            // START Product
            $page_title = xprtbreadcrumb::GetBlogTagpostTitle();
            // END Product
        }else{
            // START Product
            $page_title = xprtbreadcrumb::GetAnotherPageTitle($page);
            // END Product
        }
        $this->context->smarty->assign(array(
         'page_title' => $page_title
          ));
        return $this->display(__FILE__, 'views/templates/front/displaybreadcrumb.tpl');
    }
    public static function GetSuppliers($id_product = NULL){
        if($id_product == NULL)
            return false;
        $sql = "SELECT `id_supplier` FROM `ps_product_supplier` WHERE `id_product` = ".$id_product." GROUP BY `id_supplier`";
        $res = Db::getInstance()->executeS($sql);
        return $res;
    }
    public function hookdisplaysupplierimages($params)
    {
        if(isset($params['image_type']) && !empty($params['image_type'])){
            $image_type = '-'.$params['image_type'];
            $image_typeno = '-default-'.$params['image_type'];
        }
        else{
            $image_type = '';
            $image_typeno = '';
        }
        $suppliers = array();
        $sup_no_img_dir = _PS_SUPP_IMG_DIR_.$this->context->language->iso_code.$image_typeno.'.jpg';
        if(file_exists($sup_no_img_dir))
            $sup_no_img = _THEME_SUP_DIR_.$this->context->language->iso_code.$image_typeno.'.jpg';
        else
            $sup_no_img = _THEME_SUP_DIR_.'en'.$image_typeno.'.jpg';
        foreach(self::GetSuppliers($params['id_product']) as $supplier){
            $sup_img = _THEME_SUP_DIR_.$supplier['id_supplier'].$image_type.'.jpg';
            $sup_img_dir = _PS_SUPP_IMG_DIR_.$supplier['id_supplier'].$image_type.'.jpg';
            $suppliers[]['image'] = file_exists($sup_img_dir) ? $sup_img : $sup_no_img;
        }
        $this->context->smarty->assign(array(
            'suppliers' => $suppliers
        ));
        return $this->display(__FILE__,'views/templates/front/displaysupplierimages.tpl');
    }
    public static function GetProductsByPRD($id_product = NULL)
    {
        if($id_product == NULL)
            return false;
        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`, product_attribute_shop.id_product_attribute,
            pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`,
            pl.`name`, image_shop.`id_image`, il.`legend`, m.`name` AS manufacturer_name
        FROM `' . _DB_PREFIX_ . 'product` p
        ' . Shop::addSqlAssociation('product', 'p') . '
        LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute pa ON (pa.id_product = p.id_product)
        ' . Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.default_on=1') . '
        ' . Product::sqlStock('p', 0, false) . '
        LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
            p.`id_product` = pl.`id_product`
            AND pl.`id_lang` = ' . (int) Context::getContext()->language->id . Shop::addSqlRestrictionOnLang('pl') . '
        )
        LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_product` = p.`id_product`)' .
                Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1') . '
        LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int) Context::getContext()->language->id . ')
        LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
        WHERE  p.`id_product` = '.$id_product.'
                        AND product_shop.`active` = 1
        AND product_shop.`show_price` = 1
        AND ((image_shop.id_image IS NOT NULL OR i.id_image IS NULL) OR (image_shop.id_image IS NULL AND i.cover=1))
        AND (pa.id_product_attribute IS NULL OR product_attribute_shop.default_on = 1)';
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if(isset($result) && !empty($result)){
            $products = Product::getProductsProperties((int) Context::getContext()->language->id,$result);
            if(isset($products[0]) && !empty($products[0])){
                return $products[0];
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    public function hookdisplaynextprev($params){
        if(!isset($params['id_product']) || empty($params['id_product']))
            return false;
        $id_product = $params['id_product'];
        $prevquery= "SELECT `id_product` FROM `"._DB_PREFIX_."product` WHERE `active` = 1 AND `id_product` < ".$id_product." ORDER BY `id_product` DESC LIMIT 1";
        $nextquery= "SELECT `id_product` FROM `"._DB_PREFIX_."product` WHERE `active` = 1 AND `id_product` > ".$id_product." ORDER BY `id_product` ASC LIMIT 1";
        $id_product = (int)$params['id_product'];
        $prevresult = Db::getInstance()->executeS($prevquery);
        $nextresult = Db::getInstance()->executeS($nextquery);
        if(isset($prevresult[0]['id_product']) && !empty($prevresult[0]['id_product'])){
            $this->context->smarty->assign(array(
                'prev_product' => self::GetProductsByPRD($prevresult[0]['id_product'])
            ));
        }
        if(isset($nextresult[0]['id_product']) && !empty($nextresult[0]['id_product'])){
            $this->context->smarty->assign(array(
                'next_product' => self::GetProductsByPRD($nextresult[0]['id_product'])
            ));
        }
        return $this->display(__FILE__,'views/templates/front/displaynextprev.tpl');
    }
    public function hookdisplaywishlistcount()
    {
        if(!Module::isInstalled('blockwishlist') || !Module::isEnabled('blockwishlist'))
            return false;
        $count = 0;
        $id_wishlist = (int)$this->context->cookie->id_wishlist;
        $sql = "SELECT count(*) as count FROM `"._DB_PREFIX_."wishlist_product` WHERE `id_wishlist` = ".$id_wishlist;
        if($count = Db::getInstance()->getRow($sql)){
            $count = $count['count'];
        }else{
            $count = $count;
        }
        $this->context->smarty->assign(array(
                'count' => $count
            ));
        return $this->display(__FILE__,'views/templates/front/bottomcountwishlist.tpl');
    }
    public static function PageException($exceptions = NULL)
    {
        if($exceptions == NULL)
            return false;
        $exceptions = explode(",",$exceptions);
        $page_name = 'product';
        $this_arr = array();
        $this_arr[] = 'all';
        $this_arr[] = $page_name;
        if($page_name == 'product'){
            $id_product = Tools::getvalue('id_product');
            $this_arr[] = 'product_'.$id_product;
            // Start Get Product Category
            $prd_cat_sql = 'SELECT cp.`id_category` AS id
                FROM `'._DB_PREFIX_.'category_product` cp
                LEFT JOIN `'._DB_PREFIX_.'category` c ON (c.id_category = cp.id_category)
                '.Shop::addSqlAssociation('category', 'c').'
                WHERE cp.`id_product` = '.(int)$id_product;
            $prd_catresults = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($prd_cat_sql);
            if(isset($prd_catresults) && !empty($prd_catresults))
            {
                foreach($prd_catresults as $prd_catresult)
                {
                    $this_arr[] = 'category_'.$prd_catresult['id'];
                }
            }
            // END Get Product Category
        }
        if(isset($this_arr)){
            foreach ($this_arr as $this_arr_val) {
                if(in_array($this_arr_val,$exceptions))
                    return true;
            }
        }
        return false;
    }
    public function hookexecute()
    {
        $return  = array();
        $obj = new xprtproducttab();
        $results = $obj->GetTabData();
        if(isset($results) && !empty($results)){
            foreach ($results as $val){
                if(isset($val['selection']) && self::PageException($val['selection'])){
                    $return[] = $val;
                }
            }
        }
        return $return;
    }
    public function hookdisplayProductTab()
    {
        $results = $this->hookexecute();
        $this->context->smarty->assign(array('results' => $results));
        return $this->display(__FILE__, 'views/templates/front/producttab.tpl');
    }
    public function hookdisplayProductTabContent()
    {
        $results = $this->hookexecute();
        $this->context->smarty->assign(array('results' => $results));
        return $this->display(__FILE__, 'views/templates/front/producttabcontent.tpl');
    }
    public function hookdisplaybgpattern($params)
    {
        $id_category = 1;
        $image_path_exists = false;
        $admin_token = Tools::getAdminTokenLite('Adminxprtcategoryextraimg');
        $image_path = _PS_MODULE_DIR_.$this->name.'/images/'.$id_category.'_extrathumb.jpg';
        $image_url = _MODULE_DIR_.$this->name.'/images/'.$id_category.'_extrathumb.jpg';
        if(file_exists($image_path))
            $image_path_exists = true;
        $upload_url = $this->context->link->getAdminLink('Adminxprtcategoryextraimg').'&ajax=1&action=UploadExtraimages&id_category='.$id_category;
        $this->smarty->assign('image_path_exists',$image_path_exists);
        $this->smarty->assign('image_path',$image_path);
        $this->smarty->assign('image_url',$image_url);
        $this->smarty->assign('id_category',$id_category);
        $this->smarty->assign('admin_token',$admin_token);
        $this->smarty->assign('upload_url',$upload_url);
        return $this->display(__FILE__, 'views/templates/admin/xprtcategoryextraimg_admin.tpl');
    }
    //START GOOGLE FONTS
    public function assigngFontsValues()
    {
        $multiple_arr = array();
        $return_arr = array();
        if(!$this->fields_form){
        	$this->AllFields();
        }
        if(isset($this->fields_form) && !empty($this->fields_form)){
            foreach($this->fields_form as $key => $value){
                $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
            }
        }
        if(isset($multiple_arr) && !empty($multiple_arr)){
            foreach($multiple_arr as $mvalue){
                if(isset($mvalue['type']) && $mvalue['type'] == "googlefont"){
                    $return_arr[$mvalue['name']."_family"] = Configuration::get($mvalue['name']."_family");
                    $font_variants = Configuration::get($mvalue['name']."_variants");
                    if(isset($font_variants))
                        $return_arr[$mvalue['name']."_variants"] = explode(",",$font_variants);
                    else
                        $return_arr[$mvalue['name']."_variants"] = "";
                    $font_subsets = Configuration::get($mvalue['name']."_subsets");
                    if(isset($font_subsets))
                        $return_arr[$mvalue['name']."_subsets"] = explode(",",$font_subsets);
                    else
                        $return_arr[$mvalue['name']."_subsets"] = "";
                }
            }
        }
        return $return_arr;
    }
    public function gets_google_fonts()
    {
        $all_fonts = array();
        $new_fonts = array();
        $fonts = Tools::file_get_contents($filename = 'https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyD_6TR2RyX2VRf8bABDRXCcVqdMXB5FQvs');
        $fonts = Tools::jsonDecode($fonts);
        $google_fonts = (array)$fonts->items;
        if(isset($google_fonts)){
            foreach($google_fonts as $font){
                $new_fonts['kind'] = $font->kind;
                $new_fonts['family'] = $font->family;
                $new_fonts['category'] = $font->category;
                $new_fonts['variants'] = $font->variants;
                $new_fonts['subsets'] = $font->subsets;
                $new_fonts['version'] = $font->version;
                $new_fonts['lastModified'] = $font->lastModified;
                $new_fonts['files'] = $font->files;
                array_push($all_fonts,$new_fonts);
            }
        }
        $all_fonts = Tools::jsonEncode($all_fonts);
        @file_put_contents($this->fonts_files,$all_fonts);
    }
    public function gets_fonts_family()
    {
        $fonts_family = array();
        $all_fonts = Tools::file_get_contents($this->fonts_files);
        $all_fonts = Tools::jsonDecode($all_fonts);
        if(isset($all_fonts) && !empty($all_fonts)){
            $i= 0;
            foreach($all_fonts as $all_font){
                $fonts_family[$i]['id'] = $all_font->family;
                $fonts_family[$i]['name'] = $all_font->family;
                $i++;
            }
        }
        return $fonts_family;
    }
    public function gets_fonts_variants($family = 'ABeeZee')
    {
        if(!isset($family) && empty($family))
            return false;
        $fonts_variants = array();
        $all_fonts = Tools::file_get_contents($this->fonts_files);
        $all_fonts = Tools::jsonDecode($all_fonts);
        if(isset($all_fonts) && !empty($all_fonts)){
            foreach($all_fonts as $all_font){
                if($all_font->family == $family){
                    // START Variants
                    if(isset($all_font->variants) && !empty($all_font->variants)){
                        $i= 0;
                        foreach($all_font->variants as $variant){
                            $fonts_variants[$i]['id'] = $variant;
                            $fonts_variants[$i]['name'] = $variant;
                            $i++;
                        }
                    }
                    // END Variants
                }
            }
        }
        return $fonts_variants;
    }
    public function gets_fonts_subsets($family = 'ABeeZee')
    {
        if(!isset($family) && empty($family))
            return false;
        $fonts_subsets = array();
        $all_fonts = Tools::file_get_contents($this->fonts_files);
        $all_fonts = Tools::jsonDecode($all_fonts);
        if(isset($all_fonts) && !empty($all_fonts)){
            foreach($all_fonts as $all_font){
                if($all_font->family == $family){
                    // START subsets
                    if(isset($all_font->subsets) && !empty($all_font->subsets)){
                        $i= 0;
                        foreach($all_font->subsets as $subset){
                            $fonts_subsets[$i]['id'] = $subset;
                            $fonts_subsets[$i]['name'] = $subset;
                            $i++;
                        }
                    }
                    // END subsets
                }
            }
        }
        return $fonts_subsets;
    }
    public function ajaxProcessColorSchemaset()
    {
    	$color_array = array();
        $color_schema = Tools::getValue("color_group");
        $multiple_arr = array();
        if(!$this->fields_form){
        	$this->AllFields();
        }
        Configuration::updateValue(self::$shortname."color_group_name",$color_schema);
        foreach($this->fields_form as $key => $value){
            $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
        }
        if(isset($multiple_arr) && !empty($multiple_arr)){
            foreach($multiple_arr as $mvalue){
                if(isset($mvalue['type']) && $mvalue['type'] == "color"){
                    if($color_schema == 'default'){
                        // Configuration::updateValue(self::$shortname.$mvalue['name'],$mvalue['default_val']);
                        $color_array[$mvalue['name']] = $mvalue['default_val'];
                    }else{
                        if(isset($mvalue['predefine'][$color_schema]) && !empty($mvalue['predefine'][$color_schema])){
                            // Configuration::updateValue(self::$shortname.$mvalue['name'],$mvalue['predefine'][$color_schema]);
                            $color_array[$mvalue['name']] = $mvalue['predefine'][$color_schema];
                        }else{
                        	$color_array[$mvalue['name']] = $mvalue['default_val'];
                        }
                    }
                }
            }
        }
        die(Tools::jsonEncode($color_array));
    }
    public function ProcessSchemaset()
    {

    }
    public function ajaxProcessFilterFonts()
    {
        $results = array();
        if(isset($_POST['family']) && !empty($_POST['family']))
            $family = $_POST['family'];
        else
            $family = 'ABeeZee';
        $key = $_POST['key'];
        $font_variants = Configuration::get($key."_variants");
        $font_subsets = Configuration::get($key."_subsets");
        if(isset($font_variants))
            $variants = explode(",",$font_variants);
        else
            $variants = array();

        if(isset($font_subsets))
            $subsets = explode(",",$font_subsets);
        else
            $subsets = array();

        $results['variants'] = $this->appendoptiondata($this->gets_fonts_variants($family),$variants);
        $results['subsets'] = $this->appendoptiondata($this->gets_fonts_subsets($family),$subsets);
        die(Tools::jsonEncode($results));
    }
    public function ProcessFonts()
    {

    }
    public function appendoptiondata($values = "",$key = array())
    {
        $results = '';
        if($values == "")
            return false;
            foreach($values as $value){
                if(in_array($value['name'],$key)){
                    $results .= '<option value="'.$value['name'].'" selected = "selected">'.$value['name'].'</option>';
                }else{
                    $results .= '<option value="'.$value['name'].'">'.$value['name'].'</option>';
                }
            }
        return $results;
    }
    public function SaveGoogleFonts($key= "")
    {
        if($key == "")
            return false;
        Configuration::updateValue($key.'_family', Tools::getValue($key.'_family'));
        if(isset($_POST[$key.'_variants']) && is_array($_POST[$key.'_variants'])){
            $variants = implode(",", $_POST[$key.'_variants']);
            Configuration::updateValue($key.'_variants',$variants);
        }
        if(isset($_POST[$key.'_subsets']) && is_array($_POST[$key.'_subsets'])){
            $subsets = implode(",", $_POST[$key.'_subsets']);
            Configuration::updateValue($key.'_subsets',$subsets);
        }
    }
    public static function GetGoogleFontsURL($key= "")
    {
        if($key == "")
            return false;
        if(Tools::usingSecureMode()){
            $link = 'https://fonts.googleapis.com/css?family=';
        }else{
            $link = 'http://fonts.googleapis.com/css?family=';
        }
        $family = Configuration::get($key.'_family');
        $variants = Configuration::get($key.'_variants');
        $subsets = Configuration::get($key.'_subsets');
        if(isset($family) && !empty($family)){
            $family = str_replace(" ","+",$family);
            $link .= $family;
            if(isset($variants) && !empty($variants)){
                $link .= ':'.$variants;
                if(isset($subsets) && !empty($subsets)){
                    $link .= '&subset='.$subsets;
                }
            }
            return $link;
        }else{
            return false;
        }
    }
    //END GOOGLE FONTS
    public static function GetGoogleFontsFamily($key= "")
    {
        $family = false;
        if($key == "")
            return $family;
        $family = Configuration::get($key.'_family');
        return $family;
    }
    // Hex2RGBA color
    /* Convert hexdec color string to rgb(a) string */

    public static function hex2rgba($color, $opacity = false) {

     $default = 'rgb(0,0,0)';

     //Return default if no color provided
     if(empty($color))
              return $default;

     //Sanitize $color if "#" is provided
            if ($color[0] == '#' ) {
             $color = substr( $color, 1 );
            }

            //Check if color has 6 or 3 characters and get values
            if (strlen($color) == 6) {
                    $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
            } elseif ( strlen( $color ) == 3 ) {
                    $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
            } else {
                    return $default;
            }

            //Convert hexadec to rgb
            $rgb =  array_map('hexdec', $hex);

            //Check if opacity is set(rgba or rgb)
            if($opacity){
             if(abs($opacity) > 1)
             $opacity = 1.0;
             $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
            } else {
             $output = 'rgb('.implode(",",$rgb).')';
            }

            //Return rgb(a) color string
            return $output;
    }

    public function hookdisplayMaintenance(){
        $countdownjs = '<script type="text/javascript"  src="'. __PS_BASE_URI__ .'js/jquery/jquery-1.11.0.min.js"></script><script type="text/javascript"  src="'. __PS_BASE_URI__ .'themes/greatstore/js/plugins_lib.js"></script>
        ';
        $kr_maintanance_date = Configuration::get(self::$shortname.'maintanance_date');
        $kr_maintanance_title = Configuration::get(self::$shortname.'maintanance_title');
        $kr_maintanance_desc = Configuration::get(self::$shortname.'maintanance_desc');

        $kr_maintanance_bg_image = Configuration::get(self::$shortname.'maintanance_bg');

        $kr_maintanance_bg_image_url = _MODULE_DIR_.$this->name.'/images/'.$kr_maintanance_bg_image;

        $this->smarty->assign(array(
            'kr_maintanance_date' => $kr_maintanance_date,
            'countdownjs' => $countdownjs,
            'kr_maintanance_title' => $kr_maintanance_title,
            'kr_maintanance_desc' => $kr_maintanance_desc,
            'kr_maintanance_bg_image_url' => $kr_maintanance_bg_image_url,
            ));
        return $this->display(__FILE__, 'views/templates/front/maintenance.tpl');
    }
    // START DEMO SETUP
    private function formatHelperArray($origin_arr,$postfix=NULL)
    {
    	if($postfix){
    		$postfix = $postfix.'_module';
    	}
        $fmt_arr = array();
        foreach ($origin_arr as $module) {
            $display_name = $module;
            $module_obj = Module::getInstanceByName($module);
            if (Validate::isLoadedObject($module_obj)) {
                $display_name = $module_obj->displayName;
            }
            $tmp = array();
            $fmt_arr[$postfix.$module] = $module;
        }
        return $fmt_arr;
    }
    private function getNativeModule($type = 0)
    {
        $xml = @simplexml_load_string(Tools::file_get_contents(_PS_API_URL_.'/xml/modules_list_16.xml'));
        if ($xml) {
            $natives = array();
            switch ($type) {
                case 0:
                    foreach ($xml->modules as $row) {
                        foreach ($row->module as $row2) {
                            $natives[] = (string)$row2['name'];
                        }
                    }
                    break;
                case 1:
                    foreach ($xml->modules as $row) {
                        if ($row['type'] == 'native') {
                            foreach ($row->module as $row2) {
                                $natives[] = (string)$row2['name'];
                            }
                        }
                    }
                    break;
                case 2:
                    foreach ($xml->modules as $row) {
                        if ($row['type'] == 'partner') {
                            foreach ($row->module as $row2) {
                                $natives[] = (string)$row2['name'];
                            }
                        }
                    }
                    break;
            }
            if (count($natives) > 0) {
                return $natives;
            }
        }
        return array(
            'addsharethis',
            'bankwire',
            'blockadvertising',
            'blockbanner',
            'blockbestsellers',
            'blockcart',
            'blockcategories',
            'blockcms',
            'blockcmsinfo',
            'blockcontact',
            'blockcontactinfos',
            'blockcurrencies',
            'blockcustomerprivacy',
            'blockfacebook',
            'blocklanguages',
            'blocklayered',
            'blocklink',
            'blockmanufacturer',
            'blockmyaccount',
            'blockmyaccountfooter',
            'blocknewproducts',
            'blocknewsletter',
            'blockpaymentlogo',
            'blockpermanentlinks',
            'blockreinsurance',
            'blockrss',
            'blocksearch',
            'blocksharefb',
            'blocksocial',
            'blockspecials',
            'blockstore',
            'blocksupplier',
            'blocktags',
            'blocktopmenu',
            'blockuserinfo',
            'blockviewed',
            'blockwishlist',
            'carriercompare',
            'cashondelivery',
            'cheque',
            'crossselling',
            'dashactivity',
            'dashgoals',
            'dashproducts',
            'dashtrends',
            'dateofdelivery',
            'editorial',
            'favoriteproducts',
            'feeder',
            'followup',
            'gapi',
            'graphnvd3',
            'gridhtml',
            'homefeatured',
            'homeslider',
            'loyalty',
            'mailalerts',
            'newsletter',
            'pagesnotfound',
            'productcomments',
            'productpaymentlogos',
            'productscategory',
            'producttooltip',
            'pscleaner',
            'referralprogram',
            'sekeywords',
            'sendtoafriend',
            'socialsharing',
            'statsbestcategories',
            'statsbestcustomers',
            'statsbestmanufacturers',
            'statsbestproducts',
            'statsbestsuppliers',
            'statsbestvouchers',
            'statscarrier',
            'statscatalog',
            'statscheckup',
            'statsdata',
            'statsequipment',
            'statsforecast',
            'statslive',
            'statsnewsletter',
            'statsorigin',
            'statspersonalinfos',
            'statsproduct',
            'statsregistrations',
            'statssales',
            'statssearch',
            'statsstock',
            'statsvisits',
            'themeconfigurator',
            'trackingfront',
            'vatnumber',
            'watermark'
        );
    }
    private function getModules($xml)
    {
        $native_modules = $this->getNativeModule();
        $theme_module = array();
        $theme_module['to_install'] = array();
        $theme_module['to_enable'] = array();
        $theme_module['to_disable'] = array();
        foreach ($xml->modules->module as $row) {
            if (strval($row['action']) == 'install' && !in_array(strval($row['name']), $native_modules)) {
                $theme_module['to_install'][] = strval($row['name']);
            } elseif (strval($row['action']) == 'enable') {
                $theme_module['to_enable'][] = strval($row['name']);
            } elseif (strval($row['action']) == 'disable') {
                $theme_module['to_disable'][] = strval($row['name']);
            }
        }
        return $theme_module;
    }
    public function ChooseThemeModule($xml = NULL)
    {
	    $values = array();
	    $to_install = array();
        $to_enable = array();
        $to_disable = array();
        if ($xml) {
            $theme_module = $this->getModules($xml);
            if (isset($theme_module['to_install'])) {
                $to_install = $this->formatHelperArray($theme_module['to_install'],'to_install');
            }
            if (isset($theme_module['to_enable'])) {
                $to_enable = $this->formatHelperArray($theme_module['to_enable'],'to_enable');
            }
            if (isset($theme_module['to_disable'])) {
                $to_disable = $this->formatHelperArray($theme_module['to_disable'],'to_disable');
            }
        }
        $values = array_merge($to_install,$to_enable,$to_disable);
        return $values;
    }
    private function updateImages($xml)
    {
        $return = array();
        if (isset($xml->images->image)) {
            foreach ($xml->images->image as $row) {
                Db::getInstance()->delete('image_type', '`name` = \''.pSQL($row['name']).'\'');
                Db::getInstance()->execute('
					INSERT INTO `'._DB_PREFIX_.'image_type` (`name`, `width`, `height`, `products`, `categories`, `manufacturers`, `suppliers`, `scenes`)
					VALUES (\''.pSQL($row['name']).'\',
						'.(int)$row['width'].',
						'.(int)$row['height'].',
						'.($row['products'] == 'true' ? 1 : 0).',
						'.($row['categories'] == 'true' ? 1 : 0).',
						'.($row['manufacturers'] == 'true' ? 1 : 0).',
						'.($row['suppliers'] == 'true' ? 1 : 0).',
						'.($row['scenes'] == 'true' ? 1 : 0).')',false);
                $return['ok'][] = array(
                        'name' => strval($row['name']),
                        'width' => (int)$row['width'],
                        'height' => (int)$row['height']
                    );
            }
        }
        return $return;
    }
    private function GetHookId($hook = NULL){
    	if($hook == NULL)
    		return false;
    	$hook_id = Hook::getIdByName($hook);
    	if(!$hook_id){
    		$Hookobj = new Hook();
    		$Hookobj->name = $hook;
			$Hookobj->title = $hook;
			$Hookobj->description = $hook;
			$Hookobj->position = 0;
			$Hookobj->live_edit = 0;
			$Hookobj->add();
			if($Hookobj->id){
				$hook_id = $Hookobj->id;
			}
    	}
    	return (int)$hook_id;
    }
    private function hookModule($id_module, $module_hooks, $shop)
    {
        Db::getInstance()->execute('INSERT IGNORE INTO '._DB_PREFIX_.'module_shop (id_module, id_shop) VALUES('.(int)$id_module.', '.(int)$shop.')',false);
        Db::getInstance()->execute($sql = 'DELETE FROM `'._DB_PREFIX_.'hook_module` WHERE `id_module` = '.(int)$id_module.' AND id_shop = '.(int)$shop,false);
        foreach ($module_hooks as $hooks) {
            foreach ($hooks as $hook) {
                $sql_hook_module = 'INSERT INTO `'._DB_PREFIX_.'hook_module` (`id_module`, `id_shop`, `id_hook`, `position`)
									VALUES ('.(int)$id_module.', '.(int)$shop.', '.(int)$this->GetHookId($hook['hook']).', '.(int)$hook['position'].')';
                if (count($hook['exceptions']) > 0) {
                    foreach ($hook['exceptions'] as $exception) {
                        $sql_hook_module_except = 'INSERT INTO `'._DB_PREFIX_.'hook_module_exceptions` (`id_module`, `id_hook`, `file_name`) VALUES ('.(int)$id_module.', '.(int)$this->GetHookId($hook['hook']).', "'.pSQL($exception).'")';
                        Db::getInstance()->execute($sql_hook_module_except,false);
                    }
                }
                Db::getInstance()->execute($sql_hook_module,false);
            }
        }
    }
    public function selecteddemosetup($selected_demo = NULL)
    {
    	if($selected_demo == NULL)
    		return false;
        $xml = false;
        if (file_exists(_PS_ROOT_DIR_.'/modules/'.$this->name.'/demo/'.$selected_demo.'.xml')) {
            $xml = @simplexml_load_file(_PS_ROOT_DIR_.'/modules/'.$this->name.'/demo/'.$selected_demo.'.xml');
        }
        
        if($xml){
        	$ChooseThemeModule = $this->ChooseThemeModule($xml);
            $module_hook = array();
            foreach ($xml->modules->hooks->hook as $row) {
                $name = strval($row['module']);
                $exceptions = (isset($row['exceptions']) ? explode(',', strval($row['exceptions'])) : array());
                $module_hook[$name]['hook'][] = array(
                        'hook' => strval($row['hook']),
                        'position' => strval($row['position']),
                        'exceptions' => $exceptions
                    );
            }
            $this->img_error = $this->updateImages($xml);
            $this->modules_errors = array();
            $id_shop = $this->context->shop->id;
                foreach ($ChooseThemeModule as $key => $value) {

                    if (strncmp($key, 'to_install', strlen('to_install')) == 0) {
                        $module = Module::getInstanceByName($value);
                        if ($module) {
                            $is_installed_success = true;
                            if (!Module::isInstalled($module->name)) {
                                $is_installed_success = $module->install();
                            }
                            // else{
                            // 	$module->uninstall();
                            // 	$is_installed_success = $module->install();
                            // }
                            if ($is_installed_success) {
                                if (!Module::isEnabled($module->name)) {
                                    $module->enable();
                                }
                                // START DUMMY INSERT
                                if(method_exists($module,'xpertsampledata')){
                                	$module->xpertsampledata($selected_demo);
                                }
                                // END DUMMY INSERT
                                if ((int)$module->id > 0 && isset($module_hook[$module->name])) {
                                    $this->hookModule($module->id, $module_hook[$module->name], $id_shop);
                                }
                            } else {
                                $this->modules_errors[] = array('module_name' => $module->name, 'errors' => $module->getErrors());
                            }
                            unset($module_hook[$module->name]);
                        }
                    } elseif (strncmp($key, 'to_enable', strlen('to_enable')) == 0) {
                        $module = Module::getInstanceByName($value);
                        if ($module) {
                            $is_installed_success = true;
                            if(!Module::isInstalled($module->name)){
                                $is_installed_success = $module->install();
                            }
                            // else{
                            // 	$module->uninstall();
                            // 	$is_installed_success = $module->install();
                            // }
                            if ($is_installed_success) {
                                if (!Module::isEnabled($module->name)) {
                                    $module->enable();
                                }
                                // START DUMMY INSERT
                                if(method_exists($module,'xpertsampledata')){
                                	$module->xpertsampledata($selected_demo);
                                }
                                // END DUMMY INSERT
                                if ((int)$module->id > 0 && isset($module_hook[$module->name])) {
                                    $this->hookModule($module->id, $module_hook[$module->name], $id_shop);
                                }
                            } else {
                                $this->modules_errors[] = array('module_name' => $module->name, 'errors' => $module->getErrors());
                            }
                            unset($module_hook[$module->name]);
                        }
                    } elseif (strncmp($key, 'to_disable', strlen('to_disable')) == 0) {
                        $key_exploded = explode('_', $key);
                        $id_shop_module = (int)substr($key_exploded[2], 4);
                        if ((int)$id_shop_module > 0 && $id_shop_module != (int)$id_shop) {
                            continue;
                        }
                        $module_obj = Module::getInstanceByName($value);
                        if (Validate::isLoadedObject($module_obj)) {
                            if (Module::isEnabled($module_obj->name)) {
                                $module_obj->disable();
                            }
                            unset($module_hook[$module_obj->name]);
                        }
                    }
                }
        }
        Tools::clearCache($this->context->smarty);
    }
    // END DEMO SETUP
    public function InstallSampleData()
    {
        $multiple_arr = array();
        if(!$this->fields_form){
        	$this->AllFields();
        }
        foreach($this->fields_form as $key => $value){
            $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
        }
        // START LANG
        if(isset($multiple_arr) && !empty($multiple_arr)){
            foreach($multiple_arr as $mvalue){
                if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
                   $languages = Language::getLanguages(false);
                   foreach($languages as $lang){
                    ${$mvalue['name'].'_lang'}[$lang['id_lang']] = $mvalue['default_val'];
                   }
                }
            }
        }
        // END LANG
        if(isset($multiple_arr) && !empty($multiple_arr)){
            foreach($multiple_arr as $mvalue){
                if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
                    Configuration::updateValue(self::$shortname.$mvalue['name'],${$mvalue['name'].'_lang'});
                }else{
                    if(isset($mvalue['name'])){
                    	if(isset($mvalue['default_val'])){
                        	Configuration::updateValue(self::$shortname.$mvalue['name'],$mvalue['default_val']);
                    	}
                    }
                }
            }
        }
        return true;
    }
    public function UnInstallSampleData()
    {
        $multiple_arr = array();
        if(!$this->fields_form){
        	$this->AllFields();
        }
        foreach($this->fields_form as $key => $value){
            $multiple_arr = array_merge($multiple_arr,$value['form']['input']);
        }
        if(isset($multiple_arr) && !empty($multiple_arr)){
            foreach($multiple_arr as $mvalue){
                if(isset($mvalue['name'])){
                    Configuration::deleteByName(self::$shortname.$mvalue['name']);
                }
            }
        }
        return true;
    }
    public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'DemoWiseSampleData';
		if(method_exists($this,$func)){
        	$this->DemoWiseSampleData($demo);
        }
        return true;
	}
	public function DemoWiseSampleData($demo_number = 'demo_1')
    {
        $multiple_arr = array();
        $languages = Language::getLanguages(false);
        if(!$this->fields_form){
        	$this->AllFields();
        }
        foreach($this->fields_form as $key => $value){
        	if(empty($multiple_arr)){
            	$multiple_arr = $value['form']['input'];
        	}else{
            	$multiple_arr = array_merge($multiple_arr,$value['form']['input']);
        	}
        }
        // START LANG

        if(isset($multiple_arr) && !empty($multiple_arr)){
            foreach($multiple_arr as $mvalue){
                if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
                   foreach($languages as $lang){
	                    if(isset($mvalue['default_val']) || isset($mvalue['data_by_demo'][$demo_number])){
	                    	${$mvalue['name'].'_lang'}[$lang['id_lang']] = isset($mvalue['data_by_demo'][$demo_number]) ? $mvalue['data_by_demo'][$demo_number] : isset($mvalue['default_val']) ? $mvalue['default_val'] : "";
	                    }
                   }
                }
            }
        }
        // END LANG
        if(isset($multiple_arr) && !empty($multiple_arr)){
            foreach($multiple_arr as $mvalue){
                if(isset($mvalue['lang']) && $mvalue['lang'] == true && isset($mvalue['name'])){
                	if(isset(${$mvalue['name'].'_lang'})){
                    	Configuration::updateValue(self::$shortname.$mvalue['name'],${$mvalue['name'].'_lang'});
                	}
                }else{
                    if(isset($mvalue['name'])){
                    	if(isset($mvalue['default_val']) || isset($mvalue['data_by_demo'][$demo_number])){
								if(isset($mvalue['data_by_demo'][$demo_number])){
									$fieldsvalue = $mvalue['data_by_demo'][$demo_number];
								}elseif(isset($mvalue['default_val'])){
									$fieldsvalue = $mvalue['default_val'];
								}else{
									$fieldsvalue = "";
								}
	                        	Configuration::updateValue(self::$shortname.$mvalue['name'],$fieldsvalue);
	                        	$fieldsvalue = "";
                    	}
                    }
                }
            }
        }
        return true;
    }
	public function InsertDummyData($categories=NULL,$class=NULL)
	{
		if($categories == NULL || $class == NULL)
			return false;
		$languages = Language::getLanguages(false);
	    if(isset($categories) && !empty($categories)){
	        $classobj = new $class();
	        foreach($categories as $valu){
	        	if(isset($valu['lang']) && !empty($valu['lang'])){
	        		foreach ($valu['lang'] as $valukey => $value){
	        			foreach ($languages as $language){
	        				if(isset($valukey)){
	        					$classobj->{$valukey}[$language['id_lang']] = isset($value) ? $value : '';
	        				}
	        			}
	        		}
	        	}
        		if(isset($valu['notlang']) && !empty($valu['notlang'])){
        			foreach ($valu['notlang'] as $valukey => $value){
        				if(isset($valukey)){
        					$classobj->{$valukey} = $value;
        				}
        			}
        		}
	        	$classobj->add();
	        }
	    }
	    return true;
	}
	public function alldisabled(){
		$data = array();
		$data['active'] = 0;
		Db::getInstance()->update("xprtproducttab",$data);
		return true;
	}
	public function DummyData()
	{
	    include_once(dirname(__FILE__).'/data/dummy_data.php');
	    $this->InsertDummyData($xprt_prdtabs,'xprtproducttab');
	    return true;
	}
}