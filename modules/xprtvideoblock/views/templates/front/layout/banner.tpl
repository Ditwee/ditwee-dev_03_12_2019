{if isset($xprtvideoblock) && !empty($xprtvideoblock)}
	<div class="xprtvideoblock" {if isset($xprtvideoblock.section_margin) && $xprtvideoblock.section_margin}style="margin:{$xprtvideoblock.section_margin}"{/if}>
    	
		{if isset($xprtvideoblock.sub_title) && $xprtvideoblock.sub_title}
	        <div class="xprt_html_block_content">
	        	{$xprtvideoblock.sub_title}
	        </div>
        {/if}
	</div>
{/if}