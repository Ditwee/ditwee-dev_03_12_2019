
{if isset($xprtvideoblock) && !empty($xprtvideoblock)}
<div class="xprt_video_block_area xprt_parallax_section" style="background-image:url({$xprtvideoblock.image}); height:{$xprtvideoblock.section_height|intval}px; {if isset($xprtvideoblock.section_margin) && $xprtvideoblock.section_margin}margin:{$xprtvideoblock.section_margin}{/if}">
	<div class="xprt_video_block container" >
		<div class="xprt_video_block_content">
			<div class="xprt_video_block_top">

				<a id="xprt_video_popup{$xprtvideoblock.id_xprtvideoblock}" class="fancybox.iframe" href="{$xprtvideoblock.video_url}"><i class="icon-play"></i></a>

			</div>
			<div class="xprt_video_block_bottom">
				{if isset($xprtvideoblock.title) && !empty($xprtvideoblock.title)}
					<h2>{$xprtvideoblock.title}</h2>
				{/if}
				{if isset($xprtvideoblock.sub_title) && !empty($xprtvideoblock.sub_title)}
					<div>{$xprtvideoblock.sub_title}</div>
				{/if}
				{if isset($xprtvideoblock.link_lbl) && !empty($xprtvideoblock.link_lbl)}
					<a class="btn btn-default btn-white" href="{$xprtvideoblock.link_url}">{$xprtvideoblock.link_lbl}</a>
				{/if}
			</div>
		</div>

	</div>
</div>
{/if}

<script type="text/javascript">
	// xprt video popup
	if (!!$.prototype.fancybox)
	$('#xprt_video_popup{$xprtvideoblock.id_xprtvideoblock}').fancybox( { 
			padding: 0,
			margin: 0,
			titleShow     : false,
			openEffect	:	'elastic',
			closeEffect	:	'elastic',
			transitionIn	:	'none',
			transitionOut	:	'none',
			fitToView: false, //
			autoResize: true,
		    maxWidth: "90%", // 
			width: 800, 
			height: 450,
		    // type: 'html',
		    autoSize: false,
	        scrolling: false
			
			// 'speedIn'		:	600, 
			// 'speedOut'		:	200, 
	 } );
			
</script>
