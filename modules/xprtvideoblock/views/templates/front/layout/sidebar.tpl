{if isset($xprtvideoblock) && !empty($xprtvideoblock)}
	<div class="xprtvideoblock sidebar_style block">
		{if isset($xprtvideoblock.title)}
			<h4>
				<em>{$xprtvideoblock.title}</em>
			</h4>
		{/if}
    			
		{if isset($xprtvideoblock.sub_title) && $xprtvideoblock.sub_title}
	        <div class="block_content">
	        	{$xprtvideoblock.sub_title}
	        </div>
        {/if}

	</div>
{/if}