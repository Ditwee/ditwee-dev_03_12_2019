{if isset($xprtvideoblock) && !empty($xprtvideoblock)}
	<div class="xprtvideoblock" {if isset($xprtvideoblock.section_margin) && $xprtvideoblock.section_margin}style="margin:{$xprtvideoblock.section_margin}"{/if}>
    	{if isset($xprtvideoblock.title) && $xprtvideoblock.title}
    		<div class="page_title_area {$xprt.home_title_style}">
    			{if isset($xprtvideoblock.title)}
    				<h3 class="page-heading">
    					<em>{$xprtvideoblock.title}</em>
    					<span class="heading_carousel_arrow"></span>
    				</h3>
    			{/if}
    			{if isset($xprtvideoblock.html_sub_title) && $xprtvideoblock.html_sub_title}
    				<p class="page_subtitle d_none">{$xprtvideoblock.html_sub_title}</p>
    			{/if}
    			<div class="heading-line d_none"><span></span></div>
    		</div>
        {/if}
		
		{if isset($xprtvideoblock.sub_title) && $xprtvideoblock.sub_title}
	        <div class="xprt_html_block_content">
	        	{$xprtvideoblock.sub_title}
	        </div>
        {/if}

	</div>
{/if}