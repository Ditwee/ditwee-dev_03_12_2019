<div class="xprtwelcomeinfoblock home_welcome_block">
	<div class="home_welcome_block_content">
		{if $xprtwtb_subtitle != ''}
			<h4>{$xprtwtb_subtitle}</h4>
		{/if}
		{if $xprtwtb_title != ''}
			<h2>{$xprtwtb_title}</h2>
			<div class="heading-line"><span></span></div>
		{/if}
		{if $xprtwtb_content != ''}
			<p>{$xprtwtb_content}</p>
		{/if}
	</div>
</div>