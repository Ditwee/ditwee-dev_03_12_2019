<?php

class xprtwelcometextblock extends Module {
	public $css_array = array("xprtwelcometextblock.css", "xprtwelcometextblock.css");
	public $js_array = array("xprtwelcometextblock.js", "xprtwelcometextblock.js");
	public function __construct() {
		$this->name = 'xprtwelcometextblock';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Xpert-Idea';
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Great Store Theme Welcome Text Block');
		$this->description = $this->l('Prestashop Most Powerfull Great Store Theme Welcome Text Block by Xpert-Idea.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}
	public function install() {
		if (!parent::install()
			|| !$this->registerHook('displayheader')
			|| !$this->registerHook('displayTopColumn')
			|| !$this->xpertsampledata()
			)
			return false;
		else
			return true;
	}
	public function xpertsampledata($demo=NULL)
	{
		if(($demo==NULL) || (empty($demo)))
			$demo = "demo_1";
		$func = 'xpertsample_'.$demo;
		if(method_exists($this,$func)){
    		$this->{$func}();
	    }
	    return true;
	}
	private function xpertsample_demo_1()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
		    Configuration::updateValue('xprtwtb_title_'.$l['id_lang'],"Welcome To Our Store");
		    Configuration::updateValue('xprtwtb_subtitle_'.$l['id_lang'],"Short Introduction About");
		    Configuration::updateValue('xprtwtb_content_'.$l['id_lang'],"Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip  commodo consequat. Duis aute irure dolor in rep rehenderit. Lorem ipsum dolor sit amet ");
		}
	}
	private function xpertsample_demo_3()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
		    Configuration::updateValue('xprtwtb_title_'.$l['id_lang'],"Welcome To Our Store");
		    Configuration::updateValue('xprtwtb_subtitle_'.$l['id_lang'],"Short Introduction About");
		    Configuration::updateValue('xprtwtb_content_'.$l['id_lang'],"Lorem ipsum dolor sit amet, consectetur adipisicing elit, do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip  commodo consequat. Duis aute irure dolor in rep rehenderit. Lorem ipsum dolor sit amet ");
		}
	}
	private function xpertsample_demo_6()
	{
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
		    Configuration::updateValue('xprtwtb_title_'.$l['id_lang'],"Welcome to our Great Srore!");
		    Configuration::updateValue('xprtwtb_subtitle_'.$l['id_lang'],"");
		    Configuration::updateValue('xprtwtb_content_'.$l['id_lang'],"");
		}
	}
	public function uninstall() {
		if (!parent::uninstall()
			)
			return false;
		else
			return true;
	}
	public function SettingForm() {
		$default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
		$this->fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Setting'),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button',
			),
		);
		$this->fields_form[0]['form']['input'][] = array(
			'type' => 'text',
			'lang' => true,
			'label' => $this->l('Title'),
			'name' => 'xprtwtb_title',
		);
		$this->fields_form[0]['form']['input'][] = array(
			'type' => 'text',
			'lang' => true,
			'label' => $this->l('Sub Title'),
			'name' => 'xprtwtb_subtitle',
		);
		$this->fields_form[0]['form']['input'][] = array(
			'type' => 'textarea',
			'lang' => true,
			'label' => $this->l('Content'),
			'name' => 'xprtwtb_content',
		);
		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
		foreach (Language::getLanguages(false) as $lang) {
			$helper->languages[] = array(
				'id_lang' => $lang['id_lang'],
				'iso_code' => $lang['iso_code'],
				'name' => $lang['name'],
				'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0),
			);
		}
		$helper->toolbar_btn = array(
			'save' => array(
				'desc' => $this->l('Save'),
				'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name . 'token=' . Tools::getAdminTokenLite('AdminModules'),
			),
		);
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->title = $this->displayName;
		$helper->show_toolbar = true;
		$helper->toolbar_scroll = true;
		$helper->submit_action = 'save' . $this->name;
		$helper->tpl_vars = array(
			'fields_value' => $this->getAddFieldsValues(),
		);
		return $helper;
	}
	public function getAddFieldsValues() {
		$fields = array();
		$langs = Language::getLanguages();
		foreach($langs as $l)
		{
			$fields['xprtwtb_title'][$l['id_lang']] = Configuration::get('xprtwtb_title_'.$l['id_lang']);
			$fields['xprtwtb_subtitle'][$l['id_lang']] = Configuration::get('xprtwtb_subtitle_'.$l['id_lang']);
			$fields['xprtwtb_content'][$l['id_lang']] = Configuration::get('xprtwtb_content_'.$l['id_lang']);
		}
		return $fields;
	}
	public function getContent() {
		$html = '';
		if (Tools::isSubmit('save' . $this->name)) {
			$langs = Language::getLanguages();
			foreach($langs as $l)
			{
				Configuration::updateValue('xprtwtb_title_'.$l['id_lang'],Tools::getvalue('xprtwtb_title_'.$l['id_lang']));
				Configuration::updateValue('xprtwtb_subtitle_'.$l['id_lang'],Tools::getvalue('xprtwtb_subtitle_'.$l['id_lang']));
				Configuration::updateValue('xprtwtb_content_'.$l['id_lang'],Tools::getvalue('xprtwtb_content_'.$l['id_lang']));
			}
		}
		$helper = $this->SettingForm();
		$html .= $helper->generateForm($this->fields_form);
		return $html;
	}
	public function hookdisplayheader($params) {
		if(isset($this->css_array))
			foreach ($this->css_array as $css) {
				$this->context->controller->addCSS($this->_path.'css/'.$css);
			}
		if(isset($this->js_array))
			foreach ($this->js_array as $js) {
				$this->context->controller->addJS($this->_path.'js/'.$js);
			}
	}
	public function hookdisplayhome($params) {
		$id_lang = (int)$this->context->language->id;
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		$xprtwtb_title = Configuration::get('xprtwtb_title_'.$id_lang);
		$xprtwtb_subtitle = Configuration::get('xprtwtb_subtitle_'.$id_lang);
		$xprtwtb_content = Configuration::get('xprtwtb_content_'.$id_lang);
		if(empty($xprtwtb_title)){
			$xprtwtb_title = Configuration::get('xprtwtb_title_'.$default_lang);
		}
		if(empty($xprtwtb_subtitle)){
			$xprtwtb_subtitle = Configuration::get('xprtwtb_subtitle_'.$default_lang);
		}
		if(empty($xprtwtb_content)){
			$xprtwtb_content = Configuration::get('xprtwtb_content_'.$default_lang);
		}
		$this->smarty->assign(
			array(
				'xprtwtb_title' => $xprtwtb_title,
				'xprtwtb_subtitle' => $xprtwtb_subtitle,
				'xprtwtb_content' => $xprtwtb_content,
			)
		);
		return $this->display(__FILE__,'views/templates/front/xprtwelcometextblock.tpl');	
	}
	public function hookdisplayTopColumn($params) {
		return $this->hookdisplayhome($params);
	}
	public function hookdisplayHomeTopLeft($params) {
		return $this->hookdisplayhome($params);
	}
	public function hookdisplayNav($params) {
		$id_lang = (int)$this->context->language->id;
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		$xprtwtb_title = Configuration::get('xprtwtb_title_'.$id_lang);
		$xprtwtb_subtitle = Configuration::get('xprtwtb_subtitle_'.$id_lang);
		$xprtwtb_content = Configuration::get('xprtwtb_content_'.$id_lang);
		if(empty($xprtwtb_title)){
			$xprtwtb_title = Configuration::get('xprtwtb_title_'.$default_lang);
		}
		if(empty($xprtwtb_subtitle)){
			$xprtwtb_subtitle = Configuration::get('xprtwtb_subtitle_'.$default_lang);
		}
		if(empty($xprtwtb_content)){
			$xprtwtb_content = Configuration::get('xprtwtb_content_'.$default_lang);
		}
		$this->smarty->assign(
			array(
				'xprtwtb_title' => $xprtwtb_title,
				'xprtwtb_subtitle' => $xprtwtb_subtitle,
				'xprtwtb_content' => $xprtwtb_content,
			)
		);
		return $this->display(__FILE__,'views/templates/front/xprtwelcometextnav.tpl');
	}
}