
<!-- MODULE YOOCHOOSE -->

{literal}
<script type="text/javascript">
  var _ycq = _ycq || [];
  
  var yc_userid = '{/literal}{$user_id}{literal}';
  var yc_itemtype = {/literal}{$itemtype_id}{literal};
  _ycq.push(['_setMandator', '{/literal}{$mandator_id}{literal}']);
  
  (function() {
    var yc = document.createElement('script'); yc.type = 'text/javascript'; yc.async = true;
    yc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.yoochoose.net/yct.js';
    var ycs = document.getElementsByTagName('script')[0]; ycs.parentNode.insertBefore(yc, ycs);
  })();
 
	function yoochoose_addbacket(id_product){
		_ycq.push(['_trackEvent',yc_itemtype ,'basket', id_product ,yc_userid ]);
	}
</script>
{/literal}

<!-- MODULE YOOCHOOSE -->
