<?php

// Avoid direct access to the file
if (!defined('_PS_VERSION_'))
	exit;

class yoochoose extends Module
{
	public  $id_carrier;

	private $_html = '';
	private $_postErrors = array();
	private $_moduleName = 'yoochoose';

	private $mandator_id = "759";
	private $itemtype_id = "2";
	
	/*
	** Construct Method
	**
	*/

	public function __construct()
	{
		$this->name = 'yoochoose';
		$this->tab = 'advertising_marketing';
		$this->version = '1.0';
		$this->author = 'Nicolas TORIKIAN';
		$this->limited_countries = array('fr', 'us');

		parent::__construct ();

		$this->displayName = $this->l('Yoochoose');
		$this->description = $this->l('Module concernant yoochoose');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
 
		if (!Configuration::get('MYMODULE_NAME')) {
		  $this->warning = $this->l('No name provided');
		}
	}


	/*
	** Install / Uninstall Methods
	**
	*/

	public function install()
	{
		
		if (!parent::install() ||
			!$this->registerHook('orderConfirmation') ||
			!$this->registerHook('header') ||
			!$this->registerHook('actionAuthentication') ||
			!Configuration::updateValue('MYMODULE_NAME', 'yoochoose')
		) {
			return false;
		}
 
		return true;
	}
	
	public function uninstall()
	{
		
		if (!parent::uninstall() ||
			!$this->unregisterHook('header') ||
			!$this->unregisterHook('actionAuthentication') ||
			!$this->unregisterHook('orderConfirmation')
		)
			return false;
		
		return true;

	}

	public function hookHeader($params)
	{
		$html = "";
		
		$page_name = Dispatcher::getInstance()->getController();

		$this->smarty->assign(array(
			'mandator_id' => $this->mandator_id,
			'itemtype_id' => $this->itemtype_id
		));	
		
		$cart = $params['cart'];

		$user_id = $cart->id_customer;
		//Si pas connecté = guest
		if($user_id=="" || $user_id=="0")
		{
			//On stock le nom du cookie
			$user_id =  $this->context->cookie->getName('name');
			
		}
		
		$this->smarty->assign(array(
			'user_id'=>$user_id
		));
		
		//Page de description d'un produit
		
		//Pour le module yct.js
		$html = $this->display(__FILE__, 'yoochoose-general.tpl');
		
		
		if($page_name == 'product')
		{
			
			$id_product = (int)Tools::getValue('id_product'); 
			
			$this->smarty->assign(array(
				'event' => 'click',
				'page_id' => $id_product
			));
			
			
			$html.= $this->display(__FILE__, 'product-desc.tpl');
		
		}
		elseif($page_name == 'myaccount' && Tools::getValue('yoochoose') == 'true' )
		{
			
			$before_id =  $this->context->cookie->getName('name');
			$after_id = $this->context->cookie->id_customer;
		
			$this->smarty->assign(array(
				'event' => 'login',
				'sourceuserid' => $before_id,
				'targetuserid' => $after_id
			));
			
			$html.= $this->display(__FILE__, 'login.tpl');
		
		}
		
		return $html;
		
	}

	function hookOrderConfirmation($params){
		
		$order = $params['objOrder'];
		
		$products = $order->getProducts();
		
		$currency = new Currency($order->id_currency);
		
		$this->smarty->assign(array(
			'event' => 'buy',
			'products' => (array)$products,
			'currency_iso' => $currency->iso_code
		));
		
		return $this->display(__FILE__, 'products-buy.tpl');
		
	}
	
	function hookActionAuthentication($params){
		
		//Tools::redirect('my-account?yoochoose=true'); 

	}
		
}