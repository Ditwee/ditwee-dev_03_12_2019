<?php

class Employee extends EmployeeCore
{
    public $id_pointshop;
	public $pointshops = array();

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
		parent::__construct($id, $id_lang, $id_shop);
        
        if ($this->id){
			$this->pointshops = $this->getAssociatedPointshops();
        }
    }

    /**
	 * Return list of employees with combine name
	 */
	public static function getEmployeesWithFullname()
	{
		return Db::getInstance()->executeS('
			SELECT `id_employee`, CONCAT(`firstname`, \' \',`lastname`) as fullname
			FROM `'._DB_PREFIX_.'employee`
			WHERE `active` = 1
			ORDER BY `lastname` ASC
		');
	}

    private function getAssociatedPointshops()
    {
        if(!Module::isEnabled('acaisse'));
			return false;;
			
		$list = array();
        $sql = 'SELECT acp.id_a_caisse_pointshop, name FROM `'._DB_PREFIX_.'a_caisse_pointshop` as acp
                INNER JOIN `'._DB_PREFIX_.'a_caisse_pointshop_employee` as pe ON pe.id_pointshop = acp.id_a_caisse_pointshop 
                WHERE pe.id_employee = '.(int)$this->id;
		foreach (Db::getInstance()->executeS($sql) as $row){
			$list[$row['id_a_caisse_pointshop']] = $row['name'];
        }

		return $list;
    }
	
	public static function getAssociatedSeller($id_employee,$id_shop){
		
		$employee = new Employee((int)$id_employee);
	
		if($employee->id_profile!=6){
			return false;
		}
		else{
			if(Module::isEnabled("jmarketplace")){
			
				return Seller::getAssociatedSellerForEmployee($id_employee,$id_shop);
				
			}
		}
		
		
		
	}
	
	public static function isEmployeeAsRight($id_employee, $id_seller){
		
		$employee = new Employee((int)$id_employee);
		
		if(Validate::isLoadedObject($employee) ){
			
			if($employee->id_profile!=6)
				return true;
			else{
				$seller = new Seller((int)$id_seller);
				if(Validate::isLoadedObject($seller)){
					
					$seller_employee = explode(";",$seller->employees);
					
					if(in_array($id_employee,$seller_employee))
						return true;
				
				}
				
				return false;
			}	
		}
		
		return true;

	}
}
