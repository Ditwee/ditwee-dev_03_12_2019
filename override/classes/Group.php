<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Group extends GroupCore
{
    /** @var string color of this group */
    public $color;
    public $id_parent_group = 0;
    public $id_pointshop = 0;

    public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, \Context $context = null)
    {
        if(Module::isEnabled('acaisse')){
			self::$definition['fields']['color'] = array(
				'type' => self::TYPE_STRING,
				'validate' => 'isColor'
			);
			self::$definition['fields']['id_parent_group'] = array(
				'type' => self::TYPE_INT,
				'validate' => 'isInt'
			);
			self::$definition['fields']['id_pointshop'] = array(
				'type' => self::TYPE_INT,
				'validate' => 'isInt'
			);
		}
        parent::__construct($id_product, $full, $id_lang, $id_shop, $context);        
    }
}