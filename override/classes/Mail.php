<?php
include_once _PS_MODULE_DIR_ . 'welkomsync/classes/Communication.php';
include_once _PS_MODULE_DIR_ . 'welkomsync/classes/AbstractWelkomClass.php';
if (@file_exists(_PS_SWIFT_DIR_ . 'swift_required.php')) { //1.6
    include_once(_PS_SWIFT_DIR_ . 'swift_required.php');
} else { //1.5
    include_once(_PS_SWIFT_DIR_ . 'Swift.php');
    include_once(_PS_SWIFT_DIR_ . 'Swift/Connection/SMTP.php');
    include_once(_PS_SWIFT_DIR_ . 'Swift/Connection/NativeMail.php');
    include_once(_PS_SWIFT_DIR_ . 'Swift/Plugin/Decorator.php');
}
class Mail extends MailCore
{
}
