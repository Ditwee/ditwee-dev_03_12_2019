<?php
/**
* 2015 SNSTheme
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
*  @author    SNSTheme <contact@snstheme.com>
*  @copyright 2015 SNSTheme
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of SNSTheme
*/
class Meta extends MetaCore {
	
	public static function getMetaTags($id_lang, $page_name, $title = '')
    {
		
		if($page_name == 'module-jmarketplace-sellerprofile' && $id_seller = Tools::getValue('id_seller')){
			
			
			$seller = new Seller($id_seller);
			
			$meta = Meta::getHomeMetas($id_lang, $page_name);
			$meta['meta_title'].=$seller->name;
			$meta['meta_description'].=$seller->name;
			return $meta;
		}
		elseif($page_name == 'module-jmarketplace-sellerproductlist' && $id_seller = Tools::getValue('id_seller')){
			
			$seller = new Seller($id_seller);
			
			$meta = Meta::getHomeMetas($id_lang, $page_name);
			$meta['meta_title'].= " ".$seller->name;
			$meta['meta_description'].= " ".$seller->name;
			return $meta;
			
			
		}
		else
			return parent::getMetaTags($id_lang, $page_name, $title = '');
	}
}
