<?php

class Product extends ProductCore{
	
	  /** @var string list of pointshop id */
    public $not_available_in_pointshops = '';
    
    /**
     * added in 1.4.2
     */
    public $remove_after_payment_validation = 0;
    /**
     * added in 1.4.3
     */
    public $is_generic_product = 0;
	
	/**
     * added in module prestatillgiftcard 1.0
     */

    public $prestatillgiftcard_amount = 0;
    public $prestatillgiftcard_minimum_amount = 0;
    public $prestatillgiftcard_partial_usage = 1;
    public $prestatillgiftcard_validity_period = '+1 year';

    public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
    {
		if(Module::isEnabled('acaisse')){
			self::$definition['fields']['not_available_in_pointshops'] = array(
				'type' => self::TYPE_STRING,
				'validate' => 'isString'
			);
			//added in 1.4.2
			self::$definition['fields']['remove_after_payment_validation'] = array(
				'type' => self::TYPE_INT,
				'validate' => 'isInt'
			);
			//added in 1.4.3
			self::$definition['fields']['is_generic_product'] = array(
				'type' => self::TYPE_INT,
				'validate' => 'isInt'
			);
			
			//added in module prestatillgiftcard 1.0
			self::$definition['fields']['prestatillgiftcard_validity_period'] = array(
				'type' => self::TYPE_STRING,
				'validate' => 'isString'
			);
			self::$definition['fields']['prestatillgiftcard_amount'] = array(
				'type' => self::TYPE_INT,
				'validate' => 'isInt'
			);
			self::$definition['fields']['prestatillgiftcard_partial_usage'] = array(
				'type' => self::TYPE_INT,
				'validate' => 'isInt'
			);
			self::$definition['fields']['prestatillgiftcard_minimum_amount'] = array(
				'type' => self::TYPE_INT,
				'validate' => 'isInt'
			);
		}
        parent::__construct($id_product, $full, $id_lang, $id_shop, $context);        
    
		if(Module::isEnabled('jmarketplace')){
			$id_seller = SellerProduct::isSellerProduct( $this->id );
			if($id_seller){
				$seller = new Seller($id_seller);
				$params_seller_profile = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);
				$seller->seller_url = Jmarketplace::getJmarketplaceLink('jmarketplace_seller_rule', $params_seller_profile);
				$this->seller = (array)$seller;
			}
		}
		
    }

	public static function getProductProperties($id_lang, $row, Context $context = null){
		
		$row = parent::getProductProperties($id_lang, $row, $context);
		
		$row['madein'] = Product::getMadeIn($row['id_product']);
		
		$is_market_enable = Module::isEnabled('jmarketplace');
		if($is_market_enable){
			$id_seller = SellerProduct::isSellerProduct( $row['id_product'] );
			$seller = new Seller($id_seller);
			$params_seller_profile = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);
			$seller->seller_url = Jmarketplace::getJmarketplaceLink('jmarketplace_seller_rule', $params_seller_profile);
			$row['seller'] = (array)$seller;
		}
		
		return $row;
	
	}
	
	public static function getMadeIn($id_product){
		
		$features = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT *
			FROM `'._DB_PREFIX_.'feature_product` fp
			LEFT JOIN `'._DB_PREFIX_.'feature_value` fv ON (fp.id_feature_value = fv.id_feature_value)
			LEFT JOIN '._DB_PREFIX_.'feature f ON (f.id_feature = fp.id_feature)
			WHERE `id_product` = '.(int)$id_product.' 
			ORDER BY f.position');
		
		$isBio = false;
		$isNaturel = false;
		$isMadein = false;
		$isVegan = false;
		$typeLogo = false;
		
		foreach($features as $feature){
			
			//Correspond au made in france
			if($feature['id_feature'] == 45 && ($feature['id_feature_value'] == 579 || $feature['id_feature_value'] == 597) ){
			
				//$return = array('shortname'=>'fr','longname'=>'France');
				$isMadein = true;
			}
			
			//Correspond au BIO 
			if($feature['id_feature'] == 69 && ($feature['id_feature_value'] == 1215 || $feature['id_feature_value'] == 1194) ){
			
				//$return = array('shortname'=>'bio','longname'=>'Bio');
				$isBio = true;
				
			}
			//Correspond au naturel
			if($feature['id_feature'] == 69 && ($feature['id_feature_value'] == 1216 || $feature['id_feature_value'] == 1193) ){
			
				//$return = array('shortname'=>'naturel','longname'=>'100% Naturel');
				$isNaturel = true;
			}
			//Correspond au naturel
			if($feature['id_feature'] == 69 && $feature['id_feature_value'] == 1321 ){
			
				//$return = array('shortname'=>'naturel','longname'=>'100% Naturel');
				$isVegan = true;
			}
			
			
			//Correspond logo Bio
			if($feature['id_feature'] == 70 && ($feature['id_feature_value'] == 1305 || $feature['id_feature_value'] == 13045) ){
				$typeLogo = "bio";
			}
			//Correspond logo Fabrique en
			if($feature['id_feature'] == 70 && ($feature['id_feature_value'] == 1306 || $feature['id_feature_value'] == 1306) ){
				$typeLogo = "madein";
			}
			//Correspond logo 100% naturel
			if($feature['id_feature'] == 70 && ($feature['id_feature_value'] == 1304 || $feature['id_feature_value'] == 1304) ){
				$typeLogo = "naturel";
			}
			//Correspond logo Vegan
			if($feature['id_feature'] == 70 && ($feature['id_feature_value'] == 1322 || $feature['id_feature_value'] == 1322) ){
				$typeLogo = "vegan";
			}
			
		}
		
		//if( ($typeLogo=='bio' && $isBio) || (!$typeLogo && $isBio))
		if( $isMadein && ($typeLogo=='madein' || !$typeLogo))
			return array('shortname'=>'fr','longname'=>'France');
		
		if( $isBio && ($typeLogo=='bio' || !$typeLogo))
			return array('shortname'=>'bio','longname'=>'Bio');
		
		if( $isNaturel && ($typeLogo=='naturel' || !$typeLogo))
			return array('shortname'=>'naturel','longname'=>'100% Naturel');
		
		if( $isVegan && ($typeLogo=='vegan' || !$typeLogo))
			return array('shortname'=>'vegan','longname'=>'Vegan');
		
		
		return false;
		
	}
	
	public static function getProductsImgs($product_id)
    {
	$sql = '
		(SELECT * from `'._DB_PREFIX_.'image` 
		WHERE id_product="'.$product_id.'" and cover=1)

		 union
				 (SELECT * from `'._DB_PREFIX_.'image` 
		WHERE id_product="'.$product_id.'" and cover=0 	ORDER BY `position` LIMIT 0,1 )
	
		LIMIT 0,2
		';
        $result = Db::getInstance()->ExecuteS($sql);
	return $result;
    }

    public static function getImagesByID($id_product, $limit = 2){
        $id_image = Db::getInstance()->ExecuteS('SELECT `id_image` FROM `'._DB_PREFIX_.'image` WHERE `id_product` = '.(int)($id_product) . ' ORDER BY position ASC LIMIT 1, ' . $limit);
        $toReturn = array();
        if(!$id_image)
            return;
        else
            foreach($id_image as $image)
                $toReturn[] = $id_product . '-' . $image['id_image'];
        return $toReturn;
    }

    public function getAdjacentProducts()
    {
        $position  = Db::getInstance()->getValue('SELECT position FROM '._DB_PREFIX_.'category_product WHERE id_product = ' . (int)$this->id . ' AND id_category = ' . (int)$this->id_category_default);

        // get products that are before and after

        $previous = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
        SELECT cp.id_product, pl.link_rewrite, cp.position, pl.name,im.id_image image
        FROM '._DB_PREFIX_.'category_product cp
        LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (cp.id_product = pl.id_product)
        LEFT JOIN '._DB_PREFIX_.'product p ON (cp.id_product = p.id_product)
        LEFT JOIN `'._DB_PREFIX_.'image` im ON  (p.`id_product` = im.`id_product`AND `cover` = 1)
        WHERE p.id_category_default = '.(int)$this->id_category_default.' AND (cp.position < '. (int)($position ) .' ) AND cp.id_category = ' . (int)$this->id_category_default .' AND pl.id_lang = '.(Context::getContext()->language->id).' AND p.active = 1
        ORDER BY cp.position DESC');

        $next = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
        SELECT cp.id_product, pl.link_rewrite, cp.position, pl.name,im.id_image image
        FROM '._DB_PREFIX_.'category_product cp
        LEFT JOIN '._DB_PREFIX_.'product_lang pl ON (cp.id_product = pl.id_product)
        LEFT JOIN '._DB_PREFIX_.'product p ON (cp.id_product = p.id_product)
        LEFT JOIN `'._DB_PREFIX_.'image` im ON  (p.`id_product` = im.`id_product`AND `cover` = 1)
        WHERE p.id_category_default = '.(int)$this->id_category_default.' AND (cp.position > '. (int)($position ) .' ) AND cp.id_category = ' . (int)$this->id_category_default .' AND pl.id_lang = '.(Context::getContext()->language->id).' AND p.active = 1
        ORDER BY cp.position ASC');

        return array('previous' => $previous, 'next' => $next);
    }
	
	 public function getImages($id_lang, Context $context = null)
    {
        $images = parent::getImages($id_lang,$context);
		
		foreach($images as &$image){
		
			if(isset($image['legend'])) $image['legend'] = str_replace(' | '.Configuration::get('PS_SHOP_NAME'),"",$image['legend']).' | '.Configuration::get('PS_SHOP_NAME') ;
			if(isset($image['title'])) $image['title'] = str_replace(' | '.Configuration::get('PS_SHOP_NAME'),"",$image['legend']).' | '.Configuration::get('PS_SHOP_NAME') ;
       	
		}
		
		return $images;
    }
	
	
	
}
?>