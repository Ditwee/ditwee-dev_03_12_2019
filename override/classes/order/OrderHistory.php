<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class OrderHistory extends OrderHistoryCore
{

    public function sendEmail($order, $template_vars = false)
    {
        $data = array();
		if ($template_vars) {
            $data = array_merge($data, $template_vars);
        }
		
		$carrier = new Carrier($order->id_carrier, $order->id_lang);
	

		$liste_boutique = array('{liste_boutique}' => '','{nom_boutique}'=>'');
		if(($carrier->id_reference == 81 || $carrier->id_reference == 84) && Module::isEnabled('jmarketplace')){
			$seller = new Seller();
			$liste_boutique = array('{liste_boutique}' => $seller->formatWorkingHoursMail($order->id));
		}
		
		
		/*Modif nico */
		$product_var_tpl_list = array();
		foreach ($order->getProducts() as $product) {
		
			if(Module::isEnabled('jmarketplace')){
			
				$id_seller = SellerProduct::isSellerProduct( $product['id_product'] );
				if($id_seller){
					
					$seller = new Seller($id_seller);
					$liste_boutique['{nom_boutique}'] = $seller->name;
					
				}
			}
		
			
			$price = Product::getPriceStatic((int)$product['id_product'], false, ($product['product_attribute_id'] ? (int)$product['product_attribute_id'] : null), 6, null, false, true, $product['product_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});
			$price_wt = Product::getPriceStatic((int)$product['id_product'], true, ($product['product_attribute_id'] ? (int)$product['id_product_attribute'] : null), 2, null, false, true, $product['product_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')});

			$product_price = Product::getTaxCalculationMethod() == PS_TAX_EXC ? Tools::ps_round($price, 2) : $price_wt;

			$product_var_tpl = array(
				'reference' => $product['product_reference'],
				'name' => $product['product_name'].(isset($product['attributes']) ? ' - '.$product['attributes'] : ''),
				'unit_price' => Tools::displayPrice($product_price, Context::getContext()->currency, false),
				'price' => Tools::displayPrice($product_price * $product['product_quantity'], Context::getContext()->currency, false),
				'quantity' => $product['product_quantity'],
				'customization' => array()
			);


			$product_var_tpl_list[] = $product_var_tpl;
			// Check if is not a virutal product for the displaying of shipping
			if (!$product['is_virtual']) {
				$virtual_product &= false;
			}
		} // end foreach ($products)

		$product_list_txt = '';
		$product_list_html = '';
		if (count($product_var_tpl_list) > 0) {
			$product_list_txt = $this->getEmailTemplateContent($order->id_lang,'order_conf_product_list.txt', Mail::TYPE_TEXT, $product_var_tpl_list);
			
			$product_list_html = $this->getEmailTemplateContent($order->id_lang,'order_conf_product_list.tpl', Mail::TYPE_HTML, $product_var_tpl_list);
		}
		
		$liste_boutique['{products}'] = $product_list_html;
        $liste_boutique['{products_txt}'] = $product_list_txt;
			
		$data = array_merge($data,$liste_boutique);

		return parent::sendEmail($order, $data);
    }
	
	
	
	protected function getEmailTemplateContent($id_lang,$template_name, $mail_type, $var)
    {
        $email_configuration = Configuration::get('PS_MAIL_TYPE');
        if ($email_configuration != $mail_type && $email_configuration != Mail::TYPE_BOTH) {
            return '';
        }

		$lang = new Language($id_lang);
	
        $theme_template_path = _PS_THEME_DIR_.'mails'.DIRECTORY_SEPARATOR.$lang->iso_code.DIRECTORY_SEPARATOR.$template_name;
        $default_mail_template_path = _PS_MAIL_DIR_.$lang->iso_code.DIRECTORY_SEPARATOR.$template_name;

        if (Tools::file_exists_cache($theme_template_path)) {
            $default_mail_template_path = $theme_template_path;
        }

        if (Tools::file_exists_cache($default_mail_template_path)) {
            Context::getContext()->smarty->assign('list', $var);
            return Context::getContext()->smarty->fetch($default_mail_template_path);
        }
        return '';
    }

}
