<?php
/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2016 PrestaShop SA
 *  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

/**
 * @since 1.5
 */
class HTMLTemplateInvoice extends HTMLTemplateInvoiceCore
{
   
    public function __construct(OrderInvoice $order_invoice, $smarty, $bulk_mode = false)
    {

		parent::__construct($order_invoice, $smarty, $bulk_mode);

	}

    public function getContent()
    {

		$is_market_enable = Module::isEnabled('jmarketplace');
		if($is_market_enable){
			
			$id_seller = SellerOrder::getOrderBySeller( $this->order->id,$this->order->id_lang );
			$seller = new Seller($id_seller[0]['id_seller']);
			
			$data = array(
				'seller_name' => $seller->name,
				'seller_compte' => 'Facture établie par DITWEE au nom et pour le compte de '.$seller->name,
				'seller_adress' => $seller->address."<br/>".$seller->postcode." ".$seller->city,
				'seller_siren' => $seller->cif,
				'seller_tva' => $seller->fax,
			);
			
			$footer = array('hide_shipping' => false);
			$this->smarty->assign($footer);

			$this->smarty->assign($data);
		}
	
		return parent::getContent();
		
    }
  
}
