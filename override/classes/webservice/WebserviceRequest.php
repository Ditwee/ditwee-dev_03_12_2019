<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class WebserviceRequest extends WebserviceRequestCore
{
	/* Pour activer le mode marketplace = 1 vendeur*/
	public $marketplace = false;
	public $seller = 0;
	
    public static function getResources()
    {        
        if(!class_exists('Seller'))
            include_once _PS_MODULE_DIR_.'jmarketplace/classes/Seller.php';
		
		if(!class_exists('SellerOrder'))
            include_once _PS_MODULE_DIR_.'jmarketplace/classes/SellerOrder.php';
		
        
        if(!class_exists('SellerComment'))
            include_once _PS_MODULE_DIR_.'jmarketplace/classes/SellerComment.php';
        
        if(!class_exists('SellerCommision'))
            include_once _PS_MODULE_DIR_.'jmarketplace/classes/SellerCommision.php';
        
        if(!class_exists('SellerCommisionHistory'))
            include_once _PS_MODULE_DIR_.'jmarketplace/classes/SellerCommisionHistory.php';
        
        if(!class_exists('SellerIncidence'))
            include_once _PS_MODULE_DIR_.'jmarketplace/classes/SellerIncidence.php';
        
        if(!class_exists('SellerIncidenceMessage'))
            include_once _PS_MODULE_DIR_.'jmarketplace/classes/SellerIncidenceMessage.php';
        
        if(!class_exists('SellerPayment'))
            include_once _PS_MODULE_DIR_.'jmarketplace/classes/SellerPayment.php';
        
		if(!class_exists('SellerProduct'))
            include_once _PS_MODULE_DIR_.'jmarketplace/classes/SellerProduct.php';
		
		if(!class_exists('Jmarketplace'))
            include_once _PS_MODULE_DIR_.'jmarketplace/jmarketplace.php';
		
        $resources = parent::getResources();
        $resources['sellers'] = array('description' => 'Sellers', 'class' => 'Seller');
        $resources['seller_products'] = array('description' => 'Seller products', 'class' => 'SellerProduct');
        $resources['seller_comments'] = array('description' => 'Seller comments', 'class' => 'SellerComment');
        $resources['seller_commisions'] = array('description' => 'Seller commissions', 'class' => 'SellerCommision');
        $resources['seller_commisions_history'] = array('description' => 'Seller commissions history', 'class' => 'SellerCommisionHistory');
        $resources['seller_incidences'] = array('description' => 'Seller incidences', 'class' => 'SellerIncidence');
        $resources['seller_incidence_messages'] = array('description' => 'Seller incidences messages', 'class' => 'SellerIncidenceMessage');
        $resources['seller_payments'] = array('description' => 'Seller payments', 'class' => 'SellerPayment');
        
        ksort($resources);
        return $resources;
    }
	
	public function getSellerByKey($key){
		
		$description = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
		SELECT description
		FROM `'._DB_PREFIX_.'webservice_account`
		WHERE `key` = "'.pSQL($key).'"');
		
		$array = explode(" ", $description);

		if( $array[0] == $key && $array[1] == 'Marketplace' ){
			
			$seller = new Seller($array[2]);
			
			if($seller->email == $array[3]){
				$this->marketplace = true;
				$this->seller = $array[2];
			}
			
		}
		
		
	}
	
	public function fetch($key, $method, $url, $params, $bad_class_name, $inputXml = null){
		
		parent::fetch($key, $method, $url, $params, $bad_class_name, $inputXml);
		
		$this->getSellerByKey($key);

		if($this->marketplace){
			
			//GET products
			//Si on demande la liste complete des produits, on renvoi que ceux de la boutique
			if($this->method == 'GET' && $this->urlSegment[0] == 'products' && !isset($this->urlSegment[1])){

				$newProds = array();
				foreach($this->objects as $product){
					
					if($this->seller == SellerProduct::isSellerProduct($product->id))
						array_push($newProds, $product);

				}
				
				$temp = $this->objects['empty'];
				$this->objects = $newProds;
				$this->objects['empty'] = $temp;

			}
			
			//POST products
			//Permet de lier un nouveau produit au vendeur
			if($this->method == 'POST' && $this->urlSegment[0] == 'products' && isset($this->urlSegment[1])){
				
				SellerProduct::associateSellerProduct($this->seller, (int)$this->urlSegment[1]);
				
			}
			
			//GET custumer
			//Permet d'ajouter les clients du marketplace en ne selectionnant que les clients dont les commandes sont liées au vendeur
			if($this->method == 'GET' && $this->urlSegment[0] == 'customers' && !isset($this->urlSegment[1])){
		
				$orders = SellerOrder::getOrdersBySeller($this->seller,$this->_available_languages[0]);
				$newCustomers = array();
				
				if($orders){
					
					//On parcour les commandes
					foreach($orders as $order){
						
						$ord = new Order($order['id_order']);
						
						$cust = new Customer($ord->id_customer);
						if(!in_array($cust,$newCustomers))
							array_push($newCustomers, $cust);

					}
					
				}
				
				$temp = $this->objects['empty'];
				$this->objects = $newCustomers;
				$this->objects['empty'] = $temp;	
			}
			
			//GET orders 
			//Permet de sortir les commandes liées au vendeur
			if($this->method == 'GET' && $this->urlSegment[0] == 'orders' && !isset($this->urlSegment[1])){
				
				//SellerProduct::associateSellerProduct($this->seller, (int)$this->urlSegment[1]);
				$orders = SellerOrder::getOrdersBySeller($this->seller,$this->_available_languages[0]);
				$newOrders = array();
				//On parcour les commandes
				foreach($orders as $order){
						
					$ord = new Order($order['id_order']);
					array_push($newOrders, $ord);
				
				}
				
				$temp = $this->objects['empty'];
				$this->objects = $newOrders;
				$this->objects['empty'] = $temp;
	
			}
			
			//GET orders / id 
			//Permet de verifier si une commande est bien d'un vendeur
			if($this->method == 'GET' && $this->urlSegment[0] == 'orders' && isset($this->urlSegment[1])){
				
				$order = SellerOrder::getOrderBySeller((int)$this->urlSegment[1],$this->_available_languages[0]);
				
				if($order){
					if($order[0]['id_seller']!=$this->seller){
					$temp = $this->objects['empty'];
					$this->objects = array();
					$this->objects['empty'] = $temp;
					}
				}
			}

		}

        return $this->returnOutput();

	}
	
		
}
