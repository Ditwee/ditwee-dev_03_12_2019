<?php

class AdminController extends AdminControllerCore {
    /**
     * Surcharge de la fonction de traduction sur PS 1.7 et supérieur.
     * La fonction globale ne fonctionne pas
     * @param type $string
     * @param type $class
     * @param type $addslashes
     * @param type $htmlentities
     * @return type
     */
    protected function l($string, $class = null, $addslashes = false, $htmlentities = true)
    {
        if ( _PS_VERSION_ >= '1.7') {
            return Context::getContext()->getTranslator()->trans($string);
        } else {
            return parent::l($string, $class, $addslashes, $htmlentities);
        }
    }
	
	 public function initHeader()
    {
		
		parent::initHeader();
		$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		
		//Si l'employee connecté est un vendeur de la marketplace
		if($sellers){
			
			$this->context->smarty->assign(array(
				'show_new_orders' => false,
				'show_new_customers' => false,
				'show_new_messages' => false,
			));
		
		}

	}
}