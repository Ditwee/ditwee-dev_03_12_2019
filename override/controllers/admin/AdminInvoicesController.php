<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminInvoicesController extends AdminInvoicesControllerCore
{
    
    public function initFormByDateSellerInvoice()
    {
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('By date - Facture Vendeur'),
                'icon' => 'icon-calendar'
            ),
            'input' => array(
                array(
                    'type' => 'date',
                    'label' => $this->l('From'),
                    'name' => 'date_from_seller',
                    'maxlength' => 10,
                    'required' => true,
                    'hint' => $this->l('Format: 2011-12-31 (inclusive).')
                ),
                array(
                    'type' => 'date',
                    'label' => $this->l('To'),
                    'name' => 'date_to_seller',
                    'maxlength' => 10,
                    'required' => true,
                    'hint' => $this->l('Format: 2012-12-31 (inclusive).')
                )
            ),
            'submit' => array(
                'title' => $this->l('Generate PDF file by date for Seller invoice'),
                'id' => 'submitPrint',
                'icon' => 'process-icon-download-alt'
            )
        );

        $this->fields_value = array(
            'date_from_seller' => date('Y-m-d'),
            'date_to_seller' => date('Y-m-d')
        );

        $this->table = 'invoice_date_seller';
        $this->show_toolbar = false;
        $this->show_form_cancel_button = false;
        $this->toolbar_title = $this->l('Print PDF invoices');
        return parent::renderForm();
    }
	
	public function initFormByDateCommissionInvoice()
    {
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('By date - Facture Commission Ditwee'),
                'icon' => 'icon-calendar'
            ),
            'input' => array(
                array(
                    'type' => 'date',
                    'label' => $this->l('From'),
                    'name' => 'date_from_commission',
                    'maxlength' => 10,
                    'required' => true,
                    'hint' => $this->l('Format: 2011-12-31 (inclusive).')
                ),
                array(
                    'type' => 'date',
                    'label' => $this->l('To'),
                    'name' => 'date_to_commission',
                    'maxlength' => 10,
                    'required' => true,
                    'hint' => $this->l('Format: 2012-12-31 (inclusive).')
                )
            ),
            'submit' => array(
                'title' => $this->l('Generate PDF file by date for Commission Ditwee invoice'),
                'id' => 'submitPrint',
                'icon' => 'process-icon-download-alt'
            )
        );

        $this->fields_value = array(
            'date_from_commission' => date('Y-m-d'),
            'date_to_commission' => date('Y-m-d')
        );

        $this->table = 'invoice_date_commission';
        $this->show_toolbar = false;
        $this->show_form_cancel_button = false;
        $this->toolbar_title = $this->l('Print PDF invoices');
        return parent::renderForm();
    }

    
    public function initContent()
    {
        

        $this->content .= $this->initFormByDateCommissionInvoice();
        $this->content .= $this->initFormByDateSellerInvoice();
		parent::initContent();
    }

    public function postProcess()
    {
        
		if (Tools::isSubmit('submitAddinvoice_date_commission')) {
            if (!Validate::isDate(Tools::getValue('date_from_commission'))) {
                $this->errors[] = $this->l('Invalid "From" date');
            }

            if (!Validate::isDate(Tools::getValue('date_to_commission'))) {
                $this->errors[] = $this->l('Invalid "To" date');
            }

            if (!count($this->errors)) {
                if (count(OrderInvoice::getByDateInterval(Tools::getValue('date_from_commission'), Tools::getValue('date_to_commission')))) {
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminPdf').'&submitAction=generateSellerCommissionPDFByDate&date_from='.urlencode(Tools::getValue('date_from_commission')).'&date_to='.urlencode(Tools::getValue('date_to_commission')));
                }

                $this->errors[] = $this->l('No invoice has been found for this period.');
            }
        
        }
		elseif (Tools::isSubmit('submitAddinvoice_date_seller')) {
            if (!Validate::isDate(Tools::getValue('date_from_seller'))) {
                $this->errors[] = $this->l('Invalid "From" date');
            }

            if (!Validate::isDate(Tools::getValue('date_to_seller'))) {
                $this->errors[] = $this->l('Invalid "To" date');
            }

            if (!count($this->errors)) {
                if (count(OrderInvoice::getByDateInterval(Tools::getValue('date_from_seller'), Tools::getValue('date_to_seller')))) {
                    Tools::redirectAdmin($this->context->link->getAdminLink('AdminPdf').'&submitAction=generateSellerInvoicePDFByDate&date_from='.urlencode(Tools::getValue('date_from_seller')).'&date_to='.urlencode(Tools::getValue('date_to_seller')));
                }

                $this->errors[] = $this->l('No invoice has been found for this period.');
            }
        
        }
		else {
            parent::postProcess();
        }
    }

}
