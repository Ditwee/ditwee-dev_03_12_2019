<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminOrdersController extends AdminOrdersControllerCore
{
    public function __construct()
    {        
        parent::__construct();
        
		$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		
		//Si l'employee connecté est un vendeur de la marketplace
		if($sellers){
			
			
			$this->_join .= ' 
				LEFT JOIN `ps_order_detail` od ON (od.`id_order` = a.`id_order`)
				LEFT JOIN `ps_seller_product` sp ON (sp.`id_product` = od.`product_id`)
			';
		
			$this->_group = 'GROUP  BY a.id_order';
			$this->fields_list['reference']['filter_key'] = 'a!reference';
				/*	SELECT *
		FROM `'._DB_PREFIX_.'order_detail` od
		LEFT JOIN `'._DB_PREFIX_.'product` p ON (p.id_product = od.product_id)
		LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON (ps.id_product = p.id_product AND ps.id_shop = od.id_shop)
		WHERE od.`id_order` = '.(int)$this->id);*/
		
			$this->_where .= ' AND sp.`id_seller_product` = '.(int)$sellers;
			
		}
		
		
			if(Module::isEnabled('acaisse')){ 
				
				require_once(_PS_MODULE_DIR_.'acaisse/classes/ACaissePointshop.php');
				$pointshop_array = array();
				$pointshops = ACaissePointshop::getAll();

				foreach ($pointshops as $pointshop)
					$pointshop_array[$pointshop['id_a_caisse_pointshop']] = $pointshop['name'];
				
				$pointshop_array[0] = 'En ligne';
				 
				$this->fields_list['id_pointshop'] = array(
					'title' => $this->l('Pointshop'),
					'width' => 80,
					'search' => false,
					'type' => 'none',
					'list' => $pointshop_array,
					'filter_key' => 'a!id_pointshop',
					'filter_type' => 'int',
				); 
			}	
			
	
    }    
    
    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
        
        if(Module::isEnabled('acaisse')){        
			$pointshop_array = array();
			$pointshops = ACaissePointshop::getAll();

			foreach ($pointshops as $pointshop)
				$pointshop_array[$pointshop['id_a_caisse_pointshop']] = $pointshop['name'];
			
			$pointshop_array[0] = 'En ligne';
			
			
			$new_list = array();
			foreach($this->_list as $key=>$item)
			{  
				$add=true;
				$order = new Order($item['id_order']);
				if (Validate::isLoadedObject($order)) {
					$id_employee = $this->context->employee->id;
					$products = $order->getProducts();
					$product = reset($products);
					$id_seller = SellerProduct::existAssociationSellerProduct($product['id_product']);
				}
				
				if($item['id_pointshop'] && $order->id_ticket>1){
					$item['id_pointshop'] = "En caisse - ".$pointshop_array[$item['id_pointshop']];
					$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
					if(!$sellers){
						$add = false;	
					}
				}
				else{
					//$item['id_pointshop'] = $pointshop_array[$item['id_pointshop']];
				
					if($id_seller){
						$seller = new Seller($id_seller);
						$item['id_pointshop'] = "En ligne - ".$seller->name;
					}
				
				}
				
				if(!$item['id_pointshop']) $item['id_pointshop'] = "";
				
				if($add)
					$new_list[$key] = $item;
			}
			
			$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
			if(!$sellers){
				$this->_default_pagination=1000;	
			}
			$this->_list = $new_list;
		}
		
    }
	
	public function initContent(){
		
		
		if (Tools::isSubmit('id_order') && Tools::getValue('id_order') > 0) {
		
			$id_order = Tools::getValue('id_order');
			$order = new Order($id_order);
			if (Validate::isLoadedObject($order)) {
				$id_employee = $this->context->employee->id;
				$products = $order->getProducts();
				$product = reset($products);
				$id_seller = SellerProduct::existAssociationSellerProduct($product['id_product']);
				$is_employee_as_right = Employee::isEmployeeAsRight($id_employee,$id_seller);
				if(!$is_employee_as_right){
					$this->errors[] = Tools::displayError('The order cannot be found within your database.');
					return false;
				}
            }
			
			
		}
		
		parent::initContent();
	}
	
	public function postProcess()
    {
		
		// If id_order is sent, we instanciate a new Order object
        if (Tools::isSubmit('id_order') && Tools::getValue('id_order') > 0) {
            $order = new Order(Tools::getValue('id_order'));
            if (!Validate::isLoadedObject($order)) {
                $this->errors[] = Tools::displayError('The order cannot be found within your database.');
            }
            ShopUrl::cacheMainDomainForShop((int)$order->id_shop);
        }
		
        if (Tools::isSubmit('submitShippingNumber') && isset($order)) {
            if ($this->tabAccess['edit'] === '1') {
			
		
                $order_carrier = new OrderCarrier(Tools::getValue('id_order_carrier'));
                if (!Validate::isLoadedObject($order_carrier)) {
                    $this->errors[] = Tools::displayError('The order carrier ID is invalid.');
                } elseif (!Validate::isTrackingNumber(Tools::getValue('tracking_number'))) {
                    $this->errors[] = Tools::displayError('The tracking number is incorrect.');
                } else {
                    // update shipping number
                    // Keep these two following lines for backward compatibility, remove on 1.6 version
                    $order->shipping_number = Tools::getValue('tracking_number');
                    $order->update();

                    // Update order_carrier
                    $order_carrier->tracking_number = pSQL(Tools::getValue('tracking_number'));
                    if ($order_carrier->update()) {
                        // Send mail to customer
                        $customer = new Customer((int)$order->id_customer);
                        $carrier = new Carrier((int)$order->id_carrier, $order->id_lang);
                        if (!Validate::isLoadedObject($customer)) {
                            throw new PrestaShopException('Can\'t load Customer object');
                        }
                        if (!Validate::isLoadedObject($carrier)) {
                            throw new PrestaShopException('Can\'t load Carrier object');
                        }
                        $templateVars = array(
                            '{followup}' => str_replace('@', $order->shipping_number, $carrier->url),
                            '{firstname}' => $customer->firstname,
                            '{lastname}' => $customer->lastname,
                            '{id_order}' => $order->id,
                            '{shipping_number}' => $order->shipping_number,
                            '{order_name}' => $order->getUniqReference()
                        );
						
						if(  $order->shipping_number && ($carrier->id_reference == 83 || $carrier->id_reference == 89)){
							$templateVars['{followup}'] = $order->shipping_number;
						}
						
                        if (@Mail::Send((int)$order->id_lang, 'in_transit', Mail::l('Package in transit', (int)$order->id_lang), $templateVars,
                            $customer->email, $customer->firstname.' '.$customer->lastname, null, null, null, null,
                            _PS_MAIL_DIR_, true, (int)$order->id_shop)) {
                            Hook::exec('actionAdminOrdersTrackingNumberUpdate', array('order' => $order, 'customer' => $customer, 'carrier' => $carrier), null, false, true, false, $order->id_shop);
                            Tools::redirectAdmin(self::$currentIndex.'&id_order='.$order->id.'&vieworder&conf=4&token='.$this->token);
                        } else {
                            $this->errors[] = Tools::displayError('An error occurred while sending an email to the customer.');
                        }
                    } else {
                        $this->errors[] = Tools::displayError('The order carrier cannot be updated.');
                    }
                }
            } else {
                $this->errors[] = Tools::displayError('You do not have permission to edit this.');
            }
        }
		
		parent::postProcess();
		
	}
}