<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class AdminPdfController extends AdminPdfControllerCore
{
	
	public function getOrderForSeller($order_invoice_collection){
		
		$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		
		foreach($order_invoice_collection as &$order_invoire){
			
			$order = new Order($order_invoire->id_order);
			
			if(!$sellers){
			
				if($order->id_ticket>1 || !$order->id_ticket){
					unset($order_invoire);
				}

			}
			/*
			else{
				
				if($order->id_ticket == $sellers|| !order->id_ticket)
					unset($order_invoire);
				}
				
			}
			*/
		
		}
		
		return $order_invoice_collection;
		
	}
	public function processGenerateInvoicesPDF()
    {
        $order_invoice_collection = OrderInvoice::getByDateInterval(Tools::getValue('date_from'), Tools::getValue('date_to'));

		
        if (!count($order_invoice_collection)) {
            die(Tools::displayError('No invoice was found.'));
        }
		
		$order_invoice_collection = $this->getOrderForSeller($order_invoice_collection);

        $this->generatePDF($order_invoice_collection, PDF::TEMPLATE_INVOICE);
    }
	
	public function processGenerateSellerInvoicePDFByIdOrder()
    {
      
		if (Tools::isSubmit('id_order')) {
            
			$id_order = Tools::getValue('id_order');
			
			$order = new Order((int)$id_order);
			/*if (!Validate::isLoadedObject($order))
				die(Tools::displayError('The order cannot be found within your database.'));*/

			$order_invoice_list = $order->getInvoicesCollection();

			$pdf = new PDF($order_invoice_list, 'SellerInvoice', Context::getContext()->smarty);
			$pdf->render();
		
        }
		else{
			die(Tools::displayError('The order ID -- or the invoice order ID -- is missing.'));	
		}
		
    }
	
	public function processGenerateSellerInvoicePDFByDate()
    {
        $order_invoice_collection = OrderInvoice::getByDateInterval(Tools::getValue('date_from'), Tools::getValue('date_to'));

        if (!count($order_invoice_collection)) {
            die(Tools::displayError('No invoice was found.'));
        }

		$order_invoice_collection = $this->getOrderForSeller($order_invoice_collection);
		
        //$this->generatePDF($order_invoice_collection, PDF::TEMPLATE_INVOICE);
		$pdf = new PDF($order_invoice_collection, 'SellerInvoice', Context::getContext()->smarty);
		$pdf->render();
    }
	
	public function processGenerateSellerCommissionPDFByIdOrder()
    {
      
		if (Tools::isSubmit('id_order')) {
            
			$id_order = Tools::getValue('id_order');
			
			$order = new Order((int)$id_order);
			/*if (!Validate::isLoadedObject($order))
				die(Tools::displayError('The order cannot be found within your database.'));*/

			$order_invoice_list = $order->getInvoicesCollection();

			$pdf = new PDF($order_invoice_list, 'SellerCom', Context::getContext()->smarty);
			$pdf->render();
		
        }
		else{
			die(Tools::displayError('The order ID -- or the invoice order ID -- is missing.'));	
		}
		
    }
	
	public function processGenerateSellerCommissionPDFByDate()
    {
        $order_invoice_collection = OrderInvoice::getByDateInterval(Tools::getValue('date_from'), Tools::getValue('date_to'));

        if (!count($order_invoice_collection)) {
            die(Tools::displayError('No invoice was found.'));
        }
		
		$order_invoice_collection = $this->getOrderForSeller($order_invoice_collection);
		
        //$this->generatePDF($order_invoice_collection, PDF::TEMPLATE_INVOICE);
		$pdf = new PDF($order_invoice_collection, 'SellerCom', Context::getContext()->smarty);
		$pdf->render();
    }

}
