<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @property Product $object
 */
class AdminProductsController extends AdminProductsControllerCore
{
    
    public function __construct()
    {
        parent::__construct();
	
		$sellers = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		
		//Si l'employee connecté est un vendeur de la marketplace
		if($sellers){
	
			$this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'seller_product` sp ON (sp.`id_product` = a.`id_product`)';
			$this->_where .= ' AND sp.`id_seller_product` = '.(int)$sellers;
			
			//On enleve les tab inutiles
			$newtab = array();
			foreach($this->available_tabs as $key=>$item){
				
				if($key != 'Suppliers' && $key != 'ModuleAcaisse' && $key != 'Seo1')
					$newtab[$key] = $item;
				
			}
			$this->available_tabs = $newtab;
			
		}
		else{
			
			$this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'seller_product` sp ON (sp.`id_product` = a.`id_product`)';
			$this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'seller` s ON (s.`id_seller` = sp.`id_seller_product`)';
			$this->fields_list['seller'] = array(
				'title' => $this->l('Vendeur'),
				'width' => 100,
				'filter_key' => 's!name',
			);
			
		}
		
		$this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'manufacturer` AS manu ON (manu.`id_manufacturer` = a.`id_manufacturer`) ';
		$this->fields_list['manufacturer'] = array(
			'title' => $this->l('Manufacturer'),
			'width' => 100,
			'filter_key' => 'manu!name',
		);
		
		//die;
    }
	/*
	public function initContent(){
		
		
		if (Tools::isSubmit('id_product') && Tools::getValue('id_product') > 0) {
		
			$id_product = Tools::getValue('id_product');
			$product = new Product((int)$id_product);
			if (Validate::isLoadedObject($product)) {
				$id_employee = $this->context->employee->id;
				$id_seller = SellerProduct::existAssociationSellerProduct($product->id);
				$is_employee_as_right = Employee::isEmployeeAsRight($id_employee,$id_seller);
				if(!$is_employee_as_right){
					$this->errors[] = Tools::displayError('The product cannot be found within your database.');
					return false;
				}
            }
			
		}
		
		parent::initContent();
	}*/
	
	public function initFormFeatures($obj){
		
		parent::initFormFeatures($obj);
		
		$id_seller = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		 
		//Si l'employee connecté est un vendeur de la marketplace
		if($id_seller){
			
			$id_lang = (int)$this->context->language->id;
			$seller= new Seller((int)$id_seller);
				
			
			$boutiques = FeatureValue::getFeatureValuesWithLang($this->context->language->id, 43);
			$selected_boutique=0;
			foreach($boutiques as $boutique){
				if($seller->name==$boutique['value']) $selected_boutique = $boutique['id_feature_value'];
			}
			$citys = FeatureValue::getFeatureValuesWithLang($this->context->language->id, 44);
			$selected_city=0;
			foreach($citys as $city){
				if($seller->city==$city['value']) $selected_city = $city['id_feature_value'];
			}
			
			$this->tpl_form_vars['custom_form'].= "
				<script>
					$(document).ready(function(){
						$('#feature_43_value').val(".$selected_boutique.");
						$('#feature_44_value').val(".$selected_city.");
						$('#feature_43_value,#feature_44_value').parents('tr').attr('style','display:none');
					});
				</script>";
		}
		
	}
	
	protected function getCarrierList()
    {
		
		$carrier_list = parent::getCarrierList();
		
		$id_seller = Employee::getAssociatedSeller((int)$this->context->employee->id,(int)$this->context->shop->id);
		
		//Si l'employee connecté est un vendeur de la marketplace
		if($id_seller){
			$is_selected_carrier = false;
			foreach($carrier_list as $carrier){
				if(isset($carrier['selected'])) $is_selected_carrier = true;
			}
			//Si aucun transporteur n'avait été selectionné auparavant, on selectionne les transporteur par défaut du vendeur
			if(!$is_selected_carrier){
				$seller = new Seller((int)$id_seller);
				$seller_carriers = explode(";",$seller->carriers);
				foreach($carrier_list as &$carrier){
					if(in_array($carrier['id_reference'],$seller_carriers)) $carrier['selected'] = true;
				}
				
			}

		}
		
		return $carrier_list;
		
	}
	
}

?>