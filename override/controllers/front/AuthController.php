<?php
class AuthController extends AuthControllerCore
{
	/*
    * module: med_controlemail_free
    * date: 2018-04-28 16:46:40
    * version: 1.1
    */
    protected function processSubmitCreate()
	{
		if (!Validate::isEmail($email = Tools::getValue('email_create')) || empty($email))
			$this->errors[] = Tools::displayError('Invalid email address.');
		elseif (Customer::customerExists($email))
		{
			$this->errors[] = Tools::displayError('An account using this email address has already been registered. Please enter a valid password or request a new one. ', false);
			$_POST['email'] = $_POST['email_create'];
			unset($_POST['email_create']);
		}
		elseif ($email && ($module = Module::getInstanceByName('med_controlemail_free')) && $module->active)
		{
			if (!$module->_checkMail($email))
				$this->errors[] = Tools::displayError('Invalid email address domain name.');
			else
			{
				$this->create_account = true;
				$this->context->smarty->assign('email_create', Tools::safeOutput($email));
				$_POST['email'] = $email;
			}
		}
	}
}