<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2016 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
class CartController extends CartControllerCore
{
    /**
     * This process add or update a product in the cart
     */
    /*
    * module: jmarketplace
    * date: 2017-01-13 11:05:46
    * version: 3.6.5
    */
    protected function processChangeProductInCart()
    {
        if (Configuration::get('JMARKETPLACE_SHOW_MANAGE_ORDERS') == 1 || Configuration::get('JMARKETPLACE_SHOW_MANAGE_CARRIER') == 1) {
            $mode = (Tools::getIsset('update') && $this->id_product) ? 'update' : 'add';
            if ($this->qty == 0) {
                $this->errors[] = Tools::displayError('Null quantity.', !Tools::getValue('ajax'));
            } elseif (!$this->id_product) {
                $this->errors[] = Tools::displayError('Product not found', !Tools::getValue('ajax'));
            }
            $product = new Product($this->id_product, true, $this->context->language->id);
            if (!$product->id || !$product->active || !$product->checkAccess($this->context->cart->id_customer)) {
                $this->errors[] = Tools::displayError('This product is no longer available.', !Tools::getValue('ajax'));
                return;
            }
            $qty_to_check = $this->qty;
            $cart_products = $this->context->cart->getProducts();
            if (is_array($cart_products)) {
                foreach ($cart_products as $cart_product) {
                    if ((!isset($this->id_product_attribute) || $cart_product['id_product_attribute'] == $this->id_product_attribute) &&
                        (isset($this->id_product) && $cart_product['id_product'] == $this->id_product)) {
                        $qty_to_check = $cart_product['cart_quantity'];
                        if (Tools::getValue('op', 'up') == 'down') {
                            $qty_to_check -= $this->qty;
                        } else {
                            $qty_to_check += $this->qty;
                        }
                        break;
                    }
                }
            }
            if ($this->id_product_attribute) {
                if (!Product::isAvailableWhenOutOfStock($product->out_of_stock) && !Attribute::checkAttributeQty($this->id_product_attribute, $qty_to_check)) {
                    $this->errors[] = Tools::displayError('There isn\'t enough product in stock.', !Tools::getValue('ajax'));
                }
            } elseif ($product->hasAttributes()) {
                $minimumQuantity = ($product->out_of_stock == 2) ? !Configuration::get('PS_ORDER_OUT_OF_STOCK') : !$product->out_of_stock;
                $this->id_product_attribute = Product::getDefaultAttribute($product->id, $minimumQuantity);
                if (!$this->id_product_attribute) {
                    Tools::redirectAdmin($this->context->link->getProductLink($product));
                } elseif (!Product::isAvailableWhenOutOfStock($product->out_of_stock) && !Attribute::checkAttributeQty($this->id_product_attribute, $qty_to_check)) {
                    $this->errors[] = Tools::displayError('There isn\'t enough product in stock.', !Tools::getValue('ajax'));
                }
            } elseif (!$product->checkQty($qty_to_check)) {
                $this->errors[] = Tools::displayError('There isn\'t enough product in stock.', !Tools::getValue('ajax'));
            }
            if (!$this->errors && $mode == 'add') {
				//Nico
                //$this->deleteProductOtherSeller();
                if (!$this->context->cart->id) {
                    if (Context::getContext()->cookie->id_guest) {
                        $guest = new Guest(Context::getContext()->cookie->id_guest);
                        $this->context->cart->mobile_theme = $guest->mobile_theme;
                    }
                    $this->context->cart->add();
                    if ($this->context->cart->id) {
                        $this->context->cookie->id_cart = (int)$this->context->cart->id;
                    }
                }
                if (!$product->hasAllRequiredCustomizableFields() && !$this->customization_id) {
                    $this->errors[] = Tools::displayError('Please fill in all of the required fields, and then save your customizations.', !Tools::getValue('ajax'));
                }
                if (!$this->errors) {
                    $cart_rules = $this->context->cart->getCartRules();
                    $update_quantity = $this->context->cart->updateQty($this->qty, $this->id_product, $this->id_product_attribute, $this->customization_id, Tools::getValue('op', 'up'), $this->id_address_delivery);
                    if ($update_quantity < 0) {
                        $minimal_quantity = ($this->id_product_attribute) ? Attribute::getAttributeMinimalQty($this->id_product_attribute) : $product->minimal_quantity;
                        $this->errors[] = sprintf(Tools::displayError('You must add %d minimum quantity', !Tools::getValue('ajax')), $minimal_quantity);
                    } elseif (!$update_quantity) {
                        $this->errors[] = Tools::displayError('You already have the maximum quantity available for this product.', !Tools::getValue('ajax'));
                    } elseif ((int)Tools::getValue('allow_refresh')) {
                        $cart_rules2 = $this->context->cart->getCartRules();
                        if (count($cart_rules2) != count($cart_rules)) {
                            $this->ajax_refresh = true;
                        } else {
                            $rule_list = array();
                            foreach ($cart_rules2 as $rule) {
                                $rule_list[] = $rule['id_cart_rule'];
                            }
                            foreach ($cart_rules as $rule) {
                                if (!in_array($rule['id_cart_rule'], $rule_list)) {
                                    $this->ajax_refresh = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            $removed = CartRule::autoRemoveFromCart();
            CartRule::autoAddToCart();
            if (count($removed) && (int)Tools::getValue('allow_refresh')) {
                $this->ajax_refresh = true;
            }
        }
        else {
            CartControllerCore::processChangeProductInCart();
        }
    }
    
    /*
    * module: jmarketplace
    * date: 2017-01-13 11:05:46
    * version: 3.6.5
    */
    public function deleteProductOtherSeller() {
        if ($this->context->cart->id) {
            $id_product = Tools::getValue('id_product');
            $id_seller = Seller::getSellerByProduct($id_product);
            $cart_products = $this->context->cart->getProducts();
            if (is_array($cart_products)) {
                foreach ($cart_products as $cart_product) {
                    $id_seller_old = Seller::getSellerByProduct($cart_product['id_product']);
                    if ($id_seller_old != $id_seller)
                        $this->context->cart->deleteProduct($cart_product['id_product']);
                }
            }
        }
    }
}
