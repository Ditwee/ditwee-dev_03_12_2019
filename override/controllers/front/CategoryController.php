<?php
class CategoryController extends CategoryControllerCore {
    /*
    * module: jscomposer
    * date: 2018-04-23 19:15:38
    * version: 4.4.8
    */
    public function initContent() {
        parent::initContent();
		
		
		$this->context->smarty->assign(
                array(
                    'description_short' => Tools::truncateString($this->category->description, Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT')),
					'description_short_limit' => Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT')
                )
            );
		/*
        $description = $this->category->description = JsComposer::do_shortcode($this->category->description);
        if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer')) {
            $this->context->smarty->assign(
                array(
                    'description_short' => JsComposer::do_shortcode($description),
                )
            );
            $this->category->description = JsComposer::do_shortcode($description);
        }
        if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode')) {
            
            $this->context->smarty->assign(
                array(
                    'description_short' => smartshortcode::do_shortcode($description),                
                )
            );
            $this->category->description = smartshortcode::do_shortcode($description);            
        }
		*/
    }
}
