<?php

class ManufacturerController extends ManufacturerControllerCore
{
	
    protected function assignAll()
    {
        if (Configuration::get('PS_DISPLAY_SUPPLIERS')) {
            $data = Manufacturer::getManufacturers(false, $this->context->language->id, true, false, false, false);
            $nbProducts = count($data);
            $this->n = abs((int)Tools::getValue('n', $nbProducts));
            $this->p = abs((int)Tools::getValue('p', 1));
			
            $data = Manufacturer::getManufacturers(true, $this->context->language->id, true, $this->p, $this->n, false);
            $this->pagination($nbProducts);
            foreach ($data as &$item) {
                $item['image'] = (!file_exists(_PS_MANU_IMG_DIR_.$item['id_manufacturer'].'-'.ImageType::getFormatedName('medium').'.jpg')) ? $this->context->language->iso_code.'-default' : $item['id_manufacturer'];
            }
            $this->context->smarty->assign(array(
                'pages_nb' => ceil($nbProducts / (int)$this->n),
                'nbManufacturers' => $nbProducts,
                'mediumSize' => Image::getSize(ImageType::getFormatedName('medium')),
                'manufacturers' => $data,
                'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY')
            ));
        } else {
            $this->context->smarty->assign('nbManufacturers', 0);
        }
    }
    /*
    * module: jscomposer
    * date: 2018-04-23 19:15:38
    * version: 4.4.8
    */
    public function display()
	{
            if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer') && !empty($this->manufacturer->description))
            {
                   $this->manufacturer->description = JsComposer::do_shortcode( $this->manufacturer->description );
            }
            if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
            {
                   $this->manufacturer->description = smartshortcode::do_shortcode( $this->manufacturer->description );
            }
            return parent::display();
	}
}
