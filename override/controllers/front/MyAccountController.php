<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class MyAccountController extends MyAccountControllerCore
{

    public function initContent()
    {
       

		
		
		
        $this->context->smarty->assign(array(
            'referralprogram' => (bool)Module::isEnabled('referralprogram'),
			'mailalerts' => (bool)Module::isEnabled('mailalerts'),
            'marketplace' => (bool)Module::isEnabled('jmarketplace'),
			'whishlist' => (bool)Module::isEnabled('blockwishlist')
           
        ));
		
		if(Module::isEnabled('jmarketplace')){
			
			$this->context->smarty->assign(array(
				'marketplace_show_seller_favorite' => Configuration::get('JMARKETPLACE_SELLER_FAVORITE'),	
				'marketplace_is_seller' => Seller::isSeller($this->context->cookie->id_customer, $this->context->shop->id),
				'marketplace_is_active_seller' => Seller::isActiveSellerByCustomer($this->context->cookie->id_customer),
				'marketplace_show_contact_seller' => Configuration::get('JMARKETPLACE_SHOW_CONTACT'),
				'marketplace_show_seller_profile' => Configuration::get('JMARKETPLACE_SHOW_PROFILE'),
				
				'marketplace_show_seller_rating' => Configuration::get('JMARKETPLACE_SELLER_RATING'),
			));
			
		}
		
		 parent::initContent();
    }
}
