<?php

class ParentOrderController extends ParentOrderControllerCore
{

	public function init()
    {
		
        if (Configuration::get('PS_ORDER_PROCESS_TYPE') == 1 && Dispatcher::getInstance()->getController() != 'orderopc') {
            
			if(Tools::getIsset('id_mail') && Tools::getValue('id_mail') >0 && Tools::getIsset('wlkm_cart') && Tools::getValue('wlkm_cart') >0){
				$url = "&id_mail=".Tools::getValue('id_mail')."&wlkm_cart=".Tools::getValue('wlkm_cart');
				
				Tools::redirect('index.php?controller=order-opc'.$url);
			}
        }
		
        parent::init();
	
	}
	
}
?>