<?php
class ProductController extends ProductControllerCore {
    public function initContent() {
		
		$adjacent_products = $this->product->getAdjacentProducts();

		$this->context->smarty->assign(array(
			'prev_product'=> $adjacent_products['previous'],
			'next_product'=> $adjacent_products['next'],
			'madein' => Product::getMadeIn($this->product->id)
		));
        
		parent::initContent();
		
	}
    /*
    * module: jscomposer
    * date: 2018-04-23 19:15:38
    * version: 4.4.8
    */
    public function display()
	{
            if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer'))
            {
                   $this->product->description = JsComposer::do_shortcode( $this->product->description );
            }
            if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
            {
                   $this->product->description = smartshortcode::do_shortcode( $this->product->description );
            }
            return parent::display();
	}
}
?>
