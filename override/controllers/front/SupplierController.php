<?php
class SupplierController extends SupplierControllerCore
{
    /*
    * module: jscomposer
    * date: 2018-04-23 19:15:38
    * version: 4.4.8
    */
    public function display()
	{
            
            if (Module::isInstalled('jscomposer') && (bool) Module::isEnabled('jscomposer')&& !empty($this->supplier->description))
            {
                   $this->supplier->description = JsComposer::do_shortcode( $this->supplier->description );
            }
            if (Module::isInstalled('smartshortcode') && (bool) Module::isEnabled('smartshortcode'))
            {
                   $this->supplier->description = smartshortcode::do_shortcode( $this->supplier->description );
            }
                    
            return parent::display();
                    
	}
}
