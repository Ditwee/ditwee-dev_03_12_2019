<?php
if (!defined('_PS_VERSION_'))
    exit;

class azmegamenuOverride extends azmegamenu
{
	
	public function __construct()
    {
		parent::__construct();
		//$this->registerHook('actionObjectSellerUpdateAfter');
		//$this->registerHook('actionProductOutOfStock');
		
	}
	
	public function hookActionObjectCategoryUpdateAfter(){
	
		$this->clearCache();

	}
	
	public function hookActionObjectCategoryDeleteAfter(){
	
		$this->clearCache();

	}
	
	public function hookActionObjectCategoryAddAfter(){
	
		$this->clearCache();

	}
	
	public function hookActionObjectSellerUpdateAfter(){
	
		$this->clearCache();

	}
	
	public function hookActionProductOutOfStock(){
	
		$this->clearCache();
		
	}
	
	
	
	

}
