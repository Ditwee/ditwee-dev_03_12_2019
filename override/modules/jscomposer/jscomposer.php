<?php
if (!defined('_PS_VERSION_'))
    exit;


class JsComposerOverride extends JsComposer
{
	
	public function contenthookvalue($hook = '')
    {
	
        if (!$this->vcHookContentCount($hook))
            return false;

        $context = $this->context;
        $page = $context->controller->php_self;
        if (!is_object($this->vccawobj)) {
            $this->vccawobj = vccontentanywhere::GetInstance();
        }
        $vcaw = $this->vccawobj;

        $id_page_value = '';

        if ($id_cms = Tools::getValue('id_cms')) {
            $id_page_value = $id_cms;
        } else if ($id_category = Tools::getValue('id_category')) {
            $id_page_value = $id_category;
        } else if ($id_product = Tools::getValue('id_product')) {
            $id_page_value = $id_product;
        }
        $cacheId = 'vccc' . $page . $hook . $id_page_value;
		
		$cached = $this->isCached('jscomposer.tpl', $this->getCacheId(), $cacheId);
        if (!$cached || ($cached && (int)count($_POST))){
		
            $results = $vcaw->GetVcContentAnyWhereByHookPageFilter($hook, $page, $id_page_value);
            $this->smarty->assign(array(
                'results' => $results
            ));
			
			if((int)count($_POST)){	
				return $this->display(__FILE__, 'views/templates/front/jscomposer.tpl');	
			}
        }

        return $this->display(__FILE__, 'views/templates/front/jscomposer.tpl', $this->getCacheId(), $cacheId);
    }
}