<?php
/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_CAN_LOAD_FILES_'))
	exit;

class MailAlertsOverride extends MailAlerts
{
	
	
	public function hookActionUpdateQuantity($params)
	{
		$id_product = (int)$params['id_product'];
		$id_product_attribute = (int)$params['id_product_attribute'];

		$quantity = (int)$params['quantity'];
		$context = Context::getContext();
		$id_shop = (int)$context->shop->id;
		$id_lang = (int)$context->language->id;
		$product = new Product($id_product, false, $id_lang, $id_shop, $context);
		$product_has_attributes = $product->hasAttributes();
		$configuration = Configuration::getMultiple(
			array(
				'MA_LAST_QTIES',
				'PS_STOCK_MANAGEMENT',
				'PS_SHOP_EMAIL',
				'PS_SHOP_NAME'
			), null, null, $id_shop
		);
		$ma_last_qties = (int)$configuration['MA_LAST_QTIES'];

		$check_oos = ($product_has_attributes && $id_product_attribute) || (!$product_has_attributes && !$id_product_attribute);

		if ($check_oos &&
			$product->active == 1 &&
			(int)$quantity <= $ma_last_qties &&
			!(!$this->merchant_oos || empty($this->merchant_mails)) &&
			$configuration['PS_STOCK_MANAGEMENT'])
		{
			$iso = Language::getIsoById($id_lang);
			$product_name = Product::getProductName($id_product, $id_product_attribute, $id_lang);
			
			
			$template_vars = array(
				'{qty}' => $quantity,
				'{last_qty}' => $ma_last_qties,
				'{product}' => $product_name,
				'{url_front}' => $this->l('Page produit','mailalerts').' : '.$context->link->getProductLink($id_product),
				'{seller_info}' => ''
			);

			// Do not send mail if multiples product are created / imported.
			if (!defined('PS_MASS_PRODUCT_CREATION'))
			
			{
			
				// Send 1 email by merchant mail, because Mail::Send doesn't work with an array of recipients
				$merchant_mails = explode(self::__MA_MAIL_DELIMITOR__, $this->merchant_mails);
				
				if(Module::isEnabled('jmarketplace')){
					
					if($product->seller){
						$template_vars['{seller_info}'] = $product->seller['name'];
						$template_vars['{url_front}'].= '<br/> '.$this->l('Modifier les stocks').' : '.$this->context->link->getModuleLink('jmarketplace', 'editproduct', array('id_product' => $id_product), true);;
						$merchant_mails[] = $product->seller['email'];
					}
					
				}
				
				foreach ($merchant_mails as $merchant_mail)
				{
					Mail::Send(
						$id_lang,
						'productoutofstock',
						Mail::l('Product out of stock', $id_lang),
						$template_vars,
						$merchant_mail,
						null,
						(string)$configuration['PS_SHOP_EMAIL'],
						(string)$configuration['PS_SHOP_NAME'],
						null,
						null,
						dirname(__FILE__).'/mails/',
						false,
						$id_shop
					);
				}
				
				
			}
			
		}

		if ($this->customer_qty && $quantity > 0)
			MailAlert::sendCustomerAlert((int)$product->id, (int)$params['id_product_attribute']);
		
	}
	
	public function hookActionValidateOrder($params){
		
		if(Module::isEnabled('acaisse')){
		
			$order = $params['order'];
			$cart = $params['cart'];
			
			if(!$cart->id_pointshop){
				
				parent::hookActionValidateOrder($params);
					
			}
		
		}
		else{
		
			parent::hookActionValidateOrder($params);
		
		}
		
	}
	
}