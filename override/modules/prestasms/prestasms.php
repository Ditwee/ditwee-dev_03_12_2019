<?php
/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
 if (!defined('_CAN_LOAD_FILES_'))
	exit;

class PrestaSmsOverride extends PrestaSms
{
	
	public function hookActionValidateOrder($params){
		
		if(Module::isEnabled('acaisse')){
		
			$order = $params['order'];
			$cart = $params['cart'];
			
			if(!$cart->id_pointshop){
				
				$this->sendSMSToSeller($params);
				parent::hookActionValidateOrder($params);
					
			}
		
		}
		else{
			$this->sendSMSToSeller($params);
			parent::hookActionValidateOrder($params);
		
		}
		
	}
	
	public function sendSMSToSeller($params){
		
		$order = $params['order'];	
		$cart = $params['cart'];
		
		$products = $order->getProducts();
		
		foreach ($products as $p) {
            
			$id_seller = Seller::getSellerByProduct($p['product_id']); 
		
		}
		
		//Si produit vendu par un vendeur via le site
		if ($id_seller && (int)!$cart->id_pointshop) {
			
			$seller = new Seller($id_seller);
			
			$phone = $seller->phone;
			
			if($phone){
				$text = "Nouvelle commande sur Ditwee, connectez-vous dès maintenant à votre compte vendeur sur www.ditwee.fr. L'équipe Ditwee";
				$sms = new SmsModel(true, SmsModel::TYPE_SIMPLE, '', SmsModel::SMS_TRANSACTION);
            
				$sms->number($phone)->text($text)->unicode('')->send();
			}
			
		}
	}


}