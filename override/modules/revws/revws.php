<?php

if (!defined('_PS_VERSION_'))
	exit;

class RevwsOverride extends Revws
{

	public function __construct() {
		
		parent::__construct();
		$this->registerHook('displayDataTest2');
		$this->unregisterHook('displayFooterProduct');
		$this->unregisterHook('displayDataTest');
	}
	/*
	public function registerHooks() {

		return parent::registerHooks() && $this->setupHooks(['displayDataTest']);
	
	}
	*/
	
	public function hookDisplayDataTest2($params){
	  
		return parent::hookDisplayFooterProduct();
	 
	}
	/*
	public function hookDisplayFooterProduct() {
	 
		return;
		$set = $this->getSettings();
		if ($set->getPlacement() === 'block') {
		  $list = $this->getProductReviewList();
		  if ($list->isEmpty() && $this->getVisitor()->isGuest() && $set->hideEmptyReviews()) {
			return;
		  }
		  return $this->display(__FILE__, 'product_footer.tpl');
		}
	}
	*/
}

?>