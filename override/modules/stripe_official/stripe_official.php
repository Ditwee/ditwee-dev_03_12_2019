<?php

if (!defined('_PS_VERSION_'))
	exit;

class Stripe_officialOverride extends Stripe_official
{

	public $key_connect;
	
	public function __construct()
    {
		
		parent::__construct();
	
		$this->key_connect = "ca_B5J7rlQDaUJEuRSvpqYuDCEoqIrHTTb3";
		//Test mode
		//if (Configuration::get(self::_PS_STRIPE_.'mode')) {
        if (Configuration::get("STRIPE_MODE")) {
			$this->key_connect = "ca_B5J7DWXl79rhFq8D6j4Lv5zaVXgdjBfW";
		}
	
	}
	
	public function get_url_link_connect($id_seller = null){
		
		$url = "";
		if($id_seller){
			
			$jmarketplace = Module::getInstanceByName('jmarketplace');
			
			$seller = new Seller($id_seller);
			
			$param = array('id_seller' => $seller->id, 'link_rewrite' => $seller->link_rewrite);
        
			$url_seller_profile = $jmarketplace->getJmarketplaceLink('jmarketplace_seller_rule', $param);
		
			$url = "&stripe_user[email]=".$seller->email;
			$url .= "&stripe_user[url]=".$url_seller_profile;
			$url .= "&stripe_user[country]=FR";
			$url .= "&stripe_user[phone_number]=".$seller->phone;
			$url .= "&stripe_user[business_name]=".$seller->name;
			$url .= "&stripe_user[business_type]=".$seller->shop;
			//$url .= "&stripe_user[first_name]=".$seller->email;
			//$url .= "&stripe_user[last_name]=".$seller->email;
			//$url .= "&stripe_user[dob_day]=".$seller->email;
			//$url .= "&stripe_user[dob_month]=".$seller->email;
			//$url .= "&stripe_user[dob_year]=".$seller->email;
			$url .= "&stripe_user[street_address]=".$seller->address;
			$url .= "&stripe_user[city]=".$seller->city;
			//$url .= "&stripe_user[state]=".$seller->email;
			$url .= "&stripe_user[zip]=".$seller->postcode;
			$url .= "&stripe_user[physical_product]=true";
			
			
			
		}
		
		return "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=".$this->key_connect."&scope=read_write&redirect_uri=".$this->set_url_reditect_connect().$url;

	}
	
	public function set_url_reditect_connect(){
			
		return $this->context->link->getModuleLink('stripe_official', 'connect', array(), true);
	
	}
	
	public function get_credential_from_stripe($code){
		
		$api_key = $this->getSecretKey();
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://connect.stripe.com/oauth/token',
			CURLOPT_RETURNTRANSFER => true,
			//CURLOPT_HTTPHEADER => array("Authorization: Bearer $api_key"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query(array(
				'client_secret' => $api_key,
				'code' => $code,
				'grant_type' => 'authorization_code',
			))
		));
		
		return json_decode(curl_exec($curl));
		
		
	}
	
	public function set_customer_account($email){
		
		if (!$this->retrieveAccount($this->getSecretKey(), '', 1)) {
            die(Tools::jsonEncode(array('code' => '0', 'msg' => $this->l('Invalid Stripe credentials, please check your configuration.'))));
        }
		
		try {
			\Stripe\Stripe::setApiKey($this->getSecretKey());
			
			$customer = \Stripe\Customer::create(array(
			  "email" => $email,
			 
			));
			
			return $customer;
		}
		catch (Exception  $e) {
			die(Tools::jsonEncode(array('code' => '0', 'msg' => $e->getMessage())));
		}
		
	}
	
	public function set_sepa_source($iban, $name){
		
		
		if (!$this->retrieveAccount($this->getSecretKey(), '', 1)) {
            die(Tools::jsonEncode(array('code' => '0', 'msg' => $this->l('Invalid Stripe credentials, please check your configuration.'))));
        }

        try {
			
			\Stripe\Stripe::setApiKey($this->getSecretKey());
			
			$source = \Stripe\Source::create(array(
			  "type" => "sepa_debit",
			  "sepa_debit" => array("iban" => $iban),
			  "currency" => "eur",
			  "owner" => array(
				"name" => $name,
			  ),
			));
			
			return $source;
		}
		catch (Exception  $e) {
			die(Tools::jsonEncode(array('code' => '0', 'msg' => $e->getMessage())));
		}
	}
	
	public function link_sepa_to_customer($id_customer,$id_sepa){
		
		if (!$this->retrieveAccount($this->getSecretKey(), '', 1)) {
            die(Tools::jsonEncode(array('code' => '0', 'msg' => $this->l('Invalid Stripe credentials, please check your configuration.'))));
        }
		
		try {
			
			\Stripe\Stripe::setApiKey($this->getSecretKey());
			
			$customer = \Stripe\Customer::retrieve($id_customer);
			$customer->sources->create(array("source" => $id_sepa));
		
		}
		catch (Exception  $e) {
			die(Tools::jsonEncode(array('code' => '0', 'msg' => $e->getMessage())));
		}
		
		
	}

}

?>