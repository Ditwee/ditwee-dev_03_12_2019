{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{$style_tab}


<table width="100%" id="body" border="0" cellpadding="0" cellspacing="0" style="margin:0;">
	<!-- Invoicing -->
	<tr>
		<td colspan="12">

			{* $addresses_tab *}
			
<table id="addresses-tab" cellspacing="0" cellpadding="0">
	<tr>

		<td width="49%">
				
			{if isset($order_invoice)}{$order_invoice->shop_address}{/if}
		
		</td>
		<td width="49%">
				{if isset($seller_name)}
			
				{$seller_name}<br/>
				
				
				{if isset($seller_adress)}{$seller_adress}<br/>{/if} 
				{*
				{if isset($seller_tva)}{$seller_tva}{/if}
				{if isset($seller_siren)}{$seller_siren}<br/>{/if}
				*}
				{/if}
		</td>
	</tr>
</table>


		</td>
	</tr>

	<tr>
		<td colspan="12" height="30">&nbsp;</td>
	</tr>

	<!-- TVA Info -->
	<tr>
		<td colspan="12">

			{* $summary_tab *}

<table id="summary-tab" width="100%">
	<tr>
		<th class="header small" valign="middle">{l s='Invoice Number' pdf='true'}</th>
		<th class="header small" valign="middle">{l s='Invoice Date' pdf='true'}</th>
		<th class="header small" valign="middle">{l s='Order Reference' pdf='true'}</th>
		<th class="header small" valign="middle">{l s='Order date' pdf='true'}</th>	
	</tr>
	<tr>
		<td class="center small white">{$title|escape:'html':'UTF-8'}</td>
		<td class="center small white">{dateFormat date=$order->invoice_date full=0}</td>
		<td class="center small white">{$order->getUniqReference()}</td>
		<td class="center small white">{dateFormat date=$order->date_add full=0}</td>
	</tr>
</table>

		</td>
	</tr>

	<tr>
		<td colspan="12" height="20">&nbsp;</td>
	</tr>

	<!-- Product -->
	<tr>
		<td colspan="12">

			{*$product_tab*}

<table class="product" width="100%" cellpadding="4" cellspacing="0">

	<thead>
	<tr>
		<th class="product header small" width="{$layout.reference.width}%">{l s='Reference' pdf='true'}</th>
		<th class="product header small" width="{$layout.product.width}%">{l s='Product' pdf='true'}</th>

		
		<th class="product header-right small" width="{$layout.quantity.width}%">{l s='Unit Price' pdf='true'} <br /> {l s='(Tax excl.)' pdf='true'}</th>
		<th class="product header-right small" width="{$layout.quantity.width}%">{l s='Qty' pdf='true'}</th>
		<th class="product header-right small" width="{$layout.quantity.width}%">{l s='Total' pdf='true'} <br /> {l s='(Tax excl.)' pdf='true'}</th>
		<!--<th class="product header-right small" width="{$layout.quantity.width}%">{l s='% Commision' pdf='true'}</th>-->
		<th class="product header-right small" width="{$layout.quantity.width}%">{l s='Our Commision ' pdf='true'} <br /> {l s='(Tax excl.)' pdf='true'}</th>
		</tr>
	</thead>

	<tbody>

	<!-- PRODUCTS -->
	{foreach $order_details as $order_detail}
		{cycle values=["color_line_even", "color_line_odd"] assign=bgcolor_class}
		<tr class="product {$bgcolor_class}">

			<td class="product center">
				{$order_detail.product_reference}
			</td>
			<td class="product left">
				{if $display_product_images}
					<table width="100%">
						<tr>
							<td width="15%">
								{if isset($order_detail.image) && $order_detail.image->id}
									{$order_detail.image_tag}
								{/if}
							</td>
							<td width="5%">&nbsp;</td>
							<td width="80%">
								{$order_detail.product_name}
							</td>
						</tr>
					</table>
				{else}
					{$order_detail.product_name}
				{/if}

			</td>

			<td class="product right">
				{displayPrice currency=$order->id_currency price=$order_detail.unit_price_tax_excl_including_ecotax}
				{if $order_detail.ecotax_tax_excl > 0}
					<br>
					<small>{{displayPrice currency=$order->id_currency price=$order_detail.ecotax_tax_excl}|string_format:{l s='ecotax: %s' pdf='true'}}</small>
				{/if}
			</td>
			<td class="product right">
				{$order_detail.product_quantity}
			</td>
			<td  class="product right">
				{displayPrice currency=$order->id_currency price=$order_detail.total_price_tax_excl_including_ecotax}
			</td>
		
			<td  class="product right">
				{displayPrice currency=$order->id_currency price=$order_detail.commision_marketplace}
			</td>
	
		</tr>

		
		
	{/foreach}
	<!-- END PRODUCTS -->

	<!-- CART RULES -->

	

	</tbody>

</table>


		</td>
	</tr>

	<tr>
		<td colspan="12" height="10">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="12" height="10">

	
<table id="total-tab" width="100%" class="left">

	<tr class="bold big">
		<td class="grey">
			{l s='Total (excl tax)' pdf='true'}
		</td>
		<td class="white center">
			{displayPrice currency=$order->id_currency price=$totalProducts}
		</td>
	</tr>
	<tr >
		<td class="grey">
			{l s='% commission Ditwee' pdf='true'}
		</td>
		<td class="white center">
			{$order_detail.percent_commision}%
		</td>
	</tr>
	<tr >
		<td class="grey">
			{l s='Total commission Ditwee HT' pdf='true'}
		</td>
		<td class="white center">
			{displayPrice currency=$order->id_currency price=$totalComissionMarketplace}
		</td>
	</tr>
	<tr >
		<td class="grey">
			{l s='Total TVA sur commission' pdf='true'}
		</td>
		<td class="white center">
			{displayPrice currency=$order->id_currency price=$totalComissionMarketplaceTax}
		</td>
	</tr>
	<tr class="bold big">
		<td class="grey">
			{l s='Total commission Ditwee TTC' pdf='true'}
		</td>
		<td class="white center">
			{displayPrice currency=$order->id_currency price=$totalComissionMarketplaceWithTax}
		</td>
	</tr>
	<tr class="bold big">
		<td class="grey">
			{l s='Revenu total sur vente TTC' pdf='true'}
		</td>
		<td class="white center">
			{displayPrice currency=$order->id_currency price=$totalComissionSellerWithTax}
		</td>
	</tr>
</table>
	
		
		</td>

	</tr>

	<tr>
		<td colspan="12" height="10">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="7" class="left small">

			<table>
				<tr>
					<td>
						<p>{$legal_free_text|escape:'html':'UTF-8'|nl2br}</p>
					</td>
				</tr>
			</table>

		</td>
	</tr>
	<!-- Hook -->
	{if isset($HOOK_DISPLAY_PDF)}
	<tr>
		<td colspan="12" height="30">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="2">&nbsp;</td>
		<td colspan="10">
			{$HOOK_DISPLAY_PDF}
		</td>
	</tr>
	{/if}

	<tr>
		<td style="text-align: center; font-size: 10pt; color: #444;  width:100%;">
		Pensez à vous inscrire à notre Newsletter & suivez nous suivre 
		<br/>
		<img src="{$img_ps_dir}/cms/fb-insta.jpg" width="50" />
		<br/>
		Créé par Ditwee à Lyon <img src="{$img_ps_dir}/cms/Icon-french-heart.png" width="10" />
	
		</td>
	</tr>
	
</table>
