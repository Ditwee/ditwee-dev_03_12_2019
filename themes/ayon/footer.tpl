

{if !$content_only}

					</div><!-- #center_column -->

					{if isset($right_column_size) && !empty($right_column_size)}

						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>

					{/if}

                    </div><!-- .row -->

                    {if $page_name != 'index'}

    					</div><!-- .container -->

                    {/if}

                </div><!-- #middle-columns -->

			</div><!-- .columns-container -->

			<!-- Footer -->

			<div class="footer-wrapper">



                {if isset($roythemes.footer_lay) && $roythemes.footer_lay !== "8"}

                    {addJsDef map=''}

                    {addJsDef markers=array()}

                    {addJsDef infoWindow=''}

                    {addJsDef locationSelect=''}

                    {addJsDef defaultLat=$defaultLat}

                    {addJsDef defaultLong=$defaultLong}

                    {addJsDef hasStoreIcon=$hasStoreIcon}

                    {addJsDef img_store_dir=$img_store_dir}

                    {addJsDef img_ps_dir=$img_ps_dir}

                    {addJsDef searchUrl=$searchUrl}

                    {addJsDef logo_store=$logo_store}

                    {addJsDefL name=translation_1}{l s='No stores were found. Please try selecting a wider radius.' js=1}{/addJsDefL}

                    {addJsDefL name=translation_2}{l s='store found -- see details:' js=1}{/addJsDefL}

                    {addJsDefL name=translation_3}{l s='stores found -- view all results:' js=1}{/addJsDefL}

                    {addJsDefL name=translation_4}{l s='Phone:' js=1}{/addJsDefL}

                    {addJsDefL name=translation_5}{l s='Get directions' js=1}{/addJsDefL}

                    {addJsDefL name=translation_6}{l s='Not found' js=1}{/addJsDefL}

                {/if}



                    {if isset($roythemes.footer_lay) && $roythemes.footer_lay == "7"}

                    <div class="foot_line">

                    <div class="container">

                        {hook h='displayFooterLine'}

                    </div>

                </div>

                    {/if}



                {if isset($roythemes.footer_lay) && $roythemes.footer_lay !== "8"}

				<footer id="footer" class="container">

                    {if isset($roythemes.footer_lay) && $roythemes.footer_lay !== "7"}
						{if $page_name!='index'}
							{hook h='displayFooterTop'}
						{/if}
                    {/if}

					<div class="row displayresp">

                    {if isset($roythemes.footer_lay) && $roythemes.footer_lay !== "4" && $roythemes.footer_lay !== "6"}

						<div class="foot_left">
                            {if isset($roythemes.footer_lay) && $roythemes.footer_lay !== "3"}<img class="lazy logo_footer img-responsive"  src="#" data-src="{$modules_dir}roythemescustomizer/upload/logo-footer-{Context::getContext()->shop->id}.{if isset($roythemes.logo_footer_ext)}{$roythemes.logo_footer_ext}{else}png{/if}" alt="{$shop_name|escape:'html':'UTF-8'}" />{/if}                            

                            {hook h='displayFooterLeft'}

                            {if isset($roythemes.footer_lay) && ($roythemes.footer_lay == "3" || $roythemes.footer_lay == "4")}

                                {hook h='displayFooterLeftMap'}

                            {/if}

                            {if isset($roythemes.footer_lay) && ($roythemes.footer_lay == "5")}  

                                {if isset($roythemes.footer_copyright_display) && $roythemes.footer_copyright_display == "1"}

                                    <div class="copy_left">{if isset($roythemes.copyright_text)} {$roythemes.copyright_text} {else} <span>Copyright 2016 All Rights Reserved</span></div> {/if}

                                {/if} 

                            {/if}                        

                        </div>   

                    {/if}

                    {if isset($roythemes.footer_lay) && ($roythemes.footer_lay !== "5")}  

						<div class="foot_right">{$HOOK_FOOTER}                     

                            {if isset($roythemes.footer_lay) && $roythemes.footer_lay == "4" || $roythemes.footer_lay == "6"}

                                <div class="foot_left"><div>


                                    <img class="logo_footer img-responsive" src="{$modules_dir}roythemescustomizer/upload/logo-footer-{Context::getContext()->shop->id}.{if isset($roythemes.logo_footer_ext)}{$roythemes.logo_footer_ext}{else}png{/if}" alt="{$shop_name|escape:'html':'UTF-8'}" />

                                    {hook h='displayFooterLeft'}

                                    {if isset($roythemes.footer_lay) && ($roythemes.footer_lay == "6")}

                                        {hook h='displayFooterLeftNews'}

                                    {/if}

                                    {hook h='displayFooterLeftMap'}    
                                    <!-- BLOC PAIEMENTS FOOTER-->
                                    <div class="container" id="PaiementsDitwee">    
                                    <div class="row">
                                          <div class="col-lg-12" >
                                          <h2 style="text-align: center;">MOYENS DE PAIEMENTS</h2>
                                          </div>
                                    </div>
									<a href="/content/5-paiement-securise-sur-ditwee" title="Moyens de paiements">
                                        <div class="row">
                                          <div class="col-xs-3 col-sm-3 " >
                                            <div class="logo">
                                              <img src="#" data-src="/img/cms/paiements/mastercard.png" alt="Mastercard" class="lazy"/>
                                            </div>
                                          </div>
                                                <div class="col-xs-3 col-sm-3 " >
                                            <div class="logo">
                                              <img src="#" data-src="/img/cms/paiements/visa.png" alt="Visa" class="lazy" />
                                            </div>
                                          </div>
                                                <div class="col-xs-3 col-sm-3 " >
                                            <div class="logo">
                                              <img src="#" data-src="/img/cms/paiements/americanexpress.png" alt="Amex" class="lazy" />
                                            </div>
                                          </div>
                                                <div class="col-xs-3 col-sm-3 " >
                                            <div class="logo">
                                              <img src="#" data-src="/img/cms/paiements/paypal.png" alt="Paypal" class="lazy"/>
                                            </div>
                                          </div>
                                        </div>
									</a>
                                    </div>       
                                </div>   

                            {/if}   

                            {if isset($roythemes.footer_lay) && ($roythemes.footer_lay == "1" || $roythemes.footer_lay == "2")}

                                {hook h='displayFooterRightNews'}

                            {/if}

                            {if isset($roythemes.footer_lay) && ($roythemes.footer_lay !== "3" && $roythemes.footer_lay !== "4" && $roythemes.footer_lay !== "6")}

                                {hook h='displayFooterRightMap'}

                            {/if}

                        </div>

                    {/if} 

						<div class="foot_bottom">{hook h='displayFooterBottom'}</div>

                    </div>                    

                    {if isset($roythemes.footer_lay) && $roythemes.footer_lay !== "7"}

                        {*hook h='displayFooterBottom'*}

                    {/if}

                {if isset($roythemes.footer_lay) && $roythemes.footer_lay == "5"}

                    

                {/if}
</div>
				</footer>

                    <div class="mapwrapper">

                        <div id="map"></div>

                        <div class="store-content-select selector3">

                            <select id="locationSelect" class="form-control">

                                <option>-</option>

                            </select>

                        </div>

                    </div>

                {/if}

            {if isset($roythemes.footer_lay) && ($roythemes.footer_lay !== "5" && $roythemes.footer_lay !== "6")}   

				<div class="foot_copyright">

					<div class="container">

                        {if isset($roythemes.footer_lay) && ($roythemes.footer_lay == "3" || $roythemes.footer_lay == "4" || $roythemes.footer_lay == "7")}                        

                            <div class="copy_cell">

                                {hook h='displayFooterCopyNews'}

                            </div> 

                        {/if}

    						<div class="copy_cell {if isset($roythemes.footer_lay) && ($roythemes.footer_lay == "3" || $roythemes.footer_lay == "4" || $roythemes.footer_lay == "7")}copy_center{/if}">

                                {if isset($roythemes.footer_copyright_display) && $roythemes.footer_copyright_display == "1"}

                                    {if isset($roythemes.copyright_text)} <span class="copytext">{$roythemes.copyright_text}</span> {else} <span>Copyright 2016 All Rights Reserved</span> {/if}

                                {/if}

                            {if isset($roythemes.footer_lay) && $roythemes.footer_lay == "8"}{hook h='displayFooter'}{/if}

                            </div>                        

                            <div class="copy_cell">{if isset($roythemes.footer_lay) && $roythemes.footer_lay !== "8"}{hook h='displayFooterCopyright'}{else}{hook h='displayFooter8right'}{/if}</div>

					</div>

				</div>

            {/if}

			</div><!-- #footer -->



			<!-- #Partenaires-->
			{hook h="footerBotomPartenaire"}



		</div><!-- #page -->

        <div class="side-menu">{hook h='displaySideMobile'}</div>

        <div class="menu-close"></div>

        {hook h='displayLeviBox' mod='roylevibox'}

{/if}



{include file="$tpl_dir./global.tpl"}

        <div id="is_media"></div>

	</body>

</html>