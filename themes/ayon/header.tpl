{*

* RoyThemes AYON Theme

*}

<!DOCTYPE HTML>


<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="{$lang_iso}"><![endif]-->

<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="{$lang_iso}"><![endif]-->

<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="{$lang_iso}"><![endif]-->

<!--[if gt IE 8]> <html class="no-js ie9" lang="{$lang_iso}"><![endif]-->

<html lang="{$lang_iso}" itemscope itemtype="http://schema.org/WebPage">

	<head>

		<meta charset="utf-8" />
		
		<!--<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico" />
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />-->
		<link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png?v={$img_update_time}">
		<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png?v={$img_update_time}">
		<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png?v={$img_update_time}">
		<link rel="manifest" href="/img/favicon/site.webmanifest?v={$img_update_time}">
		<link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg?v={$img_update_time}" color="#5bbad5">
		<!--<link rel="shortcut icon" href="favicon.ico?v={$img_update_time}">-->

		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="msapplication-config" content="/img/favicon/browserconfig.xml?v={$img_update_time}">
		<meta name="theme-color" content="#ffffff">
		
		<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->

		<title>{strip}{$meta_title|replace:{' | '|cat:$shop_name}:''|replace:{' - '|cat:$shop_name}:''|escape:'html':'UTF-8'}{/strip} | {$shop_name}</title>

{if isset($meta_description) AND $meta_description}

		<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />

{/if}

{if isset($meta_keywords) AND $meta_keywords}

		<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />

{/if}

        <meta name="generator" content="PrestaShop" />

        <meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />

        <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0" />

        <meta name="apple-mobile-web-app-capable" content="yes" />

        
        {$id_shop = Context::getContext()->shop->id}



        {if isset($css_files)}

            {foreach from=$css_files key=css_uri item=media}

                <link rel="stylesheet" href="{$css_uri}" type="text/css" media="{$media}" />

            {/foreach}

        {/if}

        {if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}

            {$js_def}

            {foreach from=$js_files item=js_uri}

                <script src="{$js_uri|escape:'html':'UTF-8'}"></script>

            {/foreach}

        {/if}



        <!--[if IE 8]>

        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>

        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

        <![endif]-->


		{*
        {if isset($roythemes.font_include)}{$roythemes.font_include} {/if}
		*}


		<!--<link rel="stylesheet" href="/themes/ayon/css/fontello.css" type="text/css" media="all" />-->

		

        {$HOOK_HEADER}



        <meta property="og:title" content="{$meta_title|escape:'htmlall':'UTF-8'}"/>

        <meta property="og:site_name" content="{$shop_name|escape:'htmlall':'UTF-8'}"/>

        <meta property="og:type" content="website">

        <meta property="og:description" content="{$meta_description|escape:html:'UTF-8'}">

        {if !$page_name=='product' && !$page_name=='module-smartblog-details'}

            <meta property="og:url" content="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}"/>

            <meta property="og:image" content="{$logo_url}" />

        {/if}

        {if $page_name=='product'}

            <meta property="og:image" content="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large')}">

        {/if}

        {if $page_name=='module-smartblog-details'}

            <meta property="og:url" content="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}"/>

            <meta property="og:image" content="{$base_dir}modules/smartblog/images/{$post_img}-single-default.jpg">

        {/if}
	
		<script src="//code.tidio.co/ho0cq83kbht6ylu5ko0wjnldavgjtbdm.js"></script>
		
	</head>

	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if $content_only} content_only{/if} lang_{$lang_iso}{if $page_name !='index'} not_home{/if}{if isset($roythemes.header_trah) && $roythemes.header_trah == '1' && $page_name == 'index'} transparent{/if}{if isset($roythemes.header_trao) && $roythemes.header_trao == '1' && $page_name !== 'index'} transparent{/if} {if isset($roythemes.g_lay) && $roythemes.g_lay == '2'}boxed{/if} header{$roythemes.header_lay} plc_cat_{if isset($roythemes.nc_pc_layout)}{$roythemes.nc_pc_layout}{/if}">

	{if !$content_only}

		{if isset($restricted_country_mode) && $restricted_country_mode}

			<div id="restricted-country">

				<p>{l s='You cannot place a new order from your country.'} <span class="bold">{$geolocation_country}</span></p>

			</div>

		{/if}

			<div id="page" class="page">

				{if isset($roythemes.nc_loader) && $roythemes.nc_loader == "1" }

					<div class="loader-overlay">

						<div class="loader">

							<div class="loader-logo"></div>

								<div class="loader-con">

									{if !isset($roythemes.nc_loader_lay) || (isset($roythemes.nc_loader_lay) && $roythemes.nc_loader_lay == "1")}

											<span class="loader-cube"><span class="loader-inner"></span></span>

									{/if}

									{if isset($roythemes.nc_loader_lay)}

										{if $roythemes.nc_loader_lay == "2" || $roythemes.nc_loader_lay == "3" || $roythemes.nc_loader_lay == "5"}

											<span class="loader-obj"></span>

										{/if}

										{if $roythemes.nc_loader_lay == "4"}

											<div class="loader-obj"><i></i><i></i><i></i><i></i></div>

										{/if}

										{if $roythemes.nc_loader_lay == "6"}

											<ul class="loader-obj"><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul>

										{/if}

									{/if}

								</div>

							<div class="loader-shadow"></div>

						</div>

					</div>

				{/if}

            {hook h="displayFixed"}

			<div class="header-wrapper" data-boxed-padding="{if isset($roythemes.g_tp)}{$roythemes.g_tp}{/if}">

				<header id="header">

					{capture name='displayBanner'}{hook h='displayBanner'}{/capture}

					{if $smarty.capture.displayBanner}

						<div class="top_banner">

							<div class="container">

								<div class="row">

									{$smarty.capture.displayBanner}

								</div>

							</div>

						</div>

					{/if}

					{if isset($roythemes.header_lay) && $roythemes.header_lay == "6"}

					<div class="nav nav-panel nav-height">

						<div class="container">

							<div class="row">

								<nav>

									{if isset($roythemes.nc_pos) && $roythemes.nc_pos == "2"}

										<div class="acc_out">

											{hook h="displayAccSub"}

										</div>

									{/if}

									{hook h="displayNav"}

								</nav>

							</div>

						</div>

					</div>

					{/if}

					<div class="head">

						<div class="container head-height">

							<div class="row">
							<div id="DitweesocialMedia">
								<!--
								<a href="https://www.facebook.com/ditweeatoutesvosenvies/" target="_blank" class="fa fa-facebook"></a>
								<a href="https://www.instagram.com/ditwee.fr/" target="_blank" class="fa fa-instagram"></a>
								<a href="https://www.youtube.com/channel/UCOvYM4qAaQld96X8maOw-9w" target="_blank" class="fa fa-youtube"></a>
								-->
								<a href="/content/17-vendre-sur-ditwee" class="btn btn-default button">{l s="Vendre sur Ditwee"}</a>
							</div>

                                <div id="logo_wrapper">

                                    <div class="logo_row">

                                        <div class="logo_cell">

                                            <div id="header_logo">

                                                <a href="{$base_dir}" title="{$shop_name|escape:'html':'UTF-8'}">

                                                    <img class="logo-normal img-responsive" src="{$modules_dir}roythemescustomizer/upload/logo-normal-{$id_shop}.{if isset($roythemes.logo_normal_ext)}{$roythemes.logo_normal_ext}{else}png{/if}" alt="{$shop_name|escape:'html':'UTF-8'}" />



                                                    <img class="logo-trans img-responsive" src="{$modules_dir}roythemescustomizer/upload/logo-trans-{$id_shop}.{if isset($roythemes.logo_trans_ext)}{$roythemes.logo_trans_ext}{else}png{/if}" alt="{$shop_name|escape:'html':'UTF-8'}" />

                                                    <img class="logo-sticky img-responsive" src="{$modules_dir}roythemescustomizer/upload/logo-sticky-{$id_shop}.{if isset($roythemes.logo_sticky_ext)}{$roythemes.logo_sticky_ext}{else}png{/if}" alt="{$shop_name|escape:'html':'UTF-8'}" />

                                                </a>

                                            </div>

                                        </div>

                                    </div>

                                </div>

								<div class="header-right-block">



									{if (isset($roythemes.nc_pos) && $roythemes.nc_pos == "2") && (isset($roythemes.header_lay) && $roythemes.header_lay !== "4" && $roythemes.header_lay !== "6" && $roythemes.header_lay !== "7")}

										<div class="acc_out">

							                <div>{hook h="displayAccSub"}</div>

							            </div>

									{/if}



									{if isset($roythemes.header_lay) && $roythemes.header_lay !== "5" || $roythemes.header_lay !== "6" }
									{hook h="displayTopSearch" mod='royblocksearch'}

										<div class="account_top">{hook h="displayAccount"}</div>
										<div class="wishlist"> 
											<a 	href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|addslashes}" title="{l s='My wishlists'}">
												<i class="ft-icon-heart"></i>
												
											</a>
										</div>
									{/if}

									



									{if (isset($roythemes.nc_pos) && $roythemes.nc_pos == "2") && (isset($roythemes.header_lay) && $roythemes.header_lay == "4")}

										<div class="acc_out">

											{hook h="displayAccSub"}

										</div>

									{/if}



									{hook h="displayTopCart" mod='royblockcart'}

								</div>

								{if (isset($roythemes.nc_pos) && $roythemes.nc_pos == "2") && (isset($roythemes.header_lay) && $roythemes.header_lay == "7")}

									<div class="acc_out">

										{hook h="displayAccSub"}

									</div>

								{/if}

								{if isset($roythemes.header_lay) && $roythemes.header_lay == "5" }

								<div class="header-nav-block">

									<div class="nav nav-panel nav-height">

										<nav>{hook h="displayNav"}</nav>

									</div>

								</div>

								{/if}

								<div class="header-menu-block">

                                    <div class="menu_table">

                                        <div class="menu_wrapper">{hook h="displayMenu" mod='azmegamenu'}</div>

                                    </div>

								</div>

							</div>

						</div>

					</div>

				</header>

			</div>

			<div class="columns-container-top {if isset($roythemes.breadcrumb) && $roythemes.breadcrumb == "0" && $page_name !='index' && $page_name !='pagenotfound'}no-bread{/if}" >

				<div id="topcolumns">

					<div class="container">

						<div class="row">

							<div id="top_column" class="center_column col-xs-12 col-sm-12">

							{hook h="displayTopColumn"}</div>

							{if isset($roythemes.breadcrumb) && $roythemes.breadcrumb == "1" && $page_name !='index' && $page_name !='pagenotfound'}

								<div class="pagename">

								{assign var=shop_name_to_trim value=" - $shop_name"}

								{$meta_title|replace:$shop_name_to_trim:''|escape:'htmlall':'UTF-8'}

								</div>{include file="$tpl_dir./breadcrumb.tpl"}

							{/if}

						</div>

					</div>

				</div>

			</div>

			<div class="columns-container-middle">



				<div id="middlecolumns">

					<div class="container">

						<div class="row">

							{if isset($left_column_size) && !empty($left_column_size) && $page_name!="module-jmarketplace-sellerprofile" && $page_name!="category"}

								<div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>

							{/if}
							{if $page_name == "module-jmarketplace-sellerprofile"}
							
								{assign var=right_column_size_seller value=$right_column_size scope="global"}
								{assign var=left_column_size_seller value=$left_column_size scope="global"}
								{assign var=right_column_size value=12}
							{/if}
							{if $page_name != "module-jmarketplace-sellerprofile" && $page_name != "category"}
								<div id="center_column" class="center_column col-xs-12 col-sm-{12 - $left_column_size - $right_column_size}">
							{/if}
	{/if}

							<input type="hidden" id="nc_p_hover" name="nc_p_hover" value="{if isset($roythemes.nc_p_hover)}{$roythemes.nc_p_hover}{/if}" />





	                        {if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}

	                        {$js_def}

	                        {foreach from=$js_files item=js_uri}

	                            <script src="{$js_uri|escape:'html':'UTF-8'}"></script>

	                        {/foreach}

{/if}

