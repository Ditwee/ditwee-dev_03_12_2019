(function($) {  
	$.fn.juizScrollTo = function( speed,offset ) { 
		if ( !speed ) var speed = 'slow';  
 		if ( !offset ) var offset = 0;
		
		// coeur du plugin
		return this.each( function() {  
			$(this).on('click', function() {  
				var goscroll = false;  
				var the_hash = $(this).attr("href");  
				var regex = new RegExp("(.*)\#(.*)","gi");
				var the_element = '';
 
				if ( the_hash.match("\#(.+)") ) {  
					the_hash = the_hash.replace(regex,"$2");  
					if($("#"+the_hash).length>0) {  
						the_element = "#" + the_hash;  
						goscroll = true;  
					}  
					else if ( $("a[name=" + the_hash + "]").length>0 ) {  
						the_element = "a[name=" + the_hash + "]";  
						goscroll = true;  
					}  
					if ( goscroll ) {  
						
						$('html,body').animate( {  
							scrollTop: $(the_element).offset().top - offset
				
						}, speed, function() {
							tab_n_focus(the_hash)
							write_hash(the_hash);
						});  
						return false;  
					}  
				}  
			});  
		});
 			
		// fonctions
 			
		// écriture du hash
		function write_hash( the_hash ) {
			document.location.hash =  the_hash;
		}
 			
		// accessibilité au clavier
		function tab_n_focus( the_hash ) {  
			$(the_hash).attr('tabindex','0').focus().removeAttr('tabindex');  
		}
 
	};  
 		
	// appel de la fonction sur toutes les ancres !
	$('a.ancre').juizScrollTo('slow', $("#header").height()+10);
 		
	// fonction de slide au chargement
	function trigger_click_for_slide() {
		var the_hash = document.location.hash;
		if ( the_hash )
			$('a[href~="'+the_hash+'"]').trigger('click');
	}
	trigger_click_for_slide();
 
})(jQuery);
