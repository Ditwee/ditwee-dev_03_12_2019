/**
 * 1969-2017 Relais Colis
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@relaiscolis.com so we can send you a copy immediately.
 *
 *  @author    Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright 1969-2017 Relais Colis
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

$(document).ready(function () {
    /*$('.option-home-customer').change(function (e) {
        alert('ok');
        var option_name = $(this).attr('id');
        var cost = $('#' + option_name + '_cost').val();
        var selected = $(this).val();
        //submitUpdateOptionHome(option_name, cost, selected);
    });*/
    $('form[name=carrier_area]').submit(function (e) {
        if (($('#id_carrier' + relais_carrier_id).is(':checked')) || ($('.delivery_option_radio:checked').val() == relais_carrier_id + ',') || ($('#id_carrier' + relais_carrier_max_id).is(':checked')) || ($('.delivery_option_radio:checked').val() == relais_carrier_max_id + ','))
        {
            if (!have_selected_point) {
                if (!!$.prototype.fancybox)
                    $.fancybox.open([
                        {
                            type: 'inline',
                            autoScale: true,
                            minHeight: 30,
                            content: '<p class="fancybox-error">' + msg_order_carrier_relais + '</p>'
                        }],
                            {
                                padding: 0
                            });
                else
                    alert(msg_order_carrier);
                e.preventDefault();
            }
        }
        return true;
    });
    
    // if no relais point selected, we prevent all form of payment method the be submited 
	if($('#order-opc').size() && !(have_selected_point || have_selected_last)) {
		$("#opc_payment_methods-content form").submit(function(evt) {
			evt.preventDefault();
		});
	}
    
    // On One Page Checkout, we need to prevent the selection of a means of payment if there is no selected relais point.
    $('#order-opc').on('click', '.payment_module a', function(e) {
		
		if(($('#id_carrier' + relais_carrier_id).is(':checked')) || ($('.delivery_option_radio:checked').val() == relais_carrier_id + ',') || ($('#id_carrier' + relais_carrier_max_id).is(':checked')) || ($('.delivery_option_radio:checked').val() == relais_carrier_max_id + ','))
		{
			
			
			if (!have_selected_point) {
				e.preventDefault();
				if (!!$.prototype.fancybox) {
					$.fancybox.open([
					{
						type: 'inline',
						autoScale: true,
						minHeight: 30,
						content: '<p class="fancybox-error">' + msg_order_carrier_relais + '</p>'
					}],
					{
						padding: 0
					});
				}
				else {
					alert(msg_order_carrier_relais);
				}
				return false;
			}
			
		}
		return true;
	});
});

function submitUpdateOptionHome(option)
{
    var cost = $('#' + option + '_cost').val();
    var selected = $('#'+ option).val();
    var cart = $('#id_cart_home').val();
    var customer = $('#id_customer_home').val();
    $.ajax({
        type: 'POST',
        url: baseUri + 'modules/relaiscolis/ajax.php?rand=' + new Date().getTime(),
        async: true,
        cache: false,
        dataType: "json",
        headers: {"cache-control": "no-cache"},
        data:
                {
                    option: option,
                    selected: selected,
                    cart: cart,
                    customer: customer,
                    ajax: true,
                    token: token
                },
        success: function (jsonData)
        {
            if (jsonData.hasError)
            {
                var errors = '';
                for (error in jsonData.errors)
                    //IE6 bug fix
                    if (error != 'indexOf')
                        errors += '<li>' + jsonData.errors[error] + '</li>';
                //$('#create_account_error').html('<ol>' + errors + '</ol>').show();
            }
            else
            {
                // adding a div to display a transition
                /*$('#center_column').html('<div id="noSlide">' + $('#center_column').html() + '</div>');
                 $('#noSlide').fadeOut('slow', function()
                 {
                 $('#noSlide').html(jsonData.page);
                 $(this).fadeIn('slow', function()
                 {
                 if (typeof bindUniform !=='undefined')
                 bindUniform();
                 if (typeof bindStateInputAndUpdate !=='undefined')
                 bindStateInputAndUpdate();
                 document.location = '#account-creation';
                 });
                 });*/
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown)
        {
            error = "TECHNICAL ERROR: unable to load form.\n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus;
            if (!!$.prototype.fancybox)
            {
                $.fancybox.open([
                    {
                        type: 'inline',
                        autoScale: true,
                        minHeight: 30,
                        content: "<p class='fancybox-error'>" + error + '</p>'
                    }],
                        {
                            padding: 0
                        });
            }
            else
                alert(error);
        }
    });
}
