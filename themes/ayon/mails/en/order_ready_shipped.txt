
[{shop_url}] 

Bonjour {firstname} {lastname},

Votre commande est prête pour l'expédition.

Votre commande ayant la référence {order_name} est prête pour l'expédition. Elle va bientôt être expédié.

Merci d'avoir effectué vos achats sur {shop_name}! 		 

Vous pouvez accéder à tout moment au suivi de votre commande et
télécharger votre facture dans "Historique des commandes"
[{history_url}] de la rubrique "Mon compte"
[{my_account_url}] sur notre site. 

{shop_name} [{shop_url}] réalisé avec amour

