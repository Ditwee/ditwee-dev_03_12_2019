<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_988659f6c5d3210a3f085ecfecccf5d3'] = 'Eigener CMS-Block';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_cd4abd29bdc076fb8fabef674039cd6e'] = 'Fügt dem Shop eigene CMS-Blocks hinzu';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_d52eaeff31af37a4a7e0550008aff5df'] = 'Während des Speichervorgangs ist ein Fehler aufgetreten';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_6f16c729fadd8aa164c6c47853983dd2'] = 'Neuer eigener CMS-Block';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_9dffbf69ffba8bc38bc4e01abf4b1675'] = 'Text';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_c9cc8cce247e49bae79f15173ce97354'] = 'Speichern';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_630f6dc397fe74e52d5189e2c80f282b'] = 'Zurück zur Übersicht';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_9d55fc80bbb875322aa67fd22fc98469'] = 'Shop-Zugehörigkeit.';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_6fcdef6ca2bb47a0cf61cd41ccf274f4'] = 'ID Block';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_9f82518d468b9fee614fcc92f76bb163'] = 'Shop';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_56425383198d22fc8bb296bcca26cecf'] = 'Text';
$_MODULE['<{blockcmsinfo}modez>blockcmsinfo_ef61fb324d729c341ea8ab9901e23566'] = 'Neu';
