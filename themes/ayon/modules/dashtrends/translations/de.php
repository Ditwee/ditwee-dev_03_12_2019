<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{dashtrends}modez>dashtrends_11ff9f68afb6b8b5b8eda218d7c83a65'] = 'Umsatz';
$_MODULE['<{dashtrends}modez>dashtrends_7442e29d7d53e549b78d93c46b8cdcfc'] = 'Bestellungen';
$_MODULE['<{dashtrends}modez>dashtrends_8c804960da61b637c548c951652b0cac'] = 'Durchschnittlicher Warenkorb';
$_MODULE['<{dashtrends}modez>dashtrends_d7e637a6e9ff116de2fa89551240a94d'] = 'Besuche';
$_MODULE['<{dashtrends}modez>dashtrends_e4c3da18c66c0147144767efeb59198f'] = 'Konversionsrate';
$_MODULE['<{dashtrends}modez>dashtrends_43d729c7b81bfa5fc10e756660d877d1'] = 'Nettogewinn';
$_MODULE['<{dashtrends}modez>dashtrends_46418a037045b91e6715c4da91a2a269'] = '%s (letzte Periode)';
$_MODULE['<{dashtrends}modez>dashboard_zone_two_2938c7f7e560ed972f8a4f68e80ff834'] = 'Übersicht';
$_MODULE['<{dashtrends}modez>dashboard_zone_two_f1206f9fadc5ce41694f69129aecac26'] = 'Einstellungen';
$_MODULE['<{dashtrends}modez>dashboard_zone_two_11ff9f68afb6b8b5b8eda218d7c83a65'] = 'Umsatz';
$_MODULE['<{dashtrends}modez>dashboard_zone_two_7442e29d7d53e549b78d93c46b8cdcfc'] = 'Bestellungen';
$_MODULE['<{dashtrends}modez>dashboard_zone_two_791d6355d34dfaf60d68ef04d1ee5767'] = 'Warenkorbwert';
$_MODULE['<{dashtrends}modez>dashboard_zone_two_d7e637a6e9ff116de2fa89551240a94d'] = 'Besuche';
$_MODULE['<{dashtrends}modez>dashboard_zone_two_e4c3da18c66c0147144767efeb59198f'] = 'Konversionsrate';
$_MODULE['<{dashtrends}modez>dashboard_zone_two_43d729c7b81bfa5fc10e756660d877d1'] = 'Nettogewinn';
