{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$seller_link|escape:'html':'UTF-8'}">
        {$seller->name|escape:'html':'UTF-8'}
    </a>
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {l s='Products' mod='jmarketplace'}
    </span>
{/capture}
<h1 class="page-heading product-listing">{l s='Products of' mod='jmarketplace'} {$seller->name|escape:'html':'UTF-8'}</h1>

{if isset($products) && $products}
			
    <div class="content_sortPagiBar cat_top clearfix">
		<div class="sortPagiBar clearfix">
			{include file="$tpl_dir./product-sort.tpl"}
			{include file="$tpl_dir./product-compare.tpl"}
			<div class="product-count">
				{if ($n*$p) < $nb_products }
					{assign var='productShowing' value=$n*$p}
				{else}
					{assign var='productShowing' value=($n*$p-$nb_products-$n*$p)*-1}
				{/if}
				{if $p==1}
					{assign var='productShowingStart' value=1}
				{else}
					{assign var='productShowingStart' value=$n*$p-$n+1}
				{/if}
				{if $nb_products > 1}{l s='Showing %1$d - %2$d of %3$d items' sprintf=[$productShowingStart, $productShowing, $nb_products] mod='jmarketplace'}{else}{l s='Showing %1$d - %2$d of 1 item' sprintf=[$productShowingStart, $productShowing] mod='jmarketplace'}{/if}
			</div>
		</div>
	</div>
			
    {if $ps_version != '1.7'}    
        {include file="$tpl_dir./product-list.tpl"  colfix=3}
    {else}
        {include file="$tpl_dir./product-list.tpl" class='jmarketplace-products' id='seller_products' products=$products}
    {/if}
	
	<div class="content_sortPagiBar cat_bottom">

		<div class="sortPagiBar clearfix">
			{include file="$tpl_dir./product-sort.tpl"}
			{include file="$tpl_dir./product-compare.tpl"}
			<div class="product-count">
				{if ($n*$p) < $nb_products }
					{assign var='productShowing' value=$n*$p}
				{else}
					{assign var='productShowing' value=($n*$p-$nb_products-$n*$p)*-1}
				{/if}
				{if $p==1}
					{assign var='productShowingStart' value=1}
				{else}
					{assign var='productShowingStart' value=$n*$p-$n+1}
				{/if}
				{if $nb_products > 1}{l s='Showing %1$d - %2$d of %3$d items' sprintf=[$productShowingStart, $productShowing, $nb_products] mod='jmarketplace'}{else}{l s='Showing %1$d - %2$d of 1 item' sprintf=[$productShowingStart, $productShowing] mod='jmarketplace'}{/if}
			</div>
		</div>

		<div class="bottom-pagination-content clearfix {if !($nb_products > $products_per_page && $start!=$stop)}no-border{/if}">
			{include file="$tpl_dir./nbr-product-page.tpl"} 
			{include file="$tpl_dir./pagination.tpl" paginationId='bottom'}
		</div>
	</div>

{else}
<ul class="tab-pane">
    <li class="alert alert-info">{l s='This seller have not products.' mod='jmarketplace'}</li>
</ul>
{/if}