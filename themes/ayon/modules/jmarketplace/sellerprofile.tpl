{capture name=path}
    {if $seller_me}
		<a href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}">
            {l s='Your seller account' mod='jmarketplace'}
        </a>
    {else}
        <a href="{$link->getModuleLink('jmarketplace', 'sellers', array(), true)|escape:'html':'UTF-8'}">
            {l s='Sellers' mod='jmarketplace'}
        </a>
    {/if}
    <span class="navigation-pipe">
        {$navigationPipe|escape:'html':'UTF-8'}
    </span>
    <span class="navigation_page">
        {$seller->name|escape:'html':'UTF-8'} 
    </span>
{/capture}


<div class="container-fluid DitweeBoutiquesHeader" >    

    <div class="row">
      <div class="col-lg-12" >
       <div class="square">
          <div class="grid">
            <div class="effect-hover-header" >
            <div class="backgroundimageboutique" style="    background-image: url('{$photo2|escape:'html':'UTF-8'}');
    height: 420px;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;"></div>
              <div id="caption">
                <h1>{$seller->name|escape:'html':'UTF-8'}</h1>
              </div>     
            </div>
          </div>
        </div>
      </div>
    </div>
	
</div>
  
    <div class="row DitweeBoutique" >

	{if $show_logo }
    <div class="DitweeBoutiqueLogo" >
        <div class="logo">
         <img class="img-responsive seller-manager-photo" alt="{$seller->name|escape:'html':'UTF-8'}" src="{$photo|escape:'html':'UTF-8'}" />
        </div>
      </div>
	 {/if}
     

	    {if $show_description}
     <div class="col-md-6 col-lg-8 DitweeBoutiqueLogoDescription" >
		{$seller->description nofilter}
		
	</div>
		
  {/if}
  
      <div class="col-md-6 col-lg-4 DitweeBoutiqueInfo" >
        <div class="BlocInfoDitwee">
          <h2>{$seller->name}</h2> 

{if $show_address && $seller->show_address}		  
          <iframe style="pointer-events:auto" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDf5h2YzvI6pgjD2DdrJA3HMgISiYo9V94&q={$seller->address|escape:'url':'UTF-8'},{$seller->postcode|escape:'url':'UTF-8'}"  height="200"></iframe>
          <div id="Info">
{/if}		  
		 
		 <div class="icone"><i class="ft-icon-shop-1" style="float: left;    margin-right: 10px; font-size: 21px;color: #bf9e54;"></i>
		
		<a href="{$seller_products_link|escape:'html':'UTF-8'}">
        {l s='View products' mod='jmarketplace'}
        
      </a>
	  
		 {if $show_seller_favorite}
          
		  
		  <div class="icone"><i class="ft-icon-heart" style="float: left;    margin-right: 10px; font-size: 21px;color: #bf9e54;"></i>
		  <a title="{l s='Add to favorite seller' mod='jmarketplace'}" href="{$url_favorite_seller|escape:'html':'UTF-8'}">
            {l s='Add to favorite seller' mod='jmarketplace'}
            <i class="ft-icon-heart" aria-hidden="true"></i>
			
          </a>
		  </div>
		  
        {/if}
		
		

		{if $show_seller_rating}
        <div class="average_rating buttons_bottom_block">
          <a href="{$url_seller_comments|escape:'html':'UTF-8'}" title="{l s='View comments about' mod='jmarketplace'} {$seller->name|escape:'html':'UTF-8'}">
            {section name="i" start=0 loop=5 step=1}
              {if $averageMiddle le $smarty.section.i.index}
                <div class="star"></div>
              {else}
                <div class="star star_on"></div>
              {/if}
            {/section}
            (<span id="average_total">{$averageTotal|intval}</span>)
          </a>
        </div>
        {/if}
		
		
	  
		<div  class="icone"><i class="ft-icon-mail" style="float: left;    margin-right: 10px; font-size: 21px;color: #bf9e54;"></i>
		<a href="{$link->getPageLink('contact')}?seller={$seller->id}" 
title="{l s='Contacter la boutique' mod='jmarketplace'}">{l s='Contacter la boutique' mod='jmarketplace'}</a>
</div>

		
           {if $show_address && $seller->show_address}
          <p></p>
		  <div class="icone">
            <i class="ft-icon-location-1" style="float: left;    margin-right: 10px; height: 80px;font-size: 21px;color: #bf9e54;"></i>{$seller->address|escape:'html':'UTF-8'}
			{if $show_postcode  && $seller->show_address}
			  <br/>{$seller->postcode|escape:'html':'UTF-8'}
			{/if}
			{if $show_city  && $seller->show_address}
			  <br/>{$seller->city|escape:'html':'UTF-8'}
			{/if}
			 {if $show_country  && $seller->show_address}
			  <br/>{$seller->country|escape:'html':'UTF-8'}
			{/if}
		
            </div>
			
        {/if}
		
            {if $show_phone && 1==0}
          
		  
		  <div class="icone"><i class="ft-icon-phone" style="float: left;    margin-right: 10px; font-size: 21px;color: #bf9e54;"></i><a href="tel:{$seller->phone|replace:' ':''|escape:'html':'UTF-8'}">{$seller->phone|escape:'html':'UTF-8'}</a></div>
		  
        {/if}
		
		
			
			{if $workinghours && $seller->show_address}
	
		
			<div class="icone">
              <i class="ft-icon-clock" style="float: left;margin-right: 10px;height: 110px; font-size: 21px;color: #bf9e54;"></i>
			  <p>
			{foreach from=$workinghours item=days}
			
				{$days->day} :
				{if $days->isActive}
					{$days->timeFrom}-{$days->timeTill}
					{if $days->isBreak} / {$days->timeFrom2}-{$days->timeTill2}{/if}
				{else}
					{l s='Close' mod='jmarketplace'}
				{/if}
			<br/>
			{/foreach}
			</p>
		</div>
		{/if}
		
            
           

          </div>  

      </div>

    </div>


  </div>

	  
	
    
    {hook h='displayMarketplaceTableProfile'}

{if $show_new_products && isset($products) && $products}
    <div class="row">
      <h2 class="page-heading">{l s='A %s products selection' sprintf={$seller->name|escape:'html':'UTF-8'} mod='jmarketplace'} </h2>
   
	  <div id="left_column" class="column col-xs-12 col-sm-3">{$HOOK_LEFT_COLUMN}</div>
	  <div id="center_column" class="center_column col-xs-12 col-sm-9">
		  
		  
		  <div class="content_sortPagiBar cat_top clearfix">
			<div class="sortPagiBar clearfix">
				{include file="$tpl_dir./product-sort.tpl"}
				{include file="$tpl_dir./product-compare.tpl"}
				<div class="product-count">
					{if ($n*$p) < $nb_products }
						{assign var='productShowing' value=$n*$p}
					{else}
						{assign var='productShowing' value=($n*$p-$nb_products-$n*$p)*-1}
					{/if}
					{if $p==1}
						{assign var='productShowingStart' value=1}
					{else}
						{assign var='productShowingStart' value=$n*$p-$n+1}
					{/if}
					{if $nb_products > 1}{l s='Showing %1$d - %2$d of %3$d items' sprintf=[$productShowingStart, $productShowing, $nb_products] mod='jmarketplace'}{else}{l s='Showing %1$d - %2$d of 1 item' sprintf=[$productShowingStart, $productShowing] mod='jmarketplace'}{/if}
				</div>
			</div>
		</div>
				
		{if $ps_version != '1.7'}    
			{include file="$tpl_dir./product-list.tpl"  colfix=3}
		{else}
			{include file="$tpl_dir./product-list.tpl" class='jmarketplace-products' id='seller_products' products=$products}
		{/if}
		
		<div class="content_sortPagiBar cat_bottom">

			<div class="sortPagiBar clearfix">
				{include file="$tpl_dir./product-sort.tpl"}
				{include file="$tpl_dir./product-compare.tpl"}
				<div class="product-count">
					{if ($n*$p) < $nb_products }
						{assign var='productShowing' value=$n*$p}
					{else}
						{assign var='productShowing' value=($n*$p-$nb_products-$n*$p)*-1}
					{/if}
					{if $p==1}
						{assign var='productShowingStart' value=1}
					{else}
						{assign var='productShowingStart' value=$n*$p-$n+1}
					{/if}
					{if $nb_products > 1}{l s='Showing %1$d - %2$d of %3$d items' sprintf=[$productShowingStart, $productShowing, $nb_products] mod='jmarketplace'}{else}{l s='Showing %1$d - %2$d of 1 item' sprintf=[$productShowingStart, $productShowing] mod='jmarketplace'}{/if}
				</div>
			</div>

			<div class="bottom-pagination-content clearfix {if !($nb_products > $products_per_page && $start!=$stop)}no-border{/if}">
				{include file="$tpl_dir./nbr-product-page.tpl"} 
				{include file="$tpl_dir./pagination.tpl" paginationId='bottom'}
			</div>
		</div>
		  
	
	  </div>
    </div>
{/if}
{hook h='displayMarketplaceFooterProfile'}

<!--
{if $seller_me}
    {*include file="./footer.tpl"*}
{else}
  <ul class="footer_links clearfix">
    <li style="margin-bottom:1px;">
	
      <a class="btn btn-default" href="javascript: history.go(-1)">
        {l s='Go back' mod='jmarketplace'}
      </a>
    </li>
    <li class="pull-right">
      <a class="btn btn-default" href="{$seller_products_link|escape:'html':'UTF-8'}">
        {l s='View products of' mod='jmarketplace'}
        {$seller->name|escape:'html':'UTF-8'}
      </a>
    </li>
  </ul>
{/if}
-->

<style>

    .container-fluid{
      margin: 0px;
      padding: 0px;
    }
 
    .row{
      margin:0;
      padding:0;
    }
    .square{
    float:left;
    position: relative;
    width: 100%;
    overflow:hidden;
    }

    .DitweeBoutiqueLogo .logo img{
          border: 1px solid #bf9e54;
    }
    @media (min-width: 991px){
      .DitweeBoutiqueLogo .logo img{
          width: 80%;
    }
    }
    @media (max-width: 991px){
      .DitweeBoutiqueLogo .logo img{
          width: 50%;
    }
    }

   .DitweeBoutique{
      margin-top: 6em;
      margin-bottom: 6em;
    }
    .DitweeBoutiqueLogo{
      margin-left: auto;
    margin-right: auto;
    text-align: center;
        padding-left: 14px !important;
padding-right: 14px !important;
    }
    .DitweeBoutiqueInfo{

    }
    .DitweeBoutiqueInfo .BlocInfoDitwee{

    /* box-sizing: border-box; */
    border: 1px solid #bf9e54;

    }
    .BlocInfoDitwee {
          padding-bottom: 10px;
    }
	.BlocInfoDitwee iframe{
	width:100%
	
	}
	
    .BlocInfoDitwee h2{
         margin-left: 10px;
    /* margin-top: 10px; */
    font-size: 21px;

    }
    .BlocInfoDitwee #Info{
      font-size: 12px;
      margin-left: 10px;
      margin-bottom: 10px;
    }
    .DitweeBoutiqueInfo #adresse{
      margin-top:15px;

    }
 
	
	.DitweeBoutiqueInfo .icone{
      margin-top:15px;
    }
	

    .DitweeBoutiqueInfo h2 {
          color: #bf9e54;
    }

    .DitweeBoutiqueLogoDescription h2{
          color: #bf9e54;
    }
    .DitweeBoutiqueLogoDescription p{
        text-align: justify;
    }
    @media (max-width: 991px){
      .BlocInfoDitwee{
        margin-top: 40px;
      }
    }





.grid {
  position: relative;
  clear: both;
  margin: 0 auto;
  list-style: none;
  text-align: center;
}

/* Common style */
.grid .effect-hover-header {
  background: black;
  text-align: center;
}

.grid .effect-hover-header img {
  position: relative;
  display: block;
  min-height: 100%;
  max-width: 100%;
  opacity: 0.8;
}

.grid .effect-hover-header #caption {
  padding: 2em;
  color: #fff;
  text-transform: uppercase;
  font-size: 1.25em;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
}

.grid .effect-hover-header #caption::before,
.grid .effect-hover-header #caption::after {
  pointer-events: none;
}

.grid .effect-hover-header #caption,
.grid .effect-hover-header #caption > a {
  position: absolute;
      top: 40%;
  left: 0;
  width: 100%;
  height: 100%;
}

/* Anchor will cover the whole item by default */
/* For some effects it will show as a button */
.grid .effect-hover-header #caption > a {
  z-index: 1000;
  text-indent: 200%;
  white-space: nowrap;
  font-size: 0;
  opacity: 0;
}

.grid .effect-hover-header h2 {
  word-spacing: -0.15em;
  font-weight: 300;
  color: #bf9e54;
}

.grid .effect-hover-header h2 span {
  font-weight: 800;
}

.grid .effect-hover-header h2,
.grid .effect-hover-header p {
  margin: 0;
}

.grid .effect-hover-header p {
  letter-spacing: 1px;
  font-size: 68.5%;
}

/*---------------*/
/***** hover *****/
/*---------------*/
.effect-hover-header {
    margin: 0 0 0rem !important;
}
.effect-hover-header h1{
  display: inline-block;
    margin: 0 0 0.25em;
    padding: 0.4em 1em;
    background: #1c2021;
    color: #ffffff;
    text-transform: none;
    font-weight: 500;
    font-size: 25px !important;
}

.backgroundimageboutique {
  background: #2f3238;
}

.backgroundimageboutique {
  max-width: none;
  width: 100%;
  -webkit-transition: opacity 1s, -webkit-transform 1s;
  transition: opacity 1s, transform 1s;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
}

.effect-hover-header #caption {

}

.effect-hover-header h2 {
  position: relative;
  padding: 0.5em 0;
  color: #bf9e54;
}

.effect-hover-header p {
  display: inline-block;
  color: white;
  text-transform: none;
  font-weight: bold;
  font-size: 100%;
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(-360px,0,0);
      transform: translate3d(-500px,0,0);

}

.effect-hover-header p:first-child {
  -webkit-transition-delay: 0.15s;
  transition-delay: 0.15s;
}

.effect-hover-header p:nth-of-type(2) {
  -webkit-transition-delay: 0.1s;
  transition-delay: 0.1s;
}

.effect-hover-header p:nth-of-type(3) {
  -webkit-transition-delay: 0.05s;
  transition-delay: 0.05s;
}

.effect-hover-header:hover p:first-child {
  -webkit-transition-delay: 0s;
  transition-delay: 0s;
}

.effect-hover-header:hover p:nth-of-type(2) {
  -webkit-transition-delay: 0.05s;
  transition-delay: 0.05s;
}

.effect-hover-header:hover p:nth-of-type(3) {
  -webkit-transition-delay: 0.1s;
  transition-delay: 0.1s;
}

.effect-hover-header:hover .backgroundimageboutique{
  opacity: 0.4;
  -webkit-transform: scale3d(1.1,1.1,1);
  transform: scale3d(1.1,1.1,1);
}

.effect-hover-header:hover p {
  opacity: 1;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}

#module-jmarketplace-sellerprofile .seller-manager-photo{
    margin: -121px 20px 7px 0;
	position:relative;
}

</style>