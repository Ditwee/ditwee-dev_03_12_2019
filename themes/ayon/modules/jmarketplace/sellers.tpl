{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


	

{capture name=path}
    <span class="navigation_page">
        {l s='Sellers' mod='jmarketplace'}
    </span>
{/capture}




<div class="container-fluid DitweeVendeurBloc" >


 {if isset($sellers) && $sellers}
	
	<h1 class="page-heading product-listing">
	{l s='Sellers in Marketplace' mod='jmarketplace'}
    {strip}
		<span class="heading-counter">
		
			{if $sellers|@count == 0}{l s='There are no sellers.' mod='jmarketplace'}
			{else}
				{if $sellers|@count == 1}
					{l s='There is 1 seller'  mod='jmarketplace' }
				{else}
					{l s='There are %d sellers' sprintf=$sellers|@count mod='jmarketplace'}
				{/if}
			{/if}
		</span>
    {/strip}
	</h1>

	

		<div class="row">
			<div class="col-xs-12">
				<div id="map-ditwee" style="width: 100%;height: 400px;margin-bottom:20px" class=""></div>
			</div>
		</div>

    <div class="row" style="margin-bottom: 6em;">
	
	 {foreach from=$sellers item=seller name=jmarketplaceSellers}
	 
      <!-- Bloc gaucheZZ -->
  
	  
	  
	   <div class="col-lg-6" >
       <div class="square">
          <a href="{$seller.url|escape:'html':'UTF-8'}">
            <div class="blocBoutique">
              <div class="effect-hover-header" style="background-image: url('{$seller.photo2}')">
				</div>
            </div>
          </a>
        </div>
        <a href="{$seller.url|escape:'html':'UTF-8'}">
          <div class="DitweeVendeurInfo">
            <div class="logo">
              <img src="{$seller.photo}" alt="{$seller.name|escape:'html':'UTF-8'}" />
            </div>
            <div class="info">
              <h3>{$seller.name|escape:'html':'UTF-8'}</h3>
              <p>{$seller.shop|escape:'html':'UTF-8'}</p>
            </div>
          </div>
        </a>
      </div>
	  
	{/foreach}
	</div>
{else}
        <p class="alert alert-info">{l s='There are not sellers.' mod='jmarketplace'}</p>
    {/if}
	
</div>
{if isset($sellers) && $sellers}
  
	<script type="text/javascript">
		//<![CDATA[
    var sellers = new Array();
	sellers = {$sellers|@json_encode nofilter}

  
  {literal}
	var activeInfoWindow;
	$(document).ready(function(){
    initMap();
});


	function initMap() {
	
		var map = new google.maps.Map(document.getElementById('map-ditwee'), {
			center: {lat: 45.757, lng: 4.765},
			zoom: 5,
			/*gestureHandling: 'greedy'*/
		});
	
		var myloc = new google.maps.Marker({
			clickable: false,
			icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png',
				new google.maps.Size(22,22),
				new google.maps.Point(0,18),
				new google.maps.Point(11,11)),
			shadow: null,
			zIndex: 999,
			map: map// your google.maps.Map object
		});
		
		var image = {
		  url: '/img/icon_marker_google_ditwee.png',
		  // This marker is 20 pixels wide by 32 pixels high.
		  //size: new google.maps.Size(32, 32),
		  // The origin for this image is (0, 0).
		  //origin: new google.maps.Point(0, 0),
		  // The anchor for this image is the base of the flagpole at (0, 32).
		  //anchor: new google.maps.Point(0, 32)
		};
		
		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
				var pos = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
	
				var me = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				myloc.setPosition(me);
	
				map.setCenter(pos);
			}, function() {
			//handleLocationError(true, infoWindow, map.getCenter());
		});
		}
		else {
			// Browser doesn't support Geolocation
			handleLocationError(false, infoWindow, map.getCenter());
		}

		geocoder = new google.maps.Geocoder();

		for (var x = 0; x < sellers.length; x++) {
		
			var seller = sellers[x];
			geocode(seller,x,image,map);
			
			
			
		}
	}

	function handleLocationError(browserHasGeolocation, infoWindow, pos) {
		infoWindow.setPosition(pos);
		infoWindow.setContent(browserHasGeolocation ?
			'Error: The Geolocation service failed.' :
			'Error: Your browser doesn\'t support geolocation.');
	}
	
function geocode(seller,x,image,map){

	var ad = seller.address + "," + seller.postcode + "," + seller.country;
	if(seller.show_address==0) ad = seller.postcode + "," + seller.country;
	geocoder.geocode({'address':ad}, function(seller, x){
		return(function(results, status){
			
			if (status == google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
				setTimeout( function(){
					geocode(seller,x,image,map);
				}, 50);
			}	
			if (status == google.maps.GeocoderStatus.OK) {
				var latlng = results[0].geometry.location;
	
				var texte = '<div id="iw-container" style="max-height:150px">' +
					'<a href="'+seller.url+'"><img src="'+seller.photo+'" alt="'+seller.name+'" height="115" width="83">'+seller.name+'</a>' +
					'<div class="iw-content">';
					if(seller.show_address==1)
						texte+= seller.address + '<br/>' + seller.postcode + ' ' + seller.city;
					
					//texte+= '<br/><a href="tel://'+seller.phone+'">'+seller.phone+'</a>'+
					'<br/>' + seller.description +
					'</div>' +
					'</div>'
						
				var infowindow = new google.maps.InfoWindow({
					content : texte
				});

				var marker = new google.maps.Marker({
					position: latlng,
					title: seller.name,
					map: map,
					icon: image
				});
				
				marker.addListener('click', function() {
					activeInfoWindow&&activeInfoWindow.close();
					infowindow.open(map, marker);
					activeInfoWindow = infowindow;
				});


			}
		});
	}(seller, x));	
			
}

  {/literal}
//]]>
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDf5h2YzvI6pgjD2DdrJA3HMgISiYo9V94" defer async></script>




{/if}