<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{royblocklayered}ayon>blocklayered-no-products_5c9838becf9bbce28ba90a7426daf171'] = 'Il n\'y a aucun produit';
$_MODULE['<{royblocklayered}ayon>blocklayered_d7778d0c64b6ba21494c97f77a66885a'] = 'Filtres';
$_MODULE['<{royblocklayered}ayon>blocklayered_1262d1b9fbffb3a8e85ac9e4b449e989'] = 'Filtres actifs';
$_MODULE['<{royblocklayered}ayon>blocklayered_ea4788705e6873b424c65e91c2846b19'] = 'Supprimer';
$_MODULE['<{royblocklayered}ayon>blocklayered_b47b72ddf8a3fa1949a7fb6bb5dbc60c'] = 'Aucun filtre';
$_MODULE['<{royblocklayered}ayon>blocklayered_5da618e8e4b89c66fe86e32cdafde142'] = 'De';
$_MODULE['<{royblocklayered}ayon>blocklayered_01b6e20344b68835c5ed1ddedf20d531'] = 'à';
$_MODULE['<{royblocklayered}ayon>blocklayered_146ffe2fd9fa5bec3b63b52543793ec7'] = 'Voir plus';
$_MODULE['<{royblocklayered}ayon>blocklayered_c74ea6dbff701bfa23819583c52ebd97'] = 'Voir moins';
$_MODULE['<{royblocklayered}ayon>blocklayered_8524de963f07201e5c086830d370797f'] = 'Chargement...';
$_MODULE['<{royblocklayered}ayon>royblocklayered_3601146c4e948c32b6424d2c0a7f0118'] = 'Prix';
$_MODULE['<{royblocklayered}ayon>royblocklayered_8c489d0946f66d17d73f26366a4bf620'] = 'Poids';
$_MODULE['<{royblocklayered}ayon>royblocklayered_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nouveau';
$_MODULE['<{royblocklayered}ayon>royblocklayered_019d1ca7d50cc54b995f60d456435e87'] = 'Usagé';
$_MODULE['<{royblocklayered}ayon>royblocklayered_6da03a74721a0554b7143254225cc08a'] = 'Reconditionné';
$_MODULE['<{royblocklayered}ayon>royblocklayered_9e2941b3c81256fac10392aaca4ccfde'] = 'Condition';
$_MODULE['<{royblocklayered}ayon>royblocklayered_2d25c72c1b18e562f6654fff8e11711e'] = 'Non disponible';
$_MODULE['<{royblocklayered}ayon>royblocklayered_fcebe56087b9373f15514831184fa572'] = 'En stock';
$_MODULE['<{royblocklayered}ayon>royblocklayered_faeaec9eda6bc4c8cb6e1a9156a858be'] = 'Disponibilité';
$_MODULE['<{royblocklayered}ayon>royblocklayered_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Marques';
$_MODULE['<{royblocklayered}ayon>royblocklayered_af1b98adf7f686b84cd0b443e022b7a0'] = 'Catégories';
$_MODULE['<{royblocklayered}ayon>royblocklayered_78a5eb43deef9a7b5b9ce157b9d52ac4'] = 'prix';
$_MODULE['<{royblocklayered}ayon>royblocklayered_7edabf994b76a00cbc60c95af337db8f'] = 'poids';
