<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_e267e2be02cf3e29f4ba53b5d97cf78a'] = 'Adresse email non valide.';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_1c623b291d95f4f1b1d0c03d0dc0ffa1'] = 'Cet email est déjà enregistrée';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_3b1f17a6fd92e35bc744e986b8e7a61c'] = 'Une erreur est apparue.';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_d4197f3e8e8b4b5d06b599bc45d683bb'] = 'Votre désinscription a bien été prise en compte';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_b48cc4561e44f028e9ebfefbe422a4ef'] = 'Adresse email non valide.';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_f6618fce0acbfca15e1f2b0991ddbcd0'] = 'Cet email est déjà inscrit chez nous';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_e172cb581e84f43a3bd8ee4e3b512197'] = 'Une erreur est apparue.';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_ebc069b1b9a2c48edfa39e344103be1e'] = 'Une email de vérification vous a été envoyé. Merci de vérifier votre boite aux lettres et de la valider.';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_77c576a354d5d6f5e2c1ba50167addf8'] = 'Vous vous êtes abonné à notre newsletter avec succès! Merci, Thanks, , Gracias, Danke, 谢谢, Obrigado !';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_99c8b8e5e51bf8f00f1d66820684be9a'] = 'Cet email est déjà enregistré et/ou n\'est pas valide';
$_MODULE['<{royblocknewsletter}ayon>royblocknewsletter_4e1c51e233f1ed368c58db9ef09010ba'] = 'Merci pour l\'inscription à notre newsletter.';
