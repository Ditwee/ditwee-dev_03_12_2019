
<li class="hcc1" data-auto="{$roythemes.nc_auto_custom1}" data-max-slides="{$roythemes.nc_items_custom1}" data-nomargins="{$roythemes.nomargins_custom1}">
{if isset($products) && $products}
    <div class="home_products_title">
        {if RoyHomeCategory1::displayCategoryId() != 1 && RoyHomeCategory1::displayCategoryId() != 2}
            <a href="{$link->getCategoryLink(RoyHomeCategory1::displayCategoryId(), RoyHomeCategory1::displayCategoryLink())}"><span>{RoyHomeCategory1::displayCategoryName()}</span></a>
        {else}
            <span>{RoyHomeCategory1::displayCategoryName()}</span>
        {/if}
		<h3 class="undertitle">{l s='Check our best products' mod='royhomecategory1'}</h3>
    </div>
    <div class="rv_carousel_container">
	{include file="$tpl_dir./product-list.tpl" class="carousel-home {if isset($roythemes.nc_carousel_custom1) && $roythemes.nc_carousel_custom1 == "1"}car-custom1{/if} royhomecategory1 plc_{if isset($roythemes.nc_plc_cc1)}{$roythemes.nc_plc_cc1}{/if} items_{if isset($roythemes.nc_items_custom1)}{$roythemes.nc_items_custom1}{/if} tab-pane" id='royhomecategory1'}
    </div>
{else}
<ul id="royhomecategory1" class="carousel-home royhomecategory1 tab-pane">
	<li class="alert alert-info">{l s='No products at this time.' mod='royhomecategory1'}</li>
</ul>
{/if}
</li>