{*

* 2015 RoyThemes Copyright. This module is part of Planetes Theme. You can't copy or distibute it. You can't install it separatelly of Planetes theme and use it on another domain.

*}



<!-- Roy LeviBox -->

<div id="roy_levibox" class="roy_levibox">

        {if isset($box_fb) && $box_fb}

            <div class="box-one box-fb">

                <i class="box-current box-fb-i"></i>

                <div class="box-content box-fb-content">

                    <script>$(document).ready(function() {

                            initfb(document, 'script', 'facebook-jssdk');

                        });

                        function initfb(d, s, id)

                        {

                            var js, fjs = d.getElementsByTagName(s)[0];

                            if (d.getElementById(id))

                                return;

                            js = d.createElement(s); js.id = id;

                            js.src = "//connect.facebook.net/{$box_fb_lan}/all.js#xfbml=1&appId=334341610034299";

                            fjs.parentNode.insertBefore(js, fjs);

                        }</script>

                    <h4 >{l s='Follow us on Facebook' mod='roylevibox'}</h4>

                    <div class="facebook-fanbox">

                        <div class="fb-like-box" data-width="256" data-href="{$box_facebook_url}" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false">

                        </div>

                    </div>

                </div>

            </div>
            

        {/if}        

        {if isset($box_mail) && $box_mail}

            <div class="box-one box-mail"><i class="box-current"></i>

                <div class="box-content box-mail-content">

                    <script>

                        $('#contactable').contactable({

                            subject: '{$shop_name} Feedback message',

                            header: '{l s='Contact Us' mod='roylevibox'}',

                            url: '{$base_dir}modules/roylevibox/mail.php',

                            name: '{l s='Name' mod='roylevibox'}',

                            email: '{l s='Email' mod='roylevibox'}',

                            customermail: '{$cookie->email}',

                            message : '{l s='Message' mod='roylevibox'}',

                            submit : '{l s='SEND' mod='roylevibox'}',

                            recievedMsg : '{l s='Thank you for your message' mod='roylevibox'}',

                            notRecievedMsg : '{l s='Sorry but your message could not be sent, try again later.' mod='roylevibox'}',

                            footer: '{l s='Please feel free to get in touch, we will answer as soon as possible.' mod='roylevibox'}',

                            hideOnSubmit: true

                        });

                    </script>

                    <div id="contactable"></div>

                </div>

            </div>

        {/if}

        {if isset($box_arrow) && $box_arrow}

            <div class="box-one box-arrow">

                <i></i></div>

        {/if}

		<div class="box-one box-fb">  
			<a href="https://www.facebook.com/ditweeatoutesvosenvies/" target="_blank" class="fa fa-facebook levisocialmedia" title="Instagram Ditwee"></a>
		</div>
		
		<div class="box-one box-fb">  
			<a href="https://www.instagram.com/ditwee.fr/" target="_blank" class="fa fa-instagram levisocialmedia" title="Instagram Ditwee"></a>
		</div>
		<div class="box-one box-fb">
			
			<a href="https://www.youtube.com/channel/UCOvYM4qAaQld96X8maOw-9w" target="_blank" class="fa fa-youtube levisocialmedia" title="Youtube Ditwee"></a>
		</div>
			
		<div class="box-one box-fb visible-xs visible-sm" style="display:none!important;z-index:1000000;font-size:10px">
			<a id="go-layered_block_left" href="#" class="fa fa-filter levisocialmedia" style="z-index:1000000;" title="{l s='Filter' mod='roylevibox'}"></a>
		</div>
</div>

<!-- /Roy LeviBox -->