<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{smartblogfeed}ayon>smartblogfeed_c393a650824fd3ac8e147306a2a1b10c'] = 'Fil RSS SmartBlog.';
$_MODULE['<{smartblogfeed}ayon>smartblogfeed_f39479e80017ac7fdf59a5d92208f88e'] = 'Générer un flux RSS SmartBlog.';
$_MODULE['<{smartblogfeed}ayon>smartblogfeed_21ee0d457c804ed84627ec8345f3c357'] = 'Les paramètres ont été mis à jour avec succès.';
$_MODULE['<{smartblogfeed}ayon>smartblogfeed_70f2c15a2c9bf860a0de69d13c1f75ea'] = 'Tous les champs obligatoires';
$_MODULE['<{smartblogfeed}ayon>smartblogfeed_c54f9f209ed8fb4683e723daa4955377'] = 'Cadre général';
$_MODULE['<{smartblogfeed}ayon>smartblogfeed_0ff5174a1c0afd0f6b8f8e9495a7262e'] = 'Mise à jour Période';
$_MODULE['<{smartblogfeed}ayon>smartblogfeed_3d3294d044b2f8dc5fd6d34ca2dcd3ff'] = 'Mise à jour Fréquence';
$_MODULE['<{smartblogfeed}ayon>smartblogfeed_c07627a5fdfbe3e2044adc04bcd675b0'] = 'Mise à jour Durée';
$_MODULE['<{smartblogfeed}ayon>smartblogfeed_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
