<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{socialloginizer}ayon>socialloginizer_2fe6bc2ceee08ad4ce754e336bb5630a'] = 'Permet aux clients de se connecter au site web à travers diverses plates-formes sociales.';
$_MODULE['<{socialloginizer}ayon>socialloginizer_876f23178c29dc2552c0b48bf23cd9bd'] = 'Etes-vous sûr que vous souhaitez désinstaller?';
$_MODULE['<{socialloginizer}ayon>socialloginizer_564bc019cf433f02acc2d54c7b97cbf7'] = 'Les pouvoirs sont nécessaires pour permettre à Facebook Login';
$_MODULE['<{socialloginizer}ayon>socialloginizer_89e9d322181f99f9a0249f22c86b8f71'] = 'Les pouvoirs sont nécessaires pour permettre à Google connexion';
$_MODULE['<{socialloginizer}ayon>socialloginizer_9041069eb8b9ae2428e806ce7291a3f2'] = 'Les pouvoirs sont nécessaires pour activer la connexion en direct';
$_MODULE['<{socialloginizer}ayon>socialloginizer_887c3e3a81f7a815ca9cc41e0a63f213'] = 'Les pouvoirs sont nécessaires pour permettre linkedin connexion';
$_MODULE['<{socialloginizer}ayon>socialloginizer_a0426eb1b1c4f3555750328b4bd82fda'] = 'Les pouvoirs sont nécessaires pour permettre à Twitter Connectez-vous';
$_MODULE['<{socialloginizer}ayon>socialloginizer_8de64dca07c190f9a214a38dedfc8eca'] = 'Les pouvoirs sont nécessaires pour permettre à Yahoo connexion';
$_MODULE['<{socialloginizer}ayon>socialloginizer_341cca789e4e98f4f4b95b77316601f5'] = 'Les pouvoirs sont nécessaires pour permettre Instagram connexion';
$_MODULE['<{socialloginizer}ayon>socialloginizer_faf377742935b211d6ba3ff7b47183cd'] = 'Les pouvoirs sont nécessaires pour permettre à Amazon connexion';
$_MODULE['<{socialloginizer}ayon>socialloginizer_d4b0a24c4ba6565d7858c706739a4cd4'] = 'Les pouvoirs sont nécessaires pour activer la connexion paypal';
$_MODULE['<{socialloginizer}ayon>socialloginizer_9fccf693062bd729bfc47d1a73548cec'] = 'Les pouvoirs sont nécessaires pour activer la connexion foursquare';
$_MODULE['<{socialloginizer}ayon>socialloginizer_589c8e406f397a0048c57c9fe085d7b3'] = 'Les pouvoirs sont nécessaires pour permettre github connexion';
$_MODULE['<{socialloginizer}ayon>socialloginizer_f640fd7e57fb7c751fd5c9798becd4e9'] = 'Les pouvoirs sont nécessaires pour permettre Disqus connexion';
$_MODULE['<{socialloginizer}ayon>socialloginizer_e3316430037e78243ea2b2a3f5a7cffc'] = 'Les pouvoirs sont nécessaires pour permettre vkontakte connexion';
$_MODULE['<{socialloginizer}ayon>socialloginizer_c7193ed472e32b257d7301f2f9056583'] = 'Les pouvoirs sont nécessaires pour activer la connexion wordpress';
$_MODULE['<{socialloginizer}ayon>socialloginizer_791b2a6c069a8484d49efc6b79bc817b'] = 'Les pouvoirs sont nécessaires pour permettre la sélection connexion';
$_MODULE['<{socialloginizer}ayon>socialloginizer_5d9df15b593efc9e86a464bacc6aba8e'] = 'Réglages a été mis à jour avec succès';
$_MODULE['<{socialloginizer}ayon>socialloginizer_c0852e1aaea7b64f7fe27c967f335412'] = 'Créez un compte ou connectez-vous avec';
$_MODULE['<{socialloginizer}ayon>socialloginizer_d67850bd126f070221dcfd5fa6317043'] = 'Signer';
$_MODULE['<{socialloginizer}ayon>github_89abc3d6a6443bc9645974c55a58c3dc'] = 'Email existent déjà s\'il vous plaît choisir un autre';
$_MODULE['<{socialloginizer}ayon>instagram_89abc3d6a6443bc9645974c55a58c3dc'] = 'Email existent déjà s\'il vous plaît choisir un autre';
$_MODULE['<{socialloginizer}ayon>twitter_89abc3d6a6443bc9645974c55a58c3dc'] = 'Email existent déjà s\'il vous plaît choisir un autre';
$_MODULE['<{socialloginizer}ayon>vk_89abc3d6a6443bc9645974c55a58c3dc'] = 'Email existent déjà s\'il vous plaît choisir un autre';
$_MODULE['<{socialloginizer}ayon>ylogin_89abc3d6a6443bc9645974c55a58c3dc'] = 'Email existent déjà s\'il vous plaît choisir un autre';
$_MODULE['<{socialloginizer}ayon>loginizer_c9cc8cce247e49bae79f15173ce97354'] = 'sauvegarder';
$_MODULE['<{socialloginizer}ayon>loginizer_ea4788705e6873b424c65e91c2846b19'] = 'Annuler';
$_MODULE['<{socialloginizer}ayon>loginizer_52f4393e1b52ba63e27310ca92ba098c'] = 'réglages généraux';
$_MODULE['<{socialloginizer}ayon>loginizer_c33e404a441c6ba9648f88af3c68a1ca'] = 'Statistiques';
$_MODULE['<{socialloginizer}ayon>loginizer_c9d54fda1013c290a4ccc70423a2b62c'] = 'Paramètres Facebook';
$_MODULE['<{socialloginizer}ayon>loginizer_ef9ca24726e6c9973bd49dbfc68b83ab'] = 'Paramètres Google Plus';
$_MODULE['<{socialloginizer}ayon>loginizer_93f39c788c03d1a69d86fa4c6b452304'] = 'Réglages en direct';
$_MODULE['<{socialloginizer}ayon>loginizer_ea80521ce5e276470002e9d95cd8b146'] = 'Réglages LinkedIn';
$_MODULE['<{socialloginizer}ayon>loginizer_40d79d7103746ea893c5926dbc67f008'] = 'Twitter Paramètres';
$_MODULE['<{socialloginizer}ayon>loginizer_75c0b00557e101f7ec74ba75da1b86ca'] = 'Réglages Yahoo';
$_MODULE['<{socialloginizer}ayon>loginizer_3db0866d8ed2e1f7f20fe35c8909496c'] = 'Réglages Instagram';
$_MODULE['<{socialloginizer}ayon>loginizer_7f33809afdc8b3332d638d068834eeeb'] = 'Réglages Amazon';
$_MODULE['<{socialloginizer}ayon>loginizer_9b7012a33f36eb1054b8f58bb6009ac9'] = 'Réglages Paypal';
$_MODULE['<{socialloginizer}ayon>loginizer_b42f0515db1fc130888a38ef9286405d'] = 'Paramètres de Foursquare';
$_MODULE['<{socialloginizer}ayon>loginizer_7cb5e4bedd37264b05e14bf2dbbd40da'] = 'Réglages Github';
$_MODULE['<{socialloginizer}ayon>loginizer_29371d630d6d55ce728a94926394a407'] = 'Réglages Disqus';
$_MODULE['<{socialloginizer}ayon>loginizer_d18cc6355fedca8c64b3d0f5456704af'] = 'Réglages Vkontakte';
$_MODULE['<{socialloginizer}ayon>loginizer_d40f33d396a40b095cb998e3d13ea24c'] = 'Réglages Wordpress';
$_MODULE['<{socialloginizer}ayon>loginizer_0647af5130e2ac2de395e58f560f483e'] = 'Paramètres de Dropbox';
$_MODULE['<{socialloginizer}ayon>loginizer_483a5da192f46e3cfe8391d3ef234fc4'] = 'FAQs';
$_MODULE['<{socialloginizer}ayon>loginizer_41ecca7200a4431cf9b855a62e6ab666'] = 'Suggestions';
$_MODULE['<{socialloginizer}ayon>loginizer_2c28f18bcd5a82299a5bc177497083c0'] = 'Autres Plugins';
$_MODULE['<{socialloginizer}ayon>loginizer_e566fe9aef1502d69ccdbe28e1957535'] = 'Activer désactiver';
$_MODULE['<{socialloginizer}ayon>loginizer_53930ad37c04861438bae5174bc1eeef'] = 'Show Popup';
$_MODULE['<{socialloginizer}ayon>loginizer_ffb5e4c2dbca3f69de5cff1530346828'] = 'Afficher contextuel plutôt que d\'utiliser la redirection lorsque le client clique sur le bouton de connexion sociale';
$_MODULE['<{socialloginizer}ayon>loginizer_a2158739f32406ca2efe410eb094e9ad'] = 'Arrangements Button';
$_MODULE['<{socialloginizer}ayon>loginizer_a400bb2fe8823e80e34d1b927867231c'] = 'Modifier la position des boutons Loginizer';
$_MODULE['<{socialloginizer}ayon>loginizer_7eefa8756445f0b6d4752bbb835414b7'] = 'Glissez et déposez ces boutons pour changer l\'ordre de tri';
$_MODULE['<{socialloginizer}ayon>loginizer_a6710913e979ea201d8923d289573b9a'] = 'Voir sur';
$_MODULE['<{socialloginizer}ayon>loginizer_9992a6663f13a2b4e7b2f890ed39ecf4'] = 'SuperCheckout';
$_MODULE['<{socialloginizer}ayon>loginizer_3c5c60f18489f198d0c2e4d88dada7a2'] = 'Supercheckout est notre Une page module de caisse';
$_MODULE['<{socialloginizer}ayon>loginizer_1cf124095a544c1503f322881f956017'] = 'Ne montre pas';
$_MODULE['<{socialloginizer}ayon>loginizer_06d1525b412a91cffbc1a01bd336b9e5'] = 'Petits boutons';
$_MODULE['<{socialloginizer}ayon>loginizer_66a8ee8b9b86c753d0d8c9b6ff92ec71'] = 'Grands boutons';
$_MODULE['<{socialloginizer}ayon>loginizer_ee757807ac7b7cef95e604690f2929fc'] = 'CSS personnalisé';
$_MODULE['<{socialloginizer}ayon>loginizer_9691130dddb5f4df53c27c125ebe7f85'] = 'Fournir du code CSS pour des changements dans l\'extrémité avant de Socialloginizer';
$_MODULE['<{socialloginizer}ayon>loginizer_193cfc9be3b995831c6af2fea6650e60'] = 'Page';
$_MODULE['<{socialloginizer}ayon>loginizer_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{socialloginizer}ayon>loginizer_52f5e0bc3859bc5f5e25130b6c7e8881'] = 'Position';
$_MODULE['<{socialloginizer}ayon>loginizer_b3fcf61222a7fc54cca3c18ddcbfba30'] = 'Bouton Taille';
$_MODULE['<{socialloginizer}ayon>loginizer_cfe6e34a4c7f24aad32aa4299562f5b1'] = 'Page d\'accueil';
$_MODULE['<{socialloginizer}ayon>loginizer_2faec1f9f8cc7f8f40d521c4dd574f49'] = 'Activer';
$_MODULE['<{socialloginizer}ayon>loginizer_bcfaccebf745acfd5e75351095a5394a'] = 'Désactiver';
$_MODULE['<{socialloginizer}ayon>loginizer_bf50d5e661106d0abe925af3c2e6f7e7'] = 'Entête';
$_MODULE['<{socialloginizer}ayon>loginizer_ded40f2a77c30efc6062db0cbd857746'] = 'Bas de page';
$_MODULE['<{socialloginizer}ayon>loginizer_e4abb55720e3790fe55982fec858d213'] = 'La colonne de gauche';
$_MODULE['<{socialloginizer}ayon>loginizer_f16072c370ef52db2e329a87b5e7177a'] = 'Colonne de droite';
$_MODULE['<{socialloginizer}ayon>loginizer_3a69b34ce86dacb205936a8094f6c743'] = 'Grand';
$_MODULE['<{socialloginizer}ayon>loginizer_2660064e68655415da2628c2ae2f7592'] = 'Petit';
$_MODULE['<{socialloginizer}ayon>loginizer_175103bab005a71c49c7ef675c1304cd'] = 'Catégorie page';
$_MODULE['<{socialloginizer}ayon>loginizer_235e8d1a54ecddcf1d3ff533331ed416'] = 'Page Produit';
$_MODULE['<{socialloginizer}ayon>loginizer_4b57a7151ff7f3951defad2e80eac338'] = 'Connexion Page';
$_MODULE['<{socialloginizer}ayon>loginizer_8645fcfa76012b1d7a28349b6d66a2dc'] = 'Créer un formulaire compte';
$_MODULE['<{socialloginizer}ayon>loginizer_5a51395c0a2c6a14de05f99326b91565'] = 'Formulaire de connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_229be9f2ffd6f27c88a2ab10af9896d3'] = 'Cinq étapes Commander page';
$_MODULE['<{socialloginizer}ayon>loginizer_edcbc79cc649442a71a24053dc1ed868'] = 'Une page de la page Commander';
$_MODULE['<{socialloginizer}ayon>loginizer_87d49200bfc48e0bcfd3bae27d5616f3'] = 'CMS page';
$_MODULE['<{socialloginizer}ayon>loginizer_b4c1b6b25b37b4e95300bb755ec76e83'] = 'Produits Page spéciale';
$_MODULE['<{socialloginizer}ayon>loginizer_ab0082915316c509e1a92096a135087f'] = 'Nouveaux produits page';
$_MODULE['<{socialloginizer}ayon>loginizer_bad538bfc21d4bdebca6093a8c3ff9db'] = 'Meilleures ventes page';
$_MODULE['<{socialloginizer}ayon>loginizer_dce265d28ef8ee2bc7271c7f2a527f25'] = 'Notre Magasins page';
$_MODULE['<{socialloginizer}ayon>loginizer_0089c612ab1d3048011498e6cf983e5a'] = 'Contactez-nous page';
$_MODULE['<{socialloginizer}ayon>loginizer_4a5a98be4202e6653fe9e90e3d923bfd'] = 'Cliquez ici pour obtenir Facebook app id et l\'application secrète';
$_MODULE['<{socialloginizer}ayon>loginizer_e2c483e0ded711e9d0c7947443475c40'] = 'Activer Facebook Login';
$_MODULE['<{socialloginizer}ayon>loginizer_66dfb29331788cea731d1fe65b7cca93'] = 'Facebook App Id';
$_MODULE['<{socialloginizer}ayon>loginizer_0a732b3c2a2cd5e0c8d2d494ddf2c68a'] = 'Entrez Facebook App Id';
$_MODULE['<{socialloginizer}ayon>loginizer_fd125f225efc3440c92a219e9b396543'] = 'Facebook App secret';
$_MODULE['<{socialloginizer}ayon>loginizer_282c24ad66882a145a03423ebac02225'] = 'Entrez Facebook App secret';
$_MODULE['<{socialloginizer}ayon>loginizer_62e15137bb1df0c4e2c4b6ccbfa7982f'] = 'MailChimp Api Key';
$_MODULE['<{socialloginizer}ayon>loginizer_6efe05d79cfca34160712000266412da'] = 'Entrez MailChimp Api Key';
$_MODULE['<{socialloginizer}ayon>loginizer_d5e88b884960d7738dc754b1560d80af'] = 'Liste MailChimp';
$_MODULE['<{socialloginizer}ayon>loginizer_0a48ee459ce3be1882c55b57199e45f3'] = 'Sélectionnez Liste MailChimp';
$_MODULE['<{socialloginizer}ayon>loginizer_1604b54a253b3cb679b366f0b09c64e9'] = 'Étapes pour configurer:';
$_MODULE['<{socialloginizer}ayon>loginizer_6f4a225b29c0975663d0e4b9e1f4547f'] = 'Étape 1';
$_MODULE['<{socialloginizer}ayon>loginizer_e31ad1284b7618a9c10777ed99b12e64'] = 'Étape 2';
$_MODULE['<{socialloginizer}ayon>loginizer_2d5ce426423eeeead4f35769cd8b1b27'] = 'Etape 3';
$_MODULE['<{socialloginizer}ayon>loginizer_c83b2ce86cf5c443cfc1190bd8687987'] = 'Étape 4, 5';
$_MODULE['<{socialloginizer}ayon>loginizer_ce0718d241ac86415f71a685438c2a20'] = 'Étape 6, 7';
$_MODULE['<{socialloginizer}ayon>loginizer_d60d761a93da31e724112577c571c923'] = 'Etape 8';
$_MODULE['<{socialloginizer}ayon>loginizer_54c8107ec5763757535759e84fcccba4'] = 'Étape 9';
$_MODULE['<{socialloginizer}ayon>loginizer_bb592d8f6cdf3e122d972e125c5560ee'] = 'Etape 10, 11, 12';
$_MODULE['<{socialloginizer}ayon>loginizer_7a80ed477059c9d03ec30eb9de6b6446'] = 'Pour l\'étape n ° 9 utilisation App Domain:';
$_MODULE['<{socialloginizer}ayon>loginizer_6e26437913043169838181cf11da7483'] = 'Pour l\'étape # 11 utilisation du site Url:';
$_MODULE['<{socialloginizer}ayon>loginizer_c0f1d3db2a584ee59498c0abaa47adf7'] = '13 Etape';
$_MODULE['<{socialloginizer}ayon>loginizer_7a0c0b5f60317710d30ab337c58328cf'] = 'Etape 14, 15';
$_MODULE['<{socialloginizer}ayon>loginizer_db16a94ab2e9af7aab98fbbfa731d592'] = 'Etape 16, 17, 18, 19';
$_MODULE['<{socialloginizer}ayon>loginizer_c6499f9f8e4c0af4e7029f1d0f08fab4'] = 'Cliquez ici pour obtenir Google client id et secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_c49247323a602fa42bebf0455dee3c7e'] = 'Activer Google Plus Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_4496cfd9a1a696eb6e12d152c63d24d6'] = 'Google Plus Id client';
$_MODULE['<{socialloginizer}ayon>loginizer_7b7e2dbcb2afacab9acb1a70048774ce'] = 'Entrez Google Plus Id client';
$_MODULE['<{socialloginizer}ayon>loginizer_8875436e3670d15355b7c9733e28e8f7'] = 'Google Plus secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_c53292e3cb87979e78d96eee8121a7b5'] = 'Entrez Google Plus secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_e5afce1d8ad895b9ebf43e323f55387b'] = 'Étape 3, 4';
$_MODULE['<{socialloginizer}ayon>loginizer_aec2864af3e31061f55a124442698eec'] = 'Etape 5, 6, 7';
$_MODULE['<{socialloginizer}ayon>loginizer_b1ccca4c6376957dd0a732aa342ef2c7'] = 'Etape 10, 11, 12';
$_MODULE['<{socialloginizer}ayon>loginizer_97785b5693ddcd273b745d35aa20e552'] = 'Etape 13, 14, 15, 16, 17';
$_MODULE['<{socialloginizer}ayon>loginizer_c7e3b5ce30700b6d4dd9d110f16a5465'] = 'Pour l\'étape # 15 utilisation autorisée javascript origines:';
$_MODULE['<{socialloginizer}ayon>loginizer_c883c3e91bc0dd4c9aa0307c87dd5eb5'] = 'Pour l\'étape # 16 utilisation autorisée URL de redirection:';
$_MODULE['<{socialloginizer}ayon>loginizer_69289d1e361ad476c51b2d1c3c7d8f27'] = 'Etape 18, 19';
$_MODULE['<{socialloginizer}ayon>loginizer_0876bd8277ccd59f190a9712935fbb0f'] = 'Etape 20, 21, 22, 23';
$_MODULE['<{socialloginizer}ayon>loginizer_fb19bffc95fcc454821bac368ee0c5f9'] = 'Cliquez ici pour obtenir votre numéro de client en direct et secret client (v1)';
$_MODULE['<{socialloginizer}ayon>loginizer_c1068a4de971ecf943440174b4a290d1'] = 'Activer direct Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_c0997ed26095d7148c73b95008e24679'] = 'Client Live Id';
$_MODULE['<{socialloginizer}ayon>loginizer_1087b413bc531d99ee4deb93e280bfed'] = 'ID client en direct';
$_MODULE['<{socialloginizer}ayon>loginizer_a4197816749d767da8b45c4158bb7f55'] = 'Client secret en direct (v1)';
$_MODULE['<{socialloginizer}ayon>loginizer_b9b75793dc0a1d8225b39b8be4392339'] = 'Client Live secret';
$_MODULE['<{socialloginizer}ayon>loginizer_1bd3e1b861ef65e7c5358c02c601f29e'] = 'Étape 2, 3';
$_MODULE['<{socialloginizer}ayon>loginizer_823b09d95d8a1dd6255b35aba3242ef2'] = 'Étape 4, 5, 6, 7, 8, 9';
$_MODULE['<{socialloginizer}ayon>loginizer_9018c6cf5de06f126d8b518edcbe00cf'] = 'Pour l\'étape # 5 Utiliser domaine cible:';
$_MODULE['<{socialloginizer}ayon>loginizer_8507d14bdd7a397f65f7a8d14864fa4c'] = 'Pour l\'étape # 7 Utiliser Domaine racine:';
$_MODULE['<{socialloginizer}ayon>loginizer_61b5eff8d9d71cee51335b090e64eceb'] = 'Pour l\'étape # 8 Utilisation URL de redirection:';
$_MODULE['<{socialloginizer}ayon>loginizer_987f98e36d4b083bf4a4dcd81eab778d'] = 'Etape 10, 11';
$_MODULE['<{socialloginizer}ayon>loginizer_ff7b66b5d4382773fc29dbdc4891065e'] = 'Etape 12, 13, 14, 15';
$_MODULE['<{socialloginizer}ayon>loginizer_806ebfd56721bac3798bf95250769709'] = 'Cliquez ici pour obtenir Linked in clé API et clé secrète';
$_MODULE['<{socialloginizer}ayon>loginizer_b9ece2781f5d35af5e29ff2aff390a28'] = 'Activer LinkedIn Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_08be9353aecde2082ebcc8490a834d18'] = 'LinkedIn clé API';
$_MODULE['<{socialloginizer}ayon>loginizer_780a4389535ec0ea72ce68d10e9716fd'] = 'Entrez LinkedIn clé API';
$_MODULE['<{socialloginizer}ayon>loginizer_63da0b7fcaf4ab742e4a0225a180aa1c'] = 'LinkedIn Secret Key';
$_MODULE['<{socialloginizer}ayon>loginizer_6a0d1384d9c63351e874d87d254f782c'] = 'Entrez LinkedIn Secret Key';
$_MODULE['<{socialloginizer}ayon>loginizer_778f9cae670af7a9c0b6bd789e58ef1b'] = 'Étape 3, 4, 5, 6, 7, 8, 9, 10, 11, 12';
$_MODULE['<{socialloginizer}ayon>loginizer_a9532ec47b36c0d220a3abbae06c7fad'] = 'Pour l\'étape # 8 utilisation Site Web:';
$_MODULE['<{socialloginizer}ayon>loginizer_59ab98abfcc4509bbf292ca43388364c'] = 'Etape 13, 14, 15, 16, 17, 18, 19';
$_MODULE['<{socialloginizer}ayon>loginizer_7579878ee2778b6871ca0101edf28a66'] = 'Pour l\'étape # 17 utilisation autorisée URL de redirection:';
$_MODULE['<{socialloginizer}ayon>loginizer_e8bcd9feff0c1137d582ac36253b5cc1'] = 'Cliquez ici pour obtenir Twitter clé API et API secrète';
$_MODULE['<{socialloginizer}ayon>loginizer_a9a4279cc21f9867de5145a4083a40cb'] = 'Activer Twitter Connectez-vous';
$_MODULE['<{socialloginizer}ayon>loginizer_2e20538eccdb95fcb0c733d619bfeb22'] = 'Twitter API Key';
$_MODULE['<{socialloginizer}ayon>loginizer_0134b41e89e95846a4b8d6378ab1c633'] = 'Entrez Twitter API Key';
$_MODULE['<{socialloginizer}ayon>loginizer_d39e84d8049ec93b90d5afd234fa5c43'] = 'Twitter API secret';
$_MODULE['<{socialloginizer}ayon>loginizer_e902947a7f547c8c56866cf74f53ad9c'] = 'Entrez Twitter API secret';
$_MODULE['<{socialloginizer}ayon>loginizer_59c1c9b114b990647569e421535e7bba'] = 'Étape 3, 4, 5, 6, 7, 8';
$_MODULE['<{socialloginizer}ayon>loginizer_b08917bd240add22445816ac47c74806'] = 'Pour l\'étape # 5 utilisation Site Web:';
$_MODULE['<{socialloginizer}ayon>loginizer_cc45d5dd887147840ec416f5b12f89a7'] = 'Pour l\'étape # 6 Utiliser l\'URL de rappel:';
$_MODULE['<{socialloginizer}ayon>loginizer_67a5db2a27d24485a3488bdd68208d89'] = 'Etape 9, 10';
$_MODULE['<{socialloginizer}ayon>loginizer_1994175ee583ab766bb597fba4df9a45'] = 'Etape 11, 12, 13, 14';
$_MODULE['<{socialloginizer}ayon>loginizer_063e9d10343f3f4c45606a4e68a957cf'] = 'Cliquez ici pour obtenir Yahoo ID client et secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_8c7475bb548f83cc9fb867e8f9617298'] = 'Activer Yahoo Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_8d0d834c0924237e711fdb8bacac2b8f'] = 'Key consommateurs Yahoo';
$_MODULE['<{socialloginizer}ayon>loginizer_b4dc7e1e942ae6e2fd015f0823ca51ac'] = 'Entrez la clé consommateurs Yahoo';
$_MODULE['<{socialloginizer}ayon>loginizer_78589172f4fee456a7d000c8b6035a8b'] = 'Yahoo consommateurs secret';
$_MODULE['<{socialloginizer}ayon>loginizer_3ad40de89f08286e1d7b42709cacbbe6'] = 'Entrez Yahoo consommateurs secret';
$_MODULE['<{socialloginizer}ayon>loginizer_af09e8fe04bdf4b5bfa990b2f7579a94'] = 'Pour l\'étape # 5 Utiliser rappel domaine:';
$_MODULE['<{socialloginizer}ayon>loginizer_063c658932949ea24ab5ed212960a424'] = 'Etape 13, 14, 15, 16';
$_MODULE['<{socialloginizer}ayon>loginizer_aefebfeb4999c34cefde3f11c46aa8bc'] = 'Cliquez ici pour obtenir Instagram client id et secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_659b309caa42a11e65f7f8d074d22d87'] = 'Activer Instagram Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_3fbfcab9f06fc6c5727c87fc0a693118'] = 'Instagram client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_985a3a43be7368b346da5a93ae506011'] = 'Entrez Instagram client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_dee9d38a294b7ab413e70ecca5837485'] = 'Instagram client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_2fafbbcae57639d99029666a78cd0d83'] = 'Entrez Instagram client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_a7a58995b3cd326fd43d588eb63f3cfe'] = 'Pour l\'étape # 6 utilisation Site Web:';
$_MODULE['<{socialloginizer}ayon>loginizer_c2ce276cf1d9e09bcd7ede7a0cb7e92e'] = 'Pour l\'étape # 7 Utiliser URL de redirection:';
$_MODULE['<{socialloginizer}ayon>loginizer_3b0649c72650c313a357338dcdfb64ec'] = 'Note';
$_MODULE['<{socialloginizer}ayon>loginizer_ec1080680a7b902ac2b9524b04a9b1cb'] = 'SSL doit être activé pour utiliser cette option.';
$_MODULE['<{socialloginizer}ayon>loginizer_fa239f529e8c8066eaab7e804dd27c8f'] = 'Cliquez ici pour obtenir Amazon client id et secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_247049021d223db4b79181544e3f233c'] = 'Activer Amazon Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_b27a0fb93d6aeb0e4e80bbdcdba0dce0'] = 'Amazon client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_9be81017eadea62516044d321a295d81'] = 'Entrez Amazon client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_63e7e75c1b67732c2447e11f87e53272'] = 'Amazon client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_c342a55d5f29406cca6232f2f767b251'] = 'Entrez Amazon client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_7882a4b556b6996eaf604413a9e602d9'] = 'Étape 4';
$_MODULE['<{socialloginizer}ayon>loginizer_cdb0bf344917fa1d932dffc4809230e6'] = 'Etape 5, 6';
$_MODULE['<{socialloginizer}ayon>loginizer_0fa75da7c7cb84bff7dfb205214adf9e'] = 'Étape 7, 8, 9';
$_MODULE['<{socialloginizer}ayon>loginizer_367ede4f3b55ad8608a9a564bcb15794'] = 'Pour l\'étape # 7 Utiliser admis javascript origines:';
$_MODULE['<{socialloginizer}ayon>loginizer_cc27fe67e9ccdb55be3502e830c52834'] = 'Pour l\'étape # 8 Utilisation admis URL de retour:';
$_MODULE['<{socialloginizer}ayon>loginizer_8dc441a234c2613444e9b87e9180e232'] = 'Etape 10, 11, 12, 13';
$_MODULE['<{socialloginizer}ayon>loginizer_f4af7296eb82e241e8aba37ca3ff0c8f'] = 'Cliquez ici pour obtenir Paypal client id et secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_8769cdd79715f59715d9237b6c36aa90'] = 'Activer Paypal Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_56a75e7b1fbc942d2f3e4f1fc128f1a1'] = 'Paypal client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_fce1711161f3602a172315b6464c4669'] = 'Paypal client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_f64a26965c3d979b9c4a911af0bf63d8'] = 'Entrez Paypal client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_4d405322d579deead5cbcd5d2603710b'] = 'Étape 3, 4, 5';
$_MODULE['<{socialloginizer}ayon>loginizer_f5ce4d426c8852e0090ab32842d1cd09'] = 'Etape 6';
$_MODULE['<{socialloginizer}ayon>loginizer_4711a1fd376fd694fb64d1e209b3b2b3'] = 'Étape 7, 8, 9, 10';
$_MODULE['<{socialloginizer}ayon>loginizer_ad56135d9b4a8c9e3fafea1aee554c58'] = 'Pour l\'étape # 9 Utiliser URL de retour:';
$_MODULE['<{socialloginizer}ayon>loginizer_c1f08772aae4e5a87f2f58025a28aae7'] = 'Etape 11, 12, 13, 14, 15';
$_MODULE['<{socialloginizer}ayon>loginizer_894fa5887409aa32bd003b39652704d2'] = 'Pour l\'étape # 13 utilisation Politique de confidentialité Url:';
$_MODULE['<{socialloginizer}ayon>loginizer_ea8782ef1ae7a0488492ff16465ad65a'] = 'Pour l\'étape # 14 utilisation aggrement utilisateur Url:';
$_MODULE['<{socialloginizer}ayon>loginizer_d5e42ee116eb80a7dc7d62933d88b397'] = 'Etape 16, 17, 18, 19';
$_MODULE['<{socialloginizer}ayon>loginizer_75a254fd0a3af056827499b6a09408bd'] = 'Cliquez ici pour obtenir votre numéro de client Foursquare et secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_2176490ba1e26efe5d33979cff7f6548'] = 'Activer Foursquare Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_424496839b30913383c4616c714f670a'] = 'Id client Foursquare';
$_MODULE['<{socialloginizer}ayon>loginizer_2472a7d24afeed460a8b2e08b61cc165'] = 'Entrez Foursquare client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_66c8a27423ed224aede7c77b0cd0d45b'] = 'Foursquare client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_b6e5101e1b988743ba9bbc80d6c5611c'] = 'Entrez Foursquare client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_9b25ccb14964d465ed4ff26a87760353'] = 'Étape 3, 4, 5, 6';
$_MODULE['<{socialloginizer}ayon>loginizer_63e88e0e5b0415de7a19c591d4732396'] = 'Pour l\'étape # 5 Utilisez URL de redirection:';
$_MODULE['<{socialloginizer}ayon>loginizer_7dfb9a7fa08f2342bd5bf05d0d32931b'] = 'Étape 7, 8';
$_MODULE['<{socialloginizer}ayon>loginizer_45a571695820d5e3b171dabf753b2dd2'] = 'Etape 9, 10, 11, 12';
$_MODULE['<{socialloginizer}ayon>loginizer_bd97be82c5dcd05e4a4319e0263252a3'] = 'Cliquez ici pour obtenir github client id et secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_23fb82e31da94ec937c181ac813f3481'] = 'Activer Github Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_4411cf841014cbad35713a60faf4bdc9'] = 'Github client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_f1f786dddc9d18e6875953fdc9985ccc'] = 'Entrez Github client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_7910507e5e550e3118d43ad81529ddb2'] = 'Github client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_b04f0e90eb6bbbb0f6c70f7be45afbc3'] = 'Entrez Github client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_22ddf504839af9801ba910abfb8d1b72'] = 'Etape 2, 3, 4, 5';
$_MODULE['<{socialloginizer}ayon>loginizer_f82020446110426fbb54c1a74c1796b9'] = 'Pour l\'étape # 3 Utilisez HomePage Url:';
$_MODULE['<{socialloginizer}ayon>loginizer_3d6910ca8a58d42d199dcfa48561461e'] = 'Pour l\'étape n ° 4 utiliser l\'autorisation URL de rappel:';
$_MODULE['<{socialloginizer}ayon>loginizer_9a2732e177d36b78d538d411b60b6ff9'] = 'Etape 8, 9, 10, 11';
$_MODULE['<{socialloginizer}ayon>loginizer_5c37facd519cfdef1563524ec5bc77d6'] = 'Cliquez ici pour obtenir Disqus clé API et API secrète';
$_MODULE['<{socialloginizer}ayon>loginizer_0afca590b49a5a212b4d4e7ad5075489'] = 'Activer Disqus Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_503c8bec55ce8d3cdb8359d00652112d'] = 'Disqus Api Key';
$_MODULE['<{socialloginizer}ayon>loginizer_f86e53fc059d898db4b1947ddd2d5e32'] = 'Entrez Disqus Api Key';
$_MODULE['<{socialloginizer}ayon>loginizer_abf9c08318999f63d8341c8a16418dfb'] = 'Disqus Api secret';
$_MODULE['<{socialloginizer}ayon>loginizer_38ce0816a1fd95d9ed2eb14935b28030'] = 'Entrez Disqus Api secret';
$_MODULE['<{socialloginizer}ayon>loginizer_a0dc6fc802f358d13b0c0b599d6616fe'] = 'Étape 3, 4, 5, 6, 7';
$_MODULE['<{socialloginizer}ayon>loginizer_74549731326907e63640ce3165819e59'] = 'Etape 8, 9, 10';
$_MODULE['<{socialloginizer}ayon>loginizer_bcdc508b128585c06fbbb379cd4a182d'] = 'Pour l\'étape # 9 Utiliser l\'URL de rappel:';
$_MODULE['<{socialloginizer}ayon>loginizer_36535538606c8e656dff52b8455be46d'] = 'Etape 11, 12';
$_MODULE['<{socialloginizer}ayon>loginizer_7118efc5430f67c02d9e84e4ef89fae5'] = 'Cliquez ici pour obtenir ID d\'application et vkontakte clé sécurisée';
$_MODULE['<{socialloginizer}ayon>loginizer_12385729bec116bf01e49433a1b6d25a'] = 'Activer Vkontakte Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_75091556928fe10e62a404db243e4dd3'] = 'Vkontakte application Id';
$_MODULE['<{socialloginizer}ayon>loginizer_2d1f229bf5465014c0f1ac83d92d1b6c'] = 'Entrez Vkontakte application Id';
$_MODULE['<{socialloginizer}ayon>loginizer_27f79a22dcfa64b6134cc18d23dd2f3e'] = 'Vkontakte Secure Key';
$_MODULE['<{socialloginizer}ayon>loginizer_7b35b893234d158d807476036621c3dc'] = 'Entrez Vkontakte Secure Key';
$_MODULE['<{socialloginizer}ayon>loginizer_4e82f2330670c6c8d9933893d73376f3'] = 'Etape 2, 3, 4, 5, 6';
$_MODULE['<{socialloginizer}ayon>loginizer_bbee974f8bc734a40e0d7a4d0387e208'] = 'Pour l\'étape # 4 Utilisez Adresse du site:';
$_MODULE['<{socialloginizer}ayon>loginizer_a7f8a20eccbcc685292b1bdd5c7735ce'] = 'Pour l\'étape # 5 Utilisez Domaine de base:';
$_MODULE['<{socialloginizer}ayon>loginizer_3734061d804afb13ba93064a6a81bf10'] = 'Étape 7, 8, 9, 10, 11, 12, 13';
$_MODULE['<{socialloginizer}ayon>loginizer_fc03524c2f71686b28f85d3063e517d4'] = 'Pour l\'étape # 12 utilisation autorisée URL de redirection:';
$_MODULE['<{socialloginizer}ayon>loginizer_1cce6827c8e958f0a8fcc8bdadf64eb2'] = 'Etape 14, 15, 16, 17';
$_MODULE['<{socialloginizer}ayon>loginizer_4246dc1e1830d8c007059b157d8f2fd0'] = 'Cliquez ici pour obtenir votre numéro de client wordpress et secret client';
$_MODULE['<{socialloginizer}ayon>loginizer_e94c5a39806e8ad90301e740697c19b6'] = 'Activer Wordpress Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_a002457b1a19351428b7753cd98ada92'] = 'Wordpress client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_af2f045f3b6c923bbee9935df36696e3'] = 'Entrez Wordpress client Id';
$_MODULE['<{socialloginizer}ayon>loginizer_702e165aef5d7fe5a3a02f8955cb070b'] = 'Wordpress client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_81907bc5c2f378547b6ce2f755a1008c'] = 'Entrez Wordpress client secret';
$_MODULE['<{socialloginizer}ayon>loginizer_8018085dc20e4ba87b0abf8439132bf1'] = 'Étape 3, 4, 5, 6, 7, 8, 9, 10';
$_MODULE['<{socialloginizer}ayon>loginizer_a3583f83d1965791a38ce8b4b864cbe4'] = 'Pour l\'étape # 5 utilisation Site Web:';
$_MODULE['<{socialloginizer}ayon>loginizer_6eedfa6ad8ed104b921c74f892ab6f38'] = 'Pour l\'étape # 6 Utiliser URL de redirection:';
$_MODULE['<{socialloginizer}ayon>loginizer_fa83d63ebf7854b1586217a755179986'] = 'Pour l\'étape # 7 utilisation origine javascript:';
$_MODULE['<{socialloginizer}ayon>loginizer_5f97f3ee15cc9d2abee53d7af46e1799'] = '11 Etape';
$_MODULE['<{socialloginizer}ayon>loginizer_b9faeaed0cc63d74396f813a6702cf17'] = 'Etape 12, 13';
$_MODULE['<{socialloginizer}ayon>loginizer_20e5c967c1b5091b25ed9970c2720d30'] = 'Etape 14, 15, 16, 17';
$_MODULE['<{socialloginizer}ayon>loginizer_dfadb9275c52a76e013ad5709d7aa20a'] = 'Cliquez ici pour obtenir la clé de l\'application de la sélection et l\'application secrète';
$_MODULE['<{socialloginizer}ayon>loginizer_1338ea0a8dee157b7d8b6ecccfa8f692'] = 'Activer Dropbox Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_b421cfbf6ec8b38a3b2585de8e9e4942'] = 'Dropbox App clé';
$_MODULE['<{socialloginizer}ayon>loginizer_6cdf436785bbf0c4cc947045f92958f7'] = 'Entrez Dropbox App clé';
$_MODULE['<{socialloginizer}ayon>loginizer_0bca1a55a5fa5c0f22a0bcbe7380f2c6'] = 'Dropbox App secret';
$_MODULE['<{socialloginizer}ayon>loginizer_b9c0385fda35bb91370c568d75c6e57a'] = 'Entrez Dropbox App secret';
$_MODULE['<{socialloginizer}ayon>loginizer_940428f7b33e714b139f351684fddadb'] = 'Statistiques';
$_MODULE['<{socialloginizer}ayon>loginizer_67ef3be8ee2579dc3b9bcfcbaa1ed624'] = 'Connectez vs comte Enregistré';
$_MODULE['<{socialloginizer}ayon>loginizer_d2cde31cc4dee03a6e90989fb1fa2dd8'] = 'Statistiques factices';
$_MODULE['<{socialloginizer}ayon>loginizer_b552849e09b4a47a87641523cb99e06d'] = 'Aucune donnée disponible';
$_MODULE['<{socialloginizer}ayon>loginizer_b13e04a5b87f26ef4f7b0b0a64991f9f'] = 'Total des inscriptions';
$_MODULE['<{socialloginizer}ayon>loginizer_26ca21eb7c4170276742e8f8e7436ba7'] = 'Données factices';
$_MODULE['<{socialloginizer}ayon>loginizer_ae4a2965de05024126b28acf47c121f1'] = 'S. No.';
$_MODULE['<{socialloginizer}ayon>loginizer_06d960789f90fb6cfd52bc1a905fd36d'] = 'Type de compte';
$_MODULE['<{socialloginizer}ayon>loginizer_113dbf4eb69ff75169386314ff2977f5'] = 'Total des inscriptions';
$_MODULE['<{socialloginizer}ayon>loginizer_67e514e7bd9ac9ba5bd925b7c221d994'] = 'Total Connexion';
$_MODULE['<{socialloginizer}ayon>loginizer_004bf6c9a40003140292e97330236c53'] = 'action';
$_MODULE['<{socialloginizer}ayon>loginizer_5d5cd268b8acccf04f63d81c5d0d2cab'] = 'Voir les détails';
$_MODULE['<{socialloginizer}ayon>loginizer_d3d2e617335f08df83599665eef8a418'] = 'Fermer';
$_MODULE['<{socialloginizer}ayon>loginizer_5026b9b231bb5c7822365f78916042cd'] = 'Les clients sociaux';
$_MODULE['<{socialloginizer}ayon>loginizer_cd5801e77c68c9fcb01c9ab391665204'] = 'Montrer :';
$_MODULE['<{socialloginizer}ayon>loginizer_5405d8a903c83e89cb649f1b518ef1be'] = 'Questions fréquemment posées';
$_MODULE['<{socialloginizer}ayon>loginizer_182543fe78b3011ebd18be966d9a01e6'] = 'S.No';
$_MODULE['<{socialloginizer}ayon>loginizer_92f1b1481fa6ff46c4a3caae78354dab'] = 'Nom d\'utilisateur';
$_MODULE['<{socialloginizer}ayon>loginizer_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'Email';
$_MODULE['<{socialloginizer}ayon>loginizer_99dea78007133396a7b8ed70578ac6ae'] = 'S\'identifier';
$_MODULE['<{socialloginizer}ayon>loginizer_0ba7583639a274c434bbe6ef797115a4'] = 'registre';
$_MODULE['<{socialloginizer}ayon>loginizer_e93f994f01c537c4e2f7d8528c3eb5e9'] = 'Compter';
$_MODULE['<{socialloginizer}ayon>loginizer_6fbacda008628fae693794ef11dd031f'] = 'Note:';
$_MODULE['<{socialloginizer}ayon>loginizer_985f0794b205e25902cc45ac7a6ca940'] = 'Aucun Enregistrement Trouvé';
$_MODULE['<{socialloginizer}ayon>loginizer_6d48d95b0ccb29b350f38cb4a73feb6a'] = 'Non montrant clients supprimés.';
$_MODULE['<{socialloginizer}ayon>loginizer_cf62f0d0fb934b985477e520558b5486'] = ' La liste ci-dessus contient des données factices uniquement à des fins de visualisation.';
$_MODULE['<{socialloginizer}ayon>loginizer_ccfe2e7bd65911e18f0e99d66d58cb24'] = 'Connexion comte';
$_MODULE['<{socialloginizer}ayon>email_3f3c102ce7b54421d72057fc90b799aa'] = 'Loginizer Email page';
$_MODULE['<{socialloginizer}ayon>email_0219154f73c87a989d7813f0f9642cfc'] = 'S\'il vous plaît indiquer votre adresse e-mail afin de procéder';
$_MODULE['<{socialloginizer}ayon>error_c2c35749d999900983182da59f2b122c'] = 'Loginizer erreur';
$_MODULE['<{socialloginizer}ayon>error_6d7d8cc3fae1fdd486b167b439c2a5b9'] = 'Il ya 1 erreur';
$_MODULE['<{socialloginizer}ayon>error_c029b957c5cfa940881e4a29f12add4c'] = 'Une erreur est survenue pendant le processus.';
$_MODULE['<{socialloginizer}ayon>error_ac7c0ffca592407830577b82366c654a'] = 'Aller à la page de connexion';
$_MODULE['<{socialloginizer}ayon>error_cre_c2c35749d999900983182da59f2b122c'] = 'Loginizer erreur';
$_MODULE['<{socialloginizer}ayon>error_cre_6d7d8cc3fae1fdd486b167b439c2a5b9'] = 'Il ya 1 erreur';
$_MODULE['<{socialloginizer}ayon>error_cre_dcc91ce8ed9e9e8d4b5d86f157dd9338'] = 'Pouvoirs introuvables. S\'il vous plaît contactez l\'administrateur du site.';
$_MODULE['<{socialloginizer}ayon>error_cre_ac7c0ffca592407830577b82366c654a'] = 'Aller à la page de connexion';
$_MODULE['<{socialloginizer}ayon>github-email_3f3c102ce7b54421d72057fc90b799aa'] = 'Loginizer Email page';
$_MODULE['<{socialloginizer}ayon>github-email_a3fe02d4a0e655fb6eada403390343bb'] = 'Connexion avec Github';
$_MODULE['<{socialloginizer}ayon>github-email_4c61b9816d09b0969ad3d9a03c89b652'] = 'Entrez votre e-mail id Github afin de procéder ...';
$_MODULE['<{socialloginizer}ayon>github-email_f262fae0a2f17e8c1eb4e9eddb0160e4'] = 'Procéder';
$_MODULE['<{socialloginizer}ayon>instagram-email_3f3c102ce7b54421d72057fc90b799aa'] = 'Loginizer Email page';
$_MODULE['<{socialloginizer}ayon>instagram-email_a82eb14b877e6234ef6456ee852bcf99'] = 'Connecter ou vous inscrire avec Instagram';
$_MODULE['<{socialloginizer}ayon>instagram-email_66861bcfc6c07c9eb9d60284c0662ec2'] = 'Entrez votre e-mail id Instagram afin de procéder ...';
$_MODULE['<{socialloginizer}ayon>instagram-email_f262fae0a2f17e8c1eb4e9eddb0160e4'] = 'Procéder';
$_MODULE['<{socialloginizer}ayon>twitter-email_3f3c102ce7b54421d72057fc90b799aa'] = 'Loginizer Email page';
$_MODULE['<{socialloginizer}ayon>twitter-email_1d164af28c4a575881441f1b0e046c59'] = 'Connecter ou vous inscrire avec Twitter';
$_MODULE['<{socialloginizer}ayon>twitter-email_7d5dbc9359c19a8fd34295a2f0d828c4'] = 'Entrez votre e-mail id Twitter afin de procéder ...';
$_MODULE['<{socialloginizer}ayon>twitter-email_f262fae0a2f17e8c1eb4e9eddb0160e4'] = 'Procéder';
$_MODULE['<{socialloginizer}ayon>vk-email_3f3c102ce7b54421d72057fc90b799aa'] = 'Loginizer Email page';
$_MODULE['<{socialloginizer}ayon>vk-email_0b78ebea58e5c861afaf1d691544c50b'] = 'Connexion avec VK';
$_MODULE['<{socialloginizer}ayon>vk-email_d61b35fce3a72b08b0f14c420f7871e5'] = 'Entrez votre e-mail id VK afin de procéder ...';
$_MODULE['<{socialloginizer}ayon>vk-email_f262fae0a2f17e8c1eb4e9eddb0160e4'] = 'Procéder';
$_MODULE['<{socialloginizer}ayon>yahoo-email_3f3c102ce7b54421d72057fc90b799aa'] = 'Loginizer Email page';
$_MODULE['<{socialloginizer}ayon>yahoo-email_d672c3f3ed71b383b7b346a02dc1c089'] = 'Connecter ou vous inscrire avec Yahoo';
$_MODULE['<{socialloginizer}ayon>yahoo-email_acd6dd658484cfe37d637f84a2fec9fc'] = 'Entrez votre Email ID Yahoo afin de procéder ...';
$_MODULE['<{socialloginizer}ayon>yahoo-email_f262fae0a2f17e8c1eb4e9eddb0160e4'] = 'Procéder';
