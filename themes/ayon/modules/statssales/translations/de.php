<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statssales}modez>statssales_45c4b3e103155326596d6ccd2fea0f25'] = 'Verkäufe und Bestellungen';
$_MODULE['<{statssales}modez>statssales_d2fb07753354576172a2b144c373a610'] = 'Fügt der Statistik-Übersicht eine Grafik zur Entwicklung der Verkäufe und Bestellungen hinzu.';
$_MODULE['<{statssales}modez>statssales_6602bbeb2956c035fb4cb5e844a4861b'] = 'Erklärung';
$_MODULE['<{statssales}modez>statssales_bdaa0cab56c2880f8f60e6a2cef40e63'] = 'Order-Status';
$_MODULE['<{statssales}modez>statssales_5cc6f5194e3ef633bcab4869d79eeefa'] = 'Die Grafik berücksichtigt nur durchgeführte Bestellungen.';
$_MODULE['<{statssales}modez>statssales_c3987e4cac14a8456515f0d200da04ee'] = 'Alle Länder';
$_MODULE['<{statssales}modez>statssales_d7778d0c64b6ba21494c97f77a66885a'] = 'Filter';
$_MODULE['<{statssales}modez>statssales_9ccb8353e945f1389a9585e7f21b5a0d'] = 'Bestellungen:';
$_MODULE['<{statssales}modez>statssales_156e5c5872c9af24a5c982da07a883c2'] = 'Gekaufte Artikel:';
$_MODULE['<{statssales}modez>statssales_998e4c5c80f27dec552e99dfed34889a'] = 'CSV-Export';
$_MODULE['<{statssales}modez>statssales_ec3e48bb9aa902ba2ad608547fdcbfdc'] = 'Umsatz:';
$_MODULE['<{statssales}modez>statssales_f6825178a5fef0a97dacf963409829f0'] = 'Im Folgenden wird die Verteilung der Bestell-Status angezeigt.';
$_MODULE['<{statssales}modez>statssales_da80af4de99df74dd59e665adf1fac8f'] = 'Keine Bestellung für diesen Zeitraum';
$_MODULE['<{statssales}modez>statssales_b52b44c9d23e141b067d7e83b44bb556'] = 'Artikel';
$_MODULE['<{statssales}modez>statssales_497a2a4cf0a780ff5b60a7a6e43ea533'] = 'Umsatz-Währung: %s';
$_MODULE['<{statssales}modez>statssales_17833fb3783b26e0a9bc8b21ee85302a'] = 'Bestellungen nach Status (in %).';
