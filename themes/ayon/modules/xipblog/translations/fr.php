<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{xipblog}ayon>adminxipcomment_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{xipblog}ayon>adminxipcomment_b77006f2d0398fa7e68ef1a893ef850c'] = 'Identifiant';
$_MODULE['<{xipblog}ayon>adminxipcomment_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{xipblog}ayon>adminxipcomment_c7892ebbb139886662c6f2fc8c450710'] = 'Sujet';
$_MODULE['<{xipblog}ayon>adminxipcomment_0be8406951cdfda82f00f79328cf4efc'] = 'Commentaire';
$_MODULE['<{xipblog}ayon>adminxipcomment_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{xipblog}ayon>adminxipcomment_d3b206d196cd6be3a2764c1fb90b200f'] = 'Supprimer la sélection';
$_MODULE['<{xipblog}ayon>adminxipcomment_e25f0ecd41211b01c83e5fec41df4fe7'] = 'Supprimer les éléments sélectionnés?';
$_MODULE['<{xipblog}ayon>adminxipimagetype_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{xipblog}ayon>adminxipimagetype_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{xipblog}ayon>adminxipimagetype_32954654ac8fe66a1d09be19001de2d4'] = 'Largeur';
$_MODULE['<{xipblog}ayon>adminxipimagetype_eec6c4bdbd339edf8cbea68becb85244'] = 'Hauteur';
$_MODULE['<{xipblog}ayon>adminxipimagetype_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{xipblog}ayon>adminxipimagetype_d3b206d196cd6be3a2764c1fb90b200f'] = 'Supprimer la sélection';
$_MODULE['<{xipblog}ayon>adminxipimagetype_e25f0ecd41211b01c83e5fec41df4fe7'] = 'Supprimer les éléments sélectionnés?';
$_MODULE['<{xipblog}ayon>adminxipimagetype_7d3b3029d84bcad1ae215bf39eef1da6'] = 'Entrez votre nom de type d\'image';
$_MODULE['<{xipblog}ayon>adminxipimagetype_2ac28fab568e6f33226e33f53a3dc2dd'] = 'Entrez votre largeur';
$_MODULE['<{xipblog}ayon>adminxipimagetype_0077584d05849bb164cf97a073d541be'] = 'Entrez votre hauteur';
$_MODULE['<{xipblog}ayon>adminxipimagetype_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activer';
$_MODULE['<{xipblog}ayon>adminxipimagetype_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactiver';
$_MODULE['<{xipblog}ayon>adminxipimagetype_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{xipblog}ayon>archive_53e5aa2c97fef1555d2511de8218c544'] = 'Par';
$_MODULE['<{xipblog}ayon>archive_ed4832a84ee072b00a6740f657183598'] = 'Vues';
$_MODULE['<{xipblog}ayon>archive_a5c22ff3efbced22c34bd7b7dd482a6d'] = 'Lire plus >>';
$_MODULE['<{xipblog}ayon>comment-list_820ce35c4b7acc9fcff07fb3f96544c7'] = 'Tous les commentaires';
$_MODULE['<{xipblog}ayon>comment-list_25d8df6e580905091a0d5ef5b9e05bf0'] = 'Répondre';
$_MODULE['<{xipblog}ayon>single_53e5aa2c97fef1555d2511de8218c544'] = 'Par';
$_MODULE['<{xipblog}ayon>single_ed4832a84ee072b00a6740f657183598'] = 'Vues';
