<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-no-products_5c9838becf9bbce28ba90a7426daf171'] = 'Il n\'y a aucun produit';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_f3f43e30c8c7d78c6ac0173515e57a00'] = 'Filtres';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_1262d1b9fbffb3a8e85ac9e4b449e989'] = 'Filtres actifs:';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_ea4788705e6873b424c65e91c2846b19'] = 'Annuler';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_b47b72ddf8a3fa1949a7fb6bb5dbc60c'] = 'Pas de filtres';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_75954a3c6f2ea54cb9dff249b6b5e8e6'] = 'Plage:';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_5da618e8e4b89c66fe86e32cdafde142'] = 'De';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_01b6e20344b68835c5ed1ddedf20d531'] = 'à';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_146ffe2fd9fa5bec3b63b52543793ec7'] = 'Voir plus';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_c74ea6dbff701bfa23819583c52ebd97'] = 'Voir moins';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered-top_8524de963f07201e5c086830d370797f'] = 'Chargement';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered_b3786b970611c1a3809dd51b630812a7'] = '\"%s\" n\'est pas valide';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered_78a5eb43deef9a7b5b9ce157b9d52ac4'] = 'prix';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered_7edabf994b76a00cbc60c95af337db8f'] = 'poids';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered_3601146c4e948c32b6424d2c0a7f0118'] = 'Prix';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered_8c489d0946f66d17d73f26366a4bf620'] = 'Poids';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Marque';
$_MODULE['<{xprtblocklayered}ayon>xprtblocklayered_af1b98adf7f686b84cd0b443e022b7a0'] = 'Catégories';
