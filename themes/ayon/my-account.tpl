{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='My account'}{/capture}
<h1 class="page-heading">{l s='My account'}</h1>
{if isset($account_created)}
	<p class="alert alert-success">
		{l s='Your account has been created.'}
	</p>
{/if}
<p class="info-account">{l s='Welcome to your account. Here you can manage all of your personal information and orders.'}</p>
<div class="row addresses-lists">
	<div class="col-xs-12 col-sm-6">
		<ul class="myaccount-link-list">
			<li>
				<a href="#identityItems" title="{l s='My personal information'}" data-toggle="collapse" aria-expanded="false" aria-controls="identityItems">
					<i class="ft-icon-user-o"></i>
					<span>{l s='My account'}</span>
				</a>
				<ul style="margin-left: 20px; margin-top: 10px;" class="collapse" id="identityItems">
					<li>
					  <a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="{l s='Information'}">
						  <i class="ft-icon-user-o"></i>
						  <span>{l s='My personal information'}</span>
						</a>
					</li>
					{if $has_customer_an_address}
						<li><a href="{$link->getPageLink('address', true)|escape:'html':'UTF-8'}" title="{l s='Add my first address'}"><i class="icon-building"></i><span>{l s='Add my first address'}</span></a></li>
					{else}
						<li><a href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='Addresses'}">
						<i class="ft-icon-location-1"></i>
						<span>{l s='My addresses'}</span></a></li>
					{/if}
				</ul>
			</li>
		</ul>
	</div>
    <div class="col-xs-12 col-sm-6">
		<ul class="myaccount-link-list">
			<li>
				<a href="#orderItems" title="{l s='Orders'}" data-toggle="collapse" aria-expanded="false" aria-controls="orderItems">
					<i class="fa fa-shopping-bag"></i>
					<span>{l s='My orders'}</span>
				</a>
				<ul style="margin-left: 20px; margin-top: 10px;" class="collapse" id="orderItems">

					<li><a href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='Orders'}"><i class="fa fa-shopping-bag"></i><span>{l s='Order history and details'}</span></a></li>
					{if $returnAllowed}
							<li><a href="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" title="{l s='Merchandise returns'}"><i class="ft-icon-spin3"></i><span>{l s='My merchandise returns'}</span></a></li>
					{/if}
					
					<li><a href="{$link->getPageLink('order-slip', true)|escape:'html':'UTF-8'}" title="{l s='Credit slips'}">
					<i class="ft-icon-money"></i>
					<span>{l s='My credit slips'}</span></a></li>
					
					{if $voucherAllowed}
						<li><a href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}" title="{l s='Vouchers'}">
						<i class="ft-icon-tag"></i>
						<span>{l s='My vouchers'}</span></a></li>
					{/if}
				</ul>
			</li>
		</ul>
	</div>
<div class="clearfix"></div>
	<div class="col-xs-12 col-sm-6 clearfix">
        <ul class="myaccount-link-list">
            <li>
				<a href="#listItems" title="{l s='My favorites'}" data-toggle="collapse" aria-expanded="false" aria-controls="listItems">
					<i class="ft-icon-heart"></i>
					<span>{l s='My favorites'}</span>
				</a>
				<ul style="margin-left: 20px; margin-top: 10px;" class="collapse" id="listItems">
				{if ($whishlist)}
					<li class="lnk_wishlist">
						<a 	href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|escape:'html':'UTF-8'}" title="{l s='My wishlists'}">
							<i class="ft-icon-heart"></i>
							<span>{l s='My wishlists'}</span>
						</a>
					</li>
				{/if}
				{if ($marketplace_show_contact_seller)}
					<li>
						<a title="{l s='Seller messages'}" href="{$link->getModuleLink('jmarketplace', 'contactseller', array(), true)|escape:'html':'UTF-8'}">
							<i class="icon-envelope fa fa-envelope-o"></i>
							<span>{l s='Seller messages'}</span>
						</a>
					</li>
				{/if}
				{if $marketplace_show_seller_profile}
					<li>
						<a title="{l s='Favorite sellers'}" href="{$link->getModuleLink('jmarketplace', 'favoriteseller', array(), true)|escape:'html':'UTF-8'}">
						  <i class="fa ft-icon-shop-1"></i>
						  <i class="icon-heart fa fa-heart" style="font-size: 20px; top: -7px;"></i>
						  <span>{l s='Favorite sellers'}</span>
						</a>
					</li>
				{/if}
			
			
				{if $referralprogram}
					<li class="referralprogram"><a href="{$link->getModuleLink('referralprogram', 'program', [], true)|escape:'html'}" title="{l s='Referral program'}" rel="nofollow"><i class="ft-icon-gift-1"></i><span>{l s='Referral program'}</span></a></li>
				{/if}
				
				{if $mailalerts}
					<li class="mailalerts">
						<a href="{$link->getModuleLink('mailalerts', 'account', array(), true)|escape:'html'}" title="{l s='My alerts'}" rel="nofollow">
							<i class="icon-envelope"></i>
							<span>{l s='My alerts' }</span>
						</a>
					</li>
				{/if}
				</ul>
			</li>
        </ul>
    </div>
	
	<div class="col-xs-12 col-sm-6">
		<ul class="myaccount-link-list">
					
		{if ($marketplace_is_seller == 0)}
			<li>
				<a title="{l s='Create seller account' }" href="{$link->getModuleLink('jmarketplace', 'addseller', array(), true)|escape:'html':'UTF-8'}" class="ditwee-special">
					<i class="ft-icon-shop-1"></i>
					<span>{l s='Create seller account'}</span>
				</a>
			</li>
		{else if $marketplace_is_seller == 1 AND $marketplace_is_active_seller == 0}
			<li>
				<a href="#" class="ditwee-special">
					<i class="ft-icon-shop-1"></i>
					<span>{l s='Your seller account is pending approval.' }</span>
				</a>
			</li>  
		{else if $marketplace_is_seller == 1 AND $marketplace_is_active_seller == 1}  
			<li>
				<a title="{l s='Your seller account' }" href="{$link->getModuleLink('jmarketplace', 'selleraccount', array(), true)|escape:'html':'UTF-8'}" class="ditwee-special">
					<i class="ft-icon-shop-1"></i>
					<span>{l s='Your seller account' }</span>
				</a>
			</li> 
		{/if}
		</ul>
	</div>
</div>

{if $smarty.const._PS_MODE_DEV_}
<div class="row addresses-lists">
	<div class="col-xs-12 col-sm-6">
		<ul class="myaccount-link-list">
		
			{$HOOK_CUSTOMER_ACCOUNT}
		
		</ul>
	</div>
</div>
{/if}
		
<ul class="footer_links clearfix">
<li><a class="btn btn-default button button-small" href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{l s='Home'}"><span><i class="icon-chevron-left"></i> {l s='Home'}</span></a></li>
</ul>