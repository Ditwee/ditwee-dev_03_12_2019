<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{relaiscolisplus}default-bootstrap>relaiscolisplus_0309a6c666a7a803fdb9db95de71cf01'] = 'France';
$_MODULE['<{relaiscolisplus}default-bootstrap>relaiscolisplus_6c1674d14bf5f95742f572cddb0641a7'] = 'Belgique';
$_MODULE['<{relaiscolisplus}default-bootstrap>relaiscolisplus_d6a297c6193fd59309453a0db7a51b90'] = 'Monaco';
$_MODULE['<{relaiscolisplus}default-bootstrap>relaiscolisplus_73c1f7769f16622fe30bd2859c25599f'] = 'Relais Colis Home +';
$_MODULE['<{relaiscolisplus}default-bootstrap>relaiscolisplus_fe24d031e3718dff48460042c54279db'] = 'Relais Colis Home + pour Prestashop';
$_MODULE['<{relaiscolisplus}default-bootstrap>relaiscolisplus_08767ab8e3d8343f2df59553852e3480'] = 'Etes vous sûr de vouloir désinstaller le module ?';
$_MODULE['<{relaiscolisplus}default-bootstrap>relaiscolisplus_7fcd17835a9380be4212651e1cdae7de'] = 'Relais Colis Home +';
$_MODULE['<{relaiscolisplus}default-bootstrap>relaiscolisplus_823b8dbb2945c674275b55a7dcd064df'] = 'Livraison à domicile';
