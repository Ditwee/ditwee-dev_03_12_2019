<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{revws}default-bootstrap>review-average_dda9c06f33071c9b6fc237ee164109d8'] = '🇫🇷AVIS';
$_MODULE['<{revws}default-bootstrap>review-average_13aceb08031b13081a0d0d8f0b5691f2'] = 'Découvrez les avis';
$_MODULE['<{revws}default-bootstrap>review-average_ff7872689b6546ccf30f86adf2ac8b1a'] = 'Soyez le premier à laisser un avis ⭐️⭐️⭐️';
$_MODULE['<{revws}default-bootstrap>review-average_afe25e5f86edd170ae51564fa30dae44'] = 'Votre avis n\'a pas encore été approuvé';
$_MODULE['<{revws}default-bootstrap>review-average_1207aec090b7d7b523bb2726283ae126'] = 'Connectez vous pour laisser un avis ⭐️';
$_MODULE['<{revws}default-bootstrap>review-average_08c7d6f84301ee7d0aab0a5f67edc419'] = 'Pas encore d\'avis, exprimez vous !';
