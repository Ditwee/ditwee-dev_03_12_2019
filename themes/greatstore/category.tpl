{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{if isset($category)}
	{if $category->id AND $category->active}
    	{if $scenes || $category->description || $category->id_image}
    		{capture name='displayCatTop'}{hook h="displayCatTop"}{/capture}
    		{if $smarty.capture.displayCatTop}
	    		<div class="displaycattop_area">
	    			{$smarty.capture.displayCatTop}
	    		</div>
    		{/if}
			<div class="content_scene_cat">
            	{if $scenes}
                 	<div class="content_scene">
                        {include file="$tpl_dir./scenes.tpl" scenes=$scenes}
                        {if $category->description}
                            <div class="cat_desc rte">
                            {if Tools::strlen($category->description) > 350}
                                <div id="category_description_short">{$description_short}</div>
                                <div id="category_description_full" class="unvisible">{$category->description}</div>
                                <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
                            {else}
                                <div>{$category->description}</div>
                            {/if}
                            </div>
                        {/if}
                    </div>
				{else}
					{if $xprt.cat_scene_banner_block == '1'}
                    <div class="content_scene_cat_bg {if $xprt.cat_scene_banner_bg == '0'}content_scene_cat_normal{/if}"  {if $category->id_image && $xprt.cat_scene_banner_bg == '1'}style="background-image:url({$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_default')|escape:'html':'UTF-8'});"{/if}>
                        {if $category->description}
                            <div class="cat_desc">
                            <span class="category-name">
                                {strip}
                                    {$category->name|escape:'html':'UTF-8'}
                                    {if isset($categoryNameComplement)}
                                        {$categoryNameComplement|escape:'html':'UTF-8'}
                                    {/if}
                                {/strip}
                            </span>
                            {if Tools::strlen($category->description) > 350}
                                <div id="category_description_short" class="rte">{$description_short}</div>
                                <div id="category_description_full" class="unvisible rte">{$category->description}</div>
                                <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
                            {else}
                                <div class="rte">{$category->description}</div>
                            {/if}
                            </div>
                        {/if}
                     </div> <!-- Category Banner image end-->
					{/if}
                  {/if}
            </div>
		{/if}
		{if $xprt.cat_subcat_thumb == '1'}
		{if isset($subcategories)}
        {if (isset($display_subcategories) && $display_subcategories eq 1) || !isset($display_subcategories) }
		<div id="subcategories">
			<p class="subcategory-heading">{l s='Subcategories'}</p>
			<ul class="clearfix">
			{foreach from=$subcategories item=subcategory}
				<li>
                	<div class="subcategory-image">
						<a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}" title="{$subcategory.name|escape:'html':'UTF-8'}" class="img">
						{if $subcategory.id_image}
							{assign var="catextraextrathumb" value="xprtcatextra{$subcategory.id_category}extrathumb"}
							{assign var="catextraextrathumbimg" value="{Configuration::get({$catextraextrathumb})}"}
							{assign var="extraimage" value="{$modules_dir}xprtcategoryextraimg/images/{$catextraextrathumbimg}"}
							{if (isset($catextraextrathumbimg) && !empty($catextraextrathumbimg))}
								<img class="img-responsive" src="{$extraimage}" alt="{$subcategory.name|escape:'html':'UTF-8'}">
							{else}
								<img class="replace-2x" src="{$img_dir}no-image.jpg" alt=""/>
							{/if}
						{else}
							<img class="img-responsive replace-2x" src="{$img_cat_dir}{$lang_iso}-default-medium_default.jpg" alt="{$subcategory.name|escape:'html':'UTF-8'}" width="{$mediumSize.width}" height="{$mediumSize.height}" />
						{/if}
					</a>
                   	</div>
					<h5><a class="subcategory-name" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|truncate:25:'...'|escape:'html':'UTF-8'}</a></h5>
					{if $subcategory.description}
						<div class="cat_desc">{$subcategory.description}</div>
					{/if}
				</li>
			{/foreach}
			</ul>
		</div>
        {/if}
		{/if}
		<!-- Subcategories end -->
		{/if}
		{if $products}
			<div class="content_sortPagiBar clearfix">
            	<div class="sortPagiBar clearfix">
            		{include file="./product-sort.tpl"}
                	{include file="./nbr-product-page.tpl"}
                	{include file="$tpl_dir./pagination.tpl"}
				</div>
				{capture name='displaySortPagiBar'}{hook h='displaySortPagiBar'}{/capture}
				{if isset($smarty.capture.displaySortPagiBar) && !empty($smarty.capture.displaySortPagiBar)}
	                <div class="top-pagination-content clearfix">
						{include file="$tpl_dir./product-count.tpl"}
	            		{$smarty.capture.displaySortPagiBar}
	                </div>
                {/if}
			</div>
			<div class="lg_product">
				{include file="./product-list.tpl" products=$products}
			</div>
			<div class="content_sortPagiBar">
				<div class="bottom-pagination-content clearfix">
					{include file="$tpl_dir./product-count.tpl" paginationId='bottom'}
                    {include file="./pagination.tpl" paginationId='bottom'}
				</div>
			</div>
		{else}
			<div class="lg_product">
				<center><h4>{l s='No products in this category'}</h4></center>
			</div>
		{/if}
	{elseif $category->id}
		<p class="alert alert-warning">{l s='This category is currently unavailable.'}</p>
	{/if}
{/if}
{capture name='displayCatBottom'}{hook h="displayCatBottom"}{/capture}
{if $smarty.capture.displayCatBottom}
	<div class="displaycatbottom_area">
		{$smarty.capture.displayCatBottom}
	</div>
{/if}