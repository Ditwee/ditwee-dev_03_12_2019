{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}>
	<head>
		<meta charset="utf-8" />
		<title>{strip}{$meta_title|replace:'| DITWEE':''|replace:'- Ditwee':''|escape:'html':'UTF-8'}{/strip} | DITWEE</title>
		{if isset($meta_description) AND $meta_description}
			<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
			<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
		{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		
		<!-- FAVICON
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		-->
		<link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="/img/favicon/manifest.json">
		<link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#f1766a">
		<link rel="shortcut icon" href="/img/favicon/favicon.ico">
		<meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
		<meta name="theme-color" content="#f1766a">
		<!-- /FAVICON --> 
		
		{if isset($css_files)}
			{foreach from=$css_files key=css_uri item=media}
				<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
			{/foreach}
		{/if}
		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
			
			{$js_def|replace:'type="text/javascript"':''}
			
			{foreach from=$js_files item=js_uri}
			<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
			{/foreach}
		{/if}
		{$HOOK_HEADER}
		{* body font *}
		<link href="{xprtthemeeditormod::GetGoogleFontsURL('bodyfont')|escape:"html"}" rel='stylesheet' type='text/css'>
		{* heading font *}
		<link href="{xprtthemeeditormod::GetGoogleFontsURL('headingfont')|escape:"html"}" rel='stylesheet' type='text/css'>
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		
		<link rel="stylesheet" href="/themes/greatstore/css/fontello.css" type="text/css" media="all" />
		<link rel="stylesheet" href="/themes/greatstore/css/font-awesome.css" type="text/css" media="all" />
		
		<script src="//code.tidio.co/ho0cq83kbht6ylu5ko0wjnldavgjtbdm.js"></script>
		
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{else} show-left-column{/if}{if $hide_right_column} hide-right-column{else} show-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso} {$xprt.body_page_style} {$xprt.page_header_style} {$xprt.gs_style_demo} {if isset($xprt.home_title_style)}page_heading_{$xprt.home_title_style}{/if} loading">
	{include file="./setting_files.tpl"}
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
			</div>
		{/if}
		<div id="page">
			<div class="header-container {$xprt.page_header_style}">
				<header id="header" class="header">
					{capture name='displayBanner'}{hook h='displayBanner'}{/capture}
					{if $smarty.capture.displayBanner}
						<div class="banner">
							<div class="container">
								<div class="row">
									{$smarty.capture.displayBanner}
								</div>
							</div>
						</div>
					{/if}
					{capture name='displayNav'}{hook h='displayNav'}{/capture}
					{if $smarty.capture.displayNav}
						<div class="nav">
							<div class="container">
								<div class="row">
									<nav>{$smarty.capture.displayNav}</nav>
								</div>
							</div>
						</div>
					{/if}
					<div class="header_top_area">
						<div class="container header_top">
							<div class="row">
								<div class="header_top_left col-xs-12 col-sm-{$xprt.header_top_left_col} clearfix">
									{hook h='displayTopLeft'}
								</div>
								<div class="header_top_right col-xs-12 col-sm-{12 - $xprt.header_top_left_col} clearfix">
									{capture name='displayTopRightOne'}{hook h='displayTopRightOne'}{/capture}
									{capture name='displayTopRightTwo'}{hook h='displayTopRightTwo'}{/capture}
									{if $smarty.capture.displayTopRightOne || $smarty.capture.displayTopRightTwo}
									<div class="row">
									
										{if $smarty.capture.displayTopRightOne}
											<div class="header_top_right_one col-xs-12 col-sm-{$xprt.header_top_right_inner_col} clearfix">
												{$smarty.capture.displayTopRightOne}
												
												
											</div>
										{/if}
										{if $smarty.capture.displayTopRightTwo}
											<div class="header_top_right_two col-xs-12 col-sm-{12 - $xprt.header_top_right_inner_col} clearfix">
												{$smarty.capture.displayTopRightTwo}
											</div>
										{/if}
									</div>
									{/if}
									{if isset($HOOK_TOP)}
										<div class="row">
											<div class="header_top_right_bottom col-sm-12 clearfix">
												{$HOOK_TOP}	
											</div>
										</div>
									{/if}
								</div>
							</div>
						</div>
					</div>
					{capture name='displayMainMenu'}{hook h='displayMainMenu'}{/capture}
					{if $smarty.capture.displayMainMenu}
					<div class="main_menu_area {if $xprt.sticky_menu == 1}stickyclass{/if}">
						<div class="container main_menu">
							<div class="row">
								<div class="col-sm-12">
									{$smarty.capture.displayMainMenu}
								</div>
							</div>
						</div>
					</div>
					{/if}
				</header>
				{if $page_name !='index'}
					{if $xprt.page_heading_style == 'page_heaing_bg'}
						{include file="$tpl_dir./page-heading-area.tpl"}
					{else}
						{include file="$tpl_dir./page-heading-simple.tpl"}
					{/if}
				{/if}
			</div> <!-- header-container -->
			<div class="columns-container">
				{if $page_name == 'index'}
					<div class="top_column_area">
						<div class="container-fluid">
							<div id="slider_row" class="clearfix">
								{capture name='displayTopColumn'}{hook h='displayTopColumn'}{/capture}
								{if $smarty.capture.displayTopColumn}
									<div id="top_column" class="top_column m_height_0">{$smarty.capture.displayTopColumn}</div>
								{/if}
							</div>
						</div>
					</div>
				{/if}
				<div id="columns" class="columns">
					<div class="container">
						<div class="row">
							{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
							<div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval} {if $HOOK_LEFT_COLUMN !=''}col-sm-push-{$left_column_size}{/if} m_height_0">
	{/if}