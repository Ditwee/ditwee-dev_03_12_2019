{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
		</div> <!-- center_column -->
	</div> <!-- row -->
</div> <!-- container -->
{* START HOOK ASSIGN *}
{assign var="displayhometop" value="{hook h='displayHomeTop'}"}
{assign var="displayhomefullwidthmiddle" value="{hook h='displayHomeFullWidthMiddle'}"}
{assign var="displayhomemiddle" value="{hook h='displayHomeMiddle'}"}
{assign var="displayhomefullwidthbottom" value="{hook h='displayHomeFullWidthBottom'}"}
{assign var="displayhomebottom" value="{hook h='displayHomeBottom'}"}
{assign var="displayhometopleft" value="{hook h='displayHomeTopLeft'}"}
{assign var="displayhometopleftone" value="{hook h='displayHomeTopLeftOne'}"}
{assign var="displayhometoplefttwo" value="{hook h='displayHomeTopLeftTwo'}"}
{assign var="displayhometopright" value="{hook h='displayHomeTopRight'}"}
{assign var="displayhometoprightone" value="{hook h='displayHomeTopRightOne'}"}
{assign var="displayhometoprighttwo" value="{hook h='displayHomeTopRightTwo'}"}
{assign var="displayhometopleftrightbottom" value="{hook h='displayHomeTopLeftRightBottom'}"}
{assign var="displayhomebottomleft" value="{hook h='displayHomeBottomLeft'}"}
{assign var="displayhomebottomleftone" value="{hook h='displayHomeBottomLeftOne'}"}
{assign var="displayhomebottomlefttwo" value="{hook h='displayHomeBottomLeftTwo'}"}
{assign var="displayhomebottomright" value="{hook h='displayHomeBottomRight'}"}
{assign var="displayhomebottomrightone" value="{hook h='displayHomeBottomRightOne'}"}
{assign var="displayhomebottomrighttwo" value="{hook h='displayHomeBottomRightTwo'}"}
{* END HOOK ASSIGN *}
{* start displayhometop *}
{if isset($displayhometop) && $displayhometop|trim}
<div class="displayhometop_area">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				{$displayhometop}
			</div>
		</div>
	</div>
</div>
{/if}
{* end displayhometop *}
{* start HOOK_HOME_TAB_CONTENT *}
{if isset($HOOK_HOME_TAB_CONTENT) && $HOOK_HOME_TAB_CONTENT|trim}
	<div class="displayhometab_area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				    {if isset($HOOK_HOME_TAB) && $HOOK_HOME_TAB|trim}
				    	<div class="page_title_area {$xprt.home_title_style}">
		    		        <ul id="home-page-tabs" class="home_product_tab nav nav-tabs clearfix">
		    					{$HOOK_HOME_TAB}
		    				</ul>
		    				{if isset($xprt.home_product_tab_sub_heading)}
		    					<p class="page_subtitle d_none">{$xprt.home_product_tab_sub_heading}</p>
		    				{/if}
		    				<div class="heading-line d_none"><span></span></div>
				    	</div>
					{/if}
					<div id="home-page-tab-content" class="home_product_tab_content tab-content carousel" style="margin:{$xprt.home_product_tab_margin}">
						{$HOOK_HOME_TAB_CONTENT}
					</div>
				</div>
			</div>
		</div>
	</div>
{/if}
{* end HOOK_HOME_TAB_CONTENT *}
{* start HOOK_HOME *}
{if isset($HOOK_HOME) && $HOOK_HOME|trim}
	<div class="displayhome_area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					{$HOOK_HOME}
				</div>
			</div>
		</div>
	</div>
{/if}
{* end HOOK_HOME *}
{* start displayhometop *}
{if (isset($displayhometopleft) && $displayhometopleft)
|| (isset($displayhometopleftone) && $displayhometopleftone)
|| (isset($displayhometoplefttwo) && $displayhometoplefttwo)
|| (isset($displayhometopright) && $displayhometopright)
|| (isset($displayhometoprightone) && $displayhometoprightone)
|| (isset($displayhometoprighttwo) && $displayhometoprighttwo)
}
<div class="displayhometopleftright_area">
	<div class="container">
		<div class="row">
		    <div id="displayhometopleft" class="col-xs-12 col-sm-{$xprt.hometopleftright_col} m_height_auto">
		        {$displayhometopleft}
		        {if (isset($displayhometopleftone) && $displayhometopleftone) || (isset($displayhometoplefttwo) && $displayhometoplefttwo)}
		        <div class="row">
		            <div id="displayhometopleftone" class="col-xs-12 col-xs-6 col-sm-6">
		                {$displayhometopleftone}
		            </div>
		            <div id="displayhometoplefttwo" class="col-xs-12 col-xs-6 col-sm-6">
		                {$displayhometoplefttwo}
		            </div>
		        </div>
		        {/if}
		    </div>
		    <div id="displayhometopright" class="col-xs-12 col-sm-{12 - $xprt.hometopleftright_col} m_height_auto">
		        {$displayhometopright}
		        {if (isset($displayhometoprightone) && $displayhometoprightone) || (isset($displayhometoprighttwo) && $displayhometoprighttwo)}
		        <div class="row">
		            <div id="displayhometoprightone" class="col-xs-12 col-sm-6">
		                {$displayhometoprightone}
		            </div>
		            <div id="displayhometoprighttwo" class="col-xs-12 col-sm-6">
		                {$displayhometoprighttwo}
		            </div>
		        </div>
		        {/if}
		    </div>
		    {if isset($displayhometopleftrightbottom) && $displayhometopleftrightbottom|trim}
		    <div class="clearfix"></div>
		    <div class="displayhometopleftright_bottom col-sm-12">
		    	{$displayhometopleftrightbottom}
		    </div>
		    {/if}
		</div>
	</div>
</div>
{/if}
{* end displayhometop *}
{* start displayhomefullwidthmiddle *}
{if isset($displayhomefullwidthmiddle) && $displayhomefullwidthmiddle|trim}
<div class="homemiddlefullwidth_area">
	{$displayhomefullwidthmiddle}
</div>
{/if}
{* end displayhomefullwidthmiddle *}
{* start displayhomemiddle *}
{if isset($displayhomemiddle) && $displayhomemiddle|trim}
	<div class="displayhomemiddle_area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					{$displayhomemiddle}
				</div>
			</div>
		</div>
	</div>
{/if}
{* end displayhomemiddle *}
{* start displayhometop *}
{if (isset($displayhomebottomleft) && $displayhomebottomleft)
|| (isset($displayhomebottomleftone) && $displayhomebottomleftone)
|| (isset($displayhomebottomlefttwo) && $displayhomebottomlefttwo)
|| (isset($displayhomebottomright) && $displayhomebottomright)
|| (isset($displayhomebottomrightone) && $displayhomebottomrightone)
|| (isset($displayhomebottomrighttwo) && $displayhomebottomrighttwo)
}
<div class="displayhomebottomleftright_area">
	<div class="container">
		<div class="row">
		    <div id="displayhomebottomleft" class="col-xs-12 col-sm-{$xprt.homebottomleftright_col}">
		        {$displayhomebottomleft}
		        {if (isset($displayhomebottomleftone) && $displayhomebottomleftone) || (isset($displayhomebottomlefttwo) && $displayhomebottomlefttwo)}
		        <div class="row">
		            <div id="displayhomebottomleftone" class="col-xs-12 col-sm-6">
		                {$displayhomebottomleftone}
		            </div>
		            <div id="displayhomebottomlefttwo" class="col-xs-12 col-sm-6">
		                {$displayhomebottomlefttwo}
		            </div>
		        </div>
		        {/if}
		    </div>
		    <div id="displayhomebottomright" class="col-xs-12 col-sm-{12 - $xprt.homebottomleftright_col}">
		        {$displayhomebottomright}
		        {if (isset($displayhomebottomrightone) && $displayhomebottomrightone) || (isset($displayhomebottomrighttwo) && $displayhomebottomrighttwo)}
		        <div class="row">
		            <div id="displayhomebottomrightone" class="col-xs-12 col-sm-6">
		                {$displayhomebottomrightone}
		            </div>
		            <div id="displayhomebottomrighttwo" class="col-xs-12 col-sm-6">
		                {$displayhomebottomrighttwo}
		            </div>
		        </div>
		        {/if}
		    </div>
		</div>
	</div>
</div>
{/if}
{* end displayhometop *}
{* start displayhomefullwidthbottom *}
{if isset($displayhomefullwidthbottom) && $displayhomefullwidthbottom|trim}
<div class="homebottomfullwidth_area">
	{$displayhomefullwidthbottom}
</div>
{/if}
{* end displayhomefullwidthbottom *}
{* start displayhomebottom *}
{if isset($displayhomebottom) && $displayhomebottom|trim}
	<div class="displayhomebottom_area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					{$displayhomebottom}
				</div>
			</div>
		</div>
	</div>
{/if}
{* end displayhomebottom *}
<div class="container">
	<div class="row">
		{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
		<div class="center_column col-xs-12 col-sm-{$cols|intval} m_height_0">