jQuery(document).ready(function($){

//lazy load
// $("img.lazy").unveil();




$("img.lazy").unveil();

$(window).load(function(){
	$("img.lazy").trigger("unveil");
	// $(this).trigger("lookup");
});


$(window).load(function(){
	$("body.loading").removeClass("loading");
	// $(this).trigger("lookup");
});



// Home tab slider
if (typeof(homeTabPerLine) == 'undefined')
	homeTabPerLine = 4;
if (typeof(homeTabPerRows) == 'undefined')
	homeTabPerRows = 1;
if (typeof(homeTabProductScroll) == 'undefined')
	homeTabProductScroll = 'per_item';
if (typeof(homeTabAutoplay) == 'undefined')
	homeTabAutoplay = false;
if (typeof(homeTabNavArrows) == 'undefined')
	homeTabNavArrows = true;
if (typeof(homeTabNavDots) == 'undefined')
	homeTabNavDots = false;

$(window).load(function(){

	var homeTabCarousel = '';
	var headingArrow = '';
	
	function homeTabProductCarousel(){

		var arrowSelect = '';

		homeTabCarousel = $('#home-page-tab-content.tab-content .tab-pane.active ul.product_list');

		headingArrow = $('#home-page-tabs').prepend('<div class="heading_carousel_arrow"></div>');
				
		arrowSelect =  $('#home-page-tabs').find('.heading_carousel_arrow'); // $(this).find('.title_block');

		homeTabCarousel.slick({
		  infinite: true,
		  autoplay: homeTabAutoplay,
		  infinite: false,
		  //lazyLoad: 'ondemand',
		  //slide: '.ajax_block_product',
		  rows: homeTabPerRows,
		  slidesToShow : homeTabPerLine, //prodHometabCarousel, //prd_per_column,
		  slidesToScroll : (homeTabProductScroll == 'per_item' ? 1 : homeTabPerLine),
		  appendArrows: arrowSelect,
		  dots: homeTabNavDots,
		  arrows: homeTabNavArrows,
		  nextArrow : '<i class="slick-next arrow-double-right"></i>',
		  prevArrow : '<i class="slick-prev arrow-double-left"></i>',
		  responsive:[
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4,
						// slidesToShow : prd_per_column,
					}
				},
				{
					breakpoint: 993,
					settings: {
						slidesToShow: 3,
						// slidesToShow : prd_per_column_tab,
					}
				},
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 3,
						// slidesToShow : prd_per_column_tab,
					}
				},
				{
					breakpoint: 641,
					settings: {
						slidesToShow: 2,
						// slidesToShow : prd_per_column_tab,
					}
				},
				{
					breakpoint: 481,
					settings: {
						slidesToShow: 1,
						// slidesToShow : prd_per_column_mob,
					}
				}
			]
		 
		});

	};

	homeTabProductCarousel();

	$('#home-page-tabs li a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

		//echo.render();
		$(window).trigger("lookup");
		prodMultiImage();
		//prodHoveriImage();
		
		headingArrow.find('.heading_carousel_arrow').remove();
		homeTabCarousel.find('.heading_carousel_arrow').remove();

		homeTabCarousel.slick('unslick');
		
		homeTabProductCarousel();

		homeTabCarousel.slick('init');

	})

});







//product review scroll
$('.comments_advices li').find('a[href="#idTab5"]').click(function(){
	$('.product_details_bottom_tab ul.nav-tabs').find('a[href="#idTab5"]').parents('li').addClass('active').siblings().removeClass('active');
	$('.product_details_bottom_tab .tab-content').find('.tab-pane#idTab5').addClass('active').siblings().removeClass('active');

	var posReview = $('.product_details_bottom_tab ul.nav-tabs').find('a[href="#idTab5"]').offset();

	if($('.header_top').hasClass('sticky')){
		var stickyHeight = $('.sticky').height();

		var newPosReview = parseInt(posReview.top - stickyHeight);

	}
	else{
		var stickyHeight = $('.header_top').height();

		var newPosReview = parseInt(posReview.top - stickyHeight);

	}

	var body = $("html, body");
	body.stop().animate( {scrollTop: newPosReview }, '2000', 'swing', function() {});

});


$(window).load(function(){
	$("#kr_home_promo_title").bigtext();
});





$(window).load(function(){

	/* init Jarallax with jQuery */
    $('div.xprt_parallax_section').jarallax({
        speed: 0.7,
        type: 'scroll',
        enableTransform: !(navigator.userAgent.indexOf('MSIE')!==-1 || navigator.appVersion.indexOf('Trident/') > 0),
        // imgWidth: 1366,
        // imgHeight: 350,
    });

	// if($(document.body).width() > 768){
	// 	$('#testimonial_block_area.kr_parralax1').parallax("50%", 0.3, true);
	// };

});



/*Fancybox popup*/

if (!!$.prototype.fancybox)
$('.thumbnail_lightbox').fancybox({
    padding : 0,
    openEffect  : 'elastic'
});

// home video popup
if (!!$.prototype.fancybox)
$('#kr_video_popup').fancybox({
		padding: 0,
		margin: 0,
		titleShow     : false,
		openEffect	:	'elastic',
		closeEffect	:	'elastic',
		transitionIn	:	'none',
		transitionOut	:	'none',
		fitToView: false, //
		autoResize: true,
	    maxWidth: "90%", // 
		width: 800, 
		height: 450,
	    // type: 'html',
	    autoSize: false,
        scrolling: false
		
		// 'speedIn'		:	600, 
		// 'speedOut'		:	200, 
});

// padding : 0,
// margin:0,
// openEffect  : 'elastic',
// maxWidth	: 800,
// maxHeight	: 450,
// fitToView	: false,
// width		: '70%',
// height		: '70%',
// autoSize	: false,
// closeClick	: false,
//openEffect	: 'none',
//closeEffect	: 'none'







// 
// ! parallax section
// **********************************************************************/
// if($(window.body).width() > 767){
// 	$('.kr_parallax_section').each(function(){
//         $(this).parallax('50%',0.05);
//     });
// };






// sidebar panel 

//sidebars handler for jillix
// All sides
// var sides = ["left", "top", "right", "bottom"];
//$("h1 span.version").text($.fn.sidebar.version);

// Initialize sidebars
// for (var i = 0; i < sides.length; ++i) {
//     var cSide = sides[i];
//     $(".sidebar_panel." + cSide).sidebar({side: cSide});
// }

// Click handlers
// $(".sidebar_nav_icon[data-action]").on("click", function () {
//     var $this = $(this);
//     // console.log($this);
//     var action = $this.attr("data-action");
//     var side = $this.attr("data-side");
//     $(".sidebar_panel." + side).trigger("sidebar:" + action);
//     $this.toggleClass('active');
//     return false;
// });





// sidebar panel 

//sidebars handler
// All sides
var sides = ["left", "top", "right", "bottom"];
//$("h1 span.version").text($.fn.sidebar.version);

// Initialize sidebars
for (var i = 0; i < sides.length; ++i) {
    var cSide = sides[i];
    
    $(".sidebar_panel." + cSide).sidebar({side: cSide});
}


// Click handlers
$.fn.sidebarIconClick = function(){
	this.on('click', function(){
		
		var $this = $(this);
		var action = $this.attr("data-action");
		var side = $this.attr("data-side");
		var thispanel = $this.attr("data-id");
		$("#" + thispanel + "." + side).trigger("sidebar:" + action);
		var panelWidth = parseInt($("#" + thispanel + "." + side).css(side))*-1;
		var options = {};
		options[side] = panelWidth+'px';
		$('#page').animate(options, 200);
		$this.toggleClass('active');
		$('body').toggleClass('xprtsidebarpanel_open');
		$('#page').delay(200).css({'left':'auto','right':'auto'});
		return false;

	});
}


$( "#header_nav_icon" ).sidebarIconClick(); 

$("#header .shopping_cart > a.sidebar_click").sidebarIconClick();

$(".sidebar_panel_close").on("click", function () {
    
    var $this = $(this);
    var action = $this.attr("data-action");
    var side = $this.attr("data-side");
    $this.parents(".sidebar_panel." + side).trigger("sidebar:" + action);
    var panelWidth = parseInt($this.parents(".sidebar_panel." + side).css(side))*-1;
    var options = {};
    options[side] = panelWidth+'px';
    $('#page').animate(options, 200);
    $('body').toggleClass('xprtsidebarpanel_open');
    $('#page').delay(200).css({'left':'auto','right':'auto'});
    return false;
});











// scroll to top
//var mainMneuHeight = parseInt($('#header .main_menu_area').scrollTop());


$(window).scroll(function () {
	var winHeight = $(window).scrollTop() + $(window).height();

	// var winHeight = $('#header .main_menu_area');
	// Scroll to top function
	if( winHeight > $(window).height() ){
		$('#scroll_top').css("transform", "translateX(0px)");
	}
	else {
		$('#scroll_top').css("transform", "translateX(150px)");
	}
});

//scroll up event
$('#scroll_top').click(function(e){
	e.preventDefault();
	$('body,html').stop().animate({
		scrollTop:0
	},800,'easeInQuad')
		return false;
});




// Sticky menu

if( $('#header .main_menu_area.stickyclass').is(':hidden') ){
	var headerHeightobj = $('#header .block_top_menu').offset();
	var headerHeight = headerHeightobj.top + 40;
}else{
	var headerHeightobj = $('#header .main_menu_area.stickyclass').offset();
	
	if (typeof headerHeightobj != 'undefined') {

		var headerHeight = headerHeightobj.top;
	}
}

var docWidth = $(document.body).width();
if(typeof headerHeightobj != 'undefined' && docWidth > 300){ 
	
	var winHeight = $(window).height();
	$(window).scroll(function(){

		if(typeof headerHeight != 'undefined'){ 
			var winTopPos  = $(window).scrollTop();

			if(winTopPos > headerHeight){
				$('#header .main_menu_area.stickyclass').addClass('sticky');
			}
			else{
				$('#header .main_menu_area.stickyclass').removeClass('sticky');
			}
		}
	});
}




$('.tooltiptop').tooltipster({
	animation: 'grow',
	position: 'top',
});



/*Fancybox popup*/

if (!!$.prototype.fancybox)
$('#size_chart').fancybox({
    padding : 20,
    openEffect  : 'fade',
    maxWidth	: 500,
    maxHeight	: 450,
    fitToView	: false,
    width		: '100%',
    height		: '100%',
});








}); // doc ready
