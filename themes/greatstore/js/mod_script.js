jQuery(document).ready(function($){

	
// new product carousel
// if (typeof(xprt_newproduct_autoplay) == 'undefined')
// 	xprt_newproduct_autoplay = false;
// if (typeof(xprt_newproduct_playagain) == 'undefined')
// 	xprt_newproduct_playagain = false;
// if (typeof(xprt_newproduct_lazyload) == 'undefined')
// 	xprt_newproduct_lazyload = false;
// if (typeof(xprt_newproduct_pauseonhover) == 'undefined')
// 	xprt_newproduct_pauseonhover = true;
// if (typeof(xprt_newproduct_speed) == 'undefined')
// 	xprt_newproduct_speed = 300;
// if (typeof(xprt_newproduct_navarrow) == 'undefined')
// 	xprt_newproduct_navarrow = true;
// if (typeof(xprt_newproduct_navdots) == 'undefined')
// 	xprt_newproduct_navdots = false;
// if (typeof(xprt_newproduct_productrows) == 'undefined')
// 	xprt_newproduct_productrows = 1;
// if (typeof(xprt_newproduct_productscroll) == 'undefined')
// 	xprt_newproduct_productscroll = 'per_item';
// if (typeof(xprt_newproduct_devicelg) == 'undefined')
// 	xprt_newproduct_devicelg = 4;
// if (typeof(xprt_newproduct_devicemd) == 'undefined')
// 	xprt_newproduct_devicemd = 4;
// if (typeof(xprt_newproduct_devicesm) == 'undefined')
// 	xprt_newproduct_devicesm = 3;
// if (typeof(xprt_newproduct_devicexs) == 'undefined')
// 	xprt_newproduct_devicexs = 2;

// var xpertNewProductBlock = $('#xprtnewproductblock');
// var sliderSelect = xpertNewProductBlock.find('ul.product_list.carousel'); 
// var arrowSelect = xpertNewProductBlock.find('.heading_carousel_arrow'); 
// if (!!$.prototype.slick)
// sliderSelect.slick({
// 	infinite: xprt_newproduct_playagain,
// 	autoplay: xprt_newproduct_autoplay,
// 	infinite: xprt_newproduct_playagain,
// 	pauseOnHover: xprt_newproduct_pauseonhover,
// 	dots: xprt_newproduct_navdots,
// 	arrows: xprt_newproduct_navarrow,
// 	appendArrows: arrowSelect,
// 	nextArrow : '<i class="slick-next arrow-double-right"></i>',
// 	prevArrow : '<i class="slick-prev arrow-double-left"></i>',
// 	rows: xprt_newproduct_productrows,
// 	// slidesPerRow: 3,
// 	slidesToShow : xprt_newproduct_devicelg,
// 	slidesToScroll : (xprt_newproduct_productscroll == 'per_item' ? 1 : xprt_newproduct_devicelg),
// 	responsive:[
// 		{
// 			breakpoint: 1200,
// 			settings: {
// 				slidesToShow: xprt_newproduct_devicelg,
// 				slidesToScroll : (xprt_newproduct_productscroll == 'per_item' ? 1 : xprt_newproduct_devicelg),
// 				// slidesToShow : prd_per_column,
// 			}
// 		},
// 		{
// 			breakpoint: 993,
// 			settings: {
// 				slidesToShow: xprt_newproduct_devicemd,
// 				slidesToScroll : (xprt_newproduct_productscroll == 'per_item' ? 1 : xprt_newproduct_devicemd),
// 				// slidesToShow : 2,
// 			}
// 		},
// 		{
// 			breakpoint: 769,
// 			settings: {
// 				slidesToShow: xprt_newproduct_devicesm,
// 				slidesToScroll : (xprt_newproduct_productscroll == 'per_item' ? 1 : xprt_newproduct_devicesm),
// 				// slidesToShow : 2,
// 			}
// 		},
// 		{
// 			breakpoint: 641,
// 			settings: {
// 				slidesToShow: xprt_newproduct_devicexs,
// 				slidesToScroll : (xprt_newproduct_productscroll == 'per_item' ? 1 : xprt_newproduct_devicexs),
// 				// slidesToShow : 2,
// 			}
// 		},
// 		{
// 			breakpoint: 481,
// 			settings: {
// 				slidesToShow: 1,
// 				slidesToScroll : 1,
// 				// slidesToShow : 1,
// 			}
// 		}
// 	]
// });












// Promotional block classic carousel
var selectSlider = $('.promotional_slider .image_thumnail_carousel');
if (!!$.prototype.slick)
selectSlider.slick({
	infinite: true,
	dots: true,
	autoplay: false,
	infinite: true,
	slide: '.image_thumnail',
	slidesToShow : 1,
	slidesToScroll : 1,
	arrows: false,
	nextArrow : '<i class="slick-next arrow-double-right"></i>',
	prevArrow : '<i class="slick-prev arrow-double-left"></i>',
	variableWidth: false,
});




// Display product block default carousl
var selectSlider = $('.kr_product_layout_default .kr_product_layout_default_left');
selectSlider.find('ul.product_list.carousel').slick({
	infinite: true,
	dots: true,
	autoplay: false,
	infinite: false,
	slide: 'li',
	slidesToShow : 3,
	slidesToScroll : 1,
	arrows: true,
	nextArrow : '<i class="slick-next arrow-double-right"></i>',
	prevArrow : '<i class="slick-prev arrow-double-left"></i>',
	responsive:[
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 2,
				// slidesToShow : prd_per_column,
			}
		},
		{
			breakpoint: 993,
			settings: {
				slidesToShow: 2,
				// slidesToShow : 2,
			}
		},
		{
			breakpoint: 769,
			settings: {
				slidesToShow: 2,
				// slidesToShow : 2,
			}
		},
		{
			breakpoint: 640,
			settings: {
				slidesToShow: 1,
				// slidesToShow : 2,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 1,
				// slidesToShow : 1,
			}
		}
	]
});
// Display product block default reverse carousl
var selectSlider = $('.kr_product_layout_default .kr_product_layout_default_right');
selectSlider.find('ul.product_list.carousel').slick({
	infinite: true,
	dots: true,
	autoplay: false,
	infinite: false,
	slide: 'li',
	slidesToShow : 1,
	slidesToScroll : 1,
	arrows: true,
	nextArrow : '<i class="slick-next arrow-double-right"></i>',
	prevArrow : '<i class="slick-prev arrow-double-left"></i>',
	variableWidth: true,
});





// sidebar product carousel
$('.sidebar .block.carousel').each(function(){
	var arrowSelect = '';
	arrowSelect = $(this).find('.title_block');
	$(this).find('ul.products-block').slick({
		infinite: true,
		dots: false,
		autoplay: false,
		infinite: false,
		slidesToShow : 1,
		rows: 3,
		slidesPerRow: 1,
		slidesToScroll : 1,
		arrows: true,
		appendArrows: arrowSelect,
		nextArrow : '<i class="arrow-next arrow-double-right"></i>',
		prevArrow : '<i class="arrow-prev arrow-double-left"></i>',
	});

});
// Home testimonial slider
var selectSlider = $('#testimonial_block_area');
var tesimonialDots = selectSlider.find('.testimonial_dots');
if (!!$.prototype.slick)
selectSlider.find('ul.carousel').slick({
	dots: true,
	appendDots: tesimonialDots,
	autoplay: false,
	infinite: true,
	//slide: '.slides',
	slidesToShow : 1,
	slidesToScroll : 1,
	arrows: false,
});






// display brands
var selectSlider = $('.kr_brands_area');
var arrowSelect = '';

if (typeof(xprtBrandsColumn) == 'undefined')
	xprtBrandsColumn = 5;
// if(typeof brandSliderStyle != 'undefined'){
// 	if(brandSliderStyle == 'slider'){
// 		arrowSelect = selectSlider.find('.page-heading .heading_carousel_arrow');
// 	}
// 	else{
// 		arrowSelect = selectSlider.find('ul.kr_brands');
// 	}
// }



if (!!$.prototype.slick)
selectSlider.find('ul.kr_brands.carousel').slick({
	//infinite: true,
	dots: false,
	autoplay: false,
	infinite: false,
	slide: 'li',
	// rtl: true,
	slidesToShow : xprtBrandsColumn,//prd_per_column,
	slidesToScroll : 1,
	arrows: true,
	//appendArrows: arrowSelect,
	nextArrow : '<i class="slick-next arrow-double-right"></i>',
	prevArrow : '<i class="slick-prev arrow-double-left"></i>',
	responsive:[
		{
			breakpoint: 320,
			settings: {
				slidesToShow : 1,
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow : 1,
			}
		},
		{
			breakpoint: 640,
			settings: {
				slidesToShow : (xprtBrandsColumn - 2),
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow : (xprtBrandsColumn - 2),
			}
		},
		{
			breakpoint: 992,
			settings: {
				slidesToShow : (xprtBrandsColumn - 1),
			}
		}
		
		
	]
});





// home small product carousel
$('.xprt_product_home_small .block.carousel').each(function(){

	var arrowSelect = '';
			
	arrowSelect = $(this).children('.title_block');


	$(this).find('.products-block ul').slick({
		infinite: true,
		dots: false,
		autoplay: false,
		infinite: false,
		//slide: 'li',
		rows: 3,
		slidesToShow : 1,
		// slidesPerRow: 1,
		slidesToScroll : 1,
		arrows: true,
		appendArrows: arrowSelect,
		nextArrow : '<i class="arrow-next arrow-double-right"></i>',
		prevArrow : '<i class="arrow-prev arrow-double-left"></i>',
	});

});




// block quick search
$('.header_block .search_icon').on('click', function(e){
	e.stopPropagation();
	var subUl = $(this).next('.header_block_content');
	if ($(subUl).is(':animated')) {
        return;
    }
	subUl.animate({'width':'toggle'});
	$(this).toggleClass('active');
});
$('.header_block .search_icon').on('mouseover',function(e){
	var elementHide = $('.header_block .search_icon').next('.header_block_content');
	if(elementHide.is(':hidden')){
		$(this).trigger('click');
	}
});
//('.header_block .search_icon').on('mouseout',function(e){$(document).trigger('click');});

$(document).on('click', function(e){
	e.stopPropagation();
	if($(e.target).closest('#searchbox').length)
	  return;
	var elementHide = $('.header_block .search_icon').next('.header_block_content');
	if(elementHide.is(':visible')){
		$(elementHide).animate({'width':'hide'});
		$('.header_block .search_icon').removeClass('active');
	}
	
});



// header top nav icon /permanent link

$('#header .header_icon_block .nav_icon_regular').on('click', function(e){
	e.stopPropagation();
	var subUl = $(this).next('.header_icon_content');
	subUl.animate({'height':'toggle'});
	$(this).toggleClass('active');
});

$(document).on('click', function(e){
	e.stopPropagation();
	if($(e.target).closest('#header .header_icon_block .header_icon_content').length)
	  return;
	var elementHide = $('.header_icon_block .nav_icon_regular').next('.header_icon_content');
	if(elementHide.is(':visible')){
		$(elementHide).animate({'height':'hide'});
		$('#header .header_icon_block .nav_icon_regular').removeClass('active');
	}
	
});





// home xip blog block mod
var xipblogpost = $('.home_blog_post_area');

var sliderSelect = xipblogpost.find('.home_blog_post_inner.carousel'); 
var arrowSelect = xipblogpost.find('.heading_carousel_arrow'); 

if (typeof(xipbdp_numcolumn) == 'undefined')
	xipbdp_numcolumn = 3;


if (!!$.prototype.slick)
sliderSelect.slick({
	infinite: true,
	dots: false,
	autoplay: false,
	infinite: false,
	slide: '.blog_post',
	slidesToShow : xipbdp_numcolumn,
	slidesToScroll : 1,
	appendArrows: arrowSelect,
	arrows: true,
	prevArrow : '<i class="slick-prev arrow-double-left"></i>',
	nextArrow : '<i class="slick-next arrow-double-right"></i>',
	responsive:[
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3,
				// slidesToShow : prd_per_column,
			}
		},
		{
			breakpoint: 993,
			settings: {
				slidesToShow: 3,
				// slidesToShow : prd_per_column_tab,
			}
		},
		{
			breakpoint: 769,
			settings: {
				slidesToShow: 2,
				// slidesToShow : prd_per_column_tab,
			}
		},
		{
			breakpoint: 641,
			settings: {
				slidesToShow: 2,
				// slidesToShow : prd_per_column_tab,
			}
		},
		{
			breakpoint: 481,
			settings: {
				slidesToShow: 1,
				// slidesToShow : prd_per_column_mob,
			}
		}
	]
});




// blog post format carousel
var selectSlider = $('.home_blog_post_area .blog_post_content_top .post_thumbnail, .blog_post .post_thumbnail');

if (!!$.prototype.slick)
selectSlider.find('.post_format_items.carousel').slick({
	infinite: true,
	dots: false,
	autoplay: true,
	slide: '.item',
	slidesToShow : 1,
	slidesToScroll : 1,
	arrows: true,
	prevArrow : '<span class="arrow-prev"><i class="arrow-double-left"></i></span>',
	nextArrow : '<span class="arrow-next"><i class="arrow-double-right"></i></span>',

});








// home blog block footer
var selectSlider = $('.footer_blog_area');
selectSlider.find('.footer_blog_post.carousel').bxSlider({
	mode: 'vertical',
	slideMargin: 5,
	auto: true,
	controls: false,
	responsive: true,
	pager: false,
});





// Block cart ajax search scroll fix
$(window).scroll(function(){
if( $('body > .ac_results').is(':visible') ){
   $('body > .ac_results').hide();
}
 
});



// Instagram popup

$('#xprtinstagram a.fancy_instragram').click(function(){

	$('#xprtinstagram a.fancy_instragram').each(function(){
			$(this).attr('title', $(this).data('title'));
		});
	
	//$(this).attr('title', $(this).data('title'));

	if (!!$.prototype.fancybox)
	$(".fancy_instragram").fancybox({
		padding : 15,
		margin: 30,
		maxWidth	: 640,
		maxHeight	: 640,
		autoSize	: true,
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers:  {
	        title : {
	            type : 'inside',
	            position:"bottom"
	        },
	        overlay : {
	            showEarly : false
	        }
	    },
	    beforeClose: function(){
	    	$('a.fancy_instragram').removeAttr('title');
	    	//$('a.fancy_instragram').attr('title', $('a.fancy_instragram').data('title'));
	    	//console.log(this.title)
	    }
		// afterShow: function () {
	 //        $(".fancybox-title").wrapInner($("<a />", {

	 //            href: this.href, //or your target link
	 //            target: "_blank"
	 //        }));
	 //    }
	});//.trigger("click");

});


// Instagram carousel
var selectSlider = $('#xprtinstagram.carousel ul');

if (typeof(xprtInstaNumColumn) == 'undefined')
	xprtInstaNumColumn = 6;

if (!!$.prototype.slick)
selectSlider.slick({
	infinite: false,
	dots: false,
	autoplay: false,
	slide: 'li',
	slidesToShow : xprtInstaNumColumn,
	slidesToScroll : 1,
	arrows: false,
	prevArrow : '<span class="arrow-prev"><i class="arrow-double-left"></i></span>',
	nextArrow : '<span class="arrow-next"><i class="arrow-double-right"></i></span>',
	responsive:[
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: xprtInstaNumColumn,
				// slidesToShow : prd_per_column,
			}
		},
		{
			breakpoint: 993,
			settings: {
				slidesToShow: (xprtInstaNumColumn - 2),
				// slidesToShow : prd_per_column_tab,
			}
		},
		{
			breakpoint: 769,
			settings: {
				slidesToShow: (xprtInstaNumColumn - 2),
				// slidesToShow : prd_per_column_tab,
			}
		},
		{
			breakpoint: 641,
			settings: {
				slidesToShow: (xprtInstaNumColumn - 3),
				// slidesToShow : prd_per_column_tab,
			}
		},
		{
			breakpoint: 481,
			settings: {
				slidesToShow: 1,
				// slidesToShow : prd_per_column_mob,
			}
		}
	]
});


selectSlider.on('afterChange', function(event, slick, currentSlide){
	
	$('#xprtinstagram a.fancy_instragram').removeAttr('title');	
	
});


//xprt display simple product parallax
var selectSlider = $('.home_parallax_product_area');

if (!!$.prototype.slick)
selectSlider.find('.home_parallax_product_content.carousel').slick({
	infinite: true,
	dots: true,
	autoplay: false,
	slide: '.home_parallax_product_content_inner',
	slidesToShow : 1,
	slidesToScroll : 1,
	arrows: false,
	prevArrow : '<span class="arrow-prev"><i class="arrow-double-left"></i></span>',
	nextArrow : '<span class="arrow-next"><i class="arrow-double-right"></i></span>',

});


// home block categories
var homeCatBlock = $('.home_categories_area');

var sliderSelect = homeCatBlock.find('.home_categories.carousel'); 
var arrowSelect = homeCatBlock.find('.heading_carousel_arrow'); 

if (typeof(homeblockcategoriescol) == 'undefined')
	homeblockcategoriescol = 4;


if (!!$.prototype.slick)
sliderSelect.slick({
	infinite: true,
	dots: false,
	autoplay: false,
	infinite: false,
	slide: '.home_categories_list',
	slidesToShow : homeblockcategoriescol,
	slidesToScroll : 1,
	appendArrows: sliderSelect,
	arrows: true,
	prevArrow : '<i class="slick-prev arrow-double-left"></i>',
	nextArrow : '<i class="slick-next arrow-double-right"></i>',
	responsive:[
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3,
				// slidesToShow : prd_per_column,
			}
		},
		{
			breakpoint: 993,
			settings: {
				slidesToShow: 3,
				// slidesToShow : prd_per_column_tab,
			}
		},
		{
			breakpoint: 769,
			settings: {
				slidesToShow: 2,
				// slidesToShow : prd_per_column_tab,
			}
		},
		{
			breakpoint: 641,
			settings: {
				slidesToShow: 2,
				// slidesToShow : prd_per_column_tab,
			}
		},
		{
			breakpoint: 481,
			settings: {
				slidesToShow: 1,
				// slidesToShow : prd_per_column_mob,
			}
		}
	]
});







}); //doc ready