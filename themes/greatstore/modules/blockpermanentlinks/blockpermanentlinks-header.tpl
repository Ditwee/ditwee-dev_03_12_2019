


{capture name='displayHeaderNavIcon'}{hook h='displayHeaderNavIcon'}{/capture}
{if $smarty.capture.displayHeaderNavIcon}
<div class="blockpermanentlinks header_icon_block">
	<div {if $xprt.header_top_nav_content == 'header_nav_sidebarpanel'}id="header_nav_icon"{/if} class="header_icon {if $xprt.header_top_nav_content == 'header_nav_regular' || empty($xprt.header_top_nav_content)}nav_icon_regular{/if}" data-action="toggle" data-side="left" data-id="header_nav_sidebarpanel">
		<i class="{if isset($xprt.header_nav_icon)}{$xprt.header_nav_icon}{else}icon-bars{/if}"></i>
	</div>
	<div class="header_icon_content">
		{$smarty.capture.displayHeaderNavIcon}
	</div>
</div>
{/if}






{* <!-- Block permanent links module HEADER -->
<ul id="header_links">
	<li id="header_link_contact"><a href="{$link->getPageLink('contact', true)|escape:'html'}" title="{l s='contact' mod='blockpermanentlinks'}">{l s='contact' mod='blockpermanentlinks'}</a></li>
	<li id="header_link_sitemap"><a href="{$link->getPageLink('sitemap')|escape:'html'}" title="{l s='sitemap' mod='blockpermanentlinks'}">{l s='sitemap' mod='blockpermanentlinks'}</a></li>
	<li id="header_link_bookmark">
		<script type="text/javascript">writeBookmarkLink('{$come_from}', '{$meta_title|addslashes|addslashes}', '{l s='bookmark' mod='blockpermanentlinks' js=1}');</script>
	</li>
</ul>
<!-- /Block permanent links module HEADER --> *}
