<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_a2e9cd952cda8ba167e62b25a496c6c1'] = 'Bloque de información de usuario';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_970a31aa19d205f92ccfd1913ca04dc0'] = 'Añadir un bloque que muestre información sobre el cliente.';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_0c3bf3014aafb90201805e45b5e62881'] = 'Ver mi carrito de compra';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_a85eba4c6c699122b2bb1387ea4813ad'] = 'Carrito';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_deb10517653c255364175796ace3553f'] = 'Producto';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_068f80c7519d0528fb08e82137a72131'] = 'Productos';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_9e65b51e82f2a9b9f72ebe3e083582bb'] = 'vacío';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_2cbfb6731610056e1d0aaacde07096c1'] = 'Ver mi cuenta de cliente';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'Su cuenta';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_83218ac34c1834c26781fe4bde918ee4'] = 'Bienvenido';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_4b877ba8588b19f1b278510bf2b57ebb'] = 'Cerrer sesión';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_c87aacf5673fada1108c9f809d354311'] = 'Cerrar sesión';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_d4151a9a3959bdd43690735737034f27'] = 'Entrar a tu cuenta de cliente';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Iniciar sesión';
$_MODULE['<{blockuserinfo}greatstore>nav_2cbfb6731610056e1d0aaacde07096c1'] = 'Ver mi cuenta de cliente';
$_MODULE['<{blockuserinfo}greatstore>nav_4b877ba8588b19f1b278510bf2b57ebb'] = 'Cerrer sesión';
$_MODULE['<{blockuserinfo}greatstore>nav_c87aacf5673fada1108c9f809d354311'] = 'Cerrar sesión';
$_MODULE['<{blockuserinfo}greatstore>nav_d4151a9a3959bdd43690735737034f27'] = 'Entrar a tu cuenta de cliente';
$_MODULE['<{blockuserinfo}greatstore>nav_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Iniciar sesión';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_bea8d9e6a0d57c4a264756b4f9822ed9'] = 'Mi cuenta';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_74ecd9234b2a42ca13e775193f391833'] = 'Mis ordenes';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_89080f0eedbd5491a93157930f1e45fc'] = 'Mis devoluciones';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_9132bc7bac91dd4e1c453d4e96edf219'] = 'Mis hojas de crédito';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_e45be0a0d4a0b62b15694c1a631e6e62'] = 'Mis direcciones';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_95d2137c196c7f84df5753ed78f18332'] = 'Mis cupónes';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_b4b80a59559e84e8497f746aac634674'] = 'Administrar mi información personal';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfo_63b1ba91576576e6cf2da6fab7617e58'] = 'Mis datos personales';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_08bd40c7543007ad06e4fce31618f6ec'] = 'Cuenta';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_2cbfb6731610056e1d0aaacde07096c1'] = 'Ver mi cuenta de cliente';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_d95cf4ab2cbf1dfb63f066b50558b07d'] = 'Mi cuenta';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_9ae79c1fccd231ac7fbbf3235dbf6326'] = 'Mi lista de deseos';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_74ecd9234b2a42ca13e775193f391833'] = 'Mis ordenes';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_9fa6b1d720fe7fa56b87ad9ba4f67c35'] = 'Seguimiento de pedidos';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_89080f0eedbd5491a93157930f1e45fc'] = 'Mis devoluciones';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_9132bc7bac91dd4e1c453d4e96edf219'] = 'Mis hojas de crédito';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_e45be0a0d4a0b62b15694c1a631e6e62'] = 'Mis direcciones';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_95d2137c196c7f84df5753ed78f18332'] = 'Mis cupónes';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_b4b80a59559e84e8497f746aac634674'] = 'Administrar mi información personal';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_63b1ba91576576e6cf2da6fab7617e58'] = 'Mis datos personales';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_4b877ba8588b19f1b278510bf2b57ebb'] = 'Desconéctame';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_c87aacf5673fada1108c9f809d354311'] = 'desconectar';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_d4151a9a3959bdd43690735737034f27'] = 'Acceda a su cuenta de cliente';
$_MODULE['<{blockuserinfo}greatstore>blockuserinfonavicon_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Registrarse';
$_MODULE['<{blockuserinfo}greatstore>nav_bea8d9e6a0d57c4a264756b4f9822ed9'] = 'Mi cuenta';
$_MODULE['<{blockuserinfo}greatstore>nav_74ecd9234b2a42ca13e775193f391833'] = 'Mis ordenes';
$_MODULE['<{blockuserinfo}greatstore>nav_89080f0eedbd5491a93157930f1e45fc'] = 'Mis devoluciones';
$_MODULE['<{blockuserinfo}greatstore>nav_9132bc7bac91dd4e1c453d4e96edf219'] = 'Mis hojas de crédito';
$_MODULE['<{blockuserinfo}greatstore>nav_e45be0a0d4a0b62b15694c1a631e6e62'] = 'Mis direcciones';
$_MODULE['<{blockuserinfo}greatstore>nav_95d2137c196c7f84df5753ed78f18332'] = 'Mis cupónes';
$_MODULE['<{blockuserinfo}greatstore>nav_b4b80a59559e84e8497f746aac634674'] = 'Administrar mi información personal';
$_MODULE['<{blockuserinfo}greatstore>nav_63b1ba91576576e6cf2da6fab7617e58'] = 'Mis datos personales';
