{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if isset($orderProducts) && count($orderProducts)}
    <section id="crossselling" class="page-product-box">
    	<h3 class="productscategory_h2 page-product-heading center">
            {if $page_name == 'product'}
                <em>
                	{l s='Customers also bought' mod='crossselling'}
                	<div class="heading_carousel_arrow"></div>
                </em>
            {else}
                <em>
                	{l s='We recommend' mod='crossselling'}
                	<div class="heading_carousel_arrow"></div>
                </em>
            {/if}
        </h3>
    	<div id="crossselling_list" class="kr_products_block clearfix">
            <ul class="product_list grid row classic  {if $xprt.home_product_style == 'carousel'}carousel{/if} {if $xprt.gray_image_bg == 1}gray_image_bg{/if}">
                {foreach from=$orderProducts item='orderProduct' name=orderProduct}
                    <li class="ajax_block_product col-sm-3" itemprop="isRelatedTo" itemscope itemtype="http://schema.org/Product">
                    	<div class="product-container">
                    		<div class="left-block">
                    			<div class="product-image-container">
                    				<a class="product_img_link" href="{$orderProduct.link|escape:'html':'UTF-8'}" title="{$orderProduct.name|htmlspecialchars}" >
                    				    <img class="img-responsive" itemprop="image" src="{$orderProduct.image}" alt="{$orderProduct.name|htmlspecialchars}" />
                    				</a>
                    			</div>
                    		</div>
                    		<div class="right-block">
                    			<h5 itemprop="name">
                    			    <a class="product-name" itemprop="url" href="{$orderProduct.link|escape:'html':'UTF-8'}" title="{$orderProduct.name|htmlspecialchars}">
                    			        {$orderProduct.name|truncate:45:'...'|escape:'html':'UTF-8'}
                    			    </a>
                    			</h5>

                    			{if $crossDisplayPrice AND $orderProduct.show_price == 1 AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
                    			    <div class="content_price">
                    			        <span class="price">{convertPrice price=$orderProduct.displayed_price}</span>
                    			    </div>
                    			{/if}

                    		</div>
                    	</div>
                    </li>
                {/foreach}
            </ul>
        </div>
    </section>
{/if}
