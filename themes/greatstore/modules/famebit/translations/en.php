<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{famebit}greatstore>famebit_de84bc0e7ac9bfbc99200b7d644cb582'] = 'FameBit';
$_MODULE['<{famebit}greatstore>famebit_6f7a4ef06fdd25392df4b4a0a14abe52'] = 'The easiest way to connect with YouTuber influencers to promote your brand.';
$_MODULE['<{famebit}greatstore>configure_de84bc0e7ac9bfbc99200b7d644cb582'] = 'FameBit';
$_MODULE['<{famebit}greatstore>configure_6f7a4ef06fdd25392df4b4a0a14abe52'] = 'The easiest way to connect with YouTuber influencers to promote your brand.';
$_MODULE['<{famebit}greatstore>configure_35d4a7d5d9d6f7a4637e3d36d452e31c'] = 'Our partner FameBit makes provides a free an easy way to connect with YouTuber influencers to promote your brand.:';
$_MODULE['<{famebit}greatstore>configure_a3949d9d470dd67bb1173985b2dbc4a3'] = 'See the YouTubers who are interested in promoting your brand.';
$_MODULE['<{famebit}greatstore>configure_9b7e5f21bdec302e13cd3568fa82822f'] = 'FameBit has over 10,000 YouTube influencers and are adding more daily.';
$_MODULE['<{famebit}greatstore>configure_507541f8931b6843be49b0b8bdc28660'] = 'Get free proposals from YouTubers interested in working with your brand before ever entering your credit card.';
$_MODULE['<{famebit}greatstore>configure_06eeb135cae49380f726d85f8119cfe7'] = 'Promote your store using influencer marketing.';
$_MODULE['<{famebit}greatstore>configure_e3b5764467f70bd4c66b841e6c7639b6'] = 'FameBit support is always happy to answer your questions!';
$_MODULE['<{famebit}greatstore>configure_5c76ff7feeef2d338459291efa22eb4b'] = 'Find YouTubers to promote your brand';
