<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{logicinstagram}greatstore>logicinstagram_1f050edd90e31f3e361b622b25af2b74'] = 'INSTAGRAM FEEDS By prestalogic.ch';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_6ba576ebb868958014a02224f02fe852'] = 'Afficher les photos Instagram à partir d\'un compte';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_c888438d14855d7d96a2724ee9c306bd'] = 'Paramètres mis à jour';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_4cd587a1fb63a482d93a6ff0fed1d940'] = 'Nom d\'utilisateur Instagram :';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_46dbab4626fb6f4e773fbb527da28fde'] = 'Nombre d\'images :';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_654025bb0b700881658884745da96cdf'] = 'Vous pouvez afficher 20 images au maximum';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_d4c6b7048c249ff06908874ae2015e96'] = 'Format images :';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_c50ccbbf056a44ab60361c3152dab7ba'] = 'Miniature (150 X 150) - Recadrage carré';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_865c40080f0cadfbfa52ae4cf915423b'] = 'Basse résolution (320 x 320)';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_f04e9aeaad96ec3aa985be6868efe5bc'] = 'Résolution standard (612 x 612)';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_bbd72a2d8c2e98ef6a6bd20d709a89e9'] = 'Redimensionner la taille (en pixels) :';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_f1f26c419c29ce2c4f45a1b48aacb48c'] = 'Redimensionner la taille en pixels:';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_8aa6b19a683137d22f7eaeecbca4fb65'] = 'Rafraîchir :';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_98f5449f725c2be645dc1be794d3b6df'] = 'Chaque jour';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_f826cc3c38088c39b0a05cf87435e08f'] = 'Toutes les heures';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{logicinstagram}greatstore>default_55f015a0c5605702f913536afe70cfb0'] = 'Instagram';
$_MODULE['<{logicinstagram}greatstore>default_42b90196b487c54069097a68fe98ab6f'] = 'post';
$_MODULE['<{logicinstagram}greatstore>default_8358a582c0cdc92ec2629cbd641e57fc'] = 'suivi par';
$_MODULE['<{logicinstagram}greatstore>default_d306c37e058603cfea275b98b3286af8'] = 'suiveurs';
$_MODULE['<{logicinstagram}greatstore>default_fa36ec21476e9d82ecaae17568129931'] = 'Voir toutes nos photos Instagram';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_3903aab323863bd2e9b68218a7a65ebd'] = 'Suivre';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_cc71d8f3dc8842c0f4f55e3f5b388ec6'] = 'Likes';
$_MODULE['<{logicinstagram}greatstore>logicinstagram_a5d491060952aa8ad5fdee071be752de'] = 'Commentaires';
