{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if count($categoryProducts) > 0 && $categoryProducts !== false}
<section id="productscategory" class="page-product-box blockproductscategory">
	<h3 class="productscategory_h3 page-product-heading center">
		{* {$categoryProducts|@count}  *}
		<em>
			{l s='Related products' mod='productscategory'}
			<div class="heading_carousel_arrow"></div>
		</em>
	</h3>
	<div id="productscategory_list" class="kr_products_block clearfix">
		<ul class="product_list grid row classic  {if $xprt.home_product_style == 'carousel'}carousel{/if}   {if $xprt.gray_image_bg == 1}gray_image_bg{/if}">
		{foreach from=$categoryProducts item='categoryProduct' name=categoryProduct}
			<li class="ajax_block_product col-sm-3">

				<div class="product-container">
					<div class="left-block">
						<div class="product-image-container">
							<a class="product_img_link" href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)}" title="{$categoryProduct.name|htmlspecialchars}">
								<img class="img-responsive" src="{$link->getImageLink($categoryProduct.link_rewrite, $categoryProduct.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$categoryProduct.name|htmlspecialchars}" />
							</a>
						</div>
					</div>
					<div class="right-block">
						<h5 itemprop="name">
							<a class="product-name" href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)|escape:'html':'UTF-8'}" title="{$categoryProduct.name|htmlspecialchars}">{$categoryProduct.name|truncate:45:'...'|escape:'html':'UTF-8'}</a>
						</h5>
						
						{if $ProdDisplayPrice && $categoryProduct.show_price == 1 && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
							<div class="content_price">
							{if isset($categoryProduct.specific_prices) && $categoryProduct.specific_prices
							&& ($categoryProduct.displayed_price|number_format:2 !== $categoryProduct.price_without_reduction|number_format:2)}

								<span class="price special-price">{convertPrice price=$categoryProduct.displayed_price}</span>
								{if $categoryProduct.specific_prices.reduction && $categoryProduct.specific_prices.reduction_type == 'percentage'}
									<span class="price-percent-reduction small">-{$categoryProduct.specific_prices.reduction * 100}%</span>
								{/if}
								<span class="old-price">{displayWtPrice p=$categoryProduct.price_without_reduction}</span>

							{else}
								<span class="price">{convertPrice price=$categoryProduct.displayed_price}</span>
							{/if}
							</div>
						{else}
						<br />
						{/if}

					</div>
				</div>

				

				
				

				{* <div class="clearfix" style="margin-top:5px">
					{if !$PS_CATALOG_MODE && ($categoryProduct.allow_oosp || $categoryProduct.quantity > 0)}
						<div class="no-print">
							<a class="exclusive button ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$categoryProduct.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$categoryProduct.id_product|intval}" title="{l s='Add to cart' mod='productscategory'}">
								<span>{l s='Add to cart'}</span>
							</a>
						</div>
					{/if}
				</div> *}

			</li>
		{/foreach}
		</ul>
	</div>
</section>
{/if}
