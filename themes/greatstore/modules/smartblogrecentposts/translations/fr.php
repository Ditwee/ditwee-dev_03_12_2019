<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{smartblogrecentposts}greatstore>smartblogrecentposts_ae7bbe3f4039ab6bac9477cff467bee9'] = 'Intelligente blog les messages récents';
$_MODULE['<{smartblogrecentposts}greatstore>smartblogrecentposts_a14895030612094493056c982a7089c9'] = 'Le plus puissant Presta Shop Blog d\'étiquette de module - par smartdatasoft';
$_MODULE['<{smartblogrecentposts}greatstore>smartblogrecentposts_fa214007826415a21a8456e3e09f999d'] = 'Êtes-vous sûr de vouloir supprimer vos informations?';
$_MODULE['<{smartblogrecentposts}greatstore>smartblogrecentposts_21ee0d457c804ed84627ec8345f3c357'] = 'Les paramètres ont été mis à jour avec succès.';
$_MODULE['<{smartblogrecentposts}greatstore>smartblogrecentposts_c54f9f209ed8fb4683e723daa4955377'] = 'Cadre général';
$_MODULE['<{smartblogrecentposts}greatstore>smartblogrecentposts_d13ccfd7bbff943d14da1ec7e75a9e73'] = 'Afficher le nombre de messages récents';
$_MODULE['<{smartblogrecentposts}greatstore>smartblogrecentposts_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{smartblogrecentposts}greatstore>smartblogrecentposts_58009509dfadf30a0c307129b13137d2'] = 'Articles récents';
