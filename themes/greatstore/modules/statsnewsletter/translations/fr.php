<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_ffb7e666a70151215b4c55c6268d7d72'] = 'Bulletin';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_5106250927c6e24c99601968284066de'] = 'Ajoute un onglet avec un graphique montrant les inscriptions à la newsletter de la planche de bord Statistiques.';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_61a898af87607e3f4d41c3613d8761c7'] = 'Inscriptions des clients:';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_7fe462207f98012d9ff00cf0e6633c94'] = 'Inscriptions des visiteurs: ';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_64342cd480b27dfeefb08bace6e82fdc'] = 'Tous les deux:';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_998e4c5c80f27dec552e99dfed34889a'] = 'CSV Export';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_c01fb34ae495b6157666197caabdc303'] = 'Le module «bloc Newsletter\" doit être installé.';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_cf74c2815ab62be1efa55a4a5d3f46a4'] = 'Statistiques Newsletter';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_4b6f7d34a58ba399f077685951d06738'] = 'clientèle';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_ae5d01b6efa819cc7a7c05a8c57fcc2c'] = 'Visiteurs';
$_MODULE['<{statsnewsletter}greatstore>statsnewsletter_130c5b3473c57faa76e2a1c54e26f88e'] = 'Les deux';
