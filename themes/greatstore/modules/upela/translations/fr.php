<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{upela}greatstore>upela_8ae4a996d1c51d4169d7a6bbfa7452da'] = 'Upela';
$_MODULE['<{upela}greatstore>upela_4c8e0e3d9c8adece907241d8472eca75'] = 'La meilleure façon d’expédier un colis Économisez sur le prix, pas sur le service';
$_MODULE['<{upela}greatstore>upela_7f15f0868de3f02fa049840f51e51cf7'] = 'Aller sur Upela.com';
$_MODULE['<{upela}greatstore>upela_4c3e6a7d57377e381f273d5278d17f2c'] = 'Mode activé :';
$_MODULE['<{upela}greatstore>upela_cebd5bbe0ffdecc270a8a324e5a277dd'] = 'Production';
$_MODULE['<{upela}greatstore>upela_a9a7198c63ec0f16a965328935eff00d'] = 'Modifier le mode';
$_MODULE['<{upela}greatstore>upela_af8764b2c613b84afaac0af7b0c8be18'] = 'Le prénom est obligatoire';
$_MODULE['<{upela}greatstore>upela_a03712264a676d05b13bdc4c25a14c78'] = 'Le nom est obligatoire';
$_MODULE['<{upela}greatstore>upela_54f619e52218d05eec0cf5c85a39169d'] = 'Un email valide est obligatoire';
$_MODULE['<{upela}greatstore>upela_eb78cbd108174fe8f540887ac35e3c4a'] = 'Téléphone incorrect';
$_MODULE['<{upela}greatstore>upela_4437c2b638e8f173cf906bef9b74a9f3'] = 'Clé service web obligatoire';
$_MODULE['<{upela}greatstore>upela_8e8ee004d00532ee457a426b1c53fa6a'] = 'Nom de la boutique obligatoire';
$_MODULE['<{upela}greatstore>upela_13766c9eb277bc80426f70e7bbf3ca45'] = 'Adresse incorrecte';
$_MODULE['<{upela}greatstore>upela_5af5d0498a9949a54621f82a51697aa7'] = 'Nom de la ville incorrect';
$_MODULE['<{upela}greatstore>upela_46056563483123f186ffbe0ee10d9b55'] = 'Mot de passe incorrect';
$_MODULE['<{upela}greatstore>upela_518485bfc0a5e9f555cb8eefefe83fb3'] = 'Validation du mot de passe incorrecte';
$_MODULE['<{upela}greatstore>upela_844728e1dc874629bc7450484fa79b73'] = 'Nom de la société obligatoire';
$_MODULE['<{upela}greatstore>upela_2b3f18c584e0b7c6a9ac7865ce481256'] = 'N° d\'immatriculation obligatoire';
$_MODULE['<{upela}greatstore>upela_473c6aa3fd2631329089bb7f8addc77a'] = 'Erreur lors de la création de la boutique !';
$_MODULE['<{upela}greatstore>upela_458444cdb01ae5daaf0c3d215d2755e9'] = 'Boutique créée !';
$_MODULE['<{upela}greatstore>upela_6292b5ddf434cbaf8764c91ad2d2fe5e'] = 'Erreur : cette email est déjà utilisé ! Veuillez vous connecter ou utiliser un autre email.';
$_MODULE['<{upela}greatstore>upela_f35125b5571629016d54fb06f12aa9f8'] = 'Erreur lors de la création de l\'utilisateur !';
$_MODULE['<{upela}greatstore>upela_f1c77197e274b54029f1e42afa1097b1'] = 'Bienvenue dans Upela, votre compte est créé !';
$_MODULE['<{upela}greatstore>upela_9d163d1c0dc9c05338390cd551d97c9f'] = 'Erreur de connexion !';
$_MODULE['<{upela}greatstore>upela_4908d5bab97f9aea640a653bcfa171c0'] = 'Créer votre compte en moins de 2 minutes.';
$_MODULE['<{upela}greatstore>upela_f2fc7490cbdb0db5cd6f1a4e7edb0da1'] = 'Votre compte';
$_MODULE['<{upela}greatstore>upela_463f64a02682b4314b0c840d05d1f03b'] = 'Votre société';
$_MODULE['<{upela}greatstore>upela_b24c699eafef404c29d6e1781f51c333'] = 'Votre boutique';
$_MODULE['<{upela}greatstore>upela_04176f095283bc729f1e3926967e7034'] = 'Prénom';
$_MODULE['<{upela}greatstore>upela_dff4bf10409100d989495c6d5486035e'] = 'Nom';
$_MODULE['<{upela}greatstore>upela_bcc254b55c4a1babdf1dcb82c207506b'] = 'Téléphone';
$_MODULE['<{upela}greatstore>upela_dc647eb65e6711e155375218212b3964'] = 'Mot de passe';
$_MODULE['<{upela}greatstore>upela_4c231e0da3eaaa6a9752174f7f9cfb31'] = 'Confirmation du mot de passe';
$_MODULE['<{upela}greatstore>upela_c281f92b77ba329f692077d23636f5c9'] = 'Nom de la société';
$_MODULE['<{upela}greatstore>upela_a96d242ac549456a6410cabd3fff5568'] = 'Adresse professsionelle';
$_MODULE['<{upela}greatstore>upela_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{upela}greatstore>upela_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{upela}greatstore>upela_956205f0d2c8352f3d92aa3438f1b646'] = 'Adresse 1';
$_MODULE['<{upela}greatstore>upela_2e21e83375deefc4a3620ab667157e27'] = 'Adresse 2';
$_MODULE['<{upela}greatstore>upela_e328c30c941bf1963b2b78e3f87dafe4'] = 'Adresse 3';
$_MODULE['<{upela}greatstore>upela_59716c97497eb9694541f7c3d37b1a4d'] = 'Pays';
$_MODULE['<{upela}greatstore>upela_bc022141f45299c57e9019b0e40bba1e'] = 'Code Postal';
$_MODULE['<{upela}greatstore>upela_57d056ed0984166336b7879c2af3657f'] = 'Ville';
$_MODULE['<{upela}greatstore>upela_4f68183551e5dbd7c341347ffe308682'] = 'SIRET';
$_MODULE['<{upela}greatstore>upela_e46ece012681c62d8a72abe3754a4a9c'] = 'N° d\'immatriculation';
$_MODULE['<{upela}greatstore>upela_e72dca5d5a8a4706a206f3225324bf44'] = 'Nom de la boutique';
$_MODULE['<{upela}greatstore>upela_ad87ea508b9dc8dddc98bf041b907c55'] = 'Clé service web';
$_MODULE['<{upela}greatstore>upela_327f26f3ce177b8b9eb8ba24e0863c3d'] = 'Créer le compte';
$_MODULE['<{upela}greatstore>upela_691d31cb2fcc297fd1edd2ca67c05abe'] = 'Créer votre boutique en moins d\'une minute';
$_MODULE['<{upela}greatstore>upela_3608b457c06bae83423f12015fb50b35'] = 'Créer boutique';
$_MODULE['<{upela}greatstore>upela_c186bf31ae0a0f303a4b12d56d31ae5c'] = 'Erreur lors de la connexion à votre compte !';
$_MODULE['<{upela}greatstore>upela_0baa8ed8b58011b5cfd0be1aa10d10de'] = 'Message :';
$_MODULE['<{upela}greatstore>upela_b352b85b08aa7e587c3802207e169c08'] = 'Connexion réussie !';
$_MODULE['<{upela}greatstore>guest_configure_86b467984feb09b85bad8dd2749e719a'] = 'La solution Upela';
$_MODULE['<{upela}greatstore>guest_configure_7b1329f5ca3d485bba602a815ffd4d6d'] = 'Manuel utilisateur';
$_MODULE['<{upela}greatstore>guest_configure_f61d92234d39bce8a0d5935ffb97ba39'] = 'Paramètres de connexion';
$_MODULE['<{upela}greatstore>guest_configure_bbaff12800505b22a853e8b7f4eb6a22'] = 'Contact';
$_MODULE['<{upela}greatstore>guest_configure_8ae4a996d1c51d4169d7a6bbfa7452da'] = 'Upela';
$_MODULE['<{upela}greatstore>guest_configure_01dadf401b8dbb7c5c2f68e815a696f1'] = 'La meilleure façon d’expédier un colis';
$_MODULE['<{upela}greatstore>guest_configure_e03efdf7ca1ba6fbea27ff481c531c51'] = 'Économisez sur le prix, pas sur le service.';
$_MODULE['<{upela}greatstore>guest_configure_96aafac761c431d59db0070b0bce6cb2'] = 'Vous êtes connecté. Bienvenue';
$_MODULE['<{upela}greatstore>guest_configure_7f15f0868de3f02fa049840f51e51cf7'] = 'Aller sur Upela.com';
$_MODULE['<{upela}greatstore>guest_configure_068a442b24c2bdf3d26228540ff17333'] = 'Vous n\'êtes pas encore connecté !';
$_MODULE['<{upela}greatstore>guest_configure_1a5577f049042062ab324108fdb37248'] = 'Créer un compte PRO';
$_MODULE['<{upela}greatstore>guest_configure_92162c48b0e6494fd9b82c7b620c95d9'] = 'Se connecter à votre compte';
$_MODULE['<{upela}greatstore>guest_configure_e5e41550e3e7a879aadd388f762dcb27'] = 'Avec Upela et Prestashop, bénéficiez de tarifs négociés sur tous vos envois : plis, colis, palettes.';
$_MODULE['<{upela}greatstore>guest_configure_e327966af72586e0f5d9faaae2752b24'] = 'Nos transporteurs';
$_MODULE['<{upela}greatstore>guest_configure_f693d6ed5679693accf2f1b1923e1f50'] = 'La fiabilité des leaders du transport';
$_MODULE['<{upela}greatstore>guest_configure_d81e0d9e12d0cdec13e79f58137d2064'] = 'Des délais de livraison ultra-flexibles en 3h maxi / J+1 / J+2';
$_MODULE['<{upela}greatstore>guest_configure_28e62a28b763a2061d7288aded476e93'] = 'Un service client multi-transporteurs dédié aux Pros';
$_MODULE['<{upela}greatstore>guest_configure_871127850d11a949dddb8aa4b51ba007'] = 'Des connecteurs e-commerce gratuits pour faciliter votre logistique';
$_MODULE['<{upela}greatstore>guest_configure_ea931c7f2face4fa9ac86cdada7e3fbc'] = 'Un service douane intégré';
$_MODULE['<{upela}greatstore>guest_configure_81d44f28016bd86596b9f96c5b9c6329'] = 'Pas de minimum de facturation mensuelle';
$_MODULE['<{upela}greatstore>guest_configure_bbecbe54167646db56a920aad127ac93'] = 'Un gain de temps : en quelques clics, expédiez';
$_MODULE['<{upela}greatstore>guest_configure_7659e6df9e2964fffb243a646add33aa'] = 'en 3 clics';
$_MODULE['<{upela}greatstore>guest_configure_7eece51cf3938103677db7a5051ef8f5'] = 'Comparer';
$_MODULE['<{upela}greatstore>guest_configure_0387832795db3eb2e05f4365fba5ddac'] = 'Expédier';
$_MODULE['<{upela}greatstore>guest_configure_92ea731d3af6677905303c88689f5d55'] = 'Suivre';
$_MODULE['<{upela}greatstore>guest_configure_096d76c17abb603d3c1035903a520541'] = 'en vidéo';
$_MODULE['<{upela}greatstore>guest_configure_a49c2a92c8d82a06076e523bd2379e53'] = 'Nos connecteurs e-commerce';
$_MODULE['<{upela}greatstore>guest_configure_a663f9fa94fe55d4be1f0b3c986a0557'] = 'Gagnez en productivité et compétitivité';
$_MODULE['<{upela}greatstore>guest_configure_1ac9892f2ddc43ee1a46037c40b7a9b1'] = 'Plug and Play';
$_MODULE['<{upela}greatstore>guest_configure_dd17e10e985c48c612eb6c43e81dc1d8'] = 'Limiter les erreurs de saisies';
$_MODULE['<{upela}greatstore>guest_configure_44e988e9b892c52de058a71b1f0e532c'] = 'Mise à jour automatique des tracking';
$_MODULE['<{upela}greatstore>guest_configure_175bada5126d0fa77bb14ae96ed60b9e'] = 'Logistique intégrée';
$_MODULE['<{upela}greatstore>guest_configure_59a129db0008167cd8e94f64bb003f14'] = 'Commandes centralisées';
$_MODULE['<{upela}greatstore>guest_configure_9da91391e29c9bb911af1a7d55d3b7c6'] = 'Gestion multi-entrepôts';
$_MODULE['<{upela}greatstore>guest_configure_33354d97cd5d54f99a4e04d2f5f65d64'] = 'Chat en ligne';
$_MODULE['<{upela}greatstore>guest_configure_bcc254b55c4a1babdf1dcb82c207506b'] = 'Téléphone';
$_MODULE['<{upela}greatstore>guest_configure_54f664c70c22054ea0d8d26fc3997ce7'] = 'Online';
$_MODULE['<{upela}greatstore>guest_configure_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'Mail';
$_MODULE['<{upela}greatstore>guest_configure_6f363e03af11acd967bf6e4af5ff3682'] = 'Le support Upela';
$_MODULE['<{upela}greatstore>guest_configure_c4468175a4523261f9ca70342949f7c0'] = 'Un service client multi-transporteurs';
$_MODULE['<{upela}greatstore>guest_configure_5be3e2464e080f4dd29d99576982dcb1'] = 'Réponse le jour même';
$_MODULE['<{upela}greatstore>guest_configure_1768a553a1d04528b7bd81c93380458a'] = 'Conseillers disponibles en continu';
$_MODULE['<{upela}greatstore>guest_configure_6ebb5c36c5c76298e07a467428d39640'] = 'Suivi des envois et gestion centralisée du SAV';
$_MODULE['<{upela}greatstore>guest_configure_a62c455049d335ccbe02904e139f8142'] = 'Étudier vos cas particuliers';
$_MODULE['<{upela}greatstore>guest_configure_529cbba3326b4decc6965f09927716ea'] = 'Vous conseiller dans les démarches';
$_MODULE['<{upela}greatstore>guest_configure_385d5b0eea7e199471284b2cc50adf25'] = 'Découvrir nos outils et connaître nos services';
$_MODULE['<{upela}greatstore>guest_configure_2e5f2bf721b8e39821e4e3511a099d4d'] = 'Comment paramétrer votre compte ?';
$_MODULE['<{upela}greatstore>guest_configure_489af77c71f11672d22403a8df7edfdc'] = 'Dans votre boutique Prestashop';
$_MODULE['<{upela}greatstore>guest_configure_a6976ff369c161d362757f5577d317e2'] = '1) Activez le webservice :';
$_MODULE['<{upela}greatstore>guest_configure_0efa47691b7b9a7a9383a87a49859d12'] = 'Menu “Paramètres avancés” puis “Service Web”';
$_MODULE['<{upela}greatstore>guest_configure_4cf2b96aeb12fe8b0597286152ee71fa'] = 'Veuillez “Activer le service web” et “Activer le mode CGI de PHP”.';
$_MODULE['<{upela}greatstore>guest_configure_c314e6847a63754023c454637afe98c4'] = 'Ajoutez un nouveau service web.';
$_MODULE['<{upela}greatstore>guest_configure_794b84b2d0da43d2e4b96ff0ba447fd5'] = '2) Créez le nouveau webservice :';
$_MODULE['<{upela}greatstore>guest_configure_faefbac12c6261a8bdf6f47fcc41aa41'] = 'Générez une nouvelle clé API.';
$_MODULE['<{upela}greatstore>guest_configure_c0491fe694590e592b891462fa800a2b'] = 'Contrôlez que l’Etat du service est actif : “Oui”';
$_MODULE['<{upela}greatstore>guest_configure_3badd0ba5f628178f313bf8a281cfec8'] = 'Attribuer les permissions GET, POST et PUT pour permettre l’utilisation du webservice';
$_MODULE['<{upela}greatstore>guest_configure_61f866bc212c59fad0149fe9322cf963'] = '3) Créez ou connectez vous à votre compte Upela';
$_MODULE['<{upela}greatstore>guest_configure_bbf61c69b6c01420384aa399bc580636'] = '4) Ouvrez Upela directement depuis votre boutique';
$_MODULE['<{upela}greatstore>guest_configure_619d465b0263eb0770d6cec2ae04e751'] = 'Dans votre espace client Upela, menu \"Mes boutiques\"';
$_MODULE['<{upela}greatstore>guest_configure_29ed387615b0bdc819d9436fc8ca0456'] = '1) Vous récupérez automatiquement vos commandes de vos boutiques avec leur statut';
$_MODULE['<{upela}greatstore>guest_configure_cae8d14edd025e72c59dbab6f378c95a'] = 'Vous';
$_MODULE['<{upela}greatstore>guest_configure_15117b282328146ac6afebaa8acd80e7'] = 'générez';
$_MODULE['<{upela}greatstore>guest_configure_ca09d2b937f626a8f3ee264e353de88b'] = 'les bons de transport et';
$_MODULE['<{upela}greatstore>guest_configure_2a3f1166b041928019e4e8718d628665'] = 'expédiez';
$_MODULE['<{upela}greatstore>guest_configure_44ce4ad6a7ec015a8c20a3944395a2ab'] = 'vos colis.';
$_MODULE['<{upela}greatstore>guest_configure_072a6694c67b8b414027fa310704cd2a'] = 'Notre connecteur e-commerce met automatiquement à jour vos boutiques respectives avec le nouveau statut (envoyé) ainsi que le numéro de suivi du bon de transport.';
$_MODULE['<{upela}greatstore>guest_configure_09b2e97efdcbaf471863c5e732976049'] = '2) Expédiez votre commande';
$_MODULE['<{upela}greatstore>guest_configure_ce5c27b07861f7d5ef2178cc3b27a307'] = 'Les adresses d’origine et de destination sont automatiquement pré-remplies.';
$_MODULE['<{upela}greatstore>guest_configure_caac49de128198ec7ea40d7900b90fe4'] = 'Complétez les informations de l’envoi, y compris la description des colis (nombre, poids unitaires, dimensions).';
$_MODULE['<{upela}greatstore>guest_configure_4d8c42a88672feb405e4c9245ded5544'] = 'Cliquez sur « Comparez les offres » pour finaliser votre expédition';
$_MODULE['<{upela}greatstore>guest_configure_edfee72250a49d45993a8289e477a792'] = 'Paramètres de connexion à votre compte Upela';
$_MODULE['<{upela}greatstore>guest_configure_91e7ca86887a45c091cdfbfb7533a816'] = 'Votre compte est activé.';
$_MODULE['<{upela}greatstore>guest_configure_dc647eb65e6711e155375218212b3964'] = 'Mot de passe';
$_MODULE['<{upela}greatstore>guest_configure_42ae25231906c83927831e0ef7c317ac'] = 'Déconnexion';
$_MODULE['<{upela}greatstore>guest_configure_c65d14354cf8abb5e63737c482f6ccb3'] = 'Se connecter à votre compte';
$_MODULE['<{upela}greatstore>guest_configure_d8fb0c80157321ba76f697307c265820'] = 'Informations';
$_MODULE['<{upela}greatstore>guest_configure_2d2dc81e0ca0a8075cda93cf43ceb033'] = 'Vous avez actuellement';
$_MODULE['<{upela}greatstore>guest_configure_5b6178bcdc0dad676beb679f7bec9fe5'] = 'boutique(s)';
$_MODULE['<{upela}greatstore>guest_configure_431d7c05b4d48b7792c605bcdaea084c'] = 'boutique(s) Prestashop';
$_MODULE['<{upela}greatstore>guest_configure_eb88865bd8fda86aa24db5a587ba0f8d'] = 'Créer une boutique';
$_MODULE['<{upela}greatstore>guest_configure_f27c441dd0de52b23d626919b2b4d20b'] = 'Service client Upela';
$_MODULE['<{upela}greatstore>guest_configure_5a048fa20a2cb03df7c562a8e3f8e7fe'] = 'Contacter le support Upela';
$_MODULE['<{upela}greatstore>guest_configure_db5eb84117d06047c97c9a0191b5fffe'] = 'Support';
$_MODULE['<{upela}greatstore>guest_configure_acd92b76f3fe32da6c7e2d66a63b7817'] = 'MPG UPELA';
$_MODULE['<{upela}greatstore>guest_configure_7ffdefe7c99a57d2675a7b5a19ec01ca'] = '17BIS RUE LA BOETIE';
$_MODULE['<{upela}greatstore>guest_configure_3dce0219bc199a019a8ff0441394261b'] = '75008 PARIS - FRANCE';
$_MODULE['<{upela}greatstore>guest_configure_63884adfc7756525cfa832993ca0e908'] = 'RCS Paris 750 389 769';
$_MODULE['<{upela}greatstore>guest_configure_cb3e8cf309842fa006ce03ebab14f004'] = 'VAT n° FR12750389769';
$_MODULE['<{upela}greatstore>expedition15_b5e06045b80780320067548add81746d'] = 'Expédier avec Upela';
$_MODULE['<{upela}greatstore>expedition_b5e06045b80780320067548add81746d'] = 'Expédiez avec Upela';
