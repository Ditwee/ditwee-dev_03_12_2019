<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered_3601146c4e948c32b6424d2c0a7f0118'] = 'Prix';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered_8c489d0946f66d17d73f26366a4bf620'] = 'Poids';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Marque';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered_af1b98adf7f686b84cd0b443e022b7a0'] = 'Catégories';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_f3f43e30c8c7d78c6ac0173515e57a00'] = 'Filtres';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_1262d1b9fbffb3a8e85ac9e4b449e989'] = 'Filtres actifs:';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_ea4788705e6873b424c65e91c2846b19'] = 'Annuler';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_b47b72ddf8a3fa1949a7fb6bb5dbc60c'] = 'Pas de filtres';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_75954a3c6f2ea54cb9dff249b6b5e8e6'] = 'Plage:';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_5da618e8e4b89c66fe86e32cdafde142'] = 'De';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_01b6e20344b68835c5ed1ddedf20d531'] = 'à';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_146ffe2fd9fa5bec3b63b52543793ec7'] = 'Voir plus';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_c74ea6dbff701bfa23819583c52ebd97'] = 'Voir moins';
$_MODULE['<{xprtblocklayered}greatstore>xprtblocklayered-top_8524de963f07201e5c086830d370797f'] = 'Chargement';
