<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{xprtblockpopup}greatstore>xprtblockpopup_e267e2be02cf3e29f4ba53b5d97cf78a'] = 'Adresse email invalide';
$_MODULE['<{xprtblockpopup}greatstore>xprtblockpopup_1c623b291d95f4f1b1d0c03d0dc0ffa1'] = 'Cet email n\'est pas enregistré';
$_MODULE['<{xprtblockpopup}greatstore>xprtblockpopup_3b1f17a6fd92e35bc744e986b8e7a61c'] = 'Une erreur est apparue lors de la tentative de désincription';
$_MODULE['<{xprtblockpopup}greatstore>xprtblockpopup_d4197f3e8e8b4b5d06b599bc45d683bb'] = 'Désinscription prise en compte';
$_MODULE['<{xprtblockpopup}greatstore>xprtblockpopup_f6618fce0acbfca15e1f2b0991ddbcd0'] = 'Cet email est déjà inscrit chez nous';
$_MODULE['<{xprtblockpopup}greatstore>xprtblockpopup_e172cb581e84f43a3bd8ee4e3b512197'] = 'Une erreur est apparue lors de la tentative d\'inscription';
$_MODULE['<{xprtblockpopup}greatstore>xprtblockpopup_ebc069b1b9a2c48edfa39e344103be1e'] = 'Un email de vérification vient d\'être envoyé.';
$_MODULE['<{xprtblockpopup}greatstore>xprtblockpopup_77c576a354d5d6f5e2c1ba50167addf8'] = 'Vous êtes désormais inscrit à la newsletter';
$_MODULE['<{xprtblockpopup}greatstore>blockpopup_custom_064e7b426b0e88f3056607e3645521d2'] = 'Ne plus afficher se message';
$_MODULE['<{xprtblockpopup}greatstore>newsletter_popup_01557660faa28f8ec65992d1ddbb7b79'] = 'Email';
$_MODULE['<{xprtblockpopup}greatstore>newsletter_popup_b26917587d98330d93f87808fc9d7267'] = 'S\'inscrire';
$_MODULE['<{xprtblockpopup}greatstore>newsletter_popup_064e7b426b0e88f3056607e3645521d2'] = 'Ne plus afficher se message';
