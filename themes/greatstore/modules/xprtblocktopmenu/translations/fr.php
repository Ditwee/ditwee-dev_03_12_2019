<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_cfe6e34a4c7f24aad32aa4299562f5b1'] = 'Accueil';
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_e884e228d6d8dc8e0781cdcfb0a62ff9'] = 'Boutiques & Créateurs';
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_84be54bb4bd1b755a27d86388700d097'] = 'Marques';
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_8cf04a9734132302f96da8e113e80ce5'] = 'Accueil';
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_eacd852cc1f621763dccbda3f3c15081'] = 'Barre de recherche';
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activer';
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactiver';
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_ec211f7c20af43e742bf2570c3cb84f9'] = 'Ajouter';
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_af1b98adf7f686b84cd0b443e022b7a0'] = 'Catégories';
$_MODULE['<{xprtblocktopmenu}greatstore>xprtblocktopmenu_068f80c7519d0528fb08e82137a72131'] = 'Produits';
