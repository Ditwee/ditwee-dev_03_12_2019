<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_f38f5974cdc23279ffe6d203641a8bdf'] = 'Paramètres mise a jour';
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_26986c3388870d4148b1b5375368a83d'] = 'Produits à afficher';
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_d36bbb6066e3744039d38e580f17a2cc'] = 'Définissez le nombre de produits affichés dans ce bloc.';
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_b78a3223503896721cca1303f776159b'] = 'Titre';
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_97e1beced18eff7df7dfeb8eed99c85c'] = 'Entrez le titre du produit consulté';
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_ae1ca42050a83e3f088db858148cba66'] = 'Sous-titre';
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_a5f1cc42e025772725589a1e010a722e'] = 'Entrer le sous-titre pour le produit consulté';
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_37a79b24cf976bb2a92c3c84029699cc'] = 'Nombre de produit affiché sur le slider';
$_MODULE['<{xprtblockviewed}greatstore>xprtblockviewed_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
