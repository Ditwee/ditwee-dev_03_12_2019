<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_2107f6398c37b4b9ee1e1b5afb5d3b2a'] = 'Envoyer à un(e) ami(e)';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_5d6103b662f41b07e10687f03aca8fdc'] = 'Destinataire';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_bb6aa0be8236a10e6d3b315ebd5f2547'] = 'Nom de votre ami(e)';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_099bc8914b5be9e522a29e48cb3c01c4'] = 'Email de votre ami(e)';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_70397c4b252a5168c5ec003931cea215'] = 'Champ requis';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_94966d90747b97d1f0f206c98a8b1ac3'] = 'Envoyer';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_e81c4e4f2b7b93b481e13a8553c2ae1b'] = 'ou';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_ea4788705e6873b424c65e91c2846b19'] = 'Annuler';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_11cbd9ec2e4b628c371094b6361b9c96'] = 'Votre email a été envoyé avec succès';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_36fb3f59b4a75949a0db90e7011b21f2'] = 'Votre email ne peut pas être envoyé. Merci de vérifier l\'adresse email.';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend-extra_d1f092e79827eaffce4a33fa011fde24'] = 'Vous n\'avez pas remplis tout les champs';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend_2107f6398c37b4b9ee1e1b5afb5d3b2a'] = 'Envoyer à un(e) ami(e)';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend_20589174124c25654cac3736e737d2a3'] = 'Envoyez cette page à un(e) ami(e) qui pourrait être intéressé par ce produit ';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend_b31afdfa73c89b567778f15180c2dd6c'] = 'Votre email a été envoyé avec succès';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend_e55de03786f359e2b133f2a74612eba6'] = 'Nom de votre ami(e)';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend_19f41c3d6db934fb2db1840ddefd2c51'] = 'Email de votre ami(e)';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend_2541d938b0a58946090d7abdde0d3890'] = 'Envoyer';
$_MODULE['<{xprtsendtoafriend}greatstore>xprtsendtoafriend_68728c1897e5936032fe21ffb6b10c2e'] = 'Retour';
