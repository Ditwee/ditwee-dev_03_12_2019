{if $page_name == 'product'}
	{assign var='bread_height' value=220}
{else}
	{assign var='bread_height' value={Configuration::get('bread_height')|replace:'px':''}}
	

{/if}

<div class="page_heading_area xprt_parallax_section1" style="min-height: {$bread_height}px !important;

	{if $page_name == 'category'}
		{if $category->id_image}
		background-image:url({$link->getCatImageLink($category->link_rewrite, $category->id_image)|escape:'html':'UTF-8'})
		{else}
			{$imgbread = Configuration::get('image_category')}
			{if (isset($imgbread) && !empty($imgbread))}
				background-image:url({$modules_dir}xprtbreadcrumbmod/img/{$imgbread})
			{else}
				background-image:url({$img_dir}bg/Blog_grid.jpg)
			{/if}
		{/if}
	{elseif $page_name == 'product'}
		{$imgbread = Configuration::get('image_product')}
		{*
		{if (isset($imgbread) && !empty($imgbread))}
			background-image:url({$modules_dir}xprtbreadcrumbmod/img/{$imgbread})
		{else}
			background-image:url({$img_dir}bg/Blog_grid.jpg)
		{/if}
		*}
	{elseif $page_name == 'cms'}
		{$imgbread = Configuration::get('image_cms')}
		{if (isset($imgbread) && !empty($imgbread))}
			background-image:url({$modules_dir}xprtbreadcrumbmod/img/{$imgbread})
		{else}
			background-image:url({$img_dir}bg/Blog_grid.jpg)
		{/if}
	{elseif $page_name == 'pagenotfound'}
		{$imgbread = Configuration::get('image_404')}
		{if (isset($imgbread) && !empty($imgbread))}
			background-image:url({$modules_dir}xprtbreadcrumbmod/img/{$imgbread})
		{else}
			background-image:url({$img_dir}bg/Blog_grid.jpg)
		{/if}
	{elseif ($page_name == 'module-xipblog-archive'  || $page_name == 'module-xipblog-single' || $page_name == 'module-xipblog-search' || $page_name == 'module-xipblog-tagpost')}
		{$imgbread = Configuration::get('image_blog')}
		{if (isset($imgbread) && !empty($imgbread))}
			background-image:url({$modules_dir}xprtbreadcrumbmod/img/{$imgbread})
		{else}
			background-image:url({$img_dir}bg/Blog_grid.jpg)
		{/if}
		
	{elseif ($page_name == 'module-jmarketplace-sellerprofile')}
	
		{if (isset($photo2) && !empty($photo2))}
			background-image:url({$photo2})
		{else}
			background-image:url({$img_dir}bg/Blog_grid.jpg)
		{/if}
		
	{elseif ($page_name == 'module-jmarketplace-sellers'  || $page_name == 'module-jmarketplace-sellers' ||  strpos($page_name, 'jmarketplace') != false)}
	
		{$imgbread = Configuration::get('image_sellers')}
		{if (isset($imgbread) && !empty($imgbread))}
			background-image:url({$modules_dir}xprtbreadcrumbmod/img/{$imgbread})
		{else}
			background-image:url({$img_dir}bg/Blog_grid.jpg)
		{/if}
	{else}
		{$imgbread = Configuration::get('image_others')}
		{if (isset($imgbread) && !empty($imgbread))}
			background-image:url({$modules_dir}xprtbreadcrumbmod/img/{$imgbread})
		{else}
			background-image:url({$img_dir}bg/Blog_grid.jpg)
		{/if}
	{/if}
">
	<div class="container" style="min-height: {$bread_height}px;">
		<div class="page_heading">
			<!-- page heading -->
			{if isset($smarty.capture.path)}{assign var='path' value=$smarty.capture.path}{/if}
			{if isset($path) AND $path}
				<h2>
					<span itemscope itemtype="http://schema.org/BreadcrumbList">

						{* {if $path|strpos:'span' !== false} *}
							{$path|@replace:'<a ': '<a class="unvisible" '|@replace:'<span class="navigation-pipe">': '<span class="unvisible">'}
						{* {/if} *}
					</span>
				</h2>
			{/if}
			{include file="$tpl_dir./breadcrumb.tpl"}
		</div>
	</div>
</div> <!-- page_heading_area -->