<div class="page_heading_simple_area">
	<div class="container">
		<div class="page_heading">
			<!-- page heading -->
			{if isset($smarty.capture.path)}{assign var='path' value=$smarty.capture.path}{/if}
			{if isset($path) AND $path}
				<h2>
					<span>
						{* {if $path|strpos:'span' !== false} *}
							{$path|@replace:'<a ': '<a class="unvisible" '|@replace:'<span class="navigation-pipe">': '<span class="unvisible">'}
						{* {/if} *}
					</span>
				</h2>
			{/if}
			{include file="$tpl_dir./breadcrumb.tpl"}
		</div>
	</div>

</div> <!-- page_heading_simple_area -->