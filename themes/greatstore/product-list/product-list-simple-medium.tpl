<ul{if isset($id) && $id} id="{$id}"{/if} class="{if isset($class) && $class} {$class}{/if}">
	{foreach from=$products item=product name=myLoop}
		<li class="clearfix m_bottom_30" itemscope itemtype="https://schema.org/Product">
			<div class="products-block-image">
				<a href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}" class="content_img clearfix">
					<img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'simple_medium')|escape:'html'}" alt="{$product.legend|escape:'html':'UTF-8'}" />
				</a>

				<div class="functional-buttons clearfix">
					{if isset($quick_view) && $quick_view}
						{* {if $xprt.prd_list_quickview == 1} *}
							<div class="quick-view-wrapper">
								<a class="quick-view" title="{l s='Quick view'}" href="{$product.link|escape:'html':'UTF-8'}" data-rel="{$product.link|escape:'html':'UTF-8'}" >
									<span>{l s='Quick view'}</span>
								</a>
							</div>
						{* {/if} *}
					{/if}

				</div> <!-- functional-buttons -->

			</div> <!-- product-image-container -->
			
			
			<div class="product-content">
            	<h5>
                	<a class="product-name" href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}">
                        {$product.name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}
                    </a>
                </h5>
                
                {* <p class="product_category">{$product.category|strip_tags:'UTF-8'|truncate:50:'...'}</p> *}
                
                {* <p class="product-description">{$product.description_short|strip_tags:'UTF-8'|truncate:75:'...'}</p> *}

                {capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
                {if $smarty.capture.displayProductListReviews}
                	<div class="hook-reviews">
                	{hook h='displayProductListReviews' product=$product}
                	</div>
                {/if}

                <div class="price-box">
                	{if !$PS_CATALOG_MODE}
                    	<span class="price special-price">
                            {if !$priceDisplay}
                                {displayWtPrice p=$product.price}{else}{displayWtPrice p=$product.price_tax_exc}
                            {/if}
                        </span>
                        {if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
	                         <span class="old-price">
	                            {if !$priceDisplay}
	                                {displayWtPrice p=$product.price_tax_exc}{else}{displayWtPrice p=$product.price_without_reduction}
	                            {/if}
	                        </span>
	                         {if $product.specific_prices}
	                            {assign var='specific_prices' value=$product.specific_prices}
	                            {if $specific_prices.reduction_type == 'percentage' && ($specific_prices.from == $specific_prices.to OR ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $specific_prices.to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $specific_prices.from))}
	                                <span class="price-percent-reduction">-{$specific_prices.reduction*100|floatval}%</span>
	                            {/if}
	                        {/if}
                         {/if}
                    {/if}
                </div>
				
				<!-- button-container -->
                <div class="button-container">
                	{if $xprt.prd_list_addtocart == 1}
                		{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}
                			{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}
                				{capture}add=1&amp;id_product={$product.id_product|intval}{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
                				<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
                					<span>{l s='Add to cart'}</span>
                				</a>
                			{else}
                				<span class="button ajax_add_to_cart_button btn btn-default disabled">
                					<span>{l s='Add to cart'}</span>
                				</span>
                			{/if}
                		{/if}
                		<a class="button lnk_view btn btn-default d_none" href="{$product.link|escape:'html':'UTF-8'}" title="{l s='View'}">
                			<span>{if (isset($product.customization_required) && $product.customization_required)}{l s='Customize'}{else}{l s='More'}{/if}</span>
                		</a>
                	{/if}
                </div>

            </div>
		</li>
	{/foreach}
</ul>
