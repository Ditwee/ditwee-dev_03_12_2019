<div class="home_parallax_product_area home_simple_prod_paralax xprt_parallax_section" style="background: url({$parallaxbg}) no-repeat fixed center center/ cover;min-height:410px; {if isset($margin)}margin:{$margin};{/if}">
	<div class="container home_parallax_product">
		<div class="row home_parallax_product_content carousel">

			{foreach from=$products item=product name=myLoop}
				{* {if $product@iteration > 1}{break}{/if}  *}
				<div class="home_parallax_product_content_inner clearfix" itemscope itemtype="https://schema.org/Product">
					<div class="home_parallax_product_content_left col-xs-6 col-sm-7">
						<div class="home_parallax_product_content_left_inner">
							<h5>
						    	<a class="product-name" href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}">
						            {$product.name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}
						        </a>
						    </h5>
							{capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
							{if $smarty.capture.displayProductListReviews}
								<div class="hook-reviews">
								{hook h='displayProductListReviews' product=$product}
								</div>
							{/if}

							{if (!$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
						    	{if isset($product.available_for_order) && $product.available_for_order && !isset($restricted_country_mode)}
						    		<span class="availability">
						    			{* <label>{l s='Available: '}</label> *}
						    			{if ($product.allow_oosp || $product.quantity > 0)}
						    				<span class="{if $product.quantity <= 0 && isset($product.allow_oosp) && !$product.allow_oosp} text-danger{elseif $product.quantity <= 0} text-warning{else} text-success{/if}">
						    					{if $product.quantity <= 0}{if $product.allow_oosp}{if isset($product.available_later) && $product.available_later}{$product.available_later}{else}{l s='In Stock'}{/if}{else}{l s='Out of stock'}{/if}{else}{if isset($product.available_now) && $product.available_now}{$product.available_now}{else}{l s='In Stock'}{/if}{/if}
						    				</span>
						    			{elseif (isset($product.quantity_all_versions) && $product.quantity_all_versions > 0)}
						    				<span class="text-warning">
						    					{l s='Product available with different options'}
						    				</span>
						    			{else}
						    				<span class="text-danger">
						    					{l s='Out of stock'}
						    				</span>
						    			{/if}
						    		</span>
						    	{/if}
						    {/if}

						    <div class="product-flags">
						    	{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
						    		{if isset($product.online_only) && $product.online_only}
						    			<span class="online_only">{l s='Online only'}</span>
						    		{/if}
						    	{/if}
						    	{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
						    		{elseif isset($product.reduction) && $product.reduction && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
						    			<span class="discount">{l s='Reduced price!'}</span>
						    		{/if}
						    </div>

						    

						     {* <p class="product_category">{$product.category|strip_tags:'UTF-8'|truncate:50:'...'}</p> *}
							
							

			                <div class="price-box">
			                	{if !$PS_CATALOG_MODE}
			                    	<span class="price special-price">
			                            {if !$priceDisplay}
			                                {displayWtPrice p=$product.price}{else}{displayWtPrice p=$product.price_tax_exc}
			                            {/if}
			                        </span>
			                        {if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
				                         <span class="old-price">
				                            {if !$priceDisplay}
				                                {displayWtPrice p=$product.price_tax_exc}{else}{displayWtPrice p=$product.price_without_reduction}
				                            {/if}
				                        </span>
				                         {if $product.specific_prices}
				                            {assign var='specific_prices' value=$product.specific_prices}
				                            {if $specific_prices.reduction_type == 'percentage' && ($specific_prices.from == $specific_prices.to OR ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $specific_prices.to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $specific_prices.from))}
				                                <span class="price-percent-reduction">-{$specific_prices.reduction*100|floatval}%</span>
				                            {/if}
				                        {/if}
			                         {/if}
			                    {/if}
			                </div>

	            			<!-- product countdown -->
	            			{if !$PS_CATALOG_MODE}
	                        {if isset($product.specific_prices)}
	                        	{if ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $product.specific_prices.to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $product.specific_prices.from)}
	            					<div class="prod_countdown styled" data-date="{$product.specific_prices.to|date_format} {$product.specific_prices.to|date_format:'%H:%M:%S'}"></div>
	            					
	            				{/if}
	            			{/if}
	            			{/if}
	            			<!-- end product countdown -->
	            			<div class="button-container">
	            				{if $xprt.prd_list_addtocart == 1}
	            					{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}
	            						{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}
	            							{capture}add=1&amp;id_product={$product.id_product|intval}{if isset($product.id_product_attribute) && $product.id_product_attribute}&amp;ipa={$product.id_product_attribute|intval}{/if}{if isset($static_token)}&amp;token={$static_token}{/if}{/capture}
	            							<a class="button ajax_add_to_cart_button btn btn-default" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart'}" data-id-product-attribute="{$product.id_product_attribute|intval}" data-id-product="{$product.id_product|intval}" data-minimal_quantity="{if isset($product.product_attribute_minimal_quantity) && $product.product_attribute_minimal_quantity >= 1}{$product.product_attribute_minimal_quantity|intval}{else}{$product.minimal_quantity|intval}{/if}">
	            								<span>{l s='Add to cart'}</span>
	            							</a>
	            						{else}
	            							<span class="button ajax_add_to_cart_button btn btn-default disabled">
	            								<span>{l s='Add to cart'}</span>
	            							</span>
	            						{/if}
	            					{/if}
	            					<a class="button lnk_view btn btn-default d_none" href="{$product.link|escape:'html':'UTF-8'}" title="{l s='View'}">
	            						<span>{if (isset($product.customization_required) && $product.customization_required)}{l s='Customize'}{else}{l s='More'}{/if}</span>
	            					</a>
	            				{/if}
	            			</div>
						</div>
					</div> <!-- home_full_cool_paralax_content_left -->
					<div class="home_parallax_product_content_right col-xs-6 col-sm-5">
						<div class="product-image-container">
							<div class="product_img_link">
								<a href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" itemprop="url">
									<img class="img-responsive" data-imgs-attr="{hook h='productgridmultiimgs' product=$product count="false" img_type='home_default'}" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" itemprop="image" />
							        {* <img class="{if $xprt.prod_grid_img_style == 'prod_grid_img_style_multi'}multi_img{elseif $xprt.prod_grid_img_style == 'prod_grid_img_style_hover'}hover_img{/if} replace-2x img-responsive {if isset($xprt.lazy_load) && $xprt.lazy_load == 1}lazy{/if}" data-imgs-attr="{hook h='productgridmultiimgs' product=$product count="{if $xprt.prod_grid_img_style == 'prod_grid_img_style_multi'}false{else}true{/if}" img_type='home_default'}" {if isset($xprt.lazy_load) && $xprt.lazy_load == 1} data-src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}"{/if} {if isset($xprt.lazy_load) && $xprt.lazy_load == 1} src="{$img_dir}kr_loader.gif" {else} src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" {/if} alt="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" title="{if !empty($product.legend)}{$product.legend|escape:'html':'UTF-8'}{else}{$product.name|escape:'html':'UTF-8'}{/if}" itemprop="image" /> *}
							    </a>
							</div>

							{* {if isset($product.new) && $product.new == 1}
								<a class="new-box" href="{$product.link|escape:'html':'UTF-8'}">
									<span class="new-label">{l s='New'}</span>
								</a>
							{/if}
							{if isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
								<a class="sale-box" href="{$product.link|escape:'html':'UTF-8'}">
									<span class="sale-label">{l s='Sale!'}</span>
								</a>
							{/if} *}

						</div>
					</div><!-- home_parallax_product_content_right -->

				</div> <!-- home_parallax_product_content_inner -->

			{/foreach}
		</div>
	</div>
</div>











{* 
<ul{if isset($id) && $id} id="{$id}"{/if} class="products-block {if isset($class) && $class} {$class}{/if}">
	{foreach from=$products item=product name=myLoop}
		<li class="clearfix">
			<a href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}" class="products-block-image content_img clearfix">
				<img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html'}" alt="{$product.legend|escape:'html':'UTF-8'}" />
			</a>
			<div class="product-content">
            	<h5>
                	<a class="product-name" href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}">
                        {$product.name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}
                    </a>
                </h5>
                
                <p class="product_category">{$product.category|strip_tags:'UTF-8'|truncate:50:'...'}</p>
                
                <p class="product-description">{$product.description_short|strip_tags:'UTF-8'|truncate:75:'...'}</p>

                {capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
                {if $smarty.capture.displayProductListReviews}
                	<div class="hook-reviews">
                	{hook h='displayProductListReviews' product=$product}
                	</div>
                {/if}

                <div class="price-box">
                	{if !$PS_CATALOG_MODE}
                    	<span class="price special-price">
                            {if !$priceDisplay}
                                {displayWtPrice p=$product.price}{else}{displayWtPrice p=$product.price_tax_exc}
                            {/if}
                        </span>
                        {if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
	                         <span class="old-price">
	                            {if !$priceDisplay}
	                                {displayWtPrice p=$product.price_tax_exc}{else}{displayWtPrice p=$product.price_without_reduction}
	                            {/if}
	                        </span>
	                         {if $product.specific_prices}
	                            {assign var='specific_prices' value=$product.specific_prices}
	                            {if $specific_prices.reduction_type == 'percentage' && ($specific_prices.from == $specific_prices.to OR ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $specific_prices.to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $specific_prices.from))}
	                                <span class="price-percent-reduction">-{$specific_prices.reduction*100|floatval}%</span>
	                            {/if}
	                        {/if}
                         {/if}
                    {/if}
                </div>

            </div>
		</li>
	{/foreach}
</ul> *}
