<ul{if isset($id) && $id} id="{$id}"{/if} class="products-block {if isset($class) && $class} {$class}{/if}">
	{foreach from=$products item=product name=myLoop}
		<li class="clearfix" itemscope itemtype="https://schema.org/Product">
			<a href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}" class="products-block-image content_img clearfix">
				<img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html'}" alt="{$product.legend|escape:'html':'UTF-8'}" />
			</a>
			<div class="product-content">
            	<h5>
                	<a class="product-name" href="{$product.link|escape:'html'}" title="{$product.legend|escape:'html':'UTF-8'}">
                        {$product.name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}
                    </a>
                </h5>
                
                {* <p class="product_category">{$product.category|strip_tags:'UTF-8'|truncate:50:'...'}</p> *}
                
                {* <p class="product-description">{$product.description_short|strip_tags:'UTF-8'|truncate:75:'...'}</p> *}

                {capture name='displayProductListReviews'}{hook h='displayProductListReviews' product=$product}{/capture}
                {if $smarty.capture.displayProductListReviews}
                	<div class="hook-reviews">
                	{hook h='displayProductListReviews' product=$product}
                	</div>
                {/if}

                <div class="price-box">
                	{if !$PS_CATALOG_MODE}
                    	<span class="price special-price">
                            {if !$priceDisplay}
                                {displayWtPrice p=$product.price}{else}{displayWtPrice p=$product.price_tax_exc}
                            {/if}
                        </span>
                        {if $product.price_without_reduction > 0 && isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
	                         <span class="old-price">
	                            {if !$priceDisplay}
	                                {displayWtPrice p=$product.price_tax_exc}{else}{displayWtPrice p=$product.price_without_reduction}
	                            {/if}
	                        </span>
	                         {if $product.specific_prices}
	                            {assign var='specific_prices' value=$product.specific_prices}
	                            {if $specific_prices.reduction_type == 'percentage' && ($specific_prices.from == $specific_prices.to OR ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $specific_prices.to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $specific_prices.from))}
	                                <span class="price-percent-reduction">-{$specific_prices.reduction*100|floatval}%</span>
	                            {/if}
	                        {/if}
                         {/if}
                    {/if}
                </div>

            </div>
		</li>
	{/foreach}
</ul>
