{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}



{if isset($orderby) AND isset($orderway)}
<ul class="display hidden-xs sortPagiBar_item www">
	<li id="display-title" class="display_title">{l s='View as:'}</li>
    {* <li id="grid"><a rel="nofollow" href="#" title="{l s='Grid'}"><i class="icon_grid-2x2"></i></a></li> *}
	{if $HOOK_LEFT_COLUMN !='' || $HOOK_RIGHT_COLUMN !=''}
	    <li id="displayview2"><a rel="nofollow" href="#" title="{l s='2 per row'}"><i class="icon-th-large"></i></a></li>
	{/if}

    <li id="displayview3"><a rel="nofollow" href="#" title="{l s='3 per row'}"><i class="icon-th"></i></a></li>
    <li id="displayview4"><a rel="nofollow" href="#" title="{l s='4 per row'}"><i class="icon-table"></i></a></li>
    {if $HOOK_LEFT_COLUMN =='' && $HOOK_RIGHT_COLUMN ==''}
	    <li id="displayview5"><a rel="nofollow" href="#" title="{l s='5 per row'}"><i class="icon_grid-3x3"></i></a></li>
	{/if}
    <li id="list"><a rel="nofollow" href="#" title="{l s='List'}"><i class="icon-th-list"></i></a></li>
    {* <li id="grid"><a rel="nofollow" href="#" title="{l s='grid'}"><i class="icon_th"></i></a></li> *}
</ul>

{/if}
