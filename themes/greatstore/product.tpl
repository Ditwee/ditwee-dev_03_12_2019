{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
	{if !isset($priceDisplayPrecision)}
		{assign var='priceDisplayPrecision' value=2}
	{/if}
	{if !$priceDisplay || $priceDisplay == 2}
		{assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL, 6)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
	{elseif $priceDisplay == 1}
		{assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL, 6)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
	{/if}

{* primary block column *}
{capture name='displayProductRightSidebar'}{hook h='displayProductRightSidebar' product=$product}{/capture}
{if $xprt.prod_page_style == 'prod_thumb_bottom'}
	{assign var="collg_left" value=5}
	{assign var="collg_center" value=7}

	{assign var="colmd_left" value=5}
	{assign var="colmd_center" value=7}

	{assign var="colsm_left" value=6}
	{assign var="colsm_center" value=6}
{elseif $xprt.prod_page_style == 'prod_thumb_bottom_sidebar' && $smarty.capture.displayProductRightSidebar}
	{assign var="collg_left" value=4}
	{assign var="collg_center" value=5}

	{assign var="colmd_left" value=5}
	{assign var="colmd_center" value=4}

	{assign var="colsm_left" value=6}
	{assign var="colsm_center" value=6}
{elseif $xprt.prod_page_style == 'prod_thumb_bottom_sidebar'}
	{assign var="collg_left" value=5}
	{assign var="collg_center" value=7}

	{assign var="colmd_left" value=5}
	{assign var="colmd_center" value=7}

	{assign var="colsm_left" value=6}
	{assign var="colsm_center" value=6}
{elseif $xprt.prod_page_style == 'prod_big_left_column'}
	{assign var="collg_left" value=8}
	{assign var="collg_center" value=4}

	{assign var="colmd_left" value=8}
	{assign var="colmd_center" value=4}

	{assign var="colsm_left" value=6}
	{assign var="colsm_center" value=6}
{elseif $xprt.prod_page_style == 'prod_full_left_column'}
	{assign var="collg_left" value=12}
	{assign var="collg_center" value=12}

	{assign var="colmd_left" value=12}
	{assign var="colmd_center" value=12}

	{assign var="colsm_left" value=12}
	{assign var="colsm_center" value=12}
{else}
	{assign var="collg_left" value=6}
	{assign var="collg_center" value=6}

	{assign var="colmd_left" value=7}
	{assign var="colmd_center" value=5}

	{assign var="colsm_left" value=7}
	{assign var="colsm_center" value=5}
{/if}



<div itemscope itemtype="https://schema.org/Product">
	<meta itemprop="url" content="{$link->getProductLink($product)}">
	<div class="primary_block row {$xprt.prod_page_style} {if $xprt.prod_page_style == 'prod_thumb_bottom_sidebar' && $smarty.capture.displayProductRightSidebar}prod_sidebar{/if} {if isset($xprt.prod_bigimage_layout)}{$xprt.prod_bigimage_layout}{/if}">
		{if !$content_only}
			<div class="container">
				<div class="top-hr"></div>
			</div>
		{/if}
		{if isset($adminActionDisplay) && $adminActionDisplay}
			<div id="admin-action" class="container">
				<p class="alert alert-info">{l s='This product is not visible to your customers.'}
					<input type="hidden" id="admin-action-product-id" value="{$product->id}" />
					<a id="publish_button" class="btn btn-default button button-small" href="#">
						<span>{l s='Publish'}</span>
					</a>
					<a id="lnk_view" class="btn btn-default button button-small" href="#">
						<span>{l s='Back'}</span>
					</a>
				</p>
				<p id="admin-action-result"></p>
			</div>
		{/if}
		{if isset($confirmation) && $confirmation}
			<p class="confirmation">
				{$confirmation}
			</p>
		{/if}
		<!-- left infos-->
		<div class="pb-left-column col-xs-12 col-sm-{$colsm_left} col-md-{$colmd_left} col-lg-{$collg_left} clearfix">
			<!-- product img-->
				{if isset($madein) && $madein  }
					<div class="item-madein-{$madein.shortname}" title="{l s="Made in "} {$madein.longname} "></div>
				{/if}

				
				{if $xprt.prod_bigimage_layout == 'prod_layout_ezoom_popup'}
					{include file="$tpl_dir./product/pb-left-column-ezoom-popup.tpl"}
				{elseif $xprt.prod_bigimage_layout == 'prod_layout_multi_image' && !$content_only}
					{include file="$tpl_dir./product/pb-left-column-multiple.tpl"}
				{elseif $xprt.prod_bigimage_layout == 'prod_layout_full_style' && !$content_only}
					{include file="$tpl_dir./product/pb-left-column-full.tpl"}
				{else}
					{include file="$tpl_dir./product/pb-left-column-ezoom-popup.tpl"}
				{/if}
		

		</div> <!-- end pb-left-column -->
		<!-- end left infos-->

		<!-- center infos -->
		<div class="pb-center-column col-xs-12 col-sm-{$colsm_center} col-md-{$colmd_center} col-lg-{$collg_center}">
			{if $product->online_only}
				<p class="online_only">{l s='Online only'}</p>
			{/if}
			{if $product->specificPrice && $product->specificPrice.reduction && $productPriceWithoutReduction > $productPrice}
				<p class="discount">{l s='Reduced price!'}</p>
			{/if}

			<h1 itemprop="name">{$product->name|escape:'html':'UTF-8'}</h1>

			{if !$content_only}
				<div class="prev_next_product">
					{hook h="displaynextprev" id_product=$product->id}
				</div>
			{/if}


			<!-- displayRightColumnProduct -->
			{if isset($HOOK_EXTRA_RIGHT) && $HOOK_EXTRA_RIGHT}{$HOOK_EXTRA_RIGHT}{/if}

			<div class="clearfix m_bottom_20"></div>

			<!-- temp right column content -->
			{if ($product->show_price && !isset($restricted_country_mode)) || isset($groups) || $product->reference || (isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS)}
			<!-- add to cart form-->
			<form id="buy_block"{if $PS_CATALOG_MODE && !isset($groups) && $product->quantity > 0} class="hidden"{/if} action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
				<!-- hidden datas -->
				<p class="hidden">
					<input type="hidden" name="token" value="{$static_token}" />
					<input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
					<input type="hidden" name="add" value="1" />
					<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
				</p>
				<div class="box-info-product">
					<div class="content_prices clearfix">
						{if $product->show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
							<!-- prices -->
							<div>
								<p class="our_price_display" itemprop="offers" itemscope itemtype="https://schema.org/Offer">{strip}
									{if $product->quantity > 0}<link itemprop="availability" href="https://schema.org/InStock"/>{/if}
									{if $priceDisplay >= 0 && $priceDisplay <= 2}
										<span id="our_price_display" class="price" itemprop="price" content="{$productPrice}">{convertPrice price=$productPrice|floatval}</span>
										{if $tax_enabled  && ((isset($display_tax_label) && $display_tax_label == 1) || !isset($display_tax_label))}
											{if $priceDisplay == 1} {l s='tax excl.'}{else} {l s='tax incl.'}{/if}
										{/if}
										<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
										{hook h="displayProductPriceBlock" product=$product type="price"}
									{/if}
								{/strip}</p>
								
								<p id="old_price"{if (!$product->specificPrice || !$product->specificPrice.reduction)} class="hidden"{/if}>{strip}
									{if $priceDisplay >= 0 && $priceDisplay <= 2}
										{hook h="displayProductPriceBlock" product=$product type="old_price"}
										<span id="old_price_display"><span class="price">{if $productPriceWithoutReduction > $productPrice}{convertPrice price=$productPriceWithoutReduction|floatval}{/if}</span>{if $productPriceWithoutReduction > $productPrice && $tax_enabled && $display_tax_label == 1} {if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}{/if}</span>
									{/if}
								{/strip}</p>

								<div class="clearfix"></div>

								<p id="reduction_percent" {if $productPriceWithoutReduction <= 0 || !$product->specificPrice || $product->specificPrice.reduction_type != 'percentage'} style="display:none;"{/if}>{strip}
									<span id="reduction_percent_display">
										{if $product->specificPrice && $product->specificPrice.reduction_type == 'percentage'}-{$product->specificPrice.reduction*100}%{/if}
									</span>
								{/strip}</p>

								<p id="reduction_amount" {if $productPriceWithoutReduction <= 0 || !$product->specificPrice || $product->specificPrice.reduction_type != 'amount' || $product->specificPrice.reduction|floatval ==0} style="display:none"{/if}>{strip}
									<span id="reduction_amount_display">
									{if $product->specificPrice && $product->specificPrice.reduction_type == 'amount' && $product->specificPrice.reduction|floatval !=0}
										-{convertPrice price=$productPriceWithoutReduction|floatval-$productPrice|floatval}
									{/if}
									</span>
								{/strip}</p>

								

								{if $priceDisplay == 2}
									<br />
									<span id="pretaxe_price">{strip}
										<span id="pretaxe_price_display">{convertPrice price=$product->getPrice(false, $smarty.const.NULL)}</span> {l s='tax excl.'}
									{/strip}</span>
								{/if}

							</div> <!-- end prices -->

							{if $packItems|@count && $productPrice < $product->getNoPackPrice()}
								<p class="pack_price">{l s='Instead of'} <span style="text-decoration: line-through;">{convertPrice price=$product->getNoPackPrice()}</span></p>
							{/if}
							{if $product->ecotax != 0}
								<p class="price-ecotax">{l s='Including'} <span id="ecotax_price_display">{if $priceDisplay == 2}{$ecotax_tax_exc|convertAndFormatPrice}{else}{$ecotax_tax_inc|convertAndFormatPrice}{/if}</span> {l s='for ecotax'}
									{if $product->specificPrice && $product->specificPrice.reduction}
									<br />{l s='(not impacted by the discount)'}
									{/if}
								</p>
							{/if}
							{if !empty($product->unity) && $product->unit_price_ratio > 0.000000}
								{math equation="pprice / punit_price" pprice=$productPrice  punit_price=$product->unit_price_ratio assign=unit_price}
								<p class="unit-price"><span id="unit_price_display">{convertPrice price=$unit_price}</span> {l s='per'} {$product->unity|escape:'html':'UTF-8'}</p>
								{hook h="displayProductPriceBlock" product=$product type="unit_price"}
							{/if}
						{/if} {*close if for show price*}
						{hook h="displayProductPriceBlock" product=$product type="weight" hook_origin='product_sheet'}
                        {hook h="displayProductPriceBlock" product=$product type="after_price"}
						<div class="clear m_bottom_30"></div>
					</div> <!-- end content_prices -->

			{/if} <!-- if close for temp right content -->



					<!-- availability or doesntExist -->
					<p id="availability_statut"{if !$PS_STOCK_MANAGEMENT || ($product->quantity <= 0 && !$product->available_later && $allow_oosp) || ($product->quantity > 0 && !$product->available_now) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
						<span id="availability_label">{l s='Availability: '}</span>
						<span id="availability_value" class="{if $product->quantity <= 0 && !$allow_oosp} text-danger{elseif $product->quantity <= 0} text-warning{else} text-success{/if}">{if $product->quantity <= 0}{if $PS_STOCK_MANAGEMENT && $allow_oosp}{$product->available_later}{else}{l s='This product is no longer in stock'}{/if}{elseif $PS_STOCK_MANAGEMENT}{$product->available_now}{/if}</span>
					</p>
					{if $PS_STOCK_MANAGEMENT}
						{if !$product->is_virtual}{hook h="displayProductDeliveryTime" product=$product}{/if}
						<p class="warning_inline" id="last_quantities"{if ($product->quantity > $last_qties || $product->quantity <= 0) || $allow_oosp || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none"{/if} >{l s='Warning: Last items in stock!'}</p>
					{/if}
					<p id="availability_date"{if ($product->quantity > 0) || !$product->available_for_order || $PS_CATALOG_MODE || !isset($product->available_date) || $product->available_date < $smarty.now|date_format:'%Y-%m-%d'} style="display: none;"{/if}>
						<span id="availability_date_label">{l s='Availability date:'}</span>
						<span id="availability_date_value">{if Validate::isDate($product->available_date)}{dateFormat date=$product->available_date full=false}{/if}</span>
					</p>
					<!-- Out of stock hook -->
					<div id="oosHook"{if $product->quantity > 0} style="display: none;"{/if}>
						{$HOOK_PRODUCT_OOS}
					</div>

					<!-- START product countdown -->
		          	{if !$PS_CATALOG_MODE}
			            {if isset($product->specificPrice) && isset($product->specificPrice.to) && isset($product->specificPrice.from)}
			            	{if ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $product->specificPrice.to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $product->specificPrice.from)}
							
							<div class="prod_countdown_area">
								<div class="prod_page_countdown" data-date="{$product->specificPrice.to|date_format} {$product->specificPrice.to|date_format:'%H:%M:%S'}"></div>
								{literal}
									<script type="text/javascript">
										$(function() {
											$('.prod_page_countdown').countdown({
												render: function(data) {
												$(this.el).html("<div class='prod_count_label'>{/literal}{l s='Expires:'}{literal}</div><div class='countdown_list days'><span class='countdown_digit'>" + this.leadingZeros(data.days, 3) + "</span><span class='countdown_label'>{/literal}{l s='days'}{literal}</span></div><div class='countdown_list hrs'><span class='countdown_digit'>" + this.leadingZeros(data.hours, 2) + "</span><span class='countdown_label'>{/literal}{l s='hrs'}{literal}</span></div><div class='countdown_list min'><span class='countdown_digit'>" + this.leadingZeros(data.min, 2) + "</span><span class='countdown_label'>{/literal}{l s='min'}{literal}</span></div><div class='countdown_list sec'><span class='countdown_digit'>" + this.leadingZeros(data.sec, 2) + "</span><span class='countdown_label'>{/literal}{l s='sec'}{literal}</span></div>");
												}
											});
										});
									</script>
								{/literal}
							
							</div>

							{/if}
						{/if}
					{/if}
					<!-- END product countdown -->


					<!-- product description -->
					{if $product->description_short || $packItems|@count > 0}
						<div id="short_description_block">
							{if $product->description_short}
								<div id="short_description_content" class="rte align_justify" itemprop="description">{$product->description_short}</div>
							{/if}

							{if $product->description}
								<p class="buttons_bottom_block">
									<a href="javascript:{ldelim}{rdelim}" class="button">
										{l s='More details'}
									</a>
								</p>
							{/if}
							<!--{if $packItems|@count > 0}
								<div class="short_description_pack">
								<h3>{l s='Pack content'}</h3>
									{foreach from=$packItems item=packItem}

									<div class="pack_content">
										{$packItem.pack_quantity} x <a href="{$link->getProductLink($packItem.id_product, $packItem.link_rewrite, $packItem.category)|escape:'html':'UTF-8'}">{$packItem.name|escape:'html':'UTF-8'}</a>
										<p>{$packItem.description_short}</p>
									</div>
									{/foreach}
								</div>
							{/if}-->
						</div> <!-- end short_description_block -->
					{/if}





			<!-- if start again temp right column content -->
			{if ($product->show_price && !isset($restricted_country_mode)) || isset($groups) || $product->reference || (isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS)}

					<div class="product_attributes clearfix">



						{if isset($groups)}
							<!-- attributes -->
							<div id="attributes">
								<div class="clearfix"></div>
								{foreach from=$groups key=id_attribute_group item=group}
									{if $group.attributes|@count}
										<fieldset class="attribute_fieldset">
											<label class="attribute_label" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$id_attribute_group|intval}"{/if}>{$group.name|escape:'html':'UTF-8'}:&nbsp;</label>
											{assign var="groupName" value="group_$id_attribute_group"}
											<div class="attribute_list group_style_{$group.group_type}">
												{if ($group.group_type == 'select')}
													<select name="{$groupName}" id="group_{$id_attribute_group|intval}" class="form-control attribute_select no-print">
														{foreach from=$group.attributes key=id_attribute item=group_attribute}
															<option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</option>
														{/foreach}
													</select>
												{elseif ($group.group_type == 'color')}
													<ul id="color_to_pick_list" class="color_to_pick_list clearfix">
														{assign var="default_colorpicker" value=""}
														{foreach from=$group.attributes key=id_attribute item=group_attribute}
															{assign var='img_color_exists' value=file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
															<li{if $group.default == $id_attribute} class="selected"{/if}>
																<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" id="color_{$id_attribute|intval}" {*name="{$colors.$id_attribute.name|escape:'html':'UTF-8'}"*} class="color_pick{if ($group.default == $id_attribute)} selected{/if}"{if !$img_color_exists && isset($colors.$id_attribute.value) && $colors.$id_attribute.value} style="background:{$colors.$id_attribute.value|escape:'html':'UTF-8'};"{/if} title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}">
																	{if $img_color_exists}
																		<img src="{$img_col_dir}{$id_attribute|intval}.jpg" alt="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" width="20" height="20" />
																	{/if}
																</a>
															</li>
															{if ($group.default == $id_attribute)}
																{$default_colorpicker = $id_attribute}
															{/if}
														{/foreach}
													</ul>
													<input type="hidden" class="color_pick_hidden" name="{$groupName|escape:'html':'UTF-8'}" value="{$default_colorpicker|intval}" />
												{elseif ($group.group_type == 'radio')}
													<ul class="clearfix group_style_radio">
														{foreach from=$group.attributes key=id_attribute item=group_attribute}
															<li>
																<div class="radio"><span>
																<input type="radio" class="attribute_radio" name="{$groupName|escape:'html':'UTF-8'}" value="{$id_attribute}" {if ($group.default == $id_attribute)} checked="checked"{/if} />
																</span></div>
																<span>{$group_attribute|escape:'html':'UTF-8'}</span>
															</li>
														{/foreach}
													</ul>
												{/if}
											</div> <!-- end attribute_list -->
										</fieldset>
									{/if}
								{/foreach}
							</div> <!-- end attributes -->
						{/if}
					</div> <!-- end product_attributes -->

					<div class="box-cart-bottom">
						<!-- minimal quantity wanted -->
						<p id="minimal_quantity_wanted_p"{if $product->minimal_quantity <= 1 || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
							{l s='The minimum purchase order quantity for the product is'} <b id="minimal_quantity_label">{$product->minimal_quantity}</b>
						</p>

						<!-- quantity wanted -->
						{if !$PS_CATALOG_MODE}
						<p id="quantity_wanted_p"{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
							<label for="quantity_wanted">{l s='Qty:'}</label>

							<a href="#" data-field-qty="qty" class="btn btn-default button-minus product_quantity_down">
								<span><i class="icon_minus-06"></i></span>
							</a>

							<input type="number" min="1" name="qty" id="quantity_wanted" class="text" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />

							<a href="#" data-field-qty="qty" class="btn btn-default button-plus product_quantity_up">
								<span><i class="icon_plus"></i></span>
							</a>
						</p>
						{/if}




						<div class="box_add_to_cart {if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE}unvisible{/if}">
							<p id="add_to_cart" class="no-print">
								<button type="submit" name="Submit" class="btn btn-default btn-inv exclusive">
									<span>{if $content_only && (isset($product->customization_required) && $product->customization_required)}{l s='Customize'}{else}{l s='Add to cart'}{/if}</span>
								</button>
							</p>
						</div>
						{if !$content_only}
							<!-- displayProductButtons -->
							{if isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS}{$HOOK_PRODUCT_ACTIONS}{/if}
						{/if}

					</div> <!-- end box-cart-bottom -->
				</div> <!-- end box-info-product -->
			</form>
			{/if}
			<!-- temp right column content end -->


			<div class="clearfix"></div>
			<p id="product_reference"{if empty($product->reference) || !$product->reference} style="display: none;"{/if}>
				<label>{l s='SKU: '} </label>
				<span class="editable" itemprop="sku"{if !empty($product->reference) && $product->reference} content="{$product->reference}"{/if}>{if !isset($groups)}{$product->reference|escape:'html':'UTF-8'}{/if}</span>
			</p>

			{if !$product->is_virtual && $product->condition}
			<p id="product_condition">
				<label>{l s='Condition:'} </label>
				{if $product->condition == 'new'}
					<link itemprop="itemCondition" href="https://schema.org/NewCondition"/>
					<span class="editable">{l s='New product'}</span>
				{elseif $product->condition == 'used'}
					<link itemprop="itemCondition" href="https://schema.org/UsedCondition"/>
					<span class="editable">{l s='Used'}</span>
				{elseif $product->condition == 'refurbished'}
					<link itemprop="itemCondition" href="https://schema.org/RefurbishedCondition"/>
					<span class="editable">{l s='Refurbished'}</span>
				{/if}
			</p>
			{/if}




			{if ($display_qties == 1 && !$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && $product->available_for_order)}
				<!-- number of item in stock -->
				<p id="pQuantityAvailable"{if $product->quantity <= 0} style="display: none;"{/if}>
					<label>{l s='Quantity: '} </label>
					<span id="quantityAvailable">{$product->quantity|intval}</span>
					<span {if $product->quantity > 1} style="display: none;"{/if} id="quantityAvailableTxt">{l s='Item'}</span>
					<span {if $product->quantity == 1} style="display: none;"{/if} id="quantityAvailableTxtMultiple">{l s='Items'}</span>
				</p>
			{/if}


			{if $product->manufacturer_name}
				<p class="prod_manufacture m_bottom_0">
					<label>{l s='Brand:'} </label>
					{$product->manufacturer_name}
				</p>
			{/if}

			{* {if $product->supplier_name}
				<p class="prod_manufacture">
					<label>{l s='Supplier:'} </label>
					{$product->supplier_name}
				</p>
			{/if} *}

			

			{* {if $product->additional_shipping_cost == 0}
				<p class="prod_shipping_cost">
					<label>{l s='Shipping cost:'} </label>
					<span>{l s='Free shipping'}</span>
				</p>
			{else}
				<p class="prod_shipping_cost">
					<label>{l s='Shipping cost:'} </label>
					{$product->additional_shipping_cost}
				</p>
			{/if} *}


			
			{if !$content_only2}
				<!-- usefull links-->
				<div id="usefull_link_block" class="clearfix no-print">
					<!-- displayLeftColumnProduct -->
					{if $HOOK_EXTRA_LEFT}{$HOOK_EXTRA_LEFT}{/if}
					<p class="print">
						<a href="javascript:print();">
							{l s='Print'}
						</a>
					</p>
				</div>
			{/if}
			

			<!-- displayCenterColumnProduct -->
			{capture name='displayCenterColumnProduct'}{hook h='displayCenterColumnProduct'}{/capture}
			{if $smarty.capture.displayCenterColumnProduct}
				<div class="displayCenterColumnProduct">
					{$smarty.capture.displayCenterColumnProduct}
				</div>
			{/if}

			{if $xprt.prod_tab_pos == 'center'}
				{include file="$tpl_dir./product/tab-classic.tpl"}
			{/if}

		</div>
		<!-- end center infos-->


		<!-- pb-right-column-->
		{if $smarty.capture.displayProductRightSidebar && $xprt.prod_page_style == 'prod_thumb_bottom_sidebar'}
			{if !$content_only}
				<div class="pb-right-column col-xs-12 hidden-sm col-md-3 sidebar">

					{$smarty.capture.displayProductRightSidebar}
					
					{if $xprt.prod_tab_pos == 'right'}
						{include file="$tpl_dir./product/tab-classic.tpl"}
					{/if}

				</div> <!-- end pb-right-column-->
			{/if}
		{/if}

	</div> <!-- end primary_block -->


	{if !$content_only}

		<!--HOOK_PRODUCT_TAB -->
		{if $xprt.prod_tab_pos == 'default'}
			{if $xprt.prod_tab_style == 'general'}

				{include file="$tpl_dir./product/tab-general.tpl"}

			{elseif $xprt.prod_tab_style == 'classic'}

				{include file="$tpl_dir./product/tab-classic.tpl"}

			{/if} 
		{/if} 
		
		<!--end HOOK_PRODUCT_TAB -->

		<!-- Start Product Accessories -->

		{* {include file="$tpl_dir./product/product_accessories.tpl"} *}

		<!-- End Product Accessories -->

		<!-- displayFooterProduct -->
		{if isset($HOOK_PRODUCT_FOOTER) && $HOOK_PRODUCT_FOOTER}{$HOOK_PRODUCT_FOOTER}{/if}

	{/if}

</div> <!-- itemscope product wrapper -->
{strip}
{if isset($smarty.get.ad) && $smarty.get.ad}
	{addJsDefL name=ad}{$base_dir|cat:$smarty.get.ad|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{if isset($smarty.get.adtoken) && $smarty.get.adtoken}
	{addJsDefL name=adtoken}{$smarty.get.adtoken|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{addJsDef allowBuyWhenOutOfStock=$allow_oosp|boolval}
{addJsDef availableNowValue=$product->available_now|escape:'quotes':'UTF-8'}
{addJsDef availableLaterValue=$product->available_later|escape:'quotes':'UTF-8'}
{addJsDef attribute_anchor_separator=$attribute_anchor_separator|escape:'quotes':'UTF-8'}
{addJsDef attributesCombinations=$attributesCombinations}
{addJsDef currentDate=$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}
{if isset($combinations) && $combinations}
	{addJsDef combinations=$combinations}
	{addJsDef combinationsFromController=$combinations}
	{addJsDef displayDiscountPrice=$display_discount_price}
	{addJsDefL name='upToTxt'}{l s='Up to' js=1}{/addJsDefL}
{/if}
{if isset($combinationImages) && $combinationImages}
	{addJsDef combinationImages=$combinationImages}
{/if}
{addJsDef customizationId=$id_customization}
{addJsDef customizationFields=$customizationFields}
{addJsDef default_eco_tax=$product->ecotax|floatval}
{addJsDef displayPrice=$priceDisplay|intval}
{addJsDef ecotaxTax_rate=$ecotaxTax_rate|floatval}
{if isset($cover.id_image_only)}
	{addJsDef idDefaultImage=$cover.id_image_only|intval}
{else}
	{addJsDef idDefaultImage=0}
{/if}
{addJsDef img_ps_dir=$img_ps_dir}
{addJsDef img_prod_dir=$img_prod_dir}
{addJsDef id_product=$product->id|intval}
{addJsDef jqZoomEnabled=$jqZoomEnabled|boolval}
{addJsDef maxQuantityToAllowDisplayOfLastQuantityMessage=$last_qties|intval}
{addJsDef minimalQuantity=$product->minimal_quantity|intval}
{addJsDef noTaxForThisProduct=$no_tax|boolval}
{if isset($customer_group_without_tax)}
	{addJsDef customerGroupWithoutTax=$customer_group_without_tax|boolval}
{else}
	{addJsDef customerGroupWithoutTax=false}
{/if}
{if isset($group_reduction)}
	{addJsDef groupReduction=$group_reduction|floatval}
{else}
	{addJsDef groupReduction=false}
{/if}
{addJsDef oosHookJsCodeFunctions=Array()}
{addJsDef productHasAttributes=isset($groups)|boolval}
{addJsDef productPriceTaxExcluded=($product->getPriceWithoutReduct(true)|default:'null' - $product->ecotax)|floatval}
{addJsDef productPriceTaxIncluded=($product->getPriceWithoutReduct(false)|default:'null' - $product->ecotax * (1 + $ecotaxTax_rate / 100))|floatval}
{addJsDef productBasePriceTaxExcluded=($product->getPrice(false, null, 6, null, false, false) - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcl=($product->getPrice(false, null, 6, null, false, false)|floatval)}
{addJsDef productBasePriceTaxIncl=($product->getPrice(true, null, 6, null, false, false)|floatval)}
{addJsDef productReference=$product->reference|escape:'html':'UTF-8'}
{addJsDef productAvailableForOrder=$product->available_for_order|boolval}
{addJsDef productPriceWithoutReduction=$productPriceWithoutReduction|floatval}
{addJsDef productPrice=$productPrice|floatval}
{addJsDef productUnitPriceRatio=$product->unit_price_ratio|floatval}
{addJsDef productShowPrice=(!$PS_CATALOG_MODE && $product->show_price)|boolval}
{addJsDef PS_CATALOG_MODE=$PS_CATALOG_MODE}
{if $product->specificPrice && $product->specificPrice|@count}
	{addJsDef product_specific_price=$product->specificPrice}
{else}
	{addJsDef product_specific_price=array()}
{/if}
{if $display_qties == 1 && $product->quantity}
	{addJsDef quantityAvailable=$product->quantity}
{else}
	{addJsDef quantityAvailable=0}
{/if}
{addJsDef quantitiesDisplayAllowed=$display_qties|boolval}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'percentage'}
	{addJsDef reduction_percent=$product->specificPrice.reduction*100|floatval}
{else}
	{addJsDef reduction_percent=0}
{/if}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'amount'}
	{addJsDef reduction_price=$product->specificPrice.reduction|floatval}
{else}
	{addJsDef reduction_price=0}
{/if}
{if $product->specificPrice && $product->specificPrice.price}
	{addJsDef specific_price=$product->specificPrice.price|floatval}
{else}
	{addJsDef specific_price=0}
{/if}
{addJsDef specific_currency=($product->specificPrice && $product->specificPrice.id_currency)|boolval} {* TODO: remove if always false *}
{addJsDef stock_management=$PS_STOCK_MANAGEMENT|intval}
{addJsDef taxRate=$tax_rate|floatval}
{addJsDefL name=doesntExist}{l s='This combination does not exist for this product. Please select another combination.' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMore}{l s='This product is no longer in stock' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMoreBut}{l s='with those attributes but is available with others.' js=1}{/addJsDefL}
{addJsDefL name=fieldRequired}{l s='Please fill in all the required fields before saving your customization.' js=1}{/addJsDefL}
{addJsDefL name=uploading_in_progress}{l s='Uploading in progress, please be patient.' js=1}{/addJsDefL}
{addJsDefL name='product_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
{addJsDefL name='product_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}
{/strip}
{/if}
