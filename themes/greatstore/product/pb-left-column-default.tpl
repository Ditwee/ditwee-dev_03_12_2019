<div id="image-block" class="clearfix">
	{if $product->new}
		<span class="new-box">
			<span class="new-label">{l s='New'}</span>
		</span>
	{/if}

	{if $product->on_sale}
		<span class="sale-box no-print">
			<span class="sale-label">{l s='Sale!'}</span>
		</span>
	{/if}

	{if $have_image}
		<span id="view_full_size">
				<ul class="clearfix">
				{if isset($images)}
					{$i=0}
					{foreach from=$images item=image name=thumbnails}
						{assign var=imageIds value="`$product->id`-`$image.id_image`"}
						{if !empty($image.legend)}
							{assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
						{else}
							{assign var=imageTitle value=$product->name|escape:'html':'UTF-8'}
						{/if}
						<li id="xprtimageblc_{$image.id_image}">
							<a class="jqzoom" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}">
							<img id="bigpic_{$i}" itemprop="image" class="img-responsive" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}"{if isset($largeSize)} height="{$largeSize.height}" width="{$largeSize.width}"{/if} itemprop="image" />
							</a>
						</li>
						{$i= $i+1}
					{/foreach}
				{/if}
				</ul>
				{if !$content_only}
					<span class="span_link no-print">{l s='View larger'}</span>
				{/if}
		</span>
	{else}
		<span id="view_full_size">
			<img itemprop="image" src="{$img_prod_dir}{$lang_iso}-default-large_default.jpg" id="bigpic" alt="" title="{$product->name|escape:'html':'UTF-8'}" width="{$largeSize.width}" height="{$largeSize.height}"/>
			{if !$content_only}
				<span class="span_link">
					{l s='View larger'}
				</span>
			{/if}
		</span>
	{/if}
</div> <!-- end image-block -->

{if isset($images) && count($images) > 0}
	<!-- thumbnails -->
	<div id="views_block" class="clearfix {if isset($images) && count($images) < 2}hidden{/if}">
		{if isset($images) && count($images) > 2}

			<a class="view_scroll_spacer" id="view_scroll_left" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
				{l s='Previous'}
			</a>

		{/if}

		<div id="thumbs_list">
			<ul id="thumbs_list_frame">
			{if isset($images)}
				{foreach from=$images item=image name=thumbnails}
					{assign var=imageIds value="`$product->id`-`$image.id_image`"}
					{if !empty($image.legend)}
						{assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
					{else}
						{assign var=imageTitle value=$product->name|escape:'html':'UTF-8'}
					{/if}
					<li id="thumbnail_{$image.id_image}"{if $smarty.foreach.thumbnails.last} class="last"{/if}>

						<a{if $jqZoomEnabled && $have_image && !$content_only} href="javascript:void(0);" rel="{literal}{{/literal}gallery: 'gal1', smallimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}',largeimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}'{literal}}{/literal}"{else} href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}"	data-fancybox-group="other-views" class="nofancybox{if $image.id_image == $cover.id_image} shown{/if}"{/if} title="{$imageTitle}">
							<img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'small_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}"{if isset($smallSize)} height="{$smallSize.height}" width="{$smallSize.width}"{/if} itemprop="image" />
						</a>
					</li>
				{/foreach}
			{/if}
			</ul>
		</div> <!-- end thumbs_list -->
		{if isset($images) && count($images) > 2}
			<a id="view_scroll_right" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
				{l s='Next'}
			</a>
		{/if}
	</div> <!-- end views-block -->
	<!-- end thumbnails -->
{/if}
{if isset($images) && count($images) > 1}
	<p class="resetimg clear no-print">
		<span id="wrapResetImages" style="display: none;">
			<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" data-id="resetImages">
				<i class="icon-repeat"></i>
				{l s='Display all pictures'}
			</a>
		</span>
	</p>
{/if}