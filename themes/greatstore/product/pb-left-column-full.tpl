<div id="image-block" class="clearfix">
	{if $product->new}
		<span class="new-box">
			<span class="new-label">{l s='New'}</span>
		</span>
	{/if}
	{if $product->on_sale}
		<span class="sale-box no-print">
			<span class="sale-label">{l s='Sale!'}</span>
		</span>
	{/if}
	{if $have_image}
		<span id="view_full_size" class="prod_bigimage_slider_full">
			<ul class="clearfix">
				{if isset($images)}
					{foreach from=$images item=image name=thumbnails}
						{assign var=imageIds value="`$product->id`-`$image.id_image`"}
						{if !empty($image.legend)}
							{assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
						{else}
							{assign var=imageTitle value=$product->name|escape:'html':'UTF-8'}
						{/if}
						<li id="xprtimageblc_{$image.id_image}" style="padding:0px 1px;">
								<a title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}" data-fancybox-group="other-views" class="fancybox{if $image.id_image == $cover.id_image} shown{/if}">
									<img itemprop="image" class="img-responsive" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}"{if isset($largeSize)} height="{$largeSize.height}" width="{$largeSize.width}"{/if} itemprop="image" />
								</a>
						</li>
					{/foreach}
				{/if}
			</ul>
		</span>
	{else}
		<span id="view_full_size" class="prod_bigimage_slider1 prod_bigimage_multiple">
			<img itemprop="image" src="{$img_prod_dir}{$lang_iso}-default-large_default.jpg" id="bigpic" alt="" title="{$product->name|escape:'html':'UTF-8'}" width="{$largeSize.width}" height="{$largeSize.height}"/>
			{if !$content_only}
				<span class="span_link">
					{l s='View larger'}
				</span>
			{/if}
		</span>
	{/if}
</div> <!-- end image-block -->
{if isset($images) && count($images) > 0}
	<!-- thumbnails -->
	<div id="views_block" class="clearfix {if isset($images) && count($images) < 2}hidden{/if}">
		{if isset($images) && count($images) > 2}
			<a class="view_scroll_spacer" id="view_scroll_left" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
				{* {l s='Previous'} *}
				<i class="arrow-double-left"></i>
			</a>
		{/if}
		<div id="thumbs_list">
			<ul id="thumbs_list_frame" class="prod_bigimage_slider_full">
			{if isset($images)}
				{foreach from=$images item=image name=thumbnails}
					{assign var=imageIds value="`$product->id`-`$image.id_image`"}
					{if !empty($image.legend)}
						{assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
					{else}
						{assign var=imageTitle value=$product->name|escape:'html':'UTF-8'}
					{/if}
					<li id="thumbnail_{$image.id_image}"{if $smarty.foreach.thumbnails.last} class="last"{/if}>
						<img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'medium_thumb')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}"{if isset($smallSize)} height="{$smallSize.height}" width="{$smallSize.width}"{/if} itemprop="image" />
					</li>
				{/foreach}
			{/if}
			</ul>
		</div> <!-- end thumbs_list -->
		{if isset($images) && count($images) > 2}
			<a id="view_scroll_right" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
				{* {l s='Next'} *}
				<i class="arrow-double-right"></i>
			</a>
		{/if}
	</div> <!-- end views-block -->
	<!-- end thumbnails -->
{/if}
{if isset($images) && count($images) > 1}
	<p class="resetimg clear no-print">
		<span id="wrapResetImages" style="display: none;">
			<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" data-id="resetImages">
				<i class="icon-repeat"></i>
				{l s='Display all pictures'}
			</a>
		</span>
	</p>
{/if}