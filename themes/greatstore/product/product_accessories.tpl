<!-- Start Accessories -->
{if isset($accessories) && $accessories}
	<section id="accessories" class="page-product-box">
		<h3 class="page-product-heading">
			{l s='Accessories'}
			<div class="heading_carousel_arrow"></div>
		</h3>
			<div class=" accessories-block kr_products_block">
				<ul class="product_list grid row  {if $xprt.home_product_style == 'carousel'}carousel{/if}  clearfix {if $xprt.gray_image_bg == 1}gray_image_bg{/if}">
					{foreach from=$accessories item=accessory name=accessories_list}
						{if ($accessory.allow_oosp || $accessory.quantity_all_versions > 0 || $accessory.quantity > 0) && $accessory.available_for_order && !isset($restricted_country_mode)}
							{assign var='accessoryLink' value=$link->getProductLink($accessory.id_product, $accessory.link_rewrite, $accessory.category)}
							<li class="ajax_block_product col-sm-3 {if $smarty.foreach.accessories_list.first} first_item{elseif $smarty.foreach.accessories_list.last} last_item{else} item{/if}">
								<div class="product-container">
									<div class="left-block">
										<div class="product-image-container">
											<a class="product_img_link" href="{$accessoryLink|escape:'html':'UTF-8'}" title="{$accessory.legend|escape:'html':'UTF-8'}">
												<img class="img-responsive" src="{$link->getImageLink($accessory.link_rewrite, $accessory.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$accessory.legend|escape:'html':'UTF-8'}"/>
											</a>
										</div>
									</div>
									<div class="right-block">
										<h5 itemprop="name">
											<a class="product-name" href="{$accessoryLink|escape:'html':'UTF-8'}">
												{$accessory.name|truncate:45:'...':true|escape:'html':'UTF-8'}
											</a>
										</h5>
										{if $accessory.show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
											<div class="content_price">
												<span class="price">
													{if $priceDisplay != 1}
													{displayWtPrice p=$accessory.price}{else}{displayWtPrice p=$accessory.price_tax_exc}
													{/if}
												</span>
											</div>
										{/if}
									</div>
								</div>
							</li>
						{/if}
					{/foreach}
				</ul>
			</div>
	</section>
{/if}
<!--end Accessories -->
