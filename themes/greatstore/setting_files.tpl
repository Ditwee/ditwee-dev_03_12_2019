
{assign var='xprtshortname' value="{Configuration::get('xprtshortname')}"}
	


{if $page_name =='index'}
	{if Configuration::get("{$xprtshortname}per_line_home") == 2}
        {strip}
        	{addJsDef prd_per_column=2}
        	{addJsDef prd_per_column_lg=6}
	        {addJsDef prd_per_column_md=6}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
    {elseif Configuration::get("{$xprtshortname}per_line_home") == 3}
    	{strip}
    		{addJsDef prd_per_column=3}
        	{addJsDef prd_per_column_lg=4}
	        {addJsDef prd_per_column_md=4}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
    {elseif Configuration::get("{$xprtshortname}per_line_home") == 4}
    	{strip}
    		{addJsDef prd_per_column=4}
        	{addJsDef prd_per_column_lg=3}
	        {addJsDef prd_per_column_md=4}
	        {addJsDef prd_per_column_sm=3}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
	        
	{elseif Configuration::get("{$xprtshortname}per_line_home") == 6}
    	{strip}
    		{addJsDef prd_per_column=6}
        	{addJsDef prd_per_column_lg=2}
	        {addJsDef prd_per_column_md=3}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
	        
    {else}
    	{strip}
    		{addJsDef prd_per_column=4}
        	{addJsDef prd_per_column_lg=3}
	        {addJsDef prd_per_column_md=3}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=12}
        {/strip}
    {/if}
{elseif $page_name =='product'}
	{if Configuration::get("{$xprtshortname}per_line_product") == 2}
    	{strip}
    		{addJsDef prd_per_column=2}
        	{addJsDef prd_per_column_lg=6}
	        {addJsDef prd_per_column_md=6}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
	        
    {elseif Configuration::get("{$xprtshortname}per_line_product") == 3}
    	{strip}
    		{addJsDef prd_per_column=3}
        	{addJsDef prd_per_column_lg=4}
	        {addJsDef prd_per_column_md=6}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
	        
    {elseif Configuration::get("{$xprtshortname}per_line_product") == 4}
    	{strip}
    		{addJsDef prd_per_column=4}
        	{addJsDef prd_per_column_lg=3}
	        {addJsDef prd_per_column_md=4}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
	       
	{elseif Configuration::get("{$xprtshortname}per_line_product") == 6}
    	{strip}
    		{addJsDef prd_per_column=6}
        	{addJsDef prd_per_column_lg=2}
	        {addJsDef prd_per_column_md=3}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
    {else}
    	{strip}
    		{addJsDef prd_per_column=4}
        	{addJsDef prd_per_column_lg=3}
	        {addJsDef prd_per_column_md=4}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
    {/if}
{elseif $page_name =='category'}
	{if Configuration::get("{$xprtshortname}per_line_category") == 2}
    	{strip}
        	{addJsDef prd_per_column=2}
        	{addJsDef prd_per_column_lg=6}
	        {addJsDef prd_per_column_md=6}
	        {addJsDef prd_per_column_sm=12}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
	        
    {elseif Configuration::get("{$xprtshortname}per_line_category") == 3}
    	{strip}
    		{addJsDef prd_per_column=3}
        	{addJsDef prd_per_column_lg=4}
	        {addJsDef prd_per_column_md=4}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
    {elseif Configuration::get("{$xprtshortname}per_line_category") == 4}
    	{strip}
    		{addJsDef prd_per_column=4}
        	{addJsDef prd_per_column_lg=3}
	        {addJsDef prd_per_column_md=4}
	        {addJsDef prd_per_column_sm=4}
	        {addJsDef prd_per_column_xs=6}
        {/strip}		       
	{elseif Configuration::get("{$xprtshortname}per_line_category") == 6}
    	{strip}
    		{addJsDef prd_per_column=6}
        	{addJsDef prd_per_column_lg=2}
	        {addJsDef prd_per_column_md=3}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
	       
    {else}
    	{strip}
    		{addJsDef prd_per_column=4}
        	{addJsDef prd_per_column_lg=3}
	        {addJsDef prd_per_column_md=3}
	        {addJsDef prd_per_column_sm=12}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
    {/if}
{else}
	{if Configuration::get("{$xprtshortname}per_line_othr") == 2}
    	{strip}
    		{addJsDef prd_per_column=2}
        	{addJsDef prd_per_column_lg=6}
	        {addJsDef prd_per_column_md=6}
	        {addJsDef prd_per_column_sm=12}
	        {addJsDef prd_per_column_xs=6}
        {/strip} 
    {elseif Configuration::get("{$xprtshortname}per_line_othr") == 3}
    	{strip}
    		{addJsDef prd_per_column=3}
        	{addJsDef prd_per_column_lg=4}
	        {addJsDef prd_per_column_md=6}
	        {addJsDef prd_per_column_sm=12}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
    {elseif Configuration::get("{$xprtshortname}per_line_othr") == 4}
    	{strip}
    		{addJsDef prd_per_column=4}
        	{addJsDef prd_per_column_lg=3}
	        {addJsDef prd_per_column_md=4}
	        {addJsDef prd_per_column_sm=6}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
	{elseif Configuration::get("{$xprtshortname}per_line_othr") == 6}
    	{strip}
    		{addJsDef prd_per_column=6}
        	{addJsDef prd_per_column_lg=2}
	        {addJsDef prd_per_column_md=3}
	        {addJsDef prd_per_column_sm=12}
	        {addJsDef prd_per_column_xs=6}
        {/strip} 
    {else}
    	{strip}
    		{addJsDef prd_per_column=4}
        	{addJsDef prd_per_column_lg=3}
	        {addJsDef prd_per_column_md=4}
	        {addJsDef prd_per_column_sm=12}
	        {addJsDef prd_per_column_xs=6}
        {/strip}
    {/if}
{/if}


{addJsDef homeTabPerLine=$xprt.per_line_hometab|intval}
{addJsDef homeTabPerRows=$xprt.per_rows_hometab|intval}
{addJsDef homeTabProductScroll=$xprt.hometab_product_scroll|strval}
{addJsDef homeTabAutoplay=$xprt.hometab_autoplay|boolval}
{addJsDef homeTabNavArrows=$xprt.hometab_nav_arrows|boolval}
{addJsDef homeTabNavDots=$xprt.hometab_nav_dots|boolval}


{if $xprt.prod_page_style == 'prod_thumb_bottom' || $xprt.prod_page_style == 'prod_thumb_bottom_sidebar'}
	{assign var="thumb_list_bottom" value=thumb_list_bottom}
{else}
	{assign var="thumb_list_bottom" value=thumb_list_other}
{/if}

{strip}
	{addJsDefL name=min_item}{l s='Please select at least one product' js=1}{/addJsDefL}
	{addJsDefL name=max_item}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1}{/addJsDefL}

	{addJsDef sidebarpanel=$xprt.page_header_style}

	
	{addJsDef comparator_max_item=$comparator_max_item}
	{addJsDef comparedProductsIds=$compared_products}

	{addJsDef catLayredAccordion=$xprt.cat_layred_accordion}

	{addJsDef prodPageStyle=$thumb_list_bottom}
	{addJsDef prodBigImageStyle=$xprt.prod_bigimage_style}
	{addJsDef prodBigImageZoomStyle=$xprt.prod_bigimage_zoom_style}
	{addJsDef prod_bigimage_layout=$xprt.prod_bigimage_layout}

{/strip}
